﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Security;
using System.Security.Permissions;
using System.Threading;

namespace System.Collections
{
	/// <summary>Implements the <see cref="T:System.Collections.IList" /> interface using an array whose size is dynamically increased as required.To browse the .NET Framework source code for this type, see the Reference Source.</summary>
	// Token: 0x02000991 RID: 2449
	[ComVisible(true)]
	[DebuggerTypeProxy(typeof(ArrayList.ArrayListDebugView))]
	[DebuggerDisplay("Count = {Count}")]
	[Serializable]
	public class ArrayList : IList, ICollection, IEnumerable, ICloneable
	{
		// Token: 0x0600579D RID: 22429 RVA: 0x00002050 File Offset: 0x00000250
		internal ArrayList(bool trash)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Collections.ArrayList" /> class that is empty and has the default initial capacity.</summary>
		// Token: 0x0600579E RID: 22430 RVA: 0x0011CB88 File Offset: 0x0011AD88
		public ArrayList()
		{
			this._items = ArrayList.emptyArray;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Collections.ArrayList" /> class that is empty and has the specified initial capacity.</summary>
		/// <param name="capacity">The number of elements that the new list can initially store. </param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="capacity" /> is less than zero. </exception>
		// Token: 0x0600579F RID: 22431 RVA: 0x0011CB9C File Offset: 0x0011AD9C
		public ArrayList(int capacity)
		{
			if (capacity < 0)
			{
				throw new ArgumentOutOfRangeException("capacity", Environment.GetResourceString("'{0}' must be non-negative.", new object[]
				{
					"capacity"
				}));
			}
			if (capacity == 0)
			{
				this._items = ArrayList.emptyArray;
				return;
			}
			this._items = new object[capacity];
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Collections.ArrayList" /> class that contains elements copied from the specified collection and that has the same initial capacity as the number of elements copied.</summary>
		/// <param name="c">The <see cref="T:System.Collections.ICollection" /> whose elements are copied to the new list. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="c" /> is <see langword="null" />. </exception>
		// Token: 0x060057A0 RID: 22432 RVA: 0x0011CBF4 File Offset: 0x0011ADF4
		public ArrayList(ICollection c)
		{
			if (c == null)
			{
				throw new ArgumentNullException("c", Environment.GetResourceString("Collection cannot be null."));
			}
			int count = c.Count;
			if (count == 0)
			{
				this._items = ArrayList.emptyArray;
				return;
			}
			this._items = new object[count];
			this.AddRange(c);
		}

		/// <summary>Gets or sets the number of elements that the <see cref="T:System.Collections.ArrayList" /> can contain.</summary>
		/// <returns>The number of elements that the <see cref="T:System.Collections.ArrayList" /> can contain.</returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <see cref="P:System.Collections.ArrayList.Capacity" /> is set to a value that is less than <see cref="P:System.Collections.ArrayList.Count" />.</exception>
		/// <exception cref="T:System.OutOfMemoryException">There is not enough memory available on the system.</exception>
		// Token: 0x17000F59 RID: 3929
		// (get) Token: 0x060057A1 RID: 22433 RVA: 0x0011CC48 File Offset: 0x0011AE48
		// (set) Token: 0x060057A2 RID: 22434 RVA: 0x0011CC54 File Offset: 0x0011AE54
		public virtual int Capacity
		{
			get
			{
				return this._items.Length;
			}
			set
			{
				if (value < this._size)
				{
					throw new ArgumentOutOfRangeException("value", Environment.GetResourceString("capacity was less than the current size."));
				}
				if (value != this._items.Length)
				{
					if (value > 0)
					{
						object[] array = new object[value];
						if (this._size > 0)
						{
							Array.Copy(this._items, 0, array, 0, this._size);
						}
						this._items = array;
						return;
					}
					this._items = new object[4];
				}
			}
		}

		/// <summary>Gets the number of elements actually contained in the <see cref="T:System.Collections.ArrayList" />.</summary>
		/// <returns>The number of elements actually contained in the <see cref="T:System.Collections.ArrayList" />.</returns>
		// Token: 0x17000F5A RID: 3930
		// (get) Token: 0x060057A3 RID: 22435 RVA: 0x0011CCC6 File Offset: 0x0011AEC6
		public virtual int Count
		{
			get
			{
				return this._size;
			}
		}

		/// <summary>Gets a value indicating whether the <see cref="T:System.Collections.ArrayList" /> has a fixed size.</summary>
		/// <returns>
		///     <see langword="true" /> if the <see cref="T:System.Collections.ArrayList" /> has a fixed size; otherwise, <see langword="false" />. The default is <see langword="false" />.</returns>
		// Token: 0x17000F5B RID: 3931
		// (get) Token: 0x060057A4 RID: 22436 RVA: 0x00002526 File Offset: 0x00000726
		public virtual bool IsFixedSize
		{
			get
			{
				return false;
			}
		}

		/// <summary>Gets a value indicating whether the <see cref="T:System.Collections.ArrayList" /> is read-only.</summary>
		/// <returns>
		///     <see langword="true" /> if the <see cref="T:System.Collections.ArrayList" /> is read-only; otherwise, <see langword="false" />. The default is <see langword="false" />.</returns>
		// Token: 0x17000F5C RID: 3932
		// (get) Token: 0x060057A5 RID: 22437 RVA: 0x00002526 File Offset: 0x00000726
		public virtual bool IsReadOnly
		{
			get
			{
				return false;
			}
		}

		/// <summary>Gets a value indicating whether access to the <see cref="T:System.Collections.ArrayList" /> is synchronized (thread safe).</summary>
		/// <returns>
		///     <see langword="true" /> if access to the <see cref="T:System.Collections.ArrayList" /> is synchronized (thread safe); otherwise, <see langword="false" />. The default is <see langword="false" />.</returns>
		// Token: 0x17000F5D RID: 3933
		// (get) Token: 0x060057A6 RID: 22438 RVA: 0x00002526 File Offset: 0x00000726
		public virtual bool IsSynchronized
		{
			get
			{
				return false;
			}
		}

		/// <summary>Gets an object that can be used to synchronize access to the <see cref="T:System.Collections.ArrayList" />.</summary>
		/// <returns>An object that can be used to synchronize access to the <see cref="T:System.Collections.ArrayList" />.</returns>
		// Token: 0x17000F5E RID: 3934
		// (get) Token: 0x060057A7 RID: 22439 RVA: 0x0011CCCE File Offset: 0x0011AECE
		public virtual object SyncRoot
		{
			get
			{
				if (this._syncRoot == null)
				{
					Interlocked.CompareExchange<object>(ref this._syncRoot, new object(), null);
				}
				return this._syncRoot;
			}
		}

		/// <summary>Gets or sets the element at the specified index.</summary>
		/// <param name="index">The zero-based index of the element to get or set. </param>
		/// <returns>The element at the specified index.</returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="index" /> is less than zero.-or- 
		///         <paramref name="index" /> is equal to or greater than <see cref="P:System.Collections.ArrayList.Count" />. </exception>
		// Token: 0x17000F5F RID: 3935
		public virtual object this[int index]
		{
			get
			{
				if (index < 0 || index >= this._size)
				{
					throw new ArgumentOutOfRangeException("index", Environment.GetResourceString("Index was out of range. Must be non-negative and less than the size of the collection."));
				}
				return this._items[index];
			}
			set
			{
				if (index < 0 || index >= this._size)
				{
					throw new ArgumentOutOfRangeException("index", Environment.GetResourceString("Index was out of range. Must be non-negative and less than the size of the collection."));
				}
				this._items[index] = value;
				this._version++;
			}
		}

		/// <summary>Creates an <see cref="T:System.Collections.ArrayList" /> wrapper for a specific <see cref="T:System.Collections.IList" />.</summary>
		/// <param name="list">The <see cref="T:System.Collections.IList" /> to wrap.</param>
		/// <returns>The <see cref="T:System.Collections.ArrayList" /> wrapper around the <see cref="T:System.Collections.IList" />.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="list" /> is <see langword="null" />.</exception>
		// Token: 0x060057AA RID: 22442 RVA: 0x0011CD57 File Offset: 0x0011AF57
		public static ArrayList Adapter(IList list)
		{
			if (list == null)
			{
				throw new ArgumentNullException("list");
			}
			return new ArrayList.IListWrapper(list);
		}

		/// <summary>Adds an object to the end of the <see cref="T:System.Collections.ArrayList" />.</summary>
		/// <param name="value">The <see cref="T:System.Object" /> to be added to the end of the <see cref="T:System.Collections.ArrayList" />. The value can be <see langword="null" />. </param>
		/// <returns>The <see cref="T:System.Collections.ArrayList" /> index at which the <paramref name="value" /> has been added.</returns>
		/// <exception cref="T:System.NotSupportedException">The <see cref="T:System.Collections.ArrayList" /> is read-only.-or- The <see cref="T:System.Collections.ArrayList" /> has a fixed size. </exception>
		// Token: 0x060057AB RID: 22443 RVA: 0x0011CD70 File Offset: 0x0011AF70
		public virtual int Add(object value)
		{
			if (this._size == this._items.Length)
			{
				this.EnsureCapacity(this._size + 1);
			}
			this._items[this._size] = value;
			this._version++;
			int size = this._size;
			this._size = size + 1;
			return size;
		}

		/// <summary>Adds the elements of an <see cref="T:System.Collections.ICollection" /> to the end of the <see cref="T:System.Collections.ArrayList" />.</summary>
		/// <param name="c">The <see cref="T:System.Collections.ICollection" /> whose elements should be added to the end of the <see cref="T:System.Collections.ArrayList" />. The collection itself cannot be <see langword="null" />, but it can contain elements that are <see langword="null" />. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="c" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.NotSupportedException">The <see cref="T:System.Collections.ArrayList" /> is read-only.-or- The <see cref="T:System.Collections.ArrayList" /> has a fixed size. </exception>
		// Token: 0x060057AC RID: 22444 RVA: 0x0011CDC8 File Offset: 0x0011AFC8
		public virtual void AddRange(ICollection c)
		{
			this.InsertRange(this._size, c);
		}

		/// <summary>Searches a range of elements in the sorted <see cref="T:System.Collections.ArrayList" /> for an element using the specified comparer and returns the zero-based index of the element.</summary>
		/// <param name="index">The zero-based starting index of the range to search. </param>
		/// <param name="count">The length of the range to search. </param>
		/// <param name="value">The <see cref="T:System.Object" /> to locate. The value can be <see langword="null" />. </param>
		/// <param name="comparer">The <see cref="T:System.Collections.IComparer" /> implementation to use when comparing elements.-or- 
		///       <see langword="null" /> to use the default comparer that is the <see cref="T:System.IComparable" /> implementation of each element. </param>
		/// <returns>The zero-based index of <paramref name="value" /> in the sorted <see cref="T:System.Collections.ArrayList" />, if <paramref name="value" /> is found; otherwise, a negative number, which is the bitwise complement of the index of the next element that is larger than <paramref name="value" /> or, if there is no larger element, the bitwise complement of <see cref="P:System.Collections.ArrayList.Count" />.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="index" /> and <paramref name="count" /> do not denote a valid range in the <see cref="T:System.Collections.ArrayList" />.-or- 
		///         <paramref name="comparer" /> is <see langword="null" /> and neither <paramref name="value" /> nor the elements of <see cref="T:System.Collections.ArrayList" /> implement the <see cref="T:System.IComparable" /> interface. </exception>
		/// <exception cref="T:System.InvalidOperationException">
		///         <paramref name="comparer" /> is <see langword="null" /> and <paramref name="value" /> is not of the same type as the elements of the <see cref="T:System.Collections.ArrayList" />. </exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="index" /> is less than zero.-or- 
		///         <paramref name="count" /> is less than zero. </exception>
		// Token: 0x060057AD RID: 22445 RVA: 0x0011CDD8 File Offset: 0x0011AFD8
		public virtual int BinarySearch(int index, int count, object value, IComparer comparer)
		{
			if (index < 0)
			{
				throw new ArgumentOutOfRangeException("index", Environment.GetResourceString("Non-negative number required."));
			}
			if (count < 0)
			{
				throw new ArgumentOutOfRangeException("count", Environment.GetResourceString("Non-negative number required."));
			}
			if (this._size - index < count)
			{
				throw new ArgumentException(Environment.GetResourceString("Offset and length were out of bounds for the array or count is greater than the number of elements from index to the end of the source collection."));
			}
			return Array.BinarySearch(this._items, index, count, value, comparer);
		}

		/// <summary>Searches the entire sorted <see cref="T:System.Collections.ArrayList" /> for an element using the default comparer and returns the zero-based index of the element.</summary>
		/// <param name="value">The <see cref="T:System.Object" /> to locate. The value can be <see langword="null" />. </param>
		/// <returns>The zero-based index of <paramref name="value" /> in the sorted <see cref="T:System.Collections.ArrayList" />, if <paramref name="value" /> is found; otherwise, a negative number, which is the bitwise complement of the index of the next element that is larger than <paramref name="value" /> or, if there is no larger element, the bitwise complement of <see cref="P:System.Collections.ArrayList.Count" />.</returns>
		/// <exception cref="T:System.ArgumentException">Neither <paramref name="value" /> nor the elements of <see cref="T:System.Collections.ArrayList" /> implement the <see cref="T:System.IComparable" /> interface. </exception>
		/// <exception cref="T:System.InvalidOperationException">
		///         <paramref name="value" /> is not of the same type as the elements of the <see cref="T:System.Collections.ArrayList" />. </exception>
		// Token: 0x060057AE RID: 22446 RVA: 0x0011CE42 File Offset: 0x0011B042
		public virtual int BinarySearch(object value)
		{
			return this.BinarySearch(0, this.Count, value, null);
		}

		/// <summary>Searches the entire sorted <see cref="T:System.Collections.ArrayList" /> for an element using the specified comparer and returns the zero-based index of the element.</summary>
		/// <param name="value">The <see cref="T:System.Object" /> to locate. The value can be <see langword="null" />. </param>
		/// <param name="comparer">The <see cref="T:System.Collections.IComparer" /> implementation to use when comparing elements.-or- 
		///       <see langword="null" /> to use the default comparer that is the <see cref="T:System.IComparable" /> implementation of each element. </param>
		/// <returns>The zero-based index of <paramref name="value" /> in the sorted <see cref="T:System.Collections.ArrayList" />, if <paramref name="value" /> is found; otherwise, a negative number, which is the bitwise complement of the index of the next element that is larger than <paramref name="value" /> or, if there is no larger element, the bitwise complement of <see cref="P:System.Collections.ArrayList.Count" />.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="comparer" /> is <see langword="null" /> and neither <paramref name="value" /> nor the elements of <see cref="T:System.Collections.ArrayList" /> implement the <see cref="T:System.IComparable" /> interface. </exception>
		/// <exception cref="T:System.InvalidOperationException">
		///         <paramref name="comparer" /> is <see langword="null" /> and <paramref name="value" /> is not of the same type as the elements of the <see cref="T:System.Collections.ArrayList" />. </exception>
		// Token: 0x060057AF RID: 22447 RVA: 0x0011CE53 File Offset: 0x0011B053
		public virtual int BinarySearch(object value, IComparer comparer)
		{
			return this.BinarySearch(0, this.Count, value, comparer);
		}

		/// <summary>Removes all elements from the <see cref="T:System.Collections.ArrayList" />.</summary>
		/// <exception cref="T:System.NotSupportedException">The <see cref="T:System.Collections.ArrayList" /> is read-only.-or- The <see cref="T:System.Collections.ArrayList" /> has a fixed size. </exception>
		// Token: 0x060057B0 RID: 22448 RVA: 0x0011CE64 File Offset: 0x0011B064
		public virtual void Clear()
		{
			if (this._size > 0)
			{
				Array.Clear(this._items, 0, this._size);
				this._size = 0;
			}
			this._version++;
		}

		/// <summary>Creates a shallow copy of the <see cref="T:System.Collections.ArrayList" />.</summary>
		/// <returns>A shallow copy of the <see cref="T:System.Collections.ArrayList" />.</returns>
		// Token: 0x060057B1 RID: 22449 RVA: 0x0011CE98 File Offset: 0x0011B098
		public virtual object Clone()
		{
			ArrayList arrayList = new ArrayList(this._size);
			arrayList._size = this._size;
			arrayList._version = this._version;
			Array.Copy(this._items, 0, arrayList._items, 0, this._size);
			return arrayList;
		}

		/// <summary>Determines whether an element is in the <see cref="T:System.Collections.ArrayList" />.</summary>
		/// <param name="item">The <see cref="T:System.Object" /> to locate in the <see cref="T:System.Collections.ArrayList" />. The value can be <see langword="null" />. </param>
		/// <returns>
		///     <see langword="true" /> if <paramref name="item" /> is found in the <see cref="T:System.Collections.ArrayList" />; otherwise, <see langword="false" />.</returns>
		// Token: 0x060057B2 RID: 22450 RVA: 0x0011CEE4 File Offset: 0x0011B0E4
		public virtual bool Contains(object item)
		{
			if (item == null)
			{
				for (int i = 0; i < this._size; i++)
				{
					if (this._items[i] == null)
					{
						return true;
					}
				}
				return false;
			}
			for (int j = 0; j < this._size; j++)
			{
				if (this._items[j] != null && this._items[j].Equals(item))
				{
					return true;
				}
			}
			return false;
		}

		/// <summary>Copies the entire <see cref="T:System.Collections.ArrayList" /> to a compatible one-dimensional <see cref="T:System.Array" />, starting at the beginning of the target array.</summary>
		/// <param name="array">The one-dimensional <see cref="T:System.Array" /> that is the destination of the elements copied from <see cref="T:System.Collections.ArrayList" />. The <see cref="T:System.Array" /> must have zero-based indexing. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="array" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="array" /> is multidimensional.-or- The number of elements in the source <see cref="T:System.Collections.ArrayList" /> is greater than the number of elements that the destination <paramref name="array" /> can contain. </exception>
		/// <exception cref="T:System.InvalidCastException">The type of the source <see cref="T:System.Collections.ArrayList" /> cannot be cast automatically to the type of the destination <paramref name="array" />. </exception>
		// Token: 0x060057B3 RID: 22451 RVA: 0x0011CF41 File Offset: 0x0011B141
		public virtual void CopyTo(Array array)
		{
			this.CopyTo(array, 0);
		}

		/// <summary>Copies the entire <see cref="T:System.Collections.ArrayList" /> to a compatible one-dimensional <see cref="T:System.Array" />, starting at the specified index of the target array.</summary>
		/// <param name="array">The one-dimensional <see cref="T:System.Array" /> that is the destination of the elements copied from <see cref="T:System.Collections.ArrayList" />. The <see cref="T:System.Array" /> must have zero-based indexing. </param>
		/// <param name="arrayIndex">The zero-based index in <paramref name="array" /> at which copying begins. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="array" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="arrayIndex" /> is less than zero. </exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="array" /> is multidimensional.-or- The number of elements in the source <see cref="T:System.Collections.ArrayList" /> is greater than the available space from <paramref name="arrayIndex" /> to the end of the destination <paramref name="array" />. </exception>
		/// <exception cref="T:System.InvalidCastException">The type of the source <see cref="T:System.Collections.ArrayList" /> cannot be cast automatically to the type of the destination <paramref name="array" />. </exception>
		// Token: 0x060057B4 RID: 22452 RVA: 0x0011CF4B File Offset: 0x0011B14B
		public virtual void CopyTo(Array array, int arrayIndex)
		{
			if (array != null && array.Rank != 1)
			{
				throw new ArgumentException(Environment.GetResourceString("Only single dimensional arrays are supported for the requested action."));
			}
			Array.Copy(this._items, 0, array, arrayIndex, this._size);
		}

		/// <summary>Copies a range of elements from the <see cref="T:System.Collections.ArrayList" /> to a compatible one-dimensional <see cref="T:System.Array" />, starting at the specified index of the target array.</summary>
		/// <param name="index">The zero-based index in the source <see cref="T:System.Collections.ArrayList" /> at which copying begins. </param>
		/// <param name="array">The one-dimensional <see cref="T:System.Array" /> that is the destination of the elements copied from <see cref="T:System.Collections.ArrayList" />. The <see cref="T:System.Array" /> must have zero-based indexing. </param>
		/// <param name="arrayIndex">The zero-based index in <paramref name="array" /> at which copying begins. </param>
		/// <param name="count">The number of elements to copy. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="array" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="index" /> is less than zero.-or- 
		///         <paramref name="arrayIndex" /> is less than zero.-or- 
		///         <paramref name="count" /> is less than zero. </exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="array" /> is multidimensional.-or- 
		///         <paramref name="index" /> is equal to or greater than the <see cref="P:System.Collections.ArrayList.Count" /> of the source <see cref="T:System.Collections.ArrayList" />.-or- The number of elements from <paramref name="index" /> to the end of the source <see cref="T:System.Collections.ArrayList" /> is greater than the available space from <paramref name="arrayIndex" /> to the end of the destination <paramref name="array" />. </exception>
		/// <exception cref="T:System.InvalidCastException">The type of the source <see cref="T:System.Collections.ArrayList" /> cannot be cast automatically to the type of the destination <paramref name="array" />. </exception>
		// Token: 0x060057B5 RID: 22453 RVA: 0x0011CF80 File Offset: 0x0011B180
		public virtual void CopyTo(int index, Array array, int arrayIndex, int count)
		{
			if (this._size - index < count)
			{
				throw new ArgumentException(Environment.GetResourceString("Offset and length were out of bounds for the array or count is greater than the number of elements from index to the end of the source collection."));
			}
			if (array != null && array.Rank != 1)
			{
				throw new ArgumentException(Environment.GetResourceString("Only single dimensional arrays are supported for the requested action."));
			}
			Array.Copy(this._items, index, array, arrayIndex, count);
		}

		// Token: 0x060057B6 RID: 22454 RVA: 0x0011CFD8 File Offset: 0x0011B1D8
		private void EnsureCapacity(int min)
		{
			if (this._items.Length < min)
			{
				int num = (this._items.Length == 0) ? 4 : (this._items.Length * 2);
				if (num > 2146435071)
				{
					num = 2146435071;
				}
				if (num < min)
				{
					num = min;
				}
				this.Capacity = num;
			}
		}

		/// <summary>Returns an <see cref="T:System.Collections.IList" /> wrapper with a fixed size.</summary>
		/// <param name="list">The <see cref="T:System.Collections.IList" /> to wrap. </param>
		/// <returns>An <see cref="T:System.Collections.IList" /> wrapper with a fixed size.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="list" /> is <see langword="null" />. </exception>
		// Token: 0x060057B7 RID: 22455 RVA: 0x0011D022 File Offset: 0x0011B222
		public static IList FixedSize(IList list)
		{
			if (list == null)
			{
				throw new ArgumentNullException("list");
			}
			return new ArrayList.FixedSizeList(list);
		}

		/// <summary>Returns an <see cref="T:System.Collections.ArrayList" /> wrapper with a fixed size.</summary>
		/// <param name="list">The <see cref="T:System.Collections.ArrayList" /> to wrap. </param>
		/// <returns>An <see cref="T:System.Collections.ArrayList" /> wrapper with a fixed size.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="list" /> is <see langword="null" />. </exception>
		// Token: 0x060057B8 RID: 22456 RVA: 0x0011D038 File Offset: 0x0011B238
		public static ArrayList FixedSize(ArrayList list)
		{
			if (list == null)
			{
				throw new ArgumentNullException("list");
			}
			return new ArrayList.FixedSizeArrayList(list);
		}

		/// <summary>Returns an enumerator for the entire <see cref="T:System.Collections.ArrayList" />.</summary>
		/// <returns>An <see cref="T:System.Collections.IEnumerator" /> for the entire <see cref="T:System.Collections.ArrayList" />.</returns>
		// Token: 0x060057B9 RID: 22457 RVA: 0x0011D04E File Offset: 0x0011B24E
		public virtual IEnumerator GetEnumerator()
		{
			return new ArrayList.ArrayListEnumeratorSimple(this);
		}

		/// <summary>Returns an enumerator for a range of elements in the <see cref="T:System.Collections.ArrayList" />.</summary>
		/// <param name="index">The zero-based starting index of the <see cref="T:System.Collections.ArrayList" /> section that the enumerator should refer to. </param>
		/// <param name="count">The number of elements in the <see cref="T:System.Collections.ArrayList" /> section that the enumerator should refer to. </param>
		/// <returns>An <see cref="T:System.Collections.IEnumerator" /> for the specified range of elements in the <see cref="T:System.Collections.ArrayList" />.</returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="index" /> is less than zero.-or- 
		///         <paramref name="count" /> is less than zero. </exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="index" /> and <paramref name="count" /> do not specify a valid range in the <see cref="T:System.Collections.ArrayList" />. </exception>
		// Token: 0x060057BA RID: 22458 RVA: 0x0011D058 File Offset: 0x0011B258
		public virtual IEnumerator GetEnumerator(int index, int count)
		{
			if (index < 0)
			{
				throw new ArgumentOutOfRangeException("index", Environment.GetResourceString("Non-negative number required."));
			}
			if (count < 0)
			{
				throw new ArgumentOutOfRangeException("count", Environment.GetResourceString("Non-negative number required."));
			}
			if (this._size - index < count)
			{
				throw new ArgumentException(Environment.GetResourceString("Offset and length were out of bounds for the array or count is greater than the number of elements from index to the end of the source collection."));
			}
			return new ArrayList.ArrayListEnumerator(this, index, count);
		}

		/// <summary>Searches for the specified <see cref="T:System.Object" /> and returns the zero-based index of the first occurrence within the entire <see cref="T:System.Collections.ArrayList" />.</summary>
		/// <param name="value">The <see cref="T:System.Object" /> to locate in the <see cref="T:System.Collections.ArrayList" />. The value can be <see langword="null" />. </param>
		/// <returns>The zero-based index of the first occurrence of <paramref name="value" /> within the entire <see cref="T:System.Collections.ArrayList" />, if found; otherwise, -1.</returns>
		// Token: 0x060057BB RID: 22459 RVA: 0x0011D0BA File Offset: 0x0011B2BA
		public virtual int IndexOf(object value)
		{
			return Array.IndexOf(this._items, value, 0, this._size);
		}

		/// <summary>Searches for the specified <see cref="T:System.Object" /> and returns the zero-based index of the first occurrence within the range of elements in the <see cref="T:System.Collections.ArrayList" /> that extends from the specified index to the last element.</summary>
		/// <param name="value">The <see cref="T:System.Object" /> to locate in the <see cref="T:System.Collections.ArrayList" />. The value can be <see langword="null" />. </param>
		/// <param name="startIndex">The zero-based starting index of the search. 0 (zero) is valid in an empty list.</param>
		/// <returns>The zero-based index of the first occurrence of <paramref name="value" /> within the range of elements in the <see cref="T:System.Collections.ArrayList" /> that extends from <paramref name="startIndex" /> to the last element, if found; otherwise, -1.</returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="startIndex" /> is outside the range of valid indexes for the <see cref="T:System.Collections.ArrayList" />. </exception>
		// Token: 0x060057BC RID: 22460 RVA: 0x0011D0CF File Offset: 0x0011B2CF
		public virtual int IndexOf(object value, int startIndex)
		{
			if (startIndex > this._size)
			{
				throw new ArgumentOutOfRangeException("startIndex", Environment.GetResourceString("Index was out of range. Must be non-negative and less than the size of the collection."));
			}
			return Array.IndexOf(this._items, value, startIndex, this._size - startIndex);
		}

		/// <summary>Searches for the specified <see cref="T:System.Object" /> and returns the zero-based index of the first occurrence within the range of elements in the <see cref="T:System.Collections.ArrayList" /> that starts at the specified index and contains the specified number of elements.</summary>
		/// <param name="value">The <see cref="T:System.Object" /> to locate in the <see cref="T:System.Collections.ArrayList" />. The value can be <see langword="null" />. </param>
		/// <param name="startIndex">The zero-based starting index of the search. 0 (zero) is valid in an empty list.</param>
		/// <param name="count">The number of elements in the section to search. </param>
		/// <returns>The zero-based index of the first occurrence of <paramref name="value" /> within the range of elements in the <see cref="T:System.Collections.ArrayList" /> that starts at <paramref name="startIndex" /> and contains <paramref name="count" /> number of elements, if found; otherwise, -1.</returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="startIndex" /> is outside the range of valid indexes for the <see cref="T:System.Collections.ArrayList" />.-or- 
		///         <paramref name="count" /> is less than zero.-or- 
		///         <paramref name="startIndex" /> and <paramref name="count" /> do not specify a valid section in the <see cref="T:System.Collections.ArrayList" />. </exception>
		// Token: 0x060057BD RID: 22461 RVA: 0x0011D104 File Offset: 0x0011B304
		public virtual int IndexOf(object value, int startIndex, int count)
		{
			if (startIndex > this._size)
			{
				throw new ArgumentOutOfRangeException("startIndex", Environment.GetResourceString("Index was out of range. Must be non-negative and less than the size of the collection."));
			}
			if (count < 0 || startIndex > this._size - count)
			{
				throw new ArgumentOutOfRangeException("count", Environment.GetResourceString("Count must be positive and count must refer to a location within the string/array/collection."));
			}
			return Array.IndexOf(this._items, value, startIndex, count);
		}

		/// <summary>Inserts an element into the <see cref="T:System.Collections.ArrayList" /> at the specified index.</summary>
		/// <param name="index">The zero-based index at which <paramref name="value" /> should be inserted. </param>
		/// <param name="value">The <see cref="T:System.Object" /> to insert. The value can be <see langword="null" />. </param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="index" /> is less than zero.-or- 
		///         <paramref name="index" /> is greater than <see cref="P:System.Collections.ArrayList.Count" />. </exception>
		/// <exception cref="T:System.NotSupportedException">The <see cref="T:System.Collections.ArrayList" /> is read-only.-or- The <see cref="T:System.Collections.ArrayList" /> has a fixed size. </exception>
		// Token: 0x060057BE RID: 22462 RVA: 0x0011D164 File Offset: 0x0011B364
		public virtual void Insert(int index, object value)
		{
			if (index < 0 || index > this._size)
			{
				throw new ArgumentOutOfRangeException("index", Environment.GetResourceString("Insertion index was out of range. Must be non-negative and less than or equal to size."));
			}
			if (this._size == this._items.Length)
			{
				this.EnsureCapacity(this._size + 1);
			}
			if (index < this._size)
			{
				Array.Copy(this._items, index, this._items, index + 1, this._size - index);
			}
			this._items[index] = value;
			this._size++;
			this._version++;
		}

		/// <summary>Inserts the elements of a collection into the <see cref="T:System.Collections.ArrayList" /> at the specified index.</summary>
		/// <param name="index">The zero-based index at which the new elements should be inserted. </param>
		/// <param name="c">The <see cref="T:System.Collections.ICollection" /> whose elements should be inserted into the <see cref="T:System.Collections.ArrayList" />. The collection itself cannot be <see langword="null" />, but it can contain elements that are <see langword="null" />. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="c" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="index" /> is less than zero.-or- 
		///         <paramref name="index" /> is greater than <see cref="P:System.Collections.ArrayList.Count" />. </exception>
		/// <exception cref="T:System.NotSupportedException">The <see cref="T:System.Collections.ArrayList" /> is read-only.-or- The <see cref="T:System.Collections.ArrayList" /> has a fixed size. </exception>
		// Token: 0x060057BF RID: 22463 RVA: 0x0011D1FC File Offset: 0x0011B3FC
		public virtual void InsertRange(int index, ICollection c)
		{
			if (c == null)
			{
				throw new ArgumentNullException("c", Environment.GetResourceString("Collection cannot be null."));
			}
			if (index < 0 || index > this._size)
			{
				throw new ArgumentOutOfRangeException("index", Environment.GetResourceString("Index was out of range. Must be non-negative and less than the size of the collection."));
			}
			int count = c.Count;
			if (count > 0)
			{
				this.EnsureCapacity(this._size + count);
				if (index < this._size)
				{
					Array.Copy(this._items, index, this._items, index + count, this._size - index);
				}
				object[] array = new object[count];
				c.CopyTo(array, 0);
				array.CopyTo(this._items, index);
				this._size += count;
				this._version++;
			}
		}

		/// <summary>Searches for the specified <see cref="T:System.Object" /> and returns the zero-based index of the last occurrence within the entire <see cref="T:System.Collections.ArrayList" />.</summary>
		/// <param name="value">The <see cref="T:System.Object" /> to locate in the <see cref="T:System.Collections.ArrayList" />. The value can be <see langword="null" />. </param>
		/// <returns>The zero-based index of the last occurrence of <paramref name="value" /> within the entire the <see cref="T:System.Collections.ArrayList" />, if found; otherwise, -1.</returns>
		// Token: 0x060057C0 RID: 22464 RVA: 0x0011D2BA File Offset: 0x0011B4BA
		public virtual int LastIndexOf(object value)
		{
			return this.LastIndexOf(value, this._size - 1, this._size);
		}

		/// <summary>Searches for the specified <see cref="T:System.Object" /> and returns the zero-based index of the last occurrence within the range of elements in the <see cref="T:System.Collections.ArrayList" /> that extends from the first element to the specified index.</summary>
		/// <param name="value">The <see cref="T:System.Object" /> to locate in the <see cref="T:System.Collections.ArrayList" />. The value can be <see langword="null" />. </param>
		/// <param name="startIndex">The zero-based starting index of the backward search. </param>
		/// <returns>The zero-based index of the last occurrence of <paramref name="value" /> within the range of elements in the <see cref="T:System.Collections.ArrayList" /> that extends from the first element to <paramref name="startIndex" />, if found; otherwise, -1.</returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="startIndex" /> is outside the range of valid indexes for the <see cref="T:System.Collections.ArrayList" />. </exception>
		// Token: 0x060057C1 RID: 22465 RVA: 0x0011D2D1 File Offset: 0x0011B4D1
		public virtual int LastIndexOf(object value, int startIndex)
		{
			if (startIndex >= this._size)
			{
				throw new ArgumentOutOfRangeException("startIndex", Environment.GetResourceString("Index was out of range. Must be non-negative and less than the size of the collection."));
			}
			return this.LastIndexOf(value, startIndex, startIndex + 1);
		}

		/// <summary>Searches for the specified <see cref="T:System.Object" /> and returns the zero-based index of the last occurrence within the range of elements in the <see cref="T:System.Collections.ArrayList" /> that contains the specified number of elements and ends at the specified index.</summary>
		/// <param name="value">The <see cref="T:System.Object" /> to locate in the <see cref="T:System.Collections.ArrayList" />. The value can be <see langword="null" />. </param>
		/// <param name="startIndex">The zero-based starting index of the backward search. </param>
		/// <param name="count">The number of elements in the section to search. </param>
		/// <returns>The zero-based index of the last occurrence of <paramref name="value" /> within the range of elements in the <see cref="T:System.Collections.ArrayList" /> that contains <paramref name="count" /> number of elements and ends at <paramref name="startIndex" />, if found; otherwise, -1.</returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="startIndex" /> is outside the range of valid indexes for the <see cref="T:System.Collections.ArrayList" />.-or- 
		///         <paramref name="count" /> is less than zero.-or- 
		///         <paramref name="startIndex" /> and <paramref name="count" /> do not specify a valid section in the <see cref="T:System.Collections.ArrayList" />. </exception>
		// Token: 0x060057C2 RID: 22466 RVA: 0x0011D2FC File Offset: 0x0011B4FC
		public virtual int LastIndexOf(object value, int startIndex, int count)
		{
			if (this.Count != 0 && (startIndex < 0 || count < 0))
			{
				throw new ArgumentOutOfRangeException((startIndex < 0) ? "startIndex" : "count", Environment.GetResourceString("Non-negative number required."));
			}
			if (this._size == 0)
			{
				return -1;
			}
			if (startIndex >= this._size || count > startIndex + 1)
			{
				throw new ArgumentOutOfRangeException((startIndex >= this._size) ? "startIndex" : "count", Environment.GetResourceString("Larger than collection size."));
			}
			return Array.LastIndexOf(this._items, value, startIndex, count);
		}

		/// <summary>Returns a read-only <see cref="T:System.Collections.IList" /> wrapper.</summary>
		/// <param name="list">The <see cref="T:System.Collections.IList" /> to wrap. </param>
		/// <returns>A read-only <see cref="T:System.Collections.IList" /> wrapper around <paramref name="list" />.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="list" /> is <see langword="null" />. </exception>
		// Token: 0x060057C3 RID: 22467 RVA: 0x0011D385 File Offset: 0x0011B585
		public static IList ReadOnly(IList list)
		{
			if (list == null)
			{
				throw new ArgumentNullException("list");
			}
			return new ArrayList.ReadOnlyList(list);
		}

		/// <summary>Returns a read-only <see cref="T:System.Collections.ArrayList" /> wrapper.</summary>
		/// <param name="list">The <see cref="T:System.Collections.ArrayList" /> to wrap. </param>
		/// <returns>A read-only <see cref="T:System.Collections.ArrayList" /> wrapper around <paramref name="list" />.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="list" /> is <see langword="null" />. </exception>
		// Token: 0x060057C4 RID: 22468 RVA: 0x0011D39B File Offset: 0x0011B59B
		public static ArrayList ReadOnly(ArrayList list)
		{
			if (list == null)
			{
				throw new ArgumentNullException("list");
			}
			return new ArrayList.ReadOnlyArrayList(list);
		}

		/// <summary>Removes the first occurrence of a specific object from the <see cref="T:System.Collections.ArrayList" />.</summary>
		/// <param name="obj">The <see cref="T:System.Object" /> to remove from the <see cref="T:System.Collections.ArrayList" />. The value can be <see langword="null" />. </param>
		/// <exception cref="T:System.NotSupportedException">The <see cref="T:System.Collections.ArrayList" /> is read-only.-or- The <see cref="T:System.Collections.ArrayList" /> has a fixed size. </exception>
		// Token: 0x060057C5 RID: 22469 RVA: 0x0011D3B4 File Offset: 0x0011B5B4
		public virtual void Remove(object obj)
		{
			int num = this.IndexOf(obj);
			if (num >= 0)
			{
				this.RemoveAt(num);
			}
		}

		/// <summary>Removes the element at the specified index of the <see cref="T:System.Collections.ArrayList" />.</summary>
		/// <param name="index">The zero-based index of the element to remove. </param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="index" /> is less than zero.-or- 
		///         <paramref name="index" /> is equal to or greater than <see cref="P:System.Collections.ArrayList.Count" />. </exception>
		/// <exception cref="T:System.NotSupportedException">The <see cref="T:System.Collections.ArrayList" /> is read-only.-or- The <see cref="T:System.Collections.ArrayList" /> has a fixed size. </exception>
		// Token: 0x060057C6 RID: 22470 RVA: 0x0011D3D4 File Offset: 0x0011B5D4
		public virtual void RemoveAt(int index)
		{
			if (index < 0 || index >= this._size)
			{
				throw new ArgumentOutOfRangeException("index", Environment.GetResourceString("Index was out of range. Must be non-negative and less than the size of the collection."));
			}
			this._size--;
			if (index < this._size)
			{
				Array.Copy(this._items, index + 1, this._items, index, this._size - index);
			}
			this._items[this._size] = null;
			this._version++;
		}

		/// <summary>Removes a range of elements from the <see cref="T:System.Collections.ArrayList" />.</summary>
		/// <param name="index">The zero-based starting index of the range of elements to remove. </param>
		/// <param name="count">The number of elements to remove. </param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="index" /> is less than zero.-or- 
		///         <paramref name="count" /> is less than zero. </exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="index" /> and <paramref name="count" /> do not denote a valid range of elements in the <see cref="T:System.Collections.ArrayList" />. </exception>
		/// <exception cref="T:System.NotSupportedException">The <see cref="T:System.Collections.ArrayList" /> is read-only.-or- The <see cref="T:System.Collections.ArrayList" /> has a fixed size. </exception>
		// Token: 0x060057C7 RID: 22471 RVA: 0x0011D454 File Offset: 0x0011B654
		public virtual void RemoveRange(int index, int count)
		{
			if (index < 0)
			{
				throw new ArgumentOutOfRangeException("index", Environment.GetResourceString("Non-negative number required."));
			}
			if (count < 0)
			{
				throw new ArgumentOutOfRangeException("count", Environment.GetResourceString("Non-negative number required."));
			}
			if (this._size - index < count)
			{
				throw new ArgumentException(Environment.GetResourceString("Offset and length were out of bounds for the array or count is greater than the number of elements from index to the end of the source collection."));
			}
			if (count > 0)
			{
				int i = this._size;
				this._size -= count;
				if (index < this._size)
				{
					Array.Copy(this._items, index + count, this._items, index, this._size - index);
				}
				while (i > this._size)
				{
					this._items[--i] = null;
				}
				this._version++;
			}
		}

		/// <summary>Returns an <see cref="T:System.Collections.ArrayList" /> whose elements are copies of the specified value.</summary>
		/// <param name="value">The <see cref="T:System.Object" /> to copy multiple times in the new <see cref="T:System.Collections.ArrayList" />. The value can be <see langword="null" />. </param>
		/// <param name="count">The number of times <paramref name="value" /> should be copied. </param>
		/// <returns>An <see cref="T:System.Collections.ArrayList" /> with <paramref name="count" /> number of elements, all of which are copies of <paramref name="value" />.</returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="count" /> is less than zero. </exception>
		// Token: 0x060057C8 RID: 22472 RVA: 0x0011D514 File Offset: 0x0011B714
		public static ArrayList Repeat(object value, int count)
		{
			if (count < 0)
			{
				throw new ArgumentOutOfRangeException("count", Environment.GetResourceString("Non-negative number required."));
			}
			ArrayList arrayList = new ArrayList((count > 4) ? count : 4);
			for (int i = 0; i < count; i++)
			{
				arrayList.Add(value);
			}
			return arrayList;
		}

		/// <summary>Reverses the order of the elements in the entire <see cref="T:System.Collections.ArrayList" />.</summary>
		/// <exception cref="T:System.NotSupportedException">The <see cref="T:System.Collections.ArrayList" /> is read-only. </exception>
		// Token: 0x060057C9 RID: 22473 RVA: 0x0011D55D File Offset: 0x0011B75D
		public virtual void Reverse()
		{
			this.Reverse(0, this.Count);
		}

		/// <summary>Reverses the order of the elements in the specified range.</summary>
		/// <param name="index">The zero-based starting index of the range to reverse. </param>
		/// <param name="count">The number of elements in the range to reverse. </param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="index" /> is less than zero.-or- 
		///         <paramref name="count" /> is less than zero. </exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="index" /> and <paramref name="count" /> do not denote a valid range of elements in the <see cref="T:System.Collections.ArrayList" />. </exception>
		/// <exception cref="T:System.NotSupportedException">The <see cref="T:System.Collections.ArrayList" /> is read-only. </exception>
		// Token: 0x060057CA RID: 22474 RVA: 0x0011D56C File Offset: 0x0011B76C
		public virtual void Reverse(int index, int count)
		{
			if (index < 0)
			{
				throw new ArgumentOutOfRangeException("index", Environment.GetResourceString("Non-negative number required."));
			}
			if (count < 0)
			{
				throw new ArgumentOutOfRangeException("count", Environment.GetResourceString("Non-negative number required."));
			}
			if (this._size - index < count)
			{
				throw new ArgumentException(Environment.GetResourceString("Offset and length were out of bounds for the array or count is greater than the number of elements from index to the end of the source collection."));
			}
			Array.Reverse<object>(this._items, index, count);
			this._version++;
		}

		/// <summary>Copies the elements of a collection over a range of elements in the <see cref="T:System.Collections.ArrayList" />.</summary>
		/// <param name="index">The zero-based <see cref="T:System.Collections.ArrayList" /> index at which to start copying the elements of <paramref name="c" />. </param>
		/// <param name="c">The <see cref="T:System.Collections.ICollection" /> whose elements to copy to the <see cref="T:System.Collections.ArrayList" />. The collection itself cannot be <see langword="null" />, but it can contain elements that are <see langword="null" />. </param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="index" /> is less than zero.-or- 
		///         <paramref name="index" /> plus the number of elements in <paramref name="c" /> is greater than <see cref="P:System.Collections.ArrayList.Count" />. </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="c" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.NotSupportedException">The <see cref="T:System.Collections.ArrayList" /> is read-only. </exception>
		// Token: 0x060057CB RID: 22475 RVA: 0x0011D5E4 File Offset: 0x0011B7E4
		public virtual void SetRange(int index, ICollection c)
		{
			if (c == null)
			{
				throw new ArgumentNullException("c", Environment.GetResourceString("Collection cannot be null."));
			}
			int count = c.Count;
			if (index < 0 || index > this._size - count)
			{
				throw new ArgumentOutOfRangeException("index", Environment.GetResourceString("Index was out of range. Must be non-negative and less than the size of the collection."));
			}
			if (count > 0)
			{
				c.CopyTo(this._items, index);
				this._version++;
			}
		}

		/// <summary>Returns an <see cref="T:System.Collections.ArrayList" /> which represents a subset of the elements in the source <see cref="T:System.Collections.ArrayList" />.</summary>
		/// <param name="index">The zero-based <see cref="T:System.Collections.ArrayList" /> index at which the range starts. </param>
		/// <param name="count">The number of elements in the range. </param>
		/// <returns>An <see cref="T:System.Collections.ArrayList" /> which represents a subset of the elements in the source <see cref="T:System.Collections.ArrayList" />.</returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="index" /> is less than zero.-or- 
		///         <paramref name="count" /> is less than zero. </exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="index" /> and <paramref name="count" /> do not denote a valid range of elements in the <see cref="T:System.Collections.ArrayList" />. </exception>
		// Token: 0x060057CC RID: 22476 RVA: 0x0011D654 File Offset: 0x0011B854
		public virtual ArrayList GetRange(int index, int count)
		{
			if (index < 0 || count < 0)
			{
				throw new ArgumentOutOfRangeException((index < 0) ? "index" : "count", Environment.GetResourceString("Non-negative number required."));
			}
			if (this._size - index < count)
			{
				throw new ArgumentException(Environment.GetResourceString("Offset and length were out of bounds for the array or count is greater than the number of elements from index to the end of the source collection."));
			}
			return new ArrayList.Range(this, index, count);
		}

		/// <summary>Sorts the elements in the entire <see cref="T:System.Collections.ArrayList" />.</summary>
		/// <exception cref="T:System.NotSupportedException">The <see cref="T:System.Collections.ArrayList" /> is read-only. </exception>
		// Token: 0x060057CD RID: 22477 RVA: 0x0011D6AC File Offset: 0x0011B8AC
		public virtual void Sort()
		{
			this.Sort(0, this.Count, Comparer.Default);
		}

		/// <summary>Sorts the elements in the entire <see cref="T:System.Collections.ArrayList" /> using the specified comparer.</summary>
		/// <param name="comparer">The <see cref="T:System.Collections.IComparer" /> implementation to use when comparing elements.-or- A null reference (<see langword="Nothing" /> in Visual Basic) to use the <see cref="T:System.IComparable" /> implementation of each element. </param>
		/// <exception cref="T:System.NotSupportedException">The <see cref="T:System.Collections.ArrayList" /> is read-only. </exception>
		/// <exception cref="T:System.InvalidOperationException">An error occurred while comparing two elements.</exception>
		/// <exception cref="T:System.ArgumentException">
		///         <see langword="null" /> is passed for <paramref name="comparer" />, and the elements in the list do not implement <see cref="T:System.IComparable" />.</exception>
		// Token: 0x060057CE RID: 22478 RVA: 0x0011D6C0 File Offset: 0x0011B8C0
		public virtual void Sort(IComparer comparer)
		{
			this.Sort(0, this.Count, comparer);
		}

		/// <summary>Sorts the elements in a range of elements in <see cref="T:System.Collections.ArrayList" /> using the specified comparer.</summary>
		/// <param name="index">The zero-based starting index of the range to sort. </param>
		/// <param name="count">The length of the range to sort. </param>
		/// <param name="comparer">The <see cref="T:System.Collections.IComparer" /> implementation to use when comparing elements.-or- A null reference (<see langword="Nothing" /> in Visual Basic) to use the <see cref="T:System.IComparable" /> implementation of each element. </param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="index" /> is less than zero.-or- 
		///         <paramref name="count" /> is less than zero. </exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="index" /> and <paramref name="count" /> do not specify a valid range in the <see cref="T:System.Collections.ArrayList" />. </exception>
		/// <exception cref="T:System.NotSupportedException">The <see cref="T:System.Collections.ArrayList" /> is read-only. </exception>
		/// <exception cref="T:System.InvalidOperationException">An error occurred while comparing two elements.</exception>
		// Token: 0x060057CF RID: 22479 RVA: 0x0011D6D0 File Offset: 0x0011B8D0
		public virtual void Sort(int index, int count, IComparer comparer)
		{
			if (index < 0)
			{
				throw new ArgumentOutOfRangeException("index", Environment.GetResourceString("Non-negative number required."));
			}
			if (count < 0)
			{
				throw new ArgumentOutOfRangeException("count", Environment.GetResourceString("Non-negative number required."));
			}
			if (this._size - index < count)
			{
				throw new ArgumentException(Environment.GetResourceString("Offset and length were out of bounds for the array or count is greater than the number of elements from index to the end of the source collection."));
			}
			Array.Sort(this._items, index, count, comparer);
			this._version++;
		}

		/// <summary>Returns an <see cref="T:System.Collections.IList" /> wrapper that is synchronized (thread safe).</summary>
		/// <param name="list">The <see cref="T:System.Collections.IList" /> to synchronize. </param>
		/// <returns>An <see cref="T:System.Collections.IList" /> wrapper that is synchronized (thread safe).</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="list" /> is <see langword="null" />. </exception>
		// Token: 0x060057D0 RID: 22480 RVA: 0x0011D746 File Offset: 0x0011B946
		[HostProtection(SecurityAction.LinkDemand, Synchronization = true)]
		public static IList Synchronized(IList list)
		{
			if (list == null)
			{
				throw new ArgumentNullException("list");
			}
			return new ArrayList.SyncIList(list);
		}

		/// <summary>Returns an <see cref="T:System.Collections.ArrayList" /> wrapper that is synchronized (thread safe).</summary>
		/// <param name="list">The <see cref="T:System.Collections.ArrayList" /> to synchronize. </param>
		/// <returns>An <see cref="T:System.Collections.ArrayList" /> wrapper that is synchronized (thread safe).</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="list" /> is <see langword="null" />. </exception>
		// Token: 0x060057D1 RID: 22481 RVA: 0x0011D75C File Offset: 0x0011B95C
		[HostProtection(SecurityAction.LinkDemand, Synchronization = true)]
		public static ArrayList Synchronized(ArrayList list)
		{
			if (list == null)
			{
				throw new ArgumentNullException("list");
			}
			return new ArrayList.SyncArrayList(list);
		}

		/// <summary>Copies the elements of the <see cref="T:System.Collections.ArrayList" /> to a new <see cref="T:System.Object" /> array.</summary>
		/// <returns>An <see cref="T:System.Object" /> array containing copies of the elements of the <see cref="T:System.Collections.ArrayList" />.</returns>
		// Token: 0x060057D2 RID: 22482 RVA: 0x0011D774 File Offset: 0x0011B974
		public virtual object[] ToArray()
		{
			object[] array = new object[this._size];
			Array.Copy(this._items, 0, array, 0, this._size);
			return array;
		}

		/// <summary>Copies the elements of the <see cref="T:System.Collections.ArrayList" /> to a new array of the specified element type.</summary>
		/// <param name="type">The element <see cref="T:System.Type" /> of the destination array to create and copy elements to.</param>
		/// <returns>An array of the specified element type containing copies of the elements of the <see cref="T:System.Collections.ArrayList" />.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="type" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.InvalidCastException">The type of the source <see cref="T:System.Collections.ArrayList" /> cannot be cast automatically to the specified type. </exception>
		// Token: 0x060057D3 RID: 22483 RVA: 0x0011D7A4 File Offset: 0x0011B9A4
		[SecuritySafeCritical]
		public virtual Array ToArray(Type type)
		{
			if (type == null)
			{
				throw new ArgumentNullException("type");
			}
			Array array = Array.UnsafeCreateInstance(type, new int[]
			{
				this._size
			});
			Array.Copy(this._items, 0, array, 0, this._size);
			return array;
		}

		/// <summary>Sets the capacity to the actual number of elements in the <see cref="T:System.Collections.ArrayList" />.</summary>
		/// <exception cref="T:System.NotSupportedException">The <see cref="T:System.Collections.ArrayList" /> is read-only.-or- The <see cref="T:System.Collections.ArrayList" /> has a fixed size. </exception>
		// Token: 0x060057D4 RID: 22484 RVA: 0x0011D7F0 File Offset: 0x0011B9F0
		public virtual void TrimToSize()
		{
			this.Capacity = this._size;
		}

		// Token: 0x060057D5 RID: 22485 RVA: 0x0011D7FE File Offset: 0x0011B9FE
		// Note: this type is marked as 'beforefieldinit'.
		static ArrayList()
		{
		}

		// Token: 0x04002D20 RID: 11552
		private object[] _items;

		// Token: 0x04002D21 RID: 11553
		private int _size;

		// Token: 0x04002D22 RID: 11554
		private int _version;

		// Token: 0x04002D23 RID: 11555
		[NonSerialized]
		private object _syncRoot;

		// Token: 0x04002D24 RID: 11556
		private const int _defaultCapacity = 4;

		// Token: 0x04002D25 RID: 11557
		private static readonly object[] emptyArray = EmptyArray<object>.Value;

		// Token: 0x02000992 RID: 2450
		[Serializable]
		private class IListWrapper : ArrayList
		{
			// Token: 0x060057D6 RID: 22486 RVA: 0x0011D80A File Offset: 0x0011BA0A
			internal IListWrapper(IList list)
			{
				this._list = list;
				this._version = 0;
			}

			// Token: 0x17000F60 RID: 3936
			// (get) Token: 0x060057D7 RID: 22487 RVA: 0x0011D820 File Offset: 0x0011BA20
			// (set) Token: 0x060057D8 RID: 22488 RVA: 0x0011D82D File Offset: 0x0011BA2D
			public override int Capacity
			{
				get
				{
					return this._list.Count;
				}
				set
				{
					if (value < this.Count)
					{
						throw new ArgumentOutOfRangeException("value", Environment.GetResourceString("capacity was less than the current size."));
					}
				}
			}

			// Token: 0x17000F61 RID: 3937
			// (get) Token: 0x060057D9 RID: 22489 RVA: 0x0011D820 File Offset: 0x0011BA20
			public override int Count
			{
				get
				{
					return this._list.Count;
				}
			}

			// Token: 0x17000F62 RID: 3938
			// (get) Token: 0x060057DA RID: 22490 RVA: 0x0011D84D File Offset: 0x0011BA4D
			public override bool IsReadOnly
			{
				get
				{
					return this._list.IsReadOnly;
				}
			}

			// Token: 0x17000F63 RID: 3939
			// (get) Token: 0x060057DB RID: 22491 RVA: 0x0011D85A File Offset: 0x0011BA5A
			public override bool IsFixedSize
			{
				get
				{
					return this._list.IsFixedSize;
				}
			}

			// Token: 0x17000F64 RID: 3940
			// (get) Token: 0x060057DC RID: 22492 RVA: 0x0011D867 File Offset: 0x0011BA67
			public override bool IsSynchronized
			{
				get
				{
					return this._list.IsSynchronized;
				}
			}

			// Token: 0x17000F65 RID: 3941
			public override object this[int index]
			{
				get
				{
					return this._list[index];
				}
				set
				{
					this._list[index] = value;
					this._version++;
				}
			}

			// Token: 0x17000F66 RID: 3942
			// (get) Token: 0x060057DF RID: 22495 RVA: 0x0011D89F File Offset: 0x0011BA9F
			public override object SyncRoot
			{
				get
				{
					return this._list.SyncRoot;
				}
			}

			// Token: 0x060057E0 RID: 22496 RVA: 0x0011D8AC File Offset: 0x0011BAAC
			public override int Add(object obj)
			{
				int result = this._list.Add(obj);
				this._version++;
				return result;
			}

			// Token: 0x060057E1 RID: 22497 RVA: 0x0011D8C8 File Offset: 0x0011BAC8
			public override void AddRange(ICollection c)
			{
				this.InsertRange(this.Count, c);
			}

			// Token: 0x060057E2 RID: 22498 RVA: 0x0011D8D8 File Offset: 0x0011BAD8
			public override int BinarySearch(int index, int count, object value, IComparer comparer)
			{
				if (index < 0 || count < 0)
				{
					throw new ArgumentOutOfRangeException((index < 0) ? "index" : "count", Environment.GetResourceString("Non-negative number required."));
				}
				if (this.Count - index < count)
				{
					throw new ArgumentException(Environment.GetResourceString("Offset and length were out of bounds for the array or count is greater than the number of elements from index to the end of the source collection."));
				}
				if (comparer == null)
				{
					comparer = Comparer.Default;
				}
				int i = index;
				int num = index + count - 1;
				while (i <= num)
				{
					int num2 = (i + num) / 2;
					int num3 = comparer.Compare(value, this._list[num2]);
					if (num3 == 0)
					{
						return num2;
					}
					if (num3 < 0)
					{
						num = num2 - 1;
					}
					else
					{
						i = num2 + 1;
					}
				}
				return ~i;
			}

			// Token: 0x060057E3 RID: 22499 RVA: 0x0011D971 File Offset: 0x0011BB71
			public override void Clear()
			{
				if (this._list.IsFixedSize)
				{
					throw new NotSupportedException(Environment.GetResourceString("Collection was of a fixed size."));
				}
				this._list.Clear();
				this._version++;
			}

			// Token: 0x060057E4 RID: 22500 RVA: 0x0011D9A9 File Offset: 0x0011BBA9
			public override object Clone()
			{
				return new ArrayList.IListWrapper(this._list);
			}

			// Token: 0x060057E5 RID: 22501 RVA: 0x0011D9B6 File Offset: 0x0011BBB6
			public override bool Contains(object obj)
			{
				return this._list.Contains(obj);
			}

			// Token: 0x060057E6 RID: 22502 RVA: 0x0011D9C4 File Offset: 0x0011BBC4
			public override void CopyTo(Array array, int index)
			{
				this._list.CopyTo(array, index);
			}

			// Token: 0x060057E7 RID: 22503 RVA: 0x0011D9D4 File Offset: 0x0011BBD4
			public override void CopyTo(int index, Array array, int arrayIndex, int count)
			{
				if (array == null)
				{
					throw new ArgumentNullException("array");
				}
				if (index < 0 || arrayIndex < 0)
				{
					throw new ArgumentOutOfRangeException((index < 0) ? "index" : "arrayIndex", Environment.GetResourceString("Non-negative number required."));
				}
				if (count < 0)
				{
					throw new ArgumentOutOfRangeException("count", Environment.GetResourceString("Non-negative number required."));
				}
				if (array.Length - arrayIndex < count)
				{
					throw new ArgumentException(Environment.GetResourceString("Offset and length were out of bounds for the array or count is greater than the number of elements from index to the end of the source collection."));
				}
				if (array.Rank != 1)
				{
					throw new ArgumentException(Environment.GetResourceString("Only single dimensional arrays are supported for the requested action."));
				}
				if (this._list.Count - index < count)
				{
					throw new ArgumentException(Environment.GetResourceString("Offset and length were out of bounds for the array or count is greater than the number of elements from index to the end of the source collection."));
				}
				for (int i = index; i < index + count; i++)
				{
					array.SetValue(this._list[i], arrayIndex++);
				}
			}

			// Token: 0x060057E8 RID: 22504 RVA: 0x0011DAAE File Offset: 0x0011BCAE
			public override IEnumerator GetEnumerator()
			{
				return this._list.GetEnumerator();
			}

			// Token: 0x060057E9 RID: 22505 RVA: 0x0011DABC File Offset: 0x0011BCBC
			public override IEnumerator GetEnumerator(int index, int count)
			{
				if (index < 0 || count < 0)
				{
					throw new ArgumentOutOfRangeException((index < 0) ? "index" : "count", Environment.GetResourceString("Non-negative number required."));
				}
				if (this._list.Count - index < count)
				{
					throw new ArgumentException(Environment.GetResourceString("Offset and length were out of bounds for the array or count is greater than the number of elements from index to the end of the source collection."));
				}
				return new ArrayList.IListWrapper.IListWrapperEnumWrapper(this, index, count);
			}

			// Token: 0x060057EA RID: 22506 RVA: 0x0011DB19 File Offset: 0x0011BD19
			public override int IndexOf(object value)
			{
				return this._list.IndexOf(value);
			}

			// Token: 0x060057EB RID: 22507 RVA: 0x0011DB27 File Offset: 0x0011BD27
			public override int IndexOf(object value, int startIndex)
			{
				return this.IndexOf(value, startIndex, this._list.Count - startIndex);
			}

			// Token: 0x060057EC RID: 22508 RVA: 0x0011DB40 File Offset: 0x0011BD40
			public override int IndexOf(object value, int startIndex, int count)
			{
				if (startIndex < 0 || startIndex > this.Count)
				{
					throw new ArgumentOutOfRangeException("startIndex", Environment.GetResourceString("Index was out of range. Must be non-negative and less than the size of the collection."));
				}
				if (count < 0 || startIndex > this.Count - count)
				{
					throw new ArgumentOutOfRangeException("count", Environment.GetResourceString("Count must be positive and count must refer to a location within the string/array/collection."));
				}
				int num = startIndex + count;
				if (value == null)
				{
					for (int i = startIndex; i < num; i++)
					{
						if (this._list[i] == null)
						{
							return i;
						}
					}
					return -1;
				}
				for (int j = startIndex; j < num; j++)
				{
					if (this._list[j] != null && this._list[j].Equals(value))
					{
						return j;
					}
				}
				return -1;
			}

			// Token: 0x060057ED RID: 22509 RVA: 0x0011DBE9 File Offset: 0x0011BDE9
			public override void Insert(int index, object obj)
			{
				this._list.Insert(index, obj);
				this._version++;
			}

			// Token: 0x060057EE RID: 22510 RVA: 0x0011DC08 File Offset: 0x0011BE08
			public override void InsertRange(int index, ICollection c)
			{
				if (c == null)
				{
					throw new ArgumentNullException("c", Environment.GetResourceString("Collection cannot be null."));
				}
				if (index < 0 || index > this.Count)
				{
					throw new ArgumentOutOfRangeException("index", Environment.GetResourceString("Index was out of range. Must be non-negative and less than the size of the collection."));
				}
				if (c.Count > 0)
				{
					ArrayList arrayList = this._list as ArrayList;
					if (arrayList != null)
					{
						arrayList.InsertRange(index, c);
					}
					else
					{
						foreach (object value in c)
						{
							this._list.Insert(index++, value);
						}
					}
					this._version++;
				}
			}

			// Token: 0x060057EF RID: 22511 RVA: 0x0011DCA7 File Offset: 0x0011BEA7
			public override int LastIndexOf(object value)
			{
				return this.LastIndexOf(value, this._list.Count - 1, this._list.Count);
			}

			// Token: 0x060057F0 RID: 22512 RVA: 0x0011DCC8 File Offset: 0x0011BEC8
			public override int LastIndexOf(object value, int startIndex)
			{
				return this.LastIndexOf(value, startIndex, startIndex + 1);
			}

			// Token: 0x060057F1 RID: 22513 RVA: 0x0011DCD8 File Offset: 0x0011BED8
			public override int LastIndexOf(object value, int startIndex, int count)
			{
				if (this._list.Count == 0)
				{
					return -1;
				}
				if (startIndex < 0 || startIndex >= this._list.Count)
				{
					throw new ArgumentOutOfRangeException("startIndex", Environment.GetResourceString("Index was out of range. Must be non-negative and less than the size of the collection."));
				}
				if (count < 0 || count > startIndex + 1)
				{
					throw new ArgumentOutOfRangeException("count", Environment.GetResourceString("Count must be positive and count must refer to a location within the string/array/collection."));
				}
				int num = startIndex - count + 1;
				if (value == null)
				{
					for (int i = startIndex; i >= num; i--)
					{
						if (this._list[i] == null)
						{
							return i;
						}
					}
					return -1;
				}
				for (int j = startIndex; j >= num; j--)
				{
					if (this._list[j] != null && this._list[j].Equals(value))
					{
						return j;
					}
				}
				return -1;
			}

			// Token: 0x060057F2 RID: 22514 RVA: 0x0011DD94 File Offset: 0x0011BF94
			public override void Remove(object value)
			{
				int num = this.IndexOf(value);
				if (num >= 0)
				{
					this.RemoveAt(num);
				}
			}

			// Token: 0x060057F3 RID: 22515 RVA: 0x0011DDB4 File Offset: 0x0011BFB4
			public override void RemoveAt(int index)
			{
				this._list.RemoveAt(index);
				this._version++;
			}

			// Token: 0x060057F4 RID: 22516 RVA: 0x0011DDD0 File Offset: 0x0011BFD0
			public override void RemoveRange(int index, int count)
			{
				if (index < 0 || count < 0)
				{
					throw new ArgumentOutOfRangeException((index < 0) ? "index" : "count", Environment.GetResourceString("Non-negative number required."));
				}
				if (this._list.Count - index < count)
				{
					throw new ArgumentException(Environment.GetResourceString("Offset and length were out of bounds for the array or count is greater than the number of elements from index to the end of the source collection."));
				}
				if (count > 0)
				{
					this._version++;
				}
				while (count > 0)
				{
					this._list.RemoveAt(index);
					count--;
				}
			}

			// Token: 0x060057F5 RID: 22517 RVA: 0x0011DE50 File Offset: 0x0011C050
			public override void Reverse(int index, int count)
			{
				if (index < 0 || count < 0)
				{
					throw new ArgumentOutOfRangeException((index < 0) ? "index" : "count", Environment.GetResourceString("Non-negative number required."));
				}
				if (this._list.Count - index < count)
				{
					throw new ArgumentException(Environment.GetResourceString("Offset and length were out of bounds for the array or count is greater than the number of elements from index to the end of the source collection."));
				}
				int i = index;
				int num = index + count - 1;
				while (i < num)
				{
					object value = this._list[i];
					this._list[i++] = this._list[num];
					this._list[num--] = value;
				}
				this._version++;
			}

			// Token: 0x060057F6 RID: 22518 RVA: 0x0011DEFC File Offset: 0x0011C0FC
			public override void SetRange(int index, ICollection c)
			{
				if (c == null)
				{
					throw new ArgumentNullException("c", Environment.GetResourceString("Collection cannot be null."));
				}
				if (index < 0 || index > this._list.Count - c.Count)
				{
					throw new ArgumentOutOfRangeException("index", Environment.GetResourceString("Index was out of range. Must be non-negative and less than the size of the collection."));
				}
				if (c.Count > 0)
				{
					foreach (object value in c)
					{
						this._list[index++] = value;
					}
					this._version++;
				}
			}

			// Token: 0x060057F7 RID: 22519 RVA: 0x0011DF90 File Offset: 0x0011C190
			public override ArrayList GetRange(int index, int count)
			{
				if (index < 0 || count < 0)
				{
					throw new ArgumentOutOfRangeException((index < 0) ? "index" : "count", Environment.GetResourceString("Non-negative number required."));
				}
				if (this._list.Count - index < count)
				{
					throw new ArgumentException(Environment.GetResourceString("Offset and length were out of bounds for the array or count is greater than the number of elements from index to the end of the source collection."));
				}
				return new ArrayList.Range(this, index, count);
			}

			// Token: 0x060057F8 RID: 22520 RVA: 0x0011DFF0 File Offset: 0x0011C1F0
			public override void Sort(int index, int count, IComparer comparer)
			{
				if (index < 0 || count < 0)
				{
					throw new ArgumentOutOfRangeException((index < 0) ? "index" : "count", Environment.GetResourceString("Non-negative number required."));
				}
				if (this._list.Count - index < count)
				{
					throw new ArgumentException(Environment.GetResourceString("Offset and length were out of bounds for the array or count is greater than the number of elements from index to the end of the source collection."));
				}
				object[] array = new object[count];
				this.CopyTo(index, array, 0, count);
				Array.Sort(array, 0, count, comparer);
				for (int i = 0; i < count; i++)
				{
					this._list[i + index] = array[i];
				}
				this._version++;
			}

			// Token: 0x060057F9 RID: 22521 RVA: 0x0011E08C File Offset: 0x0011C28C
			public override object[] ToArray()
			{
				object[] array = new object[this.Count];
				this._list.CopyTo(array, 0);
				return array;
			}

			// Token: 0x060057FA RID: 22522 RVA: 0x0011E0B4 File Offset: 0x0011C2B4
			[SecuritySafeCritical]
			public override Array ToArray(Type type)
			{
				if (type == null)
				{
					throw new ArgumentNullException("type");
				}
				Array array = Array.UnsafeCreateInstance(type, new int[]
				{
					this._list.Count
				});
				this._list.CopyTo(array, 0);
				return array;
			}

			// Token: 0x060057FB RID: 22523 RVA: 0x000020D3 File Offset: 0x000002D3
			public override void TrimToSize()
			{
			}

			// Token: 0x04002D26 RID: 11558
			private IList _list;

			// Token: 0x02000993 RID: 2451
			[Serializable]
			private sealed class IListWrapperEnumWrapper : IEnumerator, ICloneable
			{
				// Token: 0x060057FC RID: 22524 RVA: 0x00002050 File Offset: 0x00000250
				private IListWrapperEnumWrapper()
				{
				}

				// Token: 0x060057FD RID: 22525 RVA: 0x0011E100 File Offset: 0x0011C300
				internal IListWrapperEnumWrapper(ArrayList.IListWrapper listWrapper, int startIndex, int count)
				{
					this._en = listWrapper.GetEnumerator();
					this._initialStartIndex = startIndex;
					this._initialCount = count;
					while (startIndex-- > 0 && this._en.MoveNext())
					{
					}
					this._remaining = count;
					this._firstCall = true;
				}

				// Token: 0x060057FE RID: 22526 RVA: 0x0011E154 File Offset: 0x0011C354
				public object Clone()
				{
					return new ArrayList.IListWrapper.IListWrapperEnumWrapper
					{
						_en = (IEnumerator)((ICloneable)this._en).Clone(),
						_initialStartIndex = this._initialStartIndex,
						_initialCount = this._initialCount,
						_remaining = this._remaining,
						_firstCall = this._firstCall
					};
				}

				// Token: 0x060057FF RID: 22527 RVA: 0x0011E1B4 File Offset: 0x0011C3B4
				public bool MoveNext()
				{
					if (this._firstCall)
					{
						this._firstCall = false;
						int remaining = this._remaining;
						this._remaining = remaining - 1;
						return remaining > 0 && this._en.MoveNext();
					}
					if (this._remaining < 0)
					{
						return false;
					}
					if (this._en.MoveNext())
					{
						int remaining = this._remaining;
						this._remaining = remaining - 1;
						return remaining > 0;
					}
					return false;
				}

				// Token: 0x17000F67 RID: 3943
				// (get) Token: 0x06005800 RID: 22528 RVA: 0x0011E220 File Offset: 0x0011C420
				public object Current
				{
					get
					{
						if (this._firstCall)
						{
							throw new InvalidOperationException(Environment.GetResourceString("Enumeration has not started. Call MoveNext."));
						}
						if (this._remaining < 0)
						{
							throw new InvalidOperationException(Environment.GetResourceString("Enumeration already finished."));
						}
						return this._en.Current;
					}
				}

				// Token: 0x06005801 RID: 22529 RVA: 0x0011E260 File Offset: 0x0011C460
				public void Reset()
				{
					this._en.Reset();
					int initialStartIndex = this._initialStartIndex;
					while (initialStartIndex-- > 0 && this._en.MoveNext())
					{
					}
					this._remaining = this._initialCount;
					this._firstCall = true;
				}

				// Token: 0x04002D27 RID: 11559
				private IEnumerator _en;

				// Token: 0x04002D28 RID: 11560
				private int _remaining;

				// Token: 0x04002D29 RID: 11561
				private int _initialStartIndex;

				// Token: 0x04002D2A RID: 11562
				private int _initialCount;

				// Token: 0x04002D2B RID: 11563
				private bool _firstCall;
			}
		}

		// Token: 0x02000994 RID: 2452
		[Serializable]
		private class SyncArrayList : ArrayList
		{
			// Token: 0x06005802 RID: 22530 RVA: 0x0011E2A7 File Offset: 0x0011C4A7
			internal SyncArrayList(ArrayList list) : base(false)
			{
				this._list = list;
				this._root = list.SyncRoot;
			}

			// Token: 0x17000F68 RID: 3944
			// (get) Token: 0x06005803 RID: 22531 RVA: 0x0011E2C4 File Offset: 0x0011C4C4
			// (set) Token: 0x06005804 RID: 22532 RVA: 0x0011E30C File Offset: 0x0011C50C
			public override int Capacity
			{
				get
				{
					object root = this._root;
					int capacity;
					lock (root)
					{
						capacity = this._list.Capacity;
					}
					return capacity;
				}
				set
				{
					object root = this._root;
					lock (root)
					{
						this._list.Capacity = value;
					}
				}
			}

			// Token: 0x17000F69 RID: 3945
			// (get) Token: 0x06005805 RID: 22533 RVA: 0x0011E354 File Offset: 0x0011C554
			public override int Count
			{
				get
				{
					object root = this._root;
					int count;
					lock (root)
					{
						count = this._list.Count;
					}
					return count;
				}
			}

			// Token: 0x17000F6A RID: 3946
			// (get) Token: 0x06005806 RID: 22534 RVA: 0x0011E39C File Offset: 0x0011C59C
			public override bool IsReadOnly
			{
				get
				{
					return this._list.IsReadOnly;
				}
			}

			// Token: 0x17000F6B RID: 3947
			// (get) Token: 0x06005807 RID: 22535 RVA: 0x0011E3A9 File Offset: 0x0011C5A9
			public override bool IsFixedSize
			{
				get
				{
					return this._list.IsFixedSize;
				}
			}

			// Token: 0x17000F6C RID: 3948
			// (get) Token: 0x06005808 RID: 22536 RVA: 0x00004E08 File Offset: 0x00003008
			public override bool IsSynchronized
			{
				get
				{
					return true;
				}
			}

			// Token: 0x17000F6D RID: 3949
			public override object this[int index]
			{
				get
				{
					object root = this._root;
					object result;
					lock (root)
					{
						result = this._list[index];
					}
					return result;
				}
				set
				{
					object root = this._root;
					lock (root)
					{
						this._list[index] = value;
					}
				}
			}

			// Token: 0x17000F6E RID: 3950
			// (get) Token: 0x0600580B RID: 22539 RVA: 0x0011E448 File Offset: 0x0011C648
			public override object SyncRoot
			{
				get
				{
					return this._root;
				}
			}

			// Token: 0x0600580C RID: 22540 RVA: 0x0011E450 File Offset: 0x0011C650
			public override int Add(object value)
			{
				object root = this._root;
				int result;
				lock (root)
				{
					result = this._list.Add(value);
				}
				return result;
			}

			// Token: 0x0600580D RID: 22541 RVA: 0x0011E498 File Offset: 0x0011C698
			public override void AddRange(ICollection c)
			{
				object root = this._root;
				lock (root)
				{
					this._list.AddRange(c);
				}
			}

			// Token: 0x0600580E RID: 22542 RVA: 0x0011E4E0 File Offset: 0x0011C6E0
			public override int BinarySearch(object value)
			{
				object root = this._root;
				int result;
				lock (root)
				{
					result = this._list.BinarySearch(value);
				}
				return result;
			}

			// Token: 0x0600580F RID: 22543 RVA: 0x0011E528 File Offset: 0x0011C728
			public override int BinarySearch(object value, IComparer comparer)
			{
				object root = this._root;
				int result;
				lock (root)
				{
					result = this._list.BinarySearch(value, comparer);
				}
				return result;
			}

			// Token: 0x06005810 RID: 22544 RVA: 0x0011E574 File Offset: 0x0011C774
			public override int BinarySearch(int index, int count, object value, IComparer comparer)
			{
				object root = this._root;
				int result;
				lock (root)
				{
					result = this._list.BinarySearch(index, count, value, comparer);
				}
				return result;
			}

			// Token: 0x06005811 RID: 22545 RVA: 0x0011E5C0 File Offset: 0x0011C7C0
			public override void Clear()
			{
				object root = this._root;
				lock (root)
				{
					this._list.Clear();
				}
			}

			// Token: 0x06005812 RID: 22546 RVA: 0x0011E608 File Offset: 0x0011C808
			public override object Clone()
			{
				object root = this._root;
				object result;
				lock (root)
				{
					result = new ArrayList.SyncArrayList((ArrayList)this._list.Clone());
				}
				return result;
			}

			// Token: 0x06005813 RID: 22547 RVA: 0x0011E65C File Offset: 0x0011C85C
			public override bool Contains(object item)
			{
				object root = this._root;
				bool result;
				lock (root)
				{
					result = this._list.Contains(item);
				}
				return result;
			}

			// Token: 0x06005814 RID: 22548 RVA: 0x0011E6A4 File Offset: 0x0011C8A4
			public override void CopyTo(Array array)
			{
				object root = this._root;
				lock (root)
				{
					this._list.CopyTo(array);
				}
			}

			// Token: 0x06005815 RID: 22549 RVA: 0x0011E6EC File Offset: 0x0011C8EC
			public override void CopyTo(Array array, int index)
			{
				object root = this._root;
				lock (root)
				{
					this._list.CopyTo(array, index);
				}
			}

			// Token: 0x06005816 RID: 22550 RVA: 0x0011E734 File Offset: 0x0011C934
			public override void CopyTo(int index, Array array, int arrayIndex, int count)
			{
				object root = this._root;
				lock (root)
				{
					this._list.CopyTo(index, array, arrayIndex, count);
				}
			}

			// Token: 0x06005817 RID: 22551 RVA: 0x0011E780 File Offset: 0x0011C980
			public override IEnumerator GetEnumerator()
			{
				object root = this._root;
				IEnumerator enumerator;
				lock (root)
				{
					enumerator = this._list.GetEnumerator();
				}
				return enumerator;
			}

			// Token: 0x06005818 RID: 22552 RVA: 0x0011E7C8 File Offset: 0x0011C9C8
			public override IEnumerator GetEnumerator(int index, int count)
			{
				object root = this._root;
				IEnumerator enumerator;
				lock (root)
				{
					enumerator = this._list.GetEnumerator(index, count);
				}
				return enumerator;
			}

			// Token: 0x06005819 RID: 22553 RVA: 0x0011E814 File Offset: 0x0011CA14
			public override int IndexOf(object value)
			{
				object root = this._root;
				int result;
				lock (root)
				{
					result = this._list.IndexOf(value);
				}
				return result;
			}

			// Token: 0x0600581A RID: 22554 RVA: 0x0011E85C File Offset: 0x0011CA5C
			public override int IndexOf(object value, int startIndex)
			{
				object root = this._root;
				int result;
				lock (root)
				{
					result = this._list.IndexOf(value, startIndex);
				}
				return result;
			}

			// Token: 0x0600581B RID: 22555 RVA: 0x0011E8A8 File Offset: 0x0011CAA8
			public override int IndexOf(object value, int startIndex, int count)
			{
				object root = this._root;
				int result;
				lock (root)
				{
					result = this._list.IndexOf(value, startIndex, count);
				}
				return result;
			}

			// Token: 0x0600581C RID: 22556 RVA: 0x0011E8F4 File Offset: 0x0011CAF4
			public override void Insert(int index, object value)
			{
				object root = this._root;
				lock (root)
				{
					this._list.Insert(index, value);
				}
			}

			// Token: 0x0600581D RID: 22557 RVA: 0x0011E93C File Offset: 0x0011CB3C
			public override void InsertRange(int index, ICollection c)
			{
				object root = this._root;
				lock (root)
				{
					this._list.InsertRange(index, c);
				}
			}

			// Token: 0x0600581E RID: 22558 RVA: 0x0011E984 File Offset: 0x0011CB84
			public override int LastIndexOf(object value)
			{
				object root = this._root;
				int result;
				lock (root)
				{
					result = this._list.LastIndexOf(value);
				}
				return result;
			}

			// Token: 0x0600581F RID: 22559 RVA: 0x0011E9CC File Offset: 0x0011CBCC
			public override int LastIndexOf(object value, int startIndex)
			{
				object root = this._root;
				int result;
				lock (root)
				{
					result = this._list.LastIndexOf(value, startIndex);
				}
				return result;
			}

			// Token: 0x06005820 RID: 22560 RVA: 0x0011EA18 File Offset: 0x0011CC18
			public override int LastIndexOf(object value, int startIndex, int count)
			{
				object root = this._root;
				int result;
				lock (root)
				{
					result = this._list.LastIndexOf(value, startIndex, count);
				}
				return result;
			}

			// Token: 0x06005821 RID: 22561 RVA: 0x0011EA64 File Offset: 0x0011CC64
			public override void Remove(object value)
			{
				object root = this._root;
				lock (root)
				{
					this._list.Remove(value);
				}
			}

			// Token: 0x06005822 RID: 22562 RVA: 0x0011EAAC File Offset: 0x0011CCAC
			public override void RemoveAt(int index)
			{
				object root = this._root;
				lock (root)
				{
					this._list.RemoveAt(index);
				}
			}

			// Token: 0x06005823 RID: 22563 RVA: 0x0011EAF4 File Offset: 0x0011CCF4
			public override void RemoveRange(int index, int count)
			{
				object root = this._root;
				lock (root)
				{
					this._list.RemoveRange(index, count);
				}
			}

			// Token: 0x06005824 RID: 22564 RVA: 0x0011EB3C File Offset: 0x0011CD3C
			public override void Reverse(int index, int count)
			{
				object root = this._root;
				lock (root)
				{
					this._list.Reverse(index, count);
				}
			}

			// Token: 0x06005825 RID: 22565 RVA: 0x0011EB84 File Offset: 0x0011CD84
			public override void SetRange(int index, ICollection c)
			{
				object root = this._root;
				lock (root)
				{
					this._list.SetRange(index, c);
				}
			}

			// Token: 0x06005826 RID: 22566 RVA: 0x0011EBCC File Offset: 0x0011CDCC
			public override ArrayList GetRange(int index, int count)
			{
				object root = this._root;
				ArrayList range;
				lock (root)
				{
					range = this._list.GetRange(index, count);
				}
				return range;
			}

			// Token: 0x06005827 RID: 22567 RVA: 0x0011EC18 File Offset: 0x0011CE18
			public override void Sort()
			{
				object root = this._root;
				lock (root)
				{
					this._list.Sort();
				}
			}

			// Token: 0x06005828 RID: 22568 RVA: 0x0011EC60 File Offset: 0x0011CE60
			public override void Sort(IComparer comparer)
			{
				object root = this._root;
				lock (root)
				{
					this._list.Sort(comparer);
				}
			}

			// Token: 0x06005829 RID: 22569 RVA: 0x0011ECA8 File Offset: 0x0011CEA8
			public override void Sort(int index, int count, IComparer comparer)
			{
				object root = this._root;
				lock (root)
				{
					this._list.Sort(index, count, comparer);
				}
			}

			// Token: 0x0600582A RID: 22570 RVA: 0x0011ECF0 File Offset: 0x0011CEF0
			public override object[] ToArray()
			{
				object root = this._root;
				object[] result;
				lock (root)
				{
					result = this._list.ToArray();
				}
				return result;
			}

			// Token: 0x0600582B RID: 22571 RVA: 0x0011ED38 File Offset: 0x0011CF38
			public override Array ToArray(Type type)
			{
				object root = this._root;
				Array result;
				lock (root)
				{
					result = this._list.ToArray(type);
				}
				return result;
			}

			// Token: 0x0600582C RID: 22572 RVA: 0x0011ED80 File Offset: 0x0011CF80
			public override void TrimToSize()
			{
				object root = this._root;
				lock (root)
				{
					this._list.TrimToSize();
				}
			}

			// Token: 0x04002D2C RID: 11564
			private ArrayList _list;

			// Token: 0x04002D2D RID: 11565
			private object _root;
		}

		// Token: 0x02000995 RID: 2453
		[Serializable]
		private class SyncIList : IList, ICollection, IEnumerable
		{
			// Token: 0x0600582D RID: 22573 RVA: 0x0011EDC8 File Offset: 0x0011CFC8
			internal SyncIList(IList list)
			{
				this._list = list;
				this._root = list.SyncRoot;
			}

			// Token: 0x17000F6F RID: 3951
			// (get) Token: 0x0600582E RID: 22574 RVA: 0x0011EDE4 File Offset: 0x0011CFE4
			public virtual int Count
			{
				get
				{
					object root = this._root;
					int count;
					lock (root)
					{
						count = this._list.Count;
					}
					return count;
				}
			}

			// Token: 0x17000F70 RID: 3952
			// (get) Token: 0x0600582F RID: 22575 RVA: 0x0011EE2C File Offset: 0x0011D02C
			public virtual bool IsReadOnly
			{
				get
				{
					return this._list.IsReadOnly;
				}
			}

			// Token: 0x17000F71 RID: 3953
			// (get) Token: 0x06005830 RID: 22576 RVA: 0x0011EE39 File Offset: 0x0011D039
			public virtual bool IsFixedSize
			{
				get
				{
					return this._list.IsFixedSize;
				}
			}

			// Token: 0x17000F72 RID: 3954
			// (get) Token: 0x06005831 RID: 22577 RVA: 0x00004E08 File Offset: 0x00003008
			public virtual bool IsSynchronized
			{
				get
				{
					return true;
				}
			}

			// Token: 0x17000F73 RID: 3955
			public virtual object this[int index]
			{
				get
				{
					object root = this._root;
					object result;
					lock (root)
					{
						result = this._list[index];
					}
					return result;
				}
				set
				{
					object root = this._root;
					lock (root)
					{
						this._list[index] = value;
					}
				}
			}

			// Token: 0x17000F74 RID: 3956
			// (get) Token: 0x06005834 RID: 22580 RVA: 0x0011EED8 File Offset: 0x0011D0D8
			public virtual object SyncRoot
			{
				get
				{
					return this._root;
				}
			}

			// Token: 0x06005835 RID: 22581 RVA: 0x0011EEE0 File Offset: 0x0011D0E0
			public virtual int Add(object value)
			{
				object root = this._root;
				int result;
				lock (root)
				{
					result = this._list.Add(value);
				}
				return result;
			}

			// Token: 0x06005836 RID: 22582 RVA: 0x0011EF28 File Offset: 0x0011D128
			public virtual void Clear()
			{
				object root = this._root;
				lock (root)
				{
					this._list.Clear();
				}
			}

			// Token: 0x06005837 RID: 22583 RVA: 0x0011EF70 File Offset: 0x0011D170
			public virtual bool Contains(object item)
			{
				object root = this._root;
				bool result;
				lock (root)
				{
					result = this._list.Contains(item);
				}
				return result;
			}

			// Token: 0x06005838 RID: 22584 RVA: 0x0011EFB8 File Offset: 0x0011D1B8
			public virtual void CopyTo(Array array, int index)
			{
				object root = this._root;
				lock (root)
				{
					this._list.CopyTo(array, index);
				}
			}

			// Token: 0x06005839 RID: 22585 RVA: 0x0011F000 File Offset: 0x0011D200
			public virtual IEnumerator GetEnumerator()
			{
				object root = this._root;
				IEnumerator enumerator;
				lock (root)
				{
					enumerator = this._list.GetEnumerator();
				}
				return enumerator;
			}

			// Token: 0x0600583A RID: 22586 RVA: 0x0011F048 File Offset: 0x0011D248
			public virtual int IndexOf(object value)
			{
				object root = this._root;
				int result;
				lock (root)
				{
					result = this._list.IndexOf(value);
				}
				return result;
			}

			// Token: 0x0600583B RID: 22587 RVA: 0x0011F090 File Offset: 0x0011D290
			public virtual void Insert(int index, object value)
			{
				object root = this._root;
				lock (root)
				{
					this._list.Insert(index, value);
				}
			}

			// Token: 0x0600583C RID: 22588 RVA: 0x0011F0D8 File Offset: 0x0011D2D8
			public virtual void Remove(object value)
			{
				object root = this._root;
				lock (root)
				{
					this._list.Remove(value);
				}
			}

			// Token: 0x0600583D RID: 22589 RVA: 0x0011F120 File Offset: 0x0011D320
			public virtual void RemoveAt(int index)
			{
				object root = this._root;
				lock (root)
				{
					this._list.RemoveAt(index);
				}
			}

			// Token: 0x04002D2E RID: 11566
			private IList _list;

			// Token: 0x04002D2F RID: 11567
			private object _root;
		}

		// Token: 0x02000996 RID: 2454
		[Serializable]
		private class FixedSizeList : IList, ICollection, IEnumerable
		{
			// Token: 0x0600583E RID: 22590 RVA: 0x0011F168 File Offset: 0x0011D368
			internal FixedSizeList(IList l)
			{
				this._list = l;
			}

			// Token: 0x17000F75 RID: 3957
			// (get) Token: 0x0600583F RID: 22591 RVA: 0x0011F177 File Offset: 0x0011D377
			public virtual int Count
			{
				get
				{
					return this._list.Count;
				}
			}

			// Token: 0x17000F76 RID: 3958
			// (get) Token: 0x06005840 RID: 22592 RVA: 0x0011F184 File Offset: 0x0011D384
			public virtual bool IsReadOnly
			{
				get
				{
					return this._list.IsReadOnly;
				}
			}

			// Token: 0x17000F77 RID: 3959
			// (get) Token: 0x06005841 RID: 22593 RVA: 0x00004E08 File Offset: 0x00003008
			public virtual bool IsFixedSize
			{
				get
				{
					return true;
				}
			}

			// Token: 0x17000F78 RID: 3960
			// (get) Token: 0x06005842 RID: 22594 RVA: 0x0011F191 File Offset: 0x0011D391
			public virtual bool IsSynchronized
			{
				get
				{
					return this._list.IsSynchronized;
				}
			}

			// Token: 0x17000F79 RID: 3961
			public virtual object this[int index]
			{
				get
				{
					return this._list[index];
				}
				set
				{
					this._list[index] = value;
				}
			}

			// Token: 0x17000F7A RID: 3962
			// (get) Token: 0x06005845 RID: 22597 RVA: 0x0011F1BB File Offset: 0x0011D3BB
			public virtual object SyncRoot
			{
				get
				{
					return this._list.SyncRoot;
				}
			}

			// Token: 0x06005846 RID: 22598 RVA: 0x0011F1C8 File Offset: 0x0011D3C8
			public virtual int Add(object obj)
			{
				throw new NotSupportedException(Environment.GetResourceString("Collection was of a fixed size."));
			}

			// Token: 0x06005847 RID: 22599 RVA: 0x0011F1C8 File Offset: 0x0011D3C8
			public virtual void Clear()
			{
				throw new NotSupportedException(Environment.GetResourceString("Collection was of a fixed size."));
			}

			// Token: 0x06005848 RID: 22600 RVA: 0x0011F1D9 File Offset: 0x0011D3D9
			public virtual bool Contains(object obj)
			{
				return this._list.Contains(obj);
			}

			// Token: 0x06005849 RID: 22601 RVA: 0x0011F1E7 File Offset: 0x0011D3E7
			public virtual void CopyTo(Array array, int index)
			{
				this._list.CopyTo(array, index);
			}

			// Token: 0x0600584A RID: 22602 RVA: 0x0011F1F6 File Offset: 0x0011D3F6
			public virtual IEnumerator GetEnumerator()
			{
				return this._list.GetEnumerator();
			}

			// Token: 0x0600584B RID: 22603 RVA: 0x0011F203 File Offset: 0x0011D403
			public virtual int IndexOf(object value)
			{
				return this._list.IndexOf(value);
			}

			// Token: 0x0600584C RID: 22604 RVA: 0x0011F1C8 File Offset: 0x0011D3C8
			public virtual void Insert(int index, object obj)
			{
				throw new NotSupportedException(Environment.GetResourceString("Collection was of a fixed size."));
			}

			// Token: 0x0600584D RID: 22605 RVA: 0x0011F1C8 File Offset: 0x0011D3C8
			public virtual void Remove(object value)
			{
				throw new NotSupportedException(Environment.GetResourceString("Collection was of a fixed size."));
			}

			// Token: 0x0600584E RID: 22606 RVA: 0x0011F1C8 File Offset: 0x0011D3C8
			public virtual void RemoveAt(int index)
			{
				throw new NotSupportedException(Environment.GetResourceString("Collection was of a fixed size."));
			}

			// Token: 0x04002D30 RID: 11568
			private IList _list;
		}

		// Token: 0x02000997 RID: 2455
		[Serializable]
		private class FixedSizeArrayList : ArrayList
		{
			// Token: 0x0600584F RID: 22607 RVA: 0x0011F211 File Offset: 0x0011D411
			internal FixedSizeArrayList(ArrayList l)
			{
				this._list = l;
				this._version = this._list._version;
			}

			// Token: 0x17000F7B RID: 3963
			// (get) Token: 0x06005850 RID: 22608 RVA: 0x0011F231 File Offset: 0x0011D431
			public override int Count
			{
				get
				{
					return this._list.Count;
				}
			}

			// Token: 0x17000F7C RID: 3964
			// (get) Token: 0x06005851 RID: 22609 RVA: 0x0011F23E File Offset: 0x0011D43E
			public override bool IsReadOnly
			{
				get
				{
					return this._list.IsReadOnly;
				}
			}

			// Token: 0x17000F7D RID: 3965
			// (get) Token: 0x06005852 RID: 22610 RVA: 0x00004E08 File Offset: 0x00003008
			public override bool IsFixedSize
			{
				get
				{
					return true;
				}
			}

			// Token: 0x17000F7E RID: 3966
			// (get) Token: 0x06005853 RID: 22611 RVA: 0x0011F24B File Offset: 0x0011D44B
			public override bool IsSynchronized
			{
				get
				{
					return this._list.IsSynchronized;
				}
			}

			// Token: 0x17000F7F RID: 3967
			public override object this[int index]
			{
				get
				{
					return this._list[index];
				}
				set
				{
					this._list[index] = value;
					this._version = this._list._version;
				}
			}

			// Token: 0x17000F80 RID: 3968
			// (get) Token: 0x06005856 RID: 22614 RVA: 0x0011F286 File Offset: 0x0011D486
			public override object SyncRoot
			{
				get
				{
					return this._list.SyncRoot;
				}
			}

			// Token: 0x06005857 RID: 22615 RVA: 0x0011F1C8 File Offset: 0x0011D3C8
			public override int Add(object obj)
			{
				throw new NotSupportedException(Environment.GetResourceString("Collection was of a fixed size."));
			}

			// Token: 0x06005858 RID: 22616 RVA: 0x0011F1C8 File Offset: 0x0011D3C8
			public override void AddRange(ICollection c)
			{
				throw new NotSupportedException(Environment.GetResourceString("Collection was of a fixed size."));
			}

			// Token: 0x06005859 RID: 22617 RVA: 0x0011F293 File Offset: 0x0011D493
			public override int BinarySearch(int index, int count, object value, IComparer comparer)
			{
				return this._list.BinarySearch(index, count, value, comparer);
			}

			// Token: 0x17000F81 RID: 3969
			// (get) Token: 0x0600585A RID: 22618 RVA: 0x0011F2A5 File Offset: 0x0011D4A5
			// (set) Token: 0x0600585B RID: 22619 RVA: 0x0011F1C8 File Offset: 0x0011D3C8
			public override int Capacity
			{
				get
				{
					return this._list.Capacity;
				}
				set
				{
					throw new NotSupportedException(Environment.GetResourceString("Collection was of a fixed size."));
				}
			}

			// Token: 0x0600585C RID: 22620 RVA: 0x0011F1C8 File Offset: 0x0011D3C8
			public override void Clear()
			{
				throw new NotSupportedException(Environment.GetResourceString("Collection was of a fixed size."));
			}

			// Token: 0x0600585D RID: 22621 RVA: 0x0011F2B2 File Offset: 0x0011D4B2
			public override object Clone()
			{
				return new ArrayList.FixedSizeArrayList(this._list)
				{
					_list = (ArrayList)this._list.Clone()
				};
			}

			// Token: 0x0600585E RID: 22622 RVA: 0x0011F2D5 File Offset: 0x0011D4D5
			public override bool Contains(object obj)
			{
				return this._list.Contains(obj);
			}

			// Token: 0x0600585F RID: 22623 RVA: 0x0011F2E3 File Offset: 0x0011D4E3
			public override void CopyTo(Array array, int index)
			{
				this._list.CopyTo(array, index);
			}

			// Token: 0x06005860 RID: 22624 RVA: 0x0011F2F2 File Offset: 0x0011D4F2
			public override void CopyTo(int index, Array array, int arrayIndex, int count)
			{
				this._list.CopyTo(index, array, arrayIndex, count);
			}

			// Token: 0x06005861 RID: 22625 RVA: 0x0011F304 File Offset: 0x0011D504
			public override IEnumerator GetEnumerator()
			{
				return this._list.GetEnumerator();
			}

			// Token: 0x06005862 RID: 22626 RVA: 0x0011F311 File Offset: 0x0011D511
			public override IEnumerator GetEnumerator(int index, int count)
			{
				return this._list.GetEnumerator(index, count);
			}

			// Token: 0x06005863 RID: 22627 RVA: 0x0011F320 File Offset: 0x0011D520
			public override int IndexOf(object value)
			{
				return this._list.IndexOf(value);
			}

			// Token: 0x06005864 RID: 22628 RVA: 0x0011F32E File Offset: 0x0011D52E
			public override int IndexOf(object value, int startIndex)
			{
				return this._list.IndexOf(value, startIndex);
			}

			// Token: 0x06005865 RID: 22629 RVA: 0x0011F33D File Offset: 0x0011D53D
			public override int IndexOf(object value, int startIndex, int count)
			{
				return this._list.IndexOf(value, startIndex, count);
			}

			// Token: 0x06005866 RID: 22630 RVA: 0x0011F1C8 File Offset: 0x0011D3C8
			public override void Insert(int index, object obj)
			{
				throw new NotSupportedException(Environment.GetResourceString("Collection was of a fixed size."));
			}

			// Token: 0x06005867 RID: 22631 RVA: 0x0011F1C8 File Offset: 0x0011D3C8
			public override void InsertRange(int index, ICollection c)
			{
				throw new NotSupportedException(Environment.GetResourceString("Collection was of a fixed size."));
			}

			// Token: 0x06005868 RID: 22632 RVA: 0x0011F34D File Offset: 0x0011D54D
			public override int LastIndexOf(object value)
			{
				return this._list.LastIndexOf(value);
			}

			// Token: 0x06005869 RID: 22633 RVA: 0x0011F35B File Offset: 0x0011D55B
			public override int LastIndexOf(object value, int startIndex)
			{
				return this._list.LastIndexOf(value, startIndex);
			}

			// Token: 0x0600586A RID: 22634 RVA: 0x0011F36A File Offset: 0x0011D56A
			public override int LastIndexOf(object value, int startIndex, int count)
			{
				return this._list.LastIndexOf(value, startIndex, count);
			}

			// Token: 0x0600586B RID: 22635 RVA: 0x0011F1C8 File Offset: 0x0011D3C8
			public override void Remove(object value)
			{
				throw new NotSupportedException(Environment.GetResourceString("Collection was of a fixed size."));
			}

			// Token: 0x0600586C RID: 22636 RVA: 0x0011F1C8 File Offset: 0x0011D3C8
			public override void RemoveAt(int index)
			{
				throw new NotSupportedException(Environment.GetResourceString("Collection was of a fixed size."));
			}

			// Token: 0x0600586D RID: 22637 RVA: 0x0011F1C8 File Offset: 0x0011D3C8
			public override void RemoveRange(int index, int count)
			{
				throw new NotSupportedException(Environment.GetResourceString("Collection was of a fixed size."));
			}

			// Token: 0x0600586E RID: 22638 RVA: 0x0011F37A File Offset: 0x0011D57A
			public override void SetRange(int index, ICollection c)
			{
				this._list.SetRange(index, c);
				this._version = this._list._version;
			}

			// Token: 0x0600586F RID: 22639 RVA: 0x0011F39C File Offset: 0x0011D59C
			public override ArrayList GetRange(int index, int count)
			{
				if (index < 0 || count < 0)
				{
					throw new ArgumentOutOfRangeException((index < 0) ? "index" : "count", Environment.GetResourceString("Non-negative number required."));
				}
				if (this.Count - index < count)
				{
					throw new ArgumentException(Environment.GetResourceString("Offset and length were out of bounds for the array or count is greater than the number of elements from index to the end of the source collection."));
				}
				return new ArrayList.Range(this, index, count);
			}

			// Token: 0x06005870 RID: 22640 RVA: 0x0011F3F4 File Offset: 0x0011D5F4
			public override void Reverse(int index, int count)
			{
				this._list.Reverse(index, count);
				this._version = this._list._version;
			}

			// Token: 0x06005871 RID: 22641 RVA: 0x0011F414 File Offset: 0x0011D614
			public override void Sort(int index, int count, IComparer comparer)
			{
				this._list.Sort(index, count, comparer);
				this._version = this._list._version;
			}

			// Token: 0x06005872 RID: 22642 RVA: 0x0011F435 File Offset: 0x0011D635
			public override object[] ToArray()
			{
				return this._list.ToArray();
			}

			// Token: 0x06005873 RID: 22643 RVA: 0x0011F442 File Offset: 0x0011D642
			public override Array ToArray(Type type)
			{
				return this._list.ToArray(type);
			}

			// Token: 0x06005874 RID: 22644 RVA: 0x0011F1C8 File Offset: 0x0011D3C8
			public override void TrimToSize()
			{
				throw new NotSupportedException(Environment.GetResourceString("Collection was of a fixed size."));
			}

			// Token: 0x04002D31 RID: 11569
			private ArrayList _list;
		}

		// Token: 0x02000998 RID: 2456
		[Serializable]
		private class ReadOnlyList : IList, ICollection, IEnumerable
		{
			// Token: 0x06005875 RID: 22645 RVA: 0x0011F450 File Offset: 0x0011D650
			internal ReadOnlyList(IList l)
			{
				this._list = l;
			}

			// Token: 0x17000F82 RID: 3970
			// (get) Token: 0x06005876 RID: 22646 RVA: 0x0011F45F File Offset: 0x0011D65F
			public virtual int Count
			{
				get
				{
					return this._list.Count;
				}
			}

			// Token: 0x17000F83 RID: 3971
			// (get) Token: 0x06005877 RID: 22647 RVA: 0x00004E08 File Offset: 0x00003008
			public virtual bool IsReadOnly
			{
				get
				{
					return true;
				}
			}

			// Token: 0x17000F84 RID: 3972
			// (get) Token: 0x06005878 RID: 22648 RVA: 0x00004E08 File Offset: 0x00003008
			public virtual bool IsFixedSize
			{
				get
				{
					return true;
				}
			}

			// Token: 0x17000F85 RID: 3973
			// (get) Token: 0x06005879 RID: 22649 RVA: 0x0011F46C File Offset: 0x0011D66C
			public virtual bool IsSynchronized
			{
				get
				{
					return this._list.IsSynchronized;
				}
			}

			// Token: 0x17000F86 RID: 3974
			public virtual object this[int index]
			{
				get
				{
					return this._list[index];
				}
				set
				{
					throw new NotSupportedException(Environment.GetResourceString("Collection is read-only."));
				}
			}

			// Token: 0x17000F87 RID: 3975
			// (get) Token: 0x0600587C RID: 22652 RVA: 0x0011F498 File Offset: 0x0011D698
			public virtual object SyncRoot
			{
				get
				{
					return this._list.SyncRoot;
				}
			}

			// Token: 0x0600587D RID: 22653 RVA: 0x0011F487 File Offset: 0x0011D687
			public virtual int Add(object obj)
			{
				throw new NotSupportedException(Environment.GetResourceString("Collection is read-only."));
			}

			// Token: 0x0600587E RID: 22654 RVA: 0x0011F487 File Offset: 0x0011D687
			public virtual void Clear()
			{
				throw new NotSupportedException(Environment.GetResourceString("Collection is read-only."));
			}

			// Token: 0x0600587F RID: 22655 RVA: 0x0011F4A5 File Offset: 0x0011D6A5
			public virtual bool Contains(object obj)
			{
				return this._list.Contains(obj);
			}

			// Token: 0x06005880 RID: 22656 RVA: 0x0011F4B3 File Offset: 0x0011D6B3
			public virtual void CopyTo(Array array, int index)
			{
				this._list.CopyTo(array, index);
			}

			// Token: 0x06005881 RID: 22657 RVA: 0x0011F4C2 File Offset: 0x0011D6C2
			public virtual IEnumerator GetEnumerator()
			{
				return this._list.GetEnumerator();
			}

			// Token: 0x06005882 RID: 22658 RVA: 0x0011F4CF File Offset: 0x0011D6CF
			public virtual int IndexOf(object value)
			{
				return this._list.IndexOf(value);
			}

			// Token: 0x06005883 RID: 22659 RVA: 0x0011F487 File Offset: 0x0011D687
			public virtual void Insert(int index, object obj)
			{
				throw new NotSupportedException(Environment.GetResourceString("Collection is read-only."));
			}

			// Token: 0x06005884 RID: 22660 RVA: 0x0011F487 File Offset: 0x0011D687
			public virtual void Remove(object value)
			{
				throw new NotSupportedException(Environment.GetResourceString("Collection is read-only."));
			}

			// Token: 0x06005885 RID: 22661 RVA: 0x0011F487 File Offset: 0x0011D687
			public virtual void RemoveAt(int index)
			{
				throw new NotSupportedException(Environment.GetResourceString("Collection is read-only."));
			}

			// Token: 0x04002D32 RID: 11570
			private IList _list;
		}

		// Token: 0x02000999 RID: 2457
		[Serializable]
		private class ReadOnlyArrayList : ArrayList
		{
			// Token: 0x06005886 RID: 22662 RVA: 0x0011F4DD File Offset: 0x0011D6DD
			internal ReadOnlyArrayList(ArrayList l)
			{
				this._list = l;
			}

			// Token: 0x17000F88 RID: 3976
			// (get) Token: 0x06005887 RID: 22663 RVA: 0x0011F4EC File Offset: 0x0011D6EC
			public override int Count
			{
				get
				{
					return this._list.Count;
				}
			}

			// Token: 0x17000F89 RID: 3977
			// (get) Token: 0x06005888 RID: 22664 RVA: 0x00004E08 File Offset: 0x00003008
			public override bool IsReadOnly
			{
				get
				{
					return true;
				}
			}

			// Token: 0x17000F8A RID: 3978
			// (get) Token: 0x06005889 RID: 22665 RVA: 0x00004E08 File Offset: 0x00003008
			public override bool IsFixedSize
			{
				get
				{
					return true;
				}
			}

			// Token: 0x17000F8B RID: 3979
			// (get) Token: 0x0600588A RID: 22666 RVA: 0x0011F4F9 File Offset: 0x0011D6F9
			public override bool IsSynchronized
			{
				get
				{
					return this._list.IsSynchronized;
				}
			}

			// Token: 0x17000F8C RID: 3980
			public override object this[int index]
			{
				get
				{
					return this._list[index];
				}
				set
				{
					throw new NotSupportedException(Environment.GetResourceString("Collection is read-only."));
				}
			}

			// Token: 0x17000F8D RID: 3981
			// (get) Token: 0x0600588D RID: 22669 RVA: 0x0011F514 File Offset: 0x0011D714
			public override object SyncRoot
			{
				get
				{
					return this._list.SyncRoot;
				}
			}

			// Token: 0x0600588E RID: 22670 RVA: 0x0011F487 File Offset: 0x0011D687
			public override int Add(object obj)
			{
				throw new NotSupportedException(Environment.GetResourceString("Collection is read-only."));
			}

			// Token: 0x0600588F RID: 22671 RVA: 0x0011F487 File Offset: 0x0011D687
			public override void AddRange(ICollection c)
			{
				throw new NotSupportedException(Environment.GetResourceString("Collection is read-only."));
			}

			// Token: 0x06005890 RID: 22672 RVA: 0x0011F521 File Offset: 0x0011D721
			public override int BinarySearch(int index, int count, object value, IComparer comparer)
			{
				return this._list.BinarySearch(index, count, value, comparer);
			}

			// Token: 0x17000F8E RID: 3982
			// (get) Token: 0x06005891 RID: 22673 RVA: 0x0011F533 File Offset: 0x0011D733
			// (set) Token: 0x06005892 RID: 22674 RVA: 0x0011F487 File Offset: 0x0011D687
			public override int Capacity
			{
				get
				{
					return this._list.Capacity;
				}
				set
				{
					throw new NotSupportedException(Environment.GetResourceString("Collection is read-only."));
				}
			}

			// Token: 0x06005893 RID: 22675 RVA: 0x0011F487 File Offset: 0x0011D687
			public override void Clear()
			{
				throw new NotSupportedException(Environment.GetResourceString("Collection is read-only."));
			}

			// Token: 0x06005894 RID: 22676 RVA: 0x0011F540 File Offset: 0x0011D740
			public override object Clone()
			{
				return new ArrayList.ReadOnlyArrayList(this._list)
				{
					_list = (ArrayList)this._list.Clone()
				};
			}

			// Token: 0x06005895 RID: 22677 RVA: 0x0011F563 File Offset: 0x0011D763
			public override bool Contains(object obj)
			{
				return this._list.Contains(obj);
			}

			// Token: 0x06005896 RID: 22678 RVA: 0x0011F571 File Offset: 0x0011D771
			public override void CopyTo(Array array, int index)
			{
				this._list.CopyTo(array, index);
			}

			// Token: 0x06005897 RID: 22679 RVA: 0x0011F580 File Offset: 0x0011D780
			public override void CopyTo(int index, Array array, int arrayIndex, int count)
			{
				this._list.CopyTo(index, array, arrayIndex, count);
			}

			// Token: 0x06005898 RID: 22680 RVA: 0x0011F592 File Offset: 0x0011D792
			public override IEnumerator GetEnumerator()
			{
				return this._list.GetEnumerator();
			}

			// Token: 0x06005899 RID: 22681 RVA: 0x0011F59F File Offset: 0x0011D79F
			public override IEnumerator GetEnumerator(int index, int count)
			{
				return this._list.GetEnumerator(index, count);
			}

			// Token: 0x0600589A RID: 22682 RVA: 0x0011F5AE File Offset: 0x0011D7AE
			public override int IndexOf(object value)
			{
				return this._list.IndexOf(value);
			}

			// Token: 0x0600589B RID: 22683 RVA: 0x0011F5BC File Offset: 0x0011D7BC
			public override int IndexOf(object value, int startIndex)
			{
				return this._list.IndexOf(value, startIndex);
			}

			// Token: 0x0600589C RID: 22684 RVA: 0x0011F5CB File Offset: 0x0011D7CB
			public override int IndexOf(object value, int startIndex, int count)
			{
				return this._list.IndexOf(value, startIndex, count);
			}

			// Token: 0x0600589D RID: 22685 RVA: 0x0011F487 File Offset: 0x0011D687
			public override void Insert(int index, object obj)
			{
				throw new NotSupportedException(Environment.GetResourceString("Collection is read-only."));
			}

			// Token: 0x0600589E RID: 22686 RVA: 0x0011F487 File Offset: 0x0011D687
			public override void InsertRange(int index, ICollection c)
			{
				throw new NotSupportedException(Environment.GetResourceString("Collection is read-only."));
			}

			// Token: 0x0600589F RID: 22687 RVA: 0x0011F5DB File Offset: 0x0011D7DB
			public override int LastIndexOf(object value)
			{
				return this._list.LastIndexOf(value);
			}

			// Token: 0x060058A0 RID: 22688 RVA: 0x0011F5E9 File Offset: 0x0011D7E9
			public override int LastIndexOf(object value, int startIndex)
			{
				return this._list.LastIndexOf(value, startIndex);
			}

			// Token: 0x060058A1 RID: 22689 RVA: 0x0011F5F8 File Offset: 0x0011D7F8
			public override int LastIndexOf(object value, int startIndex, int count)
			{
				return this._list.LastIndexOf(value, startIndex, count);
			}

			// Token: 0x060058A2 RID: 22690 RVA: 0x0011F487 File Offset: 0x0011D687
			public override void Remove(object value)
			{
				throw new NotSupportedException(Environment.GetResourceString("Collection is read-only."));
			}

			// Token: 0x060058A3 RID: 22691 RVA: 0x0011F487 File Offset: 0x0011D687
			public override void RemoveAt(int index)
			{
				throw new NotSupportedException(Environment.GetResourceString("Collection is read-only."));
			}

			// Token: 0x060058A4 RID: 22692 RVA: 0x0011F487 File Offset: 0x0011D687
			public override void RemoveRange(int index, int count)
			{
				throw new NotSupportedException(Environment.GetResourceString("Collection is read-only."));
			}

			// Token: 0x060058A5 RID: 22693 RVA: 0x0011F487 File Offset: 0x0011D687
			public override void SetRange(int index, ICollection c)
			{
				throw new NotSupportedException(Environment.GetResourceString("Collection is read-only."));
			}

			// Token: 0x060058A6 RID: 22694 RVA: 0x0011F608 File Offset: 0x0011D808
			public override ArrayList GetRange(int index, int count)
			{
				if (index < 0 || count < 0)
				{
					throw new ArgumentOutOfRangeException((index < 0) ? "index" : "count", Environment.GetResourceString("Non-negative number required."));
				}
				if (this.Count - index < count)
				{
					throw new ArgumentException(Environment.GetResourceString("Offset and length were out of bounds for the array or count is greater than the number of elements from index to the end of the source collection."));
				}
				return new ArrayList.Range(this, index, count);
			}

			// Token: 0x060058A7 RID: 22695 RVA: 0x0011F487 File Offset: 0x0011D687
			public override void Reverse(int index, int count)
			{
				throw new NotSupportedException(Environment.GetResourceString("Collection is read-only."));
			}

			// Token: 0x060058A8 RID: 22696 RVA: 0x0011F487 File Offset: 0x0011D687
			public override void Sort(int index, int count, IComparer comparer)
			{
				throw new NotSupportedException(Environment.GetResourceString("Collection is read-only."));
			}

			// Token: 0x060058A9 RID: 22697 RVA: 0x0011F660 File Offset: 0x0011D860
			public override object[] ToArray()
			{
				return this._list.ToArray();
			}

			// Token: 0x060058AA RID: 22698 RVA: 0x0011F66D File Offset: 0x0011D86D
			public override Array ToArray(Type type)
			{
				return this._list.ToArray(type);
			}

			// Token: 0x060058AB RID: 22699 RVA: 0x0011F487 File Offset: 0x0011D687
			public override void TrimToSize()
			{
				throw new NotSupportedException(Environment.GetResourceString("Collection is read-only."));
			}

			// Token: 0x04002D33 RID: 11571
			private ArrayList _list;
		}

		// Token: 0x0200099A RID: 2458
		[Serializable]
		private sealed class ArrayListEnumerator : IEnumerator, ICloneable
		{
			// Token: 0x060058AC RID: 22700 RVA: 0x0011F67B File Offset: 0x0011D87B
			internal ArrayListEnumerator(ArrayList list, int index, int count)
			{
				this.list = list;
				this.startIndex = index;
				this.index = index - 1;
				this.endIndex = this.index + count;
				this.version = list._version;
				this.currentElement = null;
			}

			// Token: 0x060058AD RID: 22701 RVA: 0x0002BE93 File Offset: 0x0002A093
			public object Clone()
			{
				return base.MemberwiseClone();
			}

			// Token: 0x060058AE RID: 22702 RVA: 0x0011F6BC File Offset: 0x0011D8BC
			public bool MoveNext()
			{
				if (this.version != this.list._version)
				{
					throw new InvalidOperationException(Environment.GetResourceString("Collection was modified; enumeration operation may not execute."));
				}
				if (this.index < this.endIndex)
				{
					ArrayList arrayList = this.list;
					int num = this.index + 1;
					this.index = num;
					this.currentElement = arrayList[num];
					return true;
				}
				this.index = this.endIndex + 1;
				return false;
			}

			// Token: 0x17000F8F RID: 3983
			// (get) Token: 0x060058AF RID: 22703 RVA: 0x0011F730 File Offset: 0x0011D930
			public object Current
			{
				get
				{
					if (this.index < this.startIndex)
					{
						throw new InvalidOperationException(Environment.GetResourceString("Enumeration has not started. Call MoveNext."));
					}
					if (this.index > this.endIndex)
					{
						throw new InvalidOperationException(Environment.GetResourceString("Enumeration already finished."));
					}
					return this.currentElement;
				}
			}

			// Token: 0x060058B0 RID: 22704 RVA: 0x0011F77F File Offset: 0x0011D97F
			public void Reset()
			{
				if (this.version != this.list._version)
				{
					throw new InvalidOperationException(Environment.GetResourceString("Collection was modified; enumeration operation may not execute."));
				}
				this.index = this.startIndex - 1;
			}

			// Token: 0x04002D34 RID: 11572
			private ArrayList list;

			// Token: 0x04002D35 RID: 11573
			private int index;

			// Token: 0x04002D36 RID: 11574
			private int endIndex;

			// Token: 0x04002D37 RID: 11575
			private int version;

			// Token: 0x04002D38 RID: 11576
			private object currentElement;

			// Token: 0x04002D39 RID: 11577
			private int startIndex;
		}

		// Token: 0x0200099B RID: 2459
		[Serializable]
		private class Range : ArrayList
		{
			// Token: 0x060058B1 RID: 22705 RVA: 0x0011F7B2 File Offset: 0x0011D9B2
			internal Range(ArrayList list, int index, int count) : base(false)
			{
				this._baseList = list;
				this._baseIndex = index;
				this._baseSize = count;
				this._baseVersion = list._version;
				this._version = list._version;
			}

			// Token: 0x060058B2 RID: 22706 RVA: 0x0011F7E8 File Offset: 0x0011D9E8
			private void InternalUpdateRange()
			{
				if (this._baseVersion != this._baseList._version)
				{
					throw new InvalidOperationException(Environment.GetResourceString("This range in the underlying list is invalid. A possible cause is that elements were removed."));
				}
			}

			// Token: 0x060058B3 RID: 22707 RVA: 0x0011F80D File Offset: 0x0011DA0D
			private void InternalUpdateVersion()
			{
				this._baseVersion++;
				this._version++;
			}

			// Token: 0x060058B4 RID: 22708 RVA: 0x0011F82C File Offset: 0x0011DA2C
			public override int Add(object value)
			{
				this.InternalUpdateRange();
				this._baseList.Insert(this._baseIndex + this._baseSize, value);
				this.InternalUpdateVersion();
				int baseSize = this._baseSize;
				this._baseSize = baseSize + 1;
				return baseSize;
			}

			// Token: 0x060058B5 RID: 22709 RVA: 0x0011F870 File Offset: 0x0011DA70
			public override void AddRange(ICollection c)
			{
				if (c == null)
				{
					throw new ArgumentNullException("c");
				}
				this.InternalUpdateRange();
				int count = c.Count;
				if (count > 0)
				{
					this._baseList.InsertRange(this._baseIndex + this._baseSize, c);
					this.InternalUpdateVersion();
					this._baseSize += count;
				}
			}

			// Token: 0x060058B6 RID: 22710 RVA: 0x0011F8CC File Offset: 0x0011DACC
			public override int BinarySearch(int index, int count, object value, IComparer comparer)
			{
				if (index < 0 || count < 0)
				{
					throw new ArgumentOutOfRangeException((index < 0) ? "index" : "count", Environment.GetResourceString("Non-negative number required."));
				}
				if (this._baseSize - index < count)
				{
					throw new ArgumentException(Environment.GetResourceString("Offset and length were out of bounds for the array or count is greater than the number of elements from index to the end of the source collection."));
				}
				this.InternalUpdateRange();
				int num = this._baseList.BinarySearch(this._baseIndex + index, count, value, comparer);
				if (num >= 0)
				{
					return num - this._baseIndex;
				}
				return num + this._baseIndex;
			}

			// Token: 0x17000F90 RID: 3984
			// (get) Token: 0x060058B7 RID: 22711 RVA: 0x0011F94F File Offset: 0x0011DB4F
			// (set) Token: 0x060058B8 RID: 22712 RVA: 0x0011D82D File Offset: 0x0011BA2D
			public override int Capacity
			{
				get
				{
					return this._baseList.Capacity;
				}
				set
				{
					if (value < this.Count)
					{
						throw new ArgumentOutOfRangeException("value", Environment.GetResourceString("capacity was less than the current size."));
					}
				}
			}

			// Token: 0x060058B9 RID: 22713 RVA: 0x0011F95C File Offset: 0x0011DB5C
			public override void Clear()
			{
				this.InternalUpdateRange();
				if (this._baseSize != 0)
				{
					this._baseList.RemoveRange(this._baseIndex, this._baseSize);
					this.InternalUpdateVersion();
					this._baseSize = 0;
				}
			}

			// Token: 0x060058BA RID: 22714 RVA: 0x0011F990 File Offset: 0x0011DB90
			public override object Clone()
			{
				this.InternalUpdateRange();
				return new ArrayList.Range(this._baseList, this._baseIndex, this._baseSize)
				{
					_baseList = (ArrayList)this._baseList.Clone()
				};
			}

			// Token: 0x060058BB RID: 22715 RVA: 0x0011F9C8 File Offset: 0x0011DBC8
			public override bool Contains(object item)
			{
				this.InternalUpdateRange();
				if (item == null)
				{
					for (int i = 0; i < this._baseSize; i++)
					{
						if (this._baseList[this._baseIndex + i] == null)
						{
							return true;
						}
					}
					return false;
				}
				for (int j = 0; j < this._baseSize; j++)
				{
					if (this._baseList[this._baseIndex + j] != null && this._baseList[this._baseIndex + j].Equals(item))
					{
						return true;
					}
				}
				return false;
			}

			// Token: 0x060058BC RID: 22716 RVA: 0x0011FA4C File Offset: 0x0011DC4C
			public override void CopyTo(Array array, int index)
			{
				if (array == null)
				{
					throw new ArgumentNullException("array");
				}
				if (array.Rank != 1)
				{
					throw new ArgumentException(Environment.GetResourceString("Only single dimensional arrays are supported for the requested action."));
				}
				if (index < 0)
				{
					throw new ArgumentOutOfRangeException("index", Environment.GetResourceString("Non-negative number required."));
				}
				if (array.Length - index < this._baseSize)
				{
					throw new ArgumentException(Environment.GetResourceString("Offset and length were out of bounds for the array or count is greater than the number of elements from index to the end of the source collection."));
				}
				this.InternalUpdateRange();
				this._baseList.CopyTo(this._baseIndex, array, index, this._baseSize);
			}

			// Token: 0x060058BD RID: 22717 RVA: 0x0011FAD8 File Offset: 0x0011DCD8
			public override void CopyTo(int index, Array array, int arrayIndex, int count)
			{
				if (array == null)
				{
					throw new ArgumentNullException("array");
				}
				if (array.Rank != 1)
				{
					throw new ArgumentException(Environment.GetResourceString("Only single dimensional arrays are supported for the requested action."));
				}
				if (index < 0 || count < 0)
				{
					throw new ArgumentOutOfRangeException((index < 0) ? "index" : "count", Environment.GetResourceString("Non-negative number required."));
				}
				if (array.Length - arrayIndex < count)
				{
					throw new ArgumentException(Environment.GetResourceString("Offset and length were out of bounds for the array or count is greater than the number of elements from index to the end of the source collection."));
				}
				if (this._baseSize - index < count)
				{
					throw new ArgumentException(Environment.GetResourceString("Offset and length were out of bounds for the array or count is greater than the number of elements from index to the end of the source collection."));
				}
				this.InternalUpdateRange();
				this._baseList.CopyTo(this._baseIndex + index, array, arrayIndex, count);
			}

			// Token: 0x17000F91 RID: 3985
			// (get) Token: 0x060058BE RID: 22718 RVA: 0x0011FB8A File Offset: 0x0011DD8A
			public override int Count
			{
				get
				{
					this.InternalUpdateRange();
					return this._baseSize;
				}
			}

			// Token: 0x17000F92 RID: 3986
			// (get) Token: 0x060058BF RID: 22719 RVA: 0x0011FB98 File Offset: 0x0011DD98
			public override bool IsReadOnly
			{
				get
				{
					return this._baseList.IsReadOnly;
				}
			}

			// Token: 0x17000F93 RID: 3987
			// (get) Token: 0x060058C0 RID: 22720 RVA: 0x0011FBA5 File Offset: 0x0011DDA5
			public override bool IsFixedSize
			{
				get
				{
					return this._baseList.IsFixedSize;
				}
			}

			// Token: 0x17000F94 RID: 3988
			// (get) Token: 0x060058C1 RID: 22721 RVA: 0x0011FBB2 File Offset: 0x0011DDB2
			public override bool IsSynchronized
			{
				get
				{
					return this._baseList.IsSynchronized;
				}
			}

			// Token: 0x060058C2 RID: 22722 RVA: 0x0011FBBF File Offset: 0x0011DDBF
			public override IEnumerator GetEnumerator()
			{
				return this.GetEnumerator(0, this._baseSize);
			}

			// Token: 0x060058C3 RID: 22723 RVA: 0x0011FBD0 File Offset: 0x0011DDD0
			public override IEnumerator GetEnumerator(int index, int count)
			{
				if (index < 0 || count < 0)
				{
					throw new ArgumentOutOfRangeException((index < 0) ? "index" : "count", Environment.GetResourceString("Non-negative number required."));
				}
				if (this._baseSize - index < count)
				{
					throw new ArgumentException(Environment.GetResourceString("Offset and length were out of bounds for the array or count is greater than the number of elements from index to the end of the source collection."));
				}
				this.InternalUpdateRange();
				return this._baseList.GetEnumerator(this._baseIndex + index, count);
			}

			// Token: 0x060058C4 RID: 22724 RVA: 0x0011FC3C File Offset: 0x0011DE3C
			public override ArrayList GetRange(int index, int count)
			{
				if (index < 0 || count < 0)
				{
					throw new ArgumentOutOfRangeException((index < 0) ? "index" : "count", Environment.GetResourceString("Non-negative number required."));
				}
				if (this._baseSize - index < count)
				{
					throw new ArgumentException(Environment.GetResourceString("Offset and length were out of bounds for the array or count is greater than the number of elements from index to the end of the source collection."));
				}
				this.InternalUpdateRange();
				return new ArrayList.Range(this, index, count);
			}

			// Token: 0x17000F95 RID: 3989
			// (get) Token: 0x060058C5 RID: 22725 RVA: 0x0011FC9A File Offset: 0x0011DE9A
			public override object SyncRoot
			{
				get
				{
					return this._baseList.SyncRoot;
				}
			}

			// Token: 0x060058C6 RID: 22726 RVA: 0x0011FCA8 File Offset: 0x0011DEA8
			public override int IndexOf(object value)
			{
				this.InternalUpdateRange();
				int num = this._baseList.IndexOf(value, this._baseIndex, this._baseSize);
				if (num >= 0)
				{
					return num - this._baseIndex;
				}
				return -1;
			}

			// Token: 0x060058C7 RID: 22727 RVA: 0x0011FCE4 File Offset: 0x0011DEE4
			public override int IndexOf(object value, int startIndex)
			{
				if (startIndex < 0)
				{
					throw new ArgumentOutOfRangeException("startIndex", Environment.GetResourceString("Non-negative number required."));
				}
				if (startIndex > this._baseSize)
				{
					throw new ArgumentOutOfRangeException("startIndex", Environment.GetResourceString("Index was out of range. Must be non-negative and less than the size of the collection."));
				}
				this.InternalUpdateRange();
				int num = this._baseList.IndexOf(value, this._baseIndex + startIndex, this._baseSize - startIndex);
				if (num >= 0)
				{
					return num - this._baseIndex;
				}
				return -1;
			}

			// Token: 0x060058C8 RID: 22728 RVA: 0x0011FD5C File Offset: 0x0011DF5C
			public override int IndexOf(object value, int startIndex, int count)
			{
				if (startIndex < 0 || startIndex > this._baseSize)
				{
					throw new ArgumentOutOfRangeException("startIndex", Environment.GetResourceString("Index was out of range. Must be non-negative and less than the size of the collection."));
				}
				if (count < 0 || startIndex > this._baseSize - count)
				{
					throw new ArgumentOutOfRangeException("count", Environment.GetResourceString("Count must be positive and count must refer to a location within the string/array/collection."));
				}
				this.InternalUpdateRange();
				int num = this._baseList.IndexOf(value, this._baseIndex + startIndex, count);
				if (num >= 0)
				{
					return num - this._baseIndex;
				}
				return -1;
			}

			// Token: 0x060058C9 RID: 22729 RVA: 0x0011FDDC File Offset: 0x0011DFDC
			public override void Insert(int index, object value)
			{
				if (index < 0 || index > this._baseSize)
				{
					throw new ArgumentOutOfRangeException("index", Environment.GetResourceString("Index was out of range. Must be non-negative and less than the size of the collection."));
				}
				this.InternalUpdateRange();
				this._baseList.Insert(this._baseIndex + index, value);
				this.InternalUpdateVersion();
				this._baseSize++;
			}

			// Token: 0x060058CA RID: 22730 RVA: 0x0011FE3C File Offset: 0x0011E03C
			public override void InsertRange(int index, ICollection c)
			{
				if (index < 0 || index > this._baseSize)
				{
					throw new ArgumentOutOfRangeException("index", Environment.GetResourceString("Index was out of range. Must be non-negative and less than the size of the collection."));
				}
				if (c == null)
				{
					throw new ArgumentNullException("c");
				}
				this.InternalUpdateRange();
				int count = c.Count;
				if (count > 0)
				{
					this._baseList.InsertRange(this._baseIndex + index, c);
					this._baseSize += count;
					this.InternalUpdateVersion();
				}
			}

			// Token: 0x060058CB RID: 22731 RVA: 0x0011FEB4 File Offset: 0x0011E0B4
			public override int LastIndexOf(object value)
			{
				this.InternalUpdateRange();
				int num = this._baseList.LastIndexOf(value, this._baseIndex + this._baseSize - 1, this._baseSize);
				if (num >= 0)
				{
					return num - this._baseIndex;
				}
				return -1;
			}

			// Token: 0x060058CC RID: 22732 RVA: 0x0011DCC8 File Offset: 0x0011BEC8
			public override int LastIndexOf(object value, int startIndex)
			{
				return this.LastIndexOf(value, startIndex, startIndex + 1);
			}

			// Token: 0x060058CD RID: 22733 RVA: 0x0011FEF8 File Offset: 0x0011E0F8
			public override int LastIndexOf(object value, int startIndex, int count)
			{
				this.InternalUpdateRange();
				if (this._baseSize == 0)
				{
					return -1;
				}
				if (startIndex >= this._baseSize)
				{
					throw new ArgumentOutOfRangeException("startIndex", Environment.GetResourceString("Index was out of range. Must be non-negative and less than the size of the collection."));
				}
				if (startIndex < 0)
				{
					throw new ArgumentOutOfRangeException("startIndex", Environment.GetResourceString("Non-negative number required."));
				}
				int num = this._baseList.LastIndexOf(value, this._baseIndex + startIndex, count);
				if (num >= 0)
				{
					return num - this._baseIndex;
				}
				return -1;
			}

			// Token: 0x060058CE RID: 22734 RVA: 0x0011FF70 File Offset: 0x0011E170
			public override void RemoveAt(int index)
			{
				if (index < 0 || index >= this._baseSize)
				{
					throw new ArgumentOutOfRangeException("index", Environment.GetResourceString("Index was out of range. Must be non-negative and less than the size of the collection."));
				}
				this.InternalUpdateRange();
				this._baseList.RemoveAt(this._baseIndex + index);
				this.InternalUpdateVersion();
				this._baseSize--;
			}

			// Token: 0x060058CF RID: 22735 RVA: 0x0011FFCC File Offset: 0x0011E1CC
			public override void RemoveRange(int index, int count)
			{
				if (index < 0 || count < 0)
				{
					throw new ArgumentOutOfRangeException((index < 0) ? "index" : "count", Environment.GetResourceString("Non-negative number required."));
				}
				if (this._baseSize - index < count)
				{
					throw new ArgumentException(Environment.GetResourceString("Offset and length were out of bounds for the array or count is greater than the number of elements from index to the end of the source collection."));
				}
				this.InternalUpdateRange();
				if (count > 0)
				{
					this._baseList.RemoveRange(this._baseIndex + index, count);
					this.InternalUpdateVersion();
					this._baseSize -= count;
				}
			}

			// Token: 0x060058D0 RID: 22736 RVA: 0x00120050 File Offset: 0x0011E250
			public override void Reverse(int index, int count)
			{
				if (index < 0 || count < 0)
				{
					throw new ArgumentOutOfRangeException((index < 0) ? "index" : "count", Environment.GetResourceString("Non-negative number required."));
				}
				if (this._baseSize - index < count)
				{
					throw new ArgumentException(Environment.GetResourceString("Offset and length were out of bounds for the array or count is greater than the number of elements from index to the end of the source collection."));
				}
				this.InternalUpdateRange();
				this._baseList.Reverse(this._baseIndex + index, count);
				this.InternalUpdateVersion();
			}

			// Token: 0x060058D1 RID: 22737 RVA: 0x001200C0 File Offset: 0x0011E2C0
			public override void SetRange(int index, ICollection c)
			{
				this.InternalUpdateRange();
				if (index < 0 || index >= this._baseSize)
				{
					throw new ArgumentOutOfRangeException("index", Environment.GetResourceString("Index was out of range. Must be non-negative and less than the size of the collection."));
				}
				this._baseList.SetRange(this._baseIndex + index, c);
				if (c.Count > 0)
				{
					this.InternalUpdateVersion();
				}
			}

			// Token: 0x060058D2 RID: 22738 RVA: 0x00120118 File Offset: 0x0011E318
			public override void Sort(int index, int count, IComparer comparer)
			{
				if (index < 0 || count < 0)
				{
					throw new ArgumentOutOfRangeException((index < 0) ? "index" : "count", Environment.GetResourceString("Non-negative number required."));
				}
				if (this._baseSize - index < count)
				{
					throw new ArgumentException(Environment.GetResourceString("Offset and length were out of bounds for the array or count is greater than the number of elements from index to the end of the source collection."));
				}
				this.InternalUpdateRange();
				this._baseList.Sort(this._baseIndex + index, count, comparer);
				this.InternalUpdateVersion();
			}

			// Token: 0x17000F96 RID: 3990
			public override object this[int index]
			{
				get
				{
					this.InternalUpdateRange();
					if (index < 0 || index >= this._baseSize)
					{
						throw new ArgumentOutOfRangeException("index", Environment.GetResourceString("Index was out of range. Must be non-negative and less than the size of the collection."));
					}
					return this._baseList[this._baseIndex + index];
				}
				set
				{
					this.InternalUpdateRange();
					if (index < 0 || index >= this._baseSize)
					{
						throw new ArgumentOutOfRangeException("index", Environment.GetResourceString("Index was out of range. Must be non-negative and less than the size of the collection."));
					}
					this._baseList[this._baseIndex + index] = value;
					this.InternalUpdateVersion();
				}
			}

			// Token: 0x060058D5 RID: 22741 RVA: 0x00120218 File Offset: 0x0011E418
			public override object[] ToArray()
			{
				this.InternalUpdateRange();
				object[] array = new object[this._baseSize];
				Array.Copy(this._baseList._items, this._baseIndex, array, 0, this._baseSize);
				return array;
			}

			// Token: 0x060058D6 RID: 22742 RVA: 0x00120258 File Offset: 0x0011E458
			[SecuritySafeCritical]
			public override Array ToArray(Type type)
			{
				if (type == null)
				{
					throw new ArgumentNullException("type");
				}
				this.InternalUpdateRange();
				Array array = Array.UnsafeCreateInstance(type, new int[]
				{
					this._baseSize
				});
				this._baseList.CopyTo(this._baseIndex, array, 0, this._baseSize);
				return array;
			}

			// Token: 0x060058D7 RID: 22743 RVA: 0x001202AF File Offset: 0x0011E4AF
			public override void TrimToSize()
			{
				throw new NotSupportedException(Environment.GetResourceString("The specified operation is not supported on Ranges."));
			}

			// Token: 0x04002D3A RID: 11578
			private ArrayList _baseList;

			// Token: 0x04002D3B RID: 11579
			private int _baseIndex;

			// Token: 0x04002D3C RID: 11580
			private int _baseSize;

			// Token: 0x04002D3D RID: 11581
			private int _baseVersion;
		}

		// Token: 0x0200099C RID: 2460
		[Serializable]
		private sealed class ArrayListEnumeratorSimple : IEnumerator, ICloneable
		{
			// Token: 0x060058D8 RID: 22744 RVA: 0x001202C0 File Offset: 0x0011E4C0
			internal ArrayListEnumeratorSimple(ArrayList list)
			{
				this.list = list;
				this.index = -1;
				this.version = list._version;
				this.isArrayList = (list.GetType() == typeof(ArrayList));
				this.currentElement = ArrayList.ArrayListEnumeratorSimple.dummyObject;
			}

			// Token: 0x060058D9 RID: 22745 RVA: 0x0002BE93 File Offset: 0x0002A093
			public object Clone()
			{
				return base.MemberwiseClone();
			}

			// Token: 0x060058DA RID: 22746 RVA: 0x00120314 File Offset: 0x0011E514
			public bool MoveNext()
			{
				if (this.version != this.list._version)
				{
					throw new InvalidOperationException(Environment.GetResourceString("Collection was modified; enumeration operation may not execute."));
				}
				if (this.isArrayList)
				{
					if (this.index < this.list._size - 1)
					{
						object[] items = this.list._items;
						int num = this.index + 1;
						this.index = num;
						this.currentElement = items[num];
						return true;
					}
					this.currentElement = ArrayList.ArrayListEnumeratorSimple.dummyObject;
					this.index = this.list._size;
					return false;
				}
				else
				{
					if (this.index < this.list.Count - 1)
					{
						ArrayList arrayList = this.list;
						int num = this.index + 1;
						this.index = num;
						this.currentElement = arrayList[num];
						return true;
					}
					this.index = this.list.Count;
					this.currentElement = ArrayList.ArrayListEnumeratorSimple.dummyObject;
					return false;
				}
			}

			// Token: 0x17000F97 RID: 3991
			// (get) Token: 0x060058DB RID: 22747 RVA: 0x001203FC File Offset: 0x0011E5FC
			public object Current
			{
				get
				{
					object obj = this.currentElement;
					if (ArrayList.ArrayListEnumeratorSimple.dummyObject != obj)
					{
						return obj;
					}
					if (this.index == -1)
					{
						throw new InvalidOperationException(Environment.GetResourceString("Enumeration has not started. Call MoveNext."));
					}
					throw new InvalidOperationException(Environment.GetResourceString("Enumeration already finished."));
				}
			}

			// Token: 0x060058DC RID: 22748 RVA: 0x00120442 File Offset: 0x0011E642
			public void Reset()
			{
				if (this.version != this.list._version)
				{
					throw new InvalidOperationException(Environment.GetResourceString("Collection was modified; enumeration operation may not execute."));
				}
				this.currentElement = ArrayList.ArrayListEnumeratorSimple.dummyObject;
				this.index = -1;
			}

			// Token: 0x060058DD RID: 22749 RVA: 0x00120479 File Offset: 0x0011E679
			// Note: this type is marked as 'beforefieldinit'.
			static ArrayListEnumeratorSimple()
			{
			}

			// Token: 0x04002D3E RID: 11582
			private ArrayList list;

			// Token: 0x04002D3F RID: 11583
			private int index;

			// Token: 0x04002D40 RID: 11584
			private int version;

			// Token: 0x04002D41 RID: 11585
			private object currentElement;

			// Token: 0x04002D42 RID: 11586
			[NonSerialized]
			private bool isArrayList;

			// Token: 0x04002D43 RID: 11587
			private static object dummyObject = new object();
		}

		// Token: 0x0200099D RID: 2461
		internal class ArrayListDebugView
		{
			// Token: 0x060058DE RID: 22750 RVA: 0x00120485 File Offset: 0x0011E685
			public ArrayListDebugView(ArrayList arrayList)
			{
				if (arrayList == null)
				{
					throw new ArgumentNullException("arrayList");
				}
				this.arrayList = arrayList;
			}

			// Token: 0x17000F98 RID: 3992
			// (get) Token: 0x060058DF RID: 22751 RVA: 0x001204A2 File Offset: 0x0011E6A2
			[DebuggerBrowsable(DebuggerBrowsableState.RootHidden)]
			public object[] Items
			{
				get
				{
					return this.arrayList.ToArray();
				}
			}

			// Token: 0x04002D44 RID: 11588
			private ArrayList arrayList;
		}
	}
}
