﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using System.Threading;

namespace System.Collections
{
	/// <summary>Represents a collection of key/value pairs that are sorted by the keys and are accessible by key and by index.</summary>
	// Token: 0x020009C6 RID: 2502
	[ComVisible(true)]
	[DebuggerDisplay("Count = {Count}")]
	[DebuggerTypeProxy(typeof(SortedList.SortedListDebugView))]
	[Serializable]
	public class SortedList : IDictionary, ICollection, IEnumerable, ICloneable
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Collections.SortedList" /> class that is empty, has the default initial capacity, and is sorted according to the <see cref="T:System.IComparable" /> interface implemented by each key added to the <see cref="T:System.Collections.SortedList" /> object.</summary>
		// Token: 0x06005A55 RID: 23125 RVA: 0x00124A1F File Offset: 0x00122C1F
		public SortedList()
		{
			this.Init();
		}

		// Token: 0x06005A56 RID: 23126 RVA: 0x00124A2D File Offset: 0x00122C2D
		private void Init()
		{
			this.keys = SortedList.emptyArray;
			this.values = SortedList.emptyArray;
			this._size = 0;
			this.comparer = new Comparer(CultureInfo.CurrentCulture);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Collections.SortedList" /> class that is empty, has the specified initial capacity, and is sorted according to the <see cref="T:System.IComparable" /> interface implemented by each key added to the <see cref="T:System.Collections.SortedList" /> object.</summary>
		/// <param name="initialCapacity">The initial number of elements that the <see cref="T:System.Collections.SortedList" /> object can contain. </param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="initialCapacity" /> is less than zero. </exception>
		/// <exception cref="T:System.OutOfMemoryException">There is not enough available memory to create a <see cref="T:System.Collections.SortedList" /> object with the specified <paramref name="initialCapacity" />.</exception>
		// Token: 0x06005A57 RID: 23127 RVA: 0x00124A5C File Offset: 0x00122C5C
		public SortedList(int initialCapacity)
		{
			if (initialCapacity < 0)
			{
				throw new ArgumentOutOfRangeException("initialCapacity", Environment.GetResourceString("Non-negative number required."));
			}
			this.keys = new object[initialCapacity];
			this.values = new object[initialCapacity];
			this.comparer = new Comparer(CultureInfo.CurrentCulture);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Collections.SortedList" /> class that is empty, has the default initial capacity, and is sorted according to the specified <see cref="T:System.Collections.IComparer" /> interface.</summary>
		/// <param name="comparer">The <see cref="T:System.Collections.IComparer" /> implementation to use when comparing keys.-or- 
		///       <see langword="null" /> to use the <see cref="T:System.IComparable" /> implementation of each key. </param>
		// Token: 0x06005A58 RID: 23128 RVA: 0x00124AB0 File Offset: 0x00122CB0
		public SortedList(IComparer comparer) : this()
		{
			if (comparer != null)
			{
				this.comparer = comparer;
			}
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Collections.SortedList" /> class that is empty, has the specified initial capacity, and is sorted according to the specified <see cref="T:System.Collections.IComparer" /> interface.</summary>
		/// <param name="comparer">The <see cref="T:System.Collections.IComparer" /> implementation to use when comparing keys.-or- 
		///       <see langword="null" /> to use the <see cref="T:System.IComparable" /> implementation of each key. </param>
		/// <param name="capacity">The initial number of elements that the <see cref="T:System.Collections.SortedList" /> object can contain. </param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="capacity" /> is less than zero. </exception>
		/// <exception cref="T:System.OutOfMemoryException">There is not enough available memory to create a <see cref="T:System.Collections.SortedList" /> object with the specified <paramref name="capacity" />.</exception>
		// Token: 0x06005A59 RID: 23129 RVA: 0x00124AC2 File Offset: 0x00122CC2
		public SortedList(IComparer comparer, int capacity) : this(comparer)
		{
			this.Capacity = capacity;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Collections.SortedList" /> class that contains elements copied from the specified dictionary, has the same initial capacity as the number of elements copied, and is sorted according to the <see cref="T:System.IComparable" /> interface implemented by each key.</summary>
		/// <param name="d">The <see cref="T:System.Collections.IDictionary" /> implementation to copy to a new <see cref="T:System.Collections.SortedList" /> object.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="d" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.InvalidCastException">One or more elements in <paramref name="d" /> do not implement the <see cref="T:System.IComparable" /> interface. </exception>
		// Token: 0x06005A5A RID: 23130 RVA: 0x00124AD2 File Offset: 0x00122CD2
		public SortedList(IDictionary d) : this(d, null)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Collections.SortedList" /> class that contains elements copied from the specified dictionary, has the same initial capacity as the number of elements copied, and is sorted according to the specified <see cref="T:System.Collections.IComparer" /> interface.</summary>
		/// <param name="d">The <see cref="T:System.Collections.IDictionary" /> implementation to copy to a new <see cref="T:System.Collections.SortedList" /> object.</param>
		/// <param name="comparer">The <see cref="T:System.Collections.IComparer" /> implementation to use when comparing keys.-or- 
		///       <see langword="null" /> to use the <see cref="T:System.IComparable" /> implementation of each key. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="d" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.InvalidCastException">
		///         <paramref name="comparer" /> is <see langword="null" />, and one or more elements in <paramref name="d" /> do not implement the <see cref="T:System.IComparable" /> interface. </exception>
		// Token: 0x06005A5B RID: 23131 RVA: 0x00124ADC File Offset: 0x00122CDC
		public SortedList(IDictionary d, IComparer comparer) : this(comparer, (d != null) ? d.Count : 0)
		{
			if (d == null)
			{
				throw new ArgumentNullException("d", Environment.GetResourceString("Dictionary cannot be null."));
			}
			d.Keys.CopyTo(this.keys, 0);
			d.Values.CopyTo(this.values, 0);
			Array.Sort(this.keys, this.values, comparer);
			this._size = d.Count;
		}

		/// <summary>Adds an element with the specified key and value to a <see cref="T:System.Collections.SortedList" /> object.</summary>
		/// <param name="key">The key of the element to add. </param>
		/// <param name="value">The value of the element to add. The value can be <see langword="null" />. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="key" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.ArgumentException">An element with the specified <paramref name="key" /> already exists in the <see cref="T:System.Collections.SortedList" /> object.-or- The <see cref="T:System.Collections.SortedList" /> is set to use the <see cref="T:System.IComparable" /> interface, and <paramref name="key" /> does not implement the <see cref="T:System.IComparable" /> interface. </exception>
		/// <exception cref="T:System.NotSupportedException">The <see cref="T:System.Collections.SortedList" /> is read-only.-or- The <see cref="T:System.Collections.SortedList" /> has a fixed size. </exception>
		/// <exception cref="T:System.OutOfMemoryException">There is not enough available memory to add the element to the <see cref="T:System.Collections.SortedList" />.</exception>
		/// <exception cref="T:System.InvalidOperationException">The comparer throws an exception. </exception>
		// Token: 0x06005A5C RID: 23132 RVA: 0x00124B58 File Offset: 0x00122D58
		public virtual void Add(object key, object value)
		{
			if (key == null)
			{
				throw new ArgumentNullException("key", Environment.GetResourceString("Key cannot be null."));
			}
			int num = Array.BinarySearch(this.keys, 0, this._size, key, this.comparer);
			if (num >= 0)
			{
				throw new ArgumentException(Environment.GetResourceString("Item has already been added. Key in dictionary: '{0}'  Key being added: '{1}'", new object[]
				{
					this.GetKey(num),
					key
				}));
			}
			this.Insert(~num, key, value);
		}

		/// <summary>Gets or sets the capacity of a <see cref="T:System.Collections.SortedList" /> object.</summary>
		/// <returns>The number of elements that the <see cref="T:System.Collections.SortedList" /> object can contain.</returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The value assigned is less than the current number of elements in the <see cref="T:System.Collections.SortedList" /> object.</exception>
		/// <exception cref="T:System.OutOfMemoryException">There is not enough memory available on the system.</exception>
		// Token: 0x17001011 RID: 4113
		// (get) Token: 0x06005A5D RID: 23133 RVA: 0x00124BC9 File Offset: 0x00122DC9
		// (set) Token: 0x06005A5E RID: 23134 RVA: 0x00124BD4 File Offset: 0x00122DD4
		public virtual int Capacity
		{
			get
			{
				return this.keys.Length;
			}
			set
			{
				if (value < this.Count)
				{
					throw new ArgumentOutOfRangeException("value", Environment.GetResourceString("capacity was less than the current size."));
				}
				if (value != this.keys.Length)
				{
					if (value > 0)
					{
						object[] destinationArray = new object[value];
						object[] destinationArray2 = new object[value];
						if (this._size > 0)
						{
							Array.Copy(this.keys, 0, destinationArray, 0, this._size);
							Array.Copy(this.values, 0, destinationArray2, 0, this._size);
						}
						this.keys = destinationArray;
						this.values = destinationArray2;
						return;
					}
					this.keys = SortedList.emptyArray;
					this.values = SortedList.emptyArray;
				}
			}
		}

		/// <summary>Gets the number of elements contained in a <see cref="T:System.Collections.SortedList" /> object.</summary>
		/// <returns>The number of elements contained in the <see cref="T:System.Collections.SortedList" /> object.</returns>
		// Token: 0x17001012 RID: 4114
		// (get) Token: 0x06005A5F RID: 23135 RVA: 0x00124C72 File Offset: 0x00122E72
		public virtual int Count
		{
			get
			{
				return this._size;
			}
		}

		/// <summary>Gets the keys in a <see cref="T:System.Collections.SortedList" /> object.</summary>
		/// <returns>An <see cref="T:System.Collections.ICollection" /> object containing the keys in the <see cref="T:System.Collections.SortedList" /> object.</returns>
		// Token: 0x17001013 RID: 4115
		// (get) Token: 0x06005A60 RID: 23136 RVA: 0x00124C7A File Offset: 0x00122E7A
		public virtual ICollection Keys
		{
			get
			{
				return this.GetKeyList();
			}
		}

		/// <summary>Gets the values in a <see cref="T:System.Collections.SortedList" /> object.</summary>
		/// <returns>An <see cref="T:System.Collections.ICollection" /> object containing the values in the <see cref="T:System.Collections.SortedList" /> object.</returns>
		// Token: 0x17001014 RID: 4116
		// (get) Token: 0x06005A61 RID: 23137 RVA: 0x00124C82 File Offset: 0x00122E82
		public virtual ICollection Values
		{
			get
			{
				return this.GetValueList();
			}
		}

		/// <summary>Gets a value indicating whether a <see cref="T:System.Collections.SortedList" /> object is read-only.</summary>
		/// <returns>
		///     <see langword="true" /> if the <see cref="T:System.Collections.SortedList" /> object is read-only; otherwise, <see langword="false" />. The default is <see langword="false" />.</returns>
		// Token: 0x17001015 RID: 4117
		// (get) Token: 0x06005A62 RID: 23138 RVA: 0x00002526 File Offset: 0x00000726
		public virtual bool IsReadOnly
		{
			get
			{
				return false;
			}
		}

		/// <summary>Gets a value indicating whether a <see cref="T:System.Collections.SortedList" /> object has a fixed size.</summary>
		/// <returns>
		///     <see langword="true" /> if the <see cref="T:System.Collections.SortedList" /> object has a fixed size; otherwise, <see langword="false" />. The default is <see langword="false" />.</returns>
		// Token: 0x17001016 RID: 4118
		// (get) Token: 0x06005A63 RID: 23139 RVA: 0x00002526 File Offset: 0x00000726
		public virtual bool IsFixedSize
		{
			get
			{
				return false;
			}
		}

		/// <summary>Gets a value indicating whether access to a <see cref="T:System.Collections.SortedList" /> object is synchronized (thread safe).</summary>
		/// <returns>
		///     <see langword="true" /> if access to the <see cref="T:System.Collections.SortedList" /> object is synchronized (thread safe); otherwise, <see langword="false" />. The default is <see langword="false" />.</returns>
		// Token: 0x17001017 RID: 4119
		// (get) Token: 0x06005A64 RID: 23140 RVA: 0x00002526 File Offset: 0x00000726
		public virtual bool IsSynchronized
		{
			get
			{
				return false;
			}
		}

		/// <summary>Gets an object that can be used to synchronize access to a <see cref="T:System.Collections.SortedList" /> object.</summary>
		/// <returns>An object that can be used to synchronize access to the <see cref="T:System.Collections.SortedList" /> object.</returns>
		// Token: 0x17001018 RID: 4120
		// (get) Token: 0x06005A65 RID: 23141 RVA: 0x00124C8A File Offset: 0x00122E8A
		public virtual object SyncRoot
		{
			get
			{
				if (this._syncRoot == null)
				{
					Interlocked.CompareExchange<object>(ref this._syncRoot, new object(), null);
				}
				return this._syncRoot;
			}
		}

		/// <summary>Removes all elements from a <see cref="T:System.Collections.SortedList" /> object.</summary>
		/// <exception cref="T:System.NotSupportedException">The <see cref="T:System.Collections.SortedList" /> object is read-only.-or- The <see cref="T:System.Collections.SortedList" /> has a fixed size. </exception>
		// Token: 0x06005A66 RID: 23142 RVA: 0x00124CAC File Offset: 0x00122EAC
		public virtual void Clear()
		{
			this.version++;
			Array.Clear(this.keys, 0, this._size);
			Array.Clear(this.values, 0, this._size);
			this._size = 0;
		}

		/// <summary>Creates a shallow copy of a <see cref="T:System.Collections.SortedList" /> object.</summary>
		/// <returns>A shallow copy of the <see cref="T:System.Collections.SortedList" /> object.</returns>
		// Token: 0x06005A67 RID: 23143 RVA: 0x00124CE8 File Offset: 0x00122EE8
		public virtual object Clone()
		{
			SortedList sortedList = new SortedList(this._size);
			Array.Copy(this.keys, 0, sortedList.keys, 0, this._size);
			Array.Copy(this.values, 0, sortedList.values, 0, this._size);
			sortedList._size = this._size;
			sortedList.version = this.version;
			sortedList.comparer = this.comparer;
			return sortedList;
		}

		/// <summary>Determines whether a <see cref="T:System.Collections.SortedList" /> object contains a specific key.</summary>
		/// <param name="key">The key to locate in the <see cref="T:System.Collections.SortedList" /> object. </param>
		/// <returns>
		///     <see langword="true" /> if the <see cref="T:System.Collections.SortedList" /> object contains an element with the specified <paramref name="key" />; otherwise, <see langword="false" />.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="key" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.InvalidOperationException">The comparer throws an exception. </exception>
		// Token: 0x06005A68 RID: 23144 RVA: 0x00124D58 File Offset: 0x00122F58
		public virtual bool Contains(object key)
		{
			return this.IndexOfKey(key) >= 0;
		}

		/// <summary>Determines whether a <see cref="T:System.Collections.SortedList" /> object contains a specific key.</summary>
		/// <param name="key">The key to locate in the <see cref="T:System.Collections.SortedList" /> object.</param>
		/// <returns>
		///     <see langword="true" /> if the <see cref="T:System.Collections.SortedList" /> object contains an element with the specified <paramref name="key" />; otherwise, <see langword="false" />.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="key" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.InvalidOperationException">The comparer throws an exception. </exception>
		// Token: 0x06005A69 RID: 23145 RVA: 0x00124D58 File Offset: 0x00122F58
		public virtual bool ContainsKey(object key)
		{
			return this.IndexOfKey(key) >= 0;
		}

		/// <summary>Determines whether a <see cref="T:System.Collections.SortedList" /> object contains a specific value.</summary>
		/// <param name="value">The value to locate in the <see cref="T:System.Collections.SortedList" /> object. The value can be <see langword="null" />. </param>
		/// <returns>
		///     <see langword="true" /> if the <see cref="T:System.Collections.SortedList" /> object contains an element with the specified <paramref name="value" />; otherwise, <see langword="false" />.</returns>
		// Token: 0x06005A6A RID: 23146 RVA: 0x00124D67 File Offset: 0x00122F67
		public virtual bool ContainsValue(object value)
		{
			return this.IndexOfValue(value) >= 0;
		}

		/// <summary>Copies <see cref="T:System.Collections.SortedList" /> elements to a one-dimensional <see cref="T:System.Array" /> object, starting at the specified index in the array.</summary>
		/// <param name="array">The one-dimensional <see cref="T:System.Array" /> object that is the destination of the <see cref="T:System.Collections.DictionaryEntry" /> objects copied from <see cref="T:System.Collections.SortedList" />. The <see cref="T:System.Array" /> must have zero-based indexing. </param>
		/// <param name="arrayIndex">The zero-based index in <paramref name="array" /> at which copying begins. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="array" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="arrayIndex" /> is less than zero. </exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="array" /> is multidimensional.-or- The number of elements in the source <see cref="T:System.Collections.SortedList" /> object is greater than the available space from <paramref name="arrayIndex" /> to the end of the destination <paramref name="array" />. </exception>
		/// <exception cref="T:System.InvalidCastException">The type of the source <see cref="T:System.Collections.SortedList" /> cannot be cast automatically to the type of the destination <paramref name="array" />. </exception>
		// Token: 0x06005A6B RID: 23147 RVA: 0x00124D78 File Offset: 0x00122F78
		public virtual void CopyTo(Array array, int arrayIndex)
		{
			if (array == null)
			{
				throw new ArgumentNullException("array", Environment.GetResourceString("Array cannot be null."));
			}
			if (array.Rank != 1)
			{
				throw new ArgumentException(Environment.GetResourceString("Only single dimensional arrays are supported for the requested action."));
			}
			if (arrayIndex < 0)
			{
				throw new ArgumentOutOfRangeException("arrayIndex", Environment.GetResourceString("Non-negative number required."));
			}
			if (array.Length - arrayIndex < this.Count)
			{
				throw new ArgumentException(Environment.GetResourceString("Destination array is not long enough to copy all the items in the collection. Check array index and length."));
			}
			for (int i = 0; i < this.Count; i++)
			{
				DictionaryEntry dictionaryEntry = new DictionaryEntry(this.keys[i], this.values[i]);
				array.SetValue(dictionaryEntry, i + arrayIndex);
			}
		}

		// Token: 0x06005A6C RID: 23148 RVA: 0x00124E28 File Offset: 0x00123028
		internal virtual KeyValuePairs[] ToKeyValuePairsArray()
		{
			KeyValuePairs[] array = new KeyValuePairs[this.Count];
			for (int i = 0; i < this.Count; i++)
			{
				array[i] = new KeyValuePairs(this.keys[i], this.values[i]);
			}
			return array;
		}

		// Token: 0x06005A6D RID: 23149 RVA: 0x00124E6C File Offset: 0x0012306C
		private void EnsureCapacity(int min)
		{
			int num = (this.keys.Length == 0) ? 16 : (this.keys.Length * 2);
			if (num > 2146435071)
			{
				num = 2146435071;
			}
			if (num < min)
			{
				num = min;
			}
			this.Capacity = num;
		}

		/// <summary>Gets the value at the specified index of a <see cref="T:System.Collections.SortedList" /> object.</summary>
		/// <param name="index">The zero-based index of the value to get. </param>
		/// <returns>The value at the specified index of the <see cref="T:System.Collections.SortedList" /> object.</returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="index" /> is outside the range of valid indexes for the <see cref="T:System.Collections.SortedList" /> object. </exception>
		// Token: 0x06005A6E RID: 23150 RVA: 0x00124EAC File Offset: 0x001230AC
		public virtual object GetByIndex(int index)
		{
			if (index < 0 || index >= this.Count)
			{
				throw new ArgumentOutOfRangeException("index", Environment.GetResourceString("Index was out of range. Must be non-negative and less than the size of the collection."));
			}
			return this.values[index];
		}

		/// <summary>Returns an <see cref="T:System.Collections.IEnumerator" /> that iterates through the <see cref="T:System.Collections.SortedList" />.</summary>
		/// <returns>An <see cref="T:System.Collections.IEnumerator" /> for the <see cref="T:System.Collections.SortedList" />.</returns>
		// Token: 0x06005A6F RID: 23151 RVA: 0x00124ED8 File Offset: 0x001230D8
		IEnumerator IEnumerable.GetEnumerator()
		{
			return new SortedList.SortedListEnumerator(this, 0, this._size, 3);
		}

		/// <summary>Returns an <see cref="T:System.Collections.IDictionaryEnumerator" /> object that iterates through a <see cref="T:System.Collections.SortedList" /> object.</summary>
		/// <returns>An <see cref="T:System.Collections.IDictionaryEnumerator" /> object for the <see cref="T:System.Collections.SortedList" /> object.</returns>
		// Token: 0x06005A70 RID: 23152 RVA: 0x00124ED8 File Offset: 0x001230D8
		public virtual IDictionaryEnumerator GetEnumerator()
		{
			return new SortedList.SortedListEnumerator(this, 0, this._size, 3);
		}

		/// <summary>Gets the key at the specified index of a <see cref="T:System.Collections.SortedList" /> object.</summary>
		/// <param name="index">The zero-based index of the key to get. </param>
		/// <returns>The key at the specified index of the <see cref="T:System.Collections.SortedList" /> object.</returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="index" /> is outside the range of valid indexes for the <see cref="T:System.Collections.SortedList" /> object.</exception>
		// Token: 0x06005A71 RID: 23153 RVA: 0x00124EE8 File Offset: 0x001230E8
		public virtual object GetKey(int index)
		{
			if (index < 0 || index >= this.Count)
			{
				throw new ArgumentOutOfRangeException("index", Environment.GetResourceString("Index was out of range. Must be non-negative and less than the size of the collection."));
			}
			return this.keys[index];
		}

		/// <summary>Gets the keys in a <see cref="T:System.Collections.SortedList" /> object.</summary>
		/// <returns>An <see cref="T:System.Collections.IList" /> object containing the keys in the <see cref="T:System.Collections.SortedList" /> object.</returns>
		// Token: 0x06005A72 RID: 23154 RVA: 0x00124F14 File Offset: 0x00123114
		public virtual IList GetKeyList()
		{
			if (this.keyList == null)
			{
				this.keyList = new SortedList.KeyList(this);
			}
			return this.keyList;
		}

		/// <summary>Gets the values in a <see cref="T:System.Collections.SortedList" /> object.</summary>
		/// <returns>An <see cref="T:System.Collections.IList" /> object containing the values in the <see cref="T:System.Collections.SortedList" /> object.</returns>
		// Token: 0x06005A73 RID: 23155 RVA: 0x00124F30 File Offset: 0x00123130
		public virtual IList GetValueList()
		{
			if (this.valueList == null)
			{
				this.valueList = new SortedList.ValueList(this);
			}
			return this.valueList;
		}

		/// <summary>Gets and sets the value associated with a specific key in a <see cref="T:System.Collections.SortedList" /> object.</summary>
		/// <param name="key">The key associated with the value to get or set. </param>
		/// <returns>The value associated with the <paramref name="key" /> parameter in the <see cref="T:System.Collections.SortedList" /> object, if <paramref name="key" /> is found; otherwise, <see langword="null" />.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="key" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.NotSupportedException">The property is set and the <see cref="T:System.Collections.SortedList" /> object is read-only.-or- The property is set, <paramref name="key" /> does not exist in the collection, and the <see cref="T:System.Collections.SortedList" /> has a fixed size. </exception>
		/// <exception cref="T:System.OutOfMemoryException">There is not enough available memory to add the element to the <see cref="T:System.Collections.SortedList" />.</exception>
		/// <exception cref="T:System.InvalidOperationException">The comparer throws an exception. </exception>
		// Token: 0x17001019 RID: 4121
		public virtual object this[object key]
		{
			get
			{
				int num = this.IndexOfKey(key);
				if (num >= 0)
				{
					return this.values[num];
				}
				return null;
			}
			set
			{
				if (key == null)
				{
					throw new ArgumentNullException("key", Environment.GetResourceString("Key cannot be null."));
				}
				int num = Array.BinarySearch(this.keys, 0, this._size, key, this.comparer);
				if (num >= 0)
				{
					this.values[num] = value;
					this.version++;
					return;
				}
				this.Insert(~num, key, value);
			}
		}

		/// <summary>Returns the zero-based index of the specified key in a <see cref="T:System.Collections.SortedList" /> object.</summary>
		/// <param name="key">The key to locate in the <see cref="T:System.Collections.SortedList" /> object. </param>
		/// <returns>The zero-based index of the <paramref name="key" /> parameter, if <paramref name="key" /> is found in the <see cref="T:System.Collections.SortedList" /> object; otherwise, -1.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="key" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.InvalidOperationException">The comparer throws an exception. </exception>
		// Token: 0x06005A76 RID: 23158 RVA: 0x00124FD8 File Offset: 0x001231D8
		public virtual int IndexOfKey(object key)
		{
			if (key == null)
			{
				throw new ArgumentNullException("key", Environment.GetResourceString("Key cannot be null."));
			}
			int num = Array.BinarySearch(this.keys, 0, this._size, key, this.comparer);
			if (num < 0)
			{
				return -1;
			}
			return num;
		}

		/// <summary>Returns the zero-based index of the first occurrence of the specified value in a <see cref="T:System.Collections.SortedList" /> object.</summary>
		/// <param name="value">The value to locate in the <see cref="T:System.Collections.SortedList" /> object. The value can be <see langword="null" />. </param>
		/// <returns>The zero-based index of the first occurrence of the <paramref name="value" /> parameter, if <paramref name="value" /> is found in the <see cref="T:System.Collections.SortedList" /> object; otherwise, -1.</returns>
		// Token: 0x06005A77 RID: 23159 RVA: 0x0012501E File Offset: 0x0012321E
		public virtual int IndexOfValue(object value)
		{
			return Array.IndexOf<object>(this.values, value, 0, this._size);
		}

		// Token: 0x06005A78 RID: 23160 RVA: 0x00125034 File Offset: 0x00123234
		private void Insert(int index, object key, object value)
		{
			if (this._size == this.keys.Length)
			{
				this.EnsureCapacity(this._size + 1);
			}
			if (index < this._size)
			{
				Array.Copy(this.keys, index, this.keys, index + 1, this._size - index);
				Array.Copy(this.values, index, this.values, index + 1, this._size - index);
			}
			this.keys[index] = key;
			this.values[index] = value;
			this._size++;
			this.version++;
		}

		/// <summary>Removes the element at the specified index of a <see cref="T:System.Collections.SortedList" /> object.</summary>
		/// <param name="index">The zero-based index of the element to remove. </param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="index" /> is outside the range of valid indexes for the <see cref="T:System.Collections.SortedList" /> object. </exception>
		/// <exception cref="T:System.NotSupportedException">The <see cref="T:System.Collections.SortedList" /> is read-only.-or- The <see cref="T:System.Collections.SortedList" /> has a fixed size. </exception>
		// Token: 0x06005A79 RID: 23161 RVA: 0x001250D0 File Offset: 0x001232D0
		public virtual void RemoveAt(int index)
		{
			if (index < 0 || index >= this.Count)
			{
				throw new ArgumentOutOfRangeException("index", Environment.GetResourceString("Index was out of range. Must be non-negative and less than the size of the collection."));
			}
			this._size--;
			if (index < this._size)
			{
				Array.Copy(this.keys, index + 1, this.keys, index, this._size - index);
				Array.Copy(this.values, index + 1, this.values, index, this._size - index);
			}
			this.keys[this._size] = null;
			this.values[this._size] = null;
			this.version++;
		}

		/// <summary>Removes the element with the specified key from a <see cref="T:System.Collections.SortedList" /> object.</summary>
		/// <param name="key">The key of the element to remove. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="key" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.NotSupportedException">The <see cref="T:System.Collections.SortedList" /> object is read-only.-or- The <see cref="T:System.Collections.SortedList" /> has a fixed size. </exception>
		// Token: 0x06005A7A RID: 23162 RVA: 0x0012517C File Offset: 0x0012337C
		public virtual void Remove(object key)
		{
			int num = this.IndexOfKey(key);
			if (num >= 0)
			{
				this.RemoveAt(num);
			}
		}

		/// <summary>Replaces the value at a specific index in a <see cref="T:System.Collections.SortedList" /> object.</summary>
		/// <param name="index">The zero-based index at which to save <paramref name="value" />. </param>
		/// <param name="value">The <see cref="T:System.Object" /> to save into the <see cref="T:System.Collections.SortedList" /> object. The value can be <see langword="null" />. </param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="index" /> is outside the range of valid indexes for the <see cref="T:System.Collections.SortedList" /> object. </exception>
		// Token: 0x06005A7B RID: 23163 RVA: 0x0012519C File Offset: 0x0012339C
		public virtual void SetByIndex(int index, object value)
		{
			if (index < 0 || index >= this.Count)
			{
				throw new ArgumentOutOfRangeException("index", Environment.GetResourceString("Index was out of range. Must be non-negative and less than the size of the collection."));
			}
			this.values[index] = value;
			this.version++;
		}

		/// <summary>Returns a synchronized (thread-safe) wrapper for a <see cref="T:System.Collections.SortedList" /> object.</summary>
		/// <param name="list">The <see cref="T:System.Collections.SortedList" /> object to synchronize. </param>
		/// <returns>A synchronized (thread-safe) wrapper for the <see cref="T:System.Collections.SortedList" /> object.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="list" /> is <see langword="null" />. </exception>
		// Token: 0x06005A7C RID: 23164 RVA: 0x001251D7 File Offset: 0x001233D7
		[HostProtection(SecurityAction.LinkDemand, Synchronization = true)]
		public static SortedList Synchronized(SortedList list)
		{
			if (list == null)
			{
				throw new ArgumentNullException("list");
			}
			return new SortedList.SyncSortedList(list);
		}

		/// <summary>Sets the capacity to the actual number of elements in a <see cref="T:System.Collections.SortedList" /> object.</summary>
		/// <exception cref="T:System.NotSupportedException">The <see cref="T:System.Collections.SortedList" /> object is read-only.-or- The <see cref="T:System.Collections.SortedList" /> has a fixed size. </exception>
		// Token: 0x06005A7D RID: 23165 RVA: 0x001251ED File Offset: 0x001233ED
		public virtual void TrimToSize()
		{
			this.Capacity = this._size;
		}

		// Token: 0x06005A7E RID: 23166 RVA: 0x001251FB File Offset: 0x001233FB
		// Note: this type is marked as 'beforefieldinit'.
		static SortedList()
		{
		}

		// Token: 0x04002DAC RID: 11692
		private object[] keys;

		// Token: 0x04002DAD RID: 11693
		private object[] values;

		// Token: 0x04002DAE RID: 11694
		private int _size;

		// Token: 0x04002DAF RID: 11695
		private int version;

		// Token: 0x04002DB0 RID: 11696
		private IComparer comparer;

		// Token: 0x04002DB1 RID: 11697
		private SortedList.KeyList keyList;

		// Token: 0x04002DB2 RID: 11698
		private SortedList.ValueList valueList;

		// Token: 0x04002DB3 RID: 11699
		[NonSerialized]
		private object _syncRoot;

		// Token: 0x04002DB4 RID: 11700
		private const int _defaultCapacity = 16;

		// Token: 0x04002DB5 RID: 11701
		private static object[] emptyArray = EmptyArray<object>.Value;

		// Token: 0x020009C7 RID: 2503
		[Serializable]
		private class SyncSortedList : SortedList
		{
			// Token: 0x06005A7F RID: 23167 RVA: 0x00125207 File Offset: 0x00123407
			internal SyncSortedList(SortedList list)
			{
				this._list = list;
				this._root = list.SyncRoot;
			}

			// Token: 0x1700101A RID: 4122
			// (get) Token: 0x06005A80 RID: 23168 RVA: 0x00125224 File Offset: 0x00123424
			public override int Count
			{
				get
				{
					object root = this._root;
					int count;
					lock (root)
					{
						count = this._list.Count;
					}
					return count;
				}
			}

			// Token: 0x1700101B RID: 4123
			// (get) Token: 0x06005A81 RID: 23169 RVA: 0x0012526C File Offset: 0x0012346C
			public override object SyncRoot
			{
				get
				{
					return this._root;
				}
			}

			// Token: 0x1700101C RID: 4124
			// (get) Token: 0x06005A82 RID: 23170 RVA: 0x00125274 File Offset: 0x00123474
			public override bool IsReadOnly
			{
				get
				{
					return this._list.IsReadOnly;
				}
			}

			// Token: 0x1700101D RID: 4125
			// (get) Token: 0x06005A83 RID: 23171 RVA: 0x00125281 File Offset: 0x00123481
			public override bool IsFixedSize
			{
				get
				{
					return this._list.IsFixedSize;
				}
			}

			// Token: 0x1700101E RID: 4126
			// (get) Token: 0x06005A84 RID: 23172 RVA: 0x00004E08 File Offset: 0x00003008
			public override bool IsSynchronized
			{
				get
				{
					return true;
				}
			}

			// Token: 0x1700101F RID: 4127
			public override object this[object key]
			{
				get
				{
					object root = this._root;
					object result;
					lock (root)
					{
						result = this._list[key];
					}
					return result;
				}
				set
				{
					object root = this._root;
					lock (root)
					{
						this._list[key] = value;
					}
				}
			}

			// Token: 0x06005A87 RID: 23175 RVA: 0x00125320 File Offset: 0x00123520
			public override void Add(object key, object value)
			{
				object root = this._root;
				lock (root)
				{
					this._list.Add(key, value);
				}
			}

			// Token: 0x17001020 RID: 4128
			// (get) Token: 0x06005A88 RID: 23176 RVA: 0x00125368 File Offset: 0x00123568
			public override int Capacity
			{
				get
				{
					object root = this._root;
					int capacity;
					lock (root)
					{
						capacity = this._list.Capacity;
					}
					return capacity;
				}
			}

			// Token: 0x06005A89 RID: 23177 RVA: 0x001253B0 File Offset: 0x001235B0
			public override void Clear()
			{
				object root = this._root;
				lock (root)
				{
					this._list.Clear();
				}
			}

			// Token: 0x06005A8A RID: 23178 RVA: 0x001253F8 File Offset: 0x001235F8
			public override object Clone()
			{
				object root = this._root;
				object result;
				lock (root)
				{
					result = this._list.Clone();
				}
				return result;
			}

			// Token: 0x06005A8B RID: 23179 RVA: 0x00125440 File Offset: 0x00123640
			public override bool Contains(object key)
			{
				object root = this._root;
				bool result;
				lock (root)
				{
					result = this._list.Contains(key);
				}
				return result;
			}

			// Token: 0x06005A8C RID: 23180 RVA: 0x00125488 File Offset: 0x00123688
			public override bool ContainsKey(object key)
			{
				object root = this._root;
				bool result;
				lock (root)
				{
					result = this._list.ContainsKey(key);
				}
				return result;
			}

			// Token: 0x06005A8D RID: 23181 RVA: 0x001254D0 File Offset: 0x001236D0
			public override bool ContainsValue(object key)
			{
				object root = this._root;
				bool result;
				lock (root)
				{
					result = this._list.ContainsValue(key);
				}
				return result;
			}

			// Token: 0x06005A8E RID: 23182 RVA: 0x00125518 File Offset: 0x00123718
			public override void CopyTo(Array array, int index)
			{
				object root = this._root;
				lock (root)
				{
					this._list.CopyTo(array, index);
				}
			}

			// Token: 0x06005A8F RID: 23183 RVA: 0x00125560 File Offset: 0x00123760
			public override object GetByIndex(int index)
			{
				object root = this._root;
				object byIndex;
				lock (root)
				{
					byIndex = this._list.GetByIndex(index);
				}
				return byIndex;
			}

			// Token: 0x06005A90 RID: 23184 RVA: 0x001255A8 File Offset: 0x001237A8
			public override IDictionaryEnumerator GetEnumerator()
			{
				object root = this._root;
				IDictionaryEnumerator enumerator;
				lock (root)
				{
					enumerator = this._list.GetEnumerator();
				}
				return enumerator;
			}

			// Token: 0x06005A91 RID: 23185 RVA: 0x001255F0 File Offset: 0x001237F0
			public override object GetKey(int index)
			{
				object root = this._root;
				object key;
				lock (root)
				{
					key = this._list.GetKey(index);
				}
				return key;
			}

			// Token: 0x06005A92 RID: 23186 RVA: 0x00125638 File Offset: 0x00123838
			public override IList GetKeyList()
			{
				object root = this._root;
				IList keyList;
				lock (root)
				{
					keyList = this._list.GetKeyList();
				}
				return keyList;
			}

			// Token: 0x06005A93 RID: 23187 RVA: 0x00125680 File Offset: 0x00123880
			public override IList GetValueList()
			{
				object root = this._root;
				IList valueList;
				lock (root)
				{
					valueList = this._list.GetValueList();
				}
				return valueList;
			}

			// Token: 0x06005A94 RID: 23188 RVA: 0x001256C8 File Offset: 0x001238C8
			public override int IndexOfKey(object key)
			{
				if (key == null)
				{
					throw new ArgumentNullException("key", Environment.GetResourceString("Key cannot be null."));
				}
				object root = this._root;
				int result;
				lock (root)
				{
					result = this._list.IndexOfKey(key);
				}
				return result;
			}

			// Token: 0x06005A95 RID: 23189 RVA: 0x00125728 File Offset: 0x00123928
			public override int IndexOfValue(object value)
			{
				object root = this._root;
				int result;
				lock (root)
				{
					result = this._list.IndexOfValue(value);
				}
				return result;
			}

			// Token: 0x06005A96 RID: 23190 RVA: 0x00125770 File Offset: 0x00123970
			public override void RemoveAt(int index)
			{
				object root = this._root;
				lock (root)
				{
					this._list.RemoveAt(index);
				}
			}

			// Token: 0x06005A97 RID: 23191 RVA: 0x001257B8 File Offset: 0x001239B8
			public override void Remove(object key)
			{
				object root = this._root;
				lock (root)
				{
					this._list.Remove(key);
				}
			}

			// Token: 0x06005A98 RID: 23192 RVA: 0x00125800 File Offset: 0x00123A00
			public override void SetByIndex(int index, object value)
			{
				object root = this._root;
				lock (root)
				{
					this._list.SetByIndex(index, value);
				}
			}

			// Token: 0x06005A99 RID: 23193 RVA: 0x00125848 File Offset: 0x00123A48
			internal override KeyValuePairs[] ToKeyValuePairsArray()
			{
				return this._list.ToKeyValuePairsArray();
			}

			// Token: 0x06005A9A RID: 23194 RVA: 0x00125858 File Offset: 0x00123A58
			public override void TrimToSize()
			{
				object root = this._root;
				lock (root)
				{
					this._list.TrimToSize();
				}
			}

			// Token: 0x04002DB6 RID: 11702
			private SortedList _list;

			// Token: 0x04002DB7 RID: 11703
			private object _root;
		}

		// Token: 0x020009C8 RID: 2504
		[Serializable]
		private class SortedListEnumerator : IDictionaryEnumerator, IEnumerator, ICloneable
		{
			// Token: 0x06005A9B RID: 23195 RVA: 0x001258A0 File Offset: 0x00123AA0
			internal SortedListEnumerator(SortedList sortedList, int index, int count, int getObjRetType)
			{
				this.sortedList = sortedList;
				this.index = index;
				this.startIndex = index;
				this.endIndex = index + count;
				this.version = sortedList.version;
				this.getObjectRetType = getObjRetType;
				this.current = false;
			}

			// Token: 0x06005A9C RID: 23196 RVA: 0x0002BE93 File Offset: 0x0002A093
			public object Clone()
			{
				return base.MemberwiseClone();
			}

			// Token: 0x17001021 RID: 4129
			// (get) Token: 0x06005A9D RID: 23197 RVA: 0x001258EC File Offset: 0x00123AEC
			public virtual object Key
			{
				get
				{
					if (this.version != this.sortedList.version)
					{
						throw new InvalidOperationException(Environment.GetResourceString("Collection was modified; enumeration operation may not execute."));
					}
					if (!this.current)
					{
						throw new InvalidOperationException(Environment.GetResourceString("Enumeration has either not started or has already finished."));
					}
					return this.key;
				}
			}

			// Token: 0x06005A9E RID: 23198 RVA: 0x0012593C File Offset: 0x00123B3C
			public virtual bool MoveNext()
			{
				if (this.version != this.sortedList.version)
				{
					throw new InvalidOperationException(Environment.GetResourceString("Collection was modified; enumeration operation may not execute."));
				}
				if (this.index < this.endIndex)
				{
					this.key = this.sortedList.keys[this.index];
					this.value = this.sortedList.values[this.index];
					this.index++;
					this.current = true;
					return true;
				}
				this.key = null;
				this.value = null;
				this.current = false;
				return false;
			}

			// Token: 0x17001022 RID: 4130
			// (get) Token: 0x06005A9F RID: 23199 RVA: 0x001259D8 File Offset: 0x00123BD8
			public virtual DictionaryEntry Entry
			{
				get
				{
					if (this.version != this.sortedList.version)
					{
						throw new InvalidOperationException(Environment.GetResourceString("Collection was modified; enumeration operation may not execute."));
					}
					if (!this.current)
					{
						throw new InvalidOperationException(Environment.GetResourceString("Enumeration has either not started or has already finished."));
					}
					return new DictionaryEntry(this.key, this.value);
				}
			}

			// Token: 0x17001023 RID: 4131
			// (get) Token: 0x06005AA0 RID: 23200 RVA: 0x00125A34 File Offset: 0x00123C34
			public virtual object Current
			{
				get
				{
					if (!this.current)
					{
						throw new InvalidOperationException(Environment.GetResourceString("Enumeration has either not started or has already finished."));
					}
					if (this.getObjectRetType == 1)
					{
						return this.key;
					}
					if (this.getObjectRetType == 2)
					{
						return this.value;
					}
					return new DictionaryEntry(this.key, this.value);
				}
			}

			// Token: 0x17001024 RID: 4132
			// (get) Token: 0x06005AA1 RID: 23201 RVA: 0x00125A90 File Offset: 0x00123C90
			public virtual object Value
			{
				get
				{
					if (this.version != this.sortedList.version)
					{
						throw new InvalidOperationException(Environment.GetResourceString("Collection was modified; enumeration operation may not execute."));
					}
					if (!this.current)
					{
						throw new InvalidOperationException(Environment.GetResourceString("Enumeration has either not started or has already finished."));
					}
					return this.value;
				}
			}

			// Token: 0x06005AA2 RID: 23202 RVA: 0x00125AE0 File Offset: 0x00123CE0
			public virtual void Reset()
			{
				if (this.version != this.sortedList.version)
				{
					throw new InvalidOperationException(Environment.GetResourceString("Collection was modified; enumeration operation may not execute."));
				}
				this.index = this.startIndex;
				this.current = false;
				this.key = null;
				this.value = null;
			}

			// Token: 0x04002DB8 RID: 11704
			private SortedList sortedList;

			// Token: 0x04002DB9 RID: 11705
			private object key;

			// Token: 0x04002DBA RID: 11706
			private object value;

			// Token: 0x04002DBB RID: 11707
			private int index;

			// Token: 0x04002DBC RID: 11708
			private int startIndex;

			// Token: 0x04002DBD RID: 11709
			private int endIndex;

			// Token: 0x04002DBE RID: 11710
			private int version;

			// Token: 0x04002DBF RID: 11711
			private bool current;

			// Token: 0x04002DC0 RID: 11712
			private int getObjectRetType;

			// Token: 0x04002DC1 RID: 11713
			internal const int Keys = 1;

			// Token: 0x04002DC2 RID: 11714
			internal const int Values = 2;

			// Token: 0x04002DC3 RID: 11715
			internal const int DictEntry = 3;
		}

		// Token: 0x020009C9 RID: 2505
		[Serializable]
		private class KeyList : IList, ICollection, IEnumerable
		{
			// Token: 0x06005AA3 RID: 23203 RVA: 0x00125B31 File Offset: 0x00123D31
			internal KeyList(SortedList sortedList)
			{
				this.sortedList = sortedList;
			}

			// Token: 0x17001025 RID: 4133
			// (get) Token: 0x06005AA4 RID: 23204 RVA: 0x00125B40 File Offset: 0x00123D40
			public virtual int Count
			{
				get
				{
					return this.sortedList._size;
				}
			}

			// Token: 0x17001026 RID: 4134
			// (get) Token: 0x06005AA5 RID: 23205 RVA: 0x00004E08 File Offset: 0x00003008
			public virtual bool IsReadOnly
			{
				get
				{
					return true;
				}
			}

			// Token: 0x17001027 RID: 4135
			// (get) Token: 0x06005AA6 RID: 23206 RVA: 0x00004E08 File Offset: 0x00003008
			public virtual bool IsFixedSize
			{
				get
				{
					return true;
				}
			}

			// Token: 0x17001028 RID: 4136
			// (get) Token: 0x06005AA7 RID: 23207 RVA: 0x00125B4D File Offset: 0x00123D4D
			public virtual bool IsSynchronized
			{
				get
				{
					return this.sortedList.IsSynchronized;
				}
			}

			// Token: 0x17001029 RID: 4137
			// (get) Token: 0x06005AA8 RID: 23208 RVA: 0x00125B5A File Offset: 0x00123D5A
			public virtual object SyncRoot
			{
				get
				{
					return this.sortedList.SyncRoot;
				}
			}

			// Token: 0x06005AA9 RID: 23209 RVA: 0x00125B67 File Offset: 0x00123D67
			public virtual int Add(object key)
			{
				throw new NotSupportedException(Environment.GetResourceString("This operation is not supported on SortedList nested types because they require modifying the original SortedList."));
			}

			// Token: 0x06005AAA RID: 23210 RVA: 0x00125B67 File Offset: 0x00123D67
			public virtual void Clear()
			{
				throw new NotSupportedException(Environment.GetResourceString("This operation is not supported on SortedList nested types because they require modifying the original SortedList."));
			}

			// Token: 0x06005AAB RID: 23211 RVA: 0x00125B78 File Offset: 0x00123D78
			public virtual bool Contains(object key)
			{
				return this.sortedList.Contains(key);
			}

			// Token: 0x06005AAC RID: 23212 RVA: 0x00125B86 File Offset: 0x00123D86
			public virtual void CopyTo(Array array, int arrayIndex)
			{
				if (array != null && array.Rank != 1)
				{
					throw new ArgumentException(Environment.GetResourceString("Only single dimensional arrays are supported for the requested action."));
				}
				Array.Copy(this.sortedList.keys, 0, array, arrayIndex, this.sortedList.Count);
			}

			// Token: 0x06005AAD RID: 23213 RVA: 0x00125B67 File Offset: 0x00123D67
			public virtual void Insert(int index, object value)
			{
				throw new NotSupportedException(Environment.GetResourceString("This operation is not supported on SortedList nested types because they require modifying the original SortedList."));
			}

			// Token: 0x1700102A RID: 4138
			public virtual object this[int index]
			{
				get
				{
					return this.sortedList.GetKey(index);
				}
				set
				{
					throw new NotSupportedException(Environment.GetResourceString("Mutating a key collection derived from a dictionary is not allowed."));
				}
			}

			// Token: 0x06005AB0 RID: 23216 RVA: 0x00125BE1 File Offset: 0x00123DE1
			public virtual IEnumerator GetEnumerator()
			{
				return new SortedList.SortedListEnumerator(this.sortedList, 0, this.sortedList.Count, 1);
			}

			// Token: 0x06005AB1 RID: 23217 RVA: 0x00125BFC File Offset: 0x00123DFC
			public virtual int IndexOf(object key)
			{
				if (key == null)
				{
					throw new ArgumentNullException("key", Environment.GetResourceString("Key cannot be null."));
				}
				int num = Array.BinarySearch(this.sortedList.keys, 0, this.sortedList.Count, key, this.sortedList.comparer);
				if (num >= 0)
				{
					return num;
				}
				return -1;
			}

			// Token: 0x06005AB2 RID: 23218 RVA: 0x00125B67 File Offset: 0x00123D67
			public virtual void Remove(object key)
			{
				throw new NotSupportedException(Environment.GetResourceString("This operation is not supported on SortedList nested types because they require modifying the original SortedList."));
			}

			// Token: 0x06005AB3 RID: 23219 RVA: 0x00125B67 File Offset: 0x00123D67
			public virtual void RemoveAt(int index)
			{
				throw new NotSupportedException(Environment.GetResourceString("This operation is not supported on SortedList nested types because they require modifying the original SortedList."));
			}

			// Token: 0x04002DC4 RID: 11716
			private SortedList sortedList;
		}

		// Token: 0x020009CA RID: 2506
		[Serializable]
		private class ValueList : IList, ICollection, IEnumerable
		{
			// Token: 0x06005AB4 RID: 23220 RVA: 0x00125C51 File Offset: 0x00123E51
			internal ValueList(SortedList sortedList)
			{
				this.sortedList = sortedList;
			}

			// Token: 0x1700102B RID: 4139
			// (get) Token: 0x06005AB5 RID: 23221 RVA: 0x00125C60 File Offset: 0x00123E60
			public virtual int Count
			{
				get
				{
					return this.sortedList._size;
				}
			}

			// Token: 0x1700102C RID: 4140
			// (get) Token: 0x06005AB6 RID: 23222 RVA: 0x00004E08 File Offset: 0x00003008
			public virtual bool IsReadOnly
			{
				get
				{
					return true;
				}
			}

			// Token: 0x1700102D RID: 4141
			// (get) Token: 0x06005AB7 RID: 23223 RVA: 0x00004E08 File Offset: 0x00003008
			public virtual bool IsFixedSize
			{
				get
				{
					return true;
				}
			}

			// Token: 0x1700102E RID: 4142
			// (get) Token: 0x06005AB8 RID: 23224 RVA: 0x00125C6D File Offset: 0x00123E6D
			public virtual bool IsSynchronized
			{
				get
				{
					return this.sortedList.IsSynchronized;
				}
			}

			// Token: 0x1700102F RID: 4143
			// (get) Token: 0x06005AB9 RID: 23225 RVA: 0x00125C7A File Offset: 0x00123E7A
			public virtual object SyncRoot
			{
				get
				{
					return this.sortedList.SyncRoot;
				}
			}

			// Token: 0x06005ABA RID: 23226 RVA: 0x00125B67 File Offset: 0x00123D67
			public virtual int Add(object key)
			{
				throw new NotSupportedException(Environment.GetResourceString("This operation is not supported on SortedList nested types because they require modifying the original SortedList."));
			}

			// Token: 0x06005ABB RID: 23227 RVA: 0x00125B67 File Offset: 0x00123D67
			public virtual void Clear()
			{
				throw new NotSupportedException(Environment.GetResourceString("This operation is not supported on SortedList nested types because they require modifying the original SortedList."));
			}

			// Token: 0x06005ABC RID: 23228 RVA: 0x00125C87 File Offset: 0x00123E87
			public virtual bool Contains(object value)
			{
				return this.sortedList.ContainsValue(value);
			}

			// Token: 0x06005ABD RID: 23229 RVA: 0x00125C95 File Offset: 0x00123E95
			public virtual void CopyTo(Array array, int arrayIndex)
			{
				if (array != null && array.Rank != 1)
				{
					throw new ArgumentException(Environment.GetResourceString("Only single dimensional arrays are supported for the requested action."));
				}
				Array.Copy(this.sortedList.values, 0, array, arrayIndex, this.sortedList.Count);
			}

			// Token: 0x06005ABE RID: 23230 RVA: 0x00125B67 File Offset: 0x00123D67
			public virtual void Insert(int index, object value)
			{
				throw new NotSupportedException(Environment.GetResourceString("This operation is not supported on SortedList nested types because they require modifying the original SortedList."));
			}

			// Token: 0x17001030 RID: 4144
			public virtual object this[int index]
			{
				get
				{
					return this.sortedList.GetByIndex(index);
				}
				set
				{
					throw new NotSupportedException(Environment.GetResourceString("This operation is not supported on SortedList nested types because they require modifying the original SortedList."));
				}
			}

			// Token: 0x06005AC1 RID: 23233 RVA: 0x00125CDF File Offset: 0x00123EDF
			public virtual IEnumerator GetEnumerator()
			{
				return new SortedList.SortedListEnumerator(this.sortedList, 0, this.sortedList.Count, 2);
			}

			// Token: 0x06005AC2 RID: 23234 RVA: 0x00125CF9 File Offset: 0x00123EF9
			public virtual int IndexOf(object value)
			{
				return Array.IndexOf<object>(this.sortedList.values, value, 0, this.sortedList.Count);
			}

			// Token: 0x06005AC3 RID: 23235 RVA: 0x00125B67 File Offset: 0x00123D67
			public virtual void Remove(object value)
			{
				throw new NotSupportedException(Environment.GetResourceString("This operation is not supported on SortedList nested types because they require modifying the original SortedList."));
			}

			// Token: 0x06005AC4 RID: 23236 RVA: 0x00125B67 File Offset: 0x00123D67
			public virtual void RemoveAt(int index)
			{
				throw new NotSupportedException(Environment.GetResourceString("This operation is not supported on SortedList nested types because they require modifying the original SortedList."));
			}

			// Token: 0x04002DC5 RID: 11717
			private SortedList sortedList;
		}

		// Token: 0x020009CB RID: 2507
		internal class SortedListDebugView
		{
			// Token: 0x06005AC5 RID: 23237 RVA: 0x00125D18 File Offset: 0x00123F18
			public SortedListDebugView(SortedList sortedList)
			{
				if (sortedList == null)
				{
					throw new ArgumentNullException("sortedList");
				}
				this.sortedList = sortedList;
			}

			// Token: 0x17001031 RID: 4145
			// (get) Token: 0x06005AC6 RID: 23238 RVA: 0x00125D35 File Offset: 0x00123F35
			[DebuggerBrowsable(DebuggerBrowsableState.RootHidden)]
			public KeyValuePairs[] Items
			{
				get
				{
					return this.sortedList.ToKeyValuePairsArray();
				}
			}

			// Token: 0x04002DC6 RID: 11718
			private SortedList sortedList;
		}
	}
}
