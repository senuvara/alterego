﻿using System;

namespace System.Collections
{
	// Token: 0x02000990 RID: 2448
	internal sealed class LowLevelComparer : IComparer
	{
		// Token: 0x0600579A RID: 22426 RVA: 0x00002050 File Offset: 0x00000250
		private LowLevelComparer()
		{
		}

		// Token: 0x0600579B RID: 22427 RVA: 0x0011CB30 File Offset: 0x0011AD30
		public int Compare(object a, object b)
		{
			if (a == b)
			{
				return 0;
			}
			if (a == null)
			{
				return -1;
			}
			if (b == null)
			{
				return 1;
			}
			IComparable comparable = a as IComparable;
			if (comparable != null)
			{
				return comparable.CompareTo(b);
			}
			IComparable comparable2 = b as IComparable;
			if (comparable2 != null)
			{
				return -comparable2.CompareTo(a);
			}
			throw new ArgumentException("At least one object must implement IComparable.");
		}

		// Token: 0x0600579C RID: 22428 RVA: 0x0011CB7C File Offset: 0x0011AD7C
		// Note: this type is marked as 'beforefieldinit'.
		static LowLevelComparer()
		{
		}

		// Token: 0x04002D1F RID: 11551
		internal static readonly LowLevelComparer Default = new LowLevelComparer();
	}
}
