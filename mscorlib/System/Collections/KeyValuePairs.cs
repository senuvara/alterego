﻿using System;
using System.Diagnostics;

namespace System.Collections
{
	// Token: 0x020009BB RID: 2491
	[DebuggerDisplay("{value}", Name = "[{key}]", Type = "")]
	internal class KeyValuePairs
	{
		// Token: 0x060059FF RID: 23039 RVA: 0x001236A2 File Offset: 0x001218A2
		public KeyValuePairs(object key, object value)
		{
			this.value = value;
			this.key = key;
		}

		// Token: 0x17000FF3 RID: 4083
		// (get) Token: 0x06005A00 RID: 23040 RVA: 0x001236B8 File Offset: 0x001218B8
		public object Key
		{
			get
			{
				return this.key;
			}
		}

		// Token: 0x17000FF4 RID: 4084
		// (get) Token: 0x06005A01 RID: 23041 RVA: 0x001236C0 File Offset: 0x001218C0
		public object Value
		{
			get
			{
				return this.value;
			}
		}

		// Token: 0x04002D87 RID: 11655
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private object key;

		// Token: 0x04002D88 RID: 11656
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private object value;
	}
}
