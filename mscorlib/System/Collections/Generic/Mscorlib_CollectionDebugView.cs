﻿using System;
using System.Diagnostics;

namespace System.Collections.Generic
{
	// Token: 0x02000A23 RID: 2595
	internal sealed class Mscorlib_CollectionDebugView<T>
	{
		// Token: 0x06005D81 RID: 23937 RVA: 0x0012E21B File Offset: 0x0012C41B
		public Mscorlib_CollectionDebugView(ICollection<T> collection)
		{
			if (collection == null)
			{
				ThrowHelper.ThrowArgumentNullException(ExceptionArgument.collection);
			}
			this.collection = collection;
		}

		// Token: 0x170010DF RID: 4319
		// (get) Token: 0x06005D82 RID: 23938 RVA: 0x0012E234 File Offset: 0x0012C434
		[DebuggerBrowsable(DebuggerBrowsableState.RootHidden)]
		public T[] Items
		{
			get
			{
				T[] array = new T[this.collection.Count];
				this.collection.CopyTo(array, 0);
				return array;
			}
		}

		// Token: 0x04002EAA RID: 11946
		private ICollection<T> collection;
	}
}
