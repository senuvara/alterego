﻿using System;

namespace System.Collections.Generic
{
	// Token: 0x02000A1D RID: 2589
	internal sealed class ObjectEqualityComparer : IEqualityComparer
	{
		// Token: 0x06005D69 RID: 23913 RVA: 0x00002050 File Offset: 0x00000250
		private ObjectEqualityComparer()
		{
		}

		// Token: 0x06005D6A RID: 23914 RVA: 0x0012DFDF File Offset: 0x0012C1DF
		int IEqualityComparer.GetHashCode(object obj)
		{
			if (obj == null)
			{
				return 0;
			}
			return obj.GetHashCode();
		}

		// Token: 0x06005D6B RID: 23915 RVA: 0x0012DFEC File Offset: 0x0012C1EC
		bool IEqualityComparer.Equals(object x, object y)
		{
			if (x == null)
			{
				return y == null;
			}
			return y != null && x.Equals(y);
		}

		// Token: 0x06005D6C RID: 23916 RVA: 0x0012E002 File Offset: 0x0012C202
		// Note: this type is marked as 'beforefieldinit'.
		static ObjectEqualityComparer()
		{
		}

		// Token: 0x04002EA7 RID: 11943
		internal static readonly ObjectEqualityComparer Default = new ObjectEqualityComparer();
	}
}
