﻿using System;

namespace System.Collections.Generic
{
	// Token: 0x02000A10 RID: 2576
	internal enum InsertionBehavior : byte
	{
		// Token: 0x04002E7C RID: 11900
		None,
		// Token: 0x04002E7D RID: 11901
		OverwriteExisting,
		// Token: 0x04002E7E RID: 11902
		ThrowOnExisting
	}
}
