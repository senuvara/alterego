﻿using System;
using System.Diagnostics;

namespace System.Collections.Generic
{
	// Token: 0x02000A1B RID: 2587
	internal sealed class DictionaryValueCollectionDebugView<TKey, TValue>
	{
		// Token: 0x06005D63 RID: 23907 RVA: 0x0012DF64 File Offset: 0x0012C164
		public DictionaryValueCollectionDebugView(ICollection<TValue> collection)
		{
			if (collection == null)
			{
				throw new ArgumentNullException("collection");
			}
			this._collection = collection;
		}

		// Token: 0x170010DC RID: 4316
		// (get) Token: 0x06005D64 RID: 23908 RVA: 0x0012DF84 File Offset: 0x0012C184
		[DebuggerBrowsable(DebuggerBrowsableState.RootHidden)]
		public TValue[] Items
		{
			get
			{
				TValue[] array = new TValue[this._collection.Count];
				this._collection.CopyTo(array, 0);
				return array;
			}
		}

		// Token: 0x04002EA5 RID: 11941
		private readonly ICollection<TValue> _collection;
	}
}
