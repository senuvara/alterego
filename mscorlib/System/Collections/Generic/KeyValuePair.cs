﻿using System;
using System.Text;

namespace System.Collections.Generic
{
	// Token: 0x02000A0B RID: 2571
	public static class KeyValuePair
	{
		// Token: 0x06005CD0 RID: 23760 RVA: 0x0012B979 File Offset: 0x00129B79
		public static KeyValuePair<TKey, TValue> Create<TKey, TValue>(TKey key, TValue value)
		{
			return new KeyValuePair<TKey, TValue>(key, value);
		}

		// Token: 0x06005CD1 RID: 23761 RVA: 0x0012B984 File Offset: 0x00129B84
		internal static string PairToString(object key, object value)
		{
			StringBuilder stringBuilder = StringBuilderCache.Acquire(16);
			stringBuilder.Append('[');
			if (key != null)
			{
				stringBuilder.Append(key);
			}
			stringBuilder.Append(", ");
			if (value != null)
			{
				stringBuilder.Append(value);
			}
			stringBuilder.Append(']');
			return StringBuilderCache.GetStringAndRelease(stringBuilder);
		}
	}
}
