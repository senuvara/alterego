﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;

namespace System.Collections.Generic
{
	// Token: 0x02000A2E RID: 2606
	[Serializable]
	internal sealed class SByteEnumEqualityComparer<T> : EnumEqualityComparer<T>, ISerializable where T : struct
	{
		// Token: 0x06005DB7 RID: 23991 RVA: 0x0012EAE0 File Offset: 0x0012CCE0
		public SByteEnumEqualityComparer()
		{
		}

		// Token: 0x06005DB8 RID: 23992 RVA: 0x0012EAE0 File Offset: 0x0012CCE0
		public SByteEnumEqualityComparer(SerializationInfo information, StreamingContext context)
		{
		}

		// Token: 0x06005DB9 RID: 23993 RVA: 0x0012EAE8 File Offset: 0x0012CCE8
		public override int GetHashCode(T obj)
		{
			return ((sbyte)JitHelpers.UnsafeEnumCast<T>(obj)).GetHashCode();
		}
	}
}
