﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;

namespace System.Collections.Generic
{
	// Token: 0x02000A18 RID: 2584
	internal class DictionaryHashHelpers
	{
		// Token: 0x170010D9 RID: 4313
		// (get) Token: 0x06005D5C RID: 23900 RVA: 0x0012DEBB File Offset: 0x0012C0BB
		internal static ConditionalWeakTable<object, SerializationInfo> SerializationInfoTable
		{
			[CompilerGenerated]
			get
			{
				return DictionaryHashHelpers.<SerializationInfoTable>k__BackingField;
			}
		} = new ConditionalWeakTable<object, SerializationInfo>();

		// Token: 0x06005D5D RID: 23901 RVA: 0x00002050 File Offset: 0x00000250
		public DictionaryHashHelpers()
		{
		}

		// Token: 0x06005D5E RID: 23902 RVA: 0x0012DEC2 File Offset: 0x0012C0C2
		// Note: this type is marked as 'beforefieldinit'.
		static DictionaryHashHelpers()
		{
		}

		// Token: 0x04002EA2 RID: 11938
		[CompilerGenerated]
		private static readonly ConditionalWeakTable<object, SerializationInfo> <SerializationInfoTable>k__BackingField;
	}
}
