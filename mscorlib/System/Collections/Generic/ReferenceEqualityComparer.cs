﻿using System;
using System.Runtime.CompilerServices;

namespace System.Collections.Generic
{
	// Token: 0x02000A09 RID: 2569
	internal sealed class ReferenceEqualityComparer<T> : IEqualityComparer<T> where T : class
	{
		// Token: 0x06005CC8 RID: 23752 RVA: 0x00002050 File Offset: 0x00000250
		private ReferenceEqualityComparer()
		{
		}

		// Token: 0x06005CC9 RID: 23753 RVA: 0x0012B8B7 File Offset: 0x00129AB7
		public bool Equals(T x, T y)
		{
			return x == y;
		}

		// Token: 0x06005CCA RID: 23754 RVA: 0x0012B8C7 File Offset: 0x00129AC7
		public int GetHashCode(T obj)
		{
			return RuntimeHelpers.GetHashCode(obj);
		}

		// Token: 0x06005CCB RID: 23755 RVA: 0x0012B8D4 File Offset: 0x00129AD4
		// Note: this type is marked as 'beforefieldinit'.
		static ReferenceEqualityComparer()
		{
		}

		// Token: 0x04002E76 RID: 11894
		internal static readonly ReferenceEqualityComparer<T> Instance = new ReferenceEqualityComparer<T>();
	}
}
