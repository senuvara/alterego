﻿using System;
using System.Diagnostics;

namespace System.Collections.Generic
{
	// Token: 0x02000A1A RID: 2586
	internal sealed class DictionaryKeyCollectionDebugView<TKey, TValue>
	{
		// Token: 0x06005D61 RID: 23905 RVA: 0x0012DF18 File Offset: 0x0012C118
		public DictionaryKeyCollectionDebugView(ICollection<TKey> collection)
		{
			if (collection == null)
			{
				throw new ArgumentNullException("collection");
			}
			this._collection = collection;
		}

		// Token: 0x170010DB RID: 4315
		// (get) Token: 0x06005D62 RID: 23906 RVA: 0x0012DF38 File Offset: 0x0012C138
		[DebuggerBrowsable(DebuggerBrowsableState.RootHidden)]
		public TKey[] Items
		{
			get
			{
				TKey[] array = new TKey[this._collection.Count];
				this._collection.CopyTo(array, 0);
				return array;
			}
		}

		// Token: 0x04002EA4 RID: 11940
		private readonly ICollection<TKey> _collection;
	}
}
