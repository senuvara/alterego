﻿using System;

namespace System.Collections.Generic
{
	// Token: 0x02000A0D RID: 2573
	internal static class IntrospectiveSortUtilities
	{
		// Token: 0x06005CD7 RID: 23767 RVA: 0x0012BA2C File Offset: 0x00129C2C
		internal static int FloorLog2(int n)
		{
			int num = 0;
			while (n >= 1)
			{
				num++;
				n /= 2;
			}
			return num;
		}

		// Token: 0x06005CD8 RID: 23768 RVA: 0x0012BA4B File Offset: 0x00129C4B
		internal static void ThrowOrIgnoreBadComparer(object comparer)
		{
			throw new ArgumentException(SR.Format("Unable to sort because the IComparer.Compare() method returns inconsistent results. Either a value does not compare equal to itself, or one value repeatedly compared to another value yields different results. IComparer: '{0}'.", comparer));
		}

		// Token: 0x04002E79 RID: 11897
		internal const int IntrosortSizeThreshold = 16;
	}
}
