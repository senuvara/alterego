﻿using System;

namespace System.Collections.Generic
{
	// Token: 0x02000A20 RID: 2592
	[Serializable]
	internal class NullableComparer<T> : Comparer<T?> where T : struct, IComparable<T>
	{
		// Token: 0x06005D77 RID: 23927 RVA: 0x0012E18C File Offset: 0x0012C38C
		public override int Compare(T? x, T? y)
		{
			if (x != null)
			{
				if (y != null)
				{
					return x.value.CompareTo(y.value);
				}
				return 1;
			}
			else
			{
				if (y != null)
				{
					return -1;
				}
				return 0;
			}
		}

		// Token: 0x06005D78 RID: 23928 RVA: 0x0012E1C7 File Offset: 0x0012C3C7
		public override bool Equals(object obj)
		{
			return obj is NullableComparer<T>;
		}

		// Token: 0x06005D79 RID: 23929 RVA: 0x0012E172 File Offset: 0x0012C372
		public override int GetHashCode()
		{
			return base.GetType().Name.GetHashCode();
		}

		// Token: 0x06005D7A RID: 23930 RVA: 0x0012E1D2 File Offset: 0x0012C3D2
		public NullableComparer()
		{
		}
	}
}
