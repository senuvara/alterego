﻿using System;

namespace System.Collections.Generic
{
	// Token: 0x02000A22 RID: 2594
	[Serializable]
	internal class ComparisonComparer<T> : Comparer<T>
	{
		// Token: 0x06005D7F RID: 23935 RVA: 0x0012E1FD File Offset: 0x0012C3FD
		public ComparisonComparer(Comparison<T> comparison)
		{
			this._comparison = comparison;
		}

		// Token: 0x06005D80 RID: 23936 RVA: 0x0012E20C File Offset: 0x0012C40C
		public override int Compare(T x, T y)
		{
			return this._comparison(x, y);
		}

		// Token: 0x04002EA9 RID: 11945
		private readonly Comparison<T> _comparison;
	}
}
