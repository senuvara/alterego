﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;

namespace System.Collections.Generic
{
	// Token: 0x02000A27 RID: 2599
	internal sealed class Mscorlib_KeyedCollectionDebugView<K, T>
	{
		// Token: 0x06005D89 RID: 23945 RVA: 0x0012E32C File Offset: 0x0012C52C
		public Mscorlib_KeyedCollectionDebugView(KeyedCollection<K, T> keyedCollection)
		{
			if (keyedCollection == null)
			{
				throw new ArgumentNullException("keyedCollection");
			}
			this.kc = keyedCollection;
		}

		// Token: 0x170010E3 RID: 4323
		// (get) Token: 0x06005D8A RID: 23946 RVA: 0x0012E34C File Offset: 0x0012C54C
		[DebuggerBrowsable(DebuggerBrowsableState.RootHidden)]
		public T[] Items
		{
			get
			{
				T[] array = new T[this.kc.Count];
				this.kc.CopyTo(array, 0);
				return array;
			}
		}

		// Token: 0x04002EAE RID: 11950
		private KeyedCollection<K, T> kc;
	}
}
