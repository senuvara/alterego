﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;
using System.Security;

namespace System.Collections.Generic
{
	// Token: 0x02000A30 RID: 2608
	[Serializable]
	internal sealed class LongEnumEqualityComparer<T> : EqualityComparer<T>, ISerializable where T : struct
	{
		// Token: 0x06005DBD RID: 23997 RVA: 0x0012EB20 File Offset: 0x0012CD20
		public override bool Equals(T x, T y)
		{
			long num = JitHelpers.UnsafeEnumCastLong<T>(x);
			long num2 = JitHelpers.UnsafeEnumCastLong<T>(y);
			return num == num2;
		}

		// Token: 0x06005DBE RID: 23998 RVA: 0x0012EB40 File Offset: 0x0012CD40
		public override int GetHashCode(T obj)
		{
			return JitHelpers.UnsafeEnumCastLong<T>(obj).GetHashCode();
		}

		// Token: 0x06005DBF RID: 23999 RVA: 0x0012EB5B File Offset: 0x0012CD5B
		public override bool Equals(object obj)
		{
			return obj is LongEnumEqualityComparer<T>;
		}

		// Token: 0x06005DC0 RID: 24000 RVA: 0x0012E172 File Offset: 0x0012C372
		public override int GetHashCode()
		{
			return base.GetType().Name.GetHashCode();
		}

		// Token: 0x06005DC1 RID: 24001 RVA: 0x0012E715 File Offset: 0x0012C915
		public LongEnumEqualityComparer()
		{
		}

		// Token: 0x06005DC2 RID: 24002 RVA: 0x0012E715 File Offset: 0x0012C915
		public LongEnumEqualityComparer(SerializationInfo information, StreamingContext context)
		{
		}

		// Token: 0x06005DC3 RID: 24003 RVA: 0x0012EB66 File Offset: 0x0012CD66
		[SecurityCritical]
		public void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.SetType(typeof(ObjectEqualityComparer<T>));
		}
	}
}
