﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;
using System.Security;

namespace System.Collections.Generic
{
	// Token: 0x02000A2D RID: 2605
	[Serializable]
	internal class EnumEqualityComparer<T> : EqualityComparer<T>, ISerializable where T : struct
	{
		// Token: 0x06005DB0 RID: 23984 RVA: 0x0012EA70 File Offset: 0x0012CC70
		public override bool Equals(T x, T y)
		{
			int num = JitHelpers.UnsafeEnumCast<T>(x);
			int num2 = JitHelpers.UnsafeEnumCast<T>(y);
			return num == num2;
		}

		// Token: 0x06005DB1 RID: 23985 RVA: 0x0012EA90 File Offset: 0x0012CC90
		public override int GetHashCode(T obj)
		{
			return JitHelpers.UnsafeEnumCast<T>(obj).GetHashCode();
		}

		// Token: 0x06005DB2 RID: 23986 RVA: 0x0012E715 File Offset: 0x0012C915
		public EnumEqualityComparer()
		{
		}

		// Token: 0x06005DB3 RID: 23987 RVA: 0x0012E715 File Offset: 0x0012C915
		protected EnumEqualityComparer(SerializationInfo information, StreamingContext context)
		{
		}

		// Token: 0x06005DB4 RID: 23988 RVA: 0x0012EAAB File Offset: 0x0012CCAB
		[SecurityCritical]
		public void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			if (Type.GetTypeCode(Enum.GetUnderlyingType(typeof(T))) != TypeCode.Int32)
			{
				info.SetType(typeof(ObjectEqualityComparer<T>));
			}
		}

		// Token: 0x06005DB5 RID: 23989 RVA: 0x0012EAD5 File Offset: 0x0012CCD5
		public override bool Equals(object obj)
		{
			return obj is EnumEqualityComparer<T>;
		}

		// Token: 0x06005DB6 RID: 23990 RVA: 0x0012E172 File Offset: 0x0012C372
		public override int GetHashCode()
		{
			return base.GetType().Name.GetHashCode();
		}
	}
}
