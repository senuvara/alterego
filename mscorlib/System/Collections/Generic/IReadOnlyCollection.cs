﻿using System;
using System.Runtime.CompilerServices;

namespace System.Collections.Generic
{
	/// <summary>Represents a strongly-typed, read-only collection of elements.</summary>
	/// <typeparam name="T">The type of the elements.</typeparam>
	// Token: 0x02000A39 RID: 2617
	[TypeDependency("System.SZArrayHelper")]
	public interface IReadOnlyCollection<out T> : IEnumerable<!0>, IEnumerable
	{
		/// <summary>Gets the number of elements in the collection.</summary>
		/// <returns>The number of elements in the collection. </returns>
		// Token: 0x170010EC RID: 4332
		// (get) Token: 0x06005DE1 RID: 24033
		int Count { get; }
	}
}
