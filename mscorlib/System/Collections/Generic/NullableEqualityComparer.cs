﻿using System;

namespace System.Collections.Generic
{
	// Token: 0x02000A2A RID: 2602
	[Serializable]
	internal class NullableEqualityComparer<T> : EqualityComparer<T?> where T : struct, IEquatable<T>
	{
		// Token: 0x06005D9B RID: 23963 RVA: 0x0012E71D File Offset: 0x0012C91D
		public override bool Equals(T? x, T? y)
		{
			if (x != null)
			{
				return y != null && x.value.Equals(y.value);
			}
			return y == null;
		}

		// Token: 0x06005D9C RID: 23964 RVA: 0x0012E758 File Offset: 0x0012C958
		public override int GetHashCode(T? obj)
		{
			return obj.GetHashCode();
		}

		// Token: 0x06005D9D RID: 23965 RVA: 0x0012E768 File Offset: 0x0012C968
		internal override int IndexOf(T?[] array, T? value, int startIndex, int count)
		{
			int num = startIndex + count;
			if (value == null)
			{
				for (int i = startIndex; i < num; i++)
				{
					if (array[i] == null)
					{
						return i;
					}
				}
			}
			else
			{
				for (int j = startIndex; j < num; j++)
				{
					if (array[j] != null && array[j].value.Equals(value.value))
					{
						return j;
					}
				}
			}
			return -1;
		}

		// Token: 0x06005D9E RID: 23966 RVA: 0x0012E7E0 File Offset: 0x0012C9E0
		internal override int LastIndexOf(T?[] array, T? value, int startIndex, int count)
		{
			int num = startIndex - count + 1;
			if (value == null)
			{
				for (int i = startIndex; i >= num; i--)
				{
					if (array[i] == null)
					{
						return i;
					}
				}
			}
			else
			{
				for (int j = startIndex; j >= num; j--)
				{
					if (array[j] != null && array[j].value.Equals(value.value))
					{
						return j;
					}
				}
			}
			return -1;
		}

		// Token: 0x06005D9F RID: 23967 RVA: 0x0012E857 File Offset: 0x0012CA57
		public override bool Equals(object obj)
		{
			return obj is NullableEqualityComparer<T>;
		}

		// Token: 0x06005DA0 RID: 23968 RVA: 0x0012E172 File Offset: 0x0012C372
		public override int GetHashCode()
		{
			return base.GetType().Name.GetHashCode();
		}

		// Token: 0x06005DA1 RID: 23969 RVA: 0x0012E862 File Offset: 0x0012CA62
		public NullableEqualityComparer()
		{
		}
	}
}
