﻿using System;
using System.Diagnostics;

namespace System.Collections.Generic
{
	// Token: 0x02000A24 RID: 2596
	internal sealed class Mscorlib_DictionaryKeyCollectionDebugView<TKey, TValue>
	{
		// Token: 0x06005D83 RID: 23939 RVA: 0x0012E260 File Offset: 0x0012C460
		public Mscorlib_DictionaryKeyCollectionDebugView(ICollection<TKey> collection)
		{
			if (collection == null)
			{
				ThrowHelper.ThrowArgumentNullException(ExceptionArgument.collection);
			}
			this.collection = collection;
		}

		// Token: 0x170010E0 RID: 4320
		// (get) Token: 0x06005D84 RID: 23940 RVA: 0x0012E278 File Offset: 0x0012C478
		[DebuggerBrowsable(DebuggerBrowsableState.RootHidden)]
		public TKey[] Items
		{
			get
			{
				TKey[] array = new TKey[this.collection.Count];
				this.collection.CopyTo(array, 0);
				return array;
			}
		}

		// Token: 0x04002EAB RID: 11947
		private ICollection<TKey> collection;
	}
}
