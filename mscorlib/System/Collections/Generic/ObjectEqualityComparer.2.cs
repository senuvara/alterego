﻿using System;

namespace System.Collections.Generic
{
	// Token: 0x02000A2B RID: 2603
	[Serializable]
	internal class ObjectEqualityComparer<T> : EqualityComparer<T>
	{
		// Token: 0x06005DA2 RID: 23970 RVA: 0x0012E86A File Offset: 0x0012CA6A
		public override bool Equals(T x, T y)
		{
			if (x != null)
			{
				return y != null && x.Equals(y);
			}
			return y == null;
		}

		// Token: 0x06005DA3 RID: 23971 RVA: 0x0012E616 File Offset: 0x0012C816
		public override int GetHashCode(T obj)
		{
			if (obj == null)
			{
				return 0;
			}
			return obj.GetHashCode();
		}

		// Token: 0x06005DA4 RID: 23972 RVA: 0x0012E8A0 File Offset: 0x0012CAA0
		internal override int IndexOf(T[] array, T value, int startIndex, int count)
		{
			int num = startIndex + count;
			if (value == null)
			{
				for (int i = startIndex; i < num; i++)
				{
					if (array[i] == null)
					{
						return i;
					}
				}
			}
			else
			{
				for (int j = startIndex; j < num; j++)
				{
					if (array[j] != null && array[j].Equals(value))
					{
						return j;
					}
				}
			}
			return -1;
		}

		// Token: 0x06005DA5 RID: 23973 RVA: 0x0012E914 File Offset: 0x0012CB14
		internal override int LastIndexOf(T[] array, T value, int startIndex, int count)
		{
			int num = startIndex - count + 1;
			if (value == null)
			{
				for (int i = startIndex; i >= num; i--)
				{
					if (array[i] == null)
					{
						return i;
					}
				}
			}
			else
			{
				for (int j = startIndex; j >= num; j--)
				{
					if (array[j] != null && array[j].Equals(value))
					{
						return j;
					}
				}
			}
			return -1;
		}

		// Token: 0x06005DA6 RID: 23974 RVA: 0x0012E987 File Offset: 0x0012CB87
		public override bool Equals(object obj)
		{
			return obj is ObjectEqualityComparer<T>;
		}

		// Token: 0x06005DA7 RID: 23975 RVA: 0x0012E172 File Offset: 0x0012C372
		public override int GetHashCode()
		{
			return base.GetType().Name.GetHashCode();
		}

		// Token: 0x06005DA8 RID: 23976 RVA: 0x0012E715 File Offset: 0x0012C915
		public ObjectEqualityComparer()
		{
		}
	}
}
