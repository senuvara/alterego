﻿using System;
using System.Diagnostics;

namespace System.Collections.Generic
{
	// Token: 0x02000A26 RID: 2598
	internal sealed class Mscorlib_DictionaryDebugView<K, V>
	{
		// Token: 0x06005D87 RID: 23943 RVA: 0x0012E2E8 File Offset: 0x0012C4E8
		public Mscorlib_DictionaryDebugView(IDictionary<K, V> dictionary)
		{
			if (dictionary == null)
			{
				ThrowHelper.ThrowArgumentNullException(ExceptionArgument.dictionary);
			}
			this.dict = dictionary;
		}

		// Token: 0x170010E2 RID: 4322
		// (get) Token: 0x06005D88 RID: 23944 RVA: 0x0012E300 File Offset: 0x0012C500
		[DebuggerBrowsable(DebuggerBrowsableState.RootHidden)]
		public KeyValuePair<K, V>[] Items
		{
			get
			{
				KeyValuePair<K, V>[] array = new KeyValuePair<K, V>[this.dict.Count];
				this.dict.CopyTo(array, 0);
				return array;
			}
		}

		// Token: 0x04002EAD RID: 11949
		private IDictionary<K, V> dict;
	}
}
