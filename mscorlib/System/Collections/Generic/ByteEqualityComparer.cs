﻿using System;
using System.Security;

namespace System.Collections.Generic
{
	// Token: 0x02000A2C RID: 2604
	[Serializable]
	internal class ByteEqualityComparer : EqualityComparer<byte>
	{
		// Token: 0x06005DA9 RID: 23977 RVA: 0x0012E992 File Offset: 0x0012CB92
		public override bool Equals(byte x, byte y)
		{
			return x == y;
		}

		// Token: 0x06005DAA RID: 23978 RVA: 0x0012E998 File Offset: 0x0012CB98
		public override int GetHashCode(byte b)
		{
			return b.GetHashCode();
		}

		// Token: 0x06005DAB RID: 23979 RVA: 0x0012E9A4 File Offset: 0x0012CBA4
		[SecuritySafeCritical]
		internal unsafe override int IndexOf(byte[] array, byte value, int startIndex, int count)
		{
			if (array == null)
			{
				throw new ArgumentNullException("array");
			}
			if (startIndex < 0)
			{
				throw new ArgumentOutOfRangeException("startIndex", Environment.GetResourceString("Index was out of range. Must be non-negative and less than the size of the collection."));
			}
			if (count < 0)
			{
				throw new ArgumentOutOfRangeException("count", Environment.GetResourceString("Count must be positive and count must refer to a location within the string/array/collection."));
			}
			if (count > array.Length - startIndex)
			{
				throw new ArgumentException(Environment.GetResourceString("Offset and length were out of bounds for the array or count is greater than the number of elements from index to the end of the source collection."));
			}
			if (count == 0)
			{
				return -1;
			}
			byte* src;
			if (array == null || array.Length == 0)
			{
				src = null;
			}
			else
			{
				src = &array[0];
			}
			return Buffer.IndexOfByte(src, value, startIndex, count);
		}

		// Token: 0x06005DAC RID: 23980 RVA: 0x0012EA34 File Offset: 0x0012CC34
		internal override int LastIndexOf(byte[] array, byte value, int startIndex, int count)
		{
			int num = startIndex - count + 1;
			for (int i = startIndex; i >= num; i--)
			{
				if (array[i] == value)
				{
					return i;
				}
			}
			return -1;
		}

		// Token: 0x06005DAD RID: 23981 RVA: 0x0012EA5D File Offset: 0x0012CC5D
		public override bool Equals(object obj)
		{
			return obj is ByteEqualityComparer;
		}

		// Token: 0x06005DAE RID: 23982 RVA: 0x0012E172 File Offset: 0x0012C372
		public override int GetHashCode()
		{
			return base.GetType().Name.GetHashCode();
		}

		// Token: 0x06005DAF RID: 23983 RVA: 0x0012EA68 File Offset: 0x0012CC68
		public ByteEqualityComparer()
		{
		}
	}
}
