﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;

namespace System.Collections.Generic
{
	// Token: 0x02000A2F RID: 2607
	[Serializable]
	internal sealed class ShortEnumEqualityComparer<T> : EnumEqualityComparer<T>, ISerializable where T : struct
	{
		// Token: 0x06005DBA RID: 23994 RVA: 0x0012EAE0 File Offset: 0x0012CCE0
		public ShortEnumEqualityComparer()
		{
		}

		// Token: 0x06005DBB RID: 23995 RVA: 0x0012EAE0 File Offset: 0x0012CCE0
		public ShortEnumEqualityComparer(SerializationInfo information, StreamingContext context)
		{
		}

		// Token: 0x06005DBC RID: 23996 RVA: 0x0012EB04 File Offset: 0x0012CD04
		public override int GetHashCode(T obj)
		{
			return ((short)JitHelpers.UnsafeEnumCast<T>(obj)).GetHashCode();
		}
	}
}
