﻿using System;

namespace System.Collections.Generic
{
	// Token: 0x02000A21 RID: 2593
	[Serializable]
	internal class ObjectComparer<T> : Comparer<T>
	{
		// Token: 0x06005D7B RID: 23931 RVA: 0x0012E1DA File Offset: 0x0012C3DA
		public override int Compare(T x, T y)
		{
			return Comparer.Default.Compare(x, y);
		}

		// Token: 0x06005D7C RID: 23932 RVA: 0x0012E1F2 File Offset: 0x0012C3F2
		public override bool Equals(object obj)
		{
			return obj is ObjectComparer<T>;
		}

		// Token: 0x06005D7D RID: 23933 RVA: 0x0012E172 File Offset: 0x0012C372
		public override int GetHashCode()
		{
			return base.GetType().Name.GetHashCode();
		}

		// Token: 0x06005D7E RID: 23934 RVA: 0x0012E184 File Offset: 0x0012C384
		public ObjectComparer()
		{
		}
	}
}
