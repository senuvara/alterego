﻿using System;

namespace System.Collections.Generic
{
	// Token: 0x02000A1C RID: 2588
	[Serializable]
	internal sealed class NonRandomizedStringEqualityComparer : EqualityComparer<string>
	{
		// Token: 0x170010DD RID: 4317
		// (get) Token: 0x06005D65 RID: 23909 RVA: 0x0012DFB0 File Offset: 0x0012C1B0
		internal new static IEqualityComparer<string> Default
		{
			get
			{
				IEqualityComparer<string> result;
				if ((result = NonRandomizedStringEqualityComparer.s_nonRandomizedComparer) == null)
				{
					result = (NonRandomizedStringEqualityComparer.s_nonRandomizedComparer = new NonRandomizedStringEqualityComparer());
				}
				return result;
			}
		}

		// Token: 0x06005D66 RID: 23910 RVA: 0x000765B7 File Offset: 0x000747B7
		public sealed override bool Equals(string x, string y)
		{
			return string.Equals(x, y);
		}

		// Token: 0x06005D67 RID: 23911 RVA: 0x0012DFCA File Offset: 0x0012C1CA
		public sealed override int GetHashCode(string obj)
		{
			if (obj == null)
			{
				return 0;
			}
			return obj.GetLegacyNonRandomizedHashCode();
		}

		// Token: 0x06005D68 RID: 23912 RVA: 0x0012DFD7 File Offset: 0x0012C1D7
		public NonRandomizedStringEqualityComparer()
		{
		}

		// Token: 0x04002EA6 RID: 11942
		private static volatile IEqualityComparer<string> s_nonRandomizedComparer;
	}
}
