﻿using System;
using System.Diagnostics;

namespace System.Collections.Generic
{
	// Token: 0x02000A19 RID: 2585
	internal sealed class IDictionaryDebugView<K, V>
	{
		// Token: 0x06005D5F RID: 23903 RVA: 0x0012DECE File Offset: 0x0012C0CE
		public IDictionaryDebugView(IDictionary<K, V> dictionary)
		{
			if (dictionary == null)
			{
				throw new ArgumentNullException("dictionary");
			}
			this._dict = dictionary;
		}

		// Token: 0x170010DA RID: 4314
		// (get) Token: 0x06005D60 RID: 23904 RVA: 0x0012DEEC File Offset: 0x0012C0EC
		[DebuggerBrowsable(DebuggerBrowsableState.RootHidden)]
		public KeyValuePair<K, V>[] Items
		{
			get
			{
				KeyValuePair<K, V>[] array = new KeyValuePair<K, V>[this._dict.Count];
				this._dict.CopyTo(array, 0);
				return array;
			}
		}

		// Token: 0x04002EA3 RID: 11939
		private readonly IDictionary<K, V> _dict;
	}
}
