﻿using System;

namespace System.Collections.Generic
{
	// Token: 0x02000A1F RID: 2591
	[Serializable]
	internal class GenericComparer<T> : Comparer<T> where T : IComparable<T>
	{
		// Token: 0x06005D73 RID: 23923 RVA: 0x0012E139 File Offset: 0x0012C339
		public override int Compare(T x, T y)
		{
			if (x != null)
			{
				if (y != null)
				{
					return x.CompareTo(y);
				}
				return 1;
			}
			else
			{
				if (y != null)
				{
					return -1;
				}
				return 0;
			}
		}

		// Token: 0x06005D74 RID: 23924 RVA: 0x0012E167 File Offset: 0x0012C367
		public override bool Equals(object obj)
		{
			return obj is GenericComparer<T>;
		}

		// Token: 0x06005D75 RID: 23925 RVA: 0x0012E172 File Offset: 0x0012C372
		public override int GetHashCode()
		{
			return base.GetType().Name.GetHashCode();
		}

		// Token: 0x06005D76 RID: 23926 RVA: 0x0012E184 File Offset: 0x0012C384
		public GenericComparer()
		{
		}
	}
}
