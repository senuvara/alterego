﻿using System;

namespace System.Collections.Generic
{
	// Token: 0x02000A29 RID: 2601
	[Serializable]
	internal class GenericEqualityComparer<T> : EqualityComparer<T> where T : IEquatable<T>
	{
		// Token: 0x06005D94 RID: 23956 RVA: 0x0012E5E8 File Offset: 0x0012C7E8
		public override bool Equals(T x, T y)
		{
			if (x != null)
			{
				return y != null && x.Equals(y);
			}
			return y == null;
		}

		// Token: 0x06005D95 RID: 23957 RVA: 0x0012E616 File Offset: 0x0012C816
		public override int GetHashCode(T obj)
		{
			if (obj == null)
			{
				return 0;
			}
			return obj.GetHashCode();
		}

		// Token: 0x06005D96 RID: 23958 RVA: 0x0012E630 File Offset: 0x0012C830
		internal override int IndexOf(T[] array, T value, int startIndex, int count)
		{
			int num = startIndex + count;
			if (value == null)
			{
				for (int i = startIndex; i < num; i++)
				{
					if (array[i] == null)
					{
						return i;
					}
				}
			}
			else
			{
				for (int j = startIndex; j < num; j++)
				{
					if (array[j] != null && array[j].Equals(value))
					{
						return j;
					}
				}
			}
			return -1;
		}

		// Token: 0x06005D97 RID: 23959 RVA: 0x0012E69C File Offset: 0x0012C89C
		internal override int LastIndexOf(T[] array, T value, int startIndex, int count)
		{
			int num = startIndex - count + 1;
			if (value == null)
			{
				for (int i = startIndex; i >= num; i--)
				{
					if (array[i] == null)
					{
						return i;
					}
				}
			}
			else
			{
				for (int j = startIndex; j >= num; j--)
				{
					if (array[j] != null && array[j].Equals(value))
					{
						return j;
					}
				}
			}
			return -1;
		}

		// Token: 0x06005D98 RID: 23960 RVA: 0x0012E70A File Offset: 0x0012C90A
		public override bool Equals(object obj)
		{
			return obj is GenericEqualityComparer<T>;
		}

		// Token: 0x06005D99 RID: 23961 RVA: 0x0012E172 File Offset: 0x0012C372
		public override int GetHashCode()
		{
			return base.GetType().Name.GetHashCode();
		}

		// Token: 0x06005D9A RID: 23962 RVA: 0x0012E715 File Offset: 0x0012C915
		public GenericEqualityComparer()
		{
		}
	}
}
