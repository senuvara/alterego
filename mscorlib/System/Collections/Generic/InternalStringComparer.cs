﻿using System;

namespace System.Collections.Generic
{
	// Token: 0x02000A31 RID: 2609
	[Serializable]
	internal sealed class InternalStringComparer : EqualityComparer<string>
	{
		// Token: 0x06005DC4 RID: 24004 RVA: 0x0012DFDF File Offset: 0x0012C1DF
		public override int GetHashCode(string obj)
		{
			if (obj == null)
			{
				return 0;
			}
			return obj.GetHashCode();
		}

		// Token: 0x06005DC5 RID: 24005 RVA: 0x0012EB78 File Offset: 0x0012CD78
		public override bool Equals(string x, string y)
		{
			if (x == null)
			{
				return y == null;
			}
			return x == y || x.Equals(y);
		}

		// Token: 0x06005DC6 RID: 24006 RVA: 0x0012EB90 File Offset: 0x0012CD90
		internal override int IndexOf(string[] array, string value, int startIndex, int count)
		{
			int num = startIndex + count;
			for (int i = startIndex; i < num; i++)
			{
				if (Array.UnsafeLoad<string>(array, i) == value)
				{
					return i;
				}
			}
			return -1;
		}

		// Token: 0x06005DC7 RID: 24007 RVA: 0x0012DFD7 File Offset: 0x0012C1D7
		public InternalStringComparer()
		{
		}
	}
}
