﻿using System;

namespace System.Collections
{
	// Token: 0x020009A6 RID: 2470
	[Serializable]
	internal sealed class EmptyReadOnlyDictionaryInternal : IDictionary, ICollection, IEnumerable
	{
		// Token: 0x06005952 RID: 22866 RVA: 0x00002050 File Offset: 0x00000250
		public EmptyReadOnlyDictionaryInternal()
		{
		}

		// Token: 0x06005953 RID: 22867 RVA: 0x00121745 File Offset: 0x0011F945
		IEnumerator IEnumerable.GetEnumerator()
		{
			return new EmptyReadOnlyDictionaryInternal.NodeEnumerator();
		}

		// Token: 0x06005954 RID: 22868 RVA: 0x0012174C File Offset: 0x0011F94C
		public void CopyTo(Array array, int index)
		{
			if (array == null)
			{
				throw new ArgumentNullException("array");
			}
			if (array.Rank != 1)
			{
				throw new ArgumentException(Environment.GetResourceString("Only single dimensional arrays are supported for the requested action."));
			}
			if (index < 0)
			{
				throw new ArgumentOutOfRangeException("index", Environment.GetResourceString("Non-negative number required."));
			}
			if (array.Length - index < this.Count)
			{
				throw new ArgumentException(Environment.GetResourceString("Index was out of range. Must be non-negative and less than the size of the collection."), "index");
			}
		}

		// Token: 0x17000FB9 RID: 4025
		// (get) Token: 0x06005955 RID: 22869 RVA: 0x00002526 File Offset: 0x00000726
		public int Count
		{
			get
			{
				return 0;
			}
		}

		// Token: 0x17000FBA RID: 4026
		// (get) Token: 0x06005956 RID: 22870 RVA: 0x00002058 File Offset: 0x00000258
		public object SyncRoot
		{
			get
			{
				return this;
			}
		}

		// Token: 0x17000FBB RID: 4027
		// (get) Token: 0x06005957 RID: 22871 RVA: 0x00002526 File Offset: 0x00000726
		public bool IsSynchronized
		{
			get
			{
				return false;
			}
		}

		// Token: 0x17000FBC RID: 4028
		public object this[object key]
		{
			get
			{
				if (key == null)
				{
					throw new ArgumentNullException("key", Environment.GetResourceString("Key cannot be null."));
				}
				return null;
			}
			set
			{
				if (key == null)
				{
					throw new ArgumentNullException("key", Environment.GetResourceString("Key cannot be null."));
				}
				if (!key.GetType().IsSerializable)
				{
					throw new ArgumentException(Environment.GetResourceString("Argument passed in is not serializable."), "key");
				}
				if (value != null && !value.GetType().IsSerializable)
				{
					throw new ArgumentException(Environment.GetResourceString("Argument passed in is not serializable."), "value");
				}
				throw new InvalidOperationException(Environment.GetResourceString("Instance is read-only."));
			}
		}

		// Token: 0x17000FBD RID: 4029
		// (get) Token: 0x0600595A RID: 22874 RVA: 0x00121857 File Offset: 0x0011FA57
		public ICollection Keys
		{
			get
			{
				return EmptyArray<object>.Value;
			}
		}

		// Token: 0x17000FBE RID: 4030
		// (get) Token: 0x0600595B RID: 22875 RVA: 0x00121857 File Offset: 0x0011FA57
		public ICollection Values
		{
			get
			{
				return EmptyArray<object>.Value;
			}
		}

		// Token: 0x0600595C RID: 22876 RVA: 0x00002526 File Offset: 0x00000726
		public bool Contains(object key)
		{
			return false;
		}

		// Token: 0x0600595D RID: 22877 RVA: 0x00121860 File Offset: 0x0011FA60
		public void Add(object key, object value)
		{
			if (key == null)
			{
				throw new ArgumentNullException("key", Environment.GetResourceString("Key cannot be null."));
			}
			if (!key.GetType().IsSerializable)
			{
				throw new ArgumentException(Environment.GetResourceString("Argument passed in is not serializable."), "key");
			}
			if (value != null && !value.GetType().IsSerializable)
			{
				throw new ArgumentException(Environment.GetResourceString("Argument passed in is not serializable."), "value");
			}
			throw new InvalidOperationException(Environment.GetResourceString("Instance is read-only."));
		}

		// Token: 0x0600595E RID: 22878 RVA: 0x001218DB File Offset: 0x0011FADB
		public void Clear()
		{
			throw new InvalidOperationException(Environment.GetResourceString("Instance is read-only."));
		}

		// Token: 0x17000FBF RID: 4031
		// (get) Token: 0x0600595F RID: 22879 RVA: 0x00004E08 File Offset: 0x00003008
		public bool IsReadOnly
		{
			get
			{
				return true;
			}
		}

		// Token: 0x17000FC0 RID: 4032
		// (get) Token: 0x06005960 RID: 22880 RVA: 0x00004E08 File Offset: 0x00003008
		public bool IsFixedSize
		{
			get
			{
				return true;
			}
		}

		// Token: 0x06005961 RID: 22881 RVA: 0x00121745 File Offset: 0x0011F945
		public IDictionaryEnumerator GetEnumerator()
		{
			return new EmptyReadOnlyDictionaryInternal.NodeEnumerator();
		}

		// Token: 0x06005962 RID: 22882 RVA: 0x001218DB File Offset: 0x0011FADB
		public void Remove(object key)
		{
			throw new InvalidOperationException(Environment.GetResourceString("Instance is read-only."));
		}

		// Token: 0x020009A7 RID: 2471
		private sealed class NodeEnumerator : IDictionaryEnumerator, IEnumerator
		{
			// Token: 0x06005963 RID: 22883 RVA: 0x00002050 File Offset: 0x00000250
			public NodeEnumerator()
			{
			}

			// Token: 0x06005964 RID: 22884 RVA: 0x00002526 File Offset: 0x00000726
			public bool MoveNext()
			{
				return false;
			}

			// Token: 0x17000FC1 RID: 4033
			// (get) Token: 0x06005965 RID: 22885 RVA: 0x001218EC File Offset: 0x0011FAEC
			public object Current
			{
				get
				{
					throw new InvalidOperationException(Environment.GetResourceString("Enumeration has either not started or has already finished."));
				}
			}

			// Token: 0x06005966 RID: 22886 RVA: 0x000020D3 File Offset: 0x000002D3
			public void Reset()
			{
			}

			// Token: 0x17000FC2 RID: 4034
			// (get) Token: 0x06005967 RID: 22887 RVA: 0x001218EC File Offset: 0x0011FAEC
			public object Key
			{
				get
				{
					throw new InvalidOperationException(Environment.GetResourceString("Enumeration has either not started or has already finished."));
				}
			}

			// Token: 0x17000FC3 RID: 4035
			// (get) Token: 0x06005968 RID: 22888 RVA: 0x001218EC File Offset: 0x0011FAEC
			public object Value
			{
				get
				{
					throw new InvalidOperationException(Environment.GetResourceString("Enumeration has either not started or has already finished."));
				}
			}

			// Token: 0x17000FC4 RID: 4036
			// (get) Token: 0x06005969 RID: 22889 RVA: 0x001218EC File Offset: 0x0011FAEC
			public DictionaryEntry Entry
			{
				get
				{
					throw new InvalidOperationException(Environment.GetResourceString("Enumeration has either not started or has already finished."));
				}
			}
		}
	}
}
