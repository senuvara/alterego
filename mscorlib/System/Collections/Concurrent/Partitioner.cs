﻿using System;
using System.Collections.Generic;

namespace System.Collections.Concurrent
{
	/// <summary>Represents a particular manner of splitting a data source into multiple partitions.</summary>
	/// <typeparam name="TSource">Type of the elements in the collection.</typeparam>
	// Token: 0x020009EF RID: 2543
	public abstract class Partitioner<TSource>
	{
		/// <summary>Partitions the underlying collection into the given number of partitions.</summary>
		/// <param name="partitionCount">The number of partitions to create.</param>
		/// <returns>A list containing <paramref name="partitionCount" /> enumerators.</returns>
		// Token: 0x06005C5A RID: 23642
		public abstract IList<IEnumerator<TSource>> GetPartitions(int partitionCount);

		/// <summary>Gets whether additional partitions can be created dynamically.</summary>
		/// <returns>true if the <see cref="T:System.Collections.Concurrent.Partitioner`1" /> can create partitions dynamically as they are requested; false if the <see cref="T:System.Collections.Concurrent.Partitioner`1" /> can only allocate partitions statically.</returns>
		// Token: 0x1700109B RID: 4251
		// (get) Token: 0x06005C5B RID: 23643 RVA: 0x00002526 File Offset: 0x00000726
		public virtual bool SupportsDynamicPartitions
		{
			get
			{
				return false;
			}
		}

		/// <summary>Creates an object that can partition the underlying collection into a variable number of partitions.</summary>
		/// <returns>An object that can create partitions over the underlying data source.</returns>
		/// <exception cref="T:System.NotSupportedException">Dynamic partitioning is not supported by the base class. You must implement it in a derived class.</exception>
		// Token: 0x06005C5C RID: 23644 RVA: 0x0012A701 File Offset: 0x00128901
		public virtual IEnumerable<TSource> GetDynamicPartitions()
		{
			throw new NotSupportedException("Dynamic partitions are not supported by this partitioner.");
		}

		/// <summary>Creates a new partitioner instance.</summary>
		// Token: 0x06005C5D RID: 23645 RVA: 0x00002050 File Offset: 0x00000250
		protected Partitioner()
		{
		}
	}
}
