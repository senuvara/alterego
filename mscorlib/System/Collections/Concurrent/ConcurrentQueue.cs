﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Threading;

namespace System.Collections.Concurrent
{
	/// <summary>Represents a thread-safe first in-first out (FIFO) collection.</summary>
	/// <typeparam name="T">The type of the elements contained in the queue.</typeparam>
	// Token: 0x020009E2 RID: 2530
	[DebuggerDisplay("Count = {Count}")]
	[DebuggerTypeProxy(typeof(IProducerConsumerCollectionDebugView<>))]
	[Serializable]
	public class ConcurrentQueue<T> : IProducerConsumerCollection<T>, IEnumerable<!0>, IEnumerable, ICollection, IReadOnlyCollection<T>
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Collections.Concurrent.ConcurrentQueue`1" /> class.</summary>
		// Token: 0x06005BF2 RID: 23538 RVA: 0x001291E8 File Offset: 0x001273E8
		public ConcurrentQueue()
		{
			this._crossSegmentLock = new object();
			this._tail = (this._head = new ConcurrentQueue<T>.Segment(32));
		}

		// Token: 0x06005BF3 RID: 23539 RVA: 0x00129220 File Offset: 0x00127420
		private void InitializeFromCollection(IEnumerable<T> collection)
		{
			this._crossSegmentLock = new object();
			int num = 32;
			ICollection<T> collection2 = collection as ICollection<!0>;
			if (collection2 != null)
			{
				int count = collection2.Count;
				if (count > num)
				{
					num = Math.Min(ConcurrentQueue<T>.RoundUpToPowerOf2(count), 1048576);
				}
			}
			this._tail = (this._head = new ConcurrentQueue<T>.Segment(num));
			foreach (T item in collection)
			{
				this.Enqueue(item);
			}
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Collections.Concurrent.ConcurrentQueue`1" /> class that contains elements copied from the specified collection</summary>
		/// <param name="collection">The collection whose elements are copied to the new <see cref="T:System.Collections.Concurrent.ConcurrentQueue`1" />.</param>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="collection" /> argument is null.</exception>
		// Token: 0x06005BF4 RID: 23540 RVA: 0x001292BC File Offset: 0x001274BC
		public ConcurrentQueue(IEnumerable<T> collection)
		{
			if (collection == null)
			{
				throw new ArgumentNullException("collection");
			}
			this.InitializeFromCollection(collection);
		}

		/// <summary>Copies the elements of the <see cref="T:System.Collections.ICollection" /> to an <see cref="T:System.Array" />, starting at a particular <see cref="T:System.Array" /> index.</summary>
		/// <param name="array">The one-dimensional <see cref="T:System.Array" /> that is the destination of the elements copied from the <see cref="T:System.Collections.Concurrent.ConcurrentBag`1" />. The <see cref="T:System.Array" /> must have zero-based indexing.</param>
		/// <param name="index">The zero-based index in <paramref name="array" /> at which copying begins.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="array" /> is a null reference (Nothing in Visual Basic).</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="index" /> is less than zero.</exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="array" /> is multidimensional. -or- <paramref name="array" /> does not have zero-based indexing. -or- <paramref name="index" /> is equal to or greater than the length of the <paramref name="array" /> -or- The number of elements in the source <see cref="T:System.Collections.ICollection" /> is greater than the available space from <paramref name="index" /> to the end of the destination <paramref name="array" />. -or- The type of the source <see cref="T:System.Collections.ICollection" /> cannot be cast automatically to the type of the destination <paramref name="array" />.</exception>
		// Token: 0x06005BF5 RID: 23541 RVA: 0x001292DC File Offset: 0x001274DC
		void ICollection.CopyTo(Array array, int index)
		{
			T[] array2 = array as T[];
			if (array2 != null)
			{
				this.CopyTo(array2, index);
				return;
			}
			if (array == null)
			{
				throw new ArgumentNullException("array");
			}
			this.ToArray().CopyTo(array, index);
		}

		/// <summary>Gets a value indicating whether access to the <see cref="T:System.Collections.ICollection" /> is synchronized with the SyncRoot.</summary>
		/// <returns>true if access to the <see cref="T:System.Collections.ICollection" /> is synchronized with the SyncRoot; otherwise, false. For <see cref="T:System.Collections.Concurrent.ConcurrentQueue`1" />, this property always returns false.</returns>
		// Token: 0x17001087 RID: 4231
		// (get) Token: 0x06005BF6 RID: 23542 RVA: 0x00002526 File Offset: 0x00000726
		bool ICollection.IsSynchronized
		{
			get
			{
				return false;
			}
		}

		/// <summary>Gets an object that can be used to synchronize access to the <see cref="T:System.Collections.ICollection" />. This property is not supported.</summary>
		/// <returns>Returns null  (Nothing in Visual Basic).</returns>
		/// <exception cref="T:System.NotSupportedException">The SyncRoot property is not supported.</exception>
		// Token: 0x17001088 RID: 4232
		// (get) Token: 0x06005BF7 RID: 23543 RVA: 0x00128AF8 File Offset: 0x00126CF8
		object ICollection.SyncRoot
		{
			get
			{
				throw new NotSupportedException("The SyncRoot property may not be used for the synchronization of concurrent collections.");
			}
		}

		/// <summary>Returns an enumerator that iterates through a collection.</summary>
		/// <returns>An <see cref="T:System.Collections.IEnumerator" /> that can be used to iterate through the collection.</returns>
		// Token: 0x06005BF8 RID: 23544 RVA: 0x00129317 File Offset: 0x00127517
		IEnumerator IEnumerable.GetEnumerator()
		{
			return ((IEnumerable<!0>)this).GetEnumerator();
		}

		// Token: 0x06005BF9 RID: 23545 RVA: 0x0012931F File Offset: 0x0012751F
		bool IProducerConsumerCollection<!0>.TryAdd(T item)
		{
			this.Enqueue(item);
			return true;
		}

		// Token: 0x06005BFA RID: 23546 RVA: 0x000C3F49 File Offset: 0x000C2149
		bool IProducerConsumerCollection<!0>.TryTake(out T item)
		{
			return this.TryDequeue(out item);
		}

		/// <summary>Gets a value that indicates whether the <see cref="T:System.Collections.Concurrent.ConcurrentQueue`1" /> is empty.</summary>
		/// <returns>true if the <see cref="T:System.Collections.Concurrent.ConcurrentQueue`1" /> is empty; otherwise, false.</returns>
		// Token: 0x17001089 RID: 4233
		// (get) Token: 0x06005BFB RID: 23547 RVA: 0x0012932C File Offset: 0x0012752C
		public bool IsEmpty
		{
			get
			{
				T t;
				return !this.TryPeek(out t, false);
			}
		}

		/// <summary>Copies the elements stored in the <see cref="T:System.Collections.Concurrent.ConcurrentQueue`1" /> to a new array.</summary>
		/// <returns>A new array containing a snapshot of elements copied from the <see cref="T:System.Collections.Concurrent.ConcurrentQueue`1" />.</returns>
		// Token: 0x06005BFC RID: 23548 RVA: 0x00129348 File Offset: 0x00127548
		public T[] ToArray()
		{
			ConcurrentQueue<T>.Segment head;
			int headHead;
			ConcurrentQueue<T>.Segment tail;
			int tailTail;
			this.SnapForObservation(out head, out headHead, out tail, out tailTail);
			T[] array = new T[ConcurrentQueue<T>.GetCount(head, headHead, tail, tailTail)];
			using (IEnumerator<T> enumerator = this.Enumerate(head, headHead, tail, tailTail))
			{
				int num = 0;
				while (enumerator.MoveNext())
				{
					!0 ! = enumerator.Current;
					array[num++] = !;
				}
			}
			return array;
		}

		/// <summary>Gets the number of elements contained in the <see cref="T:System.Collections.Concurrent.ConcurrentQueue`1" />.</summary>
		/// <returns>The number of elements contained in the <see cref="T:System.Collections.Concurrent.ConcurrentQueue`1" />.</returns>
		// Token: 0x1700108A RID: 4234
		// (get) Token: 0x06005BFD RID: 23549 RVA: 0x001293C4 File Offset: 0x001275C4
		public int Count
		{
			get
			{
				SpinWait spinWait = default(SpinWait);
				ConcurrentQueue<T>.Segment head;
				ConcurrentQueue<T>.Segment tail;
				int num;
				int num2;
				int num3;
				int num4;
				for (;;)
				{
					head = this._head;
					tail = this._tail;
					num = Volatile.Read(ref head._headAndTail.Head);
					num2 = Volatile.Read(ref head._headAndTail.Tail);
					if (head == tail)
					{
						if (head == this._head && head == this._tail && num == Volatile.Read(ref head._headAndTail.Head) && num2 == Volatile.Read(ref head._headAndTail.Tail))
						{
							break;
						}
					}
					else
					{
						if (head._nextSegment != tail)
						{
							goto IL_13C;
						}
						num3 = Volatile.Read(ref tail._headAndTail.Head);
						num4 = Volatile.Read(ref tail._headAndTail.Tail);
						if (head == this._head && tail == this._tail && num == Volatile.Read(ref head._headAndTail.Head) && num2 == Volatile.Read(ref head._headAndTail.Tail) && num3 == Volatile.Read(ref tail._headAndTail.Head) && num4 == Volatile.Read(ref tail._headAndTail.Tail))
						{
							goto Block_12;
						}
					}
					spinWait.SpinOnce();
				}
				return ConcurrentQueue<T>.GetCount(head, num, num2);
				Block_12:
				return ConcurrentQueue<T>.GetCount(head, num, num2) + ConcurrentQueue<T>.GetCount(tail, num3, num4);
				IL_13C:
				this.SnapForObservation(out head, out num, out tail, out num4);
				return (int)ConcurrentQueue<T>.GetCount(head, num, tail, num4);
			}
		}

		// Token: 0x06005BFE RID: 23550 RVA: 0x00129532 File Offset: 0x00127732
		private static int GetCount(ConcurrentQueue<T>.Segment s, int head, int tail)
		{
			if (head == tail || head == tail - s.FreezeOffset)
			{
				return 0;
			}
			head &= s._slotsMask;
			tail &= s._slotsMask;
			if (head >= tail)
			{
				return s._slots.Length - head + tail;
			}
			return tail - head;
		}

		// Token: 0x06005BFF RID: 23551 RVA: 0x00129570 File Offset: 0x00127770
		private static long GetCount(ConcurrentQueue<T>.Segment head, int headHead, ConcurrentQueue<T>.Segment tail, int tailTail)
		{
			long num = 0L;
			int num2 = ((head == tail) ? tailTail : Volatile.Read(ref head._headAndTail.Tail)) - head.FreezeOffset;
			if (headHead < num2)
			{
				headHead &= head._slotsMask;
				num2 &= head._slotsMask;
				num += (long)((headHead < num2) ? (num2 - headHead) : (head._slots.Length - headHead + num2));
			}
			if (head != tail)
			{
				for (ConcurrentQueue<T>.Segment nextSegment = head._nextSegment; nextSegment != tail; nextSegment = nextSegment._nextSegment)
				{
					num += (long)(nextSegment._headAndTail.Tail - nextSegment.FreezeOffset);
				}
				num += (long)(tailTail - tail.FreezeOffset);
			}
			return num;
		}

		/// <summary>Copies the <see cref="T:System.Collections.Concurrent.ConcurrentQueue`1" /> elements to an existing one-dimensional <see cref="T:System.Array" />, starting at the specified array index.</summary>
		/// <param name="array">The one-dimensional <see cref="T:System.Array" /> that is the destination of the elements copied from the <see cref="T:System.Collections.Concurrent.ConcurrentQueue`1" />. The <see cref="T:System.Array" /> must have zero-based indexing.</param>
		/// <param name="index">The zero-based index in <paramref name="array" /> at which copying begins.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="array" /> is a null reference (Nothing in Visual Basic).</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="index" /> is less than zero.</exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="index" /> is equal to or greater than the length of the <paramref name="array" /> -or- The number of elements in the source <see cref="T:System.Collections.Concurrent.ConcurrentQueue`1" /> is greater than the available space from <paramref name="index" /> to the end of the destination <paramref name="array" />.</exception>
		// Token: 0x06005C00 RID: 23552 RVA: 0x0012960C File Offset: 0x0012780C
		public void CopyTo(T[] array, int index)
		{
			if (array == null)
			{
				throw new ArgumentNullException("array");
			}
			if (index < 0)
			{
				throw new ArgumentOutOfRangeException("index", "The index argument must be greater than or equal zero.");
			}
			ConcurrentQueue<T>.Segment head;
			int headHead;
			ConcurrentQueue<T>.Segment tail;
			int tailTail;
			this.SnapForObservation(out head, out headHead, out tail, out tailTail);
			long count = ConcurrentQueue<T>.GetCount(head, headHead, tail, tailTail);
			if ((long)index > (long)array.Length - count)
			{
				throw new ArgumentException("The number of elements in the collection is greater than the available space from index to the end of the destination array.");
			}
			int num = index;
			using (IEnumerator<T> enumerator = this.Enumerate(head, headHead, tail, tailTail))
			{
				while (enumerator.MoveNext())
				{
					!0 ! = enumerator.Current;
					array[num++] = !;
				}
			}
		}

		/// <summary>Returns an enumerator that iterates through the <see cref="T:System.Collections.Concurrent.ConcurrentQueue`1" />.</summary>
		/// <returns>An enumerator for the contents of the <see cref="T:System.Collections.Concurrent.ConcurrentQueue`1" />.</returns>
		// Token: 0x06005C01 RID: 23553 RVA: 0x001296B8 File Offset: 0x001278B8
		public IEnumerator<T> GetEnumerator()
		{
			ConcurrentQueue<T>.Segment head;
			int headHead;
			ConcurrentQueue<T>.Segment tail;
			int tailTail;
			this.SnapForObservation(out head, out headHead, out tail, out tailTail);
			return this.Enumerate(head, headHead, tail, tailTail);
		}

		// Token: 0x06005C02 RID: 23554 RVA: 0x001296E0 File Offset: 0x001278E0
		private void SnapForObservation(out ConcurrentQueue<T>.Segment head, out int headHead, out ConcurrentQueue<T>.Segment tail, out int tailTail)
		{
			object crossSegmentLock = this._crossSegmentLock;
			lock (crossSegmentLock)
			{
				head = this._head;
				tail = this._tail;
				ConcurrentQueue<T>.Segment segment = head;
				for (;;)
				{
					segment._preservedForObservation = true;
					if (segment == tail)
					{
						break;
					}
					segment = segment._nextSegment;
				}
				tail.EnsureFrozenForEnqueues();
				headHead = Volatile.Read(ref head._headAndTail.Head);
				tailTail = Volatile.Read(ref tail._headAndTail.Tail);
			}
		}

		// Token: 0x06005C03 RID: 23555 RVA: 0x00129774 File Offset: 0x00127974
		private T GetItemWhenAvailable(ConcurrentQueue<T>.Segment segment, int i)
		{
			int num = i + 1 & segment._slotsMask;
			if ((segment._slots[i].SequenceNumber & segment._slotsMask) != num)
			{
				SpinWait spinWait = default(SpinWait);
				while ((Volatile.Read(ref segment._slots[i].SequenceNumber) & segment._slotsMask) != num)
				{
					spinWait.SpinOnce();
				}
			}
			return segment._slots[i].Item;
		}

		// Token: 0x06005C04 RID: 23556 RVA: 0x001297E9 File Offset: 0x001279E9
		private IEnumerator<T> Enumerate(ConcurrentQueue<T>.Segment head, int headHead, ConcurrentQueue<T>.Segment tail, int tailTail)
		{
			int headTail = ((head == tail) ? tailTail : Volatile.Read(ref head._headAndTail.Tail)) - head.FreezeOffset;
			if (headHead < headTail)
			{
				headHead &= head._slotsMask;
				headTail &= head._slotsMask;
				if (headHead < headTail)
				{
					int num;
					for (int i = headHead; i < headTail; i = num + 1)
					{
						yield return this.GetItemWhenAvailable(head, i);
						num = i;
					}
				}
				else
				{
					int num;
					for (int j = headHead; j < head._slots.Length; j = num + 1)
					{
						yield return this.GetItemWhenAvailable(head, j);
						num = j;
					}
					for (int k = 0; k < headTail; k = num + 1)
					{
						yield return this.GetItemWhenAvailable(head, k);
						num = k;
					}
				}
			}
			if (head != tail)
			{
				int num;
				ConcurrentQueue<T>.Segment s;
				for (s = head._nextSegment; s != tail; s = s._nextSegment)
				{
					int sTail = s._headAndTail.Tail - s.FreezeOffset;
					for (int l = 0; l < sTail; l = num + 1)
					{
						yield return this.GetItemWhenAvailable(s, l);
						num = l;
					}
				}
				s = null;
				tailTail -= tail.FreezeOffset;
				for (int m = 0; m < tailTail; m = num + 1)
				{
					yield return this.GetItemWhenAvailable(tail, m);
					num = m;
				}
			}
			yield break;
		}

		// Token: 0x06005C05 RID: 23557 RVA: 0x00129815 File Offset: 0x00127A15
		private static int RoundUpToPowerOf2(int i)
		{
			i--;
			i |= i >> 1;
			i |= i >> 2;
			i |= i >> 4;
			i |= i >> 8;
			i |= i >> 16;
			return i + 1;
		}

		/// <summary>Adds an object to the end of the <see cref="T:System.Collections.Concurrent.ConcurrentQueue`1" />.</summary>
		/// <param name="item">The object to add to the end of the <see cref="T:System.Collections.Concurrent.ConcurrentQueue`1" />. The value can be a null reference (Nothing in Visual Basic) for reference types.</param>
		// Token: 0x06005C06 RID: 23558 RVA: 0x00129843 File Offset: 0x00127A43
		public void Enqueue(T item)
		{
			if (!this._tail.TryEnqueue(item))
			{
				this.EnqueueSlow(item);
			}
		}

		// Token: 0x06005C07 RID: 23559 RVA: 0x0012985C File Offset: 0x00127A5C
		private void EnqueueSlow(T item)
		{
			for (;;)
			{
				ConcurrentQueue<T>.Segment tail = this._tail;
				if (tail.TryEnqueue(item))
				{
					break;
				}
				object crossSegmentLock = this._crossSegmentLock;
				lock (crossSegmentLock)
				{
					if (tail == this._tail)
					{
						tail.EnsureFrozenForEnqueues();
						ConcurrentQueue<T>.Segment segment = new ConcurrentQueue<T>.Segment(tail._preservedForObservation ? 32 : Math.Min(tail.Capacity * 2, 1048576));
						tail._nextSegment = segment;
						this._tail = segment;
					}
				}
			}
		}

		/// <summary>Tries to remove and return the object at the beginning of the concurrent queue.</summary>
		/// <param name="result">When this method returns, if the operation was successful, <paramref name="result" /> contains the object removed. If no object was available to be removed, the value is unspecified.</param>
		/// <returns>
		///     <see langword="true" /> if an element was removed and returned from the beginning of the <see cref="T:System.Collections.Concurrent.ConcurrentQueue`1" /> successfully; otherwise, <see langword="false" />.</returns>
		// Token: 0x06005C08 RID: 23560 RVA: 0x001298FC File Offset: 0x00127AFC
		public bool TryDequeue(out T result)
		{
			return this._head.TryDequeue(out result) || this.TryDequeueSlow(out result);
		}

		// Token: 0x06005C09 RID: 23561 RVA: 0x00129918 File Offset: 0x00127B18
		private bool TryDequeueSlow(out T item)
		{
			for (;;)
			{
				ConcurrentQueue<T>.Segment head = this._head;
				if (head.TryDequeue(out item))
				{
					break;
				}
				if (head._nextSegment == null)
				{
					goto Block_1;
				}
				if (head.TryDequeue(out item))
				{
					return true;
				}
				object crossSegmentLock = this._crossSegmentLock;
				lock (crossSegmentLock)
				{
					if (head == this._head)
					{
						this._head = head._nextSegment;
					}
				}
			}
			return true;
			Block_1:
			item = default(T);
			return false;
		}

		/// <summary>Tries to return an object from the beginning of the <see cref="T:System.Collections.Concurrent.ConcurrentQueue`1" /> without removing it.</summary>
		/// <param name="result">When this method returns, <paramref name="result" /> contains an object from the beginning of the <see cref="T:System.Collections.Concurrent.ConcurrentQueue`1" /> or an unspecified value if the operation failed.</param>
		/// <returns>
		///     <see langword="true" /> if an object was returned successfully; otherwise, <see langword="false" />.</returns>
		// Token: 0x06005C0A RID: 23562 RVA: 0x001299A8 File Offset: 0x00127BA8
		public bool TryPeek(out T result)
		{
			return this.TryPeek(out result, true);
		}

		// Token: 0x06005C0B RID: 23563 RVA: 0x001299B4 File Offset: 0x00127BB4
		private bool TryPeek(out T result, bool resultUsed)
		{
			ConcurrentQueue<T>.Segment segment = this._head;
			for (;;)
			{
				ConcurrentQueue<T>.Segment segment2 = Volatile.Read<ConcurrentQueue<T>.Segment>(ref segment._nextSegment);
				if (segment.TryPeek(out result, resultUsed))
				{
					break;
				}
				if (segment2 != null)
				{
					segment = segment2;
				}
				else if (Volatile.Read<ConcurrentQueue<T>.Segment>(ref segment._nextSegment) == null)
				{
					goto Block_3;
				}
			}
			return true;
			Block_3:
			result = default(T);
			return false;
		}

		// Token: 0x06005C0C RID: 23564 RVA: 0x00129A00 File Offset: 0x00127C00
		public void Clear()
		{
			object crossSegmentLock = this._crossSegmentLock;
			lock (crossSegmentLock)
			{
				this._tail.EnsureFrozenForEnqueues();
				this._tail = (this._head = new ConcurrentQueue<T>.Segment(32));
			}
		}

		// Token: 0x04002E05 RID: 11781
		private const int InitialSegmentLength = 32;

		// Token: 0x04002E06 RID: 11782
		private const int MaxSegmentLength = 1048576;

		// Token: 0x04002E07 RID: 11783
		private object _crossSegmentLock;

		// Token: 0x04002E08 RID: 11784
		private volatile ConcurrentQueue<T>.Segment _tail;

		// Token: 0x04002E09 RID: 11785
		private volatile ConcurrentQueue<T>.Segment _head;

		// Token: 0x020009E3 RID: 2531
		[DebuggerDisplay("Capacity = {Capacity}")]
		private sealed class Segment
		{
			// Token: 0x06005C0D RID: 23565 RVA: 0x00129A64 File Offset: 0x00127C64
			public Segment(int boundedLength)
			{
				this._slots = new ConcurrentQueue<T>.Segment.Slot[boundedLength];
				this._slotsMask = boundedLength - 1;
				for (int i = 0; i < this._slots.Length; i++)
				{
					this._slots[i].SequenceNumber = i;
				}
			}

			// Token: 0x1700108B RID: 4235
			// (get) Token: 0x06005C0E RID: 23566 RVA: 0x00129AB1 File Offset: 0x00127CB1
			internal int Capacity
			{
				get
				{
					return this._slots.Length;
				}
			}

			// Token: 0x1700108C RID: 4236
			// (get) Token: 0x06005C0F RID: 23567 RVA: 0x00129ABB File Offset: 0x00127CBB
			internal int FreezeOffset
			{
				get
				{
					return this._slots.Length * 2;
				}
			}

			// Token: 0x06005C10 RID: 23568 RVA: 0x00129AC8 File Offset: 0x00127CC8
			internal void EnsureFrozenForEnqueues()
			{
				if (!this._frozenForEnqueues)
				{
					this._frozenForEnqueues = true;
					SpinWait spinWait = default(SpinWait);
					for (;;)
					{
						int num = Volatile.Read(ref this._headAndTail.Tail);
						if (Interlocked.CompareExchange(ref this._headAndTail.Tail, num + this.FreezeOffset, num) == num)
						{
							break;
						}
						spinWait.SpinOnce();
					}
				}
			}

			// Token: 0x06005C11 RID: 23569 RVA: 0x00129B24 File Offset: 0x00127D24
			public bool TryDequeue(out T item)
			{
				SpinWait spinWait = default(SpinWait);
				int num;
				int num2;
				for (;;)
				{
					num = Volatile.Read(ref this._headAndTail.Head);
					num2 = (num & this._slotsMask);
					int num3 = Volatile.Read(ref this._slots[num2].SequenceNumber) - (num + 1);
					if (num3 == 0)
					{
						if (Interlocked.CompareExchange(ref this._headAndTail.Head, num + 1, num) == num)
						{
							break;
						}
					}
					else if (num3 < 0)
					{
						bool frozenForEnqueues = this._frozenForEnqueues;
						int num4 = Volatile.Read(ref this._headAndTail.Tail);
						if (num4 - num <= 0 || (frozenForEnqueues && num4 - this.FreezeOffset - num <= 0))
						{
							goto IL_EE;
						}
					}
					spinWait.SpinOnce();
				}
				item = this._slots[num2].Item;
				if (!Volatile.Read(ref this._preservedForObservation))
				{
					this._slots[num2].Item = default(T);
					Volatile.Write(ref this._slots[num2].SequenceNumber, num + this._slots.Length);
				}
				return true;
				IL_EE:
				item = default(T);
				return false;
			}

			// Token: 0x06005C12 RID: 23570 RVA: 0x00129C34 File Offset: 0x00127E34
			public bool TryPeek(out T result, bool resultUsed)
			{
				if (resultUsed)
				{
					this._preservedForObservation = true;
					Interlocked.MemoryBarrier();
				}
				SpinWait spinWait = default(SpinWait);
				int num2;
				for (;;)
				{
					int num = Volatile.Read(ref this._headAndTail.Head);
					num2 = (num & this._slotsMask);
					int num3 = Volatile.Read(ref this._slots[num2].SequenceNumber) - (num + 1);
					if (num3 == 0)
					{
						break;
					}
					if (num3 < 0)
					{
						bool frozenForEnqueues = this._frozenForEnqueues;
						int num4 = Volatile.Read(ref this._headAndTail.Tail);
						if (num4 - num <= 0 || (frozenForEnqueues && num4 - this.FreezeOffset - num <= 0))
						{
							goto IL_AE;
						}
					}
					spinWait.SpinOnce();
				}
				result = (resultUsed ? this._slots[num2].Item : default(T));
				return true;
				IL_AE:
				result = default(T);
				return false;
			}

			// Token: 0x06005C13 RID: 23571 RVA: 0x00129D04 File Offset: 0x00127F04
			public bool TryEnqueue(T item)
			{
				SpinWait spinWait = default(SpinWait);
				int num;
				int num2;
				for (;;)
				{
					num = Volatile.Read(ref this._headAndTail.Tail);
					num2 = (num & this._slotsMask);
					int num3 = Volatile.Read(ref this._slots[num2].SequenceNumber) - num;
					if (num3 == 0)
					{
						if (Interlocked.CompareExchange(ref this._headAndTail.Tail, num + 1, num) == num)
						{
							break;
						}
					}
					else if (num3 < 0)
					{
						return false;
					}
					spinWait.SpinOnce();
				}
				this._slots[num2].Item = item;
				Volatile.Write(ref this._slots[num2].SequenceNumber, num + 1);
				return true;
			}

			// Token: 0x04002E0A RID: 11786
			internal readonly ConcurrentQueue<T>.Segment.Slot[] _slots;

			// Token: 0x04002E0B RID: 11787
			internal readonly int _slotsMask;

			// Token: 0x04002E0C RID: 11788
			internal PaddedHeadAndTail _headAndTail;

			// Token: 0x04002E0D RID: 11789
			internal bool _preservedForObservation;

			// Token: 0x04002E0E RID: 11790
			internal bool _frozenForEnqueues;

			// Token: 0x04002E0F RID: 11791
			internal ConcurrentQueue<T>.Segment _nextSegment;

			// Token: 0x020009E4 RID: 2532
			[DebuggerDisplay("Item = {Item}, SequenceNumber = {SequenceNumber}")]
			[StructLayout(LayoutKind.Auto)]
			internal struct Slot
			{
				// Token: 0x04002E10 RID: 11792
				public T Item;

				// Token: 0x04002E11 RID: 11793
				public int SequenceNumber;
			}
		}

		// Token: 0x020009E5 RID: 2533
		[CompilerGenerated]
		private sealed class <Enumerate>d__27 : IEnumerator<!0>, IDisposable, IEnumerator
		{
			// Token: 0x06005C14 RID: 23572 RVA: 0x00129DA4 File Offset: 0x00127FA4
			[DebuggerHidden]
			public <Enumerate>d__27(int <>1__state)
			{
				this.<>1__state = <>1__state;
			}

			// Token: 0x06005C15 RID: 23573 RVA: 0x000020D3 File Offset: 0x000002D3
			[DebuggerHidden]
			void IDisposable.Dispose()
			{
			}

			// Token: 0x06005C16 RID: 23574 RVA: 0x00129DB4 File Offset: 0x00127FB4
			bool IEnumerator.MoveNext()
			{
				int num = this.<>1__state;
				ConcurrentQueue<T> concurrentQueue = this;
				switch (num)
				{
				case 0:
					this.<>1__state = -1;
					headTail = ((head == tail) ? tailTail : Volatile.Read(ref head._headAndTail.Tail)) - head.FreezeOffset;
					if (headHead >= headTail)
					{
						goto IL_1C4;
					}
					headHead &= head._slotsMask;
					headTail &= head._slotsMask;
					if (headHead >= headTail)
					{
						j = headHead;
						goto IL_160;
					}
					i = headHead;
					break;
				case 1:
				{
					this.<>1__state = -1;
					int num2 = i;
					i = num2 + 1;
					break;
				}
				case 2:
				{
					this.<>1__state = -1;
					int num2 = j;
					j = num2 + 1;
					goto IL_160;
				}
				case 3:
				{
					this.<>1__state = -1;
					int num2 = k;
					k = num2 + 1;
					goto IL_1B6;
				}
				case 4:
				{
					this.<>1__state = -1;
					int num2 = l;
					l = num2 + 1;
					goto IL_24E;
				}
				case 5:
				{
					this.<>1__state = -1;
					int num2 = m;
					m = num2 + 1;
					goto IL_2DE;
				}
				default:
					return false;
				}
				if (i >= headTail)
				{
					goto IL_1C4;
				}
				this.<>2__current = concurrentQueue.GetItemWhenAvailable(head, i);
				this.<>1__state = 1;
				return true;
				IL_160:
				if (j < head._slots.Length)
				{
					this.<>2__current = concurrentQueue.GetItemWhenAvailable(head, j);
					this.<>1__state = 2;
					return true;
				}
				k = 0;
				IL_1B6:
				if (k < headTail)
				{
					this.<>2__current = concurrentQueue.GetItemWhenAvailable(head, k);
					this.<>1__state = 3;
					return true;
				}
				IL_1C4:
				if (head != tail)
				{
					s = head._nextSegment;
					goto IL_26D;
				}
				return false;
				IL_24E:
				if (l < sTail)
				{
					this.<>2__current = concurrentQueue.GetItemWhenAvailable(s, l);
					this.<>1__state = 4;
					return true;
				}
				s = s._nextSegment;
				IL_26D:
				if (s != tail)
				{
					sTail = s._headAndTail.Tail - s.FreezeOffset;
					l = 0;
					goto IL_24E;
				}
				s = null;
				tailTail -= tail.FreezeOffset;
				m = 0;
				IL_2DE:
				if (m < tailTail)
				{
					this.<>2__current = concurrentQueue.GetItemWhenAvailable(tail, m);
					this.<>1__state = 5;
					return true;
				}
				return false;
			}

			// Token: 0x1700108D RID: 4237
			// (get) Token: 0x06005C17 RID: 23575 RVA: 0x0012A0AE File Offset: 0x001282AE
			T IEnumerator<!0>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x06005C18 RID: 23576 RVA: 0x000175EA File Offset: 0x000157EA
			[DebuggerHidden]
			void IEnumerator.Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x1700108E RID: 4238
			// (get) Token: 0x06005C19 RID: 23577 RVA: 0x0012A0B6 File Offset: 0x001282B6
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x04002E12 RID: 11794
			private int <>1__state;

			// Token: 0x04002E13 RID: 11795
			private T <>2__current;

			// Token: 0x04002E14 RID: 11796
			public ConcurrentQueue<T>.Segment head;

			// Token: 0x04002E15 RID: 11797
			public ConcurrentQueue<T>.Segment tail;

			// Token: 0x04002E16 RID: 11798
			public int tailTail;

			// Token: 0x04002E17 RID: 11799
			public int headHead;

			// Token: 0x04002E18 RID: 11800
			public ConcurrentQueue<T> <>4__this;

			// Token: 0x04002E19 RID: 11801
			private int <i>5__1;

			// Token: 0x04002E1A RID: 11802
			private int <headTail>5__2;

			// Token: 0x04002E1B RID: 11803
			private int <i>5__3;

			// Token: 0x04002E1C RID: 11804
			private int <i>5__4;

			// Token: 0x04002E1D RID: 11805
			private ConcurrentQueue<T>.Segment <s>5__5;

			// Token: 0x04002E1E RID: 11806
			private int <i>5__6;

			// Token: 0x04002E1F RID: 11807
			private int <sTail>5__7;

			// Token: 0x04002E20 RID: 11808
			private int <i>5__8;
		}
	}
}
