﻿using System;
using System.Diagnostics.Tracing;

namespace System.Collections.Concurrent
{
	// Token: 0x020009DB RID: 2523
	[EventSource(Name = "System.Collections.Concurrent.ConcurrentCollectionsEventSource", Guid = "35167F8E-49B2-4b96-AB86-435B59336B5E")]
	internal sealed class CDSCollectionETWBCLProvider : EventSource
	{
		// Token: 0x06005B91 RID: 23441 RVA: 0x000B4875 File Offset: 0x000B2A75
		private CDSCollectionETWBCLProvider()
		{
		}

		// Token: 0x06005B92 RID: 23442 RVA: 0x0012795C File Offset: 0x00125B5C
		[Event(1, Level = EventLevel.Warning)]
		public void ConcurrentStack_FastPushFailed(int spinCount)
		{
			if (base.IsEnabled(EventLevel.Warning, EventKeywords.All))
			{
				base.WriteEvent(1, spinCount);
			}
		}

		// Token: 0x06005B93 RID: 23443 RVA: 0x00127971 File Offset: 0x00125B71
		[Event(2, Level = EventLevel.Warning)]
		public void ConcurrentStack_FastPopFailed(int spinCount)
		{
			if (base.IsEnabled(EventLevel.Warning, EventKeywords.All))
			{
				base.WriteEvent(2, spinCount);
			}
		}

		// Token: 0x06005B94 RID: 23444 RVA: 0x00127986 File Offset: 0x00125B86
		[Event(3, Level = EventLevel.Warning)]
		public void ConcurrentDictionary_AcquiringAllLocks(int numOfBuckets)
		{
			if (base.IsEnabled(EventLevel.Warning, EventKeywords.All))
			{
				base.WriteEvent(3, numOfBuckets);
			}
		}

		// Token: 0x06005B95 RID: 23445 RVA: 0x0012799B File Offset: 0x00125B9B
		[Event(4, Level = EventLevel.Verbose)]
		public void ConcurrentBag_TryTakeSteals()
		{
			if (base.IsEnabled(EventLevel.Verbose, EventKeywords.All))
			{
				base.WriteEvent(4);
			}
		}

		// Token: 0x06005B96 RID: 23446 RVA: 0x001279AF File Offset: 0x00125BAF
		[Event(5, Level = EventLevel.Verbose)]
		public void ConcurrentBag_TryPeekSteals()
		{
			if (base.IsEnabled(EventLevel.Verbose, EventKeywords.All))
			{
				base.WriteEvent(5);
			}
		}

		// Token: 0x06005B97 RID: 23447 RVA: 0x001279C3 File Offset: 0x00125BC3
		// Note: this type is marked as 'beforefieldinit'.
		static CDSCollectionETWBCLProvider()
		{
		}

		// Token: 0x04002DE8 RID: 11752
		public static CDSCollectionETWBCLProvider Log = new CDSCollectionETWBCLProvider();

		// Token: 0x04002DE9 RID: 11753
		private const EventKeywords ALL_KEYWORDS = EventKeywords.All;

		// Token: 0x04002DEA RID: 11754
		private const int CONCURRENTSTACK_FASTPUSHFAILED_ID = 1;

		// Token: 0x04002DEB RID: 11755
		private const int CONCURRENTSTACK_FASTPOPFAILED_ID = 2;

		// Token: 0x04002DEC RID: 11756
		private const int CONCURRENTDICTIONARY_ACQUIRINGALLLOCKS_ID = 3;

		// Token: 0x04002DED RID: 11757
		private const int CONCURRENTBAG_TRYTAKESTEALS_ID = 4;

		// Token: 0x04002DEE RID: 11758
		private const int CONCURRENTBAG_TRYPEEKSTEALS_ID = 5;
	}
}
