﻿using System;
using System.Diagnostics;

namespace System.Collections.Concurrent
{
	// Token: 0x020009EB RID: 2539
	internal sealed class IProducerConsumerCollectionDebugView<T>
	{
		// Token: 0x06005C43 RID: 23619 RVA: 0x0012A6BA File Offset: 0x001288BA
		public IProducerConsumerCollectionDebugView(IProducerConsumerCollection<T> collection)
		{
			if (collection == null)
			{
				throw new ArgumentNullException("collection");
			}
			this._collection = collection;
		}

		// Token: 0x17001095 RID: 4245
		// (get) Token: 0x06005C44 RID: 23620 RVA: 0x0012A6D7 File Offset: 0x001288D7
		[DebuggerBrowsable(DebuggerBrowsableState.RootHidden)]
		public T[] Items
		{
			get
			{
				return this._collection.ToArray();
			}
		}

		// Token: 0x04002E2B RID: 11819
		private readonly IProducerConsumerCollection<T> _collection;
	}
}
