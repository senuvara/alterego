﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace System.Collections.Concurrent
{
	// Token: 0x020009E6 RID: 2534
	[DebuggerDisplay("Head = {Head}, Tail = {Tail}")]
	[StructLayout(LayoutKind.Explicit, Size = 384)]
	internal struct PaddedHeadAndTail
	{
		// Token: 0x04002E21 RID: 11809
		[FieldOffset(128)]
		public int Head;

		// Token: 0x04002E22 RID: 11810
		[FieldOffset(256)]
		public int Tail;
	}
}
