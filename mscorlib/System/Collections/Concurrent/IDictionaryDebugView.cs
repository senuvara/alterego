﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace System.Collections.Concurrent
{
	// Token: 0x020009E1 RID: 2529
	internal sealed class IDictionaryDebugView<K, V>
	{
		// Token: 0x06005BF0 RID: 23536 RVA: 0x0012919C File Offset: 0x0012739C
		public IDictionaryDebugView(IDictionary<K, V> dictionary)
		{
			if (dictionary == null)
			{
				throw new ArgumentNullException("dictionary");
			}
			this._dictionary = dictionary;
		}

		// Token: 0x17001086 RID: 4230
		// (get) Token: 0x06005BF1 RID: 23537 RVA: 0x001291BC File Offset: 0x001273BC
		[DebuggerBrowsable(DebuggerBrowsableState.RootHidden)]
		public KeyValuePair<K, V>[] Items
		{
			get
			{
				KeyValuePair<K, V>[] array = new KeyValuePair<K, V>[this._dictionary.Count];
				this._dictionary.CopyTo(array, 0);
				return array;
			}
		}

		// Token: 0x04002E04 RID: 11780
		private readonly IDictionary<K, V> _dictionary;
	}
}
