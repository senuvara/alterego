﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace System.Collections.Concurrent
{
	/// <summary>Represents a particular manner of splitting an orderable data source into multiple partitions.</summary>
	/// <typeparam name="TSource">Type of the elements in the collection.</typeparam>
	// Token: 0x020009EC RID: 2540
	public abstract class OrderablePartitioner<TSource> : Partitioner<TSource>
	{
		/// <summary>Called from constructors in derived classes to initialize the <see cref="T:System.Collections.Concurrent.OrderablePartitioner`1" /> class with the specified constraints on the index keys.</summary>
		/// <param name="keysOrderedInEachPartition">Indicates whether the elements in each partition are yielded in the order of increasing keys.</param>
		/// <param name="keysOrderedAcrossPartitions">Indicates whether elements in an earlier partition always come before elements in a later partition. If true, each element in partition 0 has a smaller order key than any element in partition 1, each element in partition 1 has a smaller order key than any element in partition 2, and so on.</param>
		/// <param name="keysNormalized">Indicates whether keys are normalized. If true, all order keys are distinct integers in the range [0 .. numberOfElements-1]. If false, order keys must still be distinct, but only their relative order is considered, not their absolute values.</param>
		// Token: 0x06005C45 RID: 23621 RVA: 0x0012A6E4 File Offset: 0x001288E4
		protected OrderablePartitioner(bool keysOrderedInEachPartition, bool keysOrderedAcrossPartitions, bool keysNormalized)
		{
			this.KeysOrderedInEachPartition = keysOrderedInEachPartition;
			this.KeysOrderedAcrossPartitions = keysOrderedAcrossPartitions;
			this.KeysNormalized = keysNormalized;
		}

		/// <summary>Partitions the underlying collection into the specified number of orderable partitions.</summary>
		/// <param name="partitionCount">The number of partitions to create.</param>
		/// <returns>A list containing <paramref name="partitionCount" /> enumerators.</returns>
		// Token: 0x06005C46 RID: 23622
		public abstract IList<IEnumerator<KeyValuePair<long, TSource>>> GetOrderablePartitions(int partitionCount);

		/// <summary>Creates an object that can partition the underlying collection into a variable number of partitions.</summary>
		/// <returns>An object that can create partitions over the underlying data source.</returns>
		/// <exception cref="T:System.NotSupportedException">Dynamic partitioning is not supported by this partitioner.</exception>
		// Token: 0x06005C47 RID: 23623 RVA: 0x0012A701 File Offset: 0x00128901
		public virtual IEnumerable<KeyValuePair<long, TSource>> GetOrderableDynamicPartitions()
		{
			throw new NotSupportedException("Dynamic partitions are not supported by this partitioner.");
		}

		/// <summary>Gets whether elements in each partition are yielded in the order of increasing keys.</summary>
		/// <returns>true if the elements in each partition are yielded in the order of increasing keys; otherwise false.</returns>
		// Token: 0x17001096 RID: 4246
		// (get) Token: 0x06005C48 RID: 23624 RVA: 0x0012A70D File Offset: 0x0012890D
		// (set) Token: 0x06005C49 RID: 23625 RVA: 0x0012A715 File Offset: 0x00128915
		public bool KeysOrderedInEachPartition
		{
			[CompilerGenerated]
			get
			{
				return this.<KeysOrderedInEachPartition>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<KeysOrderedInEachPartition>k__BackingField = value;
			}
		}

		/// <summary>Gets whether elements in an earlier partition always come before elements in a later partition.</summary>
		/// <returns>true if the elements in an earlier partition always come before elements in a later partition; otherwise false.</returns>
		// Token: 0x17001097 RID: 4247
		// (get) Token: 0x06005C4A RID: 23626 RVA: 0x0012A71E File Offset: 0x0012891E
		// (set) Token: 0x06005C4B RID: 23627 RVA: 0x0012A726 File Offset: 0x00128926
		public bool KeysOrderedAcrossPartitions
		{
			[CompilerGenerated]
			get
			{
				return this.<KeysOrderedAcrossPartitions>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<KeysOrderedAcrossPartitions>k__BackingField = value;
			}
		}

		/// <summary>Gets whether order keys are normalized.</summary>
		/// <returns>true if the keys are normalized; otherwise false.</returns>
		// Token: 0x17001098 RID: 4248
		// (get) Token: 0x06005C4C RID: 23628 RVA: 0x0012A72F File Offset: 0x0012892F
		// (set) Token: 0x06005C4D RID: 23629 RVA: 0x0012A737 File Offset: 0x00128937
		public bool KeysNormalized
		{
			[CompilerGenerated]
			get
			{
				return this.<KeysNormalized>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<KeysNormalized>k__BackingField = value;
			}
		}

		/// <summary>Partitions the underlying collection into the given number of ordered partitions.</summary>
		/// <param name="partitionCount">The number of partitions to create.</param>
		/// <returns>A list containing <paramref name="partitionCount" /> enumerators.</returns>
		// Token: 0x06005C4E RID: 23630 RVA: 0x0012A740 File Offset: 0x00128940
		public override IList<IEnumerator<TSource>> GetPartitions(int partitionCount)
		{
			IList<IEnumerator<KeyValuePair<long, TSource>>> orderablePartitions = this.GetOrderablePartitions(partitionCount);
			if (orderablePartitions.Count != partitionCount)
			{
				throw new InvalidOperationException("GetPartitions returned an incorrect number of partitions.");
			}
			IEnumerator<TSource>[] array = new IEnumerator<!0>[partitionCount];
			for (int i = 0; i < partitionCount; i++)
			{
				array[i] = new OrderablePartitioner<TSource>.EnumeratorDropIndices(orderablePartitions[i]);
			}
			return array;
		}

		/// <summary>Creates an object that can partition the underlying collection into a variable number of partitions.</summary>
		/// <returns>An object that can create partitions over the underlying data source.</returns>
		/// <exception cref="T:System.NotSupportedException">Dynamic partitioning is not supported by the base class. It must be implemented in derived classes.</exception>
		// Token: 0x06005C4F RID: 23631 RVA: 0x0012A78C File Offset: 0x0012898C
		public override IEnumerable<TSource> GetDynamicPartitions()
		{
			return new OrderablePartitioner<TSource>.EnumerableDropIndices(this.GetOrderableDynamicPartitions());
		}

		// Token: 0x04002E2C RID: 11820
		[CompilerGenerated]
		private bool <KeysOrderedInEachPartition>k__BackingField;

		// Token: 0x04002E2D RID: 11821
		[CompilerGenerated]
		private bool <KeysOrderedAcrossPartitions>k__BackingField;

		// Token: 0x04002E2E RID: 11822
		[CompilerGenerated]
		private bool <KeysNormalized>k__BackingField;

		// Token: 0x020009ED RID: 2541
		private class EnumerableDropIndices : IEnumerable<!0>, IEnumerable, IDisposable
		{
			// Token: 0x06005C50 RID: 23632 RVA: 0x0012A799 File Offset: 0x00128999
			public EnumerableDropIndices(IEnumerable<KeyValuePair<long, TSource>> source)
			{
				this._source = source;
			}

			// Token: 0x06005C51 RID: 23633 RVA: 0x0012A7A8 File Offset: 0x001289A8
			public IEnumerator<TSource> GetEnumerator()
			{
				return new OrderablePartitioner<TSource>.EnumeratorDropIndices(this._source.GetEnumerator());
			}

			// Token: 0x06005C52 RID: 23634 RVA: 0x0012A7BA File Offset: 0x001289BA
			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.GetEnumerator();
			}

			// Token: 0x06005C53 RID: 23635 RVA: 0x0012A7C4 File Offset: 0x001289C4
			public void Dispose()
			{
				IDisposable disposable = this._source as IDisposable;
				if (disposable != null)
				{
					disposable.Dispose();
				}
			}

			// Token: 0x04002E2F RID: 11823
			private readonly IEnumerable<KeyValuePair<long, TSource>> _source;
		}

		// Token: 0x020009EE RID: 2542
		private class EnumeratorDropIndices : IEnumerator<!0>, IDisposable, IEnumerator
		{
			// Token: 0x06005C54 RID: 23636 RVA: 0x0012A7E6 File Offset: 0x001289E6
			public EnumeratorDropIndices(IEnumerator<KeyValuePair<long, TSource>> source)
			{
				this._source = source;
			}

			// Token: 0x06005C55 RID: 23637 RVA: 0x0012A7F5 File Offset: 0x001289F5
			public bool MoveNext()
			{
				return this._source.MoveNext();
			}

			// Token: 0x17001099 RID: 4249
			// (get) Token: 0x06005C56 RID: 23638 RVA: 0x0012A804 File Offset: 0x00128A04
			public TSource Current
			{
				get
				{
					KeyValuePair<long, TSource> keyValuePair = this._source.Current;
					return keyValuePair.Value;
				}
			}

			// Token: 0x1700109A RID: 4250
			// (get) Token: 0x06005C57 RID: 23639 RVA: 0x0012A824 File Offset: 0x00128A24
			object IEnumerator.Current
			{
				get
				{
					return this.Current;
				}
			}

			// Token: 0x06005C58 RID: 23640 RVA: 0x0012A831 File Offset: 0x00128A31
			public void Dispose()
			{
				this._source.Dispose();
			}

			// Token: 0x06005C59 RID: 23641 RVA: 0x0012A83E File Offset: 0x00128A3E
			public void Reset()
			{
				this._source.Reset();
			}

			// Token: 0x04002E30 RID: 11824
			private readonly IEnumerator<KeyValuePair<long, TSource>> _source;
		}
	}
}
