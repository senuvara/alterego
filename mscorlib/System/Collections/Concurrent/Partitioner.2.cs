﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading;

namespace System.Collections.Concurrent
{
	/// <summary>Provides common partitioning strategies for arrays, lists, and enumerables.</summary>
	// Token: 0x020009F1 RID: 2545
	public static class Partitioner
	{
		/// <summary>Creates an orderable partitioner from an <see cref="T:System.Collections.Generic.IList`1" /> instance.</summary>
		/// <param name="list">The list to be partitioned.</param>
		/// <param name="loadBalance">A Boolean value that indicates whether the created partitioner should dynamically load balance between partitions rather than statically partition.</param>
		/// <typeparam name="TSource">Type of the elements in source list.</typeparam>
		/// <returns>An orderable partitioner based on the input list.</returns>
		// Token: 0x06005C5E RID: 23646 RVA: 0x0012A84B File Offset: 0x00128A4B
		public static OrderablePartitioner<TSource> Create<TSource>(IList<TSource> list, bool loadBalance)
		{
			if (list == null)
			{
				throw new ArgumentNullException("list");
			}
			if (loadBalance)
			{
				return new Partitioner.DynamicPartitionerForIList<TSource>(list);
			}
			return new Partitioner.StaticIndexRangePartitionerForIList<TSource>(list);
		}

		/// <summary>Creates an orderable partitioner from a <see cref="T:System.Array" /> instance.</summary>
		/// <param name="array">The array to be partitioned.</param>
		/// <param name="loadBalance">A Boolean value that indicates whether the created partitioner should dynamically load balance between partitions rather than statically partition.</param>
		/// <typeparam name="TSource">Type of the elements in source array.</typeparam>
		/// <returns>An orderable partitioner based on the input array.</returns>
		// Token: 0x06005C5F RID: 23647 RVA: 0x0012A86B File Offset: 0x00128A6B
		public static OrderablePartitioner<TSource> Create<TSource>(TSource[] array, bool loadBalance)
		{
			if (array == null)
			{
				throw new ArgumentNullException("array");
			}
			if (loadBalance)
			{
				return new Partitioner.DynamicPartitionerForArray<TSource>(array);
			}
			return new Partitioner.StaticIndexRangePartitionerForArray<TSource>(array);
		}

		/// <summary>Creates an orderable partitioner from a <see cref="T:System.Collections.Generic.IEnumerable`1" /> instance.</summary>
		/// <param name="source">The enumerable to be partitioned.</param>
		/// <typeparam name="TSource">Type of the elements in source enumerable.</typeparam>
		/// <returns>An orderable partitioner based on the input array.</returns>
		// Token: 0x06005C60 RID: 23648 RVA: 0x0012A88B File Offset: 0x00128A8B
		public static OrderablePartitioner<TSource> Create<TSource>(IEnumerable<TSource> source)
		{
			return Partitioner.Create<TSource>(source, EnumerablePartitionerOptions.None);
		}

		/// <summary>Creates an orderable partitioner from a <see cref="T:System.Collections.Generic.IEnumerable`1" /> instance.</summary>
		/// <param name="source">The enumerable to be partitioned.</param>
		/// <param name="partitionerOptions">Options to control the buffering behavior of the partitioner.</param>
		/// <typeparam name="TSource">Type of the elements in source enumerable.</typeparam>
		/// <returns>An orderable partitioner based on the input array.</returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The <paramref name="partitionerOptions" /> argument specifies an invalid value for <see cref="T:System.Collections.Concurrent.EnumerablePartitionerOptions" />.</exception>
		// Token: 0x06005C61 RID: 23649 RVA: 0x0012A894 File Offset: 0x00128A94
		public static OrderablePartitioner<TSource> Create<TSource>(IEnumerable<TSource> source, EnumerablePartitionerOptions partitionerOptions)
		{
			if (source == null)
			{
				throw new ArgumentNullException("source");
			}
			if ((partitionerOptions & ~EnumerablePartitionerOptions.NoBuffering) != EnumerablePartitionerOptions.None)
			{
				throw new ArgumentOutOfRangeException("partitionerOptions");
			}
			return new Partitioner.DynamicPartitionerForIEnumerable<TSource>(source, partitionerOptions);
		}

		/// <summary>Creates a partitioner that chunks the user-specified range.</summary>
		/// <param name="fromInclusive">The lower, inclusive bound of the range.</param>
		/// <param name="toExclusive">The upper, exclusive bound of the range.</param>
		/// <returns>A partitioner.</returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The <paramref name="toExclusive" /> argument is less than or equal to the <paramref name="fromInclusive" /> argument.</exception>
		// Token: 0x06005C62 RID: 23650 RVA: 0x0012A8BC File Offset: 0x00128ABC
		public static OrderablePartitioner<Tuple<long, long>> Create(long fromInclusive, long toExclusive)
		{
			int num = 3;
			if (toExclusive <= fromInclusive)
			{
				throw new ArgumentOutOfRangeException("toExclusive");
			}
			long num2 = (toExclusive - fromInclusive) / (long)(PlatformHelper.ProcessorCount * num);
			if (num2 == 0L)
			{
				num2 = 1L;
			}
			return Partitioner.Create<Tuple<long, long>>(Partitioner.CreateRanges(fromInclusive, toExclusive, num2), EnumerablePartitionerOptions.NoBuffering);
		}

		/// <summary>Creates a partitioner that chunks the user-specified range.</summary>
		/// <param name="fromInclusive">The lower, inclusive bound of the range.</param>
		/// <param name="toExclusive">The upper, exclusive bound of the range.</param>
		/// <param name="rangeSize">The size of each subrange.</param>
		/// <returns>A partitioner.</returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The <paramref name="toExclusive" /> argument is less than or equal to the <paramref name="fromInclusive" /> argument.-or-The <paramref name="rangeSize" /> argument is less than or equal to 0.</exception>
		// Token: 0x06005C63 RID: 23651 RVA: 0x0012A8FB File Offset: 0x00128AFB
		public static OrderablePartitioner<Tuple<long, long>> Create(long fromInclusive, long toExclusive, long rangeSize)
		{
			if (toExclusive <= fromInclusive)
			{
				throw new ArgumentOutOfRangeException("toExclusive");
			}
			if (rangeSize <= 0L)
			{
				throw new ArgumentOutOfRangeException("rangeSize");
			}
			return Partitioner.Create<Tuple<long, long>>(Partitioner.CreateRanges(fromInclusive, toExclusive, rangeSize), EnumerablePartitionerOptions.NoBuffering);
		}

		// Token: 0x06005C64 RID: 23652 RVA: 0x0012A92A File Offset: 0x00128B2A
		private static IEnumerable<Tuple<long, long>> CreateRanges(long fromInclusive, long toExclusive, long rangeSize)
		{
			bool shouldQuit = false;
			long i = fromInclusive;
			while (i < toExclusive && !shouldQuit)
			{
				long item = i;
				long num;
				try
				{
					num = checked(i + rangeSize);
				}
				catch (OverflowException)
				{
					num = toExclusive;
					shouldQuit = true;
				}
				if (num > toExclusive)
				{
					num = toExclusive;
				}
				yield return new Tuple<long, long>(item, num);
				i += rangeSize;
			}
			yield break;
		}

		/// <summary>Creates a partitioner that chunks the user-specified range.</summary>
		/// <param name="fromInclusive">The lower, inclusive bound of the range.</param>
		/// <param name="toExclusive">The upper, exclusive bound of the range.</param>
		/// <returns>A partitioner.</returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The <paramref name="toExclusive" /> argument is less than or equal to the <paramref name="fromInclusive" /> argument.</exception>
		// Token: 0x06005C65 RID: 23653 RVA: 0x0012A948 File Offset: 0x00128B48
		public static OrderablePartitioner<Tuple<int, int>> Create(int fromInclusive, int toExclusive)
		{
			int num = 3;
			if (toExclusive <= fromInclusive)
			{
				throw new ArgumentOutOfRangeException("toExclusive");
			}
			int num2 = (toExclusive - fromInclusive) / (PlatformHelper.ProcessorCount * num);
			if (num2 == 0)
			{
				num2 = 1;
			}
			return Partitioner.Create<Tuple<int, int>>(Partitioner.CreateRanges(fromInclusive, toExclusive, num2), EnumerablePartitionerOptions.NoBuffering);
		}

		/// <summary>Creates a partitioner that chunks the user-specified range.</summary>
		/// <param name="fromInclusive">The lower, inclusive bound of the range.</param>
		/// <param name="toExclusive">The upper, exclusive bound of the range.</param>
		/// <param name="rangeSize">The size of each subrange.</param>
		/// <returns>A partitioner.</returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The <paramref name="toExclusive" /> argument is less than or equal to the <paramref name="fromInclusive" /> argument.-or-The <paramref name="rangeSize" /> argument is less than or equal to 0.</exception>
		// Token: 0x06005C66 RID: 23654 RVA: 0x0012A985 File Offset: 0x00128B85
		public static OrderablePartitioner<Tuple<int, int>> Create(int fromInclusive, int toExclusive, int rangeSize)
		{
			if (toExclusive <= fromInclusive)
			{
				throw new ArgumentOutOfRangeException("toExclusive");
			}
			if (rangeSize <= 0)
			{
				throw new ArgumentOutOfRangeException("rangeSize");
			}
			return Partitioner.Create<Tuple<int, int>>(Partitioner.CreateRanges(fromInclusive, toExclusive, rangeSize), EnumerablePartitionerOptions.NoBuffering);
		}

		// Token: 0x06005C67 RID: 23655 RVA: 0x0012A9B3 File Offset: 0x00128BB3
		private static IEnumerable<Tuple<int, int>> CreateRanges(int fromInclusive, int toExclusive, int rangeSize)
		{
			bool shouldQuit = false;
			int i = fromInclusive;
			while (i < toExclusive && !shouldQuit)
			{
				int item = i;
				int num;
				try
				{
					num = checked(i + rangeSize);
				}
				catch (OverflowException)
				{
					num = toExclusive;
					shouldQuit = true;
				}
				if (num > toExclusive)
				{
					num = toExclusive;
				}
				yield return new Tuple<int, int>(item, num);
				i += rangeSize;
			}
			yield break;
		}

		// Token: 0x06005C68 RID: 23656 RVA: 0x0012A9D4 File Offset: 0x00128BD4
		private static int GetDefaultChunkSize<TSource>()
		{
			int result;
			if (default(TSource) != null || Nullable.GetUnderlyingType(typeof(TSource)) != null)
			{
				result = 128;
			}
			else
			{
				result = 512 / IntPtr.Size;
			}
			return result;
		}

		// Token: 0x04002E34 RID: 11828
		private const int DEFAULT_BYTES_PER_UNIT = 128;

		// Token: 0x04002E35 RID: 11829
		private const int DEFAULT_BYTES_PER_CHUNK = 512;

		// Token: 0x020009F2 RID: 2546
		private abstract class DynamicPartitionEnumerator_Abstract<TSource, TSourceReader> : IEnumerator<KeyValuePair<long, TSource>>, IDisposable, IEnumerator
		{
			// Token: 0x06005C69 RID: 23657 RVA: 0x0012AA1D File Offset: 0x00128C1D
			protected DynamicPartitionEnumerator_Abstract(TSourceReader sharedReader, Partitioner.SharedLong sharedIndex) : this(sharedReader, sharedIndex, false)
			{
			}

			// Token: 0x06005C6A RID: 23658 RVA: 0x0012AA28 File Offset: 0x00128C28
			protected DynamicPartitionEnumerator_Abstract(TSourceReader sharedReader, Partitioner.SharedLong sharedIndex, bool useSingleChunking)
			{
				this._sharedReader = sharedReader;
				this._sharedIndex = sharedIndex;
				this._maxChunkSize = (useSingleChunking ? 1 : Partitioner.DynamicPartitionEnumerator_Abstract<TSource, TSourceReader>.s_defaultMaxChunkSize);
			}

			// Token: 0x06005C6B RID: 23659
			protected abstract bool GrabNextChunk(int requestedChunkSize);

			// Token: 0x1700109C RID: 4252
			// (get) Token: 0x06005C6C RID: 23660
			// (set) Token: 0x06005C6D RID: 23661
			protected abstract bool HasNoElementsLeft { get; set; }

			// Token: 0x1700109D RID: 4253
			// (get) Token: 0x06005C6E RID: 23662
			public abstract KeyValuePair<long, TSource> Current { get; }

			// Token: 0x06005C6F RID: 23663
			public abstract void Dispose();

			// Token: 0x06005C70 RID: 23664 RVA: 0x000175EA File Offset: 0x000157EA
			public void Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x1700109E RID: 4254
			// (get) Token: 0x06005C71 RID: 23665 RVA: 0x0012AA4F File Offset: 0x00128C4F
			object IEnumerator.Current
			{
				get
				{
					return this.Current;
				}
			}

			// Token: 0x06005C72 RID: 23666 RVA: 0x0012AA5C File Offset: 0x00128C5C
			public bool MoveNext()
			{
				if (this._localOffset == null)
				{
					this._localOffset = new Partitioner.SharedInt(-1);
					this._currentChunkSize = new Partitioner.SharedInt(0);
					this._doublingCountdown = 3;
				}
				if (this._localOffset.Value < this._currentChunkSize.Value - 1)
				{
					this._localOffset.Value++;
					return true;
				}
				int requestedChunkSize;
				if (this._currentChunkSize.Value == 0)
				{
					requestedChunkSize = 1;
				}
				else if (this._doublingCountdown > 0)
				{
					requestedChunkSize = this._currentChunkSize.Value;
				}
				else
				{
					requestedChunkSize = Math.Min(this._currentChunkSize.Value * 2, this._maxChunkSize);
					this._doublingCountdown = 3;
				}
				this._doublingCountdown--;
				if (this.GrabNextChunk(requestedChunkSize))
				{
					this._localOffset.Value = 0;
					return true;
				}
				return false;
			}

			// Token: 0x06005C73 RID: 23667 RVA: 0x0012AB3D File Offset: 0x00128D3D
			// Note: this type is marked as 'beforefieldinit'.
			static DynamicPartitionEnumerator_Abstract()
			{
			}

			// Token: 0x04002E36 RID: 11830
			protected readonly TSourceReader _sharedReader;

			// Token: 0x04002E37 RID: 11831
			protected static int s_defaultMaxChunkSize = Partitioner.GetDefaultChunkSize<TSource>();

			// Token: 0x04002E38 RID: 11832
			protected Partitioner.SharedInt _currentChunkSize;

			// Token: 0x04002E39 RID: 11833
			protected Partitioner.SharedInt _localOffset;

			// Token: 0x04002E3A RID: 11834
			private const int CHUNK_DOUBLING_RATE = 3;

			// Token: 0x04002E3B RID: 11835
			private int _doublingCountdown;

			// Token: 0x04002E3C RID: 11836
			protected readonly int _maxChunkSize;

			// Token: 0x04002E3D RID: 11837
			protected readonly Partitioner.SharedLong _sharedIndex;
		}

		// Token: 0x020009F3 RID: 2547
		private class DynamicPartitionerForIEnumerable<TSource> : OrderablePartitioner<TSource>
		{
			// Token: 0x06005C74 RID: 23668 RVA: 0x0012AB49 File Offset: 0x00128D49
			internal DynamicPartitionerForIEnumerable(IEnumerable<TSource> source, EnumerablePartitionerOptions partitionerOptions) : base(true, false, true)
			{
				this._source = source;
				this._useSingleChunking = ((partitionerOptions & EnumerablePartitionerOptions.NoBuffering) > EnumerablePartitionerOptions.None);
			}

			// Token: 0x06005C75 RID: 23669 RVA: 0x0012AB68 File Offset: 0x00128D68
			public override IList<IEnumerator<KeyValuePair<long, TSource>>> GetOrderablePartitions(int partitionCount)
			{
				if (partitionCount <= 0)
				{
					throw new ArgumentOutOfRangeException("partitionCount");
				}
				IEnumerator<KeyValuePair<long, TSource>>[] array = new IEnumerator<KeyValuePair<long, TSource>>[partitionCount];
				IEnumerable<KeyValuePair<long, TSource>> enumerable = new Partitioner.DynamicPartitionerForIEnumerable<TSource>.InternalPartitionEnumerable(this._source.GetEnumerator(), this._useSingleChunking, true);
				for (int i = 0; i < partitionCount; i++)
				{
					array[i] = enumerable.GetEnumerator();
				}
				return array;
			}

			// Token: 0x06005C76 RID: 23670 RVA: 0x0012ABB9 File Offset: 0x00128DB9
			public override IEnumerable<KeyValuePair<long, TSource>> GetOrderableDynamicPartitions()
			{
				return new Partitioner.DynamicPartitionerForIEnumerable<TSource>.InternalPartitionEnumerable(this._source.GetEnumerator(), this._useSingleChunking, false);
			}

			// Token: 0x1700109F RID: 4255
			// (get) Token: 0x06005C77 RID: 23671 RVA: 0x00004E08 File Offset: 0x00003008
			public override bool SupportsDynamicPartitions
			{
				get
				{
					return true;
				}
			}

			// Token: 0x04002E3E RID: 11838
			private IEnumerable<TSource> _source;

			// Token: 0x04002E3F RID: 11839
			private readonly bool _useSingleChunking;

			// Token: 0x020009F4 RID: 2548
			private class InternalPartitionEnumerable : IEnumerable<KeyValuePair<long, TSource>>, IEnumerable, IDisposable
			{
				// Token: 0x06005C78 RID: 23672 RVA: 0x0012ABD4 File Offset: 0x00128DD4
				internal InternalPartitionEnumerable(IEnumerator<TSource> sharedReader, bool useSingleChunking, bool isStaticPartitioning)
				{
					this._sharedReader = sharedReader;
					this._sharedIndex = new Partitioner.SharedLong(-1L);
					this._hasNoElementsLeft = new Partitioner.SharedBool(false);
					this._sourceDepleted = new Partitioner.SharedBool(false);
					this._sharedLock = new object();
					this._useSingleChunking = useSingleChunking;
					if (!this._useSingleChunking)
					{
						int num = (PlatformHelper.ProcessorCount > 4) ? 4 : 1;
						this._fillBuffer = new KeyValuePair<long, TSource>[num * Partitioner.GetDefaultChunkSize<TSource>()];
					}
					if (isStaticPartitioning)
					{
						this._activePartitionCount = new Partitioner.SharedInt(0);
						return;
					}
					this._activePartitionCount = null;
				}

				// Token: 0x06005C79 RID: 23673 RVA: 0x0012AC65 File Offset: 0x00128E65
				public IEnumerator<KeyValuePair<long, TSource>> GetEnumerator()
				{
					if (this._disposed)
					{
						throw new ObjectDisposedException("Can not call GetEnumerator on partitions after the source enumerable is disposed");
					}
					return new Partitioner.DynamicPartitionerForIEnumerable<TSource>.InternalPartitionEnumerator(this._sharedReader, this._sharedIndex, this._hasNoElementsLeft, this._activePartitionCount, this, this._useSingleChunking);
				}

				// Token: 0x06005C7A RID: 23674 RVA: 0x0012AC9E File Offset: 0x00128E9E
				IEnumerator IEnumerable.GetEnumerator()
				{
					return this.GetEnumerator();
				}

				// Token: 0x06005C7B RID: 23675 RVA: 0x0012ACA8 File Offset: 0x00128EA8
				private void TryCopyFromFillBuffer(KeyValuePair<long, TSource>[] destArray, int requestedChunkSize, ref int actualNumElementsGrabbed)
				{
					actualNumElementsGrabbed = 0;
					KeyValuePair<long, TSource>[] fillBuffer = this._fillBuffer;
					if (fillBuffer == null)
					{
						return;
					}
					if (this._fillBufferCurrentPosition >= this._fillBufferSize)
					{
						return;
					}
					Interlocked.Increment(ref this._activeCopiers);
					int num = Interlocked.Add(ref this._fillBufferCurrentPosition, requestedChunkSize);
					int num2 = num - requestedChunkSize;
					if (num2 < this._fillBufferSize)
					{
						actualNumElementsGrabbed = ((num < this._fillBufferSize) ? num : (this._fillBufferSize - num2));
						Array.Copy(fillBuffer, num2, destArray, 0, actualNumElementsGrabbed);
					}
					Interlocked.Decrement(ref this._activeCopiers);
				}

				// Token: 0x06005C7C RID: 23676 RVA: 0x0012AD31 File Offset: 0x00128F31
				internal bool GrabChunk(KeyValuePair<long, TSource>[] destArray, int requestedChunkSize, ref int actualNumElementsGrabbed)
				{
					actualNumElementsGrabbed = 0;
					if (this._hasNoElementsLeft.Value)
					{
						return false;
					}
					if (this._useSingleChunking)
					{
						return this.GrabChunk_Single(destArray, requestedChunkSize, ref actualNumElementsGrabbed);
					}
					return this.GrabChunk_Buffered(destArray, requestedChunkSize, ref actualNumElementsGrabbed);
				}

				// Token: 0x06005C7D RID: 23677 RVA: 0x0012AD64 File Offset: 0x00128F64
				internal bool GrabChunk_Single(KeyValuePair<long, TSource>[] destArray, int requestedChunkSize, ref int actualNumElementsGrabbed)
				{
					object sharedLock = this._sharedLock;
					bool result;
					lock (sharedLock)
					{
						if (this._hasNoElementsLeft.Value)
						{
							result = false;
						}
						else
						{
							try
							{
								if (this._sharedReader.MoveNext())
								{
									this._sharedIndex.Value = checked(this._sharedIndex.Value + 1L);
									destArray[0] = new KeyValuePair<long, TSource>(this._sharedIndex.Value, this._sharedReader.Current);
									actualNumElementsGrabbed = 1;
									result = true;
								}
								else
								{
									this._sourceDepleted.Value = true;
									this._hasNoElementsLeft.Value = true;
									result = false;
								}
							}
							catch
							{
								this._sourceDepleted.Value = true;
								this._hasNoElementsLeft.Value = true;
								throw;
							}
						}
					}
					return result;
				}

				// Token: 0x06005C7E RID: 23678 RVA: 0x0012AE50 File Offset: 0x00129050
				internal bool GrabChunk_Buffered(KeyValuePair<long, TSource>[] destArray, int requestedChunkSize, ref int actualNumElementsGrabbed)
				{
					this.TryCopyFromFillBuffer(destArray, requestedChunkSize, ref actualNumElementsGrabbed);
					if (actualNumElementsGrabbed == requestedChunkSize)
					{
						return true;
					}
					if (this._sourceDepleted.Value)
					{
						this._hasNoElementsLeft.Value = true;
						this._fillBuffer = null;
						return actualNumElementsGrabbed > 0;
					}
					object sharedLock = this._sharedLock;
					lock (sharedLock)
					{
						if (this._sourceDepleted.Value)
						{
							return actualNumElementsGrabbed > 0;
						}
						try
						{
							if (this._activeCopiers > 0)
							{
								SpinWait spinWait = default(SpinWait);
								while (this._activeCopiers > 0)
								{
									spinWait.SpinOnce();
								}
							}
							while (actualNumElementsGrabbed < requestedChunkSize)
							{
								if (!this._sharedReader.MoveNext())
								{
									this._sourceDepleted.Value = true;
									break;
								}
								this._sharedIndex.Value = checked(this._sharedIndex.Value + 1L);
								destArray[actualNumElementsGrabbed] = new KeyValuePair<long, TSource>(this._sharedIndex.Value, this._sharedReader.Current);
								actualNumElementsGrabbed++;
							}
							KeyValuePair<long, TSource>[] fillBuffer = this._fillBuffer;
							if (!this._sourceDepleted.Value && fillBuffer != null && this._fillBufferCurrentPosition >= fillBuffer.Length)
							{
								for (int i = 0; i < fillBuffer.Length; i++)
								{
									if (!this._sharedReader.MoveNext())
									{
										this._sourceDepleted.Value = true;
										this._fillBufferSize = i;
										break;
									}
									this._sharedIndex.Value = checked(this._sharedIndex.Value + 1L);
									fillBuffer[i] = new KeyValuePair<long, TSource>(this._sharedIndex.Value, this._sharedReader.Current);
								}
								this._fillBufferCurrentPosition = 0;
							}
						}
						catch
						{
							this._sourceDepleted.Value = true;
							this._hasNoElementsLeft.Value = true;
							throw;
						}
					}
					return actualNumElementsGrabbed > 0;
				}

				// Token: 0x06005C7F RID: 23679 RVA: 0x0012B06C File Offset: 0x0012926C
				public void Dispose()
				{
					if (!this._disposed)
					{
						this._disposed = true;
						this._sharedReader.Dispose();
					}
				}

				// Token: 0x04002E40 RID: 11840
				private readonly IEnumerator<TSource> _sharedReader;

				// Token: 0x04002E41 RID: 11841
				private Partitioner.SharedLong _sharedIndex;

				// Token: 0x04002E42 RID: 11842
				private volatile KeyValuePair<long, TSource>[] _fillBuffer;

				// Token: 0x04002E43 RID: 11843
				private volatile int _fillBufferSize;

				// Token: 0x04002E44 RID: 11844
				private volatile int _fillBufferCurrentPosition;

				// Token: 0x04002E45 RID: 11845
				private volatile int _activeCopiers;

				// Token: 0x04002E46 RID: 11846
				private Partitioner.SharedBool _hasNoElementsLeft;

				// Token: 0x04002E47 RID: 11847
				private Partitioner.SharedBool _sourceDepleted;

				// Token: 0x04002E48 RID: 11848
				private object _sharedLock;

				// Token: 0x04002E49 RID: 11849
				private bool _disposed;

				// Token: 0x04002E4A RID: 11850
				private Partitioner.SharedInt _activePartitionCount;

				// Token: 0x04002E4B RID: 11851
				private readonly bool _useSingleChunking;
			}

			// Token: 0x020009F5 RID: 2549
			private class InternalPartitionEnumerator : Partitioner.DynamicPartitionEnumerator_Abstract<TSource, IEnumerator<TSource>>
			{
				// Token: 0x06005C80 RID: 23680 RVA: 0x0012B088 File Offset: 0x00129288
				internal InternalPartitionEnumerator(IEnumerator<TSource> sharedReader, Partitioner.SharedLong sharedIndex, Partitioner.SharedBool hasNoElementsLeft, Partitioner.SharedInt activePartitionCount, Partitioner.DynamicPartitionerForIEnumerable<TSource>.InternalPartitionEnumerable enumerable, bool useSingleChunking) : base(sharedReader, sharedIndex, useSingleChunking)
				{
					this._hasNoElementsLeft = hasNoElementsLeft;
					this._enumerable = enumerable;
					this._activePartitionCount = activePartitionCount;
					if (this._activePartitionCount != null)
					{
						Interlocked.Increment(ref this._activePartitionCount.Value);
					}
				}

				// Token: 0x06005C81 RID: 23681 RVA: 0x0012B0C4 File Offset: 0x001292C4
				protected override bool GrabNextChunk(int requestedChunkSize)
				{
					if (this.HasNoElementsLeft)
					{
						return false;
					}
					if (this._localList == null)
					{
						this._localList = new KeyValuePair<long, TSource>[this._maxChunkSize];
					}
					return this._enumerable.GrabChunk(this._localList, requestedChunkSize, ref this._currentChunkSize.Value);
				}

				// Token: 0x170010A0 RID: 4256
				// (get) Token: 0x06005C82 RID: 23682 RVA: 0x0012B111 File Offset: 0x00129311
				// (set) Token: 0x06005C83 RID: 23683 RVA: 0x0012B120 File Offset: 0x00129320
				protected override bool HasNoElementsLeft
				{
					get
					{
						return this._hasNoElementsLeft.Value;
					}
					set
					{
						this._hasNoElementsLeft.Value = true;
					}
				}

				// Token: 0x170010A1 RID: 4257
				// (get) Token: 0x06005C84 RID: 23684 RVA: 0x0012B130 File Offset: 0x00129330
				public override KeyValuePair<long, TSource> Current
				{
					get
					{
						if (this._currentChunkSize == null)
						{
							throw new InvalidOperationException("MoveNext must be called at least once before calling Current.");
						}
						return this._localList[this._localOffset.Value];
					}
				}

				// Token: 0x06005C85 RID: 23685 RVA: 0x0012B15D File Offset: 0x0012935D
				public override void Dispose()
				{
					if (this._activePartitionCount != null && Interlocked.Decrement(ref this._activePartitionCount.Value) == 0)
					{
						this._enumerable.Dispose();
					}
				}

				// Token: 0x04002E4C RID: 11852
				private KeyValuePair<long, TSource>[] _localList;

				// Token: 0x04002E4D RID: 11853
				private readonly Partitioner.SharedBool _hasNoElementsLeft;

				// Token: 0x04002E4E RID: 11854
				private readonly Partitioner.SharedInt _activePartitionCount;

				// Token: 0x04002E4F RID: 11855
				private Partitioner.DynamicPartitionerForIEnumerable<TSource>.InternalPartitionEnumerable _enumerable;
			}
		}

		// Token: 0x020009F6 RID: 2550
		private abstract class DynamicPartitionerForIndexRange_Abstract<TSource, TCollection> : OrderablePartitioner<TSource>
		{
			// Token: 0x06005C86 RID: 23686 RVA: 0x0012B184 File Offset: 0x00129384
			protected DynamicPartitionerForIndexRange_Abstract(TCollection data) : base(true, false, true)
			{
				this._data = data;
			}

			// Token: 0x06005C87 RID: 23687
			protected abstract IEnumerable<KeyValuePair<long, TSource>> GetOrderableDynamicPartitions_Factory(TCollection data);

			// Token: 0x06005C88 RID: 23688 RVA: 0x0012B198 File Offset: 0x00129398
			public override IList<IEnumerator<KeyValuePair<long, TSource>>> GetOrderablePartitions(int partitionCount)
			{
				if (partitionCount <= 0)
				{
					throw new ArgumentOutOfRangeException("partitionCount");
				}
				IEnumerator<KeyValuePair<long, TSource>>[] array = new IEnumerator<KeyValuePair<long, TSource>>[partitionCount];
				IEnumerable<KeyValuePair<long, TSource>> orderableDynamicPartitions_Factory = this.GetOrderableDynamicPartitions_Factory(this._data);
				for (int i = 0; i < partitionCount; i++)
				{
					array[i] = orderableDynamicPartitions_Factory.GetEnumerator();
				}
				return array;
			}

			// Token: 0x06005C89 RID: 23689 RVA: 0x0012B1DE File Offset: 0x001293DE
			public override IEnumerable<KeyValuePair<long, TSource>> GetOrderableDynamicPartitions()
			{
				return this.GetOrderableDynamicPartitions_Factory(this._data);
			}

			// Token: 0x170010A2 RID: 4258
			// (get) Token: 0x06005C8A RID: 23690 RVA: 0x00004E08 File Offset: 0x00003008
			public override bool SupportsDynamicPartitions
			{
				get
				{
					return true;
				}
			}

			// Token: 0x04002E50 RID: 11856
			private TCollection _data;
		}

		// Token: 0x020009F7 RID: 2551
		private abstract class DynamicPartitionEnumeratorForIndexRange_Abstract<TSource, TSourceReader> : Partitioner.DynamicPartitionEnumerator_Abstract<TSource, TSourceReader>
		{
			// Token: 0x06005C8B RID: 23691 RVA: 0x0012B1EC File Offset: 0x001293EC
			protected DynamicPartitionEnumeratorForIndexRange_Abstract(TSourceReader sharedReader, Partitioner.SharedLong sharedIndex) : base(sharedReader, sharedIndex)
			{
			}

			// Token: 0x170010A3 RID: 4259
			// (get) Token: 0x06005C8C RID: 23692
			protected abstract int SourceCount { get; }

			// Token: 0x06005C8D RID: 23693 RVA: 0x0012B1F8 File Offset: 0x001293F8
			protected override bool GrabNextChunk(int requestedChunkSize)
			{
				while (!this.HasNoElementsLeft)
				{
					long num = Volatile.Read(ref this._sharedIndex.Value);
					if (this.HasNoElementsLeft)
					{
						return false;
					}
					long num2 = Math.Min((long)(this.SourceCount - 1), num + (long)requestedChunkSize);
					if (Interlocked.CompareExchange(ref this._sharedIndex.Value, num2, num) == num)
					{
						this._currentChunkSize.Value = (int)(num2 - num);
						this._localOffset.Value = -1;
						this._startIndex = (int)(num + 1L);
						return true;
					}
				}
				return false;
			}

			// Token: 0x170010A4 RID: 4260
			// (get) Token: 0x06005C8E RID: 23694 RVA: 0x0012B27F File Offset: 0x0012947F
			// (set) Token: 0x06005C8F RID: 23695 RVA: 0x000020D3 File Offset: 0x000002D3
			protected override bool HasNoElementsLeft
			{
				get
				{
					return Volatile.Read(ref this._sharedIndex.Value) >= (long)(this.SourceCount - 1);
				}
				set
				{
				}
			}

			// Token: 0x06005C90 RID: 23696 RVA: 0x000020D3 File Offset: 0x000002D3
			public override void Dispose()
			{
			}

			// Token: 0x04002E51 RID: 11857
			protected int _startIndex;
		}

		// Token: 0x020009F8 RID: 2552
		private class DynamicPartitionerForIList<TSource> : Partitioner.DynamicPartitionerForIndexRange_Abstract<TSource, IList<TSource>>
		{
			// Token: 0x06005C91 RID: 23697 RVA: 0x0012B29F File Offset: 0x0012949F
			internal DynamicPartitionerForIList(IList<TSource> source) : base(source)
			{
			}

			// Token: 0x06005C92 RID: 23698 RVA: 0x0012B2A8 File Offset: 0x001294A8
			protected override IEnumerable<KeyValuePair<long, TSource>> GetOrderableDynamicPartitions_Factory(IList<TSource> _data)
			{
				return new Partitioner.DynamicPartitionerForIList<TSource>.InternalPartitionEnumerable(_data);
			}

			// Token: 0x020009F9 RID: 2553
			private class InternalPartitionEnumerable : IEnumerable<KeyValuePair<long, TSource>>, IEnumerable
			{
				// Token: 0x06005C93 RID: 23699 RVA: 0x0012B2B0 File Offset: 0x001294B0
				internal InternalPartitionEnumerable(IList<TSource> sharedReader)
				{
					this._sharedReader = sharedReader;
					this._sharedIndex = new Partitioner.SharedLong(-1L);
				}

				// Token: 0x06005C94 RID: 23700 RVA: 0x0012B2CC File Offset: 0x001294CC
				public IEnumerator<KeyValuePair<long, TSource>> GetEnumerator()
				{
					return new Partitioner.DynamicPartitionerForIList<TSource>.InternalPartitionEnumerator(this._sharedReader, this._sharedIndex);
				}

				// Token: 0x06005C95 RID: 23701 RVA: 0x0012B2DF File Offset: 0x001294DF
				IEnumerator IEnumerable.GetEnumerator()
				{
					return this.GetEnumerator();
				}

				// Token: 0x04002E52 RID: 11858
				private readonly IList<TSource> _sharedReader;

				// Token: 0x04002E53 RID: 11859
				private Partitioner.SharedLong _sharedIndex;
			}

			// Token: 0x020009FA RID: 2554
			private class InternalPartitionEnumerator : Partitioner.DynamicPartitionEnumeratorForIndexRange_Abstract<TSource, IList<TSource>>
			{
				// Token: 0x06005C96 RID: 23702 RVA: 0x0012B2E7 File Offset: 0x001294E7
				internal InternalPartitionEnumerator(IList<TSource> sharedReader, Partitioner.SharedLong sharedIndex) : base(sharedReader, sharedIndex)
				{
				}

				// Token: 0x170010A5 RID: 4261
				// (get) Token: 0x06005C97 RID: 23703 RVA: 0x0012B2F1 File Offset: 0x001294F1
				protected override int SourceCount
				{
					get
					{
						return this._sharedReader.Count;
					}
				}

				// Token: 0x170010A6 RID: 4262
				// (get) Token: 0x06005C98 RID: 23704 RVA: 0x0012B300 File Offset: 0x00129500
				public override KeyValuePair<long, TSource> Current
				{
					get
					{
						if (this._currentChunkSize == null)
						{
							throw new InvalidOperationException("MoveNext must be called at least once before calling Current.");
						}
						return new KeyValuePair<long, TSource>((long)(this._startIndex + this._localOffset.Value), this._sharedReader[this._startIndex + this._localOffset.Value]);
					}
				}
			}
		}

		// Token: 0x020009FB RID: 2555
		private class DynamicPartitionerForArray<TSource> : Partitioner.DynamicPartitionerForIndexRange_Abstract<TSource, TSource[]>
		{
			// Token: 0x06005C99 RID: 23705 RVA: 0x0012B359 File Offset: 0x00129559
			internal DynamicPartitionerForArray(TSource[] source) : base(source)
			{
			}

			// Token: 0x06005C9A RID: 23706 RVA: 0x0012B362 File Offset: 0x00129562
			protected override IEnumerable<KeyValuePair<long, TSource>> GetOrderableDynamicPartitions_Factory(TSource[] _data)
			{
				return new Partitioner.DynamicPartitionerForArray<TSource>.InternalPartitionEnumerable(_data);
			}

			// Token: 0x020009FC RID: 2556
			private class InternalPartitionEnumerable : IEnumerable<KeyValuePair<long, TSource>>, IEnumerable
			{
				// Token: 0x06005C9B RID: 23707 RVA: 0x0012B36A File Offset: 0x0012956A
				internal InternalPartitionEnumerable(TSource[] sharedReader)
				{
					this._sharedReader = sharedReader;
					this._sharedIndex = new Partitioner.SharedLong(-1L);
				}

				// Token: 0x06005C9C RID: 23708 RVA: 0x0012B386 File Offset: 0x00129586
				IEnumerator IEnumerable.GetEnumerator()
				{
					return this.GetEnumerator();
				}

				// Token: 0x06005C9D RID: 23709 RVA: 0x0012B38E File Offset: 0x0012958E
				public IEnumerator<KeyValuePair<long, TSource>> GetEnumerator()
				{
					return new Partitioner.DynamicPartitionerForArray<TSource>.InternalPartitionEnumerator(this._sharedReader, this._sharedIndex);
				}

				// Token: 0x04002E54 RID: 11860
				private readonly TSource[] _sharedReader;

				// Token: 0x04002E55 RID: 11861
				private Partitioner.SharedLong _sharedIndex;
			}

			// Token: 0x020009FD RID: 2557
			private class InternalPartitionEnumerator : Partitioner.DynamicPartitionEnumeratorForIndexRange_Abstract<TSource, TSource[]>
			{
				// Token: 0x06005C9E RID: 23710 RVA: 0x0012B3A1 File Offset: 0x001295A1
				internal InternalPartitionEnumerator(TSource[] sharedReader, Partitioner.SharedLong sharedIndex) : base(sharedReader, sharedIndex)
				{
				}

				// Token: 0x170010A7 RID: 4263
				// (get) Token: 0x06005C9F RID: 23711 RVA: 0x0012B3AB File Offset: 0x001295AB
				protected override int SourceCount
				{
					get
					{
						return this._sharedReader.Length;
					}
				}

				// Token: 0x170010A8 RID: 4264
				// (get) Token: 0x06005CA0 RID: 23712 RVA: 0x0012B3B8 File Offset: 0x001295B8
				public override KeyValuePair<long, TSource> Current
				{
					get
					{
						if (this._currentChunkSize == null)
						{
							throw new InvalidOperationException("MoveNext must be called at least once before calling Current.");
						}
						return new KeyValuePair<long, TSource>((long)(this._startIndex + this._localOffset.Value), this._sharedReader[this._startIndex + this._localOffset.Value]);
					}
				}
			}
		}

		// Token: 0x020009FE RID: 2558
		private abstract class StaticIndexRangePartitioner<TSource, TCollection> : OrderablePartitioner<TSource>
		{
			// Token: 0x06005CA1 RID: 23713 RVA: 0x0012B411 File Offset: 0x00129611
			protected StaticIndexRangePartitioner() : base(true, true, true)
			{
			}

			// Token: 0x170010A9 RID: 4265
			// (get) Token: 0x06005CA2 RID: 23714
			protected abstract int SourceCount { get; }

			// Token: 0x06005CA3 RID: 23715
			protected abstract IEnumerator<KeyValuePair<long, TSource>> CreatePartition(int startIndex, int endIndex);

			// Token: 0x06005CA4 RID: 23716 RVA: 0x0012B41C File Offset: 0x0012961C
			public override IList<IEnumerator<KeyValuePair<long, TSource>>> GetOrderablePartitions(int partitionCount)
			{
				if (partitionCount <= 0)
				{
					throw new ArgumentOutOfRangeException("partitionCount");
				}
				int num = this.SourceCount / partitionCount;
				int num2 = this.SourceCount % partitionCount;
				IEnumerator<KeyValuePair<long, TSource>>[] array = new IEnumerator<KeyValuePair<long, TSource>>[partitionCount];
				int num3 = -1;
				for (int i = 0; i < partitionCount; i++)
				{
					int num4 = num3 + 1;
					if (i < num2)
					{
						num3 = num4 + num;
					}
					else
					{
						num3 = num4 + num - 1;
					}
					array[i] = this.CreatePartition(num4, num3);
				}
				return array;
			}
		}

		// Token: 0x020009FF RID: 2559
		private abstract class StaticIndexRangePartition<TSource> : IEnumerator<KeyValuePair<long, TSource>>, IDisposable, IEnumerator
		{
			// Token: 0x06005CA5 RID: 23717 RVA: 0x0012B489 File Offset: 0x00129689
			protected StaticIndexRangePartition(int startIndex, int endIndex)
			{
				this._startIndex = startIndex;
				this._endIndex = endIndex;
				this._offset = startIndex - 1;
			}

			// Token: 0x170010AA RID: 4266
			// (get) Token: 0x06005CA6 RID: 23718
			public abstract KeyValuePair<long, TSource> Current { get; }

			// Token: 0x06005CA7 RID: 23719 RVA: 0x000020D3 File Offset: 0x000002D3
			public void Dispose()
			{
			}

			// Token: 0x06005CA8 RID: 23720 RVA: 0x000175EA File Offset: 0x000157EA
			public void Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x06005CA9 RID: 23721 RVA: 0x0012B4AA File Offset: 0x001296AA
			public bool MoveNext()
			{
				if (this._offset < this._endIndex)
				{
					this._offset++;
					return true;
				}
				this._offset = this._endIndex + 1;
				return false;
			}

			// Token: 0x170010AB RID: 4267
			// (get) Token: 0x06005CAA RID: 23722 RVA: 0x0012B4E1 File Offset: 0x001296E1
			object IEnumerator.Current
			{
				get
				{
					return this.Current;
				}
			}

			// Token: 0x04002E56 RID: 11862
			protected readonly int _startIndex;

			// Token: 0x04002E57 RID: 11863
			protected readonly int _endIndex;

			// Token: 0x04002E58 RID: 11864
			protected volatile int _offset;
		}

		// Token: 0x02000A00 RID: 2560
		private class StaticIndexRangePartitionerForIList<TSource> : Partitioner.StaticIndexRangePartitioner<TSource, IList<TSource>>
		{
			// Token: 0x06005CAB RID: 23723 RVA: 0x0012B4EE File Offset: 0x001296EE
			internal StaticIndexRangePartitionerForIList(IList<TSource> list)
			{
				this._list = list;
			}

			// Token: 0x170010AC RID: 4268
			// (get) Token: 0x06005CAC RID: 23724 RVA: 0x0012B4FD File Offset: 0x001296FD
			protected override int SourceCount
			{
				get
				{
					return this._list.Count;
				}
			}

			// Token: 0x06005CAD RID: 23725 RVA: 0x0012B50A File Offset: 0x0012970A
			protected override IEnumerator<KeyValuePair<long, TSource>> CreatePartition(int startIndex, int endIndex)
			{
				return new Partitioner.StaticIndexRangePartitionForIList<TSource>(this._list, startIndex, endIndex);
			}

			// Token: 0x04002E59 RID: 11865
			private IList<TSource> _list;
		}

		// Token: 0x02000A01 RID: 2561
		private class StaticIndexRangePartitionForIList<TSource> : Partitioner.StaticIndexRangePartition<TSource>
		{
			// Token: 0x06005CAE RID: 23726 RVA: 0x0012B519 File Offset: 0x00129719
			internal StaticIndexRangePartitionForIList(IList<TSource> list, int startIndex, int endIndex) : base(startIndex, endIndex)
			{
				this._list = list;
			}

			// Token: 0x170010AD RID: 4269
			// (get) Token: 0x06005CAF RID: 23727 RVA: 0x0012B52C File Offset: 0x0012972C
			public override KeyValuePair<long, TSource> Current
			{
				get
				{
					if (this._offset < this._startIndex)
					{
						throw new InvalidOperationException("MoveNext must be called at least once before calling Current.");
					}
					return new KeyValuePair<long, TSource>((long)this._offset, this._list[this._offset]);
				}
			}

			// Token: 0x04002E5A RID: 11866
			private volatile IList<TSource> _list;
		}

		// Token: 0x02000A02 RID: 2562
		private class StaticIndexRangePartitionerForArray<TSource> : Partitioner.StaticIndexRangePartitioner<TSource, TSource[]>
		{
			// Token: 0x06005CB0 RID: 23728 RVA: 0x0012B56C File Offset: 0x0012976C
			internal StaticIndexRangePartitionerForArray(TSource[] array)
			{
				this._array = array;
			}

			// Token: 0x170010AE RID: 4270
			// (get) Token: 0x06005CB1 RID: 23729 RVA: 0x0012B57B File Offset: 0x0012977B
			protected override int SourceCount
			{
				get
				{
					return this._array.Length;
				}
			}

			// Token: 0x06005CB2 RID: 23730 RVA: 0x0012B585 File Offset: 0x00129785
			protected override IEnumerator<KeyValuePair<long, TSource>> CreatePartition(int startIndex, int endIndex)
			{
				return new Partitioner.StaticIndexRangePartitionForArray<TSource>(this._array, startIndex, endIndex);
			}

			// Token: 0x04002E5B RID: 11867
			private TSource[] _array;
		}

		// Token: 0x02000A03 RID: 2563
		private class StaticIndexRangePartitionForArray<TSource> : Partitioner.StaticIndexRangePartition<TSource>
		{
			// Token: 0x06005CB3 RID: 23731 RVA: 0x0012B594 File Offset: 0x00129794
			internal StaticIndexRangePartitionForArray(TSource[] array, int startIndex, int endIndex) : base(startIndex, endIndex)
			{
				this._array = array;
			}

			// Token: 0x170010AF RID: 4271
			// (get) Token: 0x06005CB4 RID: 23732 RVA: 0x0012B5A7 File Offset: 0x001297A7
			public override KeyValuePair<long, TSource> Current
			{
				get
				{
					if (this._offset < this._startIndex)
					{
						throw new InvalidOperationException("MoveNext must be called at least once before calling Current.");
					}
					return new KeyValuePair<long, TSource>((long)this._offset, this._array[this._offset]);
				}
			}

			// Token: 0x04002E5C RID: 11868
			private volatile TSource[] _array;
		}

		// Token: 0x02000A04 RID: 2564
		private class SharedInt
		{
			// Token: 0x06005CB5 RID: 23733 RVA: 0x0012B5E7 File Offset: 0x001297E7
			internal SharedInt(int value)
			{
				this.Value = value;
			}

			// Token: 0x04002E5D RID: 11869
			internal volatile int Value;
		}

		// Token: 0x02000A05 RID: 2565
		private class SharedBool
		{
			// Token: 0x06005CB6 RID: 23734 RVA: 0x0012B5F8 File Offset: 0x001297F8
			internal SharedBool(bool value)
			{
				this.Value = value;
			}

			// Token: 0x04002E5E RID: 11870
			internal volatile bool Value;
		}

		// Token: 0x02000A06 RID: 2566
		private class SharedLong
		{
			// Token: 0x06005CB7 RID: 23735 RVA: 0x0012B609 File Offset: 0x00129809
			internal SharedLong(long value)
			{
				this.Value = value;
			}

			// Token: 0x04002E5F RID: 11871
			internal long Value;
		}

		// Token: 0x02000A07 RID: 2567
		[CompilerGenerated]
		private sealed class <CreateRanges>d__6 : IEnumerable<Tuple<long, long>>, IEnumerable, IEnumerator<Tuple<long, long>>, IDisposable, IEnumerator
		{
			// Token: 0x06005CB8 RID: 23736 RVA: 0x0012B618 File Offset: 0x00129818
			[DebuggerHidden]
			public <CreateRanges>d__6(int <>1__state)
			{
				this.<>1__state = <>1__state;
				this.<>l__initialThreadId = Environment.CurrentManagedThreadId;
			}

			// Token: 0x06005CB9 RID: 23737 RVA: 0x000020D3 File Offset: 0x000002D3
			[DebuggerHidden]
			void IDisposable.Dispose()
			{
			}

			// Token: 0x06005CBA RID: 23738 RVA: 0x0012B634 File Offset: 0x00129834
			bool IEnumerator.MoveNext()
			{
				int num = this.<>1__state;
				if (num != 0)
				{
					if (num != 1)
					{
						return false;
					}
					this.<>1__state = -1;
					i += rangeSize;
				}
				else
				{
					this.<>1__state = -1;
					shouldQuit = false;
					i = fromInclusive;
				}
				if (i >= toExclusive || shouldQuit)
				{
					return false;
				}
				long item = i;
				long num2;
				try
				{
					num2 = checked(i + rangeSize);
				}
				catch (OverflowException)
				{
					num2 = toExclusive;
					shouldQuit = true;
				}
				if (num2 > toExclusive)
				{
					num2 = toExclusive;
				}
				this.<>2__current = new Tuple<long, long>(item, num2);
				this.<>1__state = 1;
				return true;
			}

			// Token: 0x170010B0 RID: 4272
			// (get) Token: 0x06005CBB RID: 23739 RVA: 0x0012B6FC File Offset: 0x001298FC
			Tuple<long, long> IEnumerator<Tuple<long, long>>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x06005CBC RID: 23740 RVA: 0x000175EA File Offset: 0x000157EA
			[DebuggerHidden]
			void IEnumerator.Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x170010B1 RID: 4273
			// (get) Token: 0x06005CBD RID: 23741 RVA: 0x0012B6FC File Offset: 0x001298FC
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x06005CBE RID: 23742 RVA: 0x0012B704 File Offset: 0x00129904
			[DebuggerHidden]
			IEnumerator<Tuple<long, long>> IEnumerable<Tuple<long, long>>.GetEnumerator()
			{
				Partitioner.<CreateRanges>d__6 <CreateRanges>d__;
				if (this.<>1__state == -2 && this.<>l__initialThreadId == Environment.CurrentManagedThreadId)
				{
					this.<>1__state = 0;
					<CreateRanges>d__ = this;
				}
				else
				{
					<CreateRanges>d__ = new Partitioner.<CreateRanges>d__6(0);
				}
				<CreateRanges>d__.fromInclusive = fromInclusive;
				<CreateRanges>d__.toExclusive = toExclusive;
				<CreateRanges>d__.rangeSize = rangeSize;
				return <CreateRanges>d__;
			}

			// Token: 0x06005CBF RID: 23743 RVA: 0x0012B75F File Offset: 0x0012995F
			[DebuggerHidden]
			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.System.Collections.Generic.IEnumerable<System.Tuple<System.Int64,System.Int64>>.GetEnumerator();
			}

			// Token: 0x04002E60 RID: 11872
			private int <>1__state;

			// Token: 0x04002E61 RID: 11873
			private Tuple<long, long> <>2__current;

			// Token: 0x04002E62 RID: 11874
			private int <>l__initialThreadId;

			// Token: 0x04002E63 RID: 11875
			private long fromInclusive;

			// Token: 0x04002E64 RID: 11876
			public long <>3__fromInclusive;

			// Token: 0x04002E65 RID: 11877
			private long rangeSize;

			// Token: 0x04002E66 RID: 11878
			public long <>3__rangeSize;

			// Token: 0x04002E67 RID: 11879
			private long toExclusive;

			// Token: 0x04002E68 RID: 11880
			public long <>3__toExclusive;

			// Token: 0x04002E69 RID: 11881
			private long <i>5__1;

			// Token: 0x04002E6A RID: 11882
			private bool <shouldQuit>5__2;
		}

		// Token: 0x02000A08 RID: 2568
		[CompilerGenerated]
		private sealed class <CreateRanges>d__9 : IEnumerable<Tuple<int, int>>, IEnumerable, IEnumerator<Tuple<int, int>>, IDisposable, IEnumerator
		{
			// Token: 0x06005CC0 RID: 23744 RVA: 0x0012B767 File Offset: 0x00129967
			[DebuggerHidden]
			public <CreateRanges>d__9(int <>1__state)
			{
				this.<>1__state = <>1__state;
				this.<>l__initialThreadId = Environment.CurrentManagedThreadId;
			}

			// Token: 0x06005CC1 RID: 23745 RVA: 0x000020D3 File Offset: 0x000002D3
			[DebuggerHidden]
			void IDisposable.Dispose()
			{
			}

			// Token: 0x06005CC2 RID: 23746 RVA: 0x0012B784 File Offset: 0x00129984
			bool IEnumerator.MoveNext()
			{
				int num = this.<>1__state;
				if (num != 0)
				{
					if (num != 1)
					{
						return false;
					}
					this.<>1__state = -1;
					i += rangeSize;
				}
				else
				{
					this.<>1__state = -1;
					shouldQuit = false;
					i = fromInclusive;
				}
				if (i >= toExclusive || shouldQuit)
				{
					return false;
				}
				int item = i;
				int num2;
				try
				{
					num2 = checked(i + rangeSize);
				}
				catch (OverflowException)
				{
					num2 = toExclusive;
					shouldQuit = true;
				}
				if (num2 > toExclusive)
				{
					num2 = toExclusive;
				}
				this.<>2__current = new Tuple<int, int>(item, num2);
				this.<>1__state = 1;
				return true;
			}

			// Token: 0x170010B2 RID: 4274
			// (get) Token: 0x06005CC3 RID: 23747 RVA: 0x0012B84C File Offset: 0x00129A4C
			Tuple<int, int> IEnumerator<Tuple<int, int>>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x06005CC4 RID: 23748 RVA: 0x000175EA File Offset: 0x000157EA
			[DebuggerHidden]
			void IEnumerator.Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x170010B3 RID: 4275
			// (get) Token: 0x06005CC5 RID: 23749 RVA: 0x0012B84C File Offset: 0x00129A4C
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x06005CC6 RID: 23750 RVA: 0x0012B854 File Offset: 0x00129A54
			[DebuggerHidden]
			IEnumerator<Tuple<int, int>> IEnumerable<Tuple<int, int>>.GetEnumerator()
			{
				Partitioner.<CreateRanges>d__9 <CreateRanges>d__;
				if (this.<>1__state == -2 && this.<>l__initialThreadId == Environment.CurrentManagedThreadId)
				{
					this.<>1__state = 0;
					<CreateRanges>d__ = this;
				}
				else
				{
					<CreateRanges>d__ = new Partitioner.<CreateRanges>d__9(0);
				}
				<CreateRanges>d__.fromInclusive = fromInclusive;
				<CreateRanges>d__.toExclusive = toExclusive;
				<CreateRanges>d__.rangeSize = rangeSize;
				return <CreateRanges>d__;
			}

			// Token: 0x06005CC7 RID: 23751 RVA: 0x0012B8AF File Offset: 0x00129AAF
			[DebuggerHidden]
			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.System.Collections.Generic.IEnumerable<System.Tuple<System.Int32,System.Int32>>.GetEnumerator();
			}

			// Token: 0x04002E6B RID: 11883
			private int <>1__state;

			// Token: 0x04002E6C RID: 11884
			private Tuple<int, int> <>2__current;

			// Token: 0x04002E6D RID: 11885
			private int <>l__initialThreadId;

			// Token: 0x04002E6E RID: 11886
			private int fromInclusive;

			// Token: 0x04002E6F RID: 11887
			public int <>3__fromInclusive;

			// Token: 0x04002E70 RID: 11888
			private int rangeSize;

			// Token: 0x04002E71 RID: 11889
			public int <>3__rangeSize;

			// Token: 0x04002E72 RID: 11890
			private int toExclusive;

			// Token: 0x04002E73 RID: 11891
			public int <>3__toExclusive;

			// Token: 0x04002E74 RID: 11892
			private int <i>5__1;

			// Token: 0x04002E75 RID: 11893
			private bool <shouldQuit>5__2;
		}
	}
}
