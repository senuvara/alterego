﻿using System;

namespace System.Collections
{
	/// <summary>Defines a dictionary key/value pair that can be set or retrieved.</summary>
	// Token: 0x0200098F RID: 2447
	[Serializable]
	public struct DictionaryEntry
	{
		/// <summary>Initializes an instance of the <see cref="T:System.Collections.DictionaryEntry" /> type with the specified key and value.</summary>
		/// <param name="key">The object defined in each key/value pair. </param>
		/// <param name="value">The definition associated with <paramref name="key" />. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="key" /> is <see langword="null" /> and the .NET Framework version is 1.0 or 1.1. </exception>
		// Token: 0x06005794 RID: 22420 RVA: 0x0011CAEB File Offset: 0x0011ACEB
		public DictionaryEntry(object key, object value)
		{
			this._key = key;
			this._value = value;
		}

		/// <summary>Gets or sets the key in the key/value pair.</summary>
		/// <returns>The key in the key/value pair.</returns>
		// Token: 0x17000F57 RID: 3927
		// (get) Token: 0x06005795 RID: 22421 RVA: 0x0011CAFB File Offset: 0x0011ACFB
		// (set) Token: 0x06005796 RID: 22422 RVA: 0x0011CB03 File Offset: 0x0011AD03
		public object Key
		{
			get
			{
				return this._key;
			}
			set
			{
				this._key = value;
			}
		}

		/// <summary>Gets or sets the value in the key/value pair.</summary>
		/// <returns>The value in the key/value pair.</returns>
		// Token: 0x17000F58 RID: 3928
		// (get) Token: 0x06005797 RID: 22423 RVA: 0x0011CB0C File Offset: 0x0011AD0C
		// (set) Token: 0x06005798 RID: 22424 RVA: 0x0011CB14 File Offset: 0x0011AD14
		public object Value
		{
			get
			{
				return this._value;
			}
			set
			{
				this._value = value;
			}
		}

		// Token: 0x06005799 RID: 22425 RVA: 0x0011CB1D File Offset: 0x0011AD1D
		public void Deconstruct(out object key, out object value)
		{
			key = this.Key;
			value = this.Value;
		}

		// Token: 0x04002D1D RID: 11549
		private object _key;

		// Token: 0x04002D1E RID: 11550
		private object _value;
	}
}
