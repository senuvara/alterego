﻿using System;

namespace System.Collections
{
	// Token: 0x020009D2 RID: 2514
	[Serializable]
	internal class StructuralComparer : IComparer
	{
		// Token: 0x06005AF0 RID: 23280 RVA: 0x00126648 File Offset: 0x00124848
		public int Compare(object x, object y)
		{
			if (x == null)
			{
				if (y != null)
				{
					return -1;
				}
				return 0;
			}
			else
			{
				if (y == null)
				{
					return 1;
				}
				IStructuralComparable structuralComparable = x as IStructuralComparable;
				if (structuralComparable != null)
				{
					return structuralComparable.CompareTo(y, this);
				}
				return Comparer.Default.Compare(x, y);
			}
		}

		// Token: 0x06005AF1 RID: 23281 RVA: 0x00002050 File Offset: 0x00000250
		public StructuralComparer()
		{
		}
	}
}
