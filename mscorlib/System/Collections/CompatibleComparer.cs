﻿using System;

namespace System.Collections
{
	// Token: 0x020009A4 RID: 2468
	[Serializable]
	internal class CompatibleComparer : IEqualityComparer
	{
		// Token: 0x0600592F RID: 22831 RVA: 0x00121458 File Offset: 0x0011F658
		internal CompatibleComparer(IComparer comparer, IHashCodeProvider hashCodeProvider)
		{
			this._comparer = comparer;
			this._hcp = hashCodeProvider;
		}

		// Token: 0x06005930 RID: 22832 RVA: 0x00121470 File Offset: 0x0011F670
		public int Compare(object a, object b)
		{
			if (a == b)
			{
				return 0;
			}
			if (a == null)
			{
				return -1;
			}
			if (b == null)
			{
				return 1;
			}
			if (this._comparer != null)
			{
				return this._comparer.Compare(a, b);
			}
			IComparable comparable = a as IComparable;
			if (comparable != null)
			{
				return comparable.CompareTo(b);
			}
			throw new ArgumentException(Environment.GetResourceString("At least one object must implement IComparable."));
		}

		// Token: 0x06005931 RID: 22833 RVA: 0x001214C4 File Offset: 0x0011F6C4
		public bool Equals(object a, object b)
		{
			return this.Compare(a, b) == 0;
		}

		// Token: 0x06005932 RID: 22834 RVA: 0x001214D1 File Offset: 0x0011F6D1
		public int GetHashCode(object obj)
		{
			if (obj == null)
			{
				throw new ArgumentNullException("obj");
			}
			if (this._hcp != null)
			{
				return this._hcp.GetHashCode(obj);
			}
			return obj.GetHashCode();
		}

		// Token: 0x17000FAD RID: 4013
		// (get) Token: 0x06005933 RID: 22835 RVA: 0x001214FC File Offset: 0x0011F6FC
		internal IComparer Comparer
		{
			get
			{
				return this._comparer;
			}
		}

		// Token: 0x17000FAE RID: 4014
		// (get) Token: 0x06005934 RID: 22836 RVA: 0x00121504 File Offset: 0x0011F704
		internal IHashCodeProvider HashCodeProvider
		{
			get
			{
				return this._hcp;
			}
		}

		// Token: 0x04002D5A RID: 11610
		private IComparer _comparer;

		// Token: 0x04002D5B RID: 11611
		private IHashCodeProvider _hcp;
	}
}
