﻿using System;
using System.Threading;

namespace System.Collections
{
	// Token: 0x020009BC RID: 2492
	[Serializable]
	internal class ListDictionaryInternal : IDictionary, ICollection, IEnumerable
	{
		// Token: 0x06005A02 RID: 23042 RVA: 0x00002050 File Offset: 0x00000250
		public ListDictionaryInternal()
		{
		}

		// Token: 0x17000FF5 RID: 4085
		public object this[object key]
		{
			get
			{
				if (key == null)
				{
					throw new ArgumentNullException("key", Environment.GetResourceString("Key cannot be null."));
				}
				for (ListDictionaryInternal.DictionaryNode next = this.head; next != null; next = next.next)
				{
					if (next.key.Equals(key))
					{
						return next.value;
					}
				}
				return null;
			}
			set
			{
				if (key == null)
				{
					throw new ArgumentNullException("key", Environment.GetResourceString("Key cannot be null."));
				}
				if (!key.GetType().IsSerializable)
				{
					throw new ArgumentException(Environment.GetResourceString("Argument passed in is not serializable."), "key");
				}
				if (value != null && !value.GetType().IsSerializable)
				{
					throw new ArgumentException(Environment.GetResourceString("Argument passed in is not serializable."), "value");
				}
				this.version++;
				ListDictionaryInternal.DictionaryNode dictionaryNode = null;
				ListDictionaryInternal.DictionaryNode next = this.head;
				while (next != null && !next.key.Equals(key))
				{
					dictionaryNode = next;
					next = next.next;
				}
				if (next != null)
				{
					next.value = value;
					return;
				}
				ListDictionaryInternal.DictionaryNode dictionaryNode2 = new ListDictionaryInternal.DictionaryNode();
				dictionaryNode2.key = key;
				dictionaryNode2.value = value;
				if (dictionaryNode != null)
				{
					dictionaryNode.next = dictionaryNode2;
				}
				else
				{
					this.head = dictionaryNode2;
				}
				this.count++;
			}
		}

		// Token: 0x17000FF6 RID: 4086
		// (get) Token: 0x06005A05 RID: 23045 RVA: 0x001237F7 File Offset: 0x001219F7
		public int Count
		{
			get
			{
				return this.count;
			}
		}

		// Token: 0x17000FF7 RID: 4087
		// (get) Token: 0x06005A06 RID: 23046 RVA: 0x001237FF File Offset: 0x001219FF
		public ICollection Keys
		{
			get
			{
				return new ListDictionaryInternal.NodeKeyValueCollection(this, true);
			}
		}

		// Token: 0x17000FF8 RID: 4088
		// (get) Token: 0x06005A07 RID: 23047 RVA: 0x00002526 File Offset: 0x00000726
		public bool IsReadOnly
		{
			get
			{
				return false;
			}
		}

		// Token: 0x17000FF9 RID: 4089
		// (get) Token: 0x06005A08 RID: 23048 RVA: 0x00002526 File Offset: 0x00000726
		public bool IsFixedSize
		{
			get
			{
				return false;
			}
		}

		// Token: 0x17000FFA RID: 4090
		// (get) Token: 0x06005A09 RID: 23049 RVA: 0x00002526 File Offset: 0x00000726
		public bool IsSynchronized
		{
			get
			{
				return false;
			}
		}

		// Token: 0x17000FFB RID: 4091
		// (get) Token: 0x06005A0A RID: 23050 RVA: 0x00123808 File Offset: 0x00121A08
		public object SyncRoot
		{
			get
			{
				if (this._syncRoot == null)
				{
					Interlocked.CompareExchange<object>(ref this._syncRoot, new object(), null);
				}
				return this._syncRoot;
			}
		}

		// Token: 0x17000FFC RID: 4092
		// (get) Token: 0x06005A0B RID: 23051 RVA: 0x0012382A File Offset: 0x00121A2A
		public ICollection Values
		{
			get
			{
				return new ListDictionaryInternal.NodeKeyValueCollection(this, false);
			}
		}

		// Token: 0x06005A0C RID: 23052 RVA: 0x00123834 File Offset: 0x00121A34
		public void Add(object key, object value)
		{
			if (key == null)
			{
				throw new ArgumentNullException("key", Environment.GetResourceString("Key cannot be null."));
			}
			if (!key.GetType().IsSerializable)
			{
				throw new ArgumentException(Environment.GetResourceString("Argument passed in is not serializable."), "key");
			}
			if (value != null && !value.GetType().IsSerializable)
			{
				throw new ArgumentException(Environment.GetResourceString("Argument passed in is not serializable."), "value");
			}
			this.version++;
			ListDictionaryInternal.DictionaryNode dictionaryNode = null;
			ListDictionaryInternal.DictionaryNode next;
			for (next = this.head; next != null; next = next.next)
			{
				if (next.key.Equals(key))
				{
					throw new ArgumentException(Environment.GetResourceString("Item has already been added. Key in dictionary: '{0}'  Key being added: '{1}'", new object[]
					{
						next.key,
						key
					}));
				}
				dictionaryNode = next;
			}
			if (next != null)
			{
				next.value = value;
				return;
			}
			ListDictionaryInternal.DictionaryNode dictionaryNode2 = new ListDictionaryInternal.DictionaryNode();
			dictionaryNode2.key = key;
			dictionaryNode2.value = value;
			if (dictionaryNode != null)
			{
				dictionaryNode.next = dictionaryNode2;
			}
			else
			{
				this.head = dictionaryNode2;
			}
			this.count++;
		}

		// Token: 0x06005A0D RID: 23053 RVA: 0x00123936 File Offset: 0x00121B36
		public void Clear()
		{
			this.count = 0;
			this.head = null;
			this.version++;
		}

		// Token: 0x06005A0E RID: 23054 RVA: 0x00123954 File Offset: 0x00121B54
		public bool Contains(object key)
		{
			if (key == null)
			{
				throw new ArgumentNullException("key", Environment.GetResourceString("Key cannot be null."));
			}
			for (ListDictionaryInternal.DictionaryNode next = this.head; next != null; next = next.next)
			{
				if (next.key.Equals(key))
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x06005A0F RID: 23055 RVA: 0x001239A0 File Offset: 0x00121BA0
		public void CopyTo(Array array, int index)
		{
			if (array == null)
			{
				throw new ArgumentNullException("array");
			}
			if (array.Rank != 1)
			{
				throw new ArgumentException(Environment.GetResourceString("Only single dimensional arrays are supported for the requested action."));
			}
			if (index < 0)
			{
				throw new ArgumentOutOfRangeException("index", Environment.GetResourceString("Non-negative number required."));
			}
			if (array.Length - index < this.Count)
			{
				throw new ArgumentException(Environment.GetResourceString("Index was out of range. Must be non-negative and less than the size of the collection."), "index");
			}
			for (ListDictionaryInternal.DictionaryNode next = this.head; next != null; next = next.next)
			{
				array.SetValue(new DictionaryEntry(next.key, next.value), index);
				index++;
			}
		}

		// Token: 0x06005A10 RID: 23056 RVA: 0x00123A47 File Offset: 0x00121C47
		public IDictionaryEnumerator GetEnumerator()
		{
			return new ListDictionaryInternal.NodeEnumerator(this);
		}

		// Token: 0x06005A11 RID: 23057 RVA: 0x00123A47 File Offset: 0x00121C47
		IEnumerator IEnumerable.GetEnumerator()
		{
			return new ListDictionaryInternal.NodeEnumerator(this);
		}

		// Token: 0x06005A12 RID: 23058 RVA: 0x00123A50 File Offset: 0x00121C50
		public void Remove(object key)
		{
			if (key == null)
			{
				throw new ArgumentNullException("key", Environment.GetResourceString("Key cannot be null."));
			}
			this.version++;
			ListDictionaryInternal.DictionaryNode dictionaryNode = null;
			ListDictionaryInternal.DictionaryNode next = this.head;
			while (next != null && !next.key.Equals(key))
			{
				dictionaryNode = next;
				next = next.next;
			}
			if (next == null)
			{
				return;
			}
			if (next == this.head)
			{
				this.head = next.next;
			}
			else
			{
				dictionaryNode.next = next.next;
			}
			this.count--;
		}

		// Token: 0x04002D89 RID: 11657
		private ListDictionaryInternal.DictionaryNode head;

		// Token: 0x04002D8A RID: 11658
		private int version;

		// Token: 0x04002D8B RID: 11659
		private int count;

		// Token: 0x04002D8C RID: 11660
		[NonSerialized]
		private object _syncRoot;

		// Token: 0x020009BD RID: 2493
		private class NodeEnumerator : IDictionaryEnumerator, IEnumerator
		{
			// Token: 0x06005A13 RID: 23059 RVA: 0x00123ADD File Offset: 0x00121CDD
			public NodeEnumerator(ListDictionaryInternal list)
			{
				this.list = list;
				this.version = list.version;
				this.start = true;
				this.current = null;
			}

			// Token: 0x17000FFD RID: 4093
			// (get) Token: 0x06005A14 RID: 23060 RVA: 0x00123B06 File Offset: 0x00121D06
			public object Current
			{
				get
				{
					return this.Entry;
				}
			}

			// Token: 0x17000FFE RID: 4094
			// (get) Token: 0x06005A15 RID: 23061 RVA: 0x00123B13 File Offset: 0x00121D13
			public DictionaryEntry Entry
			{
				get
				{
					if (this.current == null)
					{
						throw new InvalidOperationException(Environment.GetResourceString("Enumeration has either not started or has already finished."));
					}
					return new DictionaryEntry(this.current.key, this.current.value);
				}
			}

			// Token: 0x17000FFF RID: 4095
			// (get) Token: 0x06005A16 RID: 23062 RVA: 0x00123B48 File Offset: 0x00121D48
			public object Key
			{
				get
				{
					if (this.current == null)
					{
						throw new InvalidOperationException(Environment.GetResourceString("Enumeration has either not started or has already finished."));
					}
					return this.current.key;
				}
			}

			// Token: 0x17001000 RID: 4096
			// (get) Token: 0x06005A17 RID: 23063 RVA: 0x00123B6D File Offset: 0x00121D6D
			public object Value
			{
				get
				{
					if (this.current == null)
					{
						throw new InvalidOperationException(Environment.GetResourceString("Enumeration has either not started or has already finished."));
					}
					return this.current.value;
				}
			}

			// Token: 0x06005A18 RID: 23064 RVA: 0x00123B94 File Offset: 0x00121D94
			public bool MoveNext()
			{
				if (this.version != this.list.version)
				{
					throw new InvalidOperationException(Environment.GetResourceString("Collection was modified; enumeration operation may not execute."));
				}
				if (this.start)
				{
					this.current = this.list.head;
					this.start = false;
				}
				else if (this.current != null)
				{
					this.current = this.current.next;
				}
				return this.current != null;
			}

			// Token: 0x06005A19 RID: 23065 RVA: 0x00123C08 File Offset: 0x00121E08
			public void Reset()
			{
				if (this.version != this.list.version)
				{
					throw new InvalidOperationException(Environment.GetResourceString("Collection was modified; enumeration operation may not execute."));
				}
				this.start = true;
				this.current = null;
			}

			// Token: 0x04002D8D RID: 11661
			private ListDictionaryInternal list;

			// Token: 0x04002D8E RID: 11662
			private ListDictionaryInternal.DictionaryNode current;

			// Token: 0x04002D8F RID: 11663
			private int version;

			// Token: 0x04002D90 RID: 11664
			private bool start;
		}

		// Token: 0x020009BE RID: 2494
		private class NodeKeyValueCollection : ICollection, IEnumerable
		{
			// Token: 0x06005A1A RID: 23066 RVA: 0x00123C3B File Offset: 0x00121E3B
			public NodeKeyValueCollection(ListDictionaryInternal list, bool isKeys)
			{
				this.list = list;
				this.isKeys = isKeys;
			}

			// Token: 0x06005A1B RID: 23067 RVA: 0x00123C54 File Offset: 0x00121E54
			void ICollection.CopyTo(Array array, int index)
			{
				if (array == null)
				{
					throw new ArgumentNullException("array");
				}
				if (array.Rank != 1)
				{
					throw new ArgumentException(Environment.GetResourceString("Only single dimensional arrays are supported for the requested action."));
				}
				if (index < 0)
				{
					throw new ArgumentOutOfRangeException("index", Environment.GetResourceString("Non-negative number required."));
				}
				if (array.Length - index < this.list.Count)
				{
					throw new ArgumentException(Environment.GetResourceString("Index was out of range. Must be non-negative and less than the size of the collection."), "index");
				}
				for (ListDictionaryInternal.DictionaryNode dictionaryNode = this.list.head; dictionaryNode != null; dictionaryNode = dictionaryNode.next)
				{
					array.SetValue(this.isKeys ? dictionaryNode.key : dictionaryNode.value, index);
					index++;
				}
			}

			// Token: 0x17001001 RID: 4097
			// (get) Token: 0x06005A1C RID: 23068 RVA: 0x00123D08 File Offset: 0x00121F08
			int ICollection.Count
			{
				get
				{
					int num = 0;
					for (ListDictionaryInternal.DictionaryNode dictionaryNode = this.list.head; dictionaryNode != null; dictionaryNode = dictionaryNode.next)
					{
						num++;
					}
					return num;
				}
			}

			// Token: 0x17001002 RID: 4098
			// (get) Token: 0x06005A1D RID: 23069 RVA: 0x00002526 File Offset: 0x00000726
			bool ICollection.IsSynchronized
			{
				get
				{
					return false;
				}
			}

			// Token: 0x17001003 RID: 4099
			// (get) Token: 0x06005A1E RID: 23070 RVA: 0x00123D34 File Offset: 0x00121F34
			object ICollection.SyncRoot
			{
				get
				{
					return this.list.SyncRoot;
				}
			}

			// Token: 0x06005A1F RID: 23071 RVA: 0x00123D41 File Offset: 0x00121F41
			IEnumerator IEnumerable.GetEnumerator()
			{
				return new ListDictionaryInternal.NodeKeyValueCollection.NodeKeyValueEnumerator(this.list, this.isKeys);
			}

			// Token: 0x04002D91 RID: 11665
			private ListDictionaryInternal list;

			// Token: 0x04002D92 RID: 11666
			private bool isKeys;

			// Token: 0x020009BF RID: 2495
			private class NodeKeyValueEnumerator : IEnumerator
			{
				// Token: 0x06005A20 RID: 23072 RVA: 0x00123D54 File Offset: 0x00121F54
				public NodeKeyValueEnumerator(ListDictionaryInternal list, bool isKeys)
				{
					this.list = list;
					this.isKeys = isKeys;
					this.version = list.version;
					this.start = true;
					this.current = null;
				}

				// Token: 0x17001004 RID: 4100
				// (get) Token: 0x06005A21 RID: 23073 RVA: 0x00123D84 File Offset: 0x00121F84
				public object Current
				{
					get
					{
						if (this.current == null)
						{
							throw new InvalidOperationException(Environment.GetResourceString("Enumeration has either not started or has already finished."));
						}
						if (!this.isKeys)
						{
							return this.current.value;
						}
						return this.current.key;
					}
				}

				// Token: 0x06005A22 RID: 23074 RVA: 0x00123DC0 File Offset: 0x00121FC0
				public bool MoveNext()
				{
					if (this.version != this.list.version)
					{
						throw new InvalidOperationException(Environment.GetResourceString("Collection was modified; enumeration operation may not execute."));
					}
					if (this.start)
					{
						this.current = this.list.head;
						this.start = false;
					}
					else if (this.current != null)
					{
						this.current = this.current.next;
					}
					return this.current != null;
				}

				// Token: 0x06005A23 RID: 23075 RVA: 0x00123E34 File Offset: 0x00122034
				public void Reset()
				{
					if (this.version != this.list.version)
					{
						throw new InvalidOperationException(Environment.GetResourceString("Collection was modified; enumeration operation may not execute."));
					}
					this.start = true;
					this.current = null;
				}

				// Token: 0x04002D93 RID: 11667
				private ListDictionaryInternal list;

				// Token: 0x04002D94 RID: 11668
				private ListDictionaryInternal.DictionaryNode current;

				// Token: 0x04002D95 RID: 11669
				private int version;

				// Token: 0x04002D96 RID: 11670
				private bool isKeys;

				// Token: 0x04002D97 RID: 11671
				private bool start;
			}
		}

		// Token: 0x020009C0 RID: 2496
		[Serializable]
		private class DictionaryNode
		{
			// Token: 0x06005A24 RID: 23076 RVA: 0x00002050 File Offset: 0x00000250
			public DictionaryNode()
			{
			}

			// Token: 0x04002D98 RID: 11672
			public object key;

			// Token: 0x04002D99 RID: 11673
			public object value;

			// Token: 0x04002D9A RID: 11674
			public ListDictionaryInternal.DictionaryNode next;
		}
	}
}
