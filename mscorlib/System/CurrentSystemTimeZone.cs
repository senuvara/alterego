﻿using System;
using System.Globalization;
using System.Runtime.CompilerServices;

namespace System
{
	// Token: 0x0200022F RID: 559
	[Serializable]
	internal class CurrentSystemTimeZone : TimeZone
	{
		// Token: 0x06001ACC RID: 6860 RVA: 0x00065C43 File Offset: 0x00063E43
		internal CurrentSystemTimeZone()
		{
			this.LocalTimeZone = TimeZoneInfo.Local;
		}

		// Token: 0x17000398 RID: 920
		// (get) Token: 0x06001ACD RID: 6861 RVA: 0x00065C56 File Offset: 0x00063E56
		public override string DaylightName
		{
			get
			{
				return this.LocalTimeZone.DaylightName;
			}
		}

		// Token: 0x17000399 RID: 921
		// (get) Token: 0x06001ACE RID: 6862 RVA: 0x00065C63 File Offset: 0x00063E63
		public override string StandardName
		{
			get
			{
				return this.LocalTimeZone.StandardName;
			}
		}

		// Token: 0x06001ACF RID: 6863 RVA: 0x00065C70 File Offset: 0x00063E70
		public override DaylightTime GetDaylightChanges(int year)
		{
			return this.LocalTimeZone.GetDaylightChanges(year);
		}

		// Token: 0x06001AD0 RID: 6864 RVA: 0x00065C7E File Offset: 0x00063E7E
		public override TimeSpan GetUtcOffset(DateTime dateTime)
		{
			if (dateTime.Kind == DateTimeKind.Utc)
			{
				return TimeSpan.Zero;
			}
			return this.LocalTimeZone.GetUtcOffset(dateTime);
		}

		// Token: 0x06001AD1 RID: 6865 RVA: 0x00065C9C File Offset: 0x00063E9C
		public override bool IsDaylightSavingTime(DateTime dateTime)
		{
			return dateTime.Kind != DateTimeKind.Utc && this.LocalTimeZone.IsDaylightSavingTime(dateTime);
		}

		// Token: 0x06001AD2 RID: 6866
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern bool GetTimeZoneData(int year, out long[] data, out string[] names, out bool daylight_inverted);

		// Token: 0x04000F07 RID: 3847
		private readonly TimeZoneInfo LocalTimeZone;
	}
}
