﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;

namespace System
{
	/// <summary>Represents an n-tuple, where n is 8 or greater.</summary>
	/// <typeparam name="T1">The type of the tuple's first component.</typeparam>
	/// <typeparam name="T2">The type of the tuple's second component.</typeparam>
	/// <typeparam name="T3">The type of the tuple's third component.</typeparam>
	/// <typeparam name="T4">The type of the tuple's fourth component.</typeparam>
	/// <typeparam name="T5">The type of the tuple's fifth component.</typeparam>
	/// <typeparam name="T6">The type of the tuple's sixth component.</typeparam>
	/// <typeparam name="T7">The type of the tuple's seventh component.</typeparam>
	/// <typeparam name="TRest">Any generic <see langword="Tuple" /> object that defines the types of the tuple's remaining components.</typeparam>
	// Token: 0x020000E0 RID: 224
	[Serializable]
	public class Tuple<T1, T2, T3, T4, T5, T6, T7, TRest> : IStructuralEquatable, IStructuralComparable, IComparable, ITupleInternal, ITuple
	{
		/// <summary>Gets the value of the current <see cref="T:System.Tuple`8" /> object's first component.</summary>
		/// <returns>The value of the current <see cref="T:System.Tuple`8" /> object's first component.</returns>
		// Token: 0x17000175 RID: 373
		// (get) Token: 0x060008F3 RID: 2291 RVA: 0x0002FDA6 File Offset: 0x0002DFA6
		public T1 Item1
		{
			get
			{
				return this.m_Item1;
			}
		}

		/// <summary>Gets the value of the current <see cref="T:System.Tuple`8" /> object's second component.</summary>
		/// <returns>The value of the current <see cref="T:System.Tuple`8" /> object's second component.</returns>
		// Token: 0x17000176 RID: 374
		// (get) Token: 0x060008F4 RID: 2292 RVA: 0x0002FDAE File Offset: 0x0002DFAE
		public T2 Item2
		{
			get
			{
				return this.m_Item2;
			}
		}

		/// <summary>Gets the value of the current <see cref="T:System.Tuple`8" /> object's third component.</summary>
		/// <returns>The value of the current <see cref="T:System.Tuple`8" /> object's third component.</returns>
		// Token: 0x17000177 RID: 375
		// (get) Token: 0x060008F5 RID: 2293 RVA: 0x0002FDB6 File Offset: 0x0002DFB6
		public T3 Item3
		{
			get
			{
				return this.m_Item3;
			}
		}

		/// <summary>Gets the value of the current <see cref="T:System.Tuple`8" /> object's fourth component.</summary>
		/// <returns>The value of the current <see cref="T:System.Tuple`8" /> object's fourth component.</returns>
		// Token: 0x17000178 RID: 376
		// (get) Token: 0x060008F6 RID: 2294 RVA: 0x0002FDBE File Offset: 0x0002DFBE
		public T4 Item4
		{
			get
			{
				return this.m_Item4;
			}
		}

		/// <summary>Gets the value of the current <see cref="T:System.Tuple`8" /> object's fifth component.</summary>
		/// <returns>The value of the current <see cref="T:System.Tuple`8" /> object's fifth component.</returns>
		// Token: 0x17000179 RID: 377
		// (get) Token: 0x060008F7 RID: 2295 RVA: 0x0002FDC6 File Offset: 0x0002DFC6
		public T5 Item5
		{
			get
			{
				return this.m_Item5;
			}
		}

		/// <summary>Gets the value of the current <see cref="T:System.Tuple`8" /> object's sixth component.</summary>
		/// <returns>The value of the current <see cref="T:System.Tuple`8" /> object's sixth component.</returns>
		// Token: 0x1700017A RID: 378
		// (get) Token: 0x060008F8 RID: 2296 RVA: 0x0002FDCE File Offset: 0x0002DFCE
		public T6 Item6
		{
			get
			{
				return this.m_Item6;
			}
		}

		/// <summary>Gets the value of the current <see cref="T:System.Tuple`8" /> object's seventh component.</summary>
		/// <returns>The value of the current <see cref="T:System.Tuple`8" /> object's seventh component.</returns>
		// Token: 0x1700017B RID: 379
		// (get) Token: 0x060008F9 RID: 2297 RVA: 0x0002FDD6 File Offset: 0x0002DFD6
		public T7 Item7
		{
			get
			{
				return this.m_Item7;
			}
		}

		/// <summary>Gets the current <see cref="T:System.Tuple`8" /> object's remaining components.</summary>
		/// <returns>The value of the current <see cref="T:System.Tuple`8" /> object's remaining components.</returns>
		// Token: 0x1700017C RID: 380
		// (get) Token: 0x060008FA RID: 2298 RVA: 0x0002FDDE File Offset: 0x0002DFDE
		public TRest Rest
		{
			get
			{
				return this.m_Rest;
			}
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Tuple`8" /> class.</summary>
		/// <param name="item1">The value of the tuple's first component.</param>
		/// <param name="item2">The value of the tuple's second component.</param>
		/// <param name="item3">The value of the tuple's third component.</param>
		/// <param name="item4">The value of the tuple's fourth component</param>
		/// <param name="item5">The value of the tuple's fifth component.</param>
		/// <param name="item6">The value of the tuple's sixth component.</param>
		/// <param name="item7">The value of the tuple's seventh component.</param>
		/// <param name="rest">Any generic <see langword="Tuple" /> object that contains the values of the tuple's remaining components.</param>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="rest" /> is not a generic <see langword="Tuple" /> object.</exception>
		// Token: 0x060008FB RID: 2299 RVA: 0x0002FDE8 File Offset: 0x0002DFE8
		public Tuple(T1 item1, T2 item2, T3 item3, T4 item4, T5 item5, T6 item6, T7 item7, TRest rest)
		{
			if (!(rest is ITupleInternal))
			{
				throw new ArgumentException("The last element of an eight element tuple must be a Tuple.");
			}
			this.m_Item1 = item1;
			this.m_Item2 = item2;
			this.m_Item3 = item3;
			this.m_Item4 = item4;
			this.m_Item5 = item5;
			this.m_Item6 = item6;
			this.m_Item7 = item7;
			this.m_Rest = rest;
		}

		/// <summary>Returns a value that indicates whether the current <see cref="T:System.Tuple`8" /> object is equal to a specified object.</summary>
		/// <param name="obj">The object to compare with this instance.</param>
		/// <returns>
		///     <see langword="true" /> if the current instance is equal to the specified object; otherwise, <see langword="false" />.</returns>
		// Token: 0x060008FC RID: 2300 RVA: 0x0002E8E2 File Offset: 0x0002CAE2
		public override bool Equals(object obj)
		{
			return ((IStructuralEquatable)this).Equals(obj, ObjectEqualityComparer.Default);
		}

		/// <summary>Returns a value that indicates whether the current <see cref="T:System.Tuple`8" /> object is equal to a specified object based on a specified comparison method.</summary>
		/// <param name="other">The object to compare with this instance.</param>
		/// <param name="comparer">An object that defines the method to use to evaluate whether the two objects are equal.</param>
		/// <returns>
		///     <see langword="true" /> if the current instance is equal to the specified object; otherwise, <see langword="false" />.</returns>
		// Token: 0x060008FD RID: 2301 RVA: 0x0002FE54 File Offset: 0x0002E054
		bool IStructuralEquatable.Equals(object other, IEqualityComparer comparer)
		{
			if (other == null)
			{
				return false;
			}
			Tuple<T1, T2, T3, T4, T5, T6, T7, TRest> tuple = other as Tuple<T1, T2, T3, T4, T5, T6, T7, TRest>;
			return tuple != null && (comparer.Equals(this.m_Item1, tuple.m_Item1) && comparer.Equals(this.m_Item2, tuple.m_Item2) && comparer.Equals(this.m_Item3, tuple.m_Item3) && comparer.Equals(this.m_Item4, tuple.m_Item4) && comparer.Equals(this.m_Item5, tuple.m_Item5) && comparer.Equals(this.m_Item6, tuple.m_Item6) && comparer.Equals(this.m_Item7, tuple.m_Item7)) && comparer.Equals(this.m_Rest, tuple.m_Rest);
		}

		/// <summary>Compares the current <see cref="T:System.Tuple`8" /> object to a specified object and returns an integer that indicates whether the current object is before, after, or in the same position as the specified object in the sort order.</summary>
		/// <param name="obj">An object to compare with the current instance.</param>
		/// <returns>A signed integer that indicates the relative position of this instance and <paramref name="obj" /> in the sort order, as shown in the following table.ValueDescriptionA negative integerThis instance precedes <paramref name="obj" />.ZeroThis instance and <paramref name="obj" /> have the same position in the sort order.A positive integerThis instance follows <paramref name="obj" />.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="obj" /> is not a <see cref="T:System.Tuple`8" /> object.</exception>
		// Token: 0x060008FE RID: 2302 RVA: 0x0002E92A File Offset: 0x0002CB2A
		int IComparable.CompareTo(object obj)
		{
			return ((IStructuralComparable)this).CompareTo(obj, LowLevelComparer.Default);
		}

		/// <summary>Compares the current <see cref="T:System.Tuple`8" /> object to a specified object by using a specified comparer and returns an integer that indicates whether the current object is before, after, or in the same position as the specified object in the sort order.</summary>
		/// <param name="other">An object to compare with the current instance.</param>
		/// <param name="comparer">An object that provides custom rules for comparison.</param>
		/// <returns>A signed integer that indicates the relative position of this instance and <paramref name="other" /> in the sort order, as shown in the following table.ValueDescriptionA negative integerThis instance precedes <paramref name="other" />.ZeroThis instance and <paramref name="other" /> have the same position in the sort order.A positive integerThis instance follows <paramref name="other" />.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="other" /> is not a <see cref="T:System.Tuple`8" /> object.</exception>
		// Token: 0x060008FF RID: 2303 RVA: 0x0002FF6C File Offset: 0x0002E16C
		int IStructuralComparable.CompareTo(object other, IComparer comparer)
		{
			if (other == null)
			{
				return 1;
			}
			Tuple<T1, T2, T3, T4, T5, T6, T7, TRest> tuple = other as Tuple<T1, T2, T3, T4, T5, T6, T7, TRest>;
			if (tuple == null)
			{
				throw new ArgumentException(SR.Format("Argument must be of type {0}.", base.GetType().ToString()), "other");
			}
			int num = comparer.Compare(this.m_Item1, tuple.m_Item1);
			if (num != 0)
			{
				return num;
			}
			num = comparer.Compare(this.m_Item2, tuple.m_Item2);
			if (num != 0)
			{
				return num;
			}
			num = comparer.Compare(this.m_Item3, tuple.m_Item3);
			if (num != 0)
			{
				return num;
			}
			num = comparer.Compare(this.m_Item4, tuple.m_Item4);
			if (num != 0)
			{
				return num;
			}
			num = comparer.Compare(this.m_Item5, tuple.m_Item5);
			if (num != 0)
			{
				return num;
			}
			num = comparer.Compare(this.m_Item6, tuple.m_Item6);
			if (num != 0)
			{
				return num;
			}
			num = comparer.Compare(this.m_Item7, tuple.m_Item7);
			if (num != 0)
			{
				return num;
			}
			return comparer.Compare(this.m_Rest, tuple.m_Rest);
		}

		/// <summary>Calculates the hash code for the current <see cref="T:System.Tuple`8" /> object.</summary>
		/// <returns>A 32-bit signed integer hash code.</returns>
		// Token: 0x06000900 RID: 2304 RVA: 0x0002E990 File Offset: 0x0002CB90
		public override int GetHashCode()
		{
			return ((IStructuralEquatable)this).GetHashCode(ObjectEqualityComparer.Default);
		}

		/// <summary>Calculates the hash code for the current <see cref="T:System.Tuple`8" /> object by using a specified computation method.</summary>
		/// <param name="comparer">An object whose <see cref="M:System.Collections.IEqualityComparer.GetHashCode(System.Object)" />  method calculates the hash code of the current <see cref="T:System.Tuple`8" /> object.</param>
		/// <returns>A 32-bit signed integer hash code.</returns>
		// Token: 0x06000901 RID: 2305 RVA: 0x000300B4 File Offset: 0x0002E2B4
		int IStructuralEquatable.GetHashCode(IEqualityComparer comparer)
		{
			ITupleInternal tupleInternal = (ITupleInternal)((object)this.m_Rest);
			if (tupleInternal.Length >= 8)
			{
				return tupleInternal.GetHashCode(comparer);
			}
			switch (8 - tupleInternal.Length)
			{
			case 1:
				return Tuple.CombineHashCodes(comparer.GetHashCode(this.m_Item7), tupleInternal.GetHashCode(comparer));
			case 2:
				return Tuple.CombineHashCodes(comparer.GetHashCode(this.m_Item6), comparer.GetHashCode(this.m_Item7), tupleInternal.GetHashCode(comparer));
			case 3:
				return Tuple.CombineHashCodes(comparer.GetHashCode(this.m_Item5), comparer.GetHashCode(this.m_Item6), comparer.GetHashCode(this.m_Item7), tupleInternal.GetHashCode(comparer));
			case 4:
				return Tuple.CombineHashCodes(comparer.GetHashCode(this.m_Item4), comparer.GetHashCode(this.m_Item5), comparer.GetHashCode(this.m_Item6), comparer.GetHashCode(this.m_Item7), tupleInternal.GetHashCode(comparer));
			case 5:
				return Tuple.CombineHashCodes(comparer.GetHashCode(this.m_Item3), comparer.GetHashCode(this.m_Item4), comparer.GetHashCode(this.m_Item5), comparer.GetHashCode(this.m_Item6), comparer.GetHashCode(this.m_Item7), tupleInternal.GetHashCode(comparer));
			case 6:
				return Tuple.CombineHashCodes(comparer.GetHashCode(this.m_Item2), comparer.GetHashCode(this.m_Item3), comparer.GetHashCode(this.m_Item4), comparer.GetHashCode(this.m_Item5), comparer.GetHashCode(this.m_Item6), comparer.GetHashCode(this.m_Item7), tupleInternal.GetHashCode(comparer));
			case 7:
				return Tuple.CombineHashCodes(comparer.GetHashCode(this.m_Item1), comparer.GetHashCode(this.m_Item2), comparer.GetHashCode(this.m_Item3), comparer.GetHashCode(this.m_Item4), comparer.GetHashCode(this.m_Item5), comparer.GetHashCode(this.m_Item6), comparer.GetHashCode(this.m_Item7), tupleInternal.GetHashCode(comparer));
			default:
				return -1;
			}
		}

		// Token: 0x06000902 RID: 2306 RVA: 0x0002E9B0 File Offset: 0x0002CBB0
		int ITupleInternal.GetHashCode(IEqualityComparer comparer)
		{
			return ((IStructuralEquatable)this).GetHashCode(comparer);
		}

		/// <summary>Returns a string that represents the value of this <see cref="T:System.Tuple`8" /> instance.</summary>
		/// <returns>The string representation of this <see cref="T:System.Tuple`8" /> object.</returns>
		// Token: 0x06000903 RID: 2307 RVA: 0x00030350 File Offset: 0x0002E550
		public override string ToString()
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("(");
			return ((ITupleInternal)this).ToString(stringBuilder);
		}

		// Token: 0x06000904 RID: 2308 RVA: 0x00030378 File Offset: 0x0002E578
		string ITupleInternal.ToString(StringBuilder sb)
		{
			sb.Append(this.m_Item1);
			sb.Append(", ");
			sb.Append(this.m_Item2);
			sb.Append(", ");
			sb.Append(this.m_Item3);
			sb.Append(", ");
			sb.Append(this.m_Item4);
			sb.Append(", ");
			sb.Append(this.m_Item5);
			sb.Append(", ");
			sb.Append(this.m_Item6);
			sb.Append(", ");
			sb.Append(this.m_Item7);
			sb.Append(", ");
			return ((ITupleInternal)((object)this.m_Rest)).ToString(sb);
		}

		/// <summary>Gets the number of elements in the <see langword="Tuple" />.</summary>
		/// <returns>The number of elements in the <see langword="Tuple" />. </returns>
		// Token: 0x1700017D RID: 381
		// (get) Token: 0x06000905 RID: 2309 RVA: 0x0003046D File Offset: 0x0002E66D
		int ITuple.Length
		{
			get
			{
				return 7 + ((ITupleInternal)((object)this.Rest)).Length;
			}
		}

		/// <summary>Gets the value of the specified <see langword="Tuple" /> element. </summary>
		/// <param name="index">The index of the specified <see langword="Tuple" /> element. <paramref name="index" /> can range from 0 for <see langword="Item1" /> to one less than the number of elements in the <see langword="Tuple" />. </param>
		/// <returns>The value of the <see langword="Tuple" /> element at the specified position.</returns>
		/// <exception cref="T:System.IndexOutOfRangeException">
		///   <paramref name="index" /> is less than 0. -or-<paramref name="index" /> is greater than or equal to <see cref="P:System.Tuple`8.System#Runtime#CompilerServices#ITuple#Length" />. </exception>
		// Token: 0x1700017E RID: 382
		object ITuple.this[int index]
		{
			get
			{
				switch (index)
				{
				case 0:
					return this.Item1;
				case 1:
					return this.Item2;
				case 2:
					return this.Item3;
				case 3:
					return this.Item4;
				case 4:
					return this.Item5;
				case 5:
					return this.Item6;
				case 6:
					return this.Item7;
				default:
					return ((ITupleInternal)((object)this.Rest))[index - 7];
				}
			}
		}

		// Token: 0x040006C4 RID: 1732
		private readonly T1 m_Item1;

		// Token: 0x040006C5 RID: 1733
		private readonly T2 m_Item2;

		// Token: 0x040006C6 RID: 1734
		private readonly T3 m_Item3;

		// Token: 0x040006C7 RID: 1735
		private readonly T4 m_Item4;

		// Token: 0x040006C8 RID: 1736
		private readonly T5 m_Item5;

		// Token: 0x040006C9 RID: 1737
		private readonly T6 m_Item6;

		// Token: 0x040006CA RID: 1738
		private readonly T7 m_Item7;

		// Token: 0x040006CB RID: 1739
		private readonly TRest m_Rest;
	}
}
