﻿using System;

namespace System
{
	// Token: 0x020000E2 RID: 226
	[AttributeUsage(AttributeTargets.All, AllowMultiple = true)]
	internal class MonoDocumentationNoteAttribute : MonoTODOAttribute
	{
		// Token: 0x0600090A RID: 2314 RVA: 0x0003053C File Offset: 0x0002E73C
		public MonoDocumentationNoteAttribute(string comment) : base(comment)
		{
		}
	}
}
