﻿using System;

namespace System
{
	// Token: 0x020001BD RID: 445
	[Flags]
	internal enum TimeZoneInfoOptions
	{
		// Token: 0x04000B1B RID: 2843
		None = 1,
		// Token: 0x04000B1C RID: 2844
		NoThrowOnInvalidTime = 2
	}
}
