﻿using System;
using System.Reflection;
using System.Runtime.Serialization;

namespace System
{
	// Token: 0x020001FD RID: 509
	[Serializable]
	internal class DelegateSerializationHolder : ISerializable, IObjectReference
	{
		// Token: 0x060018C8 RID: 6344 RVA: 0x0005DD40 File Offset: 0x0005BF40
		private DelegateSerializationHolder(SerializationInfo info, StreamingContext ctx)
		{
			DelegateSerializationHolder.DelegateEntry delegateEntry = (DelegateSerializationHolder.DelegateEntry)info.GetValue("Delegate", typeof(DelegateSerializationHolder.DelegateEntry));
			int num = 0;
			DelegateSerializationHolder.DelegateEntry delegateEntry2 = delegateEntry;
			while (delegateEntry2 != null)
			{
				delegateEntry2 = delegateEntry2.delegateEntry;
				num++;
			}
			if (num == 1)
			{
				this._delegate = delegateEntry.DeserializeDelegate(info, 0);
				return;
			}
			Delegate[] array = new Delegate[num];
			delegateEntry2 = delegateEntry;
			for (int i = 0; i < num; i++)
			{
				array[i] = delegateEntry2.DeserializeDelegate(info, i);
				delegateEntry2 = delegateEntry2.delegateEntry;
			}
			this._delegate = Delegate.Combine(array);
		}

		// Token: 0x060018C9 RID: 6345 RVA: 0x0005DDD0 File Offset: 0x0005BFD0
		public static void GetDelegateData(Delegate instance, SerializationInfo info, StreamingContext ctx)
		{
			Delegate[] invocationList = instance.GetInvocationList();
			DelegateSerializationHolder.DelegateEntry delegateEntry = null;
			for (int i = 0; i < invocationList.Length; i++)
			{
				Delegate @delegate = invocationList[i];
				string text = (@delegate.Target != null) ? ("target" + i) : null;
				DelegateSerializationHolder.DelegateEntry delegateEntry2 = new DelegateSerializationHolder.DelegateEntry(@delegate, text);
				if (delegateEntry == null)
				{
					info.AddValue("Delegate", delegateEntry2);
				}
				else
				{
					delegateEntry.delegateEntry = delegateEntry2;
				}
				delegateEntry = delegateEntry2;
				if (@delegate.Target != null)
				{
					info.AddValue(text, @delegate.Target);
				}
				info.AddValue("method" + i, @delegate.Method);
			}
			info.SetType(typeof(DelegateSerializationHolder));
		}

		// Token: 0x060018CA RID: 6346 RVA: 0x000175EA File Offset: 0x000157EA
		public void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			throw new NotSupportedException();
		}

		// Token: 0x060018CB RID: 6347 RVA: 0x0005DE81 File Offset: 0x0005C081
		public object GetRealObject(StreamingContext context)
		{
			return this._delegate;
		}

		// Token: 0x04000C6E RID: 3182
		private Delegate _delegate;

		// Token: 0x020001FE RID: 510
		[Serializable]
		private class DelegateEntry
		{
			// Token: 0x060018CC RID: 6348 RVA: 0x0005DE8C File Offset: 0x0005C08C
			public DelegateEntry(Delegate del, string targetLabel)
			{
				this.type = del.GetType().FullName;
				this.assembly = del.GetType().Assembly.FullName;
				this.target = targetLabel;
				this.targetTypeAssembly = del.Method.DeclaringType.Assembly.FullName;
				this.targetTypeName = del.Method.DeclaringType.FullName;
				this.methodName = del.Method.Name;
			}

			// Token: 0x060018CD RID: 6349 RVA: 0x0005DF10 File Offset: 0x0005C110
			public Delegate DeserializeDelegate(SerializationInfo info, int index)
			{
				object obj = null;
				if (this.target != null)
				{
					obj = info.GetValue(this.target.ToString(), typeof(object));
				}
				string name = "method" + index;
				MethodInfo methodInfo = (MethodInfo)info.GetValueNoThrow(name, typeof(MethodInfo));
				Type type = Assembly.Load(this.assembly).GetType(this.type);
				if (obj != null)
				{
					if (!(methodInfo == null))
					{
						return Delegate.CreateDelegate(type, obj, methodInfo);
					}
					return Delegate.CreateDelegate(type, obj, this.methodName);
				}
				else
				{
					if (methodInfo != null)
					{
						return Delegate.CreateDelegate(type, obj, methodInfo);
					}
					Type type2 = Assembly.Load(this.targetTypeAssembly).GetType(this.targetTypeName);
					return Delegate.CreateDelegate(type, type2, this.methodName);
				}
			}

			// Token: 0x04000C6F RID: 3183
			private string type;

			// Token: 0x04000C70 RID: 3184
			private string assembly;

			// Token: 0x04000C71 RID: 3185
			private object target;

			// Token: 0x04000C72 RID: 3186
			private string targetTypeAssembly;

			// Token: 0x04000C73 RID: 3187
			private string targetTypeName;

			// Token: 0x04000C74 RID: 3188
			private string methodName;

			// Token: 0x04000C75 RID: 3189
			public DelegateSerializationHolder.DelegateEntry delegateEntry;
		}
	}
}
