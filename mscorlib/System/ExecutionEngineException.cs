﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System
{
	/// <summary>The exception that is thrown when there is an internal error in the execution engine of the common language runtime. This class cannot be inherited.  </summary>
	// Token: 0x0200014E RID: 334
	[Obsolete("This type previously indicated an unspecified fatal error in the runtime. The runtime no longer raises this exception so this type is obsolete.")]
	[ComVisible(true)]
	[Serializable]
	public sealed class ExecutionEngineException : SystemException
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.ExecutionEngineException" /> class.</summary>
		// Token: 0x06000EEB RID: 3819 RVA: 0x0003E388 File Offset: 0x0003C588
		public ExecutionEngineException() : base(Environment.GetResourceString("Internal error in the runtime."))
		{
			base.SetErrorCode(-2146233082);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ExecutionEngineException" /> class with a specified error message.</summary>
		/// <param name="message">The message that describes the error. </param>
		// Token: 0x06000EEC RID: 3820 RVA: 0x0003E3A5 File Offset: 0x0003C5A5
		public ExecutionEngineException(string message) : base(message)
		{
			base.SetErrorCode(-2146233082);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ExecutionEngineException" /> class with a specified error message and a reference to the inner exception that is the cause of this exception.</summary>
		/// <param name="message">The error message that explains the reason for the exception. </param>
		/// <param name="innerException">The exception that is the cause of the current exception. If the <paramref name="innerException" /> parameter is not a null reference (<see langword="Nothing" /> in Visual Basic), the current exception is raised in a <see langword="catch" /> block that handles the inner exception. </param>
		// Token: 0x06000EED RID: 3821 RVA: 0x0003E3B9 File Offset: 0x0003C5B9
		public ExecutionEngineException(string message, Exception innerException) : base(message, innerException)
		{
			base.SetErrorCode(-2146233082);
		}

		// Token: 0x06000EEE RID: 3822 RVA: 0x000319C9 File Offset: 0x0002FBC9
		internal ExecutionEngineException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
}
