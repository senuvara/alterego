﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System
{
	/// <summary>The exception that is thrown when there is an attempt to divide an integral or <see cref="T:System.Decimal" /> value by zero.</summary>
	// Token: 0x0200013F RID: 319
	[ComVisible(true)]
	[Serializable]
	public class DivideByZeroException : ArithmeticException
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.DivideByZeroException" /> class.</summary>
		// Token: 0x06000E2D RID: 3629 RVA: 0x0003C3EA File Offset: 0x0003A5EA
		public DivideByZeroException() : base(Environment.GetResourceString("Attempted to divide by zero."))
		{
			base.SetErrorCode(-2147352558);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.DivideByZeroException" /> class with a specified error message.</summary>
		/// <param name="message">A <see cref="T:System.String" /> that describes the error. </param>
		// Token: 0x06000E2E RID: 3630 RVA: 0x0003C407 File Offset: 0x0003A607
		public DivideByZeroException(string message) : base(message)
		{
			base.SetErrorCode(-2147352558);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.DivideByZeroException" /> class with a specified error message and a reference to the inner exception that is the cause of this exception.</summary>
		/// <param name="message">The error message that explains the reason for the exception. </param>
		/// <param name="innerException">The exception that is the cause of the current exception. If the <paramref name="innerException" /> parameter is not <see langword="null" />, the current exception is raised in a <see langword="catch" /> block that handles the inner exception. </param>
		// Token: 0x06000E2F RID: 3631 RVA: 0x0003C41B File Offset: 0x0003A61B
		public DivideByZeroException(string message, Exception innerException) : base(message, innerException)
		{
			base.SetErrorCode(-2147352558);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.DivideByZeroException" /> class with serialized data.</summary>
		/// <param name="info">The object that holds the serialized object data. </param>
		/// <param name="context">The contextual information about the source or destination. </param>
		// Token: 0x06000E30 RID: 3632 RVA: 0x0003C430 File Offset: 0x0003A630
		protected DivideByZeroException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
}
