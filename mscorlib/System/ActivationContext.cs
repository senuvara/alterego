﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using Unity;

namespace System
{
	/// <summary>Identifies the activation context for the current application. This class cannot be inherited. </summary>
	// Token: 0x020001EB RID: 491
	[ComVisible(false)]
	[Serializable]
	public sealed class ActivationContext : IDisposable, ISerializable
	{
		// Token: 0x06001770 RID: 6000 RVA: 0x0005BCCE File Offset: 0x00059ECE
		private ActivationContext(ApplicationIdentity identity)
		{
			this._appid = identity;
		}

		/// <summary>Enables an <see cref="T:System.ActivationContext" /> object to attempt to free resources and perform other cleanup operations before the <see cref="T:System.ActivationContext" /> is reclaimed by garbage collection.</summary>
		// Token: 0x06001771 RID: 6001 RVA: 0x0005BCE0 File Offset: 0x00059EE0
		~ActivationContext()
		{
			this.Dispose(false);
		}

		/// <summary>Gets the form, or store context, for the current application. </summary>
		/// <returns>One of the enumeration values. </returns>
		// Token: 0x170002E2 RID: 738
		// (get) Token: 0x06001772 RID: 6002 RVA: 0x0005BD10 File Offset: 0x00059F10
		public ActivationContext.ContextForm Form
		{
			get
			{
				return this._form;
			}
		}

		/// <summary>Gets the application identity for the current application.</summary>
		/// <returns>An <see cref="T:System.ApplicationIdentity" /> object that identifies the current application.</returns>
		// Token: 0x170002E3 RID: 739
		// (get) Token: 0x06001773 RID: 6003 RVA: 0x0005BD18 File Offset: 0x00059F18
		public ApplicationIdentity Identity
		{
			get
			{
				return this._appid;
			}
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ActivationContext" /> class using the specified application identity.</summary>
		/// <param name="identity">An object that identifies an application.</param>
		/// <returns>An object with the specified application identity.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="identity" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.ArgumentException">No deployment or application identity is specified in <paramref name="identity" />.</exception>
		// Token: 0x06001774 RID: 6004 RVA: 0x0005BD20 File Offset: 0x00059F20
		[MonoTODO("Missing validation")]
		public static ActivationContext CreatePartialActivationContext(ApplicationIdentity identity)
		{
			if (identity == null)
			{
				throw new ArgumentNullException("identity");
			}
			return new ActivationContext(identity);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ActivationContext" /> class using the specified application identity and array of manifest paths.</summary>
		/// <param name="identity">An object that identifies an application.</param>
		/// <param name="manifestPaths">A string array of manifest paths for the application.</param>
		/// <returns>An object with the specified application identity and array of manifest paths.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="identity" /> is <see langword="null" />. -or-
		///         <paramref name="manifestPaths" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.ArgumentException">No deployment or application identity is specified in <paramref name="identity" />.-or-
		///         <paramref name="identity" /> does not match the identity in the manifests.-or-
		///         <paramref name="identity" /> does not have the same number of components as the manifest paths.</exception>
		// Token: 0x06001775 RID: 6005 RVA: 0x0005BD36 File Offset: 0x00059F36
		[MonoTODO("Missing validation")]
		public static ActivationContext CreatePartialActivationContext(ApplicationIdentity identity, string[] manifestPaths)
		{
			if (identity == null)
			{
				throw new ArgumentNullException("identity");
			}
			if (manifestPaths == null)
			{
				throw new ArgumentNullException("manifestPaths");
			}
			return new ActivationContext(identity);
		}

		/// <summary>Releases all resources used by the <see cref="T:System.ActivationContext" />. </summary>
		// Token: 0x06001776 RID: 6006 RVA: 0x0005BD5A File Offset: 0x00059F5A
		public void Dispose()
		{
			this.Dispose(true);
			GC.SuppressFinalize(this);
		}

		// Token: 0x06001777 RID: 6007 RVA: 0x0005BD69 File Offset: 0x00059F69
		private void Dispose(bool disposing)
		{
			if (this._disposed)
			{
				this._disposed = true;
			}
		}

		/// <summary>Populates a <see cref="T:System.Runtime.Serialization.SerializationInfo" /> with the data needed to serialize the target object.</summary>
		/// <param name="info">The object to populate with data.</param>
		/// <param name="context">The structure for this serialization.</param>
		// Token: 0x06001778 RID: 6008 RVA: 0x0005BD7C File Offset: 0x00059F7C
		[MonoTODO("Missing serialization support")]
		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			if (info == null)
			{
				throw new ArgumentNullException("info");
			}
		}

		// Token: 0x06001779 RID: 6009 RVA: 0x00002ABD File Offset: 0x00000CBD
		internal ActivationContext()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the ClickOnce application manifest for the current application.</summary>
		/// <returns>A byte array that contains the ClickOnce application manifest for the application that is associated with this <see cref="T:System.ActivationContext" />.</returns>
		// Token: 0x170002E4 RID: 740
		// (get) Token: 0x0600177A RID: 6010 RVA: 0x0005AB11 File Offset: 0x00058D11
		public byte[] ApplicationManifestBytes
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the ClickOnce deployment manifest for the current application.</summary>
		/// <returns>A byte array that contains the ClickOnce deployment manifest for the application that is associated with this <see cref="T:System.ActivationContext" />.</returns>
		// Token: 0x170002E5 RID: 741
		// (get) Token: 0x0600177B RID: 6011 RVA: 0x0005AB11 File Offset: 0x00058D11
		public byte[] DeploymentManifestBytes
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		// Token: 0x04000C1D RID: 3101
		private ApplicationIdentity _appid;

		// Token: 0x04000C1E RID: 3102
		private ActivationContext.ContextForm _form;

		// Token: 0x04000C1F RID: 3103
		private bool _disposed;

		/// <summary>Indicates the context for a manifest-activated application.</summary>
		// Token: 0x020001EC RID: 492
		public enum ContextForm
		{
			/// <summary>The application is not in the ClickOnce store.</summary>
			// Token: 0x04000C21 RID: 3105
			Loose,
			/// <summary>The application is contained in the ClickOnce store.</summary>
			// Token: 0x04000C22 RID: 3106
			StoreBounded
		}
	}
}
