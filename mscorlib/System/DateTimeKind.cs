﻿using System;
using System.Runtime.InteropServices;

namespace System
{
	/// <summary>Specifies whether a <see cref="T:System.DateTime" /> object represents a local time, a Coordinated Universal Time (UTC), or is not specified as either local time or UTC.</summary>
	// Token: 0x02000137 RID: 311
	[ComVisible(true)]
	[Serializable]
	public enum DateTimeKind
	{
		/// <summary>The time represented is not specified as either local time or Coordinated Universal Time (UTC).</summary>
		// Token: 0x04000883 RID: 2179
		Unspecified,
		/// <summary>The time represented is UTC.</summary>
		// Token: 0x04000884 RID: 2180
		Utc,
		/// <summary>The time represented is local time.</summary>
		// Token: 0x04000885 RID: 2181
		Local
	}
}
