﻿using System;

namespace System
{
	// Token: 0x02000165 RID: 357
	[Flags]
	internal enum ParseFlags
	{
		// Token: 0x04000977 RID: 2423
		HaveYear = 1,
		// Token: 0x04000978 RID: 2424
		HaveMonth = 2,
		// Token: 0x04000979 RID: 2425
		HaveDay = 4,
		// Token: 0x0400097A RID: 2426
		HaveHour = 8,
		// Token: 0x0400097B RID: 2427
		HaveMinute = 16,
		// Token: 0x0400097C RID: 2428
		HaveSecond = 32,
		// Token: 0x0400097D RID: 2429
		HaveTime = 64,
		// Token: 0x0400097E RID: 2430
		HaveDate = 128,
		// Token: 0x0400097F RID: 2431
		TimeZoneUsed = 256,
		// Token: 0x04000980 RID: 2432
		TimeZoneUtc = 512,
		// Token: 0x04000981 RID: 2433
		ParsedMonthName = 1024,
		// Token: 0x04000982 RID: 2434
		CaptureOffset = 2048,
		// Token: 0x04000983 RID: 2435
		YearDefault = 4096,
		// Token: 0x04000984 RID: 2436
		Rfc1123Pattern = 8192,
		// Token: 0x04000985 RID: 2437
		UtcSortPattern = 16384
	}
}
