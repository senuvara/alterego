﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.CompilerServices;
using System.Runtime.Hosting;
using System.Runtime.InteropServices;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security;
using System.Security.Policy;
using Unity;

namespace System
{
	/// <summary>Represents assembly binding information that can be added to an instance of <see cref="T:System.AppDomain" />.</summary>
	// Token: 0x020001EF RID: 495
	[ClassInterface(ClassInterfaceType.None)]
	[ComVisible(true)]
	[Serializable]
	[StructLayout(LayoutKind.Sequential)]
	public sealed class AppDomainSetup : IAppDomainSetup
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.AppDomainSetup" /> class.</summary>
		// Token: 0x0600178B RID: 6027 RVA: 0x00002050 File Offset: 0x00000250
		public AppDomainSetup()
		{
		}

		// Token: 0x0600178C RID: 6028 RVA: 0x0005BE20 File Offset: 0x0005A020
		internal AppDomainSetup(AppDomainSetup setup)
		{
			this.application_base = setup.application_base;
			this.application_name = setup.application_name;
			this.cache_path = setup.cache_path;
			this.configuration_file = setup.configuration_file;
			this.dynamic_base = setup.dynamic_base;
			this.license_file = setup.license_file;
			this.private_bin_path = setup.private_bin_path;
			this.private_bin_path_probe = setup.private_bin_path_probe;
			this.shadow_copy_directories = setup.shadow_copy_directories;
			this.shadow_copy_files = setup.shadow_copy_files;
			this.publisher_policy = setup.publisher_policy;
			this.path_changed = setup.path_changed;
			this.loader_optimization = setup.loader_optimization;
			this.disallow_binding_redirects = setup.disallow_binding_redirects;
			this.disallow_code_downloads = setup.disallow_code_downloads;
			this._activationArguments = setup._activationArguments;
			this.domain_initializer = setup.domain_initializer;
			this.application_trust = setup.application_trust;
			this.domain_initializer_args = setup.domain_initializer_args;
			this.disallow_appbase_probe = setup.disallow_appbase_probe;
			this.configuration_bytes = setup.configuration_bytes;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.AppDomainSetup" /> class with the specified activation arguments required for manifest-based activation of an application domain.</summary>
		/// <param name="activationArguments">An object that specifies information required for the manifest-based activation of a new application domain.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="activationArguments" /> is <see langword="null" />.</exception>
		// Token: 0x0600178D RID: 6029 RVA: 0x0005BF2F File Offset: 0x0005A12F
		public AppDomainSetup(ActivationArguments activationArguments)
		{
			this._activationArguments = activationArguments;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.AppDomainSetup" /> class with the specified activation context to use for manifest-based activation of an application domain.</summary>
		/// <param name="activationContext">The activation context to be used for an application domain.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="activationContext" /> is <see langword="null" />.</exception>
		// Token: 0x0600178E RID: 6030 RVA: 0x0005BF3E File Offset: 0x0005A13E
		public AppDomainSetup(ActivationContext activationContext)
		{
			this._activationArguments = new ActivationArguments(activationContext);
		}

		// Token: 0x0600178F RID: 6031 RVA: 0x0005BF54 File Offset: 0x0005A154
		private static string GetAppBase(string appBase)
		{
			if (appBase == null)
			{
				return null;
			}
			if (appBase.Length >= 8 && appBase.ToLower().StartsWith("file://"))
			{
				appBase = appBase.Substring(7);
				if (Path.DirectorySeparatorChar != '/')
				{
					appBase = appBase.Replace('/', Path.DirectorySeparatorChar);
				}
			}
			appBase = Path.GetFullPath(appBase);
			if (Path.DirectorySeparatorChar != '/')
			{
				bool flag = appBase.StartsWith("\\\\?\\", StringComparison.Ordinal);
				if (appBase.IndexOf(':', flag ? 6 : 2) != -1)
				{
					throw new NotSupportedException("The given path's format is not supported.");
				}
			}
			string directoryName = Path.GetDirectoryName(appBase);
			if (directoryName != null && directoryName.LastIndexOfAny(Path.GetInvalidPathChars()) >= 0)
			{
				throw new ArgumentException(string.Format(Locale.GetText("Invalid path characters in path: '{0}'"), appBase), "appBase");
			}
			string fileName = Path.GetFileName(appBase);
			if (fileName != null && fileName.LastIndexOfAny(Path.GetInvalidFileNameChars()) >= 0)
			{
				throw new ArgumentException(string.Format(Locale.GetText("Invalid filename characters in path: '{0}'"), appBase), "appBase");
			}
			return appBase;
		}

		/// <summary>Gets or sets the name of the directory containing the application.</summary>
		/// <returns>The name of the application base directory.</returns>
		// Token: 0x170002EB RID: 747
		// (get) Token: 0x06001790 RID: 6032 RVA: 0x0005C044 File Offset: 0x0005A244
		// (set) Token: 0x06001791 RID: 6033 RVA: 0x0005C051 File Offset: 0x0005A251
		public string ApplicationBase
		{
			[SecuritySafeCritical]
			get
			{
				return AppDomainSetup.GetAppBase(this.application_base);
			}
			set
			{
				this.application_base = value;
			}
		}

		/// <summary>Gets or sets the name of the application.</summary>
		/// <returns>The name of the application.</returns>
		// Token: 0x170002EC RID: 748
		// (get) Token: 0x06001792 RID: 6034 RVA: 0x0005C05A File Offset: 0x0005A25A
		// (set) Token: 0x06001793 RID: 6035 RVA: 0x0005C062 File Offset: 0x0005A262
		public string ApplicationName
		{
			get
			{
				return this.application_name;
			}
			set
			{
				this.application_name = value;
			}
		}

		/// <summary>Gets or sets the name of an area specific to the application where files are shadow copied. </summary>
		/// <returns>The fully qualified name of the directory path and file name where files are shadow copied.</returns>
		// Token: 0x170002ED RID: 749
		// (get) Token: 0x06001794 RID: 6036 RVA: 0x0005C06B File Offset: 0x0005A26B
		// (set) Token: 0x06001795 RID: 6037 RVA: 0x0005C073 File Offset: 0x0005A273
		public string CachePath
		{
			[SecuritySafeCritical]
			get
			{
				return this.cache_path;
			}
			set
			{
				this.cache_path = value;
			}
		}

		/// <summary>Gets or sets the name of the configuration file for an application domain.</summary>
		/// <returns>The name of the configuration file.</returns>
		// Token: 0x170002EE RID: 750
		// (get) Token: 0x06001796 RID: 6038 RVA: 0x0005C07C File Offset: 0x0005A27C
		// (set) Token: 0x06001797 RID: 6039 RVA: 0x0005C0CB File Offset: 0x0005A2CB
		public string ConfigurationFile
		{
			[SecuritySafeCritical]
			get
			{
				if (this.configuration_file == null)
				{
					return null;
				}
				if (Path.IsPathRooted(this.configuration_file))
				{
					return this.configuration_file;
				}
				if (this.ApplicationBase == null)
				{
					throw new MemberAccessException("The ApplicationBase must be set before retrieving this property.");
				}
				return Path.Combine(this.ApplicationBase, this.configuration_file);
			}
			set
			{
				this.configuration_file = value;
			}
		}

		/// <summary>Gets or sets a value that indicates whether the &lt;publisherPolicy&gt; section of the configuration file is applied to an application domain.</summary>
		/// <returns>
		///     <see langword="true" /> if the <see langword="&lt;publisherPolicy&gt;" /> section of the configuration file for an application domain is ignored; <see langword="false" /> if the declared publisher policy is honored.</returns>
		// Token: 0x170002EF RID: 751
		// (get) Token: 0x06001798 RID: 6040 RVA: 0x0005C0D4 File Offset: 0x0005A2D4
		// (set) Token: 0x06001799 RID: 6041 RVA: 0x0005C0DC File Offset: 0x0005A2DC
		public bool DisallowPublisherPolicy
		{
			get
			{
				return this.publisher_policy;
			}
			set
			{
				this.publisher_policy = value;
			}
		}

		/// <summary>Gets or sets the base directory where the directory for dynamically generated files is located.</summary>
		/// <returns>The directory where the <see cref="P:System.AppDomain.DynamicDirectory" /> is located.
		///       The return value of this property is different from the value assigned. See the Remarks section.</returns>
		/// <exception cref="T:System.MemberAccessException">This property cannot be set because the application name on the application domain is <see langword="null" />.</exception>
		// Token: 0x170002F0 RID: 752
		// (get) Token: 0x0600179A RID: 6042 RVA: 0x0005C0E8 File Offset: 0x0005A2E8
		// (set) Token: 0x0600179B RID: 6043 RVA: 0x0005C138 File Offset: 0x0005A338
		public string DynamicBase
		{
			[SecuritySafeCritical]
			get
			{
				if (this.dynamic_base == null)
				{
					return null;
				}
				if (Path.IsPathRooted(this.dynamic_base))
				{
					return this.dynamic_base;
				}
				if (this.ApplicationBase == null)
				{
					throw new MemberAccessException("The ApplicationBase must be set before retrieving this property.");
				}
				return Path.Combine(this.ApplicationBase, this.dynamic_base);
			}
			[SecuritySafeCritical]
			set
			{
				if (this.application_name == null)
				{
					throw new MemberAccessException("ApplicationName must be set before the DynamicBase can be set.");
				}
				this.dynamic_base = Path.Combine(value, ((uint)this.application_name.GetHashCode()).ToString("x"));
			}
		}

		/// <summary>Gets or sets the location of the license file associated with this domain.</summary>
		/// <returns>The location and name of the license file.</returns>
		// Token: 0x170002F1 RID: 753
		// (get) Token: 0x0600179C RID: 6044 RVA: 0x0005C17C File Offset: 0x0005A37C
		// (set) Token: 0x0600179D RID: 6045 RVA: 0x0005C184 File Offset: 0x0005A384
		public string LicenseFile
		{
			[SecuritySafeCritical]
			get
			{
				return this.license_file;
			}
			set
			{
				this.license_file = value;
			}
		}

		/// <summary>Specifies the optimization policy used to load an executable.</summary>
		/// <returns>An enumerated constant that is used with the <see cref="T:System.LoaderOptimizationAttribute" />.</returns>
		// Token: 0x170002F2 RID: 754
		// (get) Token: 0x0600179E RID: 6046 RVA: 0x0005C18D File Offset: 0x0005A38D
		// (set) Token: 0x0600179F RID: 6047 RVA: 0x0005C195 File Offset: 0x0005A395
		[MonoLimitation("In Mono this is controlled by the --share-code flag")]
		public LoaderOptimization LoaderOptimization
		{
			get
			{
				return (LoaderOptimization)this.loader_optimization;
			}
			set
			{
				this.loader_optimization = (int)value;
			}
		}

		/// <summary>Gets or sets the list of directories under the application base directory that are probed for private assemblies.</summary>
		/// <returns>A list of directory names separated by semicolons.</returns>
		// Token: 0x170002F3 RID: 755
		// (get) Token: 0x060017A0 RID: 6048 RVA: 0x0005C19E File Offset: 0x0005A39E
		// (set) Token: 0x060017A1 RID: 6049 RVA: 0x0005C1A6 File Offset: 0x0005A3A6
		public string PrivateBinPath
		{
			[SecuritySafeCritical]
			get
			{
				return this.private_bin_path;
			}
			set
			{
				this.private_bin_path = value;
				this.path_changed = true;
			}
		}

		/// <summary>Gets or sets a string value that includes or excludes <see cref="P:System.AppDomainSetup.ApplicationBase" /> from the search path for the application, and searches only <see cref="P:System.AppDomainSetup.PrivateBinPath" />.</summary>
		/// <returns>
		///     A null reference (<see langword="Nothing" /> in Visual Basic) to include the application base path when searching for assemblies; any non-null string value to exclude the path. The default value is <see langword="null" />.</returns>
		// Token: 0x170002F4 RID: 756
		// (get) Token: 0x060017A2 RID: 6050 RVA: 0x0005C1B6 File Offset: 0x0005A3B6
		// (set) Token: 0x060017A3 RID: 6051 RVA: 0x0005C1BE File Offset: 0x0005A3BE
		public string PrivateBinPathProbe
		{
			get
			{
				return this.private_bin_path_probe;
			}
			set
			{
				this.private_bin_path_probe = value;
				this.path_changed = true;
			}
		}

		/// <summary>Gets or sets the names of the directories containing assemblies to be shadow copied.</summary>
		/// <returns>A list of directory names separated by semicolons.</returns>
		// Token: 0x170002F5 RID: 757
		// (get) Token: 0x060017A4 RID: 6052 RVA: 0x0005C1CE File Offset: 0x0005A3CE
		// (set) Token: 0x060017A5 RID: 6053 RVA: 0x0005C1D6 File Offset: 0x0005A3D6
		public string ShadowCopyDirectories
		{
			[SecuritySafeCritical]
			get
			{
				return this.shadow_copy_directories;
			}
			set
			{
				this.shadow_copy_directories = value;
			}
		}

		/// <summary>Gets or sets a string that indicates whether shadow copying is turned on or off.</summary>
		/// <returns>The string value "true" to indicate that shadow copying is turned on; or "false" to indicate that shadow copying is turned off.</returns>
		// Token: 0x170002F6 RID: 758
		// (get) Token: 0x060017A6 RID: 6054 RVA: 0x0005C1DF File Offset: 0x0005A3DF
		// (set) Token: 0x060017A7 RID: 6055 RVA: 0x0005C1E7 File Offset: 0x0005A3E7
		public string ShadowCopyFiles
		{
			get
			{
				return this.shadow_copy_files;
			}
			set
			{
				this.shadow_copy_files = value;
			}
		}

		/// <summary>Gets or sets a value that indicates whether an application domain allows assembly binding redirection.</summary>
		/// <returns>
		///     <see langword="true" /> if redirection of assemblies is not allowed; <see langword="false" /> if it is allowed.</returns>
		// Token: 0x170002F7 RID: 759
		// (get) Token: 0x060017A8 RID: 6056 RVA: 0x0005C1F0 File Offset: 0x0005A3F0
		// (set) Token: 0x060017A9 RID: 6057 RVA: 0x0005C1F8 File Offset: 0x0005A3F8
		public bool DisallowBindingRedirects
		{
			get
			{
				return this.disallow_binding_redirects;
			}
			set
			{
				this.disallow_binding_redirects = value;
			}
		}

		/// <summary>Gets or sets a value that indicates whether HTTP download of assemblies is allowed for an application domain.</summary>
		/// <returns>
		///     <see langword="true" /> if HTTP download of assemblies is not allowed; <see langword="false" /> if it is allowed. </returns>
		// Token: 0x170002F8 RID: 760
		// (get) Token: 0x060017AA RID: 6058 RVA: 0x0005C201 File Offset: 0x0005A401
		// (set) Token: 0x060017AB RID: 6059 RVA: 0x0005C209 File Offset: 0x0005A409
		public bool DisallowCodeDownload
		{
			get
			{
				return this.disallow_code_downloads;
			}
			set
			{
				this.disallow_code_downloads = value;
			}
		}

		/// <summary>Gets or sets a string that specifies the target version and profile of the .NET Framework for the application domain, in a format that can be parsed by the <see cref="M:System.Runtime.Versioning.FrameworkName.#ctor(System.String)" /> constructor. </summary>
		/// <returns>The target version and profile of the .NET Framework. </returns>
		// Token: 0x170002F9 RID: 761
		// (get) Token: 0x060017AC RID: 6060 RVA: 0x0005C212 File Offset: 0x0005A412
		// (set) Token: 0x060017AD RID: 6061 RVA: 0x0005C21A File Offset: 0x0005A41A
		public string TargetFrameworkName
		{
			[CompilerGenerated]
			get
			{
				return this.<TargetFrameworkName>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<TargetFrameworkName>k__BackingField = value;
			}
		}

		/// <summary>Gets or sets data about the activation of an application domain.</summary>
		/// <returns>An object that contains data about the activation of an application domain.</returns>
		/// <exception cref="T:System.InvalidOperationException">The property is set to an <see cref="T:System.Runtime.Hosting.ActivationArguments" /> object whose application identity does not match the application identity of the <see cref="T:System.Security.Policy.ApplicationTrust" /> object returned by the <see cref="P:System.AppDomainSetup.ApplicationTrust" /> property. No exception is thrown if the <see cref="P:System.AppDomainSetup.ApplicationTrust" /> property is <see langword="null" />.</exception>
		// Token: 0x170002FA RID: 762
		// (get) Token: 0x060017AE RID: 6062 RVA: 0x0005C223 File Offset: 0x0005A423
		// (set) Token: 0x060017AF RID: 6063 RVA: 0x0005C24A File Offset: 0x0005A44A
		public ActivationArguments ActivationArguments
		{
			get
			{
				if (this._activationArguments != null)
				{
					return (ActivationArguments)this._activationArguments;
				}
				this.DeserializeNonPrimitives();
				return (ActivationArguments)this._activationArguments;
			}
			set
			{
				this._activationArguments = value;
			}
		}

		/// <summary>Gets or sets the <see cref="T:System.AppDomainInitializer" /> delegate, which represents a callback method that is invoked when the application domain is initialized.</summary>
		/// <returns>A delegate that represents a callback method that is invoked when the application domain is initialized.</returns>
		// Token: 0x170002FB RID: 763
		// (get) Token: 0x060017B0 RID: 6064 RVA: 0x0005C253 File Offset: 0x0005A453
		// (set) Token: 0x060017B1 RID: 6065 RVA: 0x0005C27A File Offset: 0x0005A47A
		[MonoLimitation("it needs to be invoked within the created domain")]
		public AppDomainInitializer AppDomainInitializer
		{
			get
			{
				if (this.domain_initializer != null)
				{
					return (AppDomainInitializer)this.domain_initializer;
				}
				this.DeserializeNonPrimitives();
				return (AppDomainInitializer)this.domain_initializer;
			}
			set
			{
				this.domain_initializer = value;
			}
		}

		/// <summary>Gets or sets the arguments passed to the callback method represented by the <see cref="T:System.AppDomainInitializer" /> delegate. The callback method is invoked when the application domain is initialized.</summary>
		/// <returns>An array of strings that is passed to the callback method represented by the <see cref="T:System.AppDomainInitializer" /> delegate, when the callback method is invoked during <see cref="T:System.AppDomain" /> initialization.</returns>
		// Token: 0x170002FC RID: 764
		// (get) Token: 0x060017B2 RID: 6066 RVA: 0x0005C283 File Offset: 0x0005A483
		// (set) Token: 0x060017B3 RID: 6067 RVA: 0x0005C28B File Offset: 0x0005A48B
		[MonoLimitation("it needs to be used to invoke the initializer within the created domain")]
		public string[] AppDomainInitializerArguments
		{
			get
			{
				return this.domain_initializer_args;
			}
			set
			{
				this.domain_initializer_args = value;
			}
		}

		/// <summary>Gets or sets an object containing security and trust information.</summary>
		/// <returns>An object that contains security and trust information. </returns>
		/// <exception cref="T:System.InvalidOperationException">The property is set to an <see cref="T:System.Security.Policy.ApplicationTrust" /> object whose application identity does not match the application identity of the <see cref="T:System.Runtime.Hosting.ActivationArguments" /> object returned by the <see cref="P:System.AppDomainSetup.ActivationArguments" /> property. No exception is thrown if the <see cref="P:System.AppDomainSetup.ActivationArguments" /> property is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentNullException">The property is set to <see langword="null" />.</exception>
		// Token: 0x170002FD RID: 765
		// (get) Token: 0x060017B4 RID: 6068 RVA: 0x0005C294 File Offset: 0x0005A494
		// (set) Token: 0x060017B5 RID: 6069 RVA: 0x0005C2CE File Offset: 0x0005A4CE
		[MonoNotSupported("This property exists but not considered.")]
		public ApplicationTrust ApplicationTrust
		{
			get
			{
				if (this.application_trust != null)
				{
					return (ApplicationTrust)this.application_trust;
				}
				this.DeserializeNonPrimitives();
				if (this.application_trust == null)
				{
					this.application_trust = new ApplicationTrust();
				}
				return (ApplicationTrust)this.application_trust;
			}
			set
			{
				this.application_trust = value;
			}
		}

		/// <summary>Specifies whether the application base path and private binary path are probed when searching for assemblies to load.</summary>
		/// <returns>
		///     <see langword="true" /> if probing is not allowed; otherwise, <see langword="false" />. The default is <see langword="false" />.</returns>
		// Token: 0x170002FE RID: 766
		// (get) Token: 0x060017B6 RID: 6070 RVA: 0x0005C2D7 File Offset: 0x0005A4D7
		// (set) Token: 0x060017B7 RID: 6071 RVA: 0x0005C2DF File Offset: 0x0005A4DF
		[MonoNotSupported("This property exists but not considered.")]
		public bool DisallowApplicationBaseProbing
		{
			get
			{
				return this.disallow_appbase_probe;
			}
			set
			{
				this.disallow_appbase_probe = value;
			}
		}

		/// <summary>Returns the XML configuration information set by the <see cref="M:System.AppDomainSetup.SetConfigurationBytes(System.Byte[])" /> method, which overrides the application's XML configuration information.</summary>
		/// <returns>An array that contains the XML configuration information that was set by the <see cref="M:System.AppDomainSetup.SetConfigurationBytes(System.Byte[])" /> method, or <see langword="null" /> if the <see cref="M:System.AppDomainSetup.SetConfigurationBytes(System.Byte[])" /> method has not been called.</returns>
		// Token: 0x060017B8 RID: 6072 RVA: 0x0005C2E8 File Offset: 0x0005A4E8
		[MonoNotSupported("This method exists but not considered.")]
		public byte[] GetConfigurationBytes()
		{
			if (this.configuration_bytes == null)
			{
				return null;
			}
			return this.configuration_bytes.Clone() as byte[];
		}

		/// <summary>Provides XML configuration information for the application domain, replacing the application's XML configuration information.</summary>
		/// <param name="value">An array that contains the XML configuration information to be used for the application domain.</param>
		// Token: 0x060017B9 RID: 6073 RVA: 0x0005C304 File Offset: 0x0005A504
		[MonoNotSupported("This method exists but not considered.")]
		public void SetConfigurationBytes(byte[] value)
		{
			this.configuration_bytes = value;
		}

		// Token: 0x060017BA RID: 6074 RVA: 0x0005C310 File Offset: 0x0005A510
		private void DeserializeNonPrimitives()
		{
			lock (this)
			{
				if (this.serialized_non_primitives != null)
				{
					BinaryFormatter binaryFormatter = new BinaryFormatter();
					MemoryStream serializationStream = new MemoryStream(this.serialized_non_primitives);
					object[] array = (object[])binaryFormatter.Deserialize(serializationStream);
					this._activationArguments = (ActivationArguments)array[0];
					this.domain_initializer = (AppDomainInitializer)array[1];
					this.application_trust = (ApplicationTrust)array[2];
					this.serialized_non_primitives = null;
				}
			}
		}

		// Token: 0x060017BB RID: 6075 RVA: 0x0005C3A0 File Offset: 0x0005A5A0
		internal void SerializeNonPrimitives()
		{
			object[] graph = new object[]
			{
				this._activationArguments,
				this.domain_initializer,
				this.application_trust
			};
			BinaryFormatter binaryFormatter = new BinaryFormatter();
			MemoryStream memoryStream = new MemoryStream();
			binaryFormatter.Serialize(memoryStream, graph);
			this.serialized_non_primitives = memoryStream.ToArray();
		}

		/// <summary>Sets the specified switches, making the application domain compatible with previous versions of the .NET Framework for the specified issues.</summary>
		/// <param name="switches">An enumerable set of string values that specify compatibility switches, or <see langword="null" /> to erase the existing compatibility switches.</param>
		// Token: 0x060017BC RID: 6076 RVA: 0x000020D3 File Offset: 0x000002D3
		[MonoTODO("not implemented, does not throw because it's used in testing moonlight")]
		public void SetCompatibilitySwitches(IEnumerable<string> switches)
		{
		}

		/// <summary>Gets or sets the display name of the assembly that provides the type of the application domain manager for application domains created using this <see cref="T:System.AppDomainSetup" /> object.</summary>
		/// <returns>The display name of the assembly that provides the <see cref="T:System.Type" /> of the application domain manager.</returns>
		// Token: 0x170002FF RID: 767
		// (get) Token: 0x060017BD RID: 6077 RVA: 0x0005AB11 File Offset: 0x00058D11
		// (set) Token: 0x060017BE RID: 6078 RVA: 0x00002ABD File Offset: 0x00000CBD
		public string AppDomainManagerAssembly
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the full name of the type that provides the application domain manager for application domains created using this <see cref="T:System.AppDomainSetup" /> object.</summary>
		/// <returns>The full name of the type, including the namespace.</returns>
		// Token: 0x17000300 RID: 768
		// (get) Token: 0x060017BF RID: 6079 RVA: 0x0005AB11 File Offset: 0x00058D11
		// (set) Token: 0x060017C0 RID: 6080 RVA: 0x00002ABD File Offset: 0x00000CBD
		public string AppDomainManagerType
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets a list of assemblies marked with the <see cref="F:System.Security.PartialTrustVisibilityLevel.NotVisibleByDefault" /> flag that are made visible to partial-trust code running in a sandboxed application domain. </summary>
		/// <returns>An array of partial assembly names, where each partial name consists of the simple assembly name and the public key.</returns>
		// Token: 0x17000301 RID: 769
		// (get) Token: 0x060017C1 RID: 6081 RVA: 0x0005AB11 File Offset: 0x00058D11
		// (set) Token: 0x060017C2 RID: 6082 RVA: 0x00002ABD File Offset: 0x00000CBD
		public string[] PartialTrustVisibleAssemblies
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets a value that indicates whether interface caching is disabled for interop calls in the application domain, so that a QueryInterface is performed on each call.</summary>
		/// <returns>
		///     <see langword="true" /> if interface caching is disabled for interop calls in application domains created with the current <see cref="T:System.AppDomainSetup" /> object; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000302 RID: 770
		// (get) Token: 0x060017C3 RID: 6083 RVA: 0x0005C3F0 File Offset: 0x0005A5F0
		// (set) Token: 0x060017C4 RID: 6084 RVA: 0x00002ABD File Offset: 0x00000CBD
		public bool SandboxInterop
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Provides the common language runtime with an alternate implementation of a string comparison function. </summary>
		/// <param name="functionName">The name of the string comparison function to override.</param>
		/// <param name="functionVersion">The function version. For .NET Framework 4.5, its value must be 1 or greater.</param>
		/// <param name="functionPointer">A pointer to the function that overrides <paramref name="functionName" />.</param>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="functionName" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="functionVersion" /> is not 1 or greater.-or-
		///         <paramref name="functionPointer" /> is <see cref="F:System.IntPtr.Zero" />. </exception>
		// Token: 0x060017C5 RID: 6085 RVA: 0x00002ABD File Offset: 0x00000CBD
		[SecurityCritical]
		public void SetNativeFunction(string functionName, int functionVersion, IntPtr functionPointer)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x04000C25 RID: 3109
		private string application_base;

		// Token: 0x04000C26 RID: 3110
		private string application_name;

		// Token: 0x04000C27 RID: 3111
		private string cache_path;

		// Token: 0x04000C28 RID: 3112
		private string configuration_file;

		// Token: 0x04000C29 RID: 3113
		private string dynamic_base;

		// Token: 0x04000C2A RID: 3114
		private string license_file;

		// Token: 0x04000C2B RID: 3115
		private string private_bin_path;

		// Token: 0x04000C2C RID: 3116
		private string private_bin_path_probe;

		// Token: 0x04000C2D RID: 3117
		private string shadow_copy_directories;

		// Token: 0x04000C2E RID: 3118
		private string shadow_copy_files;

		// Token: 0x04000C2F RID: 3119
		private bool publisher_policy;

		// Token: 0x04000C30 RID: 3120
		private bool path_changed;

		// Token: 0x04000C31 RID: 3121
		private int loader_optimization;

		// Token: 0x04000C32 RID: 3122
		private bool disallow_binding_redirects;

		// Token: 0x04000C33 RID: 3123
		private bool disallow_code_downloads;

		// Token: 0x04000C34 RID: 3124
		private object _activationArguments;

		// Token: 0x04000C35 RID: 3125
		private object domain_initializer;

		// Token: 0x04000C36 RID: 3126
		private object application_trust;

		// Token: 0x04000C37 RID: 3127
		private string[] domain_initializer_args;

		// Token: 0x04000C38 RID: 3128
		private bool disallow_appbase_probe;

		// Token: 0x04000C39 RID: 3129
		private byte[] configuration_bytes;

		// Token: 0x04000C3A RID: 3130
		private byte[] serialized_non_primitives;

		// Token: 0x04000C3B RID: 3131
		[CompilerGenerated]
		private string <TargetFrameworkName>k__BackingField;
	}
}
