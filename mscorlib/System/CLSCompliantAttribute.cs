﻿using System;
using System.Runtime.InteropServices;

namespace System
{
	/// <summary>Indicates whether a program element is compliant with the Common Language Specification (CLS). This class cannot be inherited.</summary>
	// Token: 0x02000128 RID: 296
	[AttributeUsage(AttributeTargets.All, Inherited = true, AllowMultiple = false)]
	[ComVisible(true)]
	[Serializable]
	public sealed class CLSCompliantAttribute : Attribute
	{
		/// <summary>Initializes an instance of the <see cref="T:System.CLSCompliantAttribute" /> class with a Boolean value indicating whether the indicated program element is CLS-compliant.</summary>
		/// <param name="isCompliant">
		///       <see langword="true" /> if CLS-compliant; otherwise, <see langword="false" />. </param>
		// Token: 0x06000B53 RID: 2899 RVA: 0x000354EF File Offset: 0x000336EF
		public CLSCompliantAttribute(bool isCompliant)
		{
			this.m_compliant = isCompliant;
		}

		/// <summary>Gets the Boolean value indicating whether the indicated program element is CLS-compliant.</summary>
		/// <returns>
		///     <see langword="true" /> if the program element is CLS-compliant; otherwise, <see langword="false" />.</returns>
		// Token: 0x170001B1 RID: 433
		// (get) Token: 0x06000B54 RID: 2900 RVA: 0x000354FE File Offset: 0x000336FE
		public bool IsCompliant
		{
			get
			{
				return this.m_compliant;
			}
		}

		// Token: 0x0400079E RID: 1950
		private bool m_compliant;
	}
}
