﻿using System;

namespace System
{
	// Token: 0x020001DE RID: 478
	internal static class Array_ReferenceSources
	{
		// Token: 0x04000BD0 RID: 3024
		internal const int MaxArrayLength = 2146435071;

		// Token: 0x04000BD1 RID: 3025
		internal const int MaxByteArrayLength = 2147483591;
	}
}
