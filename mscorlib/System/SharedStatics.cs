﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.ConstrainedExecution;
using System.Security;
using System.Security.Util;
using System.Threading;

namespace System
{
	// Token: 0x020001AD RID: 429
	internal sealed class SharedStatics
	{
		// Token: 0x060012B0 RID: 4784 RVA: 0x00002050 File Offset: 0x00000250
		private SharedStatics()
		{
		}

		// Token: 0x1700022A RID: 554
		// (get) Token: 0x060012B1 RID: 4785 RVA: 0x0004CC94 File Offset: 0x0004AE94
		public static string Remoting_Identity_IDGuid
		{
			[SecuritySafeCritical]
			get
			{
				if (SharedStatics._sharedStatics._Remoting_Identity_IDGuid == null)
				{
					bool flag = false;
					RuntimeHelpers.PrepareConstrainedRegions();
					try
					{
						Monitor.Enter(SharedStatics._sharedStatics, ref flag);
						if (SharedStatics._sharedStatics._Remoting_Identity_IDGuid == null)
						{
							SharedStatics._sharedStatics._Remoting_Identity_IDGuid = Guid.NewGuid().ToString().Replace('-', '_');
						}
					}
					finally
					{
						if (flag)
						{
							Monitor.Exit(SharedStatics._sharedStatics);
						}
					}
				}
				return SharedStatics._sharedStatics._Remoting_Identity_IDGuid;
			}
		}

		// Token: 0x060012B2 RID: 4786 RVA: 0x0004CD24 File Offset: 0x0004AF24
		[SecuritySafeCritical]
		public static Tokenizer.StringMaker GetSharedStringMaker()
		{
			Tokenizer.StringMaker stringMaker = null;
			bool flag = false;
			RuntimeHelpers.PrepareConstrainedRegions();
			try
			{
				Monitor.Enter(SharedStatics._sharedStatics, ref flag);
				if (SharedStatics._sharedStatics._maker != null)
				{
					stringMaker = SharedStatics._sharedStatics._maker;
					SharedStatics._sharedStatics._maker = null;
				}
			}
			finally
			{
				if (flag)
				{
					Monitor.Exit(SharedStatics._sharedStatics);
				}
			}
			if (stringMaker == null)
			{
				stringMaker = new Tokenizer.StringMaker();
			}
			return stringMaker;
		}

		// Token: 0x060012B3 RID: 4787 RVA: 0x0004CD94 File Offset: 0x0004AF94
		[SecuritySafeCritical]
		public static void ReleaseSharedStringMaker(ref Tokenizer.StringMaker maker)
		{
			bool flag = false;
			RuntimeHelpers.PrepareConstrainedRegions();
			try
			{
				Monitor.Enter(SharedStatics._sharedStatics, ref flag);
				SharedStatics._sharedStatics._maker = maker;
				maker = null;
			}
			finally
			{
				if (flag)
				{
					Monitor.Exit(SharedStatics._sharedStatics);
				}
			}
		}

		// Token: 0x060012B4 RID: 4788 RVA: 0x0004CDE4 File Offset: 0x0004AFE4
		internal static int Remoting_Identity_GetNextSeqNum()
		{
			return Interlocked.Increment(ref SharedStatics._sharedStatics._Remoting_Identity_IDSeqNum);
		}

		// Token: 0x060012B5 RID: 4789 RVA: 0x0004CDF5 File Offset: 0x0004AFF5
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		internal static long AddMemoryFailPointReservation(long size)
		{
			return Interlocked.Add(ref SharedStatics._sharedStatics._memFailPointReservedMemory, size);
		}

		// Token: 0x1700022B RID: 555
		// (get) Token: 0x060012B6 RID: 4790 RVA: 0x0004CE07 File Offset: 0x0004B007
		internal static ulong MemoryFailPointReservedMemory
		{
			get
			{
				return (ulong)Volatile.Read(ref SharedStatics._sharedStatics._memFailPointReservedMemory);
			}
		}

		// Token: 0x060012B7 RID: 4791 RVA: 0x0004CE18 File Offset: 0x0004B018
		// Note: this type is marked as 'beforefieldinit'.
		static SharedStatics()
		{
		}

		// Token: 0x04000A96 RID: 2710
		private static readonly SharedStatics _sharedStatics = new SharedStatics();

		// Token: 0x04000A97 RID: 2711
		private volatile string _Remoting_Identity_IDGuid;

		// Token: 0x04000A98 RID: 2712
		private Tokenizer.StringMaker _maker;

		// Token: 0x04000A99 RID: 2713
		private int _Remoting_Identity_IDSeqNum;

		// Token: 0x04000A9A RID: 2714
		private long _memFailPointReservedMemory;
	}
}
