﻿using System;
using System.Runtime.InteropServices;

namespace System
{
	/// <summary>Marks the program elements that are no longer in use. This class cannot be inherited.</summary>
	// Token: 0x02000198 RID: 408
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Enum | AttributeTargets.Constructor | AttributeTargets.Method | AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Event | AttributeTargets.Interface | AttributeTargets.Delegate, Inherited = false)]
	[ComVisible(true)]
	[Serializable]
	public sealed class ObsoleteAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.ObsoleteAttribute" /> class with default properties.</summary>
		// Token: 0x060011A3 RID: 4515 RVA: 0x000490C9 File Offset: 0x000472C9
		public ObsoleteAttribute()
		{
			this._message = null;
			this._error = false;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ObsoleteAttribute" /> class with a specified workaround message.</summary>
		/// <param name="message">The text string that describes alternative workarounds. </param>
		// Token: 0x060011A4 RID: 4516 RVA: 0x000490DF File Offset: 0x000472DF
		public ObsoleteAttribute(string message)
		{
			this._message = message;
			this._error = false;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ObsoleteAttribute" /> class with a workaround message and a Boolean value indicating whether the obsolete element usage is considered an error.</summary>
		/// <param name="message">The text string that describes alternative workarounds. </param>
		/// <param name="error">
		///       <see langword="true" /> if the obsolete element usage generates a compiler error; <see langword="false" /> if it generates a compiler warning. </param>
		// Token: 0x060011A5 RID: 4517 RVA: 0x000490F5 File Offset: 0x000472F5
		public ObsoleteAttribute(string message, bool error)
		{
			this._message = message;
			this._error = error;
		}

		/// <summary>Gets the workaround message, including a description of the alternative program elements.</summary>
		/// <returns>The workaround text string.</returns>
		// Token: 0x17000207 RID: 519
		// (get) Token: 0x060011A6 RID: 4518 RVA: 0x0004910B File Offset: 0x0004730B
		public string Message
		{
			get
			{
				return this._message;
			}
		}

		/// <summary>Gets a Boolean value indicating whether the compiler will treat usage of the obsolete program element as an error.</summary>
		/// <returns>
		///     <see langword="true" /> if the obsolete element usage is considered an error; otherwise, <see langword="false" />. The default is <see langword="false" />.</returns>
		// Token: 0x17000208 RID: 520
		// (get) Token: 0x060011A7 RID: 4519 RVA: 0x00049113 File Offset: 0x00047313
		public bool IsError
		{
			get
			{
				return this._error;
			}
		}

		// Token: 0x04000A13 RID: 2579
		private string _message;

		// Token: 0x04000A14 RID: 2580
		private bool _error;
	}
}
