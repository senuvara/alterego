﻿using System;
using System.Runtime.InteropServices;
using System.Text;

namespace System
{
	// Token: 0x02000227 RID: 551
	internal static class ParameterizedStrings
	{
		// Token: 0x06001A9A RID: 6810 RVA: 0x00064AA4 File Offset: 0x00062CA4
		public static string Evaluate(string format, params ParameterizedStrings.FormatParam[] args)
		{
			if (format == null)
			{
				throw new ArgumentNullException("format");
			}
			if (args == null)
			{
				throw new ArgumentNullException("args");
			}
			ParameterizedStrings.LowLevelStack lowLevelStack = ParameterizedStrings._cachedStack;
			if (lowLevelStack == null)
			{
				lowLevelStack = (ParameterizedStrings._cachedStack = new ParameterizedStrings.LowLevelStack());
			}
			else
			{
				lowLevelStack.Clear();
			}
			ParameterizedStrings.FormatParam[] array = null;
			ParameterizedStrings.FormatParam[] array2 = null;
			int num = 0;
			return ParameterizedStrings.EvaluateInternal(format, ref num, args, lowLevelStack, ref array, ref array2);
		}

		// Token: 0x06001A9B RID: 6811 RVA: 0x00064B00 File Offset: 0x00062D00
		private static string EvaluateInternal(string format, ref int pos, ParameterizedStrings.FormatParam[] args, ParameterizedStrings.LowLevelStack stack, ref ParameterizedStrings.FormatParam[] dynamicVars, ref ParameterizedStrings.FormatParam[] staticVars)
		{
			StringBuilder stringBuilder = new StringBuilder(format.Length);
			bool flag = false;
			while (pos < format.Length)
			{
				if (format[pos] == '%')
				{
					pos++;
					char c = format[pos];
					if (c <= 'X')
					{
						switch (c)
						{
						case '!':
							goto IL_529;
						case '"':
						case '#':
						case '$':
						case '(':
						case ')':
						case ',':
						case '.':
						case '@':
						case 'B':
						case 'C':
						case 'D':
						case 'E':
						case 'F':
						case 'G':
						case 'H':
						case 'I':
						case 'J':
						case 'K':
						case 'L':
						case 'M':
						case 'N':
							goto IL_678;
						case '%':
							stringBuilder.Append('%');
							goto IL_683;
						case '&':
						case '*':
						case '+':
						case '-':
						case '/':
						case '<':
						case '=':
						case '>':
						case 'A':
						case 'O':
							goto IL_3B3;
						case '\'':
							stack.Push((int)format[pos + 1]);
							pos += 2;
							goto IL_683;
						case '0':
						case '1':
						case '2':
						case '3':
						case '4':
						case '5':
						case '6':
						case '7':
						case '8':
						case '9':
						case ':':
							break;
						case ';':
							goto IL_655;
						case '?':
							flag = true;
							goto IL_683;
						case 'P':
						{
							pos++;
							int num;
							ParameterizedStrings.GetDynamicOrStaticVariables(format[pos], ref dynamicVars, ref staticVars, out num)[num] = stack.Pop();
							goto IL_683;
						}
						default:
							if (c != 'X')
							{
								goto IL_678;
							}
							break;
						}
					}
					else
					{
						switch (c)
						{
						case '^':
						case 'm':
							goto IL_3B3;
						case '_':
						case '`':
						case 'a':
						case 'b':
						case 'f':
						case 'h':
						case 'j':
						case 'k':
						case 'n':
						case 'q':
						case 'r':
							goto IL_678;
						case 'c':
							stringBuilder.Append((char)stack.Pop().Int32);
							goto IL_683;
						case 'd':
							stringBuilder.Append(stack.Pop().Int32);
							goto IL_683;
						case 'e':
							goto IL_655;
						case 'g':
						{
							pos++;
							int num2;
							ParameterizedStrings.FormatParam[] dynamicOrStaticVariables = ParameterizedStrings.GetDynamicOrStaticVariables(format[pos], ref dynamicVars, ref staticVars, out num2);
							stack.Push(dynamicOrStaticVariables[num2]);
							goto IL_683;
						}
						case 'i':
							args[0] = 1 + args[0].Int32;
							args[1] = 1 + args[1].Int32;
							goto IL_683;
						case 'l':
							stack.Push(stack.Pop().String.Length);
							goto IL_683;
						case 'o':
							break;
						case 'p':
							pos++;
							stack.Push(args[(int)(format[pos] - '1')]);
							goto IL_683;
						case 's':
							stringBuilder.Append(stack.Pop().String);
							goto IL_683;
						case 't':
						{
							bool flag2 = ParameterizedStrings.AsBool(stack.Pop().Int32);
							pos++;
							string value = ParameterizedStrings.EvaluateInternal(format, ref pos, args, stack, ref dynamicVars, ref staticVars);
							if (flag2)
							{
								stringBuilder.Append(value);
							}
							if (!ParameterizedStrings.AsBool(stack.Pop().Int32))
							{
								pos++;
								string value2 = ParameterizedStrings.EvaluateInternal(format, ref pos, args, stack, ref dynamicVars, ref staticVars);
								if (!flag2)
								{
									stringBuilder.Append(value2);
								}
								if (!ParameterizedStrings.AsBool(stack.Pop().Int32))
								{
									throw new InvalidOperationException("Terminfo database contains invalid values");
								}
							}
							if (!flag)
							{
								stack.Push(1);
								return stringBuilder.ToString();
							}
							flag = false;
							goto IL_683;
						}
						default:
							switch (c)
							{
							case 'x':
								break;
							case 'y':
							case 'z':
							case '}':
								goto IL_678;
							case '{':
							{
								pos++;
								int num3 = 0;
								while (format[pos] != '}')
								{
									num3 = num3 * 10 + (int)(format[pos] - '0');
									pos++;
								}
								stack.Push(num3);
								goto IL_683;
							}
							case '|':
								goto IL_3B3;
							case '~':
								goto IL_529;
							default:
								goto IL_678;
							}
							break;
						}
					}
					int i;
					for (i = pos; i < format.Length; i++)
					{
						char c2 = format[i];
						if (c2 == 'd' || c2 == 'o' || c2 == 'x' || c2 == 'X' || c2 == 's')
						{
							break;
						}
					}
					if (i >= format.Length)
					{
						throw new InvalidOperationException("Terminfo database contains invalid values");
					}
					string text = format.Substring(pos - 1, i - pos + 2);
					if (text.Length > 1 && text[1] == ':')
					{
						text = text.Remove(1, 1);
					}
					stringBuilder.Append(ParameterizedStrings.FormatPrintF(text, stack.Pop().Object));
					goto IL_683;
					IL_3B3:
					int @int = stack.Pop().Int32;
					int int2 = stack.Pop().Int32;
					c = format[pos];
					int value3;
					if (c <= 'A')
					{
						if (c != '&')
						{
							switch (c)
							{
							case '*':
								value3 = int2 * @int;
								break;
							case '+':
								value3 = int2 + @int;
								break;
							case ',':
							case '.':
								goto IL_514;
							case '-':
								value3 = int2 - @int;
								break;
							case '/':
								value3 = int2 / @int;
								break;
							default:
								switch (c)
								{
								case '<':
									value3 = ParameterizedStrings.AsInt(int2 < @int);
									break;
								case '=':
									value3 = ParameterizedStrings.AsInt(int2 == @int);
									break;
								case '>':
									value3 = ParameterizedStrings.AsInt(int2 > @int);
									break;
								case '?':
								case '@':
									goto IL_514;
								case 'A':
									value3 = ParameterizedStrings.AsInt(ParameterizedStrings.AsBool(int2) && ParameterizedStrings.AsBool(@int));
									break;
								default:
									goto IL_514;
								}
								break;
							}
						}
						else
						{
							value3 = (int2 & @int);
						}
					}
					else if (c <= '^')
					{
						if (c != 'O')
						{
							if (c != '^')
							{
								goto IL_514;
							}
							value3 = (int2 ^ @int);
						}
						else
						{
							value3 = ParameterizedStrings.AsInt(ParameterizedStrings.AsBool(int2) || ParameterizedStrings.AsBool(@int));
						}
					}
					else if (c != 'm')
					{
						if (c != '|')
						{
							goto IL_514;
						}
						value3 = (int2 | @int);
					}
					else
					{
						value3 = int2 % @int;
					}
					IL_517:
					stack.Push(value3);
					goto IL_683;
					IL_514:
					value3 = 0;
					goto IL_517;
					IL_529:
					int int3 = stack.Pop().Int32;
					stack.Push((format[pos] == '!') ? ParameterizedStrings.AsInt(!ParameterizedStrings.AsBool(int3)) : (~int3));
					goto IL_683;
					IL_655:
					stack.Push(ParameterizedStrings.AsInt(format[pos] == ';'));
					return stringBuilder.ToString();
					IL_678:
					throw new InvalidOperationException("Terminfo database contains invalid values");
				}
				stringBuilder.Append(format[pos]);
				IL_683:
				pos++;
			}
			stack.Push(1);
			return stringBuilder.ToString();
		}

		// Token: 0x06001A9C RID: 6812 RVA: 0x00035D4E File Offset: 0x00033F4E
		private static bool AsBool(int i)
		{
			return i != 0;
		}

		// Token: 0x06001A9D RID: 6813 RVA: 0x00035F0D File Offset: 0x0003410D
		private static int AsInt(bool b)
		{
			if (!b)
			{
				return 0;
			}
			return 1;
		}

		// Token: 0x06001A9E RID: 6814 RVA: 0x000651B8 File Offset: 0x000633B8
		private static string StringFromAsciiBytes(byte[] buffer, int offset, int length)
		{
			if (length == 0)
			{
				return string.Empty;
			}
			char[] array = new char[length];
			int i = 0;
			int num = offset;
			while (i < length)
			{
				array[i] = (char)buffer[num];
				i++;
				num++;
			}
			return new string(array);
		}

		// Token: 0x06001A9F RID: 6815
		[DllImport("libc")]
		private unsafe static extern int snprintf(byte* str, IntPtr size, string format, string arg1);

		// Token: 0x06001AA0 RID: 6816
		[DllImport("libc")]
		private unsafe static extern int snprintf(byte* str, IntPtr size, string format, int arg1);

		// Token: 0x06001AA1 RID: 6817 RVA: 0x000651F4 File Offset: 0x000633F4
		private unsafe static string FormatPrintF(string format, object arg)
		{
			string text = arg as string;
			int num = (text != null) ? ParameterizedStrings.snprintf(null, IntPtr.Zero, format, text) : ParameterizedStrings.snprintf(null, IntPtr.Zero, format, (int)arg);
			if (num == 0)
			{
				return string.Empty;
			}
			if (num < 0)
			{
				throw new InvalidOperationException("The printf operation failed");
			}
			byte[] array = new byte[num + 1];
			byte[] array2;
			byte* str;
			if ((array2 = array) == null || array2.Length == 0)
			{
				str = null;
			}
			else
			{
				str = &array2[0];
			}
			if (((text != null) ? ParameterizedStrings.snprintf(str, (IntPtr)array.Length, format, text) : ParameterizedStrings.snprintf(str, (IntPtr)array.Length, format, (int)arg)) != num)
			{
				throw new InvalidOperationException("Invalid printf operation");
			}
			array2 = null;
			return ParameterizedStrings.StringFromAsciiBytes(array, 0, num);
		}

		// Token: 0x06001AA2 RID: 6818 RVA: 0x000652B0 File Offset: 0x000634B0
		private static ParameterizedStrings.FormatParam[] GetDynamicOrStaticVariables(char c, ref ParameterizedStrings.FormatParam[] dynamicVars, ref ParameterizedStrings.FormatParam[] staticVars, out int index)
		{
			if (c >= 'A' && c <= 'Z')
			{
				index = (int)(c - 'A');
				ParameterizedStrings.FormatParam[] result;
				if ((result = staticVars) == null)
				{
					ParameterizedStrings.FormatParam[] array;
					staticVars = (array = new ParameterizedStrings.FormatParam[26]);
					result = array;
				}
				return result;
			}
			if (c >= 'a' && c <= 'z')
			{
				index = (int)(c - 'a');
				ParameterizedStrings.FormatParam[] result2;
				if ((result2 = dynamicVars) == null)
				{
					ParameterizedStrings.FormatParam[] array;
					dynamicVars = (array = new ParameterizedStrings.FormatParam[26]);
					result2 = array;
				}
				return result2;
			}
			throw new InvalidOperationException("Terminfo database contains invalid values");
		}

		// Token: 0x04000D48 RID: 3400
		[ThreadStatic]
		private static ParameterizedStrings.LowLevelStack _cachedStack;

		// Token: 0x02000228 RID: 552
		public struct FormatParam
		{
			// Token: 0x06001AA3 RID: 6819 RVA: 0x0006530D File Offset: 0x0006350D
			public FormatParam(int value)
			{
				this = new ParameterizedStrings.FormatParam(value, null);
			}

			// Token: 0x06001AA4 RID: 6820 RVA: 0x00065317 File Offset: 0x00063517
			public FormatParam(string value)
			{
				this = new ParameterizedStrings.FormatParam(0, value ?? string.Empty);
			}

			// Token: 0x06001AA5 RID: 6821 RVA: 0x0006532A File Offset: 0x0006352A
			private FormatParam(int intValue, string stringValue)
			{
				this._int32 = intValue;
				this._string = stringValue;
			}

			// Token: 0x06001AA6 RID: 6822 RVA: 0x0006533A File Offset: 0x0006353A
			public static implicit operator ParameterizedStrings.FormatParam(int value)
			{
				return new ParameterizedStrings.FormatParam(value);
			}

			// Token: 0x06001AA7 RID: 6823 RVA: 0x00065342 File Offset: 0x00063542
			public static implicit operator ParameterizedStrings.FormatParam(string value)
			{
				return new ParameterizedStrings.FormatParam(value);
			}

			// Token: 0x17000392 RID: 914
			// (get) Token: 0x06001AA8 RID: 6824 RVA: 0x0006534A File Offset: 0x0006354A
			public int Int32
			{
				get
				{
					return this._int32;
				}
			}

			// Token: 0x17000393 RID: 915
			// (get) Token: 0x06001AA9 RID: 6825 RVA: 0x00065352 File Offset: 0x00063552
			public string String
			{
				get
				{
					return this._string ?? string.Empty;
				}
			}

			// Token: 0x17000394 RID: 916
			// (get) Token: 0x06001AAA RID: 6826 RVA: 0x00065363 File Offset: 0x00063563
			public object Object
			{
				get
				{
					return this._string ?? this._int32;
				}
			}

			// Token: 0x04000D49 RID: 3401
			private readonly int _int32;

			// Token: 0x04000D4A RID: 3402
			private readonly string _string;
		}

		// Token: 0x02000229 RID: 553
		private sealed class LowLevelStack
		{
			// Token: 0x06001AAB RID: 6827 RVA: 0x0006537A File Offset: 0x0006357A
			public LowLevelStack()
			{
				this._arr = new ParameterizedStrings.FormatParam[4];
			}

			// Token: 0x06001AAC RID: 6828 RVA: 0x00065390 File Offset: 0x00063590
			public ParameterizedStrings.FormatParam Pop()
			{
				if (this._count == 0)
				{
					throw new InvalidOperationException("Terminfo: Invalid Stack");
				}
				ParameterizedStrings.FormatParam[] arr = this._arr;
				int num = this._count - 1;
				this._count = num;
				ParameterizedStrings.FormatParam result = arr[num];
				this._arr[this._count] = default(ParameterizedStrings.FormatParam);
				return result;
			}

			// Token: 0x06001AAD RID: 6829 RVA: 0x000653E4 File Offset: 0x000635E4
			public void Push(ParameterizedStrings.FormatParam item)
			{
				if (this._arr.Length == this._count)
				{
					ParameterizedStrings.FormatParam[] array = new ParameterizedStrings.FormatParam[this._arr.Length * 2];
					Array.Copy(this._arr, 0, array, 0, this._arr.Length);
					this._arr = array;
				}
				ParameterizedStrings.FormatParam[] arr = this._arr;
				int count = this._count;
				this._count = count + 1;
				arr[count] = item;
			}

			// Token: 0x06001AAE RID: 6830 RVA: 0x0006544B File Offset: 0x0006364B
			public void Clear()
			{
				Array.Clear(this._arr, 0, this._count);
				this._count = 0;
			}

			// Token: 0x04000D4B RID: 3403
			private const int DefaultSize = 4;

			// Token: 0x04000D4C RID: 3404
			private ParameterizedStrings.FormatParam[] _arr;

			// Token: 0x04000D4D RID: 3405
			private int _count;
		}
	}
}
