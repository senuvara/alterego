﻿using System;
using System.Reflection;
using System.Runtime.InteropServices;

namespace System
{
	/// <summary>Provides data for loader resolution events, such as the <see cref="E:System.AppDomain.TypeResolve" />, <see cref="E:System.AppDomain.ResourceResolve" />, <see cref="E:System.AppDomain.ReflectionOnlyAssemblyResolve" />, and <see cref="E:System.AppDomain.AssemblyResolve" /> events.</summary>
	// Token: 0x0200021E RID: 542
	[ComVisible(true)]
	public class ResolveEventArgs : EventArgs
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.ResolveEventArgs" /> class, specifying the name of the item to resolve.</summary>
		/// <param name="name">The name of an item to resolve. </param>
		// Token: 0x06001A03 RID: 6659 RVA: 0x00062683 File Offset: 0x00060883
		public ResolveEventArgs(string name)
		{
			this.m_Name = name;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ResolveEventArgs" /> class, specifying the name of the item to resolve and the assembly whose dependency is being resolved.</summary>
		/// <param name="name">The name of an item to resolve. </param>
		/// <param name="requestingAssembly">The assembly whose dependency is being resolved.</param>
		// Token: 0x06001A04 RID: 6660 RVA: 0x00062692 File Offset: 0x00060892
		public ResolveEventArgs(string name, Assembly requestingAssembly)
		{
			this.m_Name = name;
			this.m_Requesting = requestingAssembly;
		}

		/// <summary>Gets the name of the item to resolve.</summary>
		/// <returns>The name of the item to resolve.</returns>
		// Token: 0x17000379 RID: 889
		// (get) Token: 0x06001A05 RID: 6661 RVA: 0x000626A8 File Offset: 0x000608A8
		public string Name
		{
			get
			{
				return this.m_Name;
			}
		}

		/// <summary>Gets the assembly whose dependency is being resolved.</summary>
		/// <returns>The assembly that requested the item specified by the <see cref="P:System.ResolveEventArgs.Name" /> property.</returns>
		// Token: 0x1700037A RID: 890
		// (get) Token: 0x06001A06 RID: 6662 RVA: 0x000626B0 File Offset: 0x000608B0
		public Assembly RequestingAssembly
		{
			get
			{
				return this.m_Requesting;
			}
		}

		// Token: 0x04000CE6 RID: 3302
		private string m_Name;

		// Token: 0x04000CE7 RID: 3303
		private Assembly m_Requesting;
	}
}
