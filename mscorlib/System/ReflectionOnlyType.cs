﻿using System;

namespace System
{
	// Token: 0x020001AA RID: 426
	[Serializable]
	internal class ReflectionOnlyType : RuntimeType
	{
		// Token: 0x06001289 RID: 4745 RVA: 0x0004C99E File Offset: 0x0004AB9E
		private ReflectionOnlyType()
		{
		}

		// Token: 0x17000229 RID: 553
		// (get) Token: 0x0600128A RID: 4746 RVA: 0x0004C9A6 File Offset: 0x0004ABA6
		public override RuntimeTypeHandle TypeHandle
		{
			get
			{
				throw new InvalidOperationException(Environment.GetResourceString("The requested operation is invalid in the ReflectionOnly context."));
			}
		}
	}
}
