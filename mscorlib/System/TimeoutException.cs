﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System
{
	/// <summary>The exception that is thrown when the time allotted for a process or operation has expired.</summary>
	// Token: 0x020001BB RID: 443
	[ComVisible(true)]
	[Serializable]
	public class TimeoutException : SystemException
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.TimeoutException" /> class.</summary>
		// Token: 0x060013E4 RID: 5092 RVA: 0x0005124A File Offset: 0x0004F44A
		public TimeoutException() : base(Environment.GetResourceString("The operation has timed out."))
		{
			base.SetErrorCode(-2146233083);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.TimeoutException" /> class with the specified error message.</summary>
		/// <param name="message">The message that describes the error. </param>
		// Token: 0x060013E5 RID: 5093 RVA: 0x00051267 File Offset: 0x0004F467
		public TimeoutException(string message) : base(message)
		{
			base.SetErrorCode(-2146233083);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.TimeoutException" /> class with the specified error message and inner exception.</summary>
		/// <param name="message">The message that describes the error. </param>
		/// <param name="innerException">The exception that is the cause of the current exception. If the <paramref name="innerException" /> parameter is not <see langword="null" />, the current exception is raised in a <see langword="catch" /> block that handles the inner exception. </param>
		// Token: 0x060013E6 RID: 5094 RVA: 0x0005127B File Offset: 0x0004F47B
		public TimeoutException(string message, Exception innerException) : base(message, innerException)
		{
			base.SetErrorCode(-2146233083);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.TimeoutException" /> class with serialized data.</summary>
		/// <param name="info">The <see cref="T:System.Runtime.Serialization.SerializationInfo" /> object that contains serialized object data about the exception being thrown. </param>
		/// <param name="context">The <see cref="T:System.Runtime.Serialization.StreamingContext" /> object that contains contextual information about the source or destination. The <paramref name="context" /> parameter is reserved for future use, and can be specified as <see langword="null" />.</param>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="info" /> parameter is <see langword="null" />. </exception>
		/// <exception cref="T:System.Runtime.Serialization.SerializationException">The class name is <see langword="null" />, or <see cref="P:System.Exception.HResult" /> is zero (0). </exception>
		// Token: 0x060013E7 RID: 5095 RVA: 0x000319C9 File Offset: 0x0002FBC9
		protected TimeoutException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
}
