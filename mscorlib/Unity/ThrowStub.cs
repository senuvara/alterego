﻿using System;

namespace Unity
{
	// Token: 0x02000AB2 RID: 2738
	internal sealed class ThrowStub : ObjectDisposedException
	{
		// Token: 0x06005F75 RID: 24437 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public static void ThrowNotSupportedException()
		{
			throw new PlatformNotSupportedException();
		}
	}
}
