﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// Token: 0x02000A40 RID: 2624
[CompilerGenerated]
internal sealed class <PrivateImplementationDetails>
{
	// Token: 0x06005E4B RID: 24139 RVA: 0x00130084 File Offset: 0x0012E284
	internal static uint ComputeStringHash(string s)
	{
		uint num;
		if (s != null)
		{
			num = 2166136261U;
			for (int i = 0; i < s.Length; i++)
			{
				num = ((uint)s[i] ^ num) * 16777619U;
			}
		}
		return num;
	}

	// Token: 0x04002EBC RID: 11964 RVA: 0x0018BF7C File Offset: 0x0018A17C
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=256 0392525BCB01691D1F319D89F2C12BF93A478467;

	// Token: 0x04002EBD RID: 11965 RVA: 0x0018C07C File Offset: 0x0018A27C
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=72 0588059ACBD52F7EA2835882F977A9CF72EB9775;

	// Token: 0x04002EBE RID: 11966 RVA: 0x0018C0C4 File Offset: 0x0018A2C4
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=84 0A1ADB22C1D3E1F4B2448EE3F27DF9DE63329C4C;

	// Token: 0x04002EBF RID: 11967 RVA: 0x0018C118 File Offset: 0x0018A318
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=48 0E171F67DF35B781B61D8F79B0E59EE21F80A595;

	// Token: 0x04002EC0 RID: 11968 RVA: 0x0018C148 File Offset: 0x0018A348
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=24 101E9DD9BE95D87DABF071EC0007923778409A35;

	// Token: 0x04002EC1 RID: 11969 RVA: 0x0018C160 File Offset: 0x0018A360
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=52 1072FDC2B3F14476527F9AFDC6A9A3365CDD4520;

	// Token: 0x04002EC2 RID: 11970 RVA: 0x0018C194 File Offset: 0x0018A394
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=240 121EC59E23F7559B28D338D562528F6299C2DE22;

	// Token: 0x04002EC3 RID: 11971 RVA: 0x0018C284 File Offset: 0x0018A484
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=3 12D04472A8285260EA12FD3813CDFA9F2D2B548C;

	// Token: 0x04002EC4 RID: 11972 RVA: 0x0018C287 File Offset: 0x0018A487
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=3 13A35EF1A549297C70E2AD46045BBD2ECA17852D;

	// Token: 0x04002EC5 RID: 11973 RVA: 0x0018C28A File Offset: 0x0018A48A
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=24 1730F09044E91DB8371B849EFF5E6D17BDE4AED0;

	// Token: 0x04002EC6 RID: 11974 RVA: 0x0018C2A2 File Offset: 0x0018A4A2
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=2224 18ABA12DD09EAEC266A410595FF8397550CB69F3;

	// Token: 0x04002EC7 RID: 11975 RVA: 0x0018CB52 File Offset: 0x0018AD52
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=3 1A84029C80CB5518379F199F53FF08A7B764F8FD;

	// Token: 0x04002EC8 RID: 11976 RVA: 0x0018CB55 File Offset: 0x0018AD55
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=4096 1AEF3D8DF416A46288C91C724CBF7B154D9E5BF3;

	// Token: 0x04002EC9 RID: 11977 RVA: 0x0018DB55 File Offset: 0x0018BD55
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=2048 1E41C4CD0767AEA21C00DEABA2EA9407F1E6CEA5;

	// Token: 0x04002ECA RID: 11978 RVA: 0x0018E355 File Offset: 0x0018C555
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=16 1FE6CE411858B3D864679DE2139FB081F08BFACD;

	// Token: 0x04002ECB RID: 11979 RVA: 0x0018E365 File Offset: 0x0018C565
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=24 211DEF42B95FD6AB230AA0439D7B1B4125F71291;

	// Token: 0x04002ECC RID: 11980 RVA: 0x0018E37D File Offset: 0x0018C57D
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=3 235D99572263B22ADFEE10FDA0C25E12F4D94FFC;

	// Token: 0x04002ECD RID: 11981 RVA: 0x0018E380 File Offset: 0x0018C580
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=40 25420D0055076FA8D3E4DD96BC53AE24DE6E619F;

	// Token: 0x04002ECE RID: 11982 RVA: 0x0018E3A8 File Offset: 0x0018C5A8
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=1208 25CF935D2AE9EDF05DD75BCD47FF84D9255D6F6E;

	// Token: 0x04002ECF RID: 11983 RVA: 0x0018E860 File Offset: 0x0018CA60
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=42 29C1A61550F0E3260E1953D4FAD71C256218EF40;

	// Token: 0x04002ED0 RID: 11984 RVA: 0x0018E88A File Offset: 0x0018CA8A
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=12 2B33BEC8C30DFDC49DAFE20D3BDE19487850D717;

	// Token: 0x04002ED1 RID: 11985 RVA: 0x0018E896 File Offset: 0x0018CA96
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=24 2B6224F32C7F4129F429E6AE11030AB9FA140D91;

	// Token: 0x04002ED2 RID: 11986 RVA: 0x0018E8AE File Offset: 0x0018CAAE
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=36 2BA840FF6020B8FF623DBCB7188248CF853FAF4F;

	// Token: 0x04002ED3 RID: 11987 RVA: 0x0018E8D2 File Offset: 0x0018CAD2
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=6 2BDE6A90DA8A72A009B639C351305719BFAC31BB;

	// Token: 0x04002ED4 RID: 11988 RVA: 0x0018E8D8 File Offset: 0x0018CAD8
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=72 2C840AFA48C27B9C05593E468C1232CA1CC74AFD;

	// Token: 0x04002ED5 RID: 11989 RVA: 0x0018E920 File Offset: 0x0018CB20
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=16 2D1DA5BB407F0C11C3B5116196C0C6374D932B20;

	// Token: 0x04002ED6 RID: 11990 RVA: 0x0018E930 File Offset: 0x0018CB30
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=14 2D3CF0F15AC2DDEC2956EA1B7BBE43FB8B923130;

	// Token: 0x04002ED7 RID: 11991 RVA: 0x0018E93E File Offset: 0x0018CB3E
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=72 2F71D2DA12F3CD0A6A112F5A5A75B4FDC6FE8547;

	// Token: 0x04002ED8 RID: 11992 RVA: 0x0018E986 File Offset: 0x0018CB86
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=64 320B018758ECE3752FFEDBAEB1A6DB67C80B9359;

	// Token: 0x04002ED9 RID: 11993 RVA: 0x0018E9C6 File Offset: 0x0018CBC6
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=72 34476C29F6F81C989CFCA42F7C06E84C66236834;

	// Token: 0x04002EDA RID: 11994 RVA: 0x0018EA0E File Offset: 0x0018CC0E
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=2382 35EED060772F2748D13B745DAEC8CD7BD3B87604;

	// Token: 0x04002EDB RID: 11995 RVA: 0x0018F35C File Offset: 0x0018D55C
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=38 375F9AE9769A3D1DA789E9ACFE81F3A1BB14F0D3;

	// Token: 0x04002EDC RID: 11996 RVA: 0x0018F382 File Offset: 0x0018D582
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=1450 379C06C9E702D31469C29033F0DD63931EB349F5;

	// Token: 0x04002EDD RID: 11997 RVA: 0x0018F92C File Offset: 0x0018DB2C
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=10 399BD13E240F33F808CA7940293D6EC4E6FD5A00;

	// Token: 0x04002EDE RID: 11998 RVA: 0x0018F936 File Offset: 0x0018DB36
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=72 39C9CE73C7B0619D409EF28344F687C1B5C130FE;

	// Token: 0x04002EDF RID: 11999 RVA: 0x0018F97E File Offset: 0x0018DB7E
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=320 3C53AFB51FEC23491684C7BEDBC6D4E0F409F851;

	// Token: 0x04002EE0 RID: 12000 RVA: 0x0018FABE File Offset: 0x0018DCBE
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=1440 3D4480E43F2013201B4A95D1AB9224A6A823E283;

	// Token: 0x04002EE1 RID: 12001 RVA: 0x0019005E File Offset: 0x0018E25E
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=3 3E3442C7396F3F2BB4C7348F4A2074C7DC677D68;

	// Token: 0x04002EE2 RID: 12002 RVA: 0x00190061 File Offset: 0x0018E261
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=12 3E823444D2DFECF0F90B436B88F02A533CB376F1;

	// Token: 0x04002EE3 RID: 12003 RVA: 0x0019006D File Offset: 0x0018E26D
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=72 3FE6C283BCF384FD2C8789880DFF59664E2AB4A1;

	// Token: 0x04002EE4 RID: 12004 RVA: 0x001900B5 File Offset: 0x0018E2B5
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=1665 40981BAA39513E58B28DCF0103CC04DE2A0A0444;

	// Token: 0x04002EE5 RID: 12005 RVA: 0x00190736 File Offset: 0x0018E936
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=40 40E7C49413D261F3F38AD3A870C0AC69C8BDA048;

	// Token: 0x04002EE6 RID: 12006 RVA: 0x0019075E File Offset: 0x0018E95E
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=72 421EC7E82F2967DF6CA8C3605514DC6F29EE5845;

	// Token: 0x04002EE7 RID: 12007 RVA: 0x001907A6 File Offset: 0x0018E9A6
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=256 433175D38B13FFE177FDD661A309F1B528B3F6E2;

	// Token: 0x04002EE8 RID: 12008 RVA: 0x001908A6 File Offset: 0x0018EAA6
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=18128 43A5C779787F9EA37913539708165A6C752EB99E;

	// Token: 0x04002EE9 RID: 12009 RVA: 0x00194F76 File Offset: 0x00193176
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=120 46232052BC757E030490D851F265FB47FA100902;

	// Token: 0x04002EEA RID: 12010 RVA: 0x00194FEE File Offset: 0x001931EE
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=72 4858DB4AA76D3933F1CA9E6712D4FDB16903F628;

	// Token: 0x04002EEB RID: 12011 RVA: 0x00195036 File Offset: 0x00193236
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=48 4E3B533C39447AAEB59A8E48FABD7E15B5B5D195;

	// Token: 0x04002EEC RID: 12012 RVA: 0x00195066 File Offset: 0x00193266
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=40 4F7A8890F332B22B8DE0BD29D36FA7364748D76A;

	// Token: 0x04002EED RID: 12013 RVA: 0x0019508E File Offset: 0x0019328E
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=72 536422B321459B242ADED7240B7447E904E083E3;

	// Token: 0x04002EEE RID: 12014 RVA: 0x001950D6 File Offset: 0x001932D6
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=1080 5382CEF491F422BFE0D6FC46EFAFF9EF9D4C89F3;

	// Token: 0x04002EEF RID: 12015 RVA: 0x0019550E File Offset: 0x0019370E
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=10 56DFA5053B3131883637F53219E7D88CCEF35949;

	// Token: 0x04002EF0 RID: 12016 RVA: 0x00195518 File Offset: 0x00193718
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=3 57218C316B6921E2CD61027A2387EDC31A2D9471;

	// Token: 0x04002EF1 RID: 12017 RVA: 0x0019551B File Offset: 0x0019371B
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=40 57F320D62696EC99727E0FE2045A05F1289CC0C6;

	// Token: 0x04002EF2 RID: 12018 RVA: 0x00195543 File Offset: 0x00193743
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=212 594A33A00BC4F785DFD43E3C6C44FBA1242CCAF3;

	// Token: 0x04002EF3 RID: 12019 RVA: 0x00195617 File Offset: 0x00193817
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=36 5BBDF8058D4235C33F2E8DCF76004031B6187A2F;

	// Token: 0x04002EF4 RID: 12020 RVA: 0x0019563B File Offset: 0x0019383B
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=288 5BCD21C341BE6DDF8FFFAE1A23ABA24DCBB612BF;

	// Token: 0x04002EF5 RID: 12021 RVA: 0x0019575B File Offset: 0x0019395B
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=72 5BFE2819B4778217C56416C7585FF0E56EBACD89;

	// Token: 0x04002EF6 RID: 12022 RVA: 0x001957A3 File Offset: 0x001939A3
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=52 5EDFD8200B7AD157878FBA46F22A1C345662F2E3;

	// Token: 0x04002EF7 RID: 12023 RVA: 0x001957D7 File Offset: 0x001939D7
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=128 609C0E8D8DA86A09D6013D301C86BA8782C16B8C;

	// Token: 0x04002EF8 RID: 12024 RVA: 0x00195857 File Offset: 0x00193A57
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=3200 61D51C3387EB82B6D7BC9C61539B3713CCF850AE;

	// Token: 0x04002EF9 RID: 12025 RVA: 0x001964D7 File Offset: 0x001946D7
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=48 62BAB0F245E66C3EB982CF5A7015F0A7C3382283;

	// Token: 0x04002EFA RID: 12026 RVA: 0x00196507 File Offset: 0x00194707
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=2048 646036A65DECCD6835C914A46E6E44B729433B60;

	// Token: 0x04002EFB RID: 12027 RVA: 0x00196D07 File Offset: 0x00194F07
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=40 65E32B4E150FD8D24B93B0D42A17F1DAD146162B;

	// Token: 0x04002EFC RID: 12028 RVA: 0x00196D2F File Offset: 0x00194F2F
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=52 6770974FEF1E98B9C1864370E2B5B786EB0EA39E;

	// Token: 0x04002EFD RID: 12029 RVA: 0x00196D63 File Offset: 0x00194F63
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=72 67EEAD805D708D9AA4E14BF747E44CED801744F3;

	// Token: 0x04002EFE RID: 12030 RVA: 0x00196DAB File Offset: 0x00194FAB
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=120 6C71197D228427B2864C69B357FEF73D8C9D59DF;

	// Token: 0x04002EFF RID: 12031 RVA: 0x00196E23 File Offset: 0x00195023
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=9 6D49C9D487D7AD3491ECE08732D68A593CC2038D;

	// Token: 0x04002F00 RID: 12032 RVA: 0x00196E2C File Offset: 0x0019502C
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=2048 6D797C11E1D4FB68B6570CF2A92B792433527065;

	// Token: 0x04002F01 RID: 12033 RVA: 0x0019762C File Offset: 0x0019582C
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=3132 6E5DC824F803F8565AF31B42199DAE39FE7F4EA9;

	// Token: 0x04002F02 RID: 12034 RVA: 0x00198268 File Offset: 0x00196468
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=76 6FC754859E4EC74E447048364B216D825C6F8FE7;

	// Token: 0x04002F03 RID: 12035 RVA: 0x001982B4 File Offset: 0x001964B4
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=40 704939CD172085D1295FCE3F1D92431D685D7AA2;

	// Token: 0x04002F04 RID: 12036 RVA: 0x001982DC File Offset: 0x001964DC
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=24 7088AAE49F0627B72729078DE6E3182DDCF8ED99;

	// Token: 0x04002F05 RID: 12037 RVA: 0x001982F4 File Offset: 0x001964F4
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=72 7341C933A70EAE383CC50C4B945ADB8E08F06737;

	// Token: 0x04002F06 RID: 12038 RVA: 0x0019833C File Offset: 0x0019653C
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=3 736D39815215889F11249D9958F6ED12D37B9F57;

	// Token: 0x04002F07 RID: 12039 RVA: 0x0019833F File Offset: 0x0019653F
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=1472 75E0C38E75E507EDE41C1C4F5F5612852B31A4EF;

	// Token: 0x04002F08 RID: 12040 RVA: 0x001988FF File Offset: 0x00196AFF
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=4096 7F42F2EDC974BE29B2746957416ED1AEFA605F47;

	// Token: 0x04002F09 RID: 12041 RVA: 0x001998FF File Offset: 0x00197AFF
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=40 7FE820C9CF0F0B90445A71F1D262D22E4F0C4C68;

	// Token: 0x04002F0A RID: 12042 RVA: 0x00199927 File Offset: 0x00197B27
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=21252 811A927B7DADD378BE60BBDE794B9277AA9B50EC;

	// Token: 0x04002F0B RID: 12043 RVA: 0x0019EC2B File Offset: 0x0019CE2B
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=36 81917F1E21F3C22B9F916994547A614FB03E968E;

	// Token: 0x04002F0C RID: 12044 RVA: 0x0019EC4F File Offset: 0x0019CE4F
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=40 823566DA642D6EA356E15585921F2A4CA23D6760;

	// Token: 0x04002F0D RID: 12045 RVA: 0x0019EC77 File Offset: 0x0019CE77
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=12 82C2A59850B2E85BCE1A45A479537A384DF6098D;

	// Token: 0x04002F0E RID: 12046 RVA: 0x0019EC83 File Offset: 0x0019CE83
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=44 82C383F8E6E4D3D87AEBB986A5D0077E8AD157C4;

	// Token: 0x04002F0F RID: 12047 RVA: 0x0019ECAF File Offset: 0x0019CEAF
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=3 86F4F563FA2C61798AE6238D789139739428463A;

	// Token: 0x04002F10 RID: 12048 RVA: 0x0019ECB2 File Offset: 0x0019CEB2
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=40 871B9CF85DB352BAADF12BAE8F19857683E385AC;

	// Token: 0x04002F11 RID: 12049 RVA: 0x0019ECDA File Offset: 0x0019CEDA
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=16 89A040451C8CC5C8FB268BE44BDD74964C104155;

	// Token: 0x04002F12 RID: 12050 RVA: 0x0019ECEA File Offset: 0x0019CEEA
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=3 8AEFB06C426E07A0A671A1E2488B4858D694A730;

	// Token: 0x04002F13 RID: 12051 RVA: 0x0019ECED File Offset: 0x0019CEED
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=40 8CAA092E783257106251246FF5C97F88D28517A6;

	// Token: 0x04002F14 RID: 12052 RVA: 0x0019ED15 File Offset: 0x0019CF15
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=2100 8D231DD55FE1AD7631BBD0905A17D5EB616C2154;

	// Token: 0x04002F15 RID: 12053 RVA: 0x0019F549 File Offset: 0x0019D749
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=40 8E10AC2F34545DFBBF3FCBC06055D797A8C99991;

	// Token: 0x04002F16 RID: 12054 RVA: 0x0019F571 File Offset: 0x0019D771
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=256 8F22C9ECE1331718CBD268A9BBFD2F5E451441E3;

	// Token: 0x04002F17 RID: 12055 RVA: 0x0019F671 File Offset: 0x0019D871
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=5264 8F6B6AC8FBF72E6E180F63A143400289ABA06FD8;

	// Token: 0x04002F18 RID: 12056 RVA: 0x001A0B01 File Offset: 0x0019ED01
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=32 8FCC900D91A3291D6A06E115C795754507E3FB70;

	// Token: 0x04002F19 RID: 12057 RVA: 0x001A0B21 File Offset: 0x0019ED21
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=640 90A0542282A011472F94E97CEAE59F8B3B1A3291;

	// Token: 0x04002F1A RID: 12058 RVA: 0x001A0DA1 File Offset: 0x0019EFA1
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=12 93A63E90605400F34B49F0EB3361D23C89164BDA;

	// Token: 0x04002F1B RID: 12059 RVA: 0x001A0DAD File Offset: 0x0019EFAD
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=72 94841DD2F330CCB1089BF413E4FA9B04505152E2;

	// Token: 0x04002F1C RID: 12060 RVA: 0x001A0DF5 File Offset: 0x0019EFF5
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=12 95264589E48F94B7857CFF398FB72A537E13EEE2;

	// Token: 0x04002F1D RID: 12061 RVA: 0x001A0E01 File Offset: 0x0019F001
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=72 95C48758CAE1715783472FB073AB158AB8A0AB2A;

	// Token: 0x04002F1E RID: 12062 RVA: 0x001A0E49 File Offset: 0x0019F049
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=72 973417296623D8DC6961B09664E54039E44CA5D8;

	// Token: 0x04002F1F RID: 12063 RVA: 0x001A0E91 File Offset: 0x0019F091
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=3 97FB30C84FF4A41CD4625B44B2940BFC8DB43003;

	// Token: 0x04002F20 RID: 12064 RVA: 0x001A0E94 File Offset: 0x0019F094
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=4096 99E2E88877D14C7DDC4E957A0ED7079CA0E9EB24;

	// Token: 0x04002F21 RID: 12065 RVA: 0x001A1E94 File Offset: 0x001A0094
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=32 99E9CC3E542086166B51A606E9343AFE781D898B;

	// Token: 0x04002F22 RID: 12066 RVA: 0x001A1EB4 File Offset: 0x001A00B4
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=64 9A9C3962CD4753376E3507C8CB5FD8FCC4B4EDB5;

	// Token: 0x04002F23 RID: 12067 RVA: 0x001A1EF4 File Offset: 0x001A00F4
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=3 9BB00D1FCCBAF03165447FC8028E7CA07CA9FE88;

	// Token: 0x04002F24 RID: 12068 RVA: 0x001A1EF7 File Offset: 0x001A00F7
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=48 9F6EFAE188929CBC060C013CA2CA76C46E07F4B6;

	// Token: 0x04002F25 RID: 12069 RVA: 0x001A1F27 File Offset: 0x001A0127
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=40 A0074C15377C0C870B055927403EA9FA7A349D12;

	// Token: 0x04002F26 RID: 12070 RVA: 0x001A1F4F File Offset: 0x001A014F
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=130 A1319B706116AB2C6D44483F60A7D0ACEA543396;

	// Token: 0x04002F27 RID: 12071 RVA: 0x001A1FD1 File Offset: 0x001A01D1
	internal static readonly long A13AA52274D951A18029131A8DDECF76B569A15D;

	// Token: 0x04002F28 RID: 12072 RVA: 0x001A1FD9 File Offset: 0x001A01D9
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=3 A323DB0813C4D072957BA6FDA79D9776674CD06B;

	// Token: 0x04002F29 RID: 12073 RVA: 0x001A1FDC File Offset: 0x001A01DC
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=212 A5444763673307F6828C748D4B9708CFC02B0959;

	// Token: 0x04002F2A RID: 12074 RVA: 0x001A20B0 File Offset: 0x001A02B0
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=72 A6732F8E7FC23766AB329B492D6BF82E3B33233F;

	// Token: 0x04002F2B RID: 12075 RVA: 0x001A20F8 File Offset: 0x001A02F8
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=174 A705A106D95282BD15E13EEA6B0AF583FF786D83;

	// Token: 0x04002F2C RID: 12076 RVA: 0x001A21A6 File Offset: 0x001A03A6
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=1018 A8A491E4CED49AE0027560476C10D933CE70C8DF;

	// Token: 0x04002F2D RID: 12077 RVA: 0x001A25A0 File Offset: 0x001A07A0
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=72 AC791C4F39504D1184B73478943D0636258DA7B1;

	// Token: 0x04002F2E RID: 12078 RVA: 0x001A25E8 File Offset: 0x001A07E8
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=64 AEACDA021EBCFBF4FDEDBADB60A0D72CB3DB1B32;

	// Token: 0x04002F2F RID: 12079 RVA: 0x001A2628 File Offset: 0x001A0828
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=52 AFCD4E1211233E99373A3367B23105A3D624B1F2;

	// Token: 0x04002F30 RID: 12080 RVA: 0x001A265C File Offset: 0x001A085C
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=24 B2E36E778AB1CBC2AC2AA7286FBA1657C3D1B734;

	// Token: 0x04002F31 RID: 12081 RVA: 0x001A2674 File Offset: 0x001A0874
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=40 B472ED77CB3B2A66D49D179F1EE2081B70A6AB61;

	// Token: 0x04002F32 RID: 12082 RVA: 0x001A269C File Offset: 0x001A089C
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=16 B4FBD02AAB5B16E0F4BD858DA5D9E348F3CE501D;

	// Token: 0x04002F33 RID: 12083 RVA: 0x001A26AC File Offset: 0x001A08AC
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=256 B53A2C6DF21FC88B17AEFC40EB895B8D63210CDF;

	// Token: 0x04002F34 RID: 12084 RVA: 0x001A27AC File Offset: 0x001A09AC
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=4096 B6002BBF29B2704922EC3BBF0F9EE40ABF185D6B;

	// Token: 0x04002F35 RID: 12085 RVA: 0x001A37AC File Offset: 0x001A19AC
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=998 B881DA88BE0B68D8A6B6B6893822586B8B2CFC45;

	// Token: 0x04002F36 RID: 12086 RVA: 0x001A3B92 File Offset: 0x001A1D92
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=162 B8864ACB9DD69E3D42151513C840AAE270BF21C8;

	// Token: 0x04002F37 RID: 12087 RVA: 0x001A3C34 File Offset: 0x001A1E34
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=360 B8F87834C3597B2EEF22BA6D3A392CC925636401;

	// Token: 0x04002F38 RID: 12088 RVA: 0x001A3D9C File Offset: 0x001A1F9C
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=72 B9B670F134A59FB1107AF01A9FE8F8E3980B3093;

	// Token: 0x04002F39 RID: 12089 RVA: 0x001A3DE4 File Offset: 0x001A1FE4
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=88 BB29ACCF07CCA7E579822BF2E43978D1161A1C3E;

	// Token: 0x04002F3A RID: 12090 RVA: 0x001A3E3C File Offset: 0x001A203C
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=20 BE1BDEC0AA74B4DCB079943E70528096CCA985F8;

	// Token: 0x04002F3B RID: 12091 RVA: 0x001A3E50 File Offset: 0x001A2050
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=72 BEBC9ECC660A13EFC359BA3383411F698CFF25DB;

	// Token: 0x04002F3C RID: 12092 RVA: 0x001A3E98 File Offset: 0x001A2098
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=40 BEE1CFE5DFAA408E14CE4AF4DCD824FA2E42DCB7;

	// Token: 0x04002F3D RID: 12093 RVA: 0x001A3EC0 File Offset: 0x001A20C0
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=3 BF477463CE2F5EF38FC4C644BBBF4DF109E7670A;

	// Token: 0x04002F3E RID: 12094 RVA: 0x001A3EC3 File Offset: 0x001A20C3
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=6 BF5EB60806ECB74EE484105DD9D6F463BF994867;

	// Token: 0x04002F3F RID: 12095 RVA: 0x001A3EC9 File Offset: 0x001A20C9
	internal static readonly long C1A1100642BA9685B30A84D97348484E14AA1865;

	// Token: 0x04002F40 RID: 12096 RVA: 0x001A3ED1 File Offset: 0x001A20D1
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=94 C35AB913B7CBEB243E050D1C7A61174F7C67D416;

	// Token: 0x04002F41 RID: 12097 RVA: 0x001A3F2F File Offset: 0x001A212F
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=16 C6F364A0AD934EFED8909446C215752E565D77C1;

	// Token: 0x04002F42 RID: 12098 RVA: 0x001A3F3F File Offset: 0x001A213F
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=392 C76DF2590ECCF0C28E0DBDFFA8A749BA3B910823;

	// Token: 0x04002F43 RID: 12099 RVA: 0x001A40C7 File Offset: 0x001A22C7
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=174 CE5835130F5277F63D716FC9115526B0AC68FFAD;

	// Token: 0x04002F44 RID: 12100 RVA: 0x001A4175 File Offset: 0x001A2375
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=6 CE93C35B755802BC4B3D180716B048FC61701EF7;

	// Token: 0x04002F45 RID: 12101 RVA: 0x001A417B File Offset: 0x001A237B
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=64 CF0B42666EF5E37EDEA0AB8E173E42C196D03814;

	// Token: 0x04002F46 RID: 12102 RVA: 0x001A41BB File Offset: 0x001A23BB
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=256 D002CBBE1FF33721AF7C4D1D3ECAD1B7DB5258B7;

	// Token: 0x04002F47 RID: 12103 RVA: 0x001A42BB File Offset: 0x001A24BB
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=32 D117188BE8D4609C0D531C51B0BB911A4219DEBE;

	// Token: 0x04002F48 RID: 12104 RVA: 0x001A42DB File Offset: 0x001A24DB
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=32 D28E8ABDBD777A482CE0EE5C24814ACAE52AABFE;

	// Token: 0x04002F49 RID: 12105 RVA: 0x001A42FB File Offset: 0x001A24FB
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=256 D2C5BAE967587C6F3D9F2C4551911E0575A1101F;

	// Token: 0x04002F4A RID: 12106 RVA: 0x001A43FB File Offset: 0x001A25FB
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=82 D76478B994B312CD022DCA207AA2254880D2FCC9;

	// Token: 0x04002F4B RID: 12107 RVA: 0x001A444D File Offset: 0x001A264D
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=44 D78D08081C7A5AD6FBA7A8DC86BCD6D7A577C636;

	// Token: 0x04002F4C RID: 12108 RVA: 0x001A4479 File Offset: 0x001A2679
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=76 DA19DB47B583EFCF7825D2E39D661D2354F28219;

	// Token: 0x04002F4D RID: 12109 RVA: 0x001A44C5 File Offset: 0x001A26C5
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=56 DC2B830D8CD59AD6A4E4332D21CA0DCA2821AD82;

	// Token: 0x04002F4E RID: 12110 RVA: 0x001A44FD File Offset: 0x001A26FD
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=52 DD3AEFEADB1CD615F3017763F1568179FEE640B0;

	// Token: 0x04002F4F RID: 12111 RVA: 0x001A4531 File Offset: 0x001A2731
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=36 E1827270A5FE1C85F5352A66FD87BA747213D006;

	// Token: 0x04002F50 RID: 12112 RVA: 0x001A4555 File Offset: 0x001A2755
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=40 E45BAB43F7D5D038672B3E3431F92E34A7AF2571;

	// Token: 0x04002F51 RID: 12113 RVA: 0x001A457D File Offset: 0x001A277D
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=64 E75835D001C843F156FBA01B001DFE1B8029AC17;

	// Token: 0x04002F52 RID: 12114 RVA: 0x001A45BD File Offset: 0x001A27BD
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=52 E92B39D8233061927D9ACDE54665E68E7535635A;

	// Token: 0x04002F53 RID: 12115 RVA: 0x001A45F1 File Offset: 0x001A27F1
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=12 EA9506959484C55CFE0C139C624DF6060E285866;

	// Token: 0x04002F54 RID: 12116 RVA: 0x001A45FD File Offset: 0x001A27FD
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=160 EB237D93FA22EEE4936E8D363A0AD5117F5F3FB0;

	// Token: 0x04002F55 RID: 12117 RVA: 0x001A469D File Offset: 0x001A289D
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=262 EB5E9A80A40096AB74D2E226650C7258D7BC5E9D;

	// Token: 0x04002F56 RID: 12118 RVA: 0x001A47A3 File Offset: 0x001A29A3
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=64 EBF68F411848D603D059DFDEA2321C5A5EA78044;

	// Token: 0x04002F57 RID: 12119 RVA: 0x001A47E3 File Offset: 0x001A29E3
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=10 EC5BB4F59D4B9B2E9ECD3904D44A8275F23AFB11;

	// Token: 0x04002F58 RID: 12120 RVA: 0x001A47ED File Offset: 0x001A29ED
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=3 EC83FB16C20052BEE2B4025159BC2ED45C9C70C3;

	// Token: 0x04002F59 RID: 12121 RVA: 0x001A47F0 File Offset: 0x001A29F0
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=72 EC89C317EA2BF49A70EFF5E89C691E34733D7C37;

	// Token: 0x04002F5A RID: 12122 RVA: 0x001A4838 File Offset: 0x001A2A38
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=24 F05ADA84174A5FD92D1C77CC1C26174EAEDB6BD5;

	// Token: 0x04002F5B RID: 12123 RVA: 0x001A4850 File Offset: 0x001A2A50
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=40 F06E829E62F3AFBC045D064E10A4F5DF7C969612;

	// Token: 0x04002F5C RID: 12124 RVA: 0x001A4878 File Offset: 0x001A2A78
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=11614 F073AA332018FDA0D572E99448FFF1D6422BD520;

	// Token: 0x04002F5D RID: 12125 RVA: 0x001A75D6 File Offset: 0x001A57D6
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=120 F34B0E10653402E8F788F8BC3F7CD7090928A429;

	// Token: 0x04002F5E RID: 12126 RVA: 0x001A764E File Offset: 0x001A584E
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=72 F37E34BEADB04F34FCC31078A59F49856CA83D5B;

	// Token: 0x04002F5F RID: 12127 RVA: 0x001A7696 File Offset: 0x001A5896
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=94 F512A9ABF88066AAEB92684F95CC05D8101B462B;

	// Token: 0x04002F60 RID: 12128 RVA: 0x001A76F4 File Offset: 0x001A58F4
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=12 F8FAABB821300AA500C2CEC6091B3782A7FB44A4;

	// Token: 0x04002F61 RID: 12129 RVA: 0x001A7700 File Offset: 0x001A5900
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=2350 FCBD2781A933F0828ED4AAF88FD8B08D76DDD49B;

	// Token: 0x02000A41 RID: 2625
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 3)]
	private struct __StaticArrayInitTypeSize=3
	{
	}

	// Token: 0x02000A42 RID: 2626
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 6)]
	private struct __StaticArrayInitTypeSize=6
	{
	}

	// Token: 0x02000A43 RID: 2627
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 9)]
	private struct __StaticArrayInitTypeSize=9
	{
	}

	// Token: 0x02000A44 RID: 2628
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 10)]
	private struct __StaticArrayInitTypeSize=10
	{
	}

	// Token: 0x02000A45 RID: 2629
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 12)]
	private struct __StaticArrayInitTypeSize=12
	{
	}

	// Token: 0x02000A46 RID: 2630
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 14)]
	private struct __StaticArrayInitTypeSize=14
	{
	}

	// Token: 0x02000A47 RID: 2631
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 16)]
	private struct __StaticArrayInitTypeSize=16
	{
	}

	// Token: 0x02000A48 RID: 2632
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 20)]
	private struct __StaticArrayInitTypeSize=20
	{
	}

	// Token: 0x02000A49 RID: 2633
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 24)]
	private struct __StaticArrayInitTypeSize=24
	{
	}

	// Token: 0x02000A4A RID: 2634
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 32)]
	private struct __StaticArrayInitTypeSize=32
	{
	}

	// Token: 0x02000A4B RID: 2635
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 36)]
	private struct __StaticArrayInitTypeSize=36
	{
	}

	// Token: 0x02000A4C RID: 2636
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 38)]
	private struct __StaticArrayInitTypeSize=38
	{
	}

	// Token: 0x02000A4D RID: 2637
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 40)]
	private struct __StaticArrayInitTypeSize=40
	{
	}

	// Token: 0x02000A4E RID: 2638
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 42)]
	private struct __StaticArrayInitTypeSize=42
	{
	}

	// Token: 0x02000A4F RID: 2639
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 44)]
	private struct __StaticArrayInitTypeSize=44
	{
	}

	// Token: 0x02000A50 RID: 2640
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 48)]
	private struct __StaticArrayInitTypeSize=48
	{
	}

	// Token: 0x02000A51 RID: 2641
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 52)]
	private struct __StaticArrayInitTypeSize=52
	{
	}

	// Token: 0x02000A52 RID: 2642
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 56)]
	private struct __StaticArrayInitTypeSize=56
	{
	}

	// Token: 0x02000A53 RID: 2643
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 64)]
	private struct __StaticArrayInitTypeSize=64
	{
	}

	// Token: 0x02000A54 RID: 2644
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 72)]
	private struct __StaticArrayInitTypeSize=72
	{
	}

	// Token: 0x02000A55 RID: 2645
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 76)]
	private struct __StaticArrayInitTypeSize=76
	{
	}

	// Token: 0x02000A56 RID: 2646
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 82)]
	private struct __StaticArrayInitTypeSize=82
	{
	}

	// Token: 0x02000A57 RID: 2647
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 84)]
	private struct __StaticArrayInitTypeSize=84
	{
	}

	// Token: 0x02000A58 RID: 2648
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 88)]
	private struct __StaticArrayInitTypeSize=88
	{
	}

	// Token: 0x02000A59 RID: 2649
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 94)]
	private struct __StaticArrayInitTypeSize=94
	{
	}

	// Token: 0x02000A5A RID: 2650
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 120)]
	private struct __StaticArrayInitTypeSize=120
	{
	}

	// Token: 0x02000A5B RID: 2651
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 128)]
	private struct __StaticArrayInitTypeSize=128
	{
	}

	// Token: 0x02000A5C RID: 2652
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 130)]
	private struct __StaticArrayInitTypeSize=130
	{
	}

	// Token: 0x02000A5D RID: 2653
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 160)]
	private struct __StaticArrayInitTypeSize=160
	{
	}

	// Token: 0x02000A5E RID: 2654
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 162)]
	private struct __StaticArrayInitTypeSize=162
	{
	}

	// Token: 0x02000A5F RID: 2655
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 174)]
	private struct __StaticArrayInitTypeSize=174
	{
	}

	// Token: 0x02000A60 RID: 2656
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 212)]
	private struct __StaticArrayInitTypeSize=212
	{
	}

	// Token: 0x02000A61 RID: 2657
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 240)]
	private struct __StaticArrayInitTypeSize=240
	{
	}

	// Token: 0x02000A62 RID: 2658
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 256)]
	private struct __StaticArrayInitTypeSize=256
	{
	}

	// Token: 0x02000A63 RID: 2659
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 262)]
	private struct __StaticArrayInitTypeSize=262
	{
	}

	// Token: 0x02000A64 RID: 2660
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 288)]
	private struct __StaticArrayInitTypeSize=288
	{
	}

	// Token: 0x02000A65 RID: 2661
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 320)]
	private struct __StaticArrayInitTypeSize=320
	{
	}

	// Token: 0x02000A66 RID: 2662
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 360)]
	private struct __StaticArrayInitTypeSize=360
	{
	}

	// Token: 0x02000A67 RID: 2663
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 392)]
	private struct __StaticArrayInitTypeSize=392
	{
	}

	// Token: 0x02000A68 RID: 2664
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 640)]
	private struct __StaticArrayInitTypeSize=640
	{
	}

	// Token: 0x02000A69 RID: 2665
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 998)]
	private struct __StaticArrayInitTypeSize=998
	{
	}

	// Token: 0x02000A6A RID: 2666
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 1018)]
	private struct __StaticArrayInitTypeSize=1018
	{
	}

	// Token: 0x02000A6B RID: 2667
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 1080)]
	private struct __StaticArrayInitTypeSize=1080
	{
	}

	// Token: 0x02000A6C RID: 2668
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 1208)]
	private struct __StaticArrayInitTypeSize=1208
	{
	}

	// Token: 0x02000A6D RID: 2669
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 1440)]
	private struct __StaticArrayInitTypeSize=1440
	{
	}

	// Token: 0x02000A6E RID: 2670
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 1450)]
	private struct __StaticArrayInitTypeSize=1450
	{
	}

	// Token: 0x02000A6F RID: 2671
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 1472)]
	private struct __StaticArrayInitTypeSize=1472
	{
	}

	// Token: 0x02000A70 RID: 2672
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 1665)]
	private struct __StaticArrayInitTypeSize=1665
	{
	}

	// Token: 0x02000A71 RID: 2673
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 2048)]
	private struct __StaticArrayInitTypeSize=2048
	{
	}

	// Token: 0x02000A72 RID: 2674
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 2100)]
	private struct __StaticArrayInitTypeSize=2100
	{
	}

	// Token: 0x02000A73 RID: 2675
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 2224)]
	private struct __StaticArrayInitTypeSize=2224
	{
	}

	// Token: 0x02000A74 RID: 2676
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 2350)]
	private struct __StaticArrayInitTypeSize=2350
	{
	}

	// Token: 0x02000A75 RID: 2677
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 2382)]
	private struct __StaticArrayInitTypeSize=2382
	{
	}

	// Token: 0x02000A76 RID: 2678
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 3132)]
	private struct __StaticArrayInitTypeSize=3132
	{
	}

	// Token: 0x02000A77 RID: 2679
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 3200)]
	private struct __StaticArrayInitTypeSize=3200
	{
	}

	// Token: 0x02000A78 RID: 2680
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 4096)]
	private struct __StaticArrayInitTypeSize=4096
	{
	}

	// Token: 0x02000A79 RID: 2681
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 5264)]
	private struct __StaticArrayInitTypeSize=5264
	{
	}

	// Token: 0x02000A7A RID: 2682
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 11614)]
	private struct __StaticArrayInitTypeSize=11614
	{
	}

	// Token: 0x02000A7B RID: 2683
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 18128)]
	private struct __StaticArrayInitTypeSize=18128
	{
	}

	// Token: 0x02000A7C RID: 2684
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 21252)]
	private struct __StaticArrayInitTypeSize=21252
	{
	}
}
