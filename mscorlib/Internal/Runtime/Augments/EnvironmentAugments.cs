﻿using System;

namespace Internal.Runtime.Augments
{
	// Token: 0x02000009 RID: 9
	internal static class EnvironmentAugments
	{
		// Token: 0x0600000D RID: 13 RVA: 0x000020C7 File Offset: 0x000002C7
		// Note: this type is marked as 'beforefieldinit'.
		static EnvironmentAugments()
		{
		}

		// Token: 0x0400036E RID: 878
		public static readonly string StackTrace = "";
	}
}
