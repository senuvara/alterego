﻿using System;
using System.Diagnostics;

namespace Internal.Runtime.CompilerServices
{
	// Token: 0x02000008 RID: 8
	[AttributeUsage(AttributeTargets.All)]
	[Conditional("ALWAYSREMOVED")]
	internal class RelocatedTypeAttribute : Attribute
	{
		// Token: 0x0600000C RID: 12 RVA: 0x000020BF File Offset: 0x000002BF
		public RelocatedTypeAttribute(string originalAssemblySimpleName)
		{
		}
	}
}
