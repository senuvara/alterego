﻿using System;
using Microsoft.Win32.SafeHandles;

namespace Microsoft.Win32
{
	// Token: 0x0200000B RID: 11
	internal interface IRegistryApi
	{
		// Token: 0x06000010 RID: 16
		RegistryKey CreateSubKey(RegistryKey rkey, string keyname);

		// Token: 0x06000011 RID: 17
		RegistryKey OpenRemoteBaseKey(RegistryHive hKey, string machineName);

		// Token: 0x06000012 RID: 18
		RegistryKey OpenSubKey(RegistryKey rkey, string keyname, bool writtable);

		// Token: 0x06000013 RID: 19
		void Flush(RegistryKey rkey);

		// Token: 0x06000014 RID: 20
		void Close(RegistryKey rkey);

		// Token: 0x06000015 RID: 21
		object GetValue(RegistryKey rkey, string name, object default_value, RegistryValueOptions options);

		// Token: 0x06000016 RID: 22
		RegistryValueKind GetValueKind(RegistryKey rkey, string name);

		// Token: 0x06000017 RID: 23
		void SetValue(RegistryKey rkey, string name, object value);

		// Token: 0x06000018 RID: 24
		int SubKeyCount(RegistryKey rkey);

		// Token: 0x06000019 RID: 25
		int ValueCount(RegistryKey rkey);

		// Token: 0x0600001A RID: 26
		void DeleteValue(RegistryKey rkey, string value, bool throw_if_missing);

		// Token: 0x0600001B RID: 27
		void DeleteKey(RegistryKey rkey, string keyName, bool throw_if_missing);

		// Token: 0x0600001C RID: 28
		string[] GetSubKeyNames(RegistryKey rkey);

		// Token: 0x0600001D RID: 29
		string[] GetValueNames(RegistryKey rkey);

		// Token: 0x0600001E RID: 30
		string ToString(RegistryKey rkey);

		// Token: 0x0600001F RID: 31
		void SetValue(RegistryKey rkey, string name, object value, RegistryValueKind valueKind);

		// Token: 0x06000020 RID: 32
		RegistryKey CreateSubKey(RegistryKey rkey, string keyname, RegistryOptions options);

		// Token: 0x06000021 RID: 33
		RegistryKey FromHandle(SafeRegistryHandle handle);

		// Token: 0x06000022 RID: 34
		IntPtr GetHandle(RegistryKey key);
	}
}
