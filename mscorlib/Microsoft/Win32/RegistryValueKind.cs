﻿using System;
using System.Runtime.InteropServices;

namespace Microsoft.Win32
{
	/// <summary>Specifies the data types to use when storing values in the registry, or identifies the data type of a value in the registry.</summary>
	// Token: 0x02000011 RID: 17
	[ComVisible(true)]
	public enum RegistryValueKind
	{
		/// <summary>An unsupported registry data type. For example, the Microsoft Win32 API registry data type REG_RESOURCE_LIST is unsupported. Use this value to specify that the <see cref="M:Microsoft.Win32.RegistryKey.SetValue(System.String,System.Object)" /> method should determine the appropriate registry data type when storing a name/value pair.</summary>
		// Token: 0x0400038D RID: 909
		Unknown,
		/// <summary>A null-terminated string. This value is equivalent to the Win32 API registry data type REG_SZ.</summary>
		// Token: 0x0400038E RID: 910
		String,
		/// <summary>A null-terminated string that contains unexpanded references to environment variables, such as %PATH%, that are expanded when the value is retrieved. This value is equivalent to the Win32 API registry data type REG_EXPAND_SZ.</summary>
		// Token: 0x0400038F RID: 911
		ExpandString,
		/// <summary>Binary data in any form. This value is equivalent to the Win32 API registry data type REG_BINARY.</summary>
		// Token: 0x04000390 RID: 912
		Binary,
		/// <summary>A 32-bit binary number. This value is equivalent to the Win32 API registry data type REG_DWORD.</summary>
		// Token: 0x04000391 RID: 913
		DWord,
		/// <summary>An array of null-terminated strings, terminated by two null characters. This value is equivalent to the Win32 API registry data type REG_MULTI_SZ.</summary>
		// Token: 0x04000392 RID: 914
		MultiString = 7,
		/// <summary>A 64-bit binary number. This value is equivalent to the Win32 API registry data type REG_QWORD.</summary>
		// Token: 0x04000393 RID: 915
		QWord = 11,
		/// <summary>No data type.</summary>
		// Token: 0x04000394 RID: 916
		None = -1
	}
}
