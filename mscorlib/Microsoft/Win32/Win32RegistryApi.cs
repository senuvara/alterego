﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Security;
using System.Text;
using Microsoft.Win32.SafeHandles;

namespace Microsoft.Win32
{
	// Token: 0x02000018 RID: 24
	internal class Win32RegistryApi : IRegistryApi
	{
		// Token: 0x060000A9 RID: 169
		[DllImport("advapi32.dll", CharSet = CharSet.Unicode)]
		private static extern int RegCreateKeyEx(IntPtr keyBase, string keyName, int reserved, IntPtr lpClass, int options, int access, IntPtr securityAttrs, out IntPtr keyHandle, out int disposition);

		// Token: 0x060000AA RID: 170
		[DllImport("advapi32.dll", CharSet = CharSet.Unicode)]
		private static extern int RegCloseKey(IntPtr keyHandle);

		// Token: 0x060000AB RID: 171
		[DllImport("advapi32.dll", CharSet = CharSet.Unicode)]
		private static extern int RegConnectRegistry(string machineName, IntPtr hKey, out IntPtr keyHandle);

		// Token: 0x060000AC RID: 172
		[DllImport("advapi32.dll", CharSet = CharSet.Unicode)]
		private static extern int RegFlushKey(IntPtr keyHandle);

		// Token: 0x060000AD RID: 173
		[DllImport("advapi32.dll", CharSet = CharSet.Unicode)]
		private static extern int RegOpenKeyEx(IntPtr keyBase, string keyName, IntPtr reserved, int access, out IntPtr keyHandle);

		// Token: 0x060000AE RID: 174
		[DllImport("advapi32.dll", CharSet = CharSet.Unicode)]
		private static extern int RegDeleteKey(IntPtr keyHandle, string valueName);

		// Token: 0x060000AF RID: 175
		[DllImport("advapi32.dll", CharSet = CharSet.Unicode)]
		private static extern int RegDeleteValue(IntPtr keyHandle, string valueName);

		// Token: 0x060000B0 RID: 176
		[DllImport("advapi32.dll", CharSet = CharSet.Unicode, EntryPoint = "RegEnumKeyExW")]
		internal unsafe static extern int RegEnumKeyEx(IntPtr keyHandle, int dwIndex, char* lpName, ref int lpcbName, int[] lpReserved, [Out] StringBuilder lpClass, int[] lpcbClass, long[] lpftLastWriteTime);

		// Token: 0x060000B1 RID: 177
		[DllImport("advapi32.dll", CharSet = CharSet.Unicode)]
		internal unsafe static extern int RegEnumValue(IntPtr hKey, int dwIndex, char* lpValueName, ref int lpcbValueName, IntPtr lpReserved_MustBeZero, int[] lpType, byte[] lpData, int[] lpcbData);

		// Token: 0x060000B2 RID: 178
		[DllImport("advapi32.dll", CharSet = CharSet.Unicode)]
		private static extern int RegSetValueEx(IntPtr keyBase, string valueName, IntPtr reserved, RegistryValueKind type, string data, int rawDataLength);

		// Token: 0x060000B3 RID: 179
		[DllImport("advapi32.dll", CharSet = CharSet.Unicode)]
		private static extern int RegSetValueEx(IntPtr keyBase, string valueName, IntPtr reserved, RegistryValueKind type, byte[] rawData, int rawDataLength);

		// Token: 0x060000B4 RID: 180
		[DllImport("advapi32.dll", CharSet = CharSet.Unicode)]
		private static extern int RegSetValueEx(IntPtr keyBase, string valueName, IntPtr reserved, RegistryValueKind type, ref int data, int rawDataLength);

		// Token: 0x060000B5 RID: 181
		[DllImport("advapi32.dll", CharSet = CharSet.Unicode)]
		private static extern int RegSetValueEx(IntPtr keyBase, string valueName, IntPtr reserved, RegistryValueKind type, ref long data, int rawDataLength);

		// Token: 0x060000B6 RID: 182
		[DllImport("advapi32.dll", CharSet = CharSet.Unicode)]
		private static extern int RegQueryValueEx(IntPtr keyBase, string valueName, IntPtr reserved, ref RegistryValueKind type, IntPtr zero, ref int dataSize);

		// Token: 0x060000B7 RID: 183
		[DllImport("advapi32.dll", CharSet = CharSet.Unicode)]
		private static extern int RegQueryValueEx(IntPtr keyBase, string valueName, IntPtr reserved, ref RegistryValueKind type, [Out] byte[] data, ref int dataSize);

		// Token: 0x060000B8 RID: 184
		[DllImport("advapi32.dll", CharSet = CharSet.Unicode)]
		private static extern int RegQueryValueEx(IntPtr keyBase, string valueName, IntPtr reserved, ref RegistryValueKind type, ref int data, ref int dataSize);

		// Token: 0x060000B9 RID: 185
		[DllImport("advapi32.dll", CharSet = CharSet.Unicode)]
		private static extern int RegQueryValueEx(IntPtr keyBase, string valueName, IntPtr reserved, ref RegistryValueKind type, ref long data, ref int dataSize);

		// Token: 0x060000BA RID: 186
		[DllImport("advapi32.dll", CharSet = CharSet.Unicode, EntryPoint = "RegQueryInfoKeyW")]
		internal static extern int RegQueryInfoKey(IntPtr hKey, [Out] StringBuilder lpClass, int[] lpcbClass, IntPtr lpReserved_MustBeZero, ref int lpcSubKeys, int[] lpcbMaxSubKeyLen, int[] lpcbMaxClassLen, ref int lpcValues, int[] lpcbMaxValueNameLen, int[] lpcbMaxValueLen, int[] lpcbSecurityDescriptor, int[] lpftLastWriteTime);

		// Token: 0x060000BB RID: 187 RVA: 0x0000441D File Offset: 0x0000261D
		public IntPtr GetHandle(RegistryKey key)
		{
			return (IntPtr)key.InternalHandle;
		}

		// Token: 0x060000BC RID: 188 RVA: 0x0000442A File Offset: 0x0000262A
		private static bool IsHandleValid(RegistryKey key)
		{
			return key.InternalHandle != null;
		}

		// Token: 0x060000BD RID: 189 RVA: 0x00004438 File Offset: 0x00002638
		public RegistryValueKind GetValueKind(RegistryKey rkey, string name)
		{
			RegistryValueKind result = RegistryValueKind.Unknown;
			int num = 0;
			int num2 = Win32RegistryApi.RegQueryValueEx(this.GetHandle(rkey), name, IntPtr.Zero, ref result, IntPtr.Zero, ref num);
			if (num2 == 2 || num2 == 1018)
			{
				return RegistryValueKind.Unknown;
			}
			return result;
		}

		// Token: 0x060000BE RID: 190 RVA: 0x00004474 File Offset: 0x00002674
		public object GetValue(RegistryKey rkey, string name, object defaultValue, RegistryValueOptions options)
		{
			RegistryValueKind registryValueKind = RegistryValueKind.Unknown;
			int size = 0;
			IntPtr handle = this.GetHandle(rkey);
			int num = Win32RegistryApi.RegQueryValueEx(handle, name, IntPtr.Zero, ref registryValueKind, IntPtr.Zero, ref size);
			if (num == 2 || num == 1018)
			{
				return defaultValue;
			}
			if (num != 234 && num != 0)
			{
				this.GenerateException(num);
			}
			object obj;
			if (registryValueKind == RegistryValueKind.String)
			{
				byte[] data;
				num = this.GetBinaryValue(rkey, name, registryValueKind, out data, size);
				obj = RegistryKey.DecodeString(data);
			}
			else if (registryValueKind == RegistryValueKind.ExpandString)
			{
				byte[] data2;
				num = this.GetBinaryValue(rkey, name, registryValueKind, out data2, size);
				obj = RegistryKey.DecodeString(data2);
				if ((options & RegistryValueOptions.DoNotExpandEnvironmentNames) == RegistryValueOptions.None)
				{
					obj = Environment.ExpandEnvironmentVariables((string)obj);
				}
			}
			else if (registryValueKind == RegistryValueKind.DWord)
			{
				int num2 = 0;
				num = Win32RegistryApi.RegQueryValueEx(handle, name, IntPtr.Zero, ref registryValueKind, ref num2, ref size);
				obj = num2;
			}
			else if (registryValueKind == RegistryValueKind.QWord)
			{
				long num3 = 0L;
				num = Win32RegistryApi.RegQueryValueEx(handle, name, IntPtr.Zero, ref registryValueKind, ref num3, ref size);
				obj = num3;
			}
			else if (registryValueKind == RegistryValueKind.Binary)
			{
				byte[] array;
				num = this.GetBinaryValue(rkey, name, registryValueKind, out array, size);
				obj = array;
			}
			else
			{
				if (registryValueKind != RegistryValueKind.MultiString)
				{
					throw new SystemException();
				}
				obj = null;
				byte[] data3;
				num = this.GetBinaryValue(rkey, name, registryValueKind, out data3, size);
				if (num == 0)
				{
					obj = RegistryKey.DecodeString(data3).Split(new char[1]);
				}
			}
			if (num != 0)
			{
				this.GenerateException(num);
			}
			return obj;
		}

		// Token: 0x060000BF RID: 191 RVA: 0x000045C4 File Offset: 0x000027C4
		public void SetValue(RegistryKey rkey, string name, object value, RegistryValueKind valueKind)
		{
			Type type = value.GetType();
			IntPtr handle = this.GetHandle(rkey);
			switch (valueKind)
			{
			case RegistryValueKind.String:
			case RegistryValueKind.ExpandString:
				if (type == typeof(string))
				{
					string text = string.Format("{0}{1}", value, '\0');
					this.CheckResult(Win32RegistryApi.RegSetValueEx(handle, name, IntPtr.Zero, valueKind, text, text.Length * this.NativeBytesPerCharacter));
					return;
				}
				goto IL_1B7;
			case RegistryValueKind.Binary:
				goto IL_9C;
			case RegistryValueKind.DWord:
				break;
			case (RegistryValueKind)5:
			case (RegistryValueKind)6:
			case (RegistryValueKind)8:
			case (RegistryValueKind)9:
			case (RegistryValueKind)10:
				goto IL_1A4;
			case RegistryValueKind.MultiString:
				if (type == typeof(string[]))
				{
					string[] array = (string[])value;
					StringBuilder stringBuilder = new StringBuilder();
					foreach (string value2 in array)
					{
						stringBuilder.Append(value2);
						stringBuilder.Append('\0');
					}
					stringBuilder.Append('\0');
					byte[] bytes = Encoding.Unicode.GetBytes(stringBuilder.ToString());
					this.CheckResult(Win32RegistryApi.RegSetValueEx(handle, name, IntPtr.Zero, RegistryValueKind.MultiString, bytes, bytes.Length));
					return;
				}
				goto IL_1B7;
			case RegistryValueKind.QWord:
				try
				{
					long num = Convert.ToInt64(value);
					this.CheckResult(Win32RegistryApi.RegSetValueEx(handle, name, IntPtr.Zero, RegistryValueKind.QWord, ref num, 8));
					return;
				}
				catch (OverflowException)
				{
					goto IL_1B7;
				}
				break;
			default:
				goto IL_1A4;
			}
			try
			{
				int num2 = Convert.ToInt32(value);
				this.CheckResult(Win32RegistryApi.RegSetValueEx(handle, name, IntPtr.Zero, RegistryValueKind.DWord, ref num2, 4));
				return;
			}
			catch (OverflowException)
			{
				goto IL_1B7;
			}
			IL_9C:
			if (type == typeof(byte[]))
			{
				byte[] array3 = (byte[])value;
				this.CheckResult(Win32RegistryApi.RegSetValueEx(handle, name, IntPtr.Zero, RegistryValueKind.Binary, array3, array3.Length));
				return;
			}
			goto IL_1B7;
			IL_1A4:
			if (type.IsArray)
			{
				throw new ArgumentException("Only string and byte arrays can written as registry values");
			}
			IL_1B7:
			throw new ArgumentException("Type does not match the valueKind");
		}

		// Token: 0x060000C0 RID: 192 RVA: 0x000047B0 File Offset: 0x000029B0
		public void SetValue(RegistryKey rkey, string name, object value)
		{
			Type type = value.GetType();
			IntPtr handle = this.GetHandle(rkey);
			int num2;
			if (type == typeof(int))
			{
				int num = (int)value;
				num2 = Win32RegistryApi.RegSetValueEx(handle, name, IntPtr.Zero, RegistryValueKind.DWord, ref num, 4);
			}
			else if (type == typeof(byte[]))
			{
				byte[] array = (byte[])value;
				num2 = Win32RegistryApi.RegSetValueEx(handle, name, IntPtr.Zero, RegistryValueKind.Binary, array, array.Length);
			}
			else if (type == typeof(string[]))
			{
				string[] array2 = (string[])value;
				StringBuilder stringBuilder = new StringBuilder();
				foreach (string value2 in array2)
				{
					stringBuilder.Append(value2);
					stringBuilder.Append('\0');
				}
				stringBuilder.Append('\0');
				byte[] bytes = Encoding.Unicode.GetBytes(stringBuilder.ToString());
				num2 = Win32RegistryApi.RegSetValueEx(handle, name, IntPtr.Zero, RegistryValueKind.MultiString, bytes, bytes.Length);
			}
			else
			{
				if (type.IsArray)
				{
					throw new ArgumentException("Only string and byte arrays can written as registry values");
				}
				string text = string.Format("{0}{1}", value, '\0');
				num2 = Win32RegistryApi.RegSetValueEx(handle, name, IntPtr.Zero, RegistryValueKind.String, text, text.Length * this.NativeBytesPerCharacter);
			}
			if (num2 != 0)
			{
				this.GenerateException(num2);
			}
		}

		// Token: 0x060000C1 RID: 193 RVA: 0x000048FC File Offset: 0x00002AFC
		private int GetBinaryValue(RegistryKey rkey, string name, RegistryValueKind type, out byte[] data, int size)
		{
			byte[] array = new byte[size];
			int result = Win32RegistryApi.RegQueryValueEx(this.GetHandle(rkey), name, IntPtr.Zero, ref type, array, ref size);
			data = array;
			return result;
		}

		// Token: 0x060000C2 RID: 194 RVA: 0x0000492C File Offset: 0x00002B2C
		public int SubKeyCount(RegistryKey rkey)
		{
			int result = 0;
			int num = 0;
			int num2 = Win32RegistryApi.RegQueryInfoKey(this.GetHandle(rkey), null, null, IntPtr.Zero, ref result, null, null, ref num, null, null, null, null);
			if (num2 != 0)
			{
				this.GenerateException(num2);
			}
			return result;
		}

		// Token: 0x060000C3 RID: 195 RVA: 0x00004968 File Offset: 0x00002B68
		public int ValueCount(RegistryKey rkey)
		{
			int result = 0;
			int num = 0;
			int num2 = Win32RegistryApi.RegQueryInfoKey(this.GetHandle(rkey), null, null, IntPtr.Zero, ref num, null, null, ref result, null, null, null, null);
			if (num2 != 0)
			{
				this.GenerateException(num2);
			}
			return result;
		}

		// Token: 0x060000C4 RID: 196 RVA: 0x000049A4 File Offset: 0x00002BA4
		public RegistryKey OpenRemoteBaseKey(RegistryHive hKey, string machineName)
		{
			IntPtr hKey2 = new IntPtr((int)hKey);
			IntPtr keyHandle;
			int num = Win32RegistryApi.RegConnectRegistry(machineName, hKey2, out keyHandle);
			if (num != 0)
			{
				this.GenerateException(num);
			}
			return new RegistryKey(hKey, keyHandle, true);
		}

		// Token: 0x060000C5 RID: 197 RVA: 0x000049D8 File Offset: 0x00002BD8
		public RegistryKey OpenSubKey(RegistryKey rkey, string keyName, bool writable)
		{
			int num = 131097;
			if (writable)
			{
				num |= 131078;
			}
			IntPtr intPtr;
			int num2 = Win32RegistryApi.RegOpenKeyEx(this.GetHandle(rkey), keyName, IntPtr.Zero, num, out intPtr);
			if (num2 == 2 || num2 == 1018)
			{
				return null;
			}
			if (num2 != 0)
			{
				this.GenerateException(num2);
			}
			return new RegistryKey(intPtr, Win32RegistryApi.CombineName(rkey, keyName), writable);
		}

		// Token: 0x060000C6 RID: 198 RVA: 0x00004A37 File Offset: 0x00002C37
		public void Flush(RegistryKey rkey)
		{
			if (!Win32RegistryApi.IsHandleValid(rkey))
			{
				return;
			}
			Win32RegistryApi.RegFlushKey(this.GetHandle(rkey));
		}

		// Token: 0x060000C7 RID: 199 RVA: 0x00004A50 File Offset: 0x00002C50
		public void Close(RegistryKey rkey)
		{
			if (!Win32RegistryApi.IsHandleValid(rkey))
			{
				return;
			}
			SafeRegistryHandle handle = rkey.Handle;
			if (handle != null)
			{
				handle.Close();
				return;
			}
			Win32RegistryApi.RegCloseKey(this.GetHandle(rkey));
		}

		// Token: 0x060000C8 RID: 200 RVA: 0x00004A84 File Offset: 0x00002C84
		public RegistryKey FromHandle(SafeRegistryHandle handle)
		{
			return new RegistryKey(handle.DangerousGetHandle(), string.Empty, true);
		}

		// Token: 0x060000C9 RID: 201 RVA: 0x00004A9C File Offset: 0x00002C9C
		public RegistryKey CreateSubKey(RegistryKey rkey, string keyName)
		{
			IntPtr intPtr;
			int num2;
			int num = Win32RegistryApi.RegCreateKeyEx(this.GetHandle(rkey), keyName, 0, IntPtr.Zero, 0, 131103, IntPtr.Zero, out intPtr, out num2);
			if (num != 0)
			{
				this.GenerateException(num);
			}
			return new RegistryKey(intPtr, Win32RegistryApi.CombineName(rkey, keyName), true);
		}

		// Token: 0x060000CA RID: 202 RVA: 0x00004AEC File Offset: 0x00002CEC
		public RegistryKey CreateSubKey(RegistryKey rkey, string keyName, RegistryOptions options)
		{
			IntPtr intPtr;
			int num2;
			int num = Win32RegistryApi.RegCreateKeyEx(this.GetHandle(rkey), keyName, 0, IntPtr.Zero, (options == RegistryOptions.Volatile) ? 1 : 0, 131103, IntPtr.Zero, out intPtr, out num2);
			if (num != 0)
			{
				this.GenerateException(num);
			}
			return new RegistryKey(intPtr, Win32RegistryApi.CombineName(rkey, keyName), true);
		}

		// Token: 0x060000CB RID: 203 RVA: 0x00004B40 File Offset: 0x00002D40
		public void DeleteKey(RegistryKey rkey, string keyName, bool shouldThrowWhenKeyMissing)
		{
			int num = Win32RegistryApi.RegDeleteKey(this.GetHandle(rkey), keyName);
			if (num != 2)
			{
				if (num != 0)
				{
					this.GenerateException(num);
				}
				return;
			}
			if (shouldThrowWhenKeyMissing)
			{
				throw new ArgumentException("key " + keyName);
			}
		}

		// Token: 0x060000CC RID: 204 RVA: 0x00004B80 File Offset: 0x00002D80
		public void DeleteValue(RegistryKey rkey, string value, bool shouldThrowWhenKeyMissing)
		{
			int num = Win32RegistryApi.RegDeleteValue(this.GetHandle(rkey), value);
			if (num == 1018)
			{
				return;
			}
			if (num != 2)
			{
				if (num != 0)
				{
					this.GenerateException(num);
				}
				return;
			}
			if (shouldThrowWhenKeyMissing)
			{
				throw new ArgumentException("value " + value);
			}
		}

		// Token: 0x060000CD RID: 205 RVA: 0x00004BC8 File Offset: 0x00002DC8
		public unsafe string[] GetSubKeyNames(RegistryKey rkey)
		{
			int num = this.SubKeyCount(rkey);
			string[] array = new string[num];
			if (num > 0)
			{
				IntPtr handle = this.GetHandle(rkey);
				char[] array2 = new char[256];
				fixed (char* ptr = &array2[0])
				{
					char* ptr2 = ptr;
					for (int i = 0; i < num; i++)
					{
						int num2 = array2.Length;
						int num3 = Win32RegistryApi.RegEnumKeyEx(handle, i, ptr2, ref num2, null, null, null, null);
						if (num3 != 0)
						{
							this.GenerateException(num3);
						}
						array[i] = new string(ptr2);
					}
				}
			}
			return array;
		}

		// Token: 0x060000CE RID: 206 RVA: 0x00004C4C File Offset: 0x00002E4C
		public unsafe string[] GetValueNames(RegistryKey rkey)
		{
			int num = this.ValueCount(rkey);
			string[] array = new string[num];
			if (num > 0)
			{
				IntPtr handle = this.GetHandle(rkey);
				char[] array2 = new char[16384];
				fixed (char* ptr = &array2[0])
				{
					char* ptr2 = ptr;
					for (int i = 0; i < num; i++)
					{
						int num2 = array2.Length;
						int num3 = Win32RegistryApi.RegEnumValue(handle, i, ptr2, ref num2, IntPtr.Zero, null, null, null);
						if (num3 != 0 && num3 != 234)
						{
							this.GenerateException(num3);
						}
						array[i] = new string(ptr2);
					}
				}
			}
			return array;
		}

		// Token: 0x060000CF RID: 207 RVA: 0x00004CDD File Offset: 0x00002EDD
		private void CheckResult(int result)
		{
			if (result != 0)
			{
				this.GenerateException(result);
			}
		}

		// Token: 0x060000D0 RID: 208 RVA: 0x00004CEC File Offset: 0x00002EEC
		private void GenerateException(int errorCode)
		{
			if (errorCode <= 53)
			{
				switch (errorCode)
				{
				case 2:
					break;
				case 3:
				case 4:
					goto IL_72;
				case 5:
					throw new SecurityException();
				case 6:
					throw new IOException("Invalid handle.");
				default:
					if (errorCode != 53)
					{
						goto IL_72;
					}
					throw new IOException("The network path was not found.");
				}
			}
			else if (errorCode != 87)
			{
				if (errorCode == 1018)
				{
					throw RegistryKey.CreateMarkedForDeletionException();
				}
				if (errorCode != 1021)
				{
					goto IL_72;
				}
				throw new IOException("Cannot create a stable subkey under a volatile parent key.");
			}
			throw new ArgumentException();
			IL_72:
			throw new SystemException();
		}

		// Token: 0x060000D1 RID: 209 RVA: 0x000043A8 File Offset: 0x000025A8
		public string ToString(RegistryKey rkey)
		{
			return rkey.Name;
		}

		// Token: 0x060000D2 RID: 210 RVA: 0x00004D70 File Offset: 0x00002F70
		internal static string CombineName(RegistryKey rkey, string localName)
		{
			return rkey.Name + "\\" + localName;
		}

		// Token: 0x060000D3 RID: 211 RVA: 0x00004D83 File Offset: 0x00002F83
		public Win32RegistryApi()
		{
		}

		// Token: 0x040003A8 RID: 936
		private const int OpenRegKeyRead = 131097;

		// Token: 0x040003A9 RID: 937
		private const int OpenRegKeyWrite = 131078;

		// Token: 0x040003AA RID: 938
		private const int Int32ByteSize = 4;

		// Token: 0x040003AB RID: 939
		private const int Int64ByteSize = 8;

		// Token: 0x040003AC RID: 940
		private readonly int NativeBytesPerCharacter = Marshal.SystemDefaultCharSize;

		// Token: 0x040003AD RID: 941
		private const int RegOptionsNonVolatile = 0;

		// Token: 0x040003AE RID: 942
		private const int RegOptionsVolatile = 1;

		// Token: 0x040003AF RID: 943
		private const int MaxKeyLength = 255;

		// Token: 0x040003B0 RID: 944
		private const int MaxValueLength = 16383;
	}
}
