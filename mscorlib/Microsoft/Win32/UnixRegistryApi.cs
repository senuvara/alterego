﻿using System;
using System.Globalization;
using System.IO;
using Microsoft.Win32.SafeHandles;

namespace Microsoft.Win32
{
	// Token: 0x02000017 RID: 23
	internal class UnixRegistryApi : IRegistryApi
	{
		// Token: 0x06000091 RID: 145 RVA: 0x0000417E File Offset: 0x0000237E
		private static string ToUnix(string keyname)
		{
			if (keyname.IndexOf('\\') != -1)
			{
				keyname = keyname.Replace('\\', '/');
			}
			return keyname.ToLower();
		}

		// Token: 0x06000092 RID: 146 RVA: 0x0000419D File Offset: 0x0000239D
		private static bool IsWellKnownKey(string parentKeyName, string keyname)
		{
			return (parentKeyName == Registry.CurrentUser.Name || parentKeyName == Registry.LocalMachine.Name) && string.Compare("software", keyname, true, CultureInfo.InvariantCulture) == 0;
		}

		// Token: 0x06000093 RID: 147 RVA: 0x000041D9 File Offset: 0x000023D9
		public RegistryKey CreateSubKey(RegistryKey rkey, string keyname)
		{
			return this.CreateSubKey(rkey, keyname, true);
		}

		// Token: 0x06000094 RID: 148 RVA: 0x000041E4 File Offset: 0x000023E4
		public RegistryKey CreateSubKey(RegistryKey rkey, string keyname, RegistryOptions options)
		{
			return this.CreateSubKey(rkey, keyname, true, options == RegistryOptions.Volatile);
		}

		// Token: 0x06000095 RID: 149 RVA: 0x000041F3 File Offset: 0x000023F3
		public RegistryKey OpenRemoteBaseKey(RegistryHive hKey, string machineName)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06000096 RID: 150 RVA: 0x000041FC File Offset: 0x000023FC
		public RegistryKey OpenSubKey(RegistryKey rkey, string keyname, bool writable)
		{
			KeyHandler keyHandler = KeyHandler.Lookup(rkey, true);
			if (keyHandler == null)
			{
				return null;
			}
			RegistryKey registryKey = keyHandler.Probe(rkey, UnixRegistryApi.ToUnix(keyname), writable);
			if (registryKey == null && UnixRegistryApi.IsWellKnownKey(rkey.Name, keyname))
			{
				registryKey = this.CreateSubKey(rkey, keyname, writable);
			}
			return registryKey;
		}

		// Token: 0x06000097 RID: 151 RVA: 0x000041F3 File Offset: 0x000023F3
		public RegistryKey FromHandle(SafeRegistryHandle handle)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06000098 RID: 152 RVA: 0x00004244 File Offset: 0x00002444
		public void Flush(RegistryKey rkey)
		{
			KeyHandler keyHandler = KeyHandler.Lookup(rkey, false);
			if (keyHandler == null)
			{
				return;
			}
			keyHandler.Flush();
		}

		// Token: 0x06000099 RID: 153 RVA: 0x00004263 File Offset: 0x00002463
		public void Close(RegistryKey rkey)
		{
			KeyHandler.Drop(rkey);
		}

		// Token: 0x0600009A RID: 154 RVA: 0x0000426C File Offset: 0x0000246C
		public object GetValue(RegistryKey rkey, string name, object default_value, RegistryValueOptions options)
		{
			KeyHandler keyHandler = KeyHandler.Lookup(rkey, true);
			if (keyHandler == null)
			{
				return default_value;
			}
			if (keyHandler.ValueExists(name))
			{
				return keyHandler.GetValue(name, options);
			}
			return default_value;
		}

		// Token: 0x0600009B RID: 155 RVA: 0x0000429A File Offset: 0x0000249A
		public void SetValue(RegistryKey rkey, string name, object value)
		{
			KeyHandler keyHandler = KeyHandler.Lookup(rkey, true);
			if (keyHandler == null)
			{
				throw RegistryKey.CreateMarkedForDeletionException();
			}
			keyHandler.SetValue(name, value);
		}

		// Token: 0x0600009C RID: 156 RVA: 0x000042B3 File Offset: 0x000024B3
		public void SetValue(RegistryKey rkey, string name, object value, RegistryValueKind valueKind)
		{
			KeyHandler keyHandler = KeyHandler.Lookup(rkey, true);
			if (keyHandler == null)
			{
				throw RegistryKey.CreateMarkedForDeletionException();
			}
			keyHandler.SetValue(name, value, valueKind);
		}

		// Token: 0x0600009D RID: 157 RVA: 0x000042CE File Offset: 0x000024CE
		public int SubKeyCount(RegistryKey rkey)
		{
			KeyHandler keyHandler = KeyHandler.Lookup(rkey, true);
			if (keyHandler == null)
			{
				throw RegistryKey.CreateMarkedForDeletionException();
			}
			return keyHandler.GetSubKeyCount();
		}

		// Token: 0x0600009E RID: 158 RVA: 0x000042E5 File Offset: 0x000024E5
		public int ValueCount(RegistryKey rkey)
		{
			KeyHandler keyHandler = KeyHandler.Lookup(rkey, true);
			if (keyHandler == null)
			{
				throw RegistryKey.CreateMarkedForDeletionException();
			}
			return keyHandler.ValueCount;
		}

		// Token: 0x0600009F RID: 159 RVA: 0x000042FC File Offset: 0x000024FC
		public void DeleteValue(RegistryKey rkey, string name, bool throw_if_missing)
		{
			KeyHandler keyHandler = KeyHandler.Lookup(rkey, true);
			if (keyHandler == null)
			{
				return;
			}
			if (throw_if_missing && !keyHandler.ValueExists(name))
			{
				throw new ArgumentException("the given value does not exist");
			}
			keyHandler.RemoveValue(name);
		}

		// Token: 0x060000A0 RID: 160 RVA: 0x00004334 File Offset: 0x00002534
		public void DeleteKey(RegistryKey rkey, string keyname, bool throw_if_missing)
		{
			KeyHandler keyHandler = KeyHandler.Lookup(rkey, true);
			if (keyHandler == null)
			{
				if (!throw_if_missing)
				{
					return;
				}
				throw new ArgumentException("the given value does not exist");
			}
			else
			{
				if (!KeyHandler.Delete(Path.Combine(keyHandler.Dir, UnixRegistryApi.ToUnix(keyname))) && throw_if_missing)
				{
					throw new ArgumentException("the given value does not exist");
				}
				return;
			}
		}

		// Token: 0x060000A1 RID: 161 RVA: 0x00004383 File Offset: 0x00002583
		public string[] GetSubKeyNames(RegistryKey rkey)
		{
			return KeyHandler.Lookup(rkey, true).GetSubKeyNames();
		}

		// Token: 0x060000A2 RID: 162 RVA: 0x00004391 File Offset: 0x00002591
		public string[] GetValueNames(RegistryKey rkey)
		{
			KeyHandler keyHandler = KeyHandler.Lookup(rkey, true);
			if (keyHandler == null)
			{
				throw RegistryKey.CreateMarkedForDeletionException();
			}
			return keyHandler.GetValueNames();
		}

		// Token: 0x060000A3 RID: 163 RVA: 0x000043A8 File Offset: 0x000025A8
		public string ToString(RegistryKey rkey)
		{
			return rkey.Name;
		}

		// Token: 0x060000A4 RID: 164 RVA: 0x000043B0 File Offset: 0x000025B0
		private RegistryKey CreateSubKey(RegistryKey rkey, string keyname, bool writable)
		{
			return this.CreateSubKey(rkey, keyname, writable, false);
		}

		// Token: 0x060000A5 RID: 165 RVA: 0x000043BC File Offset: 0x000025BC
		private RegistryKey CreateSubKey(RegistryKey rkey, string keyname, bool writable, bool is_volatile)
		{
			KeyHandler keyHandler = KeyHandler.Lookup(rkey, true);
			if (keyHandler == null)
			{
				throw RegistryKey.CreateMarkedForDeletionException();
			}
			if (KeyHandler.VolatileKeyExists(keyHandler.Dir) && !is_volatile)
			{
				throw new IOException("Cannot create a non volatile subkey under a volatile key.");
			}
			return keyHandler.Ensure(rkey, UnixRegistryApi.ToUnix(keyname), writable, is_volatile);
		}

		// Token: 0x060000A6 RID: 166 RVA: 0x000043FC File Offset: 0x000025FC
		public RegistryValueKind GetValueKind(RegistryKey rkey, string name)
		{
			KeyHandler keyHandler = KeyHandler.Lookup(rkey, true);
			if (keyHandler != null)
			{
				return keyHandler.GetValueKind(name);
			}
			return RegistryValueKind.Unknown;
		}

		// Token: 0x060000A7 RID: 167 RVA: 0x000041F3 File Offset: 0x000023F3
		public IntPtr GetHandle(RegistryKey key)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060000A8 RID: 168 RVA: 0x00002050 File Offset: 0x00000250
		public UnixRegistryApi()
		{
		}
	}
}
