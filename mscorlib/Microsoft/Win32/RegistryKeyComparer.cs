﻿using System;
using System.Collections;

namespace Microsoft.Win32
{
	// Token: 0x02000015 RID: 21
	internal class RegistryKeyComparer : IEqualityComparer
	{
		// Token: 0x06000067 RID: 103 RVA: 0x00002B9C File Offset: 0x00000D9C
		public bool Equals(object x, object y)
		{
			return RegistryKey.IsEquals((RegistryKey)x, (RegistryKey)y);
		}

		// Token: 0x06000068 RID: 104 RVA: 0x00002BB0 File Offset: 0x00000DB0
		public int GetHashCode(object obj)
		{
			string name = ((RegistryKey)obj).Name;
			if (name == null)
			{
				return 0;
			}
			return name.GetHashCode();
		}

		// Token: 0x06000069 RID: 105 RVA: 0x00002050 File Offset: 0x00000250
		public RegistryKeyComparer()
		{
		}
	}
}
