﻿using System;
using System.Security;

namespace Microsoft.Win32.SafeHandles
{
	/// <summary>Represents a safe handle to the Windows registry.</summary>
	// Token: 0x02000020 RID: 32
	[SecurityCritical]
	public sealed class SafeRegistryHandle : SafeHandleZeroOrMinusOneIsInvalid
	{
		// Token: 0x060000E0 RID: 224 RVA: 0x00004DB1 File Offset: 0x00002FB1
		[SecurityCritical]
		internal SafeRegistryHandle() : base(true)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:Microsoft.Win32.SafeHandles.SafeRegistryHandle" /> class. </summary>
		/// <param name="preexistingHandle">An object that represents the pre-existing handle to use.</param>
		/// <param name="ownsHandle">
		///       <see langword="true" /> to reliably release the handle during the finalization phase; <see langword="false" /> to prevent reliable release.</param>
		// Token: 0x060000E1 RID: 225 RVA: 0x00004DBA File Offset: 0x00002FBA
		[SecurityCritical]
		public SafeRegistryHandle(IntPtr preexistingHandle, bool ownsHandle) : base(ownsHandle)
		{
			base.SetHandle(preexistingHandle);
		}

		// Token: 0x060000E2 RID: 226 RVA: 0x00004E08 File Offset: 0x00003008
		protected override bool ReleaseHandle()
		{
			return true;
		}
	}
}
