﻿using System;
using System.Runtime.InteropServices;
using System.Security;

namespace Microsoft.Win32.SafeHandles
{
	/// <summary>Provides a safe handle to a Windows thread or process access token. For more information see Access Tokens</summary>
	// Token: 0x02000026 RID: 38
	[SecurityCritical]
	public sealed class SafeAccessTokenHandle : SafeHandle
	{
		// Token: 0x060000EE RID: 238 RVA: 0x00004EBB File Offset: 0x000030BB
		private SafeAccessTokenHandle() : base(IntPtr.Zero, true)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:Microsoft.Win32.SafeHandles.SafeAccessTokenHandle" /> class.</summary>
		/// <param name="handle">An <see cref="T:System.IntPtr" /> object that represents the pre-existing handle to use. Using <see cref="F:System.IntPtr.Zero" /> returns an invalid handle.</param>
		// Token: 0x060000EF RID: 239 RVA: 0x00004EC9 File Offset: 0x000030C9
		public SafeAccessTokenHandle(IntPtr handle) : base(IntPtr.Zero, true)
		{
			base.SetHandle(handle);
		}

		/// <summary>Returns an invalid handle by instantiating a <see cref="T:Microsoft.Win32.SafeHandles.SafeAccessTokenHandle" /> object with <see cref="F:System.IntPtr.Zero" />.</summary>
		/// <returns>Returns a<see cref="T:Microsoft.Win32.SafeHandles.SafeAccessTokenHandle" /> object.</returns>
		// Token: 0x17000012 RID: 18
		// (get) Token: 0x060000F0 RID: 240 RVA: 0x00004EDE File Offset: 0x000030DE
		public static SafeAccessTokenHandle InvalidHandle
		{
			[SecurityCritical]
			get
			{
				return new SafeAccessTokenHandle(IntPtr.Zero);
			}
		}

		/// <summary>Gets a value that indicates whether the handle is invalid.</summary>
		/// <returns>
		///     <see langword="true" /> if the handle is not valid; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000013 RID: 19
		// (get) Token: 0x060000F1 RID: 241 RVA: 0x00004EEA File Offset: 0x000030EA
		public override bool IsInvalid
		{
			[SecurityCritical]
			get
			{
				return this.handle == IntPtr.Zero || this.handle == new IntPtr(-1);
			}
		}

		// Token: 0x060000F2 RID: 242 RVA: 0x00004E08 File Offset: 0x00003008
		[SecurityCritical]
		protected override bool ReleaseHandle()
		{
			return true;
		}
	}
}
