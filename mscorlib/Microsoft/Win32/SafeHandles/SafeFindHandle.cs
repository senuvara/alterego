﻿using System;
using System.IO;
using System.Security;

namespace Microsoft.Win32.SafeHandles
{
	// Token: 0x0200001F RID: 31
	[SecurityCritical]
	internal sealed class SafeFindHandle : SafeHandleZeroOrMinusOneIsInvalid
	{
		// Token: 0x060000DD RID: 221 RVA: 0x00004DB1 File Offset: 0x00002FB1
		[SecurityCritical]
		internal SafeFindHandle() : base(true)
		{
		}

		// Token: 0x060000DE RID: 222 RVA: 0x00004DEB File Offset: 0x00002FEB
		internal SafeFindHandle(IntPtr preexistingHandle) : base(true)
		{
			base.SetHandle(preexistingHandle);
		}

		// Token: 0x060000DF RID: 223 RVA: 0x00004DFB File Offset: 0x00002FFB
		[SecurityCritical]
		protected override bool ReleaseHandle()
		{
			return MonoIO.FindCloseFile(this.handle);
		}
	}
}
