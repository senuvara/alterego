﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Security;
using System.Text;
using System.Threading;

namespace Microsoft.Win32
{
	// Token: 0x02000016 RID: 22
	internal class KeyHandler
	{
		// Token: 0x0600006A RID: 106 RVA: 0x00002BD4 File Offset: 0x00000DD4
		static KeyHandler()
		{
			KeyHandler.CleanVolatileKeys();
		}

		// Token: 0x0600006B RID: 107 RVA: 0x00002BFE File Offset: 0x00000DFE
		private KeyHandler(RegistryKey rkey, string basedir) : this(rkey, basedir, false)
		{
		}

		// Token: 0x0600006C RID: 108 RVA: 0x00002C0C File Offset: 0x00000E0C
		private KeyHandler(RegistryKey rkey, string basedir, bool is_volatile)
		{
			string volatileDir = KeyHandler.GetVolatileDir(basedir);
			string text = basedir;
			if (Directory.Exists(basedir))
			{
				is_volatile = false;
			}
			else if (Directory.Exists(volatileDir))
			{
				text = volatileDir;
				is_volatile = true;
			}
			else if (is_volatile)
			{
				text = volatileDir;
			}
			if (!Directory.Exists(text))
			{
				try
				{
					Directory.CreateDirectory(text);
				}
				catch (UnauthorizedAccessException inner)
				{
					throw new SecurityException("No access to the given key", inner);
				}
			}
			this.Dir = basedir;
			this.ActualDir = text;
			this.IsVolatile = is_volatile;
			this.file = Path.Combine(this.ActualDir, "values.xml");
			this.Load();
		}

		// Token: 0x0600006D RID: 109 RVA: 0x00002CA8 File Offset: 0x00000EA8
		public void Load()
		{
			this.values = new Hashtable();
			if (!File.Exists(this.file))
			{
				return;
			}
			try
			{
				using (FileStream fileStream = File.OpenRead(this.file))
				{
					string text = new StreamReader(fileStream).ReadToEnd();
					if (text.Length != 0)
					{
						SecurityElement securityElement = SecurityElement.FromString(text);
						if (securityElement.Tag == "values" && securityElement.Children != null)
						{
							foreach (object obj in securityElement.Children)
							{
								SecurityElement securityElement2 = (SecurityElement)obj;
								if (securityElement2.Tag == "value")
								{
									this.LoadKey(securityElement2);
								}
							}
						}
					}
				}
			}
			catch (UnauthorizedAccessException)
			{
				this.values.Clear();
				throw new SecurityException("No access to the given key");
			}
			catch (Exception arg)
			{
				Console.Error.WriteLine("While loading registry key at {0}: {1}", this.file, arg);
				this.values.Clear();
			}
		}

		// Token: 0x0600006E RID: 110 RVA: 0x00002DE8 File Offset: 0x00000FE8
		private void LoadKey(SecurityElement se)
		{
			Hashtable attributes = se.Attributes;
			try
			{
				string text = (string)attributes["name"];
				if (text != null)
				{
					string text2 = (string)attributes["type"];
					if (text2 != null)
					{
						if (!(text2 == "int"))
						{
							if (!(text2 == "bytearray"))
							{
								if (!(text2 == "string"))
								{
									if (!(text2 == "expand"))
									{
										if (!(text2 == "qword"))
										{
											if (text2 == "string-array")
											{
												List<string> list = new List<string>();
												if (se.Children != null)
												{
													foreach (object obj in se.Children)
													{
														SecurityElement securityElement = (SecurityElement)obj;
														list.Add(securityElement.Text);
													}
												}
												this.values[text] = list.ToArray();
											}
										}
										else
										{
											this.values[text] = long.Parse(se.Text);
										}
									}
									else
									{
										this.values[text] = new ExpandString(se.Text);
									}
								}
								else
								{
									this.values[text] = ((se.Text == null) ? string.Empty : se.Text);
								}
							}
							else
							{
								this.values[text] = Convert.FromBase64String(se.Text);
							}
						}
						else
						{
							this.values[text] = int.Parse(se.Text);
						}
					}
				}
			}
			catch
			{
			}
		}

		// Token: 0x0600006F RID: 111 RVA: 0x00002FCC File Offset: 0x000011CC
		public RegistryKey Ensure(RegistryKey rkey, string extra, bool writable)
		{
			return this.Ensure(rkey, extra, writable, false);
		}

		// Token: 0x06000070 RID: 112 RVA: 0x00002FD8 File Offset: 0x000011D8
		public RegistryKey Ensure(RegistryKey rkey, string extra, bool writable, bool is_volatile)
		{
			Type typeFromHandle = typeof(KeyHandler);
			RegistryKey result;
			lock (typeFromHandle)
			{
				string text = Path.Combine(this.Dir, extra);
				KeyHandler keyHandler = (KeyHandler)KeyHandler.dir_to_handler[text];
				if (keyHandler == null)
				{
					keyHandler = new KeyHandler(rkey, text, is_volatile);
				}
				RegistryKey registryKey = new RegistryKey(keyHandler, KeyHandler.CombineName(rkey, extra), writable);
				KeyHandler.key_to_handler[registryKey] = keyHandler;
				KeyHandler.dir_to_handler[text] = keyHandler;
				result = registryKey;
			}
			return result;
		}

		// Token: 0x06000071 RID: 113 RVA: 0x00003070 File Offset: 0x00001270
		public RegistryKey Probe(RegistryKey rkey, string extra, bool writable)
		{
			RegistryKey registryKey = null;
			Type typeFromHandle = typeof(KeyHandler);
			RegistryKey result;
			lock (typeFromHandle)
			{
				string text = Path.Combine(this.Dir, extra);
				KeyHandler keyHandler = (KeyHandler)KeyHandler.dir_to_handler[text];
				if (keyHandler != null)
				{
					registryKey = new RegistryKey(keyHandler, KeyHandler.CombineName(rkey, extra), writable);
					KeyHandler.key_to_handler[registryKey] = keyHandler;
				}
				else if (Directory.Exists(text) || KeyHandler.VolatileKeyExists(text))
				{
					keyHandler = new KeyHandler(rkey, text);
					registryKey = new RegistryKey(keyHandler, KeyHandler.CombineName(rkey, extra), writable);
					KeyHandler.dir_to_handler[text] = keyHandler;
					KeyHandler.key_to_handler[registryKey] = keyHandler;
				}
				result = registryKey;
			}
			return result;
		}

		// Token: 0x06000072 RID: 114 RVA: 0x0000313C File Offset: 0x0000133C
		private static string CombineName(RegistryKey rkey, string extra)
		{
			if (extra.IndexOf('/') != -1)
			{
				extra = extra.Replace('/', '\\');
			}
			return rkey.Name + "\\" + extra;
		}

		// Token: 0x06000073 RID: 115 RVA: 0x00003168 File Offset: 0x00001368
		private static long GetSystemBootTime()
		{
			if (!File.Exists("/proc/stat"))
			{
				return -1L;
			}
			string text = null;
			try
			{
				using (StreamReader streamReader = new StreamReader("/proc/stat", Encoding.ASCII))
				{
					string text2;
					while ((text2 = streamReader.ReadLine()) != null)
					{
						if (text2.StartsWith("btime"))
						{
							text = text2;
							break;
						}
					}
				}
			}
			catch (Exception arg)
			{
				Console.Error.WriteLine("While reading system info {0}", arg);
			}
			if (text == null)
			{
				return -1L;
			}
			int num = text.IndexOf(' ');
			long result;
			if (!long.TryParse(text.Substring(num, text.Length - num), out result))
			{
				return -1L;
			}
			return result;
		}

		// Token: 0x06000074 RID: 116 RVA: 0x00003224 File Offset: 0x00001424
		private static long GetRegisteredBootTime(string path)
		{
			if (!File.Exists(path))
			{
				return -1L;
			}
			string text = null;
			try
			{
				using (StreamReader streamReader = new StreamReader(path, Encoding.ASCII))
				{
					text = streamReader.ReadLine();
				}
			}
			catch (Exception arg)
			{
				Console.Error.WriteLine("While reading registry data at {0}: {1}", path, arg);
			}
			if (text == null)
			{
				return -1L;
			}
			long result;
			if (!long.TryParse(text, out result))
			{
				return -1L;
			}
			return result;
		}

		// Token: 0x06000075 RID: 117 RVA: 0x000032A4 File Offset: 0x000014A4
		private static void SaveRegisteredBootTime(string path, long btime)
		{
			try
			{
				using (StreamWriter streamWriter = new StreamWriter(path, false, Encoding.ASCII))
				{
					streamWriter.WriteLine(btime.ToString());
				}
			}
			catch (Exception)
			{
			}
		}

		// Token: 0x06000076 RID: 118 RVA: 0x000032F8 File Offset: 0x000014F8
		private static void CleanVolatileKeys()
		{
			long systemBootTime = KeyHandler.GetSystemBootTime();
			foreach (string text in new string[]
			{
				KeyHandler.UserStore,
				KeyHandler.MachineStore
			})
			{
				if (Directory.Exists(text))
				{
					string path = Path.Combine(text, "last-btime");
					string path2 = Path.Combine(text, "volatile-keys");
					if (Directory.Exists(path2))
					{
						long registeredBootTime = KeyHandler.GetRegisteredBootTime(path);
						if (systemBootTime < 0L || registeredBootTime < 0L || registeredBootTime != systemBootTime)
						{
							Directory.Delete(path2, true);
						}
					}
					KeyHandler.SaveRegisteredBootTime(path, systemBootTime);
				}
			}
		}

		// Token: 0x06000077 RID: 119 RVA: 0x00003388 File Offset: 0x00001588
		public static bool VolatileKeyExists(string dir)
		{
			Type typeFromHandle = typeof(KeyHandler);
			lock (typeFromHandle)
			{
				KeyHandler keyHandler = (KeyHandler)KeyHandler.dir_to_handler[dir];
				if (keyHandler != null)
				{
					return keyHandler.IsVolatile;
				}
			}
			return !Directory.Exists(dir) && Directory.Exists(KeyHandler.GetVolatileDir(dir));
		}

		// Token: 0x06000078 RID: 120 RVA: 0x000033FC File Offset: 0x000015FC
		public static string GetVolatileDir(string dir)
		{
			string rootFromDir = KeyHandler.GetRootFromDir(dir);
			return dir.Replace(rootFromDir, Path.Combine(rootFromDir, "volatile-keys"));
		}

		// Token: 0x06000079 RID: 121 RVA: 0x00003424 File Offset: 0x00001624
		public static KeyHandler Lookup(RegistryKey rkey, bool createNonExisting)
		{
			Type typeFromHandle = typeof(KeyHandler);
			KeyHandler result;
			lock (typeFromHandle)
			{
				KeyHandler keyHandler = (KeyHandler)KeyHandler.key_to_handler[rkey];
				if (keyHandler != null)
				{
					result = keyHandler;
				}
				else if (!rkey.IsRoot || !createNonExisting)
				{
					result = null;
				}
				else
				{
					RegistryHive hive = rkey.Hive;
					switch (hive)
					{
					case RegistryHive.ClassesRoot:
					case RegistryHive.LocalMachine:
					case RegistryHive.Users:
					case RegistryHive.PerformanceData:
					case RegistryHive.CurrentConfig:
					case RegistryHive.DynData:
					{
						string text = Path.Combine(KeyHandler.MachineStore, hive.ToString());
						keyHandler = new KeyHandler(rkey, text);
						KeyHandler.dir_to_handler[text] = keyHandler;
						break;
					}
					case RegistryHive.CurrentUser:
					{
						string text2 = Path.Combine(KeyHandler.UserStore, hive.ToString());
						keyHandler = new KeyHandler(rkey, text2);
						KeyHandler.dir_to_handler[text2] = keyHandler;
						break;
					}
					default:
						throw new Exception("Unknown RegistryHive");
					}
					KeyHandler.key_to_handler[rkey] = keyHandler;
					result = keyHandler;
				}
			}
			return result;
		}

		// Token: 0x0600007A RID: 122 RVA: 0x00003540 File Offset: 0x00001740
		private static string GetRootFromDir(string dir)
		{
			if (dir.IndexOf(KeyHandler.UserStore) > -1)
			{
				return KeyHandler.UserStore;
			}
			if (dir.IndexOf(KeyHandler.MachineStore) > -1)
			{
				return KeyHandler.MachineStore;
			}
			throw new Exception("Could not get root for dir " + dir);
		}

		// Token: 0x0600007B RID: 123 RVA: 0x0000357C File Offset: 0x0000177C
		public static void Drop(RegistryKey rkey)
		{
			Type typeFromHandle = typeof(KeyHandler);
			lock (typeFromHandle)
			{
				KeyHandler keyHandler = (KeyHandler)KeyHandler.key_to_handler[rkey];
				if (keyHandler != null)
				{
					KeyHandler.key_to_handler.Remove(rkey);
					int num = 0;
					foreach (object obj in KeyHandler.key_to_handler)
					{
						if (((DictionaryEntry)obj).Value == keyHandler)
						{
							num++;
						}
					}
					if (num == 0)
					{
						KeyHandler.dir_to_handler.Remove(keyHandler.Dir);
					}
				}
			}
		}

		// Token: 0x0600007C RID: 124 RVA: 0x00003648 File Offset: 0x00001848
		public static void Drop(string dir)
		{
			Type typeFromHandle = typeof(KeyHandler);
			lock (typeFromHandle)
			{
				KeyHandler keyHandler = (KeyHandler)KeyHandler.dir_to_handler[dir];
				if (keyHandler != null)
				{
					KeyHandler.dir_to_handler.Remove(dir);
					ArrayList arrayList = new ArrayList();
					foreach (object obj in KeyHandler.key_to_handler)
					{
						DictionaryEntry dictionaryEntry = (DictionaryEntry)obj;
						if (dictionaryEntry.Value == keyHandler)
						{
							arrayList.Add(dictionaryEntry.Key);
						}
					}
					foreach (object key in arrayList)
					{
						KeyHandler.key_to_handler.Remove(key);
					}
				}
			}
		}

		// Token: 0x0600007D RID: 125 RVA: 0x0000375C File Offset: 0x0000195C
		public static bool Delete(string dir)
		{
			if (!Directory.Exists(dir))
			{
				string volatileDir = KeyHandler.GetVolatileDir(dir);
				if (!Directory.Exists(volatileDir))
				{
					return false;
				}
				dir = volatileDir;
			}
			Directory.Delete(dir, true);
			KeyHandler.Drop(dir);
			return true;
		}

		// Token: 0x0600007E RID: 126 RVA: 0x00003794 File Offset: 0x00001994
		public RegistryValueKind GetValueKind(string name)
		{
			if (name == null)
			{
				return RegistryValueKind.Unknown;
			}
			Hashtable obj = this.values;
			object obj2;
			lock (obj)
			{
				obj2 = this.values[name];
			}
			if (obj2 == null)
			{
				return RegistryValueKind.Unknown;
			}
			if (obj2 is int)
			{
				return RegistryValueKind.DWord;
			}
			if (obj2 is string[])
			{
				return RegistryValueKind.MultiString;
			}
			if (obj2 is long)
			{
				return RegistryValueKind.QWord;
			}
			if (obj2 is byte[])
			{
				return RegistryValueKind.Binary;
			}
			if (obj2 is string)
			{
				return RegistryValueKind.String;
			}
			if (obj2 is ExpandString)
			{
				return RegistryValueKind.ExpandString;
			}
			return RegistryValueKind.Unknown;
		}

		// Token: 0x0600007F RID: 127 RVA: 0x00003824 File Offset: 0x00001A24
		public object GetValue(string name, RegistryValueOptions options)
		{
			if (this.IsMarkedForDeletion)
			{
				return null;
			}
			if (name == null)
			{
				name = string.Empty;
			}
			Hashtable obj = this.values;
			object obj2;
			lock (obj)
			{
				obj2 = this.values[name];
			}
			ExpandString expandString = obj2 as ExpandString;
			if (expandString == null)
			{
				return obj2;
			}
			if ((options & RegistryValueOptions.DoNotExpandEnvironmentNames) == RegistryValueOptions.None)
			{
				return expandString.Expand();
			}
			return expandString.ToString();
		}

		// Token: 0x06000080 RID: 128 RVA: 0x000038A0 File Offset: 0x00001AA0
		public void SetValue(string name, object value)
		{
			this.AssertNotMarkedForDeletion();
			if (name == null)
			{
				name = string.Empty;
			}
			Hashtable obj = this.values;
			lock (obj)
			{
				if (value is int || value is string || value is byte[] || value is string[])
				{
					this.values[name] = value;
				}
				else
				{
					this.values[name] = value.ToString();
				}
			}
			this.SetDirty();
		}

		// Token: 0x06000081 RID: 129 RVA: 0x00003934 File Offset: 0x00001B34
		public string[] GetValueNames()
		{
			this.AssertNotMarkedForDeletion();
			Hashtable obj = this.values;
			string[] result;
			lock (obj)
			{
				ICollection keys = this.values.Keys;
				string[] array = new string[keys.Count];
				keys.CopyTo(array, 0);
				result = array;
			}
			return result;
		}

		// Token: 0x06000082 RID: 130 RVA: 0x00003998 File Offset: 0x00001B98
		public int GetSubKeyCount()
		{
			return this.GetSubKeyNames().Length;
		}

		// Token: 0x06000083 RID: 131 RVA: 0x000039A4 File Offset: 0x00001BA4
		public string[] GetSubKeyNames()
		{
			DirectoryInfo[] directories = new DirectoryInfo(this.ActualDir).GetDirectories();
			string[] array;
			if (this.IsVolatile || !Directory.Exists(KeyHandler.GetVolatileDir(this.Dir)))
			{
				array = new string[directories.Length];
				for (int i = 0; i < directories.Length; i++)
				{
					DirectoryInfo directoryInfo = directories[i];
					array[i] = directoryInfo.Name;
				}
				return array;
			}
			DirectoryInfo[] directories2 = new DirectoryInfo(KeyHandler.GetVolatileDir(this.Dir)).GetDirectories();
			Dictionary<string, string> dictionary = new Dictionary<string, string>();
			foreach (DirectoryInfo directoryInfo2 in directories)
			{
				dictionary[directoryInfo2.Name] = directoryInfo2.Name;
			}
			foreach (DirectoryInfo directoryInfo3 in directories2)
			{
				dictionary[directoryInfo3.Name] = directoryInfo3.Name;
			}
			array = new string[dictionary.Count];
			int num = 0;
			foreach (KeyValuePair<string, string> keyValuePair in dictionary)
			{
				array[num++] = keyValuePair.Value;
			}
			return array;
		}

		// Token: 0x06000084 RID: 132 RVA: 0x00003AE4 File Offset: 0x00001CE4
		public void SetValue(string name, object value, RegistryValueKind valueKind)
		{
			this.SetDirty();
			if (name == null)
			{
				name = string.Empty;
			}
			Hashtable obj = this.values;
			lock (obj)
			{
				switch (valueKind)
				{
				case RegistryValueKind.String:
					if (value is string)
					{
						this.values[name] = value;
						return;
					}
					goto IL_116;
				case RegistryValueKind.ExpandString:
					if (value is string)
					{
						this.values[name] = new ExpandString((string)value);
						return;
					}
					goto IL_116;
				case RegistryValueKind.Binary:
					if (value is byte[])
					{
						this.values[name] = value;
						return;
					}
					goto IL_116;
				case RegistryValueKind.DWord:
					try
					{
						this.values[name] = Convert.ToInt32(value);
						return;
					}
					catch (OverflowException)
					{
						goto IL_122;
					}
					break;
				case (RegistryValueKind)5:
				case (RegistryValueKind)6:
				case (RegistryValueKind)8:
				case (RegistryValueKind)9:
				case (RegistryValueKind)10:
					goto IL_106;
				case RegistryValueKind.MultiString:
					break;
				case RegistryValueKind.QWord:
					try
					{
						this.values[name] = Convert.ToInt64(value);
						return;
					}
					catch (OverflowException)
					{
						goto IL_122;
					}
					goto IL_106;
				default:
					goto IL_106;
				}
				if (value is string[])
				{
					this.values[name] = value;
					return;
				}
				goto IL_116;
				IL_106:
				throw new ArgumentException("unknown value", "valueKind");
				IL_116:;
			}
			IL_122:
			throw new ArgumentException("Value could not be converted to specified type", "valueKind");
		}

		// Token: 0x06000085 RID: 133 RVA: 0x00003C4C File Offset: 0x00001E4C
		private void SetDirty()
		{
			Type typeFromHandle = typeof(KeyHandler);
			lock (typeFromHandle)
			{
				if (!this.dirty)
				{
					this.dirty = true;
					new Timer(new TimerCallback(this.DirtyTimeout), null, 3000, -1);
				}
			}
		}

		// Token: 0x06000086 RID: 134 RVA: 0x00003CB4 File Offset: 0x00001EB4
		public void DirtyTimeout(object state)
		{
			this.Flush();
		}

		// Token: 0x06000087 RID: 135 RVA: 0x00003CBC File Offset: 0x00001EBC
		public void Flush()
		{
			Type typeFromHandle = typeof(KeyHandler);
			lock (typeFromHandle)
			{
				if (this.dirty)
				{
					this.Save();
					this.dirty = false;
				}
			}
		}

		// Token: 0x06000088 RID: 136 RVA: 0x00003D10 File Offset: 0x00001F10
		public bool ValueExists(string name)
		{
			if (name == null)
			{
				name = string.Empty;
			}
			Hashtable obj = this.values;
			bool result;
			lock (obj)
			{
				result = this.values.Contains(name);
			}
			return result;
		}

		// Token: 0x1700000A RID: 10
		// (get) Token: 0x06000089 RID: 137 RVA: 0x00003D64 File Offset: 0x00001F64
		public int ValueCount
		{
			get
			{
				Hashtable obj = this.values;
				int count;
				lock (obj)
				{
					count = this.values.Keys.Count;
				}
				return count;
			}
		}

		// Token: 0x1700000B RID: 11
		// (get) Token: 0x0600008A RID: 138 RVA: 0x00003DB0 File Offset: 0x00001FB0
		public bool IsMarkedForDeletion
		{
			get
			{
				return !KeyHandler.dir_to_handler.Contains(this.Dir);
			}
		}

		// Token: 0x0600008B RID: 139 RVA: 0x00003DC8 File Offset: 0x00001FC8
		public void RemoveValue(string name)
		{
			this.AssertNotMarkedForDeletion();
			Hashtable obj = this.values;
			lock (obj)
			{
				this.values.Remove(name);
			}
			this.SetDirty();
		}

		// Token: 0x0600008C RID: 140 RVA: 0x00003E1C File Offset: 0x0000201C
		~KeyHandler()
		{
			this.Flush();
		}

		// Token: 0x0600008D RID: 141 RVA: 0x00003E48 File Offset: 0x00002048
		private void Save()
		{
			if (this.IsMarkedForDeletion)
			{
				return;
			}
			SecurityElement securityElement = new SecurityElement("values");
			Hashtable obj = this.values;
			lock (obj)
			{
				if (!File.Exists(this.file) && this.values.Count == 0)
				{
					return;
				}
				foreach (object obj2 in this.values)
				{
					DictionaryEntry dictionaryEntry = (DictionaryEntry)obj2;
					object value = dictionaryEntry.Value;
					SecurityElement securityElement2 = new SecurityElement("value");
					securityElement2.AddAttribute("name", SecurityElement.Escape((string)dictionaryEntry.Key));
					if (value is string)
					{
						securityElement2.AddAttribute("type", "string");
						securityElement2.Text = SecurityElement.Escape((string)value);
					}
					else if (value is int)
					{
						securityElement2.AddAttribute("type", "int");
						securityElement2.Text = value.ToString();
					}
					else if (value is long)
					{
						securityElement2.AddAttribute("type", "qword");
						securityElement2.Text = value.ToString();
					}
					else if (value is byte[])
					{
						securityElement2.AddAttribute("type", "bytearray");
						securityElement2.Text = Convert.ToBase64String((byte[])value);
					}
					else if (value is ExpandString)
					{
						securityElement2.AddAttribute("type", "expand");
						securityElement2.Text = SecurityElement.Escape(value.ToString());
					}
					else if (value is string[])
					{
						securityElement2.AddAttribute("type", "string-array");
						foreach (string str in (string[])value)
						{
							securityElement2.AddChild(new SecurityElement("string")
							{
								Text = SecurityElement.Escape(str)
							});
						}
					}
					securityElement.AddChild(securityElement2);
				}
			}
			using (FileStream fileStream = File.Create(this.file))
			{
				StreamWriter streamWriter = new StreamWriter(fileStream);
				streamWriter.Write(securityElement.ToString());
				streamWriter.Flush();
			}
		}

		// Token: 0x0600008E RID: 142 RVA: 0x000040E8 File Offset: 0x000022E8
		private void AssertNotMarkedForDeletion()
		{
			if (this.IsMarkedForDeletion)
			{
				throw RegistryKey.CreateMarkedForDeletionException();
			}
		}

		// Token: 0x1700000C RID: 12
		// (get) Token: 0x0600008F RID: 143 RVA: 0x000040F8 File Offset: 0x000022F8
		private static string UserStore
		{
			get
			{
				if (KeyHandler.user_store == null)
				{
					KeyHandler.user_store = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), ".mono/registry");
				}
				return KeyHandler.user_store;
			}
		}

		// Token: 0x1700000D RID: 13
		// (get) Token: 0x06000090 RID: 144 RVA: 0x0000411C File Offset: 0x0000231C
		private static string MachineStore
		{
			get
			{
				if (KeyHandler.machine_store == null)
				{
					KeyHandler.machine_store = Environment.GetEnvironmentVariable("MONO_REGISTRY_PATH");
					if (KeyHandler.machine_store == null)
					{
						string machineConfigPath = Environment.GetMachineConfigPath();
						int num = machineConfigPath.IndexOf("machine.config");
						KeyHandler.machine_store = Path.Combine(Path.Combine(machineConfigPath.Substring(0, num - 1), ".."), "registry");
					}
				}
				return KeyHandler.machine_store;
			}
		}

		// Token: 0x0400039D RID: 925
		private static Hashtable key_to_handler = new Hashtable(new RegistryKeyComparer());

		// Token: 0x0400039E RID: 926
		private static Hashtable dir_to_handler = new Hashtable(new CaseInsensitiveHashCodeProvider(), new CaseInsensitiveComparer());

		// Token: 0x0400039F RID: 927
		private const string VolatileDirectoryName = "volatile-keys";

		// Token: 0x040003A0 RID: 928
		public string Dir;

		// Token: 0x040003A1 RID: 929
		private string ActualDir;

		// Token: 0x040003A2 RID: 930
		public bool IsVolatile;

		// Token: 0x040003A3 RID: 931
		private Hashtable values;

		// Token: 0x040003A4 RID: 932
		private string file;

		// Token: 0x040003A5 RID: 933
		private bool dirty;

		// Token: 0x040003A6 RID: 934
		private static string user_store;

		// Token: 0x040003A7 RID: 935
		private static string machine_store;
	}
}
