﻿using System;

namespace Microsoft.Win32
{
	/// <summary>Specifies options to use when creating a registry key.</summary>
	// Token: 0x02000010 RID: 16
	[Flags]
	[Serializable]
	public enum RegistryOptions
	{
		/// <summary>A non-volatile key. This is the default.</summary>
		// Token: 0x0400038A RID: 906
		None = 0,
		/// <summary>A volatile key. The information is stored in memory and is not preserved when the corresponding registry hive is unloaded.</summary>
		// Token: 0x0400038B RID: 907
		Volatile = 1
	}
}
