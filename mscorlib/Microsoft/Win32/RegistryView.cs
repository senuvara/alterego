﻿using System;

namespace Microsoft.Win32
{
	/// <summary>Specifies which registry view to target on a 64-bit operating system.</summary>
	// Token: 0x02000013 RID: 19
	[Serializable]
	public enum RegistryView
	{
		/// <summary>The default view.</summary>
		// Token: 0x04000399 RID: 921
		Default,
		/// <summary>The 64-bit view.</summary>
		// Token: 0x0400039A RID: 922
		Registry64 = 256,
		/// <summary>The 32-bit view.</summary>
		// Token: 0x0400039B RID: 923
		Registry32 = 512
	}
}
