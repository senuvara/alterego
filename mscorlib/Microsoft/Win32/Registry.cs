﻿using System;
using System.Runtime.InteropServices;

namespace Microsoft.Win32
{
	/// <summary>Provides <see cref="T:Microsoft.Win32.RegistryKey" /> objects that represent the root keys in the Windows registry, and <see langword="static" /> methods to access key/value pairs.</summary>
	// Token: 0x0200000C RID: 12
	[ComVisible(true)]
	public static class Registry
	{
		// Token: 0x06000023 RID: 35 RVA: 0x000020D8 File Offset: 0x000002D8
		private static RegistryKey ToKey(string keyName, bool setting)
		{
			if (keyName == null)
			{
				throw new ArgumentException("Not a valid registry key name", "keyName");
			}
			string[] array = keyName.Split(new char[]
			{
				'\\'
			});
			string text = array[0];
			uint num = <PrivateImplementationDetails>.ComputeStringHash(text);
			RegistryKey registryKey;
			if (num <= 1097425318U)
			{
				if (num != 126972219U)
				{
					if (num != 457190004U)
					{
						if (num == 1097425318U)
						{
							if (text == "HKEY_CLASSES_ROOT")
							{
								registryKey = Registry.ClassesRoot;
								goto IL_146;
							}
						}
					}
					else if (text == "HKEY_LOCAL_MACHINE")
					{
						registryKey = Registry.LocalMachine;
						goto IL_146;
					}
				}
				else if (text == "HKEY_CURRENT_CONFIG")
				{
					registryKey = Registry.CurrentConfig;
					goto IL_146;
				}
			}
			else if (num <= 1568329430U)
			{
				if (num != 1198714601U)
				{
					if (num == 1568329430U)
					{
						if (text == "HKEY_CURRENT_USER")
						{
							registryKey = Registry.CurrentUser;
							goto IL_146;
						}
					}
				}
				else if (text == "HKEY_USERS")
				{
					registryKey = Registry.Users;
					goto IL_146;
				}
			}
			else if (num != 2823865611U)
			{
				if (num == 3554990456U)
				{
					if (text == "HKEY_PERFORMANCE_DATA")
					{
						registryKey = Registry.PerformanceData;
						goto IL_146;
					}
				}
			}
			else if (text == "HKEY_DYN_DATA")
			{
				registryKey = Registry.DynData;
				goto IL_146;
			}
			throw new ArgumentException("Keyname does not start with a valid registry root", "keyName");
			IL_146:
			for (int i = 1; i < array.Length; i++)
			{
				RegistryKey registryKey2 = registryKey.OpenSubKey(array[i], setting);
				if (registryKey2 == null)
				{
					if (!setting)
					{
						return null;
					}
					registryKey2 = registryKey.CreateSubKey(array[i]);
				}
				registryKey = registryKey2;
			}
			return registryKey;
		}

		/// <summary>Sets the specified name/value pair on the specified registry key. If the specified key does not exist, it is created.</summary>
		/// <param name="keyName">The full registry path of the key, beginning with a valid registry root, such as "HKEY_CURRENT_USER".</param>
		/// <param name="valueName">The name of the name/value pair.</param>
		/// <param name="value">The value to be stored.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="value" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="keyName" /> does not begin with a valid registry root. -or-
		///         <paramref name="keyName" /> is longer than the maximum length allowed (255 characters).</exception>
		/// <exception cref="T:System.UnauthorizedAccessException">The <see cref="T:Microsoft.Win32.RegistryKey" /> is read-only, and thus cannot be written to; for example, it is a root-level node. </exception>
		/// <exception cref="T:System.Security.SecurityException">The user does not have the permissions required to create or modify registry keys. </exception>
		// Token: 0x06000024 RID: 36 RVA: 0x00002263 File Offset: 0x00000463
		public static void SetValue(string keyName, string valueName, object value)
		{
			RegistryKey registryKey = Registry.ToKey(keyName, true);
			if (valueName.Length > 255)
			{
				throw new ArgumentException("valueName is larger than 255 characters", "valueName");
			}
			if (registryKey == null)
			{
				throw new ArgumentException("cant locate that keyName", "keyName");
			}
			registryKey.SetValue(valueName, value);
		}

		/// <summary>Sets the name/value pair on the specified registry key, using the specified registry data type. If the specified key does not exist, it is created.</summary>
		/// <param name="keyName">The full registry path of the key, beginning with a valid registry root, such as "HKEY_CURRENT_USER".</param>
		/// <param name="valueName">The name of the name/value pair.</param>
		/// <param name="value">The value to be stored.</param>
		/// <param name="valueKind">The registry data type to use when storing the data.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="value" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="keyName" /> does not begin with a valid registry root.-or-
		///         <paramref name="keyName" /> is longer than the maximum length allowed (255 characters).-or- The type of <paramref name="value" /> did not match the registry data type specified by <paramref name="valueKind" />, therefore the data could not be converted properly. </exception>
		/// <exception cref="T:System.UnauthorizedAccessException">The <see cref="T:Microsoft.Win32.RegistryKey" /> is read-only, and thus cannot be written to; for example, it is a root-level node, or the key has not been opened with write access. </exception>
		/// <exception cref="T:System.Security.SecurityException">The user does not have the permissions required to create or modify registry keys. </exception>
		// Token: 0x06000025 RID: 37 RVA: 0x000022A4 File Offset: 0x000004A4
		public static void SetValue(string keyName, string valueName, object value, RegistryValueKind valueKind)
		{
			RegistryKey registryKey = Registry.ToKey(keyName, true);
			if (valueName.Length > 255)
			{
				throw new ArgumentException("valueName is larger than 255 characters", "valueName");
			}
			if (registryKey == null)
			{
				throw new ArgumentException("cant locate that keyName", "keyName");
			}
			registryKey.SetValue(valueName, value, valueKind);
		}

		/// <summary>Retrieves the value associated with the specified name, in the specified registry key. If the name is not found in the specified key, returns a default value that you provide, or <see langword="null" /> if the specified key does not exist. </summary>
		/// <param name="keyName">The full registry path of the key, beginning with a valid registry root, such as "HKEY_CURRENT_USER".</param>
		/// <param name="valueName">The name of the name/value pair.</param>
		/// <param name="defaultValue">The value to return if <paramref name="valueName" /> does not exist.</param>
		/// <returns>
		///     <see langword="null" /> if the subkey specified by <paramref name="keyName" /> does not exist; otherwise, the value associated with <paramref name="valueName" />, or <paramref name="defaultValue" /> if <paramref name="valueName" /> is not found.</returns>
		/// <exception cref="T:System.Security.SecurityException">The user does not have the permissions required to read from the registry key. </exception>
		/// <exception cref="T:System.IO.IOException">The <see cref="T:Microsoft.Win32.RegistryKey" /> that contains the specified value has been marked for deletion. </exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="keyName" /> does not begin with a valid registry root. </exception>
		// Token: 0x06000026 RID: 38 RVA: 0x000022F0 File Offset: 0x000004F0
		public static object GetValue(string keyName, string valueName, object defaultValue)
		{
			RegistryKey registryKey = Registry.ToKey(keyName, false);
			if (registryKey == null)
			{
				return defaultValue;
			}
			return registryKey.GetValue(valueName, defaultValue);
		}

		// Token: 0x06000027 RID: 39 RVA: 0x00002314 File Offset: 0x00000514
		// Note: this type is marked as 'beforefieldinit'.
		static Registry()
		{
		}

		/// <summary>Defines the types (or classes) of documents and the properties associated with those types. This field reads the Windows registry base key HKEY_CLASSES_ROOT.</summary>
		// Token: 0x0400036F RID: 879
		public static readonly RegistryKey ClassesRoot = new RegistryKey(RegistryHive.ClassesRoot);

		/// <summary>Contains configuration information pertaining to the hardware that is not specific to the user. This field reads the Windows registry base key HKEY_CURRENT_CONFIG.</summary>
		// Token: 0x04000370 RID: 880
		public static readonly RegistryKey CurrentConfig = new RegistryKey(RegistryHive.CurrentConfig);

		/// <summary>Contains information about the current user preferences. This field reads the Windows registry base key HKEY_CURRENT_USER </summary>
		// Token: 0x04000371 RID: 881
		public static readonly RegistryKey CurrentUser = new RegistryKey(RegistryHive.CurrentUser);

		/// <summary>Contains dynamic registry data. This field reads the Windows registry base key HKEY_DYN_DATA.</summary>
		/// <exception cref="T:System.ObjectDisposedException">The operating system does not support dynamic data; that is, it is not Windows 98, Windows 98 Second Edition, or Windows Millennium Edition (Windows Me).</exception>
		// Token: 0x04000372 RID: 882
		[Obsolete("Use PerformanceData instead")]
		public static readonly RegistryKey DynData = new RegistryKey(RegistryHive.DynData);

		/// <summary>Contains the configuration data for the local machine. This field reads the Windows registry base key HKEY_LOCAL_MACHINE.</summary>
		// Token: 0x04000373 RID: 883
		public static readonly RegistryKey LocalMachine = new RegistryKey(RegistryHive.LocalMachine);

		/// <summary>Contains performance information for software components. This field reads the Windows registry base key HKEY_PERFORMANCE_DATA.</summary>
		// Token: 0x04000374 RID: 884
		public static readonly RegistryKey PerformanceData = new RegistryKey(RegistryHive.PerformanceData);

		/// <summary>Contains information about the default user configuration. This field reads the Windows registry base key HKEY_USERS.</summary>
		// Token: 0x04000375 RID: 885
		public static readonly RegistryKey Users = new RegistryKey(RegistryHive.Users);
	}
}
