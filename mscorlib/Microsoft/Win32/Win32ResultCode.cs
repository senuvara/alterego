﻿using System;

namespace Microsoft.Win32
{
	// Token: 0x02000019 RID: 25
	internal class Win32ResultCode
	{
		// Token: 0x060000D4 RID: 212 RVA: 0x00002050 File Offset: 0x00000250
		public Win32ResultCode()
		{
		}

		// Token: 0x040003B1 RID: 945
		public const int Success = 0;

		// Token: 0x040003B2 RID: 946
		public const int FileNotFound = 2;

		// Token: 0x040003B3 RID: 947
		public const int AccessDenied = 5;

		// Token: 0x040003B4 RID: 948
		public const int InvalidHandle = 6;

		// Token: 0x040003B5 RID: 949
		public const int InvalidParameter = 87;

		// Token: 0x040003B6 RID: 950
		public const int MoreData = 234;

		// Token: 0x040003B7 RID: 951
		public const int NetworkPathNotFound = 53;

		// Token: 0x040003B8 RID: 952
		public const int NoMoreEntries = 259;

		// Token: 0x040003B9 RID: 953
		public const int MarkedForDeletion = 1018;

		// Token: 0x040003BA RID: 954
		public const int ChildMustBeVolatile = 1021;
	}
}
