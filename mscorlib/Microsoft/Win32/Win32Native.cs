﻿using System;
using System.IO;

namespace Microsoft.Win32
{
	// Token: 0x0200001B RID: 27
	internal static class Win32Native
	{
		// Token: 0x060000D6 RID: 214 RVA: 0x00004D96 File Offset: 0x00002F96
		public static string GetMessage(int hr)
		{
			return "Error " + hr;
		}

		// Token: 0x060000D7 RID: 215 RVA: 0x00004DA8 File Offset: 0x00002FA8
		public static int MakeHRFromErrorCode(int errorCode)
		{
			return -2147024896 | errorCode;
		}

		// Token: 0x040003BB RID: 955
		internal const string ADVAPI32 = "advapi32.dll";

		// Token: 0x040003BC RID: 956
		internal const int ERROR_SUCCESS = 0;

		// Token: 0x040003BD RID: 957
		internal const int ERROR_INVALID_FUNCTION = 1;

		// Token: 0x040003BE RID: 958
		internal const int ERROR_FILE_NOT_FOUND = 2;

		// Token: 0x040003BF RID: 959
		internal const int ERROR_PATH_NOT_FOUND = 3;

		// Token: 0x040003C0 RID: 960
		internal const int ERROR_ACCESS_DENIED = 5;

		// Token: 0x040003C1 RID: 961
		internal const int ERROR_INVALID_HANDLE = 6;

		// Token: 0x040003C2 RID: 962
		internal const int ERROR_NOT_ENOUGH_MEMORY = 8;

		// Token: 0x040003C3 RID: 963
		internal const int ERROR_INVALID_DATA = 13;

		// Token: 0x040003C4 RID: 964
		internal const int ERROR_INVALID_DRIVE = 15;

		// Token: 0x040003C5 RID: 965
		internal const int ERROR_NO_MORE_FILES = 18;

		// Token: 0x040003C6 RID: 966
		internal const int ERROR_NOT_READY = 21;

		// Token: 0x040003C7 RID: 967
		internal const int ERROR_BAD_LENGTH = 24;

		// Token: 0x040003C8 RID: 968
		internal const int ERROR_SHARING_VIOLATION = 32;

		// Token: 0x040003C9 RID: 969
		internal const int ERROR_NOT_SUPPORTED = 50;

		// Token: 0x040003CA RID: 970
		internal const int ERROR_FILE_EXISTS = 80;

		// Token: 0x040003CB RID: 971
		internal const int ERROR_INVALID_PARAMETER = 87;

		// Token: 0x040003CC RID: 972
		internal const int ERROR_BROKEN_PIPE = 109;

		// Token: 0x040003CD RID: 973
		internal const int ERROR_CALL_NOT_IMPLEMENTED = 120;

		// Token: 0x040003CE RID: 974
		internal const int ERROR_INSUFFICIENT_BUFFER = 122;

		// Token: 0x040003CF RID: 975
		internal const int ERROR_INVALID_NAME = 123;

		// Token: 0x040003D0 RID: 976
		internal const int ERROR_BAD_PATHNAME = 161;

		// Token: 0x040003D1 RID: 977
		internal const int ERROR_ALREADY_EXISTS = 183;

		// Token: 0x040003D2 RID: 978
		internal const int ERROR_ENVVAR_NOT_FOUND = 203;

		// Token: 0x040003D3 RID: 979
		internal const int ERROR_FILENAME_EXCED_RANGE = 206;

		// Token: 0x040003D4 RID: 980
		internal const int ERROR_NO_DATA = 232;

		// Token: 0x040003D5 RID: 981
		internal const int ERROR_PIPE_NOT_CONNECTED = 233;

		// Token: 0x040003D6 RID: 982
		internal const int ERROR_MORE_DATA = 234;

		// Token: 0x040003D7 RID: 983
		internal const int ERROR_DIRECTORY = 267;

		// Token: 0x040003D8 RID: 984
		internal const int ERROR_OPERATION_ABORTED = 995;

		// Token: 0x040003D9 RID: 985
		internal const int ERROR_NOT_FOUND = 1168;

		// Token: 0x040003DA RID: 986
		internal const int ERROR_NO_TOKEN = 1008;

		// Token: 0x040003DB RID: 987
		internal const int ERROR_DLL_INIT_FAILED = 1114;

		// Token: 0x040003DC RID: 988
		internal const int ERROR_NON_ACCOUNT_SID = 1257;

		// Token: 0x040003DD RID: 989
		internal const int ERROR_NOT_ALL_ASSIGNED = 1300;

		// Token: 0x040003DE RID: 990
		internal const int ERROR_UNKNOWN_REVISION = 1305;

		// Token: 0x040003DF RID: 991
		internal const int ERROR_INVALID_OWNER = 1307;

		// Token: 0x040003E0 RID: 992
		internal const int ERROR_INVALID_PRIMARY_GROUP = 1308;

		// Token: 0x040003E1 RID: 993
		internal const int ERROR_NO_SUCH_PRIVILEGE = 1313;

		// Token: 0x040003E2 RID: 994
		internal const int ERROR_PRIVILEGE_NOT_HELD = 1314;

		// Token: 0x040003E3 RID: 995
		internal const int ERROR_NONE_MAPPED = 1332;

		// Token: 0x040003E4 RID: 996
		internal const int ERROR_INVALID_ACL = 1336;

		// Token: 0x040003E5 RID: 997
		internal const int ERROR_INVALID_SID = 1337;

		// Token: 0x040003E6 RID: 998
		internal const int ERROR_INVALID_SECURITY_DESCR = 1338;

		// Token: 0x040003E7 RID: 999
		internal const int ERROR_BAD_IMPERSONATION_LEVEL = 1346;

		// Token: 0x040003E8 RID: 1000
		internal const int ERROR_CANT_OPEN_ANONYMOUS = 1347;

		// Token: 0x040003E9 RID: 1001
		internal const int ERROR_NO_SECURITY_ON_OBJECT = 1350;

		// Token: 0x040003EA RID: 1002
		internal const int ERROR_TRUSTED_RELATIONSHIP_FAILURE = 1789;

		// Token: 0x040003EB RID: 1003
		internal const FileAttributes FILE_ATTRIBUTE_DIRECTORY = FileAttributes.Directory;

		// Token: 0x0200001C RID: 28
		public class SECURITY_ATTRIBUTES
		{
			// Token: 0x060000D8 RID: 216 RVA: 0x00002050 File Offset: 0x00000250
			public SECURITY_ATTRIBUTES()
			{
			}
		}

		// Token: 0x0200001D RID: 29
		internal class WIN32_FIND_DATA
		{
			// Token: 0x060000D9 RID: 217 RVA: 0x00002050 File Offset: 0x00000250
			public WIN32_FIND_DATA()
			{
			}

			// Token: 0x040003EC RID: 1004
			internal int dwFileAttributes;

			// Token: 0x040003ED RID: 1005
			internal string cFileName;
		}
	}
}
