﻿using System;
using System.Runtime.CompilerServices;

namespace Microsoft.Win32
{
	// Token: 0x0200001A RID: 26
	internal static class NativeMethods
	{
		// Token: 0x060000D5 RID: 213
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern int GetCurrentProcessId();
	}
}
