﻿using System;
using System.Text;

namespace Microsoft.Win32
{
	// Token: 0x02000014 RID: 20
	internal class ExpandString
	{
		// Token: 0x06000064 RID: 100 RVA: 0x00002AC4 File Offset: 0x00000CC4
		public ExpandString(string s)
		{
			this.value = s;
		}

		// Token: 0x06000065 RID: 101 RVA: 0x00002AD3 File Offset: 0x00000CD3
		public override string ToString()
		{
			return this.value;
		}

		// Token: 0x06000066 RID: 102 RVA: 0x00002ADC File Offset: 0x00000CDC
		public string Expand()
		{
			StringBuilder stringBuilder = new StringBuilder();
			for (int i = 0; i < this.value.Length; i++)
			{
				if (this.value[i] == '%')
				{
					int j;
					for (j = i + 1; j < this.value.Length; j++)
					{
						if (this.value[j] == '%')
						{
							string variable = this.value.Substring(i + 1, j - i - 1);
							stringBuilder.Append(Environment.GetEnvironmentVariable(variable));
							i += j;
							break;
						}
					}
					if (j == this.value.Length)
					{
						stringBuilder.Append('%');
					}
				}
				else
				{
					stringBuilder.Append(this.value[i]);
				}
			}
			return stringBuilder.ToString();
		}

		// Token: 0x0400039C RID: 924
		private string value;
	}
}
