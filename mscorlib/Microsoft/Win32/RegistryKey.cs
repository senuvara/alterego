﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Security.AccessControl;
using System.Security.Permissions;
using System.Text;
using Microsoft.Win32.SafeHandles;
using Unity;

namespace Microsoft.Win32
{
	/// <summary>Represents a key-level node in the Windows registry. This class is a registry encapsulation.</summary>
	// Token: 0x0200000E RID: 14
	[ComVisible(true)]
	public sealed class RegistryKey : MarshalByRefObject, IDisposable
	{
		// Token: 0x06000028 RID: 40 RVA: 0x0000238A File Offset: 0x0000058A
		static RegistryKey()
		{
			if (Path.DirectorySeparatorChar == '\\')
			{
				RegistryKey.RegistryApi = new Win32RegistryApi();
				return;
			}
			RegistryKey.RegistryApi = new UnixRegistryApi();
		}

		// Token: 0x06000029 RID: 41 RVA: 0x000023AA File Offset: 0x000005AA
		internal RegistryKey(RegistryHive hiveId) : this(hiveId, new IntPtr((int)hiveId), false)
		{
		}

		// Token: 0x0600002A RID: 42 RVA: 0x000023BA File Offset: 0x000005BA
		internal RegistryKey(RegistryHive hiveId, IntPtr keyHandle, bool remoteRoot)
		{
			this.hive = hiveId;
			this.handle = keyHandle;
			this.qname = RegistryKey.GetHiveName(hiveId);
			this.isRemoteRoot = remoteRoot;
			this.isWritable = true;
		}

		// Token: 0x0600002B RID: 43 RVA: 0x000023F4 File Offset: 0x000005F4
		internal RegistryKey(object data, string keyName, bool writable)
		{
			this.handle = data;
			this.qname = keyName;
			this.isWritable = writable;
		}

		// Token: 0x0600002C RID: 44 RVA: 0x00002414 File Offset: 0x00000614
		internal static bool IsEquals(RegistryKey a, RegistryKey b)
		{
			return a.hive == b.hive && a.handle == b.handle && a.qname == b.qname && a.isRemoteRoot == b.isRemoteRoot && a.isWritable == b.isWritable;
		}

		/// <summary>Releases all resources used by the current instance of the <see cref="T:Microsoft.Win32.RegistryKey" /> class.</summary>
		// Token: 0x0600002D RID: 45 RVA: 0x0000246E File Offset: 0x0000066E
		public void Dispose()
		{
			GC.SuppressFinalize(this);
			this.Close();
		}

		/// <summary>Retrieves the name of the key.</summary>
		/// <returns>The absolute (qualified) name of the key.</returns>
		/// <exception cref="T:System.ObjectDisposedException">The <see cref="T:Microsoft.Win32.RegistryKey" /> is closed (closed keys cannot be accessed). </exception>
		// Token: 0x17000001 RID: 1
		// (get) Token: 0x0600002E RID: 46 RVA: 0x0000247C File Offset: 0x0000067C
		public string Name
		{
			get
			{
				return this.qname;
			}
		}

		/// <summary>Writes all the attributes of the specified open registry key into the registry.</summary>
		// Token: 0x0600002F RID: 47 RVA: 0x00002484 File Offset: 0x00000684
		public void Flush()
		{
			RegistryKey.RegistryApi.Flush(this);
		}

		/// <summary>Closes the key and flushes it to disk if its contents have been modified.</summary>
		// Token: 0x06000030 RID: 48 RVA: 0x00002491 File Offset: 0x00000691
		public void Close()
		{
			this.Flush();
			if (!this.isRemoteRoot && this.IsRoot)
			{
				return;
			}
			RegistryKey.RegistryApi.Close(this);
			this.handle = null;
			this.safe_handle = null;
		}

		/// <summary>Retrieves the count of subkeys of the current key.</summary>
		/// <returns>The number of subkeys of the current key.</returns>
		/// <exception cref="T:System.Security.SecurityException">The user does not have read permission for the key. </exception>
		/// <exception cref="T:System.ObjectDisposedException">The <see cref="T:Microsoft.Win32.RegistryKey" /> being manipulated is closed (closed keys cannot be accessed). </exception>
		/// <exception cref="T:System.UnauthorizedAccessException">The user does not have the necessary registry rights.</exception>
		/// <exception cref="T:System.IO.IOException">A system error occurred, for example the current key has been deleted.</exception>
		// Token: 0x17000002 RID: 2
		// (get) Token: 0x06000031 RID: 49 RVA: 0x000024C3 File Offset: 0x000006C3
		public int SubKeyCount
		{
			get
			{
				this.AssertKeyStillValid();
				return RegistryKey.RegistryApi.SubKeyCount(this);
			}
		}

		/// <summary>Retrieves the count of values in the key.</summary>
		/// <returns>The number of name/value pairs in the key.</returns>
		/// <exception cref="T:System.Security.SecurityException">The user does not have read permission for the key. </exception>
		/// <exception cref="T:System.ObjectDisposedException">The <see cref="T:Microsoft.Win32.RegistryKey" /> being manipulated is closed (closed keys cannot be accessed). </exception>
		/// <exception cref="T:System.UnauthorizedAccessException">The user does not have the necessary registry rights.</exception>
		/// <exception cref="T:System.IO.IOException">A system error occurred, for example the current key has been deleted.</exception>
		// Token: 0x17000003 RID: 3
		// (get) Token: 0x06000032 RID: 50 RVA: 0x000024D6 File Offset: 0x000006D6
		public int ValueCount
		{
			get
			{
				this.AssertKeyStillValid();
				return RegistryKey.RegistryApi.ValueCount(this);
			}
		}

		/// <summary>Gets a <see cref="T:Microsoft.Win32.SafeHandles.SafeRegistryHandle" /> object that represents the registry key that the current <see cref="T:Microsoft.Win32.RegistryKey" /> object encapsulates.</summary>
		/// <returns>The handle to the registry key.</returns>
		/// <exception cref="T:System.ObjectDisposedException">The registry key is closed. Closed keys cannot be accessed.</exception>
		/// <exception cref="T:System.UnauthorizedAccessException">The user does not have the necessary registry rights.</exception>
		/// <exception cref="T:System.IO.IOException">A system error occurred, such as deletion of the current key.</exception>
		/// <exception cref="T:System.Security.SecurityException">The user does not have the permissions required to read the key.</exception>
		// Token: 0x17000004 RID: 4
		// (get) Token: 0x06000033 RID: 51 RVA: 0x000024EC File Offset: 0x000006EC
		[ComVisible(false)]
		[MonoTODO("Not implemented in Unix")]
		public SafeRegistryHandle Handle
		{
			get
			{
				this.AssertKeyStillValid();
				if (this.safe_handle == null)
				{
					IntPtr preexistingHandle = RegistryKey.RegistryApi.GetHandle(this);
					this.safe_handle = new SafeRegistryHandle(preexistingHandle, true);
				}
				return this.safe_handle;
			}
		}

		/// <summary>Gets the view that was used to create the registry key. </summary>
		/// <returns>The view that was used to create the registry key.-or-
		///     <see cref="F:Microsoft.Win32.RegistryView.Default" />, if no view was used.</returns>
		// Token: 0x17000005 RID: 5
		// (get) Token: 0x06000034 RID: 52 RVA: 0x00002526 File Offset: 0x00000726
		[ComVisible(false)]
		[MonoLimitation("View is ignored in Mono.")]
		public RegistryView View
		{
			get
			{
				return RegistryView.Default;
			}
		}

		/// <summary>Sets the specified name/value pair.</summary>
		/// <param name="name">The name of the value to store. </param>
		/// <param name="value">The data to be stored. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="value" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="value" /> is an unsupported data type. </exception>
		/// <exception cref="T:System.ObjectDisposedException">The <see cref="T:Microsoft.Win32.RegistryKey" /> that contains the specified value is closed (closed keys cannot be accessed). </exception>
		/// <exception cref="T:System.UnauthorizedAccessException">The <see cref="T:Microsoft.Win32.RegistryKey" /> is read-only, and cannot be written to; for example, the key has not been opened with write access. -or-The <see cref="T:Microsoft.Win32.RegistryKey" /> object represents a root-level node, and the operating system is Windows Millennium Edition or Windows 98.</exception>
		/// <exception cref="T:System.Security.SecurityException">The user does not have the permissions required to create or modify registry keys. </exception>
		/// <exception cref="T:System.IO.IOException">The <see cref="T:Microsoft.Win32.RegistryKey" /> object represents a root-level node, and the operating system is Windows 2000, Windows XP, or Windows Server 2003.</exception>
		// Token: 0x06000035 RID: 53 RVA: 0x00002529 File Offset: 0x00000729
		public void SetValue(string name, object value)
		{
			this.AssertKeyStillValid();
			if (value == null)
			{
				throw new ArgumentNullException("value");
			}
			if (name != null)
			{
				this.AssertKeyNameLength(name);
			}
			if (!this.IsWritable)
			{
				throw new UnauthorizedAccessException("Cannot write to the registry key.");
			}
			RegistryKey.RegistryApi.SetValue(this, name, value);
		}

		/// <summary>Sets the value of a name/value pair in the registry key, using the specified registry data type.</summary>
		/// <param name="name">The name of the value to be stored. </param>
		/// <param name="value">The data to be stored. </param>
		/// <param name="valueKind">The registry data type to use when storing the data. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="value" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.ArgumentException">The type of <paramref name="value" /> did not match the registry data type specified by <paramref name="valueKind" />, therefore the data could not be converted properly. </exception>
		/// <exception cref="T:System.ObjectDisposedException">The <see cref="T:Microsoft.Win32.RegistryKey" /> that contains the specified value is closed (closed keys cannot be accessed). </exception>
		/// <exception cref="T:System.UnauthorizedAccessException">The <see cref="T:Microsoft.Win32.RegistryKey" /> is read-only, and cannot be written to; for example, the key has not been opened with write access.-or-The <see cref="T:Microsoft.Win32.RegistryKey" /> object represents a root-level node, and the operating system is Windows Millennium Edition or Windows 98. </exception>
		/// <exception cref="T:System.Security.SecurityException">The user does not have the permissions required to create or modify registry keys. </exception>
		/// <exception cref="T:System.IO.IOException">The <see cref="T:Microsoft.Win32.RegistryKey" /> object represents a root-level node, and the operating system is Windows 2000, Windows XP, or Windows Server 2003.</exception>
		// Token: 0x06000036 RID: 54 RVA: 0x0000256C File Offset: 0x0000076C
		[ComVisible(false)]
		public void SetValue(string name, object value, RegistryValueKind valueKind)
		{
			this.AssertKeyStillValid();
			if (value == null)
			{
				throw new ArgumentNullException("value");
			}
			if (name != null)
			{
				this.AssertKeyNameLength(name);
			}
			if (!this.IsWritable)
			{
				throw new UnauthorizedAccessException("Cannot write to the registry key.");
			}
			RegistryKey.RegistryApi.SetValue(this, name, value, valueKind);
		}

		/// <summary>Retrieves a subkey as read-only.</summary>
		/// <param name="name">The name or path of the subkey to open as read-only. </param>
		/// <returns>The subkey requested, or <see langword="null" /> if the operation failed.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="name" /> is <see langword="null" /></exception>
		/// <exception cref="T:System.ObjectDisposedException">The <see cref="T:Microsoft.Win32.RegistryKey" /> is closed (closed keys cannot be accessed). </exception>
		/// <exception cref="T:System.Security.SecurityException">The user does not have the permissions required to read the registry key. </exception>
		// Token: 0x06000037 RID: 55 RVA: 0x000025B8 File Offset: 0x000007B8
		public RegistryKey OpenSubKey(string name)
		{
			return this.OpenSubKey(name, false);
		}

		/// <summary>Retrieves a specified subkey, and specifies whether write access is to be applied to the key. </summary>
		/// <param name="name">Name or path of the subkey to open. </param>
		/// <param name="writable">Set to <see langword="true" /> if you need write access to the key. </param>
		/// <returns>The subkey requested, or <see langword="null" /> if the operation failed.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="name" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.ObjectDisposedException">The <see cref="T:Microsoft.Win32.RegistryKey" /> is closed (closed keys cannot be accessed). </exception>
		/// <exception cref="T:System.Security.SecurityException">The user does not have the permissions required to access the registry key in the specified mode. </exception>
		// Token: 0x06000038 RID: 56 RVA: 0x000025C2 File Offset: 0x000007C2
		public RegistryKey OpenSubKey(string name, bool writable)
		{
			this.AssertKeyStillValid();
			if (name == null)
			{
				throw new ArgumentNullException("name");
			}
			this.AssertKeyNameLength(name);
			return RegistryKey.RegistryApi.OpenSubKey(this, name, writable);
		}

		/// <summary>Retrieves the value associated with the specified name. Returns <see langword="null" /> if the name/value pair does not exist in the registry.</summary>
		/// <param name="name">The name of the value to retrieve. This string is not case-sensitive.</param>
		/// <returns>The value associated with <paramref name="name" />, or <see langword="null" /> if <paramref name="name" /> is not found.</returns>
		/// <exception cref="T:System.Security.SecurityException">The user does not have the permissions required to read from the registry key. </exception>
		/// <exception cref="T:System.ObjectDisposedException">The <see cref="T:Microsoft.Win32.RegistryKey" /> that contains the specified value is closed (closed keys cannot be accessed). </exception>
		/// <exception cref="T:System.IO.IOException">The <see cref="T:Microsoft.Win32.RegistryKey" /> that contains the specified value has been marked for deletion. </exception>
		/// <exception cref="T:System.UnauthorizedAccessException">The user does not have the necessary registry rights.</exception>
		// Token: 0x06000039 RID: 57 RVA: 0x000025EC File Offset: 0x000007EC
		public object GetValue(string name)
		{
			return this.GetValue(name, null);
		}

		/// <summary>Retrieves the value associated with the specified name. If the name is not found, returns the default value that you provide.</summary>
		/// <param name="name">The name of the value to retrieve. This string is not case-sensitive.</param>
		/// <param name="defaultValue">The value to return if <paramref name="name" /> does not exist. </param>
		/// <returns>The value associated with <paramref name="name" />, with any embedded environment variables left unexpanded, or <paramref name="defaultValue" /> if <paramref name="name" /> is not found.</returns>
		/// <exception cref="T:System.Security.SecurityException">The user does not have the permissions required to read from the registry key. </exception>
		/// <exception cref="T:System.ObjectDisposedException">The <see cref="T:Microsoft.Win32.RegistryKey" /> that contains the specified value is closed (closed keys cannot be accessed). </exception>
		/// <exception cref="T:System.IO.IOException">The <see cref="T:Microsoft.Win32.RegistryKey" /> that contains the specified value has been marked for deletion. </exception>
		/// <exception cref="T:System.UnauthorizedAccessException">The user does not have the necessary registry rights.</exception>
		// Token: 0x0600003A RID: 58 RVA: 0x000025F6 File Offset: 0x000007F6
		public object GetValue(string name, object defaultValue)
		{
			this.AssertKeyStillValid();
			return RegistryKey.RegistryApi.GetValue(this, name, defaultValue, RegistryValueOptions.None);
		}

		/// <summary>Retrieves the value associated with the specified name and retrieval options. If the name is not found, returns the default value that you provide.</summary>
		/// <param name="name">The name of the value to retrieve. This string is not case-sensitive.</param>
		/// <param name="defaultValue">The value to return if <paramref name="name" /> does not exist. </param>
		/// <param name="options">One of the enumeration values that specifies optional processing of the retrieved value.</param>
		/// <returns>The value associated with <paramref name="name" />, processed according to the specified <paramref name="options" />, or <paramref name="defaultValue" /> if <paramref name="name" /> is not found.</returns>
		/// <exception cref="T:System.Security.SecurityException">The user does not have the permissions required to read from the registry key. </exception>
		/// <exception cref="T:System.ObjectDisposedException">The <see cref="T:Microsoft.Win32.RegistryKey" /> that contains the specified value is closed (closed keys cannot be accessed). </exception>
		/// <exception cref="T:System.IO.IOException">The <see cref="T:Microsoft.Win32.RegistryKey" /> that contains the specified value has been marked for deletion. </exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="options" /> is not a valid <see cref="T:Microsoft.Win32.RegistryValueOptions" /> value; for example, an invalid value is cast to <see cref="T:Microsoft.Win32.RegistryValueOptions" />.</exception>
		/// <exception cref="T:System.UnauthorizedAccessException">The user does not have the necessary registry rights.</exception>
		// Token: 0x0600003B RID: 59 RVA: 0x0000260C File Offset: 0x0000080C
		[ComVisible(false)]
		public object GetValue(string name, object defaultValue, RegistryValueOptions options)
		{
			this.AssertKeyStillValid();
			return RegistryKey.RegistryApi.GetValue(this, name, defaultValue, options);
		}

		/// <summary>Retrieves the registry data type of the value associated with the specified name.</summary>
		/// <param name="name">The name of the value whose registry data type is to be retrieved. This string is not case-sensitive.</param>
		/// <returns>The registry data type of the value associated with <paramref name="name" />.</returns>
		/// <exception cref="T:System.Security.SecurityException">The user does not have the permissions required to read from the registry key. </exception>
		/// <exception cref="T:System.ObjectDisposedException">The <see cref="T:Microsoft.Win32.RegistryKey" /> that contains the specified value is closed (closed keys cannot be accessed). </exception>
		/// <exception cref="T:System.IO.IOException">The subkey that contains the specified value does not exist.-or-The name/value pair specified by <paramref name="name" /> does not exist.This exception is not thrown on Windows 95, Windows 98, or Windows Millennium Edition.</exception>
		/// <exception cref="T:System.UnauthorizedAccessException">The user does not have the necessary registry rights.</exception>
		// Token: 0x0600003C RID: 60 RVA: 0x00002622 File Offset: 0x00000822
		[ComVisible(false)]
		public RegistryValueKind GetValueKind(string name)
		{
			return RegistryKey.RegistryApi.GetValueKind(this, name);
		}

		/// <summary>Creates a new subkey or opens an existing subkey for write access.  </summary>
		/// <param name="subkey">The name or path of the subkey to create or open. This string is not case-sensitive.</param>
		/// <returns>The newly created subkey, or <see langword="null" /> if the operation failed. If a zero-length string is specified for <paramref name="subkey" />, the current <see cref="T:Microsoft.Win32.RegistryKey" /> object is returned.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="subkey" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.Security.SecurityException">The user does not have the permissions required to create or open the registry key. </exception>
		/// <exception cref="T:System.ObjectDisposedException">The <see cref="T:Microsoft.Win32.RegistryKey" /> on which this method is being invoked is closed (closed keys cannot be accessed). </exception>
		/// <exception cref="T:System.UnauthorizedAccessException">The <see cref="T:Microsoft.Win32.RegistryKey" /> cannot be written to; for example, it was not opened as a writable key , or the user does not have the necessary access rights. </exception>
		/// <exception cref="T:System.IO.IOException">The nesting level exceeds 510.-or-A system error occurred, such as deletion of the key, or an attempt to create a key in the <see cref="F:Microsoft.Win32.Registry.LocalMachine" /> root.</exception>
		// Token: 0x0600003D RID: 61 RVA: 0x00002630 File Offset: 0x00000830
		public RegistryKey CreateSubKey(string subkey)
		{
			this.AssertKeyStillValid();
			this.AssertKeyNameNotNull(subkey);
			this.AssertKeyNameLength(subkey);
			if (!this.IsWritable)
			{
				throw new UnauthorizedAccessException("Cannot write to the registry key.");
			}
			return RegistryKey.RegistryApi.CreateSubKey(this, subkey);
		}

		/// <summary>Creates a new subkey or opens an existing subkey for write access, using the specified permission check option. </summary>
		/// <param name="subkey">The name or path of the subkey to create or open. This string is not case-sensitive.</param>
		/// <param name="permissionCheck">One of the enumeration values that specifies whether the key is opened for read or read/write access.</param>
		/// <returns>The newly created subkey, or <see langword="null" /> if the operation failed. If a zero-length string is specified for <paramref name="subkey" />, the current <see cref="T:Microsoft.Win32.RegistryKey" /> object is returned.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="subkey" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.Security.SecurityException">The user does not have the permissions required to create or open the registry key. </exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="permissionCheck" /> contains an invalid value.</exception>
		/// <exception cref="T:System.ObjectDisposedException">The <see cref="T:Microsoft.Win32.RegistryKey" /> on which this method is being invoked is closed (closed keys cannot be accessed). </exception>
		/// <exception cref="T:System.UnauthorizedAccessException">The <see cref="T:Microsoft.Win32.RegistryKey" /> cannot be written to; for example, it was not opened as a writable key, or the user does not have the necessary access rights. </exception>
		/// <exception cref="T:System.IO.IOException">The nesting level exceeds 510.-or-A system error occurred, such as deletion of the key, or an attempt to create a key in the <see cref="F:Microsoft.Win32.Registry.LocalMachine" /> root.</exception>
		// Token: 0x0600003E RID: 62 RVA: 0x00002665 File Offset: 0x00000865
		[ComVisible(false)]
		[MonoLimitation("permissionCheck is ignored in Mono")]
		public RegistryKey CreateSubKey(string subkey, RegistryKeyPermissionCheck permissionCheck)
		{
			return this.CreateSubKey(subkey);
		}

		/// <summary>Creates a new subkey or opens an existing subkey for write access, using the specified permission check option and registry security. </summary>
		/// <param name="subkey">The name or path of the subkey to create or open. This string is not case-sensitive.</param>
		/// <param name="permissionCheck">One of the enumeration values that specifies whether the key is opened for read or read/write access.</param>
		/// <param name="registrySecurity">The access control security for the new key.</param>
		/// <returns>The newly created subkey, or <see langword="null" /> if the operation failed. If a zero-length string is specified for <paramref name="subkey" />, the current <see cref="T:Microsoft.Win32.RegistryKey" /> object is returned.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="subkey" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.Security.SecurityException">The user does not have the permissions required to create or open the registry key. </exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="permissionCheck" /> contains an invalid value.</exception>
		/// <exception cref="T:System.ObjectDisposedException">The <see cref="T:Microsoft.Win32.RegistryKey" /> on which this method is being invoked is closed (closed keys cannot be accessed). </exception>
		/// <exception cref="T:System.UnauthorizedAccessException">The current <see cref="T:Microsoft.Win32.RegistryKey" /> cannot be written to; for example, it was not opened as a writable key, or the user does not have the necessary access rights.</exception>
		/// <exception cref="T:System.IO.IOException">The nesting level exceeds 510.-or-A system error occurred, such as deletion of the key, or an attempt to create a key in the <see cref="F:Microsoft.Win32.Registry.LocalMachine" /> root.</exception>
		// Token: 0x0600003F RID: 63 RVA: 0x00002665 File Offset: 0x00000865
		[ComVisible(false)]
		[MonoLimitation("permissionCheck and registrySecurity are ignored in Mono")]
		public RegistryKey CreateSubKey(string subkey, RegistryKeyPermissionCheck permissionCheck, RegistrySecurity registrySecurity)
		{
			return this.CreateSubKey(subkey);
		}

		/// <summary>Creates a subkey or opens a subkey for write access, using the specified permission check and registry options. </summary>
		/// <param name="subkey">The name or path of the subkey to create or open. </param>
		/// <param name="permissionCheck">One of the enumeration values that specifies whether the key is opened for read or read/write access.</param>
		/// <param name="options">The registry option to use; for example, that creates a volatile key. </param>
		/// <returns>The newly created subkey, or <see langword="null" /> if the operation failed.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="subkey " />is <see langword="null" />.</exception>
		/// <exception cref="T:System.ObjectDisposedException">The current <see cref="T:Microsoft.Win32.RegistryKey" /> object is closed (closed keys cannot be accessed). </exception>
		/// <exception cref="T:System.UnauthorizedAccessException">The current <see cref="T:Microsoft.Win32.RegistryKey" /> object cannot be written to; for example, it was not opened as a writable key, or the user does not have the required access rights.</exception>
		/// <exception cref="T:System.IO.IOException">The nesting level exceeds 510.-or-A system error occurred, such as deletion of the key or an attempt to create a key in the <see cref="F:Microsoft.Win32.Registry.LocalMachine" /> root. </exception>
		/// <exception cref="T:System.Security.SecurityException">The user does not have the permissions required to create or open the registry key.</exception>
		// Token: 0x06000040 RID: 64 RVA: 0x0000266E File Offset: 0x0000086E
		[ComVisible(false)]
		[MonoLimitation("permissionCheck is ignored in Mono")]
		public RegistryKey CreateSubKey(string subkey, RegistryKeyPermissionCheck permissionCheck, RegistryOptions options)
		{
			this.AssertKeyStillValid();
			this.AssertKeyNameNotNull(subkey);
			this.AssertKeyNameLength(subkey);
			if (!this.IsWritable)
			{
				throw new UnauthorizedAccessException("Cannot write to the registry key.");
			}
			return RegistryKey.RegistryApi.CreateSubKey(this, subkey, options);
		}

		/// <summary>Creates a subkey or opens a subkey for write access, using the specified permission check option, registry option, and registry security.</summary>
		/// <param name="subkey">The name or path of the subkey to create or open.</param>
		/// <param name="permissionCheck">One of the enumeration values that specifies whether the key is opened for read or read/write access.</param>
		/// <param name="registryOptions">The registry option to use.</param>
		/// <param name="registrySecurity">The access control security for the new subkey. </param>
		/// <returns>The newly created subkey, or <see langword="null" /> if the operation failed.  </returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="subkey " />is <see langword="null" />.</exception>
		/// <exception cref="T:System.ObjectDisposedException">The current <see cref="T:Microsoft.Win32.RegistryKey" /> object is closed. Closed keys cannot be accessed. </exception>
		/// <exception cref="T:System.UnauthorizedAccessException">The current <see cref="T:Microsoft.Win32.RegistryKey" /> object cannot be written to; for example, it was not opened as a writable key, or the user does not have the required access rights.</exception>
		/// <exception cref="T:System.IO.IOException">The nesting level exceeds 510.-or-A system error occurred, such as deletion of the key or an attempt to create a key in the <see cref="F:Microsoft.Win32.Registry.LocalMachine" /> root. </exception>
		/// <exception cref="T:System.Security.SecurityException">The user does not have the permissions required to create or open the registry key.</exception>
		// Token: 0x06000041 RID: 65 RVA: 0x000026A4 File Offset: 0x000008A4
		[MonoLimitation("permissionCheck and registrySecurity are ignored in Mono")]
		[ComVisible(false)]
		public RegistryKey CreateSubKey(string subkey, RegistryKeyPermissionCheck permissionCheck, RegistryOptions registryOptions, RegistrySecurity registrySecurity)
		{
			return this.CreateSubKey(subkey, permissionCheck, registryOptions);
		}

		/// <summary>Creates a new subkey or opens an existing subkey with the specified access. Available starting in .NET Framework 4.6</summary>
		/// <param name="subkey">The name or path of the subkey to create or open. This string is not case-sensitive.</param>
		/// <param name="writable">
		///       <see langword="true" /> to indicate the new subkey is writable; otherwise, <see langword="false" />.</param>
		/// <returns>The newly created subkey, or <see langword="null" /> if the operation failed. If a zero-length string is specified for <paramref name="subkey" />, the current <see cref="T:Microsoft.Win32.RegistryKey" /> object is returned.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="subkey" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.Security.SecurityException">The user does not have the permissions required to create or open the registry key. </exception>
		/// <exception cref="T:System.UnauthorizedAccessException">The current <see cref="T:Microsoft.Win32.RegistryKey" /> cannot be written to; for example, it was not opened as a writable key, or the user does not have the necessary access rights.</exception>
		/// <exception cref="T:System.IO.IOException">The nesting level exceeds 510.-or-A system error occurred, such as deletion of the key, or an attempt to create a key in the <see cref="F:Microsoft.Win32.Registry.LocalMachine" /> root.</exception>
		// Token: 0x06000042 RID: 66 RVA: 0x000026AF File Offset: 0x000008AF
		[ComVisible(false)]
		public RegistryKey CreateSubKey(string subkey, bool writable)
		{
			return this.CreateSubKey(subkey, writable ? RegistryKeyPermissionCheck.ReadWriteSubTree : RegistryKeyPermissionCheck.ReadSubTree);
		}

		/// <summary>Creates a new subkey or opens an existing subkey with the specified access. Available starting in .NET Framework 4.6</summary>
		/// <param name="subkey">The name or path of the subkey to create or open. This string is not case-sensitive.</param>
		/// <param name="writable">
		///       <see langword="true" /> to indicate the new subkey is writable; otherwise, <see langword="false" />.</param>
		/// <param name="options">The registry option to use.</param>
		/// <returns>The newly created subkey, or <see langword="null" /> if the operation failed. If a zero-length string is specified for <paramref name="subkey" />, the current <see cref="T:Microsoft.Win32.RegistryKey" /> object is returned.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="subkey" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="options" /> does not specify a valid Option</exception>
		/// <exception cref="T:System.Security.SecurityException">The user does not have the permissions required to create or open the registry key. </exception>
		/// <exception cref="T:System.UnauthorizedAccessException">The current <see cref="T:Microsoft.Win32.RegistryKey" /> cannot be written to; for example, it was not opened as a writable key, or the user does not have the necessary access rights.</exception>
		/// <exception cref="T:System.IO.IOException">The nesting level exceeds 510.-or-A system error occurred, such as deletion of the key, or an attempt to create a key in the <see cref="F:Microsoft.Win32.Registry.LocalMachine" /> root.</exception>
		// Token: 0x06000043 RID: 67 RVA: 0x000026BF File Offset: 0x000008BF
		[ComVisible(false)]
		public RegistryKey CreateSubKey(string subkey, bool writable, RegistryOptions options)
		{
			return this.CreateSubKey(subkey, writable ? RegistryKeyPermissionCheck.ReadWriteSubTree : RegistryKeyPermissionCheck.ReadSubTree, options);
		}

		/// <summary>Deletes the specified subkey. </summary>
		/// <param name="subkey">The name of the subkey to delete. This string is not case-sensitive.</param>
		/// <exception cref="T:System.InvalidOperationException">The <paramref name="subkey" /> has child subkeys </exception>
		/// <exception cref="T:System.ArgumentException">The <paramref name="subkey" /> parameter does not specify a valid registry key </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="subkey" /> is <see langword="null" /></exception>
		/// <exception cref="T:System.Security.SecurityException">The user does not have the permissions required to delete the key. </exception>
		/// <exception cref="T:System.ObjectDisposedException">The <see cref="T:Microsoft.Win32.RegistryKey" /> being manipulated is closed (closed keys cannot be accessed). </exception>
		/// <exception cref="T:System.UnauthorizedAccessException">The user does not have the necessary registry rights.</exception>
		// Token: 0x06000044 RID: 68 RVA: 0x000026D0 File Offset: 0x000008D0
		public void DeleteSubKey(string subkey)
		{
			this.DeleteSubKey(subkey, true);
		}

		/// <summary>Deletes the specified subkey, and specifies whether an exception is raised if the subkey is not found. </summary>
		/// <param name="subkey">The name of the subkey to delete. This string is not case-sensitive.</param>
		/// <param name="throwOnMissingSubKey">Indicates whether an exception should be raised if the specified subkey cannot be found. If this argument is <see langword="true" /> and the specified subkey does not exist, an exception is raised. If this argument is <see langword="false" /> and the specified subkey does not exist, no action is taken. </param>
		/// <exception cref="T:System.InvalidOperationException">
		///         <paramref name="subkey" /> has child subkeys. </exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="subkey" /> does not specify a valid registry key, and <paramref name="throwOnMissingSubKey" /> is <see langword="true" />. </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="subkey" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.Security.SecurityException">The user does not have the permissions required to delete the key. </exception>
		/// <exception cref="T:System.ObjectDisposedException">The <see cref="T:Microsoft.Win32.RegistryKey" /> being manipulated is closed (closed keys cannot be accessed). </exception>
		/// <exception cref="T:System.UnauthorizedAccessException">The user does not have the necessary registry rights.</exception>
		// Token: 0x06000045 RID: 69 RVA: 0x000026DC File Offset: 0x000008DC
		public void DeleteSubKey(string subkey, bool throwOnMissingSubKey)
		{
			this.AssertKeyStillValid();
			this.AssertKeyNameNotNull(subkey);
			this.AssertKeyNameLength(subkey);
			if (!this.IsWritable)
			{
				throw new UnauthorizedAccessException("Cannot write to the registry key.");
			}
			RegistryKey registryKey = this.OpenSubKey(subkey);
			if (registryKey == null)
			{
				if (throwOnMissingSubKey)
				{
					throw new ArgumentException("Cannot delete a subkey tree because the subkey does not exist.");
				}
				return;
			}
			else
			{
				if (registryKey.SubKeyCount > 0)
				{
					throw new InvalidOperationException("Registry key has subkeys and recursive removes are not supported by this method.");
				}
				registryKey.Close();
				RegistryKey.RegistryApi.DeleteKey(this, subkey, throwOnMissingSubKey);
				return;
			}
		}

		/// <summary>Deletes a subkey and any child subkeys recursively. </summary>
		/// <param name="subkey">The subkey to delete. This string is not case-sensitive.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="subkey" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.ArgumentException">Deletion of a root hive is attempted.-or-
		///         <paramref name="subkey" /> does not specify a valid registry subkey. </exception>
		/// <exception cref="T:System.IO.IOException">An I/O error has occurred.</exception>
		/// <exception cref="T:System.Security.SecurityException">The user does not have the permissions required to delete the key. </exception>
		/// <exception cref="T:System.ObjectDisposedException">The <see cref="T:Microsoft.Win32.RegistryKey" /> being manipulated is closed (closed keys cannot be accessed). </exception>
		/// <exception cref="T:System.UnauthorizedAccessException">The user does not have the necessary registry rights.</exception>
		// Token: 0x06000046 RID: 70 RVA: 0x00002751 File Offset: 0x00000951
		public void DeleteSubKeyTree(string subkey)
		{
			this.DeleteSubKeyTree(subkey, true);
		}

		/// <summary>Deletes the specified subkey and any child subkeys recursively, and specifies whether an exception is raised if the subkey is not found. </summary>
		/// <param name="subkey">The name of the subkey to delete. This string is not case-sensitive.</param>
		/// <param name="throwOnMissingSubKey">Indicates whether an exception should be raised if the specified subkey cannot be found. If this argument is <see langword="true" /> and the specified subkey does not exist, an exception is raised. If this argument is <see langword="false" /> and the specified subkey does not exist, no action is taken.</param>
		/// <exception cref="T:System.ArgumentException">An attempt was made to delete the root hive of the tree.-or-
		///         <paramref name="subkey" /> does not specify a valid registry subkey, and <paramref name="throwOnMissingSubKey" /> is <see langword="true" />.</exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="subkey" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ObjectDisposedException">The <see cref="T:Microsoft.Win32.RegistryKey" /> is closed (closed keys cannot be accessed).</exception>
		/// <exception cref="T:System.UnauthorizedAccessException">The user does not have the necessary registry rights.</exception>
		/// <exception cref="T:System.Security.SecurityException">The user does not have the permissions required to delete the key.</exception>
		// Token: 0x06000047 RID: 71 RVA: 0x0000275C File Offset: 0x0000095C
		public void DeleteSubKeyTree(string subkey, bool throwOnMissingSubKey)
		{
			this.AssertKeyStillValid();
			this.AssertKeyNameNotNull(subkey);
			this.AssertKeyNameLength(subkey);
			RegistryKey registryKey = this.OpenSubKey(subkey, true);
			if (registryKey != null)
			{
				registryKey.DeleteChildKeysAndValues();
				registryKey.Close();
				this.DeleteSubKey(subkey, false);
				return;
			}
			if (!throwOnMissingSubKey)
			{
				return;
			}
			throw new ArgumentException("Cannot delete a subkey tree because the subkey does not exist.");
		}

		/// <summary>Deletes the specified value from this key.</summary>
		/// <param name="name">The name of the value to delete. </param>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="name" /> is not a valid reference to a value. </exception>
		/// <exception cref="T:System.Security.SecurityException">The user does not have the permissions required to delete the value. </exception>
		/// <exception cref="T:System.ObjectDisposedException">The <see cref="T:Microsoft.Win32.RegistryKey" /> being manipulated is closed (closed keys cannot be accessed). </exception>
		/// <exception cref="T:System.UnauthorizedAccessException">The <see cref="T:Microsoft.Win32.RegistryKey" /> being manipulated is read-only. </exception>
		// Token: 0x06000048 RID: 72 RVA: 0x000027AC File Offset: 0x000009AC
		public void DeleteValue(string name)
		{
			this.DeleteValue(name, true);
		}

		/// <summary>Deletes the specified value from this key, and specifies whether an exception is raised if the value is not found.</summary>
		/// <param name="name">The name of the value to delete. </param>
		/// <param name="throwOnMissingValue">Indicates whether an exception should be raised if the specified value cannot be found. If this argument is <see langword="true" /> and the specified value does not exist, an exception is raised. If this argument is <see langword="false" /> and the specified value does not exist, no action is taken. </param>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="name" /> is not a valid reference to a value and <paramref name="throwOnMissingValue" /> is <see langword="true" />. -or- 
		///         <paramref name="name" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.Security.SecurityException">The user does not have the permissions required to delete the value. </exception>
		/// <exception cref="T:System.ObjectDisposedException">The <see cref="T:Microsoft.Win32.RegistryKey" /> being manipulated is closed (closed keys cannot be accessed). </exception>
		/// <exception cref="T:System.UnauthorizedAccessException">The <see cref="T:Microsoft.Win32.RegistryKey" /> being manipulated is read-only. </exception>
		// Token: 0x06000049 RID: 73 RVA: 0x000027B6 File Offset: 0x000009B6
		public void DeleteValue(string name, bool throwOnMissingValue)
		{
			this.AssertKeyStillValid();
			if (name == null)
			{
				throw new ArgumentNullException("name");
			}
			if (!this.IsWritable)
			{
				throw new UnauthorizedAccessException("Cannot write to the registry key.");
			}
			RegistryKey.RegistryApi.DeleteValue(this, name, throwOnMissingValue);
		}

		/// <summary>Returns the access control security for the current registry key.</summary>
		/// <returns>An object that describes the access control permissions on the registry key represented by the current <see cref="T:Microsoft.Win32.RegistryKey" />.</returns>
		/// <exception cref="T:System.Security.SecurityException">The user does not have the necessary permissions.</exception>
		/// <exception cref="T:System.ObjectDisposedException">The <see cref="T:Microsoft.Win32.RegistryKey" /> being manipulated is closed (closed keys cannot be accessed).</exception>
		/// <exception cref="T:System.InvalidOperationException">The current key has been deleted.</exception>
		// Token: 0x0600004A RID: 74 RVA: 0x000027EC File Offset: 0x000009EC
		public RegistrySecurity GetAccessControl()
		{
			return this.GetAccessControl(AccessControlSections.Access | AccessControlSections.Owner | AccessControlSections.Group);
		}

		/// <summary>Returns the specified sections of the access control security for the current registry key.</summary>
		/// <param name="includeSections">A bitwise combination of enumeration values that specifies the type of security information to get. </param>
		/// <returns>An object that describes the access control permissions on the registry key represented by the current <see cref="T:Microsoft.Win32.RegistryKey" />.</returns>
		/// <exception cref="T:System.Security.SecurityException">The user does not have the necessary permissions.</exception>
		/// <exception cref="T:System.ObjectDisposedException">The <see cref="T:Microsoft.Win32.RegistryKey" /> being manipulated is closed (closed keys cannot be accessed).</exception>
		/// <exception cref="T:System.InvalidOperationException">The current key has been deleted.</exception>
		// Token: 0x0600004B RID: 75 RVA: 0x000027F6 File Offset: 0x000009F6
		public RegistrySecurity GetAccessControl(AccessControlSections includeSections)
		{
			return new RegistrySecurity(this.Name, includeSections);
		}

		/// <summary>Retrieves an array of strings that contains all the subkey names.</summary>
		/// <returns>An array of strings that contains the names of the subkeys for the current key.</returns>
		/// <exception cref="T:System.Security.SecurityException">The user does not have the permissions required to read from the key. </exception>
		/// <exception cref="T:System.ObjectDisposedException">The <see cref="T:Microsoft.Win32.RegistryKey" /> being manipulated is closed (closed keys cannot be accessed). </exception>
		/// <exception cref="T:System.UnauthorizedAccessException">The user does not have the necessary registry rights.</exception>
		/// <exception cref="T:System.IO.IOException">A system error occurred, for example the current key has been deleted.</exception>
		// Token: 0x0600004C RID: 76 RVA: 0x00002804 File Offset: 0x00000A04
		public string[] GetSubKeyNames()
		{
			this.AssertKeyStillValid();
			return RegistryKey.RegistryApi.GetSubKeyNames(this);
		}

		/// <summary>Retrieves an array of strings that contains all the value names associated with this key.</summary>
		/// <returns>An array of strings that contains the value names for the current key.</returns>
		/// <exception cref="T:System.Security.SecurityException">The user does not have the permissions required to read from the registry key. </exception>
		/// <exception cref="T:System.ObjectDisposedException">The <see cref="T:Microsoft.Win32.RegistryKey" />  being manipulated is closed (closed keys cannot be accessed). </exception>
		/// <exception cref="T:System.UnauthorizedAccessException">The user does not have the necessary registry rights.</exception>
		/// <exception cref="T:System.IO.IOException">A system error occurred; for example, the current key has been deleted.</exception>
		// Token: 0x0600004D RID: 77 RVA: 0x00002817 File Offset: 0x00000A17
		public string[] GetValueNames()
		{
			this.AssertKeyStillValid();
			return RegistryKey.RegistryApi.GetValueNames(this);
		}

		/// <summary>Creates a registry key from a specified handle.</summary>
		/// <param name="handle">The handle to the registry key.</param>
		/// <returns>A registry key.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="handle" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.UnauthorizedAccessException">The user does not have the necessary registry rights.</exception>
		/// <exception cref="T:System.Security.SecurityException">The user does not have the permissions required to perform this action.</exception>
		// Token: 0x0600004E RID: 78 RVA: 0x0000282A File Offset: 0x00000A2A
		[MonoTODO("Not implemented on unix")]
		[ComVisible(false)]
		[SecurityPermission(SecurityAction.Demand, Flags = SecurityPermissionFlag.UnmanagedCode)]
		public static RegistryKey FromHandle(SafeRegistryHandle handle)
		{
			if (handle == null)
			{
				throw new ArgumentNullException("handle");
			}
			return RegistryKey.RegistryApi.FromHandle(handle);
		}

		/// <summary>Creates a registry key from a specified handle and registry view setting. </summary>
		/// <param name="handle">The handle to the registry key.</param>
		/// <param name="view">The registry view to use.</param>
		/// <returns>A registry key.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="view" /> is invalid.</exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="handle" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.UnauthorizedAccessException">The user does not have the necessary registry rights.</exception>
		/// <exception cref="T:System.Security.SecurityException">The user does not have the permissions required to perform this action.</exception>
		// Token: 0x0600004F RID: 79 RVA: 0x00002845 File Offset: 0x00000A45
		[MonoTODO("Not implemented on unix")]
		[ComVisible(false)]
		[SecurityPermission(SecurityAction.Demand, Flags = SecurityPermissionFlag.UnmanagedCode)]
		public static RegistryKey FromHandle(SafeRegistryHandle handle, RegistryView view)
		{
			return RegistryKey.FromHandle(handle);
		}

		/// <summary>Opens a new <see cref="T:Microsoft.Win32.RegistryKey" /> that represents the requested key on a remote machine.</summary>
		/// <param name="hKey">The HKEY to open, from the <see cref="T:Microsoft.Win32.RegistryHive" /> enumeration. </param>
		/// <param name="machineName">The remote machine. </param>
		/// <returns>The requested registry key.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="hKey" /> is invalid.</exception>
		/// <exception cref="T:System.IO.IOException">
		///         <paramref name="machineName" /> is not found.</exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="machineName" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.Security.SecurityException">The user does not have the proper permissions to perform this operation. </exception>
		/// <exception cref="T:System.UnauthorizedAccessException">The user does not have the necessary registry rights.</exception>
		// Token: 0x06000050 RID: 80 RVA: 0x0000284D File Offset: 0x00000A4D
		[MonoTODO("Not implemented on unix")]
		public static RegistryKey OpenRemoteBaseKey(RegistryHive hKey, string machineName)
		{
			if (machineName == null)
			{
				throw new ArgumentNullException("machineName");
			}
			return RegistryKey.RegistryApi.OpenRemoteBaseKey(hKey, machineName);
		}

		/// <summary>Opens a new registry key that represents the requested key on a remote machine with the specified view.</summary>
		/// <param name="hKey">The HKEY to open from the <see cref="T:Microsoft.Win32.RegistryHive" /> enumeration.. </param>
		/// <param name="machineName">The remote machine.</param>
		/// <param name="view">The registry view to use.</param>
		/// <returns>The requested registry key.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="hKey" /> or <paramref name="view" /> is invalid.</exception>
		/// <exception cref="T:System.IO.IOException">
		///         <paramref name="machineName" /> is not found.</exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="machineName" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="machineName" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.UnauthorizedAccessException">The user does not have the necessary registry rights.</exception>
		/// <exception cref="T:System.Security.SecurityException">The user does not have the required permissions to perform this operation.</exception>
		// Token: 0x06000051 RID: 81 RVA: 0x0000284D File Offset: 0x00000A4D
		[MonoTODO("Not implemented on unix")]
		[ComVisible(false)]
		public static RegistryKey OpenRemoteBaseKey(RegistryHive hKey, string machineName, RegistryView view)
		{
			if (machineName == null)
			{
				throw new ArgumentNullException("machineName");
			}
			return RegistryKey.RegistryApi.OpenRemoteBaseKey(hKey, machineName);
		}

		/// <summary>Opens a new <see cref="T:Microsoft.Win32.RegistryKey" /> that represents the requested key on the local machine with the specified view.</summary>
		/// <param name="hKey">The HKEY to open.</param>
		/// <param name="view">The registry view to use.</param>
		/// <returns>The requested registry key.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="hKey" /> or <paramref name="view" /> is invalid.</exception>
		/// <exception cref="T:System.UnauthorizedAccessException">The user does not have the necessary registry rights.</exception>
		/// <exception cref="T:System.Security.SecurityException">The user does not have the permissions required to perform this action.</exception>
		// Token: 0x06000052 RID: 82 RVA: 0x0000286C File Offset: 0x00000A6C
		[ComVisible(false)]
		[MonoLimitation("View is ignored in Mono")]
		public static RegistryKey OpenBaseKey(RegistryHive hKey, RegistryView view)
		{
			switch (hKey)
			{
			case RegistryHive.ClassesRoot:
				return Registry.ClassesRoot;
			case RegistryHive.CurrentUser:
				return Registry.CurrentUser;
			case RegistryHive.LocalMachine:
				return Registry.LocalMachine;
			case RegistryHive.Users:
				return Registry.Users;
			case RegistryHive.PerformanceData:
				return Registry.PerformanceData;
			case RegistryHive.CurrentConfig:
				return Registry.CurrentConfig;
			case RegistryHive.DynData:
				return Registry.DynData;
			default:
				throw new ArgumentException("hKey");
			}
		}

		/// <summary>Retrieves the specified subkey for read or read/write access.</summary>
		/// <param name="name">The name or path of the subkey to create or open.</param>
		/// <param name="permissionCheck">One of the enumeration values that specifies whether the key is opened for read or read/write access.</param>
		/// <returns>The subkey requested, or <see langword="null" /> if the operation failed.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="name" /> is <see langword="null" /></exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="permissionCheck" /> contains an invalid value.</exception>
		/// <exception cref="T:System.ObjectDisposedException">The <see cref="T:Microsoft.Win32.RegistryKey" /> is closed (closed keys cannot be accessed). </exception>
		/// <exception cref="T:System.Security.SecurityException">The user does not have the permissions required to read the registry key. </exception>
		// Token: 0x06000053 RID: 83 RVA: 0x000028D7 File Offset: 0x00000AD7
		[ComVisible(false)]
		public RegistryKey OpenSubKey(string name, RegistryKeyPermissionCheck permissionCheck)
		{
			return this.OpenSubKey(name, permissionCheck == RegistryKeyPermissionCheck.ReadWriteSubTree);
		}

		/// <summary>Retrieves a subkey with the specified name and .Available starting in .NET Framework 4.6</summary>
		/// <param name="name">The name or path of the subkey to create or open.</param>
		/// <param name="rights">The rights for the registry key.</param>
		/// <returns>The subkey requested, or <see langword="null" /> if the operation failed.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="name" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.ObjectDisposedException">The <see cref="T:Microsoft.Win32.RegistryKey" /> is closed (closed keys cannot be accessed). </exception>
		/// <exception cref="T:System.Security.SecurityException">The user does not have the permissions required to access the registry key in the specified mode. </exception>
		// Token: 0x06000054 RID: 84 RVA: 0x000028E4 File Offset: 0x00000AE4
		[ComVisible(false)]
		[MonoLimitation("rights are ignored in Mono")]
		public RegistryKey OpenSubKey(string name, RegistryRights rights)
		{
			return this.OpenSubKey(name);
		}

		/// <summary>Retrieves the specified subkey for read or read/write access, requesting the specified access rights.</summary>
		/// <param name="name">The name or path of the subkey to create or open.</param>
		/// <param name="permissionCheck">One of the enumeration values that specifies whether the key is opened for read or read/write access.</param>
		/// <param name="rights">A bitwise combination of enumeration values that specifies the desired security access.</param>
		/// <returns>The subkey requested, or <see langword="null" /> if the operation failed.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="name" /> is <see langword="null" /></exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="permissionCheck" /> contains an invalid value.</exception>
		/// <exception cref="T:System.ObjectDisposedException">The <see cref="T:Microsoft.Win32.RegistryKey" /> is closed (closed keys cannot be accessed). </exception>
		/// <exception cref="T:System.Security.SecurityException">
		///         <paramref name="rights" /> includes invalid registry rights values.-or-The user does not have the requested permissions. </exception>
		// Token: 0x06000055 RID: 85 RVA: 0x000028D7 File Offset: 0x00000AD7
		[MonoLimitation("rights are ignored in Mono")]
		[ComVisible(false)]
		public RegistryKey OpenSubKey(string name, RegistryKeyPermissionCheck permissionCheck, RegistryRights rights)
		{
			return this.OpenSubKey(name, permissionCheck == RegistryKeyPermissionCheck.ReadWriteSubTree);
		}

		/// <summary>Applies Windows access control security to an existing registry key.</summary>
		/// <param name="registrySecurity">The access control security to apply to the current subkey. </param>
		/// <exception cref="T:System.UnauthorizedAccessException">The current <see cref="T:Microsoft.Win32.RegistryKey" /> object represents a key with access control security, and the caller does not have <see cref="F:System.Security.AccessControl.RegistryRights.ChangePermissions" /> rights.</exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="registrySecurity" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ObjectDisposedException">The <see cref="T:Microsoft.Win32.RegistryKey" /> being manipulated is closed (closed keys cannot be accessed).</exception>
		// Token: 0x06000056 RID: 86 RVA: 0x000028ED File Offset: 0x00000AED
		public void SetAccessControl(RegistrySecurity registrySecurity)
		{
			if (registrySecurity == null)
			{
				throw new ArgumentNullException("registrySecurity");
			}
			registrySecurity.PersistModifications(this.Name);
		}

		/// <summary>Retrieves a string representation of this key.</summary>
		/// <returns>A string representing the key. If the specified key is invalid (cannot be found) then <see langword="null" /> is returned.</returns>
		/// <exception cref="T:System.ObjectDisposedException">The <see cref="T:Microsoft.Win32.RegistryKey" /> being accessed is closed (closed keys cannot be accessed). </exception>
		// Token: 0x06000057 RID: 87 RVA: 0x00002909 File Offset: 0x00000B09
		public override string ToString()
		{
			this.AssertKeyStillValid();
			return RegistryKey.RegistryApi.ToString(this);
		}

		// Token: 0x17000006 RID: 6
		// (get) Token: 0x06000058 RID: 88 RVA: 0x0000291C File Offset: 0x00000B1C
		internal bool IsRoot
		{
			get
			{
				return this.hive != null;
			}
		}

		// Token: 0x17000007 RID: 7
		// (get) Token: 0x06000059 RID: 89 RVA: 0x00002927 File Offset: 0x00000B27
		private bool IsWritable
		{
			get
			{
				return this.isWritable;
			}
		}

		// Token: 0x17000008 RID: 8
		// (get) Token: 0x0600005A RID: 90 RVA: 0x0000292F File Offset: 0x00000B2F
		internal RegistryHive Hive
		{
			get
			{
				if (!this.IsRoot)
				{
					throw new NotSupportedException();
				}
				return (RegistryHive)this.hive;
			}
		}

		// Token: 0x17000009 RID: 9
		// (get) Token: 0x0600005B RID: 91 RVA: 0x0000294A File Offset: 0x00000B4A
		internal object InternalHandle
		{
			get
			{
				return this.handle;
			}
		}

		// Token: 0x0600005C RID: 92 RVA: 0x00002952 File Offset: 0x00000B52
		private void AssertKeyStillValid()
		{
			if (this.handle == null)
			{
				throw new ObjectDisposedException("Microsoft.Win32.RegistryKey");
			}
		}

		// Token: 0x0600005D RID: 93 RVA: 0x00002967 File Offset: 0x00000B67
		private void AssertKeyNameNotNull(string subKeyName)
		{
			if (subKeyName == null)
			{
				throw new ArgumentNullException("name");
			}
		}

		// Token: 0x0600005E RID: 94 RVA: 0x00002977 File Offset: 0x00000B77
		private void AssertKeyNameLength(string name)
		{
			if (name.Length > 255)
			{
				throw new ArgumentException("Name of registry key cannot be greater than 255 characters");
			}
		}

		// Token: 0x0600005F RID: 95 RVA: 0x00002994 File Offset: 0x00000B94
		private void DeleteChildKeysAndValues()
		{
			if (this.IsRoot)
			{
				return;
			}
			foreach (string text in this.GetSubKeyNames())
			{
				RegistryKey registryKey = this.OpenSubKey(text, true);
				registryKey.DeleteChildKeysAndValues();
				registryKey.Close();
				this.DeleteSubKey(text, false);
			}
			foreach (string name in this.GetValueNames())
			{
				this.DeleteValue(name, false);
			}
		}

		// Token: 0x06000060 RID: 96 RVA: 0x00002A00 File Offset: 0x00000C00
		internal static string DecodeString(byte[] data)
		{
			string text = Encoding.Unicode.GetString(data);
			if (text.IndexOf('\0') != -1)
			{
				text = text.TrimEnd(new char[1]);
			}
			return text;
		}

		// Token: 0x06000061 RID: 97 RVA: 0x00002A31 File Offset: 0x00000C31
		internal static IOException CreateMarkedForDeletionException()
		{
			throw new IOException("Illegal operation attempted on a registry key that has been marked for deletion.");
		}

		// Token: 0x06000062 RID: 98 RVA: 0x00002A40 File Offset: 0x00000C40
		private static string GetHiveName(RegistryHive hive)
		{
			switch (hive)
			{
			case RegistryHive.ClassesRoot:
				return "HKEY_CLASSES_ROOT";
			case RegistryHive.CurrentUser:
				return "HKEY_CURRENT_USER";
			case RegistryHive.LocalMachine:
				return "HKEY_LOCAL_MACHINE";
			case RegistryHive.Users:
				return "HKEY_USERS";
			case RegistryHive.PerformanceData:
				return "HKEY_PERFORMANCE_DATA";
			case RegistryHive.CurrentConfig:
				return "HKEY_CURRENT_CONFIG";
			case RegistryHive.DynData:
				return "HKEY_DYN_DATA";
			default:
				throw new NotImplementedException(string.Format("Registry hive '{0}' is not implemented.", hive.ToString()));
			}
		}

		// Token: 0x06000063 RID: 99 RVA: 0x00002ABD File Offset: 0x00000CBD
		internal RegistryKey()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x0400037E RID: 894
		private object handle;

		// Token: 0x0400037F RID: 895
		private SafeRegistryHandle safe_handle;

		// Token: 0x04000380 RID: 896
		private object hive;

		// Token: 0x04000381 RID: 897
		private readonly string qname;

		// Token: 0x04000382 RID: 898
		private readonly bool isRemoteRoot;

		// Token: 0x04000383 RID: 899
		private readonly bool isWritable;

		// Token: 0x04000384 RID: 900
		private static readonly IRegistryApi RegistryApi;
	}
}
