﻿using System;
using System.Collections.Generic;

namespace Mono.Globalization.Unicode
{
	// Token: 0x02000049 RID: 73
	internal class ContractionComparer : IComparer<Contraction>
	{
		// Token: 0x060001C6 RID: 454 RVA: 0x00007C74 File Offset: 0x00005E74
		public int Compare(Contraction c1, Contraction c2)
		{
			char[] source = c1.Source;
			char[] source2 = c2.Source;
			int num = (source.Length > source2.Length) ? source2.Length : source.Length;
			for (int i = 0; i < num; i++)
			{
				if (source[i] != source2[i])
				{
					return (int)(source[i] - source2[i]);
				}
			}
			if (source.Length != source2.Length)
			{
				return source.Length - source2.Length;
			}
			return c1.Index - c2.Index;
		}

		// Token: 0x060001C7 RID: 455 RVA: 0x00002050 File Offset: 0x00000250
		public ContractionComparer()
		{
		}

		// Token: 0x060001C8 RID: 456 RVA: 0x00007CD9 File Offset: 0x00005ED9
		// Note: this type is marked as 'beforefieldinit'.
		static ContractionComparer()
		{
		}

		// Token: 0x04000455 RID: 1109
		public static readonly ContractionComparer Instance = new ContractionComparer();
	}
}
