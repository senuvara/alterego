﻿using System;

namespace Mono.Globalization.Unicode
{
	// Token: 0x02000047 RID: 71
	internal class TailoringInfo
	{
		// Token: 0x060001C4 RID: 452 RVA: 0x00007C28 File Offset: 0x00005E28
		public TailoringInfo(int lcid, int tailoringIndex, int tailoringCount, bool frenchSort)
		{
			this.LCID = lcid;
			this.TailoringIndex = tailoringIndex;
			this.TailoringCount = tailoringCount;
			this.FrenchSort = frenchSort;
		}

		// Token: 0x0400044D RID: 1101
		public readonly int LCID;

		// Token: 0x0400044E RID: 1102
		public readonly int TailoringIndex;

		// Token: 0x0400044F RID: 1103
		public readonly int TailoringCount;

		// Token: 0x04000450 RID: 1104
		public readonly bool FrenchSort;
	}
}
