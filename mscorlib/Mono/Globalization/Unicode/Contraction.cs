﻿using System;

namespace Mono.Globalization.Unicode
{
	// Token: 0x02000048 RID: 72
	internal class Contraction
	{
		// Token: 0x060001C5 RID: 453 RVA: 0x00007C4D File Offset: 0x00005E4D
		public Contraction(int index, char[] source, string replacement, byte[] sortkey)
		{
			this.Index = index;
			this.Source = source;
			this.Replacement = replacement;
			this.SortKey = sortkey;
		}

		// Token: 0x04000451 RID: 1105
		public int Index;

		// Token: 0x04000452 RID: 1106
		public readonly char[] Source;

		// Token: 0x04000453 RID: 1107
		public readonly string Replacement;

		// Token: 0x04000454 RID: 1108
		public readonly byte[] SortKey;
	}
}
