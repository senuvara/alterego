﻿using System;

namespace Mono.Globalization.Unicode
{
	// Token: 0x0200004A RID: 74
	internal class Level2Map
	{
		// Token: 0x060001C9 RID: 457 RVA: 0x00007CE5 File Offset: 0x00005EE5
		public Level2Map(byte source, byte replace)
		{
			this.Source = source;
			this.Replace = replace;
		}

		// Token: 0x04000456 RID: 1110
		public byte Source;

		// Token: 0x04000457 RID: 1111
		public byte Replace;
	}
}
