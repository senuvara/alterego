﻿using System;

namespace Mono.Globalization.Unicode
{
	// Token: 0x02000045 RID: 69
	internal class CodePointIndexer
	{
		// Token: 0x060001BF RID: 447 RVA: 0x000079B0 File Offset: 0x00005BB0
		public static Array CompressArray(Array source, Type type, CodePointIndexer indexer)
		{
			int num = 0;
			for (int i = 0; i < indexer.ranges.Length; i++)
			{
				num += indexer.ranges[i].Count;
			}
			Array array = Array.CreateInstance(type, num);
			for (int j = 0; j < indexer.ranges.Length; j++)
			{
				Array.Copy(source, indexer.ranges[j].Start, array, indexer.ranges[j].IndexStart, indexer.ranges[j].Count);
			}
			return array;
		}

		// Token: 0x060001C0 RID: 448 RVA: 0x00007A3C File Offset: 0x00005C3C
		public CodePointIndexer(int[] starts, int[] ends, int defaultIndex, int defaultCP)
		{
			this.defaultIndex = defaultIndex;
			this.defaultCP = defaultCP;
			this.ranges = new CodePointIndexer.TableRange[starts.Length];
			for (int i = 0; i < this.ranges.Length; i++)
			{
				this.ranges[i] = new CodePointIndexer.TableRange(starts[i], ends[i], (i == 0) ? 0 : (this.ranges[i - 1].IndexStart + this.ranges[i - 1].Count));
			}
			for (int j = 0; j < this.ranges.Length; j++)
			{
				this.TotalCount += this.ranges[j].Count;
			}
		}

		// Token: 0x060001C1 RID: 449 RVA: 0x00007AF4 File Offset: 0x00005CF4
		public int ToIndex(int cp)
		{
			for (int i = 0; i < this.ranges.Length; i++)
			{
				if (cp < this.ranges[i].Start)
				{
					return this.defaultIndex;
				}
				if (cp < this.ranges[i].End)
				{
					return cp - this.ranges[i].Start + this.ranges[i].IndexStart;
				}
			}
			return this.defaultIndex;
		}

		// Token: 0x060001C2 RID: 450 RVA: 0x00007B70 File Offset: 0x00005D70
		public int ToCodePoint(int i)
		{
			for (int j = 0; j < this.ranges.Length; j++)
			{
				if (i < this.ranges[j].IndexStart)
				{
					return this.defaultCP;
				}
				if (i < this.ranges[j].IndexEnd)
				{
					return i - this.ranges[j].IndexStart + this.ranges[j].Start;
				}
			}
			return this.defaultCP;
		}

		// Token: 0x04000444 RID: 1092
		private readonly CodePointIndexer.TableRange[] ranges;

		// Token: 0x04000445 RID: 1093
		public readonly int TotalCount;

		// Token: 0x04000446 RID: 1094
		private int defaultIndex;

		// Token: 0x04000447 RID: 1095
		private int defaultCP;

		// Token: 0x02000046 RID: 70
		[Serializable]
		internal struct TableRange
		{
			// Token: 0x060001C3 RID: 451 RVA: 0x00007BEB File Offset: 0x00005DEB
			public TableRange(int start, int end, int indexStart)
			{
				this.Start = start;
				this.End = end;
				this.Count = this.End - this.Start;
				this.IndexStart = indexStart;
				this.IndexEnd = this.IndexStart + this.Count;
			}

			// Token: 0x04000448 RID: 1096
			public readonly int Start;

			// Token: 0x04000449 RID: 1097
			public readonly int End;

			// Token: 0x0400044A RID: 1098
			public readonly int Count;

			// Token: 0x0400044B RID: 1099
			public readonly int IndexStart;

			// Token: 0x0400044C RID: 1100
			public readonly int IndexEnd;
		}
	}
}
