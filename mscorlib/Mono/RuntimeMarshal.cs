﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;

namespace Mono
{
	// Token: 0x02000032 RID: 50
	internal static class RuntimeMarshal
	{
		// Token: 0x0600016C RID: 364 RVA: 0x00006B0C File Offset: 0x00004D0C
		internal unsafe static string PtrToUtf8String(IntPtr ptr)
		{
			if (ptr == IntPtr.Zero)
			{
				return string.Empty;
			}
			byte* ptr2 = (byte*)((void*)ptr);
			int num = 0;
			try
			{
				while (*(ptr2++) != 0)
				{
					num++;
				}
			}
			catch (NullReferenceException)
			{
				throw new ArgumentOutOfRangeException("ptr", "Value does not refer to a valid string.");
			}
			return new string((sbyte*)((void*)ptr), 0, num, Encoding.UTF8);
		}

		// Token: 0x0600016D RID: 365 RVA: 0x00006B78 File Offset: 0x00004D78
		internal static SafeStringMarshal MarshalString(string str)
		{
			return new SafeStringMarshal(str);
		}

		// Token: 0x0600016E RID: 366 RVA: 0x00006B80 File Offset: 0x00004D80
		private unsafe static int DecodeBlobSize(IntPtr in_ptr, out IntPtr out_ptr)
		{
			byte* ptr = (byte*)((void*)in_ptr);
			uint result;
			if ((*ptr & 128) == 0)
			{
				result = (uint)(*ptr & 127);
				ptr++;
			}
			else if ((*ptr & 64) == 0)
			{
				result = (uint)(((int)(*ptr & 63) << 8) + (int)ptr[1]);
				ptr += 2;
			}
			else
			{
				result = (uint)(((int)(*ptr & 31) << 24) + ((int)ptr[1] << 16) + ((int)ptr[2] << 8) + (int)ptr[3]);
				ptr += 4;
			}
			out_ptr = (IntPtr)((void*)ptr);
			return (int)result;
		}

		// Token: 0x0600016F RID: 367 RVA: 0x00006BF0 File Offset: 0x00004DF0
		internal static byte[] DecodeBlobArray(IntPtr ptr)
		{
			IntPtr source;
			int num = RuntimeMarshal.DecodeBlobSize(ptr, out source);
			byte[] array = new byte[num];
			Marshal.Copy(source, array, 0, num);
			return array;
		}

		// Token: 0x06000170 RID: 368 RVA: 0x00006C17 File Offset: 0x00004E17
		internal static int AsciHexDigitValue(int c)
		{
			if (c >= 48 && c <= 57)
			{
				return c - 48;
			}
			if (c >= 97 && c <= 102)
			{
				return c - 97 + 10;
			}
			return c - 65 + 10;
		}

		// Token: 0x06000171 RID: 369
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern void FreeAssemblyName(ref MonoAssemblyName name, bool freeStruct);
	}
}
