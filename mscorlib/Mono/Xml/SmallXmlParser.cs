﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;

namespace Mono.Xml
{
	// Token: 0x02000040 RID: 64
	internal class SmallXmlParser
	{
		// Token: 0x0600018D RID: 397 RVA: 0x00006DD8 File Offset: 0x00004FD8
		public SmallXmlParser()
		{
		}

		// Token: 0x0600018E RID: 398 RVA: 0x00006E30 File Offset: 0x00005030
		private Exception Error(string msg)
		{
			return new SmallXmlParserException(msg, this.line, this.column);
		}

		// Token: 0x0600018F RID: 399 RVA: 0x00006E44 File Offset: 0x00005044
		private Exception UnexpectedEndError()
		{
			string[] array = new string[this.elementNames.Count];
			this.elementNames.CopyTo(array, 0);
			return this.Error(string.Format("Unexpected end of stream. Element stack content is {0}", string.Join(",", array)));
		}

		// Token: 0x06000190 RID: 400 RVA: 0x00006E8C File Offset: 0x0000508C
		private bool IsNameChar(char c, bool start)
		{
			if (c <= '.')
			{
				if (c == '-' || c == '.')
				{
					return !start;
				}
			}
			else if (c == ':' || c == '_')
			{
				return true;
			}
			if (c > 'Ā')
			{
				if (c == 'ՙ' || c == 'ۥ' || c == 'ۦ')
				{
					return true;
				}
				if ('ʻ' <= c && c <= 'ˁ')
				{
					return true;
				}
			}
			switch (char.GetUnicodeCategory(c))
			{
			case UnicodeCategory.UppercaseLetter:
			case UnicodeCategory.LowercaseLetter:
			case UnicodeCategory.TitlecaseLetter:
			case UnicodeCategory.OtherLetter:
			case UnicodeCategory.LetterNumber:
				return true;
			case UnicodeCategory.ModifierLetter:
			case UnicodeCategory.NonSpacingMark:
			case UnicodeCategory.SpacingCombiningMark:
			case UnicodeCategory.EnclosingMark:
			case UnicodeCategory.DecimalDigitNumber:
				return !start;
			default:
				return false;
			}
		}

		// Token: 0x06000191 RID: 401 RVA: 0x00006F2E File Offset: 0x0000512E
		private bool IsWhitespace(int c)
		{
			return c - 9 <= 1 || c == 13 || c == 32;
		}

		// Token: 0x06000192 RID: 402 RVA: 0x00006F44 File Offset: 0x00005144
		public void SkipWhitespaces()
		{
			this.SkipWhitespaces(false);
		}

		// Token: 0x06000193 RID: 403 RVA: 0x00006F4D File Offset: 0x0000514D
		private void HandleWhitespaces()
		{
			while (this.IsWhitespace(this.Peek()))
			{
				this.buffer.Append((char)this.Read());
			}
			if (this.Peek() != 60 && this.Peek() >= 0)
			{
				this.isWhitespace = false;
			}
		}

		// Token: 0x06000194 RID: 404 RVA: 0x00006F8C File Offset: 0x0000518C
		public void SkipWhitespaces(bool expected)
		{
			for (;;)
			{
				int num = this.Peek();
				if (num - 9 > 1 && num != 13 && num != 32)
				{
					break;
				}
				this.Read();
				if (expected)
				{
					expected = false;
				}
			}
			if (expected)
			{
				throw this.Error("Whitespace is expected.");
			}
		}

		// Token: 0x06000195 RID: 405 RVA: 0x00006FCF File Offset: 0x000051CF
		private int Peek()
		{
			return this.reader.Peek();
		}

		// Token: 0x06000196 RID: 406 RVA: 0x00006FDC File Offset: 0x000051DC
		private int Read()
		{
			int num = this.reader.Read();
			if (num == 10)
			{
				this.resetColumn = true;
			}
			if (this.resetColumn)
			{
				this.line++;
				this.resetColumn = false;
				this.column = 1;
				return num;
			}
			this.column++;
			return num;
		}

		// Token: 0x06000197 RID: 407 RVA: 0x00007034 File Offset: 0x00005234
		public void Expect(int c)
		{
			int num = this.Read();
			if (num < 0)
			{
				throw this.UnexpectedEndError();
			}
			if (num != c)
			{
				throw this.Error(string.Format("Expected '{0}' but got {1}", (char)c, (char)num));
			}
		}

		// Token: 0x06000198 RID: 408 RVA: 0x00007078 File Offset: 0x00005278
		private string ReadUntil(char until, bool handleReferences)
		{
			while (this.Peek() >= 0)
			{
				char c = (char)this.Read();
				if (c == until)
				{
					string result = this.buffer.ToString();
					this.buffer.Length = 0;
					return result;
				}
				if (handleReferences && c == '&')
				{
					this.ReadReference();
				}
				else
				{
					this.buffer.Append(c);
				}
			}
			throw this.UnexpectedEndError();
		}

		// Token: 0x06000199 RID: 409 RVA: 0x000070D8 File Offset: 0x000052D8
		public string ReadName()
		{
			int num = 0;
			if (this.Peek() < 0 || !this.IsNameChar((char)this.Peek(), true))
			{
				throw this.Error("XML name start character is expected.");
			}
			for (int i = this.Peek(); i >= 0; i = this.Peek())
			{
				char c = (char)i;
				if (!this.IsNameChar(c, false))
				{
					break;
				}
				if (num == this.nameBuffer.Length)
				{
					char[] destinationArray = new char[num * 2];
					Array.Copy(this.nameBuffer, destinationArray, num);
					this.nameBuffer = destinationArray;
				}
				this.nameBuffer[num++] = c;
				this.Read();
			}
			if (num == 0)
			{
				throw this.Error("Valid XML name is expected.");
			}
			return new string(this.nameBuffer, 0, num);
		}

		// Token: 0x0600019A RID: 410 RVA: 0x00007188 File Offset: 0x00005388
		public void Parse(TextReader input, SmallXmlParser.IContentHandler handler)
		{
			this.reader = input;
			this.handler = handler;
			handler.OnStartParsing(this);
			while (this.Peek() >= 0)
			{
				this.ReadContent();
			}
			this.HandleBufferedContent();
			if (this.elementNames.Count > 0)
			{
				throw this.Error(string.Format("Insufficient close tag: {0}", this.elementNames.Peek()));
			}
			handler.OnEndParsing(this);
			this.Cleanup();
		}

		// Token: 0x0600019B RID: 411 RVA: 0x000071F8 File Offset: 0x000053F8
		private void Cleanup()
		{
			this.line = 1;
			this.column = 0;
			this.handler = null;
			this.reader = null;
			this.elementNames.Clear();
			this.xmlSpaces.Clear();
			this.attributes.Clear();
			this.buffer.Length = 0;
			this.xmlSpace = null;
			this.isWhitespace = false;
		}

		// Token: 0x0600019C RID: 412 RVA: 0x0000725C File Offset: 0x0000545C
		public void ReadContent()
		{
			if (this.IsWhitespace(this.Peek()))
			{
				if (this.buffer.Length == 0)
				{
					this.isWhitespace = true;
				}
				this.HandleWhitespaces();
			}
			if (this.Peek() != 60)
			{
				this.ReadCharacters();
				return;
			}
			this.Read();
			int num = this.Peek();
			if (num != 33)
			{
				if (num != 47)
				{
					string text;
					if (num != 63)
					{
						this.HandleBufferedContent();
						text = this.ReadName();
						while (this.Peek() != 62 && this.Peek() != 47)
						{
							this.ReadAttribute(this.attributes);
						}
						this.handler.OnStartElement(text, this.attributes);
						this.attributes.Clear();
						this.SkipWhitespaces();
						if (this.Peek() == 47)
						{
							this.Read();
							this.handler.OnEndElement(text);
						}
						else
						{
							this.elementNames.Push(text);
							this.xmlSpaces.Push(this.xmlSpace);
						}
						this.Expect(62);
						return;
					}
					this.HandleBufferedContent();
					this.Read();
					text = this.ReadName();
					this.SkipWhitespaces();
					string text2 = string.Empty;
					if (this.Peek() != 63)
					{
						for (;;)
						{
							text2 += this.ReadUntil('?', false);
							if (this.Peek() == 62)
							{
								break;
							}
							text2 += "?";
						}
					}
					this.handler.OnProcessingInstruction(text, text2);
					this.Expect(62);
					return;
				}
				else
				{
					this.HandleBufferedContent();
					if (this.elementNames.Count == 0)
					{
						throw this.UnexpectedEndError();
					}
					this.Read();
					string text = this.ReadName();
					this.SkipWhitespaces();
					string text3 = (string)this.elementNames.Pop();
					this.xmlSpaces.Pop();
					if (this.xmlSpaces.Count > 0)
					{
						this.xmlSpace = (string)this.xmlSpaces.Peek();
					}
					else
					{
						this.xmlSpace = null;
					}
					if (text != text3)
					{
						throw this.Error(string.Format("End tag mismatch: expected {0} but found {1}", text3, text));
					}
					this.handler.OnEndElement(text);
					this.Expect(62);
					return;
				}
			}
			else
			{
				this.Read();
				if (this.Peek() == 91)
				{
					this.Read();
					if (this.ReadName() != "CDATA")
					{
						throw this.Error("Invalid declaration markup");
					}
					this.Expect(91);
					this.ReadCDATASection();
					return;
				}
				else
				{
					if (this.Peek() == 45)
					{
						this.ReadComment();
						return;
					}
					if (this.ReadName() != "DOCTYPE")
					{
						throw this.Error("Invalid declaration markup.");
					}
					throw this.Error("This parser does not support document type.");
				}
			}
		}

		// Token: 0x0600019D RID: 413 RVA: 0x000074F4 File Offset: 0x000056F4
		private void HandleBufferedContent()
		{
			if (this.buffer.Length == 0)
			{
				return;
			}
			if (this.isWhitespace)
			{
				this.handler.OnIgnorableWhitespace(this.buffer.ToString());
			}
			else
			{
				this.handler.OnChars(this.buffer.ToString());
			}
			this.buffer.Length = 0;
			this.isWhitespace = false;
		}

		// Token: 0x0600019E RID: 414 RVA: 0x00007558 File Offset: 0x00005758
		private void ReadCharacters()
		{
			this.isWhitespace = false;
			for (;;)
			{
				int num = this.Peek();
				if (num == -1)
				{
					break;
				}
				if (num != 38)
				{
					if (num == 60)
					{
						return;
					}
					this.buffer.Append((char)this.Read());
				}
				else
				{
					this.Read();
					this.ReadReference();
				}
			}
		}

		// Token: 0x0600019F RID: 415 RVA: 0x000075A8 File Offset: 0x000057A8
		private void ReadReference()
		{
			if (this.Peek() == 35)
			{
				this.Read();
				this.ReadCharacterReference();
				return;
			}
			string a = this.ReadName();
			this.Expect(59);
			if (a == "amp")
			{
				this.buffer.Append('&');
				return;
			}
			if (a == "quot")
			{
				this.buffer.Append('"');
				return;
			}
			if (a == "apos")
			{
				this.buffer.Append('\'');
				return;
			}
			if (a == "lt")
			{
				this.buffer.Append('<');
				return;
			}
			if (!(a == "gt"))
			{
				throw this.Error("General non-predefined entity reference is not supported in this parser.");
			}
			this.buffer.Append('>');
		}

		// Token: 0x060001A0 RID: 416 RVA: 0x00007678 File Offset: 0x00005878
		private int ReadCharacterReference()
		{
			int num = 0;
			if (this.Peek() == 120)
			{
				this.Read();
				for (int i = this.Peek(); i >= 0; i = this.Peek())
				{
					if (48 <= i && i <= 57)
					{
						num <<= 4 + i - 48;
					}
					else if (65 <= i && i <= 70)
					{
						num <<= 4 + i - 65 + 10;
					}
					else
					{
						if (97 > i || i > 102)
						{
							break;
						}
						num <<= 4 + i - 97 + 10;
					}
					this.Read();
				}
			}
			else
			{
				int num2 = this.Peek();
				while (num2 >= 0 && 48 <= num2 && num2 <= 57)
				{
					num <<= 4 + num2 - 48;
					this.Read();
					num2 = this.Peek();
				}
			}
			return num;
		}

		// Token: 0x060001A1 RID: 417 RVA: 0x00007734 File Offset: 0x00005934
		private void ReadAttribute(SmallXmlParser.AttrListImpl a)
		{
			this.SkipWhitespaces(true);
			if (this.Peek() == 47 || this.Peek() == 62)
			{
				return;
			}
			string text = this.ReadName();
			this.SkipWhitespaces();
			this.Expect(61);
			this.SkipWhitespaces();
			int num = this.Read();
			string value;
			if (num != 34)
			{
				if (num != 39)
				{
					throw this.Error("Invalid attribute value markup.");
				}
				value = this.ReadUntil('\'', true);
			}
			else
			{
				value = this.ReadUntil('"', true);
			}
			if (text == "xml:space")
			{
				this.xmlSpace = value;
			}
			a.Add(text, value);
		}

		// Token: 0x060001A2 RID: 418 RVA: 0x000077CC File Offset: 0x000059CC
		private void ReadCDATASection()
		{
			int num = 0;
			while (this.Peek() >= 0)
			{
				char c = (char)this.Read();
				if (c == ']')
				{
					num++;
				}
				else
				{
					if (c == '>' && num > 1)
					{
						for (int i = num; i > 2; i--)
						{
							this.buffer.Append(']');
						}
						return;
					}
					for (int j = 0; j < num; j++)
					{
						this.buffer.Append(']');
					}
					num = 0;
					this.buffer.Append(c);
				}
			}
			throw this.UnexpectedEndError();
		}

		// Token: 0x060001A3 RID: 419 RVA: 0x0000784C File Offset: 0x00005A4C
		private void ReadComment()
		{
			this.Expect(45);
			this.Expect(45);
			while (this.Read() != 45 || this.Read() != 45)
			{
			}
			if (this.Read() != 62)
			{
				throw this.Error("'--' is not allowed inside comment markup.");
			}
		}

		// Token: 0x04000434 RID: 1076
		private SmallXmlParser.IContentHandler handler;

		// Token: 0x04000435 RID: 1077
		private TextReader reader;

		// Token: 0x04000436 RID: 1078
		private Stack elementNames = new Stack();

		// Token: 0x04000437 RID: 1079
		private Stack xmlSpaces = new Stack();

		// Token: 0x04000438 RID: 1080
		private string xmlSpace;

		// Token: 0x04000439 RID: 1081
		private StringBuilder buffer = new StringBuilder(200);

		// Token: 0x0400043A RID: 1082
		private char[] nameBuffer = new char[30];

		// Token: 0x0400043B RID: 1083
		private bool isWhitespace;

		// Token: 0x0400043C RID: 1084
		private SmallXmlParser.AttrListImpl attributes = new SmallXmlParser.AttrListImpl();

		// Token: 0x0400043D RID: 1085
		private int line = 1;

		// Token: 0x0400043E RID: 1086
		private int column;

		// Token: 0x0400043F RID: 1087
		private bool resetColumn;

		// Token: 0x02000041 RID: 65
		public interface IContentHandler
		{
			// Token: 0x060001A4 RID: 420
			void OnStartParsing(SmallXmlParser parser);

			// Token: 0x060001A5 RID: 421
			void OnEndParsing(SmallXmlParser parser);

			// Token: 0x060001A6 RID: 422
			void OnStartElement(string name, SmallXmlParser.IAttrList attrs);

			// Token: 0x060001A7 RID: 423
			void OnEndElement(string name);

			// Token: 0x060001A8 RID: 424
			void OnProcessingInstruction(string name, string text);

			// Token: 0x060001A9 RID: 425
			void OnChars(string text);

			// Token: 0x060001AA RID: 426
			void OnIgnorableWhitespace(string text);
		}

		// Token: 0x02000042 RID: 66
		public interface IAttrList
		{
			// Token: 0x17000022 RID: 34
			// (get) Token: 0x060001AB RID: 427
			int Length { get; }

			// Token: 0x17000023 RID: 35
			// (get) Token: 0x060001AC RID: 428
			bool IsEmpty { get; }

			// Token: 0x060001AD RID: 429
			string GetName(int i);

			// Token: 0x060001AE RID: 430
			string GetValue(int i);

			// Token: 0x060001AF RID: 431
			string GetValue(string name);

			// Token: 0x17000024 RID: 36
			// (get) Token: 0x060001B0 RID: 432
			string[] Names { get; }

			// Token: 0x17000025 RID: 37
			// (get) Token: 0x060001B1 RID: 433
			string[] Values { get; }
		}

		// Token: 0x02000043 RID: 67
		private class AttrListImpl : SmallXmlParser.IAttrList
		{
			// Token: 0x17000026 RID: 38
			// (get) Token: 0x060001B2 RID: 434 RVA: 0x00007888 File Offset: 0x00005A88
			public int Length
			{
				get
				{
					return this.attrNames.Count;
				}
			}

			// Token: 0x17000027 RID: 39
			// (get) Token: 0x060001B3 RID: 435 RVA: 0x00007895 File Offset: 0x00005A95
			public bool IsEmpty
			{
				get
				{
					return this.attrNames.Count == 0;
				}
			}

			// Token: 0x060001B4 RID: 436 RVA: 0x000078A5 File Offset: 0x00005AA5
			public string GetName(int i)
			{
				return this.attrNames[i];
			}

			// Token: 0x060001B5 RID: 437 RVA: 0x000078B3 File Offset: 0x00005AB3
			public string GetValue(int i)
			{
				return this.attrValues[i];
			}

			// Token: 0x060001B6 RID: 438 RVA: 0x000078C4 File Offset: 0x00005AC4
			public string GetValue(string name)
			{
				for (int i = 0; i < this.attrNames.Count; i++)
				{
					if (this.attrNames[i] == name)
					{
						return this.attrValues[i];
					}
				}
				return null;
			}

			// Token: 0x17000028 RID: 40
			// (get) Token: 0x060001B7 RID: 439 RVA: 0x00007909 File Offset: 0x00005B09
			public string[] Names
			{
				get
				{
					return this.attrNames.ToArray();
				}
			}

			// Token: 0x17000029 RID: 41
			// (get) Token: 0x060001B8 RID: 440 RVA: 0x00007916 File Offset: 0x00005B16
			public string[] Values
			{
				get
				{
					return this.attrValues.ToArray();
				}
			}

			// Token: 0x060001B9 RID: 441 RVA: 0x00007923 File Offset: 0x00005B23
			internal void Clear()
			{
				this.attrNames.Clear();
				this.attrValues.Clear();
			}

			// Token: 0x060001BA RID: 442 RVA: 0x0000793B File Offset: 0x00005B3B
			internal void Add(string name, string value)
			{
				this.attrNames.Add(name);
				this.attrValues.Add(value);
			}

			// Token: 0x060001BB RID: 443 RVA: 0x00007955 File Offset: 0x00005B55
			public AttrListImpl()
			{
			}

			// Token: 0x04000440 RID: 1088
			private List<string> attrNames = new List<string>();

			// Token: 0x04000441 RID: 1089
			private List<string> attrValues = new List<string>();
		}
	}
}
