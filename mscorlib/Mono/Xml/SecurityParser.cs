﻿using System;
using System.Collections;
using System.IO;
using System.Security;

namespace Mono.Xml
{
	// Token: 0x0200003E RID: 62
	internal class SecurityParser : SmallXmlParser, SmallXmlParser.IContentHandler
	{
		// Token: 0x0600017B RID: 379 RVA: 0x00006CE8 File Offset: 0x00004EE8
		public SecurityParser()
		{
			this.stack = new Stack();
		}

		// Token: 0x0600017C RID: 380 RVA: 0x00006CFB File Offset: 0x00004EFB
		public void LoadXml(string xml)
		{
			this.root = null;
			this.stack.Clear();
			base.Parse(new StringReader(xml), this);
		}

		// Token: 0x0600017D RID: 381 RVA: 0x00006D1C File Offset: 0x00004F1C
		public SecurityElement ToXml()
		{
			return this.root;
		}

		// Token: 0x0600017E RID: 382 RVA: 0x000020D3 File Offset: 0x000002D3
		public void OnStartParsing(SmallXmlParser parser)
		{
		}

		// Token: 0x0600017F RID: 383 RVA: 0x000020D3 File Offset: 0x000002D3
		public void OnProcessingInstruction(string name, string text)
		{
		}

		// Token: 0x06000180 RID: 384 RVA: 0x000020D3 File Offset: 0x000002D3
		public void OnIgnorableWhitespace(string s)
		{
		}

		// Token: 0x06000181 RID: 385 RVA: 0x00006D24 File Offset: 0x00004F24
		public void OnStartElement(string name, SmallXmlParser.IAttrList attrs)
		{
			SecurityElement securityElement = new SecurityElement(name);
			if (this.root == null)
			{
				this.root = securityElement;
				this.current = securityElement;
			}
			else
			{
				((SecurityElement)this.stack.Peek()).AddChild(securityElement);
			}
			this.stack.Push(securityElement);
			this.current = securityElement;
			int length = attrs.Length;
			for (int i = 0; i < length; i++)
			{
				this.current.AddAttribute(attrs.GetName(i), SecurityElement.Escape(attrs.GetValue(i)));
			}
		}

		// Token: 0x06000182 RID: 386 RVA: 0x00006DAA File Offset: 0x00004FAA
		public void OnEndElement(string name)
		{
			this.current = (SecurityElement)this.stack.Pop();
		}

		// Token: 0x06000183 RID: 387 RVA: 0x00006DC2 File Offset: 0x00004FC2
		public void OnChars(string ch)
		{
			this.current.Text = SecurityElement.Escape(ch);
		}

		// Token: 0x06000184 RID: 388 RVA: 0x000020D3 File Offset: 0x000002D3
		public void OnEndParsing(SmallXmlParser parser)
		{
		}

		// Token: 0x04000431 RID: 1073
		private SecurityElement root;

		// Token: 0x04000432 RID: 1074
		private SecurityElement current;

		// Token: 0x04000433 RID: 1075
		private Stack stack;
	}
}
