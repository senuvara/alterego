﻿using System;

namespace Mono.Xml
{
	// Token: 0x02000044 RID: 68
	internal class SmallXmlParserException : SystemException
	{
		// Token: 0x060001BC RID: 444 RVA: 0x00007973 File Offset: 0x00005B73
		public SmallXmlParserException(string msg, int line, int column) : base(string.Format("{0}. At ({1},{2})", msg, line, column))
		{
			this.line = line;
			this.column = column;
		}

		// Token: 0x1700002A RID: 42
		// (get) Token: 0x060001BD RID: 445 RVA: 0x000079A0 File Offset: 0x00005BA0
		public int Line
		{
			get
			{
				return this.line;
			}
		}

		// Token: 0x1700002B RID: 43
		// (get) Token: 0x060001BE RID: 446 RVA: 0x000079A8 File Offset: 0x00005BA8
		public int Column
		{
			get
			{
				return this.column;
			}
		}

		// Token: 0x04000442 RID: 1090
		private int line;

		// Token: 0x04000443 RID: 1091
		private int column;
	}
}
