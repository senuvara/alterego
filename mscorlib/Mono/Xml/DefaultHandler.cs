﻿using System;

namespace Mono.Xml
{
	// Token: 0x0200003F RID: 63
	internal class DefaultHandler : SmallXmlParser.IContentHandler
	{
		// Token: 0x06000185 RID: 389 RVA: 0x000020D3 File Offset: 0x000002D3
		public void OnStartParsing(SmallXmlParser parser)
		{
		}

		// Token: 0x06000186 RID: 390 RVA: 0x000020D3 File Offset: 0x000002D3
		public void OnEndParsing(SmallXmlParser parser)
		{
		}

		// Token: 0x06000187 RID: 391 RVA: 0x000020D3 File Offset: 0x000002D3
		public void OnStartElement(string name, SmallXmlParser.IAttrList attrs)
		{
		}

		// Token: 0x06000188 RID: 392 RVA: 0x000020D3 File Offset: 0x000002D3
		public void OnEndElement(string name)
		{
		}

		// Token: 0x06000189 RID: 393 RVA: 0x000020D3 File Offset: 0x000002D3
		public void OnChars(string s)
		{
		}

		// Token: 0x0600018A RID: 394 RVA: 0x000020D3 File Offset: 0x000002D3
		public void OnIgnorableWhitespace(string s)
		{
		}

		// Token: 0x0600018B RID: 395 RVA: 0x000020D3 File Offset: 0x000002D3
		public void OnProcessingInstruction(string name, string text)
		{
		}

		// Token: 0x0600018C RID: 396 RVA: 0x00002050 File Offset: 0x00000250
		public DefaultHandler()
		{
		}
	}
}
