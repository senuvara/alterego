﻿using System;

namespace Mono.Math.Prime
{
	// Token: 0x020000B2 RID: 178
	// (Invoke) Token: 0x06000609 RID: 1545
	internal delegate bool PrimalityTest(BigInteger bi, ConfidenceFactor confidence);
}
