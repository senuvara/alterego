﻿using System;

namespace Mono.Math.Prime
{
	// Token: 0x020000B1 RID: 177
	internal enum ConfidenceFactor
	{
		// Token: 0x04000656 RID: 1622
		ExtraLow,
		// Token: 0x04000657 RID: 1623
		Low,
		// Token: 0x04000658 RID: 1624
		Medium,
		// Token: 0x04000659 RID: 1625
		High,
		// Token: 0x0400065A RID: 1626
		ExtraHigh,
		// Token: 0x0400065B RID: 1627
		Provable
	}
}
