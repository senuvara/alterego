﻿using System;

namespace Mono.Math.Prime.Generator
{
	// Token: 0x020000B4 RID: 180
	internal class NextPrimeFinder : SequentialSearchPrimeGeneratorBase
	{
		// Token: 0x06000611 RID: 1553 RVA: 0x00021947 File Offset: 0x0001FB47
		protected override BigInteger GenerateSearchBase(int bits, object Context)
		{
			if (Context == null)
			{
				throw new ArgumentNullException("Context");
			}
			BigInteger bigInteger = new BigInteger((BigInteger)Context);
			bigInteger.SetBit(0U);
			return bigInteger;
		}

		// Token: 0x06000612 RID: 1554 RVA: 0x00021969 File Offset: 0x0001FB69
		public NextPrimeFinder()
		{
		}
	}
}
