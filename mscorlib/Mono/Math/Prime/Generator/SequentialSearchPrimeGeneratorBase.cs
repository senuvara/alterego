﻿using System;

namespace Mono.Math.Prime.Generator
{
	// Token: 0x020000B6 RID: 182
	internal class SequentialSearchPrimeGeneratorBase : PrimeGeneratorBase
	{
		// Token: 0x06000619 RID: 1561 RVA: 0x0002199D File Offset: 0x0001FB9D
		protected virtual BigInteger GenerateSearchBase(int bits, object context)
		{
			BigInteger bigInteger = BigInteger.GenerateRandom(bits);
			bigInteger.SetBit(0U);
			return bigInteger;
		}

		// Token: 0x0600061A RID: 1562 RVA: 0x000219AC File Offset: 0x0001FBAC
		public override BigInteger GenerateNewPrime(int bits)
		{
			return this.GenerateNewPrime(bits, null);
		}

		// Token: 0x0600061B RID: 1563 RVA: 0x000219B8 File Offset: 0x0001FBB8
		public virtual BigInteger GenerateNewPrime(int bits, object context)
		{
			BigInteger bigInteger = this.GenerateSearchBase(bits, context);
			uint num = bigInteger % 3234846615U;
			int trialDivisionBounds = this.TrialDivisionBounds;
			uint[] smallPrimes = BigInteger.smallPrimes;
			for (;;)
			{
				if (num % 3U != 0U && num % 5U != 0U && num % 7U != 0U && num % 11U != 0U && num % 13U != 0U && num % 17U != 0U && num % 19U != 0U && num % 23U != 0U && num % 29U != 0U)
				{
					int num2 = 10;
					while (num2 < smallPrimes.Length && (ulong)smallPrimes[num2] <= (ulong)((long)trialDivisionBounds))
					{
						if (bigInteger % smallPrimes[num2] == 0U)
						{
							goto IL_9D;
						}
						num2++;
					}
					if (this.IsPrimeAcceptable(bigInteger, context) && this.PrimalityTest(bigInteger, this.Confidence))
					{
						break;
					}
				}
				IL_9D:
				num += 2U;
				if (num >= 3234846615U)
				{
					num -= 3234846615U;
				}
				bigInteger.Incr2();
			}
			return bigInteger;
		}

		// Token: 0x0600061C RID: 1564 RVA: 0x00004E08 File Offset: 0x00003008
		protected virtual bool IsPrimeAcceptable(BigInteger bi, object context)
		{
			return true;
		}

		// Token: 0x0600061D RID: 1565 RVA: 0x00021A80 File Offset: 0x0001FC80
		public SequentialSearchPrimeGeneratorBase()
		{
		}
	}
}
