﻿using System;

namespace Mono.Math.Prime.Generator
{
	// Token: 0x020000B5 RID: 181
	internal abstract class PrimeGeneratorBase
	{
		// Token: 0x1700010D RID: 269
		// (get) Token: 0x06000613 RID: 1555 RVA: 0x00021971 File Offset: 0x0001FB71
		public virtual ConfidenceFactor Confidence
		{
			get
			{
				return ConfidenceFactor.Medium;
			}
		}

		// Token: 0x1700010E RID: 270
		// (get) Token: 0x06000614 RID: 1556 RVA: 0x00021974 File Offset: 0x0001FB74
		public virtual PrimalityTest PrimalityTest
		{
			get
			{
				return new PrimalityTest(PrimalityTests.RabinMillerTest);
			}
		}

		// Token: 0x1700010F RID: 271
		// (get) Token: 0x06000615 RID: 1557 RVA: 0x00021982 File Offset: 0x0001FB82
		public virtual int TrialDivisionBounds
		{
			get
			{
				return 4000;
			}
		}

		// Token: 0x06000616 RID: 1558 RVA: 0x00021989 File Offset: 0x0001FB89
		protected bool PostTrialDivisionTests(BigInteger bi)
		{
			return this.PrimalityTest(bi, this.Confidence);
		}

		// Token: 0x06000617 RID: 1559
		public abstract BigInteger GenerateNewPrime(int bits);

		// Token: 0x06000618 RID: 1560 RVA: 0x00002050 File Offset: 0x00000250
		protected PrimeGeneratorBase()
		{
		}
	}
}
