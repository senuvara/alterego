﻿using System;
using System.Runtime.CompilerServices;

namespace Mono
{
	// Token: 0x0200002B RID: 43
	public static class Runtime
	{
		// Token: 0x0600013C RID: 316
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void mono_runtime_install_handlers();

		// Token: 0x0600013D RID: 317 RVA: 0x00006756 File Offset: 0x00004956
		public static void InstallSignalHandlers()
		{
			Runtime.mono_runtime_install_handlers();
		}

		// Token: 0x0600013E RID: 318
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void mono_runtime_cleanup_handlers();

		// Token: 0x0600013F RID: 319 RVA: 0x0000675D File Offset: 0x0000495D
		public static void RemoveSignalHandlers()
		{
			Runtime.mono_runtime_cleanup_handlers();
		}

		// Token: 0x06000140 RID: 320
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern string GetDisplayName();

		// Token: 0x06000141 RID: 321
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern string GetNativeStackTrace(Exception exception);

		// Token: 0x06000142 RID: 322 RVA: 0x00004E08 File Offset: 0x00003008
		public static bool SetGCAllowSynchronousMajor(bool flag)
		{
			return true;
		}
	}
}
