﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace Mono
{
	// Token: 0x0200003A RID: 58
	internal struct MonoAssemblyName
	{
		// Token: 0x0400041F RID: 1055
		private const int MONO_PUBLIC_KEY_TOKEN_LENGTH = 17;

		// Token: 0x04000420 RID: 1056
		internal IntPtr name;

		// Token: 0x04000421 RID: 1057
		internal IntPtr culture;

		// Token: 0x04000422 RID: 1058
		internal IntPtr hash_value;

		// Token: 0x04000423 RID: 1059
		internal IntPtr public_key;

		// Token: 0x04000424 RID: 1060
		[FixedBuffer(typeof(byte), 17)]
		internal MonoAssemblyName.<public_key_token>e__FixedBuffer public_key_token;

		// Token: 0x04000425 RID: 1061
		internal uint hash_alg;

		// Token: 0x04000426 RID: 1062
		internal uint hash_len;

		// Token: 0x04000427 RID: 1063
		internal uint flags;

		// Token: 0x04000428 RID: 1064
		internal ushort major;

		// Token: 0x04000429 RID: 1065
		internal ushort minor;

		// Token: 0x0400042A RID: 1066
		internal ushort build;

		// Token: 0x0400042B RID: 1067
		internal ushort revision;

		// Token: 0x0400042C RID: 1068
		internal ushort arch;

		// Token: 0x0200003B RID: 59
		[CompilerGenerated]
		[UnsafeValueType]
		[StructLayout(LayoutKind.Sequential, Size = 17)]
		public struct <public_key_token>e__FixedBuffer
		{
			// Token: 0x0400042D RID: 1069
			public byte FixedElementField;
		}
	}
}
