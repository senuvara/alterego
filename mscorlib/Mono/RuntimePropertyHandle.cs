﻿using System;

namespace Mono
{
	// Token: 0x02000030 RID: 48
	internal struct RuntimePropertyHandle
	{
		// Token: 0x0600015E RID: 350 RVA: 0x00006A08 File Offset: 0x00004C08
		internal RuntimePropertyHandle(IntPtr v)
		{
			this.value = v;
		}

		// Token: 0x1700001C RID: 28
		// (get) Token: 0x0600015F RID: 351 RVA: 0x00006A11 File Offset: 0x00004C11
		public IntPtr Value
		{
			get
			{
				return this.value;
			}
		}

		// Token: 0x06000160 RID: 352 RVA: 0x00006A1C File Offset: 0x00004C1C
		public override bool Equals(object obj)
		{
			return obj != null && !(base.GetType() != obj.GetType()) && this.value == ((RuntimePropertyHandle)obj).Value;
		}

		// Token: 0x06000161 RID: 353 RVA: 0x00006A64 File Offset: 0x00004C64
		public bool Equals(RuntimePropertyHandle handle)
		{
			return this.value == handle.Value;
		}

		// Token: 0x06000162 RID: 354 RVA: 0x00006A78 File Offset: 0x00004C78
		public override int GetHashCode()
		{
			return this.value.GetHashCode();
		}

		// Token: 0x06000163 RID: 355 RVA: 0x00006A85 File Offset: 0x00004C85
		public static bool operator ==(RuntimePropertyHandle left, RuntimePropertyHandle right)
		{
			return left.Equals(right);
		}

		// Token: 0x06000164 RID: 356 RVA: 0x00006A8F File Offset: 0x00004C8F
		public static bool operator !=(RuntimePropertyHandle left, RuntimePropertyHandle right)
		{
			return !left.Equals(right);
		}

		// Token: 0x040003FC RID: 1020
		private IntPtr value;
	}
}
