﻿using System;

namespace Mono
{
	// Token: 0x02000033 RID: 51
	internal static class RuntimeStructs
	{
		// Token: 0x02000034 RID: 52
		internal struct RemoteClass
		{
			// Token: 0x040003FE RID: 1022
			internal IntPtr default_vtable;

			// Token: 0x040003FF RID: 1023
			internal IntPtr xdomain_vtable;

			// Token: 0x04000400 RID: 1024
			internal unsafe RuntimeStructs.MonoClass* proxy_class;

			// Token: 0x04000401 RID: 1025
			internal IntPtr proxy_class_name;

			// Token: 0x04000402 RID: 1026
			internal uint interface_count;
		}

		// Token: 0x02000035 RID: 53
		internal struct MonoClass
		{
		}

		// Token: 0x02000036 RID: 54
		internal struct GenericParamInfo
		{
			// Token: 0x04000403 RID: 1027
			internal unsafe RuntimeStructs.MonoClass* pklass;

			// Token: 0x04000404 RID: 1028
			internal IntPtr name;

			// Token: 0x04000405 RID: 1029
			internal ushort flags;

			// Token: 0x04000406 RID: 1030
			internal uint token;

			// Token: 0x04000407 RID: 1031
			internal unsafe RuntimeStructs.MonoClass** constraints;
		}

		// Token: 0x02000037 RID: 55
		internal struct GPtrArray
		{
			// Token: 0x04000408 RID: 1032
			internal unsafe IntPtr* data;

			// Token: 0x04000409 RID: 1033
			internal int len;
		}

		// Token: 0x02000038 RID: 56
		private struct HandleStackMark
		{
			// Token: 0x0400040A RID: 1034
			private int size;

			// Token: 0x0400040B RID: 1035
			private int interior_size;

			// Token: 0x0400040C RID: 1036
			private IntPtr chunk;
		}

		// Token: 0x02000039 RID: 57
		private struct MonoError
		{
			// Token: 0x0400040D RID: 1037
			private ushort error_code;

			// Token: 0x0400040E RID: 1038
			private ushort hidden_0;

			// Token: 0x0400040F RID: 1039
			private IntPtr hidden_1;

			// Token: 0x04000410 RID: 1040
			private IntPtr hidden_2;

			// Token: 0x04000411 RID: 1041
			private IntPtr hidden_3;

			// Token: 0x04000412 RID: 1042
			private IntPtr hidden_4;

			// Token: 0x04000413 RID: 1043
			private IntPtr hidden_5;

			// Token: 0x04000414 RID: 1044
			private IntPtr hidden_6;

			// Token: 0x04000415 RID: 1045
			private IntPtr hidden_7;

			// Token: 0x04000416 RID: 1046
			private IntPtr hidden_8;

			// Token: 0x04000417 RID: 1047
			private IntPtr hidden_11;

			// Token: 0x04000418 RID: 1048
			private IntPtr hidden_12;

			// Token: 0x04000419 RID: 1049
			private IntPtr hidden_13;

			// Token: 0x0400041A RID: 1050
			private IntPtr hidden_14;

			// Token: 0x0400041B RID: 1051
			private IntPtr hidden_15;

			// Token: 0x0400041C RID: 1052
			private IntPtr hidden_16;

			// Token: 0x0400041D RID: 1053
			private IntPtr hidden_17;

			// Token: 0x0400041E RID: 1054
			private IntPtr hidden_18;
		}
	}
}
