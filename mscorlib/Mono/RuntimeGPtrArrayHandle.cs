﻿using System;
using System.Runtime.CompilerServices;

namespace Mono
{
	// Token: 0x02000031 RID: 49
	internal struct RuntimeGPtrArrayHandle
	{
		// Token: 0x06000165 RID: 357 RVA: 0x00006A9C File Offset: 0x00004C9C
		internal unsafe RuntimeGPtrArrayHandle(RuntimeStructs.GPtrArray* value)
		{
			this.value = value;
		}

		// Token: 0x06000166 RID: 358 RVA: 0x00006AA5 File Offset: 0x00004CA5
		internal unsafe RuntimeGPtrArrayHandle(IntPtr ptr)
		{
			this.value = (RuntimeStructs.GPtrArray*)((void*)ptr);
		}

		// Token: 0x1700001D RID: 29
		// (get) Token: 0x06000167 RID: 359 RVA: 0x00006AB3 File Offset: 0x00004CB3
		internal unsafe int Length
		{
			get
			{
				return this.value->len;
			}
		}

		// Token: 0x1700001E RID: 30
		internal IntPtr this[int i]
		{
			get
			{
				return this.Lookup(i);
			}
		}

		// Token: 0x06000169 RID: 361 RVA: 0x00006AC9 File Offset: 0x00004CC9
		internal unsafe IntPtr Lookup(int i)
		{
			if (i >= 0 && i < this.Length)
			{
				return this.value->data[i];
			}
			throw new IndexOutOfRangeException();
		}

		// Token: 0x0600016A RID: 362
		[MethodImpl(MethodImplOptions.InternalCall)]
		private unsafe static extern void GPtrArrayFree(RuntimeStructs.GPtrArray* value);

		// Token: 0x0600016B RID: 363 RVA: 0x00006AF4 File Offset: 0x00004CF4
		internal static void DestroyAndFree(ref RuntimeGPtrArrayHandle h)
		{
			RuntimeGPtrArrayHandle.GPtrArrayFree(h.value);
			h.value = null;
		}

		// Token: 0x040003FD RID: 1021
		private unsafe RuntimeStructs.GPtrArray* value;
	}
}
