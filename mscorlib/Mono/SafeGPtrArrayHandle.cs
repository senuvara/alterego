﻿using System;

namespace Mono
{
	// Token: 0x0200003C RID: 60
	internal struct SafeGPtrArrayHandle : IDisposable
	{
		// Token: 0x06000172 RID: 370 RVA: 0x00006C41 File Offset: 0x00004E41
		internal SafeGPtrArrayHandle(IntPtr ptr)
		{
			this.handle = new RuntimeGPtrArrayHandle(ptr);
		}

		// Token: 0x06000173 RID: 371 RVA: 0x00006C4F File Offset: 0x00004E4F
		public void Dispose()
		{
			RuntimeGPtrArrayHandle.DestroyAndFree(ref this.handle);
		}

		// Token: 0x1700001F RID: 31
		// (get) Token: 0x06000174 RID: 372 RVA: 0x00006C5C File Offset: 0x00004E5C
		internal int Length
		{
			get
			{
				return this.handle.Length;
			}
		}

		// Token: 0x17000020 RID: 32
		internal IntPtr this[int i]
		{
			get
			{
				return this.handle[i];
			}
		}

		// Token: 0x0400042E RID: 1070
		private RuntimeGPtrArrayHandle handle;
	}
}
