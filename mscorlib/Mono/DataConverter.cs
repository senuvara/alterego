﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Mono
{
	// Token: 0x02000027 RID: 39
	internal abstract class DataConverter
	{
		// Token: 0x060000F3 RID: 243
		public abstract double GetDouble(byte[] data, int index);

		// Token: 0x060000F4 RID: 244
		public abstract float GetFloat(byte[] data, int index);

		// Token: 0x060000F5 RID: 245
		public abstract long GetInt64(byte[] data, int index);

		// Token: 0x060000F6 RID: 246
		public abstract int GetInt32(byte[] data, int index);

		// Token: 0x060000F7 RID: 247
		public abstract short GetInt16(byte[] data, int index);

		// Token: 0x060000F8 RID: 248
		[CLSCompliant(false)]
		public abstract uint GetUInt32(byte[] data, int index);

		// Token: 0x060000F9 RID: 249
		[CLSCompliant(false)]
		public abstract ushort GetUInt16(byte[] data, int index);

		// Token: 0x060000FA RID: 250
		[CLSCompliant(false)]
		public abstract ulong GetUInt64(byte[] data, int index);

		// Token: 0x060000FB RID: 251
		public abstract void PutBytes(byte[] dest, int destIdx, double value);

		// Token: 0x060000FC RID: 252
		public abstract void PutBytes(byte[] dest, int destIdx, float value);

		// Token: 0x060000FD RID: 253
		public abstract void PutBytes(byte[] dest, int destIdx, int value);

		// Token: 0x060000FE RID: 254
		public abstract void PutBytes(byte[] dest, int destIdx, long value);

		// Token: 0x060000FF RID: 255
		public abstract void PutBytes(byte[] dest, int destIdx, short value);

		// Token: 0x06000100 RID: 256
		[CLSCompliant(false)]
		public abstract void PutBytes(byte[] dest, int destIdx, ushort value);

		// Token: 0x06000101 RID: 257
		[CLSCompliant(false)]
		public abstract void PutBytes(byte[] dest, int destIdx, uint value);

		// Token: 0x06000102 RID: 258
		[CLSCompliant(false)]
		public abstract void PutBytes(byte[] dest, int destIdx, ulong value);

		// Token: 0x06000103 RID: 259 RVA: 0x00004F14 File Offset: 0x00003114
		public byte[] GetBytes(double value)
		{
			byte[] array = new byte[8];
			this.PutBytes(array, 0, value);
			return array;
		}

		// Token: 0x06000104 RID: 260 RVA: 0x00004F34 File Offset: 0x00003134
		public byte[] GetBytes(float value)
		{
			byte[] array = new byte[4];
			this.PutBytes(array, 0, value);
			return array;
		}

		// Token: 0x06000105 RID: 261 RVA: 0x00004F54 File Offset: 0x00003154
		public byte[] GetBytes(int value)
		{
			byte[] array = new byte[4];
			this.PutBytes(array, 0, value);
			return array;
		}

		// Token: 0x06000106 RID: 262 RVA: 0x00004F74 File Offset: 0x00003174
		public byte[] GetBytes(long value)
		{
			byte[] array = new byte[8];
			this.PutBytes(array, 0, value);
			return array;
		}

		// Token: 0x06000107 RID: 263 RVA: 0x00004F94 File Offset: 0x00003194
		public byte[] GetBytes(short value)
		{
			byte[] array = new byte[2];
			this.PutBytes(array, 0, value);
			return array;
		}

		// Token: 0x06000108 RID: 264 RVA: 0x00004FB4 File Offset: 0x000031B4
		[CLSCompliant(false)]
		public byte[] GetBytes(ushort value)
		{
			byte[] array = new byte[2];
			this.PutBytes(array, 0, value);
			return array;
		}

		// Token: 0x06000109 RID: 265 RVA: 0x00004FD4 File Offset: 0x000031D4
		[CLSCompliant(false)]
		public byte[] GetBytes(uint value)
		{
			byte[] array = new byte[4];
			this.PutBytes(array, 0, value);
			return array;
		}

		// Token: 0x0600010A RID: 266 RVA: 0x00004FF4 File Offset: 0x000031F4
		[CLSCompliant(false)]
		public byte[] GetBytes(ulong value)
		{
			byte[] array = new byte[8];
			this.PutBytes(array, 0, value);
			return array;
		}

		// Token: 0x17000014 RID: 20
		// (get) Token: 0x0600010B RID: 267 RVA: 0x00005012 File Offset: 0x00003212
		public static DataConverter LittleEndian
		{
			get
			{
				if (!BitConverter.IsLittleEndian)
				{
					return DataConverter.SwapConv;
				}
				return DataConverter.CopyConv;
			}
		}

		// Token: 0x17000015 RID: 21
		// (get) Token: 0x0600010C RID: 268 RVA: 0x00005026 File Offset: 0x00003226
		public static DataConverter BigEndian
		{
			get
			{
				if (!BitConverter.IsLittleEndian)
				{
					return DataConverter.CopyConv;
				}
				return DataConverter.SwapConv;
			}
		}

		// Token: 0x17000016 RID: 22
		// (get) Token: 0x0600010D RID: 269 RVA: 0x0000503A File Offset: 0x0000323A
		public static DataConverter Native
		{
			get
			{
				return DataConverter.CopyConv;
			}
		}

		// Token: 0x0600010E RID: 270 RVA: 0x00005041 File Offset: 0x00003241
		private static int Align(int current, int align)
		{
			return (current + align - 1) / align * align;
		}

		// Token: 0x0600010F RID: 271 RVA: 0x0000504C File Offset: 0x0000324C
		public static byte[] Pack(string description, params object[] args)
		{
			int num = 0;
			DataConverter.PackContext packContext = new DataConverter.PackContext();
			packContext.conv = DataConverter.CopyConv;
			packContext.description = description;
			packContext.i = 0;
			while (packContext.i < description.Length)
			{
				object oarg;
				if (num < args.Length)
				{
					oarg = args[num];
				}
				else
				{
					if (packContext.repeat != 0)
					{
						break;
					}
					oarg = null;
				}
				int i = packContext.i;
				if (DataConverter.PackOne(packContext, oarg))
				{
					num++;
					if (packContext.repeat > 0)
					{
						DataConverter.PackContext packContext2 = packContext;
						int num2 = packContext2.repeat - 1;
						packContext2.repeat = num2;
						if (num2 > 0)
						{
							packContext.i = i;
						}
						else
						{
							packContext.i++;
						}
					}
					else
					{
						packContext.i++;
					}
				}
				else
				{
					packContext.i++;
				}
			}
			return packContext.Get();
		}

		// Token: 0x06000110 RID: 272 RVA: 0x00005118 File Offset: 0x00003318
		public static byte[] PackEnumerable(string description, IEnumerable args)
		{
			DataConverter.PackContext packContext = new DataConverter.PackContext();
			packContext.conv = DataConverter.CopyConv;
			packContext.description = description;
			IEnumerator enumerator = args.GetEnumerator();
			bool flag = enumerator.MoveNext();
			packContext.i = 0;
			while (packContext.i < description.Length)
			{
				object oarg;
				if (flag)
				{
					oarg = enumerator.Current;
				}
				else
				{
					if (packContext.repeat != 0)
					{
						break;
					}
					oarg = null;
				}
				int i = packContext.i;
				if (DataConverter.PackOne(packContext, oarg))
				{
					flag = enumerator.MoveNext();
					if (packContext.repeat > 0)
					{
						DataConverter.PackContext packContext2 = packContext;
						int num = packContext2.repeat - 1;
						packContext2.repeat = num;
						if (num > 0)
						{
							packContext.i = i;
						}
						else
						{
							packContext.i++;
						}
					}
					else
					{
						packContext.i++;
					}
				}
				else
				{
					packContext.i++;
				}
			}
			return packContext.Get();
		}

		// Token: 0x06000111 RID: 273 RVA: 0x000051F8 File Offset: 0x000033F8
		private static bool PackOne(DataConverter.PackContext b, object oarg)
		{
			char c = b.description[b.i];
			if (c <= 'S')
			{
				if (c <= 'C')
				{
					switch (c)
					{
					case '!':
						b.align = -1;
						return false;
					case '"':
					case '#':
					case '&':
					case '\'':
					case '(':
					case ')':
					case '+':
					case ',':
					case '-':
					case '.':
					case '/':
					case '0':
						goto IL_44B;
					case '$':
						break;
					case '%':
						b.conv = DataConverter.Native;
						return false;
					case '*':
						b.repeat = int.MaxValue;
						return false;
					case '1':
					case '2':
					case '3':
					case '4':
					case '5':
					case '6':
					case '7':
					case '8':
					case '9':
						b.repeat = (int)((short)b.description[b.i] - 48);
						return false;
					default:
						if (c != 'C')
						{
							goto IL_44B;
						}
						b.Add(new byte[]
						{
							Convert.ToByte(oarg)
						});
						return true;
					}
				}
				else
				{
					if (c == 'I')
					{
						b.Add(b.conv.GetBytes(Convert.ToUInt32(oarg)));
						return true;
					}
					if (c == 'L')
					{
						b.Add(b.conv.GetBytes(Convert.ToUInt64(oarg)));
						return true;
					}
					if (c != 'S')
					{
						goto IL_44B;
					}
					b.Add(b.conv.GetBytes(Convert.ToUInt16(oarg)));
					return true;
				}
			}
			else if (c <= 'l')
			{
				switch (c)
				{
				case '[':
				{
					int num = -1;
					int num2 = b.i + 1;
					while (num2 < b.description.Length && b.description[num2] != ']')
					{
						int num3 = (int)((short)b.description[num2] - 48);
						if (num3 >= 0 && num3 <= 9)
						{
							if (num == -1)
							{
								num = num3;
							}
							else
							{
								num = num * 10 + num3;
							}
						}
						num2++;
					}
					if (num == -1)
					{
						throw new ArgumentException("invalid size specification");
					}
					b.i = num2;
					b.repeat = num;
					return false;
				}
				case '\\':
				case ']':
				case '`':
				case 'a':
				case 'e':
				case 'g':
				case 'h':
					goto IL_44B;
				case '^':
					b.conv = DataConverter.BigEndian;
					return false;
				case '_':
					b.conv = DataConverter.LittleEndian;
					return false;
				case 'b':
					b.Add(new byte[]
					{
						Convert.ToByte(oarg)
					});
					return true;
				case 'c':
					b.Add(new byte[]
					{
						(byte)Convert.ToSByte(oarg)
					});
					return true;
				case 'd':
					b.Add(b.conv.GetBytes(Convert.ToDouble(oarg)));
					return true;
				case 'f':
					b.Add(b.conv.GetBytes(Convert.ToSingle(oarg)));
					return true;
				case 'i':
					b.Add(b.conv.GetBytes(Convert.ToInt32(oarg)));
					return true;
				default:
					if (c != 'l')
					{
						goto IL_44B;
					}
					b.Add(b.conv.GetBytes(Convert.ToInt64(oarg)));
					return true;
				}
			}
			else
			{
				if (c == 's')
				{
					b.Add(b.conv.GetBytes(Convert.ToInt16(oarg)));
					return true;
				}
				if (c == 'x')
				{
					b.Add(new byte[1]);
					return false;
				}
				if (c != 'z')
				{
					goto IL_44B;
				}
			}
			bool flag = b.description[b.i] == 'z';
			b.i++;
			if (b.i >= b.description.Length)
			{
				throw new ArgumentException("$ description needs a type specified", "description");
			}
			char c2 = b.description[b.i];
			Encoding encoding;
			switch (c2)
			{
			case '3':
			{
				encoding = Encoding.GetEncoding(12000);
				int num3 = 4;
				goto IL_416;
			}
			case '4':
			{
				encoding = Encoding.GetEncoding(12001);
				int num3 = 4;
				goto IL_416;
			}
			case '5':
				break;
			case '6':
			{
				encoding = Encoding.Unicode;
				int num3 = 2;
				goto IL_416;
			}
			case '7':
			{
				encoding = Encoding.UTF7;
				int num3 = 1;
				goto IL_416;
			}
			case '8':
			{
				encoding = Encoding.UTF8;
				int num3 = 1;
				goto IL_416;
			}
			default:
				if (c2 == 'b')
				{
					encoding = Encoding.BigEndianUnicode;
					int num3 = 2;
					goto IL_416;
				}
				break;
			}
			throw new ArgumentException("Invalid format for $ specifier", "description");
			IL_416:
			if (b.align == -1)
			{
				b.align = 4;
			}
			b.Add(encoding.GetBytes(Convert.ToString(oarg)));
			if (flag)
			{
				int num3;
				b.Add(new byte[num3]);
				return true;
			}
			return true;
			IL_44B:
			throw new ArgumentException(string.Format("invalid format specified `{0}'", b.description[b.i]));
		}

		// Token: 0x06000112 RID: 274 RVA: 0x00005677 File Offset: 0x00003877
		private static bool Prepare(byte[] buffer, ref int idx, int size, ref bool align)
		{
			if (align)
			{
				idx = DataConverter.Align(idx, size);
				align = false;
			}
			if (idx + size > buffer.Length)
			{
				idx = buffer.Length;
				return false;
			}
			return true;
		}

		// Token: 0x06000113 RID: 275 RVA: 0x0000569C File Offset: 0x0000389C
		public static IList Unpack(string description, byte[] buffer, int startIndex)
		{
			DataConverter dataConverter = DataConverter.CopyConv;
			List<object> list = new List<object>();
			int num = startIndex;
			bool flag = false;
			int num2 = 0;
			int num3 = 0;
			while (num3 < description.Length && num < buffer.Length)
			{
				int num4 = num3;
				char c = description[num3];
				if (c <= 'S')
				{
					if (c <= 'C')
					{
						switch (c)
						{
						case '!':
							flag = true;
							break;
						case '"':
						case '#':
						case '&':
						case '\'':
						case '(':
						case ')':
						case '+':
						case ',':
						case '-':
						case '.':
						case '/':
						case '0':
							goto IL_5C2;
						case '$':
							goto IL_3E0;
						case '%':
							dataConverter = DataConverter.Native;
							break;
						case '*':
							num2 = int.MaxValue;
							break;
						case '1':
						case '2':
						case '3':
						case '4':
						case '5':
						case '6':
						case '7':
						case '8':
						case '9':
							num2 = (int)((short)description[num3] - 48);
							num4 = num3 + 1;
							break;
						default:
							if (c != 'C')
							{
								goto IL_5C2;
							}
							goto IL_303;
						}
					}
					else if (c != 'I')
					{
						if (c != 'L')
						{
							if (c != 'S')
							{
								goto IL_5C2;
							}
							if (DataConverter.Prepare(buffer, ref num, 2, ref flag))
							{
								list.Add(dataConverter.GetUInt16(buffer, num));
								num += 2;
							}
						}
						else if (DataConverter.Prepare(buffer, ref num, 8, ref flag))
						{
							list.Add(dataConverter.GetUInt64(buffer, num));
							num += 8;
						}
					}
					else if (DataConverter.Prepare(buffer, ref num, 4, ref flag))
					{
						list.Add(dataConverter.GetUInt32(buffer, num));
						num += 4;
					}
				}
				else if (c <= 'l')
				{
					switch (c)
					{
					case '[':
					{
						int num5 = -1;
						int num6 = num3 + 1;
						while (num6 < description.Length && description[num6] != ']')
						{
							int num7 = (int)((short)description[num6] - 48);
							if (num7 >= 0 && num7 <= 9)
							{
								if (num5 == -1)
								{
									num5 = num7;
								}
								else
								{
									num5 = num5 * 10 + num7;
								}
							}
							num6++;
						}
						if (num5 == -1)
						{
							throw new ArgumentException("invalid size specification");
						}
						num3 = num6;
						num4 = num3 + 1;
						num2 = num5;
						break;
					}
					case '\\':
					case ']':
					case '`':
					case 'a':
					case 'e':
					case 'g':
					case 'h':
						goto IL_5C2;
					case '^':
						dataConverter = DataConverter.BigEndian;
						break;
					case '_':
						dataConverter = DataConverter.LittleEndian;
						break;
					case 'b':
						if (DataConverter.Prepare(buffer, ref num, 1, ref flag))
						{
							list.Add(buffer[num]);
							num++;
						}
						break;
					case 'c':
						goto IL_303;
					case 'd':
						if (DataConverter.Prepare(buffer, ref num, 8, ref flag))
						{
							list.Add(dataConverter.GetDouble(buffer, num));
							num += 8;
						}
						break;
					case 'f':
						if (DataConverter.Prepare(buffer, ref num, 4, ref flag))
						{
							list.Add(dataConverter.GetFloat(buffer, num));
							num += 4;
						}
						break;
					case 'i':
						if (DataConverter.Prepare(buffer, ref num, 4, ref flag))
						{
							list.Add(dataConverter.GetInt32(buffer, num));
							num += 4;
						}
						break;
					default:
						if (c != 'l')
						{
							goto IL_5C2;
						}
						if (DataConverter.Prepare(buffer, ref num, 8, ref flag))
						{
							list.Add(dataConverter.GetInt64(buffer, num));
							num += 8;
						}
						break;
					}
				}
				else if (c != 's')
				{
					if (c != 'x')
					{
						if (c != 'z')
						{
							goto IL_5C2;
						}
						goto IL_3E0;
					}
					else
					{
						num++;
					}
				}
				else if (DataConverter.Prepare(buffer, ref num, 2, ref flag))
				{
					list.Add(dataConverter.GetInt16(buffer, num));
					num += 2;
				}
				IL_5DF:
				if (num2 <= 0)
				{
					num3++;
					continue;
				}
				if (--num2 > 0)
				{
					num3 = num4;
					continue;
				}
				continue;
				IL_303:
				if (DataConverter.Prepare(buffer, ref num, 1, ref flag))
				{
					char c2;
					if (description[num3] == 'c')
					{
						c2 = (char)((sbyte)buffer[num]);
					}
					else
					{
						c2 = (char)buffer[num];
					}
					list.Add(c2);
					num++;
					goto IL_5DF;
				}
				goto IL_5DF;
				IL_3E0:
				num3++;
				if (num3 >= description.Length)
				{
					throw new ArgumentException("$ description needs a type specified", "description");
				}
				char c3 = description[num3];
				if (flag)
				{
					num = DataConverter.Align(num, 4);
					flag = false;
				}
				if (num < buffer.Length)
				{
					int num7;
					Encoding encoding;
					switch (c3)
					{
					case '3':
						encoding = Encoding.GetEncoding(12000);
						num7 = 4;
						break;
					case '4':
						encoding = Encoding.GetEncoding(12001);
						num7 = 4;
						break;
					case '5':
						goto IL_49C;
					case '6':
						encoding = Encoding.Unicode;
						num7 = 2;
						break;
					case '7':
						encoding = Encoding.UTF7;
						num7 = 1;
						break;
					case '8':
						encoding = Encoding.UTF8;
						num7 = 1;
						break;
					default:
						if (c3 != 'b')
						{
							goto IL_49C;
						}
						encoding = Encoding.BigEndianUnicode;
						num7 = 2;
						break;
					}
					int i = num;
					switch (num7)
					{
					case 1:
						while (i < buffer.Length && buffer[i] != 0)
						{
							i++;
						}
						list.Add(encoding.GetChars(buffer, num, i - num));
						if (i == buffer.Length)
						{
							num = i;
							goto IL_5DF;
						}
						num = i + 1;
						goto IL_5DF;
					case 2:
						while (i < buffer.Length)
						{
							if (i + 1 == buffer.Length)
							{
								i++;
								break;
							}
							if (buffer[i] == 0 && buffer[i + 1] == 0)
							{
								break;
							}
							i++;
						}
						list.Add(encoding.GetChars(buffer, num, i - num));
						if (i == buffer.Length)
						{
							num = i;
							goto IL_5DF;
						}
						num = i + 2;
						goto IL_5DF;
					case 3:
						goto IL_5DF;
					case 4:
						while (i < buffer.Length)
						{
							if (i + 3 >= buffer.Length)
							{
								i = buffer.Length;
								break;
							}
							if (buffer[i] == 0 && buffer[i + 1] == 0 && buffer[i + 2] == 0 && buffer[i + 3] == 0)
							{
								break;
							}
							i++;
						}
						list.Add(encoding.GetChars(buffer, num, i - num));
						if (i == buffer.Length)
						{
							num = i;
							goto IL_5DF;
						}
						num = i + 4;
						goto IL_5DF;
					default:
						goto IL_5DF;
					}
					IL_49C:
					throw new ArgumentException("Invalid format for $ specifier", "description");
				}
				goto IL_5DF;
				IL_5C2:
				throw new ArgumentException(string.Format("invalid format specified `{0}'", description[num3]));
			}
			return list;
		}

		// Token: 0x06000114 RID: 276 RVA: 0x00005CB7 File Offset: 0x00003EB7
		internal void Check(byte[] dest, int destIdx, int size)
		{
			if (dest == null)
			{
				throw new ArgumentNullException("dest");
			}
			if (destIdx < 0 || destIdx > dest.Length - size)
			{
				throw new ArgumentException("destIdx");
			}
		}

		// Token: 0x06000115 RID: 277 RVA: 0x00002050 File Offset: 0x00000250
		protected DataConverter()
		{
		}

		// Token: 0x06000116 RID: 278 RVA: 0x00005CDE File Offset: 0x00003EDE
		// Note: this type is marked as 'beforefieldinit'.
		static DataConverter()
		{
		}

		// Token: 0x040003EE RID: 1006
		private static readonly DataConverter SwapConv = new DataConverter.SwapConverter();

		// Token: 0x040003EF RID: 1007
		private static readonly DataConverter CopyConv = new DataConverter.CopyConverter();

		// Token: 0x040003F0 RID: 1008
		public static readonly bool IsLittleEndian = BitConverter.IsLittleEndian;

		// Token: 0x02000028 RID: 40
		private class PackContext
		{
			// Token: 0x06000117 RID: 279 RVA: 0x00005D00 File Offset: 0x00003F00
			public void Add(byte[] group)
			{
				if (this.buffer == null)
				{
					this.buffer = group;
					this.next = group.Length;
					return;
				}
				if (this.align != 0)
				{
					if (this.align == -1)
					{
						this.next = DataConverter.Align(this.next, group.Length);
					}
					else
					{
						this.next = DataConverter.Align(this.next, this.align);
					}
					this.align = 0;
				}
				if (this.next + group.Length > this.buffer.Length)
				{
					byte[] destinationArray = new byte[Math.Max(this.next, 16) * 2 + group.Length];
					Array.Copy(this.buffer, destinationArray, this.buffer.Length);
					Array.Copy(group, 0, destinationArray, this.next, group.Length);
					this.next += group.Length;
					this.buffer = destinationArray;
					return;
				}
				Array.Copy(group, 0, this.buffer, this.next, group.Length);
				this.next += group.Length;
			}

			// Token: 0x06000118 RID: 280 RVA: 0x00005DFC File Offset: 0x00003FFC
			public byte[] Get()
			{
				if (this.buffer == null)
				{
					return new byte[0];
				}
				if (this.buffer.Length != this.next)
				{
					byte[] array = new byte[this.next];
					Array.Copy(this.buffer, array, this.next);
					return array;
				}
				return this.buffer;
			}

			// Token: 0x06000119 RID: 281 RVA: 0x00002050 File Offset: 0x00000250
			public PackContext()
			{
			}

			// Token: 0x040003F1 RID: 1009
			public byte[] buffer;

			// Token: 0x040003F2 RID: 1010
			private int next;

			// Token: 0x040003F3 RID: 1011
			public string description;

			// Token: 0x040003F4 RID: 1012
			public int i;

			// Token: 0x040003F5 RID: 1013
			public DataConverter conv;

			// Token: 0x040003F6 RID: 1014
			public int repeat;

			// Token: 0x040003F7 RID: 1015
			public int align;
		}

		// Token: 0x02000029 RID: 41
		private class CopyConverter : DataConverter
		{
			// Token: 0x0600011A RID: 282 RVA: 0x00005E50 File Offset: 0x00004050
			public unsafe override double GetDouble(byte[] data, int index)
			{
				if (data == null)
				{
					throw new ArgumentNullException("data");
				}
				if (data.Length - index < 8)
				{
					throw new ArgumentException("index");
				}
				if (index < 0)
				{
					throw new ArgumentException("index");
				}
				double result;
				byte* ptr = (byte*)(&result);
				for (int i = 0; i < 8; i++)
				{
					ptr[i] = data[index + i];
				}
				return result;
			}

			// Token: 0x0600011B RID: 283 RVA: 0x00005EA8 File Offset: 0x000040A8
			public unsafe override ulong GetUInt64(byte[] data, int index)
			{
				if (data == null)
				{
					throw new ArgumentNullException("data");
				}
				if (data.Length - index < 8)
				{
					throw new ArgumentException("index");
				}
				if (index < 0)
				{
					throw new ArgumentException("index");
				}
				ulong result;
				byte* ptr = (byte*)(&result);
				for (int i = 0; i < 8; i++)
				{
					ptr[i] = data[index + i];
				}
				return result;
			}

			// Token: 0x0600011C RID: 284 RVA: 0x00005F00 File Offset: 0x00004100
			public unsafe override long GetInt64(byte[] data, int index)
			{
				if (data == null)
				{
					throw new ArgumentNullException("data");
				}
				if (data.Length - index < 8)
				{
					throw new ArgumentException("index");
				}
				if (index < 0)
				{
					throw new ArgumentException("index");
				}
				long result;
				byte* ptr = (byte*)(&result);
				for (int i = 0; i < 8; i++)
				{
					ptr[i] = data[index + i];
				}
				return result;
			}

			// Token: 0x0600011D RID: 285 RVA: 0x00005F58 File Offset: 0x00004158
			public unsafe override float GetFloat(byte[] data, int index)
			{
				if (data == null)
				{
					throw new ArgumentNullException("data");
				}
				if (data.Length - index < 4)
				{
					throw new ArgumentException("index");
				}
				if (index < 0)
				{
					throw new ArgumentException("index");
				}
				float result;
				byte* ptr = (byte*)(&result);
				for (int i = 0; i < 4; i++)
				{
					ptr[i] = data[index + i];
				}
				return result;
			}

			// Token: 0x0600011E RID: 286 RVA: 0x00005FB0 File Offset: 0x000041B0
			public unsafe override int GetInt32(byte[] data, int index)
			{
				if (data == null)
				{
					throw new ArgumentNullException("data");
				}
				if (data.Length - index < 4)
				{
					throw new ArgumentException("index");
				}
				if (index < 0)
				{
					throw new ArgumentException("index");
				}
				int result;
				byte* ptr = (byte*)(&result);
				for (int i = 0; i < 4; i++)
				{
					ptr[i] = data[index + i];
				}
				return result;
			}

			// Token: 0x0600011F RID: 287 RVA: 0x00006008 File Offset: 0x00004208
			public unsafe override uint GetUInt32(byte[] data, int index)
			{
				if (data == null)
				{
					throw new ArgumentNullException("data");
				}
				if (data.Length - index < 4)
				{
					throw new ArgumentException("index");
				}
				if (index < 0)
				{
					throw new ArgumentException("index");
				}
				uint result;
				byte* ptr = (byte*)(&result);
				for (int i = 0; i < 4; i++)
				{
					ptr[i] = data[index + i];
				}
				return result;
			}

			// Token: 0x06000120 RID: 288 RVA: 0x00006060 File Offset: 0x00004260
			public unsafe override short GetInt16(byte[] data, int index)
			{
				if (data == null)
				{
					throw new ArgumentNullException("data");
				}
				if (data.Length - index < 2)
				{
					throw new ArgumentException("index");
				}
				if (index < 0)
				{
					throw new ArgumentException("index");
				}
				short result;
				byte* ptr = (byte*)(&result);
				for (int i = 0; i < 2; i++)
				{
					ptr[i] = data[index + i];
				}
				return result;
			}

			// Token: 0x06000121 RID: 289 RVA: 0x000060B8 File Offset: 0x000042B8
			public unsafe override ushort GetUInt16(byte[] data, int index)
			{
				if (data == null)
				{
					throw new ArgumentNullException("data");
				}
				if (data.Length - index < 2)
				{
					throw new ArgumentException("index");
				}
				if (index < 0)
				{
					throw new ArgumentException("index");
				}
				ushort result;
				byte* ptr = (byte*)(&result);
				for (int i = 0; i < 2; i++)
				{
					ptr[i] = data[index + i];
				}
				return result;
			}

			// Token: 0x06000122 RID: 290 RVA: 0x00006110 File Offset: 0x00004310
			public unsafe override void PutBytes(byte[] dest, int destIdx, double value)
			{
				base.Check(dest, destIdx, 8);
				fixed (byte* ptr = &dest[destIdx])
				{
					ref long ptr2 = ref *(long*)ptr;
					long* ptr3 = (long*)(&value);
					ptr2 = *ptr3;
				}
			}

			// Token: 0x06000123 RID: 291 RVA: 0x0000613C File Offset: 0x0000433C
			public unsafe override void PutBytes(byte[] dest, int destIdx, float value)
			{
				base.Check(dest, destIdx, 4);
				fixed (byte* ptr = &dest[destIdx])
				{
					ref int ptr2 = ref *(int*)ptr;
					uint* ptr3 = (uint*)(&value);
					ptr2 = (int)(*ptr3);
				}
			}

			// Token: 0x06000124 RID: 292 RVA: 0x00006168 File Offset: 0x00004368
			public unsafe override void PutBytes(byte[] dest, int destIdx, int value)
			{
				base.Check(dest, destIdx, 4);
				fixed (byte* ptr = &dest[destIdx])
				{
					ref int ptr2 = ref *(int*)ptr;
					uint* ptr3 = (uint*)(&value);
					ptr2 = (int)(*ptr3);
				}
			}

			// Token: 0x06000125 RID: 293 RVA: 0x00006194 File Offset: 0x00004394
			public unsafe override void PutBytes(byte[] dest, int destIdx, uint value)
			{
				base.Check(dest, destIdx, 4);
				fixed (byte* ptr = &dest[destIdx])
				{
					ref int ptr2 = ref *(int*)ptr;
					uint* ptr3 = &value;
					ptr2 = (int)(*ptr3);
				}
			}

			// Token: 0x06000126 RID: 294 RVA: 0x000061C0 File Offset: 0x000043C0
			public unsafe override void PutBytes(byte[] dest, int destIdx, long value)
			{
				base.Check(dest, destIdx, 8);
				fixed (byte* ptr = &dest[destIdx])
				{
					ref long ptr2 = ref *(long*)ptr;
					long* ptr3 = &value;
					ptr2 = *ptr3;
				}
			}

			// Token: 0x06000127 RID: 295 RVA: 0x000061EC File Offset: 0x000043EC
			public unsafe override void PutBytes(byte[] dest, int destIdx, ulong value)
			{
				base.Check(dest, destIdx, 8);
				fixed (byte* ptr = &dest[destIdx])
				{
					ref long ptr2 = ref *(long*)ptr;
					ulong* ptr3 = &value;
					ptr2 = (long)(*ptr3);
				}
			}

			// Token: 0x06000128 RID: 296 RVA: 0x00006218 File Offset: 0x00004418
			public unsafe override void PutBytes(byte[] dest, int destIdx, short value)
			{
				base.Check(dest, destIdx, 2);
				fixed (byte* ptr = &dest[destIdx])
				{
					ref short ptr2 = ref *(short*)ptr;
					ushort* ptr3 = (ushort*)(&value);
					ptr2 = (short)(*ptr3);
				}
			}

			// Token: 0x06000129 RID: 297 RVA: 0x00006244 File Offset: 0x00004444
			public unsafe override void PutBytes(byte[] dest, int destIdx, ushort value)
			{
				base.Check(dest, destIdx, 2);
				fixed (byte* ptr = &dest[destIdx])
				{
					ref short ptr2 = ref *(short*)ptr;
					ushort* ptr3 = &value;
					ptr2 = (short)(*ptr3);
				}
			}

			// Token: 0x0600012A RID: 298 RVA: 0x0000626E File Offset: 0x0000446E
			public CopyConverter()
			{
			}
		}

		// Token: 0x0200002A RID: 42
		private class SwapConverter : DataConverter
		{
			// Token: 0x0600012B RID: 299 RVA: 0x00006278 File Offset: 0x00004478
			public unsafe override double GetDouble(byte[] data, int index)
			{
				if (data == null)
				{
					throw new ArgumentNullException("data");
				}
				if (data.Length - index < 8)
				{
					throw new ArgumentException("index");
				}
				if (index < 0)
				{
					throw new ArgumentException("index");
				}
				double result;
				byte* ptr = (byte*)(&result);
				for (int i = 0; i < 8; i++)
				{
					ptr[7 - i] = data[index + i];
				}
				return result;
			}

			// Token: 0x0600012C RID: 300 RVA: 0x000062D4 File Offset: 0x000044D4
			public unsafe override ulong GetUInt64(byte[] data, int index)
			{
				if (data == null)
				{
					throw new ArgumentNullException("data");
				}
				if (data.Length - index < 8)
				{
					throw new ArgumentException("index");
				}
				if (index < 0)
				{
					throw new ArgumentException("index");
				}
				ulong result;
				byte* ptr = (byte*)(&result);
				for (int i = 0; i < 8; i++)
				{
					ptr[7 - i] = data[index + i];
				}
				return result;
			}

			// Token: 0x0600012D RID: 301 RVA: 0x00006330 File Offset: 0x00004530
			public unsafe override long GetInt64(byte[] data, int index)
			{
				if (data == null)
				{
					throw new ArgumentNullException("data");
				}
				if (data.Length - index < 8)
				{
					throw new ArgumentException("index");
				}
				if (index < 0)
				{
					throw new ArgumentException("index");
				}
				long result;
				byte* ptr = (byte*)(&result);
				for (int i = 0; i < 8; i++)
				{
					ptr[7 - i] = data[index + i];
				}
				return result;
			}

			// Token: 0x0600012E RID: 302 RVA: 0x0000638C File Offset: 0x0000458C
			public unsafe override float GetFloat(byte[] data, int index)
			{
				if (data == null)
				{
					throw new ArgumentNullException("data");
				}
				if (data.Length - index < 4)
				{
					throw new ArgumentException("index");
				}
				if (index < 0)
				{
					throw new ArgumentException("index");
				}
				float result;
				byte* ptr = (byte*)(&result);
				for (int i = 0; i < 4; i++)
				{
					ptr[3 - i] = data[index + i];
				}
				return result;
			}

			// Token: 0x0600012F RID: 303 RVA: 0x000063E8 File Offset: 0x000045E8
			public unsafe override int GetInt32(byte[] data, int index)
			{
				if (data == null)
				{
					throw new ArgumentNullException("data");
				}
				if (data.Length - index < 4)
				{
					throw new ArgumentException("index");
				}
				if (index < 0)
				{
					throw new ArgumentException("index");
				}
				int result;
				byte* ptr = (byte*)(&result);
				for (int i = 0; i < 4; i++)
				{
					ptr[3 - i] = data[index + i];
				}
				return result;
			}

			// Token: 0x06000130 RID: 304 RVA: 0x00006444 File Offset: 0x00004644
			public unsafe override uint GetUInt32(byte[] data, int index)
			{
				if (data == null)
				{
					throw new ArgumentNullException("data");
				}
				if (data.Length - index < 4)
				{
					throw new ArgumentException("index");
				}
				if (index < 0)
				{
					throw new ArgumentException("index");
				}
				uint result;
				byte* ptr = (byte*)(&result);
				for (int i = 0; i < 4; i++)
				{
					ptr[3 - i] = data[index + i];
				}
				return result;
			}

			// Token: 0x06000131 RID: 305 RVA: 0x000064A0 File Offset: 0x000046A0
			public unsafe override short GetInt16(byte[] data, int index)
			{
				if (data == null)
				{
					throw new ArgumentNullException("data");
				}
				if (data.Length - index < 2)
				{
					throw new ArgumentException("index");
				}
				if (index < 0)
				{
					throw new ArgumentException("index");
				}
				short result;
				byte* ptr = (byte*)(&result);
				for (int i = 0; i < 2; i++)
				{
					ptr[1 - i] = data[index + i];
				}
				return result;
			}

			// Token: 0x06000132 RID: 306 RVA: 0x000064FC File Offset: 0x000046FC
			public unsafe override ushort GetUInt16(byte[] data, int index)
			{
				if (data == null)
				{
					throw new ArgumentNullException("data");
				}
				if (data.Length - index < 2)
				{
					throw new ArgumentException("index");
				}
				if (index < 0)
				{
					throw new ArgumentException("index");
				}
				ushort result;
				byte* ptr = (byte*)(&result);
				for (int i = 0; i < 2; i++)
				{
					ptr[1 - i] = data[index + i];
				}
				return result;
			}

			// Token: 0x06000133 RID: 307 RVA: 0x00006558 File Offset: 0x00004758
			public unsafe override void PutBytes(byte[] dest, int destIdx, double value)
			{
				base.Check(dest, destIdx, 8);
				fixed (byte* ptr = &dest[destIdx])
				{
					byte* ptr2 = ptr;
					byte* ptr3 = (byte*)(&value);
					for (int i = 0; i < 8; i++)
					{
						ptr2[i] = ptr3[7 - i];
					}
				}
			}

			// Token: 0x06000134 RID: 308 RVA: 0x00006598 File Offset: 0x00004798
			public unsafe override void PutBytes(byte[] dest, int destIdx, float value)
			{
				base.Check(dest, destIdx, 4);
				fixed (byte* ptr = &dest[destIdx])
				{
					byte* ptr2 = ptr;
					byte* ptr3 = (byte*)(&value);
					for (int i = 0; i < 4; i++)
					{
						ptr2[i] = ptr3[3 - i];
					}
				}
			}

			// Token: 0x06000135 RID: 309 RVA: 0x000065D8 File Offset: 0x000047D8
			public unsafe override void PutBytes(byte[] dest, int destIdx, int value)
			{
				base.Check(dest, destIdx, 4);
				fixed (byte* ptr = &dest[destIdx])
				{
					byte* ptr2 = ptr;
					byte* ptr3 = (byte*)(&value);
					for (int i = 0; i < 4; i++)
					{
						ptr2[i] = ptr3[3 - i];
					}
				}
			}

			// Token: 0x06000136 RID: 310 RVA: 0x00006618 File Offset: 0x00004818
			public unsafe override void PutBytes(byte[] dest, int destIdx, uint value)
			{
				base.Check(dest, destIdx, 4);
				fixed (byte* ptr = &dest[destIdx])
				{
					byte* ptr2 = ptr;
					byte* ptr3 = (byte*)(&value);
					for (int i = 0; i < 4; i++)
					{
						ptr2[i] = ptr3[3 - i];
					}
				}
			}

			// Token: 0x06000137 RID: 311 RVA: 0x00006658 File Offset: 0x00004858
			public unsafe override void PutBytes(byte[] dest, int destIdx, long value)
			{
				base.Check(dest, destIdx, 8);
				fixed (byte* ptr = &dest[destIdx])
				{
					byte* ptr2 = ptr;
					byte* ptr3 = (byte*)(&value);
					for (int i = 0; i < 8; i++)
					{
						ptr2[i] = ptr3[7 - i];
					}
				}
			}

			// Token: 0x06000138 RID: 312 RVA: 0x00006698 File Offset: 0x00004898
			public unsafe override void PutBytes(byte[] dest, int destIdx, ulong value)
			{
				base.Check(dest, destIdx, 8);
				fixed (byte* ptr = &dest[destIdx])
				{
					byte* ptr2 = ptr;
					byte* ptr3 = (byte*)(&value);
					for (int i = 0; i < 8; i++)
					{
						ptr2[i] = ptr3[7 - i];
					}
				}
			}

			// Token: 0x06000139 RID: 313 RVA: 0x000066D8 File Offset: 0x000048D8
			public unsafe override void PutBytes(byte[] dest, int destIdx, short value)
			{
				base.Check(dest, destIdx, 2);
				fixed (byte* ptr = &dest[destIdx])
				{
					byte* ptr2 = ptr;
					byte* ptr3 = (byte*)(&value);
					for (int i = 0; i < 2; i++)
					{
						ptr2[i] = ptr3[1 - i];
					}
				}
			}

			// Token: 0x0600013A RID: 314 RVA: 0x00006718 File Offset: 0x00004918
			public unsafe override void PutBytes(byte[] dest, int destIdx, ushort value)
			{
				base.Check(dest, destIdx, 2);
				fixed (byte* ptr = &dest[destIdx])
				{
					byte* ptr2 = ptr;
					byte* ptr3 = (byte*)(&value);
					for (int i = 0; i < 2; i++)
					{
						ptr2[i] = ptr3[1 - i];
					}
				}
			}

			// Token: 0x0600013B RID: 315 RVA: 0x0000626E File Offset: 0x0000446E
			public SwapConverter()
			{
			}
		}
	}
}
