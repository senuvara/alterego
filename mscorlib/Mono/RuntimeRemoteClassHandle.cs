﻿using System;

namespace Mono
{
	// Token: 0x0200002D RID: 45
	internal struct RuntimeRemoteClassHandle
	{
		// Token: 0x0600014F RID: 335 RVA: 0x000068A2 File Offset: 0x00004AA2
		internal unsafe RuntimeRemoteClassHandle(RuntimeStructs.RemoteClass* value)
		{
			this.value = value;
		}

		// Token: 0x17000018 RID: 24
		// (get) Token: 0x06000150 RID: 336 RVA: 0x000068AB File Offset: 0x00004AAB
		internal unsafe RuntimeClassHandle ProxyClass
		{
			get
			{
				return new RuntimeClassHandle(this.value->proxy_class);
			}
		}

		// Token: 0x040003F9 RID: 1017
		private unsafe RuntimeStructs.RemoteClass* value;
	}
}
