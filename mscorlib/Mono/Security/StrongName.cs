﻿using System;
using System.Configuration.Assemblies;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Security.Cryptography;
using Mono.Security.Cryptography;

namespace Mono.Security
{
	// Token: 0x02000061 RID: 97
	internal sealed class StrongName
	{
		// Token: 0x060002CB RID: 715 RVA: 0x00002050 File Offset: 0x00000250
		public StrongName()
		{
		}

		// Token: 0x060002CC RID: 716 RVA: 0x0000DE5D File Offset: 0x0000C05D
		public StrongName(int keySize)
		{
			this.rsa = new RSAManaged(keySize);
		}

		// Token: 0x060002CD RID: 717 RVA: 0x0000DE74 File Offset: 0x0000C074
		public StrongName(byte[] data)
		{
			if (data == null)
			{
				throw new ArgumentNullException("data");
			}
			if (data.Length == 16)
			{
				int i = 0;
				int num = 0;
				while (i < data.Length)
				{
					num += (int)data[i++];
				}
				if (num == 4)
				{
					this.publicKey = (byte[])data.Clone();
					return;
				}
			}
			else
			{
				this.RSA = CryptoConvert.FromCapiKeyBlob(data);
				if (this.rsa == null)
				{
					throw new ArgumentException("data isn't a correctly encoded RSA public key");
				}
			}
		}

		// Token: 0x060002CE RID: 718 RVA: 0x0000DEE7 File Offset: 0x0000C0E7
		public StrongName(RSA rsa)
		{
			if (rsa == null)
			{
				throw new ArgumentNullException("rsa");
			}
			this.RSA = rsa;
		}

		// Token: 0x060002CF RID: 719 RVA: 0x0000DF04 File Offset: 0x0000C104
		private void InvalidateCache()
		{
			this.publicKey = null;
			this.keyToken = null;
		}

		// Token: 0x1700005B RID: 91
		// (get) Token: 0x060002D0 RID: 720 RVA: 0x0000DF14 File Offset: 0x0000C114
		public bool CanSign
		{
			get
			{
				if (this.rsa == null)
				{
					return false;
				}
				if (this.RSA is RSACryptoServiceProvider)
				{
					return !(this.rsa as RSACryptoServiceProvider).PublicOnly;
				}
				if (this.RSA is RSAManaged)
				{
					return !(this.rsa as RSAManaged).PublicOnly;
				}
				bool result;
				try
				{
					RSAParameters rsaparameters = this.rsa.ExportParameters(true);
					result = (rsaparameters.D != null && rsaparameters.P != null && rsaparameters.Q != null);
				}
				catch (CryptographicException)
				{
					result = false;
				}
				return result;
			}
		}

		// Token: 0x1700005C RID: 92
		// (get) Token: 0x060002D1 RID: 721 RVA: 0x0000DFB0 File Offset: 0x0000C1B0
		// (set) Token: 0x060002D2 RID: 722 RVA: 0x0000DFCB File Offset: 0x0000C1CB
		public RSA RSA
		{
			get
			{
				if (this.rsa == null)
				{
					this.rsa = RSA.Create();
				}
				return this.rsa;
			}
			set
			{
				this.rsa = value;
				this.InvalidateCache();
			}
		}

		// Token: 0x1700005D RID: 93
		// (get) Token: 0x060002D3 RID: 723 RVA: 0x0000DFDC File Offset: 0x0000C1DC
		public byte[] PublicKey
		{
			get
			{
				if (this.publicKey == null)
				{
					byte[] array = CryptoConvert.ToCapiKeyBlob(this.rsa, false);
					this.publicKey = new byte[32 + (this.rsa.KeySize >> 3)];
					this.publicKey[0] = array[4];
					this.publicKey[1] = array[5];
					this.publicKey[2] = array[6];
					this.publicKey[3] = array[7];
					this.publicKey[4] = 4;
					this.publicKey[5] = 128;
					this.publicKey[6] = 0;
					this.publicKey[7] = 0;
					byte[] bytes = BitConverterLE.GetBytes(this.publicKey.Length - 12);
					this.publicKey[8] = bytes[0];
					this.publicKey[9] = bytes[1];
					this.publicKey[10] = bytes[2];
					this.publicKey[11] = bytes[3];
					this.publicKey[12] = 6;
					Buffer.BlockCopy(array, 1, this.publicKey, 13, this.publicKey.Length - 13);
					this.publicKey[23] = 49;
				}
				return (byte[])this.publicKey.Clone();
			}
		}

		// Token: 0x1700005E RID: 94
		// (get) Token: 0x060002D4 RID: 724 RVA: 0x0000E0F0 File Offset: 0x0000C2F0
		public byte[] PublicKeyToken
		{
			get
			{
				if (this.keyToken == null)
				{
					byte[] array = this.PublicKey;
					if (array == null)
					{
						return null;
					}
					byte[] array2 = HashAlgorithm.Create(this.TokenAlgorithm).ComputeHash(array);
					this.keyToken = new byte[8];
					Buffer.BlockCopy(array2, array2.Length - 8, this.keyToken, 0, 8);
					Array.Reverse<byte>(this.keyToken, 0, 8);
				}
				return (byte[])this.keyToken.Clone();
			}
		}

		// Token: 0x1700005F RID: 95
		// (get) Token: 0x060002D5 RID: 725 RVA: 0x0000E15F File Offset: 0x0000C35F
		// (set) Token: 0x060002D6 RID: 726 RVA: 0x0000E17C File Offset: 0x0000C37C
		public string TokenAlgorithm
		{
			get
			{
				if (this.tokenAlgorithm == null)
				{
					this.tokenAlgorithm = "SHA1";
				}
				return this.tokenAlgorithm;
			}
			set
			{
				string a = value.ToUpper(CultureInfo.InvariantCulture);
				if (a == "SHA1" || a == "MD5")
				{
					this.tokenAlgorithm = value;
					this.InvalidateCache();
					return;
				}
				throw new ArgumentException("Unsupported hash algorithm for token");
			}
		}

		// Token: 0x060002D7 RID: 727 RVA: 0x0000E1C7 File Offset: 0x0000C3C7
		public byte[] GetBytes()
		{
			return CryptoConvert.ToCapiPrivateKeyBlob(this.RSA);
		}

		// Token: 0x060002D8 RID: 728 RVA: 0x0000E1D4 File Offset: 0x0000C3D4
		private uint RVAtoPosition(uint r, int sections, byte[] headers)
		{
			for (int i = 0; i < sections; i++)
			{
				uint num = BitConverterLE.ToUInt32(headers, i * 40 + 20);
				uint num2 = BitConverterLE.ToUInt32(headers, i * 40 + 12);
				int num3 = (int)BitConverterLE.ToUInt32(headers, i * 40 + 8);
				if (num2 <= r && (ulong)r < (ulong)num2 + (ulong)((long)num3))
				{
					return num + r - num2;
				}
			}
			return 0U;
		}

		// Token: 0x060002D9 RID: 729 RVA: 0x0000E22C File Offset: 0x0000C42C
		internal StrongName.StrongNameSignature StrongHash(Stream stream, StrongName.StrongNameOptions options)
		{
			StrongName.StrongNameSignature strongNameSignature = new StrongName.StrongNameSignature();
			HashAlgorithm hashAlgorithm = HashAlgorithm.Create(this.TokenAlgorithm);
			CryptoStream cryptoStream = new CryptoStream(Stream.Null, hashAlgorithm, CryptoStreamMode.Write);
			byte[] array = new byte[128];
			stream.Read(array, 0, 128);
			if (BitConverterLE.ToUInt16(array, 0) != 23117)
			{
				return null;
			}
			uint num = BitConverterLE.ToUInt32(array, 60);
			cryptoStream.Write(array, 0, 128);
			if (num != 128U)
			{
				byte[] array2 = new byte[num - 128U];
				stream.Read(array2, 0, array2.Length);
				cryptoStream.Write(array2, 0, array2.Length);
			}
			byte[] array3 = new byte[248];
			stream.Read(array3, 0, 248);
			if (BitConverterLE.ToUInt32(array3, 0) != 17744U)
			{
				return null;
			}
			if (BitConverterLE.ToUInt16(array3, 4) != 332)
			{
				return null;
			}
			byte[] src = new byte[8];
			Buffer.BlockCopy(src, 0, array3, 88, 4);
			Buffer.BlockCopy(src, 0, array3, 152, 8);
			cryptoStream.Write(array3, 0, 248);
			ushort num2 = BitConverterLE.ToUInt16(array3, 6);
			int num3 = (int)(num2 * 40);
			byte[] array4 = new byte[num3];
			stream.Read(array4, 0, num3);
			cryptoStream.Write(array4, 0, num3);
			uint r = BitConverterLE.ToUInt32(array3, 232);
			uint num4 = this.RVAtoPosition(r, (int)num2, array4);
			int num5 = (int)BitConverterLE.ToUInt32(array3, 236);
			byte[] array5 = new byte[num5];
			stream.Position = (long)((ulong)num4);
			stream.Read(array5, 0, num5);
			uint r2 = BitConverterLE.ToUInt32(array5, 32);
			strongNameSignature.SignaturePosition = this.RVAtoPosition(r2, (int)num2, array4);
			strongNameSignature.SignatureLength = BitConverterLE.ToUInt32(array5, 36);
			uint r3 = BitConverterLE.ToUInt32(array5, 8);
			strongNameSignature.MetadataPosition = this.RVAtoPosition(r3, (int)num2, array4);
			strongNameSignature.MetadataLength = BitConverterLE.ToUInt32(array5, 12);
			if (options == StrongName.StrongNameOptions.Metadata)
			{
				cryptoStream.Close();
				hashAlgorithm.Initialize();
				byte[] array6 = new byte[strongNameSignature.MetadataLength];
				stream.Position = (long)((ulong)strongNameSignature.MetadataPosition);
				stream.Read(array6, 0, array6.Length);
				strongNameSignature.Hash = hashAlgorithm.ComputeHash(array6);
				return strongNameSignature;
			}
			for (int i = 0; i < (int)num2; i++)
			{
				uint num6 = BitConverterLE.ToUInt32(array4, i * 40 + 20);
				int num7 = (int)BitConverterLE.ToUInt32(array4, i * 40 + 16);
				byte[] array7 = new byte[num7];
				stream.Position = (long)((ulong)num6);
				stream.Read(array7, 0, num7);
				if (num6 <= strongNameSignature.SignaturePosition && (ulong)strongNameSignature.SignaturePosition < (ulong)num6 + (ulong)((long)num7))
				{
					int num8 = (int)(strongNameSignature.SignaturePosition - num6);
					if (num8 > 0)
					{
						cryptoStream.Write(array7, 0, num8);
					}
					strongNameSignature.Signature = new byte[strongNameSignature.SignatureLength];
					Buffer.BlockCopy(array7, num8, strongNameSignature.Signature, 0, (int)strongNameSignature.SignatureLength);
					Array.Reverse<byte>(strongNameSignature.Signature);
					int num9 = (int)((long)num8 + (long)((ulong)strongNameSignature.SignatureLength));
					int num10 = num7 - num9;
					if (num10 > 0)
					{
						cryptoStream.Write(array7, num9, num10);
					}
				}
				else
				{
					cryptoStream.Write(array7, 0, num7);
				}
			}
			cryptoStream.Close();
			strongNameSignature.Hash = hashAlgorithm.Hash;
			return strongNameSignature;
		}

		// Token: 0x060002DA RID: 730 RVA: 0x0000E554 File Offset: 0x0000C754
		public byte[] Hash(string fileName)
		{
			FileStream fileStream = File.OpenRead(fileName);
			StrongName.StrongNameSignature strongNameSignature = this.StrongHash(fileStream, StrongName.StrongNameOptions.Metadata);
			fileStream.Close();
			return strongNameSignature.Hash;
		}

		// Token: 0x060002DB RID: 731 RVA: 0x0000E57C File Offset: 0x0000C77C
		public bool Sign(string fileName)
		{
			bool result = false;
			StrongName.StrongNameSignature strongNameSignature;
			using (FileStream fileStream = File.OpenRead(fileName))
			{
				strongNameSignature = this.StrongHash(fileStream, StrongName.StrongNameOptions.Signature);
				fileStream.Close();
			}
			if (strongNameSignature.Hash == null)
			{
				return false;
			}
			byte[] array = null;
			try
			{
				RSAPKCS1SignatureFormatter rsapkcs1SignatureFormatter = new RSAPKCS1SignatureFormatter(this.rsa);
				rsapkcs1SignatureFormatter.SetHashAlgorithm(this.TokenAlgorithm);
				array = rsapkcs1SignatureFormatter.CreateSignature(strongNameSignature.Hash);
				Array.Reverse<byte>(array);
			}
			catch (CryptographicException)
			{
				return false;
			}
			using (FileStream fileStream2 = File.OpenWrite(fileName))
			{
				fileStream2.Position = (long)((ulong)strongNameSignature.SignaturePosition);
				fileStream2.Write(array, 0, array.Length);
				fileStream2.Close();
				result = true;
			}
			return result;
		}

		// Token: 0x060002DC RID: 732 RVA: 0x0000E650 File Offset: 0x0000C850
		public bool Verify(string fileName)
		{
			bool result = false;
			using (FileStream fileStream = File.OpenRead(fileName))
			{
				result = this.Verify(fileStream);
				fileStream.Close();
			}
			return result;
		}

		// Token: 0x060002DD RID: 733 RVA: 0x0000E694 File Offset: 0x0000C894
		public bool Verify(Stream stream)
		{
			StrongName.StrongNameSignature strongNameSignature = this.StrongHash(stream, StrongName.StrongNameOptions.Signature);
			if (strongNameSignature.Hash == null)
			{
				return false;
			}
			bool result;
			try
			{
				AssemblyHashAlgorithm algorithm = AssemblyHashAlgorithm.SHA1;
				if (this.tokenAlgorithm == "MD5")
				{
					algorithm = AssemblyHashAlgorithm.MD5;
				}
				result = StrongName.Verify(this.rsa, algorithm, strongNameSignature.Hash, strongNameSignature.Signature);
			}
			catch (CryptographicException)
			{
				result = false;
			}
			return result;
		}

		// Token: 0x060002DE RID: 734 RVA: 0x0000E704 File Offset: 0x0000C904
		public static bool IsAssemblyStrongnamed(string assemblyName)
		{
			if (!StrongName.initialized)
			{
				object obj = StrongName.lockObject;
				lock (obj)
				{
					if (!StrongName.initialized)
					{
						StrongNameManager.LoadConfig(Environment.GetMachineConfigPath());
						StrongName.initialized = true;
					}
				}
			}
			bool flag;
			try
			{
				AssemblyName assemblyName2 = AssemblyName.GetAssemblyName(assemblyName);
				if (assemblyName2 == null)
				{
					flag = false;
				}
				else
				{
					byte[] mappedPublicKey = StrongNameManager.GetMappedPublicKey(assemblyName2.GetPublicKeyToken());
					if (mappedPublicKey == null || mappedPublicKey.Length < 12)
					{
						mappedPublicKey = assemblyName2.GetPublicKey();
						if (mappedPublicKey == null || mappedPublicKey.Length < 12)
						{
							return false;
						}
					}
					if (!StrongNameManager.MustVerify(assemblyName2))
					{
						flag = true;
					}
					else
					{
						flag = new StrongName(CryptoConvert.FromCapiPublicKeyBlob(mappedPublicKey, 12)).Verify(assemblyName);
					}
				}
			}
			catch
			{
				flag = false;
			}
			return flag;
		}

		// Token: 0x060002DF RID: 735 RVA: 0x0000E7CC File Offset: 0x0000C9CC
		public static bool VerifySignature(byte[] publicKey, int algorithm, byte[] hash, byte[] signature)
		{
			bool result;
			try
			{
				result = StrongName.Verify(CryptoConvert.FromCapiPublicKeyBlob(publicKey), (AssemblyHashAlgorithm)algorithm, hash, signature);
			}
			catch
			{
				result = false;
			}
			return result;
		}

		// Token: 0x060002E0 RID: 736 RVA: 0x0000E800 File Offset: 0x0000CA00
		private static bool Verify(RSA rsa, AssemblyHashAlgorithm algorithm, byte[] hash, byte[] signature)
		{
			RSAPKCS1SignatureDeformatter rsapkcs1SignatureDeformatter = new RSAPKCS1SignatureDeformatter(rsa);
			if (algorithm != AssemblyHashAlgorithm.None)
			{
				if (algorithm == AssemblyHashAlgorithm.MD5)
				{
					rsapkcs1SignatureDeformatter.SetHashAlgorithm("MD5");
					goto IL_34;
				}
				if (algorithm != AssemblyHashAlgorithm.SHA1)
				{
				}
			}
			rsapkcs1SignatureDeformatter.SetHashAlgorithm("SHA1");
			IL_34:
			return rsapkcs1SignatureDeformatter.VerifySignature(hash, signature);
		}

		// Token: 0x060002E1 RID: 737 RVA: 0x0000E849 File Offset: 0x0000CA49
		// Note: this type is marked as 'beforefieldinit'.
		static StrongName()
		{
		}

		// Token: 0x040004E4 RID: 1252
		private RSA rsa;

		// Token: 0x040004E5 RID: 1253
		private byte[] publicKey;

		// Token: 0x040004E6 RID: 1254
		private byte[] keyToken;

		// Token: 0x040004E7 RID: 1255
		private string tokenAlgorithm;

		// Token: 0x040004E8 RID: 1256
		private static object lockObject = new object();

		// Token: 0x040004E9 RID: 1257
		private static bool initialized;

		// Token: 0x02000062 RID: 98
		internal class StrongNameSignature
		{
			// Token: 0x17000060 RID: 96
			// (get) Token: 0x060002E2 RID: 738 RVA: 0x0000E855 File Offset: 0x0000CA55
			// (set) Token: 0x060002E3 RID: 739 RVA: 0x0000E85D File Offset: 0x0000CA5D
			public byte[] Hash
			{
				get
				{
					return this.hash;
				}
				set
				{
					this.hash = value;
				}
			}

			// Token: 0x17000061 RID: 97
			// (get) Token: 0x060002E4 RID: 740 RVA: 0x0000E866 File Offset: 0x0000CA66
			// (set) Token: 0x060002E5 RID: 741 RVA: 0x0000E86E File Offset: 0x0000CA6E
			public byte[] Signature
			{
				get
				{
					return this.signature;
				}
				set
				{
					this.signature = value;
				}
			}

			// Token: 0x17000062 RID: 98
			// (get) Token: 0x060002E6 RID: 742 RVA: 0x0000E877 File Offset: 0x0000CA77
			// (set) Token: 0x060002E7 RID: 743 RVA: 0x0000E87F File Offset: 0x0000CA7F
			public uint MetadataPosition
			{
				get
				{
					return this.metadataPosition;
				}
				set
				{
					this.metadataPosition = value;
				}
			}

			// Token: 0x17000063 RID: 99
			// (get) Token: 0x060002E8 RID: 744 RVA: 0x0000E888 File Offset: 0x0000CA88
			// (set) Token: 0x060002E9 RID: 745 RVA: 0x0000E890 File Offset: 0x0000CA90
			public uint MetadataLength
			{
				get
				{
					return this.metadataLength;
				}
				set
				{
					this.metadataLength = value;
				}
			}

			// Token: 0x17000064 RID: 100
			// (get) Token: 0x060002EA RID: 746 RVA: 0x0000E899 File Offset: 0x0000CA99
			// (set) Token: 0x060002EB RID: 747 RVA: 0x0000E8A1 File Offset: 0x0000CAA1
			public uint SignaturePosition
			{
				get
				{
					return this.signaturePosition;
				}
				set
				{
					this.signaturePosition = value;
				}
			}

			// Token: 0x17000065 RID: 101
			// (get) Token: 0x060002EC RID: 748 RVA: 0x0000E8AA File Offset: 0x0000CAAA
			// (set) Token: 0x060002ED RID: 749 RVA: 0x0000E8B2 File Offset: 0x0000CAB2
			public uint SignatureLength
			{
				get
				{
					return this.signatureLength;
				}
				set
				{
					this.signatureLength = value;
				}
			}

			// Token: 0x17000066 RID: 102
			// (get) Token: 0x060002EE RID: 750 RVA: 0x0000E8BB File Offset: 0x0000CABB
			// (set) Token: 0x060002EF RID: 751 RVA: 0x0000E8C3 File Offset: 0x0000CAC3
			public byte CliFlag
			{
				get
				{
					return this.cliFlag;
				}
				set
				{
					this.cliFlag = value;
				}
			}

			// Token: 0x17000067 RID: 103
			// (get) Token: 0x060002F0 RID: 752 RVA: 0x0000E8CC File Offset: 0x0000CACC
			// (set) Token: 0x060002F1 RID: 753 RVA: 0x0000E8D4 File Offset: 0x0000CAD4
			public uint CliFlagPosition
			{
				get
				{
					return this.cliFlagPosition;
				}
				set
				{
					this.cliFlagPosition = value;
				}
			}

			// Token: 0x060002F2 RID: 754 RVA: 0x00002050 File Offset: 0x00000250
			public StrongNameSignature()
			{
			}

			// Token: 0x040004EA RID: 1258
			private byte[] hash;

			// Token: 0x040004EB RID: 1259
			private byte[] signature;

			// Token: 0x040004EC RID: 1260
			private uint signaturePosition;

			// Token: 0x040004ED RID: 1261
			private uint signatureLength;

			// Token: 0x040004EE RID: 1262
			private uint metadataPosition;

			// Token: 0x040004EF RID: 1263
			private uint metadataLength;

			// Token: 0x040004F0 RID: 1264
			private byte cliFlag;

			// Token: 0x040004F1 RID: 1265
			private uint cliFlagPosition;
		}

		// Token: 0x02000063 RID: 99
		internal enum StrongNameOptions
		{
			// Token: 0x040004F3 RID: 1267
			Metadata,
			// Token: 0x040004F4 RID: 1268
			Signature
		}
	}
}
