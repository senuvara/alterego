﻿using System;
using System.Text;

namespace Mono.Security.X509
{
	// Token: 0x0200007D RID: 125
	internal class X520
	{
		// Token: 0x0600044B RID: 1099 RVA: 0x00002050 File Offset: 0x00000250
		public X520()
		{
		}

		// Token: 0x0200007E RID: 126
		public abstract class AttributeTypeAndValue
		{
			// Token: 0x0600044C RID: 1100 RVA: 0x00017D7D File Offset: 0x00015F7D
			protected AttributeTypeAndValue(string oid, int upperBound)
			{
				this.oid = oid;
				this.upperBound = upperBound;
				this.encoding = byte.MaxValue;
			}

			// Token: 0x0600044D RID: 1101 RVA: 0x00017D9E File Offset: 0x00015F9E
			protected AttributeTypeAndValue(string oid, int upperBound, byte encoding)
			{
				this.oid = oid;
				this.upperBound = upperBound;
				this.encoding = encoding;
			}

			// Token: 0x170000CE RID: 206
			// (get) Token: 0x0600044E RID: 1102 RVA: 0x00017DBB File Offset: 0x00015FBB
			// (set) Token: 0x0600044F RID: 1103 RVA: 0x00017DC4 File Offset: 0x00015FC4
			public string Value
			{
				get
				{
					return this.attrValue;
				}
				set
				{
					if (this.attrValue != null && this.attrValue.Length > this.upperBound)
					{
						throw new FormatException(string.Format(Locale.GetText("Value length bigger than upperbound ({0})."), this.upperBound));
					}
					this.attrValue = value;
				}
			}

			// Token: 0x170000CF RID: 207
			// (get) Token: 0x06000450 RID: 1104 RVA: 0x00017E13 File Offset: 0x00016013
			public ASN1 ASN1
			{
				get
				{
					return this.GetASN1();
				}
			}

			// Token: 0x06000451 RID: 1105 RVA: 0x00017E1C File Offset: 0x0001601C
			internal ASN1 GetASN1(byte encoding)
			{
				byte b = encoding;
				if (b == 255)
				{
					b = this.SelectBestEncoding();
				}
				ASN1 asn = new ASN1(48);
				asn.Add(ASN1Convert.FromOid(this.oid));
				if (b != 19)
				{
					if (b != 22)
					{
						if (b == 30)
						{
							asn.Add(new ASN1(30, Encoding.BigEndianUnicode.GetBytes(this.attrValue)));
						}
					}
					else
					{
						asn.Add(new ASN1(22, Encoding.ASCII.GetBytes(this.attrValue)));
					}
				}
				else
				{
					asn.Add(new ASN1(19, Encoding.ASCII.GetBytes(this.attrValue)));
				}
				return asn;
			}

			// Token: 0x06000452 RID: 1106 RVA: 0x00017EC4 File Offset: 0x000160C4
			internal ASN1 GetASN1()
			{
				return this.GetASN1(this.encoding);
			}

			// Token: 0x06000453 RID: 1107 RVA: 0x00017ED2 File Offset: 0x000160D2
			public byte[] GetBytes(byte encoding)
			{
				return this.GetASN1(encoding).GetBytes();
			}

			// Token: 0x06000454 RID: 1108 RVA: 0x00017EE0 File Offset: 0x000160E0
			public byte[] GetBytes()
			{
				return this.GetASN1().GetBytes();
			}

			// Token: 0x06000455 RID: 1109 RVA: 0x00017EF0 File Offset: 0x000160F0
			private byte SelectBestEncoding()
			{
				foreach (char c in this.attrValue)
				{
					if (c == '@' || c == '_')
					{
						return 30;
					}
					if (c > '\u007f')
					{
						return 30;
					}
				}
				return 19;
			}

			// Token: 0x040005AE RID: 1454
			private string oid;

			// Token: 0x040005AF RID: 1455
			private string attrValue;

			// Token: 0x040005B0 RID: 1456
			private int upperBound;

			// Token: 0x040005B1 RID: 1457
			private byte encoding;
		}

		// Token: 0x0200007F RID: 127
		public class Name : X520.AttributeTypeAndValue
		{
			// Token: 0x06000456 RID: 1110 RVA: 0x00017F34 File Offset: 0x00016134
			public Name() : base("2.5.4.41", 32768)
			{
			}
		}

		// Token: 0x02000080 RID: 128
		public class CommonName : X520.AttributeTypeAndValue
		{
			// Token: 0x06000457 RID: 1111 RVA: 0x00017F46 File Offset: 0x00016146
			public CommonName() : base("2.5.4.3", 64)
			{
			}
		}

		// Token: 0x02000081 RID: 129
		public class SerialNumber : X520.AttributeTypeAndValue
		{
			// Token: 0x06000458 RID: 1112 RVA: 0x00017F55 File Offset: 0x00016155
			public SerialNumber() : base("2.5.4.5", 64, 19)
			{
			}
		}

		// Token: 0x02000082 RID: 130
		public class LocalityName : X520.AttributeTypeAndValue
		{
			// Token: 0x06000459 RID: 1113 RVA: 0x00017F66 File Offset: 0x00016166
			public LocalityName() : base("2.5.4.7", 128)
			{
			}
		}

		// Token: 0x02000083 RID: 131
		public class StateOrProvinceName : X520.AttributeTypeAndValue
		{
			// Token: 0x0600045A RID: 1114 RVA: 0x00017F78 File Offset: 0x00016178
			public StateOrProvinceName() : base("2.5.4.8", 128)
			{
			}
		}

		// Token: 0x02000084 RID: 132
		public class OrganizationName : X520.AttributeTypeAndValue
		{
			// Token: 0x0600045B RID: 1115 RVA: 0x00017F8A File Offset: 0x0001618A
			public OrganizationName() : base("2.5.4.10", 64)
			{
			}
		}

		// Token: 0x02000085 RID: 133
		public class OrganizationalUnitName : X520.AttributeTypeAndValue
		{
			// Token: 0x0600045C RID: 1116 RVA: 0x00017F99 File Offset: 0x00016199
			public OrganizationalUnitName() : base("2.5.4.11", 64)
			{
			}
		}

		// Token: 0x02000086 RID: 134
		public class EmailAddress : X520.AttributeTypeAndValue
		{
			// Token: 0x0600045D RID: 1117 RVA: 0x00017FA8 File Offset: 0x000161A8
			public EmailAddress() : base("1.2.840.113549.1.9.1", 128, 22)
			{
			}
		}

		// Token: 0x02000087 RID: 135
		public class DomainComponent : X520.AttributeTypeAndValue
		{
			// Token: 0x0600045E RID: 1118 RVA: 0x00017FBC File Offset: 0x000161BC
			public DomainComponent() : base("0.9.2342.19200300.100.1.25", int.MaxValue, 22)
			{
			}
		}

		// Token: 0x02000088 RID: 136
		public class UserId : X520.AttributeTypeAndValue
		{
			// Token: 0x0600045F RID: 1119 RVA: 0x00017FD0 File Offset: 0x000161D0
			public UserId() : base("0.9.2342.19200300.100.1.1", 256)
			{
			}
		}

		// Token: 0x02000089 RID: 137
		public class Oid : X520.AttributeTypeAndValue
		{
			// Token: 0x06000460 RID: 1120 RVA: 0x00017FE2 File Offset: 0x000161E2
			public Oid(string oid) : base(oid, int.MaxValue)
			{
			}
		}

		// Token: 0x0200008A RID: 138
		public class Title : X520.AttributeTypeAndValue
		{
			// Token: 0x06000461 RID: 1121 RVA: 0x00017FF0 File Offset: 0x000161F0
			public Title() : base("2.5.4.12", 64)
			{
			}
		}

		// Token: 0x0200008B RID: 139
		public class CountryName : X520.AttributeTypeAndValue
		{
			// Token: 0x06000462 RID: 1122 RVA: 0x00017FFF File Offset: 0x000161FF
			public CountryName() : base("2.5.4.6", 2, 19)
			{
			}
		}

		// Token: 0x0200008C RID: 140
		public class DnQualifier : X520.AttributeTypeAndValue
		{
			// Token: 0x06000463 RID: 1123 RVA: 0x0001800F File Offset: 0x0001620F
			public DnQualifier() : base("2.5.4.46", 2, 19)
			{
			}
		}

		// Token: 0x0200008D RID: 141
		public class Surname : X520.AttributeTypeAndValue
		{
			// Token: 0x06000464 RID: 1124 RVA: 0x0001801F File Offset: 0x0001621F
			public Surname() : base("2.5.4.4", 32768)
			{
			}
		}

		// Token: 0x0200008E RID: 142
		public class GivenName : X520.AttributeTypeAndValue
		{
			// Token: 0x06000465 RID: 1125 RVA: 0x00018031 File Offset: 0x00016231
			public GivenName() : base("2.5.4.42", 16)
			{
			}
		}

		// Token: 0x0200008F RID: 143
		public class Initial : X520.AttributeTypeAndValue
		{
			// Token: 0x06000466 RID: 1126 RVA: 0x00018040 File Offset: 0x00016240
			public Initial() : base("2.5.4.43", 5)
			{
			}
		}
	}
}
