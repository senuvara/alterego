﻿using System;

namespace Mono.Security.X509
{
	// Token: 0x0200006A RID: 106
	internal class PKCS9
	{
		// Token: 0x06000335 RID: 821 RVA: 0x00002050 File Offset: 0x00000250
		public PKCS9()
		{
		}

		// Token: 0x04000522 RID: 1314
		public const string friendlyName = "1.2.840.113549.1.9.20";

		// Token: 0x04000523 RID: 1315
		public const string localKeyId = "1.2.840.113549.1.9.21";
	}
}
