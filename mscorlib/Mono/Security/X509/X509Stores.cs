﻿using System;
using System.IO;

namespace Mono.Security.X509
{
	// Token: 0x0200007B RID: 123
	internal class X509Stores
	{
		// Token: 0x06000442 RID: 1090 RVA: 0x00017B61 File Offset: 0x00015D61
		internal X509Stores(string path, bool newFormat)
		{
			this._storePath = path;
			this._newFormat = newFormat;
		}

		// Token: 0x170000C9 RID: 201
		// (get) Token: 0x06000443 RID: 1091 RVA: 0x00017B78 File Offset: 0x00015D78
		public X509Store Personal
		{
			get
			{
				if (this._personal == null)
				{
					string path = Path.Combine(this._storePath, "My");
					this._personal = new X509Store(path, false, false);
				}
				return this._personal;
			}
		}

		// Token: 0x170000CA RID: 202
		// (get) Token: 0x06000444 RID: 1092 RVA: 0x00017BB4 File Offset: 0x00015DB4
		public X509Store OtherPeople
		{
			get
			{
				if (this._other == null)
				{
					string path = Path.Combine(this._storePath, "AddressBook");
					this._other = new X509Store(path, false, false);
				}
				return this._other;
			}
		}

		// Token: 0x170000CB RID: 203
		// (get) Token: 0x06000445 RID: 1093 RVA: 0x00017BF0 File Offset: 0x00015DF0
		public X509Store IntermediateCA
		{
			get
			{
				if (this._intermediate == null)
				{
					string path = Path.Combine(this._storePath, "CA");
					this._intermediate = new X509Store(path, true, this._newFormat);
				}
				return this._intermediate;
			}
		}

		// Token: 0x170000CC RID: 204
		// (get) Token: 0x06000446 RID: 1094 RVA: 0x00017C30 File Offset: 0x00015E30
		public X509Store TrustedRoot
		{
			get
			{
				if (this._trusted == null)
				{
					string path = Path.Combine(this._storePath, "Trust");
					this._trusted = new X509Store(path, true, this._newFormat);
				}
				return this._trusted;
			}
		}

		// Token: 0x170000CD RID: 205
		// (get) Token: 0x06000447 RID: 1095 RVA: 0x00017C70 File Offset: 0x00015E70
		public X509Store Untrusted
		{
			get
			{
				if (this._untrusted == null)
				{
					string path = Path.Combine(this._storePath, "Disallowed");
					this._untrusted = new X509Store(path, false, this._newFormat);
				}
				return this._untrusted;
			}
		}

		// Token: 0x06000448 RID: 1096 RVA: 0x00017CB0 File Offset: 0x00015EB0
		public void Clear()
		{
			if (this._personal != null)
			{
				this._personal.Clear();
			}
			this._personal = null;
			if (this._other != null)
			{
				this._other.Clear();
			}
			this._other = null;
			if (this._intermediate != null)
			{
				this._intermediate.Clear();
			}
			this._intermediate = null;
			if (this._trusted != null)
			{
				this._trusted.Clear();
			}
			this._trusted = null;
			if (this._untrusted != null)
			{
				this._untrusted.Clear();
			}
			this._untrusted = null;
		}

		// Token: 0x06000449 RID: 1097 RVA: 0x00017D40 File Offset: 0x00015F40
		public X509Store Open(string storeName, bool create)
		{
			if (storeName == null)
			{
				throw new ArgumentNullException("storeName");
			}
			string path = Path.Combine(this._storePath, storeName);
			if (!create && !Directory.Exists(path))
			{
				return null;
			}
			return new X509Store(path, true, false);
		}

		// Token: 0x040005A2 RID: 1442
		private string _storePath;

		// Token: 0x040005A3 RID: 1443
		private bool _newFormat;

		// Token: 0x040005A4 RID: 1444
		private X509Store _personal;

		// Token: 0x040005A5 RID: 1445
		private X509Store _other;

		// Token: 0x040005A6 RID: 1446
		private X509Store _intermediate;

		// Token: 0x040005A7 RID: 1447
		private X509Store _trusted;

		// Token: 0x040005A8 RID: 1448
		private X509Store _untrusted;

		// Token: 0x0200007C RID: 124
		public class Names
		{
			// Token: 0x0600044A RID: 1098 RVA: 0x00002050 File Offset: 0x00000250
			public Names()
			{
			}

			// Token: 0x040005A9 RID: 1449
			public const string Personal = "My";

			// Token: 0x040005AA RID: 1450
			public const string OtherPeople = "AddressBook";

			// Token: 0x040005AB RID: 1451
			public const string IntermediateCA = "CA";

			// Token: 0x040005AC RID: 1452
			public const string TrustedRoot = "Trust";

			// Token: 0x040005AD RID: 1453
			public const string Untrusted = "Disallowed";
		}
	}
}
