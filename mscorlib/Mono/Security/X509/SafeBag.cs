﻿using System;

namespace Mono.Security.X509
{
	// Token: 0x0200006B RID: 107
	internal class SafeBag
	{
		// Token: 0x06000336 RID: 822 RVA: 0x00010D64 File Offset: 0x0000EF64
		public SafeBag(string bagOID, ASN1 asn1)
		{
			this._bagOID = bagOID;
			this._asn1 = asn1;
		}

		// Token: 0x17000079 RID: 121
		// (get) Token: 0x06000337 RID: 823 RVA: 0x00010D7A File Offset: 0x0000EF7A
		public string BagOID
		{
			get
			{
				return this._bagOID;
			}
		}

		// Token: 0x1700007A RID: 122
		// (get) Token: 0x06000338 RID: 824 RVA: 0x00010D82 File Offset: 0x0000EF82
		public ASN1 ASN1
		{
			get
			{
				return this._asn1;
			}
		}

		// Token: 0x04000524 RID: 1316
		private string _bagOID;

		// Token: 0x04000525 RID: 1317
		private ASN1 _asn1;
	}
}
