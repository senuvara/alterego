﻿using System;
using System.Collections;

namespace Mono.Security.X509
{
	// Token: 0x02000073 RID: 115
	[Serializable]
	internal class X509CertificateCollection : CollectionBase, IEnumerable
	{
		// Token: 0x060003D2 RID: 978 RVA: 0x000164F7 File Offset: 0x000146F7
		public X509CertificateCollection()
		{
		}

		// Token: 0x060003D3 RID: 979 RVA: 0x000164FF File Offset: 0x000146FF
		public X509CertificateCollection(X509Certificate[] value)
		{
			this.AddRange(value);
		}

		// Token: 0x060003D4 RID: 980 RVA: 0x0001650E File Offset: 0x0001470E
		public X509CertificateCollection(X509CertificateCollection value)
		{
			this.AddRange(value);
		}

		// Token: 0x170000AB RID: 171
		public X509Certificate this[int index]
		{
			get
			{
				return (X509Certificate)base.InnerList[index];
			}
			set
			{
				base.InnerList[index] = value;
			}
		}

		// Token: 0x060003D7 RID: 983 RVA: 0x0001653F File Offset: 0x0001473F
		public int Add(X509Certificate value)
		{
			if (value == null)
			{
				throw new ArgumentNullException("value");
			}
			return base.InnerList.Add(value);
		}

		// Token: 0x060003D8 RID: 984 RVA: 0x0001655C File Offset: 0x0001475C
		public void AddRange(X509Certificate[] value)
		{
			if (value == null)
			{
				throw new ArgumentNullException("value");
			}
			for (int i = 0; i < value.Length; i++)
			{
				base.InnerList.Add(value[i]);
			}
		}

		// Token: 0x060003D9 RID: 985 RVA: 0x00016594 File Offset: 0x00014794
		public void AddRange(X509CertificateCollection value)
		{
			if (value == null)
			{
				throw new ArgumentNullException("value");
			}
			for (int i = 0; i < value.InnerList.Count; i++)
			{
				base.InnerList.Add(value[i]);
			}
		}

		// Token: 0x060003DA RID: 986 RVA: 0x000165D8 File Offset: 0x000147D8
		public bool Contains(X509Certificate value)
		{
			return this.IndexOf(value) != -1;
		}

		// Token: 0x060003DB RID: 987 RVA: 0x000165E7 File Offset: 0x000147E7
		public void CopyTo(X509Certificate[] array, int index)
		{
			base.InnerList.CopyTo(array, index);
		}

		// Token: 0x060003DC RID: 988 RVA: 0x000165F6 File Offset: 0x000147F6
		public new X509CertificateCollection.X509CertificateEnumerator GetEnumerator()
		{
			return new X509CertificateCollection.X509CertificateEnumerator(this);
		}

		// Token: 0x060003DD RID: 989 RVA: 0x000165FE File Offset: 0x000147FE
		IEnumerator IEnumerable.GetEnumerator()
		{
			return base.InnerList.GetEnumerator();
		}

		// Token: 0x060003DE RID: 990 RVA: 0x0001660B File Offset: 0x0001480B
		public override int GetHashCode()
		{
			return base.InnerList.GetHashCode();
		}

		// Token: 0x060003DF RID: 991 RVA: 0x00016618 File Offset: 0x00014818
		public int IndexOf(X509Certificate value)
		{
			if (value == null)
			{
				throw new ArgumentNullException("value");
			}
			byte[] hash = value.Hash;
			for (int i = 0; i < base.InnerList.Count; i++)
			{
				X509Certificate x509Certificate = (X509Certificate)base.InnerList[i];
				if (this.Compare(x509Certificate.Hash, hash))
				{
					return i;
				}
			}
			return -1;
		}

		// Token: 0x060003E0 RID: 992 RVA: 0x00016674 File Offset: 0x00014874
		public void Insert(int index, X509Certificate value)
		{
			base.InnerList.Insert(index, value);
		}

		// Token: 0x060003E1 RID: 993 RVA: 0x00016683 File Offset: 0x00014883
		public void Remove(X509Certificate value)
		{
			base.InnerList.Remove(value);
		}

		// Token: 0x060003E2 RID: 994 RVA: 0x00016694 File Offset: 0x00014894
		private bool Compare(byte[] array1, byte[] array2)
		{
			if (array1 == null && array2 == null)
			{
				return true;
			}
			if (array1 == null || array2 == null)
			{
				return false;
			}
			if (array1.Length != array2.Length)
			{
				return false;
			}
			for (int i = 0; i < array1.Length; i++)
			{
				if (array1[i] != array2[i])
				{
					return false;
				}
			}
			return true;
		}

		// Token: 0x02000074 RID: 116
		public class X509CertificateEnumerator : IEnumerator
		{
			// Token: 0x060003E3 RID: 995 RVA: 0x000166D4 File Offset: 0x000148D4
			public X509CertificateEnumerator(X509CertificateCollection mappings)
			{
				this.enumerator = ((IEnumerable)mappings).GetEnumerator();
			}

			// Token: 0x170000AC RID: 172
			// (get) Token: 0x060003E4 RID: 996 RVA: 0x000166E8 File Offset: 0x000148E8
			public X509Certificate Current
			{
				get
				{
					return (X509Certificate)this.enumerator.Current;
				}
			}

			// Token: 0x170000AD RID: 173
			// (get) Token: 0x060003E5 RID: 997 RVA: 0x000166FA File Offset: 0x000148FA
			object IEnumerator.Current
			{
				get
				{
					return this.enumerator.Current;
				}
			}

			// Token: 0x060003E6 RID: 998 RVA: 0x00016707 File Offset: 0x00014907
			bool IEnumerator.MoveNext()
			{
				return this.enumerator.MoveNext();
			}

			// Token: 0x060003E7 RID: 999 RVA: 0x00016714 File Offset: 0x00014914
			void IEnumerator.Reset()
			{
				this.enumerator.Reset();
			}

			// Token: 0x060003E8 RID: 1000 RVA: 0x00016707 File Offset: 0x00014907
			public bool MoveNext()
			{
				return this.enumerator.MoveNext();
			}

			// Token: 0x060003E9 RID: 1001 RVA: 0x00016714 File Offset: 0x00014914
			public void Reset()
			{
				this.enumerator.Reset();
			}

			// Token: 0x04000582 RID: 1410
			private IEnumerator enumerator;
		}
	}
}
