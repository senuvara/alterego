﻿using System;
using System.Globalization;
using System.Text;

namespace Mono.Security.X509
{
	// Token: 0x02000077 RID: 119
	internal class X509Extension
	{
		// Token: 0x060003FB RID: 1019 RVA: 0x00016B85 File Offset: 0x00014D85
		protected X509Extension()
		{
			this.extnCritical = false;
		}

		// Token: 0x060003FC RID: 1020 RVA: 0x00016B94 File Offset: 0x00014D94
		public X509Extension(ASN1 asn1)
		{
			if (asn1.Tag != 48 || asn1.Count < 2)
			{
				throw new ArgumentException(Locale.GetText("Invalid X.509 extension."));
			}
			if (asn1[0].Tag != 6)
			{
				throw new ArgumentException(Locale.GetText("Invalid X.509 extension."));
			}
			this.extnOid = ASN1Convert.ToOid(asn1[0]);
			this.extnCritical = (asn1[1].Tag == 1 && asn1[1].Value[0] == byte.MaxValue);
			this.extnValue = asn1[asn1.Count - 1];
			if (this.extnValue.Tag == 4 && this.extnValue.Length > 0 && this.extnValue.Count == 0)
			{
				try
				{
					ASN1 asn2 = new ASN1(this.extnValue.Value);
					this.extnValue.Value = null;
					this.extnValue.Add(asn2);
				}
				catch
				{
				}
			}
			this.Decode();
		}

		// Token: 0x060003FD RID: 1021 RVA: 0x00016CAC File Offset: 0x00014EAC
		public X509Extension(X509Extension extension)
		{
			if (extension == null)
			{
				throw new ArgumentNullException("extension");
			}
			if (extension.Value == null || extension.Value.Tag != 4 || extension.Value.Count != 1)
			{
				throw new ArgumentException(Locale.GetText("Invalid X.509 extension."));
			}
			this.extnOid = extension.Oid;
			this.extnCritical = extension.Critical;
			this.extnValue = extension.Value;
			this.Decode();
		}

		// Token: 0x060003FE RID: 1022 RVA: 0x000020D3 File Offset: 0x000002D3
		protected virtual void Decode()
		{
		}

		// Token: 0x060003FF RID: 1023 RVA: 0x000020D3 File Offset: 0x000002D3
		protected virtual void Encode()
		{
		}

		// Token: 0x170000B2 RID: 178
		// (get) Token: 0x06000400 RID: 1024 RVA: 0x00016D2C File Offset: 0x00014F2C
		public ASN1 ASN1
		{
			get
			{
				ASN1 asn = new ASN1(48);
				asn.Add(ASN1Convert.FromOid(this.extnOid));
				if (this.extnCritical)
				{
					asn.Add(new ASN1(1, new byte[]
					{
						byte.MaxValue
					}));
				}
				this.Encode();
				asn.Add(this.extnValue);
				return asn;
			}
		}

		// Token: 0x170000B3 RID: 179
		// (get) Token: 0x06000401 RID: 1025 RVA: 0x00016D8A File Offset: 0x00014F8A
		public string Oid
		{
			get
			{
				return this.extnOid;
			}
		}

		// Token: 0x170000B4 RID: 180
		// (get) Token: 0x06000402 RID: 1026 RVA: 0x00016D92 File Offset: 0x00014F92
		// (set) Token: 0x06000403 RID: 1027 RVA: 0x00016D9A File Offset: 0x00014F9A
		public bool Critical
		{
			get
			{
				return this.extnCritical;
			}
			set
			{
				this.extnCritical = value;
			}
		}

		// Token: 0x170000B5 RID: 181
		// (get) Token: 0x06000404 RID: 1028 RVA: 0x00016D8A File Offset: 0x00014F8A
		public virtual string Name
		{
			get
			{
				return this.extnOid;
			}
		}

		// Token: 0x170000B6 RID: 182
		// (get) Token: 0x06000405 RID: 1029 RVA: 0x00016DA3 File Offset: 0x00014FA3
		public ASN1 Value
		{
			get
			{
				if (this.extnValue == null)
				{
					this.Encode();
				}
				return this.extnValue;
			}
		}

		// Token: 0x06000406 RID: 1030 RVA: 0x00016DBC File Offset: 0x00014FBC
		public override bool Equals(object obj)
		{
			if (obj == null)
			{
				return false;
			}
			X509Extension x509Extension = obj as X509Extension;
			if (x509Extension == null)
			{
				return false;
			}
			if (this.extnCritical != x509Extension.extnCritical)
			{
				return false;
			}
			if (this.extnOid != x509Extension.extnOid)
			{
				return false;
			}
			if (this.extnValue.Length != x509Extension.extnValue.Length)
			{
				return false;
			}
			for (int i = 0; i < this.extnValue.Length; i++)
			{
				if (this.extnValue[i] != x509Extension.extnValue[i])
				{
					return false;
				}
			}
			return true;
		}

		// Token: 0x06000407 RID: 1031 RVA: 0x00016E4C File Offset: 0x0001504C
		public byte[] GetBytes()
		{
			return this.ASN1.GetBytes();
		}

		// Token: 0x06000408 RID: 1032 RVA: 0x00016E59 File Offset: 0x00015059
		public override int GetHashCode()
		{
			return this.extnOid.GetHashCode();
		}

		// Token: 0x06000409 RID: 1033 RVA: 0x00016E68 File Offset: 0x00015068
		private void WriteLine(StringBuilder sb, int n, int pos)
		{
			byte[] value = this.extnValue.Value;
			int num = pos;
			for (int i = 0; i < 8; i++)
			{
				if (i < n)
				{
					sb.Append(value[num++].ToString("X2", CultureInfo.InvariantCulture));
					sb.Append(" ");
				}
				else
				{
					sb.Append("   ");
				}
			}
			sb.Append("  ");
			num = pos;
			for (int j = 0; j < n; j++)
			{
				byte b = value[num++];
				if (b < 32)
				{
					sb.Append(".");
				}
				else
				{
					sb.Append(Convert.ToChar(b));
				}
			}
			sb.Append(Environment.NewLine);
		}

		// Token: 0x0600040A RID: 1034 RVA: 0x00016F20 File Offset: 0x00015120
		public override string ToString()
		{
			StringBuilder stringBuilder = new StringBuilder();
			int num = this.extnValue.Length >> 3;
			int n = this.extnValue.Length - (num << 3);
			int num2 = 0;
			for (int i = 0; i < num; i++)
			{
				this.WriteLine(stringBuilder, 8, num2);
				num2 += 8;
			}
			this.WriteLine(stringBuilder, n, num2);
			return stringBuilder.ToString();
		}

		// Token: 0x04000590 RID: 1424
		protected string extnOid;

		// Token: 0x04000591 RID: 1425
		protected bool extnCritical;

		// Token: 0x04000592 RID: 1426
		protected ASN1 extnValue;
	}
}
