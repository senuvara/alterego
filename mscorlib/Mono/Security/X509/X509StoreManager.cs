﻿using System;
using System.Collections;
using System.IO;

namespace Mono.Security.X509
{
	// Token: 0x0200007A RID: 122
	internal sealed class X509StoreManager
	{
		// Token: 0x06000434 RID: 1076 RVA: 0x00002050 File Offset: 0x00000250
		private X509StoreManager()
		{
		}

		// Token: 0x170000BC RID: 188
		// (get) Token: 0x06000435 RID: 1077 RVA: 0x00017914 File Offset: 0x00015B14
		internal static string CurrentUserPath
		{
			get
			{
				if (X509StoreManager._userPath == null)
				{
					X509StoreManager._userPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), ".mono");
					X509StoreManager._userPath = Path.Combine(X509StoreManager._userPath, "certs");
				}
				return X509StoreManager._userPath;
			}
		}

		// Token: 0x170000BD RID: 189
		// (get) Token: 0x06000436 RID: 1078 RVA: 0x0001794C File Offset: 0x00015B4C
		internal static string LocalMachinePath
		{
			get
			{
				if (X509StoreManager._localMachinePath == null)
				{
					X509StoreManager._localMachinePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), ".mono");
					X509StoreManager._localMachinePath = Path.Combine(X509StoreManager._localMachinePath, "certs");
				}
				return X509StoreManager._localMachinePath;
			}
		}

		// Token: 0x170000BE RID: 190
		// (get) Token: 0x06000437 RID: 1079 RVA: 0x00017984 File Offset: 0x00015B84
		internal static string NewCurrentUserPath
		{
			get
			{
				if (X509StoreManager._newUserPath == null)
				{
					X509StoreManager._newUserPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), ".mono");
					X509StoreManager._newUserPath = Path.Combine(X509StoreManager._newUserPath, "new-certs");
				}
				return X509StoreManager._newUserPath;
			}
		}

		// Token: 0x170000BF RID: 191
		// (get) Token: 0x06000438 RID: 1080 RVA: 0x000179BC File Offset: 0x00015BBC
		internal static string NewLocalMachinePath
		{
			get
			{
				if (X509StoreManager._newLocalMachinePath == null)
				{
					X509StoreManager._newLocalMachinePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), ".mono");
					X509StoreManager._newLocalMachinePath = Path.Combine(X509StoreManager._newLocalMachinePath, "new-certs");
				}
				return X509StoreManager._newLocalMachinePath;
			}
		}

		// Token: 0x170000C0 RID: 192
		// (get) Token: 0x06000439 RID: 1081 RVA: 0x000179F4 File Offset: 0x00015BF4
		public static X509Stores CurrentUser
		{
			get
			{
				if (X509StoreManager._userStore == null)
				{
					X509StoreManager._userStore = new X509Stores(X509StoreManager.CurrentUserPath, false);
				}
				return X509StoreManager._userStore;
			}
		}

		// Token: 0x170000C1 RID: 193
		// (get) Token: 0x0600043A RID: 1082 RVA: 0x00017A12 File Offset: 0x00015C12
		public static X509Stores LocalMachine
		{
			get
			{
				if (X509StoreManager._machineStore == null)
				{
					X509StoreManager._machineStore = new X509Stores(X509StoreManager.LocalMachinePath, false);
				}
				return X509StoreManager._machineStore;
			}
		}

		// Token: 0x170000C2 RID: 194
		// (get) Token: 0x0600043B RID: 1083 RVA: 0x00017A30 File Offset: 0x00015C30
		public static X509Stores NewCurrentUser
		{
			get
			{
				if (X509StoreManager._newUserStore == null)
				{
					X509StoreManager._newUserStore = new X509Stores(X509StoreManager.NewCurrentUserPath, true);
				}
				return X509StoreManager._newUserStore;
			}
		}

		// Token: 0x170000C3 RID: 195
		// (get) Token: 0x0600043C RID: 1084 RVA: 0x00017A4E File Offset: 0x00015C4E
		public static X509Stores NewLocalMachine
		{
			get
			{
				if (X509StoreManager._newMachineStore == null)
				{
					X509StoreManager._newMachineStore = new X509Stores(X509StoreManager.NewLocalMachinePath, true);
				}
				return X509StoreManager._newMachineStore;
			}
		}

		// Token: 0x170000C4 RID: 196
		// (get) Token: 0x0600043D RID: 1085 RVA: 0x00017A6C File Offset: 0x00015C6C
		public static X509CertificateCollection IntermediateCACertificates
		{
			get
			{
				X509CertificateCollection x509CertificateCollection = new X509CertificateCollection();
				x509CertificateCollection.AddRange(X509StoreManager.CurrentUser.IntermediateCA.Certificates);
				x509CertificateCollection.AddRange(X509StoreManager.LocalMachine.IntermediateCA.Certificates);
				return x509CertificateCollection;
			}
		}

		// Token: 0x170000C5 RID: 197
		// (get) Token: 0x0600043E RID: 1086 RVA: 0x00017A9D File Offset: 0x00015C9D
		public static ArrayList IntermediateCACrls
		{
			get
			{
				ArrayList arrayList = new ArrayList();
				arrayList.AddRange(X509StoreManager.CurrentUser.IntermediateCA.Crls);
				arrayList.AddRange(X509StoreManager.LocalMachine.IntermediateCA.Crls);
				return arrayList;
			}
		}

		// Token: 0x170000C6 RID: 198
		// (get) Token: 0x0600043F RID: 1087 RVA: 0x00017ACE File Offset: 0x00015CCE
		public static X509CertificateCollection TrustedRootCertificates
		{
			get
			{
				X509CertificateCollection x509CertificateCollection = new X509CertificateCollection();
				x509CertificateCollection.AddRange(X509StoreManager.CurrentUser.TrustedRoot.Certificates);
				x509CertificateCollection.AddRange(X509StoreManager.LocalMachine.TrustedRoot.Certificates);
				return x509CertificateCollection;
			}
		}

		// Token: 0x170000C7 RID: 199
		// (get) Token: 0x06000440 RID: 1088 RVA: 0x00017AFF File Offset: 0x00015CFF
		public static ArrayList TrustedRootCACrls
		{
			get
			{
				ArrayList arrayList = new ArrayList();
				arrayList.AddRange(X509StoreManager.CurrentUser.TrustedRoot.Crls);
				arrayList.AddRange(X509StoreManager.LocalMachine.TrustedRoot.Crls);
				return arrayList;
			}
		}

		// Token: 0x170000C8 RID: 200
		// (get) Token: 0x06000441 RID: 1089 RVA: 0x00017B30 File Offset: 0x00015D30
		public static X509CertificateCollection UntrustedCertificates
		{
			get
			{
				X509CertificateCollection x509CertificateCollection = new X509CertificateCollection();
				x509CertificateCollection.AddRange(X509StoreManager.CurrentUser.Untrusted.Certificates);
				x509CertificateCollection.AddRange(X509StoreManager.LocalMachine.Untrusted.Certificates);
				return x509CertificateCollection;
			}
		}

		// Token: 0x0400059A RID: 1434
		private static string _userPath;

		// Token: 0x0400059B RID: 1435
		private static string _localMachinePath;

		// Token: 0x0400059C RID: 1436
		private static string _newUserPath;

		// Token: 0x0400059D RID: 1437
		private static string _newLocalMachinePath;

		// Token: 0x0400059E RID: 1438
		private static X509Stores _userStore;

		// Token: 0x0400059F RID: 1439
		private static X509Stores _machineStore;

		// Token: 0x040005A0 RID: 1440
		private static X509Stores _newUserStore;

		// Token: 0x040005A1 RID: 1441
		private static X509Stores _newMachineStore;
	}
}
