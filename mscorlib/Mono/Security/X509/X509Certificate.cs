﻿using System;
using System.Runtime.Serialization;
using System.Security.Cryptography;
using System.Security.Permissions;
using System.Text;
using Mono.Security.Cryptography;

namespace Mono.Security.X509
{
	// Token: 0x02000072 RID: 114
	internal class X509Certificate : ISerializable
	{
		// Token: 0x060003AC RID: 940 RVA: 0x00015908 File Offset: 0x00013B08
		private void Parse(byte[] data)
		{
			try
			{
				this.decoder = new ASN1(data);
				if (this.decoder.Tag != 48)
				{
					throw new CryptographicException(X509Certificate.encoding_error);
				}
				if (this.decoder[0].Tag != 48)
				{
					throw new CryptographicException(X509Certificate.encoding_error);
				}
				ASN1 asn = this.decoder[0];
				int num = 0;
				ASN1 asn2 = this.decoder[0][num];
				this.version = 1;
				if (asn2.Tag == 160 && asn2.Count > 0)
				{
					this.version += (int)asn2[0].Value[0];
					num++;
				}
				ASN1 asn3 = this.decoder[0][num++];
				if (asn3.Tag != 2)
				{
					throw new CryptographicException(X509Certificate.encoding_error);
				}
				this.serialnumber = asn3.Value;
				Array.Reverse<byte>(this.serialnumber, 0, this.serialnumber.Length);
				num++;
				this.issuer = asn.Element(num++, 48);
				this.m_issuername = X501.ToString(this.issuer);
				ASN1 asn4 = asn.Element(num++, 48);
				ASN1 time = asn4[0];
				this.m_from = ASN1Convert.ToDateTime(time);
				ASN1 time2 = asn4[1];
				this.m_until = ASN1Convert.ToDateTime(time2);
				this.subject = asn.Element(num++, 48);
				this.m_subject = X501.ToString(this.subject);
				ASN1 asn5 = asn.Element(num++, 48);
				ASN1 asn6 = asn5.Element(0, 48);
				ASN1 asn7 = asn6.Element(0, 6);
				this.m_keyalgo = ASN1Convert.ToOid(asn7);
				ASN1 asn8 = asn6[1];
				this.m_keyalgoparams = ((asn6.Count > 1) ? asn8.GetBytes() : null);
				ASN1 asn9 = asn5.Element(1, 3);
				int num2 = asn9.Length - 1;
				this.m_publickey = new byte[num2];
				Buffer.BlockCopy(asn9.Value, 1, this.m_publickey, 0, num2);
				byte[] value = this.decoder[2].Value;
				this.signature = new byte[value.Length - 1];
				Buffer.BlockCopy(value, 1, this.signature, 0, this.signature.Length);
				asn6 = this.decoder[1];
				asn7 = asn6.Element(0, 6);
				this.m_signaturealgo = ASN1Convert.ToOid(asn7);
				asn8 = asn6[1];
				if (asn8 != null)
				{
					this.m_signaturealgoparams = asn8.GetBytes();
				}
				else
				{
					this.m_signaturealgoparams = null;
				}
				ASN1 asn10 = asn.Element(num, 129);
				if (asn10 != null)
				{
					num++;
					this.issuerUniqueID = asn10.Value;
				}
				ASN1 asn11 = asn.Element(num, 130);
				if (asn11 != null)
				{
					num++;
					this.subjectUniqueID = asn11.Value;
				}
				ASN1 asn12 = asn.Element(num, 163);
				if (asn12 != null && asn12.Count == 1)
				{
					this.extensions = new X509ExtensionCollection(asn12[0]);
				}
				else
				{
					this.extensions = new X509ExtensionCollection(null);
				}
				this.m_encodedcert = (byte[])data.Clone();
			}
			catch (Exception inner)
			{
				throw new CryptographicException(X509Certificate.encoding_error, inner);
			}
		}

		// Token: 0x060003AD RID: 941 RVA: 0x00015C54 File Offset: 0x00013E54
		public X509Certificate(byte[] data)
		{
			if (data != null)
			{
				if (data.Length != 0 && data[0] != 48)
				{
					try
					{
						data = X509Certificate.PEM("CERTIFICATE", data);
					}
					catch (Exception inner)
					{
						throw new CryptographicException(X509Certificate.encoding_error, inner);
					}
				}
				this.Parse(data);
			}
		}

		// Token: 0x060003AE RID: 942 RVA: 0x00015CA8 File Offset: 0x00013EA8
		private byte[] GetUnsignedBigInteger(byte[] integer)
		{
			if (integer[0] == 0)
			{
				int num = integer.Length - 1;
				byte[] array = new byte[num];
				Buffer.BlockCopy(integer, 1, array, 0, num);
				return array;
			}
			return integer;
		}

		// Token: 0x17000096 RID: 150
		// (get) Token: 0x060003AF RID: 943 RVA: 0x00015CD4 File Offset: 0x00013ED4
		// (set) Token: 0x060003B0 RID: 944 RVA: 0x00015E0E File Offset: 0x0001400E
		public DSA DSA
		{
			get
			{
				if (this.m_keyalgoparams == null)
				{
					throw new CryptographicException("Missing key algorithm parameters.");
				}
				if (this._dsa == null && this.m_keyalgo == "1.2.840.10040.4.1")
				{
					DSAParameters dsaparameters = default(DSAParameters);
					ASN1 asn = new ASN1(this.m_publickey);
					if (asn == null || asn.Tag != 2)
					{
						return null;
					}
					dsaparameters.Y = this.GetUnsignedBigInteger(asn.Value);
					ASN1 asn2 = new ASN1(this.m_keyalgoparams);
					if (asn2 == null || asn2.Tag != 48 || asn2.Count < 3)
					{
						return null;
					}
					if (asn2[0].Tag != 2 || asn2[1].Tag != 2 || asn2[2].Tag != 2)
					{
						return null;
					}
					dsaparameters.P = this.GetUnsignedBigInteger(asn2[0].Value);
					dsaparameters.Q = this.GetUnsignedBigInteger(asn2[1].Value);
					dsaparameters.G = this.GetUnsignedBigInteger(asn2[2].Value);
					this._dsa = new DSACryptoServiceProvider(dsaparameters.Y.Length << 3);
					this._dsa.ImportParameters(dsaparameters);
				}
				return this._dsa;
			}
			set
			{
				this._dsa = value;
				if (value != null)
				{
					this._rsa = null;
				}
			}
		}

		// Token: 0x17000097 RID: 151
		// (get) Token: 0x060003B1 RID: 945 RVA: 0x00015E21 File Offset: 0x00014021
		public X509ExtensionCollection Extensions
		{
			get
			{
				return this.extensions;
			}
		}

		// Token: 0x17000098 RID: 152
		// (get) Token: 0x060003B2 RID: 946 RVA: 0x00015E2C File Offset: 0x0001402C
		public byte[] Hash
		{
			get
			{
				if (this.certhash == null)
				{
					if (this.decoder == null || this.decoder.Count < 1)
					{
						return null;
					}
					string text = PKCS1.HashNameFromOid(this.m_signaturealgo, false);
					if (text == null)
					{
						return null;
					}
					byte[] bytes = this.decoder[0].GetBytes();
					using (HashAlgorithm hashAlgorithm = PKCS1.CreateFromName(text))
					{
						this.certhash = hashAlgorithm.ComputeHash(bytes, 0, bytes.Length);
					}
				}
				return (byte[])this.certhash.Clone();
			}
		}

		// Token: 0x17000099 RID: 153
		// (get) Token: 0x060003B3 RID: 947 RVA: 0x00015EC4 File Offset: 0x000140C4
		public virtual string IssuerName
		{
			get
			{
				return this.m_issuername;
			}
		}

		// Token: 0x1700009A RID: 154
		// (get) Token: 0x060003B4 RID: 948 RVA: 0x00015ECC File Offset: 0x000140CC
		public virtual string KeyAlgorithm
		{
			get
			{
				return this.m_keyalgo;
			}
		}

		// Token: 0x1700009B RID: 155
		// (get) Token: 0x060003B5 RID: 949 RVA: 0x00015ED4 File Offset: 0x000140D4
		// (set) Token: 0x060003B6 RID: 950 RVA: 0x00015EF0 File Offset: 0x000140F0
		public virtual byte[] KeyAlgorithmParameters
		{
			get
			{
				if (this.m_keyalgoparams == null)
				{
					return null;
				}
				return (byte[])this.m_keyalgoparams.Clone();
			}
			set
			{
				this.m_keyalgoparams = value;
			}
		}

		// Token: 0x1700009C RID: 156
		// (get) Token: 0x060003B7 RID: 951 RVA: 0x00015EF9 File Offset: 0x000140F9
		public virtual byte[] PublicKey
		{
			get
			{
				if (this.m_publickey == null)
				{
					return null;
				}
				return (byte[])this.m_publickey.Clone();
			}
		}

		// Token: 0x1700009D RID: 157
		// (get) Token: 0x060003B8 RID: 952 RVA: 0x00015F18 File Offset: 0x00014118
		// (set) Token: 0x060003B9 RID: 953 RVA: 0x00015FCD File Offset: 0x000141CD
		public virtual RSA RSA
		{
			get
			{
				if (this._rsa == null && this.m_keyalgo == "1.2.840.113549.1.1.1")
				{
					RSAParameters rsaparameters = default(RSAParameters);
					ASN1 asn = new ASN1(this.m_publickey);
					ASN1 asn2 = asn[0];
					if (asn2 == null || asn2.Tag != 2)
					{
						return null;
					}
					ASN1 asn3 = asn[1];
					if (asn3.Tag != 2)
					{
						return null;
					}
					rsaparameters.Modulus = this.GetUnsignedBigInteger(asn2.Value);
					rsaparameters.Exponent = asn3.Value;
					int dwKeySize = rsaparameters.Modulus.Length << 3;
					this._rsa = new RSACryptoServiceProvider(dwKeySize);
					this._rsa.ImportParameters(rsaparameters);
				}
				return this._rsa;
			}
			set
			{
				if (value != null)
				{
					this._dsa = null;
				}
				this._rsa = value;
			}
		}

		// Token: 0x1700009E RID: 158
		// (get) Token: 0x060003BA RID: 954 RVA: 0x00015FE0 File Offset: 0x000141E0
		public virtual byte[] RawData
		{
			get
			{
				if (this.m_encodedcert == null)
				{
					return null;
				}
				return (byte[])this.m_encodedcert.Clone();
			}
		}

		// Token: 0x1700009F RID: 159
		// (get) Token: 0x060003BB RID: 955 RVA: 0x00015FFC File Offset: 0x000141FC
		public virtual byte[] SerialNumber
		{
			get
			{
				if (this.serialnumber == null)
				{
					return null;
				}
				return (byte[])this.serialnumber.Clone();
			}
		}

		// Token: 0x170000A0 RID: 160
		// (get) Token: 0x060003BC RID: 956 RVA: 0x00016018 File Offset: 0x00014218
		public virtual byte[] Signature
		{
			get
			{
				if (this.signature == null)
				{
					return null;
				}
				string signaturealgo = this.m_signaturealgo;
				uint num = <PrivateImplementationDetails>.ComputeStringHash(signaturealgo);
				if (num <= 719034781U)
				{
					if (num <= 601591448U)
					{
						if (num != 510574318U)
						{
							if (num != 601591448U)
							{
								goto IL_21C;
							}
							if (!(signaturealgo == "1.2.840.113549.1.1.5"))
							{
								goto IL_21C;
							}
						}
						else
						{
							if (!(signaturealgo == "1.2.840.10040.4.3"))
							{
								goto IL_21C;
							}
							ASN1 asn = new ASN1(this.signature);
							if (asn == null || asn.Count != 2)
							{
								return null;
							}
							byte[] value = asn[0].Value;
							byte[] value2 = asn[1].Value;
							byte[] array = new byte[40];
							int num2 = Math.Max(0, value.Length - 20);
							int dstOffset = Math.Max(0, 20 - value.Length);
							Buffer.BlockCopy(value, num2, array, dstOffset, value.Length - num2);
							int num3 = Math.Max(0, value2.Length - 20);
							int dstOffset2 = Math.Max(20, 40 - value2.Length);
							Buffer.BlockCopy(value2, num3, array, dstOffset2, value2.Length - num3);
							return array;
						}
					}
					else if (num != 618369067U)
					{
						if (num != 702257162U)
						{
							if (num != 719034781U)
							{
								goto IL_21C;
							}
							if (!(signaturealgo == "1.2.840.113549.1.1.2"))
							{
								goto IL_21C;
							}
						}
						else if (!(signaturealgo == "1.2.840.113549.1.1.3"))
						{
							goto IL_21C;
						}
					}
					else if (!(signaturealgo == "1.2.840.113549.1.1.4"))
					{
						goto IL_21C;
					}
				}
				else if (num <= 2477476687U)
				{
					if (num != 875536856U)
					{
						if (num != 2477476687U)
						{
							goto IL_21C;
						}
						if (!(signaturealgo == "1.2.840.113549.1.1.11"))
						{
							goto IL_21C;
						}
					}
					else if (!(signaturealgo == "1.3.14.3.2.29"))
					{
						goto IL_21C;
					}
				}
				else if (num != 2494254306U)
				{
					if (num != 2511031925U)
					{
						if (num != 3493391575U)
						{
							goto IL_21C;
						}
						if (!(signaturealgo == "1.3.36.3.3.1.2"))
						{
							goto IL_21C;
						}
					}
					else if (!(signaturealgo == "1.2.840.113549.1.1.13"))
					{
						goto IL_21C;
					}
				}
				else if (!(signaturealgo == "1.2.840.113549.1.1.12"))
				{
					goto IL_21C;
				}
				return (byte[])this.signature.Clone();
				IL_21C:
				throw new CryptographicException("Unsupported hash algorithm: " + this.m_signaturealgo);
			}
		}

		// Token: 0x170000A1 RID: 161
		// (get) Token: 0x060003BD RID: 957 RVA: 0x00016256 File Offset: 0x00014456
		public virtual string SignatureAlgorithm
		{
			get
			{
				return this.m_signaturealgo;
			}
		}

		// Token: 0x170000A2 RID: 162
		// (get) Token: 0x060003BE RID: 958 RVA: 0x0001625E File Offset: 0x0001445E
		public virtual byte[] SignatureAlgorithmParameters
		{
			get
			{
				if (this.m_signaturealgoparams == null)
				{
					return this.m_signaturealgoparams;
				}
				return (byte[])this.m_signaturealgoparams.Clone();
			}
		}

		// Token: 0x170000A3 RID: 163
		// (get) Token: 0x060003BF RID: 959 RVA: 0x0001627F File Offset: 0x0001447F
		public virtual string SubjectName
		{
			get
			{
				return this.m_subject;
			}
		}

		// Token: 0x170000A4 RID: 164
		// (get) Token: 0x060003C0 RID: 960 RVA: 0x00016287 File Offset: 0x00014487
		public virtual DateTime ValidFrom
		{
			get
			{
				return this.m_from;
			}
		}

		// Token: 0x170000A5 RID: 165
		// (get) Token: 0x060003C1 RID: 961 RVA: 0x0001628F File Offset: 0x0001448F
		public virtual DateTime ValidUntil
		{
			get
			{
				return this.m_until;
			}
		}

		// Token: 0x170000A6 RID: 166
		// (get) Token: 0x060003C2 RID: 962 RVA: 0x00016297 File Offset: 0x00014497
		public int Version
		{
			get
			{
				return this.version;
			}
		}

		// Token: 0x170000A7 RID: 167
		// (get) Token: 0x060003C3 RID: 963 RVA: 0x0001629F File Offset: 0x0001449F
		public bool IsCurrent
		{
			get
			{
				return this.WasCurrent(DateTime.UtcNow);
			}
		}

		// Token: 0x060003C4 RID: 964 RVA: 0x000162AC File Offset: 0x000144AC
		public bool WasCurrent(DateTime instant)
		{
			return instant > this.ValidFrom && instant <= this.ValidUntil;
		}

		// Token: 0x170000A8 RID: 168
		// (get) Token: 0x060003C5 RID: 965 RVA: 0x000162CA File Offset: 0x000144CA
		public byte[] IssuerUniqueIdentifier
		{
			get
			{
				if (this.issuerUniqueID == null)
				{
					return null;
				}
				return (byte[])this.issuerUniqueID.Clone();
			}
		}

		// Token: 0x170000A9 RID: 169
		// (get) Token: 0x060003C6 RID: 966 RVA: 0x000162E6 File Offset: 0x000144E6
		public byte[] SubjectUniqueIdentifier
		{
			get
			{
				if (this.subjectUniqueID == null)
				{
					return null;
				}
				return (byte[])this.subjectUniqueID.Clone();
			}
		}

		// Token: 0x060003C7 RID: 967 RVA: 0x00016302 File Offset: 0x00014502
		internal bool VerifySignature(DSA dsa)
		{
			DSASignatureDeformatter dsasignatureDeformatter = new DSASignatureDeformatter(dsa);
			dsasignatureDeformatter.SetHashAlgorithm("SHA1");
			return dsasignatureDeformatter.VerifySignature(this.Hash, this.Signature);
		}

		// Token: 0x060003C8 RID: 968 RVA: 0x00016326 File Offset: 0x00014526
		internal bool VerifySignature(RSA rsa)
		{
			if (this.m_signaturealgo == "1.2.840.10040.4.3")
			{
				return false;
			}
			RSAPKCS1SignatureDeformatter rsapkcs1SignatureDeformatter = new RSAPKCS1SignatureDeformatter(rsa);
			rsapkcs1SignatureDeformatter.SetHashAlgorithm(PKCS1.HashNameFromOid(this.m_signaturealgo, true));
			return rsapkcs1SignatureDeformatter.VerifySignature(this.Hash, this.Signature);
		}

		// Token: 0x060003C9 RID: 969 RVA: 0x00016368 File Offset: 0x00014568
		public bool VerifySignature(AsymmetricAlgorithm aa)
		{
			if (aa == null)
			{
				throw new ArgumentNullException("aa");
			}
			if (aa is RSA)
			{
				return this.VerifySignature(aa as RSA);
			}
			if (aa is DSA)
			{
				return this.VerifySignature(aa as DSA);
			}
			throw new NotSupportedException("Unknown Asymmetric Algorithm " + aa.ToString());
		}

		// Token: 0x060003CA RID: 970 RVA: 0x000163C2 File Offset: 0x000145C2
		public bool CheckSignature(byte[] hash, string hashAlgorithm, byte[] signature)
		{
			return ((RSACryptoServiceProvider)this.RSA).VerifyHash(hash, hashAlgorithm, signature);
		}

		// Token: 0x170000AA RID: 170
		// (get) Token: 0x060003CB RID: 971 RVA: 0x000163D8 File Offset: 0x000145D8
		public bool IsSelfSigned
		{
			get
			{
				if (this.m_issuername != this.m_subject)
				{
					return false;
				}
				bool result;
				try
				{
					if (this.RSA != null)
					{
						result = this.VerifySignature(this.RSA);
					}
					else if (this.DSA != null)
					{
						result = this.VerifySignature(this.DSA);
					}
					else
					{
						result = false;
					}
				}
				catch (CryptographicException)
				{
					result = false;
				}
				return result;
			}
		}

		// Token: 0x060003CC RID: 972 RVA: 0x00016444 File Offset: 0x00014644
		public ASN1 GetIssuerName()
		{
			return this.issuer;
		}

		// Token: 0x060003CD RID: 973 RVA: 0x0001644C File Offset: 0x0001464C
		public ASN1 GetSubjectName()
		{
			return this.subject;
		}

		// Token: 0x060003CE RID: 974 RVA: 0x00016454 File Offset: 0x00014654
		protected X509Certificate(SerializationInfo info, StreamingContext context)
		{
			this.Parse((byte[])info.GetValue("raw", typeof(byte[])));
		}

		// Token: 0x060003CF RID: 975 RVA: 0x0001647C File Offset: 0x0001467C
		[SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
		public virtual void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("raw", this.m_encodedcert);
		}

		// Token: 0x060003D0 RID: 976 RVA: 0x00016490 File Offset: 0x00014690
		private static byte[] PEM(string type, byte[] data)
		{
			string @string = Encoding.ASCII.GetString(data);
			string text = string.Format("-----BEGIN {0}-----", type);
			string value = string.Format("-----END {0}-----", type);
			int num = @string.IndexOf(text) + text.Length;
			int num2 = @string.IndexOf(value, num);
			return Convert.FromBase64String(@string.Substring(num, num2 - num));
		}

		// Token: 0x060003D1 RID: 977 RVA: 0x000164E6 File Offset: 0x000146E6
		// Note: this type is marked as 'beforefieldinit'.
		static X509Certificate()
		{
		}

		// Token: 0x04000569 RID: 1385
		private ASN1 decoder;

		// Token: 0x0400056A RID: 1386
		private byte[] m_encodedcert;

		// Token: 0x0400056B RID: 1387
		private DateTime m_from;

		// Token: 0x0400056C RID: 1388
		private DateTime m_until;

		// Token: 0x0400056D RID: 1389
		private ASN1 issuer;

		// Token: 0x0400056E RID: 1390
		private string m_issuername;

		// Token: 0x0400056F RID: 1391
		private string m_keyalgo;

		// Token: 0x04000570 RID: 1392
		private byte[] m_keyalgoparams;

		// Token: 0x04000571 RID: 1393
		private ASN1 subject;

		// Token: 0x04000572 RID: 1394
		private string m_subject;

		// Token: 0x04000573 RID: 1395
		private byte[] m_publickey;

		// Token: 0x04000574 RID: 1396
		private byte[] signature;

		// Token: 0x04000575 RID: 1397
		private string m_signaturealgo;

		// Token: 0x04000576 RID: 1398
		private byte[] m_signaturealgoparams;

		// Token: 0x04000577 RID: 1399
		private byte[] certhash;

		// Token: 0x04000578 RID: 1400
		private RSA _rsa;

		// Token: 0x04000579 RID: 1401
		private DSA _dsa;

		// Token: 0x0400057A RID: 1402
		private const string OID_DSA = "1.2.840.10040.4.1";

		// Token: 0x0400057B RID: 1403
		private const string OID_RSA = "1.2.840.113549.1.1.1";

		// Token: 0x0400057C RID: 1404
		private int version;

		// Token: 0x0400057D RID: 1405
		private byte[] serialnumber;

		// Token: 0x0400057E RID: 1406
		private byte[] issuerUniqueID;

		// Token: 0x0400057F RID: 1407
		private byte[] subjectUniqueID;

		// Token: 0x04000580 RID: 1408
		private X509ExtensionCollection extensions;

		// Token: 0x04000581 RID: 1409
		private static string encoding_error = Locale.GetText("Input data cannot be coded as a valid certificate.");
	}
}
