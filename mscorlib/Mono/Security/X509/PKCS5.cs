﻿using System;

namespace Mono.Security.X509
{
	// Token: 0x02000069 RID: 105
	internal class PKCS5
	{
		// Token: 0x06000334 RID: 820 RVA: 0x00002050 File Offset: 0x00000250
		public PKCS5()
		{
		}

		// Token: 0x0400051C RID: 1308
		public const string pbeWithMD2AndDESCBC = "1.2.840.113549.1.5.1";

		// Token: 0x0400051D RID: 1309
		public const string pbeWithMD5AndDESCBC = "1.2.840.113549.1.5.3";

		// Token: 0x0400051E RID: 1310
		public const string pbeWithMD2AndRC2CBC = "1.2.840.113549.1.5.4";

		// Token: 0x0400051F RID: 1311
		public const string pbeWithMD5AndRC2CBC = "1.2.840.113549.1.5.6";

		// Token: 0x04000520 RID: 1312
		public const string pbeWithSHA1AndDESCBC = "1.2.840.113549.1.5.10";

		// Token: 0x04000521 RID: 1313
		public const string pbeWithSHA1AndRC2CBC = "1.2.840.113549.1.5.11";
	}
}
