﻿using System;

namespace Mono.Security.X509
{
	// Token: 0x02000076 RID: 118
	[Flags]
	[Serializable]
	internal enum X509ChainStatusFlags
	{
		// Token: 0x04000589 RID: 1417
		InvalidBasicConstraints = 1024,
		// Token: 0x0400058A RID: 1418
		NoError = 0,
		// Token: 0x0400058B RID: 1419
		NotSignatureValid = 8,
		// Token: 0x0400058C RID: 1420
		NotTimeNested = 2,
		// Token: 0x0400058D RID: 1421
		NotTimeValid = 1,
		// Token: 0x0400058E RID: 1422
		PartialChain = 65536,
		// Token: 0x0400058F RID: 1423
		UntrustedRoot = 32
	}
}
