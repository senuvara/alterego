﻿using System;
using System.Security.Permissions;
using Mono.Security.X509.Extensions;

namespace Mono.Security.X509
{
	// Token: 0x02000075 RID: 117
	internal class X509Chain
	{
		// Token: 0x060003EA RID: 1002 RVA: 0x00016721 File Offset: 0x00014921
		public X509Chain()
		{
			this.certs = new X509CertificateCollection();
		}

		// Token: 0x060003EB RID: 1003 RVA: 0x00016734 File Offset: 0x00014934
		public X509Chain(X509CertificateCollection chain) : this()
		{
			this._chain = new X509CertificateCollection();
			this._chain.AddRange(chain);
		}

		// Token: 0x170000AE RID: 174
		// (get) Token: 0x060003EC RID: 1004 RVA: 0x00016753 File Offset: 0x00014953
		public X509CertificateCollection Chain
		{
			get
			{
				return this._chain;
			}
		}

		// Token: 0x170000AF RID: 175
		// (get) Token: 0x060003ED RID: 1005 RVA: 0x0001675B File Offset: 0x0001495B
		public X509Certificate Root
		{
			get
			{
				return this._root;
			}
		}

		// Token: 0x170000B0 RID: 176
		// (get) Token: 0x060003EE RID: 1006 RVA: 0x00016763 File Offset: 0x00014963
		public X509ChainStatusFlags Status
		{
			get
			{
				return this._status;
			}
		}

		// Token: 0x170000B1 RID: 177
		// (get) Token: 0x060003EF RID: 1007 RVA: 0x0001676B File Offset: 0x0001496B
		// (set) Token: 0x060003F0 RID: 1008 RVA: 0x0001679D File Offset: 0x0001499D
		public X509CertificateCollection TrustAnchors
		{
			get
			{
				if (this.roots == null)
				{
					this.roots = new X509CertificateCollection();
					this.roots.AddRange(X509StoreManager.TrustedRootCertificates);
					return this.roots;
				}
				return this.roots;
			}
			[SecurityPermission(SecurityAction.Demand, Flags = SecurityPermissionFlag.ControlPolicy)]
			set
			{
				this.roots = value;
			}
		}

		// Token: 0x060003F1 RID: 1009 RVA: 0x000167A6 File Offset: 0x000149A6
		public void LoadCertificate(X509Certificate x509)
		{
			this.certs.Add(x509);
		}

		// Token: 0x060003F2 RID: 1010 RVA: 0x000167B5 File Offset: 0x000149B5
		public void LoadCertificates(X509CertificateCollection collection)
		{
			this.certs.AddRange(collection);
		}

		// Token: 0x060003F3 RID: 1011 RVA: 0x000167C4 File Offset: 0x000149C4
		public X509Certificate FindByIssuerName(string issuerName)
		{
			foreach (X509Certificate x509Certificate in this.certs)
			{
				if (x509Certificate.IssuerName == issuerName)
				{
					return x509Certificate;
				}
			}
			return null;
		}

		// Token: 0x060003F4 RID: 1012 RVA: 0x00016828 File Offset: 0x00014A28
		public bool Build(X509Certificate leaf)
		{
			this._status = X509ChainStatusFlags.NoError;
			if (this._chain == null)
			{
				this._chain = new X509CertificateCollection();
				X509Certificate x509Certificate = leaf;
				X509Certificate potentialRoot = x509Certificate;
				while (x509Certificate != null && !x509Certificate.IsSelfSigned)
				{
					potentialRoot = x509Certificate;
					this._chain.Add(x509Certificate);
					x509Certificate = this.FindCertificateParent(x509Certificate);
				}
				this._root = this.FindCertificateRoot(potentialRoot);
			}
			else
			{
				int count = this._chain.Count;
				if (count > 0)
				{
					if (this.IsParent(leaf, this._chain[0]))
					{
						int num = 1;
						while (num < count && this.IsParent(this._chain[num - 1], this._chain[num]))
						{
							num++;
						}
						if (num == count)
						{
							this._root = this.FindCertificateRoot(this._chain[count - 1]);
						}
					}
				}
				else
				{
					this._root = this.FindCertificateRoot(leaf);
				}
			}
			if (this._chain != null && this._status == X509ChainStatusFlags.NoError)
			{
				foreach (X509Certificate cert in this._chain)
				{
					if (!this.IsValid(cert))
					{
						return false;
					}
				}
				if (!this.IsValid(leaf))
				{
					if (this._status == X509ChainStatusFlags.NotTimeNested)
					{
						this._status = X509ChainStatusFlags.NotTimeValid;
					}
					return false;
				}
				if (this._root != null && !this.IsValid(this._root))
				{
					return false;
				}
			}
			return this._status == X509ChainStatusFlags.NoError;
		}

		// Token: 0x060003F5 RID: 1013 RVA: 0x000169B4 File Offset: 0x00014BB4
		public void Reset()
		{
			this._status = X509ChainStatusFlags.NoError;
			this.roots = null;
			this.certs.Clear();
			if (this._chain != null)
			{
				this._chain.Clear();
			}
		}

		// Token: 0x060003F6 RID: 1014 RVA: 0x000169E2 File Offset: 0x00014BE2
		private bool IsValid(X509Certificate cert)
		{
			if (!cert.IsCurrent)
			{
				this._status = X509ChainStatusFlags.NotTimeNested;
				return false;
			}
			return true;
		}

		// Token: 0x060003F7 RID: 1015 RVA: 0x000169F8 File Offset: 0x00014BF8
		private X509Certificate FindCertificateParent(X509Certificate child)
		{
			foreach (X509Certificate x509Certificate in this.certs)
			{
				if (this.IsParent(child, x509Certificate))
				{
					return x509Certificate;
				}
			}
			return null;
		}

		// Token: 0x060003F8 RID: 1016 RVA: 0x00016A58 File Offset: 0x00014C58
		private X509Certificate FindCertificateRoot(X509Certificate potentialRoot)
		{
			if (potentialRoot == null)
			{
				this._status = X509ChainStatusFlags.PartialChain;
				return null;
			}
			if (this.IsTrusted(potentialRoot))
			{
				return potentialRoot;
			}
			foreach (X509Certificate x509Certificate in this.TrustAnchors)
			{
				if (this.IsParent(potentialRoot, x509Certificate))
				{
					return x509Certificate;
				}
			}
			if (potentialRoot.IsSelfSigned)
			{
				this._status = X509ChainStatusFlags.UntrustedRoot;
				return potentialRoot;
			}
			this._status = X509ChainStatusFlags.PartialChain;
			return null;
		}

		// Token: 0x060003F9 RID: 1017 RVA: 0x00016AF0 File Offset: 0x00014CF0
		private bool IsTrusted(X509Certificate potentialTrusted)
		{
			return this.TrustAnchors.Contains(potentialTrusted);
		}

		// Token: 0x060003FA RID: 1018 RVA: 0x00016B00 File Offset: 0x00014D00
		private bool IsParent(X509Certificate child, X509Certificate parent)
		{
			if (child.IssuerName != parent.SubjectName)
			{
				return false;
			}
			if (parent.Version > 2 && !this.IsTrusted(parent))
			{
				X509Extension x509Extension = parent.Extensions["2.5.29.19"];
				if (x509Extension != null)
				{
					if (!new BasicConstraintsExtension(x509Extension).CertificateAuthority)
					{
						this._status = X509ChainStatusFlags.InvalidBasicConstraints;
					}
				}
				else
				{
					this._status = X509ChainStatusFlags.InvalidBasicConstraints;
				}
			}
			if (!child.VerifySignature(parent.RSA))
			{
				this._status = X509ChainStatusFlags.NotSignatureValid;
				return false;
			}
			return true;
		}

		// Token: 0x04000583 RID: 1411
		private X509CertificateCollection roots;

		// Token: 0x04000584 RID: 1412
		private X509CertificateCollection certs;

		// Token: 0x04000585 RID: 1413
		private X509Certificate _root;

		// Token: 0x04000586 RID: 1414
		private X509CertificateCollection _chain;

		// Token: 0x04000587 RID: 1415
		private X509ChainStatusFlags _status;
	}
}
