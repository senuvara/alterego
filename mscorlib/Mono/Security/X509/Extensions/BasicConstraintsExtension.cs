﻿using System;
using System.Globalization;
using System.Text;

namespace Mono.Security.X509.Extensions
{
	// Token: 0x02000090 RID: 144
	internal class BasicConstraintsExtension : X509Extension
	{
		// Token: 0x06000467 RID: 1127 RVA: 0x0001804E File Offset: 0x0001624E
		public BasicConstraintsExtension()
		{
			this.extnOid = "2.5.29.19";
			this.pathLenConstraint = -1;
		}

		// Token: 0x06000468 RID: 1128 RVA: 0x00018068 File Offset: 0x00016268
		public BasicConstraintsExtension(ASN1 asn1) : base(asn1)
		{
		}

		// Token: 0x06000469 RID: 1129 RVA: 0x00018071 File Offset: 0x00016271
		public BasicConstraintsExtension(X509Extension extension) : base(extension)
		{
		}

		// Token: 0x0600046A RID: 1130 RVA: 0x0001807C File Offset: 0x0001627C
		protected override void Decode()
		{
			this.cA = false;
			this.pathLenConstraint = -1;
			ASN1 asn = new ASN1(this.extnValue.Value);
			if (asn.Tag != 48)
			{
				throw new ArgumentException("Invalid BasicConstraints extension");
			}
			int num = 0;
			ASN1 asn2 = asn[num++];
			if (asn2 != null && asn2.Tag == 1)
			{
				this.cA = (asn2.Value[0] == byte.MaxValue);
				asn2 = asn[num++];
			}
			if (asn2 != null && asn2.Tag == 2)
			{
				this.pathLenConstraint = ASN1Convert.ToInt32(asn2);
			}
		}

		// Token: 0x0600046B RID: 1131 RVA: 0x00018110 File Offset: 0x00016310
		protected override void Encode()
		{
			ASN1 asn = new ASN1(48);
			if (this.cA)
			{
				asn.Add(new ASN1(1, new byte[]
				{
					byte.MaxValue
				}));
			}
			if (this.cA && this.pathLenConstraint >= 0)
			{
				asn.Add(ASN1Convert.FromInt32(this.pathLenConstraint));
			}
			this.extnValue = new ASN1(4);
			this.extnValue.Add(asn);
		}

		// Token: 0x170000D0 RID: 208
		// (get) Token: 0x0600046C RID: 1132 RVA: 0x00018184 File Offset: 0x00016384
		// (set) Token: 0x0600046D RID: 1133 RVA: 0x0001818C File Offset: 0x0001638C
		public bool CertificateAuthority
		{
			get
			{
				return this.cA;
			}
			set
			{
				this.cA = value;
			}
		}

		// Token: 0x170000D1 RID: 209
		// (get) Token: 0x0600046E RID: 1134 RVA: 0x00018195 File Offset: 0x00016395
		public override string Name
		{
			get
			{
				return "Basic Constraints";
			}
		}

		// Token: 0x170000D2 RID: 210
		// (get) Token: 0x0600046F RID: 1135 RVA: 0x0001819C File Offset: 0x0001639C
		// (set) Token: 0x06000470 RID: 1136 RVA: 0x000181A4 File Offset: 0x000163A4
		public int PathLenConstraint
		{
			get
			{
				return this.pathLenConstraint;
			}
			set
			{
				if (value < -1)
				{
					throw new ArgumentOutOfRangeException(Locale.GetText("PathLenConstraint must be positive or -1 for none ({0}).", new object[]
					{
						value
					}));
				}
				this.pathLenConstraint = value;
			}
		}

		// Token: 0x06000471 RID: 1137 RVA: 0x000181D0 File Offset: 0x000163D0
		public override string ToString()
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("Subject Type=");
			stringBuilder.Append(this.cA ? "CA" : "End Entity");
			stringBuilder.Append(Environment.NewLine);
			stringBuilder.Append("Path Length Constraint=");
			if (this.pathLenConstraint == -1)
			{
				stringBuilder.Append("None");
			}
			else
			{
				stringBuilder.Append(this.pathLenConstraint.ToString(CultureInfo.InvariantCulture));
			}
			stringBuilder.Append(Environment.NewLine);
			return stringBuilder.ToString();
		}

		// Token: 0x040005B2 RID: 1458
		public const int NoPathLengthConstraint = -1;

		// Token: 0x040005B3 RID: 1459
		private bool cA;

		// Token: 0x040005B4 RID: 1460
		private int pathLenConstraint;
	}
}
