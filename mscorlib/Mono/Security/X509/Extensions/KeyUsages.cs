﻿using System;

namespace Mono.Security.X509.Extensions
{
	// Token: 0x02000091 RID: 145
	[Flags]
	internal enum KeyUsages
	{
		// Token: 0x040005B6 RID: 1462
		digitalSignature = 128,
		// Token: 0x040005B7 RID: 1463
		nonRepudiation = 64,
		// Token: 0x040005B8 RID: 1464
		keyEncipherment = 32,
		// Token: 0x040005B9 RID: 1465
		dataEncipherment = 16,
		// Token: 0x040005BA RID: 1466
		keyAgreement = 8,
		// Token: 0x040005BB RID: 1467
		keyCertSign = 4,
		// Token: 0x040005BC RID: 1468
		cRLSign = 2,
		// Token: 0x040005BD RID: 1469
		encipherOnly = 1,
		// Token: 0x040005BE RID: 1470
		decipherOnly = 2048,
		// Token: 0x040005BF RID: 1471
		none = 0
	}
}
