﻿using System;
using System.Globalization;
using System.Text;

namespace Mono.Security.X509.Extensions
{
	// Token: 0x02000093 RID: 147
	internal class SubjectKeyIdentifierExtension : X509Extension
	{
		// Token: 0x0600047C RID: 1148 RVA: 0x000185C5 File Offset: 0x000167C5
		public SubjectKeyIdentifierExtension()
		{
			this.extnOid = "2.5.29.14";
		}

		// Token: 0x0600047D RID: 1149 RVA: 0x00018068 File Offset: 0x00016268
		public SubjectKeyIdentifierExtension(ASN1 asn1) : base(asn1)
		{
		}

		// Token: 0x0600047E RID: 1150 RVA: 0x00018071 File Offset: 0x00016271
		public SubjectKeyIdentifierExtension(X509Extension extension) : base(extension)
		{
		}

		// Token: 0x0600047F RID: 1151 RVA: 0x000185D8 File Offset: 0x000167D8
		protected override void Decode()
		{
			ASN1 asn = new ASN1(this.extnValue.Value);
			if (asn.Tag != 4)
			{
				throw new ArgumentException("Invalid SubjectKeyIdentifier extension");
			}
			this.ski = asn.Value;
		}

		// Token: 0x06000480 RID: 1152 RVA: 0x00018618 File Offset: 0x00016818
		protected override void Encode()
		{
			if (this.ski == null)
			{
				throw new InvalidOperationException("Invalid SubjectKeyIdentifier extension");
			}
			ASN1 asn = new ASN1(4, this.ski);
			this.extnValue = new ASN1(4);
			this.extnValue.Add(asn);
		}

		// Token: 0x170000D5 RID: 213
		// (get) Token: 0x06000481 RID: 1153 RVA: 0x0001865E File Offset: 0x0001685E
		public override string Name
		{
			get
			{
				return "Subject Key Identifier";
			}
		}

		// Token: 0x170000D6 RID: 214
		// (get) Token: 0x06000482 RID: 1154 RVA: 0x00018665 File Offset: 0x00016865
		// (set) Token: 0x06000483 RID: 1155 RVA: 0x00018681 File Offset: 0x00016881
		public byte[] Identifier
		{
			get
			{
				if (this.ski == null)
				{
					return null;
				}
				return (byte[])this.ski.Clone();
			}
			set
			{
				this.ski = value;
			}
		}

		// Token: 0x06000484 RID: 1156 RVA: 0x0001868C File Offset: 0x0001688C
		public override string ToString()
		{
			if (this.ski == null)
			{
				return null;
			}
			StringBuilder stringBuilder = new StringBuilder();
			for (int i = 0; i < this.ski.Length; i++)
			{
				stringBuilder.Append(this.ski[i].ToString("X2", CultureInfo.InvariantCulture));
				if (i % 2 == 1)
				{
					stringBuilder.Append(" ");
				}
			}
			return stringBuilder.ToString();
		}

		// Token: 0x040005C1 RID: 1473
		private byte[] ski;
	}
}
