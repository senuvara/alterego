﻿using System;
using System.Globalization;
using System.Text;

namespace Mono.Security.X509.Extensions
{
	// Token: 0x02000092 RID: 146
	internal class KeyUsageExtension : X509Extension
	{
		// Token: 0x06000472 RID: 1138 RVA: 0x00018068 File Offset: 0x00016268
		public KeyUsageExtension(ASN1 asn1) : base(asn1)
		{
		}

		// Token: 0x06000473 RID: 1139 RVA: 0x00018071 File Offset: 0x00016271
		public KeyUsageExtension(X509Extension extension) : base(extension)
		{
		}

		// Token: 0x06000474 RID: 1140 RVA: 0x00018262 File Offset: 0x00016462
		public KeyUsageExtension()
		{
			this.extnOid = "2.5.29.15";
		}

		// Token: 0x06000475 RID: 1141 RVA: 0x00018278 File Offset: 0x00016478
		protected override void Decode()
		{
			ASN1 asn = new ASN1(this.extnValue.Value);
			if (asn.Tag != 3)
			{
				throw new ArgumentException("Invalid KeyUsage extension");
			}
			int i = 1;
			while (i < asn.Value.Length)
			{
				this.kubits = (this.kubits << 8) + (int)asn.Value[i++];
			}
		}

		// Token: 0x06000476 RID: 1142 RVA: 0x000182D4 File Offset: 0x000164D4
		protected override void Encode()
		{
			this.extnValue = new ASN1(4);
			ushort num = (ushort)this.kubits;
			if (num <= 0)
			{
				ASN1 extnValue = this.extnValue;
				byte tag = 3;
				byte[] array = new byte[2];
				array[0] = 7;
				extnValue.Add(new ASN1(tag, array));
				return;
			}
			byte b = 15;
			while (b > 0 && (num & 32768) != 32768)
			{
				num = (ushort)(num << 1);
				b -= 1;
			}
			if (this.kubits > 255)
			{
				b -= 8;
				this.extnValue.Add(new ASN1(3, new byte[]
				{
					b,
					(byte)this.kubits,
					(byte)(this.kubits >> 8)
				}));
				return;
			}
			this.extnValue.Add(new ASN1(3, new byte[]
			{
				b,
				(byte)this.kubits
			}));
		}

		// Token: 0x170000D3 RID: 211
		// (get) Token: 0x06000477 RID: 1143 RVA: 0x000183A8 File Offset: 0x000165A8
		// (set) Token: 0x06000478 RID: 1144 RVA: 0x000183B0 File Offset: 0x000165B0
		public KeyUsages KeyUsage
		{
			get
			{
				return (KeyUsages)this.kubits;
			}
			set
			{
				this.kubits = Convert.ToInt32(value, CultureInfo.InvariantCulture);
			}
		}

		// Token: 0x170000D4 RID: 212
		// (get) Token: 0x06000479 RID: 1145 RVA: 0x000183C8 File Offset: 0x000165C8
		public override string Name
		{
			get
			{
				return "Key Usage";
			}
		}

		// Token: 0x0600047A RID: 1146 RVA: 0x000183D0 File Offset: 0x000165D0
		public bool Support(KeyUsages usage)
		{
			int num = Convert.ToInt32(usage, CultureInfo.InvariantCulture);
			return (num & this.kubits) == num;
		}

		// Token: 0x0600047B RID: 1147 RVA: 0x000183FC File Offset: 0x000165FC
		public override string ToString()
		{
			StringBuilder stringBuilder = new StringBuilder();
			if (this.Support(KeyUsages.digitalSignature))
			{
				stringBuilder.Append("Digital Signature");
			}
			if (this.Support(KeyUsages.nonRepudiation))
			{
				if (stringBuilder.Length > 0)
				{
					stringBuilder.Append(" , ");
				}
				stringBuilder.Append("Non-Repudiation");
			}
			if (this.Support(KeyUsages.keyEncipherment))
			{
				if (stringBuilder.Length > 0)
				{
					stringBuilder.Append(" , ");
				}
				stringBuilder.Append("Key Encipherment");
			}
			if (this.Support(KeyUsages.dataEncipherment))
			{
				if (stringBuilder.Length > 0)
				{
					stringBuilder.Append(" , ");
				}
				stringBuilder.Append("Data Encipherment");
			}
			if (this.Support(KeyUsages.keyAgreement))
			{
				if (stringBuilder.Length > 0)
				{
					stringBuilder.Append(" , ");
				}
				stringBuilder.Append("Key Agreement");
			}
			if (this.Support(KeyUsages.keyCertSign))
			{
				if (stringBuilder.Length > 0)
				{
					stringBuilder.Append(" , ");
				}
				stringBuilder.Append("Certificate Signing");
			}
			if (this.Support(KeyUsages.cRLSign))
			{
				if (stringBuilder.Length > 0)
				{
					stringBuilder.Append(" , ");
				}
				stringBuilder.Append("CRL Signing");
			}
			if (this.Support(KeyUsages.encipherOnly))
			{
				if (stringBuilder.Length > 0)
				{
					stringBuilder.Append(" , ");
				}
				stringBuilder.Append("Encipher Only ");
			}
			if (this.Support(KeyUsages.decipherOnly))
			{
				if (stringBuilder.Length > 0)
				{
					stringBuilder.Append(" , ");
				}
				stringBuilder.Append("Decipher Only");
			}
			stringBuilder.Append("(");
			stringBuilder.Append(this.kubits.ToString("X2", CultureInfo.InvariantCulture));
			stringBuilder.Append(")");
			stringBuilder.Append(Environment.NewLine);
			return stringBuilder.ToString();
		}

		// Token: 0x040005C0 RID: 1472
		private int kubits;
	}
}
