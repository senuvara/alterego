﻿using System;
using System.Collections;
using System.Globalization;
using System.IO;
using System.Text;
using Mono.Security.X509.Extensions;

namespace Mono.Security.X509
{
	// Token: 0x02000079 RID: 121
	internal class X509Store
	{
		// Token: 0x0600041C RID: 1052 RVA: 0x000172A0 File Offset: 0x000154A0
		internal X509Store(string path, bool crl, bool newFormat)
		{
			this._storePath = path;
			this._crl = crl;
			this._newFormat = newFormat;
		}

		// Token: 0x170000B9 RID: 185
		// (get) Token: 0x0600041D RID: 1053 RVA: 0x000172BD File Offset: 0x000154BD
		public X509CertificateCollection Certificates
		{
			get
			{
				if (this._certificates == null)
				{
					this._certificates = this.BuildCertificatesCollection(this._storePath);
				}
				return this._certificates;
			}
		}

		// Token: 0x170000BA RID: 186
		// (get) Token: 0x0600041E RID: 1054 RVA: 0x000172DF File Offset: 0x000154DF
		public ArrayList Crls
		{
			get
			{
				if (!this._crl)
				{
					this._crls = new ArrayList();
				}
				if (this._crls == null)
				{
					this._crls = this.BuildCrlsCollection(this._storePath);
				}
				return this._crls;
			}
		}

		// Token: 0x170000BB RID: 187
		// (get) Token: 0x0600041F RID: 1055 RVA: 0x00017314 File Offset: 0x00015514
		public string Name
		{
			get
			{
				if (this._name == null)
				{
					int num = this._storePath.LastIndexOf(Path.DirectorySeparatorChar);
					this._name = this._storePath.Substring(num + 1);
				}
				return this._name;
			}
		}

		// Token: 0x06000420 RID: 1056 RVA: 0x00017354 File Offset: 0x00015554
		public void Clear()
		{
			this.ClearCertificates();
			this.ClearCrls();
		}

		// Token: 0x06000421 RID: 1057 RVA: 0x00017362 File Offset: 0x00015562
		private void ClearCertificates()
		{
			if (this._certificates != null)
			{
				this._certificates.Clear();
			}
			this._certificates = null;
		}

		// Token: 0x06000422 RID: 1058 RVA: 0x0001737E File Offset: 0x0001557E
		private void ClearCrls()
		{
			if (this._crls != null)
			{
				this._crls.Clear();
			}
			this._crls = null;
		}

		// Token: 0x06000423 RID: 1059 RVA: 0x0001739C File Offset: 0x0001559C
		public void Import(X509Certificate certificate)
		{
			this.CheckStore(this._storePath, true);
			if (this._newFormat)
			{
				this.ImportNewFormat(certificate);
				return;
			}
			string text = Path.Combine(this._storePath, this.GetUniqueName(certificate, null));
			if (!File.Exists(text))
			{
				text = Path.Combine(this._storePath, this.GetUniqueNameWithSerial(certificate));
				if (!File.Exists(text))
				{
					using (FileStream fileStream = File.Create(text))
					{
						byte[] rawData = certificate.RawData;
						fileStream.Write(rawData, 0, rawData.Length);
						fileStream.Close();
					}
					this.ClearCertificates();
					return;
				}
			}
			else
			{
				string path = Path.Combine(this._storePath, this.GetUniqueNameWithSerial(certificate));
				if (this.GetUniqueNameWithSerial(this.LoadCertificate(text)) != this.GetUniqueNameWithSerial(certificate))
				{
					using (FileStream fileStream2 = File.Create(path))
					{
						byte[] rawData2 = certificate.RawData;
						fileStream2.Write(rawData2, 0, rawData2.Length);
						fileStream2.Close();
					}
					this.ClearCertificates();
				}
			}
		}

		// Token: 0x06000424 RID: 1060 RVA: 0x000174B8 File Offset: 0x000156B8
		public void Import(X509Crl crl)
		{
			this.CheckStore(this._storePath, true);
			if (this._newFormat)
			{
				throw new NotSupportedException();
			}
			string path = Path.Combine(this._storePath, this.GetUniqueName(crl));
			if (!File.Exists(path))
			{
				using (FileStream fileStream = File.Create(path))
				{
					byte[] rawData = crl.RawData;
					fileStream.Write(rawData, 0, rawData.Length);
				}
				this.ClearCrls();
			}
		}

		// Token: 0x06000425 RID: 1061 RVA: 0x00017538 File Offset: 0x00015738
		public void Remove(X509Certificate certificate)
		{
			if (this._newFormat)
			{
				this.RemoveNewFormat(certificate);
				return;
			}
			string path = Path.Combine(this._storePath, this.GetUniqueNameWithSerial(certificate));
			if (File.Exists(path))
			{
				File.Delete(path);
				this.ClearCertificates();
				return;
			}
			path = Path.Combine(this._storePath, this.GetUniqueName(certificate, null));
			if (File.Exists(path))
			{
				File.Delete(path);
				this.ClearCertificates();
			}
		}

		// Token: 0x06000426 RID: 1062 RVA: 0x000175A8 File Offset: 0x000157A8
		public void Remove(X509Crl crl)
		{
			if (this._newFormat)
			{
				throw new NotSupportedException();
			}
			string path = Path.Combine(this._storePath, this.GetUniqueName(crl));
			if (File.Exists(path))
			{
				File.Delete(path);
				this.ClearCrls();
			}
		}

		// Token: 0x06000427 RID: 1063 RVA: 0x000175EA File Offset: 0x000157EA
		private void ImportNewFormat(X509Certificate certificate)
		{
			throw new NotSupportedException();
		}

		// Token: 0x06000428 RID: 1064 RVA: 0x000175EA File Offset: 0x000157EA
		private void RemoveNewFormat(X509Certificate certificate)
		{
			throw new NotSupportedException();
		}

		// Token: 0x06000429 RID: 1065 RVA: 0x000175F1 File Offset: 0x000157F1
		private string GetUniqueNameWithSerial(X509Certificate certificate)
		{
			return this.GetUniqueName(certificate, certificate.SerialNumber);
		}

		// Token: 0x0600042A RID: 1066 RVA: 0x00017600 File Offset: 0x00015800
		private string GetUniqueName(X509Certificate certificate, byte[] serial = null)
		{
			byte[] array = this.GetUniqueName(certificate.Extensions, serial);
			string method;
			if (array == null)
			{
				method = "tbp";
				array = certificate.Hash;
			}
			else
			{
				method = "ski";
			}
			return this.GetUniqueName(method, array, ".cer");
		}

		// Token: 0x0600042B RID: 1067 RVA: 0x00017640 File Offset: 0x00015840
		private string GetUniqueName(X509Crl crl)
		{
			byte[] array = this.GetUniqueName(crl.Extensions, null);
			string method;
			if (array == null)
			{
				method = "tbp";
				array = crl.Hash;
			}
			else
			{
				method = "ski";
			}
			return this.GetUniqueName(method, array, ".crl");
		}

		// Token: 0x0600042C RID: 1068 RVA: 0x00017680 File Offset: 0x00015880
		private byte[] GetUniqueName(X509ExtensionCollection extensions, byte[] serial = null)
		{
			X509Extension x509Extension = extensions["2.5.29.14"];
			if (x509Extension == null)
			{
				return null;
			}
			SubjectKeyIdentifierExtension subjectKeyIdentifierExtension = new SubjectKeyIdentifierExtension(x509Extension);
			if (serial == null)
			{
				return subjectKeyIdentifierExtension.Identifier;
			}
			byte[] array = new byte[subjectKeyIdentifierExtension.Identifier.Length + serial.Length];
			Buffer.BlockCopy(subjectKeyIdentifierExtension.Identifier, 0, array, 0, subjectKeyIdentifierExtension.Identifier.Length);
			Buffer.BlockCopy(serial, 0, array, subjectKeyIdentifierExtension.Identifier.Length, serial.Length);
			return array;
		}

		// Token: 0x0600042D RID: 1069 RVA: 0x000176EC File Offset: 0x000158EC
		private string GetUniqueName(string method, byte[] name, string fileExtension)
		{
			StringBuilder stringBuilder = new StringBuilder(method);
			stringBuilder.Append("-");
			foreach (byte b in name)
			{
				stringBuilder.Append(b.ToString("X2", CultureInfo.InvariantCulture));
			}
			stringBuilder.Append(fileExtension);
			return stringBuilder.ToString();
		}

		// Token: 0x0600042E RID: 1070 RVA: 0x00017748 File Offset: 0x00015948
		private byte[] Load(string filename)
		{
			byte[] array = null;
			using (FileStream fileStream = File.OpenRead(filename))
			{
				array = new byte[fileStream.Length];
				fileStream.Read(array, 0, array.Length);
				fileStream.Close();
			}
			return array;
		}

		// Token: 0x0600042F RID: 1071 RVA: 0x0001779C File Offset: 0x0001599C
		private X509Certificate LoadCertificate(string filename)
		{
			return new X509Certificate(this.Load(filename));
		}

		// Token: 0x06000430 RID: 1072 RVA: 0x000177AA File Offset: 0x000159AA
		private X509Crl LoadCrl(string filename)
		{
			return new X509Crl(this.Load(filename));
		}

		// Token: 0x06000431 RID: 1073 RVA: 0x000177B8 File Offset: 0x000159B8
		private bool CheckStore(string path, bool throwException)
		{
			bool result;
			try
			{
				if (Directory.Exists(path))
				{
					result = true;
				}
				else
				{
					Directory.CreateDirectory(path);
					result = Directory.Exists(path);
				}
			}
			catch
			{
				if (throwException)
				{
					throw;
				}
				result = false;
			}
			return result;
		}

		// Token: 0x06000432 RID: 1074 RVA: 0x000177FC File Offset: 0x000159FC
		private X509CertificateCollection BuildCertificatesCollection(string storeName)
		{
			X509CertificateCollection x509CertificateCollection = new X509CertificateCollection();
			string path = Path.Combine(this._storePath, storeName);
			if (!this.CheckStore(path, false))
			{
				return x509CertificateCollection;
			}
			string[] files = Directory.GetFiles(path, this._newFormat ? "*.0" : "*.cer");
			if (files != null && files.Length != 0)
			{
				foreach (string filename in files)
				{
					try
					{
						X509Certificate value = this.LoadCertificate(filename);
						x509CertificateCollection.Add(value);
					}
					catch
					{
					}
				}
			}
			return x509CertificateCollection;
		}

		// Token: 0x06000433 RID: 1075 RVA: 0x00017890 File Offset: 0x00015A90
		private ArrayList BuildCrlsCollection(string storeName)
		{
			ArrayList arrayList = new ArrayList();
			string path = Path.Combine(this._storePath, storeName);
			if (!this.CheckStore(path, false))
			{
				return arrayList;
			}
			string[] files = Directory.GetFiles(path, "*.crl");
			if (files != null && files.Length != 0)
			{
				foreach (string filename in files)
				{
					try
					{
						X509Crl value = this.LoadCrl(filename);
						arrayList.Add(value);
					}
					catch
					{
					}
				}
			}
			return arrayList;
		}

		// Token: 0x04000594 RID: 1428
		private string _storePath;

		// Token: 0x04000595 RID: 1429
		private X509CertificateCollection _certificates;

		// Token: 0x04000596 RID: 1430
		private ArrayList _crls;

		// Token: 0x04000597 RID: 1431
		private bool _crl;

		// Token: 0x04000598 RID: 1432
		private bool _newFormat;

		// Token: 0x04000599 RID: 1433
		private string _name;
	}
}
