﻿using System;
using System.Collections;
using System.IO;
using System.Security.Cryptography;
using Mono.Security.Cryptography;
using Mono.Security.X509.Extensions;

namespace Mono.Security.X509
{
	// Token: 0x02000070 RID: 112
	internal class X509Crl
	{
		// Token: 0x0600038D RID: 909 RVA: 0x000150FE File Offset: 0x000132FE
		public X509Crl(byte[] crl)
		{
			if (crl == null)
			{
				throw new ArgumentNullException("crl");
			}
			this.encoded = (byte[])crl.Clone();
			this.Parse(this.encoded);
		}

		// Token: 0x0600038E RID: 910 RVA: 0x00015134 File Offset: 0x00013334
		private void Parse(byte[] crl)
		{
			string text = "Input data cannot be coded as a valid CRL.";
			try
			{
				ASN1 asn = new ASN1(this.encoded);
				if (asn.Tag != 48 || asn.Count != 3)
				{
					throw new CryptographicException(text);
				}
				ASN1 asn2 = asn[0];
				if (asn2.Tag != 48 || asn2.Count < 3)
				{
					throw new CryptographicException(text);
				}
				int num = 0;
				if (asn2[num].Tag == 2)
				{
					this.version = asn2[num++].Value[0] + 1;
				}
				else
				{
					this.version = 1;
				}
				this.signatureOID = ASN1Convert.ToOid(asn2[num++][0]);
				this.issuer = X501.ToString(asn2[num++]);
				this.thisUpdate = ASN1Convert.ToDateTime(asn2[num++]);
				ASN1 asn3 = asn2[num++];
				if (asn3.Tag == 23 || asn3.Tag == 24)
				{
					this.nextUpdate = ASN1Convert.ToDateTime(asn3);
					asn3 = asn2[num++];
				}
				this.entries = new ArrayList();
				if (asn3 != null && asn3.Tag == 48)
				{
					ASN1 asn4 = asn3;
					for (int i = 0; i < asn4.Count; i++)
					{
						this.entries.Add(new X509Crl.X509CrlEntry(asn4[i]));
					}
				}
				else
				{
					num--;
				}
				ASN1 asn5 = asn2[num];
				if (asn5 != null && asn5.Tag == 160 && asn5.Count == 1)
				{
					this.extensions = new X509ExtensionCollection(asn5[0]);
				}
				else
				{
					this.extensions = new X509ExtensionCollection(null);
				}
				string b = ASN1Convert.ToOid(asn[1][0]);
				if (this.signatureOID != b)
				{
					throw new CryptographicException(text + " [Non-matching signature algorithms in CRL]");
				}
				byte[] value = asn[2].Value;
				this.signature = new byte[value.Length - 1];
				Buffer.BlockCopy(value, 1, this.signature, 0, this.signature.Length);
			}
			catch
			{
				throw new CryptographicException(text);
			}
		}

		// Token: 0x17000086 RID: 134
		// (get) Token: 0x0600038F RID: 911 RVA: 0x00015374 File Offset: 0x00013574
		public ArrayList Entries
		{
			get
			{
				return ArrayList.ReadOnly(this.entries);
			}
		}

		// Token: 0x17000087 RID: 135
		public X509Crl.X509CrlEntry this[int index]
		{
			get
			{
				return (X509Crl.X509CrlEntry)this.entries[index];
			}
		}

		// Token: 0x17000088 RID: 136
		public X509Crl.X509CrlEntry this[byte[] serialNumber]
		{
			get
			{
				return this.GetCrlEntry(serialNumber);
			}
		}

		// Token: 0x17000089 RID: 137
		// (get) Token: 0x06000392 RID: 914 RVA: 0x0001539D File Offset: 0x0001359D
		public X509ExtensionCollection Extensions
		{
			get
			{
				return this.extensions;
			}
		}

		// Token: 0x1700008A RID: 138
		// (get) Token: 0x06000393 RID: 915 RVA: 0x000153A8 File Offset: 0x000135A8
		public byte[] Hash
		{
			get
			{
				if (this.hash_value == null)
				{
					byte[] bytes = new ASN1(this.encoded)[0].GetBytes();
					using (HashAlgorithm hashAlgorithm = PKCS1.CreateFromOid(this.signatureOID))
					{
						this.hash_value = hashAlgorithm.ComputeHash(bytes);
					}
				}
				return this.hash_value;
			}
		}

		// Token: 0x1700008B RID: 139
		// (get) Token: 0x06000394 RID: 916 RVA: 0x00015410 File Offset: 0x00013610
		public string IssuerName
		{
			get
			{
				return this.issuer;
			}
		}

		// Token: 0x1700008C RID: 140
		// (get) Token: 0x06000395 RID: 917 RVA: 0x00015418 File Offset: 0x00013618
		public DateTime NextUpdate
		{
			get
			{
				return this.nextUpdate;
			}
		}

		// Token: 0x1700008D RID: 141
		// (get) Token: 0x06000396 RID: 918 RVA: 0x00015420 File Offset: 0x00013620
		public DateTime ThisUpdate
		{
			get
			{
				return this.thisUpdate;
			}
		}

		// Token: 0x1700008E RID: 142
		// (get) Token: 0x06000397 RID: 919 RVA: 0x00015428 File Offset: 0x00013628
		public string SignatureAlgorithm
		{
			get
			{
				return this.signatureOID;
			}
		}

		// Token: 0x1700008F RID: 143
		// (get) Token: 0x06000398 RID: 920 RVA: 0x00015430 File Offset: 0x00013630
		public byte[] Signature
		{
			get
			{
				if (this.signature == null)
				{
					return null;
				}
				return (byte[])this.signature.Clone();
			}
		}

		// Token: 0x17000090 RID: 144
		// (get) Token: 0x06000399 RID: 921 RVA: 0x0001544C File Offset: 0x0001364C
		public byte[] RawData
		{
			get
			{
				return (byte[])this.encoded.Clone();
			}
		}

		// Token: 0x17000091 RID: 145
		// (get) Token: 0x0600039A RID: 922 RVA: 0x0001545E File Offset: 0x0001365E
		public byte Version
		{
			get
			{
				return this.version;
			}
		}

		// Token: 0x17000092 RID: 146
		// (get) Token: 0x0600039B RID: 923 RVA: 0x00015466 File Offset: 0x00013666
		public bool IsCurrent
		{
			get
			{
				return this.WasCurrent(DateTime.Now);
			}
		}

		// Token: 0x0600039C RID: 924 RVA: 0x00015473 File Offset: 0x00013673
		public bool WasCurrent(DateTime instant)
		{
			if (this.nextUpdate == DateTime.MinValue)
			{
				return instant >= this.thisUpdate;
			}
			return instant >= this.thisUpdate && instant <= this.nextUpdate;
		}

		// Token: 0x0600039D RID: 925 RVA: 0x0001544C File Offset: 0x0001364C
		public byte[] GetBytes()
		{
			return (byte[])this.encoded.Clone();
		}

		// Token: 0x0600039E RID: 926 RVA: 0x000154B0 File Offset: 0x000136B0
		private bool Compare(byte[] array1, byte[] array2)
		{
			if (array1 == null && array2 == null)
			{
				return true;
			}
			if (array1 == null || array2 == null)
			{
				return false;
			}
			if (array1.Length != array2.Length)
			{
				return false;
			}
			for (int i = 0; i < array1.Length; i++)
			{
				if (array1[i] != array2[i])
				{
					return false;
				}
			}
			return true;
		}

		// Token: 0x0600039F RID: 927 RVA: 0x000154F0 File Offset: 0x000136F0
		public X509Crl.X509CrlEntry GetCrlEntry(X509Certificate x509)
		{
			if (x509 == null)
			{
				throw new ArgumentNullException("x509");
			}
			return this.GetCrlEntry(x509.SerialNumber);
		}

		// Token: 0x060003A0 RID: 928 RVA: 0x0001550C File Offset: 0x0001370C
		public X509Crl.X509CrlEntry GetCrlEntry(byte[] serialNumber)
		{
			if (serialNumber == null)
			{
				throw new ArgumentNullException("serialNumber");
			}
			for (int i = 0; i < this.entries.Count; i++)
			{
				X509Crl.X509CrlEntry x509CrlEntry = (X509Crl.X509CrlEntry)this.entries[i];
				if (this.Compare(serialNumber, x509CrlEntry.SerialNumber))
				{
					return x509CrlEntry;
				}
			}
			return null;
		}

		// Token: 0x060003A1 RID: 929 RVA: 0x00015564 File Offset: 0x00013764
		public bool VerifySignature(X509Certificate x509)
		{
			if (x509 == null)
			{
				throw new ArgumentNullException("x509");
			}
			if (x509.Version >= 3)
			{
				BasicConstraintsExtension basicConstraintsExtension = null;
				X509Extension x509Extension = x509.Extensions["2.5.29.19"];
				if (x509Extension != null)
				{
					basicConstraintsExtension = new BasicConstraintsExtension(x509Extension);
					if (!basicConstraintsExtension.CertificateAuthority)
					{
						return false;
					}
				}
				x509Extension = x509.Extensions["2.5.29.15"];
				if (x509Extension != null)
				{
					KeyUsageExtension keyUsageExtension = new KeyUsageExtension(x509Extension);
					if (!keyUsageExtension.Support(KeyUsages.cRLSign) && (basicConstraintsExtension == null || !keyUsageExtension.Support(KeyUsages.digitalSignature)))
					{
						return false;
					}
				}
			}
			if (this.issuer != x509.SubjectName)
			{
				return false;
			}
			string a = this.signatureOID;
			if (a == "1.2.840.10040.4.3")
			{
				return this.VerifySignature(x509.DSA);
			}
			return this.VerifySignature(x509.RSA);
		}

		// Token: 0x060003A2 RID: 930 RVA: 0x00015628 File Offset: 0x00013828
		internal bool VerifySignature(DSA dsa)
		{
			if (this.signatureOID != "1.2.840.10040.4.3")
			{
				throw new CryptographicException("Unsupported hash algorithm: " + this.signatureOID);
			}
			DSASignatureDeformatter dsasignatureDeformatter = new DSASignatureDeformatter(dsa);
			dsasignatureDeformatter.SetHashAlgorithm("SHA1");
			ASN1 asn = new ASN1(this.signature);
			if (asn == null || asn.Count != 2)
			{
				return false;
			}
			byte[] value = asn[0].Value;
			byte[] value2 = asn[1].Value;
			byte[] array = new byte[40];
			int num = Math.Max(0, value.Length - 20);
			int dstOffset = Math.Max(0, 20 - value.Length);
			Buffer.BlockCopy(value, num, array, dstOffset, value.Length - num);
			int num2 = Math.Max(0, value2.Length - 20);
			int dstOffset2 = Math.Max(20, 40 - value2.Length);
			Buffer.BlockCopy(value2, num2, array, dstOffset2, value2.Length - num2);
			return dsasignatureDeformatter.VerifySignature(this.Hash, array);
		}

		// Token: 0x060003A3 RID: 931 RVA: 0x00015717 File Offset: 0x00013917
		internal bool VerifySignature(RSA rsa)
		{
			RSAPKCS1SignatureDeformatter rsapkcs1SignatureDeformatter = new RSAPKCS1SignatureDeformatter(rsa);
			rsapkcs1SignatureDeformatter.SetHashAlgorithm(PKCS1.HashNameFromOid(this.signatureOID, true));
			return rsapkcs1SignatureDeformatter.VerifySignature(this.Hash, this.signature);
		}

		// Token: 0x060003A4 RID: 932 RVA: 0x00015744 File Offset: 0x00013944
		public bool VerifySignature(AsymmetricAlgorithm aa)
		{
			if (aa == null)
			{
				throw new ArgumentNullException("aa");
			}
			if (aa is RSA)
			{
				return this.VerifySignature(aa as RSA);
			}
			if (aa is DSA)
			{
				return this.VerifySignature(aa as DSA);
			}
			throw new NotSupportedException("Unknown Asymmetric Algorithm " + aa.ToString());
		}

		// Token: 0x060003A5 RID: 933 RVA: 0x000157A0 File Offset: 0x000139A0
		public static X509Crl CreateFromFile(string filename)
		{
			byte[] array = null;
			using (FileStream fileStream = File.Open(filename, FileMode.Open, FileAccess.Read, FileShare.Read))
			{
				array = new byte[fileStream.Length];
				fileStream.Read(array, 0, array.Length);
				fileStream.Close();
			}
			return new X509Crl(array);
		}

		// Token: 0x0400055C RID: 1372
		private string issuer;

		// Token: 0x0400055D RID: 1373
		private byte version;

		// Token: 0x0400055E RID: 1374
		private DateTime thisUpdate;

		// Token: 0x0400055F RID: 1375
		private DateTime nextUpdate;

		// Token: 0x04000560 RID: 1376
		private ArrayList entries;

		// Token: 0x04000561 RID: 1377
		private string signatureOID;

		// Token: 0x04000562 RID: 1378
		private byte[] signature;

		// Token: 0x04000563 RID: 1379
		private X509ExtensionCollection extensions;

		// Token: 0x04000564 RID: 1380
		private byte[] encoded;

		// Token: 0x04000565 RID: 1381
		private byte[] hash_value;

		// Token: 0x02000071 RID: 113
		public class X509CrlEntry
		{
			// Token: 0x060003A6 RID: 934 RVA: 0x000157FC File Offset: 0x000139FC
			internal X509CrlEntry(byte[] serialNumber, DateTime revocationDate, X509ExtensionCollection extensions)
			{
				this.sn = serialNumber;
				this.revocationDate = revocationDate;
				if (extensions == null)
				{
					this.extensions = new X509ExtensionCollection();
					return;
				}
				this.extensions = extensions;
			}

			// Token: 0x060003A7 RID: 935 RVA: 0x00015828 File Offset: 0x00013A28
			internal X509CrlEntry(ASN1 entry)
			{
				this.sn = entry[0].Value;
				Array.Reverse<byte>(this.sn);
				this.revocationDate = ASN1Convert.ToDateTime(entry[1]);
				this.extensions = new X509ExtensionCollection(entry[2]);
			}

			// Token: 0x17000093 RID: 147
			// (get) Token: 0x060003A8 RID: 936 RVA: 0x0001587C File Offset: 0x00013A7C
			public byte[] SerialNumber
			{
				get
				{
					return (byte[])this.sn.Clone();
				}
			}

			// Token: 0x17000094 RID: 148
			// (get) Token: 0x060003A9 RID: 937 RVA: 0x0001588E File Offset: 0x00013A8E
			public DateTime RevocationDate
			{
				get
				{
					return this.revocationDate;
				}
			}

			// Token: 0x17000095 RID: 149
			// (get) Token: 0x060003AA RID: 938 RVA: 0x00015896 File Offset: 0x00013A96
			public X509ExtensionCollection Extensions
			{
				get
				{
					return this.extensions;
				}
			}

			// Token: 0x060003AB RID: 939 RVA: 0x000158A0 File Offset: 0x00013AA0
			public byte[] GetBytes()
			{
				ASN1 asn = new ASN1(48);
				asn.Add(new ASN1(2, this.sn));
				asn.Add(ASN1Convert.FromDateTime(this.revocationDate));
				if (this.extensions.Count > 0)
				{
					asn.Add(new ASN1(this.extensions.GetBytes()));
				}
				return asn.GetBytes();
			}

			// Token: 0x04000566 RID: 1382
			private byte[] sn;

			// Token: 0x04000567 RID: 1383
			private DateTime revocationDate;

			// Token: 0x04000568 RID: 1384
			private X509ExtensionCollection extensions;
		}
	}
}
