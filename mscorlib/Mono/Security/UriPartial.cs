﻿using System;

namespace Mono.Security
{
	// Token: 0x02000066 RID: 102
	internal enum UriPartial
	{
		// Token: 0x040004F9 RID: 1273
		Scheme,
		// Token: 0x040004FA RID: 1274
		Authority,
		// Token: 0x040004FB RID: 1275
		Path
	}
}
