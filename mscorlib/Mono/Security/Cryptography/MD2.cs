﻿using System;
using System.Security.Cryptography;

namespace Mono.Security.Cryptography
{
	// Token: 0x02000099 RID: 153
	internal abstract class MD2 : HashAlgorithm
	{
		// Token: 0x060004DA RID: 1242 RVA: 0x0001A19D File Offset: 0x0001839D
		protected MD2()
		{
			this.HashSizeValue = 128;
		}

		// Token: 0x060004DB RID: 1243 RVA: 0x0001A1B0 File Offset: 0x000183B0
		public new static MD2 Create()
		{
			return new MD2Managed();
		}

		// Token: 0x060004DC RID: 1244 RVA: 0x0001A1B8 File Offset: 0x000183B8
		public new static MD2 Create(string hashName)
		{
			object obj = CryptoConfig.CreateFromName(hashName);
			if (obj == null)
			{
				obj = new MD2Managed();
			}
			return (MD2)obj;
		}
	}
}
