﻿using System;
using System.Security.Cryptography;

namespace Mono.Security.Cryptography
{
	// Token: 0x020000A2 RID: 162
	internal abstract class RC4 : SymmetricAlgorithm
	{
		// Token: 0x06000530 RID: 1328 RVA: 0x0001C009 File Offset: 0x0001A209
		public RC4()
		{
			this.KeySizeValue = 128;
			this.BlockSizeValue = 64;
			this.FeedbackSizeValue = this.BlockSizeValue;
			this.LegalBlockSizesValue = RC4.s_legalBlockSizes;
			this.LegalKeySizesValue = RC4.s_legalKeySizes;
		}

		// Token: 0x170000EE RID: 238
		// (get) Token: 0x06000531 RID: 1329 RVA: 0x0001C046 File Offset: 0x0001A246
		// (set) Token: 0x06000532 RID: 1330 RVA: 0x000020D3 File Offset: 0x000002D3
		public override byte[] IV
		{
			get
			{
				return new byte[0];
			}
			set
			{
			}
		}

		// Token: 0x06000533 RID: 1331 RVA: 0x0001C04E File Offset: 0x0001A24E
		public new static RC4 Create()
		{
			return new ARC4Managed();
		}

		// Token: 0x06000534 RID: 1332 RVA: 0x0001C058 File Offset: 0x0001A258
		public new static RC4 Create(string algName)
		{
			object obj = CryptoConfig.CreateFromName(algName);
			if (obj == null)
			{
				obj = new ARC4Managed();
			}
			return (RC4)obj;
		}

		// Token: 0x06000535 RID: 1333 RVA: 0x0001C07B File Offset: 0x0001A27B
		// Note: this type is marked as 'beforefieldinit'.
		static RC4()
		{
		}

		// Token: 0x040005FC RID: 1532
		private static KeySizes[] s_legalBlockSizes = new KeySizes[]
		{
			new KeySizes(64, 64, 0)
		};

		// Token: 0x040005FD RID: 1533
		private static KeySizes[] s_legalKeySizes = new KeySizes[]
		{
			new KeySizes(40, 2048, 8)
		};
	}
}
