﻿using System;
using System.Security.Cryptography;

namespace Mono.Security.Cryptography
{
	// Token: 0x020000A9 RID: 169
	internal class MACAlgorithm
	{
		// Token: 0x0600058A RID: 1418 RVA: 0x0001E610 File Offset: 0x0001C810
		public MACAlgorithm(SymmetricAlgorithm algorithm)
		{
			this.algo = algorithm;
			this.algo.Mode = CipherMode.CBC;
			this.blockSize = this.algo.BlockSize >> 3;
			this.algo.IV = new byte[this.blockSize];
			this.block = new byte[this.blockSize];
		}

		// Token: 0x0600058B RID: 1419 RVA: 0x0001E670 File Offset: 0x0001C870
		public void Initialize(byte[] key)
		{
			this.algo.Key = key;
			if (this.enc == null)
			{
				this.enc = this.algo.CreateEncryptor();
			}
			Array.Clear(this.block, 0, this.blockSize);
			this.blockCount = 0;
		}

		// Token: 0x0600058C RID: 1420 RVA: 0x0001E6B0 File Offset: 0x0001C8B0
		public void Core(byte[] rgb, int ib, int cb)
		{
			int num = Math.Min(this.blockSize - this.blockCount, cb);
			Array.Copy(rgb, ib, this.block, this.blockCount, num);
			this.blockCount += num;
			if (this.blockCount == this.blockSize)
			{
				this.enc.TransformBlock(this.block, 0, this.blockSize, this.block, 0);
				int num2 = (cb - num) / this.blockSize;
				for (int i = 0; i < num2; i++)
				{
					this.enc.TransformBlock(rgb, num, this.blockSize, this.block, 0);
					num += this.blockSize;
				}
				this.blockCount = cb - num;
				if (this.blockCount > 0)
				{
					Array.Copy(rgb, num, this.block, 0, this.blockCount);
				}
			}
		}

		// Token: 0x0600058D RID: 1421 RVA: 0x0001E788 File Offset: 0x0001C988
		public byte[] Final()
		{
			byte[] result;
			if (this.blockCount > 0 || (this.algo.Padding != PaddingMode.Zeros && this.algo.Padding != PaddingMode.None))
			{
				result = this.enc.TransformFinalBlock(this.block, 0, this.blockCount);
			}
			else
			{
				result = (byte[])this.block.Clone();
			}
			if (!this.enc.CanReuseTransform)
			{
				this.enc.Dispose();
				this.enc = null;
			}
			return result;
		}

		// Token: 0x0400062B RID: 1579
		private SymmetricAlgorithm algo;

		// Token: 0x0400062C RID: 1580
		private ICryptoTransform enc;

		// Token: 0x0400062D RID: 1581
		private byte[] block;

		// Token: 0x0400062E RID: 1582
		private int blockSize;

		// Token: 0x0400062F RID: 1583
		private int blockCount;
	}
}
