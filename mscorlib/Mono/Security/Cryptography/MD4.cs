﻿using System;
using System.Security.Cryptography;

namespace Mono.Security.Cryptography
{
	// Token: 0x0200009B RID: 155
	internal abstract class MD4 : HashAlgorithm
	{
		// Token: 0x060004E4 RID: 1252 RVA: 0x0001A19D File Offset: 0x0001839D
		protected MD4()
		{
			this.HashSizeValue = 128;
		}

		// Token: 0x060004E5 RID: 1253 RVA: 0x0001A486 File Offset: 0x00018686
		public new static MD4 Create()
		{
			return new MD4Managed();
		}

		// Token: 0x060004E6 RID: 1254 RVA: 0x0001A490 File Offset: 0x00018690
		public new static MD4 Create(string hashName)
		{
			object obj = CryptoConfig.CreateFromName(hashName);
			if (obj == null)
			{
				obj = new MD4Managed();
			}
			return (MD4)obj;
		}
	}
}
