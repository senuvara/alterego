﻿using System;
using System.Collections;
using System.Security.Cryptography;

namespace Mono.Security.Cryptography
{
	// Token: 0x0200009E RID: 158
	internal sealed class PKCS8
	{
		// Token: 0x06000510 RID: 1296 RVA: 0x00002050 File Offset: 0x00000250
		private PKCS8()
		{
		}

		// Token: 0x06000511 RID: 1297 RVA: 0x0001B754 File Offset: 0x00019954
		public static PKCS8.KeyInfo GetType(byte[] data)
		{
			if (data == null)
			{
				throw new ArgumentNullException("data");
			}
			PKCS8.KeyInfo result = PKCS8.KeyInfo.Unknown;
			try
			{
				ASN1 asn = new ASN1(data);
				if (asn.Tag == 48 && asn.Count > 0)
				{
					byte tag = asn[0].Tag;
					if (tag != 2)
					{
						if (tag == 48)
						{
							result = PKCS8.KeyInfo.EncryptedPrivateKey;
						}
					}
					else
					{
						result = PKCS8.KeyInfo.PrivateKey;
					}
				}
			}
			catch
			{
				throw new CryptographicException("invalid ASN.1 data");
			}
			return result;
		}

		// Token: 0x0200009F RID: 159
		public enum KeyInfo
		{
			// Token: 0x040005F1 RID: 1521
			PrivateKey,
			// Token: 0x040005F2 RID: 1522
			EncryptedPrivateKey,
			// Token: 0x040005F3 RID: 1523
			Unknown
		}

		// Token: 0x020000A0 RID: 160
		public class PrivateKeyInfo
		{
			// Token: 0x06000512 RID: 1298 RVA: 0x0001B7C8 File Offset: 0x000199C8
			public PrivateKeyInfo()
			{
				this._version = 0;
				this._list = new ArrayList();
			}

			// Token: 0x06000513 RID: 1299 RVA: 0x0001B7E2 File Offset: 0x000199E2
			public PrivateKeyInfo(byte[] data) : this()
			{
				this.Decode(data);
			}

			// Token: 0x170000E6 RID: 230
			// (get) Token: 0x06000514 RID: 1300 RVA: 0x0001B7F1 File Offset: 0x000199F1
			// (set) Token: 0x06000515 RID: 1301 RVA: 0x0001B7F9 File Offset: 0x000199F9
			public string Algorithm
			{
				get
				{
					return this._algorithm;
				}
				set
				{
					this._algorithm = value;
				}
			}

			// Token: 0x170000E7 RID: 231
			// (get) Token: 0x06000516 RID: 1302 RVA: 0x0001B802 File Offset: 0x00019A02
			public ArrayList Attributes
			{
				get
				{
					return this._list;
				}
			}

			// Token: 0x170000E8 RID: 232
			// (get) Token: 0x06000517 RID: 1303 RVA: 0x0001B80A File Offset: 0x00019A0A
			// (set) Token: 0x06000518 RID: 1304 RVA: 0x0001B826 File Offset: 0x00019A26
			public byte[] PrivateKey
			{
				get
				{
					if (this._key == null)
					{
						return null;
					}
					return (byte[])this._key.Clone();
				}
				set
				{
					if (value == null)
					{
						throw new ArgumentNullException("PrivateKey");
					}
					this._key = (byte[])value.Clone();
				}
			}

			// Token: 0x170000E9 RID: 233
			// (get) Token: 0x06000519 RID: 1305 RVA: 0x0001B847 File Offset: 0x00019A47
			// (set) Token: 0x0600051A RID: 1306 RVA: 0x0001B84F File Offset: 0x00019A4F
			public int Version
			{
				get
				{
					return this._version;
				}
				set
				{
					if (value < 0)
					{
						throw new ArgumentOutOfRangeException("negative version");
					}
					this._version = value;
				}
			}

			// Token: 0x0600051B RID: 1307 RVA: 0x0001B868 File Offset: 0x00019A68
			private void Decode(byte[] data)
			{
				ASN1 asn = new ASN1(data);
				if (asn.Tag != 48)
				{
					throw new CryptographicException("invalid PrivateKeyInfo");
				}
				ASN1 asn2 = asn[0];
				if (asn2.Tag != 2)
				{
					throw new CryptographicException("invalid version");
				}
				this._version = (int)asn2.Value[0];
				ASN1 asn3 = asn[1];
				if (asn3.Tag != 48)
				{
					throw new CryptographicException("invalid algorithm");
				}
				ASN1 asn4 = asn3[0];
				if (asn4.Tag != 6)
				{
					throw new CryptographicException("missing algorithm OID");
				}
				this._algorithm = ASN1Convert.ToOid(asn4);
				ASN1 asn5 = asn[2];
				this._key = asn5.Value;
				if (asn.Count > 3)
				{
					ASN1 asn6 = asn[3];
					for (int i = 0; i < asn6.Count; i++)
					{
						this._list.Add(asn6[i]);
					}
				}
			}

			// Token: 0x0600051C RID: 1308 RVA: 0x0001B950 File Offset: 0x00019B50
			public byte[] GetBytes()
			{
				ASN1 asn = new ASN1(48);
				asn.Add(ASN1Convert.FromOid(this._algorithm));
				asn.Add(new ASN1(5));
				ASN1 asn2 = new ASN1(48);
				asn2.Add(new ASN1(2, new byte[]
				{
					(byte)this._version
				}));
				asn2.Add(asn);
				asn2.Add(new ASN1(4, this._key));
				if (this._list.Count > 0)
				{
					ASN1 asn3 = new ASN1(160);
					foreach (object obj in this._list)
					{
						ASN1 asn4 = (ASN1)obj;
						asn3.Add(asn4);
					}
					asn2.Add(asn3);
				}
				return asn2.GetBytes();
			}

			// Token: 0x0600051D RID: 1309 RVA: 0x0001BA40 File Offset: 0x00019C40
			private static byte[] RemoveLeadingZero(byte[] bigInt)
			{
				int srcOffset = 0;
				int num = bigInt.Length;
				if (bigInt[0] == 0)
				{
					srcOffset = 1;
					num--;
				}
				byte[] array = new byte[num];
				Buffer.BlockCopy(bigInt, srcOffset, array, 0, num);
				return array;
			}

			// Token: 0x0600051E RID: 1310 RVA: 0x0001BA70 File Offset: 0x00019C70
			private static byte[] Normalize(byte[] bigInt, int length)
			{
				if (bigInt.Length == length)
				{
					return bigInt;
				}
				if (bigInt.Length > length)
				{
					return PKCS8.PrivateKeyInfo.RemoveLeadingZero(bigInt);
				}
				byte[] array = new byte[length];
				Buffer.BlockCopy(bigInt, 0, array, length - bigInt.Length, bigInt.Length);
				return array;
			}

			// Token: 0x0600051F RID: 1311 RVA: 0x0001BAAC File Offset: 0x00019CAC
			public static RSA DecodeRSA(byte[] keypair)
			{
				ASN1 asn = new ASN1(keypair);
				if (asn.Tag != 48)
				{
					throw new CryptographicException("invalid private key format");
				}
				if (asn[0].Tag != 2)
				{
					throw new CryptographicException("missing version");
				}
				if (asn.Count < 9)
				{
					throw new CryptographicException("not enough key parameters");
				}
				RSAParameters rsaparameters = new RSAParameters
				{
					Modulus = PKCS8.PrivateKeyInfo.RemoveLeadingZero(asn[1].Value)
				};
				int num = rsaparameters.Modulus.Length;
				int length = num >> 1;
				rsaparameters.D = PKCS8.PrivateKeyInfo.Normalize(asn[3].Value, num);
				rsaparameters.DP = PKCS8.PrivateKeyInfo.Normalize(asn[6].Value, length);
				rsaparameters.DQ = PKCS8.PrivateKeyInfo.Normalize(asn[7].Value, length);
				rsaparameters.Exponent = PKCS8.PrivateKeyInfo.RemoveLeadingZero(asn[2].Value);
				rsaparameters.InverseQ = PKCS8.PrivateKeyInfo.Normalize(asn[8].Value, length);
				rsaparameters.P = PKCS8.PrivateKeyInfo.Normalize(asn[4].Value, length);
				rsaparameters.Q = PKCS8.PrivateKeyInfo.Normalize(asn[5].Value, length);
				RSA rsa = null;
				try
				{
					rsa = RSA.Create();
					rsa.ImportParameters(rsaparameters);
				}
				catch (CryptographicException)
				{
					rsa = new RSACryptoServiceProvider(new CspParameters
					{
						Flags = CspProviderFlags.UseMachineKeyStore
					});
					rsa.ImportParameters(rsaparameters);
				}
				return rsa;
			}

			// Token: 0x06000520 RID: 1312 RVA: 0x0001BC24 File Offset: 0x00019E24
			public static byte[] Encode(RSA rsa)
			{
				RSAParameters rsaparameters = rsa.ExportParameters(true);
				ASN1 asn = new ASN1(48);
				asn.Add(new ASN1(2, new byte[1]));
				asn.Add(ASN1Convert.FromUnsignedBigInteger(rsaparameters.Modulus));
				asn.Add(ASN1Convert.FromUnsignedBigInteger(rsaparameters.Exponent));
				asn.Add(ASN1Convert.FromUnsignedBigInteger(rsaparameters.D));
				asn.Add(ASN1Convert.FromUnsignedBigInteger(rsaparameters.P));
				asn.Add(ASN1Convert.FromUnsignedBigInteger(rsaparameters.Q));
				asn.Add(ASN1Convert.FromUnsignedBigInteger(rsaparameters.DP));
				asn.Add(ASN1Convert.FromUnsignedBigInteger(rsaparameters.DQ));
				asn.Add(ASN1Convert.FromUnsignedBigInteger(rsaparameters.InverseQ));
				return asn.GetBytes();
			}

			// Token: 0x06000521 RID: 1313 RVA: 0x0001BCEC File Offset: 0x00019EEC
			public static DSA DecodeDSA(byte[] privateKey, DSAParameters dsaParameters)
			{
				ASN1 asn = new ASN1(privateKey);
				if (asn.Tag != 2)
				{
					throw new CryptographicException("invalid private key format");
				}
				dsaParameters.X = PKCS8.PrivateKeyInfo.Normalize(asn.Value, 20);
				DSA dsa = DSA.Create();
				dsa.ImportParameters(dsaParameters);
				return dsa;
			}

			// Token: 0x06000522 RID: 1314 RVA: 0x0001BD34 File Offset: 0x00019F34
			public static byte[] Encode(DSA dsa)
			{
				return ASN1Convert.FromUnsignedBigInteger(dsa.ExportParameters(true).X).GetBytes();
			}

			// Token: 0x06000523 RID: 1315 RVA: 0x0001BD4C File Offset: 0x00019F4C
			public static byte[] Encode(AsymmetricAlgorithm aa)
			{
				if (aa is RSA)
				{
					return PKCS8.PrivateKeyInfo.Encode((RSA)aa);
				}
				if (aa is DSA)
				{
					return PKCS8.PrivateKeyInfo.Encode((DSA)aa);
				}
				throw new CryptographicException("Unknown asymmetric algorithm {0}", aa.ToString());
			}

			// Token: 0x040005F4 RID: 1524
			private int _version;

			// Token: 0x040005F5 RID: 1525
			private string _algorithm;

			// Token: 0x040005F6 RID: 1526
			private byte[] _key;

			// Token: 0x040005F7 RID: 1527
			private ArrayList _list;
		}

		// Token: 0x020000A1 RID: 161
		public class EncryptedPrivateKeyInfo
		{
			// Token: 0x06000524 RID: 1316 RVA: 0x00002050 File Offset: 0x00000250
			public EncryptedPrivateKeyInfo()
			{
			}

			// Token: 0x06000525 RID: 1317 RVA: 0x0001BD86 File Offset: 0x00019F86
			public EncryptedPrivateKeyInfo(byte[] data) : this()
			{
				this.Decode(data);
			}

			// Token: 0x170000EA RID: 234
			// (get) Token: 0x06000526 RID: 1318 RVA: 0x0001BD95 File Offset: 0x00019F95
			// (set) Token: 0x06000527 RID: 1319 RVA: 0x0001BD9D File Offset: 0x00019F9D
			public string Algorithm
			{
				get
				{
					return this._algorithm;
				}
				set
				{
					this._algorithm = value;
				}
			}

			// Token: 0x170000EB RID: 235
			// (get) Token: 0x06000528 RID: 1320 RVA: 0x0001BDA6 File Offset: 0x00019FA6
			// (set) Token: 0x06000529 RID: 1321 RVA: 0x0001BDC2 File Offset: 0x00019FC2
			public byte[] EncryptedData
			{
				get
				{
					if (this._data != null)
					{
						return (byte[])this._data.Clone();
					}
					return null;
				}
				set
				{
					this._data = ((value == null) ? null : ((byte[])value.Clone()));
				}
			}

			// Token: 0x170000EC RID: 236
			// (get) Token: 0x0600052A RID: 1322 RVA: 0x0001BDDB File Offset: 0x00019FDB
			// (set) Token: 0x0600052B RID: 1323 RVA: 0x0001BE11 File Offset: 0x0001A011
			public byte[] Salt
			{
				get
				{
					if (this._salt == null)
					{
						RandomNumberGenerator randomNumberGenerator = RandomNumberGenerator.Create();
						this._salt = new byte[8];
						randomNumberGenerator.GetBytes(this._salt);
					}
					return (byte[])this._salt.Clone();
				}
				set
				{
					this._salt = (byte[])value.Clone();
				}
			}

			// Token: 0x170000ED RID: 237
			// (get) Token: 0x0600052C RID: 1324 RVA: 0x0001BE24 File Offset: 0x0001A024
			// (set) Token: 0x0600052D RID: 1325 RVA: 0x0001BE2C File Offset: 0x0001A02C
			public int IterationCount
			{
				get
				{
					return this._iterations;
				}
				set
				{
					if (value < 0)
					{
						throw new ArgumentOutOfRangeException("IterationCount", "Negative");
					}
					this._iterations = value;
				}
			}

			// Token: 0x0600052E RID: 1326 RVA: 0x0001BE4C File Offset: 0x0001A04C
			private void Decode(byte[] data)
			{
				ASN1 asn = new ASN1(data);
				if (asn.Tag != 48)
				{
					throw new CryptographicException("invalid EncryptedPrivateKeyInfo");
				}
				ASN1 asn2 = asn[0];
				if (asn2.Tag != 48)
				{
					throw new CryptographicException("invalid encryptionAlgorithm");
				}
				ASN1 asn3 = asn2[0];
				if (asn3.Tag != 6)
				{
					throw new CryptographicException("invalid algorithm");
				}
				this._algorithm = ASN1Convert.ToOid(asn3);
				if (asn2.Count > 1)
				{
					ASN1 asn4 = asn2[1];
					if (asn4.Tag != 48)
					{
						throw new CryptographicException("invalid parameters");
					}
					ASN1 asn5 = asn4[0];
					if (asn5.Tag != 4)
					{
						throw new CryptographicException("invalid salt");
					}
					this._salt = asn5.Value;
					ASN1 asn6 = asn4[1];
					if (asn6.Tag != 2)
					{
						throw new CryptographicException("invalid iterationCount");
					}
					this._iterations = ASN1Convert.ToInt32(asn6);
				}
				ASN1 asn7 = asn[1];
				if (asn7.Tag != 4)
				{
					throw new CryptographicException("invalid EncryptedData");
				}
				this._data = asn7.Value;
			}

			// Token: 0x0600052F RID: 1327 RVA: 0x0001BF58 File Offset: 0x0001A158
			public byte[] GetBytes()
			{
				if (this._algorithm == null)
				{
					throw new CryptographicException("No algorithm OID specified");
				}
				ASN1 asn = new ASN1(48);
				asn.Add(ASN1Convert.FromOid(this._algorithm));
				if (this._iterations > 0 || this._salt != null)
				{
					ASN1 asn2 = new ASN1(4, this._salt);
					ASN1 asn3 = ASN1Convert.FromInt32(this._iterations);
					ASN1 asn4 = new ASN1(48);
					asn4.Add(asn2);
					asn4.Add(asn3);
					asn.Add(asn4);
				}
				ASN1 asn5 = new ASN1(4, this._data);
				ASN1 asn6 = new ASN1(48);
				asn6.Add(asn);
				asn6.Add(asn5);
				return asn6.GetBytes();
			}

			// Token: 0x040005F8 RID: 1528
			private string _algorithm;

			// Token: 0x040005F9 RID: 1529
			private byte[] _salt;

			// Token: 0x040005FA RID: 1530
			private int _iterations;

			// Token: 0x040005FB RID: 1531
			private byte[] _data;
		}
	}
}
