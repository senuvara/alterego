﻿using System;
using System.Security.Cryptography;

namespace Mono.Security.Cryptography
{
	// Token: 0x02000096 RID: 150
	internal sealed class KeyBuilder
	{
		// Token: 0x060004B2 RID: 1202 RVA: 0x00002050 File Offset: 0x00000250
		private KeyBuilder()
		{
		}

		// Token: 0x170000DC RID: 220
		// (get) Token: 0x060004B3 RID: 1203 RVA: 0x000198CD File Offset: 0x00017ACD
		private static RandomNumberGenerator Rng
		{
			get
			{
				if (KeyBuilder.rng == null)
				{
					KeyBuilder.rng = RandomNumberGenerator.Create();
				}
				return KeyBuilder.rng;
			}
		}

		// Token: 0x060004B4 RID: 1204 RVA: 0x000198E8 File Offset: 0x00017AE8
		public static byte[] Key(int size)
		{
			byte[] array = new byte[size];
			KeyBuilder.Rng.GetBytes(array);
			return array;
		}

		// Token: 0x060004B5 RID: 1205 RVA: 0x00019908 File Offset: 0x00017B08
		public static byte[] IV(int size)
		{
			byte[] array = new byte[size];
			KeyBuilder.Rng.GetBytes(array);
			return array;
		}

		// Token: 0x040005C7 RID: 1479
		private static RandomNumberGenerator rng;
	}
}
