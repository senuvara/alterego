﻿using System;
using System.Globalization;
using System.IO;
using System.Runtime.CompilerServices;
using System.Security;
using System.Security.Cryptography;
using System.Text;
using Mono.Xml;

namespace Mono.Security.Cryptography
{
	// Token: 0x02000098 RID: 152
	internal class KeyPairPersistence
	{
		// Token: 0x060004BD RID: 1213 RVA: 0x00019ACB File Offset: 0x00017CCB
		public KeyPairPersistence(CspParameters parameters) : this(parameters, null)
		{
		}

		// Token: 0x060004BE RID: 1214 RVA: 0x00019AD5 File Offset: 0x00017CD5
		public KeyPairPersistence(CspParameters parameters, string keyPair)
		{
			if (parameters == null)
			{
				throw new ArgumentNullException("parameters");
			}
			this._params = this.Copy(parameters);
			this._keyvalue = keyPair;
		}

		// Token: 0x170000DD RID: 221
		// (get) Token: 0x060004BF RID: 1215 RVA: 0x00019B00 File Offset: 0x00017D00
		public string Filename
		{
			get
			{
				if (this._filename == null)
				{
					this._filename = string.Format(CultureInfo.InvariantCulture, "[{0}][{1}][{2}].xml", this._params.ProviderType, this.ContainerName, this._params.KeyNumber);
					if (this.UseMachineKeyStore)
					{
						this._filename = Path.Combine(KeyPairPersistence.MachinePath, this._filename);
					}
					else
					{
						this._filename = Path.Combine(KeyPairPersistence.UserPath, this._filename);
					}
				}
				return this._filename;
			}
		}

		// Token: 0x170000DE RID: 222
		// (get) Token: 0x060004C0 RID: 1216 RVA: 0x00019B8C File Offset: 0x00017D8C
		// (set) Token: 0x060004C1 RID: 1217 RVA: 0x00019B94 File Offset: 0x00017D94
		public string KeyValue
		{
			get
			{
				return this._keyvalue;
			}
			set
			{
				if (this.CanChange)
				{
					this._keyvalue = value;
				}
			}
		}

		// Token: 0x170000DF RID: 223
		// (get) Token: 0x060004C2 RID: 1218 RVA: 0x00019BA5 File Offset: 0x00017DA5
		public CspParameters Parameters
		{
			get
			{
				return this.Copy(this._params);
			}
		}

		// Token: 0x060004C3 RID: 1219 RVA: 0x00019BB4 File Offset: 0x00017DB4
		public bool Load()
		{
			bool flag = File.Exists(this.Filename);
			if (flag)
			{
				using (StreamReader streamReader = File.OpenText(this.Filename))
				{
					this.FromXml(streamReader.ReadToEnd());
				}
			}
			return flag;
		}

		// Token: 0x060004C4 RID: 1220 RVA: 0x00019C08 File Offset: 0x00017E08
		public void Save()
		{
			using (FileStream fileStream = File.Open(this.Filename, FileMode.Create))
			{
				StreamWriter streamWriter = new StreamWriter(fileStream, Encoding.UTF8);
				streamWriter.Write(this.ToXml());
				streamWriter.Close();
			}
			if (this.UseMachineKeyStore)
			{
				KeyPairPersistence.ProtectMachine(this.Filename);
				return;
			}
			KeyPairPersistence.ProtectUser(this.Filename);
		}

		// Token: 0x060004C5 RID: 1221 RVA: 0x00019C7C File Offset: 0x00017E7C
		public void Remove()
		{
			File.Delete(this.Filename);
		}

		// Token: 0x170000E0 RID: 224
		// (get) Token: 0x060004C6 RID: 1222 RVA: 0x00019C8C File Offset: 0x00017E8C
		private static string UserPath
		{
			get
			{
				object obj = KeyPairPersistence.lockobj;
				lock (obj)
				{
					if (KeyPairPersistence._userPath == null || !KeyPairPersistence._userPathExists)
					{
						KeyPairPersistence._userPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), ".mono");
						KeyPairPersistence._userPath = Path.Combine(KeyPairPersistence._userPath, "keypairs");
						KeyPairPersistence._userPathExists = Directory.Exists(KeyPairPersistence._userPath);
						if (!KeyPairPersistence._userPathExists)
						{
							try
							{
								Directory.CreateDirectory(KeyPairPersistence._userPath);
							}
							catch (Exception inner)
							{
								throw new CryptographicException(string.Format(Locale.GetText("Could not create user key store '{0}'."), KeyPairPersistence._userPath), inner);
							}
							KeyPairPersistence._userPathExists = true;
						}
					}
					if (!KeyPairPersistence.IsUserProtected(KeyPairPersistence._userPath) && !KeyPairPersistence.ProtectUser(KeyPairPersistence._userPath))
					{
						throw new IOException(string.Format(Locale.GetText("Could not secure user key store '{0}'."), KeyPairPersistence._userPath));
					}
				}
				if (!KeyPairPersistence.IsUserProtected(KeyPairPersistence._userPath))
				{
					throw new CryptographicException(string.Format(Locale.GetText("Improperly protected user's key pairs in '{0}'."), KeyPairPersistence._userPath));
				}
				return KeyPairPersistence._userPath;
			}
		}

		// Token: 0x170000E1 RID: 225
		// (get) Token: 0x060004C7 RID: 1223 RVA: 0x00019DAC File Offset: 0x00017FAC
		private static string MachinePath
		{
			get
			{
				object obj = KeyPairPersistence.lockobj;
				lock (obj)
				{
					if (KeyPairPersistence._machinePath == null || !KeyPairPersistence._machinePathExists)
					{
						KeyPairPersistence._machinePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), ".mono");
						KeyPairPersistence._machinePath = Path.Combine(KeyPairPersistence._machinePath, "keypairs");
						KeyPairPersistence._machinePathExists = Directory.Exists(KeyPairPersistence._machinePath);
						if (!KeyPairPersistence._machinePathExists)
						{
							try
							{
								Directory.CreateDirectory(KeyPairPersistence._machinePath);
							}
							catch (Exception inner)
							{
								throw new CryptographicException(string.Format(Locale.GetText("Could not create machine key store '{0}'."), KeyPairPersistence._machinePath), inner);
							}
							KeyPairPersistence._machinePathExists = true;
						}
					}
					if (!KeyPairPersistence.IsMachineProtected(KeyPairPersistence._machinePath) && !KeyPairPersistence.ProtectMachine(KeyPairPersistence._machinePath))
					{
						throw new IOException(string.Format(Locale.GetText("Could not secure machine key store '{0}'."), KeyPairPersistence._machinePath));
					}
				}
				if (!KeyPairPersistence.IsMachineProtected(KeyPairPersistence._machinePath))
				{
					throw new CryptographicException(string.Format(Locale.GetText("Improperly protected machine's key pairs in '{0}'."), KeyPairPersistence._machinePath));
				}
				return KeyPairPersistence._machinePath;
			}
		}

		// Token: 0x060004C8 RID: 1224
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern bool _CanSecure(string root);

		// Token: 0x060004C9 RID: 1225
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern bool _ProtectUser(string path);

		// Token: 0x060004CA RID: 1226
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern bool _ProtectMachine(string path);

		// Token: 0x060004CB RID: 1227
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern bool _IsUserProtected(string path);

		// Token: 0x060004CC RID: 1228
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern bool _IsMachineProtected(string path);

		// Token: 0x060004CD RID: 1229 RVA: 0x00019ECC File Offset: 0x000180CC
		private static bool CanSecure(string path)
		{
			int platform = (int)Environment.OSVersion.Platform;
			return platform == 4 || platform == 128 || platform == 6 || KeyPairPersistence._CanSecure(Path.GetPathRoot(path));
		}

		// Token: 0x060004CE RID: 1230 RVA: 0x00019F01 File Offset: 0x00018101
		private static bool ProtectUser(string path)
		{
			return !KeyPairPersistence.CanSecure(path) || KeyPairPersistence._ProtectUser(path);
		}

		// Token: 0x060004CF RID: 1231 RVA: 0x00019F13 File Offset: 0x00018113
		private static bool ProtectMachine(string path)
		{
			return !KeyPairPersistence.CanSecure(path) || KeyPairPersistence._ProtectMachine(path);
		}

		// Token: 0x060004D0 RID: 1232 RVA: 0x00019F25 File Offset: 0x00018125
		private static bool IsUserProtected(string path)
		{
			return !KeyPairPersistence.CanSecure(path) || KeyPairPersistence._IsUserProtected(path);
		}

		// Token: 0x060004D1 RID: 1233 RVA: 0x00019F37 File Offset: 0x00018137
		private static bool IsMachineProtected(string path)
		{
			return !KeyPairPersistence.CanSecure(path) || KeyPairPersistence._IsMachineProtected(path);
		}

		// Token: 0x170000E2 RID: 226
		// (get) Token: 0x060004D2 RID: 1234 RVA: 0x00019F49 File Offset: 0x00018149
		private bool CanChange
		{
			get
			{
				return this._keyvalue == null;
			}
		}

		// Token: 0x170000E3 RID: 227
		// (get) Token: 0x060004D3 RID: 1235 RVA: 0x00019F54 File Offset: 0x00018154
		private bool UseDefaultKeyContainer
		{
			get
			{
				return (this._params.Flags & CspProviderFlags.UseDefaultKeyContainer) == CspProviderFlags.UseDefaultKeyContainer;
			}
		}

		// Token: 0x170000E4 RID: 228
		// (get) Token: 0x060004D4 RID: 1236 RVA: 0x00019F66 File Offset: 0x00018166
		private bool UseMachineKeyStore
		{
			get
			{
				return (this._params.Flags & CspProviderFlags.UseMachineKeyStore) == CspProviderFlags.UseMachineKeyStore;
			}
		}

		// Token: 0x170000E5 RID: 229
		// (get) Token: 0x060004D5 RID: 1237 RVA: 0x00019F78 File Offset: 0x00018178
		private string ContainerName
		{
			get
			{
				if (this._container == null)
				{
					if (this.UseDefaultKeyContainer)
					{
						this._container = "default";
					}
					else if (this._params.KeyContainerName == null || this._params.KeyContainerName.Length == 0)
					{
						this._container = Guid.NewGuid().ToString();
					}
					else
					{
						byte[] bytes = Encoding.UTF8.GetBytes(this._params.KeyContainerName);
						byte[] b = MD5.Create().ComputeHash(bytes);
						this._container = new Guid(b).ToString();
					}
				}
				return this._container;
			}
		}

		// Token: 0x060004D6 RID: 1238 RVA: 0x0001A021 File Offset: 0x00018221
		private CspParameters Copy(CspParameters p)
		{
			return new CspParameters(p.ProviderType, p.ProviderName, p.KeyContainerName)
			{
				KeyNumber = p.KeyNumber,
				Flags = p.Flags
			};
		}

		// Token: 0x060004D7 RID: 1239 RVA: 0x0001A054 File Offset: 0x00018254
		private void FromXml(string xml)
		{
			SecurityParser securityParser = new SecurityParser();
			securityParser.LoadXml(xml);
			SecurityElement securityElement = securityParser.ToXml();
			if (securityElement.Tag == "KeyPair")
			{
				SecurityElement securityElement2 = securityElement.SearchForChildByTag("KeyValue");
				if (securityElement2.Children.Count > 0)
				{
					this._keyvalue = securityElement2.Children[0].ToString();
				}
			}
		}

		// Token: 0x060004D8 RID: 1240 RVA: 0x0001A0B8 File Offset: 0x000182B8
		private string ToXml()
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.AppendFormat("<KeyPair>{0}\t<Properties>{0}\t\t<Provider ", Environment.NewLine);
			if (this._params.ProviderName != null && this._params.ProviderName.Length != 0)
			{
				stringBuilder.AppendFormat("Name=\"{0}\" ", this._params.ProviderName);
			}
			stringBuilder.AppendFormat("Type=\"{0}\" />{1}\t\t<Container ", this._params.ProviderType, Environment.NewLine);
			stringBuilder.AppendFormat("Name=\"{0}\" />{1}\t</Properties>{1}\t<KeyValue", this.ContainerName, Environment.NewLine);
			if (this._params.KeyNumber != -1)
			{
				stringBuilder.AppendFormat(" Id=\"{0}\" ", this._params.KeyNumber);
			}
			stringBuilder.AppendFormat(">{1}\t\t{0}{1}\t</KeyValue>{1}</KeyPair>{1}", this.KeyValue, Environment.NewLine);
			return stringBuilder.ToString();
		}

		// Token: 0x060004D9 RID: 1241 RVA: 0x0001A191 File Offset: 0x00018391
		// Note: this type is marked as 'beforefieldinit'.
		static KeyPairPersistence()
		{
		}

		// Token: 0x040005CC RID: 1484
		private static bool _userPathExists;

		// Token: 0x040005CD RID: 1485
		private static string _userPath;

		// Token: 0x040005CE RID: 1486
		private static bool _machinePathExists;

		// Token: 0x040005CF RID: 1487
		private static string _machinePath;

		// Token: 0x040005D0 RID: 1488
		private CspParameters _params;

		// Token: 0x040005D1 RID: 1489
		private string _keyvalue;

		// Token: 0x040005D2 RID: 1490
		private string _filename;

		// Token: 0x040005D3 RID: 1491
		private string _container;

		// Token: 0x040005D4 RID: 1492
		private static object lockobj = new object();
	}
}
