﻿using System;
using System.Runtime.CompilerServices;
using System.Security.Cryptography;
using System.Threading;
using Mono.Math;

namespace Mono.Security.Cryptography
{
	// Token: 0x020000A6 RID: 166
	internal class DSAManaged : DSA
	{
		// Token: 0x06000565 RID: 1381 RVA: 0x0001D888 File Offset: 0x0001BA88
		public DSAManaged() : this(1024)
		{
		}

		// Token: 0x06000566 RID: 1382 RVA: 0x0001D895 File Offset: 0x0001BA95
		public DSAManaged(int dwKeySize)
		{
			this.KeySizeValue = dwKeySize;
			this.LegalKeySizesValue = new KeySizes[1];
			this.LegalKeySizesValue[0] = new KeySizes(512, 1024, 64);
		}

		// Token: 0x06000567 RID: 1383 RVA: 0x0001D8CC File Offset: 0x0001BACC
		~DSAManaged()
		{
			this.Dispose(false);
		}

		// Token: 0x06000568 RID: 1384 RVA: 0x0001D8FC File Offset: 0x0001BAFC
		private void Generate()
		{
			this.GenerateParams(base.KeySize);
			this.GenerateKeyPair();
			this.keypairGenerated = true;
			if (this.KeyGenerated != null)
			{
				this.KeyGenerated(this, null);
			}
		}

		// Token: 0x06000569 RID: 1385 RVA: 0x0001D92C File Offset: 0x0001BB2C
		private void GenerateKeyPair()
		{
			this.x = BigInteger.GenerateRandom(160);
			while (this.x == 0U || this.x >= this.q)
			{
				this.x.Randomize();
			}
			this.y = this.g.ModPow(this.x, this.p);
		}

		// Token: 0x0600056A RID: 1386 RVA: 0x0001D994 File Offset: 0x0001BB94
		private void add(byte[] a, byte[] b, int value)
		{
			uint num = (uint)((int)(b[b.Length - 1] & byte.MaxValue) + value);
			a[b.Length - 1] = (byte)num;
			num >>= 8;
			for (int i = b.Length - 2; i >= 0; i--)
			{
				num += (uint)(b[i] & byte.MaxValue);
				a[i] = (byte)num;
				num >>= 8;
			}
		}

		// Token: 0x0600056B RID: 1387 RVA: 0x0001D9E4 File Offset: 0x0001BBE4
		private void GenerateParams(int keyLength)
		{
			byte[] array = new byte[20];
			byte[] array2 = new byte[20];
			byte[] array3 = new byte[20];
			byte[] array4 = new byte[20];
			SHA1 sha = SHA1.Create();
			int num = (keyLength - 1) / 160;
			byte[] array5 = new byte[keyLength / 8];
			bool flag = false;
			while (!flag)
			{
				do
				{
					this.Random.GetBytes(array);
					array2 = sha.ComputeHash(array);
					Array.Copy(array, 0, array3, 0, array.Length);
					this.add(array3, array, 1);
					array3 = sha.ComputeHash(array3);
					for (int num2 = 0; num2 != array4.Length; num2++)
					{
						array4[num2] = (array2[num2] ^ array3[num2]);
					}
					byte[] array6 = array4;
					int num3 = 0;
					array6[num3] |= 128;
					byte[] array7 = array4;
					int num4 = 19;
					array7[num4] |= 1;
					this.q = new BigInteger(array4);
				}
				while (!this.q.IsProbablePrime());
				this.counter = 0;
				int num5 = 2;
				while (this.counter < 4096)
				{
					for (int i = 0; i < num; i++)
					{
						this.add(array2, array, num5 + i);
						array2 = sha.ComputeHash(array2);
						Array.Copy(array2, 0, array5, array5.Length - (i + 1) * array2.Length, array2.Length);
					}
					this.add(array2, array, num5 + num);
					array2 = sha.ComputeHash(array2);
					Array.Copy(array2, array2.Length - (array5.Length - num * array2.Length), array5, 0, array5.Length - num * array2.Length);
					byte[] array8 = array5;
					int num6 = 0;
					array8[num6] |= 128;
					BigInteger bi = new BigInteger(array5);
					BigInteger bi2 = bi % (this.q * 2);
					this.p = bi - (bi2 - 1);
					if (this.p.TestBit((uint)(keyLength - 1)) && this.p.IsProbablePrime())
					{
						flag = true;
						break;
					}
					this.counter++;
					num5 += num + 1;
				}
			}
			BigInteger exp = (this.p - 1) / this.q;
			for (;;)
			{
				BigInteger bigInteger = BigInteger.GenerateRandom(keyLength);
				if (!(bigInteger <= 1) && !(bigInteger >= this.p - 1))
				{
					this.g = bigInteger.ModPow(exp, this.p);
					if (!(this.g <= 1))
					{
						break;
					}
				}
			}
			this.seed = new BigInteger(array);
			this.j = (this.p - 1) / this.q;
		}

		// Token: 0x170000FA RID: 250
		// (get) Token: 0x0600056C RID: 1388 RVA: 0x0001DC8A File Offset: 0x0001BE8A
		private RandomNumberGenerator Random
		{
			get
			{
				if (this.rng == null)
				{
					this.rng = RandomNumberGenerator.Create();
				}
				return this.rng;
			}
		}

		// Token: 0x170000FB RID: 251
		// (get) Token: 0x0600056D RID: 1389 RVA: 0x0001DCA5 File Offset: 0x0001BEA5
		public override int KeySize
		{
			get
			{
				if (this.keypairGenerated)
				{
					return this.p.BitCount();
				}
				return base.KeySize;
			}
		}

		// Token: 0x170000FC RID: 252
		// (get) Token: 0x0600056E RID: 1390 RVA: 0x0000CEAE File Offset: 0x0000B0AE
		public override string KeyExchangeAlgorithm
		{
			get
			{
				return null;
			}
		}

		// Token: 0x170000FD RID: 253
		// (get) Token: 0x0600056F RID: 1391 RVA: 0x0001DCC1 File Offset: 0x0001BEC1
		public bool PublicOnly
		{
			get
			{
				return this.keypairGenerated && this.x == null;
			}
		}

		// Token: 0x170000FE RID: 254
		// (get) Token: 0x06000570 RID: 1392 RVA: 0x0001DCD9 File Offset: 0x0001BED9
		public override string SignatureAlgorithm
		{
			get
			{
				return "http://www.w3.org/2000/09/xmldsig#dsa-sha1";
			}
		}

		// Token: 0x06000571 RID: 1393 RVA: 0x0001DCE0 File Offset: 0x0001BEE0
		private byte[] NormalizeArray(byte[] array)
		{
			int num = array.Length % 4;
			if (num > 0)
			{
				byte[] array2 = new byte[array.Length + 4 - num];
				Array.Copy(array, 0, array2, 4 - num, array.Length);
				return array2;
			}
			return array;
		}

		// Token: 0x06000572 RID: 1394 RVA: 0x0001DD18 File Offset: 0x0001BF18
		public override DSAParameters ExportParameters(bool includePrivateParameters)
		{
			if (this.m_disposed)
			{
				throw new ObjectDisposedException(Locale.GetText("Keypair was disposed"));
			}
			if (!this.keypairGenerated)
			{
				this.Generate();
			}
			if (includePrivateParameters && this.x == null)
			{
				throw new CryptographicException("no private key to export");
			}
			DSAParameters result = default(DSAParameters);
			result.P = this.NormalizeArray(this.p.GetBytes());
			result.Q = this.NormalizeArray(this.q.GetBytes());
			result.G = this.NormalizeArray(this.g.GetBytes());
			result.Y = this.NormalizeArray(this.y.GetBytes());
			if (!this.j_missing)
			{
				result.J = this.NormalizeArray(this.j.GetBytes());
			}
			if (this.seed != 0U)
			{
				result.Seed = this.NormalizeArray(this.seed.GetBytes());
				result.Counter = this.counter;
			}
			if (includePrivateParameters)
			{
				byte[] bytes = this.x.GetBytes();
				if (bytes.Length == 20)
				{
					result.X = this.NormalizeArray(bytes);
				}
			}
			return result;
		}

		// Token: 0x06000573 RID: 1395 RVA: 0x0001DE48 File Offset: 0x0001C048
		public override void ImportParameters(DSAParameters parameters)
		{
			if (this.m_disposed)
			{
				throw new ObjectDisposedException(Locale.GetText("Keypair was disposed"));
			}
			if (parameters.P == null || parameters.Q == null || parameters.G == null)
			{
				throw new CryptographicException(Locale.GetText("Missing mandatory DSA parameters (P, Q or G)."));
			}
			if (parameters.X == null && parameters.Y == null)
			{
				throw new CryptographicException(Locale.GetText("Missing both public (Y) and private (X) keys."));
			}
			this.p = new BigInteger(parameters.P);
			this.q = new BigInteger(parameters.Q);
			this.g = new BigInteger(parameters.G);
			if (parameters.X != null)
			{
				this.x = new BigInteger(parameters.X);
			}
			else
			{
				this.x = null;
			}
			if (parameters.Y != null)
			{
				this.y = new BigInteger(parameters.Y);
			}
			else
			{
				this.y = this.g.ModPow(this.x, this.p);
			}
			if (parameters.J != null)
			{
				this.j = new BigInteger(parameters.J);
			}
			else
			{
				this.j = (this.p - 1) / this.q;
				this.j_missing = true;
			}
			if (parameters.Seed != null)
			{
				this.seed = new BigInteger(parameters.Seed);
				this.counter = parameters.Counter;
			}
			else
			{
				this.seed = 0;
			}
			this.keypairGenerated = true;
		}

		// Token: 0x06000574 RID: 1396 RVA: 0x0001DFC0 File Offset: 0x0001C1C0
		public override byte[] CreateSignature(byte[] rgbHash)
		{
			if (this.m_disposed)
			{
				throw new ObjectDisposedException(Locale.GetText("Keypair was disposed"));
			}
			if (rgbHash == null)
			{
				throw new ArgumentNullException("rgbHash");
			}
			if (rgbHash.Length != 20)
			{
				throw new CryptographicException("invalid hash length");
			}
			if (!this.keypairGenerated)
			{
				this.Generate();
			}
			if (this.x == null)
			{
				throw new CryptographicException("no private key available for signature");
			}
			BigInteger bi = new BigInteger(rgbHash);
			BigInteger bigInteger = BigInteger.GenerateRandom(160);
			while (bigInteger >= this.q)
			{
				bigInteger.Randomize();
			}
			BigInteger bigInteger2 = this.g.ModPow(bigInteger, this.p) % this.q;
			BigInteger bigInteger3 = bigInteger.ModInverse(this.q) * (bi + this.x * bigInteger2) % this.q;
			byte[] array = new byte[40];
			byte[] bytes = bigInteger2.GetBytes();
			byte[] bytes2 = bigInteger3.GetBytes();
			int destinationIndex = 20 - bytes.Length;
			Array.Copy(bytes, 0, array, destinationIndex, bytes.Length);
			destinationIndex = 40 - bytes2.Length;
			Array.Copy(bytes2, 0, array, destinationIndex, bytes2.Length);
			return array;
		}

		// Token: 0x06000575 RID: 1397 RVA: 0x0001E0E8 File Offset: 0x0001C2E8
		public override bool VerifySignature(byte[] rgbHash, byte[] rgbSignature)
		{
			if (this.m_disposed)
			{
				throw new ObjectDisposedException(Locale.GetText("Keypair was disposed"));
			}
			if (rgbHash == null)
			{
				throw new ArgumentNullException("rgbHash");
			}
			if (rgbSignature == null)
			{
				throw new ArgumentNullException("rgbSignature");
			}
			if (rgbHash.Length != 20)
			{
				throw new CryptographicException("invalid hash length");
			}
			if (rgbSignature.Length != 40)
			{
				throw new CryptographicException("invalid signature length");
			}
			if (!this.keypairGenerated)
			{
				return false;
			}
			bool result;
			try
			{
				BigInteger bi = new BigInteger(rgbHash);
				byte[] array = new byte[20];
				Array.Copy(rgbSignature, 0, array, 0, 20);
				BigInteger bigInteger = new BigInteger(array);
				Array.Copy(rgbSignature, 20, array, 0, 20);
				BigInteger bigInteger2 = new BigInteger(array);
				if (bigInteger < 0 || this.q <= bigInteger)
				{
					result = false;
				}
				else if (bigInteger2 < 0 || this.q <= bigInteger2)
				{
					result = false;
				}
				else
				{
					BigInteger bi2 = bigInteger2.ModInverse(this.q);
					BigInteger bigInteger3 = bi * bi2 % this.q;
					BigInteger bigInteger4 = bigInteger * bi2 % this.q;
					bigInteger3 = this.g.ModPow(bigInteger3, this.p);
					bigInteger4 = this.y.ModPow(bigInteger4, this.p);
					result = (bigInteger3 * bigInteger4 % this.p % this.q == bigInteger);
				}
			}
			catch
			{
				throw new CryptographicException("couldn't compute signature verification");
			}
			return result;
		}

		// Token: 0x06000576 RID: 1398 RVA: 0x0001E28C File Offset: 0x0001C48C
		protected override void Dispose(bool disposing)
		{
			if (!this.m_disposed)
			{
				if (this.x != null)
				{
					this.x.Clear();
					this.x = null;
				}
				if (disposing)
				{
					if (this.p != null)
					{
						this.p.Clear();
						this.p = null;
					}
					if (this.q != null)
					{
						this.q.Clear();
						this.q = null;
					}
					if (this.g != null)
					{
						this.g.Clear();
						this.g = null;
					}
					if (this.j != null)
					{
						this.j.Clear();
						this.j = null;
					}
					if (this.seed != null)
					{
						this.seed.Clear();
						this.seed = null;
					}
					if (this.y != null)
					{
						this.y.Clear();
						this.y = null;
					}
				}
			}
			this.m_disposed = true;
		}

		// Token: 0x14000002 RID: 2
		// (add) Token: 0x06000577 RID: 1399 RVA: 0x0001E394 File Offset: 0x0001C594
		// (remove) Token: 0x06000578 RID: 1400 RVA: 0x0001E3CC File Offset: 0x0001C5CC
		public event DSAManaged.KeyGeneratedEventHandler KeyGenerated
		{
			[CompilerGenerated]
			add
			{
				DSAManaged.KeyGeneratedEventHandler keyGeneratedEventHandler = this.KeyGenerated;
				DSAManaged.KeyGeneratedEventHandler keyGeneratedEventHandler2;
				do
				{
					keyGeneratedEventHandler2 = keyGeneratedEventHandler;
					DSAManaged.KeyGeneratedEventHandler value2 = (DSAManaged.KeyGeneratedEventHandler)Delegate.Combine(keyGeneratedEventHandler2, value);
					keyGeneratedEventHandler = Interlocked.CompareExchange<DSAManaged.KeyGeneratedEventHandler>(ref this.KeyGenerated, value2, keyGeneratedEventHandler2);
				}
				while (keyGeneratedEventHandler != keyGeneratedEventHandler2);
			}
			[CompilerGenerated]
			remove
			{
				DSAManaged.KeyGeneratedEventHandler keyGeneratedEventHandler = this.KeyGenerated;
				DSAManaged.KeyGeneratedEventHandler keyGeneratedEventHandler2;
				do
				{
					keyGeneratedEventHandler2 = keyGeneratedEventHandler;
					DSAManaged.KeyGeneratedEventHandler value2 = (DSAManaged.KeyGeneratedEventHandler)Delegate.Remove(keyGeneratedEventHandler2, value);
					keyGeneratedEventHandler = Interlocked.CompareExchange<DSAManaged.KeyGeneratedEventHandler>(ref this.KeyGenerated, value2, keyGeneratedEventHandler2);
				}
				while (keyGeneratedEventHandler != keyGeneratedEventHandler2);
			}
		}

		// Token: 0x04000618 RID: 1560
		private const int defaultKeySize = 1024;

		// Token: 0x04000619 RID: 1561
		private bool keypairGenerated;

		// Token: 0x0400061A RID: 1562
		private bool m_disposed;

		// Token: 0x0400061B RID: 1563
		private BigInteger p;

		// Token: 0x0400061C RID: 1564
		private BigInteger q;

		// Token: 0x0400061D RID: 1565
		private BigInteger g;

		// Token: 0x0400061E RID: 1566
		private BigInteger x;

		// Token: 0x0400061F RID: 1567
		private BigInteger y;

		// Token: 0x04000620 RID: 1568
		private BigInteger j;

		// Token: 0x04000621 RID: 1569
		private BigInteger seed;

		// Token: 0x04000622 RID: 1570
		private int counter;

		// Token: 0x04000623 RID: 1571
		private bool j_missing;

		// Token: 0x04000624 RID: 1572
		private RandomNumberGenerator rng;

		// Token: 0x04000625 RID: 1573
		[CompilerGenerated]
		private DSAManaged.KeyGeneratedEventHandler KeyGenerated;

		// Token: 0x020000A7 RID: 167
		// (Invoke) Token: 0x0600057A RID: 1402
		public delegate void KeyGeneratedEventHandler(object sender, EventArgs e);
	}
}
