﻿using System;
using System.Security.Cryptography;

namespace Mono.Security.Cryptography
{
	// Token: 0x020000A8 RID: 168
	internal class HMACAlgorithm
	{
		// Token: 0x0600057D RID: 1405 RVA: 0x0001E401 File Offset: 0x0001C601
		public HMACAlgorithm(string algoName)
		{
			this.CreateHash(algoName);
		}

		// Token: 0x0600057E RID: 1406 RVA: 0x0001E410 File Offset: 0x0001C610
		~HMACAlgorithm()
		{
			this.Dispose();
		}

		// Token: 0x0600057F RID: 1407 RVA: 0x0001E43C File Offset: 0x0001C63C
		private void CreateHash(string algoName)
		{
			this.algo = HashAlgorithm.Create(algoName);
			this.hashName = algoName;
			this.block = new BlockProcessor(this.algo, 8);
		}

		// Token: 0x06000580 RID: 1408 RVA: 0x0001E463 File Offset: 0x0001C663
		public void Dispose()
		{
			if (this.key != null)
			{
				Array.Clear(this.key, 0, this.key.Length);
			}
		}

		// Token: 0x170000FF RID: 255
		// (get) Token: 0x06000581 RID: 1409 RVA: 0x0001E481 File Offset: 0x0001C681
		public HashAlgorithm Algo
		{
			get
			{
				return this.algo;
			}
		}

		// Token: 0x17000100 RID: 256
		// (get) Token: 0x06000582 RID: 1410 RVA: 0x0001E489 File Offset: 0x0001C689
		// (set) Token: 0x06000583 RID: 1411 RVA: 0x0001E491 File Offset: 0x0001C691
		public string HashName
		{
			get
			{
				return this.hashName;
			}
			set
			{
				this.CreateHash(value);
			}
		}

		// Token: 0x17000101 RID: 257
		// (get) Token: 0x06000584 RID: 1412 RVA: 0x0001E49A File Offset: 0x0001C69A
		// (set) Token: 0x06000585 RID: 1413 RVA: 0x0001E4A2 File Offset: 0x0001C6A2
		public byte[] Key
		{
			get
			{
				return this.key;
			}
			set
			{
				if (value != null && value.Length > 64)
				{
					this.key = this.algo.ComputeHash(value);
					return;
				}
				this.key = (byte[])value.Clone();
			}
		}

		// Token: 0x06000586 RID: 1414 RVA: 0x0001E4D4 File Offset: 0x0001C6D4
		public void Initialize()
		{
			this.hash = null;
			this.block.Initialize();
			byte[] array = this.KeySetup(this.key, 54);
			this.algo.Initialize();
			this.block.Core(array);
			Array.Clear(array, 0, array.Length);
		}

		// Token: 0x06000587 RID: 1415 RVA: 0x0001E524 File Offset: 0x0001C724
		private byte[] KeySetup(byte[] key, byte padding)
		{
			byte[] array = new byte[64];
			for (int i = 0; i < key.Length; i++)
			{
				array[i] = (key[i] ^ padding);
			}
			for (int j = key.Length; j < 64; j++)
			{
				array[j] = padding;
			}
			return array;
		}

		// Token: 0x06000588 RID: 1416 RVA: 0x0001E564 File Offset: 0x0001C764
		public void Core(byte[] rgb, int ib, int cb)
		{
			this.block.Core(rgb, ib, cb);
		}

		// Token: 0x06000589 RID: 1417 RVA: 0x0001E574 File Offset: 0x0001C774
		public byte[] Final()
		{
			this.block.Final();
			byte[] array = this.algo.Hash;
			byte[] array2 = this.KeySetup(this.key, 92);
			this.algo.Initialize();
			this.algo.TransformBlock(array2, 0, array2.Length, array2, 0);
			this.algo.TransformFinalBlock(array, 0, array.Length);
			this.hash = this.algo.Hash;
			this.algo.Clear();
			Array.Clear(array2, 0, array2.Length);
			Array.Clear(array, 0, array.Length);
			return this.hash;
		}

		// Token: 0x04000626 RID: 1574
		private byte[] key;

		// Token: 0x04000627 RID: 1575
		private byte[] hash;

		// Token: 0x04000628 RID: 1576
		private HashAlgorithm algo;

		// Token: 0x04000629 RID: 1577
		private string hashName;

		// Token: 0x0400062A RID: 1578
		private BlockProcessor block;
	}
}
