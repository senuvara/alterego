﻿using System;
using System.Security.Cryptography;

namespace Mono.Security.Cryptography
{
	// Token: 0x02000094 RID: 148
	internal class ARC4Managed : RC4, ICryptoTransform, IDisposable
	{
		// Token: 0x06000485 RID: 1157 RVA: 0x000186F6 File Offset: 0x000168F6
		public ARC4Managed()
		{
			this.state = new byte[256];
			this.m_disposed = false;
		}

		// Token: 0x06000486 RID: 1158 RVA: 0x00018718 File Offset: 0x00016918
		~ARC4Managed()
		{
			this.Dispose(true);
		}

		// Token: 0x06000487 RID: 1159 RVA: 0x00018748 File Offset: 0x00016948
		protected override void Dispose(bool disposing)
		{
			if (!this.m_disposed)
			{
				this.x = 0;
				this.y = 0;
				if (this.key != null)
				{
					Array.Clear(this.key, 0, this.key.Length);
					this.key = null;
				}
				Array.Clear(this.state, 0, this.state.Length);
				this.state = null;
				GC.SuppressFinalize(this);
				this.m_disposed = true;
			}
		}

		// Token: 0x170000D7 RID: 215
		// (get) Token: 0x06000488 RID: 1160 RVA: 0x000187B6 File Offset: 0x000169B6
		// (set) Token: 0x06000489 RID: 1161 RVA: 0x000187D8 File Offset: 0x000169D8
		public override byte[] Key
		{
			get
			{
				if (this.KeyValue == null)
				{
					this.GenerateKey();
				}
				return (byte[])this.KeyValue.Clone();
			}
			set
			{
				if (value == null)
				{
					throw new ArgumentNullException("Key");
				}
				this.KeyValue = (this.key = (byte[])value.Clone());
				this.KeySetup(this.key);
			}
		}

		// Token: 0x170000D8 RID: 216
		// (get) Token: 0x0600048A RID: 1162 RVA: 0x00002526 File Offset: 0x00000726
		public bool CanReuseTransform
		{
			get
			{
				return false;
			}
		}

		// Token: 0x0600048B RID: 1163 RVA: 0x00018819 File Offset: 0x00016A19
		public override ICryptoTransform CreateEncryptor(byte[] rgbKey, byte[] rgvIV)
		{
			this.Key = rgbKey;
			return this;
		}

		// Token: 0x0600048C RID: 1164 RVA: 0x00018823 File Offset: 0x00016A23
		public override ICryptoTransform CreateDecryptor(byte[] rgbKey, byte[] rgvIV)
		{
			this.Key = rgbKey;
			return this.CreateEncryptor();
		}

		// Token: 0x0600048D RID: 1165 RVA: 0x00018832 File Offset: 0x00016A32
		public override void GenerateIV()
		{
			this.IV = new byte[0];
		}

		// Token: 0x0600048E RID: 1166 RVA: 0x00018840 File Offset: 0x00016A40
		public override void GenerateKey()
		{
			this.KeyValue = KeyBuilder.Key(this.KeySizeValue >> 3);
		}

		// Token: 0x170000D9 RID: 217
		// (get) Token: 0x0600048F RID: 1167 RVA: 0x00004E08 File Offset: 0x00003008
		public bool CanTransformMultipleBlocks
		{
			get
			{
				return true;
			}
		}

		// Token: 0x170000DA RID: 218
		// (get) Token: 0x06000490 RID: 1168 RVA: 0x00004E08 File Offset: 0x00003008
		public int InputBlockSize
		{
			get
			{
				return 1;
			}
		}

		// Token: 0x170000DB RID: 219
		// (get) Token: 0x06000491 RID: 1169 RVA: 0x00004E08 File Offset: 0x00003008
		public int OutputBlockSize
		{
			get
			{
				return 1;
			}
		}

		// Token: 0x06000492 RID: 1170 RVA: 0x00018858 File Offset: 0x00016A58
		private void KeySetup(byte[] key)
		{
			byte b = 0;
			byte b2 = 0;
			for (int i = 0; i < 256; i++)
			{
				this.state[i] = (byte)i;
			}
			this.x = 0;
			this.y = 0;
			for (int j = 0; j < 256; j++)
			{
				b2 = key[(int)b] + this.state[j] + b2;
				byte b3 = this.state[j];
				this.state[j] = this.state[(int)b2];
				this.state[(int)b2] = b3;
				b = (byte)((int)(b + 1) % key.Length);
			}
		}

		// Token: 0x06000493 RID: 1171 RVA: 0x000188E0 File Offset: 0x00016AE0
		private void CheckInput(byte[] inputBuffer, int inputOffset, int inputCount)
		{
			if (inputBuffer == null)
			{
				throw new ArgumentNullException("inputBuffer");
			}
			if (inputOffset < 0)
			{
				throw new ArgumentOutOfRangeException("inputOffset", "< 0");
			}
			if (inputCount < 0)
			{
				throw new ArgumentOutOfRangeException("inputCount", "< 0");
			}
			if (inputOffset > inputBuffer.Length - inputCount)
			{
				throw new ArgumentException(Locale.GetText("Overflow"), "inputBuffer");
			}
		}

		// Token: 0x06000494 RID: 1172 RVA: 0x00018940 File Offset: 0x00016B40
		public int TransformBlock(byte[] inputBuffer, int inputOffset, int inputCount, byte[] outputBuffer, int outputOffset)
		{
			this.CheckInput(inputBuffer, inputOffset, inputCount);
			if (outputBuffer == null)
			{
				throw new ArgumentNullException("outputBuffer");
			}
			if (outputOffset < 0)
			{
				throw new ArgumentOutOfRangeException("outputOffset", "< 0");
			}
			if (outputOffset > outputBuffer.Length - inputCount)
			{
				throw new ArgumentException(Locale.GetText("Overflow"), "outputBuffer");
			}
			return this.InternalTransformBlock(inputBuffer, inputOffset, inputCount, outputBuffer, outputOffset);
		}

		// Token: 0x06000495 RID: 1173 RVA: 0x000189A8 File Offset: 0x00016BA8
		private int InternalTransformBlock(byte[] inputBuffer, int inputOffset, int inputCount, byte[] outputBuffer, int outputOffset)
		{
			for (int i = 0; i < inputCount; i++)
			{
				this.x += 1;
				this.y = this.state[(int)this.x] + this.y;
				byte b = this.state[(int)this.x];
				this.state[(int)this.x] = this.state[(int)this.y];
				this.state[(int)this.y] = b;
				byte b2 = this.state[(int)this.x] + this.state[(int)this.y];
				outputBuffer[outputOffset + i] = (inputBuffer[inputOffset + i] ^ this.state[(int)b2]);
			}
			return inputCount;
		}

		// Token: 0x06000496 RID: 1174 RVA: 0x00018A5C File Offset: 0x00016C5C
		public byte[] TransformFinalBlock(byte[] inputBuffer, int inputOffset, int inputCount)
		{
			this.CheckInput(inputBuffer, inputOffset, inputCount);
			byte[] array = new byte[inputCount];
			this.InternalTransformBlock(inputBuffer, inputOffset, inputCount, array, 0);
			return array;
		}

		// Token: 0x040005C2 RID: 1474
		private byte[] key;

		// Token: 0x040005C3 RID: 1475
		private byte[] state;

		// Token: 0x040005C4 RID: 1476
		private byte x;

		// Token: 0x040005C5 RID: 1477
		private byte y;

		// Token: 0x040005C6 RID: 1478
		private bool m_disposed;
	}
}
