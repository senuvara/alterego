﻿using System;
using System.Security.Cryptography;

namespace Mono.Security.Cryptography
{
	// Token: 0x02000097 RID: 151
	internal class BlockProcessor
	{
		// Token: 0x060004B6 RID: 1206 RVA: 0x00019928 File Offset: 0x00017B28
		public BlockProcessor(ICryptoTransform transform) : this(transform, transform.InputBlockSize)
		{
		}

		// Token: 0x060004B7 RID: 1207 RVA: 0x00019937 File Offset: 0x00017B37
		public BlockProcessor(ICryptoTransform transform, int blockSize)
		{
			if (transform == null)
			{
				throw new ArgumentNullException("transform");
			}
			if (blockSize <= 0)
			{
				throw new ArgumentOutOfRangeException("blockSize");
			}
			this.transform = transform;
			this.blockSize = blockSize;
			this.block = new byte[blockSize];
		}

		// Token: 0x060004B8 RID: 1208 RVA: 0x00019978 File Offset: 0x00017B78
		~BlockProcessor()
		{
			Array.Clear(this.block, 0, this.blockSize);
		}

		// Token: 0x060004B9 RID: 1209 RVA: 0x000199B0 File Offset: 0x00017BB0
		public void Initialize()
		{
			Array.Clear(this.block, 0, this.blockSize);
			this.blockCount = 0;
		}

		// Token: 0x060004BA RID: 1210 RVA: 0x000199CB File Offset: 0x00017BCB
		public void Core(byte[] rgb)
		{
			this.Core(rgb, 0, rgb.Length);
		}

		// Token: 0x060004BB RID: 1211 RVA: 0x000199D8 File Offset: 0x00017BD8
		public void Core(byte[] rgb, int ib, int cb)
		{
			int num = Math.Min(this.blockSize - this.blockCount, cb);
			Buffer.BlockCopy(rgb, ib, this.block, this.blockCount, num);
			this.blockCount += num;
			if (this.blockCount == this.blockSize)
			{
				this.transform.TransformBlock(this.block, 0, this.blockSize, this.block, 0);
				int num2 = (cb - num) / this.blockSize;
				for (int i = 0; i < num2; i++)
				{
					this.transform.TransformBlock(rgb, num + ib, this.blockSize, this.block, 0);
					num += this.blockSize;
				}
				this.blockCount = cb - num;
				if (this.blockCount > 0)
				{
					Buffer.BlockCopy(rgb, num + ib, this.block, 0, this.blockCount);
				}
			}
		}

		// Token: 0x060004BC RID: 1212 RVA: 0x00019AB1 File Offset: 0x00017CB1
		public byte[] Final()
		{
			return this.transform.TransformFinalBlock(this.block, 0, this.blockCount);
		}

		// Token: 0x040005C8 RID: 1480
		private ICryptoTransform transform;

		// Token: 0x040005C9 RID: 1481
		private byte[] block;

		// Token: 0x040005CA RID: 1482
		private int blockSize;

		// Token: 0x040005CB RID: 1483
		private int blockCount;
	}
}
