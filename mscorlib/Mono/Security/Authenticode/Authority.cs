﻿using System;

namespace Mono.Security.Authenticode
{
	// Token: 0x020000AA RID: 170
	internal enum Authority
	{
		// Token: 0x04000631 RID: 1585
		Individual,
		// Token: 0x04000632 RID: 1586
		Commercial,
		// Token: 0x04000633 RID: 1587
		Maximum
	}
}
