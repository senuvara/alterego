﻿using System;
using System.Collections;
using System.Security.Cryptography;
using Mono.Security.X509;

namespace Mono.Security
{
	// Token: 0x02000058 RID: 88
	internal sealed class PKCS7
	{
		// Token: 0x06000272 RID: 626 RVA: 0x00002050 File Offset: 0x00000250
		private PKCS7()
		{
		}

		// Token: 0x06000273 RID: 627 RVA: 0x0000CAF0 File Offset: 0x0000ACF0
		public static ASN1 Attribute(string oid, ASN1 value)
		{
			ASN1 asn = new ASN1(48);
			asn.Add(ASN1Convert.FromOid(oid));
			asn.Add(new ASN1(49)).Add(value);
			return asn;
		}

		// Token: 0x06000274 RID: 628 RVA: 0x0000CB1A File Offset: 0x0000AD1A
		public static ASN1 AlgorithmIdentifier(string oid)
		{
			ASN1 asn = new ASN1(48);
			asn.Add(ASN1Convert.FromOid(oid));
			asn.Add(new ASN1(5));
			return asn;
		}

		// Token: 0x06000275 RID: 629 RVA: 0x0000CB3D File Offset: 0x0000AD3D
		public static ASN1 AlgorithmIdentifier(string oid, ASN1 parameters)
		{
			ASN1 asn = new ASN1(48);
			asn.Add(ASN1Convert.FromOid(oid));
			asn.Add(parameters);
			return asn;
		}

		// Token: 0x06000276 RID: 630 RVA: 0x0000CB5C File Offset: 0x0000AD5C
		public static ASN1 IssuerAndSerialNumber(X509Certificate x509)
		{
			ASN1 asn = null;
			ASN1 asn2 = null;
			ASN1 asn3 = new ASN1(x509.RawData);
			int i = 0;
			bool flag = false;
			while (i < asn3[0].Count)
			{
				ASN1 asn4 = asn3[0][i++];
				if (asn4.Tag == 2)
				{
					asn2 = asn4;
				}
				else if (asn4.Tag == 48)
				{
					if (flag)
					{
						asn = asn4;
						break;
					}
					flag = true;
				}
			}
			ASN1 asn5 = new ASN1(48);
			asn5.Add(asn);
			asn5.Add(asn2);
			return asn5;
		}

		// Token: 0x02000059 RID: 89
		public class Oid
		{
			// Token: 0x06000277 RID: 631 RVA: 0x00002050 File Offset: 0x00000250
			public Oid()
			{
			}

			// Token: 0x040004B6 RID: 1206
			public const string rsaEncryption = "1.2.840.113549.1.1.1";

			// Token: 0x040004B7 RID: 1207
			public const string data = "1.2.840.113549.1.7.1";

			// Token: 0x040004B8 RID: 1208
			public const string signedData = "1.2.840.113549.1.7.2";

			// Token: 0x040004B9 RID: 1209
			public const string envelopedData = "1.2.840.113549.1.7.3";

			// Token: 0x040004BA RID: 1210
			public const string signedAndEnvelopedData = "1.2.840.113549.1.7.4";

			// Token: 0x040004BB RID: 1211
			public const string digestedData = "1.2.840.113549.1.7.5";

			// Token: 0x040004BC RID: 1212
			public const string encryptedData = "1.2.840.113549.1.7.6";

			// Token: 0x040004BD RID: 1213
			public const string contentType = "1.2.840.113549.1.9.3";

			// Token: 0x040004BE RID: 1214
			public const string messageDigest = "1.2.840.113549.1.9.4";

			// Token: 0x040004BF RID: 1215
			public const string signingTime = "1.2.840.113549.1.9.5";

			// Token: 0x040004C0 RID: 1216
			public const string countersignature = "1.2.840.113549.1.9.6";
		}

		// Token: 0x0200005A RID: 90
		public class ContentInfo
		{
			// Token: 0x06000278 RID: 632 RVA: 0x0000CBDF File Offset: 0x0000ADDF
			public ContentInfo()
			{
				this.content = new ASN1(160);
			}

			// Token: 0x06000279 RID: 633 RVA: 0x0000CBF7 File Offset: 0x0000ADF7
			public ContentInfo(string oid) : this()
			{
				this.contentType = oid;
			}

			// Token: 0x0600027A RID: 634 RVA: 0x0000CC06 File Offset: 0x0000AE06
			public ContentInfo(byte[] data) : this(new ASN1(data))
			{
			}

			// Token: 0x0600027B RID: 635 RVA: 0x0000CC14 File Offset: 0x0000AE14
			public ContentInfo(ASN1 asn1)
			{
				if (asn1.Tag != 48 || (asn1.Count < 1 && asn1.Count > 2))
				{
					throw new ArgumentException("Invalid ASN1");
				}
				if (asn1[0].Tag != 6)
				{
					throw new ArgumentException("Invalid contentType");
				}
				this.contentType = ASN1Convert.ToOid(asn1[0]);
				if (asn1.Count > 1)
				{
					if (asn1[1].Tag != 160)
					{
						throw new ArgumentException("Invalid content");
					}
					this.content = asn1[1];
				}
			}

			// Token: 0x17000034 RID: 52
			// (get) Token: 0x0600027C RID: 636 RVA: 0x0000CCAE File Offset: 0x0000AEAE
			public ASN1 ASN1
			{
				get
				{
					return this.GetASN1();
				}
			}

			// Token: 0x17000035 RID: 53
			// (get) Token: 0x0600027D RID: 637 RVA: 0x0000CCB6 File Offset: 0x0000AEB6
			// (set) Token: 0x0600027E RID: 638 RVA: 0x0000CCBE File Offset: 0x0000AEBE
			public ASN1 Content
			{
				get
				{
					return this.content;
				}
				set
				{
					this.content = value;
				}
			}

			// Token: 0x17000036 RID: 54
			// (get) Token: 0x0600027F RID: 639 RVA: 0x0000CCC7 File Offset: 0x0000AEC7
			// (set) Token: 0x06000280 RID: 640 RVA: 0x0000CCCF File Offset: 0x0000AECF
			public string ContentType
			{
				get
				{
					return this.contentType;
				}
				set
				{
					this.contentType = value;
				}
			}

			// Token: 0x06000281 RID: 641 RVA: 0x0000CCD8 File Offset: 0x0000AED8
			internal ASN1 GetASN1()
			{
				ASN1 asn = new ASN1(48);
				asn.Add(ASN1Convert.FromOid(this.contentType));
				if (this.content != null && this.content.Count > 0)
				{
					asn.Add(this.content);
				}
				return asn;
			}

			// Token: 0x06000282 RID: 642 RVA: 0x0000CD23 File Offset: 0x0000AF23
			public byte[] GetBytes()
			{
				return this.GetASN1().GetBytes();
			}

			// Token: 0x040004C1 RID: 1217
			private string contentType;

			// Token: 0x040004C2 RID: 1218
			private ASN1 content;
		}

		// Token: 0x0200005B RID: 91
		public class EncryptedData
		{
			// Token: 0x06000283 RID: 643 RVA: 0x0000CD30 File Offset: 0x0000AF30
			public EncryptedData()
			{
				this._version = 0;
			}

			// Token: 0x06000284 RID: 644 RVA: 0x0000CD3F File Offset: 0x0000AF3F
			public EncryptedData(byte[] data) : this(new ASN1(data))
			{
			}

			// Token: 0x06000285 RID: 645 RVA: 0x0000CD50 File Offset: 0x0000AF50
			public EncryptedData(ASN1 asn1) : this()
			{
				if (asn1.Tag != 48 || asn1.Count < 2)
				{
					throw new ArgumentException("Invalid EncryptedData");
				}
				if (asn1[0].Tag != 2)
				{
					throw new ArgumentException("Invalid version");
				}
				this._version = asn1[0].Value[0];
				ASN1 asn2 = asn1[1];
				if (asn2.Tag != 48)
				{
					throw new ArgumentException("missing EncryptedContentInfo");
				}
				ASN1 asn3 = asn2[0];
				if (asn3.Tag != 6)
				{
					throw new ArgumentException("missing EncryptedContentInfo.ContentType");
				}
				this._content = new PKCS7.ContentInfo(ASN1Convert.ToOid(asn3));
				ASN1 asn4 = asn2[1];
				if (asn4.Tag != 48)
				{
					throw new ArgumentException("missing EncryptedContentInfo.ContentEncryptionAlgorithmIdentifier");
				}
				this._encryptionAlgorithm = new PKCS7.ContentInfo(ASN1Convert.ToOid(asn4[0]));
				this._encryptionAlgorithm.Content = asn4[1];
				ASN1 asn5 = asn2[2];
				if (asn5.Tag != 128)
				{
					throw new ArgumentException("missing EncryptedContentInfo.EncryptedContent");
				}
				this._encrypted = asn5.Value;
			}

			// Token: 0x17000037 RID: 55
			// (get) Token: 0x06000286 RID: 646 RVA: 0x0000CE69 File Offset: 0x0000B069
			public ASN1 ASN1
			{
				get
				{
					return this.GetASN1();
				}
			}

			// Token: 0x17000038 RID: 56
			// (get) Token: 0x06000287 RID: 647 RVA: 0x0000CE71 File Offset: 0x0000B071
			public PKCS7.ContentInfo ContentInfo
			{
				get
				{
					return this._content;
				}
			}

			// Token: 0x17000039 RID: 57
			// (get) Token: 0x06000288 RID: 648 RVA: 0x0000CE79 File Offset: 0x0000B079
			public PKCS7.ContentInfo EncryptionAlgorithm
			{
				get
				{
					return this._encryptionAlgorithm;
				}
			}

			// Token: 0x1700003A RID: 58
			// (get) Token: 0x06000289 RID: 649 RVA: 0x0000CE81 File Offset: 0x0000B081
			public byte[] EncryptedContent
			{
				get
				{
					if (this._encrypted == null)
					{
						return null;
					}
					return (byte[])this._encrypted.Clone();
				}
			}

			// Token: 0x1700003B RID: 59
			// (get) Token: 0x0600028A RID: 650 RVA: 0x0000CE9D File Offset: 0x0000B09D
			// (set) Token: 0x0600028B RID: 651 RVA: 0x0000CEA5 File Offset: 0x0000B0A5
			public byte Version
			{
				get
				{
					return this._version;
				}
				set
				{
					this._version = value;
				}
			}

			// Token: 0x0600028C RID: 652 RVA: 0x0000CEAE File Offset: 0x0000B0AE
			internal ASN1 GetASN1()
			{
				return null;
			}

			// Token: 0x0600028D RID: 653 RVA: 0x0000CEB1 File Offset: 0x0000B0B1
			public byte[] GetBytes()
			{
				return this.GetASN1().GetBytes();
			}

			// Token: 0x040004C3 RID: 1219
			private byte _version;

			// Token: 0x040004C4 RID: 1220
			private PKCS7.ContentInfo _content;

			// Token: 0x040004C5 RID: 1221
			private PKCS7.ContentInfo _encryptionAlgorithm;

			// Token: 0x040004C6 RID: 1222
			private byte[] _encrypted;
		}

		// Token: 0x0200005C RID: 92
		public class EnvelopedData
		{
			// Token: 0x0600028E RID: 654 RVA: 0x0000CEBE File Offset: 0x0000B0BE
			public EnvelopedData()
			{
				this._version = 0;
				this._content = new PKCS7.ContentInfo();
				this._encryptionAlgorithm = new PKCS7.ContentInfo();
				this._recipientInfos = new ArrayList();
			}

			// Token: 0x0600028F RID: 655 RVA: 0x0000CEEE File Offset: 0x0000B0EE
			public EnvelopedData(byte[] data) : this(new ASN1(data))
			{
			}

			// Token: 0x06000290 RID: 656 RVA: 0x0000CEFC File Offset: 0x0000B0FC
			public EnvelopedData(ASN1 asn1) : this()
			{
				if (asn1[0].Tag != 48 || asn1[0].Count < 3)
				{
					throw new ArgumentException("Invalid EnvelopedData");
				}
				if (asn1[0][0].Tag != 2)
				{
					throw new ArgumentException("Invalid version");
				}
				this._version = asn1[0][0].Value[0];
				ASN1 asn2 = asn1[0][1];
				if (asn2.Tag != 49)
				{
					throw new ArgumentException("missing RecipientInfos");
				}
				for (int i = 0; i < asn2.Count; i++)
				{
					ASN1 data = asn2[i];
					this._recipientInfos.Add(new PKCS7.RecipientInfo(data));
				}
				ASN1 asn3 = asn1[0][2];
				if (asn3.Tag != 48)
				{
					throw new ArgumentException("missing EncryptedContentInfo");
				}
				ASN1 asn4 = asn3[0];
				if (asn4.Tag != 6)
				{
					throw new ArgumentException("missing EncryptedContentInfo.ContentType");
				}
				this._content = new PKCS7.ContentInfo(ASN1Convert.ToOid(asn4));
				ASN1 asn5 = asn3[1];
				if (asn5.Tag != 48)
				{
					throw new ArgumentException("missing EncryptedContentInfo.ContentEncryptionAlgorithmIdentifier");
				}
				this._encryptionAlgorithm = new PKCS7.ContentInfo(ASN1Convert.ToOid(asn5[0]));
				this._encryptionAlgorithm.Content = asn5[1];
				ASN1 asn6 = asn3[2];
				if (asn6.Tag != 128)
				{
					throw new ArgumentException("missing EncryptedContentInfo.EncryptedContent");
				}
				this._encrypted = asn6.Value;
			}

			// Token: 0x1700003C RID: 60
			// (get) Token: 0x06000291 RID: 657 RVA: 0x0000D088 File Offset: 0x0000B288
			public ArrayList RecipientInfos
			{
				get
				{
					return this._recipientInfos;
				}
			}

			// Token: 0x1700003D RID: 61
			// (get) Token: 0x06000292 RID: 658 RVA: 0x0000D090 File Offset: 0x0000B290
			public ASN1 ASN1
			{
				get
				{
					return this.GetASN1();
				}
			}

			// Token: 0x1700003E RID: 62
			// (get) Token: 0x06000293 RID: 659 RVA: 0x0000D098 File Offset: 0x0000B298
			public PKCS7.ContentInfo ContentInfo
			{
				get
				{
					return this._content;
				}
			}

			// Token: 0x1700003F RID: 63
			// (get) Token: 0x06000294 RID: 660 RVA: 0x0000D0A0 File Offset: 0x0000B2A0
			public PKCS7.ContentInfo EncryptionAlgorithm
			{
				get
				{
					return this._encryptionAlgorithm;
				}
			}

			// Token: 0x17000040 RID: 64
			// (get) Token: 0x06000295 RID: 661 RVA: 0x0000D0A8 File Offset: 0x0000B2A8
			public byte[] EncryptedContent
			{
				get
				{
					if (this._encrypted == null)
					{
						return null;
					}
					return (byte[])this._encrypted.Clone();
				}
			}

			// Token: 0x17000041 RID: 65
			// (get) Token: 0x06000296 RID: 662 RVA: 0x0000D0C4 File Offset: 0x0000B2C4
			// (set) Token: 0x06000297 RID: 663 RVA: 0x0000D0CC File Offset: 0x0000B2CC
			public byte Version
			{
				get
				{
					return this._version;
				}
				set
				{
					this._version = value;
				}
			}

			// Token: 0x06000298 RID: 664 RVA: 0x0000D0D5 File Offset: 0x0000B2D5
			internal ASN1 GetASN1()
			{
				return new ASN1(48);
			}

			// Token: 0x06000299 RID: 665 RVA: 0x0000D0DE File Offset: 0x0000B2DE
			public byte[] GetBytes()
			{
				return this.GetASN1().GetBytes();
			}

			// Token: 0x040004C7 RID: 1223
			private byte _version;

			// Token: 0x040004C8 RID: 1224
			private PKCS7.ContentInfo _content;

			// Token: 0x040004C9 RID: 1225
			private PKCS7.ContentInfo _encryptionAlgorithm;

			// Token: 0x040004CA RID: 1226
			private ArrayList _recipientInfos;

			// Token: 0x040004CB RID: 1227
			private byte[] _encrypted;
		}

		// Token: 0x0200005D RID: 93
		public class RecipientInfo
		{
			// Token: 0x0600029A RID: 666 RVA: 0x00002050 File Offset: 0x00000250
			public RecipientInfo()
			{
			}

			// Token: 0x0600029B RID: 667 RVA: 0x0000D0EC File Offset: 0x0000B2EC
			public RecipientInfo(ASN1 data)
			{
				if (data.Tag != 48)
				{
					throw new ArgumentException("Invalid RecipientInfo");
				}
				ASN1 asn = data[0];
				if (asn.Tag != 2)
				{
					throw new ArgumentException("missing Version");
				}
				this._version = (int)asn.Value[0];
				ASN1 asn2 = data[1];
				if (asn2.Tag == 128 && this._version == 3)
				{
					this._ski = asn2.Value;
				}
				else
				{
					this._issuer = X501.ToString(asn2[0]);
					this._serial = asn2[1].Value;
				}
				ASN1 asn3 = data[2];
				this._oid = ASN1Convert.ToOid(asn3[0]);
				ASN1 asn4 = data[3];
				this._key = asn4.Value;
			}

			// Token: 0x17000042 RID: 66
			// (get) Token: 0x0600029C RID: 668 RVA: 0x0000D1BC File Offset: 0x0000B3BC
			public string Oid
			{
				get
				{
					return this._oid;
				}
			}

			// Token: 0x17000043 RID: 67
			// (get) Token: 0x0600029D RID: 669 RVA: 0x0000D1C4 File Offset: 0x0000B3C4
			public byte[] Key
			{
				get
				{
					if (this._key == null)
					{
						return null;
					}
					return (byte[])this._key.Clone();
				}
			}

			// Token: 0x17000044 RID: 68
			// (get) Token: 0x0600029E RID: 670 RVA: 0x0000D1E0 File Offset: 0x0000B3E0
			public byte[] SubjectKeyIdentifier
			{
				get
				{
					if (this._ski == null)
					{
						return null;
					}
					return (byte[])this._ski.Clone();
				}
			}

			// Token: 0x17000045 RID: 69
			// (get) Token: 0x0600029F RID: 671 RVA: 0x0000D1FC File Offset: 0x0000B3FC
			public string Issuer
			{
				get
				{
					return this._issuer;
				}
			}

			// Token: 0x17000046 RID: 70
			// (get) Token: 0x060002A0 RID: 672 RVA: 0x0000D204 File Offset: 0x0000B404
			public byte[] Serial
			{
				get
				{
					if (this._serial == null)
					{
						return null;
					}
					return (byte[])this._serial.Clone();
				}
			}

			// Token: 0x17000047 RID: 71
			// (get) Token: 0x060002A1 RID: 673 RVA: 0x0000D220 File Offset: 0x0000B420
			public int Version
			{
				get
				{
					return this._version;
				}
			}

			// Token: 0x040004CC RID: 1228
			private int _version;

			// Token: 0x040004CD RID: 1229
			private string _oid;

			// Token: 0x040004CE RID: 1230
			private byte[] _key;

			// Token: 0x040004CF RID: 1231
			private byte[] _ski;

			// Token: 0x040004D0 RID: 1232
			private string _issuer;

			// Token: 0x040004D1 RID: 1233
			private byte[] _serial;
		}

		// Token: 0x0200005E RID: 94
		public class SignedData
		{
			// Token: 0x060002A2 RID: 674 RVA: 0x0000D228 File Offset: 0x0000B428
			public SignedData()
			{
				this.version = 1;
				this.contentInfo = new PKCS7.ContentInfo();
				this.certs = new X509CertificateCollection();
				this.crls = new ArrayList();
				this.signerInfo = new PKCS7.SignerInfo();
				this.mda = true;
				this.signed = false;
			}

			// Token: 0x060002A3 RID: 675 RVA: 0x0000D27C File Offset: 0x0000B47C
			public SignedData(byte[] data) : this(new ASN1(data))
			{
			}

			// Token: 0x060002A4 RID: 676 RVA: 0x0000D28C File Offset: 0x0000B48C
			public SignedData(ASN1 asn1)
			{
				if (asn1[0].Tag != 48 || asn1[0].Count < 4)
				{
					throw new ArgumentException("Invalid SignedData");
				}
				if (asn1[0][0].Tag != 2)
				{
					throw new ArgumentException("Invalid version");
				}
				this.version = asn1[0][0].Value[0];
				this.contentInfo = new PKCS7.ContentInfo(asn1[0][2]);
				int num = 3;
				this.certs = new X509CertificateCollection();
				if (asn1[0][num].Tag == 160)
				{
					for (int i = 0; i < asn1[0][num].Count; i++)
					{
						this.certs.Add(new X509Certificate(asn1[0][num][i].GetBytes()));
					}
					num++;
				}
				this.crls = new ArrayList();
				if (asn1[0][num].Tag == 161)
				{
					for (int j = 0; j < asn1[0][num].Count; j++)
					{
						this.crls.Add(asn1[0][num][j].GetBytes());
					}
					num++;
				}
				if (asn1[0][num].Count > 0)
				{
					this.signerInfo = new PKCS7.SignerInfo(asn1[0][num]);
				}
				else
				{
					this.signerInfo = new PKCS7.SignerInfo();
				}
				if (this.signerInfo.HashName != null)
				{
					this.HashName = this.OidToName(this.signerInfo.HashName);
				}
				this.mda = (this.signerInfo.AuthenticatedAttributes.Count > 0);
			}

			// Token: 0x17000048 RID: 72
			// (get) Token: 0x060002A5 RID: 677 RVA: 0x0000D46B File Offset: 0x0000B66B
			public ASN1 ASN1
			{
				get
				{
					return this.GetASN1();
				}
			}

			// Token: 0x17000049 RID: 73
			// (get) Token: 0x060002A6 RID: 678 RVA: 0x0000D473 File Offset: 0x0000B673
			public X509CertificateCollection Certificates
			{
				get
				{
					return this.certs;
				}
			}

			// Token: 0x1700004A RID: 74
			// (get) Token: 0x060002A7 RID: 679 RVA: 0x0000D47B File Offset: 0x0000B67B
			public PKCS7.ContentInfo ContentInfo
			{
				get
				{
					return this.contentInfo;
				}
			}

			// Token: 0x1700004B RID: 75
			// (get) Token: 0x060002A8 RID: 680 RVA: 0x0000D483 File Offset: 0x0000B683
			public ArrayList Crls
			{
				get
				{
					return this.crls;
				}
			}

			// Token: 0x1700004C RID: 76
			// (get) Token: 0x060002A9 RID: 681 RVA: 0x0000D48B File Offset: 0x0000B68B
			// (set) Token: 0x060002AA RID: 682 RVA: 0x0000D493 File Offset: 0x0000B693
			public string HashName
			{
				get
				{
					return this.hashAlgorithm;
				}
				set
				{
					this.hashAlgorithm = value;
					this.signerInfo.HashName = value;
				}
			}

			// Token: 0x1700004D RID: 77
			// (get) Token: 0x060002AB RID: 683 RVA: 0x0000D4A8 File Offset: 0x0000B6A8
			public PKCS7.SignerInfo SignerInfo
			{
				get
				{
					return this.signerInfo;
				}
			}

			// Token: 0x1700004E RID: 78
			// (get) Token: 0x060002AC RID: 684 RVA: 0x0000D4B0 File Offset: 0x0000B6B0
			// (set) Token: 0x060002AD RID: 685 RVA: 0x0000D4B8 File Offset: 0x0000B6B8
			public byte Version
			{
				get
				{
					return this.version;
				}
				set
				{
					this.version = value;
				}
			}

			// Token: 0x1700004F RID: 79
			// (get) Token: 0x060002AE RID: 686 RVA: 0x0000D4C1 File Offset: 0x0000B6C1
			// (set) Token: 0x060002AF RID: 687 RVA: 0x0000D4C9 File Offset: 0x0000B6C9
			public bool UseAuthenticatedAttributes
			{
				get
				{
					return this.mda;
				}
				set
				{
					this.mda = value;
				}
			}

			// Token: 0x060002B0 RID: 688 RVA: 0x0000D4D4 File Offset: 0x0000B6D4
			public bool VerifySignature(AsymmetricAlgorithm aa)
			{
				if (aa == null)
				{
					return false;
				}
				RSAPKCS1SignatureDeformatter rsapkcs1SignatureDeformatter = new RSAPKCS1SignatureDeformatter(aa);
				rsapkcs1SignatureDeformatter.SetHashAlgorithm(this.hashAlgorithm);
				HashAlgorithm hashAlgorithm = HashAlgorithm.Create(this.hashAlgorithm);
				byte[] signature = this.signerInfo.Signature;
				byte[] array;
				if (this.mda)
				{
					ASN1 asn = new ASN1(49);
					foreach (object obj in this.signerInfo.AuthenticatedAttributes)
					{
						ASN1 asn2 = (ASN1)obj;
						asn.Add(asn2);
					}
					array = hashAlgorithm.ComputeHash(asn.GetBytes());
				}
				else
				{
					array = hashAlgorithm.ComputeHash(this.contentInfo.Content[0].Value);
				}
				return array != null && signature != null && rsapkcs1SignatureDeformatter.VerifySignature(array, signature);
			}

			// Token: 0x060002B1 RID: 689 RVA: 0x0000D5C0 File Offset: 0x0000B7C0
			internal string OidToName(string oid)
			{
				if (oid == "1.3.14.3.2.26")
				{
					return "SHA1";
				}
				if (oid == "1.2.840.113549.2.2")
				{
					return "MD2";
				}
				if (oid == "1.2.840.113549.2.5")
				{
					return "MD5";
				}
				if (oid == "2.16.840.1.101.3.4.1")
				{
					return "SHA256";
				}
				if (oid == "2.16.840.1.101.3.4.2")
				{
					return "SHA384";
				}
				if (!(oid == "2.16.840.1.101.3.4.3"))
				{
					return oid;
				}
				return "SHA512";
			}

			// Token: 0x060002B2 RID: 690 RVA: 0x0000D644 File Offset: 0x0000B844
			internal ASN1 GetASN1()
			{
				ASN1 asn = new ASN1(48);
				byte[] data = new byte[]
				{
					this.version
				};
				asn.Add(new ASN1(2, data));
				ASN1 asn2 = asn.Add(new ASN1(49));
				if (this.hashAlgorithm != null)
				{
					string oid = CryptoConfig.MapNameToOID(this.hashAlgorithm);
					asn2.Add(PKCS7.AlgorithmIdentifier(oid));
				}
				ASN1 asn3 = this.contentInfo.ASN1;
				asn.Add(asn3);
				if (!this.signed && this.hashAlgorithm != null)
				{
					if (this.mda)
					{
						ASN1 value = PKCS7.Attribute("1.2.840.113549.1.9.3", asn3[0]);
						this.signerInfo.AuthenticatedAttributes.Add(value);
						byte[] data2 = HashAlgorithm.Create(this.hashAlgorithm).ComputeHash(asn3[1][0].Value);
						ASN1 asn4 = new ASN1(48);
						ASN1 value2 = PKCS7.Attribute("1.2.840.113549.1.9.4", asn4.Add(new ASN1(4, data2)));
						this.signerInfo.AuthenticatedAttributes.Add(value2);
					}
					else
					{
						RSAPKCS1SignatureFormatter rsapkcs1SignatureFormatter = new RSAPKCS1SignatureFormatter(this.signerInfo.Key);
						rsapkcs1SignatureFormatter.SetHashAlgorithm(this.hashAlgorithm);
						byte[] rgbHash = HashAlgorithm.Create(this.hashAlgorithm).ComputeHash(asn3[1][0].Value);
						this.signerInfo.Signature = rsapkcs1SignatureFormatter.CreateSignature(rgbHash);
					}
					this.signed = true;
				}
				if (this.certs.Count > 0)
				{
					ASN1 asn5 = asn.Add(new ASN1(160));
					foreach (X509Certificate x509Certificate in this.certs)
					{
						asn5.Add(new ASN1(x509Certificate.RawData));
					}
				}
				if (this.crls.Count > 0)
				{
					ASN1 asn6 = asn.Add(new ASN1(161));
					foreach (object obj in this.crls)
					{
						byte[] data3 = (byte[])obj;
						asn6.Add(new ASN1(data3));
					}
				}
				ASN1 asn7 = asn.Add(new ASN1(49));
				if (this.signerInfo.Key != null)
				{
					asn7.Add(this.signerInfo.ASN1);
				}
				return asn;
			}

			// Token: 0x060002B3 RID: 691 RVA: 0x0000D8E4 File Offset: 0x0000BAE4
			public byte[] GetBytes()
			{
				return this.GetASN1().GetBytes();
			}

			// Token: 0x040004D2 RID: 1234
			private byte version;

			// Token: 0x040004D3 RID: 1235
			private string hashAlgorithm;

			// Token: 0x040004D4 RID: 1236
			private PKCS7.ContentInfo contentInfo;

			// Token: 0x040004D5 RID: 1237
			private X509CertificateCollection certs;

			// Token: 0x040004D6 RID: 1238
			private ArrayList crls;

			// Token: 0x040004D7 RID: 1239
			private PKCS7.SignerInfo signerInfo;

			// Token: 0x040004D8 RID: 1240
			private bool mda;

			// Token: 0x040004D9 RID: 1241
			private bool signed;
		}

		// Token: 0x0200005F RID: 95
		public class SignerInfo
		{
			// Token: 0x060002B4 RID: 692 RVA: 0x0000D8F1 File Offset: 0x0000BAF1
			public SignerInfo()
			{
				this.version = 1;
				this.authenticatedAttributes = new ArrayList();
				this.unauthenticatedAttributes = new ArrayList();
			}

			// Token: 0x060002B5 RID: 693 RVA: 0x0000D916 File Offset: 0x0000BB16
			public SignerInfo(byte[] data) : this(new ASN1(data))
			{
			}

			// Token: 0x060002B6 RID: 694 RVA: 0x0000D924 File Offset: 0x0000BB24
			public SignerInfo(ASN1 asn1) : this()
			{
				if (asn1[0].Tag != 48 || asn1[0].Count < 5)
				{
					throw new ArgumentException("Invalid SignedData");
				}
				if (asn1[0][0].Tag != 2)
				{
					throw new ArgumentException("Invalid version");
				}
				this.version = asn1[0][0].Value[0];
				ASN1 asn2 = asn1[0][1];
				if (asn2.Tag == 128 && this.version == 3)
				{
					this.ski = asn2.Value;
				}
				else
				{
					this.issuer = X501.ToString(asn2[0]);
					this.serial = asn2[1].Value;
				}
				ASN1 asn3 = asn1[0][2];
				this.hashAlgorithm = ASN1Convert.ToOid(asn3[0]);
				int num = 3;
				ASN1 asn4 = asn1[0][num];
				if (asn4.Tag == 160)
				{
					num++;
					for (int i = 0; i < asn4.Count; i++)
					{
						this.authenticatedAttributes.Add(asn4[i]);
					}
				}
				num++;
				ASN1 asn5 = asn1[0][num++];
				if (asn5.Tag == 4)
				{
					this.signature = asn5.Value;
				}
				ASN1 asn6 = asn1[0][num];
				if (asn6 != null && asn6.Tag == 161)
				{
					for (int j = 0; j < asn6.Count; j++)
					{
						this.unauthenticatedAttributes.Add(asn6[j]);
					}
				}
			}

			// Token: 0x17000050 RID: 80
			// (get) Token: 0x060002B7 RID: 695 RVA: 0x0000DAD5 File Offset: 0x0000BCD5
			public string IssuerName
			{
				get
				{
					return this.issuer;
				}
			}

			// Token: 0x17000051 RID: 81
			// (get) Token: 0x060002B8 RID: 696 RVA: 0x0000DADD File Offset: 0x0000BCDD
			public byte[] SerialNumber
			{
				get
				{
					if (this.serial == null)
					{
						return null;
					}
					return (byte[])this.serial.Clone();
				}
			}

			// Token: 0x17000052 RID: 82
			// (get) Token: 0x060002B9 RID: 697 RVA: 0x0000DAF9 File Offset: 0x0000BCF9
			public byte[] SubjectKeyIdentifier
			{
				get
				{
					if (this.ski == null)
					{
						return null;
					}
					return (byte[])this.ski.Clone();
				}
			}

			// Token: 0x17000053 RID: 83
			// (get) Token: 0x060002BA RID: 698 RVA: 0x0000DB15 File Offset: 0x0000BD15
			public ASN1 ASN1
			{
				get
				{
					return this.GetASN1();
				}
			}

			// Token: 0x17000054 RID: 84
			// (get) Token: 0x060002BB RID: 699 RVA: 0x0000DB1D File Offset: 0x0000BD1D
			public ArrayList AuthenticatedAttributes
			{
				get
				{
					return this.authenticatedAttributes;
				}
			}

			// Token: 0x17000055 RID: 85
			// (get) Token: 0x060002BC RID: 700 RVA: 0x0000DB25 File Offset: 0x0000BD25
			// (set) Token: 0x060002BD RID: 701 RVA: 0x0000DB2D File Offset: 0x0000BD2D
			public X509Certificate Certificate
			{
				get
				{
					return this.x509;
				}
				set
				{
					this.x509 = value;
				}
			}

			// Token: 0x17000056 RID: 86
			// (get) Token: 0x060002BE RID: 702 RVA: 0x0000DB36 File Offset: 0x0000BD36
			// (set) Token: 0x060002BF RID: 703 RVA: 0x0000DB3E File Offset: 0x0000BD3E
			public string HashName
			{
				get
				{
					return this.hashAlgorithm;
				}
				set
				{
					this.hashAlgorithm = value;
				}
			}

			// Token: 0x17000057 RID: 87
			// (get) Token: 0x060002C0 RID: 704 RVA: 0x0000DB47 File Offset: 0x0000BD47
			// (set) Token: 0x060002C1 RID: 705 RVA: 0x0000DB4F File Offset: 0x0000BD4F
			public AsymmetricAlgorithm Key
			{
				get
				{
					return this.key;
				}
				set
				{
					this.key = value;
				}
			}

			// Token: 0x17000058 RID: 88
			// (get) Token: 0x060002C2 RID: 706 RVA: 0x0000DB58 File Offset: 0x0000BD58
			// (set) Token: 0x060002C3 RID: 707 RVA: 0x0000DB74 File Offset: 0x0000BD74
			public byte[] Signature
			{
				get
				{
					if (this.signature == null)
					{
						return null;
					}
					return (byte[])this.signature.Clone();
				}
				set
				{
					if (value != null)
					{
						this.signature = (byte[])value.Clone();
					}
				}
			}

			// Token: 0x17000059 RID: 89
			// (get) Token: 0x060002C4 RID: 708 RVA: 0x0000DB8A File Offset: 0x0000BD8A
			public ArrayList UnauthenticatedAttributes
			{
				get
				{
					return this.unauthenticatedAttributes;
				}
			}

			// Token: 0x1700005A RID: 90
			// (get) Token: 0x060002C5 RID: 709 RVA: 0x0000DB92 File Offset: 0x0000BD92
			// (set) Token: 0x060002C6 RID: 710 RVA: 0x0000DB9A File Offset: 0x0000BD9A
			public byte Version
			{
				get
				{
					return this.version;
				}
				set
				{
					this.version = value;
				}
			}

			// Token: 0x060002C7 RID: 711 RVA: 0x0000DBA4 File Offset: 0x0000BDA4
			internal ASN1 GetASN1()
			{
				if (this.key == null || this.hashAlgorithm == null)
				{
					return null;
				}
				byte[] data = new byte[]
				{
					this.version
				};
				ASN1 asn = new ASN1(48);
				asn.Add(new ASN1(2, data));
				asn.Add(PKCS7.IssuerAndSerialNumber(this.x509));
				string oid = CryptoConfig.MapNameToOID(this.hashAlgorithm);
				asn.Add(PKCS7.AlgorithmIdentifier(oid));
				ASN1 asn2 = null;
				if (this.authenticatedAttributes.Count > 0)
				{
					asn2 = asn.Add(new ASN1(160));
					this.authenticatedAttributes.Sort(new PKCS7.SortedSet());
					foreach (object obj in this.authenticatedAttributes)
					{
						ASN1 asn3 = (ASN1)obj;
						asn2.Add(asn3);
					}
				}
				if (this.key is RSA)
				{
					asn.Add(PKCS7.AlgorithmIdentifier("1.2.840.113549.1.1.1"));
					if (asn2 != null)
					{
						RSAPKCS1SignatureFormatter rsapkcs1SignatureFormatter = new RSAPKCS1SignatureFormatter(this.key);
						rsapkcs1SignatureFormatter.SetHashAlgorithm(this.hashAlgorithm);
						byte[] bytes = asn2.GetBytes();
						bytes[0] = 49;
						byte[] rgbHash = HashAlgorithm.Create(this.hashAlgorithm).ComputeHash(bytes);
						this.signature = rsapkcs1SignatureFormatter.CreateSignature(rgbHash);
					}
					asn.Add(new ASN1(4, this.signature));
					if (this.unauthenticatedAttributes.Count > 0)
					{
						ASN1 asn4 = asn.Add(new ASN1(161));
						this.unauthenticatedAttributes.Sort(new PKCS7.SortedSet());
						foreach (object obj2 in this.unauthenticatedAttributes)
						{
							ASN1 asn5 = (ASN1)obj2;
							asn4.Add(asn5);
						}
					}
					return asn;
				}
				if (this.key is DSA)
				{
					throw new NotImplementedException("not yet");
				}
				throw new CryptographicException("Unknown assymetric algorithm");
			}

			// Token: 0x060002C8 RID: 712 RVA: 0x0000DDC4 File Offset: 0x0000BFC4
			public byte[] GetBytes()
			{
				return this.GetASN1().GetBytes();
			}

			// Token: 0x040004DA RID: 1242
			private byte version;

			// Token: 0x040004DB RID: 1243
			private X509Certificate x509;

			// Token: 0x040004DC RID: 1244
			private string hashAlgorithm;

			// Token: 0x040004DD RID: 1245
			private AsymmetricAlgorithm key;

			// Token: 0x040004DE RID: 1246
			private ArrayList authenticatedAttributes;

			// Token: 0x040004DF RID: 1247
			private ArrayList unauthenticatedAttributes;

			// Token: 0x040004E0 RID: 1248
			private byte[] signature;

			// Token: 0x040004E1 RID: 1249
			private string issuer;

			// Token: 0x040004E2 RID: 1250
			private byte[] serial;

			// Token: 0x040004E3 RID: 1251
			private byte[] ski;
		}

		// Token: 0x02000060 RID: 96
		internal class SortedSet : IComparer
		{
			// Token: 0x060002C9 RID: 713 RVA: 0x0000DDD4 File Offset: 0x0000BFD4
			public int Compare(object x, object y)
			{
				if (x == null)
				{
					if (y != null)
					{
						return -1;
					}
					return 0;
				}
				else
				{
					if (y == null)
					{
						return 1;
					}
					ASN1 asn = x as ASN1;
					ASN1 asn2 = y as ASN1;
					if (asn == null || asn2 == null)
					{
						throw new ArgumentException(Locale.GetText("Invalid objects."));
					}
					byte[] bytes = asn.GetBytes();
					byte[] bytes2 = asn2.GetBytes();
					int num = 0;
					while (num < bytes.Length && num != bytes2.Length)
					{
						if (bytes[num] != bytes2[num])
						{
							if (bytes[num] >= bytes2[num])
							{
								return 1;
							}
							return -1;
						}
						else
						{
							num++;
						}
					}
					if (bytes.Length > bytes2.Length)
					{
						return 1;
					}
					if (bytes.Length < bytes2.Length)
					{
						return -1;
					}
					return 0;
				}
			}

			// Token: 0x060002CA RID: 714 RVA: 0x00002050 File Offset: 0x00000250
			public SortedSet()
			{
			}
		}
	}
}
