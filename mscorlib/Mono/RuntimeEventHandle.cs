﻿using System;

namespace Mono
{
	// Token: 0x0200002F RID: 47
	internal struct RuntimeEventHandle
	{
		// Token: 0x06000157 RID: 343 RVA: 0x00006976 File Offset: 0x00004B76
		internal RuntimeEventHandle(IntPtr v)
		{
			this.value = v;
		}

		// Token: 0x1700001B RID: 27
		// (get) Token: 0x06000158 RID: 344 RVA: 0x0000697F File Offset: 0x00004B7F
		public IntPtr Value
		{
			get
			{
				return this.value;
			}
		}

		// Token: 0x06000159 RID: 345 RVA: 0x00006988 File Offset: 0x00004B88
		public override bool Equals(object obj)
		{
			return obj != null && !(base.GetType() != obj.GetType()) && this.value == ((RuntimeEventHandle)obj).Value;
		}

		// Token: 0x0600015A RID: 346 RVA: 0x000069D0 File Offset: 0x00004BD0
		public bool Equals(RuntimeEventHandle handle)
		{
			return this.value == handle.Value;
		}

		// Token: 0x0600015B RID: 347 RVA: 0x000069E4 File Offset: 0x00004BE4
		public override int GetHashCode()
		{
			return this.value.GetHashCode();
		}

		// Token: 0x0600015C RID: 348 RVA: 0x000069F1 File Offset: 0x00004BF1
		public static bool operator ==(RuntimeEventHandle left, RuntimeEventHandle right)
		{
			return left.Equals(right);
		}

		// Token: 0x0600015D RID: 349 RVA: 0x000069FB File Offset: 0x00004BFB
		public static bool operator !=(RuntimeEventHandle left, RuntimeEventHandle right)
		{
			return !left.Equals(right);
		}

		// Token: 0x040003FB RID: 1019
		private IntPtr value;
	}
}
