﻿using System;
using System.Runtime.CompilerServices;

namespace Mono
{
	// Token: 0x0200003D RID: 61
	internal struct SafeStringMarshal : IDisposable
	{
		// Token: 0x06000176 RID: 374
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern IntPtr StringToUtf8(string str);

		// Token: 0x06000177 RID: 375
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void GFree(IntPtr ptr);

		// Token: 0x06000178 RID: 376 RVA: 0x00006C77 File Offset: 0x00004E77
		public SafeStringMarshal(string str)
		{
			this.str = str;
			this.marshaled_string = IntPtr.Zero;
		}

		// Token: 0x17000021 RID: 33
		// (get) Token: 0x06000179 RID: 377 RVA: 0x00006C8B File Offset: 0x00004E8B
		public IntPtr Value
		{
			get
			{
				if (this.marshaled_string == IntPtr.Zero && this.str != null)
				{
					this.marshaled_string = SafeStringMarshal.StringToUtf8(this.str);
				}
				return this.marshaled_string;
			}
		}

		// Token: 0x0600017A RID: 378 RVA: 0x00006CBE File Offset: 0x00004EBE
		public void Dispose()
		{
			if (this.marshaled_string != IntPtr.Zero)
			{
				SafeStringMarshal.GFree(this.marshaled_string);
				this.marshaled_string = IntPtr.Zero;
			}
		}

		// Token: 0x0400042F RID: 1071
		private readonly string str;

		// Token: 0x04000430 RID: 1072
		private IntPtr marshaled_string;
	}
}
