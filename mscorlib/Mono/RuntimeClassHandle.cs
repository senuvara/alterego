﻿using System;
using System.Runtime.CompilerServices;

namespace Mono
{
	// Token: 0x0200002C RID: 44
	internal struct RuntimeClassHandle
	{
		// Token: 0x06000143 RID: 323 RVA: 0x00006764 File Offset: 0x00004964
		internal unsafe RuntimeClassHandle(RuntimeStructs.MonoClass* value)
		{
			this.value = value;
		}

		// Token: 0x06000144 RID: 324 RVA: 0x0000676D File Offset: 0x0000496D
		internal unsafe RuntimeClassHandle(IntPtr ptr)
		{
			this.value = (RuntimeStructs.MonoClass*)((void*)ptr);
		}

		// Token: 0x17000017 RID: 23
		// (get) Token: 0x06000145 RID: 325 RVA: 0x0000677B File Offset: 0x0000497B
		internal unsafe RuntimeStructs.MonoClass* Value
		{
			get
			{
				return this.value;
			}
		}

		// Token: 0x06000146 RID: 326 RVA: 0x00006784 File Offset: 0x00004984
		public override bool Equals(object obj)
		{
			return obj != null && !(base.GetType() != obj.GetType()) && this.value == ((RuntimeClassHandle)obj).Value;
		}

		// Token: 0x06000147 RID: 327 RVA: 0x000067CC File Offset: 0x000049CC
		public unsafe override int GetHashCode()
		{
			return ((IntPtr)((void*)this.value)).GetHashCode();
		}

		// Token: 0x06000148 RID: 328 RVA: 0x000067EC File Offset: 0x000049EC
		public bool Equals(RuntimeClassHandle handle)
		{
			return this.value == handle.Value;
		}

		// Token: 0x06000149 RID: 329 RVA: 0x000067FD File Offset: 0x000049FD
		public static bool operator ==(RuntimeClassHandle left, object right)
		{
			return right != null && right is RuntimeClassHandle && left.Equals((RuntimeClassHandle)right);
		}

		// Token: 0x0600014A RID: 330 RVA: 0x00006819 File Offset: 0x00004A19
		public static bool operator !=(RuntimeClassHandle left, object right)
		{
			return right == null || !(right is RuntimeClassHandle) || !left.Equals((RuntimeClassHandle)right);
		}

		// Token: 0x0600014B RID: 331 RVA: 0x00006838 File Offset: 0x00004A38
		public static bool operator ==(object left, RuntimeClassHandle right)
		{
			return left != null && left is RuntimeClassHandle && ((RuntimeClassHandle)left).Equals(right);
		}

		// Token: 0x0600014C RID: 332 RVA: 0x00006864 File Offset: 0x00004A64
		public static bool operator !=(object left, RuntimeClassHandle right)
		{
			return left == null || !(left is RuntimeClassHandle) || !((RuntimeClassHandle)left).Equals(right);
		}

		// Token: 0x0600014D RID: 333
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal unsafe static extern IntPtr GetTypeFromClass(RuntimeStructs.MonoClass* klass);

		// Token: 0x0600014E RID: 334 RVA: 0x00006890 File Offset: 0x00004A90
		internal RuntimeTypeHandle GetTypeHandle()
		{
			return new RuntimeTypeHandle(RuntimeClassHandle.GetTypeFromClass(this.value));
		}

		// Token: 0x040003F8 RID: 1016
		private unsafe RuntimeStructs.MonoClass* value;
	}
}
