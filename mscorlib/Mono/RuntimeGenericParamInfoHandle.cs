﻿using System;
using System.Reflection;

namespace Mono
{
	// Token: 0x0200002E RID: 46
	internal struct RuntimeGenericParamInfoHandle
	{
		// Token: 0x06000151 RID: 337 RVA: 0x000068BD File Offset: 0x00004ABD
		internal unsafe RuntimeGenericParamInfoHandle(RuntimeStructs.GenericParamInfo* value)
		{
			this.value = value;
		}

		// Token: 0x06000152 RID: 338 RVA: 0x000068C6 File Offset: 0x00004AC6
		internal unsafe RuntimeGenericParamInfoHandle(IntPtr ptr)
		{
			this.value = (RuntimeStructs.GenericParamInfo*)((void*)ptr);
		}

		// Token: 0x17000019 RID: 25
		// (get) Token: 0x06000153 RID: 339 RVA: 0x000068D4 File Offset: 0x00004AD4
		internal Type[] Constraints
		{
			get
			{
				return this.GetConstraints();
			}
		}

		// Token: 0x1700001A RID: 26
		// (get) Token: 0x06000154 RID: 340 RVA: 0x000068DC File Offset: 0x00004ADC
		internal unsafe GenericParameterAttributes Attributes
		{
			get
			{
				return (GenericParameterAttributes)this.value->flags;
			}
		}

		// Token: 0x06000155 RID: 341 RVA: 0x000068EC File Offset: 0x00004AEC
		private unsafe Type[] GetConstraints()
		{
			int constraintsCount = this.GetConstraintsCount();
			Type[] array = new Type[constraintsCount];
			for (int i = 0; i < constraintsCount; i++)
			{
				RuntimeClassHandle runtimeClassHandle = new RuntimeClassHandle(*(IntPtr*)(this.value->constraints + (IntPtr)i * (IntPtr)sizeof(RuntimeStructs.MonoClass*) / (IntPtr)sizeof(RuntimeStructs.MonoClass*)));
				array[i] = Type.GetTypeFromHandle(runtimeClassHandle.GetTypeHandle());
			}
			return array;
		}

		// Token: 0x06000156 RID: 342 RVA: 0x00006940 File Offset: 0x00004B40
		private unsafe int GetConstraintsCount()
		{
			int num = 0;
			RuntimeStructs.MonoClass** ptr = this.value->constraints;
			while (ptr != null && *(IntPtr*)ptr != (IntPtr)((UIntPtr)0))
			{
				ptr += sizeof(RuntimeStructs.MonoClass*) / sizeof(RuntimeStructs.MonoClass*);
				num++;
			}
			return num;
		}

		// Token: 0x040003FA RID: 1018
		private unsafe RuntimeStructs.GenericParamInfo* value;
	}
}
