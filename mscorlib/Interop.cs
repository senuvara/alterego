﻿using System;

// Token: 0x02000002 RID: 2
internal static class Interop
{
	// Token: 0x02000003 RID: 3
	internal static class Libraries
	{
		// Token: 0x04000001 RID: 1
		internal const string CoreLibNative = "System.Private.CoreLib.Native";
	}
}
