﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;

namespace UnityEngine.AI
{
	// Token: 0x02000010 RID: 16
	[NativeHeader("Modules/AI/Public/NavMeshBuildSettings.h")]
	public struct NavMeshBuildSettings
	{
		// Token: 0x1700000F RID: 15
		// (get) Token: 0x060000B1 RID: 177 RVA: 0x00002E74 File Offset: 0x00001074
		// (set) Token: 0x060000B2 RID: 178 RVA: 0x00002E8F File Offset: 0x0000108F
		public int agentTypeID
		{
			get
			{
				return this.m_AgentTypeID;
			}
			set
			{
				this.m_AgentTypeID = value;
			}
		}

		// Token: 0x17000010 RID: 16
		// (get) Token: 0x060000B3 RID: 179 RVA: 0x00002E9C File Offset: 0x0000109C
		// (set) Token: 0x060000B4 RID: 180 RVA: 0x00002EB7 File Offset: 0x000010B7
		public float agentRadius
		{
			get
			{
				return this.m_AgentRadius;
			}
			set
			{
				this.m_AgentRadius = value;
			}
		}

		// Token: 0x17000011 RID: 17
		// (get) Token: 0x060000B5 RID: 181 RVA: 0x00002EC4 File Offset: 0x000010C4
		// (set) Token: 0x060000B6 RID: 182 RVA: 0x00002EDF File Offset: 0x000010DF
		public float agentHeight
		{
			get
			{
				return this.m_AgentHeight;
			}
			set
			{
				this.m_AgentHeight = value;
			}
		}

		// Token: 0x17000012 RID: 18
		// (get) Token: 0x060000B7 RID: 183 RVA: 0x00002EEC File Offset: 0x000010EC
		// (set) Token: 0x060000B8 RID: 184 RVA: 0x00002F07 File Offset: 0x00001107
		public float agentSlope
		{
			get
			{
				return this.m_AgentSlope;
			}
			set
			{
				this.m_AgentSlope = value;
			}
		}

		// Token: 0x17000013 RID: 19
		// (get) Token: 0x060000B9 RID: 185 RVA: 0x00002F14 File Offset: 0x00001114
		// (set) Token: 0x060000BA RID: 186 RVA: 0x00002F2F File Offset: 0x0000112F
		public float agentClimb
		{
			get
			{
				return this.m_AgentClimb;
			}
			set
			{
				this.m_AgentClimb = value;
			}
		}

		// Token: 0x17000014 RID: 20
		// (get) Token: 0x060000BB RID: 187 RVA: 0x00002F3C File Offset: 0x0000113C
		// (set) Token: 0x060000BC RID: 188 RVA: 0x00002F57 File Offset: 0x00001157
		public float minRegionArea
		{
			get
			{
				return this.m_MinRegionArea;
			}
			set
			{
				this.m_MinRegionArea = value;
			}
		}

		// Token: 0x17000015 RID: 21
		// (get) Token: 0x060000BD RID: 189 RVA: 0x00002F64 File Offset: 0x00001164
		// (set) Token: 0x060000BE RID: 190 RVA: 0x00002F85 File Offset: 0x00001185
		public bool overrideVoxelSize
		{
			get
			{
				return this.m_OverrideVoxelSize != 0;
			}
			set
			{
				this.m_OverrideVoxelSize = ((!value) ? 0 : 1);
			}
		}

		// Token: 0x17000016 RID: 22
		// (get) Token: 0x060000BF RID: 191 RVA: 0x00002F9C File Offset: 0x0000119C
		// (set) Token: 0x060000C0 RID: 192 RVA: 0x00002FB7 File Offset: 0x000011B7
		public float voxelSize
		{
			get
			{
				return this.m_VoxelSize;
			}
			set
			{
				this.m_VoxelSize = value;
			}
		}

		// Token: 0x17000017 RID: 23
		// (get) Token: 0x060000C1 RID: 193 RVA: 0x00002FC4 File Offset: 0x000011C4
		// (set) Token: 0x060000C2 RID: 194 RVA: 0x00002FE5 File Offset: 0x000011E5
		public bool overrideTileSize
		{
			get
			{
				return this.m_OverrideTileSize != 0;
			}
			set
			{
				this.m_OverrideTileSize = ((!value) ? 0 : 1);
			}
		}

		// Token: 0x17000018 RID: 24
		// (get) Token: 0x060000C3 RID: 195 RVA: 0x00002FFC File Offset: 0x000011FC
		// (set) Token: 0x060000C4 RID: 196 RVA: 0x00003017 File Offset: 0x00001217
		public int tileSize
		{
			get
			{
				return this.m_TileSize;
			}
			set
			{
				this.m_TileSize = value;
			}
		}

		// Token: 0x17000019 RID: 25
		// (get) Token: 0x060000C5 RID: 197 RVA: 0x00003024 File Offset: 0x00001224
		// (set) Token: 0x060000C6 RID: 198 RVA: 0x0000303F File Offset: 0x0000123F
		public NavMeshBuildDebugSettings debug
		{
			get
			{
				return this.m_Debug;
			}
			set
			{
				this.m_Debug = value;
			}
		}

		// Token: 0x060000C7 RID: 199 RVA: 0x0000304C File Offset: 0x0000124C
		public string[] ValidationReport(Bounds buildBounds)
		{
			return NavMeshBuildSettings.InternalValidationReport(this, buildBounds);
		}

		// Token: 0x060000C8 RID: 200 RVA: 0x0000306D File Offset: 0x0000126D
		[FreeFunction]
		[NativeHeader("Modules/AI/Public/NavMeshBuildSettings.h")]
		private static string[] InternalValidationReport(NavMeshBuildSettings buildSettings, Bounds buildBounds)
		{
			return NavMeshBuildSettings.InternalValidationReport_Injected(ref buildSettings, ref buildBounds);
		}

		// Token: 0x060000C9 RID: 201
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern string[] InternalValidationReport_Injected(ref NavMeshBuildSettings buildSettings, ref Bounds buildBounds);

		// Token: 0x04000035 RID: 53
		private int m_AgentTypeID;

		// Token: 0x04000036 RID: 54
		private float m_AgentRadius;

		// Token: 0x04000037 RID: 55
		private float m_AgentHeight;

		// Token: 0x04000038 RID: 56
		private float m_AgentSlope;

		// Token: 0x04000039 RID: 57
		private float m_AgentClimb;

		// Token: 0x0400003A RID: 58
		private float m_LedgeDropHeight;

		// Token: 0x0400003B RID: 59
		private float m_MaxJumpAcrossDistance;

		// Token: 0x0400003C RID: 60
		private float m_MinRegionArea;

		// Token: 0x0400003D RID: 61
		private int m_OverrideVoxelSize;

		// Token: 0x0400003E RID: 62
		private float m_VoxelSize;

		// Token: 0x0400003F RID: 63
		private int m_OverrideTileSize;

		// Token: 0x04000040 RID: 64
		private int m_TileSize;

		// Token: 0x04000041 RID: 65
		private int m_AccuratePlacement;

		// Token: 0x04000042 RID: 66
		private NavMeshBuildDebugSettings m_Debug;
	}
}
