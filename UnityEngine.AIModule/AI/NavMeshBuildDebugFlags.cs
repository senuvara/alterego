﻿using System;

namespace UnityEngine.AI
{
	// Token: 0x0200000B RID: 11
	[Flags]
	public enum NavMeshBuildDebugFlags
	{
		// Token: 0x04000018 RID: 24
		None = 0,
		// Token: 0x04000019 RID: 25
		InputGeometry = 1,
		// Token: 0x0400001A RID: 26
		Voxels = 2,
		// Token: 0x0400001B RID: 27
		Regions = 4,
		// Token: 0x0400001C RID: 28
		RawContours = 8,
		// Token: 0x0400001D RID: 29
		SimplifiedContours = 16,
		// Token: 0x0400001E RID: 30
		PolygonMeshes = 32,
		// Token: 0x0400001F RID: 31
		PolygonMeshesDetail = 64,
		// Token: 0x04000020 RID: 32
		All = 127
	}
}
