﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Scripting;
using UnityEngine.Scripting.APIUpdating;

namespace UnityEngine.AI
{
	// Token: 0x0200001A RID: 26
	[MovedFrom("UnityEngine")]
	public sealed class OffMeshLink : Behaviour
	{
		// Token: 0x0600015B RID: 347 RVA: 0x00003129 File Offset: 0x00001329
		public OffMeshLink()
		{
		}

		// Token: 0x17000051 RID: 81
		// (get) Token: 0x0600015C RID: 348
		// (set) Token: 0x0600015D RID: 349
		public extern bool activated { [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] get; [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000052 RID: 82
		// (get) Token: 0x0600015E RID: 350
		public extern bool occupied { [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000053 RID: 83
		// (get) Token: 0x0600015F RID: 351
		// (set) Token: 0x06000160 RID: 352
		public extern float costOverride { [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] get; [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000054 RID: 84
		// (get) Token: 0x06000161 RID: 353
		// (set) Token: 0x06000162 RID: 354
		public extern bool biDirectional { [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] get; [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x06000163 RID: 355
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void UpdatePositions();

		// Token: 0x17000055 RID: 85
		// (get) Token: 0x06000164 RID: 356
		// (set) Token: 0x06000165 RID: 357
		[Obsolete("Use area instead.")]
		public extern int navMeshLayer { [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] get; [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000056 RID: 86
		// (get) Token: 0x06000166 RID: 358
		// (set) Token: 0x06000167 RID: 359
		public extern int area { [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] get; [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000057 RID: 87
		// (get) Token: 0x06000168 RID: 360
		// (set) Token: 0x06000169 RID: 361
		public extern bool autoUpdatePositions { [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] get; [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000058 RID: 88
		// (get) Token: 0x0600016A RID: 362
		// (set) Token: 0x0600016B RID: 363
		public extern Transform startTransform { [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] get; [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000059 RID: 89
		// (get) Token: 0x0600016C RID: 364
		// (set) Token: 0x0600016D RID: 365
		public extern Transform endTransform { [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] get; [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] set; }
	}
}
