﻿using System;
using UnityEngine.Scripting.APIUpdating;

namespace UnityEngine.AI
{
	// Token: 0x02000016 RID: 22
	[MovedFrom("UnityEngine")]
	public enum NavMeshObstacleShape
	{
		// Token: 0x04000051 RID: 81
		Capsule,
		// Token: 0x04000052 RID: 82
		Box
	}
}
