﻿using System;
using UnityEngine.Bindings;

namespace UnityEngine.AI
{
	// Token: 0x02000011 RID: 17
	[NativeHeader("Modules/AI/Public/NavMeshBuildDebugSettings.h")]
	public struct NavMeshBuildDebugSettings
	{
		// Token: 0x1700001A RID: 26
		// (get) Token: 0x060000CA RID: 202 RVA: 0x00003078 File Offset: 0x00001278
		// (set) Token: 0x060000CB RID: 203 RVA: 0x00003093 File Offset: 0x00001293
		public NavMeshBuildDebugFlags flags
		{
			get
			{
				return (NavMeshBuildDebugFlags)this.m_Flags;
			}
			set
			{
				this.m_Flags = (byte)value;
			}
		}

		// Token: 0x04000043 RID: 67
		private byte m_Flags;
	}
}
