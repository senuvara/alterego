﻿using System;

namespace UnityEngine.AI
{
	// Token: 0x02000020 RID: 32
	public struct NavMeshLinkInstance
	{
		// Token: 0x1700006D RID: 109
		// (get) Token: 0x0600019A RID: 410 RVA: 0x00003820 File Offset: 0x00001A20
		public bool valid
		{
			get
			{
				return this.m_Handle != 0 && NavMesh.IsValidLinkHandle(this.m_Handle);
			}
		}

		// Token: 0x1700006E RID: 110
		// (get) Token: 0x0600019B RID: 411 RVA: 0x00003850 File Offset: 0x00001A50
		// (set) Token: 0x0600019C RID: 412 RVA: 0x0000386B File Offset: 0x00001A6B
		internal int id
		{
			get
			{
				return this.m_Handle;
			}
			set
			{
				this.m_Handle = value;
			}
		}

		// Token: 0x0600019D RID: 413 RVA: 0x00003875 File Offset: 0x00001A75
		public void Remove()
		{
			NavMesh.RemoveLinkInternal(this.id);
		}

		// Token: 0x1700006F RID: 111
		// (get) Token: 0x0600019E RID: 414 RVA: 0x00003884 File Offset: 0x00001A84
		// (set) Token: 0x0600019F RID: 415 RVA: 0x000038A4 File Offset: 0x00001AA4
		public Object owner
		{
			get
			{
				return NavMesh.InternalGetLinkOwner(this.id);
			}
			set
			{
				int ownerID = (!(value != null)) ? 0 : value.GetInstanceID();
				if (!NavMesh.InternalSetLinkOwner(this.id, ownerID))
				{
					Debug.LogError("Cannot set 'owner' on an invalid NavMeshLinkInstance");
				}
			}
		}

		// Token: 0x0400006D RID: 109
		private int m_Handle;
	}
}
