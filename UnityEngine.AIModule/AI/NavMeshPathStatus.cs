﻿using System;
using UnityEngine.Scripting.APIUpdating;

namespace UnityEngine.AI
{
	// Token: 0x02000012 RID: 18
	[MovedFrom("UnityEngine")]
	public enum NavMeshPathStatus
	{
		// Token: 0x04000045 RID: 69
		PathComplete,
		// Token: 0x04000046 RID: 70
		PathPartial,
		// Token: 0x04000047 RID: 71
		PathInvalid
	}
}
