﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Scripting;
using UnityEngine.Scripting.APIUpdating;

namespace UnityEngine.AI
{
	// Token: 0x02000017 RID: 23
	[MovedFrom("UnityEngine")]
	public sealed class NavMeshObstacle : Behaviour
	{
		// Token: 0x06000138 RID: 312 RVA: 0x00003129 File Offset: 0x00001329
		public NavMeshObstacle()
		{
		}

		// Token: 0x17000041 RID: 65
		// (get) Token: 0x06000139 RID: 313
		// (set) Token: 0x0600013A RID: 314
		public extern float height { [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] get; [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000042 RID: 66
		// (get) Token: 0x0600013B RID: 315
		// (set) Token: 0x0600013C RID: 316
		public extern float radius { [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] get; [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000043 RID: 67
		// (get) Token: 0x0600013D RID: 317 RVA: 0x00003360 File Offset: 0x00001560
		// (set) Token: 0x0600013E RID: 318 RVA: 0x0000337E File Offset: 0x0000157E
		public Vector3 velocity
		{
			get
			{
				Vector3 result;
				this.INTERNAL_get_velocity(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_velocity(ref value);
			}
		}

		// Token: 0x0600013F RID: 319
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_velocity(out Vector3 value);

		// Token: 0x06000140 RID: 320
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_velocity(ref Vector3 value);

		// Token: 0x17000044 RID: 68
		// (get) Token: 0x06000141 RID: 321
		// (set) Token: 0x06000142 RID: 322
		public extern bool carving { [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] get; [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000045 RID: 69
		// (get) Token: 0x06000143 RID: 323
		// (set) Token: 0x06000144 RID: 324
		public extern bool carveOnlyStationary { [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] get; [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000046 RID: 70
		// (get) Token: 0x06000145 RID: 325
		// (set) Token: 0x06000146 RID: 326
		public extern float carvingMoveThreshold { [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] get; [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000047 RID: 71
		// (get) Token: 0x06000147 RID: 327
		// (set) Token: 0x06000148 RID: 328
		public extern float carvingTimeToStationary { [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] get; [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000048 RID: 72
		// (get) Token: 0x06000149 RID: 329
		// (set) Token: 0x0600014A RID: 330
		public extern NavMeshObstacleShape shape { [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] get; [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000049 RID: 73
		// (get) Token: 0x0600014B RID: 331 RVA: 0x0000338C File Offset: 0x0000158C
		// (set) Token: 0x0600014C RID: 332 RVA: 0x000033AA File Offset: 0x000015AA
		public Vector3 center
		{
			get
			{
				Vector3 result;
				this.INTERNAL_get_center(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_center(ref value);
			}
		}

		// Token: 0x0600014D RID: 333
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_center(out Vector3 value);

		// Token: 0x0600014E RID: 334
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_center(ref Vector3 value);

		// Token: 0x1700004A RID: 74
		// (get) Token: 0x0600014F RID: 335 RVA: 0x000033B8 File Offset: 0x000015B8
		// (set) Token: 0x06000150 RID: 336 RVA: 0x000033D6 File Offset: 0x000015D6
		public Vector3 size
		{
			get
			{
				Vector3 result;
				this.INTERNAL_get_size(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_size(ref value);
			}
		}

		// Token: 0x06000151 RID: 337
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_size(out Vector3 value);

		// Token: 0x06000152 RID: 338
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_size(ref Vector3 value);

		// Token: 0x06000153 RID: 339
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern void FitExtents();
	}
}
