﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine.AI
{
	// Token: 0x0200000E RID: 14
	[NativeHeader("Modules/AI/Public/NavMeshBindingTypes.h")]
	[UsedByNativeCode]
	public struct NavMeshBuildSource
	{
		// Token: 0x17000005 RID: 5
		// (get) Token: 0x0600009A RID: 154 RVA: 0x00002C70 File Offset: 0x00000E70
		// (set) Token: 0x0600009B RID: 155 RVA: 0x00002C8B File Offset: 0x00000E8B
		public Matrix4x4 transform
		{
			get
			{
				return this.m_Transform;
			}
			set
			{
				this.m_Transform = value;
			}
		}

		// Token: 0x17000006 RID: 6
		// (get) Token: 0x0600009C RID: 156 RVA: 0x00002C98 File Offset: 0x00000E98
		// (set) Token: 0x0600009D RID: 157 RVA: 0x00002CB3 File Offset: 0x00000EB3
		public Vector3 size
		{
			get
			{
				return this.m_Size;
			}
			set
			{
				this.m_Size = value;
			}
		}

		// Token: 0x17000007 RID: 7
		// (get) Token: 0x0600009E RID: 158 RVA: 0x00002CC0 File Offset: 0x00000EC0
		// (set) Token: 0x0600009F RID: 159 RVA: 0x00002CDB File Offset: 0x00000EDB
		public NavMeshBuildSourceShape shape
		{
			get
			{
				return this.m_Shape;
			}
			set
			{
				this.m_Shape = value;
			}
		}

		// Token: 0x17000008 RID: 8
		// (get) Token: 0x060000A0 RID: 160 RVA: 0x00002CE8 File Offset: 0x00000EE8
		// (set) Token: 0x060000A1 RID: 161 RVA: 0x00002D03 File Offset: 0x00000F03
		public int area
		{
			get
			{
				return this.m_Area;
			}
			set
			{
				this.m_Area = value;
			}
		}

		// Token: 0x17000009 RID: 9
		// (get) Token: 0x060000A2 RID: 162 RVA: 0x00002D10 File Offset: 0x00000F10
		// (set) Token: 0x060000A3 RID: 163 RVA: 0x00002D30 File Offset: 0x00000F30
		public Object sourceObject
		{
			get
			{
				return NavMeshBuildSource.InternalGetObject(this.m_InstanceID);
			}
			set
			{
				this.m_InstanceID = ((!(value != null)) ? 0 : value.GetInstanceID());
			}
		}

		// Token: 0x1700000A RID: 10
		// (get) Token: 0x060000A4 RID: 164 RVA: 0x00002D54 File Offset: 0x00000F54
		// (set) Token: 0x060000A5 RID: 165 RVA: 0x00002D74 File Offset: 0x00000F74
		public Component component
		{
			get
			{
				return NavMeshBuildSource.InternalGetComponent(this.m_ComponentID);
			}
			set
			{
				this.m_ComponentID = ((!(value != null)) ? 0 : value.GetInstanceID());
			}
		}

		// Token: 0x060000A6 RID: 166
		[StaticAccessor("NavMeshBuildSource", StaticAccessorType.DoubleColon)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern Component InternalGetComponent(int instanceID);

		// Token: 0x060000A7 RID: 167
		[StaticAccessor("NavMeshBuildSource", StaticAccessorType.DoubleColon)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern Object InternalGetObject(int instanceID);

		// Token: 0x0400002B RID: 43
		private Matrix4x4 m_Transform;

		// Token: 0x0400002C RID: 44
		private Vector3 m_Size;

		// Token: 0x0400002D RID: 45
		private NavMeshBuildSourceShape m_Shape;

		// Token: 0x0400002E RID: 46
		private int m_Area;

		// Token: 0x0400002F RID: 47
		private int m_InstanceID;

		// Token: 0x04000030 RID: 48
		private int m_ComponentID;
	}
}
