﻿using System;

namespace UnityEngine.AI
{
	// Token: 0x0200000C RID: 12
	public enum NavMeshBuildSourceShape
	{
		// Token: 0x04000022 RID: 34
		Mesh,
		// Token: 0x04000023 RID: 35
		Terrain,
		// Token: 0x04000024 RID: 36
		Box,
		// Token: 0x04000025 RID: 37
		Sphere,
		// Token: 0x04000026 RID: 38
		Capsule,
		// Token: 0x04000027 RID: 39
		ModifierBox
	}
}
