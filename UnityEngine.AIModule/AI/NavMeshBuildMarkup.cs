﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;

namespace UnityEngine.AI
{
	// Token: 0x0200000F RID: 15
	[NativeHeader("Modules/AI/Public/NavMeshBindingTypes.h")]
	public struct NavMeshBuildMarkup
	{
		// Token: 0x1700000B RID: 11
		// (get) Token: 0x060000A8 RID: 168 RVA: 0x00002D98 File Offset: 0x00000F98
		// (set) Token: 0x060000A9 RID: 169 RVA: 0x00002DB9 File Offset: 0x00000FB9
		public bool overrideArea
		{
			get
			{
				return this.m_OverrideArea != 0;
			}
			set
			{
				this.m_OverrideArea = ((!value) ? 0 : 1);
			}
		}

		// Token: 0x1700000C RID: 12
		// (get) Token: 0x060000AA RID: 170 RVA: 0x00002DD0 File Offset: 0x00000FD0
		// (set) Token: 0x060000AB RID: 171 RVA: 0x00002DEB File Offset: 0x00000FEB
		public int area
		{
			get
			{
				return this.m_Area;
			}
			set
			{
				this.m_Area = value;
			}
		}

		// Token: 0x1700000D RID: 13
		// (get) Token: 0x060000AC RID: 172 RVA: 0x00002DF8 File Offset: 0x00000FF8
		// (set) Token: 0x060000AD RID: 173 RVA: 0x00002E19 File Offset: 0x00001019
		public bool ignoreFromBuild
		{
			get
			{
				return this.m_IgnoreFromBuild != 0;
			}
			set
			{
				this.m_IgnoreFromBuild = ((!value) ? 0 : 1);
			}
		}

		// Token: 0x1700000E RID: 14
		// (get) Token: 0x060000AE RID: 174 RVA: 0x00002E30 File Offset: 0x00001030
		// (set) Token: 0x060000AF RID: 175 RVA: 0x00002E50 File Offset: 0x00001050
		public Transform root
		{
			get
			{
				return NavMeshBuildMarkup.InternalGetRootGO(this.m_InstanceID);
			}
			set
			{
				this.m_InstanceID = ((!(value != null)) ? 0 : value.GetInstanceID());
			}
		}

		// Token: 0x060000B0 RID: 176
		[StaticAccessor("NavMeshBuildMarkup", StaticAccessorType.DoubleColon)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern Transform InternalGetRootGO(int instanceID);

		// Token: 0x04000031 RID: 49
		private int m_OverrideArea;

		// Token: 0x04000032 RID: 50
		private int m_Area;

		// Token: 0x04000033 RID: 51
		private int m_IgnoreFromBuild;

		// Token: 0x04000034 RID: 52
		private int m_InstanceID;
	}
}
