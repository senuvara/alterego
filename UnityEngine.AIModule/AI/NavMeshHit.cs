﻿using System;
using UnityEngine.Scripting.APIUpdating;

namespace UnityEngine.AI
{
	// Token: 0x0200001B RID: 27
	[MovedFrom("UnityEngine")]
	public struct NavMeshHit
	{
		// Token: 0x1700005A RID: 90
		// (get) Token: 0x0600016E RID: 366 RVA: 0x000034A4 File Offset: 0x000016A4
		// (set) Token: 0x0600016F RID: 367 RVA: 0x000034BF File Offset: 0x000016BF
		public Vector3 position
		{
			get
			{
				return this.m_Position;
			}
			set
			{
				this.m_Position = value;
			}
		}

		// Token: 0x1700005B RID: 91
		// (get) Token: 0x06000170 RID: 368 RVA: 0x000034CC File Offset: 0x000016CC
		// (set) Token: 0x06000171 RID: 369 RVA: 0x000034E7 File Offset: 0x000016E7
		public Vector3 normal
		{
			get
			{
				return this.m_Normal;
			}
			set
			{
				this.m_Normal = value;
			}
		}

		// Token: 0x1700005C RID: 92
		// (get) Token: 0x06000172 RID: 370 RVA: 0x000034F4 File Offset: 0x000016F4
		// (set) Token: 0x06000173 RID: 371 RVA: 0x0000350F File Offset: 0x0000170F
		public float distance
		{
			get
			{
				return this.m_Distance;
			}
			set
			{
				this.m_Distance = value;
			}
		}

		// Token: 0x1700005D RID: 93
		// (get) Token: 0x06000174 RID: 372 RVA: 0x0000351C File Offset: 0x0000171C
		// (set) Token: 0x06000175 RID: 373 RVA: 0x00003537 File Offset: 0x00001737
		public int mask
		{
			get
			{
				return this.m_Mask;
			}
			set
			{
				this.m_Mask = value;
			}
		}

		// Token: 0x1700005E RID: 94
		// (get) Token: 0x06000176 RID: 374 RVA: 0x00003544 File Offset: 0x00001744
		// (set) Token: 0x06000177 RID: 375 RVA: 0x00003565 File Offset: 0x00001765
		public bool hit
		{
			get
			{
				return this.m_Hit != 0;
			}
			set
			{
				this.m_Hit = ((!value) ? 0 : 1);
			}
		}

		// Token: 0x0400005D RID: 93
		private Vector3 m_Position;

		// Token: 0x0400005E RID: 94
		private Vector3 m_Normal;

		// Token: 0x0400005F RID: 95
		private float m_Distance;

		// Token: 0x04000060 RID: 96
		private int m_Mask;

		// Token: 0x04000061 RID: 97
		private int m_Hit;
	}
}
