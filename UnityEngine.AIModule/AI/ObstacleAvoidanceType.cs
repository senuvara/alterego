﻿using System;
using UnityEngine.Scripting.APIUpdating;

namespace UnityEngine.AI
{
	// Token: 0x02000014 RID: 20
	[MovedFrom("UnityEngine")]
	public enum ObstacleAvoidanceType
	{
		// Token: 0x0400004B RID: 75
		NoObstacleAvoidance,
		// Token: 0x0400004C RID: 76
		LowQualityObstacleAvoidance,
		// Token: 0x0400004D RID: 77
		MedQualityObstacleAvoidance,
		// Token: 0x0400004E RID: 78
		GoodQualityObstacleAvoidance,
		// Token: 0x0400004F RID: 79
		HighQualityObstacleAvoidance
	}
}
