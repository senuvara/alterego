﻿using System;

namespace UnityEngine.AI
{
	// Token: 0x0200000D RID: 13
	public enum NavMeshCollectGeometry
	{
		// Token: 0x04000029 RID: 41
		RenderMeshes,
		// Token: 0x0400002A RID: 42
		PhysicsColliders
	}
}
