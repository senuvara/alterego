﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine.Scripting;
using UnityEngine.Scripting.APIUpdating;

namespace UnityEngine.AI
{
	// Token: 0x02000013 RID: 19
	[MovedFrom("UnityEngine")]
	[StructLayout(LayoutKind.Sequential)]
	public sealed class NavMeshPath
	{
		// Token: 0x060000CC RID: 204
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern NavMeshPath();

		// Token: 0x060000CD RID: 205
		[ThreadAndSerializationSafe]
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void DestroyNavMeshPath();

		// Token: 0x060000CE RID: 206 RVA: 0x000030A0 File Offset: 0x000012A0
		~NavMeshPath()
		{
			this.DestroyNavMeshPath();
			this.m_Ptr = IntPtr.Zero;
		}

		// Token: 0x060000CF RID: 207
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern int GetCornersNonAlloc(Vector3[] results);

		// Token: 0x060000D0 RID: 208
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern Vector3[] CalculateCornersInternal();

		// Token: 0x060000D1 RID: 209
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void ClearCornersInternal();

		// Token: 0x060000D2 RID: 210 RVA: 0x000030DC File Offset: 0x000012DC
		public void ClearCorners()
		{
			this.ClearCornersInternal();
			this.m_corners = null;
		}

		// Token: 0x060000D3 RID: 211 RVA: 0x000030EC File Offset: 0x000012EC
		private void CalculateCorners()
		{
			if (this.m_corners == null)
			{
				this.m_corners = this.CalculateCornersInternal();
			}
		}

		// Token: 0x1700001B RID: 27
		// (get) Token: 0x060000D4 RID: 212 RVA: 0x00003108 File Offset: 0x00001308
		public Vector3[] corners
		{
			get
			{
				this.CalculateCorners();
				return this.m_corners;
			}
		}

		// Token: 0x1700001C RID: 28
		// (get) Token: 0x060000D5 RID: 213
		public extern NavMeshPathStatus status { [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x04000048 RID: 72
		internal IntPtr m_Ptr;

		// Token: 0x04000049 RID: 73
		internal Vector3[] m_corners;
	}
}
