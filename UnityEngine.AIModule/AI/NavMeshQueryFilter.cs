﻿using System;

namespace UnityEngine.AI
{
	// Token: 0x02000021 RID: 33
	public struct NavMeshQueryFilter
	{
		// Token: 0x17000070 RID: 112
		// (get) Token: 0x060001A0 RID: 416 RVA: 0x000038E8 File Offset: 0x00001AE8
		internal float[] costs
		{
			get
			{
				return this.m_AreaCost;
			}
		}

		// Token: 0x17000071 RID: 113
		// (get) Token: 0x060001A1 RID: 417 RVA: 0x00003904 File Offset: 0x00001B04
		// (set) Token: 0x060001A2 RID: 418 RVA: 0x0000391F File Offset: 0x00001B1F
		public int areaMask
		{
			get
			{
				return this.m_AreaMask;
			}
			set
			{
				this.m_AreaMask = value;
			}
		}

		// Token: 0x17000072 RID: 114
		// (get) Token: 0x060001A3 RID: 419 RVA: 0x0000392C File Offset: 0x00001B2C
		// (set) Token: 0x060001A4 RID: 420 RVA: 0x00003947 File Offset: 0x00001B47
		public int agentTypeID
		{
			get
			{
				return this.m_AgentTypeID;
			}
			set
			{
				this.m_AgentTypeID = value;
			}
		}

		// Token: 0x060001A5 RID: 421 RVA: 0x00003954 File Offset: 0x00001B54
		public float GetAreaCost(int areaIndex)
		{
			float result;
			if (this.m_AreaCost == null)
			{
				if (areaIndex < 0 || areaIndex >= 32)
				{
					string message = string.Format("The valid range is [0:{0}]", 31);
					throw new IndexOutOfRangeException(message);
				}
				result = 1f;
			}
			else
			{
				result = this.m_AreaCost[areaIndex];
			}
			return result;
		}

		// Token: 0x060001A6 RID: 422 RVA: 0x000039B4 File Offset: 0x00001BB4
		public void SetAreaCost(int areaIndex, float cost)
		{
			if (this.m_AreaCost == null)
			{
				this.m_AreaCost = new float[32];
				for (int i = 0; i < 32; i++)
				{
					this.m_AreaCost[i] = 1f;
				}
			}
			this.m_AreaCost[areaIndex] = cost;
		}

		// Token: 0x0400006E RID: 110
		private const int AREA_COST_ELEMENT_COUNT = 32;

		// Token: 0x0400006F RID: 111
		private int m_AreaMask;

		// Token: 0x04000070 RID: 112
		private int m_AgentTypeID;

		// Token: 0x04000071 RID: 113
		private float[] m_AreaCost;
	}
}
