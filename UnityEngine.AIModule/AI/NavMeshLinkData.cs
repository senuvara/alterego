﻿using System;

namespace UnityEngine.AI
{
	// Token: 0x0200001F RID: 31
	public struct NavMeshLinkData
	{
		// Token: 0x17000066 RID: 102
		// (get) Token: 0x0600018C RID: 396 RVA: 0x000036F8 File Offset: 0x000018F8
		// (set) Token: 0x0600018D RID: 397 RVA: 0x00003713 File Offset: 0x00001913
		public Vector3 startPosition
		{
			get
			{
				return this.m_StartPosition;
			}
			set
			{
				this.m_StartPosition = value;
			}
		}

		// Token: 0x17000067 RID: 103
		// (get) Token: 0x0600018E RID: 398 RVA: 0x00003720 File Offset: 0x00001920
		// (set) Token: 0x0600018F RID: 399 RVA: 0x0000373B File Offset: 0x0000193B
		public Vector3 endPosition
		{
			get
			{
				return this.m_EndPosition;
			}
			set
			{
				this.m_EndPosition = value;
			}
		}

		// Token: 0x17000068 RID: 104
		// (get) Token: 0x06000190 RID: 400 RVA: 0x00003748 File Offset: 0x00001948
		// (set) Token: 0x06000191 RID: 401 RVA: 0x00003763 File Offset: 0x00001963
		public float costModifier
		{
			get
			{
				return this.m_CostModifier;
			}
			set
			{
				this.m_CostModifier = value;
			}
		}

		// Token: 0x17000069 RID: 105
		// (get) Token: 0x06000192 RID: 402 RVA: 0x00003770 File Offset: 0x00001970
		// (set) Token: 0x06000193 RID: 403 RVA: 0x00003791 File Offset: 0x00001991
		public bool bidirectional
		{
			get
			{
				return this.m_Bidirectional != 0;
			}
			set
			{
				this.m_Bidirectional = ((!value) ? 0 : 1);
			}
		}

		// Token: 0x1700006A RID: 106
		// (get) Token: 0x06000194 RID: 404 RVA: 0x000037A8 File Offset: 0x000019A8
		// (set) Token: 0x06000195 RID: 405 RVA: 0x000037C3 File Offset: 0x000019C3
		public float width
		{
			get
			{
				return this.m_Width;
			}
			set
			{
				this.m_Width = value;
			}
		}

		// Token: 0x1700006B RID: 107
		// (get) Token: 0x06000196 RID: 406 RVA: 0x000037D0 File Offset: 0x000019D0
		// (set) Token: 0x06000197 RID: 407 RVA: 0x000037EB File Offset: 0x000019EB
		public int area
		{
			get
			{
				return this.m_Area;
			}
			set
			{
				this.m_Area = value;
			}
		}

		// Token: 0x1700006C RID: 108
		// (get) Token: 0x06000198 RID: 408 RVA: 0x000037F8 File Offset: 0x000019F8
		// (set) Token: 0x06000199 RID: 409 RVA: 0x00003813 File Offset: 0x00001A13
		public int agentTypeID
		{
			get
			{
				return this.m_AgentTypeID;
			}
			set
			{
				this.m_AgentTypeID = value;
			}
		}

		// Token: 0x04000066 RID: 102
		private Vector3 m_StartPosition;

		// Token: 0x04000067 RID: 103
		private Vector3 m_EndPosition;

		// Token: 0x04000068 RID: 104
		private float m_CostModifier;

		// Token: 0x04000069 RID: 105
		private int m_Bidirectional;

		// Token: 0x0400006A RID: 106
		private float m_Width;

		// Token: 0x0400006B RID: 107
		private int m_Area;

		// Token: 0x0400006C RID: 108
		private int m_AgentTypeID;
	}
}
