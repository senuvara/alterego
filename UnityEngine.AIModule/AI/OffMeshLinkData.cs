﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Scripting;
using UnityEngine.Scripting.APIUpdating;

namespace UnityEngine.AI
{
	// Token: 0x02000019 RID: 25
	[MovedFrom("UnityEngine")]
	public struct OffMeshLinkData
	{
		// Token: 0x1700004B RID: 75
		// (get) Token: 0x06000154 RID: 340 RVA: 0x000033E4 File Offset: 0x000015E4
		public bool valid
		{
			get
			{
				return this.m_Valid != 0;
			}
		}

		// Token: 0x1700004C RID: 76
		// (get) Token: 0x06000155 RID: 341 RVA: 0x00003408 File Offset: 0x00001608
		public bool activated
		{
			get
			{
				return this.m_Activated != 0;
			}
		}

		// Token: 0x1700004D RID: 77
		// (get) Token: 0x06000156 RID: 342 RVA: 0x0000342C File Offset: 0x0000162C
		public OffMeshLinkType linkType
		{
			get
			{
				return this.m_LinkType;
			}
		}

		// Token: 0x1700004E RID: 78
		// (get) Token: 0x06000157 RID: 343 RVA: 0x00003448 File Offset: 0x00001648
		public Vector3 startPos
		{
			get
			{
				return this.m_StartPos;
			}
		}

		// Token: 0x1700004F RID: 79
		// (get) Token: 0x06000158 RID: 344 RVA: 0x00003464 File Offset: 0x00001664
		public Vector3 endPos
		{
			get
			{
				return this.m_EndPos;
			}
		}

		// Token: 0x17000050 RID: 80
		// (get) Token: 0x06000159 RID: 345 RVA: 0x00003480 File Offset: 0x00001680
		public OffMeshLink offMeshLink
		{
			get
			{
				return this.GetOffMeshLinkInternal(this.m_InstanceID);
			}
		}

		// Token: 0x0600015A RID: 346
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern OffMeshLink GetOffMeshLinkInternal(int instanceID);

		// Token: 0x04000057 RID: 87
		private int m_Valid;

		// Token: 0x04000058 RID: 88
		private int m_Activated;

		// Token: 0x04000059 RID: 89
		private int m_InstanceID;

		// Token: 0x0400005A RID: 90
		private OffMeshLinkType m_LinkType;

		// Token: 0x0400005B RID: 91
		private Vector3 m_StartPos;

		// Token: 0x0400005C RID: 92
		private Vector3 m_EndPos;
	}
}
