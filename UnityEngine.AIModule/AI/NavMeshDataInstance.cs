﻿using System;

namespace UnityEngine.AI
{
	// Token: 0x0200001E RID: 30
	public struct NavMeshDataInstance
	{
		// Token: 0x17000063 RID: 99
		// (get) Token: 0x06000186 RID: 390 RVA: 0x00003630 File Offset: 0x00001830
		public bool valid
		{
			get
			{
				return this.m_Handle != 0 && NavMesh.IsValidNavMeshDataHandle(this.m_Handle);
			}
		}

		// Token: 0x17000064 RID: 100
		// (get) Token: 0x06000187 RID: 391 RVA: 0x00003660 File Offset: 0x00001860
		// (set) Token: 0x06000188 RID: 392 RVA: 0x0000367B File Offset: 0x0000187B
		internal int id
		{
			get
			{
				return this.m_Handle;
			}
			set
			{
				this.m_Handle = value;
			}
		}

		// Token: 0x06000189 RID: 393 RVA: 0x00003685 File Offset: 0x00001885
		public void Remove()
		{
			NavMesh.RemoveNavMeshDataInternal(this.id);
		}

		// Token: 0x17000065 RID: 101
		// (get) Token: 0x0600018A RID: 394 RVA: 0x00003694 File Offset: 0x00001894
		// (set) Token: 0x0600018B RID: 395 RVA: 0x000036B4 File Offset: 0x000018B4
		public Object owner
		{
			get
			{
				return NavMesh.InternalGetOwner(this.id);
			}
			set
			{
				int ownerID = (!(value != null)) ? 0 : value.GetInstanceID();
				if (!NavMesh.InternalSetOwner(this.id, ownerID))
				{
					Debug.LogError("Cannot set 'owner' on an invalid NavMeshDataInstance");
				}
			}
		}

		// Token: 0x04000065 RID: 101
		private int m_Handle;
	}
}
