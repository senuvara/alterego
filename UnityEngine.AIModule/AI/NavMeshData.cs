﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Scripting;

namespace UnityEngine.AI
{
	// Token: 0x0200001D RID: 29
	public sealed class NavMeshData : Object
	{
		// Token: 0x06000179 RID: 377 RVA: 0x00003597 File Offset: 0x00001797
		public NavMeshData()
		{
			NavMeshData.Internal_Create(this, 0);
		}

		// Token: 0x0600017A RID: 378 RVA: 0x000035A7 File Offset: 0x000017A7
		public NavMeshData(int agentTypeID)
		{
			NavMeshData.Internal_Create(this, agentTypeID);
		}

		// Token: 0x0600017B RID: 379
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void Internal_Create([Writable] NavMeshData mono, int agentTypeID);

		// Token: 0x17000060 RID: 96
		// (get) Token: 0x0600017C RID: 380 RVA: 0x000035B8 File Offset: 0x000017B8
		public Bounds sourceBounds
		{
			get
			{
				Bounds result;
				this.INTERNAL_get_sourceBounds(out result);
				return result;
			}
		}

		// Token: 0x0600017D RID: 381
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_sourceBounds(out Bounds value);

		// Token: 0x17000061 RID: 97
		// (get) Token: 0x0600017E RID: 382 RVA: 0x000035D8 File Offset: 0x000017D8
		// (set) Token: 0x0600017F RID: 383 RVA: 0x000035F6 File Offset: 0x000017F6
		public Vector3 position
		{
			get
			{
				Vector3 result;
				this.INTERNAL_get_position(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_position(ref value);
			}
		}

		// Token: 0x06000180 RID: 384
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_position(out Vector3 value);

		// Token: 0x06000181 RID: 385
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_position(ref Vector3 value);

		// Token: 0x17000062 RID: 98
		// (get) Token: 0x06000182 RID: 386 RVA: 0x00003604 File Offset: 0x00001804
		// (set) Token: 0x06000183 RID: 387 RVA: 0x00003622 File Offset: 0x00001822
		public Quaternion rotation
		{
			get
			{
				Quaternion result;
				this.INTERNAL_get_rotation(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_rotation(ref value);
			}
		}

		// Token: 0x06000184 RID: 388
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_rotation(out Quaternion value);

		// Token: 0x06000185 RID: 389
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_rotation(ref Quaternion value);
	}
}
