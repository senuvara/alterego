﻿using System;
using UnityEngine.Scripting;
using UnityEngine.Scripting.APIUpdating;

namespace UnityEngine.AI
{
	// Token: 0x0200001C RID: 28
	[MovedFrom("UnityEngine")]
	[UsedByNativeCode]
	public struct NavMeshTriangulation
	{
		// Token: 0x1700005F RID: 95
		// (get) Token: 0x06000178 RID: 376 RVA: 0x0000357C File Offset: 0x0000177C
		[Obsolete("Use areas instead.")]
		public int[] layers
		{
			get
			{
				return this.areas;
			}
		}

		// Token: 0x04000062 RID: 98
		public Vector3[] vertices;

		// Token: 0x04000063 RID: 99
		public int[] indices;

		// Token: 0x04000064 RID: 100
		public int[] areas;
	}
}
