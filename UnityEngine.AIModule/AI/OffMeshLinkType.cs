﻿using System;
using UnityEngine.Scripting.APIUpdating;

namespace UnityEngine.AI
{
	// Token: 0x02000018 RID: 24
	[MovedFrom("UnityEngine")]
	public enum OffMeshLinkType
	{
		// Token: 0x04000054 RID: 84
		LinkTypeManual,
		// Token: 0x04000055 RID: 85
		LinkTypeDropDown,
		// Token: 0x04000056 RID: 86
		LinkTypeJumpAcross
	}
}
