﻿using System;
using System.Runtime.CompilerServices;
using Unity.Jobs;
using UnityEngine.Bindings;

namespace UnityEngine.Experimental.AI
{
	// Token: 0x02000006 RID: 6
	[StaticAccessor("NavMeshWorldBindings", StaticAccessorType.DoubleColon)]
	public struct NavMeshWorld
	{
		// Token: 0x0600000A RID: 10 RVA: 0x00002198 File Offset: 0x00000398
		public bool IsValid()
		{
			return this.world != IntPtr.Zero;
		}

		// Token: 0x0600000B RID: 11 RVA: 0x000021C0 File Offset: 0x000003C0
		public static NavMeshWorld GetDefaultWorld()
		{
			NavMeshWorld result;
			NavMeshWorld.GetDefaultWorld_Injected(out result);
			return result;
		}

		// Token: 0x0600000C RID: 12 RVA: 0x000021D5 File Offset: 0x000003D5
		private static void AddDependencyInternal(IntPtr navmesh, JobHandle handle)
		{
			NavMeshWorld.AddDependencyInternal_Injected(navmesh, ref handle);
		}

		// Token: 0x0600000D RID: 13 RVA: 0x000021DF File Offset: 0x000003DF
		public void AddDependency(JobHandle job)
		{
			NavMeshWorld.AddDependencyInternal(this.world, job);
		}

		// Token: 0x0600000E RID: 14
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void GetDefaultWorld_Injected(out NavMeshWorld ret);

		// Token: 0x0600000F RID: 15
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void AddDependencyInternal_Injected(IntPtr navmesh, ref JobHandle handle);

		// Token: 0x04000013 RID: 19
		internal IntPtr world;
	}
}
