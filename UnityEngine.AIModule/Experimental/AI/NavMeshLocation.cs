﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace UnityEngine.Experimental.AI
{
	// Token: 0x02000003 RID: 3
	public struct NavMeshLocation
	{
		// Token: 0x06000007 RID: 7 RVA: 0x0000214C File Offset: 0x0000034C
		internal NavMeshLocation(Vector3 position, PolygonId polygon)
		{
			this.position = position;
			this.polygon = polygon;
		}

		// Token: 0x17000001 RID: 1
		// (get) Token: 0x06000008 RID: 8 RVA: 0x00002160 File Offset: 0x00000360
		public PolygonId polygon
		{
			[CompilerGenerated]
			get
			{
				return this.<polygon>k__BackingField;
			}
		}

		// Token: 0x17000002 RID: 2
		// (get) Token: 0x06000009 RID: 9 RVA: 0x0000217C File Offset: 0x0000037C
		public Vector3 position
		{
			[CompilerGenerated]
			get
			{
				return this.<position>k__BackingField;
			}
		}

		// Token: 0x04000002 RID: 2
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private readonly PolygonId <polygon>k__BackingField;

		// Token: 0x04000003 RID: 3
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private readonly Vector3 <position>k__BackingField;
	}
}
