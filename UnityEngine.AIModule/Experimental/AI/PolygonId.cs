﻿using System;

namespace UnityEngine.Experimental.AI
{
	// Token: 0x02000002 RID: 2
	public struct PolygonId : IEquatable<PolygonId>
	{
		// Token: 0x06000001 RID: 1 RVA: 0x00002050 File Offset: 0x00000250
		public bool IsNull()
		{
			return this.polyRef == 0UL;
		}

		// Token: 0x06000002 RID: 2 RVA: 0x00002070 File Offset: 0x00000270
		public static bool operator ==(PolygonId x, PolygonId y)
		{
			return x.polyRef == y.polyRef;
		}

		// Token: 0x06000003 RID: 3 RVA: 0x00002098 File Offset: 0x00000298
		public static bool operator !=(PolygonId x, PolygonId y)
		{
			return x.polyRef != y.polyRef;
		}

		// Token: 0x06000004 RID: 4 RVA: 0x000020C0 File Offset: 0x000002C0
		public override int GetHashCode()
		{
			return this.polyRef.GetHashCode();
		}

		// Token: 0x06000005 RID: 5 RVA: 0x000020E8 File Offset: 0x000002E8
		public bool Equals(PolygonId rhs)
		{
			return rhs == this;
		}

		// Token: 0x06000006 RID: 6 RVA: 0x0000210C File Offset: 0x0000030C
		public override bool Equals(object obj)
		{
			bool result;
			if (obj == null || !(obj is PolygonId))
			{
				result = false;
			}
			else
			{
				PolygonId x = (PolygonId)obj;
				result = (x == this);
			}
			return result;
		}

		// Token: 0x04000001 RID: 1
		internal ulong polyRef;
	}
}
