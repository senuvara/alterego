﻿using System;

namespace UnityEngine.Experimental.AI
{
	// Token: 0x02000005 RID: 5
	public enum NavMeshPolyTypes
	{
		// Token: 0x04000011 RID: 17
		Ground,
		// Token: 0x04000012 RID: 18
		OffMeshConnection
	}
}
