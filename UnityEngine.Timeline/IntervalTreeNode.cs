﻿using System;

namespace UnityEngine
{
	// Token: 0x02000038 RID: 56
	internal struct IntervalTreeNode
	{
		// Token: 0x040000DA RID: 218
		public long center;

		// Token: 0x040000DB RID: 219
		public int first;

		// Token: 0x040000DC RID: 220
		public int last;

		// Token: 0x040000DD RID: 221
		public int left;

		// Token: 0x040000DE RID: 222
		public int right;
	}
}
