﻿using System;

namespace UnityEngine
{
	// Token: 0x02000037 RID: 55
	internal interface IInterval
	{
		// Token: 0x1700007B RID: 123
		// (get) Token: 0x060001B8 RID: 440
		long intervalStart { get; }

		// Token: 0x1700007C RID: 124
		// (get) Token: 0x060001B9 RID: 441
		long intervalEnd { get; }

		// Token: 0x1700007D RID: 125
		// (get) Token: 0x060001BA RID: 442
		// (set) Token: 0x060001BB RID: 443
		int intervalBit { get; set; }
	}
}
