﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	// Token: 0x02000039 RID: 57
	internal class IntervalTree<T> where T : class, IInterval
	{
		// Token: 0x060001BC RID: 444 RVA: 0x00009312 File Offset: 0x00007512
		public IntervalTree()
		{
		}

		// Token: 0x1700007E RID: 126
		// (get) Token: 0x060001BD RID: 445 RVA: 0x00009330 File Offset: 0x00007530
		// (set) Token: 0x060001BE RID: 446 RVA: 0x0000934A File Offset: 0x0000754A
		public bool dirty
		{
			[CompilerGenerated]
			get
			{
				return this.<dirty>k__BackingField;
			}
			[CompilerGenerated]
			internal set
			{
				this.<dirty>k__BackingField = value;
			}
		}

		// Token: 0x060001BF RID: 447 RVA: 0x00009354 File Offset: 0x00007554
		public void Add(T item)
		{
			if (item != null)
			{
				this.m_Entries.Add(new IntervalTree<T>.Entry
				{
					intervalStart = item.intervalStart,
					intervalEnd = item.intervalEnd,
					item = item
				});
				this.dirty = true;
			}
		}

		// Token: 0x060001C0 RID: 448 RVA: 0x000093C0 File Offset: 0x000075C0
		public void IntersectsWith(long value, int bitFlag, List<T> results)
		{
			if (this.m_Entries.Count != 0)
			{
				if (this.dirty)
				{
					this.Rebuild();
					this.dirty = false;
				}
				if (this.m_Nodes.Count > 0)
				{
					this.Query(this.m_Nodes[0], value, bitFlag, results);
				}
			}
		}

		// Token: 0x060001C1 RID: 449 RVA: 0x00009424 File Offset: 0x00007624
		public void UpdateIntervals()
		{
			bool flag = false;
			for (int i = 0; i < this.m_Entries.Count; i++)
			{
				IntervalTree<T>.Entry entry = this.m_Entries[i];
				long intervalStart = entry.item.intervalStart;
				long intervalEnd = entry.item.intervalEnd;
				flag |= (entry.intervalStart != intervalStart);
				flag |= (entry.intervalEnd != intervalEnd);
				this.m_Entries[i] = new IntervalTree<T>.Entry
				{
					intervalStart = intervalStart,
					intervalEnd = intervalEnd,
					item = entry.item
				};
			}
			this.dirty = (this.dirty || flag);
		}

		// Token: 0x060001C2 RID: 450 RVA: 0x000094EC File Offset: 0x000076EC
		private void Query(IntervalTreeNode node, long value, int bitflag, List<T> results)
		{
			for (int i = node.first; i <= node.last; i++)
			{
				IntervalTree<T>.Entry entry = this.m_Entries[i];
				if (value >= entry.intervalStart && value < entry.intervalEnd)
				{
					entry.item.intervalBit = bitflag;
					results.Add(entry.item);
				}
			}
			if (node.center != 9223372036854775807L)
			{
				if (node.left != -1 && value < node.center)
				{
					this.Query(this.m_Nodes[node.left], value, bitflag, results);
				}
				if (node.right != -1 && value > node.center)
				{
					this.Query(this.m_Nodes[node.right], value, bitflag, results);
				}
			}
		}

		// Token: 0x060001C3 RID: 451 RVA: 0x000095EA File Offset: 0x000077EA
		private void Rebuild()
		{
			this.m_Nodes.Clear();
			this.m_Nodes.Capacity = this.m_Entries.Capacity;
			this.Rebuild(0, this.m_Entries.Count - 1);
		}

		// Token: 0x060001C4 RID: 452 RVA: 0x00009624 File Offset: 0x00007824
		private int Rebuild(int start, int end)
		{
			IntervalTreeNode intervalTreeNode = default(IntervalTreeNode);
			int num = end - start + 1;
			int result;
			if (num < 10)
			{
				intervalTreeNode = new IntervalTreeNode
				{
					center = long.MaxValue,
					first = start,
					last = end,
					left = -1,
					right = -1
				};
				this.m_Nodes.Add(intervalTreeNode);
				result = this.m_Nodes.Count - 1;
			}
			else
			{
				long num2 = long.MaxValue;
				long num3 = long.MinValue;
				for (int i = start; i <= end; i++)
				{
					IntervalTree<T>.Entry entry = this.m_Entries[i];
					num2 = Math.Min(num2, entry.intervalStart);
					num3 = Math.Max(num3, entry.intervalEnd);
				}
				long num4 = (num3 + num2) / 2L;
				intervalTreeNode.center = num4;
				int num5 = start;
				int num6 = end;
				for (;;)
				{
					while (num5 <= end && this.m_Entries[num5].intervalEnd < num4)
					{
						num5++;
					}
					while (num6 >= start && this.m_Entries[num6].intervalEnd >= num4)
					{
						num6--;
					}
					if (num5 > num6)
					{
						break;
					}
					IntervalTree<T>.Entry value = this.m_Entries[num5];
					IntervalTree<T>.Entry value2 = this.m_Entries[num6];
					this.m_Entries[num6] = value;
					this.m_Entries[num5] = value2;
				}
				intervalTreeNode.first = num5;
				num6 = end;
				for (;;)
				{
					while (num5 <= end && this.m_Entries[num5].intervalStart <= num4)
					{
						num5++;
					}
					while (num6 >= start && this.m_Entries[num6].intervalStart > num4)
					{
						num6--;
					}
					if (num5 > num6)
					{
						break;
					}
					IntervalTree<T>.Entry value3 = this.m_Entries[num5];
					IntervalTree<T>.Entry value4 = this.m_Entries[num6];
					this.m_Entries[num6] = value3;
					this.m_Entries[num5] = value4;
				}
				intervalTreeNode.last = num6;
				this.m_Nodes.Add(default(IntervalTreeNode));
				int num7 = this.m_Nodes.Count - 1;
				intervalTreeNode.left = -1;
				intervalTreeNode.right = -1;
				if (start < intervalTreeNode.first)
				{
					intervalTreeNode.left = this.Rebuild(start, intervalTreeNode.first - 1);
				}
				if (end > intervalTreeNode.last)
				{
					intervalTreeNode.right = this.Rebuild(intervalTreeNode.last + 1, end);
				}
				this.m_Nodes[num7] = intervalTreeNode;
				result = num7;
			}
			return result;
		}

		// Token: 0x040000DF RID: 223
		private const int kMinNodeSize = 10;

		// Token: 0x040000E0 RID: 224
		private const int kInvalidNode = -1;

		// Token: 0x040000E1 RID: 225
		private const long kCenterUnknown = 9223372036854775807L;

		// Token: 0x040000E2 RID: 226
		private readonly List<IntervalTree<T>.Entry> m_Entries = new List<IntervalTree<T>.Entry>();

		// Token: 0x040000E3 RID: 227
		private readonly List<IntervalTreeNode> m_Nodes = new List<IntervalTreeNode>();

		// Token: 0x040000E4 RID: 228
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private bool <dirty>k__BackingField;

		// Token: 0x0200003A RID: 58
		internal struct Entry
		{
			// Token: 0x040000E5 RID: 229
			public long intervalStart;

			// Token: 0x040000E6 RID: 230
			public long intervalEnd;

			// Token: 0x040000E7 RID: 231
			public T item;
		}
	}
}
