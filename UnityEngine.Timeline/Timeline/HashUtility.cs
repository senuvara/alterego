﻿using System;

namespace UnityEngine.Timeline
{
	// Token: 0x0200004B RID: 75
	internal static class HashUtility
	{
		// Token: 0x06000226 RID: 550 RVA: 0x0000AFD0 File Offset: 0x000091D0
		public static int CombineHash(this int h1, int h2)
		{
			return h1 ^ (int)((long)h2 + (long)((ulong)-1640531527) + (long)((long)h1 << 6) + (long)(h1 >> 2));
		}

		// Token: 0x06000227 RID: 551 RVA: 0x0000AFFC File Offset: 0x000091FC
		public static int CombineHash(int h1, int h2, int h3)
		{
			return h1.CombineHash(h2).CombineHash(h3);
		}

		// Token: 0x06000228 RID: 552 RVA: 0x0000B020 File Offset: 0x00009220
		public static int CombineHash(int h1, int h2, int h3, int h4)
		{
			return HashUtility.CombineHash(h1, h2, h3).CombineHash(h4);
		}

		// Token: 0x06000229 RID: 553 RVA: 0x0000B044 File Offset: 0x00009244
		public static int CombineHash(int h1, int h2, int h3, int h4, int h5)
		{
			return HashUtility.CombineHash(h1, h2, h3, h4).CombineHash(h5);
		}

		// Token: 0x0600022A RID: 554 RVA: 0x0000B06C File Offset: 0x0000926C
		public static int CombineHash(int h1, int h2, int h3, int h4, int h5, int h6)
		{
			return HashUtility.CombineHash(h1, h2, h3, h4, h5).CombineHash(h6);
		}

		// Token: 0x0600022B RID: 555 RVA: 0x0000B094 File Offset: 0x00009294
		public static int CombineHash(int h1, int h2, int h3, int h4, int h5, int h6, int h7)
		{
			return HashUtility.CombineHash(h1, h2, h3, h4, h5, h6).CombineHash(h7);
		}

		// Token: 0x0600022C RID: 556 RVA: 0x0000B0C0 File Offset: 0x000092C0
		public static int CombineHash(int[] hashes)
		{
			int result;
			if (hashes == null || hashes.Length == 0)
			{
				result = 0;
			}
			else
			{
				int num = hashes[0];
				for (int i = 1; i < hashes.Length; i++)
				{
					num = num.CombineHash(hashes[i]);
				}
				result = num;
			}
			return result;
		}
	}
}
