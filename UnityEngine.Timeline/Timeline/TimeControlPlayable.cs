﻿using System;
using UnityEngine.Playables;

namespace UnityEngine.Timeline
{
	// Token: 0x02000048 RID: 72
	public class TimeControlPlayable : PlayableBehaviour
	{
		// Token: 0x0600021A RID: 538 RVA: 0x0000AC18 File Offset: 0x00008E18
		public TimeControlPlayable()
		{
		}

		// Token: 0x0600021B RID: 539 RVA: 0x0000AC20 File Offset: 0x00008E20
		public static ScriptPlayable<TimeControlPlayable> Create(PlayableGraph graph, ITimeControl timeControl)
		{
			ScriptPlayable<TimeControlPlayable> result;
			if (timeControl == null)
			{
				result = ScriptPlayable<TimeControlPlayable>.Null;
			}
			else
			{
				ScriptPlayable<TimeControlPlayable> scriptPlayable = ScriptPlayable<TimeControlPlayable>.Create(graph, 0);
				scriptPlayable.GetBehaviour().Initialize(timeControl);
				result = scriptPlayable;
			}
			return result;
		}

		// Token: 0x0600021C RID: 540 RVA: 0x0000AC5C File Offset: 0x00008E5C
		public void Initialize(ITimeControl timeControl)
		{
			this.m_timeControl = timeControl;
		}

		// Token: 0x0600021D RID: 541 RVA: 0x0000AC66 File Offset: 0x00008E66
		public override void PrepareFrame(Playable playable, FrameData info)
		{
			if (this.m_timeControl != null)
			{
				this.m_timeControl.SetTime(playable.GetTime<Playable>());
			}
		}

		// Token: 0x0600021E RID: 542 RVA: 0x0000AC85 File Offset: 0x00008E85
		public override void OnBehaviourPlay(Playable playable, FrameData info)
		{
			if (this.m_timeControl != null)
			{
				if (!this.m_started)
				{
					this.m_timeControl.OnControlTimeStart();
					this.m_started = true;
				}
			}
		}

		// Token: 0x0600021F RID: 543 RVA: 0x0000ACB7 File Offset: 0x00008EB7
		public override void OnBehaviourPause(Playable playable, FrameData info)
		{
			if (this.m_timeControl != null)
			{
				if (this.m_started)
				{
					this.m_timeControl.OnControlTimeStop();
					this.m_started = false;
				}
			}
		}

		// Token: 0x04000106 RID: 262
		private ITimeControl m_timeControl;

		// Token: 0x04000107 RID: 263
		private bool m_started;
	}
}
