﻿using System;

namespace UnityEngine.Timeline
{
	// Token: 0x02000011 RID: 17
	[AttributeUsage(AttributeTargets.Class, Inherited = false)]
	internal class SupportsChildTracksAttribute : Attribute
	{
		// Token: 0x06000068 RID: 104 RVA: 0x00004B78 File Offset: 0x00002D78
		public SupportsChildTracksAttribute(Type childType = null, int levels = 2147483647)
		{
			this.childType = childType;
			this.levels = levels;
		}

		// Token: 0x0400002E RID: 46
		public readonly Type childType;

		// Token: 0x0400002F RID: 47
		public readonly int levels;
	}
}
