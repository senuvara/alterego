﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine.Playables;

namespace UnityEngine.Timeline
{
	// Token: 0x02000046 RID: 70
	public class ParticleControlPlayable : PlayableBehaviour
	{
		// Token: 0x06000209 RID: 521 RVA: 0x0000A6A8 File Offset: 0x000088A8
		public ParticleControlPlayable()
		{
		}

		// Token: 0x0600020A RID: 522 RVA: 0x0000A6C4 File Offset: 0x000088C4
		public static ScriptPlayable<ParticleControlPlayable> Create(PlayableGraph graph, ParticleSystem component, uint randomSeed)
		{
			ScriptPlayable<ParticleControlPlayable> result;
			if (component == null)
			{
				result = ScriptPlayable<ParticleControlPlayable>.Null;
			}
			else
			{
				ScriptPlayable<ParticleControlPlayable> scriptPlayable = ScriptPlayable<ParticleControlPlayable>.Create(graph, 0);
				scriptPlayable.GetBehaviour().Initialize(component, randomSeed);
				result = scriptPlayable;
			}
			return result;
		}

		// Token: 0x17000095 RID: 149
		// (get) Token: 0x0600020B RID: 523 RVA: 0x0000A708 File Offset: 0x00008908
		// (set) Token: 0x0600020C RID: 524 RVA: 0x0000A722 File Offset: 0x00008922
		public ParticleSystem particleSystem
		{
			[CompilerGenerated]
			get
			{
				return this.<particleSystem>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<particleSystem>k__BackingField = value;
			}
		}

		// Token: 0x0600020D RID: 525 RVA: 0x0000A72B File Offset: 0x0000892B
		public void Initialize(ParticleSystem ps, uint randomSeed)
		{
			this.m_RandomSeed = Math.Max(1U, randomSeed);
			this.particleSystem = ps;
			this.m_SystemTime = 0f;
			this.SetRandomSeed();
		}

		// Token: 0x0600020E RID: 526 RVA: 0x0000A754 File Offset: 0x00008954
		private void SetRandomSeed()
		{
			this.particleSystem.Stop(true, ParticleSystemStopBehavior.StopEmittingAndClear);
			ParticleSystem[] componentsInChildren = this.particleSystem.gameObject.GetComponentsInChildren<ParticleSystem>();
			uint num = this.m_RandomSeed;
			foreach (ParticleSystem particleSystem in componentsInChildren)
			{
				if (particleSystem.useAutoRandomSeed)
				{
					particleSystem.useAutoRandomSeed = false;
					particleSystem.randomSeed = num;
					num += 1U;
				}
			}
		}

		// Token: 0x0600020F RID: 527 RVA: 0x0000A7C8 File Offset: 0x000089C8
		public override void PrepareFrame(Playable playable, FrameData data)
		{
			if (!(this.particleSystem == null) && this.particleSystem.gameObject.activeInHierarchy)
			{
				float num = (float)playable.GetTime<Playable>();
				bool flag = Mathf.Approximately(this.m_LastTime, -1f) || !Mathf.Approximately(this.m_LastTime, num);
				if (flag)
				{
					float num2 = Time.fixedDeltaTime * 0.5f;
					float num3 = num;
					float num4 = num3 - this.m_LastTime;
					float num5 = this.particleSystem.main.startDelay.Evaluate(this.particleSystem.randomSeed);
					float num6 = this.particleSystem.main.duration + num5;
					float num7 = (num3 <= num6) ? (this.m_SystemTime - num5) : this.m_SystemTime;
					bool flag2 = num3 < this.m_LastTime || num3 < num2 || Mathf.Approximately(this.m_LastTime, -1f) || num4 > this.particleSystem.main.duration || Mathf.Abs(num7 - this.particleSystem.time) >= Time.maximumParticleDeltaTime;
					if (flag2)
					{
						this.particleSystem.Simulate(0f, true, true);
						this.particleSystem.Simulate(num3, true, false);
						this.m_SystemTime = num3;
					}
					else
					{
						float num8 = (num3 <= num6) ? num6 : this.particleSystem.main.duration;
						float num9 = num3 % num8;
						float num10 = num9 - this.m_SystemTime;
						if (num10 < -num2)
						{
							num10 = num9 + num6 - this.m_SystemTime;
						}
						this.particleSystem.Simulate(num10, true, false);
						this.m_SystemTime += num10;
					}
					this.m_LastTime = num;
				}
			}
		}

		// Token: 0x06000210 RID: 528 RVA: 0x0000A9C2 File Offset: 0x00008BC2
		public override void OnBehaviourPlay(Playable playable, FrameData info)
		{
			this.m_LastTime = -1f;
		}

		// Token: 0x06000211 RID: 529 RVA: 0x0000A9D0 File Offset: 0x00008BD0
		public override void OnBehaviourPause(Playable playable, FrameData info)
		{
			this.m_LastTime = -1f;
		}

		// Token: 0x04000100 RID: 256
		private const float kUnsetTime = -1f;

		// Token: 0x04000101 RID: 257
		private float m_LastTime = -1f;

		// Token: 0x04000102 RID: 258
		private uint m_RandomSeed = 1U;

		// Token: 0x04000103 RID: 259
		private float m_SystemTime;

		// Token: 0x04000104 RID: 260
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private ParticleSystem <particleSystem>k__BackingField;
	}
}
