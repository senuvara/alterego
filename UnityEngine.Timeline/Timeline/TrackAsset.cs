﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading;
using UnityEngine.Animations;
using UnityEngine.Playables;
using UnityEngine.Serialization;

namespace UnityEngine.Timeline
{
	// Token: 0x0200001B RID: 27
	[IgnoreOnPlayableTrack]
	[Serializable]
	public abstract class TrackAsset : PlayableAsset, IPropertyPreview, ISerializationCallbackReceiver
	{
		// Token: 0x060000CD RID: 205 RVA: 0x00002724 File Offset: 0x00000924
		protected TrackAsset()
		{
		}

		// Token: 0x17000037 RID: 55
		// (get) Token: 0x060000CE RID: 206 RVA: 0x00002744 File Offset: 0x00000944
		public double start
		{
			get
			{
				this.UpdateDuration();
				return (double)this.m_Start;
			}
		}

		// Token: 0x17000038 RID: 56
		// (get) Token: 0x060000CF RID: 207 RVA: 0x0000276C File Offset: 0x0000096C
		public double end
		{
			get
			{
				this.UpdateDuration();
				return (double)this.m_End;
			}
		}

		// Token: 0x17000039 RID: 57
		// (get) Token: 0x060000D0 RID: 208 RVA: 0x00002794 File Offset: 0x00000994
		public sealed override double duration
		{
			get
			{
				this.UpdateDuration();
				return (double)(this.m_End - this.m_Start);
			}
		}

		// Token: 0x1700003A RID: 58
		// (get) Token: 0x060000D1 RID: 209 RVA: 0x000027C8 File Offset: 0x000009C8
		// (set) Token: 0x060000D2 RID: 210 RVA: 0x000027E3 File Offset: 0x000009E3
		public bool muted
		{
			get
			{
				return this.m_Muted;
			}
			set
			{
				this.m_Muted = value;
			}
		}

		// Token: 0x1700003B RID: 59
		// (get) Token: 0x060000D3 RID: 211 RVA: 0x000027F0 File Offset: 0x000009F0
		public TimelineAsset timelineAsset
		{
			get
			{
				TrackAsset trackAsset = this;
				while (trackAsset != null)
				{
					TimelineAsset result;
					if (trackAsset.parent == null)
					{
						result = null;
					}
					else
					{
						TimelineAsset timelineAsset = trackAsset.parent as TimelineAsset;
						if (!(timelineAsset != null))
						{
							trackAsset = (trackAsset.parent as TrackAsset);
							continue;
						}
						result = timelineAsset;
					}
					return result;
				}
				return null;
			}
		}

		// Token: 0x1700003C RID: 60
		// (get) Token: 0x060000D4 RID: 212 RVA: 0x00002860 File Offset: 0x00000A60
		// (set) Token: 0x060000D5 RID: 213 RVA: 0x0000287B File Offset: 0x00000A7B
		public PlayableAsset parent
		{
			get
			{
				return this.m_Parent;
			}
			internal set
			{
				this.m_Parent = value;
			}
		}

		// Token: 0x060000D6 RID: 214 RVA: 0x00002888 File Offset: 0x00000A88
		public IEnumerable<TimelineClip> GetClips()
		{
			return this.clips;
		}

		// Token: 0x1700003D RID: 61
		// (get) Token: 0x060000D7 RID: 215 RVA: 0x000028A4 File Offset: 0x00000AA4
		internal TimelineClip[] clips
		{
			get
			{
				if (this.m_Clips == null)
				{
					this.m_Clips = new List<TimelineClip>();
				}
				if (this.m_ClipsCache == null)
				{
					this.m_CacheSorted = false;
					this.m_ClipsCache = this.m_Clips.ToArray();
				}
				return this.m_ClipsCache;
			}
		}

		// Token: 0x1700003E RID: 62
		// (get) Token: 0x060000D8 RID: 216 RVA: 0x000028FC File Offset: 0x00000AFC
		public virtual bool isEmpty
		{
			get
			{
				return !this.hasClips;
			}
		}

		// Token: 0x1700003F RID: 63
		// (get) Token: 0x060000D9 RID: 217 RVA: 0x0000291C File Offset: 0x00000B1C
		internal bool hasClips
		{
			get
			{
				return this.m_Clips != null && this.m_Clips.Count != 0;
			}
		}

		// Token: 0x17000040 RID: 64
		// (get) Token: 0x060000DA RID: 218 RVA: 0x00002950 File Offset: 0x00000B50
		public bool isSubTrack
		{
			get
			{
				TrackAsset trackAsset = this.parent as TrackAsset;
				return trackAsset != null && trackAsset.GetType() == base.GetType();
			}
		}

		// Token: 0x17000041 RID: 65
		// (get) Token: 0x060000DB RID: 219 RVA: 0x00002990 File Offset: 0x00000B90
		public override IEnumerable<PlayableBinding> outputs
		{
			get
			{
				TrackBindingTypeAttribute attribute;
				if (!TrackAsset.s_TrackBindingTypeAttributeCache.TryGetValue(base.GetType(), out attribute))
				{
					attribute = (TrackBindingTypeAttribute)Attribute.GetCustomAttribute(base.GetType(), typeof(TrackBindingTypeAttribute));
					TrackAsset.s_TrackBindingTypeAttributeCache.Add(base.GetType(), attribute);
				}
				Type trackBindingType = (attribute == null) ? null : attribute.type;
				yield return ScriptPlayableBinding.Create(base.name, this, trackBindingType);
				yield break;
			}
		}

		// Token: 0x060000DC RID: 220 RVA: 0x000029BC File Offset: 0x00000BBC
		public IEnumerable<TrackAsset> GetChildTracks()
		{
			this.UpdateChildTrackCache();
			return this.m_ChildTrackCache;
		}

		// Token: 0x17000042 RID: 66
		// (get) Token: 0x060000DD RID: 221 RVA: 0x000029E0 File Offset: 0x00000BE0
		// (set) Token: 0x060000DE RID: 222 RVA: 0x000029FB File Offset: 0x00000BFB
		internal string customPlayableTypename
		{
			get
			{
				return this.m_CustomPlayableFullTypename;
			}
			set
			{
				this.m_CustomPlayableFullTypename = value;
			}
		}

		// Token: 0x17000043 RID: 67
		// (get) Token: 0x060000DF RID: 223 RVA: 0x00002A08 File Offset: 0x00000C08
		// (set) Token: 0x060000E0 RID: 224 RVA: 0x00002A23 File Offset: 0x00000C23
		internal AnimationClip animClip
		{
			get
			{
				return this.m_AnimClip;
			}
			set
			{
				this.m_AnimClip = value;
			}
		}

		// Token: 0x17000044 RID: 68
		// (get) Token: 0x060000E1 RID: 225 RVA: 0x00002A30 File Offset: 0x00000C30
		internal List<ScriptableObject> subTracksObjects
		{
			get
			{
				return this.m_Children;
			}
		}

		// Token: 0x17000045 RID: 69
		// (get) Token: 0x060000E2 RID: 226 RVA: 0x00002A4C File Offset: 0x00000C4C
		// (set) Token: 0x060000E3 RID: 227 RVA: 0x00002A67 File Offset: 0x00000C67
		public bool locked
		{
			get
			{
				return this.m_Locked;
			}
			set
			{
				this.m_Locked = value;
			}
		}

		// Token: 0x17000046 RID: 70
		// (get) Token: 0x060000E4 RID: 228 RVA: 0x00002A74 File Offset: 0x00000C74
		public bool lockedInHierarchy
		{
			get
			{
				return this.locked || this.parentLocked;
			}
		}

		// Token: 0x17000047 RID: 71
		// (get) Token: 0x060000E5 RID: 229 RVA: 0x00002AA0 File Offset: 0x00000CA0
		internal bool parentLocked
		{
			get
			{
				TrackAsset trackAsset = this.parent as TrackAsset;
				return trackAsset != null && trackAsset.lockedInHierarchy;
			}
		}

		// Token: 0x17000048 RID: 72
		// (get) Token: 0x060000E6 RID: 230 RVA: 0x00002AD8 File Offset: 0x00000CD8
		internal virtual bool compilable
		{
			get
			{
				return !this.muted && !this.isEmpty;
			}
		}

		// Token: 0x060000E7 RID: 231 RVA: 0x00002B04 File Offset: 0x00000D04
		private void __internalAwake()
		{
			if (this.m_Clips == null)
			{
				this.m_Clips = new List<TimelineClip>();
			}
			this.m_ChildTrackCache = null;
			if (this.m_Children == null)
			{
				this.m_Children = new List<ScriptableObject>();
			}
			for (int i = this.m_Children.Count - 1; i >= 0; i--)
			{
			}
		}

		// Token: 0x060000E8 RID: 232 RVA: 0x00002B68 File Offset: 0x00000D68
		public virtual Playable CreateTrackMixer(PlayableGraph graph, GameObject go, int inputCount)
		{
			return Playable.Create(graph, inputCount);
		}

		// Token: 0x060000E9 RID: 233 RVA: 0x00002B84 File Offset: 0x00000D84
		public sealed override Playable CreatePlayable(PlayableGraph graph, GameObject go)
		{
			return Playable.Null;
		}

		// Token: 0x060000EA RID: 234 RVA: 0x00002BA0 File Offset: 0x00000DA0
		public TimelineClip CreateDefaultClip()
		{
			object[] customAttributes = base.GetType().GetCustomAttributes(typeof(TrackClipTypeAttribute), true);
			Type type = null;
			foreach (object obj in customAttributes)
			{
				TrackClipTypeAttribute trackClipTypeAttribute = obj as TrackClipTypeAttribute;
				if (trackClipTypeAttribute != null && typeof(IPlayableAsset).IsAssignableFrom(trackClipTypeAttribute.inspectedType) && typeof(ScriptableObject).IsAssignableFrom(trackClipTypeAttribute.inspectedType))
				{
					type = trackClipTypeAttribute.inspectedType;
					break;
				}
			}
			TimelineClip result;
			if (type == null)
			{
				Debug.LogWarning("Cannot create a default clip for type " + base.GetType());
				result = null;
			}
			else
			{
				result = this.CreateAndAddNewClipOfType(type);
			}
			return result;
		}

		// Token: 0x060000EB RID: 235 RVA: 0x00002C70 File Offset: 0x00000E70
		public TimelineClip CreateClip<T>() where T : ScriptableObject, IPlayableAsset
		{
			return this.CreateClip(typeof(T));
		}

		// Token: 0x060000EC RID: 236 RVA: 0x00002C98 File Offset: 0x00000E98
		internal TimelineClip CreateClip(Type requestedType)
		{
			if (this.ValidateClipType(requestedType))
			{
				return this.CreateAndAddNewClipOfType(requestedType);
			}
			throw new InvalidOperationException(string.Concat(new object[]
			{
				"Clips of type ",
				requestedType,
				" are not permitted on tracks of type ",
				base.GetType()
			}));
		}

		// Token: 0x060000ED RID: 237 RVA: 0x00002CF0 File Offset: 0x00000EF0
		internal TimelineClip CreateAndAddNewClipOfType(Type requestedType)
		{
			TimelineClip timelineClip = this.CreateClipOfType(requestedType);
			this.AddClip(timelineClip);
			return timelineClip;
		}

		// Token: 0x060000EE RID: 238 RVA: 0x00002D18 File Offset: 0x00000F18
		internal TimelineClip CreateClipOfType(Type requestedType)
		{
			if (!this.ValidateClipType(requestedType))
			{
				throw new InvalidOperationException(string.Concat(new object[]
				{
					"Clips of type ",
					requestedType,
					" are not permitted on tracks of type ",
					base.GetType()
				}));
			}
			ScriptableObject scriptableObject = ScriptableObject.CreateInstance(requestedType);
			if (scriptableObject == null)
			{
				throw new InvalidOperationException("Could not create an instance of the ScriptableObject type " + requestedType.Name);
			}
			scriptableObject.name = requestedType.Name;
			TimelineCreateUtilities.SaveAssetIntoObject(scriptableObject, this);
			return this.CreateClipFromAsset(scriptableObject);
		}

		// Token: 0x060000EF RID: 239 RVA: 0x00002DAC File Offset: 0x00000FAC
		internal TimelineClip CreateClipFromPlayableAsset(IPlayableAsset asset)
		{
			if (asset == null)
			{
				throw new ArgumentNullException("asset");
			}
			if (asset as ScriptableObject == null)
			{
				throw new ArgumentException("CreateClipFromPlayableAsset  only supports ScriptableObject-derived Types");
			}
			if (!this.ValidateClipType(asset.GetType()))
			{
				throw new InvalidOperationException(string.Concat(new object[]
				{
					"Clips of type ",
					asset.GetType(),
					" are not permitted on tracks of type ",
					base.GetType()
				}));
			}
			return this.CreateClipFromAsset(asset as ScriptableObject);
		}

		// Token: 0x060000F0 RID: 240 RVA: 0x00002E40 File Offset: 0x00001040
		private TimelineClip CreateClipFromAsset(ScriptableObject playableAsset)
		{
			TimelineClip timelineClip = this.CreateNewClipContainerInternal();
			timelineClip.displayName = playableAsset.name;
			timelineClip.asset = playableAsset;
			IPlayableAsset playableAsset2 = playableAsset as IPlayableAsset;
			if (playableAsset2 != null)
			{
				double duration = playableAsset2.duration;
				if (!double.IsInfinity(duration) && duration > 0.0)
				{
					timelineClip.duration = Math.Min(Math.Max(duration, TimelineClip.kMinDuration), TimelineClip.kMaxTimeValue);
				}
			}
			try
			{
				this.OnCreateClip(timelineClip);
			}
			catch (Exception ex)
			{
				Debug.LogError(ex.Message, playableAsset);
				return null;
			}
			return timelineClip;
		}

		// Token: 0x060000F1 RID: 241 RVA: 0x00002EF4 File Offset: 0x000010F4
		internal void AddClip(TimelineClip newClip)
		{
			if (!this.m_Clips.Contains(newClip))
			{
				this.m_Clips.Add(newClip);
				this.m_ClipsCache = null;
			}
		}

		// Token: 0x060000F2 RID: 242 RVA: 0x00002F20 File Offset: 0x00001120
		internal Playable CreatePlayableGraph(PlayableGraph graph, GameObject go, IntervalTree<RuntimeElement> tree)
		{
			this.UpdateDuration();
			return this.OnCreatePlayableGraph(graph, go, tree);
		}

		// Token: 0x060000F3 RID: 243 RVA: 0x00002F44 File Offset: 0x00001144
		internal virtual Playable OnCreatePlayableGraph(PlayableGraph graph, GameObject go, IntervalTree<RuntimeElement> tree)
		{
			if (tree == null)
			{
				throw new ArgumentException("IntervalTree argument cannot be null", "tree");
			}
			if (go == null)
			{
				throw new ArgumentException("GameObject argument cannot be null", "go");
			}
			Playable playable = this.CreateTrackMixer(graph, go, this.clips.Length);
			for (int i = 0; i < this.clips.Length; i++)
			{
				Playable playable2 = this.CreatePlayable(graph, go, this.clips[i]);
				if (playable2.IsValid<Playable>())
				{
					playable2.SetDuration(this.clips[i].duration);
					RuntimeClip item = new RuntimeClip(this.clips[i], playable2, playable);
					tree.Add(item);
					graph.Connect<Playable, Playable>(playable2, 0, playable, i);
					playable.SetInputWeight(i, 0f);
				}
			}
			return playable;
		}

		// Token: 0x060000F4 RID: 244 RVA: 0x0000301C File Offset: 0x0000121C
		internal void SortClips()
		{
			TimelineClip[] clips = this.clips;
			if (!this.m_CacheSorted)
			{
				Array.Sort<TimelineClip>(this.clips, (TimelineClip clip1, TimelineClip clip2) => clip1.start.CompareTo(clip2.start));
				this.m_CacheSorted = true;
			}
		}

		// Token: 0x060000F5 RID: 245 RVA: 0x0000306D File Offset: 0x0000126D
		internal void ClearClipsInternal()
		{
			this.m_Clips = new List<TimelineClip>();
			this.m_ClipsCache = null;
		}

		// Token: 0x060000F6 RID: 246 RVA: 0x00003082 File Offset: 0x00001282
		internal void ClearSubTracksInternal()
		{
			this.m_Children = new List<ScriptableObject>();
			this.Invalidate();
		}

		// Token: 0x060000F7 RID: 247 RVA: 0x00003096 File Offset: 0x00001296
		internal void OnClipMove()
		{
			this.m_CacheSorted = false;
		}

		// Token: 0x060000F8 RID: 248 RVA: 0x000030A0 File Offset: 0x000012A0
		internal TimelineClip CreateNewClipContainerInternal()
		{
			TimelineClip timelineClip = new TimelineClip(this);
			timelineClip.asset = null;
			double num = 0.0;
			for (int i = 0; i < this.m_Clips.Count - 1; i++)
			{
				double num2 = this.m_Clips[i].duration;
				if (double.IsInfinity(num2))
				{
					num2 = (double)TimelineClip.kDefaultClipDurationInSeconds;
				}
				num = Math.Max(num, this.m_Clips[i].start + num2);
			}
			timelineClip.mixInCurve = AnimationCurve.EaseInOut(0f, 0f, 1f, 1f);
			timelineClip.mixOutCurve = AnimationCurve.EaseInOut(0f, 1f, 1f, 0f);
			timelineClip.start = num;
			timelineClip.duration = (double)TimelineClip.kDefaultClipDurationInSeconds;
			timelineClip.displayName = "untitled";
			return timelineClip;
		}

		// Token: 0x060000F9 RID: 249 RVA: 0x0000318A File Offset: 0x0000138A
		internal void AddChild(TrackAsset child)
		{
			if (!(child == null))
			{
				this.m_Children.Add(child);
				child.parent = this;
				this.Invalidate();
			}
		}

		// Token: 0x060000FA RID: 250 RVA: 0x000031B8 File Offset: 0x000013B8
		internal bool AddChildAfter(TrackAsset child, TrackAsset other)
		{
			bool result;
			if (child == null)
			{
				result = false;
			}
			else
			{
				int num = this.m_Children.IndexOf(other);
				if (num >= 0 && num != this.m_Children.Count - 1)
				{
					this.m_Children.Insert(num + 1, child);
				}
				else
				{
					this.m_Children.Add(child);
				}
				this.Invalidate();
				child.parent = this;
				result = true;
			}
			return result;
		}

		// Token: 0x060000FB RID: 251 RVA: 0x00003238 File Offset: 0x00001438
		internal bool RemoveSubTrack(TrackAsset child)
		{
			bool result;
			if (this.m_Children.Remove(child))
			{
				this.Invalidate();
				child.parent = null;
				result = true;
			}
			else
			{
				result = false;
			}
			return result;
		}

		// Token: 0x060000FC RID: 252 RVA: 0x00003274 File Offset: 0x00001474
		internal void RemoveClip(TimelineClip clip)
		{
			this.m_Clips.Remove(clip);
			this.m_ClipsCache = null;
		}

		// Token: 0x060000FD RID: 253 RVA: 0x0000328C File Offset: 0x0000148C
		internal virtual void GetEvaluationTime(out double outStart, out double outDuration)
		{
			if (this.clips.Length == 0)
			{
				outStart = 0.0;
				outDuration = 0.0;
			}
			else
			{
				outStart = double.MaxValue;
				double num = 0.0;
				for (int i = 0; i < this.clips.Length; i++)
				{
					outStart = Math.Min(this.clips[i].start, outStart);
					num = Math.Max(this.clips[i].start + this.clips[i].duration, num);
				}
				outStart = Math.Max(outStart, 0.0);
				outDuration = num - outStart;
			}
		}

		// Token: 0x060000FE RID: 254 RVA: 0x00003343 File Offset: 0x00001543
		internal virtual void GetSequenceTime(out double outStart, out double outDuration)
		{
			this.GetEvaluationTime(out outStart, out outDuration);
		}

		// Token: 0x060000FF RID: 255 RVA: 0x00003350 File Offset: 0x00001550
		public virtual void GatherProperties(PlayableDirector director, IPropertyCollector driver)
		{
			GameObject gameObjectBinding = this.GetGameObjectBinding(director);
			if (gameObjectBinding != null)
			{
				driver.PushActiveGameObject(gameObjectBinding);
			}
			if (this.animClip != null)
			{
				driver.AddFromClip(this.animClip);
			}
			foreach (TimelineClip timelineClip in this.clips)
			{
				if (timelineClip.curves != null && timelineClip.asset != null)
				{
					driver.AddObjectProperties(timelineClip.asset, timelineClip.curves);
				}
				IPropertyPreview propertyPreview = timelineClip.asset as IPropertyPreview;
				if (propertyPreview != null)
				{
					propertyPreview.GatherProperties(director, driver);
				}
			}
			foreach (TrackAsset trackAsset in this.GetChildTracks())
			{
				if (trackAsset != null)
				{
					trackAsset.GatherProperties(director, driver);
				}
			}
			if (gameObjectBinding != null)
			{
				driver.PopActiveGameObject();
			}
		}

		// Token: 0x06000100 RID: 256 RVA: 0x00003480 File Offset: 0x00001680
		internal GameObject GetGameObjectBinding(PlayableDirector director)
		{
			GameObject result;
			if (director == null)
			{
				result = null;
			}
			else
			{
				Object genericBinding = director.GetGenericBinding(this);
				GameObject gameObject = genericBinding as GameObject;
				if (gameObject != null)
				{
					result = gameObject;
				}
				else
				{
					Component component = genericBinding as Component;
					if (component != null)
					{
						result = component.gameObject;
					}
					else
					{
						result = null;
					}
				}
			}
			return result;
		}

		// Token: 0x06000101 RID: 257 RVA: 0x000034EC File Offset: 0x000016EC
		internal bool ValidateClipType(Type clipType)
		{
			foreach (TrackClipTypeAttribute trackClipTypeAttribute in base.GetType().GetCustomAttributes(typeof(TrackClipTypeAttribute), true))
			{
				if (trackClipTypeAttribute.inspectedType.IsAssignableFrom(clipType))
				{
					return true;
				}
			}
			return typeof(PlayableTrack).IsAssignableFrom(base.GetType()) && typeof(IPlayableAsset).IsAssignableFrom(clipType) && typeof(ScriptableObject).IsAssignableFrom(clipType);
		}

		// Token: 0x06000102 RID: 258 RVA: 0x00003591 File Offset: 0x00001791
		protected virtual void OnCreateClip(TimelineClip clip)
		{
		}

		// Token: 0x06000103 RID: 259 RVA: 0x00003594 File Offset: 0x00001794
		protected internal virtual void UpdateDuration()
		{
			int h = (!(this.m_AnimClip != null)) ? 0 : this.m_AnimClip.frameRate.GetHashCode().CombineHash(this.m_AnimClip.length.GetHashCode());
			int num = this.GetClipsHash().CombineHash(h);
			if (num != this.m_ItemsHash)
			{
				this.m_ItemsHash = num;
				double num2;
				double num3;
				this.GetSequenceTime(out num2, out num3);
				this.m_Start = (DiscreteTime)num2;
				this.m_End = (DiscreteTime)(num2 + num3);
				this.CalculateExtrapolationTimes();
			}
		}

		// Token: 0x14000001 RID: 1
		// (add) Token: 0x06000104 RID: 260 RVA: 0x00003644 File Offset: 0x00001844
		// (remove) Token: 0x06000105 RID: 261 RVA: 0x00003678 File Offset: 0x00001878
		internal static event Action<TimelineClip, GameObject, Playable> OnPlayableCreate
		{
			add
			{
				Action<TimelineClip, GameObject, Playable> action = TrackAsset.OnPlayableCreate;
				Action<TimelineClip, GameObject, Playable> action2;
				do
				{
					action2 = action;
					action = Interlocked.CompareExchange<Action<TimelineClip, GameObject, Playable>>(ref TrackAsset.OnPlayableCreate, (Action<TimelineClip, GameObject, Playable>)Delegate.Combine(action2, value), action);
				}
				while (action != action2);
			}
			remove
			{
				Action<TimelineClip, GameObject, Playable> action = TrackAsset.OnPlayableCreate;
				Action<TimelineClip, GameObject, Playable> action2;
				do
				{
					action2 = action;
					action = Interlocked.CompareExchange<Action<TimelineClip, GameObject, Playable>>(ref TrackAsset.OnPlayableCreate, (Action<TimelineClip, GameObject, Playable>)Delegate.Remove(action2, value), action);
				}
				while (action != action2);
			}
		}

		// Token: 0x06000106 RID: 262 RVA: 0x000036AC File Offset: 0x000018AC
		protected virtual Playable CreatePlayable(PlayableGraph graph, GameObject gameObject, TimelineClip clip)
		{
			if (!graph.IsValid())
			{
				throw new ArgumentException("graph must be a valid PlayableGraph");
			}
			if (clip == null)
			{
				throw new ArgumentNullException("clip");
			}
			IPlayableAsset playableAsset = clip.asset as IPlayableAsset;
			Playable result;
			if (playableAsset != null)
			{
				Playable playable = playableAsset.CreatePlayable(graph, gameObject);
				if (playable.IsValid<Playable>())
				{
					playable.SetAnimatedProperties(clip.curves);
					playable.SetSpeed(clip.timeScale);
					if (TrackAsset.OnPlayableCreate != null)
					{
						TrackAsset.OnPlayableCreate(clip, gameObject, playable);
					}
				}
				result = playable;
			}
			else
			{
				result = Playable.Null;
			}
			return result;
		}

		// Token: 0x06000107 RID: 263 RVA: 0x00003750 File Offset: 0x00001950
		internal void Invalidate()
		{
			this.m_ChildTrackCache = null;
			TimelineAsset timelineAsset = this.timelineAsset;
			if (timelineAsset != null)
			{
				timelineAsset.Invalidate();
			}
		}

		// Token: 0x06000108 RID: 264 RVA: 0x00003780 File Offset: 0x00001980
		private void UpdateChildTrackCache()
		{
			if (this.m_ChildTrackCache == null)
			{
				if (this.m_Children == null || this.m_Children.Count == 0)
				{
					this.m_ChildTrackCache = TrackAsset.s_EmptyCache;
				}
				else
				{
					List<TrackAsset> list = new List<TrackAsset>(this.m_Children.Count);
					for (int i = 0; i < this.m_Children.Count; i++)
					{
						TrackAsset trackAsset = this.m_Children[i] as TrackAsset;
						if (trackAsset != null)
						{
							list.Add(trackAsset);
						}
					}
					this.m_ChildTrackCache = list;
				}
			}
		}

		// Token: 0x06000109 RID: 265 RVA: 0x00003824 File Offset: 0x00001A24
		protected internal virtual int Hash()
		{
			return this.clips.Length;
		}

		// Token: 0x0600010A RID: 266 RVA: 0x00003844 File Offset: 0x00001A44
		private int GetClipsHash()
		{
			int num = 0;
			foreach (TimelineClip timelineClip in this.m_Clips)
			{
				num = num.CombineHash(timelineClip.Hash());
			}
			return num;
		}

		// Token: 0x0600010B RID: 267 RVA: 0x000038B4 File Offset: 0x00001AB4
		protected virtual void OnBeforeTrackSerialize()
		{
		}

		// Token: 0x0600010C RID: 268 RVA: 0x000038B7 File Offset: 0x00001AB7
		protected virtual void OnAfterTrackDeserialize()
		{
		}

		// Token: 0x0600010D RID: 269 RVA: 0x000038BA File Offset: 0x00001ABA
		protected internal virtual void OnUpgradeFromVersion(int oldVersion)
		{
		}

		// Token: 0x0600010E RID: 270 RVA: 0x000038C0 File Offset: 0x00001AC0
		void ISerializationCallbackReceiver.OnBeforeSerialize()
		{
			this.m_Version = 2;
			for (int i = this.m_Children.Count - 1; i >= 0; i--)
			{
				TrackAsset trackAsset = this.m_Children[i] as TrackAsset;
				if (trackAsset != null && trackAsset.parent != this)
				{
					trackAsset.parent = this;
				}
			}
			this.OnBeforeTrackSerialize();
		}

		// Token: 0x0600010F RID: 271 RVA: 0x00003931 File Offset: 0x00001B31
		void ISerializationCallbackReceiver.OnAfterDeserialize()
		{
			this.m_ClipsCache = null;
			this.Invalidate();
			if (this.m_Version < 2)
			{
				this.UpgradeToLatestVersion();
				this.OnUpgradeFromVersion(this.m_Version);
			}
			this.OnAfterTrackDeserialize();
		}

		// Token: 0x06000110 RID: 272 RVA: 0x00003967 File Offset: 0x00001B67
		private void UpgradeToLatestVersion()
		{
		}

		// Token: 0x06000111 RID: 273 RVA: 0x0000396A File Offset: 0x00001B6A
		// Note: this type is marked as 'beforefieldinit'.
		static TrackAsset()
		{
		}

		// Token: 0x06000112 RID: 274 RVA: 0x00003984 File Offset: 0x00001B84
		[CompilerGenerated]
		private static int <SortClips>m__0(TimelineClip clip1, TimelineClip clip2)
		{
			return clip1.start.CompareTo(clip2.start);
		}

		// Token: 0x0400005F RID: 95
		[SerializeField]
		[HideInInspector]
		private bool m_Locked;

		// Token: 0x04000060 RID: 96
		[SerializeField]
		[HideInInspector]
		private bool m_Muted;

		// Token: 0x04000061 RID: 97
		[SerializeField]
		[HideInInspector]
		private string m_CustomPlayableFullTypename = string.Empty;

		// Token: 0x04000062 RID: 98
		[SerializeField]
		[FormerlySerializedAs("m_animClip")]
		[HideInInspector]
		private AnimationClip m_AnimClip;

		// Token: 0x04000063 RID: 99
		[SerializeField]
		[HideInInspector]
		private PlayableAsset m_Parent;

		// Token: 0x04000064 RID: 100
		[SerializeField]
		[HideInInspector]
		private List<ScriptableObject> m_Children;

		// Token: 0x04000065 RID: 101
		[NonSerialized]
		private int m_ItemsHash;

		// Token: 0x04000066 RID: 102
		[NonSerialized]
		private TimelineClip[] m_ClipsCache;

		// Token: 0x04000067 RID: 103
		private DiscreteTime m_Start;

		// Token: 0x04000068 RID: 104
		private DiscreteTime m_End;

		// Token: 0x04000069 RID: 105
		private bool m_CacheSorted;

		// Token: 0x0400006A RID: 106
		private static TrackAsset[] s_EmptyCache = new TrackAsset[0];

		// Token: 0x0400006B RID: 107
		private IEnumerable<TrackAsset> m_ChildTrackCache;

		// Token: 0x0400006C RID: 108
		private static Dictionary<Type, TrackBindingTypeAttribute> s_TrackBindingTypeAttributeCache = new Dictionary<Type, TrackBindingTypeAttribute>();

		// Token: 0x0400006D RID: 109
		[SerializeField]
		[HideInInspector]
		protected internal List<TimelineClip> m_Clips = new List<TimelineClip>();

		// Token: 0x0400006E RID: 110
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private static Action<TimelineClip, GameObject, Playable> OnPlayableCreate;

		// Token: 0x0400006F RID: 111
		protected internal const int k_LatestVersion = 2;

		// Token: 0x04000070 RID: 112
		[SerializeField]
		[HideInInspector]
		private int m_Version;

		// Token: 0x04000071 RID: 113
		[CompilerGenerated]
		private static Comparison<TimelineClip> <>f__am$cache0;

		// Token: 0x0200002F RID: 47
		protected internal enum Versions
		{
			// Token: 0x040000C0 RID: 192
			Initial,
			// Token: 0x040000C1 RID: 193
			RotationAsEuler,
			// Token: 0x040000C2 RID: 194
			RootMotionUpgrade
		}

		// Token: 0x02000030 RID: 48
		private static class TrackAssetUpgrade
		{
		}

		// Token: 0x02000053 RID: 83
		[CompilerGenerated]
		private sealed class <>c__Iterator0 : IEnumerable, IEnumerable<PlayableBinding>, IEnumerator, IDisposable, IEnumerator<PlayableBinding>
		{
			// Token: 0x06000258 RID: 600 RVA: 0x000039AC File Offset: 0x00001BAC
			[DebuggerHidden]
			public <>c__Iterator0()
			{
			}

			// Token: 0x06000259 RID: 601 RVA: 0x000039B4 File Offset: 0x00001BB4
			public bool MoveNext()
			{
				uint num = (uint)this.$PC;
				this.$PC = -1;
				switch (num)
				{
				case 0U:
					if (!TrackAsset.s_TrackBindingTypeAttributeCache.TryGetValue(base.GetType(), out attribute))
					{
						attribute = (TrackBindingTypeAttribute)Attribute.GetCustomAttribute(base.GetType(), typeof(TrackBindingTypeAttribute));
						TrackAsset.s_TrackBindingTypeAttributeCache.Add(base.GetType(), attribute);
					}
					trackBindingType = ((attribute == null) ? null : attribute.type);
					this.$current = ScriptPlayableBinding.Create(base.name, this, trackBindingType);
					if (!this.$disposing)
					{
						this.$PC = 1;
					}
					return true;
				case 1U:
					this.$PC = -1;
					break;
				}
				return false;
			}

			// Token: 0x17000099 RID: 153
			// (get) Token: 0x0600025A RID: 602 RVA: 0x00003AA8 File Offset: 0x00001CA8
			PlayableBinding IEnumerator<PlayableBinding>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.$current;
				}
			}

			// Token: 0x1700009A RID: 154
			// (get) Token: 0x0600025B RID: 603 RVA: 0x00003AC4 File Offset: 0x00001CC4
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.$current;
				}
			}

			// Token: 0x0600025C RID: 604 RVA: 0x00003AE3 File Offset: 0x00001CE3
			[DebuggerHidden]
			public void Dispose()
			{
				this.$disposing = true;
				this.$PC = -1;
			}

			// Token: 0x0600025D RID: 605 RVA: 0x00003AF3 File Offset: 0x00001CF3
			[DebuggerHidden]
			public void Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x0600025E RID: 606 RVA: 0x00003AFC File Offset: 0x00001CFC
			[DebuggerHidden]
			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.System.Collections.Generic.IEnumerable<UnityEngine.Playables.PlayableBinding>.GetEnumerator();
			}

			// Token: 0x0600025F RID: 607 RVA: 0x00003B18 File Offset: 0x00001D18
			[DebuggerHidden]
			IEnumerator<PlayableBinding> IEnumerable<PlayableBinding>.GetEnumerator()
			{
				if (Interlocked.CompareExchange(ref this.$PC, 0, -2) == -2)
				{
					return this;
				}
				TrackAsset.<>c__Iterator0 <>c__Iterator = new TrackAsset.<>c__Iterator0();
				<>c__Iterator.$this = this;
				return <>c__Iterator;
			}

			// Token: 0x04000117 RID: 279
			internal TrackBindingTypeAttribute <attribute>__0;

			// Token: 0x04000118 RID: 280
			internal Type <trackBindingType>__0;

			// Token: 0x04000119 RID: 281
			internal TrackAsset $this;

			// Token: 0x0400011A RID: 282
			internal PlayableBinding $current;

			// Token: 0x0400011B RID: 283
			internal bool $disposing;

			// Token: 0x0400011C RID: 284
			internal int $PC;
		}
	}
}
