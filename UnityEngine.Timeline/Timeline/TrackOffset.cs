﻿using System;

namespace UnityEngine.Timeline
{
	// Token: 0x02000024 RID: 36
	public enum TrackOffset
	{
		// Token: 0x04000098 RID: 152
		ApplyTransformOffsets,
		// Token: 0x04000099 RID: 153
		ApplySceneOffsets,
		// Token: 0x0400009A RID: 154
		Auto
	}
}
