﻿using System;

namespace UnityEngine.Timeline
{
	// Token: 0x0200000F RID: 15
	[Flags]
	public enum TrackBindingFlags
	{
		// Token: 0x04000029 RID: 41
		None = 0,
		// Token: 0x0400002A RID: 42
		AllowCreateComponent = 1,
		// Token: 0x0400002B RID: 43
		All = 1
	}
}
