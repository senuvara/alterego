﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine.Playables;
using UnityEngine.Serialization;

namespace UnityEngine.Timeline
{
	// Token: 0x02000016 RID: 22
	[Serializable]
	public class TimelineClip : ISerializationCallbackReceiver
	{
		// Token: 0x0600006D RID: 109 RVA: 0x00004BA0 File Offset: 0x00002DA0
		internal TimelineClip(TrackAsset parent)
		{
			this.parentTrack = parent;
		}

		// Token: 0x17000010 RID: 16
		// (get) Token: 0x0600006E RID: 110 RVA: 0x00004BF8 File Offset: 0x00002DF8
		// (set) Token: 0x0600006F RID: 111 RVA: 0x00004C12 File Offset: 0x00002E12
		internal int dirtyHash
		{
			[CompilerGenerated]
			get
			{
				return this.<dirtyHash>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<dirtyHash>k__BackingField = value;
			}
		}

		// Token: 0x17000011 RID: 17
		// (get) Token: 0x06000070 RID: 112 RVA: 0x00004C1C File Offset: 0x00002E1C
		public bool hasPreExtrapolation
		{
			get
			{
				return this.m_PreExtrapolationMode != TimelineClip.ClipExtrapolation.None && this.m_PreExtrapolationTime > 0.0;
			}
		}

		// Token: 0x17000012 RID: 18
		// (get) Token: 0x06000071 RID: 113 RVA: 0x00004C50 File Offset: 0x00002E50
		public bool hasPostExtrapolation
		{
			get
			{
				return this.m_PostExtrapolationMode != TimelineClip.ClipExtrapolation.None && this.m_PostExtrapolationTime > 0.0;
			}
		}

		// Token: 0x17000013 RID: 19
		// (get) Token: 0x06000072 RID: 114 RVA: 0x00004C84 File Offset: 0x00002E84
		// (set) Token: 0x06000073 RID: 115 RVA: 0x00004CD2 File Offset: 0x00002ED2
		public double timeScale
		{
			get
			{
				return (!this.clipCaps.HasAny(ClipCaps.SpeedMultiplier)) ? 1.0 : Math.Max(TimelineClip.kTimeScaleMin, Math.Min(this.m_TimeScale, TimelineClip.kTimeScaleMax));
			}
			set
			{
				this.m_TimeScale = ((!this.clipCaps.HasAny(ClipCaps.SpeedMultiplier)) ? 1.0 : Math.Max(TimelineClip.kTimeScaleMin, Math.Min(value, TimelineClip.kTimeScaleMax)));
			}
		}

		// Token: 0x17000014 RID: 20
		// (get) Token: 0x06000074 RID: 116 RVA: 0x00004D10 File Offset: 0x00002F10
		// (set) Token: 0x06000075 RID: 117 RVA: 0x00004D2C File Offset: 0x00002F2C
		public double start
		{
			get
			{
				return this.m_Start;
			}
			set
			{
				double num = Math.Max(TimelineClip.SanitizeTimeValue(value, this.m_Start), 0.0);
				if (this.m_ParentTrack != null && this.m_Start != num)
				{
					this.m_ParentTrack.OnClipMove();
				}
				this.m_Start = num;
			}
		}

		// Token: 0x17000015 RID: 21
		// (get) Token: 0x06000076 RID: 118 RVA: 0x00004D84 File Offset: 0x00002F84
		// (set) Token: 0x06000077 RID: 119 RVA: 0x00004D9F File Offset: 0x00002F9F
		public double duration
		{
			get
			{
				return this.m_Duration;
			}
			set
			{
				this.m_Duration = Math.Max(TimelineClip.SanitizeTimeValue(value, this.m_Duration), double.Epsilon);
			}
		}

		// Token: 0x17000016 RID: 22
		// (get) Token: 0x06000078 RID: 120 RVA: 0x00004DC4 File Offset: 0x00002FC4
		public double end
		{
			get
			{
				return this.m_Start + this.m_Duration;
			}
		}

		// Token: 0x17000017 RID: 23
		// (get) Token: 0x06000079 RID: 121 RVA: 0x00004DE8 File Offset: 0x00002FE8
		// (set) Token: 0x0600007A RID: 122 RVA: 0x00004E24 File Offset: 0x00003024
		public double clipIn
		{
			get
			{
				return (!this.clipCaps.HasAny(ClipCaps.ClipIn)) ? 0.0 : this.m_ClipIn;
			}
			set
			{
				this.m_ClipIn = ((!this.clipCaps.HasAny(ClipCaps.ClipIn)) ? 0.0 : Math.Max(Math.Min(TimelineClip.SanitizeTimeValue(value, this.m_ClipIn), TimelineClip.kMaxTimeValue), 0.0));
			}
		}

		// Token: 0x17000018 RID: 24
		// (get) Token: 0x0600007B RID: 123 RVA: 0x00004E7C File Offset: 0x0000307C
		// (set) Token: 0x0600007C RID: 124 RVA: 0x00004E97 File Offset: 0x00003097
		public string displayName
		{
			get
			{
				return this.m_DisplayName;
			}
			set
			{
				this.m_DisplayName = value;
			}
		}

		// Token: 0x17000019 RID: 25
		// (get) Token: 0x0600007D RID: 125 RVA: 0x00004EA4 File Offset: 0x000030A4
		public double clipAssetDuration
		{
			get
			{
				IPlayableAsset playableAsset = this.m_Asset as IPlayableAsset;
				return (playableAsset == null) ? double.MaxValue : playableAsset.duration;
			}
		}

		// Token: 0x1700001A RID: 26
		// (get) Token: 0x0600007E RID: 126 RVA: 0x00004EE0 File Offset: 0x000030E0
		public AnimationClip curves
		{
			get
			{
				return this.m_AnimationCurves;
			}
		}

		// Token: 0x1700001B RID: 27
		// (get) Token: 0x0600007F RID: 127 RVA: 0x00004EFC File Offset: 0x000030FC
		// (set) Token: 0x06000080 RID: 128 RVA: 0x00004F17 File Offset: 0x00003117
		public Object asset
		{
			get
			{
				return this.m_Asset;
			}
			set
			{
				this.m_Asset = value;
			}
		}

		// Token: 0x1700001C RID: 28
		// (get) Token: 0x06000081 RID: 129 RVA: 0x00004F24 File Offset: 0x00003124
		// (set) Token: 0x06000082 RID: 130 RVA: 0x00004F3A File Offset: 0x0000313A
		[Obsolete("underlyingAsset property is obsolete. Use asset property instead", true)]
		public Object underlyingAsset
		{
			get
			{
				return null;
			}
			set
			{
			}
		}

		// Token: 0x1700001D RID: 29
		// (get) Token: 0x06000083 RID: 131 RVA: 0x00004F40 File Offset: 0x00003140
		// (set) Token: 0x06000084 RID: 132 RVA: 0x00004F5C File Offset: 0x0000315C
		public TrackAsset parentTrack
		{
			get
			{
				return this.m_ParentTrack;
			}
			set
			{
				if (!(this.m_ParentTrack == value))
				{
					if (this.m_ParentTrack != null)
					{
						this.m_ParentTrack.RemoveClip(this);
					}
					this.m_ParentTrack = value;
					if (this.m_ParentTrack != null)
					{
						this.m_ParentTrack.AddClip(this);
					}
				}
			}
		}

		// Token: 0x1700001E RID: 30
		// (get) Token: 0x06000085 RID: 133 RVA: 0x00004FC4 File Offset: 0x000031C4
		// (set) Token: 0x06000086 RID: 134 RVA: 0x00005024 File Offset: 0x00003224
		public double easeInDuration
		{
			get
			{
				return (!this.clipCaps.HasAny(ClipCaps.Blending)) ? 0.0 : Math.Min(Math.Max(this.m_EaseInDuration, 0.0), this.duration * 0.49);
			}
			set
			{
				this.m_EaseInDuration = ((!this.clipCaps.HasAny(ClipCaps.Blending)) ? 0.0 : Math.Max(0.0, Math.Min(TimelineClip.SanitizeTimeValue(value, this.m_EaseInDuration), this.duration * 0.49)));
			}
		}

		// Token: 0x1700001F RID: 31
		// (get) Token: 0x06000087 RID: 135 RVA: 0x00005088 File Offset: 0x00003288
		// (set) Token: 0x06000088 RID: 136 RVA: 0x000050E8 File Offset: 0x000032E8
		public double easeOutDuration
		{
			get
			{
				return (!this.clipCaps.HasAny(ClipCaps.Blending)) ? 0.0 : Math.Min(Math.Max(this.m_EaseOutDuration, 0.0), this.duration * 0.49);
			}
			set
			{
				this.m_EaseOutDuration = ((!this.clipCaps.HasAny(ClipCaps.Blending)) ? 0.0 : Math.Max(0.0, Math.Min(TimelineClip.SanitizeTimeValue(value, this.m_EaseOutDuration), this.duration * 0.49)));
			}
		}

		// Token: 0x17000020 RID: 32
		// (get) Token: 0x06000089 RID: 137 RVA: 0x0000514C File Offset: 0x0000334C
		[Obsolete("Use easeOutTime instead (UnityUpgradable) -> easeOutTime", true)]
		public double eastOutTime
		{
			get
			{
				return this.duration - this.easeOutDuration + this.m_Start;
			}
		}

		// Token: 0x17000021 RID: 33
		// (get) Token: 0x0600008A RID: 138 RVA: 0x00005178 File Offset: 0x00003378
		public double easeOutTime
		{
			get
			{
				return this.duration - this.easeOutDuration + this.m_Start;
			}
		}

		// Token: 0x17000022 RID: 34
		// (get) Token: 0x0600008B RID: 139 RVA: 0x000051A4 File Offset: 0x000033A4
		// (set) Token: 0x0600008C RID: 140 RVA: 0x000051DF File Offset: 0x000033DF
		public double blendInDuration
		{
			get
			{
				return (!this.clipCaps.HasAny(ClipCaps.Blending)) ? 0.0 : this.m_BlendInDuration;
			}
			set
			{
				this.m_BlendInDuration = ((!this.clipCaps.HasAny(ClipCaps.Blending)) ? 0.0 : TimelineClip.SanitizeTimeValue(value, this.m_BlendInDuration));
			}
		}

		// Token: 0x17000023 RID: 35
		// (get) Token: 0x0600008D RID: 141 RVA: 0x00005214 File Offset: 0x00003414
		// (set) Token: 0x0600008E RID: 142 RVA: 0x0000524F File Offset: 0x0000344F
		public double blendOutDuration
		{
			get
			{
				return (!this.clipCaps.HasAny(ClipCaps.Blending)) ? 0.0 : this.m_BlendOutDuration;
			}
			set
			{
				this.m_BlendOutDuration = ((!this.clipCaps.HasAny(ClipCaps.Blending)) ? 0.0 : TimelineClip.SanitizeTimeValue(value, this.m_BlendOutDuration));
			}
		}

		// Token: 0x17000024 RID: 36
		// (get) Token: 0x0600008F RID: 143 RVA: 0x00005284 File Offset: 0x00003484
		// (set) Token: 0x06000090 RID: 144 RVA: 0x0000529F File Offset: 0x0000349F
		public TimelineClip.BlendCurveMode blendInCurveMode
		{
			get
			{
				return this.m_BlendInCurveMode;
			}
			set
			{
				this.m_BlendInCurveMode = value;
			}
		}

		// Token: 0x17000025 RID: 37
		// (get) Token: 0x06000091 RID: 145 RVA: 0x000052AC File Offset: 0x000034AC
		// (set) Token: 0x06000092 RID: 146 RVA: 0x000052C7 File Offset: 0x000034C7
		public TimelineClip.BlendCurveMode blendOutCurveMode
		{
			get
			{
				return this.m_BlendOutCurveMode;
			}
			set
			{
				this.m_BlendOutCurveMode = value;
			}
		}

		// Token: 0x17000026 RID: 38
		// (get) Token: 0x06000093 RID: 147 RVA: 0x000052D4 File Offset: 0x000034D4
		public bool hasBlendIn
		{
			get
			{
				return this.clipCaps.HasAny(ClipCaps.Blending) && this.m_BlendInDuration > 0.0;
			}
		}

		// Token: 0x17000027 RID: 39
		// (get) Token: 0x06000094 RID: 148 RVA: 0x00005310 File Offset: 0x00003510
		public bool hasBlendOut
		{
			get
			{
				return this.clipCaps.HasAny(ClipCaps.Blending) && this.m_BlendOutDuration > 0.0;
			}
		}

		// Token: 0x17000028 RID: 40
		// (get) Token: 0x06000095 RID: 149 RVA: 0x0000534C File Offset: 0x0000354C
		// (set) Token: 0x06000096 RID: 150 RVA: 0x0000538E File Offset: 0x0000358E
		public AnimationCurve mixInCurve
		{
			get
			{
				if (this.m_MixInCurve == null || this.m_MixInCurve.length < 2)
				{
					this.m_MixInCurve = TimelineClip.GetDefaultMixInCurve();
				}
				return this.m_MixInCurve;
			}
			set
			{
				this.m_MixInCurve = value;
			}
		}

		// Token: 0x17000029 RID: 41
		// (get) Token: 0x06000097 RID: 151 RVA: 0x00005398 File Offset: 0x00003598
		public float mixInPercentage
		{
			get
			{
				return (float)(this.mixInDuration / this.duration);
			}
		}

		// Token: 0x1700002A RID: 42
		// (get) Token: 0x06000098 RID: 152 RVA: 0x000053BC File Offset: 0x000035BC
		public double mixInDuration
		{
			get
			{
				return (!this.hasBlendIn) ? this.easeInDuration : this.blendInDuration;
			}
		}

		// Token: 0x1700002B RID: 43
		// (get) Token: 0x06000099 RID: 153 RVA: 0x000053F0 File Offset: 0x000035F0
		// (set) Token: 0x0600009A RID: 154 RVA: 0x00005432 File Offset: 0x00003632
		public AnimationCurve mixOutCurve
		{
			get
			{
				if (this.m_MixOutCurve == null || this.m_MixOutCurve.length < 2)
				{
					this.m_MixOutCurve = TimelineClip.GetDefaultMixOutCurve();
				}
				return this.m_MixOutCurve;
			}
			set
			{
				this.m_MixOutCurve = value;
			}
		}

		// Token: 0x1700002C RID: 44
		// (get) Token: 0x0600009B RID: 155 RVA: 0x0000543C File Offset: 0x0000363C
		public double mixOutTime
		{
			get
			{
				return this.duration - this.mixOutDuration + this.m_Start;
			}
		}

		// Token: 0x1700002D RID: 45
		// (get) Token: 0x0600009C RID: 156 RVA: 0x00005468 File Offset: 0x00003668
		public double mixOutDuration
		{
			get
			{
				return (!this.hasBlendOut) ? this.easeOutDuration : this.blendOutDuration;
			}
		}

		// Token: 0x1700002E RID: 46
		// (get) Token: 0x0600009D RID: 157 RVA: 0x0000549C File Offset: 0x0000369C
		public float mixOutPercentage
		{
			get
			{
				return (float)(this.mixOutDuration / this.duration);
			}
		}

		// Token: 0x1700002F RID: 47
		// (get) Token: 0x0600009E RID: 158 RVA: 0x000054C0 File Offset: 0x000036C0
		// (set) Token: 0x0600009F RID: 159 RVA: 0x000054DB File Offset: 0x000036DB
		public bool recordable
		{
			get
			{
				return this.m_Recordable;
			}
			internal set
			{
				this.m_Recordable = value;
			}
		}

		// Token: 0x17000030 RID: 48
		// (get) Token: 0x060000A0 RID: 160 RVA: 0x000054E8 File Offset: 0x000036E8
		[Obsolete("exposedParameter is deprecated and will be removed in a future release")]
		public List<string> exposedParameters
		{
			get
			{
				List<string> result;
				if ((result = this.m_ExposedParameterNames) == null)
				{
					result = (this.m_ExposedParameterNames = new List<string>());
				}
				return result;
			}
		}

		// Token: 0x17000031 RID: 49
		// (get) Token: 0x060000A1 RID: 161 RVA: 0x00005518 File Offset: 0x00003718
		public ClipCaps clipCaps
		{
			get
			{
				ITimelineClipAsset timelineClipAsset = this.asset as ITimelineClipAsset;
				return (timelineClipAsset == null) ? TimelineClip.kDefaultClipCaps : timelineClipAsset.clipCaps;
			}
		}

		// Token: 0x060000A2 RID: 162 RVA: 0x00005550 File Offset: 0x00003750
		internal int Hash()
		{
			int hashCode = this.m_Start.GetHashCode();
			int hashCode2 = this.m_Duration.GetHashCode();
			int hashCode3 = this.m_TimeScale.GetHashCode();
			int hashCode4 = this.m_ClipIn.GetHashCode();
			int preExtrapolationMode = (int)this.m_PreExtrapolationMode;
			int hashCode5 = preExtrapolationMode.GetHashCode();
			int postExtrapolationMode = (int)this.m_PostExtrapolationMode;
			return HashUtility.CombineHash(hashCode, hashCode2, hashCode3, hashCode4, hashCode5, postExtrapolationMode.GetHashCode());
		}

		// Token: 0x060000A3 RID: 163 RVA: 0x000055D8 File Offset: 0x000037D8
		public float EvaluateMixOut(double time)
		{
			float result;
			if (!this.clipCaps.HasAny(ClipCaps.Blending))
			{
				result = 1f;
			}
			else if (this.mixOutDuration > (double)Mathf.Epsilon)
			{
				float num = (float)(time - this.mixOutTime) / (float)this.mixOutDuration;
				num = Mathf.Clamp01(this.mixOutCurve.Evaluate(num));
				result = num;
			}
			else
			{
				result = 1f;
			}
			return result;
		}

		// Token: 0x060000A4 RID: 164 RVA: 0x0000564C File Offset: 0x0000384C
		public float EvaluateMixIn(double time)
		{
			float result;
			if (!this.clipCaps.HasAny(ClipCaps.Blending))
			{
				result = 1f;
			}
			else if (this.mixInDuration > (double)Mathf.Epsilon)
			{
				float num = (float)(time - this.m_Start) / (float)this.mixInDuration;
				num = Mathf.Clamp01(this.mixInCurve.Evaluate(num));
				result = num;
			}
			else
			{
				result = 1f;
			}
			return result;
		}

		// Token: 0x060000A5 RID: 165 RVA: 0x000056C0 File Offset: 0x000038C0
		private static AnimationCurve GetDefaultMixInCurve()
		{
			return AnimationCurve.EaseInOut(0f, 0f, 1f, 1f);
		}

		// Token: 0x060000A6 RID: 166 RVA: 0x000056F0 File Offset: 0x000038F0
		private static AnimationCurve GetDefaultMixOutCurve()
		{
			return AnimationCurve.EaseInOut(0f, 1f, 1f, 0f);
		}

		// Token: 0x060000A7 RID: 167 RVA: 0x00005720 File Offset: 0x00003920
		public double ToLocalTime(double time)
		{
			double result;
			if (time < 0.0)
			{
				result = time;
			}
			else
			{
				if (this.IsPreExtrapolatedTime(time))
				{
					time = TimelineClip.GetExtrapolatedTime(time - this.m_Start, this.m_PreExtrapolationMode, this.m_Duration);
				}
				else if (this.IsPostExtrapolatedTime(time))
				{
					time = TimelineClip.GetExtrapolatedTime(time - this.m_Start, this.m_PostExtrapolationMode, this.m_Duration);
				}
				else
				{
					time -= this.m_Start;
				}
				time *= this.timeScale;
				time += this.clipIn;
				result = time;
			}
			return result;
		}

		// Token: 0x060000A8 RID: 168 RVA: 0x000057C4 File Offset: 0x000039C4
		public double ToLocalTimeUnbound(double time)
		{
			return (time - this.m_Start) * this.timeScale + this.clipIn;
		}

		// Token: 0x060000A9 RID: 169 RVA: 0x000057F0 File Offset: 0x000039F0
		internal double FromLocalTimeUnbound(double time)
		{
			return (time - this.clipIn) / this.timeScale + this.m_Start;
		}

		// Token: 0x17000032 RID: 50
		// (get) Token: 0x060000AA RID: 170 RVA: 0x0000581C File Offset: 0x00003A1C
		public AnimationClip animationClip
		{
			get
			{
				AnimationClip result;
				if (this.m_Asset == null)
				{
					result = null;
				}
				else
				{
					AnimationPlayableAsset animationPlayableAsset = this.m_Asset as AnimationPlayableAsset;
					result = ((!(animationPlayableAsset != null)) ? null : animationPlayableAsset.clip);
				}
				return result;
			}
		}

		// Token: 0x060000AB RID: 171 RVA: 0x00005870 File Offset: 0x00003A70
		private static double SanitizeTimeValue(double value, double defaultValue)
		{
			double result;
			if (double.IsInfinity(value) || double.IsNaN(value))
			{
				Debug.LogError("Invalid time value assigned");
				result = defaultValue;
			}
			else
			{
				result = Math.Max(-TimelineClip.kMaxTimeValue, Math.Min(TimelineClip.kMaxTimeValue, value));
			}
			return result;
		}

		// Token: 0x17000033 RID: 51
		// (get) Token: 0x060000AC RID: 172 RVA: 0x000058C4 File Offset: 0x00003AC4
		// (set) Token: 0x060000AD RID: 173 RVA: 0x000058F6 File Offset: 0x00003AF6
		public TimelineClip.ClipExtrapolation postExtrapolationMode
		{
			get
			{
				return (!this.clipCaps.HasAny(ClipCaps.Extrapolation)) ? TimelineClip.ClipExtrapolation.None : this.m_PostExtrapolationMode;
			}
			internal set
			{
				this.m_PostExtrapolationMode = ((!this.clipCaps.HasAny(ClipCaps.Extrapolation)) ? TimelineClip.ClipExtrapolation.None : value);
			}
		}

		// Token: 0x17000034 RID: 52
		// (get) Token: 0x060000AE RID: 174 RVA: 0x00005918 File Offset: 0x00003B18
		// (set) Token: 0x060000AF RID: 175 RVA: 0x0000594A File Offset: 0x00003B4A
		public TimelineClip.ClipExtrapolation preExtrapolationMode
		{
			get
			{
				return (!this.clipCaps.HasAny(ClipCaps.Extrapolation)) ? TimelineClip.ClipExtrapolation.None : this.m_PreExtrapolationMode;
			}
			internal set
			{
				this.m_PreExtrapolationMode = ((!this.clipCaps.HasAny(ClipCaps.Extrapolation)) ? TimelineClip.ClipExtrapolation.None : value);
			}
		}

		// Token: 0x060000B0 RID: 176 RVA: 0x0000596B File Offset: 0x00003B6B
		internal void SetPostExtrapolationTime(double time)
		{
			this.m_PostExtrapolationTime = time;
		}

		// Token: 0x060000B1 RID: 177 RVA: 0x00005975 File Offset: 0x00003B75
		internal void SetPreExtrapolationTime(double time)
		{
			this.m_PreExtrapolationTime = time;
		}

		// Token: 0x060000B2 RID: 178 RVA: 0x00005980 File Offset: 0x00003B80
		public bool IsExtrapolatedTime(double sequenceTime)
		{
			return this.IsPreExtrapolatedTime(sequenceTime) || this.IsPostExtrapolatedTime(sequenceTime);
		}

		// Token: 0x060000B3 RID: 179 RVA: 0x000059AC File Offset: 0x00003BAC
		public bool IsPreExtrapolatedTime(double sequenceTime)
		{
			return this.preExtrapolationMode != TimelineClip.ClipExtrapolation.None && sequenceTime < this.m_Start && sequenceTime >= this.m_Start - this.m_PreExtrapolationTime;
		}

		// Token: 0x060000B4 RID: 180 RVA: 0x000059F0 File Offset: 0x00003BF0
		public bool IsPostExtrapolatedTime(double sequenceTime)
		{
			return this.postExtrapolationMode != TimelineClip.ClipExtrapolation.None && sequenceTime > this.end && sequenceTime - this.end < this.m_PostExtrapolationTime;
		}

		// Token: 0x17000035 RID: 53
		// (get) Token: 0x060000B5 RID: 181 RVA: 0x00005A30 File Offset: 0x00003C30
		public double extrapolatedStart
		{
			get
			{
				double result;
				if (this.m_PreExtrapolationMode != TimelineClip.ClipExtrapolation.None)
				{
					result = this.m_Start - this.m_PreExtrapolationTime;
				}
				else
				{
					result = this.m_Start;
				}
				return result;
			}
		}

		// Token: 0x17000036 RID: 54
		// (get) Token: 0x060000B6 RID: 182 RVA: 0x00005A6C File Offset: 0x00003C6C
		public double extrapolatedDuration
		{
			get
			{
				double num = this.m_Duration;
				if (this.m_PostExtrapolationMode != TimelineClip.ClipExtrapolation.None)
				{
					num += Math.Min(this.m_PostExtrapolationTime, TimelineClip.kMaxTimeValue);
				}
				if (this.m_PreExtrapolationMode != TimelineClip.ClipExtrapolation.None)
				{
					num += this.m_PreExtrapolationTime;
				}
				return num;
			}
		}

		// Token: 0x060000B7 RID: 183 RVA: 0x00005ABC File Offset: 0x00003CBC
		private static double GetExtrapolatedTime(double time, TimelineClip.ClipExtrapolation mode, double duration)
		{
			double result;
			if (duration == 0.0)
			{
				result = 0.0;
			}
			else
			{
				switch (mode)
				{
				case TimelineClip.ClipExtrapolation.Hold:
					if (time < 0.0)
					{
						return 0.0;
					}
					if (time > duration)
					{
						return duration;
					}
					break;
				case TimelineClip.ClipExtrapolation.Loop:
					if (time < 0.0)
					{
						time = duration - -time % duration;
					}
					else if (time > duration)
					{
						time %= duration;
					}
					break;
				case TimelineClip.ClipExtrapolation.PingPong:
					if (time < 0.0)
					{
						time = duration * 2.0 - -time % (duration * 2.0);
						time = duration - Math.Abs(time - duration);
					}
					else
					{
						time %= duration * 2.0;
						time = duration - Math.Abs(time - duration);
					}
					break;
				}
				result = time;
			}
			return result;
		}

		// Token: 0x060000B8 RID: 184 RVA: 0x00005BD8 File Offset: 0x00003DD8
		internal void AllocateAnimatedParameterCurves()
		{
			if (this.m_AnimationCurves == null)
			{
				this.m_AnimationCurves = new AnimationClip
				{
					legacy = true
				};
			}
		}

		// Token: 0x060000B9 RID: 185 RVA: 0x00005C0D File Offset: 0x00003E0D
		internal void ClearAnimatedParameterCurves()
		{
			this.m_AnimationCurves = null;
		}

		// Token: 0x060000BA RID: 186 RVA: 0x00005C17 File Offset: 0x00003E17
		void ISerializationCallbackReceiver.OnBeforeSerialize()
		{
			this.m_Version = 1;
		}

		// Token: 0x060000BB RID: 187 RVA: 0x00005C21 File Offset: 0x00003E21
		void ISerializationCallbackReceiver.OnAfterDeserialize()
		{
			if (this.m_Version < 1)
			{
				this.UpgradeToLatestVersion();
			}
		}

		// Token: 0x060000BC RID: 188 RVA: 0x00005C38 File Offset: 0x00003E38
		public override string ToString()
		{
			return UnityString.Format("{0} ({1:F2}, {2:F2}):{3:F2} | {4}", new object[]
			{
				this.displayName,
				this.start,
				this.end,
				this.clipIn,
				this.parentTrack
			});
		}

		// Token: 0x060000BD RID: 189 RVA: 0x00005C99 File Offset: 0x00003E99
		private void UpgradeToLatestVersion()
		{
			if (this.m_Version < 1)
			{
				TimelineClip.TimelineClipUpgrade.UpgradeClipInFromGlobalToLocal(this);
			}
		}

		// Token: 0x060000BE RID: 190 RVA: 0x00005CB0 File Offset: 0x00003EB0
		// Note: this type is marked as 'beforefieldinit'.
		static TimelineClip()
		{
		}

		// Token: 0x04000030 RID: 48
		public static readonly ClipCaps kDefaultClipCaps = ClipCaps.Blending;

		// Token: 0x04000031 RID: 49
		public static readonly float kDefaultClipDurationInSeconds = 5f;

		// Token: 0x04000032 RID: 50
		public static readonly double kTimeScaleMin = 0.001;

		// Token: 0x04000033 RID: 51
		public static readonly double kTimeScaleMax = 1000.0;

		// Token: 0x04000034 RID: 52
		internal static readonly double kMinDuration = 0.016666666666666666;

		// Token: 0x04000035 RID: 53
		internal static readonly double kMaxTimeValue = 1000000.0;

		// Token: 0x04000036 RID: 54
		[SerializeField]
		private double m_Start;

		// Token: 0x04000037 RID: 55
		[SerializeField]
		private double m_ClipIn;

		// Token: 0x04000038 RID: 56
		[SerializeField]
		private Object m_Asset;

		// Token: 0x04000039 RID: 57
		[SerializeField]
		[FormerlySerializedAs("m_HackDuration")]
		private double m_Duration;

		// Token: 0x0400003A RID: 58
		[SerializeField]
		private double m_TimeScale = 1.0;

		// Token: 0x0400003B RID: 59
		[SerializeField]
		private TrackAsset m_ParentTrack;

		// Token: 0x0400003C RID: 60
		[SerializeField]
		private double m_EaseInDuration;

		// Token: 0x0400003D RID: 61
		[SerializeField]
		private double m_EaseOutDuration;

		// Token: 0x0400003E RID: 62
		[SerializeField]
		private double m_BlendInDuration = -1.0;

		// Token: 0x0400003F RID: 63
		[SerializeField]
		private double m_BlendOutDuration = -1.0;

		// Token: 0x04000040 RID: 64
		[SerializeField]
		private AnimationCurve m_MixInCurve;

		// Token: 0x04000041 RID: 65
		[SerializeField]
		private AnimationCurve m_MixOutCurve;

		// Token: 0x04000042 RID: 66
		[SerializeField]
		private TimelineClip.BlendCurveMode m_BlendInCurveMode = TimelineClip.BlendCurveMode.Auto;

		// Token: 0x04000043 RID: 67
		[SerializeField]
		private TimelineClip.BlendCurveMode m_BlendOutCurveMode = TimelineClip.BlendCurveMode.Auto;

		// Token: 0x04000044 RID: 68
		[SerializeField]
		private List<string> m_ExposedParameterNames;

		// Token: 0x04000045 RID: 69
		[SerializeField]
		private AnimationClip m_AnimationCurves;

		// Token: 0x04000046 RID: 70
		[SerializeField]
		private bool m_Recordable;

		// Token: 0x04000047 RID: 71
		[SerializeField]
		private TimelineClip.ClipExtrapolation m_PostExtrapolationMode;

		// Token: 0x04000048 RID: 72
		[SerializeField]
		private TimelineClip.ClipExtrapolation m_PreExtrapolationMode;

		// Token: 0x04000049 RID: 73
		[SerializeField]
		private double m_PostExtrapolationTime;

		// Token: 0x0400004A RID: 74
		[SerializeField]
		private double m_PreExtrapolationTime;

		// Token: 0x0400004B RID: 75
		[SerializeField]
		private string m_DisplayName;

		// Token: 0x0400004C RID: 76
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private int <dirtyHash>k__BackingField;

		// Token: 0x0400004D RID: 77
		private const int k_LatestVersion = 1;

		// Token: 0x0400004E RID: 78
		[SerializeField]
		[HideInInspector]
		private int m_Version;

		// Token: 0x02000017 RID: 23
		public enum ClipExtrapolation
		{
			// Token: 0x04000050 RID: 80
			None,
			// Token: 0x04000051 RID: 81
			Hold,
			// Token: 0x04000052 RID: 82
			Loop,
			// Token: 0x04000053 RID: 83
			PingPong,
			// Token: 0x04000054 RID: 84
			Continue
		}

		// Token: 0x02000018 RID: 24
		public enum BlendCurveMode
		{
			// Token: 0x04000056 RID: 86
			Auto,
			// Token: 0x04000057 RID: 87
			Manual
		}

		// Token: 0x0200002B RID: 43
		private enum Versions
		{
			// Token: 0x040000BB RID: 187
			Initial,
			// Token: 0x040000BC RID: 188
			ClipInFromGlobalToLocal
		}

		// Token: 0x0200002C RID: 44
		private static class TimelineClipUpgrade
		{
			// Token: 0x0600018B RID: 395 RVA: 0x00005D06 File Offset: 0x00003F06
			public static void UpgradeClipInFromGlobalToLocal(TimelineClip clip)
			{
				if (clip.m_ClipIn > 0.0 && clip.m_TimeScale > 1.401298464324817E-45)
				{
					clip.m_ClipIn *= clip.m_TimeScale;
				}
			}
		}
	}
}
