﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading;
using UnityEngine.Playables;

namespace UnityEngine.Timeline
{
	// Token: 0x02000034 RID: 52
	[NotKeyable]
	[Serializable]
	public class ControlPlayableAsset : PlayableAsset, IPropertyPreview, ITimelineClipAsset, IClipInitializer, IDirectorDriver
	{
		// Token: 0x0600019D RID: 413 RVA: 0x000085AC File Offset: 0x000067AC
		public ControlPlayableAsset()
		{
		}

		// Token: 0x0600019E RID: 414 RVA: 0x000085E9 File Offset: 0x000067E9
		public void OnEnable()
		{
			if (this.particleRandomSeed == 0U)
			{
				this.particleRandomSeed = (uint)Random.Range(1, 10000);
			}
		}

		// Token: 0x17000076 RID: 118
		// (get) Token: 0x0600019F RID: 415 RVA: 0x00008608 File Offset: 0x00006808
		public override double duration
		{
			get
			{
				return this.m_Duration;
			}
		}

		// Token: 0x17000077 RID: 119
		// (get) Token: 0x060001A0 RID: 416 RVA: 0x00008624 File Offset: 0x00006824
		public ClipCaps clipCaps
		{
			get
			{
				return ClipCaps.ClipIn | ClipCaps.SpeedMultiplier | ((!this.m_SupportLoop) ? ClipCaps.None : ClipCaps.Looping);
			}
		}

		// Token: 0x060001A1 RID: 417 RVA: 0x00008650 File Offset: 0x00006850
		public override Playable CreatePlayable(PlayableGraph graph, GameObject go)
		{
			if (this.prefabGameObject != null)
			{
				if (ControlPlayableAsset.s_CreatedPrefabs.Contains(this.prefabGameObject))
				{
					Debug.LogWarningFormat("Control Track Clip ({0}) is causing a prefab to instantiate itself recursively. Aborting further instances.", new object[]
					{
						base.name
					});
					return Playable.Create(graph, 0);
				}
				ControlPlayableAsset.s_CreatedPrefabs.Add(this.prefabGameObject);
			}
			Playable playable = Playable.Null;
			List<Playable> list = new List<Playable>();
			GameObject gameObject = this.sourceGameObject.Resolve(graph.GetResolver());
			if (this.prefabGameObject != null)
			{
				Transform parentTransform = (!(gameObject != null)) ? null : gameObject.transform;
				ScriptPlayable<PrefabControlPlayable> playable2 = PrefabControlPlayable.Create(graph, this.prefabGameObject, parentTransform);
				gameObject = playable2.GetBehaviour().prefabInstance;
				list.Add(playable2);
			}
			this.m_Duration = PlayableBinding.DefaultDuration;
			this.m_SupportLoop = false;
			if (gameObject != null)
			{
				IList<PlayableDirector> directors = (!this.updateDirector) ? ControlPlayableAsset.k_EmptyDirectorsList : this.GetComponent<PlayableDirector>(gameObject);
				IList<ParticleSystem> particleSystems = (!this.updateParticle) ? ControlPlayableAsset.k_EmptyParticlesList : this.GetParticleSystemRoots(gameObject);
				this.UpdateDurationAndLoopFlag(directors, particleSystems);
				PlayableDirector component = go.GetComponent<PlayableDirector>();
				if (component != null)
				{
					this.m_ControlDirectorAsset = component.playableAsset;
				}
				if (go == gameObject && this.prefabGameObject == null)
				{
					Debug.LogWarningFormat("Control Playable ({0}) is referencing the same PlayableDirector component than the one in which it is playing.", new object[]
					{
						base.name
					});
					this.active = false;
					if (!this.searchHierarchy)
					{
						this.updateDirector = false;
					}
				}
				if (this.active)
				{
					this.CreateActivationPlayable(gameObject, graph, list);
				}
				if (this.updateDirector)
				{
					this.SearchHierarchyAndConnectDirector(directors, graph, list, this.prefabGameObject != null);
				}
				if (this.updateParticle)
				{
					this.SearchHiearchyAndConnectParticleSystem(particleSystems, graph, list);
				}
				if (this.updateITimeControl)
				{
					ControlPlayableAsset.SearchHierarchyAndConnectControlableScripts(ControlPlayableAsset.GetControlableScripts(gameObject), graph, list);
				}
				playable = ControlPlayableAsset.ConnectPlayablesToMixer(graph, list);
			}
			if (this.prefabGameObject != null)
			{
				ControlPlayableAsset.s_CreatedPrefabs.Remove(this.prefabGameObject);
			}
			if (!playable.IsValid<Playable>())
			{
				playable = Playable.Create(graph, 0);
			}
			return playable;
		}

		// Token: 0x060001A2 RID: 418 RVA: 0x000088B4 File Offset: 0x00006AB4
		private static Playable ConnectPlayablesToMixer(PlayableGraph graph, List<Playable> playables)
		{
			Playable playable = Playable.Create(graph, playables.Count);
			for (int num = 0; num != playables.Count; num++)
			{
				ControlPlayableAsset.ConnectMixerAndPlayable(graph, playable, playables[num], num);
			}
			playable.SetPropagateSetTime(true);
			return playable;
		}

		// Token: 0x060001A3 RID: 419 RVA: 0x00008908 File Offset: 0x00006B08
		private void CreateActivationPlayable(GameObject root, PlayableGraph graph, List<Playable> outplayables)
		{
			ScriptPlayable<ActivationControlPlayable> playable = ActivationControlPlayable.Create(graph, root, this.postPlayback);
			if (playable.IsValid<ScriptPlayable<ActivationControlPlayable>>())
			{
				outplayables.Add(playable);
			}
		}

		// Token: 0x060001A4 RID: 420 RVA: 0x0000893C File Offset: 0x00006B3C
		private void SearchHiearchyAndConnectParticleSystem(IEnumerable<ParticleSystem> particleSystems, PlayableGraph graph, List<Playable> outplayables)
		{
			foreach (ParticleSystem particleSystem in particleSystems)
			{
				if (particleSystem != null)
				{
					outplayables.Add(ParticleControlPlayable.Create(graph, particleSystem, this.particleRandomSeed));
				}
			}
		}

		// Token: 0x060001A5 RID: 421 RVA: 0x000089B4 File Offset: 0x00006BB4
		private void SearchHierarchyAndConnectDirector(IEnumerable<PlayableDirector> directors, PlayableGraph graph, List<Playable> outplayables, bool disableSelfReferences)
		{
			foreach (PlayableDirector playableDirector in directors)
			{
				if (playableDirector != null)
				{
					if (playableDirector.playableAsset != this.m_ControlDirectorAsset)
					{
						outplayables.Add(DirectorControlPlayable.Create(graph, playableDirector));
					}
					else if (disableSelfReferences)
					{
						playableDirector.enabled = false;
					}
				}
			}
		}

		// Token: 0x060001A6 RID: 422 RVA: 0x00008A54 File Offset: 0x00006C54
		private static void SearchHierarchyAndConnectControlableScripts(IEnumerable<MonoBehaviour> controlableScripts, PlayableGraph graph, List<Playable> outplayables)
		{
			foreach (MonoBehaviour monoBehaviour in controlableScripts)
			{
				outplayables.Add(TimeControlPlayable.Create(graph, (ITimeControl)monoBehaviour));
			}
		}

		// Token: 0x060001A7 RID: 423 RVA: 0x00008ABC File Offset: 0x00006CBC
		private static void ConnectMixerAndPlayable(PlayableGraph graph, Playable mixer, Playable playable, int portIndex)
		{
			graph.Connect<Playable, Playable>(playable, 0, mixer, portIndex);
			mixer.SetInputWeight(playable, 1f);
		}

		// Token: 0x060001A8 RID: 424 RVA: 0x00008AD8 File Offset: 0x00006CD8
		internal IList<T> GetComponent<T>(GameObject gameObject)
		{
			List<T> list = new List<T>();
			if (gameObject != null)
			{
				if (this.searchHierarchy)
				{
					gameObject.GetComponentsInChildren<T>(true, list);
				}
				else
				{
					gameObject.GetComponents<T>(list);
				}
			}
			return list;
		}

		// Token: 0x060001A9 RID: 425 RVA: 0x00008B28 File Offset: 0x00006D28
		private static IEnumerable<MonoBehaviour> GetControlableScripts(GameObject root)
		{
			if (root == null)
			{
				yield break;
			}
			foreach (MonoBehaviour script in root.GetComponentsInChildren<MonoBehaviour>())
			{
				if (script is ITimeControl)
				{
					yield return script;
				}
			}
			yield break;
		}

		// Token: 0x060001AA RID: 426 RVA: 0x00008B54 File Offset: 0x00006D54
		private void UpdateDurationAndLoopFlag(IList<PlayableDirector> directors, IList<ParticleSystem> particleSystems)
		{
			if (directors.Count != 0 || particleSystems.Count != 0)
			{
				double num = double.NegativeInfinity;
				bool flag = false;
				foreach (PlayableDirector playableDirector in directors)
				{
					if (playableDirector.playableAsset != null)
					{
						double num2 = playableDirector.playableAsset.duration;
						if (playableDirector.playableAsset is TimelineAsset && num2 > 0.0)
						{
							num2 = (double)((DiscreteTime)num2).OneTickAfter();
						}
						num = Math.Max(num, num2);
						flag = (flag || playableDirector.extrapolationMode == DirectorWrapMode.Loop);
					}
				}
				foreach (ParticleSystem particleSystem in particleSystems)
				{
					num = Math.Max(num, (double)particleSystem.main.duration);
					flag = (flag || particleSystem.main.loop);
				}
				this.m_Duration = ((!double.IsNegativeInfinity(num)) ? num : PlayableBinding.DefaultDuration);
				this.m_SupportLoop = flag;
			}
		}

		// Token: 0x060001AB RID: 427 RVA: 0x00008CDC File Offset: 0x00006EDC
		private IList<ParticleSystem> GetParticleSystemRoots(GameObject go)
		{
			IList<ParticleSystem> result;
			if (this.searchHierarchy)
			{
				List<ParticleSystem> list = new List<ParticleSystem>();
				ControlPlayableAsset.GetParticleSystemRoots(go.transform, list);
				result = list;
			}
			else
			{
				result = this.GetComponent<ParticleSystem>(go);
			}
			return result;
		}

		// Token: 0x060001AC RID: 428 RVA: 0x00008D20 File Offset: 0x00006F20
		private static void GetParticleSystemRoots(Transform t, ICollection<ParticleSystem> roots)
		{
			ParticleSystem component = t.GetComponent<ParticleSystem>();
			if (component != null)
			{
				roots.Add(component);
			}
			else
			{
				for (int i = 0; i < t.childCount; i++)
				{
					ControlPlayableAsset.GetParticleSystemRoots(t.GetChild(i), roots);
				}
			}
		}

		// Token: 0x060001AD RID: 429 RVA: 0x00008D74 File Offset: 0x00006F74
		public void GatherProperties(PlayableDirector director, IPropertyCollector driver)
		{
			if (!(director == null))
			{
				if (!ControlPlayableAsset.s_ProcessedDirectors.Contains(director))
				{
					ControlPlayableAsset.s_ProcessedDirectors.Add(director);
					GameObject gameObject = this.sourceGameObject.Resolve(director);
					if (gameObject != null)
					{
						if (this.updateParticle)
						{
							foreach (ParticleSystem particleSystem in gameObject.GetComponentsInChildren<ParticleSystem>(true))
							{
								driver.AddFromName<ParticleSystem>(particleSystem.gameObject, "randomSeed");
								driver.AddFromName<ParticleSystem>(particleSystem.gameObject, "autoRandomSeed");
							}
						}
						if (this.active)
						{
							driver.AddFromName(gameObject, "m_IsActive");
						}
						if (this.updateITimeControl)
						{
							foreach (MonoBehaviour monoBehaviour in ControlPlayableAsset.GetControlableScripts(gameObject))
							{
								IPropertyPreview propertyPreview = monoBehaviour as IPropertyPreview;
								if (propertyPreview != null)
								{
									propertyPreview.GatherProperties(director, driver);
								}
								else
								{
									driver.AddFromComponent(monoBehaviour.gameObject, monoBehaviour);
								}
							}
						}
						if (this.updateDirector)
						{
							foreach (PlayableDirector playableDirector in this.GetComponent<PlayableDirector>(gameObject))
							{
								if (!(playableDirector == null))
								{
									TimelineAsset timelineAsset = playableDirector.playableAsset as TimelineAsset;
									if (!(timelineAsset == null))
									{
										timelineAsset.GatherProperties(playableDirector, driver);
									}
								}
							}
						}
					}
					ControlPlayableAsset.s_ProcessedDirectors.Remove(director);
				}
			}
		}

		// Token: 0x060001AE RID: 430 RVA: 0x00008F64 File Offset: 0x00007164
		void IClipInitializer.OnCreate(TimelineClip newClip, TrackAsset track, IExposedPropertyTable table)
		{
			GameObject gameObject = null;
			if (table != null)
			{
				gameObject = this.sourceGameObject.Resolve(table);
			}
			if (gameObject == null && this.prefabGameObject != null)
			{
				gameObject = this.prefabGameObject;
			}
			if (gameObject)
			{
				IList<PlayableDirector> component = this.GetComponent<PlayableDirector>(gameObject);
				IList<ParticleSystem> component2 = this.GetComponent<ParticleSystem>(gameObject);
				this.UpdateDurationAndLoopFlag(component, component2);
				newClip.displayName = gameObject.name;
			}
		}

		// Token: 0x060001AF RID: 431 RVA: 0x00008FDC File Offset: 0x000071DC
		IList<PlayableDirector> IDirectorDriver.GetDrivenDirectors(IExposedPropertyTable resolver)
		{
			IList<PlayableDirector> result;
			if (!this.updateDirector || this.prefabGameObject != null || resolver == null)
			{
				result = new List<PlayableDirector>(0);
			}
			else
			{
				GameObject gameObject = this.sourceGameObject.Resolve(resolver);
				if (gameObject == null)
				{
					result = new List<PlayableDirector>(0);
				}
				else
				{
					List<PlayableDirector> list = new List<PlayableDirector>();
					foreach (PlayableDirector playableDirector in this.GetComponent<PlayableDirector>(gameObject))
					{
						if (playableDirector.playableAsset is TimelineAsset)
						{
							list.Add(playableDirector);
						}
					}
					result = list;
				}
			}
			return result;
		}

		// Token: 0x060001B0 RID: 432 RVA: 0x000090B0 File Offset: 0x000072B0
		// Note: this type is marked as 'beforefieldinit'.
		static ControlPlayableAsset()
		{
		}

		// Token: 0x040000C7 RID: 199
		private const int k_MaxRandInt = 10000;

		// Token: 0x040000C8 RID: 200
		private static readonly List<PlayableDirector> k_EmptyDirectorsList = new List<PlayableDirector>(0);

		// Token: 0x040000C9 RID: 201
		private static readonly List<ParticleSystem> k_EmptyParticlesList = new List<ParticleSystem>(0);

		// Token: 0x040000CA RID: 202
		[SerializeField]
		public ExposedReference<GameObject> sourceGameObject;

		// Token: 0x040000CB RID: 203
		[SerializeField]
		public GameObject prefabGameObject;

		// Token: 0x040000CC RID: 204
		[SerializeField]
		public bool updateParticle = true;

		// Token: 0x040000CD RID: 205
		[SerializeField]
		public uint particleRandomSeed;

		// Token: 0x040000CE RID: 206
		[SerializeField]
		public bool updateDirector = true;

		// Token: 0x040000CF RID: 207
		[SerializeField]
		public bool updateITimeControl = true;

		// Token: 0x040000D0 RID: 208
		[SerializeField]
		public bool searchHierarchy = true;

		// Token: 0x040000D1 RID: 209
		[SerializeField]
		public bool active = true;

		// Token: 0x040000D2 RID: 210
		[SerializeField]
		public ActivationControlPlayable.PostPlaybackState postPlayback = ActivationControlPlayable.PostPlaybackState.Revert;

		// Token: 0x040000D3 RID: 211
		private PlayableAsset m_ControlDirectorAsset;

		// Token: 0x040000D4 RID: 212
		private double m_Duration = PlayableBinding.DefaultDuration;

		// Token: 0x040000D5 RID: 213
		private bool m_SupportLoop;

		// Token: 0x040000D6 RID: 214
		private static HashSet<PlayableDirector> s_ProcessedDirectors = new HashSet<PlayableDirector>();

		// Token: 0x040000D7 RID: 215
		private static HashSet<GameObject> s_CreatedPrefabs = new HashSet<GameObject>();

		// Token: 0x02000058 RID: 88
		[CompilerGenerated]
		private sealed class <GetControlableScripts>c__Iterator0 : IEnumerable, IEnumerable<MonoBehaviour>, IEnumerator, IDisposable, IEnumerator<MonoBehaviour>
		{
			// Token: 0x06000280 RID: 640 RVA: 0x000090DC File Offset: 0x000072DC
			[DebuggerHidden]
			public <GetControlableScripts>c__Iterator0()
			{
			}

			// Token: 0x06000281 RID: 641 RVA: 0x000090E4 File Offset: 0x000072E4
			public bool MoveNext()
			{
				uint num = (uint)this.$PC;
				this.$PC = -1;
				switch (num)
				{
				case 0U:
					if (root == null)
					{
						return false;
					}
					componentsInChildren = root.GetComponentsInChildren<MonoBehaviour>();
					i = 0;
					break;
				case 1U:
					IL_9A:
					i++;
					break;
				default:
					return false;
				}
				if (i >= componentsInChildren.Length)
				{
					this.$PC = -1;
				}
				else
				{
					script = componentsInChildren[i];
					if (script is ITimeControl)
					{
						this.$current = script;
						if (!this.$disposing)
						{
							this.$PC = 1;
						}
						return true;
					}
					goto IL_9A;
				}
				return false;
			}

			// Token: 0x170000A3 RID: 163
			// (get) Token: 0x06000282 RID: 642 RVA: 0x000091B8 File Offset: 0x000073B8
			MonoBehaviour IEnumerator<MonoBehaviour>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.$current;
				}
			}

			// Token: 0x170000A4 RID: 164
			// (get) Token: 0x06000283 RID: 643 RVA: 0x000091D4 File Offset: 0x000073D4
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.$current;
				}
			}

			// Token: 0x06000284 RID: 644 RVA: 0x000091EE File Offset: 0x000073EE
			[DebuggerHidden]
			public void Dispose()
			{
				this.$disposing = true;
				this.$PC = -1;
			}

			// Token: 0x06000285 RID: 645 RVA: 0x000091FE File Offset: 0x000073FE
			[DebuggerHidden]
			public void Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x06000286 RID: 646 RVA: 0x00009208 File Offset: 0x00007408
			[DebuggerHidden]
			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.System.Collections.Generic.IEnumerable<UnityEngine.MonoBehaviour>.GetEnumerator();
			}

			// Token: 0x06000287 RID: 647 RVA: 0x00009224 File Offset: 0x00007424
			[DebuggerHidden]
			IEnumerator<MonoBehaviour> IEnumerable<MonoBehaviour>.GetEnumerator()
			{
				if (Interlocked.CompareExchange(ref this.$PC, 0, -2) == -2)
				{
					return this;
				}
				ControlPlayableAsset.<GetControlableScripts>c__Iterator0 <GetControlableScripts>c__Iterator = new ControlPlayableAsset.<GetControlableScripts>c__Iterator0();
				<GetControlableScripts>c__Iterator.root = root;
				return <GetControlableScripts>c__Iterator;
			}

			// Token: 0x0400012D RID: 301
			internal GameObject root;

			// Token: 0x0400012E RID: 302
			internal MonoBehaviour[] $locvar0;

			// Token: 0x0400012F RID: 303
			internal int $locvar1;

			// Token: 0x04000130 RID: 304
			internal MonoBehaviour <script>__1;

			// Token: 0x04000131 RID: 305
			internal MonoBehaviour $current;

			// Token: 0x04000132 RID: 306
			internal bool $disposing;

			// Token: 0x04000133 RID: 307
			internal int $PC;
		}
	}
}
