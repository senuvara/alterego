﻿using System;

namespace UnityEngine.Timeline
{
	// Token: 0x02000035 RID: 53
	[TrackClipType(typeof(ControlPlayableAsset), false)]
	public class ControlTrack : TrackAsset
	{
		// Token: 0x060001B1 RID: 433 RVA: 0x00009258 File Offset: 0x00007458
		public ControlTrack()
		{
		}
	}
}
