﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading;
using UnityEngine.Audio;
using UnityEngine.Playables;

namespace UnityEngine.Timeline
{
	// Token: 0x02000033 RID: 51
	[TrackClipType(typeof(AudioPlayableAsset), false)]
	[TrackBindingType(typeof(AudioSource))]
	[Serializable]
	public class AudioTrack : TrackAsset
	{
		// Token: 0x06000199 RID: 409 RVA: 0x000082F4 File Offset: 0x000064F4
		public AudioTrack()
		{
		}

		// Token: 0x0600019A RID: 410 RVA: 0x000082FC File Offset: 0x000064FC
		public TimelineClip CreateClip(AudioClip clip)
		{
			TimelineClip result;
			if (clip == null)
			{
				result = null;
			}
			else
			{
				TimelineClip timelineClip = base.CreateDefaultClip();
				AudioPlayableAsset audioPlayableAsset = timelineClip.asset as AudioPlayableAsset;
				if (audioPlayableAsset != null)
				{
					audioPlayableAsset.clip = clip;
				}
				timelineClip.duration = (double)clip.length;
				timelineClip.displayName = clip.name;
				result = timelineClip;
			}
			return result;
		}

		// Token: 0x0600019B RID: 411 RVA: 0x00008364 File Offset: 0x00006564
		internal override Playable OnCreatePlayableGraph(PlayableGraph graph, GameObject go, IntervalTree<RuntimeElement> tree)
		{
			AudioMixerPlayable audioMixerPlayable = AudioMixerPlayable.Create(graph, base.clips.Length, false);
			for (int i = 0; i < base.clips.Length; i++)
			{
				TimelineClip timelineClip = base.clips[i];
				PlayableAsset playableAsset = timelineClip.asset as PlayableAsset;
				if (!(playableAsset == null))
				{
					float num = 0.1f;
					AudioPlayableAsset audioPlayableAsset = timelineClip.asset as AudioPlayableAsset;
					if (audioPlayableAsset != null)
					{
						num = audioPlayableAsset.bufferingTime;
					}
					Playable playable = playableAsset.CreatePlayable(graph, go);
					if (playable.IsValid<Playable>())
					{
						tree.Add(new ScheduleRuntimeClip(timelineClip, playable, audioMixerPlayable, (double)num, 0.1));
						graph.Connect<Playable, AudioMixerPlayable>(playable, 0, audioMixerPlayable, i);
						playable.SetSpeed(timelineClip.timeScale);
						playable.SetDuration(timelineClip.extrapolatedDuration);
						audioMixerPlayable.SetInputWeight(playable, 1f);
					}
				}
			}
			return audioMixerPlayable;
		}

		// Token: 0x17000075 RID: 117
		// (get) Token: 0x0600019C RID: 412 RVA: 0x00008468 File Offset: 0x00006668
		public override IEnumerable<PlayableBinding> outputs
		{
			get
			{
				yield return AudioPlayableBinding.Create(base.name, this);
				yield break;
			}
		}

		// Token: 0x02000057 RID: 87
		[CompilerGenerated]
		private sealed class <>c__Iterator0 : IEnumerable, IEnumerable<PlayableBinding>, IEnumerator, IDisposable, IEnumerator<PlayableBinding>
		{
			// Token: 0x06000278 RID: 632 RVA: 0x00008492 File Offset: 0x00006692
			[DebuggerHidden]
			public <>c__Iterator0()
			{
			}

			// Token: 0x06000279 RID: 633 RVA: 0x0000849C File Offset: 0x0000669C
			public bool MoveNext()
			{
				uint num = (uint)this.$PC;
				this.$PC = -1;
				switch (num)
				{
				case 0U:
					this.$current = AudioPlayableBinding.Create(base.name, this);
					if (!this.$disposing)
					{
						this.$PC = 1;
					}
					return true;
				case 1U:
					this.$PC = -1;
					break;
				}
				return false;
			}

			// Token: 0x170000A1 RID: 161
			// (get) Token: 0x0600027A RID: 634 RVA: 0x00008508 File Offset: 0x00006708
			PlayableBinding IEnumerator<PlayableBinding>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.$current;
				}
			}

			// Token: 0x170000A2 RID: 162
			// (get) Token: 0x0600027B RID: 635 RVA: 0x00008524 File Offset: 0x00006724
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.$current;
				}
			}

			// Token: 0x0600027C RID: 636 RVA: 0x00008543 File Offset: 0x00006743
			[DebuggerHidden]
			public void Dispose()
			{
				this.$disposing = true;
				this.$PC = -1;
			}

			// Token: 0x0600027D RID: 637 RVA: 0x00008553 File Offset: 0x00006753
			[DebuggerHidden]
			public void Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x0600027E RID: 638 RVA: 0x0000855C File Offset: 0x0000675C
			[DebuggerHidden]
			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.System.Collections.Generic.IEnumerable<UnityEngine.Playables.PlayableBinding>.GetEnumerator();
			}

			// Token: 0x0600027F RID: 639 RVA: 0x00008578 File Offset: 0x00006778
			[DebuggerHidden]
			IEnumerator<PlayableBinding> IEnumerable<PlayableBinding>.GetEnumerator()
			{
				if (Interlocked.CompareExchange(ref this.$PC, 0, -2) == -2)
				{
					return this;
				}
				AudioTrack.<>c__Iterator0 <>c__Iterator = new AudioTrack.<>c__Iterator0();
				<>c__Iterator.$this = this;
				return <>c__Iterator;
			}

			// Token: 0x04000129 RID: 297
			internal AudioTrack $this;

			// Token: 0x0400012A RID: 298
			internal PlayableBinding $current;

			// Token: 0x0400012B RID: 299
			internal bool $disposing;

			// Token: 0x0400012C RID: 300
			internal int $PC;
		}
	}
}
