﻿using System;

namespace UnityEngine.Timeline
{
	// Token: 0x02000004 RID: 4
	internal struct DiscreteTime : IComparable
	{
		// Token: 0x06000008 RID: 8 RVA: 0x0000216A File Offset: 0x0000036A
		public DiscreteTime(DiscreteTime time)
		{
			this.m_DiscreteTime = time.m_DiscreteTime;
		}

		// Token: 0x06000009 RID: 9 RVA: 0x0000217A File Offset: 0x0000037A
		private DiscreteTime(long time)
		{
			this.m_DiscreteTime = time;
		}

		// Token: 0x0600000A RID: 10 RVA: 0x00002184 File Offset: 0x00000384
		public DiscreteTime(double time)
		{
			this.m_DiscreteTime = DiscreteTime.DoubleToDiscreteTime(time);
		}

		// Token: 0x0600000B RID: 11 RVA: 0x00002193 File Offset: 0x00000393
		public DiscreteTime(float time)
		{
			this.m_DiscreteTime = DiscreteTime.FloatToDiscreteTime(time);
		}

		// Token: 0x0600000C RID: 12 RVA: 0x000021A2 File Offset: 0x000003A2
		public DiscreteTime(int time)
		{
			this.m_DiscreteTime = DiscreteTime.IntToDiscreteTime(time);
		}

		// Token: 0x0600000D RID: 13 RVA: 0x000021B1 File Offset: 0x000003B1
		public DiscreteTime(int frame, double fps)
		{
			this.m_DiscreteTime = DiscreteTime.DoubleToDiscreteTime((double)frame * fps);
		}

		// Token: 0x17000001 RID: 1
		// (get) Token: 0x0600000E RID: 14 RVA: 0x000021C4 File Offset: 0x000003C4
		public static double tickValue
		{
			get
			{
				return 1E-12;
			}
		}

		// Token: 0x0600000F RID: 15 RVA: 0x000021E4 File Offset: 0x000003E4
		public DiscreteTime OneTickBefore()
		{
			return new DiscreteTime(this.m_DiscreteTime - 1L);
		}

		// Token: 0x06000010 RID: 16 RVA: 0x00002208 File Offset: 0x00000408
		public DiscreteTime OneTickAfter()
		{
			return new DiscreteTime(this.m_DiscreteTime + 1L);
		}

		// Token: 0x06000011 RID: 17 RVA: 0x0000222C File Offset: 0x0000042C
		public long GetTick()
		{
			return this.m_DiscreteTime;
		}

		// Token: 0x06000012 RID: 18 RVA: 0x00002248 File Offset: 0x00000448
		public int CompareTo(object obj)
		{
			int result;
			if (obj is DiscreteTime)
			{
				result = this.m_DiscreteTime.CompareTo(((DiscreteTime)obj).m_DiscreteTime);
			}
			else
			{
				result = 1;
			}
			return result;
		}

		// Token: 0x06000013 RID: 19 RVA: 0x0000228C File Offset: 0x0000048C
		public bool Equals(DiscreteTime other)
		{
			return this.m_DiscreteTime == other.m_DiscreteTime;
		}

		// Token: 0x06000014 RID: 20 RVA: 0x000022B0 File Offset: 0x000004B0
		public override bool Equals(object obj)
		{
			return obj is DiscreteTime && this.Equals((DiscreteTime)obj);
		}

		// Token: 0x06000015 RID: 21 RVA: 0x000022E4 File Offset: 0x000004E4
		private static long DoubleToDiscreteTime(double time)
		{
			double num = time / 1E-12 + 0.5;
			if (num < 9.223372036854776E+18 && num > -9.223372036854776E+18)
			{
				return (long)num;
			}
			throw new ArgumentOutOfRangeException("Time is over the discrete range.");
		}

		// Token: 0x06000016 RID: 22 RVA: 0x0000233C File Offset: 0x0000053C
		private static long FloatToDiscreteTime(float time)
		{
			float num = time / 1E-12f + 0.5f;
			if (num < 9.223372E+18f && num > -9.223372E+18f)
			{
				return (long)num;
			}
			throw new ArgumentOutOfRangeException("Time is over the discrete range.");
		}

		// Token: 0x06000017 RID: 23 RVA: 0x00002384 File Offset: 0x00000584
		private static long IntToDiscreteTime(int time)
		{
			return DiscreteTime.DoubleToDiscreteTime((double)time);
		}

		// Token: 0x06000018 RID: 24 RVA: 0x000023A0 File Offset: 0x000005A0
		private static double ToDouble(long time)
		{
			return (double)time * 1E-12;
		}

		// Token: 0x06000019 RID: 25 RVA: 0x000023C4 File Offset: 0x000005C4
		private static float ToFloat(long time)
		{
			return (float)DiscreteTime.ToDouble(time);
		}

		// Token: 0x0600001A RID: 26 RVA: 0x000023E0 File Offset: 0x000005E0
		public static explicit operator double(DiscreteTime b)
		{
			return DiscreteTime.ToDouble(b.m_DiscreteTime);
		}

		// Token: 0x0600001B RID: 27 RVA: 0x00002404 File Offset: 0x00000604
		public static explicit operator float(DiscreteTime b)
		{
			return DiscreteTime.ToFloat(b.m_DiscreteTime);
		}

		// Token: 0x0600001C RID: 28 RVA: 0x00002428 File Offset: 0x00000628
		public static explicit operator long(DiscreteTime b)
		{
			return b.m_DiscreteTime;
		}

		// Token: 0x0600001D RID: 29 RVA: 0x00002444 File Offset: 0x00000644
		public static explicit operator DiscreteTime(double time)
		{
			return new DiscreteTime(time);
		}

		// Token: 0x0600001E RID: 30 RVA: 0x00002460 File Offset: 0x00000660
		public static explicit operator DiscreteTime(float time)
		{
			return new DiscreteTime(time);
		}

		// Token: 0x0600001F RID: 31 RVA: 0x0000247C File Offset: 0x0000067C
		public static implicit operator DiscreteTime(int time)
		{
			return new DiscreteTime(time);
		}

		// Token: 0x06000020 RID: 32 RVA: 0x00002498 File Offset: 0x00000698
		public static explicit operator DiscreteTime(long time)
		{
			return new DiscreteTime(time);
		}

		// Token: 0x06000021 RID: 33 RVA: 0x000024B4 File Offset: 0x000006B4
		public static bool operator ==(DiscreteTime lhs, DiscreteTime rhs)
		{
			return lhs.m_DiscreteTime == rhs.m_DiscreteTime;
		}

		// Token: 0x06000022 RID: 34 RVA: 0x000024DC File Offset: 0x000006DC
		public static bool operator !=(DiscreteTime lhs, DiscreteTime rhs)
		{
			return !(lhs == rhs);
		}

		// Token: 0x06000023 RID: 35 RVA: 0x000024FC File Offset: 0x000006FC
		public static bool operator >(DiscreteTime lhs, DiscreteTime rhs)
		{
			return lhs.m_DiscreteTime > rhs.m_DiscreteTime;
		}

		// Token: 0x06000024 RID: 36 RVA: 0x00002524 File Offset: 0x00000724
		public static bool operator <(DiscreteTime lhs, DiscreteTime rhs)
		{
			return lhs.m_DiscreteTime < rhs.m_DiscreteTime;
		}

		// Token: 0x06000025 RID: 37 RVA: 0x0000254C File Offset: 0x0000074C
		public static bool operator <=(DiscreteTime lhs, DiscreteTime rhs)
		{
			return lhs.m_DiscreteTime <= rhs.m_DiscreteTime;
		}

		// Token: 0x06000026 RID: 38 RVA: 0x00002574 File Offset: 0x00000774
		public static bool operator >=(DiscreteTime lhs, DiscreteTime rhs)
		{
			return lhs.m_DiscreteTime >= rhs.m_DiscreteTime;
		}

		// Token: 0x06000027 RID: 39 RVA: 0x0000259C File Offset: 0x0000079C
		public static DiscreteTime operator +(DiscreteTime lhs, DiscreteTime rhs)
		{
			return new DiscreteTime(lhs.m_DiscreteTime + rhs.m_DiscreteTime);
		}

		// Token: 0x06000028 RID: 40 RVA: 0x000025C8 File Offset: 0x000007C8
		public static DiscreteTime operator -(DiscreteTime lhs, DiscreteTime rhs)
		{
			return new DiscreteTime(lhs.m_DiscreteTime - rhs.m_DiscreteTime);
		}

		// Token: 0x06000029 RID: 41 RVA: 0x000025F4 File Offset: 0x000007F4
		public override string ToString()
		{
			return this.m_DiscreteTime.ToString();
		}

		// Token: 0x0600002A RID: 42 RVA: 0x00002620 File Offset: 0x00000820
		public override int GetHashCode()
		{
			return this.m_DiscreteTime.GetHashCode();
		}

		// Token: 0x0600002B RID: 43 RVA: 0x0000264C File Offset: 0x0000084C
		public static DiscreteTime Min(DiscreteTime lhs, DiscreteTime rhs)
		{
			return new DiscreteTime(Math.Min(lhs.m_DiscreteTime, rhs.m_DiscreteTime));
		}

		// Token: 0x0600002C RID: 44 RVA: 0x0000267C File Offset: 0x0000087C
		public static DiscreteTime Max(DiscreteTime lhs, DiscreteTime rhs)
		{
			return new DiscreteTime(Math.Max(lhs.m_DiscreteTime, rhs.m_DiscreteTime));
		}

		// Token: 0x0600002D RID: 45 RVA: 0x000026AC File Offset: 0x000008AC
		public static double SnapToNearestTick(double time)
		{
			long time2 = DiscreteTime.DoubleToDiscreteTime(time);
			return DiscreteTime.ToDouble(time2);
		}

		// Token: 0x0600002E RID: 46 RVA: 0x000026D0 File Offset: 0x000008D0
		public static float SnapToNearestTick(float time)
		{
			long time2 = DiscreteTime.FloatToDiscreteTime(time);
			return DiscreteTime.ToFloat(time2);
		}

		// Token: 0x0600002F RID: 47 RVA: 0x000026F4 File Offset: 0x000008F4
		public static long GetNearestTick(double time)
		{
			return DiscreteTime.DoubleToDiscreteTime(time);
		}

		// Token: 0x06000030 RID: 48 RVA: 0x0000270F File Offset: 0x0000090F
		// Note: this type is marked as 'beforefieldinit'.
		static DiscreteTime()
		{
		}

		// Token: 0x04000009 RID: 9
		private const double k_Tick = 1E-12;

		// Token: 0x0400000A RID: 10
		public static readonly DiscreteTime kMaxTime = new DiscreteTime(long.MaxValue);

		// Token: 0x0400000B RID: 11
		private readonly long m_DiscreteTime;
	}
}
