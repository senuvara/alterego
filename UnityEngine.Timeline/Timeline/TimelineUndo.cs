﻿using System;
using System.Diagnostics;

namespace UnityEngine.Timeline
{
	// Token: 0x02000050 RID: 80
	internal static class TimelineUndo
	{
		// Token: 0x0600024C RID: 588 RVA: 0x0000BA0E File Offset: 0x00009C0E
		public static void PushDestroyUndo(TimelineAsset timeline, Object thingToDirty, Object objectToDestroy, string operation)
		{
			if (objectToDestroy != null)
			{
				Object.Destroy(objectToDestroy);
			}
		}

		// Token: 0x0600024D RID: 589 RVA: 0x0000BA23 File Offset: 0x00009C23
		[Conditional("UNITY_EDITOR")]
		public static void PushUndo(Object thingToDirty, string operation)
		{
		}

		// Token: 0x0600024E RID: 590 RVA: 0x0000BA26 File Offset: 0x00009C26
		[Conditional("UNITY_EDITOR")]
		public static void RegisterCreatedObjectUndo(Object thingCreated, string operation)
		{
		}
	}
}
