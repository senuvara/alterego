﻿using System;
using System.Collections.Generic;
using UnityEngine.Playables;

namespace UnityEngine.Timeline
{
	// Token: 0x02000006 RID: 6
	internal interface IDirectorDriver
	{
		// Token: 0x06000034 RID: 52
		IList<PlayableDirector> GetDrivenDirectors(IExposedPropertyTable resolver);
	}
}
