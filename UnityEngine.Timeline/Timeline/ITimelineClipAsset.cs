﻿using System;

namespace UnityEngine.Timeline
{
	// Token: 0x02000014 RID: 20
	public interface ITimelineClipAsset
	{
		// Token: 0x1700000F RID: 15
		// (get) Token: 0x0600006B RID: 107
		ClipCaps clipCaps { get; }
	}
}
