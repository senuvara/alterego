﻿using System;
using System.Collections.Generic;

namespace UnityEngine.Timeline
{
	// Token: 0x0200004C RID: 76
	public interface IPropertyCollector
	{
		// Token: 0x0600022D RID: 557
		void PushActiveGameObject(GameObject gameObject);

		// Token: 0x0600022E RID: 558
		void PopActiveGameObject();

		// Token: 0x0600022F RID: 559
		void AddFromClip(AnimationClip clip);

		// Token: 0x06000230 RID: 560
		void AddFromClips(IEnumerable<AnimationClip> clips);

		// Token: 0x06000231 RID: 561
		void AddFromName<T>(string name) where T : Component;

		// Token: 0x06000232 RID: 562
		void AddFromName(string name);

		// Token: 0x06000233 RID: 563
		void AddFromClip(GameObject obj, AnimationClip clip);

		// Token: 0x06000234 RID: 564
		void AddFromClips(GameObject obj, IEnumerable<AnimationClip> clips);

		// Token: 0x06000235 RID: 565
		void AddFromName<T>(GameObject obj, string name) where T : Component;

		// Token: 0x06000236 RID: 566
		void AddFromName(GameObject obj, string name);

		// Token: 0x06000237 RID: 567
		void AddFromComponent(GameObject obj, Component component);

		// Token: 0x06000238 RID: 568
		void AddObjectProperties(Object obj, AnimationClip clip);
	}
}
