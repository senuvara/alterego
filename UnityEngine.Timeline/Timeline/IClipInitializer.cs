﻿using System;

namespace UnityEngine.Timeline
{
	// Token: 0x02000015 RID: 21
	internal interface IClipInitializer
	{
		// Token: 0x0600006C RID: 108
		void OnCreate(TimelineClip owningClip, TrackAsset owningTrack, IExposedPropertyTable resolver);
	}
}
