﻿using System;
using UnityEngine.Playables;

namespace UnityEngine.Timeline
{
	// Token: 0x02000036 RID: 54
	internal class InfiniteRuntimeClip : RuntimeElement
	{
		// Token: 0x060001B2 RID: 434 RVA: 0x0000928B File Offset: 0x0000748B
		public InfiniteRuntimeClip(Playable playable)
		{
			this.m_Playable = playable;
		}

		// Token: 0x17000078 RID: 120
		// (get) Token: 0x060001B3 RID: 435 RVA: 0x0000929C File Offset: 0x0000749C
		public override long intervalStart
		{
			get
			{
				return 0L;
			}
		}

		// Token: 0x17000079 RID: 121
		// (get) Token: 0x060001B4 RID: 436 RVA: 0x000092B4 File Offset: 0x000074B4
		public override long intervalEnd
		{
			get
			{
				return InfiniteRuntimeClip.kIntervalEnd;
			}
		}

		// Token: 0x1700007A RID: 122
		// (set) Token: 0x060001B5 RID: 437 RVA: 0x000092CE File Offset: 0x000074CE
		public override bool enable
		{
			set
			{
				if (value)
				{
					this.m_Playable.Play<Playable>();
				}
				else
				{
					this.m_Playable.Pause<Playable>();
				}
			}
		}

		// Token: 0x060001B6 RID: 438 RVA: 0x000092F2 File Offset: 0x000074F2
		public override void EvaluateAt(double localTime, FrameData frameData)
		{
			this.m_Playable.SetTime(localTime);
		}

		// Token: 0x060001B7 RID: 439 RVA: 0x00009301 File Offset: 0x00007501
		// Note: this type is marked as 'beforefieldinit'.
		static InfiniteRuntimeClip()
		{
		}

		// Token: 0x040000D8 RID: 216
		private Playable m_Playable;

		// Token: 0x040000D9 RID: 217
		private static readonly long kIntervalEnd = DiscreteTime.GetNearestTick(TimelineClip.kMaxTimeValue);
	}
}
