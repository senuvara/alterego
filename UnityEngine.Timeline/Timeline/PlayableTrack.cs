﻿using System;

namespace UnityEngine.Timeline
{
	// Token: 0x02000049 RID: 73
	[Serializable]
	public class PlayableTrack : TrackAsset
	{
		// Token: 0x06000220 RID: 544 RVA: 0x0000ACE9 File Offset: 0x00008EE9
		public PlayableTrack()
		{
		}

		// Token: 0x06000221 RID: 545 RVA: 0x0000ACF1 File Offset: 0x00008EF1
		protected override void OnCreateClip(TimelineClip clip)
		{
			if (clip.asset != null)
			{
				clip.displayName = clip.asset.GetType().Name;
			}
		}
	}
}
