﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace UnityEngine.Timeline
{
	// Token: 0x0200004F RID: 79
	internal static class TimelineCreateUtilities
	{
		// Token: 0x06000249 RID: 585 RVA: 0x0000B738 File Offset: 0x00009938
		public static string GenerateUniqueActorName(List<ScriptableObject> tracks, string name)
		{
			string name2;
			if (!tracks.Exists((ScriptableObject x) => x != null && x.name == name))
			{
				name2 = name;
			}
			else
			{
				int num = 0;
				string text = name;
				if (!string.IsNullOrEmpty(name) && name[name.Length - 1] == ')')
				{
					int num2 = name.LastIndexOf('(');
					if (num2 > 0)
					{
						string s = name.Substring(num2 + 1, name.Length - num2 - 2);
						if (int.TryParse(s, out num))
						{
							num++;
							text = name.Substring(0, num2);
						}
					}
				}
				text = text.TrimEnd(new char[0]);
				for (int i = num; i < num + 5000; i++)
				{
					if (i > 0)
					{
						string result = string.Format("{0} ({1})", text, i);
						if (!tracks.Exists((ScriptableObject x) => x != null && x.name == result))
						{
							return result;
						}
					}
				}
				name2 = name;
			}
			return name2;
		}

		// Token: 0x0600024A RID: 586 RVA: 0x0000B894 File Offset: 0x00009A94
		public static void SaveAssetIntoObject(Object childAsset, Object masterAsset)
		{
			if ((masterAsset.hideFlags & HideFlags.DontSave) != HideFlags.None)
			{
				childAsset.hideFlags |= HideFlags.DontSave;
			}
			else
			{
				childAsset.hideFlags |= HideFlags.HideInHierarchy;
			}
		}

		// Token: 0x0600024B RID: 587 RVA: 0x0000B8CC File Offset: 0x00009ACC
		internal static bool ValidateParentTrack(TrackAsset parent, Type childType)
		{
			bool result;
			if (childType == null || !typeof(TrackAsset).IsAssignableFrom(childType))
			{
				result = false;
			}
			else if (parent == null)
			{
				result = true;
			}
			else
			{
				SupportsChildTracksAttribute supportsChildTracksAttribute = Attribute.GetCustomAttribute(parent.GetType(), typeof(SupportsChildTracksAttribute)) as SupportsChildTracksAttribute;
				if (supportsChildTracksAttribute == null)
				{
					result = false;
				}
				else if (supportsChildTracksAttribute.childType == null)
				{
					result = true;
				}
				else if (childType == supportsChildTracksAttribute.childType)
				{
					int num = 0;
					TrackAsset trackAsset = parent;
					while (trackAsset != null && trackAsset.isSubTrack)
					{
						num++;
						trackAsset = (trackAsset.parent as TrackAsset);
					}
					result = (num < supportsChildTracksAttribute.levels);
				}
				else
				{
					result = false;
				}
			}
			return result;
		}

		// Token: 0x02000059 RID: 89
		[CompilerGenerated]
		private sealed class <GenerateUniqueActorName>c__AnonStorey0
		{
			// Token: 0x06000288 RID: 648 RVA: 0x0000B99F File Offset: 0x00009B9F
			public <GenerateUniqueActorName>c__AnonStorey0()
			{
			}

			// Token: 0x06000289 RID: 649 RVA: 0x0000B9A8 File Offset: 0x00009BA8
			internal bool <>m__0(ScriptableObject x)
			{
				return x != null && x.name == this.name;
			}

			// Token: 0x04000134 RID: 308
			internal string name;
		}

		// Token: 0x0200005A RID: 90
		[CompilerGenerated]
		private sealed class <GenerateUniqueActorName>c__AnonStorey1
		{
			// Token: 0x0600028A RID: 650 RVA: 0x0000B9D6 File Offset: 0x00009BD6
			public <GenerateUniqueActorName>c__AnonStorey1()
			{
			}

			// Token: 0x0600028B RID: 651 RVA: 0x0000B9E0 File Offset: 0x00009BE0
			internal bool <>m__0(ScriptableObject x)
			{
				return x != null && x.name == this.result;
			}

			// Token: 0x04000135 RID: 309
			internal string result;
		}
	}
}
