﻿using System;
using UnityEngine.Playables;

namespace UnityEngine.Timeline
{
	// Token: 0x0200001E RID: 30
	[TrackClipType(typeof(ActivationPlayableAsset))]
	[TrackBindingType(typeof(GameObject))]
	[Serializable]
	public class ActivationTrack : TrackAsset
	{
		// Token: 0x0600011C RID: 284 RVA: 0x000064A4 File Offset: 0x000046A4
		public ActivationTrack()
		{
		}

		// Token: 0x1700004B RID: 75
		// (get) Token: 0x0600011D RID: 285 RVA: 0x000064B4 File Offset: 0x000046B4
		internal override bool compilable
		{
			get
			{
				return this.isEmpty || base.compilable;
			}
		}

		// Token: 0x1700004C RID: 76
		// (get) Token: 0x0600011E RID: 286 RVA: 0x000064E0 File Offset: 0x000046E0
		// (set) Token: 0x0600011F RID: 287 RVA: 0x000064FB File Offset: 0x000046FB
		public ActivationTrack.PostPlaybackState postPlaybackState
		{
			get
			{
				return this.m_PostPlaybackState;
			}
			set
			{
				this.m_PostPlaybackState = value;
				this.UpdateTrackMode();
			}
		}

		// Token: 0x06000120 RID: 288 RVA: 0x0000650C File Offset: 0x0000470C
		public override Playable CreateTrackMixer(PlayableGraph graph, GameObject go, int inputCount)
		{
			ScriptPlayable<ActivationMixerPlayable> playable = ActivationMixerPlayable.Create(graph, inputCount);
			this.m_ActivationMixer = playable.GetBehaviour();
			this.UpdateTrackMode();
			return playable;
		}

		// Token: 0x06000121 RID: 289 RVA: 0x00006542 File Offset: 0x00004742
		internal void UpdateTrackMode()
		{
			if (this.m_ActivationMixer != null)
			{
				this.m_ActivationMixer.postPlaybackState = this.m_PostPlaybackState;
			}
		}

		// Token: 0x06000122 RID: 290 RVA: 0x00006564 File Offset: 0x00004764
		public override void GatherProperties(PlayableDirector director, IPropertyCollector driver)
		{
			GameObject gameObjectBinding = base.GetGameObjectBinding(director);
			if (gameObjectBinding != null)
			{
				driver.AddFromName(gameObjectBinding, "m_IsActive");
			}
		}

		// Token: 0x06000123 RID: 291 RVA: 0x00006594 File Offset: 0x00004794
		protected override void OnCreateClip(TimelineClip clip)
		{
			clip.displayName = "Active";
			base.OnCreateClip(clip);
		}

		// Token: 0x04000075 RID: 117
		[SerializeField]
		private ActivationTrack.PostPlaybackState m_PostPlaybackState = ActivationTrack.PostPlaybackState.LeaveAsIs;

		// Token: 0x04000076 RID: 118
		private ActivationMixerPlayable m_ActivationMixer;

		// Token: 0x0200001F RID: 31
		public enum PostPlaybackState
		{
			// Token: 0x04000078 RID: 120
			Active,
			// Token: 0x04000079 RID: 121
			Inactive,
			// Token: 0x0400007A RID: 122
			Revert,
			// Token: 0x0400007B RID: 123
			LeaveAsIs
		}
	}
}
