﻿using System;
using System.Collections;
using UnityEngine.Playables;

namespace UnityEngine.Timeline
{
	// Token: 0x02000047 RID: 71
	public class PrefabControlPlayable : PlayableBehaviour
	{
		// Token: 0x06000212 RID: 530 RVA: 0x0000A9DE File Offset: 0x00008BDE
		public PrefabControlPlayable()
		{
		}

		// Token: 0x06000213 RID: 531 RVA: 0x0000A9E8 File Offset: 0x00008BE8
		public static ScriptPlayable<PrefabControlPlayable> Create(PlayableGraph graph, GameObject prefabGameObject, Transform parentTransform)
		{
			ScriptPlayable<PrefabControlPlayable> result;
			if (prefabGameObject == null)
			{
				result = ScriptPlayable<PrefabControlPlayable>.Null;
			}
			else
			{
				ScriptPlayable<PrefabControlPlayable> scriptPlayable = ScriptPlayable<PrefabControlPlayable>.Create(graph, 0);
				scriptPlayable.GetBehaviour().Initialize(prefabGameObject, parentTransform);
				result = scriptPlayable;
			}
			return result;
		}

		// Token: 0x17000096 RID: 150
		// (get) Token: 0x06000214 RID: 532 RVA: 0x0000AA2C File Offset: 0x00008C2C
		public GameObject prefabInstance
		{
			get
			{
				return this.m_Instance;
			}
		}

		// Token: 0x06000215 RID: 533 RVA: 0x0000AA48 File Offset: 0x00008C48
		public GameObject Initialize(GameObject prefabGameObject, Transform parentTransform)
		{
			if (prefabGameObject == null)
			{
				throw new ArgumentNullException("Prefab cannot be null");
			}
			if (this.m_Instance != null)
			{
				Debug.LogWarningFormat("Prefab Control Playable ({0}) has already been initialized with a Prefab ({1}).", new object[]
				{
					prefabGameObject.name,
					this.m_Instance.name
				});
			}
			else
			{
				this.m_Instance = Object.Instantiate<GameObject>(prefabGameObject, parentTransform, false);
				this.m_Instance.name = prefabGameObject.name + " [Timeline]";
				this.m_Instance.SetActive(false);
				PrefabControlPlayable.SetHideFlagsRecursive(this.m_Instance);
			}
			return this.m_Instance;
		}

		// Token: 0x06000216 RID: 534 RVA: 0x0000AAFB File Offset: 0x00008CFB
		public override void OnPlayableDestroy(Playable playable)
		{
			if (this.m_Instance)
			{
				if (Application.isPlaying)
				{
					Object.Destroy(this.m_Instance);
				}
				else
				{
					Object.DestroyImmediate(this.m_Instance);
				}
			}
		}

		// Token: 0x06000217 RID: 535 RVA: 0x0000AB35 File Offset: 0x00008D35
		public override void OnBehaviourPlay(Playable playable, FrameData info)
		{
			if (!(this.m_Instance == null))
			{
				this.m_Instance.SetActive(true);
			}
		}

		// Token: 0x06000218 RID: 536 RVA: 0x0000AB5A File Offset: 0x00008D5A
		public override void OnBehaviourPause(Playable playable, FrameData info)
		{
			if (this.m_Instance != null && info.effectivePlayState == PlayState.Paused)
			{
				this.m_Instance.SetActive(false);
			}
		}

		// Token: 0x06000219 RID: 537 RVA: 0x0000AB88 File Offset: 0x00008D88
		private static void SetHideFlagsRecursive(GameObject gameObject)
		{
			gameObject.hideFlags = (HideFlags.DontSaveInEditor | HideFlags.DontSaveInBuild);
			if (!Application.isPlaying)
			{
				gameObject.hideFlags |= HideFlags.HideInHierarchy;
			}
			IEnumerator enumerator = gameObject.transform.GetEnumerator();
			try
			{
				while (enumerator.MoveNext())
				{
					object obj = enumerator.Current;
					Transform transform = (Transform)obj;
					PrefabControlPlayable.SetHideFlagsRecursive(transform.gameObject);
				}
			}
			finally
			{
				IDisposable disposable;
				if ((disposable = (enumerator as IDisposable)) != null)
				{
					disposable.Dispose();
				}
			}
		}

		// Token: 0x04000105 RID: 261
		private GameObject m_Instance;
	}
}
