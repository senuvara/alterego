﻿using System;
using UnityEngine.Audio;
using UnityEngine.Playables;

namespace UnityEngine.Timeline
{
	// Token: 0x0200003E RID: 62
	internal class ScheduleRuntimeClip : RuntimeClipBase
	{
		// Token: 0x060001DC RID: 476 RVA: 0x00009BCE File Offset: 0x00007DCE
		public ScheduleRuntimeClip(TimelineClip clip, Playable clipPlayable, Playable parentMixer, double startDelay = 0.2, double finishTail = 0.1)
		{
			this.Create(clip, clipPlayable, parentMixer, startDelay, finishTail);
		}

		// Token: 0x1700008D RID: 141
		// (get) Token: 0x060001DD RID: 477 RVA: 0x00009BEC File Offset: 0x00007DEC
		public override double start
		{
			get
			{
				return Math.Max(0.0, this.m_Clip.start - this.m_StartDelay);
			}
		}

		// Token: 0x1700008E RID: 142
		// (get) Token: 0x060001DE RID: 478 RVA: 0x00009C24 File Offset: 0x00007E24
		public override double duration
		{
			get
			{
				return this.m_Clip.duration + this.m_FinishTail + this.m_Clip.start - this.start;
			}
		}

		// Token: 0x060001DF RID: 479 RVA: 0x00009C5E File Offset: 0x00007E5E
		public void SetTime(double time)
		{
			this.m_Playable.SetTime(time);
		}

		// Token: 0x1700008F RID: 143
		// (get) Token: 0x060001E0 RID: 480 RVA: 0x00009C70 File Offset: 0x00007E70
		public TimelineClip clip
		{
			get
			{
				return this.m_Clip;
			}
		}

		// Token: 0x17000090 RID: 144
		// (get) Token: 0x060001E1 RID: 481 RVA: 0x00009C8C File Offset: 0x00007E8C
		public Playable mixer
		{
			get
			{
				return this.m_ParentMixer;
			}
		}

		// Token: 0x17000091 RID: 145
		// (get) Token: 0x060001E2 RID: 482 RVA: 0x00009CA8 File Offset: 0x00007EA8
		public Playable playable
		{
			get
			{
				return this.m_Playable;
			}
		}

		// Token: 0x060001E3 RID: 483 RVA: 0x00009CC3 File Offset: 0x00007EC3
		private void Create(TimelineClip clip, Playable clipPlayable, Playable parentMixer, double startDelay, double finishTail)
		{
			this.m_Clip = clip;
			this.m_Playable = clipPlayable;
			this.m_ParentMixer = parentMixer;
			this.m_StartDelay = startDelay;
			this.m_FinishTail = finishTail;
			clipPlayable.Pause<Playable>();
		}

		// Token: 0x17000092 RID: 146
		// (set) Token: 0x060001E4 RID: 484 RVA: 0x00009CF4 File Offset: 0x00007EF4
		public override bool enable
		{
			set
			{
				if (value && this.m_Playable.GetPlayState<Playable>() != PlayState.Playing)
				{
					this.m_Playable.Play<Playable>();
				}
				else if (!value && this.m_Playable.GetPlayState<Playable>() != PlayState.Paused)
				{
					this.m_Playable.Pause<Playable>();
					if (this.m_ParentMixer.IsValid<Playable>())
					{
						this.m_ParentMixer.SetInputWeight(this.m_Playable, 0f);
					}
				}
				this.m_Started = (this.m_Started && value);
			}
		}

		// Token: 0x060001E5 RID: 485 RVA: 0x00009D84 File Offset: 0x00007F84
		public override void EvaluateAt(double localTime, FrameData frameData)
		{
			if (frameData.timeHeld)
			{
				this.enable = false;
			}
			else
			{
				bool flag = frameData.seekOccurred || frameData.timeLooped || frameData.evaluationType == FrameData.EvaluationType.Evaluate;
				if (localTime <= this.start + this.duration - this.m_FinishTail)
				{
					float weight = this.clip.EvaluateMixIn(localTime) * this.clip.EvaluateMixOut(localTime);
					if (this.mixer.IsValid<Playable>())
					{
						this.mixer.SetInputWeight(this.playable, weight);
					}
					if (!this.m_Started || flag)
					{
						double startTime = this.clip.ToLocalTime(Math.Max(localTime, this.clip.start));
						double startDelay = Math.Max(this.clip.start - localTime, 0.0) * this.clip.timeScale;
						double duration = this.m_Clip.duration * this.clip.timeScale;
						if (this.m_Playable.IsPlayableOfType<AudioClipPlayable>())
						{
							((AudioClipPlayable)this.m_Playable).Seek(startTime, startDelay, duration);
						}
						this.m_Started = true;
					}
				}
			}
		}

		// Token: 0x040000EC RID: 236
		private TimelineClip m_Clip;

		// Token: 0x040000ED RID: 237
		private Playable m_Playable;

		// Token: 0x040000EE RID: 238
		private Playable m_ParentMixer;

		// Token: 0x040000EF RID: 239
		private double m_StartDelay;

		// Token: 0x040000F0 RID: 240
		private double m_FinishTail;

		// Token: 0x040000F1 RID: 241
		private bool m_Started = false;
	}
}
