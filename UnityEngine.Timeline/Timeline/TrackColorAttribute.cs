﻿using System;

namespace UnityEngine.Timeline
{
	// Token: 0x02000031 RID: 49
	[AttributeUsage(AttributeTargets.Class)]
	public class TrackColorAttribute : Attribute
	{
		// Token: 0x0600018C RID: 396 RVA: 0x00008030 File Offset: 0x00006230
		public TrackColorAttribute(float r, float g, float b)
		{
			this.m_Color = new Color(r, g, b);
		}

		// Token: 0x1700006E RID: 110
		// (get) Token: 0x0600018D RID: 397 RVA: 0x00008048 File Offset: 0x00006248
		public Color color
		{
			get
			{
				return this.m_Color;
			}
		}

		// Token: 0x040000C3 RID: 195
		private Color m_Color;
	}
}
