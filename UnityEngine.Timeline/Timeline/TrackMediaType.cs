﻿using System;

namespace UnityEngine.Timeline
{
	// Token: 0x0200000A RID: 10
	[AttributeUsage(AttributeTargets.Class)]
	[Obsolete("TrackMediaType has been deprecated. It is no longer required, and will be removed in a future release.", false)]
	public class TrackMediaType : Attribute
	{
		// Token: 0x0600005E RID: 94 RVA: 0x00004B08 File Offset: 0x00002D08
		public TrackMediaType(TimelineAsset.MediaType mt)
		{
			this.m_MediaType = mt;
		}

		// Token: 0x0400001E RID: 30
		public readonly TimelineAsset.MediaType m_MediaType;
	}
}
