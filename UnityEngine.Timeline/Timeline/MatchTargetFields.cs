﻿using System;

namespace UnityEngine.Timeline
{
	// Token: 0x02000023 RID: 35
	[Flags]
	public enum MatchTargetFields
	{
		// Token: 0x04000091 RID: 145
		PositionX = 1,
		// Token: 0x04000092 RID: 146
		PositionY = 2,
		// Token: 0x04000093 RID: 147
		PositionZ = 4,
		// Token: 0x04000094 RID: 148
		RotationX = 8,
		// Token: 0x04000095 RID: 149
		RotationY = 16,
		// Token: 0x04000096 RID: 150
		RotationZ = 32
	}
}
