﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading;
using UnityEngine.Audio;
using UnityEngine.Playables;

namespace UnityEngine.Timeline
{
	// Token: 0x02000032 RID: 50
	[Serializable]
	public class AudioPlayableAsset : PlayableAsset, ITimelineClipAsset
	{
		// Token: 0x0600018E RID: 398 RVA: 0x00008063 File Offset: 0x00006263
		public AudioPlayableAsset()
		{
		}

		// Token: 0x1700006F RID: 111
		// (get) Token: 0x0600018F RID: 399 RVA: 0x00008078 File Offset: 0x00006278
		// (set) Token: 0x06000190 RID: 400 RVA: 0x00008093 File Offset: 0x00006293
		internal float bufferingTime
		{
			get
			{
				return this.m_bufferingTime;
			}
			set
			{
				this.m_bufferingTime = value;
			}
		}

		// Token: 0x17000070 RID: 112
		// (get) Token: 0x06000191 RID: 401 RVA: 0x000080A0 File Offset: 0x000062A0
		// (set) Token: 0x06000192 RID: 402 RVA: 0x000080BB File Offset: 0x000062BB
		public AudioClip clip
		{
			get
			{
				return this.m_Clip;
			}
			set
			{
				this.m_Clip = value;
			}
		}

		// Token: 0x17000071 RID: 113
		// (get) Token: 0x06000193 RID: 403 RVA: 0x000080C8 File Offset: 0x000062C8
		// (set) Token: 0x06000194 RID: 404 RVA: 0x000080E3 File Offset: 0x000062E3
		public bool loop
		{
			get
			{
				return this.m_Loop;
			}
			set
			{
				this.m_Loop = value;
			}
		}

		// Token: 0x17000072 RID: 114
		// (get) Token: 0x06000195 RID: 405 RVA: 0x000080F0 File Offset: 0x000062F0
		public override double duration
		{
			get
			{
				double result;
				if (this.m_Clip == null)
				{
					result = base.duration;
				}
				else
				{
					result = (double)this.m_Clip.samples / (double)this.m_Clip.frequency;
				}
				return result;
			}
		}

		// Token: 0x17000073 RID: 115
		// (get) Token: 0x06000196 RID: 406 RVA: 0x0000813C File Offset: 0x0000633C
		public override IEnumerable<PlayableBinding> outputs
		{
			get
			{
				yield return AudioPlayableBinding.Create(base.name, this);
				yield break;
			}
		}

		// Token: 0x06000197 RID: 407 RVA: 0x00008168 File Offset: 0x00006368
		public override Playable CreatePlayable(PlayableGraph graph, GameObject go)
		{
			Playable result;
			if (this.m_Clip == null)
			{
				result = Playable.Null;
			}
			else
			{
				result = AudioClipPlayable.Create(graph, this.m_Clip, this.m_Loop);
			}
			return result;
		}

		// Token: 0x17000074 RID: 116
		// (get) Token: 0x06000198 RID: 408 RVA: 0x000081B0 File Offset: 0x000063B0
		public ClipCaps clipCaps
		{
			get
			{
				return ClipCaps.ClipIn | ClipCaps.SpeedMultiplier | ClipCaps.Blending | ((!this.m_Loop) ? ClipCaps.None : ClipCaps.Looping);
			}
		}

		// Token: 0x040000C4 RID: 196
		[SerializeField]
		private AudioClip m_Clip;

		// Token: 0x040000C5 RID: 197
		[SerializeField]
		private bool m_Loop;

		// Token: 0x040000C6 RID: 198
		[SerializeField]
		[HideInInspector]
		private float m_bufferingTime = 0.1f;

		// Token: 0x02000056 RID: 86
		[CompilerGenerated]
		private sealed class <>c__Iterator0 : IEnumerable, IEnumerable<PlayableBinding>, IEnumerator, IDisposable, IEnumerator<PlayableBinding>
		{
			// Token: 0x06000270 RID: 624 RVA: 0x000081DA File Offset: 0x000063DA
			[DebuggerHidden]
			public <>c__Iterator0()
			{
			}

			// Token: 0x06000271 RID: 625 RVA: 0x000081E4 File Offset: 0x000063E4
			public bool MoveNext()
			{
				uint num = (uint)this.$PC;
				this.$PC = -1;
				switch (num)
				{
				case 0U:
					this.$current = AudioPlayableBinding.Create(base.name, this);
					if (!this.$disposing)
					{
						this.$PC = 1;
					}
					return true;
				case 1U:
					this.$PC = -1;
					break;
				}
				return false;
			}

			// Token: 0x1700009F RID: 159
			// (get) Token: 0x06000272 RID: 626 RVA: 0x00008250 File Offset: 0x00006450
			PlayableBinding IEnumerator<PlayableBinding>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.$current;
				}
			}

			// Token: 0x170000A0 RID: 160
			// (get) Token: 0x06000273 RID: 627 RVA: 0x0000826C File Offset: 0x0000646C
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.$current;
				}
			}

			// Token: 0x06000274 RID: 628 RVA: 0x0000828B File Offset: 0x0000648B
			[DebuggerHidden]
			public void Dispose()
			{
				this.$disposing = true;
				this.$PC = -1;
			}

			// Token: 0x06000275 RID: 629 RVA: 0x0000829B File Offset: 0x0000649B
			[DebuggerHidden]
			public void Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x06000276 RID: 630 RVA: 0x000082A4 File Offset: 0x000064A4
			[DebuggerHidden]
			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.System.Collections.Generic.IEnumerable<UnityEngine.Playables.PlayableBinding>.GetEnumerator();
			}

			// Token: 0x06000277 RID: 631 RVA: 0x000082C0 File Offset: 0x000064C0
			[DebuggerHidden]
			IEnumerator<PlayableBinding> IEnumerable<PlayableBinding>.GetEnumerator()
			{
				if (Interlocked.CompareExchange(ref this.$PC, 0, -2) == -2)
				{
					return this;
				}
				AudioPlayableAsset.<>c__Iterator0 <>c__Iterator = new AudioPlayableAsset.<>c__Iterator0();
				<>c__Iterator.$this = this;
				return <>c__Iterator;
			}

			// Token: 0x04000125 RID: 293
			internal AudioPlayableAsset $this;

			// Token: 0x04000126 RID: 294
			internal PlayableBinding $current;

			// Token: 0x04000127 RID: 295
			internal bool $disposing;

			// Token: 0x04000128 RID: 296
			internal int $PC;
		}
	}
}
