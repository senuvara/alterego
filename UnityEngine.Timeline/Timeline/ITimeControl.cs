﻿using System;

namespace UnityEngine.Timeline
{
	// Token: 0x02000045 RID: 69
	public interface ITimeControl
	{
		// Token: 0x06000206 RID: 518
		void SetTime(double time);

		// Token: 0x06000207 RID: 519
		void OnControlTimeStart();

		// Token: 0x06000208 RID: 520
		void OnControlTimeStop();
	}
}
