﻿using System;
using UnityEngine.Playables;

namespace UnityEngine.Timeline
{
	// Token: 0x0200004D RID: 77
	public interface IPropertyPreview
	{
		// Token: 0x06000239 RID: 569
		void GatherProperties(PlayableDirector director, IPropertyCollector driver);
	}
}
