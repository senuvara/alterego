﻿using System;
using UnityEngine.Playables;

namespace UnityEngine.Timeline
{
	// Token: 0x0200003B RID: 59
	internal class RuntimeClip : RuntimeClipBase
	{
		// Token: 0x060001C5 RID: 453 RVA: 0x00009973 File Offset: 0x00007B73
		public RuntimeClip(TimelineClip clip, Playable clipPlayable, Playable parentMixer)
		{
			this.Create(clip, clipPlayable, parentMixer);
		}

		// Token: 0x1700007F RID: 127
		// (get) Token: 0x060001C6 RID: 454 RVA: 0x00009988 File Offset: 0x00007B88
		public override double start
		{
			get
			{
				return this.m_Clip.extrapolatedStart;
			}
		}

		// Token: 0x17000080 RID: 128
		// (get) Token: 0x060001C7 RID: 455 RVA: 0x000099A8 File Offset: 0x00007BA8
		public override double duration
		{
			get
			{
				return this.m_Clip.extrapolatedDuration;
			}
		}

		// Token: 0x060001C8 RID: 456 RVA: 0x000099C8 File Offset: 0x00007BC8
		private void Create(TimelineClip clip, Playable clipPlayable, Playable parentMixer)
		{
			this.m_Clip = clip;
			this.m_Playable = clipPlayable;
			this.m_ParentMixer = parentMixer;
			clipPlayable.Pause<Playable>();
		}

		// Token: 0x17000081 RID: 129
		// (get) Token: 0x060001C9 RID: 457 RVA: 0x000099E8 File Offset: 0x00007BE8
		public TimelineClip clip
		{
			get
			{
				return this.m_Clip;
			}
		}

		// Token: 0x17000082 RID: 130
		// (get) Token: 0x060001CA RID: 458 RVA: 0x00009A04 File Offset: 0x00007C04
		public Playable mixer
		{
			get
			{
				return this.m_ParentMixer;
			}
		}

		// Token: 0x17000083 RID: 131
		// (get) Token: 0x060001CB RID: 459 RVA: 0x00009A20 File Offset: 0x00007C20
		public Playable playable
		{
			get
			{
				return this.m_Playable;
			}
		}

		// Token: 0x17000084 RID: 132
		// (set) Token: 0x060001CC RID: 460 RVA: 0x00009A3C File Offset: 0x00007C3C
		public override bool enable
		{
			set
			{
				if (value && this.m_Playable.GetPlayState<Playable>() != PlayState.Playing)
				{
					this.m_Playable.Play<Playable>();
				}
				else if (!value && this.m_Playable.GetPlayState<Playable>() != PlayState.Paused)
				{
					this.m_Playable.Pause<Playable>();
					if (this.m_ParentMixer.IsValid<Playable>())
					{
						this.m_ParentMixer.SetInputWeight(this.m_Playable, 0f);
					}
				}
			}
		}

		// Token: 0x060001CD RID: 461 RVA: 0x00009ABC File Offset: 0x00007CBC
		public void SetTime(double time)
		{
			this.m_Playable.SetTime(time);
		}

		// Token: 0x060001CE RID: 462 RVA: 0x00009ACB File Offset: 0x00007CCB
		public void SetDuration(double duration)
		{
			this.m_Playable.SetDuration(duration);
		}

		// Token: 0x060001CF RID: 463 RVA: 0x00009ADC File Offset: 0x00007CDC
		public override void EvaluateAt(double localTime, FrameData frameData)
		{
			this.enable = true;
			float weight;
			if (this.clip.IsPreExtrapolatedTime(localTime))
			{
				weight = this.clip.EvaluateMixIn((double)((float)this.clip.start));
			}
			else if (this.clip.IsPostExtrapolatedTime(localTime))
			{
				weight = this.clip.EvaluateMixOut((double)((float)this.clip.end));
			}
			else
			{
				weight = this.clip.EvaluateMixIn(localTime) * this.clip.EvaluateMixOut(localTime);
			}
			if (this.mixer.IsValid<Playable>())
			{
				this.mixer.SetInputWeight(this.playable, weight);
			}
			double time = this.clip.ToLocalTime(localTime);
			if (time.CompareTo(0.0) >= 0)
			{
				this.SetTime(time);
			}
			this.SetDuration(this.clip.extrapolatedDuration);
		}

		// Token: 0x040000E8 RID: 232
		private TimelineClip m_Clip;

		// Token: 0x040000E9 RID: 233
		private Playable m_Playable;

		// Token: 0x040000EA RID: 234
		private Playable m_ParentMixer;
	}
}
