﻿using System;

namespace UnityEngine.Timeline
{
	// Token: 0x02000002 RID: 2
	[Flags]
	public enum ClipCaps
	{
		// Token: 0x04000002 RID: 2
		None = 0,
		// Token: 0x04000003 RID: 3
		Looping = 1,
		// Token: 0x04000004 RID: 4
		Extrapolation = 2,
		// Token: 0x04000005 RID: 5
		ClipIn = 4,
		// Token: 0x04000006 RID: 6
		SpeedMultiplier = 8,
		// Token: 0x04000007 RID: 7
		Blending = 16,
		// Token: 0x04000008 RID: 8
		All = -1
	}
}
