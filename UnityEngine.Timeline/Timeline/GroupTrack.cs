﻿using System;
using System.Collections.Generic;
using UnityEngine.Playables;

namespace UnityEngine.Timeline
{
	// Token: 0x02000005 RID: 5
	[TrackClipType(typeof(TrackAsset))]
	[SupportsChildTracks(null, 2147483647)]
	[Serializable]
	public class GroupTrack : TrackAsset
	{
		// Token: 0x06000031 RID: 49 RVA: 0x00003B4C File Offset: 0x00001D4C
		public GroupTrack()
		{
		}

		// Token: 0x17000002 RID: 2
		// (get) Token: 0x06000032 RID: 50 RVA: 0x00003B54 File Offset: 0x00001D54
		internal override bool compilable
		{
			get
			{
				return false;
			}
		}

		// Token: 0x17000003 RID: 3
		// (get) Token: 0x06000033 RID: 51 RVA: 0x00003B6C File Offset: 0x00001D6C
		public override IEnumerable<PlayableBinding> outputs
		{
			get
			{
				return PlayableBinding.None;
			}
		}
	}
}
