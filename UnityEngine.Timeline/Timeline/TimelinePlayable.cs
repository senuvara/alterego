﻿using System;
using System.Collections.Generic;
using UnityEngine.Animations;
using UnityEngine.Audio;
using UnityEngine.Playables;

namespace UnityEngine.Timeline
{
	// Token: 0x0200001A RID: 26
	public class TimelinePlayable : PlayableBehaviour
	{
		// Token: 0x060000C0 RID: 192 RVA: 0x00005D44 File Offset: 0x00003F44
		public TimelinePlayable()
		{
		}

		// Token: 0x060000C1 RID: 193 RVA: 0x00005D80 File Offset: 0x00003F80
		public static ScriptPlayable<TimelinePlayable> Create(PlayableGraph graph, IEnumerable<TrackAsset> tracks, GameObject go, bool autoRebalance, bool createOutputs)
		{
			if (tracks == null)
			{
				throw new ArgumentNullException("Tracks list is null", "tracks");
			}
			if (go == null)
			{
				throw new ArgumentNullException("GameObject parameter is null", "go");
			}
			ScriptPlayable<TimelinePlayable> scriptPlayable = ScriptPlayable<TimelinePlayable>.Create(graph, 0);
			scriptPlayable.SetTraversalMode(PlayableTraversalMode.Passthrough);
			TimelinePlayable behaviour = scriptPlayable.GetBehaviour();
			behaviour.Compile(graph, scriptPlayable, tracks, go, autoRebalance, createOutputs);
			return scriptPlayable;
		}

		// Token: 0x060000C2 RID: 194 RVA: 0x00005DF4 File Offset: 0x00003FF4
		public void Compile(PlayableGraph graph, Playable timelinePlayable, IEnumerable<TrackAsset> tracks, GameObject go, bool autoRebalance, bool createOutputs)
		{
			if (tracks == null)
			{
				throw new ArgumentNullException("Tracks list is null", "tracks");
			}
			if (go == null)
			{
				throw new ArgumentNullException("GameObject parameter is null", "go");
			}
			List<TrackAsset> list = new List<TrackAsset>(tracks);
			int capacity = list.Count * 2 + list.Count;
			this.m_CurrentListOfActiveClips = new List<RuntimeElement>(capacity);
			this.m_ActiveClips = new List<RuntimeElement>(capacity);
			this.m_EvaluateCallbacks.Clear();
			this.m_PlayableCache.Clear();
			this.CompileTrackList(graph, timelinePlayable, list, go, createOutputs);
		}

		// Token: 0x060000C3 RID: 195 RVA: 0x00005E88 File Offset: 0x00004088
		private void CompileTrackList(PlayableGraph graph, Playable timelinePlayable, IEnumerable<TrackAsset> tracks, GameObject go, bool createOutputs)
		{
			foreach (TrackAsset trackAsset in tracks)
			{
				if (trackAsset.compilable)
				{
					if (!this.m_PlayableCache.ContainsKey(trackAsset))
					{
						trackAsset.SortClips();
						this.CreateTrackPlayable(graph, timelinePlayable, trackAsset, go, createOutputs);
					}
				}
			}
		}

		// Token: 0x060000C4 RID: 196 RVA: 0x00005F10 File Offset: 0x00004110
		private void CreateTrackOutput(PlayableGraph graph, TrackAsset track, Playable playable, int port)
		{
			if (!track.isSubTrack)
			{
				IEnumerable<PlayableBinding> outputs = track.outputs;
				foreach (PlayableBinding playableBinding in outputs)
				{
					PlayableOutput output = playableBinding.CreateOutput(graph);
					output.SetReferenceObject(playableBinding.sourceObject);
					output.SetSourcePlayable(playable);
					output.SetSourceOutputPort(port);
					output.SetWeight(1f);
					if (track as AnimationTrack != null)
					{
						this.EvaluateWeightsForAnimationPlayableOutput(track, (AnimationPlayableOutput)output);
					}
					if (output.IsPlayableOutputOfType<AudioPlayableOutput>())
					{
						((AudioPlayableOutput)output).SetEvaluateOnSeek(!TimelinePlayable.muteAudioScrubbing);
					}
				}
			}
		}

		// Token: 0x060000C5 RID: 197 RVA: 0x00005FEC File Offset: 0x000041EC
		private void EvaluateWeightsForAnimationPlayableOutput(TrackAsset track, AnimationPlayableOutput animOutput)
		{
			this.m_EvaluateCallbacks.Add(new AnimationOutputWeightProcessor(animOutput));
		}

		// Token: 0x060000C6 RID: 198 RVA: 0x00006000 File Offset: 0x00004200
		private static Playable CreatePlayableGraph(PlayableGraph graph, TrackAsset asset, GameObject go, IntervalTree<RuntimeElement> tree)
		{
			return asset.CreatePlayableGraph(graph, go, tree);
		}

		// Token: 0x060000C7 RID: 199 RVA: 0x00006020 File Offset: 0x00004220
		private Playable CreateTrackPlayable(PlayableGraph graph, Playable timelinePlayable, TrackAsset track, GameObject go, bool createOutputs)
		{
			Playable result;
			Playable playable;
			if (!track.compilable)
			{
				result = timelinePlayable;
			}
			else if (this.m_PlayableCache.TryGetValue(track, out playable))
			{
				result = playable;
			}
			else if (track.name == "root")
			{
				result = timelinePlayable;
			}
			else
			{
				TrackAsset trackAsset = track.parent as TrackAsset;
				Playable playable2 = (!(trackAsset != null)) ? timelinePlayable : this.CreateTrackPlayable(graph, timelinePlayable, trackAsset, go, createOutputs);
				Playable playable3 = TimelinePlayable.CreatePlayableGraph(graph, track, go, this.m_IntervalTree);
				bool flag = false;
				if (!playable3.IsValid<Playable>())
				{
					throw new InvalidOperationException(string.Concat(new object[]
					{
						track.name,
						"(",
						track.GetType(),
						") did not produce a valid playable. Use the compilable property to indicate whether the track is valid for processing"
					}));
				}
				if (playable2.IsValid<Playable>() && playable3.IsValid<Playable>())
				{
					int inputCount = playable2.GetInputCount<Playable>();
					playable2.SetInputCount(inputCount + 1);
					flag = graph.Connect<Playable, Playable>(playable3, 0, playable2, inputCount);
					playable2.SetInputWeight(inputCount, 1f);
				}
				if (createOutputs && flag)
				{
					this.CreateTrackOutput(graph, track, playable2, playable2.GetInputCount<Playable>() - 1);
				}
				this.CacheTrack(track, playable3, (!flag) ? -1 : (playable2.GetInputCount<Playable>() - 1), playable2);
				result = playable3;
			}
			return result;
		}

		// Token: 0x060000C8 RID: 200 RVA: 0x00006186 File Offset: 0x00004386
		public override void PrepareFrame(Playable playable, FrameData info)
		{
			this.Evaluate(playable, info);
		}

		// Token: 0x060000C9 RID: 201 RVA: 0x00006194 File Offset: 0x00004394
		private void Evaluate(Playable playable, FrameData frameData)
		{
			if (this.m_IntervalTree != null)
			{
				double time = playable.GetTime<Playable>();
				this.m_ActiveBit = ((this.m_ActiveBit != 0) ? 0 : 1);
				this.m_CurrentListOfActiveClips.Clear();
				this.m_IntervalTree.IntersectsWith(DiscreteTime.GetNearestTick(time), this.m_ActiveBit, this.m_CurrentListOfActiveClips);
				for (int i = 0; i < this.m_ActiveClips.Count; i++)
				{
					RuntimeElement runtimeElement = this.m_ActiveClips[i];
					if (runtimeElement.intervalBit != this.m_ActiveBit)
					{
						runtimeElement.enable = false;
					}
				}
				this.m_ActiveClips.Clear();
				for (int j = 0; j < this.m_CurrentListOfActiveClips.Count; j++)
				{
					this.m_CurrentListOfActiveClips[j].EvaluateAt(time, frameData);
					this.m_ActiveClips.Add(this.m_CurrentListOfActiveClips[j]);
				}
				int count = this.m_EvaluateCallbacks.Count;
				for (int k = 0; k < count; k++)
				{
					this.m_EvaluateCallbacks[k].Evaluate();
				}
			}
		}

		// Token: 0x060000CA RID: 202 RVA: 0x000062C9 File Offset: 0x000044C9
		private void CacheTrack(TrackAsset track, Playable playable, int port, Playable parent)
		{
			this.m_PlayableCache[track] = playable;
		}

		// Token: 0x060000CB RID: 203 RVA: 0x000062D9 File Offset: 0x000044D9
		private static void ForAOTCompilationOnly()
		{
			new List<IntervalTree<RuntimeElement>.Entry>();
		}

		// Token: 0x060000CC RID: 204 RVA: 0x000062E2 File Offset: 0x000044E2
		// Note: this type is marked as 'beforefieldinit'.
		static TimelinePlayable()
		{
		}

		// Token: 0x04000058 RID: 88
		private IntervalTree<RuntimeElement> m_IntervalTree = new IntervalTree<RuntimeElement>();

		// Token: 0x04000059 RID: 89
		private List<RuntimeElement> m_ActiveClips = new List<RuntimeElement>();

		// Token: 0x0400005A RID: 90
		private List<RuntimeElement> m_CurrentListOfActiveClips;

		// Token: 0x0400005B RID: 91
		private int m_ActiveBit = 0;

		// Token: 0x0400005C RID: 92
		private List<ITimelineEvaluateCallback> m_EvaluateCallbacks = new List<ITimelineEvaluateCallback>();

		// Token: 0x0400005D RID: 93
		private Dictionary<TrackAsset, Playable> m_PlayableCache = new Dictionary<TrackAsset, Playable>();

		// Token: 0x0400005E RID: 94
		internal static bool muteAudioScrubbing = true;
	}
}
