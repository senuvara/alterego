﻿using System;
using System.Collections.Generic;
using UnityEngine.Playables;

namespace UnityEngine.Timeline
{
	// Token: 0x02000043 RID: 67
	[Obsolete("For best performance use PlayableAsset and PlayableBehaviour.")]
	[Serializable]
	public class BasicPlayableBehaviour : ScriptableObject, IPlayableAsset, IPlayableBehaviour
	{
		// Token: 0x060001EF RID: 495 RVA: 0x0000A192 File Offset: 0x00008392
		public BasicPlayableBehaviour()
		{
		}

		// Token: 0x17000093 RID: 147
		// (get) Token: 0x060001F0 RID: 496 RVA: 0x0000A19C File Offset: 0x0000839C
		public virtual double duration
		{
			get
			{
				return PlayableBinding.DefaultDuration;
			}
		}

		// Token: 0x17000094 RID: 148
		// (get) Token: 0x060001F1 RID: 497 RVA: 0x0000A1B8 File Offset: 0x000083B8
		public virtual IEnumerable<PlayableBinding> outputs
		{
			get
			{
				return PlayableBinding.None;
			}
		}

		// Token: 0x060001F2 RID: 498 RVA: 0x0000A1D2 File Offset: 0x000083D2
		public virtual void OnGraphStart(Playable playable)
		{
		}

		// Token: 0x060001F3 RID: 499 RVA: 0x0000A1D5 File Offset: 0x000083D5
		public virtual void OnGraphStop(Playable playable)
		{
		}

		// Token: 0x060001F4 RID: 500 RVA: 0x0000A1D8 File Offset: 0x000083D8
		public virtual void OnPlayableCreate(Playable playable)
		{
		}

		// Token: 0x060001F5 RID: 501 RVA: 0x0000A1DB File Offset: 0x000083DB
		public virtual void OnPlayableDestroy(Playable playable)
		{
		}

		// Token: 0x060001F6 RID: 502 RVA: 0x0000A1DE File Offset: 0x000083DE
		public virtual void OnBehaviourPlay(Playable playable, FrameData info)
		{
		}

		// Token: 0x060001F7 RID: 503 RVA: 0x0000A1E1 File Offset: 0x000083E1
		public virtual void OnBehaviourPause(Playable playable, FrameData info)
		{
		}

		// Token: 0x060001F8 RID: 504 RVA: 0x0000A1E4 File Offset: 0x000083E4
		public virtual void PrepareFrame(Playable playable, FrameData info)
		{
		}

		// Token: 0x060001F9 RID: 505 RVA: 0x0000A1E7 File Offset: 0x000083E7
		public virtual void ProcessFrame(Playable playable, FrameData info, object playerData)
		{
		}

		// Token: 0x060001FA RID: 506 RVA: 0x0000A1EC File Offset: 0x000083EC
		public virtual Playable CreatePlayable(PlayableGraph graph, GameObject owner)
		{
			return ScriptPlayable<BasicPlayableBehaviour>.Create(graph, this, 0);
		}
	}
}
