﻿using System;

namespace UnityEngine.Timeline
{
	// Token: 0x02000019 RID: 25
	internal interface ITimelineEvaluateCallback
	{
		// Token: 0x060000BF RID: 191
		void Evaluate();
	}
}
