﻿using System;
using UnityEngine.Playables;

namespace UnityEngine.Timeline
{
	// Token: 0x02000044 RID: 68
	public class DirectorControlPlayable : PlayableBehaviour
	{
		// Token: 0x060001FB RID: 507 RVA: 0x0000A20E File Offset: 0x0000840E
		public DirectorControlPlayable()
		{
		}

		// Token: 0x060001FC RID: 508 RVA: 0x0000A22C File Offset: 0x0000842C
		public static ScriptPlayable<DirectorControlPlayable> Create(PlayableGraph graph, PlayableDirector director)
		{
			ScriptPlayable<DirectorControlPlayable> result;
			if (director == null)
			{
				result = ScriptPlayable<DirectorControlPlayable>.Null;
			}
			else
			{
				ScriptPlayable<DirectorControlPlayable> scriptPlayable = ScriptPlayable<DirectorControlPlayable>.Create(graph, 0);
				scriptPlayable.GetBehaviour().director = director;
				result = scriptPlayable;
			}
			return result;
		}

		// Token: 0x060001FD RID: 509 RVA: 0x0000A270 File Offset: 0x00008470
		public override void PrepareFrame(Playable playable, FrameData info)
		{
			if (!(this.director == null) && this.director.isActiveAndEnabled && !(this.director.playableAsset == null))
			{
				this.m_SyncTime |= (info.evaluationType == FrameData.EvaluationType.Evaluate || this.DetectDiscontinuity(playable, info));
				this.SyncSpeed((double)info.effectiveSpeed);
				this.SyncPlayState(playable.GetGraph<Playable>(), playable.GetTime<Playable>());
			}
		}

		// Token: 0x060001FE RID: 510 RVA: 0x0000A300 File Offset: 0x00008500
		public override void OnBehaviourPlay(Playable playable, FrameData info)
		{
			this.m_SyncTime = true;
			if (this.director != null && this.director.playableAsset != null)
			{
				this.m_AssetDuration = this.director.playableAsset.duration;
			}
		}

		// Token: 0x060001FF RID: 511 RVA: 0x0000A354 File Offset: 0x00008554
		public override void OnBehaviourPause(Playable playable, FrameData info)
		{
			if (this.director != null && this.director.playableAsset != null)
			{
				if (info.effectivePlayState == PlayState.Playing)
				{
					this.director.Pause();
				}
				else
				{
					this.director.Stop();
				}
			}
		}

		// Token: 0x06000200 RID: 512 RVA: 0x0000A3B4 File Offset: 0x000085B4
		public override void ProcessFrame(Playable playable, FrameData info, object playerData)
		{
			if (!(this.director == null) && this.director.isActiveAndEnabled && !(this.director.playableAsset == null))
			{
				if (this.m_SyncTime || this.DetectOutOfSync(playable))
				{
					this.UpdateTime(playable);
					this.director.Evaluate();
				}
				this.m_SyncTime = false;
			}
		}

		// Token: 0x06000201 RID: 513 RVA: 0x0000A430 File Offset: 0x00008630
		private void SyncSpeed(double speed)
		{
			if (this.director.playableGraph.IsValid())
			{
				int rootPlayableCount = this.director.playableGraph.GetRootPlayableCount();
				for (int i = 0; i < rootPlayableCount; i++)
				{
					Playable rootPlayable = this.director.playableGraph.GetRootPlayable(i);
					if (rootPlayable.IsValid<Playable>())
					{
						rootPlayable.SetSpeed(speed);
					}
				}
			}
		}

		// Token: 0x06000202 RID: 514 RVA: 0x0000A4B0 File Offset: 0x000086B0
		private void SyncPlayState(PlayableGraph graph, double playableTime)
		{
			bool flag = playableTime >= this.m_AssetDuration && this.director.extrapolationMode == DirectorWrapMode.None;
			if (graph.IsPlaying() && !flag)
			{
				this.director.Play();
			}
			else
			{
				this.director.Pause();
			}
		}

		// Token: 0x06000203 RID: 515 RVA: 0x0000A50C File Offset: 0x0000870C
		private bool DetectDiscontinuity(Playable playable, FrameData info)
		{
			return Math.Abs(playable.GetTime<Playable>() - playable.GetPreviousTime<Playable>() - info.m_DeltaTime * (double)info.m_EffectiveSpeed) > DiscreteTime.tickValue;
		}

		// Token: 0x06000204 RID: 516 RVA: 0x0000A54C File Offset: 0x0000874C
		private bool DetectOutOfSync(Playable playable)
		{
			double num = playable.GetTime<Playable>();
			if (playable.GetTime<Playable>() >= this.m_AssetDuration)
			{
				if (this.director.extrapolationMode == DirectorWrapMode.None)
				{
					return false;
				}
				if (this.director.extrapolationMode == DirectorWrapMode.Hold)
				{
					num = this.m_AssetDuration;
				}
				else if (this.m_AssetDuration > 1.401298464324817E-45)
				{
					num %= this.m_AssetDuration;
				}
			}
			return !Mathf.Approximately((float)num, (float)this.director.time);
		}

		// Token: 0x06000205 RID: 517 RVA: 0x0000A5F0 File Offset: 0x000087F0
		private void UpdateTime(Playable playable)
		{
			double num = Math.Max(0.1, this.director.playableAsset.duration);
			DirectorWrapMode extrapolationMode = this.director.extrapolationMode;
			if (extrapolationMode != DirectorWrapMode.Hold)
			{
				if (extrapolationMode != DirectorWrapMode.Loop)
				{
					if (extrapolationMode == DirectorWrapMode.None)
					{
						this.director.time = playable.GetTime<Playable>();
					}
				}
				else
				{
					this.director.time = Math.Max(0.0, playable.GetTime<Playable>() % num);
				}
			}
			else
			{
				this.director.time = Math.Min(num, Math.Max(0.0, playable.GetTime<Playable>()));
			}
		}

		// Token: 0x040000FD RID: 253
		public PlayableDirector director;

		// Token: 0x040000FE RID: 254
		private bool m_SyncTime = false;

		// Token: 0x040000FF RID: 255
		private double m_AssetDuration = double.MaxValue;
	}
}
