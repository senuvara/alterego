﻿using System;

namespace UnityEngine.Timeline
{
	// Token: 0x02000025 RID: 37
	internal enum AppliedOffsetMode
	{
		// Token: 0x0400009C RID: 156
		NoRootTransform,
		// Token: 0x0400009D RID: 157
		TransformOffset,
		// Token: 0x0400009E RID: 158
		SceneOffset,
		// Token: 0x0400009F RID: 159
		TransformOffsetLegacy,
		// Token: 0x040000A0 RID: 160
		SceneOffsetLegacy,
		// Token: 0x040000A1 RID: 161
		SceneOffsetEditor,
		// Token: 0x040000A2 RID: 162
		SceneOffsetLegacyEditor
	}
}
