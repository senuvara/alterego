﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading;
using UnityEngine.Animations;
using UnityEngine.Playables;

namespace UnityEngine.Timeline
{
	// Token: 0x02000027 RID: 39
	[TrackClipType(typeof(AnimationPlayableAsset), false)]
	[TrackBindingType(typeof(Animator))]
	[SupportsChildTracks(typeof(AnimationTrack), 1)]
	[Serializable]
	public class AnimationTrack : TrackAsset
	{
		// Token: 0x0600014E RID: 334 RVA: 0x000070FC File Offset: 0x000052FC
		public AnimationTrack()
		{
		}

		// Token: 0x1700005A RID: 90
		// (get) Token: 0x0600014F RID: 335 RVA: 0x00007180 File Offset: 0x00005380
		// (set) Token: 0x06000150 RID: 336 RVA: 0x0000719B File Offset: 0x0000539B
		public Vector3 position
		{
			get
			{
				return this.m_Position;
			}
			set
			{
				this.m_Position = value;
			}
		}

		// Token: 0x1700005B RID: 91
		// (get) Token: 0x06000151 RID: 337 RVA: 0x000071A8 File Offset: 0x000053A8
		// (set) Token: 0x06000152 RID: 338 RVA: 0x000071C8 File Offset: 0x000053C8
		public Quaternion rotation
		{
			get
			{
				return Quaternion.Euler(this.m_EulerAngles);
			}
			set
			{
				this.m_EulerAngles = value.eulerAngles;
			}
		}

		// Token: 0x1700005C RID: 92
		// (get) Token: 0x06000153 RID: 339 RVA: 0x000071D8 File Offset: 0x000053D8
		// (set) Token: 0x06000154 RID: 340 RVA: 0x000071F3 File Offset: 0x000053F3
		public Vector3 eulerAngles
		{
			get
			{
				return this.m_EulerAngles;
			}
			set
			{
				this.m_EulerAngles = value;
			}
		}

		// Token: 0x1700005D RID: 93
		// (get) Token: 0x06000155 RID: 341 RVA: 0x00007200 File Offset: 0x00005400
		// (set) Token: 0x06000156 RID: 342 RVA: 0x00007216 File Offset: 0x00005416
		[Obsolete("applyOffset is deprecated. Use trackOffset instead", true)]
		public bool applyOffsets
		{
			get
			{
				return false;
			}
			set
			{
			}
		}

		// Token: 0x1700005E RID: 94
		// (get) Token: 0x06000157 RID: 343 RVA: 0x0000721C File Offset: 0x0000541C
		// (set) Token: 0x06000158 RID: 344 RVA: 0x00007237 File Offset: 0x00005437
		public TrackOffset trackOffset
		{
			get
			{
				return this.m_TrackOffset;
			}
			set
			{
				this.m_TrackOffset = value;
			}
		}

		// Token: 0x1700005F RID: 95
		// (get) Token: 0x06000159 RID: 345 RVA: 0x00007244 File Offset: 0x00005444
		// (set) Token: 0x0600015A RID: 346 RVA: 0x0000725F File Offset: 0x0000545F
		public MatchTargetFields matchTargetFields
		{
			get
			{
				return this.m_MatchTargetFields;
			}
			set
			{
				this.m_MatchTargetFields = (value & MatchTargetFieldConstants.All);
			}
		}

		// Token: 0x17000060 RID: 96
		// (get) Token: 0x0600015B RID: 347 RVA: 0x00007270 File Offset: 0x00005470
		// (set) Token: 0x0600015C RID: 348 RVA: 0x0000728B File Offset: 0x0000548B
		internal bool openClipRemoveOffset
		{
			get
			{
				return this.m_OpenClipRemoveOffset;
			}
			set
			{
				this.m_OpenClipRemoveOffset = value;
			}
		}

		// Token: 0x17000061 RID: 97
		// (get) Token: 0x0600015D RID: 349 RVA: 0x00007298 File Offset: 0x00005498
		private bool compilableIsolated
		{
			get
			{
				return !base.muted && (this.m_Clips.Count > 0 || (base.animClip != null && !base.animClip.empty));
			}
		}

		// Token: 0x17000062 RID: 98
		// (get) Token: 0x0600015E RID: 350 RVA: 0x000072F4 File Offset: 0x000054F4
		// (set) Token: 0x0600015F RID: 351 RVA: 0x0000730F File Offset: 0x0000550F
		public AvatarMask avatarMask
		{
			get
			{
				return this.m_AvatarMask;
			}
			set
			{
				this.m_AvatarMask = value;
			}
		}

		// Token: 0x17000063 RID: 99
		// (get) Token: 0x06000160 RID: 352 RVA: 0x0000731C File Offset: 0x0000551C
		// (set) Token: 0x06000161 RID: 353 RVA: 0x00007337 File Offset: 0x00005537
		public bool applyAvatarMask
		{
			get
			{
				return this.m_ApplyAvatarMask;
			}
			set
			{
				this.m_ApplyAvatarMask = value;
			}
		}

		// Token: 0x17000064 RID: 100
		// (get) Token: 0x06000162 RID: 354 RVA: 0x00007344 File Offset: 0x00005544
		internal override bool compilable
		{
			get
			{
				bool result;
				if (this.compilableIsolated)
				{
					result = true;
				}
				else
				{
					foreach (TrackAsset trackAsset in base.GetChildTracks())
					{
						if (trackAsset.compilable)
						{
							return true;
						}
					}
					result = false;
				}
				return result;
			}
		}

		// Token: 0x17000065 RID: 101
		// (get) Token: 0x06000163 RID: 355 RVA: 0x000073C8 File Offset: 0x000055C8
		public override IEnumerable<PlayableBinding> outputs
		{
			get
			{
				yield return AnimationPlayableBinding.Create(base.name, this);
				yield break;
			}
		}

		// Token: 0x17000066 RID: 102
		// (get) Token: 0x06000164 RID: 356 RVA: 0x000073F4 File Offset: 0x000055F4
		public bool inClipMode
		{
			get
			{
				return base.clips != null && base.clips.Length != 0;
			}
		}

		// Token: 0x17000067 RID: 103
		// (get) Token: 0x06000165 RID: 357 RVA: 0x00007428 File Offset: 0x00005628
		// (set) Token: 0x06000166 RID: 358 RVA: 0x00007443 File Offset: 0x00005643
		public Vector3 openClipOffsetPosition
		{
			get
			{
				return this.m_OpenClipOffsetPosition;
			}
			set
			{
				this.m_OpenClipOffsetPosition = value;
			}
		}

		// Token: 0x17000068 RID: 104
		// (get) Token: 0x06000167 RID: 359 RVA: 0x00007450 File Offset: 0x00005650
		// (set) Token: 0x06000168 RID: 360 RVA: 0x00007470 File Offset: 0x00005670
		public Quaternion openClipOffsetRotation
		{
			get
			{
				return Quaternion.Euler(this.m_OpenClipOffsetEulerAngles);
			}
			set
			{
				this.m_OpenClipOffsetEulerAngles = value.eulerAngles;
			}
		}

		// Token: 0x17000069 RID: 105
		// (get) Token: 0x06000169 RID: 361 RVA: 0x00007480 File Offset: 0x00005680
		// (set) Token: 0x0600016A RID: 362 RVA: 0x0000749B File Offset: 0x0000569B
		public Vector3 openClipOffsetEulerAngles
		{
			get
			{
				return this.m_OpenClipOffsetEulerAngles;
			}
			set
			{
				this.m_OpenClipOffsetEulerAngles = value;
			}
		}

		// Token: 0x1700006A RID: 106
		// (get) Token: 0x0600016B RID: 363 RVA: 0x000074A8 File Offset: 0x000056A8
		// (set) Token: 0x0600016C RID: 364 RVA: 0x000074C3 File Offset: 0x000056C3
		internal bool infiniteClipApplyFootIK
		{
			get
			{
				return this.m_InfiniteClipApplyFootIK;
			}
			set
			{
				this.m_InfiniteClipApplyFootIK = value;
			}
		}

		// Token: 0x1700006B RID: 107
		// (get) Token: 0x0600016D RID: 365 RVA: 0x000074D0 File Offset: 0x000056D0
		// (set) Token: 0x0600016E RID: 366 RVA: 0x000074EB File Offset: 0x000056EB
		internal double openClipTimeOffset
		{
			get
			{
				return this.m_OpenClipTimeOffset;
			}
			set
			{
				this.m_OpenClipTimeOffset = value;
			}
		}

		// Token: 0x1700006C RID: 108
		// (get) Token: 0x0600016F RID: 367 RVA: 0x000074F8 File Offset: 0x000056F8
		// (set) Token: 0x06000170 RID: 368 RVA: 0x00007513 File Offset: 0x00005713
		public TimelineClip.ClipExtrapolation openClipPreExtrapolation
		{
			get
			{
				return this.m_OpenClipPreExtrapolation;
			}
			set
			{
				this.m_OpenClipPreExtrapolation = value;
			}
		}

		// Token: 0x1700006D RID: 109
		// (get) Token: 0x06000171 RID: 369 RVA: 0x00007520 File Offset: 0x00005720
		// (set) Token: 0x06000172 RID: 370 RVA: 0x0000753B File Offset: 0x0000573B
		public TimelineClip.ClipExtrapolation openClipPostExtrapolation
		{
			get
			{
				return this.m_OpenClipPostExtrapolation;
			}
			set
			{
				this.m_OpenClipPostExtrapolation = value;
			}
		}

		// Token: 0x06000173 RID: 371 RVA: 0x00007545 File Offset: 0x00005745
		[ContextMenu("Reset Offsets")]
		private void ResetOffsets()
		{
			this.m_Position = Vector3.zero;
			this.m_EulerAngles = Vector3.zero;
			this.UpdateClipOffsets();
		}

		// Token: 0x06000174 RID: 372 RVA: 0x00007564 File Offset: 0x00005764
		public TimelineClip CreateClip(AnimationClip clip)
		{
			TimelineClip result;
			if (clip == null)
			{
				result = null;
			}
			else
			{
				TimelineClip timelineClip = base.CreateClip<AnimationPlayableAsset>();
				this.AssignAnimationClip(timelineClip, clip);
				result = timelineClip;
			}
			return result;
		}

		// Token: 0x06000175 RID: 373 RVA: 0x0000759C File Offset: 0x0000579C
		protected override void OnCreateClip(TimelineClip clip)
		{
			TimelineClip.ClipExtrapolation clipExtrapolation = TimelineClip.ClipExtrapolation.None;
			if (!base.isSubTrack)
			{
				clipExtrapolation = TimelineClip.ClipExtrapolation.Hold;
			}
			clip.preExtrapolationMode = clipExtrapolation;
			clip.postExtrapolationMode = clipExtrapolation;
		}

		// Token: 0x06000176 RID: 374 RVA: 0x000075C7 File Offset: 0x000057C7
		internal void UpdateClipOffsets()
		{
		}

		// Token: 0x06000177 RID: 375 RVA: 0x000075CC File Offset: 0x000057CC
		private Playable CompileTrackPlayable(PlayableGraph graph, TrackAsset track, GameObject go, IntervalTree<RuntimeElement> tree, AppliedOffsetMode mode)
		{
			AnimationMixerPlayable animationMixerPlayable = AnimationMixerPlayable.Create(graph, track.clips.Length, false);
			for (int i = 0; i < track.clips.Length; i++)
			{
				TimelineClip timelineClip = track.clips[i];
				PlayableAsset playableAsset = timelineClip.asset as PlayableAsset;
				if (!(playableAsset == null))
				{
					AnimationPlayableAsset animationPlayableAsset = playableAsset as AnimationPlayableAsset;
					if (animationPlayableAsset != null)
					{
						animationPlayableAsset.appliedOffsetMode = mode;
					}
					Playable playable = playableAsset.CreatePlayable(graph, go);
					if (playable.IsValid<Playable>())
					{
						RuntimeClip item = new RuntimeClip(timelineClip, playable, animationMixerPlayable);
						tree.Add(item);
						graph.Connect<Playable, AnimationMixerPlayable>(playable, 0, animationMixerPlayable, i);
						animationMixerPlayable.SetInputWeight(i, 0f);
					}
				}
			}
			return this.ApplyTrackOffset(graph, animationMixerPlayable, go, mode);
		}

		// Token: 0x06000178 RID: 376 RVA: 0x000076AC File Offset: 0x000058AC
		internal override Playable OnCreatePlayableGraph(PlayableGraph graph, GameObject go, IntervalTree<RuntimeElement> tree)
		{
			if (base.isSubTrack)
			{
				throw new InvalidOperationException("Nested animation tracks should never be asked to create a graph directly");
			}
			List<AnimationTrack> list = new List<AnimationTrack>();
			if (this.compilableIsolated)
			{
				list.Add(this);
			}
			bool flag = this.AnimatesRootTransform();
			foreach (TrackAsset trackAsset in base.GetChildTracks())
			{
				AnimationTrack animationTrack = trackAsset as AnimationTrack;
				if (animationTrack != null && animationTrack.compilable)
				{
					flag |= animationTrack.AnimatesRootTransform();
					list.Add(animationTrack);
				}
			}
			AppliedOffsetMode offsetMode = this.GetOffsetMode(go, flag);
			AnimationLayerMixerPlayable animationLayerMixerPlayable = AnimationTrack.CreateGroupMixer(graph, go, list.Count);
			for (int i = 0; i < list.Count; i++)
			{
				Playable source = (!list[i].inClipMode) ? list[i].CreateInfiniteTrackPlayable(graph, go, tree, offsetMode) : this.CompileTrackPlayable(graph, list[i], go, tree, offsetMode);
				graph.Connect<Playable, AnimationLayerMixerPlayable>(source, 0, animationLayerMixerPlayable, i);
				animationLayerMixerPlayable.SetInputWeight(i, (float)((!list[i].inClipMode) ? 1 : 0));
				if (list[i].applyAvatarMask && list[i].avatarMask != null)
				{
					animationLayerMixerPlayable.SetLayerMaskFromAvatarMask((uint)i, list[i].avatarMask);
				}
			}
			Playable playable = animationLayerMixerPlayable;
			Playable result;
			if (!this.RequiresMotionXPlayable(offsetMode, go))
			{
				result = playable;
			}
			else
			{
				AnimationMotionXToDeltaPlayable animationMotionXToDeltaPlayable = AnimationMotionXToDeltaPlayable.Create(graph);
				graph.Connect<Playable, AnimationMotionXToDeltaPlayable>(playable, 0, animationMotionXToDeltaPlayable, 0);
				animationMotionXToDeltaPlayable.SetInputWeight(0, 1f);
				animationMotionXToDeltaPlayable.SetAbsoluteMotion(AnimationTrack.UsesAbsoluteMotion(offsetMode));
				result = animationMotionXToDeltaPlayable;
			}
			return result;
		}

		// Token: 0x06000179 RID: 377 RVA: 0x000078B8 File Offset: 0x00005AB8
		private bool RequiresMotionXPlayable(AppliedOffsetMode mode, GameObject gameObject)
		{
			bool result;
			if (mode == AppliedOffsetMode.NoRootTransform)
			{
				result = false;
			}
			else if (mode == AppliedOffsetMode.SceneOffsetLegacy)
			{
				Animator binding = this.GetBinding((!(gameObject != null)) ? null : gameObject.GetComponent<PlayableDirector>());
				result = (binding != null && binding.hasRootMotion);
			}
			else
			{
				result = true;
			}
			return result;
		}

		// Token: 0x0600017A RID: 378 RVA: 0x00007920 File Offset: 0x00005B20
		private static bool UsesAbsoluteMotion(AppliedOffsetMode mode)
		{
			return mode != AppliedOffsetMode.SceneOffset && mode != AppliedOffsetMode.SceneOffsetLegacy;
		}

		// Token: 0x0600017B RID: 379 RVA: 0x00007948 File Offset: 0x00005B48
		private bool HasController(GameObject gameObject)
		{
			Animator binding = this.GetBinding((!(gameObject != null)) ? null : gameObject.GetComponent<PlayableDirector>());
			return binding != null && binding.runtimeAnimatorController != null;
		}

		// Token: 0x0600017C RID: 380 RVA: 0x00007998 File Offset: 0x00005B98
		internal Animator GetBinding(PlayableDirector director)
		{
			Animator result;
			if (director == null)
			{
				result = null;
			}
			else
			{
				Object key = this;
				if (base.isSubTrack)
				{
					key = base.parent;
				}
				Object @object = null;
				if (director != null)
				{
					@object = director.GetGenericBinding(key);
				}
				Animator animator = null;
				if (@object != null)
				{
					animator = (@object as Animator);
					GameObject gameObject = @object as GameObject;
					if (animator == null && gameObject != null)
					{
						animator = gameObject.GetComponent<Animator>();
					}
				}
				result = animator;
			}
			return result;
		}

		// Token: 0x0600017D RID: 381 RVA: 0x00007A2C File Offset: 0x00005C2C
		private static AnimationLayerMixerPlayable CreateGroupMixer(PlayableGraph graph, GameObject go, int inputCount)
		{
			return AnimationLayerMixerPlayable.Create(graph, inputCount);
		}

		// Token: 0x0600017E RID: 382 RVA: 0x00007A48 File Offset: 0x00005C48
		private Playable CreateInfiniteTrackPlayable(PlayableGraph graph, GameObject go, IntervalTree<RuntimeElement> tree, AppliedOffsetMode mode)
		{
			Playable result;
			if (base.animClip == null)
			{
				result = Playable.Null;
			}
			else
			{
				AnimationMixerPlayable animationMixerPlayable = AnimationMixerPlayable.Create(graph, 1, false);
				Playable playable = AnimationPlayableAsset.CreatePlayable(graph, base.animClip, this.m_OpenClipOffsetPosition, this.m_OpenClipOffsetEulerAngles, false, mode, this.infiniteClipApplyFootIK);
				if (playable.IsValid<Playable>())
				{
					tree.Add(new InfiniteRuntimeClip(playable));
					graph.Connect<Playable, AnimationMixerPlayable>(playable, 0, animationMixerPlayable, 0);
					animationMixerPlayable.SetInputWeight(0, 1f);
				}
				result = this.ApplyTrackOffset(graph, animationMixerPlayable, go, mode);
			}
			return result;
		}

		// Token: 0x0600017F RID: 383 RVA: 0x00007AE8 File Offset: 0x00005CE8
		private Playable ApplyTrackOffset(PlayableGraph graph, Playable root, GameObject go, AppliedOffsetMode mode)
		{
			Playable result;
			if (mode == AppliedOffsetMode.SceneOffsetLegacy || mode == AppliedOffsetMode.SceneOffset || mode == AppliedOffsetMode.NoRootTransform || !this.AnimatesRootTransform())
			{
				result = root;
			}
			else
			{
				Vector3 position = this.position;
				Quaternion rotation = this.rotation;
				AnimationOffsetPlayable animationOffsetPlayable = AnimationOffsetPlayable.Create(graph, position, rotation, 1);
				graph.Connect<Playable, AnimationOffsetPlayable>(root, 0, animationOffsetPlayable, 0);
				animationOffsetPlayable.SetInputWeight(0, 1f);
				result = animationOffsetPlayable;
			}
			return result;
		}

		// Token: 0x06000180 RID: 384 RVA: 0x00007B5C File Offset: 0x00005D5C
		internal override void GetEvaluationTime(out double outStart, out double outDuration)
		{
			if (this.inClipMode)
			{
				base.GetEvaluationTime(out outStart, out outDuration);
			}
			else
			{
				outStart = 0.0;
				outDuration = TimelineClip.kMaxTimeValue;
			}
		}

		// Token: 0x06000181 RID: 385 RVA: 0x00007B90 File Offset: 0x00005D90
		internal override void GetSequenceTime(out double outStart, out double outDuration)
		{
			if (this.inClipMode)
			{
				base.GetSequenceTime(out outStart, out outDuration);
			}
			else
			{
				outStart = 0.0;
				outDuration = 0.0;
				if (base.animClip != null)
				{
					outDuration = (double)base.animClip.length;
				}
			}
		}

		// Token: 0x06000182 RID: 386 RVA: 0x00007BF0 File Offset: 0x00005DF0
		private void AssignAnimationClip(TimelineClip clip, AnimationClip animClip)
		{
			if (clip != null && !(animClip == null))
			{
				if (animClip.legacy)
				{
					throw new InvalidOperationException("Legacy Animation Clips are not supported");
				}
				AnimationPlayableAsset animationPlayableAsset = clip.asset as AnimationPlayableAsset;
				if (animationPlayableAsset != null)
				{
					animationPlayableAsset.clip = animClip;
					animationPlayableAsset.name = animClip.name;
					double duration = animationPlayableAsset.duration;
					if (!double.IsInfinity(duration) && duration >= TimelineClip.kMinDuration && duration < TimelineClip.kMaxTimeValue)
					{
						clip.duration = duration;
					}
				}
				clip.displayName = animClip.name;
			}
		}

		// Token: 0x06000183 RID: 387 RVA: 0x00007C93 File Offset: 0x00005E93
		public override void GatherProperties(PlayableDirector director, IPropertyCollector driver)
		{
		}

		// Token: 0x06000184 RID: 388 RVA: 0x00007C98 File Offset: 0x00005E98
		private void GetAnimationClips(List<AnimationClip> animClips)
		{
			foreach (TimelineClip timelineClip in base.clips)
			{
				AnimationPlayableAsset animationPlayableAsset = timelineClip.asset as AnimationPlayableAsset;
				if (animationPlayableAsset != null && animationPlayableAsset.clip != null)
				{
					animClips.Add(animationPlayableAsset.clip);
				}
			}
			if (base.animClip != null)
			{
				animClips.Add(base.animClip);
			}
			foreach (TrackAsset trackAsset in base.GetChildTracks())
			{
				AnimationTrack animationTrack = trackAsset as AnimationTrack;
				if (animationTrack != null)
				{
					animationTrack.GetAnimationClips(animClips);
				}
			}
		}

		// Token: 0x06000185 RID: 389 RVA: 0x00007D88 File Offset: 0x00005F88
		private AppliedOffsetMode GetOffsetMode(GameObject go, bool animatesRootTransform)
		{
			AppliedOffsetMode result;
			if (!animatesRootTransform)
			{
				result = AppliedOffsetMode.NoRootTransform;
			}
			else if (this.m_TrackOffset == TrackOffset.ApplyTransformOffsets)
			{
				result = AppliedOffsetMode.TransformOffset;
			}
			else if (this.m_TrackOffset == TrackOffset.ApplySceneOffsets)
			{
				result = ((!Application.isPlaying) ? AppliedOffsetMode.SceneOffsetEditor : AppliedOffsetMode.SceneOffset);
			}
			else if (this.HasController(go))
			{
				if (!Application.isPlaying)
				{
					result = AppliedOffsetMode.SceneOffsetLegacyEditor;
				}
				else
				{
					result = AppliedOffsetMode.SceneOffsetLegacy;
				}
			}
			else
			{
				result = AppliedOffsetMode.TransformOffsetLegacy;
			}
			return result;
		}

		// Token: 0x06000186 RID: 390 RVA: 0x00007E08 File Offset: 0x00006008
		internal bool AnimatesRootTransform()
		{
			bool result;
			if (AnimationPlayableAsset.HasRootTransforms(base.animClip))
			{
				result = true;
			}
			else
			{
				foreach (TimelineClip timelineClip in base.GetClips())
				{
					AnimationPlayableAsset animationPlayableAsset = timelineClip.asset as AnimationPlayableAsset;
					if (animationPlayableAsset != null && animationPlayableAsset.hasRootTransforms)
					{
						return true;
					}
				}
				result = false;
			}
			return result;
		}

		// Token: 0x06000187 RID: 391 RVA: 0x00007EA8 File Offset: 0x000060A8
		protected internal override void OnUpgradeFromVersion(int oldVersion)
		{
			if (oldVersion < 1)
			{
				AnimationTrack.AnimationTrackUpgrade.ConvertRotationsToEuler(this);
			}
			if (oldVersion < 2)
			{
				AnimationTrack.AnimationTrackUpgrade.ConvertRootMotion(this);
			}
		}

		// Token: 0x040000A7 RID: 167
		[SerializeField]
		private TimelineClip.ClipExtrapolation m_OpenClipPreExtrapolation = TimelineClip.ClipExtrapolation.None;

		// Token: 0x040000A8 RID: 168
		[SerializeField]
		private TimelineClip.ClipExtrapolation m_OpenClipPostExtrapolation = TimelineClip.ClipExtrapolation.None;

		// Token: 0x040000A9 RID: 169
		[SerializeField]
		private Vector3 m_OpenClipOffsetPosition = Vector3.zero;

		// Token: 0x040000AA RID: 170
		[SerializeField]
		private Vector3 m_OpenClipOffsetEulerAngles = Vector3.zero;

		// Token: 0x040000AB RID: 171
		[SerializeField]
		private double m_OpenClipTimeOffset;

		// Token: 0x040000AC RID: 172
		[SerializeField]
		private bool m_OpenClipRemoveOffset;

		// Token: 0x040000AD RID: 173
		[SerializeField]
		private bool m_InfiniteClipApplyFootIK = true;

		// Token: 0x040000AE RID: 174
		[SerializeField]
		private MatchTargetFields m_MatchTargetFields = MatchTargetFieldConstants.All;

		// Token: 0x040000AF RID: 175
		[SerializeField]
		private Vector3 m_Position = Vector3.zero;

		// Token: 0x040000B0 RID: 176
		[SerializeField]
		private Vector3 m_EulerAngles = Vector3.zero;

		// Token: 0x040000B1 RID: 177
		[SerializeField]
		private AvatarMask m_AvatarMask;

		// Token: 0x040000B2 RID: 178
		[SerializeField]
		private bool m_ApplyAvatarMask = true;

		// Token: 0x040000B3 RID: 179
		[SerializeField]
		private TrackOffset m_TrackOffset = TrackOffset.ApplyTransformOffsets;

		// Token: 0x040000B4 RID: 180
		[SerializeField]
		[Obsolete("Use m_OpenClipOffsetEuler Instead", false)]
		[HideInInspector]
		private Quaternion m_OpenClipOffsetRotation = Quaternion.identity;

		// Token: 0x040000B5 RID: 181
		[SerializeField]
		[Obsolete("Use m_RotationEuler Instead", false)]
		[HideInInspector]
		private Quaternion m_Rotation = Quaternion.identity;

		// Token: 0x040000B6 RID: 182
		[SerializeField]
		[Obsolete("Use m_RootTransformOffsetMode", false)]
		[HideInInspector]
		private bool m_ApplyOffsets;

		// Token: 0x0200002A RID: 42
		private static class AnimationTrackUpgrade
		{
			// Token: 0x06000189 RID: 393 RVA: 0x00007EC5 File Offset: 0x000060C5
			public static void ConvertRotationsToEuler(AnimationTrack track)
			{
				track.m_EulerAngles = track.m_Rotation.eulerAngles;
				track.m_OpenClipOffsetEulerAngles = track.m_OpenClipOffsetRotation.eulerAngles;
			}

			// Token: 0x0600018A RID: 394 RVA: 0x00007EEA File Offset: 0x000060EA
			public static void ConvertRootMotion(AnimationTrack track)
			{
				track.m_TrackOffset = TrackOffset.Auto;
				if (!track.m_ApplyOffsets)
				{
					track.m_Position = Vector3.zero;
					track.m_EulerAngles = Vector3.zero;
				}
			}
		}

		// Token: 0x02000055 RID: 85
		[CompilerGenerated]
		private sealed class <>c__Iterator0 : IEnumerable, IEnumerable<PlayableBinding>, IEnumerator, IDisposable, IEnumerator<PlayableBinding>
		{
			// Token: 0x06000268 RID: 616 RVA: 0x00007F17 File Offset: 0x00006117
			[DebuggerHidden]
			public <>c__Iterator0()
			{
			}

			// Token: 0x06000269 RID: 617 RVA: 0x00007F20 File Offset: 0x00006120
			public bool MoveNext()
			{
				uint num = (uint)this.$PC;
				this.$PC = -1;
				switch (num)
				{
				case 0U:
					this.$current = AnimationPlayableBinding.Create(base.name, this);
					if (!this.$disposing)
					{
						this.$PC = 1;
					}
					return true;
				case 1U:
					this.$PC = -1;
					break;
				}
				return false;
			}

			// Token: 0x1700009D RID: 157
			// (get) Token: 0x0600026A RID: 618 RVA: 0x00007F8C File Offset: 0x0000618C
			PlayableBinding IEnumerator<PlayableBinding>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.$current;
				}
			}

			// Token: 0x1700009E RID: 158
			// (get) Token: 0x0600026B RID: 619 RVA: 0x00007FA8 File Offset: 0x000061A8
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.$current;
				}
			}

			// Token: 0x0600026C RID: 620 RVA: 0x00007FC7 File Offset: 0x000061C7
			[DebuggerHidden]
			public void Dispose()
			{
				this.$disposing = true;
				this.$PC = -1;
			}

			// Token: 0x0600026D RID: 621 RVA: 0x00007FD7 File Offset: 0x000061D7
			[DebuggerHidden]
			public void Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x0600026E RID: 622 RVA: 0x00007FE0 File Offset: 0x000061E0
			[DebuggerHidden]
			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.System.Collections.Generic.IEnumerable<UnityEngine.Playables.PlayableBinding>.GetEnumerator();
			}

			// Token: 0x0600026F RID: 623 RVA: 0x00007FFC File Offset: 0x000061FC
			[DebuggerHidden]
			IEnumerator<PlayableBinding> IEnumerable<PlayableBinding>.GetEnumerator()
			{
				if (Interlocked.CompareExchange(ref this.$PC, 0, -2) == -2)
				{
					return this;
				}
				AnimationTrack.<>c__Iterator0 <>c__Iterator = new AnimationTrack.<>c__Iterator0();
				<>c__Iterator.$this = this;
				return <>c__Iterator;
			}

			// Token: 0x04000121 RID: 289
			internal AnimationTrack $this;

			// Token: 0x04000122 RID: 290
			internal PlayableBinding $current;

			// Token: 0x04000123 RID: 291
			internal bool $disposing;

			// Token: 0x04000124 RID: 292
			internal int $PC;
		}
	}
}
