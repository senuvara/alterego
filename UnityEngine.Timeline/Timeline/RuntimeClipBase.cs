﻿using System;

namespace UnityEngine.Timeline
{
	// Token: 0x0200003C RID: 60
	internal abstract class RuntimeClipBase : RuntimeElement
	{
		// Token: 0x060001D0 RID: 464 RVA: 0x00009923 File Offset: 0x00007B23
		protected RuntimeClipBase()
		{
		}

		// Token: 0x17000085 RID: 133
		// (get) Token: 0x060001D1 RID: 465
		public abstract double start { get; }

		// Token: 0x17000086 RID: 134
		// (get) Token: 0x060001D2 RID: 466
		public abstract double duration { get; }

		// Token: 0x17000087 RID: 135
		// (get) Token: 0x060001D3 RID: 467 RVA: 0x0000992C File Offset: 0x00007B2C
		public override long intervalStart
		{
			get
			{
				return DiscreteTime.GetNearestTick(this.start);
			}
		}

		// Token: 0x17000088 RID: 136
		// (get) Token: 0x060001D4 RID: 468 RVA: 0x0000994C File Offset: 0x00007B4C
		public override long intervalEnd
		{
			get
			{
				return DiscreteTime.GetNearestTick(this.start + this.duration);
			}
		}
	}
}
