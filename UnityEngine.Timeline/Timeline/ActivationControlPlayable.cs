﻿using System;
using UnityEngine.Playables;

namespace UnityEngine.Timeline
{
	// Token: 0x02000040 RID: 64
	public class ActivationControlPlayable : PlayableBehaviour
	{
		// Token: 0x060001E8 RID: 488 RVA: 0x00009FF5 File Offset: 0x000081F5
		public ActivationControlPlayable()
		{
		}

		// Token: 0x060001E9 RID: 489 RVA: 0x0000A00C File Offset: 0x0000820C
		public static ScriptPlayable<ActivationControlPlayable> Create(PlayableGraph graph, GameObject gameObject, ActivationControlPlayable.PostPlaybackState postPlaybackState)
		{
			ScriptPlayable<ActivationControlPlayable> result;
			if (gameObject == null)
			{
				result = ScriptPlayable<ActivationControlPlayable>.Null;
			}
			else
			{
				ScriptPlayable<ActivationControlPlayable> scriptPlayable = ScriptPlayable<ActivationControlPlayable>.Create(graph, 0);
				ActivationControlPlayable behaviour = scriptPlayable.GetBehaviour();
				behaviour.gameObject = gameObject;
				behaviour.postPlayback = postPlaybackState;
				result = scriptPlayable;
			}
			return result;
		}

		// Token: 0x060001EA RID: 490 RVA: 0x0000A057 File Offset: 0x00008257
		public override void OnBehaviourPlay(Playable playable, FrameData info)
		{
			if (!(this.gameObject == null))
			{
				this.gameObject.SetActive(true);
			}
		}

		// Token: 0x060001EB RID: 491 RVA: 0x0000A07C File Offset: 0x0000827C
		public override void OnBehaviourPause(Playable playable, FrameData info)
		{
			if (this.gameObject != null && info.effectivePlayState == PlayState.Paused)
			{
				this.gameObject.SetActive(false);
			}
		}

		// Token: 0x060001EC RID: 492 RVA: 0x0000A0AA File Offset: 0x000082AA
		public override void ProcessFrame(Playable playable, FrameData info, object userData)
		{
			if (this.gameObject != null)
			{
				this.gameObject.SetActive(true);
			}
		}

		// Token: 0x060001ED RID: 493 RVA: 0x0000A0CA File Offset: 0x000082CA
		public override void OnGraphStart(Playable playable)
		{
			if (this.gameObject != null)
			{
				if (this.m_InitialState == ActivationControlPlayable.InitialState.Unset)
				{
					this.m_InitialState = ((!this.gameObject.activeSelf) ? ActivationControlPlayable.InitialState.Inactive : ActivationControlPlayable.InitialState.Active);
				}
			}
		}

		// Token: 0x060001EE RID: 494 RVA: 0x0000A108 File Offset: 0x00008308
		public override void OnPlayableDestroy(Playable playable)
		{
			if (!(this.gameObject == null) && this.m_InitialState != ActivationControlPlayable.InitialState.Unset)
			{
				ActivationControlPlayable.PostPlaybackState postPlaybackState = this.postPlayback;
				if (postPlaybackState != ActivationControlPlayable.PostPlaybackState.Active)
				{
					if (postPlaybackState != ActivationControlPlayable.PostPlaybackState.Inactive)
					{
						if (postPlaybackState == ActivationControlPlayable.PostPlaybackState.Revert)
						{
							this.gameObject.SetActive(this.m_InitialState == ActivationControlPlayable.InitialState.Active);
						}
					}
					else
					{
						this.gameObject.SetActive(false);
					}
				}
				else
				{
					this.gameObject.SetActive(true);
				}
			}
		}

		// Token: 0x040000F2 RID: 242
		public GameObject gameObject = null;

		// Token: 0x040000F3 RID: 243
		public ActivationControlPlayable.PostPlaybackState postPlayback = ActivationControlPlayable.PostPlaybackState.Revert;

		// Token: 0x040000F4 RID: 244
		private ActivationControlPlayable.InitialState m_InitialState;

		// Token: 0x02000041 RID: 65
		public enum PostPlaybackState
		{
			// Token: 0x040000F6 RID: 246
			Active,
			// Token: 0x040000F7 RID: 247
			Inactive,
			// Token: 0x040000F8 RID: 248
			Revert
		}

		// Token: 0x02000042 RID: 66
		private enum InitialState
		{
			// Token: 0x040000FA RID: 250
			Unset,
			// Token: 0x040000FB RID: 251
			Active,
			// Token: 0x040000FC RID: 252
			Inactive
		}
	}
}
