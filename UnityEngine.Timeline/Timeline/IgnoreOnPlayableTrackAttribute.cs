﻿using System;

namespace UnityEngine.Timeline
{
	// Token: 0x02000012 RID: 18
	[AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = true)]
	internal class IgnoreOnPlayableTrackAttribute : Attribute
	{
		// Token: 0x06000069 RID: 105 RVA: 0x00004B8F File Offset: 0x00002D8F
		public IgnoreOnPlayableTrackAttribute()
		{
		}
	}
}
