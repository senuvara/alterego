﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading;
using UnityEngine.Animations;
using UnityEngine.Playables;

namespace UnityEngine.Timeline
{
	// Token: 0x02000022 RID: 34
	[NotKeyable]
	[Serializable]
	public class AnimationPlayableAsset : PlayableAsset, ITimelineClipAsset, IPropertyPreview, IClipInitializer, ISerializationCallbackReceiver
	{
		// Token: 0x06000128 RID: 296 RVA: 0x000069B0 File Offset: 0x00004BB0
		public AnimationPlayableAsset()
		{
		}

		// Token: 0x1700004D RID: 77
		// (get) Token: 0x06000129 RID: 297 RVA: 0x00006A04 File Offset: 0x00004C04
		// (set) Token: 0x0600012A RID: 298 RVA: 0x00006A1F File Offset: 0x00004C1F
		public Vector3 position
		{
			get
			{
				return this.m_Position;
			}
			set
			{
				this.m_Position = value;
			}
		}

		// Token: 0x1700004E RID: 78
		// (get) Token: 0x0600012B RID: 299 RVA: 0x00006A2C File Offset: 0x00004C2C
		// (set) Token: 0x0600012C RID: 300 RVA: 0x00006A4C File Offset: 0x00004C4C
		public Quaternion rotation
		{
			get
			{
				return Quaternion.Euler(this.m_EulerAngles);
			}
			set
			{
				this.m_EulerAngles = value.eulerAngles;
			}
		}

		// Token: 0x1700004F RID: 79
		// (get) Token: 0x0600012D RID: 301 RVA: 0x00006A5C File Offset: 0x00004C5C
		// (set) Token: 0x0600012E RID: 302 RVA: 0x00006A77 File Offset: 0x00004C77
		public Vector3 eulerAngles
		{
			get
			{
				return this.m_EulerAngles;
			}
			set
			{
				this.m_EulerAngles = value;
			}
		}

		// Token: 0x17000050 RID: 80
		// (get) Token: 0x0600012F RID: 303 RVA: 0x00006A84 File Offset: 0x00004C84
		// (set) Token: 0x06000130 RID: 304 RVA: 0x00006A9F File Offset: 0x00004C9F
		public bool useTrackMatchFields
		{
			get
			{
				return this.m_UseTrackMatchFields;
			}
			set
			{
				this.m_UseTrackMatchFields = value;
			}
		}

		// Token: 0x17000051 RID: 81
		// (get) Token: 0x06000131 RID: 305 RVA: 0x00006AAC File Offset: 0x00004CAC
		// (set) Token: 0x06000132 RID: 306 RVA: 0x00006AC7 File Offset: 0x00004CC7
		public MatchTargetFields matchTargetFields
		{
			get
			{
				return this.m_MatchTargetFields;
			}
			set
			{
				this.m_MatchTargetFields = value;
			}
		}

		// Token: 0x17000052 RID: 82
		// (get) Token: 0x06000133 RID: 307 RVA: 0x00006AD4 File Offset: 0x00004CD4
		// (set) Token: 0x06000134 RID: 308 RVA: 0x00006AEF File Offset: 0x00004CEF
		public bool removeStartOffset
		{
			get
			{
				return this.m_RemoveStartOffset;
			}
			set
			{
				this.m_RemoveStartOffset = value;
			}
		}

		// Token: 0x17000053 RID: 83
		// (get) Token: 0x06000135 RID: 309 RVA: 0x00006AFC File Offset: 0x00004CFC
		// (set) Token: 0x06000136 RID: 310 RVA: 0x00006B17 File Offset: 0x00004D17
		public bool applyFootIK
		{
			get
			{
				return this.m_ApplyFootIK;
			}
			set
			{
				this.m_ApplyFootIK = value;
			}
		}

		// Token: 0x17000054 RID: 84
		// (get) Token: 0x06000137 RID: 311 RVA: 0x00006B24 File Offset: 0x00004D24
		internal bool hasRootTransforms
		{
			get
			{
				return this.m_Clip != null && AnimationPlayableAsset.HasRootTransforms(this.m_Clip);
			}
		}

		// Token: 0x17000055 RID: 85
		// (get) Token: 0x06000138 RID: 312 RVA: 0x00006B58 File Offset: 0x00004D58
		// (set) Token: 0x06000139 RID: 313 RVA: 0x00006B72 File Offset: 0x00004D72
		internal AppliedOffsetMode appliedOffsetMode
		{
			[CompilerGenerated]
			get
			{
				return this.<appliedOffsetMode>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<appliedOffsetMode>k__BackingField = value;
			}
		}

		// Token: 0x17000056 RID: 86
		// (get) Token: 0x0600013A RID: 314 RVA: 0x00006B7C File Offset: 0x00004D7C
		// (set) Token: 0x0600013B RID: 315 RVA: 0x00006B97 File Offset: 0x00004D97
		public AnimationClip clip
		{
			get
			{
				return this.m_Clip;
			}
			set
			{
				if (value != null)
				{
					base.name = "AnimationPlayableAsset of " + value.name;
				}
				this.m_Clip = value;
			}
		}

		// Token: 0x17000057 RID: 87
		// (get) Token: 0x0600013C RID: 316 RVA: 0x00006BC4 File Offset: 0x00004DC4
		public override double duration
		{
			get
			{
				double result;
				if (this.clip == null || this.clip.empty)
				{
					result = base.duration;
				}
				else
				{
					double num = (double)this.clip.length;
					if (num < 1.401298464324817E-45)
					{
						result = base.duration;
					}
					else
					{
						if (this.clip.frameRate > 0f)
						{
							double num2 = (double)Mathf.Round(this.clip.length * this.clip.frameRate);
							num = num2 / (double)this.clip.frameRate;
						}
						result = num;
					}
				}
				return result;
			}
		}

		// Token: 0x17000058 RID: 88
		// (get) Token: 0x0600013D RID: 317 RVA: 0x00006C74 File Offset: 0x00004E74
		public override IEnumerable<PlayableBinding> outputs
		{
			get
			{
				yield return AnimationPlayableBinding.Create(base.name, this);
				yield break;
			}
		}

		// Token: 0x0600013E RID: 318 RVA: 0x00006CA0 File Offset: 0x00004EA0
		public override Playable CreatePlayable(PlayableGraph graph, GameObject go)
		{
			return AnimationPlayableAsset.CreatePlayable(graph, this.m_Clip, this.position, this.eulerAngles, this.removeStartOffset, this.appliedOffsetMode, this.applyFootIK);
		}

		// Token: 0x0600013F RID: 319 RVA: 0x00006CE4 File Offset: 0x00004EE4
		internal static Playable CreatePlayable(PlayableGraph graph, AnimationClip clip, Vector3 positionOffset, Vector3 eulerOffset, bool removeStartOffset, AppliedOffsetMode mode, bool applyFootIK)
		{
			Playable result;
			if (clip == null || clip.legacy)
			{
				result = Playable.Null;
			}
			else
			{
				AnimationClipPlayable playable = AnimationClipPlayable.Create(graph, clip);
				playable.SetRemoveStartOffset(removeStartOffset);
				playable.SetApplyFootIK(applyFootIK);
				Playable playable2 = playable;
				if (AnimationPlayableAsset.ShouldApplyScaleRemove(mode))
				{
					AnimationRemoveScalePlayable animationRemoveScalePlayable = AnimationRemoveScalePlayable.Create(graph, 1);
					graph.Connect<Playable, AnimationRemoveScalePlayable>(playable2, 0, animationRemoveScalePlayable, 0);
					animationRemoveScalePlayable.SetInputWeight(0, 1f);
					playable2 = animationRemoveScalePlayable;
				}
				if (AnimationPlayableAsset.ShouldApplyOffset(mode, clip))
				{
					AnimationOffsetPlayable animationOffsetPlayable = AnimationOffsetPlayable.Create(graph, positionOffset, Quaternion.Euler(eulerOffset), 1);
					graph.Connect<Playable, AnimationOffsetPlayable>(playable2, 0, animationOffsetPlayable, 0);
					animationOffsetPlayable.SetInputWeight(0, 1f);
					playable2 = animationOffsetPlayable;
				}
				result = playable2;
			}
			return result;
		}

		// Token: 0x06000140 RID: 320 RVA: 0x00006DB4 File Offset: 0x00004FB4
		private static bool ShouldApplyOffset(AppliedOffsetMode mode, AnimationClip clip)
		{
			return mode != AppliedOffsetMode.NoRootTransform && mode != AppliedOffsetMode.SceneOffsetLegacy && AnimationPlayableAsset.HasRootTransforms(clip);
		}

		// Token: 0x06000141 RID: 321 RVA: 0x00006DE4 File Offset: 0x00004FE4
		private static bool ShouldApplyScaleRemove(AppliedOffsetMode mode)
		{
			return mode == AppliedOffsetMode.SceneOffsetLegacyEditor || mode == AppliedOffsetMode.SceneOffsetLegacy || mode == AppliedOffsetMode.TransformOffsetLegacy;
		}

		// Token: 0x17000059 RID: 89
		// (get) Token: 0x06000142 RID: 322 RVA: 0x00006E10 File Offset: 0x00005010
		public ClipCaps clipCaps
		{
			get
			{
				ClipCaps clipCaps = ClipCaps.All;
				if (this.m_Clip == null || !this.m_Clip.isLooping)
				{
					clipCaps &= ~ClipCaps.Looping;
				}
				if (this.m_Clip == null || this.m_Clip.empty)
				{
					clipCaps &= ~ClipCaps.ClipIn;
				}
				return clipCaps;
			}
		}

		// Token: 0x06000143 RID: 323 RVA: 0x00006E74 File Offset: 0x00005074
		public void ResetOffsets()
		{
			this.position = Vector3.zero;
			this.eulerAngles = Vector3.zero;
		}

		// Token: 0x06000144 RID: 324 RVA: 0x00006E8D File Offset: 0x0000508D
		public void GatherProperties(PlayableDirector director, IPropertyCollector driver)
		{
			driver.AddFromClip(this.m_Clip);
		}

		// Token: 0x06000145 RID: 325 RVA: 0x00006E9C File Offset: 0x0000509C
		void IClipInitializer.OnCreate(TimelineClip newClip, TrackAsset owner, IExposedPropertyTable resolver)
		{
			if (this.clip != null && this.clip.legacy)
			{
				this.clip = null;
				Debug.LogError("Legacy Animation Clips are not supported");
			}
		}

		// Token: 0x06000146 RID: 326 RVA: 0x00006ED4 File Offset: 0x000050D4
		internal static bool HasRootTransforms(AnimationClip clip)
		{
			return !(clip == null) && !clip.empty && (clip.hasRootMotion || clip.hasGenericRootTransform || clip.hasMotionCurves || clip.hasRootCurves);
		}

		// Token: 0x06000147 RID: 327 RVA: 0x00006F31 File Offset: 0x00005131
		void ISerializationCallbackReceiver.OnBeforeSerialize()
		{
			this.m_Version = AnimationPlayableAsset.k_LatestVersion;
		}

		// Token: 0x06000148 RID: 328 RVA: 0x00006F3F File Offset: 0x0000513F
		void ISerializationCallbackReceiver.OnAfterDeserialize()
		{
			if (this.m_Version < AnimationPlayableAsset.k_LatestVersion)
			{
				this.OnUpgradeFromVersion(this.m_Version);
			}
		}

		// Token: 0x06000149 RID: 329 RVA: 0x00006F60 File Offset: 0x00005160
		private void OnUpgradeFromVersion(int oldVersion)
		{
			if (oldVersion < 1)
			{
				AnimationPlayableAsset.AnimationPlayableAssetUpgrade.ConvertRotationToEuler(this);
			}
		}

		// Token: 0x0600014A RID: 330 RVA: 0x00006F70 File Offset: 0x00005170
		// Note: this type is marked as 'beforefieldinit'.
		static AnimationPlayableAsset()
		{
		}

		// Token: 0x04000085 RID: 133
		[SerializeField]
		private AnimationClip m_Clip;

		// Token: 0x04000086 RID: 134
		[SerializeField]
		private Vector3 m_Position = Vector3.zero;

		// Token: 0x04000087 RID: 135
		[SerializeField]
		private Vector3 m_EulerAngles = Vector3.zero;

		// Token: 0x04000088 RID: 136
		[SerializeField]
		private bool m_UseTrackMatchFields = true;

		// Token: 0x04000089 RID: 137
		[SerializeField]
		private MatchTargetFields m_MatchTargetFields = MatchTargetFieldConstants.All;

		// Token: 0x0400008A RID: 138
		[SerializeField]
		private bool m_RemoveStartOffset = true;

		// Token: 0x0400008B RID: 139
		[SerializeField]
		private bool m_ApplyFootIK = true;

		// Token: 0x0400008C RID: 140
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private AppliedOffsetMode <appliedOffsetMode>k__BackingField;

		// Token: 0x0400008D RID: 141
		private static readonly int k_LatestVersion = 1;

		// Token: 0x0400008E RID: 142
		[SerializeField]
		[HideInInspector]
		private int m_Version;

		// Token: 0x0400008F RID: 143
		[SerializeField]
		[Obsolete("Use m_RotationEuler Instead", false)]
		[HideInInspector]
		private Quaternion m_Rotation = Quaternion.identity;

		// Token: 0x02000028 RID: 40
		private enum Versions
		{
			// Token: 0x040000B8 RID: 184
			Initial,
			// Token: 0x040000B9 RID: 185
			RotationAsEuler
		}

		// Token: 0x02000029 RID: 41
		private static class AnimationPlayableAssetUpgrade
		{
			// Token: 0x06000188 RID: 392 RVA: 0x00006F78 File Offset: 0x00005178
			public static void ConvertRotationToEuler(AnimationPlayableAsset asset)
			{
				asset.m_EulerAngles = asset.m_Rotation.eulerAngles;
			}
		}

		// Token: 0x02000054 RID: 84
		[CompilerGenerated]
		private sealed class <>c__Iterator0 : IEnumerable, IEnumerable<PlayableBinding>, IEnumerator, IDisposable, IEnumerator<PlayableBinding>
		{
			// Token: 0x06000260 RID: 608 RVA: 0x00006F8C File Offset: 0x0000518C
			[DebuggerHidden]
			public <>c__Iterator0()
			{
			}

			// Token: 0x06000261 RID: 609 RVA: 0x00006F94 File Offset: 0x00005194
			public bool MoveNext()
			{
				uint num = (uint)this.$PC;
				this.$PC = -1;
				switch (num)
				{
				case 0U:
					this.$current = AnimationPlayableBinding.Create(base.name, this);
					if (!this.$disposing)
					{
						this.$PC = 1;
					}
					return true;
				case 1U:
					this.$PC = -1;
					break;
				}
				return false;
			}

			// Token: 0x1700009B RID: 155
			// (get) Token: 0x06000262 RID: 610 RVA: 0x00007000 File Offset: 0x00005200
			PlayableBinding IEnumerator<PlayableBinding>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.$current;
				}
			}

			// Token: 0x1700009C RID: 156
			// (get) Token: 0x06000263 RID: 611 RVA: 0x0000701C File Offset: 0x0000521C
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.$current;
				}
			}

			// Token: 0x06000264 RID: 612 RVA: 0x0000703B File Offset: 0x0000523B
			[DebuggerHidden]
			public void Dispose()
			{
				this.$disposing = true;
				this.$PC = -1;
			}

			// Token: 0x06000265 RID: 613 RVA: 0x0000704B File Offset: 0x0000524B
			[DebuggerHidden]
			public void Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x06000266 RID: 614 RVA: 0x00007054 File Offset: 0x00005254
			[DebuggerHidden]
			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.System.Collections.Generic.IEnumerable<UnityEngine.Playables.PlayableBinding>.GetEnumerator();
			}

			// Token: 0x06000267 RID: 615 RVA: 0x00007070 File Offset: 0x00005270
			[DebuggerHidden]
			IEnumerator<PlayableBinding> IEnumerable<PlayableBinding>.GetEnumerator()
			{
				if (Interlocked.CompareExchange(ref this.$PC, 0, -2) == -2)
				{
					return this;
				}
				AnimationPlayableAsset.<>c__Iterator0 <>c__Iterator = new AnimationPlayableAsset.<>c__Iterator0();
				<>c__Iterator.$this = this;
				return <>c__Iterator;
			}

			// Token: 0x0400011D RID: 285
			internal AnimationPlayableAsset $this;

			// Token: 0x0400011E RID: 286
			internal PlayableBinding $current;

			// Token: 0x0400011F RID: 287
			internal bool $disposing;

			// Token: 0x04000120 RID: 288
			internal int $PC;
		}
	}
}
