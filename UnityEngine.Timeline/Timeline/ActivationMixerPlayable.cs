﻿using System;
using UnityEngine.Playables;

namespace UnityEngine.Timeline
{
	// Token: 0x0200001C RID: 28
	internal class ActivationMixerPlayable : PlayableBehaviour
	{
		// Token: 0x06000113 RID: 275 RVA: 0x000062EA File Offset: 0x000044EA
		public ActivationMixerPlayable()
		{
		}

		// Token: 0x06000114 RID: 276 RVA: 0x000062F4 File Offset: 0x000044F4
		public static ScriptPlayable<ActivationMixerPlayable> Create(PlayableGraph graph, int inputCount)
		{
			return ScriptPlayable<ActivationMixerPlayable>.Create(graph, inputCount);
		}

		// Token: 0x17000049 RID: 73
		// (get) Token: 0x06000115 RID: 277 RVA: 0x00006310 File Offset: 0x00004510
		// (set) Token: 0x06000116 RID: 278 RVA: 0x0000632B File Offset: 0x0000452B
		public ActivationTrack.PostPlaybackState postPlaybackState
		{
			get
			{
				return this.m_PostPlaybackState;
			}
			set
			{
				this.m_PostPlaybackState = value;
			}
		}

		// Token: 0x06000117 RID: 279 RVA: 0x00006338 File Offset: 0x00004538
		public override void OnPlayableDestroy(Playable playable)
		{
			if (!(this.m_BoundGameObject == null))
			{
				switch (this.m_PostPlaybackState)
				{
				case ActivationTrack.PostPlaybackState.Active:
					this.m_BoundGameObject.SetActive(true);
					break;
				case ActivationTrack.PostPlaybackState.Inactive:
					this.m_BoundGameObject.SetActive(false);
					break;
				case ActivationTrack.PostPlaybackState.Revert:
					this.m_BoundGameObject.SetActive(this.m_BoundGameObjectInitialStateIsActive);
					break;
				}
			}
		}

		// Token: 0x06000118 RID: 280 RVA: 0x000063BC File Offset: 0x000045BC
		public override void ProcessFrame(Playable playable, FrameData info, object playerData)
		{
			if (this.m_BoundGameObject == null)
			{
				this.m_BoundGameObject = (playerData as GameObject);
				this.m_BoundGameObjectInitialStateIsActive = (this.m_BoundGameObject != null && this.m_BoundGameObject.activeSelf);
			}
			if (!(this.m_BoundGameObject == null))
			{
				int inputCount = playable.GetInputCount<Playable>();
				bool active = false;
				for (int i = 0; i < inputCount; i++)
				{
					if (playable.GetInputWeight(i) > 0f)
					{
						active = true;
						break;
					}
				}
				this.m_BoundGameObject.SetActive(active);
			}
		}

		// Token: 0x04000072 RID: 114
		private ActivationTrack.PostPlaybackState m_PostPlaybackState;

		// Token: 0x04000073 RID: 115
		private bool m_BoundGameObjectInitialStateIsActive;

		// Token: 0x04000074 RID: 116
		private GameObject m_BoundGameObject;
	}
}
