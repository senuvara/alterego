﻿using System;

namespace UnityEngine.Timeline
{
	// Token: 0x02000026 RID: 38
	internal static class MatchTargetFieldConstants
	{
		// Token: 0x0600014B RID: 331 RVA: 0x000070A4 File Offset: 0x000052A4
		public static bool HasAny(this MatchTargetFields me, MatchTargetFields fields)
		{
			return (me & fields) != MatchTargetFieldConstants.None;
		}

		// Token: 0x0600014C RID: 332 RVA: 0x000070C8 File Offset: 0x000052C8
		public static MatchTargetFields Toggle(this MatchTargetFields me, MatchTargetFields flag)
		{
			return me ^ flag;
		}

		// Token: 0x0600014D RID: 333 RVA: 0x000070E0 File Offset: 0x000052E0
		// Note: this type is marked as 'beforefieldinit'.
		static MatchTargetFieldConstants()
		{
		}

		// Token: 0x040000A3 RID: 163
		public static MatchTargetFields All = MatchTargetFields.PositionX | MatchTargetFields.PositionY | MatchTargetFields.PositionZ | MatchTargetFields.RotationX | MatchTargetFields.RotationY | MatchTargetFields.RotationZ;

		// Token: 0x040000A4 RID: 164
		public static MatchTargetFields None = (MatchTargetFields)0;

		// Token: 0x040000A5 RID: 165
		public static MatchTargetFields Position = MatchTargetFields.PositionX | MatchTargetFields.PositionY | MatchTargetFields.PositionZ;

		// Token: 0x040000A6 RID: 166
		public static MatchTargetFields Rotation = MatchTargetFields.RotationX | MatchTargetFields.RotationY | MatchTargetFields.RotationZ;
	}
}
