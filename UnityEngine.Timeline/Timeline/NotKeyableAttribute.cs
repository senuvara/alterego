﻿using System;

namespace UnityEngine.Timeline
{
	// Token: 0x0200000E RID: 14
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Field)]
	public class NotKeyableAttribute : Attribute
	{
		// Token: 0x06000065 RID: 101 RVA: 0x00004B42 File Offset: 0x00002D42
		public NotKeyableAttribute()
		{
		}
	}
}
