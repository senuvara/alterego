﻿using System;
using System.Collections.Generic;
using UnityEngine.Animations;
using UnityEngine.Playables;

namespace UnityEngine.Timeline
{
	// Token: 0x02000020 RID: 32
	internal class AnimationOutputWeightProcessor : ITimelineEvaluateCallback
	{
		// Token: 0x06000124 RID: 292 RVA: 0x000065A9 File Offset: 0x000047A9
		public AnimationOutputWeightProcessor(AnimationPlayableOutput output)
		{
			this.m_Output = output;
			output.SetWeight(0f);
			this.FindMixers();
		}

		// Token: 0x06000125 RID: 293 RVA: 0x000065D8 File Offset: 0x000047D8
		private void FindMixers()
		{
			this.m_Mixers.Clear();
			this.m_PoseMixer = AnimationMixerPlayable.Null;
			this.m_LayerMixer = AnimationLayerMixerPlayable.Null;
			this.m_MotionXPlayable = AnimationMotionXToDeltaPlayable.Null;
			Playable sourcePlayable = this.m_Output.GetSourcePlayable<AnimationPlayableOutput>();
			int sourceOutputPort = this.m_Output.GetSourceOutputPort<AnimationPlayableOutput>();
			if (sourcePlayable.IsValid<Playable>() && sourceOutputPort >= 0 && sourceOutputPort < sourcePlayable.GetInputCount<Playable>())
			{
				Playable input = sourcePlayable.GetInput(sourceOutputPort);
				Playable playable = input;
				if (playable.IsPlayableOfType<AnimationMotionXToDeltaPlayable>())
				{
					this.m_MotionXPlayable = (AnimationMotionXToDeltaPlayable)playable;
					input = this.m_MotionXPlayable.GetInput(0);
				}
				if (input.IsValid<Playable>() && input.IsPlayableOfType<AnimationMixerPlayable>())
				{
					this.m_PoseMixer = (AnimationMixerPlayable)input;
					Playable input2 = this.m_PoseMixer.GetInput(0);
					if (input2.IsValid<Playable>() && input2.IsPlayableOfType<AnimationLayerMixerPlayable>())
					{
						this.m_LayerMixer = (AnimationLayerMixerPlayable)input2;
					}
				}
				else if (input.IsValid<Playable>() && input.IsPlayableOfType<AnimationLayerMixerPlayable>())
				{
					this.m_LayerMixer = (AnimationLayerMixerPlayable)input;
				}
				if (this.m_LayerMixer.IsValid<AnimationLayerMixerPlayable>())
				{
					int inputCount = this.m_LayerMixer.GetInputCount<AnimationLayerMixerPlayable>();
					for (int i = 0; i < inputCount; i++)
					{
						this.FindMixers(this.m_LayerMixer, i, this.m_LayerMixer.GetInput(i));
					}
				}
			}
		}

		// Token: 0x06000126 RID: 294 RVA: 0x0000675C File Offset: 0x0000495C
		private void FindMixers(Playable parent, int port, Playable node)
		{
			if (node.IsValid<Playable>())
			{
				Type playableType = node.GetPlayableType();
				if (playableType == typeof(AnimationMixerPlayable) || playableType == typeof(AnimationLayerMixerPlayable))
				{
					int inputCount = node.GetInputCount<Playable>();
					for (int i = 0; i < inputCount; i++)
					{
						this.FindMixers(node, i, node.GetInput(i));
					}
					AnimationOutputWeightProcessor.WeightInfo item = new AnimationOutputWeightProcessor.WeightInfo
					{
						parentMixer = parent,
						mixer = node,
						port = port,
						modulate = (playableType == typeof(AnimationLayerMixerPlayable))
					};
					this.m_Mixers.Add(item);
				}
				else
				{
					int inputCount2 = node.GetInputCount<Playable>();
					for (int j = 0; j < inputCount2; j++)
					{
						this.FindMixers(parent, port, node.GetInput(j));
					}
				}
			}
		}

		// Token: 0x06000127 RID: 295 RVA: 0x0000684C File Offset: 0x00004A4C
		public void Evaluate()
		{
			this.m_Output.SetWeight(1f);
			for (int i = 0; i < this.m_Mixers.Count; i++)
			{
				AnimationOutputWeightProcessor.WeightInfo weightInfo = this.m_Mixers[i];
				float num = (!weightInfo.modulate) ? 1f : weightInfo.parentMixer.GetInputWeight(weightInfo.port);
				weightInfo.parentMixer.SetInputWeight(weightInfo.port, num * WeightUtility.NormalizeMixer(weightInfo.mixer));
			}
			float num2 = WeightUtility.NormalizeMixer(this.m_LayerMixer);
			Animator target = this.m_Output.GetTarget();
			if (!(target == null))
			{
				bool flag = !Application.isPlaying && this.m_MotionXPlayable.IsValid<AnimationMotionXToDeltaPlayable>() && this.m_MotionXPlayable.IsAbsoluteMotion();
				if (flag)
				{
					this.m_PoseMixer.SetInputWeight(0, num2);
					this.m_PoseMixer.SetInputWeight(1, 1f - num2);
				}
				else
				{
					if (!this.m_PoseMixer.Equals(AnimationMixerPlayable.Null))
					{
						this.m_PoseMixer.SetInputWeight(0, 1f);
						this.m_PoseMixer.SetInputWeight(1, 0f);
					}
					this.m_Output.SetWeight(num2);
				}
			}
		}

		// Token: 0x0400007C RID: 124
		private AnimationPlayableOutput m_Output;

		// Token: 0x0400007D RID: 125
		private AnimationMotionXToDeltaPlayable m_MotionXPlayable;

		// Token: 0x0400007E RID: 126
		private AnimationMixerPlayable m_PoseMixer;

		// Token: 0x0400007F RID: 127
		private AnimationLayerMixerPlayable m_LayerMixer;

		// Token: 0x04000080 RID: 128
		private readonly List<AnimationOutputWeightProcessor.WeightInfo> m_Mixers = new List<AnimationOutputWeightProcessor.WeightInfo>();

		// Token: 0x02000021 RID: 33
		private struct WeightInfo
		{
			// Token: 0x04000081 RID: 129
			public Playable mixer;

			// Token: 0x04000082 RID: 130
			public Playable parentMixer;

			// Token: 0x04000083 RID: 131
			public int port;

			// Token: 0x04000084 RID: 132
			public bool modulate;
		}
	}
}
