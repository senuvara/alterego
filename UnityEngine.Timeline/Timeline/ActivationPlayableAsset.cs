﻿using System;
using UnityEngine.Playables;

namespace UnityEngine.Timeline
{
	// Token: 0x0200001D RID: 29
	internal class ActivationPlayableAsset : PlayableAsset, ITimelineClipAsset
	{
		// Token: 0x06000119 RID: 281 RVA: 0x00006466 File Offset: 0x00004666
		public ActivationPlayableAsset()
		{
		}

		// Token: 0x1700004A RID: 74
		// (get) Token: 0x0600011A RID: 282 RVA: 0x00006470 File Offset: 0x00004670
		public ClipCaps clipCaps
		{
			get
			{
				return ClipCaps.None;
			}
		}

		// Token: 0x0600011B RID: 283 RVA: 0x00006488 File Offset: 0x00004688
		public override Playable CreatePlayable(PlayableGraph graph, GameObject go)
		{
			return Playable.Create(graph, 0);
		}
	}
}
