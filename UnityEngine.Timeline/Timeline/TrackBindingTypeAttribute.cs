﻿using System;

namespace UnityEngine.Timeline
{
	// Token: 0x02000010 RID: 16
	[AttributeUsage(AttributeTargets.Class)]
	public class TrackBindingTypeAttribute : Attribute
	{
		// Token: 0x06000066 RID: 102 RVA: 0x00004B4A File Offset: 0x00002D4A
		public TrackBindingTypeAttribute(Type type)
		{
			this.type = type;
			this.flags = TrackBindingFlags.AllowCreateComponent;
		}

		// Token: 0x06000067 RID: 103 RVA: 0x00004B61 File Offset: 0x00002D61
		public TrackBindingTypeAttribute(Type type, TrackBindingFlags flags)
		{
			this.type = type;
			this.flags = flags;
		}

		// Token: 0x0400002C RID: 44
		public readonly Type type;

		// Token: 0x0400002D RID: 45
		public readonly TrackBindingFlags flags;
	}
}
