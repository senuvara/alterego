﻿using System;

namespace UnityEngine.Timeline
{
	// Token: 0x0200000D RID: 13
	[AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
	public class TrackClipTypeAttribute : Attribute
	{
		// Token: 0x06000063 RID: 99 RVA: 0x00004B18 File Offset: 0x00002D18
		public TrackClipTypeAttribute(Type clipClass)
		{
			this.inspectedType = clipClass;
			this.allowAutoCreate = true;
		}

		// Token: 0x06000064 RID: 100 RVA: 0x00004B2F File Offset: 0x00002D2F
		public TrackClipTypeAttribute(Type clipClass, bool allowAutoCreate)
		{
			this.inspectedType = clipClass;
		}

		// Token: 0x04000026 RID: 38
		public readonly Type inspectedType;

		// Token: 0x04000027 RID: 39
		public readonly bool allowAutoCreate;
	}
}
