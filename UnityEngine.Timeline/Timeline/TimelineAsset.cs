﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading;
using UnityEngine.Playables;

namespace UnityEngine.Timeline
{
	// Token: 0x02000008 RID: 8
	[Serializable]
	public class TimelineAsset : PlayableAsset, ISerializationCallbackReceiver, ITimelineClipAsset, IPropertyPreview
	{
		// Token: 0x06000036 RID: 54 RVA: 0x00003B8E File Offset: 0x00001D8E
		public TimelineAsset()
		{
		}

		// Token: 0x17000004 RID: 4
		// (get) Token: 0x06000037 RID: 55 RVA: 0x00003BA4 File Offset: 0x00001DA4
		public TimelineAsset.EditorSettings editorSettings
		{
			get
			{
				return this.m_EditorSettings;
			}
		}

		// Token: 0x17000005 RID: 5
		// (get) Token: 0x06000038 RID: 56 RVA: 0x00003BC0 File Offset: 0x00001DC0
		public override double duration
		{
			get
			{
				double result;
				if (this.m_DurationMode == TimelineAsset.DurationMode.BasedOnClips)
				{
					result = this.CalculateDuration();
				}
				else
				{
					result = this.m_FixedDuration;
				}
				return result;
			}
		}

		// Token: 0x17000006 RID: 6
		// (get) Token: 0x06000039 RID: 57 RVA: 0x00003BF4 File Offset: 0x00001DF4
		// (set) Token: 0x0600003A RID: 58 RVA: 0x00003C41 File Offset: 0x00001E41
		public double fixedDuration
		{
			get
			{
				DiscreteTime lhs = (DiscreteTime)this.m_FixedDuration;
				double result;
				if (lhs <= 0)
				{
					result = 0.0;
				}
				else
				{
					result = (double)lhs.OneTickBefore();
				}
				return result;
			}
			set
			{
				this.m_FixedDuration = Math.Max(0.0, value);
			}
		}

		// Token: 0x17000007 RID: 7
		// (get) Token: 0x0600003B RID: 59 RVA: 0x00003C5C File Offset: 0x00001E5C
		// (set) Token: 0x0600003C RID: 60 RVA: 0x00003C77 File Offset: 0x00001E77
		public TimelineAsset.DurationMode durationMode
		{
			get
			{
				return this.m_DurationMode;
			}
			set
			{
				this.m_DurationMode = value;
			}
		}

		// Token: 0x17000008 RID: 8
		// (get) Token: 0x0600003D RID: 61 RVA: 0x00003C84 File Offset: 0x00001E84
		public override IEnumerable<PlayableBinding> outputs
		{
			get
			{
				foreach (TrackAsset outputTracks in this.GetOutputTracks())
				{
					foreach (PlayableBinding output in outputTracks.outputs)
					{
						yield return output;
					}
				}
				yield break;
			}
		}

		// Token: 0x17000009 RID: 9
		// (get) Token: 0x0600003E RID: 62 RVA: 0x00003CB0 File Offset: 0x00001EB0
		public ClipCaps clipCaps
		{
			get
			{
				ClipCaps clipCaps = ClipCaps.All;
				foreach (TrackAsset trackAsset in this.GetRootTracks())
				{
					foreach (TimelineClip timelineClip in trackAsset.clips)
					{
						clipCaps &= timelineClip.clipCaps;
					}
				}
				return clipCaps;
			}
		}

		// Token: 0x1700000A RID: 10
		// (get) Token: 0x0600003F RID: 63 RVA: 0x00003D44 File Offset: 0x00001F44
		public int outputTrackCount
		{
			get
			{
				this.GetOutputTracks();
				return this.m_CacheOutputTracks.Length;
			}
		}

		// Token: 0x1700000B RID: 11
		// (get) Token: 0x06000040 RID: 64 RVA: 0x00003D68 File Offset: 0x00001F68
		public int rootTrackCount
		{
			get
			{
				this.UpdateRootTrackCache();
				return this.m_CacheRootTracks.Count;
			}
		}

		// Token: 0x06000041 RID: 65 RVA: 0x00003D8E File Offset: 0x00001F8E
		private void OnValidate()
		{
			this.editorSettings.fps = TimelineAsset.GetValidFramerate(this.editorSettings.fps);
		}

		// Token: 0x06000042 RID: 66 RVA: 0x00003DAC File Offset: 0x00001FAC
		private static float GetValidFramerate(float framerate)
		{
			return Mathf.Clamp(framerate, TimelineAsset.EditorSettings.kMinFps, TimelineAsset.EditorSettings.kMaxFps);
		}

		// Token: 0x06000043 RID: 67 RVA: 0x00003DD4 File Offset: 0x00001FD4
		public TrackAsset GetRootTrack(int index)
		{
			this.UpdateRootTrackCache();
			return this.m_CacheRootTracks[index];
		}

		// Token: 0x06000044 RID: 68 RVA: 0x00003DFC File Offset: 0x00001FFC
		public IEnumerable<TrackAsset> GetRootTracks()
		{
			this.UpdateRootTrackCache();
			return this.m_CacheRootTracks;
		}

		// Token: 0x06000045 RID: 69 RVA: 0x00003E20 File Offset: 0x00002020
		public TrackAsset GetOutputTrack(int index)
		{
			this.UpdateOutputTrackCache();
			return this.m_CacheOutputTracks[index];
		}

		// Token: 0x06000046 RID: 70 RVA: 0x00003E44 File Offset: 0x00002044
		public IEnumerable<TrackAsset> GetOutputTracks()
		{
			this.UpdateOutputTrackCache();
			return this.m_CacheOutputTracks;
		}

		// Token: 0x06000047 RID: 71 RVA: 0x00003E68 File Offset: 0x00002068
		private void UpdateRootTrackCache()
		{
			if (this.m_CacheRootTracks == null)
			{
				if (this.m_Tracks == null)
				{
					this.m_CacheRootTracks = new List<TrackAsset>();
				}
				else
				{
					this.m_CacheRootTracks = new List<TrackAsset>(this.m_Tracks.Count);
					for (int i = 0; i < this.m_Tracks.Count; i++)
					{
						TrackAsset trackAsset = this.m_Tracks[i] as TrackAsset;
						if (trackAsset != null)
						{
							this.m_CacheRootTracks.Add(trackAsset);
						}
					}
				}
			}
		}

		// Token: 0x06000048 RID: 72 RVA: 0x00003F00 File Offset: 0x00002100
		private void UpdateOutputTrackCache()
		{
			if (this.m_CacheOutputTracks == null)
			{
				List<TrackAsset> list = new List<TrackAsset>();
				foreach (TrackAsset trackAsset in this.flattenedTracks)
				{
					if (trackAsset != null && trackAsset.GetType() != typeof(GroupTrack) && !trackAsset.isSubTrack)
					{
						list.Add(trackAsset);
					}
				}
				this.m_CacheOutputTracks = list.ToArray();
			}
		}

		// Token: 0x1700000C RID: 12
		// (get) Token: 0x06000049 RID: 73 RVA: 0x00003FA8 File Offset: 0x000021A8
		internal IEnumerable<TrackAsset> flattenedTracks
		{
			get
			{
				if (this.m_CacheFlattenedTracks == null)
				{
					this.m_CacheFlattenedTracks = new List<TrackAsset>(this.m_Tracks.Count * 2);
					this.UpdateRootTrackCache();
					this.m_CacheFlattenedTracks.AddRange(this.m_CacheRootTracks);
					for (int i = 0; i < this.m_CacheRootTracks.Count; i++)
					{
						TimelineAsset.AddSubTracksRecursive(this.m_CacheRootTracks[i], ref this.m_CacheFlattenedTracks);
					}
				}
				return this.m_CacheFlattenedTracks;
			}
		}

		// Token: 0x1700000D RID: 13
		// (get) Token: 0x0600004A RID: 74 RVA: 0x00004034 File Offset: 0x00002234
		internal List<ScriptableObject> trackObjects
		{
			get
			{
				return this.m_Tracks;
			}
		}

		// Token: 0x0600004B RID: 75 RVA: 0x0000404F File Offset: 0x0000224F
		internal void AddTrackInternal(TrackAsset track)
		{
			this.m_Tracks.Add(track);
			track.parent = this;
			this.Invalidate();
		}

		// Token: 0x0600004C RID: 76 RVA: 0x0000406C File Offset: 0x0000226C
		internal void RemoveTrack(TrackAsset track)
		{
			this.m_Tracks.Remove(track);
			this.Invalidate();
			TrackAsset trackAsset = track.parent as TrackAsset;
			if (trackAsset != null)
			{
				trackAsset.RemoveSubTrack(track);
			}
		}

		// Token: 0x0600004D RID: 77 RVA: 0x000040B0 File Offset: 0x000022B0
		internal int GenerateNewId()
		{
			this.m_NextId++;
			return base.GetInstanceID().GetHashCode().CombineHash(this.m_NextId.GetHashCode());
		}

		// Token: 0x0600004E RID: 78 RVA: 0x00004100 File Offset: 0x00002300
		public override Playable CreatePlayable(PlayableGraph graph, GameObject go)
		{
			bool autoRebalance = false;
			bool createOutputs = graph.GetPlayableCount() == 0;
			ScriptPlayable<TimelinePlayable> playable = TimelinePlayable.Create(graph, this.GetOutputTracks(), go, autoRebalance, createOutputs);
			return (!playable.IsValid<ScriptPlayable<TimelinePlayable>>()) ? Playable.Null : playable;
		}

		// Token: 0x0600004F RID: 79 RVA: 0x0000414D File Offset: 0x0000234D
		void ISerializationCallbackReceiver.OnBeforeSerialize()
		{
			this.m_Version = 0;
		}

		// Token: 0x06000050 RID: 80 RVA: 0x00004157 File Offset: 0x00002357
		void ISerializationCallbackReceiver.OnAfterDeserialize()
		{
			this.Invalidate();
			if (this.m_Version < 0)
			{
				this.UpgradeToLatestVersion();
			}
		}

		// Token: 0x06000051 RID: 81 RVA: 0x00004174 File Offset: 0x00002374
		private void __internalAwake()
		{
			if (this.m_Tracks == null)
			{
				this.m_Tracks = new List<ScriptableObject>();
			}
			for (int i = this.m_Tracks.Count - 1; i >= 0; i--)
			{
				TrackAsset trackAsset = this.m_Tracks[i] as TrackAsset;
				if (trackAsset != null)
				{
					trackAsset.parent = this;
				}
			}
		}

		// Token: 0x06000052 RID: 82 RVA: 0x000041E0 File Offset: 0x000023E0
		public void GatherProperties(PlayableDirector director, IPropertyCollector driver)
		{
			IEnumerable<TrackAsset> outputTracks = this.GetOutputTracks();
			foreach (TrackAsset trackAsset in outputTracks)
			{
				trackAsset.GatherProperties(director, driver);
			}
		}

		// Token: 0x06000053 RID: 83 RVA: 0x00004240 File Offset: 0x00002440
		internal void Invalidate()
		{
			this.m_CacheRootTracks = null;
			this.m_CacheOutputTracks = null;
			this.m_CacheFlattenedTracks = null;
		}

		// Token: 0x06000054 RID: 84 RVA: 0x00004258 File Offset: 0x00002458
		private double CalculateDuration()
		{
			IEnumerable<TrackAsset> flattenedTracks = this.flattenedTracks;
			DiscreteTime lhs = new DiscreteTime(0);
			foreach (TrackAsset trackAsset in flattenedTracks)
			{
				if (!trackAsset.muted)
				{
					lhs = DiscreteTime.Max(lhs, (DiscreteTime)trackAsset.end);
				}
			}
			double result;
			if (lhs <= 0)
			{
				result = 0.0;
			}
			else
			{
				result = (double)lhs.OneTickBefore();
			}
			return result;
		}

		// Token: 0x06000055 RID: 85 RVA: 0x00004310 File Offset: 0x00002510
		private static void AddSubTracksRecursive(TrackAsset track, ref List<TrackAsset> allTracks)
		{
			if (!(track == null))
			{
				allTracks.AddRange(track.GetChildTracks());
				foreach (TrackAsset track2 in track.GetChildTracks())
				{
					TimelineAsset.AddSubTracksRecursive(track2, ref allTracks);
				}
			}
		}

		// Token: 0x06000056 RID: 86 RVA: 0x0000438C File Offset: 0x0000258C
		public TrackAsset CreateTrack(Type type, TrackAsset parent, string name)
		{
			if (parent != null && parent.timelineAsset != this)
			{
				throw new InvalidOperationException("Addtrack cannot parent to a track not in the Timeline");
			}
			if (!typeof(TrackAsset).IsAssignableFrom(type))
			{
				throw new InvalidOperationException("Supplied type must be a track asset");
			}
			if (parent != null)
			{
				if (!TimelineCreateUtilities.ValidateParentTrack(parent, type))
				{
					throw new InvalidOperationException("Cannot assign a child of type " + type.Name + "to a parent of type " + parent.GetType().Name);
				}
			}
			PlayableAsset masterAsset = (!(parent != null)) ? this : parent;
			string text = name;
			if (string.IsNullOrEmpty(text))
			{
				text = type.Name;
			}
			string text2;
			if (parent != null)
			{
				text2 = TimelineCreateUtilities.GenerateUniqueActorName(parent.subTracksObjects, text);
			}
			else
			{
				text2 = TimelineCreateUtilities.GenerateUniqueActorName(this.trackObjects, text);
			}
			TrackAsset trackAsset = this.AllocateTrack(parent, text2, type);
			if (trackAsset != null)
			{
				trackAsset.name = text2;
				TimelineCreateUtilities.SaveAssetIntoObject(trackAsset, masterAsset);
			}
			return trackAsset;
		}

		// Token: 0x06000057 RID: 87 RVA: 0x000044AC File Offset: 0x000026AC
		public T CreateTrack<T>(TrackAsset parent, string name) where T : TrackAsset, new()
		{
			return (T)((object)this.CreateTrack(typeof(T), parent, name));
		}

		// Token: 0x06000058 RID: 88 RVA: 0x000044D8 File Offset: 0x000026D8
		public bool DeleteClip(TimelineClip clip)
		{
			bool result;
			if (clip == null || clip.parentTrack == null)
			{
				result = false;
			}
			else if (this != clip.parentTrack.timelineAsset)
			{
				Debug.LogError("Cannot delete a clip from this timeline");
				result = false;
			}
			else
			{
				if (clip.curves != null)
				{
					TimelineUndo.PushDestroyUndo(this, clip.parentTrack, clip.curves, "Delete Curves");
				}
				if (clip.asset != null)
				{
					this.DeleteRecordedAnimation(clip);
					TimelineUndo.PushDestroyUndo(this, clip.parentTrack, clip.asset, "Delete Clip Asset");
				}
				TrackAsset parentTrack = clip.parentTrack;
				parentTrack.RemoveClip(clip);
				parentTrack.CalculateExtrapolationTimes();
				result = true;
			}
			return result;
		}

		// Token: 0x06000059 RID: 89 RVA: 0x000045A8 File Offset: 0x000027A8
		public bool DeleteTrack(TrackAsset track)
		{
			bool result;
			if (track.timelineAsset != this)
			{
				result = false;
			}
			else
			{
				TrackAsset x = track.parent as TrackAsset;
				if (x != null)
				{
				}
				IEnumerable<TrackAsset> childTracks = track.GetChildTracks();
				foreach (TrackAsset track2 in childTracks)
				{
					this.DeleteTrack(track2);
				}
				this.DeleteRecordingClip(track);
				List<TimelineClip> list = new List<TimelineClip>(track.clips);
				foreach (TimelineClip clip in list)
				{
					this.DeleteClip(clip);
				}
				this.RemoveTrack(track);
				TimelineUndo.PushDestroyUndo(this, this, track, "Delete Track");
				result = true;
			}
			return result;
		}

		// Token: 0x0600005A RID: 90 RVA: 0x000046BC File Offset: 0x000028BC
		internal TrackAsset AllocateTrack(TrackAsset trackAssetParent, string trackName, Type trackType)
		{
			if (trackAssetParent != null && trackAssetParent.timelineAsset != this)
			{
				throw new InvalidOperationException("Addtrack cannot parent to a track not in the Timeline");
			}
			if (!typeof(TrackAsset).IsAssignableFrom(trackType))
			{
				throw new InvalidOperationException("Supplied type must be a track asset");
			}
			TrackAsset trackAsset = (TrackAsset)ScriptableObject.CreateInstance(trackType);
			trackAsset.name = trackName;
			if (trackAssetParent != null)
			{
				trackAssetParent.AddChild(trackAsset);
			}
			else
			{
				this.AddTrackInternal(trackAsset);
			}
			return trackAsset;
		}

		// Token: 0x0600005B RID: 91 RVA: 0x0000474C File Offset: 0x0000294C
		private void DeleteRecordingClip(TrackAsset track)
		{
			AnimationTrack animationTrack = track as AnimationTrack;
			if (!(animationTrack == null) && !(animationTrack.animClip == null))
			{
				TimelineUndo.PushDestroyUndo(this, track, animationTrack.animClip, "Delete Track");
			}
		}

		// Token: 0x0600005C RID: 92 RVA: 0x00004798 File Offset: 0x00002998
		private void DeleteRecordedAnimation(TimelineClip clip)
		{
			if (clip != null)
			{
				if (clip.curves != null)
				{
					TimelineUndo.PushDestroyUndo(this, clip.parentTrack, clip.curves, "Delete Parameters");
				}
				if (clip.recordable)
				{
					AnimationPlayableAsset animationPlayableAsset = clip.asset as AnimationPlayableAsset;
					if (!(animationPlayableAsset == null) && !(animationPlayableAsset.clip == null))
					{
						TimelineUndo.PushDestroyUndo(this, animationPlayableAsset, animationPlayableAsset.clip, "Delete Recording");
					}
				}
			}
		}

		// Token: 0x0600005D RID: 93 RVA: 0x00004829 File Offset: 0x00002A29
		private void UpgradeToLatestVersion()
		{
		}

		// Token: 0x0400000C RID: 12
		[HideInInspector]
		[SerializeField]
		private int m_NextId;

		// Token: 0x0400000D RID: 13
		[HideInInspector]
		[SerializeField]
		private List<ScriptableObject> m_Tracks;

		// Token: 0x0400000E RID: 14
		[HideInInspector]
		[SerializeField]
		private double m_FixedDuration;

		// Token: 0x0400000F RID: 15
		[HideInInspector]
		[NonSerialized]
		private TrackAsset[] m_CacheOutputTracks;

		// Token: 0x04000010 RID: 16
		[HideInInspector]
		[NonSerialized]
		private List<TrackAsset> m_CacheRootTracks;

		// Token: 0x04000011 RID: 17
		[HideInInspector]
		[NonSerialized]
		private List<TrackAsset> m_CacheFlattenedTracks;

		// Token: 0x04000012 RID: 18
		[HideInInspector]
		[SerializeField]
		private TimelineAsset.EditorSettings m_EditorSettings = new TimelineAsset.EditorSettings();

		// Token: 0x04000013 RID: 19
		[SerializeField]
		private TimelineAsset.DurationMode m_DurationMode;

		// Token: 0x04000014 RID: 20
		private const int k_LatestVersion = 0;

		// Token: 0x04000015 RID: 21
		[SerializeField]
		[HideInInspector]
		private int m_Version;

		// Token: 0x02000009 RID: 9
		[Obsolete("MediaType has been deprecated. It is no longer required, and will be removed in a future release.", false)]
		public enum MediaType
		{
			// Token: 0x04000017 RID: 23
			Animation,
			// Token: 0x04000018 RID: 24
			Audio,
			// Token: 0x04000019 RID: 25
			Texture,
			// Token: 0x0400001A RID: 26
			[Obsolete("Use Texture MediaType instead. (UnityUpgradable) -> UnityEngine.Timeline.TimelineAsset/MediaType.Texture", false)]
			Video = 2,
			// Token: 0x0400001B RID: 27
			Script,
			// Token: 0x0400001C RID: 28
			Hybrid,
			// Token: 0x0400001D RID: 29
			Group
		}

		// Token: 0x0200000B RID: 11
		public enum DurationMode
		{
			// Token: 0x04000020 RID: 32
			BasedOnClips,
			// Token: 0x04000021 RID: 33
			FixedLength
		}

		// Token: 0x0200000C RID: 12
		[Serializable]
		public class EditorSettings
		{
			// Token: 0x0600005F RID: 95 RVA: 0x0000482C File Offset: 0x00002A2C
			public EditorSettings()
			{
			}

			// Token: 0x1700000E RID: 14
			// (get) Token: 0x06000060 RID: 96 RVA: 0x00004840 File Offset: 0x00002A40
			// (set) Token: 0x06000061 RID: 97 RVA: 0x0000485B File Offset: 0x00002A5B
			public float fps
			{
				get
				{
					return this.m_Framerate;
				}
				set
				{
					this.m_Framerate = TimelineAsset.GetValidFramerate(value);
				}
			}

			// Token: 0x06000062 RID: 98 RVA: 0x0000486A File Offset: 0x00002A6A
			// Note: this type is marked as 'beforefieldinit'.
			static EditorSettings()
			{
			}

			// Token: 0x04000022 RID: 34
			internal static readonly float kMinFps = (float)TimeUtility.kFrameRateEpsilon;

			// Token: 0x04000023 RID: 35
			internal static readonly float kMaxFps = 1000f;

			// Token: 0x04000024 RID: 36
			internal static readonly float kDefaultFps = 60f;

			// Token: 0x04000025 RID: 37
			[HideInInspector]
			[SerializeField]
			private float m_Framerate = TimelineAsset.EditorSettings.kDefaultFps;
		}

		// Token: 0x0200002D RID: 45
		private enum Versions
		{
			// Token: 0x040000BE RID: 190
			Initial
		}

		// Token: 0x0200002E RID: 46
		private static class TimelineAssetUpgrade
		{
		}

		// Token: 0x02000052 RID: 82
		[CompilerGenerated]
		private sealed class <>c__Iterator0 : IEnumerable, IEnumerable<PlayableBinding>, IEnumerator, IDisposable, IEnumerator<PlayableBinding>
		{
			// Token: 0x06000250 RID: 592 RVA: 0x0000488B File Offset: 0x00002A8B
			[DebuggerHidden]
			public <>c__Iterator0()
			{
			}

			// Token: 0x06000251 RID: 593 RVA: 0x00004894 File Offset: 0x00002A94
			public bool MoveNext()
			{
				uint num = (uint)this.$PC;
				this.$PC = -1;
				bool flag = false;
				switch (num)
				{
				case 0U:
					enumerator = base.GetOutputTracks().GetEnumerator();
					num = 4294967293U;
					break;
				case 1U:
					break;
				default:
					return false;
				}
				try
				{
					switch (num)
					{
					case 1U:
						Block_4:
						try
						{
							switch (num)
							{
							}
							if (enumerator2.MoveNext())
							{
								output = enumerator2.Current;
								this.$current = output;
								if (!this.$disposing)
								{
									this.$PC = 1;
								}
								flag = true;
								return true;
							}
						}
						finally
						{
							if (!flag)
							{
								if (enumerator2 != null)
								{
									enumerator2.Dispose();
								}
							}
						}
						break;
					}
					if (enumerator.MoveNext())
					{
						outputTracks = enumerator.Current;
						enumerator2 = outputTracks.outputs.GetEnumerator();
						num = 4294967293U;
						goto Block_4;
					}
				}
				finally
				{
					if (!flag)
					{
						if (enumerator != null)
						{
							enumerator.Dispose();
						}
					}
				}
				this.$PC = -1;
				return false;
			}

			// Token: 0x17000097 RID: 151
			// (get) Token: 0x06000252 RID: 594 RVA: 0x000049E8 File Offset: 0x00002BE8
			PlayableBinding IEnumerator<PlayableBinding>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.$current;
				}
			}

			// Token: 0x17000098 RID: 152
			// (get) Token: 0x06000253 RID: 595 RVA: 0x00004A04 File Offset: 0x00002C04
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.$current;
				}
			}

			// Token: 0x06000254 RID: 596 RVA: 0x00004A24 File Offset: 0x00002C24
			[DebuggerHidden]
			public void Dispose()
			{
				uint num = (uint)this.$PC;
				this.$disposing = true;
				this.$PC = -1;
				switch (num)
				{
				case 1U:
					try
					{
						try
						{
						}
						finally
						{
							if (enumerator2 != null)
							{
								enumerator2.Dispose();
							}
						}
					}
					finally
					{
						if (enumerator != null)
						{
							enumerator.Dispose();
						}
					}
					break;
				}
			}

			// Token: 0x06000255 RID: 597 RVA: 0x00004AB0 File Offset: 0x00002CB0
			[DebuggerHidden]
			public void Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x06000256 RID: 598 RVA: 0x00004AB8 File Offset: 0x00002CB8
			[DebuggerHidden]
			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.System.Collections.Generic.IEnumerable<UnityEngine.Playables.PlayableBinding>.GetEnumerator();
			}

			// Token: 0x06000257 RID: 599 RVA: 0x00004AD4 File Offset: 0x00002CD4
			[DebuggerHidden]
			IEnumerator<PlayableBinding> IEnumerable<PlayableBinding>.GetEnumerator()
			{
				if (Interlocked.CompareExchange(ref this.$PC, 0, -2) == -2)
				{
					return this;
				}
				TimelineAsset.<>c__Iterator0 <>c__Iterator = new TimelineAsset.<>c__Iterator0();
				<>c__Iterator.$this = this;
				return <>c__Iterator;
			}

			// Token: 0x0400010F RID: 271
			internal IEnumerator<TrackAsset> $locvar0;

			// Token: 0x04000110 RID: 272
			internal TrackAsset <outputTracks>__1;

			// Token: 0x04000111 RID: 273
			internal IEnumerator<PlayableBinding> $locvar1;

			// Token: 0x04000112 RID: 274
			internal PlayableBinding <output>__2;

			// Token: 0x04000113 RID: 275
			internal TimelineAsset $this;

			// Token: 0x04000114 RID: 276
			internal PlayableBinding $current;

			// Token: 0x04000115 RID: 277
			internal bool $disposing;

			// Token: 0x04000116 RID: 278
			internal int $PC;
		}
	}
}
