﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine.Playables;

namespace UnityEngine.Timeline
{
	// Token: 0x0200003D RID: 61
	internal abstract class RuntimeElement : IInterval
	{
		// Token: 0x060001D5 RID: 469 RVA: 0x00009260 File Offset: 0x00007460
		protected RuntimeElement()
		{
		}

		// Token: 0x17000089 RID: 137
		// (get) Token: 0x060001D6 RID: 470
		public abstract long intervalStart { get; }

		// Token: 0x1700008A RID: 138
		// (get) Token: 0x060001D7 RID: 471
		public abstract long intervalEnd { get; }

		// Token: 0x1700008B RID: 139
		// (get) Token: 0x060001D8 RID: 472 RVA: 0x00009268 File Offset: 0x00007468
		// (set) Token: 0x060001D9 RID: 473 RVA: 0x00009282 File Offset: 0x00007482
		public int intervalBit
		{
			[CompilerGenerated]
			get
			{
				return this.<intervalBit>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<intervalBit>k__BackingField = value;
			}
		}

		// Token: 0x1700008C RID: 140
		// (set) Token: 0x060001DA RID: 474
		public abstract bool enable { set; }

		// Token: 0x060001DB RID: 475
		public abstract void EvaluateAt(double localTime, FrameData frameData);

		// Token: 0x040000EB RID: 235
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private int <intervalBit>k__BackingField;
	}
}
