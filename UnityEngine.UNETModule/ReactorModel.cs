﻿using System;

namespace UnityEngine.Networking
{
	// Token: 0x02000027 RID: 39
	public enum ReactorModel
	{
		// Token: 0x04000093 RID: 147
		SelectReactor,
		// Token: 0x04000094 RID: 148
		FixRateReactor
	}
}
