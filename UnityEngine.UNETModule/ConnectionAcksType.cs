﻿using System;

namespace UnityEngine.Networking
{
	// Token: 0x02000028 RID: 40
	public enum ConnectionAcksType
	{
		// Token: 0x04000096 RID: 150
		Acks32 = 1,
		// Token: 0x04000097 RID: 151
		Acks64,
		// Token: 0x04000098 RID: 152
		Acks96,
		// Token: 0x04000099 RID: 153
		Acks128
	}
}
