﻿using System;

namespace UnityEngine.Networking
{
	// Token: 0x0200002C RID: 44
	[Obsolete("The UNET transport will be removed in the future as soon a replacement is ready.")]
	[Serializable]
	public class GlobalConfig
	{
		// Token: 0x06000192 RID: 402 RVA: 0x00005A2C File Offset: 0x00003C2C
		public GlobalConfig()
		{
			this.m_ThreadAwakeTimeout = 1U;
			this.m_ReactorModel = ReactorModel.SelectReactor;
			this.m_ReactorMaximumReceivedMessages = 1024;
			this.m_ReactorMaximumSentMessages = 1024;
			this.m_MaxPacketSize = 2000;
			this.m_MaxHosts = 16;
			this.m_ThreadPoolSize = 1;
			this.m_MinTimerTimeout = 1U;
			this.m_MaxTimerTimeout = 12000U;
			this.m_MinNetSimulatorTimeout = 1U;
			this.m_MaxNetSimulatorTimeout = 12000U;
			this.m_ConnectionReadyForSend = null;
			this.m_NetworkEventAvailable = null;
		}

		// Token: 0x17000061 RID: 97
		// (get) Token: 0x06000193 RID: 403 RVA: 0x00005AB0 File Offset: 0x00003CB0
		// (set) Token: 0x06000194 RID: 404 RVA: 0x00005ACB File Offset: 0x00003CCB
		public uint ThreadAwakeTimeout
		{
			get
			{
				return this.m_ThreadAwakeTimeout;
			}
			set
			{
				if (value == 0U)
				{
					throw new ArgumentOutOfRangeException("Minimal thread awake timeout should be > 0");
				}
				this.m_ThreadAwakeTimeout = value;
			}
		}

		// Token: 0x17000062 RID: 98
		// (get) Token: 0x06000195 RID: 405 RVA: 0x00005AE8 File Offset: 0x00003CE8
		// (set) Token: 0x06000196 RID: 406 RVA: 0x00005B03 File Offset: 0x00003D03
		public ReactorModel ReactorModel
		{
			get
			{
				return this.m_ReactorModel;
			}
			set
			{
				this.m_ReactorModel = value;
			}
		}

		// Token: 0x17000063 RID: 99
		// (get) Token: 0x06000197 RID: 407 RVA: 0x00005B10 File Offset: 0x00003D10
		// (set) Token: 0x06000198 RID: 408 RVA: 0x00005B2B File Offset: 0x00003D2B
		public ushort ReactorMaximumReceivedMessages
		{
			get
			{
				return this.m_ReactorMaximumReceivedMessages;
			}
			set
			{
				this.m_ReactorMaximumReceivedMessages = value;
			}
		}

		// Token: 0x17000064 RID: 100
		// (get) Token: 0x06000199 RID: 409 RVA: 0x00005B38 File Offset: 0x00003D38
		// (set) Token: 0x0600019A RID: 410 RVA: 0x00005B53 File Offset: 0x00003D53
		public ushort ReactorMaximumSentMessages
		{
			get
			{
				return this.m_ReactorMaximumSentMessages;
			}
			set
			{
				this.m_ReactorMaximumSentMessages = value;
			}
		}

		// Token: 0x17000065 RID: 101
		// (get) Token: 0x0600019B RID: 411 RVA: 0x00005B60 File Offset: 0x00003D60
		// (set) Token: 0x0600019C RID: 412 RVA: 0x00005B7B File Offset: 0x00003D7B
		public ushort MaxPacketSize
		{
			get
			{
				return this.m_MaxPacketSize;
			}
			set
			{
				this.m_MaxPacketSize = value;
			}
		}

		// Token: 0x17000066 RID: 102
		// (get) Token: 0x0600019D RID: 413 RVA: 0x00005B88 File Offset: 0x00003D88
		// (set) Token: 0x0600019E RID: 414 RVA: 0x00005BA4 File Offset: 0x00003DA4
		public ushort MaxHosts
		{
			get
			{
				return this.m_MaxHosts;
			}
			set
			{
				if (value == 0)
				{
					throw new ArgumentOutOfRangeException("MaxHosts", "Maximum hosts number should be > 0");
				}
				if (value > 128)
				{
					throw new ArgumentOutOfRangeException("MaxHosts", "Maximum hosts number should be <= " + 128.ToString());
				}
				this.m_MaxHosts = value;
			}
		}

		// Token: 0x17000067 RID: 103
		// (get) Token: 0x0600019F RID: 415 RVA: 0x00005C04 File Offset: 0x00003E04
		// (set) Token: 0x060001A0 RID: 416 RVA: 0x00005C1F File Offset: 0x00003E1F
		public byte ThreadPoolSize
		{
			get
			{
				return this.m_ThreadPoolSize;
			}
			set
			{
				this.m_ThreadPoolSize = value;
			}
		}

		// Token: 0x17000068 RID: 104
		// (get) Token: 0x060001A1 RID: 417 RVA: 0x00005C2C File Offset: 0x00003E2C
		// (set) Token: 0x060001A2 RID: 418 RVA: 0x00005C47 File Offset: 0x00003E47
		public uint MinTimerTimeout
		{
			get
			{
				return this.m_MinTimerTimeout;
			}
			set
			{
				if (value > this.MaxTimerTimeout)
				{
					throw new ArgumentOutOfRangeException("MinTimerTimeout should be < MaxTimerTimeout");
				}
				if (value == 0U)
				{
					throw new ArgumentOutOfRangeException("MinTimerTimeout should be > 0");
				}
				this.m_MinTimerTimeout = value;
			}
		}

		// Token: 0x17000069 RID: 105
		// (get) Token: 0x060001A3 RID: 419 RVA: 0x00005C7C File Offset: 0x00003E7C
		// (set) Token: 0x060001A4 RID: 420 RVA: 0x00005C98 File Offset: 0x00003E98
		public uint MaxTimerTimeout
		{
			get
			{
				return this.m_MaxTimerTimeout;
			}
			set
			{
				if (value == 0U)
				{
					throw new ArgumentOutOfRangeException("MaxTimerTimeout should be > 0");
				}
				if (value > 12000U)
				{
					throw new ArgumentOutOfRangeException("MaxTimerTimeout should be <=" + 12000U.ToString());
				}
				this.m_MaxTimerTimeout = value;
			}
		}

		// Token: 0x1700006A RID: 106
		// (get) Token: 0x060001A5 RID: 421 RVA: 0x00005CEC File Offset: 0x00003EEC
		// (set) Token: 0x060001A6 RID: 422 RVA: 0x00005D07 File Offset: 0x00003F07
		public uint MinNetSimulatorTimeout
		{
			get
			{
				return this.m_MinNetSimulatorTimeout;
			}
			set
			{
				if (value > this.MaxNetSimulatorTimeout)
				{
					throw new ArgumentOutOfRangeException("MinNetSimulatorTimeout should be < MaxTimerTimeout");
				}
				if (value == 0U)
				{
					throw new ArgumentOutOfRangeException("MinNetSimulatorTimeout should be > 0");
				}
				this.m_MinNetSimulatorTimeout = value;
			}
		}

		// Token: 0x1700006B RID: 107
		// (get) Token: 0x060001A7 RID: 423 RVA: 0x00005D3C File Offset: 0x00003F3C
		// (set) Token: 0x060001A8 RID: 424 RVA: 0x00005D58 File Offset: 0x00003F58
		public uint MaxNetSimulatorTimeout
		{
			get
			{
				return this.m_MaxNetSimulatorTimeout;
			}
			set
			{
				if (value == 0U)
				{
					throw new ArgumentOutOfRangeException("MaxNetSimulatorTimeout should be > 0");
				}
				if (value > 12000U)
				{
					throw new ArgumentOutOfRangeException("MaxNetSimulatorTimeout should be <=" + 12000U.ToString());
				}
				this.m_MaxNetSimulatorTimeout = value;
			}
		}

		// Token: 0x1700006C RID: 108
		// (get) Token: 0x060001A9 RID: 425 RVA: 0x00005DAC File Offset: 0x00003FAC
		// (set) Token: 0x060001AA RID: 426 RVA: 0x00005DC7 File Offset: 0x00003FC7
		public Action<int> NetworkEventAvailable
		{
			get
			{
				return this.m_NetworkEventAvailable;
			}
			set
			{
				this.m_NetworkEventAvailable = value;
			}
		}

		// Token: 0x1700006D RID: 109
		// (get) Token: 0x060001AB RID: 427 RVA: 0x00005DD4 File Offset: 0x00003FD4
		// (set) Token: 0x060001AC RID: 428 RVA: 0x00005DEF File Offset: 0x00003FEF
		public Action<int, int> ConnectionReadyForSend
		{
			get
			{
				return this.m_ConnectionReadyForSend;
			}
			set
			{
				this.m_ConnectionReadyForSend = value;
			}
		}

		// Token: 0x040000BF RID: 191
		private const uint g_MaxTimerTimeout = 12000U;

		// Token: 0x040000C0 RID: 192
		private const uint g_MaxNetSimulatorTimeout = 12000U;

		// Token: 0x040000C1 RID: 193
		private const ushort g_MaxHosts = 128;

		// Token: 0x040000C2 RID: 194
		[SerializeField]
		private uint m_ThreadAwakeTimeout;

		// Token: 0x040000C3 RID: 195
		[SerializeField]
		private ReactorModel m_ReactorModel;

		// Token: 0x040000C4 RID: 196
		[SerializeField]
		private ushort m_ReactorMaximumReceivedMessages;

		// Token: 0x040000C5 RID: 197
		[SerializeField]
		private ushort m_ReactorMaximumSentMessages;

		// Token: 0x040000C6 RID: 198
		[SerializeField]
		private ushort m_MaxPacketSize;

		// Token: 0x040000C7 RID: 199
		[SerializeField]
		private ushort m_MaxHosts;

		// Token: 0x040000C8 RID: 200
		[SerializeField]
		private byte m_ThreadPoolSize;

		// Token: 0x040000C9 RID: 201
		[SerializeField]
		private uint m_MinTimerTimeout;

		// Token: 0x040000CA RID: 202
		[SerializeField]
		private uint m_MaxTimerTimeout;

		// Token: 0x040000CB RID: 203
		[SerializeField]
		private uint m_MinNetSimulatorTimeout;

		// Token: 0x040000CC RID: 204
		[SerializeField]
		private uint m_MaxNetSimulatorTimeout;

		// Token: 0x040000CD RID: 205
		[SerializeField]
		private Action<int, int> m_ConnectionReadyForSend;

		// Token: 0x040000CE RID: 206
		[SerializeField]
		private Action<int> m_NetworkEventAvailable;
	}
}
