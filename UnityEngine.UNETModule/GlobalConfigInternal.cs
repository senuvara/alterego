﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;

namespace UnityEngine.Networking
{
	// Token: 0x02000031 RID: 49
	[NativeHeader("Runtime/Networking/UNETConfiguration.h")]
	[NativeConditional("ENABLE_NETWORK && ENABLE_UNET", true)]
	internal class GlobalConfigInternal : IDisposable
	{
		// Token: 0x060001E2 RID: 482 RVA: 0x000062F4 File Offset: 0x000044F4
		public GlobalConfigInternal(GlobalConfig config)
		{
			if (config == null)
			{
				throw new NullReferenceException("config is not defined");
			}
			this.m_Ptr = GlobalConfigInternal.InternalCreate();
			this.ThreadAwakeTimeout = config.ThreadAwakeTimeout;
			this.ReactorModel = (byte)config.ReactorModel;
			this.ReactorMaximumReceivedMessages = config.ReactorMaximumReceivedMessages;
			this.ReactorMaximumSentMessages = config.ReactorMaximumSentMessages;
			this.MaxPacketSize = config.MaxPacketSize;
			this.MaxHosts = config.MaxHosts;
			if (config.ThreadPoolSize == 0 || config.ThreadPoolSize > 254)
			{
				throw new ArgumentOutOfRangeException("Worker thread pool size should be >= 1 && < 254 (for server only)");
			}
			byte threadPoolSize = config.ThreadPoolSize;
			if (config.ThreadPoolSize > 1)
			{
				Debug.LogWarning("Worker thread pool size can be > 1 only for server platforms: Win, OSX or Linux");
				threadPoolSize = 1;
			}
			this.ThreadPoolSize = threadPoolSize;
			this.MinTimerTimeout = config.MinTimerTimeout;
			this.MaxTimerTimeout = config.MaxTimerTimeout;
			this.MinNetSimulatorTimeout = config.MinNetSimulatorTimeout;
			this.MaxNetSimulatorTimeout = config.MaxNetSimulatorTimeout;
		}

		// Token: 0x060001E3 RID: 483 RVA: 0x000063EB File Offset: 0x000045EB
		protected virtual void Dispose(bool disposing)
		{
			if (this.m_Ptr != IntPtr.Zero)
			{
				GlobalConfigInternal.InternalDestroy(this.m_Ptr);
				this.m_Ptr = IntPtr.Zero;
			}
		}

		// Token: 0x060001E4 RID: 484 RVA: 0x0000641C File Offset: 0x0000461C
		~GlobalConfigInternal()
		{
			this.Dispose(false);
		}

		// Token: 0x060001E5 RID: 485 RVA: 0x000063EB File Offset: 0x000045EB
		public void Dispose()
		{
			if (this.m_Ptr != IntPtr.Zero)
			{
				GlobalConfigInternal.InternalDestroy(this.m_Ptr);
				this.m_Ptr = IntPtr.Zero;
			}
		}

		// Token: 0x060001E6 RID: 486
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern IntPtr InternalCreate();

		// Token: 0x060001E7 RID: 487
		[NativeMethod(IsThreadSafe = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void InternalDestroy(IntPtr ptr);

		// Token: 0x17000087 RID: 135
		// (set) Token: 0x060001E8 RID: 488
		[NativeProperty("m_ThreadAwakeTimeout", TargetType.Field)]
		private extern uint ThreadAwakeTimeout { [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000088 RID: 136
		// (set) Token: 0x060001E9 RID: 489
		[NativeProperty("m_ReactorModel", TargetType.Field)]
		private extern byte ReactorModel { [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000089 RID: 137
		// (set) Token: 0x060001EA RID: 490
		[NativeProperty("m_ReactorMaximumReceivedMessages", TargetType.Field)]
		private extern ushort ReactorMaximumReceivedMessages { [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700008A RID: 138
		// (set) Token: 0x060001EB RID: 491
		[NativeProperty("m_ReactorMaximumSentMessages", TargetType.Field)]
		private extern ushort ReactorMaximumSentMessages { [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700008B RID: 139
		// (set) Token: 0x060001EC RID: 492
		[NativeProperty("m_MaxPacketSize", TargetType.Field)]
		private extern ushort MaxPacketSize { [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700008C RID: 140
		// (set) Token: 0x060001ED RID: 493
		[NativeProperty("m_MaxHosts", TargetType.Field)]
		private extern ushort MaxHosts { [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700008D RID: 141
		// (set) Token: 0x060001EE RID: 494
		[NativeProperty("m_ThreadPoolSize", TargetType.Field)]
		private extern byte ThreadPoolSize { [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700008E RID: 142
		// (set) Token: 0x060001EF RID: 495
		[NativeProperty("m_MinTimerTimeout", TargetType.Field)]
		private extern uint MinTimerTimeout { [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700008F RID: 143
		// (set) Token: 0x060001F0 RID: 496
		[NativeProperty("m_MaxTimerTimeout", TargetType.Field)]
		private extern uint MaxTimerTimeout { [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000090 RID: 144
		// (set) Token: 0x060001F1 RID: 497
		[NativeProperty("m_MinNetSimulatorTimeout", TargetType.Field)]
		private extern uint MinNetSimulatorTimeout { [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000091 RID: 145
		// (set) Token: 0x060001F2 RID: 498
		[NativeProperty("m_MaxNetSimulatorTimeout", TargetType.Field)]
		private extern uint MaxNetSimulatorTimeout { [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x040000D7 RID: 215
		public IntPtr m_Ptr;
	}
}
