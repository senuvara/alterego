﻿using System;

namespace UnityEngine.Networking
{
	// Token: 0x02000026 RID: 38
	public enum NetworkError
	{
		// Token: 0x04000085 RID: 133
		Ok,
		// Token: 0x04000086 RID: 134
		WrongHost,
		// Token: 0x04000087 RID: 135
		WrongConnection,
		// Token: 0x04000088 RID: 136
		WrongChannel,
		// Token: 0x04000089 RID: 137
		NoResources,
		// Token: 0x0400008A RID: 138
		BadMessage,
		// Token: 0x0400008B RID: 139
		Timeout,
		// Token: 0x0400008C RID: 140
		MessageToLong,
		// Token: 0x0400008D RID: 141
		WrongOperation,
		// Token: 0x0400008E RID: 142
		VersionMismatch,
		// Token: 0x0400008F RID: 143
		CRCMismatch,
		// Token: 0x04000090 RID: 144
		DNSFailure,
		// Token: 0x04000091 RID: 145
		UsageError
	}
}
