﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine.Networking.Types;

namespace UnityEngine.Networking.Match
{
	// Token: 0x0200000A RID: 10
	internal class DestroyMatchRequest : Request
	{
		// Token: 0x0600003C RID: 60 RVA: 0x00002251 File Offset: 0x00000451
		public DestroyMatchRequest()
		{
		}

		// Token: 0x17000014 RID: 20
		// (get) Token: 0x0600003D RID: 61 RVA: 0x00002744 File Offset: 0x00000944
		// (set) Token: 0x0600003E RID: 62 RVA: 0x0000275E File Offset: 0x0000095E
		public NetworkID networkId
		{
			[CompilerGenerated]
			get
			{
				return this.<networkId>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<networkId>k__BackingField = value;
			}
		}

		// Token: 0x0600003F RID: 63 RVA: 0x00002768 File Offset: 0x00000968
		public override string ToString()
		{
			return UnityString.Format("[{0}]-networkId:0x{1}", new object[]
			{
				base.ToString(),
				this.networkId.ToString("X")
			});
		}

		// Token: 0x06000040 RID: 64 RVA: 0x000027B4 File Offset: 0x000009B4
		public override bool IsValid()
		{
			return base.IsValid() && this.networkId != NetworkID.Invalid;
		}

		// Token: 0x04000025 RID: 37
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private NetworkID <networkId>k__BackingField;
	}
}
