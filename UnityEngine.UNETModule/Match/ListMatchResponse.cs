﻿using System;
using System.Collections.Generic;

namespace UnityEngine.Networking.Match
{
	// Token: 0x02000010 RID: 16
	[Serializable]
	internal class ListMatchResponse : BasicResponse
	{
		// Token: 0x06000061 RID: 97 RVA: 0x00002CD9 File Offset: 0x00000ED9
		public ListMatchResponse()
		{
			this.matches = new List<MatchDesc>();
		}

		// Token: 0x06000062 RID: 98 RVA: 0x00002CED File Offset: 0x00000EED
		public ListMatchResponse(List<MatchDesc> otherMatches)
		{
			this.matches = otherMatches;
		}

		// Token: 0x06000063 RID: 99 RVA: 0x00002D00 File Offset: 0x00000F00
		public override string ToString()
		{
			return UnityString.Format("[{0}]-matches.Count:{1}", new object[]
			{
				base.ToString(),
				(this.matches != null) ? this.matches.Count : 0
			});
		}

		// Token: 0x04000040 RID: 64
		public List<MatchDesc> matches;
	}
}
