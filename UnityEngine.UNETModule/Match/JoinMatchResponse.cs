﻿using System;
using UnityEngine.Networking.Types;

namespace UnityEngine.Networking.Match
{
	// Token: 0x02000009 RID: 9
	[Serializable]
	internal class JoinMatchResponse : BasicResponse
	{
		// Token: 0x0600003A RID: 58 RVA: 0x000026A0 File Offset: 0x000008A0
		public JoinMatchResponse()
		{
		}

		// Token: 0x0600003B RID: 59 RVA: 0x000026B0 File Offset: 0x000008B0
		public override string ToString()
		{
			return UnityString.Format("[{0}]-address:{1},port:{2},networkId:0x{3},accessTokenString.IsEmpty:{4},nodeId:0x{5},usingRelay:{6}", new object[]
			{
				base.ToString(),
				this.address,
				this.port,
				this.networkId.ToString("X"),
				string.IsNullOrEmpty(this.accessTokenString),
				this.nodeId.ToString("X"),
				this.usingRelay
			});
		}

		// Token: 0x0400001E RID: 30
		public string address;

		// Token: 0x0400001F RID: 31
		public int port;

		// Token: 0x04000020 RID: 32
		public int domain = 0;

		// Token: 0x04000021 RID: 33
		public ulong networkId;

		// Token: 0x04000022 RID: 34
		public string accessTokenString;

		// Token: 0x04000023 RID: 35
		public NodeID nodeId;

		// Token: 0x04000024 RID: 36
		public bool usingRelay;
	}
}
