﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace UnityEngine.Networking.Match
{
	// Token: 0x02000006 RID: 6
	internal class CreateMatchRequest : Request
	{
		// Token: 0x06000018 RID: 24 RVA: 0x00002251 File Offset: 0x00000451
		public CreateMatchRequest()
		{
		}

		// Token: 0x17000007 RID: 7
		// (get) Token: 0x06000019 RID: 25 RVA: 0x0000225C File Offset: 0x0000045C
		// (set) Token: 0x0600001A RID: 26 RVA: 0x00002276 File Offset: 0x00000476
		public string name
		{
			[CompilerGenerated]
			get
			{
				return this.<name>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<name>k__BackingField = value;
			}
		}

		// Token: 0x17000008 RID: 8
		// (get) Token: 0x0600001B RID: 27 RVA: 0x00002280 File Offset: 0x00000480
		// (set) Token: 0x0600001C RID: 28 RVA: 0x0000229A File Offset: 0x0000049A
		public uint size
		{
			[CompilerGenerated]
			get
			{
				return this.<size>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<size>k__BackingField = value;
			}
		}

		// Token: 0x17000009 RID: 9
		// (get) Token: 0x0600001D RID: 29 RVA: 0x000022A4 File Offset: 0x000004A4
		// (set) Token: 0x0600001E RID: 30 RVA: 0x000022BE File Offset: 0x000004BE
		public string publicAddress
		{
			[CompilerGenerated]
			get
			{
				return this.<publicAddress>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<publicAddress>k__BackingField = value;
			}
		}

		// Token: 0x1700000A RID: 10
		// (get) Token: 0x0600001F RID: 31 RVA: 0x000022C8 File Offset: 0x000004C8
		// (set) Token: 0x06000020 RID: 32 RVA: 0x000022E2 File Offset: 0x000004E2
		public string privateAddress
		{
			[CompilerGenerated]
			get
			{
				return this.<privateAddress>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<privateAddress>k__BackingField = value;
			}
		}

		// Token: 0x1700000B RID: 11
		// (get) Token: 0x06000021 RID: 33 RVA: 0x000022EC File Offset: 0x000004EC
		// (set) Token: 0x06000022 RID: 34 RVA: 0x00002306 File Offset: 0x00000506
		public int eloScore
		{
			[CompilerGenerated]
			get
			{
				return this.<eloScore>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<eloScore>k__BackingField = value;
			}
		}

		// Token: 0x1700000C RID: 12
		// (get) Token: 0x06000023 RID: 35 RVA: 0x00002310 File Offset: 0x00000510
		// (set) Token: 0x06000024 RID: 36 RVA: 0x0000232A File Offset: 0x0000052A
		public bool advertise
		{
			[CompilerGenerated]
			get
			{
				return this.<advertise>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<advertise>k__BackingField = value;
			}
		}

		// Token: 0x1700000D RID: 13
		// (get) Token: 0x06000025 RID: 37 RVA: 0x00002334 File Offset: 0x00000534
		// (set) Token: 0x06000026 RID: 38 RVA: 0x0000234E File Offset: 0x0000054E
		public string password
		{
			[CompilerGenerated]
			get
			{
				return this.<password>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<password>k__BackingField = value;
			}
		}

		// Token: 0x1700000E RID: 14
		// (get) Token: 0x06000027 RID: 39 RVA: 0x00002358 File Offset: 0x00000558
		// (set) Token: 0x06000028 RID: 40 RVA: 0x00002372 File Offset: 0x00000572
		public Dictionary<string, long> matchAttributes
		{
			[CompilerGenerated]
			get
			{
				return this.<matchAttributes>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<matchAttributes>k__BackingField = value;
			}
		}

		// Token: 0x06000029 RID: 41 RVA: 0x0000237C File Offset: 0x0000057C
		public override string ToString()
		{
			return UnityString.Format("[{0}]-name:{1},size:{2},publicAddress:{3},privateAddress:{4},eloScore:{5},advertise:{6},HasPassword:{7},matchAttributes.Count:{8}", new object[]
			{
				base.ToString(),
				this.name,
				this.size,
				this.publicAddress,
				this.privateAddress,
				this.eloScore,
				this.advertise,
				(!string.IsNullOrEmpty(this.password)) ? "YES" : "NO",
				(this.matchAttributes != null) ? this.matchAttributes.Count : 0
			});
		}

		// Token: 0x0600002A RID: 42 RVA: 0x00002438 File Offset: 0x00000638
		public override bool IsValid()
		{
			return base.IsValid() && this.size >= 2U && (this.matchAttributes == null || this.matchAttributes.Count <= 10);
		}

		// Token: 0x0400000A RID: 10
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private string <name>k__BackingField;

		// Token: 0x0400000B RID: 11
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private uint <size>k__BackingField;

		// Token: 0x0400000C RID: 12
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private string <publicAddress>k__BackingField;

		// Token: 0x0400000D RID: 13
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private string <privateAddress>k__BackingField;

		// Token: 0x0400000E RID: 14
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private int <eloScore>k__BackingField;

		// Token: 0x0400000F RID: 15
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private bool <advertise>k__BackingField;

		// Token: 0x04000010 RID: 16
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private string <password>k__BackingField;

		// Token: 0x04000011 RID: 17
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private Dictionary<string, long> <matchAttributes>k__BackingField;
	}
}
