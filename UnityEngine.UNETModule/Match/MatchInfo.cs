﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine.Networking.Types;

namespace UnityEngine.Networking.Match
{
	// Token: 0x0200001B RID: 27
	[Obsolete("The matchmaker and relay feature will be removed in the future, minimal support will continue until this can be safely done.")]
	public class MatchInfo
	{
		// Token: 0x0600007A RID: 122 RVA: 0x00002F2E File Offset: 0x0000112E
		public MatchInfo()
		{
		}

		// Token: 0x0600007B RID: 123 RVA: 0x00002FF4 File Offset: 0x000011F4
		internal MatchInfo(CreateMatchResponse matchResponse)
		{
			this.address = matchResponse.address;
			this.port = matchResponse.port;
			this.domain = matchResponse.domain;
			this.networkId = (NetworkID)matchResponse.networkId;
			this.accessToken = new NetworkAccessToken(matchResponse.accessTokenString);
			this.nodeId = matchResponse.nodeId;
			this.usingRelay = matchResponse.usingRelay;
		}

		// Token: 0x0600007C RID: 124 RVA: 0x00003064 File Offset: 0x00001264
		internal MatchInfo(JoinMatchResponse matchResponse)
		{
			this.address = matchResponse.address;
			this.port = matchResponse.port;
			this.domain = matchResponse.domain;
			this.networkId = (NetworkID)matchResponse.networkId;
			this.accessToken = new NetworkAccessToken(matchResponse.accessTokenString);
			this.nodeId = matchResponse.nodeId;
			this.usingRelay = matchResponse.usingRelay;
		}

		// Token: 0x17000022 RID: 34
		// (get) Token: 0x0600007D RID: 125 RVA: 0x000030D4 File Offset: 0x000012D4
		// (set) Token: 0x0600007E RID: 126 RVA: 0x000030EE File Offset: 0x000012EE
		public string address
		{
			[CompilerGenerated]
			get
			{
				return this.<address>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<address>k__BackingField = value;
			}
		}

		// Token: 0x17000023 RID: 35
		// (get) Token: 0x0600007F RID: 127 RVA: 0x000030F8 File Offset: 0x000012F8
		// (set) Token: 0x06000080 RID: 128 RVA: 0x00003112 File Offset: 0x00001312
		public int port
		{
			[CompilerGenerated]
			get
			{
				return this.<port>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<port>k__BackingField = value;
			}
		}

		// Token: 0x17000024 RID: 36
		// (get) Token: 0x06000081 RID: 129 RVA: 0x0000311C File Offset: 0x0000131C
		// (set) Token: 0x06000082 RID: 130 RVA: 0x00003136 File Offset: 0x00001336
		public int domain
		{
			[CompilerGenerated]
			get
			{
				return this.<domain>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<domain>k__BackingField = value;
			}
		}

		// Token: 0x17000025 RID: 37
		// (get) Token: 0x06000083 RID: 131 RVA: 0x00003140 File Offset: 0x00001340
		// (set) Token: 0x06000084 RID: 132 RVA: 0x0000315A File Offset: 0x0000135A
		public NetworkID networkId
		{
			[CompilerGenerated]
			get
			{
				return this.<networkId>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<networkId>k__BackingField = value;
			}
		}

		// Token: 0x17000026 RID: 38
		// (get) Token: 0x06000085 RID: 133 RVA: 0x00003164 File Offset: 0x00001364
		// (set) Token: 0x06000086 RID: 134 RVA: 0x0000317E File Offset: 0x0000137E
		public NetworkAccessToken accessToken
		{
			[CompilerGenerated]
			get
			{
				return this.<accessToken>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<accessToken>k__BackingField = value;
			}
		}

		// Token: 0x17000027 RID: 39
		// (get) Token: 0x06000087 RID: 135 RVA: 0x00003188 File Offset: 0x00001388
		// (set) Token: 0x06000088 RID: 136 RVA: 0x000031A2 File Offset: 0x000013A2
		public NodeID nodeId
		{
			[CompilerGenerated]
			get
			{
				return this.<nodeId>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<nodeId>k__BackingField = value;
			}
		}

		// Token: 0x17000028 RID: 40
		// (get) Token: 0x06000089 RID: 137 RVA: 0x000031AC File Offset: 0x000013AC
		// (set) Token: 0x0600008A RID: 138 RVA: 0x000031C6 File Offset: 0x000013C6
		public bool usingRelay
		{
			[CompilerGenerated]
			get
			{
				return this.<usingRelay>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<usingRelay>k__BackingField = value;
			}
		}

		// Token: 0x0600008B RID: 139 RVA: 0x000031D0 File Offset: 0x000013D0
		public override string ToString()
		{
			return UnityString.Format("{0} @ {1}:{2} [{3},{4}]", new object[]
			{
				this.networkId,
				this.address,
				this.port,
				this.nodeId,
				this.usingRelay
			});
		}

		// Token: 0x04000055 RID: 85
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private string <address>k__BackingField;

		// Token: 0x04000056 RID: 86
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private int <port>k__BackingField;

		// Token: 0x04000057 RID: 87
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private int <domain>k__BackingField;

		// Token: 0x04000058 RID: 88
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private NetworkID <networkId>k__BackingField;

		// Token: 0x04000059 RID: 89
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private NetworkAccessToken <accessToken>k__BackingField;

		// Token: 0x0400005A RID: 90
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private NodeID <nodeId>k__BackingField;

		// Token: 0x0400005B RID: 91
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private bool <usingRelay>k__BackingField;
	}
}
