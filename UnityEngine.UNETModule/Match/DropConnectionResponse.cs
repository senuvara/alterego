﻿using System;
using UnityEngine.Networking.Types;

namespace UnityEngine.Networking.Match
{
	// Token: 0x0200000C RID: 12
	[Serializable]
	internal class DropConnectionResponse : Response
	{
		// Token: 0x06000048 RID: 72 RVA: 0x00002249 File Offset: 0x00000449
		public DropConnectionResponse()
		{
		}

		// Token: 0x06000049 RID: 73 RVA: 0x000028D0 File Offset: 0x00000AD0
		public override string ToString()
		{
			return UnityString.Format("[{0}]-networkId:{1}", new object[]
			{
				base.ToString(),
				this.networkId.ToString("X")
			});
		}

		// Token: 0x04000028 RID: 40
		public ulong networkId;

		// Token: 0x04000029 RID: 41
		public NodeID nodeId;
	}
}
