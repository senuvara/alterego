﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine.Networking.Types;

namespace UnityEngine.Networking.Match
{
	// Token: 0x0200001C RID: 28
	[Obsolete("The matchmaker and relay feature will be removed in the future, minimal support will continue until this can be safely done.")]
	public class MatchInfoSnapshot
	{
		// Token: 0x0600008C RID: 140 RVA: 0x00002F2E File Offset: 0x0000112E
		public MatchInfoSnapshot()
		{
		}

		// Token: 0x0600008D RID: 141 RVA: 0x00003238 File Offset: 0x00001438
		internal MatchInfoSnapshot(MatchDesc matchDesc)
		{
			this.networkId = (NetworkID)matchDesc.networkId;
			this.hostNodeId = matchDesc.hostNodeId;
			this.name = matchDesc.name;
			this.averageEloScore = matchDesc.averageEloScore;
			this.maxSize = matchDesc.maxSize;
			this.currentSize = matchDesc.currentSize;
			this.isPrivate = matchDesc.isPrivate;
			this.matchAttributes = matchDesc.matchAttributes;
			this.directConnectInfos = new List<MatchInfoSnapshot.MatchInfoDirectConnectSnapshot>();
			foreach (MatchDirectConnectInfo matchDirectConnectInfo in matchDesc.directConnectInfos)
			{
				this.directConnectInfos.Add(new MatchInfoSnapshot.MatchInfoDirectConnectSnapshot(matchDirectConnectInfo));
			}
		}

		// Token: 0x17000029 RID: 41
		// (get) Token: 0x0600008E RID: 142 RVA: 0x00003314 File Offset: 0x00001514
		// (set) Token: 0x0600008F RID: 143 RVA: 0x0000332E File Offset: 0x0000152E
		public NetworkID networkId
		{
			[CompilerGenerated]
			get
			{
				return this.<networkId>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<networkId>k__BackingField = value;
			}
		}

		// Token: 0x1700002A RID: 42
		// (get) Token: 0x06000090 RID: 144 RVA: 0x00003338 File Offset: 0x00001538
		// (set) Token: 0x06000091 RID: 145 RVA: 0x00003352 File Offset: 0x00001552
		public NodeID hostNodeId
		{
			[CompilerGenerated]
			get
			{
				return this.<hostNodeId>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<hostNodeId>k__BackingField = value;
			}
		}

		// Token: 0x1700002B RID: 43
		// (get) Token: 0x06000092 RID: 146 RVA: 0x0000335C File Offset: 0x0000155C
		// (set) Token: 0x06000093 RID: 147 RVA: 0x00003376 File Offset: 0x00001576
		public string name
		{
			[CompilerGenerated]
			get
			{
				return this.<name>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<name>k__BackingField = value;
			}
		}

		// Token: 0x1700002C RID: 44
		// (get) Token: 0x06000094 RID: 148 RVA: 0x00003380 File Offset: 0x00001580
		// (set) Token: 0x06000095 RID: 149 RVA: 0x0000339A File Offset: 0x0000159A
		public int averageEloScore
		{
			[CompilerGenerated]
			get
			{
				return this.<averageEloScore>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<averageEloScore>k__BackingField = value;
			}
		}

		// Token: 0x1700002D RID: 45
		// (get) Token: 0x06000096 RID: 150 RVA: 0x000033A4 File Offset: 0x000015A4
		// (set) Token: 0x06000097 RID: 151 RVA: 0x000033BE File Offset: 0x000015BE
		public int maxSize
		{
			[CompilerGenerated]
			get
			{
				return this.<maxSize>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<maxSize>k__BackingField = value;
			}
		}

		// Token: 0x1700002E RID: 46
		// (get) Token: 0x06000098 RID: 152 RVA: 0x000033C8 File Offset: 0x000015C8
		// (set) Token: 0x06000099 RID: 153 RVA: 0x000033E2 File Offset: 0x000015E2
		public int currentSize
		{
			[CompilerGenerated]
			get
			{
				return this.<currentSize>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<currentSize>k__BackingField = value;
			}
		}

		// Token: 0x1700002F RID: 47
		// (get) Token: 0x0600009A RID: 154 RVA: 0x000033EC File Offset: 0x000015EC
		// (set) Token: 0x0600009B RID: 155 RVA: 0x00003406 File Offset: 0x00001606
		public bool isPrivate
		{
			[CompilerGenerated]
			get
			{
				return this.<isPrivate>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<isPrivate>k__BackingField = value;
			}
		}

		// Token: 0x17000030 RID: 48
		// (get) Token: 0x0600009C RID: 156 RVA: 0x00003410 File Offset: 0x00001610
		// (set) Token: 0x0600009D RID: 157 RVA: 0x0000342A File Offset: 0x0000162A
		public Dictionary<string, long> matchAttributes
		{
			[CompilerGenerated]
			get
			{
				return this.<matchAttributes>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<matchAttributes>k__BackingField = value;
			}
		}

		// Token: 0x17000031 RID: 49
		// (get) Token: 0x0600009E RID: 158 RVA: 0x00003434 File Offset: 0x00001634
		// (set) Token: 0x0600009F RID: 159 RVA: 0x0000344E File Offset: 0x0000164E
		public List<MatchInfoSnapshot.MatchInfoDirectConnectSnapshot> directConnectInfos
		{
			[CompilerGenerated]
			get
			{
				return this.<directConnectInfos>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<directConnectInfos>k__BackingField = value;
			}
		}

		// Token: 0x0400005C RID: 92
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private NetworkID <networkId>k__BackingField;

		// Token: 0x0400005D RID: 93
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private NodeID <hostNodeId>k__BackingField;

		// Token: 0x0400005E RID: 94
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private string <name>k__BackingField;

		// Token: 0x0400005F RID: 95
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private int <averageEloScore>k__BackingField;

		// Token: 0x04000060 RID: 96
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private int <maxSize>k__BackingField;

		// Token: 0x04000061 RID: 97
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private int <currentSize>k__BackingField;

		// Token: 0x04000062 RID: 98
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private bool <isPrivate>k__BackingField;

		// Token: 0x04000063 RID: 99
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private Dictionary<string, long> <matchAttributes>k__BackingField;

		// Token: 0x04000064 RID: 100
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private List<MatchInfoSnapshot.MatchInfoDirectConnectSnapshot> <directConnectInfos>k__BackingField;

		// Token: 0x0200001D RID: 29
		public class MatchInfoDirectConnectSnapshot
		{
			// Token: 0x060000A0 RID: 160 RVA: 0x00002F2E File Offset: 0x0000112E
			public MatchInfoDirectConnectSnapshot()
			{
			}

			// Token: 0x060000A1 RID: 161 RVA: 0x00003457 File Offset: 0x00001657
			internal MatchInfoDirectConnectSnapshot(MatchDirectConnectInfo matchDirectConnectInfo)
			{
				this.nodeId = matchDirectConnectInfo.nodeId;
				this.publicAddress = matchDirectConnectInfo.publicAddress;
				this.privateAddress = matchDirectConnectInfo.privateAddress;
				this.hostPriority = matchDirectConnectInfo.hostPriority;
			}

			// Token: 0x17000032 RID: 50
			// (get) Token: 0x060000A2 RID: 162 RVA: 0x00003490 File Offset: 0x00001690
			// (set) Token: 0x060000A3 RID: 163 RVA: 0x000034AA File Offset: 0x000016AA
			public NodeID nodeId
			{
				[CompilerGenerated]
				get
				{
					return this.<nodeId>k__BackingField;
				}
				[CompilerGenerated]
				private set
				{
					this.<nodeId>k__BackingField = value;
				}
			}

			// Token: 0x17000033 RID: 51
			// (get) Token: 0x060000A4 RID: 164 RVA: 0x000034B4 File Offset: 0x000016B4
			// (set) Token: 0x060000A5 RID: 165 RVA: 0x000034CE File Offset: 0x000016CE
			public string publicAddress
			{
				[CompilerGenerated]
				get
				{
					return this.<publicAddress>k__BackingField;
				}
				[CompilerGenerated]
				private set
				{
					this.<publicAddress>k__BackingField = value;
				}
			}

			// Token: 0x17000034 RID: 52
			// (get) Token: 0x060000A6 RID: 166 RVA: 0x000034D8 File Offset: 0x000016D8
			// (set) Token: 0x060000A7 RID: 167 RVA: 0x000034F2 File Offset: 0x000016F2
			public string privateAddress
			{
				[CompilerGenerated]
				get
				{
					return this.<privateAddress>k__BackingField;
				}
				[CompilerGenerated]
				private set
				{
					this.<privateAddress>k__BackingField = value;
				}
			}

			// Token: 0x17000035 RID: 53
			// (get) Token: 0x060000A8 RID: 168 RVA: 0x000034FC File Offset: 0x000016FC
			// (set) Token: 0x060000A9 RID: 169 RVA: 0x00003516 File Offset: 0x00001716
			public HostPriority hostPriority
			{
				[CompilerGenerated]
				get
				{
					return this.<hostPriority>k__BackingField;
				}
				[CompilerGenerated]
				private set
				{
					this.<hostPriority>k__BackingField = value;
				}
			}

			// Token: 0x04000065 RID: 101
			[DebuggerBrowsable(DebuggerBrowsableState.Never)]
			[CompilerGenerated]
			private NodeID <nodeId>k__BackingField;

			// Token: 0x04000066 RID: 102
			[CompilerGenerated]
			[DebuggerBrowsable(DebuggerBrowsableState.Never)]
			private string <publicAddress>k__BackingField;

			// Token: 0x04000067 RID: 103
			[CompilerGenerated]
			[DebuggerBrowsable(DebuggerBrowsableState.Never)]
			private string <privateAddress>k__BackingField;

			// Token: 0x04000068 RID: 104
			[DebuggerBrowsable(DebuggerBrowsableState.Never)]
			[CompilerGenerated]
			private HostPriority <hostPriority>k__BackingField;
		}
	}
}
