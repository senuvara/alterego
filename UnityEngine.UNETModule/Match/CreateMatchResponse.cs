﻿using System;
using UnityEngine.Networking.Types;

namespace UnityEngine.Networking.Match
{
	// Token: 0x02000007 RID: 7
	internal class CreateMatchResponse : BasicResponse
	{
		// Token: 0x0600002B RID: 43 RVA: 0x0000248A File Offset: 0x0000068A
		public CreateMatchResponse()
		{
		}

		// Token: 0x0600002C RID: 44 RVA: 0x0000249C File Offset: 0x0000069C
		public override string ToString()
		{
			return UnityString.Format("[{0}]-address:{1},port:{2},networkId:0x{3},accessTokenString.IsEmpty:{4},nodeId:0x{5},usingRelay:{6}", new object[]
			{
				base.ToString(),
				this.address,
				this.port,
				this.networkId.ToString("X"),
				string.IsNullOrEmpty(this.accessTokenString),
				this.nodeId.ToString("X"),
				this.usingRelay
			});
		}

		// Token: 0x04000012 RID: 18
		public string address;

		// Token: 0x04000013 RID: 19
		public int port;

		// Token: 0x04000014 RID: 20
		public int domain = 0;

		// Token: 0x04000015 RID: 21
		public ulong networkId;

		// Token: 0x04000016 RID: 22
		public string accessTokenString;

		// Token: 0x04000017 RID: 23
		public NodeID nodeId;

		// Token: 0x04000018 RID: 24
		public bool usingRelay;
	}
}
