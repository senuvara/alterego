﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine.Networking.Types;

namespace UnityEngine.Networking.Match
{
	// Token: 0x02000008 RID: 8
	internal class JoinMatchRequest : Request
	{
		// Token: 0x0600002D RID: 45 RVA: 0x00002251 File Offset: 0x00000451
		public JoinMatchRequest()
		{
		}

		// Token: 0x1700000F RID: 15
		// (get) Token: 0x0600002E RID: 46 RVA: 0x00002530 File Offset: 0x00000730
		// (set) Token: 0x0600002F RID: 47 RVA: 0x0000254A File Offset: 0x0000074A
		public NetworkID networkId
		{
			[CompilerGenerated]
			get
			{
				return this.<networkId>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<networkId>k__BackingField = value;
			}
		}

		// Token: 0x17000010 RID: 16
		// (get) Token: 0x06000030 RID: 48 RVA: 0x00002554 File Offset: 0x00000754
		// (set) Token: 0x06000031 RID: 49 RVA: 0x0000256E File Offset: 0x0000076E
		public string publicAddress
		{
			[CompilerGenerated]
			get
			{
				return this.<publicAddress>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<publicAddress>k__BackingField = value;
			}
		}

		// Token: 0x17000011 RID: 17
		// (get) Token: 0x06000032 RID: 50 RVA: 0x00002578 File Offset: 0x00000778
		// (set) Token: 0x06000033 RID: 51 RVA: 0x00002592 File Offset: 0x00000792
		public string privateAddress
		{
			[CompilerGenerated]
			get
			{
				return this.<privateAddress>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<privateAddress>k__BackingField = value;
			}
		}

		// Token: 0x17000012 RID: 18
		// (get) Token: 0x06000034 RID: 52 RVA: 0x0000259C File Offset: 0x0000079C
		// (set) Token: 0x06000035 RID: 53 RVA: 0x000025B6 File Offset: 0x000007B6
		public int eloScore
		{
			[CompilerGenerated]
			get
			{
				return this.<eloScore>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<eloScore>k__BackingField = value;
			}
		}

		// Token: 0x17000013 RID: 19
		// (get) Token: 0x06000036 RID: 54 RVA: 0x000025C0 File Offset: 0x000007C0
		// (set) Token: 0x06000037 RID: 55 RVA: 0x000025DA File Offset: 0x000007DA
		public string password
		{
			[CompilerGenerated]
			get
			{
				return this.<password>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<password>k__BackingField = value;
			}
		}

		// Token: 0x06000038 RID: 56 RVA: 0x000025E4 File Offset: 0x000007E4
		public override string ToString()
		{
			return UnityString.Format("[{0}]-networkId:0x{1},publicAddress:{2},privateAddress:{3},eloScore:{4},HasPassword:{5}", new object[]
			{
				base.ToString(),
				this.networkId.ToString("X"),
				this.publicAddress,
				this.privateAddress,
				this.eloScore,
				(!string.IsNullOrEmpty(this.password)) ? "YES" : "NO"
			});
		}

		// Token: 0x06000039 RID: 57 RVA: 0x00002670 File Offset: 0x00000870
		public override bool IsValid()
		{
			return base.IsValid() && this.networkId != NetworkID.Invalid;
		}

		// Token: 0x04000019 RID: 25
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private NetworkID <networkId>k__BackingField;

		// Token: 0x0400001A RID: 26
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private string <publicAddress>k__BackingField;

		// Token: 0x0400001B RID: 27
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private string <privateAddress>k__BackingField;

		// Token: 0x0400001C RID: 28
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private int <eloScore>k__BackingField;

		// Token: 0x0400001D RID: 29
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private string <password>k__BackingField;
	}
}
