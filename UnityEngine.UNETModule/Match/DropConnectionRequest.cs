﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine.Networking.Types;

namespace UnityEngine.Networking.Match
{
	// Token: 0x0200000B RID: 11
	internal class DropConnectionRequest : Request
	{
		// Token: 0x06000041 RID: 65 RVA: 0x00002251 File Offset: 0x00000451
		public DropConnectionRequest()
		{
		}

		// Token: 0x17000015 RID: 21
		// (get) Token: 0x06000042 RID: 66 RVA: 0x000027E4 File Offset: 0x000009E4
		// (set) Token: 0x06000043 RID: 67 RVA: 0x000027FE File Offset: 0x000009FE
		public NetworkID networkId
		{
			[CompilerGenerated]
			get
			{
				return this.<networkId>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<networkId>k__BackingField = value;
			}
		}

		// Token: 0x17000016 RID: 22
		// (get) Token: 0x06000044 RID: 68 RVA: 0x00002808 File Offset: 0x00000A08
		// (set) Token: 0x06000045 RID: 69 RVA: 0x00002822 File Offset: 0x00000A22
		public NodeID nodeId
		{
			[CompilerGenerated]
			get
			{
				return this.<nodeId>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<nodeId>k__BackingField = value;
			}
		}

		// Token: 0x06000046 RID: 70 RVA: 0x0000282C File Offset: 0x00000A2C
		public override string ToString()
		{
			return UnityString.Format("[{0}]-networkId:0x{1},nodeId:0x{2}", new object[]
			{
				base.ToString(),
				this.networkId.ToString("X"),
				this.nodeId.ToString("X")
			});
		}

		// Token: 0x06000047 RID: 71 RVA: 0x00002894 File Offset: 0x00000A94
		public override bool IsValid()
		{
			return base.IsValid() && this.networkId != NetworkID.Invalid && this.nodeId != NodeID.Invalid;
		}

		// Token: 0x04000026 RID: 38
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private NetworkID <networkId>k__BackingField;

		// Token: 0x04000027 RID: 39
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private NodeID <nodeId>k__BackingField;
	}
}
