﻿using System;
using UnityEngine.Networking.Types;

namespace UnityEngine.Networking.Match
{
	// Token: 0x0200000E RID: 14
	[Serializable]
	internal class MatchDirectConnectInfo
	{
		// Token: 0x0600005D RID: 93 RVA: 0x00002050 File Offset: 0x00000250
		public MatchDirectConnectInfo()
		{
		}

		// Token: 0x0600005E RID: 94 RVA: 0x00002BB4 File Offset: 0x00000DB4
		public override string ToString()
		{
			return UnityString.Format("[{0}]-nodeId:{1},publicAddress:{2},privateAddress:{3},hostPriority:{4}", new object[]
			{
				base.ToString(),
				this.nodeId,
				this.publicAddress,
				this.privateAddress,
				this.hostPriority
			});
		}

		// Token: 0x04000033 RID: 51
		public NodeID nodeId;

		// Token: 0x04000034 RID: 52
		public string publicAddress;

		// Token: 0x04000035 RID: 53
		public string privateAddress;

		// Token: 0x04000036 RID: 54
		public HostPriority hostPriority;
	}
}
