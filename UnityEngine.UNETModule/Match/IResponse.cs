﻿using System;

namespace UnityEngine.Networking.Match
{
	// Token: 0x02000003 RID: 3
	internal interface IResponse
	{
		// Token: 0x06000011 RID: 17
		void SetSuccess();

		// Token: 0x06000012 RID: 18
		void SetFailure(string info);
	}
}
