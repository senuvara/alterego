﻿using System;

namespace UnityEngine.Networking.Match
{
	// Token: 0x02000004 RID: 4
	[Serializable]
	internal abstract class Response : IResponse
	{
		// Token: 0x06000013 RID: 19 RVA: 0x00002050 File Offset: 0x00000250
		protected Response()
		{
		}

		// Token: 0x06000014 RID: 20 RVA: 0x000021D0 File Offset: 0x000003D0
		public void SetSuccess()
		{
			this.success = true;
			this.extendedInfo = "";
		}

		// Token: 0x06000015 RID: 21 RVA: 0x000021E5 File Offset: 0x000003E5
		public void SetFailure(string info)
		{
			this.success = false;
			this.extendedInfo += info;
		}

		// Token: 0x06000016 RID: 22 RVA: 0x00002204 File Offset: 0x00000404
		public override string ToString()
		{
			return UnityString.Format("[{0}]-success:{1}-extendedInfo:{2}", new object[]
			{
				base.ToString(),
				this.success,
				this.extendedInfo
			});
		}

		// Token: 0x04000008 RID: 8
		public bool success;

		// Token: 0x04000009 RID: 9
		public string extendedInfo;
	}
}
