﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine.Networking.Types;

namespace UnityEngine.Networking.Match
{
	// Token: 0x02000012 RID: 18
	internal class SetMatchAttributesRequest : Request
	{
		// Token: 0x06000065 RID: 101 RVA: 0x00002251 File Offset: 0x00000451
		public SetMatchAttributesRequest()
		{
		}

		// Token: 0x1700001F RID: 31
		// (get) Token: 0x06000066 RID: 102 RVA: 0x00002D5C File Offset: 0x00000F5C
		// (set) Token: 0x06000067 RID: 103 RVA: 0x00002D76 File Offset: 0x00000F76
		public NetworkID networkId
		{
			[CompilerGenerated]
			get
			{
				return this.<networkId>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<networkId>k__BackingField = value;
			}
		}

		// Token: 0x17000020 RID: 32
		// (get) Token: 0x06000068 RID: 104 RVA: 0x00002D80 File Offset: 0x00000F80
		// (set) Token: 0x06000069 RID: 105 RVA: 0x00002D9A File Offset: 0x00000F9A
		public bool isListed
		{
			[CompilerGenerated]
			get
			{
				return this.<isListed>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<isListed>k__BackingField = value;
			}
		}

		// Token: 0x0600006A RID: 106 RVA: 0x00002DA4 File Offset: 0x00000FA4
		public override string ToString()
		{
			return UnityString.Format("[{0}]-networkId:{1},isListed:{2}", new object[]
			{
				base.ToString(),
				this.networkId.ToString("X"),
				this.isListed
			});
		}

		// Token: 0x0600006B RID: 107 RVA: 0x00002DFC File Offset: 0x00000FFC
		public override bool IsValid()
		{
			return base.IsValid() && this.networkId != NetworkID.Invalid;
		}

		// Token: 0x04000041 RID: 65
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private NetworkID <networkId>k__BackingField;

		// Token: 0x04000042 RID: 66
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private bool <isListed>k__BackingField;
	}
}
