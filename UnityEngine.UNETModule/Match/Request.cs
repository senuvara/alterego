﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine.Networking.Types;

namespace UnityEngine.Networking.Match
{
	// Token: 0x02000002 RID: 2
	internal abstract class Request
	{
		// Token: 0x06000001 RID: 1 RVA: 0x00002050 File Offset: 0x00000250
		protected Request()
		{
		}

		// Token: 0x17000001 RID: 1
		// (get) Token: 0x06000002 RID: 2 RVA: 0x00002058 File Offset: 0x00000258
		// (set) Token: 0x06000003 RID: 3 RVA: 0x00002072 File Offset: 0x00000272
		public int version
		{
			[CompilerGenerated]
			get
			{
				return this.<version>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<version>k__BackingField = value;
			}
		}

		// Token: 0x17000002 RID: 2
		// (get) Token: 0x06000004 RID: 4 RVA: 0x0000207C File Offset: 0x0000027C
		// (set) Token: 0x06000005 RID: 5 RVA: 0x00002096 File Offset: 0x00000296
		public SourceID sourceId
		{
			[CompilerGenerated]
			get
			{
				return this.<sourceId>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<sourceId>k__BackingField = value;
			}
		}

		// Token: 0x17000003 RID: 3
		// (get) Token: 0x06000006 RID: 6 RVA: 0x000020A0 File Offset: 0x000002A0
		// (set) Token: 0x06000007 RID: 7 RVA: 0x000020BA File Offset: 0x000002BA
		public string projectId
		{
			[CompilerGenerated]
			get
			{
				return this.<projectId>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<projectId>k__BackingField = value;
			}
		}

		// Token: 0x17000004 RID: 4
		// (get) Token: 0x06000008 RID: 8 RVA: 0x000020C4 File Offset: 0x000002C4
		// (set) Token: 0x06000009 RID: 9 RVA: 0x000020DE File Offset: 0x000002DE
		public AppID appId
		{
			[CompilerGenerated]
			get
			{
				return this.<appId>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<appId>k__BackingField = value;
			}
		}

		// Token: 0x17000005 RID: 5
		// (get) Token: 0x0600000A RID: 10 RVA: 0x000020E8 File Offset: 0x000002E8
		// (set) Token: 0x0600000B RID: 11 RVA: 0x00002102 File Offset: 0x00000302
		public string accessTokenString
		{
			[CompilerGenerated]
			get
			{
				return this.<accessTokenString>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<accessTokenString>k__BackingField = value;
			}
		}

		// Token: 0x17000006 RID: 6
		// (get) Token: 0x0600000C RID: 12 RVA: 0x0000210C File Offset: 0x0000030C
		// (set) Token: 0x0600000D RID: 13 RVA: 0x00002126 File Offset: 0x00000326
		public int domain
		{
			[CompilerGenerated]
			get
			{
				return this.<domain>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<domain>k__BackingField = value;
			}
		}

		// Token: 0x0600000E RID: 14 RVA: 0x00002130 File Offset: 0x00000330
		public virtual bool IsValid()
		{
			return this.sourceId != SourceID.Invalid;
		}

		// Token: 0x0600000F RID: 15 RVA: 0x00002154 File Offset: 0x00000354
		public override string ToString()
		{
			return UnityString.Format("[{0}]-SourceID:0x{1},projectId:{2},accessTokenString.IsEmpty:{3},domain:{4}", new object[]
			{
				base.ToString(),
				this.sourceId.ToString("X"),
				this.projectId,
				string.IsNullOrEmpty(this.accessTokenString),
				this.domain
			});
		}

		// Token: 0x06000010 RID: 16 RVA: 0x000021C8 File Offset: 0x000003C8
		// Note: this type is marked as 'beforefieldinit'.
		static Request()
		{
		}

		// Token: 0x04000001 RID: 1
		public static readonly int currentVersion = 3;

		// Token: 0x04000002 RID: 2
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private int <version>k__BackingField;

		// Token: 0x04000003 RID: 3
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private SourceID <sourceId>k__BackingField;

		// Token: 0x04000004 RID: 4
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private string <projectId>k__BackingField;

		// Token: 0x04000005 RID: 5
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private AppID <appId>k__BackingField;

		// Token: 0x04000006 RID: 6
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private string <accessTokenString>k__BackingField;

		// Token: 0x04000007 RID: 7
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private int <domain>k__BackingField;
	}
}
