﻿using System;
using System.Collections.Generic;
using UnityEngine.Networking.Types;

namespace UnityEngine.Networking.Match
{
	// Token: 0x0200000F RID: 15
	[Serializable]
	internal class MatchDesc
	{
		// Token: 0x0600005F RID: 95 RVA: 0x00002050 File Offset: 0x00000250
		public MatchDesc()
		{
		}

		// Token: 0x06000060 RID: 96 RVA: 0x00002C10 File Offset: 0x00000E10
		public override string ToString()
		{
			return UnityString.Format("[{0}]-networkId:0x{1},name:{2},averageEloScore:{3},maxSize:{4},currentSize:{5},isPrivate:{6},matchAttributes.Count:{7},hostNodeId:{8},directConnectInfos.Count:{9}", new object[]
			{
				base.ToString(),
				this.networkId.ToString("X"),
				this.name,
				this.averageEloScore,
				this.maxSize,
				this.currentSize,
				this.isPrivate,
				(this.matchAttributes != null) ? this.matchAttributes.Count : 0,
				this.hostNodeId,
				this.directConnectInfos.Count
			});
		}

		// Token: 0x04000037 RID: 55
		public ulong networkId;

		// Token: 0x04000038 RID: 56
		public string name;

		// Token: 0x04000039 RID: 57
		public int averageEloScore;

		// Token: 0x0400003A RID: 58
		public int maxSize;

		// Token: 0x0400003B RID: 59
		public int currentSize;

		// Token: 0x0400003C RID: 60
		public bool isPrivate;

		// Token: 0x0400003D RID: 61
		public Dictionary<string, long> matchAttributes;

		// Token: 0x0400003E RID: 62
		public NodeID hostNodeId;

		// Token: 0x0400003F RID: 63
		public List<MatchDirectConnectInfo> directConnectInfos;
	}
}
