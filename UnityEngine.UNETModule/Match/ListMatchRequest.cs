﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace UnityEngine.Networking.Match
{
	// Token: 0x0200000D RID: 13
	internal class ListMatchRequest : Request
	{
		// Token: 0x0600004A RID: 74 RVA: 0x00002251 File Offset: 0x00000451
		public ListMatchRequest()
		{
		}

		// Token: 0x17000017 RID: 23
		// (get) Token: 0x0600004B RID: 75 RVA: 0x00002914 File Offset: 0x00000B14
		// (set) Token: 0x0600004C RID: 76 RVA: 0x0000292E File Offset: 0x00000B2E
		public int pageSize
		{
			[CompilerGenerated]
			get
			{
				return this.<pageSize>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<pageSize>k__BackingField = value;
			}
		}

		// Token: 0x17000018 RID: 24
		// (get) Token: 0x0600004D RID: 77 RVA: 0x00002938 File Offset: 0x00000B38
		// (set) Token: 0x0600004E RID: 78 RVA: 0x00002952 File Offset: 0x00000B52
		public int pageNum
		{
			[CompilerGenerated]
			get
			{
				return this.<pageNum>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<pageNum>k__BackingField = value;
			}
		}

		// Token: 0x17000019 RID: 25
		// (get) Token: 0x0600004F RID: 79 RVA: 0x0000295C File Offset: 0x00000B5C
		// (set) Token: 0x06000050 RID: 80 RVA: 0x00002976 File Offset: 0x00000B76
		public string nameFilter
		{
			[CompilerGenerated]
			get
			{
				return this.<nameFilter>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<nameFilter>k__BackingField = value;
			}
		}

		// Token: 0x1700001A RID: 26
		// (get) Token: 0x06000051 RID: 81 RVA: 0x00002980 File Offset: 0x00000B80
		// (set) Token: 0x06000052 RID: 82 RVA: 0x0000299A File Offset: 0x00000B9A
		public bool filterOutPrivateMatches
		{
			[CompilerGenerated]
			get
			{
				return this.<filterOutPrivateMatches>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<filterOutPrivateMatches>k__BackingField = value;
			}
		}

		// Token: 0x1700001B RID: 27
		// (get) Token: 0x06000053 RID: 83 RVA: 0x000029A4 File Offset: 0x00000BA4
		// (set) Token: 0x06000054 RID: 84 RVA: 0x000029BE File Offset: 0x00000BBE
		public int eloScore
		{
			[CompilerGenerated]
			get
			{
				return this.<eloScore>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<eloScore>k__BackingField = value;
			}
		}

		// Token: 0x1700001C RID: 28
		// (get) Token: 0x06000055 RID: 85 RVA: 0x000029C8 File Offset: 0x00000BC8
		// (set) Token: 0x06000056 RID: 86 RVA: 0x000029E2 File Offset: 0x00000BE2
		public Dictionary<string, long> matchAttributeFilterLessThan
		{
			[CompilerGenerated]
			get
			{
				return this.<matchAttributeFilterLessThan>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<matchAttributeFilterLessThan>k__BackingField = value;
			}
		}

		// Token: 0x1700001D RID: 29
		// (get) Token: 0x06000057 RID: 87 RVA: 0x000029EC File Offset: 0x00000BEC
		// (set) Token: 0x06000058 RID: 88 RVA: 0x00002A06 File Offset: 0x00000C06
		public Dictionary<string, long> matchAttributeFilterEqualTo
		{
			[CompilerGenerated]
			get
			{
				return this.<matchAttributeFilterEqualTo>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<matchAttributeFilterEqualTo>k__BackingField = value;
			}
		}

		// Token: 0x1700001E RID: 30
		// (get) Token: 0x06000059 RID: 89 RVA: 0x00002A10 File Offset: 0x00000C10
		// (set) Token: 0x0600005A RID: 90 RVA: 0x00002A2A File Offset: 0x00000C2A
		public Dictionary<string, long> matchAttributeFilterGreaterThan
		{
			[CompilerGenerated]
			get
			{
				return this.<matchAttributeFilterGreaterThan>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<matchAttributeFilterGreaterThan>k__BackingField = value;
			}
		}

		// Token: 0x0600005B RID: 91 RVA: 0x00002A34 File Offset: 0x00000C34
		public override string ToString()
		{
			return UnityString.Format("[{0}]-pageSize:{1},pageNum:{2},nameFilter:{3}, filterOutPrivateMatches:{4}, eloScore:{5}, matchAttributeFilterLessThan.Count:{6}, matchAttributeFilterEqualTo.Count:{7}, matchAttributeFilterGreaterThan.Count:{8}", new object[]
			{
				base.ToString(),
				this.pageSize,
				this.pageNum,
				this.nameFilter,
				this.filterOutPrivateMatches,
				this.eloScore,
				(this.matchAttributeFilterLessThan != null) ? this.matchAttributeFilterLessThan.Count : 0,
				(this.matchAttributeFilterEqualTo != null) ? this.matchAttributeFilterEqualTo.Count : 0,
				(this.matchAttributeFilterGreaterThan != null) ? this.matchAttributeFilterGreaterThan.Count : 0
			});
		}

		// Token: 0x0600005C RID: 92 RVA: 0x00002B10 File Offset: 0x00000D10
		public override bool IsValid()
		{
			int num = (this.matchAttributeFilterLessThan != null) ? this.matchAttributeFilterLessThan.Count : 0;
			num += ((this.matchAttributeFilterEqualTo != null) ? this.matchAttributeFilterEqualTo.Count : 0);
			num += ((this.matchAttributeFilterGreaterThan != null) ? this.matchAttributeFilterGreaterThan.Count : 0);
			return base.IsValid() && this.pageSize >= 1 && this.pageSize <= 1000 && num <= 10;
		}

		// Token: 0x0400002A RID: 42
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private int <pageSize>k__BackingField;

		// Token: 0x0400002B RID: 43
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private int <pageNum>k__BackingField;

		// Token: 0x0400002C RID: 44
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private string <nameFilter>k__BackingField;

		// Token: 0x0400002D RID: 45
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private bool <filterOutPrivateMatches>k__BackingField;

		// Token: 0x0400002E RID: 46
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private int <eloScore>k__BackingField;

		// Token: 0x0400002F RID: 47
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private Dictionary<string, long> <matchAttributeFilterLessThan>k__BackingField;

		// Token: 0x04000030 RID: 48
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private Dictionary<string, long> <matchAttributeFilterEqualTo>k__BackingField;

		// Token: 0x04000031 RID: 49
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private Dictionary<string, long> <matchAttributeFilterGreaterThan>k__BackingField;

		// Token: 0x04000032 RID: 50
		[Obsolete("This bool is deprecated in favor of filterOutPrivateMatches")]
		public bool includePasswordMatches;
	}
}
