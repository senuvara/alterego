﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine.Networking.Types;

namespace UnityEngine.Networking.Match
{
	// Token: 0x0200001E RID: 30
	[Obsolete("The matchmaker and relay feature will be removed in the future, minimal support will continue until this can be safely done.")]
	public class NetworkMatch : MonoBehaviour
	{
		// Token: 0x060000AA RID: 170 RVA: 0x0000351F File Offset: 0x0000171F
		public NetworkMatch()
		{
		}

		// Token: 0x17000036 RID: 54
		// (get) Token: 0x060000AB RID: 171 RVA: 0x00003538 File Offset: 0x00001738
		// (set) Token: 0x060000AC RID: 172 RVA: 0x00003553 File Offset: 0x00001753
		public Uri baseUri
		{
			get
			{
				return this.m_BaseUri;
			}
			set
			{
				this.m_BaseUri = value;
			}
		}

		// Token: 0x060000AD RID: 173 RVA: 0x00002F4E File Offset: 0x0000114E
		[Obsolete("This function is not used any longer to interface with the matchmaker. Please set up your project by logging in through the editor connect dialog.", true)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public void SetProgramAppID(AppID programAppID)
		{
		}

		// Token: 0x060000AE RID: 174 RVA: 0x00003560 File Offset: 0x00001760
		public Coroutine CreateMatch(string matchName, uint matchSize, bool matchAdvertise, string matchPassword, string publicClientAddress, string privateClientAddress, int eloScoreForMatch, int requestDomain, NetworkMatch.DataResponseDelegate<MatchInfo> callback)
		{
			Coroutine result;
			if (Application.platform == RuntimePlatform.WebGLPlayer)
			{
				Debug.LogError("Matchmaking is not supported on WebGL player.");
				result = null;
			}
			else
			{
				result = this.CreateMatch(new CreateMatchRequest
				{
					name = matchName,
					size = matchSize,
					advertise = matchAdvertise,
					password = matchPassword,
					publicAddress = publicClientAddress,
					privateAddress = privateClientAddress,
					eloScore = eloScoreForMatch,
					domain = requestDomain
				}, callback);
			}
			return result;
		}

		// Token: 0x060000AF RID: 175 RVA: 0x000035E0 File Offset: 0x000017E0
		internal Coroutine CreateMatch(CreateMatchRequest req, NetworkMatch.DataResponseDelegate<MatchInfo> callback)
		{
			Coroutine result;
			if (callback == null)
			{
				Debug.Log("callback supplied is null, aborting CreateMatch Request.");
				result = null;
			}
			else
			{
				Uri uri = new Uri(this.baseUri, "/json/reply/CreateMatchRequest");
				Debug.Log("MatchMakingClient Create :" + uri);
				WWWForm wwwform = new WWWForm();
				wwwform.AddField("version", Request.currentVersion);
				wwwform.AddField("projectId", Application.cloudProjectId);
				wwwform.AddField("sourceId", Utility.GetSourceID().ToString());
				wwwform.AddField("accessTokenString", 0);
				wwwform.AddField("domain", req.domain);
				wwwform.AddField("name", req.name);
				wwwform.AddField("size", req.size.ToString());
				wwwform.AddField("advertise", req.advertise.ToString());
				wwwform.AddField("password", req.password);
				wwwform.AddField("publicAddress", req.publicAddress);
				wwwform.AddField("privateAddress", req.privateAddress);
				wwwform.AddField("eloScore", req.eloScore.ToString());
				wwwform.headers["Accept"] = "application/json";
				UnityWebRequest client = UnityWebRequest.Post(uri.ToString(), wwwform);
				result = base.StartCoroutine(this.ProcessMatchResponse<CreateMatchResponse, NetworkMatch.DataResponseDelegate<MatchInfo>>(client, new NetworkMatch.InternalResponseDelegate<CreateMatchResponse, NetworkMatch.DataResponseDelegate<MatchInfo>>(this.OnMatchCreate), callback));
			}
			return result;
		}

		// Token: 0x060000B0 RID: 176 RVA: 0x00003772 File Offset: 0x00001972
		internal virtual void OnMatchCreate(CreateMatchResponse response, NetworkMatch.DataResponseDelegate<MatchInfo> userCallback)
		{
			if (response.success)
			{
				Utility.SetAccessTokenForNetwork((NetworkID)response.networkId, new NetworkAccessToken(response.accessTokenString));
			}
			userCallback(response.success, response.extendedInfo, new MatchInfo(response));
		}

		// Token: 0x060000B1 RID: 177 RVA: 0x000037B0 File Offset: 0x000019B0
		public Coroutine JoinMatch(NetworkID netId, string matchPassword, string publicClientAddress, string privateClientAddress, int eloScoreForClient, int requestDomain, NetworkMatch.DataResponseDelegate<MatchInfo> callback)
		{
			return this.JoinMatch(new JoinMatchRequest
			{
				networkId = netId,
				password = matchPassword,
				publicAddress = publicClientAddress,
				privateAddress = privateClientAddress,
				eloScore = eloScoreForClient,
				domain = requestDomain
			}, callback);
		}

		// Token: 0x060000B2 RID: 178 RVA: 0x00003804 File Offset: 0x00001A04
		internal Coroutine JoinMatch(JoinMatchRequest req, NetworkMatch.DataResponseDelegate<MatchInfo> callback)
		{
			Coroutine result;
			if (callback == null)
			{
				Debug.Log("callback supplied is null, aborting JoinMatch Request.");
				result = null;
			}
			else
			{
				Uri uri = new Uri(this.baseUri, "/json/reply/JoinMatchRequest");
				Debug.Log("MatchMakingClient Join :" + uri);
				WWWForm wwwform = new WWWForm();
				wwwform.AddField("version", Request.currentVersion);
				wwwform.AddField("projectId", Application.cloudProjectId);
				wwwform.AddField("sourceId", Utility.GetSourceID().ToString());
				wwwform.AddField("accessTokenString", 0);
				wwwform.AddField("domain", req.domain);
				wwwform.AddField("networkId", req.networkId.ToString());
				wwwform.AddField("password", req.password);
				wwwform.AddField("publicAddress", req.publicAddress);
				wwwform.AddField("privateAddress", req.privateAddress);
				wwwform.AddField("eloScore", req.eloScore.ToString());
				wwwform.headers["Accept"] = "application/json";
				UnityWebRequest client = UnityWebRequest.Post(uri.ToString(), wwwform);
				result = base.StartCoroutine(this.ProcessMatchResponse<JoinMatchResponse, NetworkMatch.DataResponseDelegate<MatchInfo>>(client, new NetworkMatch.InternalResponseDelegate<JoinMatchResponse, NetworkMatch.DataResponseDelegate<MatchInfo>>(this.OnMatchJoined), callback));
			}
			return result;
		}

		// Token: 0x060000B3 RID: 179 RVA: 0x00003964 File Offset: 0x00001B64
		internal void OnMatchJoined(JoinMatchResponse response, NetworkMatch.DataResponseDelegate<MatchInfo> userCallback)
		{
			if (response.success)
			{
				Utility.SetAccessTokenForNetwork((NetworkID)response.networkId, new NetworkAccessToken(response.accessTokenString));
			}
			userCallback(response.success, response.extendedInfo, new MatchInfo(response));
		}

		// Token: 0x060000B4 RID: 180 RVA: 0x000039A0 File Offset: 0x00001BA0
		public Coroutine DestroyMatch(NetworkID netId, int requestDomain, NetworkMatch.BasicResponseDelegate callback)
		{
			return this.DestroyMatch(new DestroyMatchRequest
			{
				networkId = netId,
				domain = requestDomain
			}, callback);
		}

		// Token: 0x060000B5 RID: 181 RVA: 0x000039D4 File Offset: 0x00001BD4
		internal Coroutine DestroyMatch(DestroyMatchRequest req, NetworkMatch.BasicResponseDelegate callback)
		{
			Coroutine result;
			if (callback == null)
			{
				Debug.Log("callback supplied is null, aborting DestroyMatch Request.");
				result = null;
			}
			else
			{
				Uri uri = new Uri(this.baseUri, "/json/reply/DestroyMatchRequest");
				Debug.Log("MatchMakingClient Destroy :" + uri.ToString());
				WWWForm wwwform = new WWWForm();
				wwwform.AddField("version", Request.currentVersion);
				wwwform.AddField("projectId", Application.cloudProjectId);
				wwwform.AddField("sourceId", Utility.GetSourceID().ToString());
				wwwform.AddField("accessTokenString", Utility.GetAccessTokenForNetwork(req.networkId).GetByteString());
				wwwform.AddField("domain", req.domain);
				wwwform.AddField("networkId", req.networkId.ToString());
				wwwform.headers["Accept"] = "application/json";
				UnityWebRequest client = UnityWebRequest.Post(uri.ToString(), wwwform);
				result = base.StartCoroutine(this.ProcessMatchResponse<BasicResponse, NetworkMatch.BasicResponseDelegate>(client, new NetworkMatch.InternalResponseDelegate<BasicResponse, NetworkMatch.BasicResponseDelegate>(this.OnMatchDestroyed), callback));
			}
			return result;
		}

		// Token: 0x060000B6 RID: 182 RVA: 0x00003AF5 File Offset: 0x00001CF5
		internal void OnMatchDestroyed(BasicResponse response, NetworkMatch.BasicResponseDelegate userCallback)
		{
			userCallback(response.success, response.extendedInfo);
		}

		// Token: 0x060000B7 RID: 183 RVA: 0x00003B0C File Offset: 0x00001D0C
		public Coroutine DropConnection(NetworkID netId, NodeID dropNodeId, int requestDomain, NetworkMatch.BasicResponseDelegate callback)
		{
			return this.DropConnection(new DropConnectionRequest
			{
				networkId = netId,
				nodeId = dropNodeId,
				domain = requestDomain
			}, callback);
		}

		// Token: 0x060000B8 RID: 184 RVA: 0x00003B48 File Offset: 0x00001D48
		internal Coroutine DropConnection(DropConnectionRequest req, NetworkMatch.BasicResponseDelegate callback)
		{
			Coroutine result;
			if (callback == null)
			{
				Debug.Log("callback supplied is null, aborting DropConnection Request.");
				result = null;
			}
			else
			{
				Uri uri = new Uri(this.baseUri, "/json/reply/DropConnectionRequest");
				Debug.Log("MatchMakingClient DropConnection :" + uri);
				WWWForm wwwform = new WWWForm();
				wwwform.AddField("version", Request.currentVersion);
				wwwform.AddField("projectId", Application.cloudProjectId);
				wwwform.AddField("sourceId", Utility.GetSourceID().ToString());
				wwwform.AddField("accessTokenString", Utility.GetAccessTokenForNetwork(req.networkId).GetByteString());
				wwwform.AddField("domain", req.domain);
				wwwform.AddField("networkId", req.networkId.ToString());
				wwwform.AddField("nodeId", req.nodeId.ToString());
				wwwform.headers["Accept"] = "application/json";
				UnityWebRequest client = UnityWebRequest.Post(uri.ToString(), wwwform);
				result = base.StartCoroutine(this.ProcessMatchResponse<DropConnectionResponse, NetworkMatch.BasicResponseDelegate>(client, new NetworkMatch.InternalResponseDelegate<DropConnectionResponse, NetworkMatch.BasicResponseDelegate>(this.OnDropConnection), callback));
			}
			return result;
		}

		// Token: 0x060000B9 RID: 185 RVA: 0x00003AF5 File Offset: 0x00001CF5
		internal void OnDropConnection(DropConnectionResponse response, NetworkMatch.BasicResponseDelegate userCallback)
		{
			userCallback(response.success, response.extendedInfo);
		}

		// Token: 0x060000BA RID: 186 RVA: 0x00003C84 File Offset: 0x00001E84
		public Coroutine ListMatches(int startPageNumber, int resultPageSize, string matchNameFilter, bool filterOutPrivateMatchesFromResults, int eloScoreTarget, int requestDomain, NetworkMatch.DataResponseDelegate<List<MatchInfoSnapshot>> callback)
		{
			Coroutine result;
			if (Application.platform == RuntimePlatform.WebGLPlayer)
			{
				Debug.LogError("Matchmaking is not supported on WebGL player.");
				result = null;
			}
			else
			{
				result = this.ListMatches(new ListMatchRequest
				{
					pageNum = startPageNumber,
					pageSize = resultPageSize,
					nameFilter = matchNameFilter,
					filterOutPrivateMatches = filterOutPrivateMatchesFromResults,
					eloScore = eloScoreTarget,
					domain = requestDomain
				}, callback);
			}
			return result;
		}

		// Token: 0x060000BB RID: 187 RVA: 0x00003CF4 File Offset: 0x00001EF4
		internal Coroutine ListMatches(ListMatchRequest req, NetworkMatch.DataResponseDelegate<List<MatchInfoSnapshot>> callback)
		{
			Coroutine result;
			if (callback == null)
			{
				Debug.Log("callback supplied is null, aborting ListMatch Request.");
				result = null;
			}
			else
			{
				Uri uri = new Uri(this.baseUri, "/json/reply/ListMatchRequest");
				Debug.Log("MatchMakingClient ListMatches :" + uri);
				WWWForm wwwform = new WWWForm();
				wwwform.AddField("version", Request.currentVersion);
				wwwform.AddField("projectId", Application.cloudProjectId);
				wwwform.AddField("sourceId", Utility.GetSourceID().ToString());
				wwwform.AddField("accessTokenString", 0);
				wwwform.AddField("domain", req.domain);
				wwwform.AddField("pageSize", req.pageSize);
				wwwform.AddField("pageNum", req.pageNum);
				wwwform.AddField("nameFilter", req.nameFilter);
				wwwform.AddField("filterOutPrivateMatches", req.filterOutPrivateMatches.ToString());
				wwwform.AddField("eloScore", req.eloScore.ToString());
				wwwform.headers["Accept"] = "application/json";
				UnityWebRequest client = UnityWebRequest.Post(uri.ToString(), wwwform);
				result = base.StartCoroutine(this.ProcessMatchResponse<ListMatchResponse, NetworkMatch.DataResponseDelegate<List<MatchInfoSnapshot>>>(client, new NetworkMatch.InternalResponseDelegate<ListMatchResponse, NetworkMatch.DataResponseDelegate<List<MatchInfoSnapshot>>>(this.OnMatchList), callback));
			}
			return result;
		}

		// Token: 0x060000BC RID: 188 RVA: 0x00003E54 File Offset: 0x00002054
		internal void OnMatchList(ListMatchResponse response, NetworkMatch.DataResponseDelegate<List<MatchInfoSnapshot>> userCallback)
		{
			List<MatchInfoSnapshot> list = new List<MatchInfoSnapshot>();
			foreach (MatchDesc matchDesc in response.matches)
			{
				list.Add(new MatchInfoSnapshot(matchDesc));
			}
			userCallback(response.success, response.extendedInfo, list);
		}

		// Token: 0x060000BD RID: 189 RVA: 0x00003ED4 File Offset: 0x000020D4
		public Coroutine SetMatchAttributes(NetworkID networkId, bool isListed, int requestDomain, NetworkMatch.BasicResponseDelegate callback)
		{
			return this.SetMatchAttributes(new SetMatchAttributesRequest
			{
				networkId = networkId,
				isListed = isListed,
				domain = requestDomain
			}, callback);
		}

		// Token: 0x060000BE RID: 190 RVA: 0x00003F10 File Offset: 0x00002110
		internal Coroutine SetMatchAttributes(SetMatchAttributesRequest req, NetworkMatch.BasicResponseDelegate callback)
		{
			Coroutine result;
			if (callback == null)
			{
				Debug.Log("callback supplied is null, aborting SetMatchAttributes Request.");
				result = null;
			}
			else
			{
				Uri uri = new Uri(this.baseUri, "/json/reply/SetMatchAttributesRequest");
				Debug.Log("MatchMakingClient SetMatchAttributes :" + uri);
				WWWForm wwwform = new WWWForm();
				wwwform.AddField("version", Request.currentVersion);
				wwwform.AddField("projectId", Application.cloudProjectId);
				wwwform.AddField("sourceId", Utility.GetSourceID().ToString());
				wwwform.AddField("accessTokenString", Utility.GetAccessTokenForNetwork(req.networkId).GetByteString());
				wwwform.AddField("domain", req.domain);
				wwwform.AddField("networkId", req.networkId.ToString());
				wwwform.AddField("isListed", req.isListed.ToString());
				wwwform.headers["Accept"] = "application/json";
				UnityWebRequest client = UnityWebRequest.Post(uri.ToString(), wwwform);
				result = base.StartCoroutine(this.ProcessMatchResponse<BasicResponse, NetworkMatch.BasicResponseDelegate>(client, new NetworkMatch.InternalResponseDelegate<BasicResponse, NetworkMatch.BasicResponseDelegate>(this.OnSetMatchAttributes), callback));
			}
			return result;
		}

		// Token: 0x060000BF RID: 191 RVA: 0x00003AF5 File Offset: 0x00001CF5
		internal void OnSetMatchAttributes(BasicResponse response, NetworkMatch.BasicResponseDelegate userCallback)
		{
			userCallback(response.success, response.extendedInfo);
		}

		// Token: 0x060000C0 RID: 192 RVA: 0x0000404C File Offset: 0x0000224C
		private IEnumerator ProcessMatchResponse<JSONRESPONSE, USERRESPONSEDELEGATETYPE>(UnityWebRequest client, NetworkMatch.InternalResponseDelegate<JSONRESPONSE, USERRESPONSEDELEGATETYPE> internalCallback, USERRESPONSEDELEGATETYPE userCallback) where JSONRESPONSE : Response, new()
		{
			yield return client.SendWebRequest();
			JSONRESPONSE jsonInterface = Activator.CreateInstance<JSONRESPONSE>();
			if (!client.isNetworkError && !client.isHttpError)
			{
				try
				{
					JsonUtility.FromJsonOverwrite(client.downloadHandler.text, jsonInterface);
				}
				catch (ArgumentException ex)
				{
					jsonInterface.SetFailure(UnityString.Format("ArgumentException:[{0}] ", new object[]
					{
						ex.ToString()
					}));
				}
			}
			else
			{
				jsonInterface.SetFailure(UnityString.Format("Request error:[{0}] Raw response:[{1}]", new object[]
				{
					client.error,
					client.downloadHandler.text
				}));
			}
			client.Dispose();
			internalCallback(jsonInterface, userCallback);
			yield break;
		}

		// Token: 0x04000069 RID: 105
		private Uri m_BaseUri = new Uri("https://mm.unet.unity3d.com");

		// Token: 0x0200001F RID: 31
		// (Invoke) Token: 0x060000C2 RID: 194
		public delegate void BasicResponseDelegate(bool success, string extendedInfo);

		// Token: 0x02000020 RID: 32
		// (Invoke) Token: 0x060000C6 RID: 198
		public delegate void DataResponseDelegate<T>(bool success, string extendedInfo, T responseData);

		// Token: 0x02000021 RID: 33
		// (Invoke) Token: 0x060000CA RID: 202
		private delegate void InternalResponseDelegate<T, U>(T response, U userCallback);

		// Token: 0x02000022 RID: 34
		[CompilerGenerated]
		private sealed class <ProcessMatchResponse>c__Iterator0<JSONRESPONSE, USERRESPONSEDELEGATETYPE> : IEnumerator, IDisposable, IEnumerator<object> where JSONRESPONSE : Response, new()
		{
			// Token: 0x060000CD RID: 205 RVA: 0x00002050 File Offset: 0x00000250
			[DebuggerHidden]
			public <ProcessMatchResponse>c__Iterator0()
			{
			}

			// Token: 0x060000CE RID: 206 RVA: 0x0000407C File Offset: 0x0000227C
			public bool MoveNext()
			{
				uint num = (uint)this.$PC;
				this.$PC = -1;
				switch (num)
				{
				case 0U:
					this.$current = client.SendWebRequest();
					if (!this.$disposing)
					{
						this.$PC = 1;
					}
					return true;
				case 1U:
					jsonInterface = Activator.CreateInstance<JSONRESPONSE>();
					if (!client.isNetworkError && !client.isHttpError)
					{
						try
						{
							JsonUtility.FromJsonOverwrite(client.downloadHandler.text, jsonInterface);
						}
						catch (ArgumentException ex)
						{
							jsonInterface.SetFailure(UnityString.Format("ArgumentException:[{0}] ", new object[]
							{
								ex.ToString()
							}));
						}
					}
					else
					{
						jsonInterface.SetFailure(UnityString.Format("Request error:[{0}] Raw response:[{1}]", new object[]
						{
							client.error,
							client.downloadHandler.text
						}));
					}
					client.Dispose();
					internalCallback(jsonInterface, userCallback);
					this.$PC = -1;
					break;
				}
				return false;
			}

			// Token: 0x17000037 RID: 55
			// (get) Token: 0x060000CF RID: 207 RVA: 0x000041DC File Offset: 0x000023DC
			object IEnumerator<object>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.$current;
				}
			}

			// Token: 0x17000038 RID: 56
			// (get) Token: 0x060000D0 RID: 208 RVA: 0x000041F8 File Offset: 0x000023F8
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.$current;
				}
			}

			// Token: 0x060000D1 RID: 209 RVA: 0x00004212 File Offset: 0x00002412
			[DebuggerHidden]
			public void Dispose()
			{
				this.$disposing = true;
				this.$PC = -1;
			}

			// Token: 0x060000D2 RID: 210 RVA: 0x00004222 File Offset: 0x00002422
			[DebuggerHidden]
			public void Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x0400006A RID: 106
			internal UnityWebRequest client;

			// Token: 0x0400006B RID: 107
			internal JSONRESPONSE <jsonInterface>__0;

			// Token: 0x0400006C RID: 108
			internal NetworkMatch.InternalResponseDelegate<JSONRESPONSE, USERRESPONSEDELEGATETYPE> internalCallback;

			// Token: 0x0400006D RID: 109
			internal USERRESPONSEDELEGATETYPE userCallback;

			// Token: 0x0400006E RID: 110
			internal object $current;

			// Token: 0x0400006F RID: 111
			internal bool $disposing;

			// Token: 0x04000070 RID: 112
			internal int $PC;
		}
	}
}
