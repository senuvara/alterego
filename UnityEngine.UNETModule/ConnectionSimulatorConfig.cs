﻿using System;

namespace UnityEngine.Networking
{
	// Token: 0x0200002D RID: 45
	[Obsolete("The UNET transport will be removed in the future as soon a replacement is ready.")]
	public class ConnectionSimulatorConfig : IDisposable
	{
		// Token: 0x060001AD RID: 429 RVA: 0x00005DF9 File Offset: 0x00003FF9
		public ConnectionSimulatorConfig(int outMinDelay, int outAvgDelay, int inMinDelay, int inAvgDelay, float packetLossPercentage)
		{
			this.m_OutMinDelay = outMinDelay;
			this.m_OutAvgDelay = outAvgDelay;
			this.m_InMinDelay = inMinDelay;
			this.m_InAvgDelay = inAvgDelay;
			this.m_PacketLossPercentage = packetLossPercentage;
		}

		// Token: 0x060001AE RID: 430 RVA: 0x00002F4E File Offset: 0x0000114E
		[ThreadAndSerializationSafe]
		public void Dispose()
		{
		}

		// Token: 0x060001AF RID: 431 RVA: 0x00005E28 File Offset: 0x00004028
		~ConnectionSimulatorConfig()
		{
			this.Dispose();
		}

		// Token: 0x040000CF RID: 207
		internal int m_OutMinDelay;

		// Token: 0x040000D0 RID: 208
		internal int m_OutAvgDelay;

		// Token: 0x040000D1 RID: 209
		internal int m_InMinDelay;

		// Token: 0x040000D2 RID: 210
		internal int m_InAvgDelay;

		// Token: 0x040000D3 RID: 211
		internal float m_PacketLossPercentage;
	}
}
