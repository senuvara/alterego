﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine.Networking.Types;

namespace UnityEngine.Networking
{
	// Token: 0x0200001A RID: 26
	public class Utility
	{
		// Token: 0x06000071 RID: 113 RVA: 0x00002F2E File Offset: 0x0000112E
		private Utility()
		{
		}

		// Token: 0x17000021 RID: 33
		// (get) Token: 0x06000072 RID: 114 RVA: 0x00002F38 File Offset: 0x00001138
		// (set) Token: 0x06000073 RID: 115 RVA: 0x00002F4E File Offset: 0x0000114E
		[Obsolete("This property is unused and should not be referenced in code.", true)]
		public static bool useRandomSourceID
		{
			get
			{
				return false;
			}
			set
			{
			}
		}

		// Token: 0x06000074 RID: 116 RVA: 0x00002F54 File Offset: 0x00001154
		public static SourceID GetSourceID()
		{
			return (SourceID)((long)SystemInfo.deviceUniqueIdentifier.GetHashCode());
		}

		// Token: 0x06000075 RID: 117 RVA: 0x00002F4E File Offset: 0x0000114E
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Obsolete("This function is unused and should not be referenced in code. Please sign in and setup your project in the editor instead.", true)]
		public static void SetAppID(AppID newAppID)
		{
		}

		// Token: 0x06000076 RID: 118 RVA: 0x00002F74 File Offset: 0x00001174
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Obsolete("This function is unused and should not be referenced in code. Please sign in and setup your project in the editor instead.", true)]
		public static AppID GetAppID()
		{
			return AppID.Invalid;
		}

		// Token: 0x06000077 RID: 119 RVA: 0x00002F8B File Offset: 0x0000118B
		public static void SetAccessTokenForNetwork(NetworkID netId, NetworkAccessToken accessToken)
		{
			if (Utility.s_dictTokens.ContainsKey(netId))
			{
				Utility.s_dictTokens.Remove(netId);
			}
			Utility.s_dictTokens.Add(netId, accessToken);
		}

		// Token: 0x06000078 RID: 120 RVA: 0x00002FB8 File Offset: 0x000011B8
		public static NetworkAccessToken GetAccessTokenForNetwork(NetworkID netId)
		{
			NetworkAccessToken result;
			if (!Utility.s_dictTokens.TryGetValue(netId, out result))
			{
				result = new NetworkAccessToken();
			}
			return result;
		}

		// Token: 0x06000079 RID: 121 RVA: 0x00002FE8 File Offset: 0x000011E8
		// Note: this type is marked as 'beforefieldinit'.
		static Utility()
		{
		}

		// Token: 0x04000054 RID: 84
		private static Dictionary<NetworkID, NetworkAccessToken> s_dictTokens = new Dictionary<NetworkID, NetworkAccessToken>();
	}
}
