﻿using System;
using System.Collections.Generic;

namespace UnityEngine.Networking
{
	// Token: 0x0200002B RID: 43
	[Obsolete("The UNET transport will be removed in the future as soon a replacement is ready.")]
	[Serializable]
	public class HostTopology
	{
		// Token: 0x06000184 RID: 388 RVA: 0x00005774 File Offset: 0x00003974
		public HostTopology(ConnectionConfig defaultConfig, int maxDefaultConnections)
		{
			if (defaultConfig == null)
			{
				throw new NullReferenceException("config is not defined");
			}
			if (maxDefaultConnections <= 0)
			{
				throw new ArgumentOutOfRangeException("maxConnections", "Number of connections should be > 0");
			}
			if (maxDefaultConnections >= 65535)
			{
				throw new ArgumentOutOfRangeException("maxConnections", "Number of connections should be < 65535");
			}
			ConnectionConfig.Validate(defaultConfig);
			this.m_DefConfig = new ConnectionConfig(defaultConfig);
			this.m_MaxDefConnections = maxDefaultConnections;
		}

		// Token: 0x06000185 RID: 389 RVA: 0x00005820 File Offset: 0x00003A20
		private HostTopology()
		{
		}

		// Token: 0x1700005A RID: 90
		// (get) Token: 0x06000186 RID: 390 RVA: 0x00005870 File Offset: 0x00003A70
		public ConnectionConfig DefaultConfig
		{
			get
			{
				return this.m_DefConfig;
			}
		}

		// Token: 0x1700005B RID: 91
		// (get) Token: 0x06000187 RID: 391 RVA: 0x0000588C File Offset: 0x00003A8C
		public int MaxDefaultConnections
		{
			get
			{
				return this.m_MaxDefConnections;
			}
		}

		// Token: 0x1700005C RID: 92
		// (get) Token: 0x06000188 RID: 392 RVA: 0x000058A8 File Offset: 0x00003AA8
		public int SpecialConnectionConfigsCount
		{
			get
			{
				return this.m_SpecialConnections.Count;
			}
		}

		// Token: 0x1700005D RID: 93
		// (get) Token: 0x06000189 RID: 393 RVA: 0x000058C8 File Offset: 0x00003AC8
		public List<ConnectionConfig> SpecialConnectionConfigs
		{
			get
			{
				return this.m_SpecialConnections;
			}
		}

		// Token: 0x0600018A RID: 394 RVA: 0x000058E4 File Offset: 0x00003AE4
		public ConnectionConfig GetSpecialConnectionConfig(int i)
		{
			if (i > this.m_SpecialConnections.Count || i == 0)
			{
				throw new ArgumentException("special configuration index is out of valid range");
			}
			return this.m_SpecialConnections[i - 1];
		}

		// Token: 0x1700005E RID: 94
		// (get) Token: 0x0600018B RID: 395 RVA: 0x0000592C File Offset: 0x00003B2C
		// (set) Token: 0x0600018C RID: 396 RVA: 0x00005947 File Offset: 0x00003B47
		public ushort ReceivedMessagePoolSize
		{
			get
			{
				return this.m_ReceivedMessagePoolSize;
			}
			set
			{
				this.m_ReceivedMessagePoolSize = value;
			}
		}

		// Token: 0x1700005F RID: 95
		// (get) Token: 0x0600018D RID: 397 RVA: 0x00005954 File Offset: 0x00003B54
		// (set) Token: 0x0600018E RID: 398 RVA: 0x0000596F File Offset: 0x00003B6F
		public ushort SentMessagePoolSize
		{
			get
			{
				return this.m_SentMessagePoolSize;
			}
			set
			{
				this.m_SentMessagePoolSize = value;
			}
		}

		// Token: 0x17000060 RID: 96
		// (get) Token: 0x0600018F RID: 399 RVA: 0x0000597C File Offset: 0x00003B7C
		// (set) Token: 0x06000190 RID: 400 RVA: 0x00005997 File Offset: 0x00003B97
		public float MessagePoolSizeGrowthFactor
		{
			get
			{
				return this.m_MessagePoolSizeGrowthFactor;
			}
			set
			{
				if ((double)value <= 0.5 || (double)value > 1.0)
				{
					throw new ArgumentException("pool growth factor should be varied between 0.5 and 1.0");
				}
				this.m_MessagePoolSizeGrowthFactor = value;
			}
		}

		// Token: 0x06000191 RID: 401 RVA: 0x000059CC File Offset: 0x00003BCC
		public int AddSpecialConnectionConfig(ConnectionConfig config)
		{
			if (this.m_MaxDefConnections + this.m_SpecialConnections.Count + 1 >= 65535)
			{
				throw new ArgumentOutOfRangeException("maxConnections", "Number of connections should be < 65535");
			}
			this.m_SpecialConnections.Add(new ConnectionConfig(config));
			return this.m_SpecialConnections.Count;
		}

		// Token: 0x040000B9 RID: 185
		[SerializeField]
		private ConnectionConfig m_DefConfig = null;

		// Token: 0x040000BA RID: 186
		[SerializeField]
		private int m_MaxDefConnections = 0;

		// Token: 0x040000BB RID: 187
		[SerializeField]
		private List<ConnectionConfig> m_SpecialConnections = new List<ConnectionConfig>();

		// Token: 0x040000BC RID: 188
		[SerializeField]
		private ushort m_ReceivedMessagePoolSize = 1024;

		// Token: 0x040000BD RID: 189
		[SerializeField]
		private ushort m_SentMessagePoolSize = 1024;

		// Token: 0x040000BE RID: 190
		[SerializeField]
		private float m_MessagePoolSizeGrowthFactor = 0.75f;
	}
}
