﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;

namespace UnityEngine.Networking
{
	// Token: 0x0200002F RID: 47
	[NativeConditional("ENABLE_NETWORK && ENABLE_UNET", true)]
	[NativeHeader("Runtime/Networking/UNETConfiguration.h")]
	internal class HostTopologyInternal : IDisposable
	{
		// Token: 0x060001D2 RID: 466 RVA: 0x0000613C File Offset: 0x0000433C
		public HostTopologyInternal(HostTopology topology)
		{
			ConnectionConfigInternal config = new ConnectionConfigInternal(topology.DefaultConfig);
			this.m_Ptr = HostTopologyInternal.InternalCreate(config, topology.MaxDefaultConnections);
			for (int i = 1; i <= topology.SpecialConnectionConfigsCount; i++)
			{
				ConnectionConfig specialConnectionConfig = topology.GetSpecialConnectionConfig(i);
				ConnectionConfigInternal config2 = new ConnectionConfigInternal(specialConnectionConfig);
				this.AddSpecialConnectionConfig(config2);
			}
			this.ReceivedMessagePoolSize = topology.ReceivedMessagePoolSize;
			this.SentMessagePoolSize = topology.SentMessagePoolSize;
			this.MessagePoolSizeGrowthFactor = topology.MessagePoolSizeGrowthFactor;
		}

		// Token: 0x060001D3 RID: 467 RVA: 0x000061C2 File Offset: 0x000043C2
		protected virtual void Dispose(bool disposing)
		{
			if (this.m_Ptr != IntPtr.Zero)
			{
				HostTopologyInternal.InternalDestroy(this.m_Ptr);
				this.m_Ptr = IntPtr.Zero;
			}
		}

		// Token: 0x060001D4 RID: 468 RVA: 0x000061F4 File Offset: 0x000043F4
		~HostTopologyInternal()
		{
			this.Dispose(false);
		}

		// Token: 0x060001D5 RID: 469 RVA: 0x000061C2 File Offset: 0x000043C2
		public void Dispose()
		{
			if (this.m_Ptr != IntPtr.Zero)
			{
				HostTopologyInternal.InternalDestroy(this.m_Ptr);
				this.m_Ptr = IntPtr.Zero;
			}
		}

		// Token: 0x060001D6 RID: 470
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern IntPtr InternalCreate(ConnectionConfigInternal config, int maxDefaultConnections);

		// Token: 0x060001D7 RID: 471
		[NativeMethod(IsThreadSafe = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void InternalDestroy(IntPtr ptr);

		// Token: 0x060001D8 RID: 472
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern ushort AddSpecialConnectionConfig(ConnectionConfigInternal config);

		// Token: 0x17000084 RID: 132
		// (set) Token: 0x060001D9 RID: 473
		[NativeProperty("m_ReceivedMessagePoolSize", TargetType.Field)]
		private extern ushort ReceivedMessagePoolSize { [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000085 RID: 133
		// (set) Token: 0x060001DA RID: 474
		[NativeProperty("m_SentMessagePoolSize", TargetType.Field)]
		private extern ushort SentMessagePoolSize { [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000086 RID: 134
		// (set) Token: 0x060001DB RID: 475
		[NativeProperty("m_MessagePoolSizeGrowthFactor", TargetType.Field)]
		private extern float MessagePoolSizeGrowthFactor { [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x040000D5 RID: 213
		public IntPtr m_Ptr;
	}
}
