﻿using System;

namespace UnityEngine.Networking
{
	// Token: 0x02000024 RID: 36
	public enum NetworkEventType
	{
		// Token: 0x04000073 RID: 115
		DataEvent,
		// Token: 0x04000074 RID: 116
		ConnectEvent,
		// Token: 0x04000075 RID: 117
		DisconnectEvent,
		// Token: 0x04000076 RID: 118
		Nothing,
		// Token: 0x04000077 RID: 119
		BroadcastEvent
	}
}
