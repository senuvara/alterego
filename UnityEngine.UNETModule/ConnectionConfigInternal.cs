﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine.Bindings;

namespace UnityEngine.Networking
{
	// Token: 0x0200002E RID: 46
	[NativeHeader("Runtime/Networking/UNETManager.h")]
	[NativeHeader("Runtime/Networking/UNETConfiguration.h")]
	[NativeHeader("Runtime/Networking/UNetTypes.h")]
	[NativeConditional("ENABLE_NETWORK && ENABLE_UNET", true)]
	[StructLayout(LayoutKind.Sequential)]
	internal class ConnectionConfigInternal : IDisposable
	{
		// Token: 0x060001B0 RID: 432 RVA: 0x00005E58 File Offset: 0x00004058
		public ConnectionConfigInternal(ConnectionConfig config)
		{
			if (config == null)
			{
				throw new NullReferenceException("config is not defined");
			}
			this.m_Ptr = ConnectionConfigInternal.InternalCreate();
			if (!this.SetPacketSize(config.PacketSize))
			{
				throw new ArgumentOutOfRangeException("PacketSize is too small");
			}
			this.FragmentSize = config.FragmentSize;
			this.ResendTimeout = config.ResendTimeout;
			this.DisconnectTimeout = config.DisconnectTimeout;
			this.ConnectTimeout = config.ConnectTimeout;
			this.MinUpdateTimeout = config.MinUpdateTimeout;
			this.PingTimeout = config.PingTimeout;
			this.ReducedPingTimeout = config.ReducedPingTimeout;
			this.AllCostTimeout = config.AllCostTimeout;
			this.NetworkDropThreshold = config.NetworkDropThreshold;
			this.OverflowDropThreshold = config.OverflowDropThreshold;
			this.MaxConnectionAttempt = config.MaxConnectionAttempt;
			this.AckDelay = config.AckDelay;
			this.SendDelay = config.SendDelay;
			this.MaxCombinedReliableMessageSize = config.MaxCombinedReliableMessageSize;
			this.MaxCombinedReliableMessageCount = config.MaxCombinedReliableMessageCount;
			this.MaxSentMessageQueueSize = config.MaxSentMessageQueueSize;
			this.AcksType = (byte)config.AcksType;
			this.UsePlatformSpecificProtocols = config.UsePlatformSpecificProtocols;
			this.InitialBandwidth = config.InitialBandwidth;
			this.BandwidthPeakFactor = config.BandwidthPeakFactor;
			this.WebSocketReceiveBufferMaxSize = config.WebSocketReceiveBufferMaxSize;
			this.UdpSocketReceiveBufferMaxSize = config.UdpSocketReceiveBufferMaxSize;
			if (config.SSLCertFilePath != null)
			{
				int num = this.SetSSLCertFilePath(config.SSLCertFilePath);
				if (num != 0)
				{
					throw new ArgumentOutOfRangeException("SSLCertFilePath cannot be > than " + num.ToString());
				}
			}
			if (config.SSLPrivateKeyFilePath != null)
			{
				int num2 = this.SetSSLPrivateKeyFilePath(config.SSLPrivateKeyFilePath);
				if (num2 != 0)
				{
					throw new ArgumentOutOfRangeException("SSLPrivateKeyFilePath cannot be > than " + num2.ToString());
				}
			}
			if (config.SSLCAFilePath != null)
			{
				int num3 = this.SetSSLCAFilePath(config.SSLCAFilePath);
				if (num3 != 0)
				{
					throw new ArgumentOutOfRangeException("SSLCAFilePath cannot be > than " + num3.ToString());
				}
			}
			byte b = 0;
			while ((int)b < config.ChannelCount)
			{
				this.AddChannel((int)((byte)config.GetChannel(b)));
				b += 1;
			}
			byte b2 = 0;
			while ((int)b2 < config.SharedOrderChannelCount)
			{
				IList<byte> sharedOrderChannels = config.GetSharedOrderChannels(b2);
				byte[] array = new byte[sharedOrderChannels.Count];
				sharedOrderChannels.CopyTo(array, 0);
				this.MakeChannelsSharedOrder(array);
				b2 += 1;
			}
		}

		// Token: 0x060001B1 RID: 433 RVA: 0x000060D6 File Offset: 0x000042D6
		protected virtual void Dispose(bool disposing)
		{
			if (this.m_Ptr != IntPtr.Zero)
			{
				ConnectionConfigInternal.InternalDestroy(this.m_Ptr);
				this.m_Ptr = IntPtr.Zero;
			}
		}

		// Token: 0x060001B2 RID: 434 RVA: 0x00006108 File Offset: 0x00004308
		~ConnectionConfigInternal()
		{
			this.Dispose(false);
		}

		// Token: 0x060001B3 RID: 435 RVA: 0x000060D6 File Offset: 0x000042D6
		public void Dispose()
		{
			if (this.m_Ptr != IntPtr.Zero)
			{
				ConnectionConfigInternal.InternalDestroy(this.m_Ptr);
				this.m_Ptr = IntPtr.Zero;
			}
		}

		// Token: 0x060001B4 RID: 436
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern IntPtr InternalCreate();

		// Token: 0x060001B5 RID: 437
		[NativeMethod(IsThreadSafe = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void InternalDestroy(IntPtr ptr);

		// Token: 0x060001B6 RID: 438
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern byte AddChannel(int value);

		// Token: 0x060001B7 RID: 439
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern bool SetPacketSize(ushort value);

		// Token: 0x1700006E RID: 110
		// (set) Token: 0x060001B8 RID: 440
		[NativeProperty("m_ProtocolRequired.m_FragmentSize", TargetType.Field)]
		private extern ushort FragmentSize { [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700006F RID: 111
		// (set) Token: 0x060001B9 RID: 441
		[NativeProperty("m_ProtocolRequired.m_ResendTimeout", TargetType.Field)]
		private extern uint ResendTimeout { [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000070 RID: 112
		// (set) Token: 0x060001BA RID: 442
		[NativeProperty("m_ProtocolRequired.m_DisconnectTimeout", TargetType.Field)]
		private extern uint DisconnectTimeout { [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000071 RID: 113
		// (set) Token: 0x060001BB RID: 443
		[NativeProperty("m_ProtocolRequired.m_ConnectTimeout", TargetType.Field)]
		private extern uint ConnectTimeout { [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000072 RID: 114
		// (set) Token: 0x060001BC RID: 444
		[NativeProperty("m_ProtocolOptional.m_MinUpdateTimeout", TargetType.Field)]
		private extern uint MinUpdateTimeout { [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000073 RID: 115
		// (set) Token: 0x060001BD RID: 445
		[NativeProperty("m_ProtocolRequired.m_PingTimeout", TargetType.Field)]
		private extern uint PingTimeout { [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000074 RID: 116
		// (set) Token: 0x060001BE RID: 446
		[NativeProperty("m_ProtocolRequired.m_ReducedPingTimeout", TargetType.Field)]
		private extern uint ReducedPingTimeout { [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000075 RID: 117
		// (set) Token: 0x060001BF RID: 447
		[NativeProperty("m_ProtocolRequired.m_AllCostTimeout", TargetType.Field)]
		private extern uint AllCostTimeout { [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000076 RID: 118
		// (set) Token: 0x060001C0 RID: 448
		[NativeProperty("m_ProtocolOptional.m_NetworkDropThreshold", TargetType.Field)]
		private extern byte NetworkDropThreshold { [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000077 RID: 119
		// (set) Token: 0x060001C1 RID: 449
		[NativeProperty("m_ProtocolOptional.m_OverflowDropThreshold", TargetType.Field)]
		private extern byte OverflowDropThreshold { [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000078 RID: 120
		// (set) Token: 0x060001C2 RID: 450
		[NativeProperty("m_ProtocolOptional.m_MaxConnectionAttempt", TargetType.Field)]
		private extern byte MaxConnectionAttempt { [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000079 RID: 121
		// (set) Token: 0x060001C3 RID: 451
		[NativeProperty("m_ProtocolOptional.m_AckDelay", TargetType.Field)]
		private extern uint AckDelay { [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700007A RID: 122
		// (set) Token: 0x060001C4 RID: 452
		[NativeProperty("m_ProtocolOptional.m_SendDelay", TargetType.Field)]
		private extern uint SendDelay { [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700007B RID: 123
		// (set) Token: 0x060001C5 RID: 453
		[NativeProperty("m_ProtocolOptional.m_MaxCombinedReliableMessageSize", TargetType.Field)]
		private extern ushort MaxCombinedReliableMessageSize { [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700007C RID: 124
		// (set) Token: 0x060001C6 RID: 454
		[NativeProperty("m_ProtocolOptional.m_MaxCombinedReliableMessageAmount", TargetType.Field)]
		private extern ushort MaxCombinedReliableMessageCount { [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700007D RID: 125
		// (set) Token: 0x060001C7 RID: 455
		[NativeProperty("m_ProtocolOptional.m_MaxSentMessageQueueSize", TargetType.Field)]
		private extern ushort MaxSentMessageQueueSize { [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700007E RID: 126
		// (set) Token: 0x060001C8 RID: 456
		[NativeProperty("m_ProtocolRequired.m_AcksType", TargetType.Field)]
		private extern byte AcksType { [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700007F RID: 127
		// (set) Token: 0x060001C9 RID: 457
		[NativeProperty("m_ProtocolRequired.m_UsePlatformSpecificProtocols", TargetType.Field)]
		private extern bool UsePlatformSpecificProtocols { [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000080 RID: 128
		// (set) Token: 0x060001CA RID: 458
		[NativeProperty("m_ProtocolOptional.m_InitialBandwidth", TargetType.Field)]
		private extern uint InitialBandwidth { [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000081 RID: 129
		// (set) Token: 0x060001CB RID: 459
		[NativeProperty("m_ProtocolOptional.m_BandwidthPeakFactor", TargetType.Field)]
		private extern float BandwidthPeakFactor { [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000082 RID: 130
		// (set) Token: 0x060001CC RID: 460
		[NativeProperty("m_ProtocolOptional.m_WebSocketReceiveBufferMaxSize", TargetType.Field)]
		private extern ushort WebSocketReceiveBufferMaxSize { [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000083 RID: 131
		// (set) Token: 0x060001CD RID: 461
		[NativeProperty("m_ProtocolOptional.m_UdpSocketReceiveBufferMaxSize", TargetType.Field)]
		private extern uint UdpSocketReceiveBufferMaxSize { [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x060001CE RID: 462
		[NativeMethod("SetSSLCertFilePath")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern int SetSSLCertFilePath(string value);

		// Token: 0x060001CF RID: 463
		[NativeMethod("SetSSLPrivateKeyFilePath")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern int SetSSLPrivateKeyFilePath(string value);

		// Token: 0x060001D0 RID: 464
		[NativeMethod("SetSSLCAFilePath")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern int SetSSLCAFilePath(string value);

		// Token: 0x060001D1 RID: 465
		[NativeMethod("MakeChannelsSharedOrder")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern bool MakeChannelsSharedOrder(byte[] values);

		// Token: 0x040000D4 RID: 212
		public IntPtr m_Ptr;
	}
}
