﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine.Bindings;
using UnityEngine.Networking.Types;

namespace UnityEngine.Networking
{
	// Token: 0x02000023 RID: 35
	[Obsolete("The UNET transport will be removed in the future as soon a replacement is ready.")]
	[NativeHeader("Runtime/Networking/UNETConfiguration.h")]
	[NativeHeader("Runtime/Networking/UNetTypes.h")]
	[NativeHeader("Runtime/Networking/UNETManager.h")]
	[NativeConditional("ENABLE_NETWORK && ENABLE_UNET", true)]
	public sealed class NetworkTransport
	{
		// Token: 0x060000D3 RID: 211 RVA: 0x00002F2E File Offset: 0x0000112E
		private NetworkTransport()
		{
		}

		// Token: 0x060000D4 RID: 212 RVA: 0x0000422C File Offset: 0x0000242C
		internal static bool DoesEndPointUsePlatformProtocols(EndPoint endPoint)
		{
			if (endPoint.GetType().FullName == "UnityEngine.PS4.SceEndPoint")
			{
				SocketAddress socketAddress = endPoint.Serialize();
				if (socketAddress[8] != 0 || socketAddress[9] != 0)
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x060000D5 RID: 213 RVA: 0x00004288 File Offset: 0x00002488
		public static int ConnectEndPoint(int hostId, EndPoint endPoint, int exceptionConnectionId, out byte error)
		{
			error = 0;
			byte[] array = new byte[]
			{
				95,
				36,
				19,
				246
			};
			if (endPoint == null)
			{
				throw new NullReferenceException("Null EndPoint provided");
			}
			if (endPoint.GetType().FullName != "UnityEngine.XboxOne.XboxOneEndPoint" && endPoint.GetType().FullName != "UnityEngine.PS4.SceEndPoint")
			{
				throw new ArgumentException("Endpoint of type XboxOneEndPoint or SceEndPoint  required");
			}
			int result;
			if (endPoint.GetType().FullName == "UnityEngine.XboxOne.XboxOneEndPoint")
			{
				if (endPoint.AddressFamily != AddressFamily.InterNetworkV6)
				{
					throw new ArgumentException("XboxOneEndPoint has an invalid family");
				}
				SocketAddress socketAddress = endPoint.Serialize();
				if (socketAddress.Size != 14)
				{
					throw new ArgumentException("XboxOneEndPoint has an invalid size");
				}
				if (socketAddress[0] != 0 || socketAddress[1] != 0)
				{
					throw new ArgumentException("XboxOneEndPoint has an invalid family signature");
				}
				if (socketAddress[2] != array[0] || socketAddress[3] != array[1] || socketAddress[4] != array[2] || socketAddress[5] != array[3])
				{
					throw new ArgumentException("XboxOneEndPoint has an invalid signature");
				}
				byte[] array2 = new byte[8];
				for (int i = 0; i < array2.Length; i++)
				{
					array2[i] = socketAddress[6 + i];
				}
				IntPtr intPtr = new IntPtr(BitConverter.ToInt64(array2, 0));
				if (intPtr == IntPtr.Zero)
				{
					throw new ArgumentException("XboxOneEndPoint has an invalid SOCKET_STORAGE pointer");
				}
				byte[] array3 = new byte[2];
				Marshal.Copy(intPtr, array3, 0, array3.Length);
				AddressFamily addressFamily = (AddressFamily)(((int)array3[1] << 8) + (int)array3[0]);
				if (addressFamily != AddressFamily.InterNetworkV6)
				{
					throw new ArgumentException("XboxOneEndPoint has corrupt or invalid SOCKET_STORAGE pointer");
				}
				result = NetworkTransport.Internal_ConnectEndPoint(hostId, array2, 128, exceptionConnectionId, out error);
			}
			else
			{
				SocketAddress socketAddress2 = endPoint.Serialize();
				if (socketAddress2.Size != 16)
				{
					throw new ArgumentException("EndPoint has an invalid size");
				}
				if ((int)socketAddress2[0] != socketAddress2.Size)
				{
					throw new ArgumentException("EndPoint has an invalid size value");
				}
				if (socketAddress2[1] != 2)
				{
					throw new ArgumentException("EndPoint has an invalid family value");
				}
				byte[] array4 = new byte[16];
				for (int j = 0; j < array4.Length; j++)
				{
					array4[j] = socketAddress2[j];
				}
				int num = NetworkTransport.Internal_ConnectEndPoint(hostId, array4, 16, exceptionConnectionId, out error);
				result = num;
			}
			return result;
		}

		// Token: 0x060000D6 RID: 214 RVA: 0x00004503 File Offset: 0x00002703
		public static void Init()
		{
			NetworkTransport.InitializeClass();
		}

		// Token: 0x060000D7 RID: 215 RVA: 0x0000450B File Offset: 0x0000270B
		public static void Init(GlobalConfig config)
		{
			if (config.NetworkEventAvailable != null)
			{
				NetworkTransport.SetNetworkEventAvailableCallback(config.NetworkEventAvailable);
			}
			if (config.ConnectionReadyForSend != null)
			{
				NetworkTransport.SetConnectionReadyForSendCallback(config.ConnectionReadyForSend);
			}
			NetworkTransport.InitializeClassWithConfig(new GlobalConfigInternal(config));
		}

		// Token: 0x060000D8 RID: 216
		[FreeFunction("UNETManager::InitializeClass")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void InitializeClass();

		// Token: 0x060000D9 RID: 217
		[FreeFunction("UNETManager::InitializeClassWithConfig")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void InitializeClassWithConfig(GlobalConfigInternal config);

		// Token: 0x060000DA RID: 218 RVA: 0x00004545 File Offset: 0x00002745
		public static void Shutdown()
		{
			NetworkTransport.Cleanup();
		}

		// Token: 0x060000DB RID: 219 RVA: 0x00004550 File Offset: 0x00002750
		[Obsolete("This function has been deprecated. Use AssetDatabase utilities instead.")]
		public static string GetAssetId(GameObject go)
		{
			return "";
		}

		// Token: 0x060000DC RID: 220 RVA: 0x0000456A File Offset: 0x0000276A
		public static void AddSceneId(int id)
		{
			if (id > NetworkTransport.s_nextSceneId)
			{
				NetworkTransport.s_nextSceneId = id + 1;
			}
		}

		// Token: 0x060000DD RID: 221 RVA: 0x00004584 File Offset: 0x00002784
		public static int GetNextSceneId()
		{
			return NetworkTransport.s_nextSceneId++;
		}

		// Token: 0x060000DE RID: 222 RVA: 0x000045A8 File Offset: 0x000027A8
		public static int AddHostWithSimulator(HostTopology topology, int minTimeout, int maxTimeout, int port, string ip)
		{
			if (topology == null)
			{
				throw new NullReferenceException("topology is not defined");
			}
			NetworkTransport.CheckTopology(topology);
			return NetworkTransport.AddHostInternal(new HostTopologyInternal(topology), ip, port, minTimeout, maxTimeout);
		}

		// Token: 0x060000DF RID: 223 RVA: 0x000045E4 File Offset: 0x000027E4
		public static int AddHostWithSimulator(HostTopology topology, int minTimeout, int maxTimeout, int port)
		{
			return NetworkTransport.AddHostWithSimulator(topology, minTimeout, maxTimeout, port, null);
		}

		// Token: 0x060000E0 RID: 224 RVA: 0x00004604 File Offset: 0x00002804
		public static int AddHostWithSimulator(HostTopology topology, int minTimeout, int maxTimeout)
		{
			return NetworkTransport.AddHostWithSimulator(topology, minTimeout, maxTimeout, 0, null);
		}

		// Token: 0x060000E1 RID: 225 RVA: 0x00004624 File Offset: 0x00002824
		public static int AddHost(HostTopology topology, int port, string ip)
		{
			return NetworkTransport.AddHostWithSimulator(topology, 0, 0, port, ip);
		}

		// Token: 0x060000E2 RID: 226 RVA: 0x00004644 File Offset: 0x00002844
		public static int AddHost(HostTopology topology, int port)
		{
			return NetworkTransport.AddHost(topology, port, null);
		}

		// Token: 0x060000E3 RID: 227 RVA: 0x00004664 File Offset: 0x00002864
		public static int AddHost(HostTopology topology)
		{
			return NetworkTransport.AddHost(topology, 0, null);
		}

		// Token: 0x060000E4 RID: 228
		[FreeFunction("UNETManager::Get()->AddHost")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int AddHostInternal(HostTopologyInternal topologyInt, string ip, int port, int minTimeout, int maxTimeout);

		// Token: 0x060000E5 RID: 229 RVA: 0x00004684 File Offset: 0x00002884
		public static int AddWebsocketHost(HostTopology topology, int port, string ip)
		{
			if (port != 0)
			{
				if (NetworkTransport.IsPortOpen(ip, port))
				{
					throw new InvalidOperationException("Cannot open web socket on port " + port + " It has been already occupied.");
				}
			}
			if (topology == null)
			{
				throw new NullReferenceException("topology is not defined");
			}
			NetworkTransport.CheckTopology(topology);
			return NetworkTransport.AddWsHostInternal(new HostTopologyInternal(topology), ip, port);
		}

		// Token: 0x060000E6 RID: 230 RVA: 0x000046EC File Offset: 0x000028EC
		public static int AddWebsocketHost(HostTopology topology, int port)
		{
			return NetworkTransport.AddWebsocketHost(topology, port, null);
		}

		// Token: 0x060000E7 RID: 231
		[FreeFunction("UNETManager::Get()->AddWsHost")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int AddWsHostInternal(HostTopologyInternal topologyInt, string ip, int port);

		// Token: 0x060000E8 RID: 232 RVA: 0x0000470C File Offset: 0x0000290C
		private static bool IsPortOpen(string ip, int port)
		{
			TimeSpan timeout = TimeSpan.FromMilliseconds(500.0);
			string host = (ip != null) ? ip : "127.0.0.1";
			try
			{
				using (TcpClient tcpClient = new TcpClient())
				{
					IAsyncResult asyncResult = tcpClient.BeginConnect(host, port, null, null);
					if (!asyncResult.AsyncWaitHandle.WaitOne(timeout))
					{
						return false;
					}
					tcpClient.EndConnect(asyncResult);
				}
			}
			catch
			{
				return false;
			}
			return true;
		}

		// Token: 0x060000E9 RID: 233 RVA: 0x000047BC File Offset: 0x000029BC
		public static void ConnectAsNetworkHost(int hostId, string address, int port, NetworkID network, SourceID source, NodeID node, out byte error)
		{
			NetworkTransport.ConnectAsNetworkHostInternal(hostId, address, port, (ulong)network, (ulong)source, (ushort)node, out error);
		}

		// Token: 0x060000EA RID: 234
		[FreeFunction("UNETManager::Get()->ConnectAsNetworkHost")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void ConnectAsNetworkHostInternal(int hostId, string address, int port, ulong network, ulong source, ushort node, out byte error);

		// Token: 0x060000EB RID: 235
		[FreeFunction("UNETManager::Get()->DisconnectNetworkHost")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void DisconnectNetworkHost(int hostId, out byte error);

		// Token: 0x060000EC RID: 236 RVA: 0x000047D0 File Offset: 0x000029D0
		public static NetworkEventType ReceiveRelayEventFromHost(int hostId, out byte error)
		{
			return (NetworkEventType)NetworkTransport.ReceiveRelayEventFromHostInternal(hostId, out error);
		}

		// Token: 0x060000ED RID: 237
		[FreeFunction("UNETManager::Get()->PopRelayHostData")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int ReceiveRelayEventFromHostInternal(int hostId, out byte error);

		// Token: 0x060000EE RID: 238 RVA: 0x000047EC File Offset: 0x000029EC
		public static int ConnectToNetworkPeer(int hostId, string address, int port, int exceptionConnectionId, int relaySlotId, NetworkID network, SourceID source, NodeID node, int bytesPerSec, float bucketSizeFactor, out byte error)
		{
			return NetworkTransport.ConnectToNetworkPeerInternal(hostId, address, port, exceptionConnectionId, relaySlotId, (ulong)network, (ulong)source, (ushort)node, bytesPerSec, bucketSizeFactor, out error);
		}

		// Token: 0x060000EF RID: 239 RVA: 0x00004818 File Offset: 0x00002A18
		public static int ConnectToNetworkPeer(int hostId, string address, int port, int exceptionConnectionId, int relaySlotId, NetworkID network, SourceID source, NodeID node, out byte error)
		{
			return NetworkTransport.ConnectToNetworkPeer(hostId, address, port, exceptionConnectionId, relaySlotId, network, source, node, 0, 0f, out error);
		}

		// Token: 0x060000F0 RID: 240
		[FreeFunction("UNETManager::Get()->ConnectToNetworkPeer")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int ConnectToNetworkPeerInternal(int hostId, string address, int port, int exceptionConnectionId, int relaySlotId, ulong network, ulong source, ushort node, int bytesPerSec, float bucketSizeFactor, out byte error);

		// Token: 0x060000F1 RID: 241 RVA: 0x00004848 File Offset: 0x00002A48
		[Obsolete("GetCurrentIncomingMessageAmount has been deprecated.")]
		public static int GetCurrentIncomingMessageAmount()
		{
			return 0;
		}

		// Token: 0x060000F2 RID: 242 RVA: 0x00004860 File Offset: 0x00002A60
		[Obsolete("GetCurrentOutgoingMessageAmount has been deprecated.")]
		public static int GetCurrentOutgoingMessageAmount()
		{
			return 0;
		}

		// Token: 0x060000F3 RID: 243
		[FreeFunction("UNETManager::Get()->GetIncomingMessageQueueSize")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern int GetIncomingMessageQueueSize(int hostId, out byte error);

		// Token: 0x060000F4 RID: 244
		[FreeFunction("UNETManager::Get()->GetOutgoingMessageQueueSize")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern int GetOutgoingMessageQueueSize(int hostId, out byte error);

		// Token: 0x060000F5 RID: 245
		[FreeFunction("UNETManager::Get()->GetCurrentRTT")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern int GetCurrentRTT(int hostId, int connectionId, out byte error);

		// Token: 0x060000F6 RID: 246 RVA: 0x00004878 File Offset: 0x00002A78
		[Obsolete("GetCurrentRtt() has been deprecated.")]
		public static int GetCurrentRtt(int hostId, int connectionId, out byte error)
		{
			return NetworkTransport.GetCurrentRTT(hostId, connectionId, out error);
		}

		// Token: 0x060000F7 RID: 247
		[FreeFunction("UNETManager::Get()->GetIncomingPacketLossCount")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern int GetIncomingPacketLossCount(int hostId, int connectionId, out byte error);

		// Token: 0x060000F8 RID: 248 RVA: 0x00004898 File Offset: 0x00002A98
		[Obsolete("GetNetworkLostPacketNum() has been deprecated.")]
		public static int GetNetworkLostPacketNum(int hostId, int connectionId, out byte error)
		{
			return NetworkTransport.GetIncomingPacketLossCount(hostId, connectionId, out error);
		}

		// Token: 0x060000F9 RID: 249
		[FreeFunction("UNETManager::Get()->GetIncomingPacketCount")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern int GetIncomingPacketCount(int hostId, int connectionId, out byte error);

		// Token: 0x060000FA RID: 250
		[FreeFunction("UNETManager::Get()->GetOutgoingPacketNetworkLossPercent")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern int GetOutgoingPacketNetworkLossPercent(int hostId, int connectionId, out byte error);

		// Token: 0x060000FB RID: 251
		[FreeFunction("UNETManager::Get()->GetOutgoingPacketOverflowLossPercent")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern int GetOutgoingPacketOverflowLossPercent(int hostId, int connectionId, out byte error);

		// Token: 0x060000FC RID: 252
		[FreeFunction("UNETManager::Get()->GetMaxAllowedBandwidth")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern int GetMaxAllowedBandwidth(int hostId, int connectionId, out byte error);

		// Token: 0x060000FD RID: 253
		[FreeFunction("UNETManager::Get()->GetAckBufferCount")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern int GetAckBufferCount(int hostId, int connectionId, out byte error);

		// Token: 0x060000FE RID: 254
		[FreeFunction("UNETManager::Get()->GetIncomingPacketDropCountForAllHosts")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern int GetIncomingPacketDropCountForAllHosts();

		// Token: 0x060000FF RID: 255
		[FreeFunction("UNETManager::Get()->GetIncomingPacketCountForAllHosts")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern int GetIncomingPacketCountForAllHosts();

		// Token: 0x06000100 RID: 256
		[FreeFunction("UNETManager::Get()->GetOutgoingPacketCount")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern int GetOutgoingPacketCount();

		// Token: 0x06000101 RID: 257
		[FreeFunction("UNETManager::Get()->GetOutgoingPacketCount")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern int GetOutgoingPacketCountForHost(int hostId, out byte error);

		// Token: 0x06000102 RID: 258
		[FreeFunction("UNETManager::Get()->GetOutgoingPacketCount")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern int GetOutgoingPacketCountForConnection(int hostId, int connectionId, out byte error);

		// Token: 0x06000103 RID: 259
		[FreeFunction("UNETManager::Get()->GetOutgoingMessageCount")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern int GetOutgoingMessageCount();

		// Token: 0x06000104 RID: 260
		[FreeFunction("UNETManager::Get()->GetOutgoingMessageCount")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern int GetOutgoingMessageCountForHost(int hostId, out byte error);

		// Token: 0x06000105 RID: 261
		[FreeFunction("UNETManager::Get()->GetOutgoingMessageCount")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern int GetOutgoingMessageCountForConnection(int hostId, int connectionId, out byte error);

		// Token: 0x06000106 RID: 262
		[FreeFunction("UNETManager::Get()->GetOutgoingUserBytesCount")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern int GetOutgoingUserBytesCount();

		// Token: 0x06000107 RID: 263
		[FreeFunction("UNETManager::Get()->GetOutgoingUserBytesCount")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern int GetOutgoingUserBytesCountForHost(int hostId, out byte error);

		// Token: 0x06000108 RID: 264
		[FreeFunction("UNETManager::Get()->GetOutgoingUserBytesCount")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern int GetOutgoingUserBytesCountForConnection(int hostId, int connectionId, out byte error);

		// Token: 0x06000109 RID: 265
		[FreeFunction("UNETManager::Get()->GetOutgoingSystemBytesCount")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern int GetOutgoingSystemBytesCount();

		// Token: 0x0600010A RID: 266
		[FreeFunction("UNETManager::Get()->GetOutgoingSystemBytesCount")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern int GetOutgoingSystemBytesCountForHost(int hostId, out byte error);

		// Token: 0x0600010B RID: 267
		[FreeFunction("UNETManager::Get()->GetOutgoingSystemBytesCount")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern int GetOutgoingSystemBytesCountForConnection(int hostId, int connectionId, out byte error);

		// Token: 0x0600010C RID: 268
		[FreeFunction("UNETManager::Get()->GetOutgoingFullBytesCount")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern int GetOutgoingFullBytesCount();

		// Token: 0x0600010D RID: 269
		[FreeFunction("UNETManager::Get()->GetOutgoingFullBytesCount")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern int GetOutgoingFullBytesCountForHost(int hostId, out byte error);

		// Token: 0x0600010E RID: 270
		[FreeFunction("UNETManager::Get()->GetOutgoingFullBytesCount")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern int GetOutgoingFullBytesCountForConnection(int hostId, int connectionId, out byte error);

		// Token: 0x0600010F RID: 271 RVA: 0x000048B8 File Offset: 0x00002AB8
		[Obsolete("GetPacketSentRate has been deprecated.")]
		public static int GetPacketSentRate(int hostId, int connectionId, out byte error)
		{
			error = 0;
			return 0;
		}

		// Token: 0x06000110 RID: 272 RVA: 0x000048D4 File Offset: 0x00002AD4
		[Obsolete("GetPacketReceivedRate has been deprecated.")]
		public static int GetPacketReceivedRate(int hostId, int connectionId, out byte error)
		{
			error = 0;
			return 0;
		}

		// Token: 0x06000111 RID: 273 RVA: 0x000048F0 File Offset: 0x00002AF0
		[Obsolete("GetRemotePacketReceivedRate has been deprecated.")]
		public static int GetRemotePacketReceivedRate(int hostId, int connectionId, out byte error)
		{
			error = 0;
			return 0;
		}

		// Token: 0x06000112 RID: 274 RVA: 0x0000490C File Offset: 0x00002B0C
		[Obsolete("GetNetIOTimeuS has been deprecated.")]
		public static int GetNetIOTimeuS()
		{
			return 0;
		}

		// Token: 0x06000113 RID: 275
		[FreeFunction("UNETManager::Get()->GetConnectionInfo")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern string GetConnectionInfo(int hostId, int connectionId, out int port, out ulong network, out ushort dstNode, out byte error);

		// Token: 0x06000114 RID: 276 RVA: 0x00004924 File Offset: 0x00002B24
		public static void GetConnectionInfo(int hostId, int connectionId, out string address, out int port, out NetworkID network, out NodeID dstNode, out byte error)
		{
			ulong num;
			ushort num2;
			address = NetworkTransport.GetConnectionInfo(hostId, connectionId, out port, out num, out num2, out error);
			network = (NetworkID)num;
			dstNode = (NodeID)num2;
		}

		// Token: 0x06000115 RID: 277
		[FreeFunction("UNETManager::Get()->GetNetworkTimestamp")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern int GetNetworkTimestamp();

		// Token: 0x06000116 RID: 278
		[FreeFunction("UNETManager::Get()->GetRemoteDelayTimeMS")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern int GetRemoteDelayTimeMS(int hostId, int connectionId, int remoteTime, out byte error);

		// Token: 0x06000117 RID: 279 RVA: 0x0000494C File Offset: 0x00002B4C
		public static bool StartSendMulticast(int hostId, int channelId, byte[] buffer, int size, out byte error)
		{
			return NetworkTransport.StartSendMulticastInternal(hostId, channelId, buffer, size, out error);
		}

		// Token: 0x06000118 RID: 280
		[FreeFunction("UNETManager::Get()->StartSendMulticast")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool StartSendMulticastInternal(int hostId, int channelId, [Out] byte[] buffer, int size, out byte error);

		// Token: 0x06000119 RID: 281
		[FreeFunction("UNETManager::Get()->SendMulticast")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern bool SendMulticast(int hostId, int connectionId, out byte error);

		// Token: 0x0600011A RID: 282
		[FreeFunction("UNETManager::Get()->FinishSendMulticast")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern bool FinishSendMulticast(int hostId, out byte error);

		// Token: 0x0600011B RID: 283
		[FreeFunction("UNETManager::Get()->GetMaxPacketSize")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int GetMaxPacketSize();

		// Token: 0x0600011C RID: 284
		[FreeFunction("UNETManager::Get()->RemoveHost")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern bool RemoveHost(int hostId);

		// Token: 0x17000039 RID: 57
		// (get) Token: 0x0600011D RID: 285 RVA: 0x0000496C File Offset: 0x00002B6C
		public static bool IsStarted
		{
			get
			{
				return NetworkTransport.IsStartedInternal();
			}
		}

		// Token: 0x0600011E RID: 286
		[FreeFunction("UNETManager::IsStarted")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool IsStartedInternal();

		// Token: 0x0600011F RID: 287
		[FreeFunction("UNETManager::Get()->Connect")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern int Connect(int hostId, string address, int port, int exeptionConnectionId, out byte error);

		// Token: 0x06000120 RID: 288
		[FreeFunction("UNETManager::Get()->ConnectWithSimulator")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int ConnectWithSimulatorInternal(int hostId, string address, int port, int exeptionConnectionId, out byte error, ConnectionSimulatorConfigInternal conf);

		// Token: 0x06000121 RID: 289 RVA: 0x00004988 File Offset: 0x00002B88
		public static int ConnectWithSimulator(int hostId, string address, int port, int exeptionConnectionId, out byte error, ConnectionSimulatorConfig conf)
		{
			return NetworkTransport.ConnectWithSimulatorInternal(hostId, address, port, exeptionConnectionId, out error, new ConnectionSimulatorConfigInternal(conf));
		}

		// Token: 0x06000122 RID: 290
		[FreeFunction("UNETManager::Get()->Disconnect")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern bool Disconnect(int hostId, int connectionId, out byte error);

		// Token: 0x06000123 RID: 291
		[FreeFunction("UNETManager::Get()->ConnectSockAddr")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int Internal_ConnectEndPoint(int hostId, [Out] byte[] sockAddrStorage, int sockAddrStorageLen, int exceptionConnectionId, out byte error);

		// Token: 0x06000124 RID: 292 RVA: 0x000049B0 File Offset: 0x00002BB0
		public static bool Send(int hostId, int connectionId, int channelId, byte[] buffer, int size, out byte error)
		{
			if (buffer == null)
			{
				throw new NullReferenceException("send buffer is not initialized");
			}
			return NetworkTransport.SendWrapper(hostId, connectionId, channelId, buffer, size, out error);
		}

		// Token: 0x06000125 RID: 293
		[FreeFunction("UNETManager::Get()->Send")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool SendWrapper(int hostId, int connectionId, int channelId, [Out] byte[] buffer, int size, out byte error);

		// Token: 0x06000126 RID: 294 RVA: 0x000049E4 File Offset: 0x00002BE4
		public static bool QueueMessageForSending(int hostId, int connectionId, int channelId, byte[] buffer, int size, out byte error)
		{
			if (buffer == null)
			{
				throw new NullReferenceException("send buffer is not initialized");
			}
			return NetworkTransport.QueueMessageForSendingWrapper(hostId, connectionId, channelId, buffer, size, out error);
		}

		// Token: 0x06000127 RID: 295
		[FreeFunction("UNETManager::Get()->QueueMessageForSending")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool QueueMessageForSendingWrapper(int hostId, int connectionId, int channelId, [Out] byte[] buffer, int size, out byte error);

		// Token: 0x06000128 RID: 296
		[FreeFunction("UNETManager::Get()->SendQueuedMessages")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern bool SendQueuedMessages(int hostId, int connectionId, out byte error);

		// Token: 0x06000129 RID: 297 RVA: 0x00004A18 File Offset: 0x00002C18
		public static NetworkEventType Receive(out int hostId, out int connectionId, out int channelId, byte[] buffer, int bufferSize, out int receivedSize, out byte error)
		{
			return (NetworkEventType)NetworkTransport.PopData(out hostId, out connectionId, out channelId, buffer, bufferSize, out receivedSize, out error);
		}

		// Token: 0x0600012A RID: 298
		[FreeFunction("UNETManager::Get()->PopData")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int PopData(out int hostId, out int connectionId, out int channelId, [Out] byte[] buffer, int bufferSize, out int receivedSize, out byte error);

		// Token: 0x0600012B RID: 299 RVA: 0x00004A3C File Offset: 0x00002C3C
		public static NetworkEventType ReceiveFromHost(int hostId, out int connectionId, out int channelId, byte[] buffer, int bufferSize, out int receivedSize, out byte error)
		{
			return (NetworkEventType)NetworkTransport.PopDataFromHost(hostId, out connectionId, out channelId, buffer, bufferSize, out receivedSize, out error);
		}

		// Token: 0x0600012C RID: 300
		[FreeFunction("UNETManager::Get()->PopDataFromHost")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int PopDataFromHost(int hostId, out int connectionId, out int channelId, [Out] byte[] buffer, int bufferSize, out int receivedSize, out byte error);

		// Token: 0x0600012D RID: 301
		[FreeFunction("UNETManager::Get()->SetPacketStat")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void SetPacketStat(int direction, int packetStatId, int numMsgs, int numBytes);

		// Token: 0x0600012E RID: 302
		[NativeThrows]
		[FreeFunction("UNETManager::SetNetworkEventAvailableCallback")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void SetNetworkEventAvailableCallback(Action<int> callback);

		// Token: 0x0600012F RID: 303
		[FreeFunction("UNETManager::Cleanup")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void Cleanup();

		// Token: 0x06000130 RID: 304
		[NativeThrows]
		[FreeFunction("UNETManager::SetConnectionReadyForSendCallback")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void SetConnectionReadyForSendCallback(Action<int, int> callback);

		// Token: 0x06000131 RID: 305
		[FreeFunction("UNETManager::Get()->NotifyWhenConnectionReadyForSend")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern bool NotifyWhenConnectionReadyForSend(int hostId, int connectionId, int notificationLevel, out byte error);

		// Token: 0x06000132 RID: 306
		[FreeFunction("UNETManager::Get()->GetHostPort")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern int GetHostPort(int hostId);

		// Token: 0x06000133 RID: 307
		[FreeFunction("UNETManager::Get()->StartBroadcastDiscoveryWithData")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool StartBroadcastDiscoveryWithData(int hostId, int broadcastPort, int key, int version, int subversion, [Out] byte[] buffer, int size, int timeout, out byte error);

		// Token: 0x06000134 RID: 308
		[FreeFunction("UNETManager::Get()->StartBroadcastDiscoveryWithoutData")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool StartBroadcastDiscoveryWithoutData(int hostId, int broadcastPort, int key, int version, int subversion, int timeout, out byte error);

		// Token: 0x06000135 RID: 309 RVA: 0x00004A60 File Offset: 0x00002C60
		public static bool StartBroadcastDiscovery(int hostId, int broadcastPort, int key, int version, int subversion, byte[] buffer, int size, int timeout, out byte error)
		{
			if (buffer != null)
			{
				if (buffer.Length < size)
				{
					throw new ArgumentOutOfRangeException(string.Concat(new object[]
					{
						"Size: ",
						size,
						" > buffer.Length ",
						buffer.Length
					}));
				}
				if (size == 0)
				{
					throw new ArgumentOutOfRangeException("Size is zero while buffer exists, please pass null and 0 as buffer and size parameters");
				}
			}
			bool result;
			if (buffer == null)
			{
				result = NetworkTransport.StartBroadcastDiscoveryWithoutData(hostId, broadcastPort, key, version, subversion, timeout, out error);
			}
			else
			{
				result = NetworkTransport.StartBroadcastDiscoveryWithData(hostId, broadcastPort, key, version, subversion, buffer, size, timeout, out error);
			}
			return result;
		}

		// Token: 0x06000136 RID: 310
		[FreeFunction("UNETManager::Get()->StopBroadcastDiscovery")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void StopBroadcastDiscovery();

		// Token: 0x06000137 RID: 311
		[FreeFunction("UNETManager::Get()->IsBroadcastDiscoveryRunning")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern bool IsBroadcastDiscoveryRunning();

		// Token: 0x06000138 RID: 312
		[FreeFunction("UNETManager::Get()->SetBroadcastCredentials")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void SetBroadcastCredentials(int hostId, int key, int version, int subversion, out byte error);

		// Token: 0x06000139 RID: 313
		[FreeFunction("UNETManager::Get()->GetBroadcastConnectionInfoInternal")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern string GetBroadcastConnectionInfo(int hostId, out int port, out byte error);

		// Token: 0x0600013A RID: 314 RVA: 0x00004B01 File Offset: 0x00002D01
		public static void GetBroadcastConnectionInfo(int hostId, out string address, out int port, out byte error)
		{
			address = NetworkTransport.GetBroadcastConnectionInfo(hostId, out port, out error);
		}

		// Token: 0x0600013B RID: 315 RVA: 0x00004B0E File Offset: 0x00002D0E
		public static void GetBroadcastConnectionMessage(int hostId, byte[] buffer, int bufferSize, out int receivedSize, out byte error)
		{
			NetworkTransport.GetBroadcastConnectionMessageInternal(hostId, buffer, bufferSize, out receivedSize, out error);
		}

		// Token: 0x0600013C RID: 316
		[FreeFunction("UNETManager::Get()->GetBroadcastConnectionMessage")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void GetBroadcastConnectionMessageInternal(int hostId, [Out] byte[] buffer, int bufferSize, out int receivedSize, out byte error);

		// Token: 0x0600013D RID: 317 RVA: 0x00004B1C File Offset: 0x00002D1C
		private static void CheckTopology(HostTopology topology)
		{
			int maxPacketSize = NetworkTransport.GetMaxPacketSize();
			if ((int)topology.DefaultConfig.PacketSize > maxPacketSize)
			{
				throw new ArgumentOutOfRangeException("Default config: packet size should be less than packet size defined in global config: " + maxPacketSize.ToString());
			}
			for (int i = 0; i < topology.SpecialConnectionConfigs.Count; i++)
			{
				if ((int)topology.SpecialConnectionConfigs[i].PacketSize > maxPacketSize)
				{
					throw new ArgumentOutOfRangeException("Special config " + i.ToString() + ": packet size should be less than packet size defined in global config: " + maxPacketSize.ToString());
				}
			}
		}

		// Token: 0x0600013E RID: 318 RVA: 0x00004BC3 File Offset: 0x00002DC3
		// Note: this type is marked as 'beforefieldinit'.
		static NetworkTransport()
		{
		}

		// Token: 0x04000071 RID: 113
		private static int s_nextSceneId = 1;
	}
}
