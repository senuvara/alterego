﻿using System;
using System.ComponentModel;

namespace UnityEngine.Networking.Types
{
	// Token: 0x02000017 RID: 23
	[DefaultValue(NodeID.Invalid)]
	public enum NodeID : ushort
	{
		// Token: 0x0400004F RID: 79
		Invalid
	}
}
