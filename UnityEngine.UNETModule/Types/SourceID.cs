﻿using System;
using System.ComponentModel;

namespace UnityEngine.Networking.Types
{
	// Token: 0x02000015 RID: 21
	[DefaultValue(18446744073709551615UL)]
	public enum SourceID : ulong
	{
		// Token: 0x0400004B RID: 75
		Invalid = 18446744073709551615UL
	}
}
