﻿using System;
using System.ComponentModel;

namespace UnityEngine.Networking.Types
{
	// Token: 0x02000014 RID: 20
	[DefaultValue(18446744073709551615UL)]
	public enum AppID : ulong
	{
		// Token: 0x04000049 RID: 73
		Invalid = 18446744073709551615UL
	}
}
