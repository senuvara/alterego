﻿using System;
using System.ComponentModel;

namespace UnityEngine.Networking.Types
{
	// Token: 0x02000016 RID: 22
	[DefaultValue(18446744073709551615UL)]
	public enum NetworkID : ulong
	{
		// Token: 0x0400004D RID: 77
		Invalid = 18446744073709551615UL
	}
}
