﻿using System;

namespace UnityEngine.Networking.Types
{
	// Token: 0x02000019 RID: 25
	public class NetworkAccessToken
	{
		// Token: 0x0600006C RID: 108 RVA: 0x00002E2C File Offset: 0x0000102C
		public NetworkAccessToken()
		{
			this.array = new byte[64];
		}

		// Token: 0x0600006D RID: 109 RVA: 0x00002E42 File Offset: 0x00001042
		public NetworkAccessToken(byte[] array)
		{
			this.array = array;
		}

		// Token: 0x0600006E RID: 110 RVA: 0x00002E54 File Offset: 0x00001054
		public NetworkAccessToken(string strArray)
		{
			try
			{
				this.array = Convert.FromBase64String(strArray);
			}
			catch (Exception)
			{
				this.array = new byte[64];
			}
		}

		// Token: 0x0600006F RID: 111 RVA: 0x00002EA0 File Offset: 0x000010A0
		public string GetByteString()
		{
			return Convert.ToBase64String(this.array);
		}

		// Token: 0x06000070 RID: 112 RVA: 0x00002EC0 File Offset: 0x000010C0
		public bool IsValid()
		{
			bool result;
			if (this.array == null || this.array.Length != 64)
			{
				result = false;
			}
			else
			{
				bool flag = false;
				foreach (byte b in this.array)
				{
					if (b != 0)
					{
						flag = true;
						break;
					}
				}
				result = flag;
			}
			return result;
		}

		// Token: 0x04000052 RID: 82
		private const int NETWORK_ACCESS_TOKEN_SIZE = 64;

		// Token: 0x04000053 RID: 83
		public byte[] array;
	}
}
