﻿using System;
using System.ComponentModel;

namespace UnityEngine.Networking.Types
{
	// Token: 0x02000018 RID: 24
	[DefaultValue(HostPriority.Invalid)]
	public enum HostPriority
	{
		// Token: 0x04000051 RID: 81
		Invalid = 2147483647
	}
}
