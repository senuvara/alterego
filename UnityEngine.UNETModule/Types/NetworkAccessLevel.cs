﻿using System;
using System.ComponentModel;

namespace UnityEngine.Networking.Types
{
	// Token: 0x02000013 RID: 19
	[DefaultValue(NetworkAccessLevel.Invalid)]
	public enum NetworkAccessLevel : ulong
	{
		// Token: 0x04000044 RID: 68
		Invalid,
		// Token: 0x04000045 RID: 69
		User,
		// Token: 0x04000046 RID: 70
		Owner,
		// Token: 0x04000047 RID: 71
		Admin = 4UL
	}
}
