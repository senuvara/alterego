﻿using System;
using System.Collections.Generic;

namespace UnityEngine.Networking
{
	// Token: 0x0200002A RID: 42
	[Obsolete("The UNET transport will be removed in the future as soon a replacement is ready.")]
	[Serializable]
	public class ConnectionConfig
	{
		// Token: 0x06000144 RID: 324 RVA: 0x00004C64 File Offset: 0x00002E64
		public ConnectionConfig()
		{
			this.m_PacketSize = 1440;
			this.m_FragmentSize = 500;
			this.m_ResendTimeout = 1200U;
			this.m_DisconnectTimeout = 2000U;
			this.m_ConnectTimeout = 2000U;
			this.m_MinUpdateTimeout = 10U;
			this.m_PingTimeout = 500U;
			this.m_ReducedPingTimeout = 100U;
			this.m_AllCostTimeout = 20U;
			this.m_NetworkDropThreshold = 5;
			this.m_OverflowDropThreshold = 5;
			this.m_MaxConnectionAttempt = 10;
			this.m_AckDelay = 33U;
			this.m_SendDelay = 10U;
			this.m_MaxCombinedReliableMessageSize = 100;
			this.m_MaxCombinedReliableMessageCount = 10;
			this.m_MaxSentMessageQueueSize = 512;
			this.m_AcksType = ConnectionAcksType.Acks32;
			this.m_UsePlatformSpecificProtocols = false;
			this.m_InitialBandwidth = 0U;
			this.m_BandwidthPeakFactor = 2f;
			this.m_WebSocketReceiveBufferMaxSize = 0;
			this.m_UdpSocketReceiveBufferMaxSize = 0U;
			this.m_SSLCertFilePath = null;
			this.m_SSLPrivateKeyFilePath = null;
			this.m_SSLCAFilePath = null;
		}

		// Token: 0x06000145 RID: 325 RVA: 0x00004D6C File Offset: 0x00002F6C
		public ConnectionConfig(ConnectionConfig config)
		{
			if (config == null)
			{
				throw new NullReferenceException("config is not defined");
			}
			this.m_PacketSize = config.m_PacketSize;
			this.m_FragmentSize = config.m_FragmentSize;
			this.m_ResendTimeout = config.m_ResendTimeout;
			this.m_DisconnectTimeout = config.m_DisconnectTimeout;
			this.m_ConnectTimeout = config.m_ConnectTimeout;
			this.m_MinUpdateTimeout = config.m_MinUpdateTimeout;
			this.m_PingTimeout = config.m_PingTimeout;
			this.m_ReducedPingTimeout = config.m_ReducedPingTimeout;
			this.m_AllCostTimeout = config.m_AllCostTimeout;
			this.m_NetworkDropThreshold = config.m_NetworkDropThreshold;
			this.m_OverflowDropThreshold = config.m_OverflowDropThreshold;
			this.m_MaxConnectionAttempt = config.m_MaxConnectionAttempt;
			this.m_AckDelay = config.m_AckDelay;
			this.m_SendDelay = config.m_SendDelay;
			this.m_MaxCombinedReliableMessageSize = config.MaxCombinedReliableMessageSize;
			this.m_MaxCombinedReliableMessageCount = config.m_MaxCombinedReliableMessageCount;
			this.m_MaxSentMessageQueueSize = config.m_MaxSentMessageQueueSize;
			this.m_AcksType = config.m_AcksType;
			this.m_UsePlatformSpecificProtocols = config.m_UsePlatformSpecificProtocols;
			this.m_InitialBandwidth = config.m_InitialBandwidth;
			if (this.m_InitialBandwidth == 0U)
			{
				this.m_InitialBandwidth = (uint)(this.m_PacketSize * 1000) / this.m_MinUpdateTimeout;
			}
			this.m_BandwidthPeakFactor = config.m_BandwidthPeakFactor;
			this.m_WebSocketReceiveBufferMaxSize = config.m_WebSocketReceiveBufferMaxSize;
			this.m_UdpSocketReceiveBufferMaxSize = config.m_UdpSocketReceiveBufferMaxSize;
			this.m_SSLCertFilePath = config.m_SSLCertFilePath;
			this.m_SSLPrivateKeyFilePath = config.m_SSLPrivateKeyFilePath;
			this.m_SSLCAFilePath = config.m_SSLCAFilePath;
			foreach (ChannelQOS channel in config.m_Channels)
			{
				this.m_Channels.Add(new ChannelQOS(channel));
			}
			foreach (List<byte> item in config.m_SharedOrderChannels)
			{
				this.m_SharedOrderChannels.Add(item);
			}
		}

		// Token: 0x06000146 RID: 326 RVA: 0x00004FB4 File Offset: 0x000031B4
		public static void Validate(ConnectionConfig config)
		{
			if (config.m_PacketSize < 128)
			{
				throw new ArgumentOutOfRangeException("PacketSize should be > " + 128.ToString());
			}
			if (config.m_FragmentSize >= config.m_PacketSize - 128)
			{
				throw new ArgumentOutOfRangeException("FragmentSize should be < PacketSize - " + 128.ToString());
			}
			if (config.m_Channels.Count > 255)
			{
				throw new ArgumentOutOfRangeException("Channels number should be less than 256");
			}
		}

		// Token: 0x1700003C RID: 60
		// (get) Token: 0x06000147 RID: 327 RVA: 0x00005050 File Offset: 0x00003250
		// (set) Token: 0x06000148 RID: 328 RVA: 0x0000506B File Offset: 0x0000326B
		public ushort PacketSize
		{
			get
			{
				return this.m_PacketSize;
			}
			set
			{
				this.m_PacketSize = value;
			}
		}

		// Token: 0x1700003D RID: 61
		// (get) Token: 0x06000149 RID: 329 RVA: 0x00005078 File Offset: 0x00003278
		// (set) Token: 0x0600014A RID: 330 RVA: 0x00005093 File Offset: 0x00003293
		public ushort FragmentSize
		{
			get
			{
				return this.m_FragmentSize;
			}
			set
			{
				this.m_FragmentSize = value;
			}
		}

		// Token: 0x1700003E RID: 62
		// (get) Token: 0x0600014B RID: 331 RVA: 0x000050A0 File Offset: 0x000032A0
		// (set) Token: 0x0600014C RID: 332 RVA: 0x000050BB File Offset: 0x000032BB
		public uint ResendTimeout
		{
			get
			{
				return this.m_ResendTimeout;
			}
			set
			{
				this.m_ResendTimeout = value;
			}
		}

		// Token: 0x1700003F RID: 63
		// (get) Token: 0x0600014D RID: 333 RVA: 0x000050C8 File Offset: 0x000032C8
		// (set) Token: 0x0600014E RID: 334 RVA: 0x000050E3 File Offset: 0x000032E3
		public uint DisconnectTimeout
		{
			get
			{
				return this.m_DisconnectTimeout;
			}
			set
			{
				this.m_DisconnectTimeout = value;
			}
		}

		// Token: 0x17000040 RID: 64
		// (get) Token: 0x0600014F RID: 335 RVA: 0x000050F0 File Offset: 0x000032F0
		// (set) Token: 0x06000150 RID: 336 RVA: 0x0000510B File Offset: 0x0000330B
		public uint ConnectTimeout
		{
			get
			{
				return this.m_ConnectTimeout;
			}
			set
			{
				this.m_ConnectTimeout = value;
			}
		}

		// Token: 0x17000041 RID: 65
		// (get) Token: 0x06000151 RID: 337 RVA: 0x00005118 File Offset: 0x00003318
		// (set) Token: 0x06000152 RID: 338 RVA: 0x00005133 File Offset: 0x00003333
		public uint MinUpdateTimeout
		{
			get
			{
				return this.m_MinUpdateTimeout;
			}
			set
			{
				if (value == 0U)
				{
					throw new ArgumentOutOfRangeException("Minimal update timeout should be > 0");
				}
				this.m_MinUpdateTimeout = value;
			}
		}

		// Token: 0x17000042 RID: 66
		// (get) Token: 0x06000153 RID: 339 RVA: 0x00005150 File Offset: 0x00003350
		// (set) Token: 0x06000154 RID: 340 RVA: 0x0000516B File Offset: 0x0000336B
		public uint PingTimeout
		{
			get
			{
				return this.m_PingTimeout;
			}
			set
			{
				this.m_PingTimeout = value;
			}
		}

		// Token: 0x17000043 RID: 67
		// (get) Token: 0x06000155 RID: 341 RVA: 0x00005178 File Offset: 0x00003378
		// (set) Token: 0x06000156 RID: 342 RVA: 0x00005193 File Offset: 0x00003393
		public uint ReducedPingTimeout
		{
			get
			{
				return this.m_ReducedPingTimeout;
			}
			set
			{
				this.m_ReducedPingTimeout = value;
			}
		}

		// Token: 0x17000044 RID: 68
		// (get) Token: 0x06000157 RID: 343 RVA: 0x000051A0 File Offset: 0x000033A0
		// (set) Token: 0x06000158 RID: 344 RVA: 0x000051BB File Offset: 0x000033BB
		public uint AllCostTimeout
		{
			get
			{
				return this.m_AllCostTimeout;
			}
			set
			{
				this.m_AllCostTimeout = value;
			}
		}

		// Token: 0x17000045 RID: 69
		// (get) Token: 0x06000159 RID: 345 RVA: 0x000051C8 File Offset: 0x000033C8
		// (set) Token: 0x0600015A RID: 346 RVA: 0x000051E3 File Offset: 0x000033E3
		public byte NetworkDropThreshold
		{
			get
			{
				return this.m_NetworkDropThreshold;
			}
			set
			{
				this.m_NetworkDropThreshold = value;
			}
		}

		// Token: 0x17000046 RID: 70
		// (get) Token: 0x0600015B RID: 347 RVA: 0x000051F0 File Offset: 0x000033F0
		// (set) Token: 0x0600015C RID: 348 RVA: 0x0000520B File Offset: 0x0000340B
		public byte OverflowDropThreshold
		{
			get
			{
				return this.m_OverflowDropThreshold;
			}
			set
			{
				this.m_OverflowDropThreshold = value;
			}
		}

		// Token: 0x17000047 RID: 71
		// (get) Token: 0x0600015D RID: 349 RVA: 0x00005218 File Offset: 0x00003418
		// (set) Token: 0x0600015E RID: 350 RVA: 0x00005233 File Offset: 0x00003433
		public byte MaxConnectionAttempt
		{
			get
			{
				return this.m_MaxConnectionAttempt;
			}
			set
			{
				this.m_MaxConnectionAttempt = value;
			}
		}

		// Token: 0x17000048 RID: 72
		// (get) Token: 0x0600015F RID: 351 RVA: 0x00005240 File Offset: 0x00003440
		// (set) Token: 0x06000160 RID: 352 RVA: 0x0000525B File Offset: 0x0000345B
		public uint AckDelay
		{
			get
			{
				return this.m_AckDelay;
			}
			set
			{
				this.m_AckDelay = value;
			}
		}

		// Token: 0x17000049 RID: 73
		// (get) Token: 0x06000161 RID: 353 RVA: 0x00005268 File Offset: 0x00003468
		// (set) Token: 0x06000162 RID: 354 RVA: 0x00005283 File Offset: 0x00003483
		public uint SendDelay
		{
			get
			{
				return this.m_SendDelay;
			}
			set
			{
				this.m_SendDelay = value;
			}
		}

		// Token: 0x1700004A RID: 74
		// (get) Token: 0x06000163 RID: 355 RVA: 0x00005290 File Offset: 0x00003490
		// (set) Token: 0x06000164 RID: 356 RVA: 0x000052AB File Offset: 0x000034AB
		public ushort MaxCombinedReliableMessageSize
		{
			get
			{
				return this.m_MaxCombinedReliableMessageSize;
			}
			set
			{
				this.m_MaxCombinedReliableMessageSize = value;
			}
		}

		// Token: 0x1700004B RID: 75
		// (get) Token: 0x06000165 RID: 357 RVA: 0x000052B8 File Offset: 0x000034B8
		// (set) Token: 0x06000166 RID: 358 RVA: 0x000052D3 File Offset: 0x000034D3
		public ushort MaxCombinedReliableMessageCount
		{
			get
			{
				return this.m_MaxCombinedReliableMessageCount;
			}
			set
			{
				this.m_MaxCombinedReliableMessageCount = value;
			}
		}

		// Token: 0x1700004C RID: 76
		// (get) Token: 0x06000167 RID: 359 RVA: 0x000052E0 File Offset: 0x000034E0
		// (set) Token: 0x06000168 RID: 360 RVA: 0x000052FB File Offset: 0x000034FB
		public ushort MaxSentMessageQueueSize
		{
			get
			{
				return this.m_MaxSentMessageQueueSize;
			}
			set
			{
				this.m_MaxSentMessageQueueSize = value;
			}
		}

		// Token: 0x1700004D RID: 77
		// (get) Token: 0x06000169 RID: 361 RVA: 0x00005308 File Offset: 0x00003508
		// (set) Token: 0x0600016A RID: 362 RVA: 0x00005323 File Offset: 0x00003523
		public ConnectionAcksType AcksType
		{
			get
			{
				return this.m_AcksType;
			}
			set
			{
				this.m_AcksType = value;
			}
		}

		// Token: 0x1700004E RID: 78
		// (get) Token: 0x0600016B RID: 363 RVA: 0x00005330 File Offset: 0x00003530
		// (set) Token: 0x0600016C RID: 364 RVA: 0x00005351 File Offset: 0x00003551
		[Obsolete("IsAcksLong is deprecated. Use AcksType = ConnectionAcksType.Acks64", false)]
		public bool IsAcksLong
		{
			get
			{
				return this.m_AcksType != ConnectionAcksType.Acks32;
			}
			set
			{
				if (value && this.m_AcksType == ConnectionAcksType.Acks32)
				{
					this.m_AcksType = ConnectionAcksType.Acks64;
				}
				else if (!value)
				{
					this.m_AcksType = ConnectionAcksType.Acks32;
				}
			}
		}

		// Token: 0x1700004F RID: 79
		// (get) Token: 0x0600016D RID: 365 RVA: 0x00005380 File Offset: 0x00003580
		// (set) Token: 0x0600016E RID: 366 RVA: 0x0000539B File Offset: 0x0000359B
		public bool UsePlatformSpecificProtocols
		{
			get
			{
				return this.m_UsePlatformSpecificProtocols;
			}
			set
			{
				if (value && Application.platform != RuntimePlatform.PS4)
				{
					throw new ArgumentOutOfRangeException("Platform specific protocols are not supported on this platform");
				}
				this.m_UsePlatformSpecificProtocols = value;
			}
		}

		// Token: 0x17000050 RID: 80
		// (get) Token: 0x0600016F RID: 367 RVA: 0x000053C4 File Offset: 0x000035C4
		// (set) Token: 0x06000170 RID: 368 RVA: 0x000053DF File Offset: 0x000035DF
		public uint InitialBandwidth
		{
			get
			{
				return this.m_InitialBandwidth;
			}
			set
			{
				this.m_InitialBandwidth = value;
			}
		}

		// Token: 0x17000051 RID: 81
		// (get) Token: 0x06000171 RID: 369 RVA: 0x000053EC File Offset: 0x000035EC
		// (set) Token: 0x06000172 RID: 370 RVA: 0x00005407 File Offset: 0x00003607
		public float BandwidthPeakFactor
		{
			get
			{
				return this.m_BandwidthPeakFactor;
			}
			set
			{
				this.m_BandwidthPeakFactor = value;
			}
		}

		// Token: 0x17000052 RID: 82
		// (get) Token: 0x06000173 RID: 371 RVA: 0x00005414 File Offset: 0x00003614
		// (set) Token: 0x06000174 RID: 372 RVA: 0x0000542F File Offset: 0x0000362F
		public ushort WebSocketReceiveBufferMaxSize
		{
			get
			{
				return this.m_WebSocketReceiveBufferMaxSize;
			}
			set
			{
				this.m_WebSocketReceiveBufferMaxSize = value;
			}
		}

		// Token: 0x17000053 RID: 83
		// (get) Token: 0x06000175 RID: 373 RVA: 0x0000543C File Offset: 0x0000363C
		// (set) Token: 0x06000176 RID: 374 RVA: 0x00005457 File Offset: 0x00003657
		public uint UdpSocketReceiveBufferMaxSize
		{
			get
			{
				return this.m_UdpSocketReceiveBufferMaxSize;
			}
			set
			{
				this.m_UdpSocketReceiveBufferMaxSize = value;
			}
		}

		// Token: 0x17000054 RID: 84
		// (get) Token: 0x06000177 RID: 375 RVA: 0x00005464 File Offset: 0x00003664
		// (set) Token: 0x06000178 RID: 376 RVA: 0x0000547F File Offset: 0x0000367F
		public string SSLCertFilePath
		{
			get
			{
				return this.m_SSLCertFilePath;
			}
			set
			{
				this.m_SSLCertFilePath = value;
			}
		}

		// Token: 0x17000055 RID: 85
		// (get) Token: 0x06000179 RID: 377 RVA: 0x0000548C File Offset: 0x0000368C
		// (set) Token: 0x0600017A RID: 378 RVA: 0x000054A7 File Offset: 0x000036A7
		public string SSLPrivateKeyFilePath
		{
			get
			{
				return this.m_SSLPrivateKeyFilePath;
			}
			set
			{
				this.m_SSLPrivateKeyFilePath = value;
			}
		}

		// Token: 0x17000056 RID: 86
		// (get) Token: 0x0600017B RID: 379 RVA: 0x000054B4 File Offset: 0x000036B4
		// (set) Token: 0x0600017C RID: 380 RVA: 0x000054CF File Offset: 0x000036CF
		public string SSLCAFilePath
		{
			get
			{
				return this.m_SSLCAFilePath;
			}
			set
			{
				this.m_SSLCAFilePath = value;
			}
		}

		// Token: 0x17000057 RID: 87
		// (get) Token: 0x0600017D RID: 381 RVA: 0x000054DC File Offset: 0x000036DC
		public int ChannelCount
		{
			get
			{
				return this.m_Channels.Count;
			}
		}

		// Token: 0x17000058 RID: 88
		// (get) Token: 0x0600017E RID: 382 RVA: 0x000054FC File Offset: 0x000036FC
		public int SharedOrderChannelCount
		{
			get
			{
				return this.m_SharedOrderChannels.Count;
			}
		}

		// Token: 0x0600017F RID: 383 RVA: 0x0000551C File Offset: 0x0000371C
		public byte AddChannel(QosType value)
		{
			if (this.m_Channels.Count > 255)
			{
				throw new ArgumentOutOfRangeException("Channels Count should be less than 256");
			}
			if (!Enum.IsDefined(typeof(QosType), value))
			{
				throw new ArgumentOutOfRangeException("requested qos type doesn't exist: " + (int)value);
			}
			ChannelQOS item = new ChannelQOS(value);
			this.m_Channels.Add(item);
			return (byte)(this.m_Channels.Count - 1);
		}

		// Token: 0x06000180 RID: 384 RVA: 0x000055A4 File Offset: 0x000037A4
		public void MakeChannelsSharedOrder(List<byte> channelIndices)
		{
			if (channelIndices == null)
			{
				throw new NullReferenceException("channelIndices must not be null");
			}
			if (channelIndices.Count == 0)
			{
				throw new ArgumentOutOfRangeException("Received empty list of shared order channel indexes");
			}
			byte b = 0;
			while ((int)b < channelIndices.Count)
			{
				byte b2 = channelIndices[(int)b];
				if ((int)b2 >= this.m_Channels.Count)
				{
					throw new ArgumentOutOfRangeException("Shared order channel list contains wrong channel index " + b2);
				}
				ChannelQOS channelQOS = this.m_Channels[(int)b2];
				if (channelQOS.BelongsToSharedOrderChannel)
				{
					throw new ArgumentException("Channel with index " + b2 + " has been already included to other shared order channel");
				}
				if (channelQOS.QOS != QosType.Reliable && channelQOS.QOS != QosType.Unreliable)
				{
					throw new ArgumentException("Only Reliable and Unreliable QoS are allowed for shared order channel, wrong channel is with index " + b2);
				}
				b += 1;
			}
			byte b3 = 0;
			while ((int)b3 < channelIndices.Count)
			{
				byte index = channelIndices[(int)b3];
				this.m_Channels[(int)index].m_BelongsSharedOrderChannel = true;
				b3 += 1;
			}
			List<byte> item = new List<byte>(channelIndices);
			this.m_SharedOrderChannels.Add(item);
		}

		// Token: 0x06000181 RID: 385 RVA: 0x000056D0 File Offset: 0x000038D0
		public QosType GetChannel(byte idx)
		{
			if ((int)idx >= this.m_Channels.Count)
			{
				throw new ArgumentOutOfRangeException("requested index greater than maximum channels count");
			}
			return this.m_Channels[(int)idx].QOS;
		}

		// Token: 0x06000182 RID: 386 RVA: 0x00005714 File Offset: 0x00003914
		public IList<byte> GetSharedOrderChannels(byte idx)
		{
			if ((int)idx >= this.m_SharedOrderChannels.Count)
			{
				throw new ArgumentOutOfRangeException("requested index greater than maximum shared order channels count");
			}
			return this.m_SharedOrderChannels[(int)idx].AsReadOnly();
		}

		// Token: 0x17000059 RID: 89
		// (get) Token: 0x06000183 RID: 387 RVA: 0x00005758 File Offset: 0x00003958
		public List<ChannelQOS> Channels
		{
			get
			{
				return this.m_Channels;
			}
		}

		// Token: 0x0400009C RID: 156
		private const int g_MinPacketSize = 128;

		// Token: 0x0400009D RID: 157
		[SerializeField]
		private ushort m_PacketSize;

		// Token: 0x0400009E RID: 158
		[SerializeField]
		private ushort m_FragmentSize;

		// Token: 0x0400009F RID: 159
		[SerializeField]
		private uint m_ResendTimeout;

		// Token: 0x040000A0 RID: 160
		[SerializeField]
		private uint m_DisconnectTimeout;

		// Token: 0x040000A1 RID: 161
		[SerializeField]
		private uint m_ConnectTimeout;

		// Token: 0x040000A2 RID: 162
		[SerializeField]
		private uint m_MinUpdateTimeout;

		// Token: 0x040000A3 RID: 163
		[SerializeField]
		private uint m_PingTimeout;

		// Token: 0x040000A4 RID: 164
		[SerializeField]
		private uint m_ReducedPingTimeout;

		// Token: 0x040000A5 RID: 165
		[SerializeField]
		private uint m_AllCostTimeout;

		// Token: 0x040000A6 RID: 166
		[SerializeField]
		private byte m_NetworkDropThreshold;

		// Token: 0x040000A7 RID: 167
		[SerializeField]
		private byte m_OverflowDropThreshold;

		// Token: 0x040000A8 RID: 168
		[SerializeField]
		private byte m_MaxConnectionAttempt;

		// Token: 0x040000A9 RID: 169
		[SerializeField]
		private uint m_AckDelay;

		// Token: 0x040000AA RID: 170
		[SerializeField]
		private uint m_SendDelay;

		// Token: 0x040000AB RID: 171
		[SerializeField]
		private ushort m_MaxCombinedReliableMessageSize;

		// Token: 0x040000AC RID: 172
		[SerializeField]
		private ushort m_MaxCombinedReliableMessageCount;

		// Token: 0x040000AD RID: 173
		[SerializeField]
		private ushort m_MaxSentMessageQueueSize;

		// Token: 0x040000AE RID: 174
		[SerializeField]
		private ConnectionAcksType m_AcksType;

		// Token: 0x040000AF RID: 175
		[SerializeField]
		private bool m_UsePlatformSpecificProtocols;

		// Token: 0x040000B0 RID: 176
		[SerializeField]
		private uint m_InitialBandwidth;

		// Token: 0x040000B1 RID: 177
		[SerializeField]
		private float m_BandwidthPeakFactor;

		// Token: 0x040000B2 RID: 178
		[SerializeField]
		private ushort m_WebSocketReceiveBufferMaxSize;

		// Token: 0x040000B3 RID: 179
		[SerializeField]
		private uint m_UdpSocketReceiveBufferMaxSize;

		// Token: 0x040000B4 RID: 180
		[SerializeField]
		private string m_SSLCertFilePath;

		// Token: 0x040000B5 RID: 181
		[SerializeField]
		private string m_SSLPrivateKeyFilePath;

		// Token: 0x040000B6 RID: 182
		[SerializeField]
		private string m_SSLCAFilePath;

		// Token: 0x040000B7 RID: 183
		[SerializeField]
		internal List<ChannelQOS> m_Channels = new List<ChannelQOS>();

		// Token: 0x040000B8 RID: 184
		[SerializeField]
		internal List<List<byte>> m_SharedOrderChannels = new List<List<byte>>();
	}
}
