﻿using System;

namespace UnityEngine.Networking
{
	// Token: 0x02000025 RID: 37
	public enum QosType
	{
		// Token: 0x04000079 RID: 121
		Unreliable,
		// Token: 0x0400007A RID: 122
		UnreliableFragmented,
		// Token: 0x0400007B RID: 123
		UnreliableSequenced,
		// Token: 0x0400007C RID: 124
		Reliable,
		// Token: 0x0400007D RID: 125
		ReliableFragmented,
		// Token: 0x0400007E RID: 126
		ReliableSequenced,
		// Token: 0x0400007F RID: 127
		StateUpdate,
		// Token: 0x04000080 RID: 128
		ReliableStateUpdate,
		// Token: 0x04000081 RID: 129
		AllCostDelivery,
		// Token: 0x04000082 RID: 130
		UnreliableFragmentedSequenced,
		// Token: 0x04000083 RID: 131
		ReliableFragmentedSequenced
	}
}
