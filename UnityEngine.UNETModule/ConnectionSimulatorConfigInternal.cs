﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;

namespace UnityEngine.Networking
{
	// Token: 0x02000030 RID: 48
	[NativeHeader("Runtime/Networking/UNETConfiguration.h")]
	[NativeConditional("ENABLE_NETWORK && ENABLE_UNET", true)]
	internal class ConnectionSimulatorConfigInternal : IDisposable
	{
		// Token: 0x060001DC RID: 476 RVA: 0x00006228 File Offset: 0x00004428
		public ConnectionSimulatorConfigInternal(ConnectionSimulatorConfig config)
		{
			this.m_Ptr = ConnectionSimulatorConfigInternal.InternalCreate(config.m_OutMinDelay, config.m_OutAvgDelay, config.m_InMinDelay, config.m_InAvgDelay, config.m_PacketLossPercentage);
		}

		// Token: 0x060001DD RID: 477 RVA: 0x0000625A File Offset: 0x0000445A
		protected virtual void Dispose(bool disposing)
		{
			if (this.m_Ptr != IntPtr.Zero)
			{
				ConnectionSimulatorConfigInternal.InternalDestroy(this.m_Ptr);
				this.m_Ptr = IntPtr.Zero;
			}
			GC.SuppressFinalize(this);
		}

		// Token: 0x060001DE RID: 478 RVA: 0x00006290 File Offset: 0x00004490
		~ConnectionSimulatorConfigInternal()
		{
			this.Dispose(false);
		}

		// Token: 0x060001DF RID: 479 RVA: 0x000062C4 File Offset: 0x000044C4
		public void Dispose()
		{
			if (this.m_Ptr != IntPtr.Zero)
			{
				ConnectionSimulatorConfigInternal.InternalDestroy(this.m_Ptr);
				this.m_Ptr = IntPtr.Zero;
			}
		}

		// Token: 0x060001E0 RID: 480
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern IntPtr InternalCreate(int outMinDelay, int outAvgDelay, int inMinDelay, int inAvgDelay, float packetLossPercentage);

		// Token: 0x060001E1 RID: 481
		[NativeMethod(IsThreadSafe = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void InternalDestroy(IntPtr ptr);

		// Token: 0x040000D6 RID: 214
		public IntPtr m_Ptr;
	}
}
