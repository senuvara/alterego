﻿using System;

namespace UnityEngine.Networking
{
	// Token: 0x02000029 RID: 41
	[Obsolete("The UNET transport will be removed in the future as soon a replacement is ready.")]
	[Serializable]
	public class ChannelQOS
	{
		// Token: 0x0600013F RID: 319 RVA: 0x00004BCB File Offset: 0x00002DCB
		public ChannelQOS(QosType value)
		{
			this.m_Type = value;
			this.m_BelongsSharedOrderChannel = false;
		}

		// Token: 0x06000140 RID: 320 RVA: 0x00004BE2 File Offset: 0x00002DE2
		public ChannelQOS()
		{
			this.m_Type = QosType.Unreliable;
			this.m_BelongsSharedOrderChannel = false;
		}

		// Token: 0x06000141 RID: 321 RVA: 0x00004BF9 File Offset: 0x00002DF9
		public ChannelQOS(ChannelQOS channel)
		{
			if (channel == null)
			{
				throw new NullReferenceException("channel is not defined");
			}
			this.m_Type = channel.m_Type;
			this.m_BelongsSharedOrderChannel = channel.m_BelongsSharedOrderChannel;
		}

		// Token: 0x1700003A RID: 58
		// (get) Token: 0x06000142 RID: 322 RVA: 0x00004C2C File Offset: 0x00002E2C
		public QosType QOS
		{
			get
			{
				return this.m_Type;
			}
		}

		// Token: 0x1700003B RID: 59
		// (get) Token: 0x06000143 RID: 323 RVA: 0x00004C48 File Offset: 0x00002E48
		public bool BelongsToSharedOrderChannel
		{
			get
			{
				return this.m_BelongsSharedOrderChannel;
			}
		}

		// Token: 0x0400009A RID: 154
		[SerializeField]
		internal QosType m_Type;

		// Token: 0x0400009B RID: 155
		[SerializeField]
		internal bool m_BelongsSharedOrderChannel;
	}
}
