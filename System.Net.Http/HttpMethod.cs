﻿using System;
using System.Net.Http.Headers;

namespace System.Net.Http
{
	/// <summary>A helper class for retrieving and comparing standard HTTP methods and for creating new HTTP methods.</summary>
	// Token: 0x0200001C RID: 28
	public class HttpMethod : IEquatable<HttpMethod>
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Net.Http.HttpMethod" /> class with a specific HTTP method.</summary>
		/// <param name="method">The HTTP method.</param>
		// Token: 0x060000AA RID: 170 RVA: 0x0000494A File Offset: 0x00002B4A
		public HttpMethod(string method)
		{
			if (string.IsNullOrEmpty(method))
			{
				throw new ArgumentException("method");
			}
			Parser.Token.Check(method);
			this.method = method;
		}

		/// <summary>Represents an HTTP DELETE protocol method.</summary>
		/// <returns>Returns <see cref="T:System.Net.Http.HttpMethod" />.</returns>
		// Token: 0x1700001F RID: 31
		// (get) Token: 0x060000AB RID: 171 RVA: 0x00004972 File Offset: 0x00002B72
		public static HttpMethod Delete
		{
			get
			{
				return HttpMethod.delete_method;
			}
		}

		/// <summary>Represents an HTTP GET protocol method.</summary>
		/// <returns>Returns <see cref="T:System.Net.Http.HttpMethod" />.</returns>
		// Token: 0x17000020 RID: 32
		// (get) Token: 0x060000AC RID: 172 RVA: 0x00004979 File Offset: 0x00002B79
		public static HttpMethod Get
		{
			get
			{
				return HttpMethod.get_method;
			}
		}

		/// <summary>Represents an HTTP HEAD protocol method. The HEAD method is identical to GET except that the server only returns message-headers in the response, without a message-body.</summary>
		/// <returns>Returns <see cref="T:System.Net.Http.HttpMethod" />.</returns>
		// Token: 0x17000021 RID: 33
		// (get) Token: 0x060000AD RID: 173 RVA: 0x00004980 File Offset: 0x00002B80
		public static HttpMethod Head
		{
			get
			{
				return HttpMethod.head_method;
			}
		}

		/// <summary>An HTTP method. </summary>
		/// <returns>An HTTP method represented as a <see cref="T:System.String" />.</returns>
		// Token: 0x17000022 RID: 34
		// (get) Token: 0x060000AE RID: 174 RVA: 0x00004987 File Offset: 0x00002B87
		public string Method
		{
			get
			{
				return this.method;
			}
		}

		/// <summary>Represents an HTTP OPTIONS protocol method.</summary>
		/// <returns>Returns <see cref="T:System.Net.Http.HttpMethod" />.</returns>
		// Token: 0x17000023 RID: 35
		// (get) Token: 0x060000AF RID: 175 RVA: 0x0000498F File Offset: 0x00002B8F
		public static HttpMethod Options
		{
			get
			{
				return HttpMethod.options_method;
			}
		}

		/// <summary>Represents an HTTP POST protocol method that is used to post a new entity as an addition to a URI.</summary>
		/// <returns>Returns <see cref="T:System.Net.Http.HttpMethod" />.</returns>
		// Token: 0x17000024 RID: 36
		// (get) Token: 0x060000B0 RID: 176 RVA: 0x00004996 File Offset: 0x00002B96
		public static HttpMethod Post
		{
			get
			{
				return HttpMethod.post_method;
			}
		}

		/// <summary>Represents an HTTP PUT protocol method that is used to replace an entity identified by a URI.</summary>
		/// <returns>Returns <see cref="T:System.Net.Http.HttpMethod" />.</returns>
		// Token: 0x17000025 RID: 37
		// (get) Token: 0x060000B1 RID: 177 RVA: 0x0000499D File Offset: 0x00002B9D
		public static HttpMethod Put
		{
			get
			{
				return HttpMethod.put_method;
			}
		}

		/// <summary>Represents an HTTP TRACE protocol method.</summary>
		/// <returns>Returns <see cref="T:System.Net.Http.HttpMethod" />.</returns>
		// Token: 0x17000026 RID: 38
		// (get) Token: 0x060000B2 RID: 178 RVA: 0x000049A4 File Offset: 0x00002BA4
		public static HttpMethod Trace
		{
			get
			{
				return HttpMethod.trace_method;
			}
		}

		/// <summary>The equality operator for comparing two <see cref="T:System.Net.Http.HttpMethod" /> objects.</summary>
		/// <param name="left">The left <see cref="T:System.Net.Http.HttpMethod" /> to an equality operator.</param>
		/// <param name="right">The right  <see cref="T:System.Net.Http.HttpMethod" /> to an equality operator.</param>
		/// <returns>
		///     <see langword="true" /> if the specified <paramref name="left" /> and <paramref name="right" /> parameters are equal; otherwise, <see langword="false" />.</returns>
		// Token: 0x060000B3 RID: 179 RVA: 0x000049AB File Offset: 0x00002BAB
		public static bool operator ==(HttpMethod left, HttpMethod right)
		{
			if (left == null || right == null)
			{
				return left == right;
			}
			return left.Equals(right);
		}

		/// <summary>The inequality operator for comparing two <see cref="T:System.Net.Http.HttpMethod" /> objects.</summary>
		/// <param name="left">The left <see cref="T:System.Net.Http.HttpMethod" /> to an inequality operator.</param>
		/// <param name="right">The right  <see cref="T:System.Net.Http.HttpMethod" /> to an inequality operator.</param>
		/// <returns>
		///     <see langword="true" /> if the specified <paramref name="left" /> and <paramref name="right" /> parameters are inequal; otherwise, <see langword="false" />.</returns>
		// Token: 0x060000B4 RID: 180 RVA: 0x000049BF File Offset: 0x00002BBF
		public static bool operator !=(HttpMethod left, HttpMethod right)
		{
			return !(left == right);
		}

		/// <summary>Determines whether the specified <see cref="T:System.Net.Http.HttpMethod" /> is equal to the current <see cref="T:System.Object" />.</summary>
		/// <param name="other">The HTTP method to compare with the current object.</param>
		/// <returns>
		///     <see langword="true" /> if the specified object is equal to the current object; otherwise, <see langword="false" />.</returns>
		// Token: 0x060000B5 RID: 181 RVA: 0x000049CB File Offset: 0x00002BCB
		public bool Equals(HttpMethod other)
		{
			return string.Equals(this.method, other.method, StringComparison.OrdinalIgnoreCase);
		}

		/// <summary>Determines whether the specified <see cref="T:System.Object" /> is equal to the current <see cref="T:System.Object" />.</summary>
		/// <param name="obj">The object to compare with the current object.</param>
		/// <returns>
		///     <see langword="true" /> if the specified object is equal to the current object; otherwise, <see langword="false" />.</returns>
		// Token: 0x060000B6 RID: 182 RVA: 0x000049E0 File Offset: 0x00002BE0
		public override bool Equals(object obj)
		{
			HttpMethod httpMethod = obj as HttpMethod;
			return httpMethod != null && this.Equals(httpMethod);
		}

		/// <summary>Serves as a hash function for this type.</summary>
		/// <returns>A hash code for the current <see cref="T:System.Object" />.</returns>
		// Token: 0x060000B7 RID: 183 RVA: 0x00004A00 File Offset: 0x00002C00
		public override int GetHashCode()
		{
			return this.method.GetHashCode();
		}

		/// <summary>Returns a string that represents the current object.</summary>
		/// <returns>A string representing the current object.</returns>
		// Token: 0x060000B8 RID: 184 RVA: 0x00004987 File Offset: 0x00002B87
		public override string ToString()
		{
			return this.method;
		}

		// Token: 0x060000B9 RID: 185 RVA: 0x00004A10 File Offset: 0x00002C10
		// Note: this type is marked as 'beforefieldinit'.
		static HttpMethod()
		{
		}

		// Token: 0x040000AB RID: 171
		private static readonly HttpMethod delete_method = new HttpMethod("DELETE");

		// Token: 0x040000AC RID: 172
		private static readonly HttpMethod get_method = new HttpMethod("GET");

		// Token: 0x040000AD RID: 173
		private static readonly HttpMethod head_method = new HttpMethod("HEAD");

		// Token: 0x040000AE RID: 174
		private static readonly HttpMethod options_method = new HttpMethod("OPTIONS");

		// Token: 0x040000AF RID: 175
		private static readonly HttpMethod post_method = new HttpMethod("POST");

		// Token: 0x040000B0 RID: 176
		private static readonly HttpMethod put_method = new HttpMethod("PUT");

		// Token: 0x040000B1 RID: 177
		private static readonly HttpMethod trace_method = new HttpMethod("TRACE");

		// Token: 0x040000B2 RID: 178
		private readonly string method;
	}
}
