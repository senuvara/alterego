﻿using System;
using System.Diagnostics;
using System.IO;
using System.Net.Http.Headers;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace System.Net.Http
{
	/// <summary>A base class representing an HTTP entity body and content headers.</summary>
	// Token: 0x02000013 RID: 19
	public abstract class HttpContent : IDisposable
	{
		/// <summary>Gets the HTTP content headers as defined in RFC 2616.</summary>
		/// <returns>The content headers as defined in RFC 2616.</returns>
		// Token: 0x1700001D RID: 29
		// (get) Token: 0x06000080 RID: 128 RVA: 0x0000400C File Offset: 0x0000220C
		public HttpContentHeaders Headers
		{
			get
			{
				HttpContentHeaders result;
				if ((result = this.headers) == null)
				{
					result = (this.headers = new HttpContentHeaders(this));
				}
				return result;
			}
		}

		// Token: 0x1700001E RID: 30
		// (get) Token: 0x06000081 RID: 129 RVA: 0x00004034 File Offset: 0x00002234
		internal long? LoadedBufferLength
		{
			get
			{
				if (this.buffer != null)
				{
					return new long?(this.buffer.Length);
				}
				return null;
			}
		}

		// Token: 0x06000082 RID: 130 RVA: 0x00004063 File Offset: 0x00002263
		internal void CopyTo(Stream stream)
		{
			this.CopyToAsync(stream).Wait();
		}

		/// <summary>Serialize the HTTP content into a stream of bytes and copies it to the stream object provided as the <paramref name="stream" /> parameter.</summary>
		/// <param name="stream">The target stream.</param>
		/// <returns>The task object representing the asynchronous operation.</returns>
		// Token: 0x06000083 RID: 131 RVA: 0x00004071 File Offset: 0x00002271
		public Task CopyToAsync(Stream stream)
		{
			return this.CopyToAsync(stream, null);
		}

		/// <summary>Serialize the HTTP content into a stream of bytes and copies it to the stream object provided as the <paramref name="stream" /> parameter.</summary>
		/// <param name="stream">The target stream.</param>
		/// <param name="context">Information about the transport (channel binding token, for example). This parameter may be <see langword="null" />.</param>
		/// <returns>The task object representing the asynchronous operation.</returns>
		// Token: 0x06000084 RID: 132 RVA: 0x0000407B File Offset: 0x0000227B
		public Task CopyToAsync(Stream stream, TransportContext context)
		{
			if (stream == null)
			{
				throw new ArgumentNullException("stream");
			}
			if (this.buffer != null)
			{
				return this.buffer.CopyToAsync(stream);
			}
			return this.SerializeToStreamAsync(stream, context);
		}

		/// <summary>Serialize the HTTP content to a memory stream as an asynchronous operation.</summary>
		/// <returns>The task object representing the asynchronous operation.</returns>
		// Token: 0x06000085 RID: 133 RVA: 0x000040A8 File Offset: 0x000022A8
		protected virtual async Task<Stream> CreateContentReadStreamAsync()
		{
			await this.LoadIntoBufferAsync().ConfigureAwait(false);
			return this.buffer;
		}

		// Token: 0x06000086 RID: 134 RVA: 0x000040ED File Offset: 0x000022ED
		private static HttpContent.FixedMemoryStream CreateFixedMemoryStream(long maxBufferSize)
		{
			return new HttpContent.FixedMemoryStream(maxBufferSize);
		}

		/// <summary>Releases the unmanaged resources and disposes of the managed resources used by the <see cref="T:System.Net.Http.HttpContent" />.</summary>
		// Token: 0x06000087 RID: 135 RVA: 0x000040F5 File Offset: 0x000022F5
		public void Dispose()
		{
			this.Dispose(true);
		}

		/// <summary>Releases the unmanaged resources used by the <see cref="T:System.Net.Http.HttpContent" /> and optionally disposes of the managed resources.</summary>
		/// <param name="disposing">
		///       <see langword="true" /> to release both managed and unmanaged resources; <see langword="false" /> to releases only unmanaged resources.</param>
		// Token: 0x06000088 RID: 136 RVA: 0x000040FE File Offset: 0x000022FE
		protected virtual void Dispose(bool disposing)
		{
			if (disposing && !this.disposed)
			{
				this.disposed = true;
				if (this.buffer != null)
				{
					this.buffer.Dispose();
				}
			}
		}

		/// <summary>Serialize the HTTP content to a memory buffer as an asynchronous operation.</summary>
		/// <returns>The task object representing the asynchronous operation.</returns>
		// Token: 0x06000089 RID: 137 RVA: 0x00004125 File Offset: 0x00002325
		public Task LoadIntoBufferAsync()
		{
			return this.LoadIntoBufferAsync(2147483647L);
		}

		/// <summary>Serialize the HTTP content to a memory buffer as an asynchronous operation.</summary>
		/// <param name="maxBufferSize">The maximum size, in bytes, of the buffer to use.</param>
		/// <returns>The task object representing the asynchronous operation.</returns>
		// Token: 0x0600008A RID: 138 RVA: 0x00004134 File Offset: 0x00002334
		public async Task LoadIntoBufferAsync(long maxBufferSize)
		{
			if (this.disposed)
			{
				throw new ObjectDisposedException(base.GetType().ToString());
			}
			if (this.buffer == null)
			{
				this.buffer = HttpContent.CreateFixedMemoryStream(maxBufferSize);
				await this.SerializeToStreamAsync(this.buffer, null).ConfigureAwait(false);
				this.buffer.Seek(0L, SeekOrigin.Begin);
			}
		}

		/// <summary>Serialize the HTTP content and return a stream that represents the content as an asynchronous operation. </summary>
		/// <returns>The task object representing the asynchronous operation.</returns>
		// Token: 0x0600008B RID: 139 RVA: 0x00004184 File Offset: 0x00002384
		public async Task<Stream> ReadAsStreamAsync()
		{
			if (this.disposed)
			{
				throw new ObjectDisposedException(base.GetType().ToString());
			}
			Stream result;
			if (this.buffer != null)
			{
				result = new MemoryStream(this.buffer.GetBuffer(), 0, (int)this.buffer.Length, false);
			}
			else
			{
				if (this.stream == null)
				{
					Stream stream = await this.CreateContentReadStreamAsync().ConfigureAwait(false);
					this.stream = stream;
				}
				result = this.stream;
			}
			return result;
		}

		/// <summary>Serialize the HTTP content to a byte array as an asynchronous operation.</summary>
		/// <returns>The task object representing the asynchronous operation.</returns>
		// Token: 0x0600008C RID: 140 RVA: 0x000041CC File Offset: 0x000023CC
		public async Task<byte[]> ReadAsByteArrayAsync()
		{
			await this.LoadIntoBufferAsync().ConfigureAwait(false);
			return this.buffer.ToArray();
		}

		/// <summary>Serialize the HTTP content to a string as an asynchronous operation.</summary>
		/// <returns>The task object representing the asynchronous operation.</returns>
		// Token: 0x0600008D RID: 141 RVA: 0x00004214 File Offset: 0x00002414
		public async Task<string> ReadAsStringAsync()
		{
			await this.LoadIntoBufferAsync().ConfigureAwait(false);
			string result;
			if (this.buffer.Length == 0L)
			{
				result = string.Empty;
			}
			else
			{
				byte[] array = this.buffer.GetBuffer();
				int num = (int)this.buffer.Length;
				int num2 = 0;
				Encoding encoding;
				if (this.headers != null && this.headers.ContentType != null && this.headers.ContentType.CharSet != null)
				{
					encoding = Encoding.GetEncoding(this.headers.ContentType.CharSet);
					num2 = HttpContent.StartsWith(array, num, encoding.GetPreamble());
				}
				else
				{
					encoding = (HttpContent.GetEncodingFromBuffer(array, num, ref num2) ?? Encoding.UTF8);
				}
				result = encoding.GetString(array, num2, num - num2);
			}
			return result;
		}

		// Token: 0x0600008E RID: 142 RVA: 0x0000425C File Offset: 0x0000245C
		private static Encoding GetEncodingFromBuffer(byte[] buffer, int length, ref int preambleLength)
		{
			foreach (Encoding encoding in new Encoding[]
			{
				Encoding.UTF8,
				Encoding.UTF32,
				Encoding.Unicode
			})
			{
				if ((preambleLength = HttpContent.StartsWith(buffer, length, encoding.GetPreamble())) != 0)
				{
					return encoding;
				}
			}
			return null;
		}

		// Token: 0x0600008F RID: 143 RVA: 0x000042B4 File Offset: 0x000024B4
		private static int StartsWith(byte[] array, int length, byte[] value)
		{
			if (length < value.Length)
			{
				return 0;
			}
			for (int i = 0; i < value.Length; i++)
			{
				if (array[i] != value[i])
				{
					return 0;
				}
			}
			return value.Length;
		}

		/// <summary>Serialize the HTTP content to a stream as an asynchronous operation.</summary>
		/// <param name="stream">The target stream.</param>
		/// <param name="context">Information about the transport (channel binding token, for example). This parameter may be <see langword="null" />.</param>
		/// <returns>The task object representing the asynchronous operation.</returns>
		// Token: 0x06000090 RID: 144
		protected internal abstract Task SerializeToStreamAsync(Stream stream, TransportContext context);

		/// <summary>Determines whether the HTTP content has a valid length in bytes.</summary>
		/// <param name="length">The length in bytes of the HTTP content.</param>
		/// <returns>
		///     <see langword="true" /> if <paramref name="length" /> is a valid length; otherwise, <see langword="false" />.</returns>
		// Token: 0x06000091 RID: 145
		protected internal abstract bool TryComputeLength(out long length);

		/// <summary>Initializes a new instance of the <see cref="T:System.Net.Http.HttpContent" /> class.</summary>
		// Token: 0x06000092 RID: 146 RVA: 0x000039D8 File Offset: 0x00001BD8
		protected HttpContent()
		{
		}

		// Token: 0x0400008F RID: 143
		private HttpContent.FixedMemoryStream buffer;

		// Token: 0x04000090 RID: 144
		private Stream stream;

		// Token: 0x04000091 RID: 145
		private bool disposed;

		// Token: 0x04000092 RID: 146
		private HttpContentHeaders headers;

		// Token: 0x02000014 RID: 20
		private sealed class FixedMemoryStream : MemoryStream
		{
			// Token: 0x06000093 RID: 147 RVA: 0x000042E4 File Offset: 0x000024E4
			public FixedMemoryStream(long maxSize)
			{
				this.maxSize = maxSize;
			}

			// Token: 0x06000094 RID: 148 RVA: 0x000042F3 File Offset: 0x000024F3
			private void CheckOverflow(int count)
			{
				if (this.Length + (long)count > this.maxSize)
				{
					throw new HttpRequestException(string.Format("Cannot write more bytes to the buffer than the configured maximum buffer size: {0}", this.maxSize));
				}
			}

			// Token: 0x06000095 RID: 149 RVA: 0x00004321 File Offset: 0x00002521
			public override void WriteByte(byte value)
			{
				this.CheckOverflow(1);
				base.WriteByte(value);
			}

			// Token: 0x06000096 RID: 150 RVA: 0x00004331 File Offset: 0x00002531
			public override void Write(byte[] buffer, int offset, int count)
			{
				this.CheckOverflow(count);
				base.Write(buffer, offset, count);
			}

			// Token: 0x04000093 RID: 147
			private readonly long maxSize;
		}

		// Token: 0x02000015 RID: 21
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <CreateContentReadStreamAsync>d__12 : IAsyncStateMachine
		{
			// Token: 0x06000097 RID: 151 RVA: 0x00004344 File Offset: 0x00002544
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				HttpContent httpContent = this;
				Stream buffer;
				try
				{
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num != 0)
					{
						configuredTaskAwaiter = httpContent.LoadIntoBufferAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, HttpContent.<CreateContentReadStreamAsync>d__12>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					configuredTaskAwaiter.GetResult();
					buffer = httpContent.buffer;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult(buffer);
			}

			// Token: 0x06000098 RID: 152 RVA: 0x0000440C File Offset: 0x0000260C
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000094 RID: 148
			public int <>1__state;

			// Token: 0x04000095 RID: 149
			public AsyncTaskMethodBuilder<Stream> <>t__builder;

			// Token: 0x04000096 RID: 150
			public HttpContent <>4__this;

			// Token: 0x04000097 RID: 151
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x02000016 RID: 22
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <LoadIntoBufferAsync>d__17 : IAsyncStateMachine
		{
			// Token: 0x06000099 RID: 153 RVA: 0x0000441C File Offset: 0x0000261C
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				HttpContent httpContent = this;
				try
				{
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num != 0)
					{
						if (httpContent.disposed)
						{
							throw new ObjectDisposedException(httpContent.GetType().ToString());
						}
						if (httpContent.buffer != null)
						{
							goto IL_DA;
						}
						httpContent.buffer = HttpContent.CreateFixedMemoryStream(maxBufferSize);
						configuredTaskAwaiter = httpContent.SerializeToStreamAsync(httpContent.buffer, null).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, HttpContent.<LoadIntoBufferAsync>d__17>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					configuredTaskAwaiter.GetResult();
					httpContent.buffer.Seek(0L, SeekOrigin.Begin);
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_DA:
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x0600009A RID: 154 RVA: 0x00004528 File Offset: 0x00002728
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000098 RID: 152
			public int <>1__state;

			// Token: 0x04000099 RID: 153
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x0400009A RID: 154
			public HttpContent <>4__this;

			// Token: 0x0400009B RID: 155
			public long maxBufferSize;

			// Token: 0x0400009C RID: 156
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x02000017 RID: 23
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ReadAsStreamAsync>d__18 : IAsyncStateMachine
		{
			// Token: 0x0600009B RID: 155 RVA: 0x00004538 File Offset: 0x00002738
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				HttpContent httpContent = this;
				Stream result;
				try
				{
					ConfiguredTaskAwaitable<Stream>.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num != 0)
					{
						if (httpContent.disposed)
						{
							throw new ObjectDisposedException(httpContent.GetType().ToString());
						}
						if (httpContent.buffer != null)
						{
							result = new MemoryStream(httpContent.buffer.GetBuffer(), 0, (int)httpContent.buffer.Length, false);
							goto IL_F0;
						}
						if (httpContent.stream != null)
						{
							goto IL_CE;
						}
						configuredTaskAwaiter = httpContent.CreateContentReadStreamAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable<Stream>.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<Stream>.ConfiguredTaskAwaiter, HttpContent.<ReadAsStreamAsync>d__18>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable<Stream>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<Stream>.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					Stream result2 = configuredTaskAwaiter.GetResult();
					httpContent.stream = result2;
					IL_CE:
					result = httpContent.stream;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_F0:
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x0600009C RID: 156 RVA: 0x0000465C File Offset: 0x0000285C
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x0400009D RID: 157
			public int <>1__state;

			// Token: 0x0400009E RID: 158
			public AsyncTaskMethodBuilder<Stream> <>t__builder;

			// Token: 0x0400009F RID: 159
			public HttpContent <>4__this;

			// Token: 0x040000A0 RID: 160
			private ConfiguredTaskAwaitable<Stream>.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x02000018 RID: 24
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ReadAsByteArrayAsync>d__19 : IAsyncStateMachine
		{
			// Token: 0x0600009D RID: 157 RVA: 0x0000466C File Offset: 0x0000286C
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				HttpContent httpContent = this;
				byte[] result;
				try
				{
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num != 0)
					{
						configuredTaskAwaiter = httpContent.LoadIntoBufferAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, HttpContent.<ReadAsByteArrayAsync>d__19>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					configuredTaskAwaiter.GetResult();
					result = httpContent.buffer.ToArray();
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x0600009E RID: 158 RVA: 0x00004738 File Offset: 0x00002938
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x040000A1 RID: 161
			public int <>1__state;

			// Token: 0x040000A2 RID: 162
			public AsyncTaskMethodBuilder<byte[]> <>t__builder;

			// Token: 0x040000A3 RID: 163
			public HttpContent <>4__this;

			// Token: 0x040000A4 RID: 164
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x02000019 RID: 25
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ReadAsStringAsync>d__20 : IAsyncStateMachine
		{
			// Token: 0x0600009F RID: 159 RVA: 0x00004748 File Offset: 0x00002948
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				HttpContent httpContent = this;
				string result;
				try
				{
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num != 0)
					{
						configuredTaskAwaiter = httpContent.LoadIntoBufferAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, HttpContent.<ReadAsStringAsync>d__20>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					configuredTaskAwaiter.GetResult();
					if (httpContent.buffer.Length == 0L)
					{
						result = string.Empty;
					}
					else
					{
						byte[] buffer = httpContent.buffer.GetBuffer();
						int num3 = (int)httpContent.buffer.Length;
						int num4 = 0;
						Encoding encoding;
						if (httpContent.headers != null && httpContent.headers.ContentType != null && httpContent.headers.ContentType.CharSet != null)
						{
							encoding = Encoding.GetEncoding(httpContent.headers.ContentType.CharSet);
							num4 = HttpContent.StartsWith(buffer, num3, encoding.GetPreamble());
						}
						else
						{
							encoding = (HttpContent.GetEncodingFromBuffer(buffer, num3, ref num4) ?? Encoding.UTF8);
						}
						result = encoding.GetString(buffer, num4, num3 - num4);
					}
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x060000A0 RID: 160 RVA: 0x000048C4 File Offset: 0x00002AC4
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x040000A5 RID: 165
			public int <>1__state;

			// Token: 0x040000A6 RID: 166
			public AsyncTaskMethodBuilder<string> <>t__builder;

			// Token: 0x040000A7 RID: 167
			public HttpContent <>4__this;

			// Token: 0x040000A8 RID: 168
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}
	}
}
