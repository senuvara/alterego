﻿using System;

namespace System.Net.Http
{
	/// <summary>A base class for exceptions thrown by the <see cref="T:System.Net.Http.HttpClient" /> and <see cref="T:System.Net.Http.HttpMessageHandler" /> classes.</summary>
	// Token: 0x0200001D RID: 29
	[Serializable]
	public class HttpRequestException : Exception
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Net.Http.HttpRequestException" /> class.</summary>
		// Token: 0x060000BA RID: 186 RVA: 0x00004A86 File Offset: 0x00002C86
		public HttpRequestException()
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Net.Http.HttpRequestException" /> class with a specific message that describes the current exception.</summary>
		/// <param name="message">A message that describes the current exception.</param>
		// Token: 0x060000BB RID: 187 RVA: 0x00004A8E File Offset: 0x00002C8E
		public HttpRequestException(string message) : base(message)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Net.Http.HttpRequestException" /> class with a specific message that describes the current exception and an inner exception.</summary>
		/// <param name="message">A message that describes the current exception.</param>
		/// <param name="inner">The inner exception.</param>
		// Token: 0x060000BC RID: 188 RVA: 0x00004A97 File Offset: 0x00002C97
		public HttpRequestException(string message, Exception inner) : base(message, inner)
		{
		}
	}
}
