﻿using System;

namespace Unity
{
	// Token: 0x02000062 RID: 98
	internal sealed class ThrowStub : ObjectDisposedException
	{
		// Token: 0x06000364 RID: 868 RVA: 0x0000C9B0 File Offset: 0x0000ABB0
		public static void ThrowNotSupportedException()
		{
			throw new PlatformNotSupportedException();
		}
	}
}
