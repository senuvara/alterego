﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Net.Security;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;
using System.Threading;
using System.Threading.Tasks;

namespace System.Net.Http
{
	/// <summary>The default message handler used by <see cref="T:System.Net.Http.HttpClient" />.  </summary>
	// Token: 0x0200000F RID: 15
	public class HttpClientHandler : HttpMessageHandler
	{
		/// <summary>Creates an instance of a <see cref="T:System.Net.Http.HttpClientHandler" /> class.</summary>
		// Token: 0x0600004A RID: 74 RVA: 0x0000344C File Offset: 0x0000164C
		public HttpClientHandler()
		{
			this.allowAutoRedirect = true;
			this.maxAutomaticRedirections = 50;
			this.maxRequestContentBufferSize = 2147483647L;
			this.useCookies = true;
			this.useProxy = true;
			this.connectionGroupName = "HttpClientHandler" + Interlocked.Increment(ref HttpClientHandler.groupCounter);
		}

		// Token: 0x0600004B RID: 75 RVA: 0x000034A7 File Offset: 0x000016A7
		internal void EnsureModifiability()
		{
			if (this.sentRequest)
			{
				throw new InvalidOperationException("This instance has already started one or more requests. Properties can only be modified before sending the first request.");
			}
		}

		/// <summary>Gets or sets a value that indicates whether the handler should follow redirection responses.</summary>
		/// <returns>
		///     <see langword="true" /> if the if the handler should follow redirection responses; otherwise <see langword="false" />. The default value is <see langword="true" />.</returns>
		// Token: 0x17000006 RID: 6
		// (get) Token: 0x0600004C RID: 76 RVA: 0x000034BC File Offset: 0x000016BC
		// (set) Token: 0x0600004D RID: 77 RVA: 0x000034C4 File Offset: 0x000016C4
		public bool AllowAutoRedirect
		{
			get
			{
				return this.allowAutoRedirect;
			}
			set
			{
				this.EnsureModifiability();
				this.allowAutoRedirect = value;
			}
		}

		/// <summary>Gets or sets the type of decompression method used by the handler for automatic decompression of the HTTP content response.</summary>
		/// <returns>The automatic decompression method used by the handler. The default value is <see cref="F:System.Net.DecompressionMethods.None" />.</returns>
		// Token: 0x17000007 RID: 7
		// (get) Token: 0x0600004E RID: 78 RVA: 0x000034D3 File Offset: 0x000016D3
		// (set) Token: 0x0600004F RID: 79 RVA: 0x000034DB File Offset: 0x000016DB
		public DecompressionMethods AutomaticDecompression
		{
			get
			{
				return this.automaticDecompression;
			}
			set
			{
				this.EnsureModifiability();
				this.automaticDecompression = value;
			}
		}

		/// <summary>Gets or sets a value that indicates if the certificate is automatically picked from the certificate store or if the caller is allowed to pass in a specific client certificate.</summary>
		/// <returns>The collection of security certificates associated with this handler.</returns>
		// Token: 0x17000008 RID: 8
		// (get) Token: 0x06000050 RID: 80 RVA: 0x000034EA File Offset: 0x000016EA
		// (set) Token: 0x06000051 RID: 81 RVA: 0x000034F2 File Offset: 0x000016F2
		public ClientCertificateOption ClientCertificateOptions
		{
			get
			{
				return this.certificate;
			}
			set
			{
				this.EnsureModifiability();
				this.certificate = value;
			}
		}

		/// <summary>Gets or sets the cookie container used to store server cookies by the handler.</summary>
		/// <returns>The cookie container used to store server cookies by the handler.</returns>
		// Token: 0x17000009 RID: 9
		// (get) Token: 0x06000052 RID: 82 RVA: 0x00003504 File Offset: 0x00001704
		// (set) Token: 0x06000053 RID: 83 RVA: 0x00003529 File Offset: 0x00001729
		public CookieContainer CookieContainer
		{
			get
			{
				CookieContainer result;
				if ((result = this.cookieContainer) == null)
				{
					result = (this.cookieContainer = new CookieContainer());
				}
				return result;
			}
			set
			{
				this.EnsureModifiability();
				this.cookieContainer = value;
			}
		}

		/// <summary>Gets or sets authentication information used by this handler.</summary>
		/// <returns>The authentication credentials associated with the handler. The default is <see langword="null" />.</returns>
		// Token: 0x1700000A RID: 10
		// (get) Token: 0x06000054 RID: 84 RVA: 0x00003538 File Offset: 0x00001738
		// (set) Token: 0x06000055 RID: 85 RVA: 0x00003540 File Offset: 0x00001740
		public ICredentials Credentials
		{
			get
			{
				return this.credentials;
			}
			set
			{
				this.EnsureModifiability();
				this.credentials = value;
			}
		}

		/// <summary>Gets or sets the maximum number of redirects that the handler follows.</summary>
		/// <returns>The maximum number of redirection responses that the handler follows. The default value is 50.</returns>
		// Token: 0x1700000B RID: 11
		// (get) Token: 0x06000056 RID: 86 RVA: 0x0000354F File Offset: 0x0000174F
		// (set) Token: 0x06000057 RID: 87 RVA: 0x00003557 File Offset: 0x00001757
		public int MaxAutomaticRedirections
		{
			get
			{
				return this.maxAutomaticRedirections;
			}
			set
			{
				this.EnsureModifiability();
				if (value <= 0)
				{
					throw new ArgumentOutOfRangeException();
				}
				this.maxAutomaticRedirections = value;
			}
		}

		/// <summary>Gets or sets the maximum request content buffer size used by the handler.</summary>
		/// <returns>The maximum request content buffer size in bytes. The default value is 2 gigabytes.</returns>
		// Token: 0x1700000C RID: 12
		// (get) Token: 0x06000058 RID: 88 RVA: 0x00003570 File Offset: 0x00001770
		// (set) Token: 0x06000059 RID: 89 RVA: 0x00003578 File Offset: 0x00001778
		public long MaxRequestContentBufferSize
		{
			get
			{
				return this.maxRequestContentBufferSize;
			}
			set
			{
				this.EnsureModifiability();
				if (value < 0L)
				{
					throw new ArgumentOutOfRangeException();
				}
				this.maxRequestContentBufferSize = value;
			}
		}

		/// <summary>Gets or sets a value that indicates whether the handler sends an Authorization header with the request.</summary>
		/// <returns>
		///     <see langword="true" /> for the handler to send an HTTP Authorization header with requests after authentication has taken place; otherwise, <see langword="false" />. The default is <see langword="false" />.</returns>
		// Token: 0x1700000D RID: 13
		// (get) Token: 0x0600005A RID: 90 RVA: 0x00003592 File Offset: 0x00001792
		// (set) Token: 0x0600005B RID: 91 RVA: 0x0000359A File Offset: 0x0000179A
		public bool PreAuthenticate
		{
			get
			{
				return this.preAuthenticate;
			}
			set
			{
				this.EnsureModifiability();
				this.preAuthenticate = value;
			}
		}

		/// <summary>Gets or sets proxy information used by the handler.</summary>
		/// <returns>The proxy information used by the handler. The default value is <see langword="null" />.</returns>
		// Token: 0x1700000E RID: 14
		// (get) Token: 0x0600005C RID: 92 RVA: 0x000035A9 File Offset: 0x000017A9
		// (set) Token: 0x0600005D RID: 93 RVA: 0x000035B1 File Offset: 0x000017B1
		public IWebProxy Proxy
		{
			get
			{
				return this.proxy;
			}
			set
			{
				this.EnsureModifiability();
				if (!this.UseProxy)
				{
					throw new InvalidOperationException();
				}
				this.proxy = value;
			}
		}

		/// <summary>Gets a value that indicates whether the handler supports automatic response content decompression.</summary>
		/// <returns>
		///     <see langword="true" /> if the if the handler supports automatic response content decompression; otherwise <see langword="false" />. The default value is <see langword="true" />.</returns>
		// Token: 0x1700000F RID: 15
		// (get) Token: 0x0600005E RID: 94 RVA: 0x000035CE File Offset: 0x000017CE
		public virtual bool SupportsAutomaticDecompression
		{
			get
			{
				return true;
			}
		}

		/// <summary>Gets a value that indicates whether the handler supports proxy settings.</summary>
		/// <returns>
		///     <see langword="true" /> if the if the handler supports proxy settings; otherwise <see langword="false" />. The default value is <see langword="true" />.</returns>
		// Token: 0x17000010 RID: 16
		// (get) Token: 0x0600005F RID: 95 RVA: 0x000035CE File Offset: 0x000017CE
		public virtual bool SupportsProxy
		{
			get
			{
				return true;
			}
		}

		/// <summary>Gets a value that indicates whether the handler supports configuration settings for the <see cref="P:System.Net.Http.HttpClientHandler.AllowAutoRedirect" /> and <see cref="P:System.Net.Http.HttpClientHandler.MaxAutomaticRedirections" /> properties.</summary>
		/// <returns>
		///     <see langword="true" /> if the if the handler supports configuration settings for the <see cref="P:System.Net.Http.HttpClientHandler.AllowAutoRedirect" /> and <see cref="P:System.Net.Http.HttpClientHandler.MaxAutomaticRedirections" /> properties; otherwise <see langword="false" />. The default value is <see langword="true" />.</returns>
		// Token: 0x17000011 RID: 17
		// (get) Token: 0x06000060 RID: 96 RVA: 0x000035CE File Offset: 0x000017CE
		public virtual bool SupportsRedirectConfiguration
		{
			get
			{
				return true;
			}
		}

		/// <summary>Gets or sets a value that indicates whether the handler uses the  <see cref="P:System.Net.Http.HttpClientHandler.CookieContainer" /> property  to store server cookies and uses these cookies when sending requests.</summary>
		/// <returns>
		///     <see langword="true" /> if the if the handler supports uses the  <see cref="P:System.Net.Http.HttpClientHandler.CookieContainer" /> property  to store server cookies and uses these cookies when sending requests; otherwise <see langword="false" />. The default value is <see langword="true" />.</returns>
		// Token: 0x17000012 RID: 18
		// (get) Token: 0x06000061 RID: 97 RVA: 0x000035D1 File Offset: 0x000017D1
		// (set) Token: 0x06000062 RID: 98 RVA: 0x000035D9 File Offset: 0x000017D9
		public bool UseCookies
		{
			get
			{
				return this.useCookies;
			}
			set
			{
				this.EnsureModifiability();
				this.useCookies = value;
			}
		}

		/// <summary>Gets or sets a value that controls whether default credentials are sent with requests by the handler.</summary>
		/// <returns>
		///     <see langword="true" /> if the default credentials are used; otherwise <see langword="false" />. The default value is <see langword="false" />.</returns>
		// Token: 0x17000013 RID: 19
		// (get) Token: 0x06000063 RID: 99 RVA: 0x000035E8 File Offset: 0x000017E8
		// (set) Token: 0x06000064 RID: 100 RVA: 0x000035F0 File Offset: 0x000017F0
		public bool UseDefaultCredentials
		{
			get
			{
				return this.useDefaultCredentials;
			}
			set
			{
				this.EnsureModifiability();
				this.useDefaultCredentials = value;
			}
		}

		/// <summary>Gets or sets a value that indicates whether the handler uses a proxy for requests. </summary>
		/// <returns>
		///     <see langword="true" /> if the handler should use a proxy for requests; otherwise <see langword="false" />. The default value is <see langword="true" />.</returns>
		// Token: 0x17000014 RID: 20
		// (get) Token: 0x06000065 RID: 101 RVA: 0x000035FF File Offset: 0x000017FF
		// (set) Token: 0x06000066 RID: 102 RVA: 0x00003607 File Offset: 0x00001807
		public bool UseProxy
		{
			get
			{
				return this.useProxy;
			}
			set
			{
				this.EnsureModifiability();
				this.useProxy = value;
			}
		}

		/// <summary>Releases the unmanaged resources used by the <see cref="T:System.Net.Http.HttpClientHandler" /> and optionally disposes of the managed resources.</summary>
		/// <param name="disposing">
		///       <see langword="true" /> to release both managed and unmanaged resources; <see langword="false" /> to releases only unmanaged resources.</param>
		// Token: 0x06000067 RID: 103 RVA: 0x00003616 File Offset: 0x00001816
		protected override void Dispose(bool disposing)
		{
			if (disposing && !this.disposed)
			{
				Volatile.Write(ref this.disposed, true);
				ServicePointManager.CloseConnectionGroup(this.connectionGroupName);
			}
			base.Dispose(disposing);
		}

		// Token: 0x06000068 RID: 104 RVA: 0x00003644 File Offset: 0x00001844
		internal virtual HttpWebRequest CreateWebRequest(HttpRequestMessage request)
		{
			HttpWebRequest httpWebRequest = new HttpWebRequest(request.RequestUri);
			httpWebRequest.ThrowOnError = false;
			httpWebRequest.AllowWriteStreamBuffering = false;
			httpWebRequest.ConnectionGroupName = this.connectionGroupName;
			httpWebRequest.Method = request.Method.Method;
			httpWebRequest.ProtocolVersion = request.Version;
			if (httpWebRequest.ProtocolVersion == HttpVersion.Version10)
			{
				httpWebRequest.KeepAlive = request.Headers.ConnectionKeepAlive;
			}
			else
			{
				httpWebRequest.KeepAlive = (request.Headers.ConnectionClose != true);
			}
			if (this.allowAutoRedirect)
			{
				httpWebRequest.AllowAutoRedirect = true;
				httpWebRequest.MaximumAutomaticRedirections = this.maxAutomaticRedirections;
			}
			else
			{
				httpWebRequest.AllowAutoRedirect = false;
			}
			httpWebRequest.AutomaticDecompression = this.automaticDecompression;
			httpWebRequest.PreAuthenticate = this.preAuthenticate;
			if (this.useCookies)
			{
				httpWebRequest.CookieContainer = this.CookieContainer;
			}
			if (this.useDefaultCredentials)
			{
				httpWebRequest.UseDefaultCredentials = true;
			}
			else
			{
				httpWebRequest.Credentials = this.credentials;
			}
			if (this.useProxy)
			{
				httpWebRequest.Proxy = this.proxy;
			}
			else
			{
				httpWebRequest.Proxy = null;
			}
			httpWebRequest.ServicePoint.Expect100Continue = (request.Headers.ExpectContinue == true);
			WebHeaderCollection headers = httpWebRequest.Headers;
			foreach (KeyValuePair<string, IEnumerable<string>> keyValuePair in request.Headers)
			{
				IEnumerable<string> enumerable = keyValuePair.Value;
				if (keyValuePair.Key == "Host")
				{
					httpWebRequest.Host = request.Headers.Host;
				}
				else
				{
					if (keyValuePair.Key == "Transfer-Encoding")
					{
						enumerable = from l in enumerable
						where l != "chunked"
						select l;
					}
					string singleHeaderString = HttpHeaders.GetSingleHeaderString(keyValuePair.Key, enumerable);
					if (singleHeaderString != null)
					{
						headers.AddInternal(keyValuePair.Key, singleHeaderString);
					}
				}
			}
			return httpWebRequest;
		}

		// Token: 0x06000069 RID: 105 RVA: 0x00003878 File Offset: 0x00001A78
		private HttpResponseMessage CreateResponseMessage(HttpWebResponse wr, HttpRequestMessage requestMessage, CancellationToken cancellationToken)
		{
			HttpResponseMessage httpResponseMessage = new HttpResponseMessage(wr.StatusCode);
			httpResponseMessage.RequestMessage = requestMessage;
			httpResponseMessage.ReasonPhrase = wr.StatusDescription;
			httpResponseMessage.Content = new StreamContent(wr.GetResponseStream(), cancellationToken);
			WebHeaderCollection headers = wr.Headers;
			for (int i = 0; i < headers.Count; i++)
			{
				string key = headers.GetKey(i);
				string[] values = headers.GetValues(i);
				HttpHeaders headers2;
				if (HttpHeaders.GetKnownHeaderKind(key) == HttpHeaderKind.Content)
				{
					headers2 = httpResponseMessage.Content.Headers;
				}
				else
				{
					headers2 = httpResponseMessage.Headers;
				}
				headers2.TryAddWithoutValidation(key, values);
			}
			requestMessage.RequestUri = wr.ResponseUri;
			return httpResponseMessage;
		}

		// Token: 0x0600006A RID: 106 RVA: 0x00003918 File Offset: 0x00001B18
		private static bool MethodHasBody(HttpMethod method)
		{
			string method2 = method.Method;
			return !(method2 == "HEAD") && !(method2 == "GET") && !(method2 == "MKCOL") && !(method2 == "CONNECT") && !(method2 == "TRACE");
		}

		/// <summary>Creates an instance of  <see cref="T:System.Net.Http.HttpResponseMessage" /> based on the information provided in the <see cref="T:System.Net.Http.HttpRequestMessage" /> as an operation that will not block.</summary>
		/// <param name="request">The HTTP request message.</param>
		/// <param name="cancellationToken">A cancellation token to cancel the operation.</param>
		/// <returns>The task object representing the asynchronous operation.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="request" /> was <see langword="null" />.</exception>
		// Token: 0x0600006B RID: 107 RVA: 0x00003970 File Offset: 0x00001B70
		protected internal override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
		{
			if (this.disposed)
			{
				throw new ObjectDisposedException(base.GetType().ToString());
			}
			Volatile.Write(ref this.sentRequest, true);
			HttpWebRequest wrequest = this.CreateWebRequest(request);
			HttpWebResponse wresponse = null;
			try
			{
				using (cancellationToken.Register(delegate(object l)
				{
					((HttpWebRequest)l).Abort();
				}, wrequest))
				{
					HttpContent content = request.Content;
					if (content != null)
					{
						WebHeaderCollection headers = wrequest.Headers;
						foreach (KeyValuePair<string, IEnumerable<string>> keyValuePair in content.Headers)
						{
							foreach (string value in keyValuePair.Value)
							{
								headers.AddInternal(keyValuePair.Key, value);
							}
						}
						if (request.Headers.TransferEncodingChunked == true)
						{
							wrequest.SendChunked = true;
						}
						else
						{
							long? contentLength = content.Headers.ContentLength;
							if (contentLength != null)
							{
								wrequest.ContentLength = contentLength.Value;
							}
							else
							{
								if (this.MaxRequestContentBufferSize == 0L)
								{
									throw new InvalidOperationException("The content length of the request content can't be determined. Either set TransferEncodingChunked to true, load content into buffer, or set MaxRequestContentBufferSize.");
								}
								await content.LoadIntoBufferAsync(this.MaxRequestContentBufferSize).ConfigureAwait(false);
								wrequest.ContentLength = content.Headers.ContentLength.Value;
							}
						}
						wrequest.ResendContentFactory = new Action<Stream>(content.CopyTo);
						using (Stream stream = await wrequest.GetRequestStreamAsync().ConfigureAwait(false))
						{
							await request.Content.CopyToAsync(stream).ConfigureAwait(false);
						}
						Stream stream = null;
					}
					else if (HttpClientHandler.MethodHasBody(request.Method))
					{
						wrequest.ContentLength = 0L;
					}
					wresponse = (HttpWebResponse)(await wrequest.GetResponseAsync().ConfigureAwait(false));
					content = null;
				}
				CancellationTokenRegistration cancellationTokenRegistration = default(CancellationTokenRegistration);
			}
			catch (WebException ex)
			{
				if (ex.Status != WebExceptionStatus.RequestCanceled)
				{
					throw new HttpRequestException("An error occurred while sending the request", ex);
				}
			}
			catch (IOException inner)
			{
				throw new HttpRequestException("An error occurred while sending the request", inner);
			}
			HttpResponseMessage result;
			if (cancellationToken.IsCancellationRequested)
			{
				TaskCompletionSource<HttpResponseMessage> taskCompletionSource = new TaskCompletionSource<HttpResponseMessage>();
				taskCompletionSource.SetCanceled();
				result = await taskCompletionSource.Task;
			}
			else
			{
				result = this.CreateResponseMessage(wresponse, request, cancellationToken);
			}
			return result;
		}

		/// <summary>Gets or sets a value that indicates whether the certificate is checked against the certificate authority revocation list.</summary>
		/// <returns>
		///   <see langword="true" /> if the certificate revocation list is checked; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000015 RID: 21
		// (get) Token: 0x0600006C RID: 108 RVA: 0x000039C5 File Offset: 0x00001BC5
		// (set) Token: 0x0600006D RID: 109 RVA: 0x000039C5 File Offset: 0x00001BC5
		public bool CheckCertificateRevocationList
		{
			get
			{
				throw new NotImplementedException();
			}
			set
			{
				throw new NotImplementedException();
			}
		}

		/// <summary>Gets the maximum allowed length of the response headers.</summary>
		/// <returns>The length, in kilobytes (1024 bytes), of the response headers.</returns>
		// Token: 0x17000016 RID: 22
		// (get) Token: 0x0600006E RID: 110 RVA: 0x000039C5 File Offset: 0x00001BC5
		public X509CertificateCollection ClientCertificates
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		/// <summary>Gets or sets the credentials to submit to the proxy server for authentication.</summary>
		/// <returns>The credentials needed to authenticate a request to the proxy server.</returns>
		// Token: 0x17000017 RID: 23
		// (get) Token: 0x0600006F RID: 111 RVA: 0x000039C5 File Offset: 0x00001BC5
		// (set) Token: 0x06000070 RID: 112 RVA: 0x000039C5 File Offset: 0x00001BC5
		public ICredentials DefaultProxyCredentials
		{
			get
			{
				throw new NotImplementedException();
			}
			set
			{
				throw new NotImplementedException();
			}
		}

		/// <summary>Gets or sets the maximum number of concurrent connections allowed by an <see cref="T:System.Net.Http.HttpClient" /> object.</summary>
		/// <returns>The maximum number of concurrent connections allowed by an <see cref="T:System.Net.Http.HttpClient" /> object.</returns>
		// Token: 0x17000018 RID: 24
		// (get) Token: 0x06000071 RID: 113 RVA: 0x000039C5 File Offset: 0x00001BC5
		// (set) Token: 0x06000072 RID: 114 RVA: 0x000039C5 File Offset: 0x00001BC5
		public int MaxConnectionsPerServer
		{
			get
			{
				throw new NotImplementedException();
			}
			set
			{
				throw new NotImplementedException();
			}
		}

		/// <summary>Gets or sets the maxiumum response header length.</summary>
		/// <returns>The maximum response header length.</returns>
		// Token: 0x17000019 RID: 25
		// (get) Token: 0x06000073 RID: 115 RVA: 0x000039C5 File Offset: 0x00001BC5
		// (set) Token: 0x06000074 RID: 116 RVA: 0x000039C5 File Offset: 0x00001BC5
		public int MaxResponseHeadersLength
		{
			get
			{
				throw new NotImplementedException();
			}
			set
			{
				throw new NotImplementedException();
			}
		}

		/// <summary>Gets a set of properties for the all HttpClient requests.</summary>
		/// <returns>A set of custom properties.</returns>
		// Token: 0x1700001A RID: 26
		// (get) Token: 0x06000075 RID: 117 RVA: 0x000039C5 File Offset: 0x00001BC5
		public IDictionary<string, object> Properties
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		/// <summary>Gets or sets a callback method to validate the server certificate.</summary>
		/// <returns>A callback method to validate the server certificate.</returns>
		// Token: 0x1700001B RID: 27
		// (get) Token: 0x06000076 RID: 118 RVA: 0x000039C5 File Offset: 0x00001BC5
		// (set) Token: 0x06000077 RID: 119 RVA: 0x000039C5 File Offset: 0x00001BC5
		public Func<HttpRequestMessage, X509Certificate2, X509Chain, SslPolicyErrors, bool> ServerCertificateCustomValidationCallback
		{
			get
			{
				throw new NotImplementedException();
			}
			set
			{
				throw new NotImplementedException();
			}
		}

		/// <summary>Gets or sets the SSL protocol used by the HttpClient objects managed by the HttpClientHandler object.</summary>
		/// <returns>One of the values defined in the SecurityProtocolType enumeration.</returns>
		// Token: 0x1700001C RID: 28
		// (get) Token: 0x06000078 RID: 120 RVA: 0x000039C5 File Offset: 0x00001BC5
		// (set) Token: 0x06000079 RID: 121 RVA: 0x000039C5 File Offset: 0x00001BC5
		public SslProtocols SslProtocols
		{
			get
			{
				throw new NotImplementedException();
			}
			set
			{
				throw new NotImplementedException();
			}
		}

		// Token: 0x0400006B RID: 107
		private static long groupCounter;

		// Token: 0x0400006C RID: 108
		private bool allowAutoRedirect;

		// Token: 0x0400006D RID: 109
		private DecompressionMethods automaticDecompression;

		// Token: 0x0400006E RID: 110
		private CookieContainer cookieContainer;

		// Token: 0x0400006F RID: 111
		private ICredentials credentials;

		// Token: 0x04000070 RID: 112
		private int maxAutomaticRedirections;

		// Token: 0x04000071 RID: 113
		private long maxRequestContentBufferSize;

		// Token: 0x04000072 RID: 114
		private bool preAuthenticate;

		// Token: 0x04000073 RID: 115
		private IWebProxy proxy;

		// Token: 0x04000074 RID: 116
		private bool useCookies;

		// Token: 0x04000075 RID: 117
		private bool useDefaultCredentials;

		// Token: 0x04000076 RID: 118
		private bool useProxy;

		// Token: 0x04000077 RID: 119
		private ClientCertificateOption certificate;

		// Token: 0x04000078 RID: 120
		private bool sentRequest;

		// Token: 0x04000079 RID: 121
		private string connectionGroupName;

		// Token: 0x0400007A RID: 122
		private bool disposed;

		// Token: 0x02000010 RID: 16
		[CompilerGenerated]
		[Serializable]
		private sealed class <>c
		{
			// Token: 0x0600007A RID: 122 RVA: 0x000039CC File Offset: 0x00001BCC
			// Note: this type is marked as 'beforefieldinit'.
			static <>c()
			{
			}

			// Token: 0x0600007B RID: 123 RVA: 0x000039D8 File Offset: 0x00001BD8
			public <>c()
			{
			}

			// Token: 0x0600007C RID: 124 RVA: 0x000039E0 File Offset: 0x00001BE0
			internal bool <CreateWebRequest>b__61_0(string l)
			{
				return l != "chunked";
			}

			// Token: 0x0600007D RID: 125 RVA: 0x000039ED File Offset: 0x00001BED
			internal void <SendAsync>b__64_0(object l)
			{
				((HttpWebRequest)l).Abort();
			}

			// Token: 0x0400007B RID: 123
			public static readonly HttpClientHandler.<>c <>9 = new HttpClientHandler.<>c();

			// Token: 0x0400007C RID: 124
			public static Func<string, bool> <>9__61_0;

			// Token: 0x0400007D RID: 125
			public static Action<object> <>9__64_0;
		}

		// Token: 0x02000011 RID: 17
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <SendAsync>d__64 : IAsyncStateMachine
		{
			// Token: 0x0600007E RID: 126 RVA: 0x000039FC File Offset: 0x00001BFC
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				HttpClientHandler httpClientHandler = this;
				HttpResponseMessage result3;
				try
				{
					TaskAwaiter<HttpResponseMessage> taskAwaiter;
					if (num > 3)
					{
						if (num == 4)
						{
							TaskAwaiter<HttpResponseMessage> taskAwaiter2;
							taskAwaiter = taskAwaiter2;
							taskAwaiter2 = default(TaskAwaiter<HttpResponseMessage>);
							num = (num2 = -1);
							goto IL_4F5;
						}
						if (httpClientHandler.disposed)
						{
							throw new ObjectDisposedException(httpClientHandler.GetType().ToString());
						}
						Volatile.Write(ref httpClientHandler.sentRequest, true);
						wrequest = httpClientHandler.CreateWebRequest(request);
						wresponse = null;
					}
					try
					{
						if (num > 3)
						{
							cancellationTokenRegistration = cancellationToken.Register(new Action<object>(HttpClientHandler.<>c.<>9.<SendAsync>b__64_0), wrequest);
						}
						try
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
							ConfiguredTaskAwaitable<Stream>.ConfiguredTaskAwaiter configuredTaskAwaiter3;
							ConfiguredTaskAwaitable<WebResponse>.ConfiguredTaskAwaiter configuredTaskAwaiter5;
							switch (num)
							{
							case 0:
							{
								ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
								configuredTaskAwaiter = configuredTaskAwaiter2;
								configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
								num = (num2 = -1);
								break;
							}
							case 1:
							{
								ConfiguredTaskAwaitable<Stream>.ConfiguredTaskAwaiter configuredTaskAwaiter4;
								configuredTaskAwaiter3 = configuredTaskAwaiter4;
								configuredTaskAwaiter4 = default(ConfiguredTaskAwaitable<Stream>.ConfiguredTaskAwaiter);
								num = (num2 = -1);
								goto IL_2E7;
							}
							case 2:
								IL_2F8:
								try
								{
									if (num != 2)
									{
										configuredTaskAwaiter = request.Content.CopyToAsync(stream).ConfigureAwait(false).GetAwaiter();
										if (!configuredTaskAwaiter.IsCompleted)
										{
											num = (num2 = 2);
											ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
											this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, HttpClientHandler.<SendAsync>d__64>(ref configuredTaskAwaiter, ref this);
											return;
										}
									}
									else
									{
										ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
										configuredTaskAwaiter = configuredTaskAwaiter2;
										configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
										num = (num2 = -1);
									}
									configuredTaskAwaiter.GetResult();
								}
								finally
								{
									if (num < 0 && stream != null)
									{
										((IDisposable)stream).Dispose();
									}
								}
								stream = null;
								goto IL_3B7;
							case 3:
							{
								ConfiguredTaskAwaitable<WebResponse>.ConfiguredTaskAwaiter configuredTaskAwaiter6;
								configuredTaskAwaiter5 = configuredTaskAwaiter6;
								configuredTaskAwaiter6 = default(ConfiguredTaskAwaitable<WebResponse>.ConfiguredTaskAwaiter);
								num = (num2 = -1);
								goto IL_41D;
							}
							default:
								content = request.Content;
								if (content != null)
								{
									WebHeaderCollection headers = wrequest.Headers;
									IEnumerator<KeyValuePair<string, IEnumerable<string>>> enumerator = content.Headers.GetEnumerator();
									try
									{
										while (enumerator.MoveNext())
										{
											KeyValuePair<string, IEnumerable<string>> keyValuePair = enumerator.Current;
											IEnumerator<string> enumerator2 = keyValuePair.Value.GetEnumerator();
											try
											{
												while (enumerator2.MoveNext())
												{
													string value = enumerator2.Current;
													headers.AddInternal(keyValuePair.Key, value);
												}
											}
											finally
											{
												if (num < 0 && enumerator2 != null)
												{
													enumerator2.Dispose();
												}
											}
										}
									}
									finally
									{
										if (num < 0 && enumerator != null)
										{
											enumerator.Dispose();
										}
									}
									if (request.Headers.TransferEncodingChunked == true)
									{
										wrequest.SendChunked = true;
										goto IL_265;
									}
									long? contentLength = content.Headers.ContentLength;
									if (contentLength != null)
									{
										wrequest.ContentLength = contentLength.Value;
										goto IL_265;
									}
									if (httpClientHandler.MaxRequestContentBufferSize == 0L)
									{
										throw new InvalidOperationException("The content length of the request content can't be determined. Either set TransferEncodingChunked to true, load content into buffer, or set MaxRequestContentBufferSize.");
									}
									configuredTaskAwaiter = content.LoadIntoBufferAsync(httpClientHandler.MaxRequestContentBufferSize).ConfigureAwait(false).GetAwaiter();
									if (!configuredTaskAwaiter.IsCompleted)
									{
										num = (num2 = 0);
										ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
										this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, HttpClientHandler.<SendAsync>d__64>(ref configuredTaskAwaiter, ref this);
										return;
									}
								}
								else
								{
									if (HttpClientHandler.MethodHasBody(request.Method))
									{
										wrequest.ContentLength = 0L;
										goto IL_3B7;
									}
									goto IL_3B7;
								}
								break;
							}
							configuredTaskAwaiter.GetResult();
							wrequest.ContentLength = content.Headers.ContentLength.Value;
							IL_265:
							wrequest.ResendContentFactory = new Action<Stream>(content.CopyTo);
							configuredTaskAwaiter3 = wrequest.GetRequestStreamAsync().ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter3.IsCompleted)
							{
								num = (num2 = 1);
								ConfiguredTaskAwaitable<Stream>.ConfiguredTaskAwaiter configuredTaskAwaiter4 = configuredTaskAwaiter3;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<Stream>.ConfiguredTaskAwaiter, HttpClientHandler.<SendAsync>d__64>(ref configuredTaskAwaiter3, ref this);
								return;
							}
							IL_2E7:
							Stream result = configuredTaskAwaiter3.GetResult();
							stream = result;
							goto IL_2F8;
							IL_3B7:
							configuredTaskAwaiter5 = wrequest.GetResponseAsync().ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter5.IsCompleted)
							{
								num = (num2 = 3);
								ConfiguredTaskAwaitable<WebResponse>.ConfiguredTaskAwaiter configuredTaskAwaiter6 = configuredTaskAwaiter5;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<WebResponse>.ConfiguredTaskAwaiter, HttpClientHandler.<SendAsync>d__64>(ref configuredTaskAwaiter5, ref this);
								return;
							}
							IL_41D:
							WebResponse result2 = configuredTaskAwaiter5.GetResult();
							wresponse = (HttpWebResponse)result2;
							content = null;
						}
						finally
						{
							if (num < 0)
							{
								((IDisposable)cancellationTokenRegistration).Dispose();
							}
						}
						cancellationTokenRegistration = default(CancellationTokenRegistration);
					}
					catch (WebException ex)
					{
						if (ex.Status != WebExceptionStatus.RequestCanceled)
						{
							throw new HttpRequestException("An error occurred while sending the request", ex);
						}
					}
					catch (IOException inner)
					{
						throw new HttpRequestException("An error occurred while sending the request", inner);
					}
					if (!cancellationToken.IsCancellationRequested)
					{
						result3 = httpClientHandler.CreateResponseMessage(wresponse, request, cancellationToken);
						goto IL_533;
					}
					TaskCompletionSource<HttpResponseMessage> taskCompletionSource = new TaskCompletionSource<HttpResponseMessage>();
					taskCompletionSource.SetCanceled();
					taskAwaiter = taskCompletionSource.Task.GetAwaiter();
					if (!taskAwaiter.IsCompleted)
					{
						num = (num2 = 4);
						TaskAwaiter<HttpResponseMessage> taskAwaiter2 = taskAwaiter;
						this.<>t__builder.AwaitUnsafeOnCompleted<TaskAwaiter<HttpResponseMessage>, HttpClientHandler.<SendAsync>d__64>(ref taskAwaiter, ref this);
						return;
					}
					IL_4F5:
					result3 = taskAwaiter.GetResult();
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_533:
				num2 = -2;
				this.<>t__builder.SetResult(result3);
			}

			// Token: 0x0600007F RID: 127 RVA: 0x00003FFC File Offset: 0x000021FC
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x0400007E RID: 126
			public int <>1__state;

			// Token: 0x0400007F RID: 127
			public AsyncTaskMethodBuilder<HttpResponseMessage> <>t__builder;

			// Token: 0x04000080 RID: 128
			public HttpClientHandler <>4__this;

			// Token: 0x04000081 RID: 129
			public HttpRequestMessage request;

			// Token: 0x04000082 RID: 130
			public CancellationToken cancellationToken;

			// Token: 0x04000083 RID: 131
			private HttpWebRequest <wrequest>5__1;

			// Token: 0x04000084 RID: 132
			private HttpContent <content>5__2;

			// Token: 0x04000085 RID: 133
			private Stream <stream>5__3;

			// Token: 0x04000086 RID: 134
			private HttpWebResponse <wresponse>5__4;

			// Token: 0x04000087 RID: 135
			private CancellationTokenRegistration <>7__wrap1;

			// Token: 0x04000088 RID: 136
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;

			// Token: 0x04000089 RID: 137
			private ConfiguredTaskAwaitable<Stream>.ConfiguredTaskAwaiter <>u__2;

			// Token: 0x0400008A RID: 138
			private ConfiguredTaskAwaitable<WebResponse>.ConfiguredTaskAwaiter <>u__3;

			// Token: 0x0400008B RID: 139
			private TaskAwaiter<HttpResponseMessage> <>u__4;
		}
	}
}
