﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// Token: 0x02000060 RID: 96
[CompilerGenerated]
internal sealed class <PrivateImplementationDetails>
{
	// Token: 0x06000363 RID: 867 RVA: 0x0000C978 File Offset: 0x0000AB78
	internal static uint ComputeStringHash(string s)
	{
		uint num;
		if (s != null)
		{
			num = 2166136261U;
			for (int i = 0; i < s.Length; i++)
			{
				num = ((uint)s[i] ^ num) * 16777619U;
			}
		}
		return num;
	}

	// Token: 0x04000157 RID: 343 RVA: 0x0000C9B8 File Offset: 0x0000ABB8
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=127 A097044521F478B3A2A9A3AC52887BA733E4DE56;

	// Token: 0x02000061 RID: 97
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 127)]
	private struct __StaticArrayInitTypeSize=127
	{
	}
}
