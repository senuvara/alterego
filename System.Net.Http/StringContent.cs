﻿using System;
using System.Net.Http.Headers;
using System.Text;

namespace System.Net.Http
{
	/// <summary>Provides HTTP content based on a string.</summary>
	// Token: 0x02000026 RID: 38
	public class StringContent : ByteArrayContent
	{
		/// <summary>Creates a new instance of the <see cref="T:System.Net.Http.StringContent" /> class.</summary>
		/// <param name="content">The content used to initialize the <see cref="T:System.Net.Http.StringContent" />.</param>
		// Token: 0x06000102 RID: 258 RVA: 0x00005AF7 File Offset: 0x00003CF7
		public StringContent(string content) : this(content, null, null)
		{
		}

		/// <summary>Creates a new instance of the <see cref="T:System.Net.Http.StringContent" /> class.</summary>
		/// <param name="content">The content used to initialize the <see cref="T:System.Net.Http.StringContent" />.</param>
		/// <param name="encoding">The encoding to use for the content.</param>
		// Token: 0x06000103 RID: 259 RVA: 0x00005B02 File Offset: 0x00003D02
		public StringContent(string content, Encoding encoding) : this(content, encoding, null)
		{
		}

		/// <summary>Creates a new instance of the <see cref="T:System.Net.Http.StringContent" /> class.</summary>
		/// <param name="content">The content used to initialize the <see cref="T:System.Net.Http.StringContent" />.</param>
		/// <param name="encoding">The encoding to use for the content.</param>
		/// <param name="mediaType">The media type to use for the content.</param>
		// Token: 0x06000104 RID: 260 RVA: 0x00005B0D File Offset: 0x00003D0D
		public StringContent(string content, Encoding encoding, string mediaType) : base(StringContent.GetByteArray(content, encoding))
		{
			base.Headers.ContentType = new MediaTypeHeaderValue(mediaType ?? "text/plain")
			{
				CharSet = (encoding ?? Encoding.UTF8).WebName
			};
		}

		// Token: 0x06000105 RID: 261 RVA: 0x00005B4B File Offset: 0x00003D4B
		private static byte[] GetByteArray(string content, Encoding encoding)
		{
			return (encoding ?? Encoding.UTF8).GetBytes(content);
		}
	}
}
