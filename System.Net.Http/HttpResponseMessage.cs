﻿using System;
using System.Net.Http.Headers;
using System.Runtime.CompilerServices;
using System.Text;

namespace System.Net.Http
{
	/// <summary>Represents a HTTP response message including the status code and data.</summary>
	// Token: 0x0200001F RID: 31
	public class HttpResponseMessage : IDisposable
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Net.Http.HttpResponseMessage" /> class.</summary>
		// Token: 0x060000CF RID: 207 RVA: 0x00004D50 File Offset: 0x00002F50
		public HttpResponseMessage() : this(HttpStatusCode.OK)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Net.Http.HttpResponseMessage" /> class with a specific <see cref="P:System.Net.Http.HttpResponseMessage.StatusCode" />.</summary>
		/// <param name="statusCode">The status code of the HTTP response.</param>
		// Token: 0x060000D0 RID: 208 RVA: 0x00004D5D File Offset: 0x00002F5D
		public HttpResponseMessage(HttpStatusCode statusCode)
		{
			this.StatusCode = statusCode;
		}

		/// <summary>Gets or sets the content of a HTTP response message. </summary>
		/// <returns>The content of the HTTP response message.</returns>
		// Token: 0x1700002D RID: 45
		// (get) Token: 0x060000D1 RID: 209 RVA: 0x00004D6C File Offset: 0x00002F6C
		// (set) Token: 0x060000D2 RID: 210 RVA: 0x00004D74 File Offset: 0x00002F74
		public HttpContent Content
		{
			[CompilerGenerated]
			get
			{
				return this.<Content>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<Content>k__BackingField = value;
			}
		}

		/// <summary>Gets the collection of HTTP response headers. </summary>
		/// <returns>The collection of HTTP response headers.</returns>
		// Token: 0x1700002E RID: 46
		// (get) Token: 0x060000D3 RID: 211 RVA: 0x00004D80 File Offset: 0x00002F80
		public HttpResponseHeaders Headers
		{
			get
			{
				HttpResponseHeaders result;
				if ((result = this.headers) == null)
				{
					result = (this.headers = new HttpResponseHeaders());
				}
				return result;
			}
		}

		/// <summary>Gets a value that indicates if the HTTP response was successful.</summary>
		/// <returns>A value that indicates if the HTTP response was successful. <see langword="true" /> if <see cref="P:System.Net.Http.HttpResponseMessage.StatusCode" /> was in the range 200-299; otherwise <see langword="false" />.</returns>
		// Token: 0x1700002F RID: 47
		// (get) Token: 0x060000D4 RID: 212 RVA: 0x00004DA5 File Offset: 0x00002FA5
		public bool IsSuccessStatusCode
		{
			get
			{
				return this.statusCode >= HttpStatusCode.OK && this.statusCode < HttpStatusCode.MultipleChoices;
			}
		}

		/// <summary>Gets or sets the reason phrase which typically is sent by servers together with the status code. </summary>
		/// <returns>The reason phrase sent by the server.</returns>
		// Token: 0x17000030 RID: 48
		// (get) Token: 0x060000D5 RID: 213 RVA: 0x00004DC3 File Offset: 0x00002FC3
		// (set) Token: 0x060000D6 RID: 214 RVA: 0x00004DDA File Offset: 0x00002FDA
		public string ReasonPhrase
		{
			get
			{
				return this.reasonPhrase ?? HttpStatusDescription.Get(this.statusCode);
			}
			set
			{
				this.reasonPhrase = value;
			}
		}

		/// <summary>Gets or sets the request message which led to this response message.</summary>
		/// <returns>The request message which led to this response message.</returns>
		// Token: 0x17000031 RID: 49
		// (get) Token: 0x060000D7 RID: 215 RVA: 0x00004DE3 File Offset: 0x00002FE3
		// (set) Token: 0x060000D8 RID: 216 RVA: 0x00004DEB File Offset: 0x00002FEB
		public HttpRequestMessage RequestMessage
		{
			[CompilerGenerated]
			get
			{
				return this.<RequestMessage>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<RequestMessage>k__BackingField = value;
			}
		}

		/// <summary>Gets or sets the status code of the HTTP response.</summary>
		/// <returns>The status code of the HTTP response.</returns>
		// Token: 0x17000032 RID: 50
		// (get) Token: 0x060000D9 RID: 217 RVA: 0x00004DF4 File Offset: 0x00002FF4
		// (set) Token: 0x060000DA RID: 218 RVA: 0x00004DFC File Offset: 0x00002FFC
		public HttpStatusCode StatusCode
		{
			get
			{
				return this.statusCode;
			}
			set
			{
				if (value < (HttpStatusCode)0)
				{
					throw new ArgumentOutOfRangeException();
				}
				this.statusCode = value;
			}
		}

		/// <summary>Gets or sets the HTTP message version. </summary>
		/// <returns>The HTTP message version. The default is 1.1. </returns>
		// Token: 0x17000033 RID: 51
		// (get) Token: 0x060000DB RID: 219 RVA: 0x00004E0F File Offset: 0x0000300F
		// (set) Token: 0x060000DC RID: 220 RVA: 0x00004E20 File Offset: 0x00003020
		public Version Version
		{
			get
			{
				return this.version ?? HttpVersion.Version11;
			}
			set
			{
				if (value == null)
				{
					throw new ArgumentNullException("Version");
				}
				this.version = value;
			}
		}

		/// <summary>Releases the unmanaged resources and disposes of unmanaged resources used by the <see cref="T:System.Net.Http.HttpResponseMessage" />.</summary>
		// Token: 0x060000DD RID: 221 RVA: 0x00004E3D File Offset: 0x0000303D
		public void Dispose()
		{
			this.Dispose(true);
		}

		/// <summary>Releases the unmanaged resources used by the <see cref="T:System.Net.Http.HttpResponseMessage" /> and optionally disposes of the managed resources.</summary>
		/// <param name="disposing">
		///       <see langword="true" /> to release both managed and unmanaged resources; <see langword="false" /> to releases only unmanaged resources.</param>
		// Token: 0x060000DE RID: 222 RVA: 0x00004E46 File Offset: 0x00003046
		protected virtual void Dispose(bool disposing)
		{
			if (disposing && !this.disposed)
			{
				this.disposed = true;
				if (this.Content != null)
				{
					this.Content.Dispose();
				}
			}
		}

		/// <summary>Throws an exception if the <see cref="P:System.Net.Http.HttpResponseMessage.IsSuccessStatusCode" /> property for the HTTP response is <see langword="false" />.</summary>
		/// <returns>The HTTP response message if the call is successful.</returns>
		// Token: 0x060000DF RID: 223 RVA: 0x00004E6D File Offset: 0x0000306D
		public HttpResponseMessage EnsureSuccessStatusCode()
		{
			if (this.IsSuccessStatusCode)
			{
				return this;
			}
			throw new HttpRequestException(string.Format("{0} ({1})", (int)this.statusCode, this.ReasonPhrase));
		}

		/// <summary>Returns a string that represents the current object.</summary>
		/// <returns>A string representation of the current object.</returns>
		// Token: 0x060000E0 RID: 224 RVA: 0x00004E9C File Offset: 0x0000309C
		public override string ToString()
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("StatusCode: ").Append((int)this.StatusCode);
			stringBuilder.Append(", ReasonPhrase: '").Append(this.ReasonPhrase ?? "<null>");
			stringBuilder.Append("', Version: ").Append(this.Version);
			stringBuilder.Append(", Content: ").Append((this.Content != null) ? this.Content.ToString() : "<null>");
			stringBuilder.Append(", Headers:\r\n{\r\n").Append(this.Headers);
			if (this.Content != null)
			{
				stringBuilder.Append(this.Content.Headers);
			}
			stringBuilder.Append("}");
			return stringBuilder.ToString();
		}

		// Token: 0x040000BB RID: 187
		private HttpResponseHeaders headers;

		// Token: 0x040000BC RID: 188
		private string reasonPhrase;

		// Token: 0x040000BD RID: 189
		private HttpStatusCode statusCode;

		// Token: 0x040000BE RID: 190
		private Version version;

		// Token: 0x040000BF RID: 191
		private bool disposed;

		// Token: 0x040000C0 RID: 192
		[CompilerGenerated]
		private HttpContent <Content>k__BackingField;

		// Token: 0x040000C1 RID: 193
		[CompilerGenerated]
		private HttpRequestMessage <RequestMessage>k__BackingField;
	}
}
