﻿using System;
using System.Diagnostics;
using System.IO;
using System.Net.Http.Headers;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;

namespace System.Net.Http
{
	/// <summary>Provides a base class for sending HTTP requests and receiving HTTP responses from a resource identified by a URI. </summary>
	// Token: 0x02000007 RID: 7
	public class HttpClient : HttpMessageInvoker
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Net.Http.HttpClient" /> class.</summary>
		// Token: 0x0600000F RID: 15 RVA: 0x00002281 File Offset: 0x00000481
		public HttpClient() : this(new HttpClientHandler(), true)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Net.Http.HttpClient" /> class with a specific handler.</summary>
		/// <param name="handler">The HTTP handler stack to use for sending requests. </param>
		// Token: 0x06000010 RID: 16 RVA: 0x0000228F File Offset: 0x0000048F
		public HttpClient(HttpMessageHandler handler) : this(handler, true)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Net.Http.HttpClient" /> class with a specific handler.</summary>
		/// <param name="handler">The <see cref="T:System.Net.Http.HttpMessageHandler" /> responsible for processing the HTTP response messages.</param>
		/// <param name="disposeHandler">
		///       <see langword="true" /> if the inner handler should be disposed of by Dispose(), <see langword="false" /> if you intend to reuse the inner handler.</param>
		// Token: 0x06000011 RID: 17 RVA: 0x00002299 File Offset: 0x00000499
		public HttpClient(HttpMessageHandler handler, bool disposeHandler) : base(handler, disposeHandler)
		{
			this.buffer_size = 2147483647L;
			this.timeout = HttpClient.TimeoutDefault;
			this.cts = new CancellationTokenSource();
		}

		/// <summary>Gets or sets the base address of Uniform Resource Identifier (URI) of the Internet resource used when sending requests.</summary>
		/// <returns>The base address of Uniform Resource Identifier (URI) of the Internet resource used when sending requests.</returns>
		// Token: 0x17000002 RID: 2
		// (get) Token: 0x06000012 RID: 18 RVA: 0x000022C5 File Offset: 0x000004C5
		// (set) Token: 0x06000013 RID: 19 RVA: 0x000022CD File Offset: 0x000004CD
		public Uri BaseAddress
		{
			get
			{
				return this.base_address;
			}
			set
			{
				this.base_address = value;
			}
		}

		/// <summary>Gets the headers which should be sent with each request.</summary>
		/// <returns>The headers which should be sent with each request.</returns>
		// Token: 0x17000003 RID: 3
		// (get) Token: 0x06000014 RID: 20 RVA: 0x000022D8 File Offset: 0x000004D8
		public HttpRequestHeaders DefaultRequestHeaders
		{
			get
			{
				HttpRequestHeaders result;
				if ((result = this.headers) == null)
				{
					result = (this.headers = new HttpRequestHeaders());
				}
				return result;
			}
		}

		/// <summary>Gets or sets the maximum number of bytes to buffer when reading the response content.</summary>
		/// <returns>The maximum number of bytes to buffer when reading the response content. The default value for this property is 2 gigabytes.</returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The size specified is less than or equal to zero.</exception>
		/// <exception cref="T:System.InvalidOperationException">An operation has already been started on the current instance. </exception>
		/// <exception cref="T:System.ObjectDisposedException">The current instance has been disposed. </exception>
		// Token: 0x17000004 RID: 4
		// (get) Token: 0x06000015 RID: 21 RVA: 0x000022FD File Offset: 0x000004FD
		// (set) Token: 0x06000016 RID: 22 RVA: 0x00002305 File Offset: 0x00000505
		public long MaxResponseContentBufferSize
		{
			get
			{
				return this.buffer_size;
			}
			set
			{
				if (value <= 0L)
				{
					throw new ArgumentOutOfRangeException();
				}
				this.buffer_size = value;
			}
		}

		/// <summary>Gets or sets the timespan to wait before the request times out.</summary>
		/// <returns>The timespan to wait before the request times out.</returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The timeout specified is less than or equal to zero and is not <see cref="F:System.Threading.Timeout.InfiniteTimeSpan" />.</exception>
		/// <exception cref="T:System.InvalidOperationException">An operation has already been started on the current instance. </exception>
		/// <exception cref="T:System.ObjectDisposedException">The current instance has been disposed.</exception>
		// Token: 0x17000005 RID: 5
		// (get) Token: 0x06000017 RID: 23 RVA: 0x00002319 File Offset: 0x00000519
		// (set) Token: 0x06000018 RID: 24 RVA: 0x00002321 File Offset: 0x00000521
		public TimeSpan Timeout
		{
			get
			{
				return this.timeout;
			}
			set
			{
				if (value != System.Threading.Timeout.InfiniteTimeSpan && (value <= TimeSpan.Zero || value.TotalMilliseconds > 2147483647.0))
				{
					throw new ArgumentOutOfRangeException();
				}
				this.timeout = value;
			}
		}

		/// <summary>Cancel all pending requests on this instance.</summary>
		// Token: 0x06000019 RID: 25 RVA: 0x0000235C File Offset: 0x0000055C
		public void CancelPendingRequests()
		{
			using (CancellationTokenSource cancellationTokenSource = Interlocked.Exchange<CancellationTokenSource>(ref this.cts, new CancellationTokenSource()))
			{
				cancellationTokenSource.Cancel();
			}
		}

		/// <summary>Releases the unmanaged resources used by the <see cref="T:System.Net.Http.HttpClient" /> and optionally disposes of the managed resources.</summary>
		/// <param name="disposing">
		///       <see langword="true" /> to release both managed and unmanaged resources; <see langword="false" /> to releases only unmanaged resources.</param>
		// Token: 0x0600001A RID: 26 RVA: 0x0000239C File Offset: 0x0000059C
		protected override void Dispose(bool disposing)
		{
			if (disposing && !this.disposed)
			{
				this.disposed = true;
				this.cts.Dispose();
			}
			base.Dispose(disposing);
		}

		/// <summary>Send a DELETE request to the specified Uri as an asynchronous operation.</summary>
		/// <param name="requestUri">The Uri the request is sent to.</param>
		/// <returns>The task object representing the asynchronous operation.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="requestUri" /> was <see langword="null" />.</exception>
		/// <exception cref="T:System.InvalidOperationException">The request message was already sent by the <see cref="T:System.Net.Http.HttpClient" /> instance.</exception>
		/// <exception cref="T:System.Net.Http.HttpRequestException">The request failed due to an underlying issue such as network connectivity, DNS failure, server certificate validation or timeout.</exception>
		// Token: 0x0600001B RID: 27 RVA: 0x000023C2 File Offset: 0x000005C2
		public Task<HttpResponseMessage> DeleteAsync(string requestUri)
		{
			return this.SendAsync(new HttpRequestMessage(HttpMethod.Delete, requestUri));
		}

		/// <summary>Send a DELETE request to the specified Uri with a cancellation token as an asynchronous operation.</summary>
		/// <param name="requestUri">The Uri the request is sent to.</param>
		/// <param name="cancellationToken">A cancellation token that can be used by other objects or threads to receive notice of cancellation.</param>
		/// <returns>The task object representing the asynchronous operation.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="requestUri" /> was <see langword="null" />.</exception>
		/// <exception cref="T:System.InvalidOperationException">The request message was already sent by the <see cref="T:System.Net.Http.HttpClient" /> instance.</exception>
		/// <exception cref="T:System.Net.Http.HttpRequestException">The request failed due to an underlying issue such as network connectivity, DNS failure, server certificate validation or timeout.</exception>
		// Token: 0x0600001C RID: 28 RVA: 0x000023D5 File Offset: 0x000005D5
		public Task<HttpResponseMessage> DeleteAsync(string requestUri, CancellationToken cancellationToken)
		{
			return this.SendAsync(new HttpRequestMessage(HttpMethod.Delete, requestUri), cancellationToken);
		}

		/// <summary>Send a DELETE request to the specified Uri as an asynchronous operation.</summary>
		/// <param name="requestUri">The Uri the request is sent to.</param>
		/// <returns>The task object representing the asynchronous operation.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="requestUri" /> was <see langword="null" />.</exception>
		/// <exception cref="T:System.InvalidOperationException">The request message was already sent by the <see cref="T:System.Net.Http.HttpClient" /> instance.</exception>
		/// <exception cref="T:System.Net.Http.HttpRequestException">The request failed due to an underlying issue such as network connectivity, DNS failure, server certificate validation or timeout.</exception>
		// Token: 0x0600001D RID: 29 RVA: 0x000023E9 File Offset: 0x000005E9
		public Task<HttpResponseMessage> DeleteAsync(Uri requestUri)
		{
			return this.SendAsync(new HttpRequestMessage(HttpMethod.Delete, requestUri));
		}

		/// <summary>Send a DELETE request to the specified Uri with a cancellation token as an asynchronous operation.</summary>
		/// <param name="requestUri">The Uri the request is sent to.</param>
		/// <param name="cancellationToken">A cancellation token that can be used by other objects or threads to receive notice of cancellation.</param>
		/// <returns>The task object representing the asynchronous operation.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="requestUri" /> was <see langword="null" />.</exception>
		/// <exception cref="T:System.InvalidOperationException">The request message was already sent by the <see cref="T:System.Net.Http.HttpClient" /> instance.</exception>
		/// <exception cref="T:System.Net.Http.HttpRequestException">The request failed due to an underlying issue such as network connectivity, DNS failure, server certificate validation or timeout.</exception>
		// Token: 0x0600001E RID: 30 RVA: 0x000023FC File Offset: 0x000005FC
		public Task<HttpResponseMessage> DeleteAsync(Uri requestUri, CancellationToken cancellationToken)
		{
			return this.SendAsync(new HttpRequestMessage(HttpMethod.Delete, requestUri), cancellationToken);
		}

		/// <summary>Send a GET request to the specified Uri as an asynchronous operation.</summary>
		/// <param name="requestUri">The Uri the request is sent to.</param>
		/// <returns>The task object representing the asynchronous operation.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="requestUri" /> was <see langword="null" />.</exception>
		/// <exception cref="T:System.Net.Http.HttpRequestException">The request failed due to an underlying issue such as network connectivity, DNS failure, server certificate validation or timeout.</exception>
		// Token: 0x0600001F RID: 31 RVA: 0x00002410 File Offset: 0x00000610
		public Task<HttpResponseMessage> GetAsync(string requestUri)
		{
			return this.SendAsync(new HttpRequestMessage(HttpMethod.Get, requestUri));
		}

		/// <summary>Send a GET request to the specified Uri with a cancellation token as an asynchronous operation.</summary>
		/// <param name="requestUri">The Uri the request is sent to.</param>
		/// <param name="cancellationToken">A cancellation token that can be used by other objects or threads to receive notice of cancellation.</param>
		/// <returns>The task object representing the asynchronous operation.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="requestUri" /> was <see langword="null" />.</exception>
		/// <exception cref="T:System.Net.Http.HttpRequestException">The request failed due to an underlying issue such as network connectivity, DNS failure, server certificate validation or timeout.</exception>
		// Token: 0x06000020 RID: 32 RVA: 0x00002423 File Offset: 0x00000623
		public Task<HttpResponseMessage> GetAsync(string requestUri, CancellationToken cancellationToken)
		{
			return this.SendAsync(new HttpRequestMessage(HttpMethod.Get, requestUri), cancellationToken);
		}

		/// <summary>Send a GET request to the specified Uri with an HTTP completion option as an asynchronous operation.</summary>
		/// <param name="requestUri">The Uri the request is sent to.</param>
		/// <param name="completionOption">An HTTP completion option value that indicates when the operation should be considered completed.</param>
		/// <returns>The task object representing the asynchronous operation.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="requestUri" /> was <see langword="null" />.</exception>
		/// <exception cref="T:System.Net.Http.HttpRequestException">The request failed due to an underlying issue such as network connectivity, DNS failure, server certificate validation or timeout.</exception>
		// Token: 0x06000021 RID: 33 RVA: 0x00002437 File Offset: 0x00000637
		public Task<HttpResponseMessage> GetAsync(string requestUri, HttpCompletionOption completionOption)
		{
			return this.SendAsync(new HttpRequestMessage(HttpMethod.Get, requestUri), completionOption);
		}

		/// <summary>Send a GET request to the specified Uri with an HTTP completion option and a cancellation token as an asynchronous operation.</summary>
		/// <param name="requestUri">The Uri the request is sent to.</param>
		/// <param name="completionOption">An HTTP  completion option value that indicates when the operation should be considered completed.</param>
		/// <param name="cancellationToken">A cancellation token that can be used by other objects or threads to receive notice of cancellation.</param>
		/// <returns>The task object representing the asynchronous operation.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="requestUri" /> was <see langword="null" />.</exception>
		/// <exception cref="T:System.Net.Http.HttpRequestException">The request failed due to an underlying issue such as network connectivity, DNS failure, server certificate validation or timeout.</exception>
		// Token: 0x06000022 RID: 34 RVA: 0x0000244B File Offset: 0x0000064B
		public Task<HttpResponseMessage> GetAsync(string requestUri, HttpCompletionOption completionOption, CancellationToken cancellationToken)
		{
			return this.SendAsync(new HttpRequestMessage(HttpMethod.Get, requestUri), completionOption, cancellationToken);
		}

		/// <summary>Send a GET request to the specified Uri as an asynchronous operation.</summary>
		/// <param name="requestUri">The Uri the request is sent to.</param>
		/// <returns>The task object representing the asynchronous operation.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="requestUri" /> was <see langword="null" />.</exception>
		/// <exception cref="T:System.Net.Http.HttpRequestException">The request failed due to an underlying issue such as network connectivity, DNS failure, server certificate validation or timeout.</exception>
		// Token: 0x06000023 RID: 35 RVA: 0x00002460 File Offset: 0x00000660
		public Task<HttpResponseMessage> GetAsync(Uri requestUri)
		{
			return this.SendAsync(new HttpRequestMessage(HttpMethod.Get, requestUri));
		}

		/// <summary>Send a GET request to the specified Uri with a cancellation token as an asynchronous operation.</summary>
		/// <param name="requestUri">The Uri the request is sent to.</param>
		/// <param name="cancellationToken">A cancellation token that can be used by other objects or threads to receive notice of cancellation.</param>
		/// <returns>The task object representing the asynchronous operation.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="requestUri" /> was <see langword="null" />.</exception>
		/// <exception cref="T:System.Net.Http.HttpRequestException">The request failed due to an underlying issue such as network connectivity, DNS failure, server certificate validation or timeout.</exception>
		// Token: 0x06000024 RID: 36 RVA: 0x00002473 File Offset: 0x00000673
		public Task<HttpResponseMessage> GetAsync(Uri requestUri, CancellationToken cancellationToken)
		{
			return this.SendAsync(new HttpRequestMessage(HttpMethod.Get, requestUri), cancellationToken);
		}

		/// <summary>Send a GET request to the specified Uri with an HTTP completion option as an asynchronous operation.</summary>
		/// <param name="requestUri">The Uri the request is sent to.</param>
		/// <param name="completionOption">An HTTP completion option value that indicates when the operation should be considered completed.</param>
		/// <returns>The task object representing the asynchronous operation.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="requestUri" /> was <see langword="null" />.</exception>
		/// <exception cref="T:System.Net.Http.HttpRequestException">The request failed due to an underlying issue such as network connectivity, DNS failure, server certificate validation or timeout.</exception>
		// Token: 0x06000025 RID: 37 RVA: 0x00002487 File Offset: 0x00000687
		public Task<HttpResponseMessage> GetAsync(Uri requestUri, HttpCompletionOption completionOption)
		{
			return this.SendAsync(new HttpRequestMessage(HttpMethod.Get, requestUri), completionOption);
		}

		/// <summary>Send a GET request to the specified Uri with an HTTP completion option and a cancellation token as an asynchronous operation.</summary>
		/// <param name="requestUri">The Uri the request is sent to.</param>
		/// <param name="completionOption">An HTTP  completion option value that indicates when the operation should be considered completed.</param>
		/// <param name="cancellationToken">A cancellation token that can be used by other objects or threads to receive notice of cancellation.</param>
		/// <returns>The task object representing the asynchronous operation.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="requestUri" /> was <see langword="null" />.</exception>
		/// <exception cref="T:System.Net.Http.HttpRequestException">The request failed due to an underlying issue such as network connectivity, DNS failure, server certificate validation or timeout.</exception>
		// Token: 0x06000026 RID: 38 RVA: 0x0000249B File Offset: 0x0000069B
		public Task<HttpResponseMessage> GetAsync(Uri requestUri, HttpCompletionOption completionOption, CancellationToken cancellationToken)
		{
			return this.SendAsync(new HttpRequestMessage(HttpMethod.Get, requestUri), completionOption, cancellationToken);
		}

		/// <summary>Send a POST request to the specified Uri as an asynchronous operation.</summary>
		/// <param name="requestUri">The Uri the request is sent to.</param>
		/// <param name="content">The HTTP request content sent to the server.</param>
		/// <returns>The task object representing the asynchronous operation.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="requestUri" /> was <see langword="null" />.</exception>
		/// <exception cref="T:System.Net.Http.HttpRequestException">The request failed due to an underlying issue such as network connectivity, DNS failure, server certificate validation or timeout.</exception>
		// Token: 0x06000027 RID: 39 RVA: 0x000024B0 File Offset: 0x000006B0
		public Task<HttpResponseMessage> PostAsync(string requestUri, HttpContent content)
		{
			return this.SendAsync(new HttpRequestMessage(HttpMethod.Post, requestUri)
			{
				Content = content
			});
		}

		/// <summary>Send a POST request with a cancellation token as an asynchronous operation.</summary>
		/// <param name="requestUri">The Uri the request is sent to.</param>
		/// <param name="content">The HTTP request content sent to the server.</param>
		/// <param name="cancellationToken">A cancellation token that can be used by other objects or threads to receive notice of cancellation.</param>
		/// <returns>The task object representing the asynchronous operation.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="requestUri" /> was <see langword="null" />.</exception>
		/// <exception cref="T:System.Net.Http.HttpRequestException">The request failed due to an underlying issue such as network connectivity, DNS failure, server certificate validation or timeout.</exception>
		// Token: 0x06000028 RID: 40 RVA: 0x000024CA File Offset: 0x000006CA
		public Task<HttpResponseMessage> PostAsync(string requestUri, HttpContent content, CancellationToken cancellationToken)
		{
			return this.SendAsync(new HttpRequestMessage(HttpMethod.Post, requestUri)
			{
				Content = content
			}, cancellationToken);
		}

		/// <summary>Send a POST request to the specified Uri as an asynchronous operation.</summary>
		/// <param name="requestUri">The Uri the request is sent to.</param>
		/// <param name="content">The HTTP request content sent to the server.</param>
		/// <returns>The task object representing the asynchronous operation.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="requestUri" /> was <see langword="null" />.</exception>
		/// <exception cref="T:System.Net.Http.HttpRequestException">The request failed due to an underlying issue such as network connectivity, DNS failure, server certificate validation or timeout.</exception>
		// Token: 0x06000029 RID: 41 RVA: 0x000024E5 File Offset: 0x000006E5
		public Task<HttpResponseMessage> PostAsync(Uri requestUri, HttpContent content)
		{
			return this.SendAsync(new HttpRequestMessage(HttpMethod.Post, requestUri)
			{
				Content = content
			});
		}

		/// <summary>Send a POST request with a cancellation token as an asynchronous operation.</summary>
		/// <param name="requestUri">The Uri the request is sent to.</param>
		/// <param name="content">The HTTP request content sent to the server.</param>
		/// <param name="cancellationToken">A cancellation token that can be used by other objects or threads to receive notice of cancellation.</param>
		/// <returns>The task object representing the asynchronous operation.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="requestUri" /> was <see langword="null" />.</exception>
		/// <exception cref="T:System.Net.Http.HttpRequestException">The request failed due to an underlying issue such as network connectivity, DNS failure, server certificate validation or timeout.</exception>
		// Token: 0x0600002A RID: 42 RVA: 0x000024FF File Offset: 0x000006FF
		public Task<HttpResponseMessage> PostAsync(Uri requestUri, HttpContent content, CancellationToken cancellationToken)
		{
			return this.SendAsync(new HttpRequestMessage(HttpMethod.Post, requestUri)
			{
				Content = content
			}, cancellationToken);
		}

		/// <summary>Send a PUT request to the specified Uri as an asynchronous operation.</summary>
		/// <param name="requestUri">The Uri the request is sent to.</param>
		/// <param name="content">The HTTP request content sent to the server.</param>
		/// <returns>The task object representing the asynchronous operation.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="requestUri" /> was <see langword="null" />.</exception>
		/// <exception cref="T:System.Net.Http.HttpRequestException">The request failed due to an underlying issue such as network connectivity, DNS failure, server certificate validation or timeout.</exception>
		// Token: 0x0600002B RID: 43 RVA: 0x0000251A File Offset: 0x0000071A
		public Task<HttpResponseMessage> PutAsync(Uri requestUri, HttpContent content)
		{
			return this.SendAsync(new HttpRequestMessage(HttpMethod.Put, requestUri)
			{
				Content = content
			});
		}

		/// <summary>Send a PUT request with a cancellation token as an asynchronous operation.</summary>
		/// <param name="requestUri">The Uri the request is sent to.</param>
		/// <param name="content">The HTTP request content sent to the server.</param>
		/// <param name="cancellationToken">A cancellation token that can be used by other objects or threads to receive notice of cancellation.</param>
		/// <returns>The task object representing the asynchronous operation.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="requestUri" /> was <see langword="null" />.</exception>
		/// <exception cref="T:System.Net.Http.HttpRequestException">The request failed due to an underlying issue such as network connectivity, DNS failure, server certificate validation or timeout.</exception>
		// Token: 0x0600002C RID: 44 RVA: 0x00002534 File Offset: 0x00000734
		public Task<HttpResponseMessage> PutAsync(Uri requestUri, HttpContent content, CancellationToken cancellationToken)
		{
			return this.SendAsync(new HttpRequestMessage(HttpMethod.Put, requestUri)
			{
				Content = content
			}, cancellationToken);
		}

		/// <summary>Send a PUT request to the specified Uri as an asynchronous operation.</summary>
		/// <param name="requestUri">The Uri the request is sent to.</param>
		/// <param name="content">The HTTP request content sent to the server.</param>
		/// <returns>The task object representing the asynchronous operation.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="requestUri" /> was <see langword="null" />.</exception>
		/// <exception cref="T:System.Net.Http.HttpRequestException">The request failed due to an underlying issue such as network connectivity, DNS failure, server certificate validation or timeout.</exception>
		// Token: 0x0600002D RID: 45 RVA: 0x0000254F File Offset: 0x0000074F
		public Task<HttpResponseMessage> PutAsync(string requestUri, HttpContent content)
		{
			return this.SendAsync(new HttpRequestMessage(HttpMethod.Put, requestUri)
			{
				Content = content
			});
		}

		/// <summary>Send a PUT request with a cancellation token as an asynchronous operation.</summary>
		/// <param name="requestUri">The Uri the request is sent to.</param>
		/// <param name="content">The HTTP request content sent to the server.</param>
		/// <param name="cancellationToken">A cancellation token that can be used by other objects or threads to receive notice of cancellation.</param>
		/// <returns>The task object representing the asynchronous operation.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="requestUri" /> was <see langword="null" />.</exception>
		/// <exception cref="T:System.Net.Http.HttpRequestException">The request failed due to an underlying issue such as network connectivity, DNS failure, server certificate validation or timeout.</exception>
		// Token: 0x0600002E RID: 46 RVA: 0x00002569 File Offset: 0x00000769
		public Task<HttpResponseMessage> PutAsync(string requestUri, HttpContent content, CancellationToken cancellationToken)
		{
			return this.SendAsync(new HttpRequestMessage(HttpMethod.Put, requestUri)
			{
				Content = content
			}, cancellationToken);
		}

		/// <summary>Send an HTTP request as an asynchronous operation.</summary>
		/// <param name="request">The HTTP request message to send.</param>
		/// <returns>The task object representing the asynchronous operation.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="request" /> was <see langword="null" />.</exception>
		/// <exception cref="T:System.InvalidOperationException">The request message was already sent by the <see cref="T:System.Net.Http.HttpClient" /> instance.</exception>
		/// <exception cref="T:System.Net.Http.HttpRequestException">The request failed due to an underlying issue such as network connectivity, DNS failure, server certificate validation or timeout.</exception>
		// Token: 0x0600002F RID: 47 RVA: 0x00002584 File Offset: 0x00000784
		public Task<HttpResponseMessage> SendAsync(HttpRequestMessage request)
		{
			return this.SendAsync(request, HttpCompletionOption.ResponseContentRead, CancellationToken.None);
		}

		/// <summary>Send an HTTP request as an asynchronous operation. </summary>
		/// <param name="request">The HTTP request message to send.</param>
		/// <param name="completionOption">When the operation should complete (as soon as a response is available or after reading the whole response content).</param>
		/// <returns>The task object representing the asynchronous operation.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="request" /> was <see langword="null" />.</exception>
		/// <exception cref="T:System.InvalidOperationException">The request message was already sent by the <see cref="T:System.Net.Http.HttpClient" /> instance.</exception>
		/// <exception cref="T:System.Net.Http.HttpRequestException">The request failed due to an underlying issue such as network connectivity, DNS failure, server certificate validation or timeout.</exception>
		// Token: 0x06000030 RID: 48 RVA: 0x00002593 File Offset: 0x00000793
		public Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, HttpCompletionOption completionOption)
		{
			return this.SendAsync(request, completionOption, CancellationToken.None);
		}

		/// <summary>Send an HTTP request as an asynchronous operation.</summary>
		/// <param name="request">The HTTP request message to send.</param>
		/// <param name="cancellationToken">The cancellation token to cancel operation.</param>
		/// <returns>The task object representing the asynchronous operation.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="request" /> was <see langword="null" />.</exception>
		/// <exception cref="T:System.InvalidOperationException">The request message was already sent by the <see cref="T:System.Net.Http.HttpClient" /> instance.</exception>
		/// <exception cref="T:System.Net.Http.HttpRequestException">The request failed due to an underlying issue such as network connectivity, DNS failure, server certificate validation or timeout.</exception>
		// Token: 0x06000031 RID: 49 RVA: 0x000025A2 File Offset: 0x000007A2
		public override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
		{
			return this.SendAsync(request, HttpCompletionOption.ResponseContentRead, cancellationToken);
		}

		/// <summary>Send an HTTP request as an asynchronous operation.</summary>
		/// <param name="request">The HTTP request message to send.</param>
		/// <param name="completionOption">When the operation should complete (as soon as a response is available or after reading the whole response content).</param>
		/// <param name="cancellationToken">The cancellation token to cancel operation.</param>
		/// <returns>The task object representing the asynchronous operation.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="request" /> was <see langword="null" />.</exception>
		/// <exception cref="T:System.InvalidOperationException">The request message was already sent by the <see cref="T:System.Net.Http.HttpClient" /> instance.</exception>
		/// <exception cref="T:System.Net.Http.HttpRequestException">The request failed due to an underlying issue such as network connectivity, DNS failure, server certificate validation or timeout.</exception>
		// Token: 0x06000032 RID: 50 RVA: 0x000025B0 File Offset: 0x000007B0
		public Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, HttpCompletionOption completionOption, CancellationToken cancellationToken)
		{
			if (request == null)
			{
				throw new ArgumentNullException("request");
			}
			if (request.SetIsUsed())
			{
				throw new InvalidOperationException("Cannot send the same request message multiple times");
			}
			Uri requestUri = request.RequestUri;
			if (requestUri == null)
			{
				if (this.base_address == null)
				{
					throw new InvalidOperationException("The request URI must either be an absolute URI or BaseAddress must be set");
				}
				request.RequestUri = this.base_address;
			}
			else if (!requestUri.IsAbsoluteUri || (requestUri.Scheme == Uri.UriSchemeFile && requestUri.OriginalString.StartsWith("/", StringComparison.Ordinal)))
			{
				if (this.base_address == null)
				{
					throw new InvalidOperationException("The request URI must either be an absolute URI or BaseAddress must be set");
				}
				request.RequestUri = new Uri(this.base_address, requestUri);
			}
			if (this.headers != null)
			{
				request.Headers.AddHeaders(this.headers);
			}
			return this.SendAsyncWorker(request, completionOption, cancellationToken);
		}

		// Token: 0x06000033 RID: 51 RVA: 0x00002690 File Offset: 0x00000890
		private async Task<HttpResponseMessage> SendAsyncWorker(HttpRequestMessage request, HttpCompletionOption completionOption, CancellationToken cancellationToken)
		{
			HttpResponseMessage result;
			using (CancellationTokenSource lcts = CancellationTokenSource.CreateLinkedTokenSource(this.cts.Token, cancellationToken))
			{
				lcts.CancelAfter(this.timeout);
				Task<HttpResponseMessage> task = base.SendAsync(request, lcts.Token);
				if (task == null)
				{
					throw new InvalidOperationException("Handler failed to return a value");
				}
				HttpResponseMessage httpResponseMessage = await task.ConfigureAwait(false);
				HttpResponseMessage response = httpResponseMessage;
				if (response == null)
				{
					throw new InvalidOperationException("Handler failed to return a response");
				}
				if (response.Content != null && (completionOption & HttpCompletionOption.ResponseHeadersRead) == HttpCompletionOption.ResponseContentRead)
				{
					await response.Content.LoadIntoBufferAsync(this.MaxResponseContentBufferSize).ConfigureAwait(false);
				}
				result = response;
			}
			return result;
		}

		/// <summary>Send a GET request to the specified Uri and return the response body as a byte array in an asynchronous operation.</summary>
		/// <param name="requestUri">The Uri the request is sent to.</param>
		/// <returns>The task object representing the asynchronous operation.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="requestUri" /> was <see langword="null" />.</exception>
		/// <exception cref="T:System.Net.Http.HttpRequestException">The request failed due to an underlying issue such as network connectivity, DNS failure, server certificate validation or timeout.</exception>
		// Token: 0x06000034 RID: 52 RVA: 0x000026F0 File Offset: 0x000008F0
		public async Task<byte[]> GetByteArrayAsync(string requestUri)
		{
			HttpResponseMessage httpResponseMessage = await this.GetAsync(requestUri, HttpCompletionOption.ResponseContentRead).ConfigureAwait(false);
			byte[] result;
			using (HttpResponseMessage resp = httpResponseMessage)
			{
				resp.EnsureSuccessStatusCode();
				result = await resp.Content.ReadAsByteArrayAsync().ConfigureAwait(false);
			}
			return result;
		}

		/// <summary>Send a GET request to the specified Uri and return the response body as a byte array in an asynchronous operation.</summary>
		/// <param name="requestUri">The Uri the request is sent to.</param>
		/// <returns>The task object representing the asynchronous operation.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="requestUri" /> was <see langword="null" />.</exception>
		/// <exception cref="T:System.Net.Http.HttpRequestException">The request failed due to an underlying issue such as network connectivity, DNS failure, server certificate validation or timeout.</exception>
		// Token: 0x06000035 RID: 53 RVA: 0x00002740 File Offset: 0x00000940
		public async Task<byte[]> GetByteArrayAsync(Uri requestUri)
		{
			HttpResponseMessage httpResponseMessage = await this.GetAsync(requestUri, HttpCompletionOption.ResponseContentRead).ConfigureAwait(false);
			byte[] result;
			using (HttpResponseMessage resp = httpResponseMessage)
			{
				resp.EnsureSuccessStatusCode();
				result = await resp.Content.ReadAsByteArrayAsync().ConfigureAwait(false);
			}
			return result;
		}

		/// <summary>Send a GET request to the specified Uri and return the response body as a stream in an asynchronous operation.</summary>
		/// <param name="requestUri">The Uri the request is sent to.</param>
		/// <returns>The task object representing the asynchronous operation.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="requestUri" /> was <see langword="null" />.</exception>
		/// <exception cref="T:System.Net.Http.HttpRequestException">The request failed due to an underlying issue such as network connectivity, DNS failure, server certificate validation or timeout.</exception>
		// Token: 0x06000036 RID: 54 RVA: 0x00002790 File Offset: 0x00000990
		public async Task<Stream> GetStreamAsync(string requestUri)
		{
			object obj = await this.GetAsync(requestUri, HttpCompletionOption.ResponseHeadersRead).ConfigureAwait(false);
			obj.EnsureSuccessStatusCode();
			return await obj.Content.ReadAsStreamAsync().ConfigureAwait(false);
		}

		/// <summary>Send a GET request to the specified Uri and return the response body as a stream in an asynchronous operation.</summary>
		/// <param name="requestUri">The Uri the request is sent to.</param>
		/// <returns>The task object representing the asynchronous operation.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="requestUri" /> was <see langword="null" />.</exception>
		/// <exception cref="T:System.Net.Http.HttpRequestException">The request failed due to an underlying issue such as network connectivity, DNS failure, server certificate validation or timeout.</exception>
		// Token: 0x06000037 RID: 55 RVA: 0x000027E0 File Offset: 0x000009E0
		public async Task<Stream> GetStreamAsync(Uri requestUri)
		{
			object obj = await this.GetAsync(requestUri, HttpCompletionOption.ResponseHeadersRead).ConfigureAwait(false);
			obj.EnsureSuccessStatusCode();
			return await obj.Content.ReadAsStreamAsync().ConfigureAwait(false);
		}

		/// <summary>Send a GET request to the specified Uri and return the response body as a string in an asynchronous operation.</summary>
		/// <param name="requestUri">The Uri the request is sent to.</param>
		/// <returns>The task object representing the asynchronous operation.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="requestUri" /> was <see langword="null" />.</exception>
		/// <exception cref="T:System.Net.Http.HttpRequestException">The request failed due to an underlying issue such as network connectivity, DNS failure, server certificate validation or timeout.</exception>
		// Token: 0x06000038 RID: 56 RVA: 0x00002830 File Offset: 0x00000A30
		public async Task<string> GetStringAsync(string requestUri)
		{
			HttpResponseMessage httpResponseMessage = await this.GetAsync(requestUri, HttpCompletionOption.ResponseContentRead).ConfigureAwait(false);
			string result;
			using (HttpResponseMessage resp = httpResponseMessage)
			{
				resp.EnsureSuccessStatusCode();
				result = await resp.Content.ReadAsStringAsync().ConfigureAwait(false);
			}
			return result;
		}

		/// <summary>Send a GET request to the specified Uri and return the response body as a string in an asynchronous operation.</summary>
		/// <param name="requestUri">The Uri the request is sent to.</param>
		/// <returns>The task object representing the asynchronous operation.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="requestUri" /> was <see langword="null" />.</exception>
		/// <exception cref="T:System.Net.Http.HttpRequestException">The request failed due to an underlying issue such as network connectivity, DNS failure, server certificate validation or timeout.</exception>
		// Token: 0x06000039 RID: 57 RVA: 0x00002880 File Offset: 0x00000A80
		public async Task<string> GetStringAsync(Uri requestUri)
		{
			HttpResponseMessage httpResponseMessage = await this.GetAsync(requestUri, HttpCompletionOption.ResponseContentRead).ConfigureAwait(false);
			string result;
			using (HttpResponseMessage resp = httpResponseMessage)
			{
				resp.EnsureSuccessStatusCode();
				result = await resp.Content.ReadAsStringAsync().ConfigureAwait(false);
			}
			return result;
		}

		// Token: 0x0600003A RID: 58 RVA: 0x000028CD File Offset: 0x00000ACD
		// Note: this type is marked as 'beforefieldinit'.
		static HttpClient()
		{
		}

		// Token: 0x0600003B RID: 59 RVA: 0x000028E2 File Offset: 0x00000AE2
		[CompilerGenerated]
		[DebuggerHidden]
		private Task<HttpResponseMessage> <>n__0(HttpRequestMessage request, CancellationToken cancellationToken)
		{
			return base.SendAsync(request, cancellationToken);
		}

		// Token: 0x04000032 RID: 50
		private static readonly TimeSpan TimeoutDefault = TimeSpan.FromSeconds(100.0);

		// Token: 0x04000033 RID: 51
		private Uri base_address;

		// Token: 0x04000034 RID: 52
		private CancellationTokenSource cts;

		// Token: 0x04000035 RID: 53
		private bool disposed;

		// Token: 0x04000036 RID: 54
		private HttpRequestHeaders headers;

		// Token: 0x04000037 RID: 55
		private long buffer_size;

		// Token: 0x04000038 RID: 56
		private TimeSpan timeout;

		// Token: 0x02000008 RID: 8
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <SendAsyncWorker>d__47 : IAsyncStateMachine
		{
			// Token: 0x0600003C RID: 60 RVA: 0x000028EC File Offset: 0x00000AEC
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				HttpClient httpClient = this;
				HttpResponseMessage result2;
				try
				{
					if (num > 1)
					{
						lcts = CancellationTokenSource.CreateLinkedTokenSource(httpClient.cts.Token, cancellationToken);
					}
					try
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
						ConfiguredTaskAwaitable<HttpResponseMessage>.ConfiguredTaskAwaiter configuredTaskAwaiter3;
						if (num != 0)
						{
							if (num == 1)
							{
								ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
								configuredTaskAwaiter = configuredTaskAwaiter2;
								configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
								num = (num2 = -1);
								goto IL_171;
							}
							lcts.CancelAfter(httpClient.timeout);
							Task<HttpResponseMessage> task = httpClient.<>n__0(request, lcts.Token);
							if (task == null)
							{
								throw new InvalidOperationException("Handler failed to return a value");
							}
							configuredTaskAwaiter3 = task.ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter3.IsCompleted)
							{
								num = (num2 = 0);
								ConfiguredTaskAwaitable<HttpResponseMessage>.ConfiguredTaskAwaiter configuredTaskAwaiter4 = configuredTaskAwaiter3;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<HttpResponseMessage>.ConfiguredTaskAwaiter, HttpClient.<SendAsyncWorker>d__47>(ref configuredTaskAwaiter3, ref this);
								return;
							}
						}
						else
						{
							ConfiguredTaskAwaitable<HttpResponseMessage>.ConfiguredTaskAwaiter configuredTaskAwaiter4;
							configuredTaskAwaiter3 = configuredTaskAwaiter4;
							configuredTaskAwaiter4 = default(ConfiguredTaskAwaitable<HttpResponseMessage>.ConfiguredTaskAwaiter);
							num = (num2 = -1);
						}
						HttpResponseMessage result = configuredTaskAwaiter3.GetResult();
						response = result;
						if (response == null)
						{
							throw new InvalidOperationException("Handler failed to return a response");
						}
						if (response.Content == null || (completionOption & HttpCompletionOption.ResponseHeadersRead) != HttpCompletionOption.ResponseContentRead)
						{
							goto IL_178;
						}
						configuredTaskAwaiter = response.Content.LoadIntoBufferAsync(httpClient.MaxResponseContentBufferSize).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num = (num2 = 1);
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, HttpClient.<SendAsyncWorker>d__47>(ref configuredTaskAwaiter, ref this);
							return;
						}
						IL_171:
						configuredTaskAwaiter.GetResult();
						IL_178:
						result2 = response;
					}
					finally
					{
						if (num < 0 && lcts != null)
						{
							((IDisposable)lcts).Dispose();
						}
					}
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult(result2);
			}

			// Token: 0x0600003D RID: 61 RVA: 0x00002AF4 File Offset: 0x00000CF4
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000039 RID: 57
			public int <>1__state;

			// Token: 0x0400003A RID: 58
			public AsyncTaskMethodBuilder<HttpResponseMessage> <>t__builder;

			// Token: 0x0400003B RID: 59
			public HttpClient <>4__this;

			// Token: 0x0400003C RID: 60
			public CancellationToken cancellationToken;

			// Token: 0x0400003D RID: 61
			public HttpRequestMessage request;

			// Token: 0x0400003E RID: 62
			public HttpCompletionOption completionOption;

			// Token: 0x0400003F RID: 63
			private HttpResponseMessage <response>5__1;

			// Token: 0x04000040 RID: 64
			private CancellationTokenSource <lcts>5__2;

			// Token: 0x04000041 RID: 65
			private ConfiguredTaskAwaitable<HttpResponseMessage>.ConfiguredTaskAwaiter <>u__1;

			// Token: 0x04000042 RID: 66
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__2;
		}

		// Token: 0x02000009 RID: 9
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <GetByteArrayAsync>d__48 : IAsyncStateMachine
		{
			// Token: 0x0600003E RID: 62 RVA: 0x00002B04 File Offset: 0x00000D04
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				HttpClient httpClient = this;
				byte[] result2;
				try
				{
					ConfiguredTaskAwaitable<HttpResponseMessage>.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num != 0)
					{
						if (num == 1)
						{
							goto IL_8C;
						}
						configuredTaskAwaiter = httpClient.GetAsync(requestUri, HttpCompletionOption.ResponseContentRead).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num = (num2 = 0);
							ConfiguredTaskAwaitable<HttpResponseMessage>.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<HttpResponseMessage>.ConfiguredTaskAwaiter, HttpClient.<GetByteArrayAsync>d__48>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable<HttpResponseMessage>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<HttpResponseMessage>.ConfiguredTaskAwaiter);
						num = (num2 = -1);
					}
					HttpResponseMessage result = configuredTaskAwaiter.GetResult();
					resp = result;
					IL_8C:
					try
					{
						ConfiguredTaskAwaitable<byte[]>.ConfiguredTaskAwaiter configuredTaskAwaiter3;
						if (num != 1)
						{
							resp.EnsureSuccessStatusCode();
							configuredTaskAwaiter3 = resp.Content.ReadAsByteArrayAsync().ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter3.IsCompleted)
							{
								num = (num2 = 1);
								ConfiguredTaskAwaitable<byte[]>.ConfiguredTaskAwaiter configuredTaskAwaiter4 = configuredTaskAwaiter3;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<byte[]>.ConfiguredTaskAwaiter, HttpClient.<GetByteArrayAsync>d__48>(ref configuredTaskAwaiter3, ref this);
								return;
							}
						}
						else
						{
							ConfiguredTaskAwaitable<byte[]>.ConfiguredTaskAwaiter configuredTaskAwaiter4;
							configuredTaskAwaiter3 = configuredTaskAwaiter4;
							configuredTaskAwaiter4 = default(ConfiguredTaskAwaitable<byte[]>.ConfiguredTaskAwaiter);
							num = (num2 = -1);
						}
						result2 = configuredTaskAwaiter3.GetResult();
					}
					finally
					{
						if (num < 0 && resp != null)
						{
							((IDisposable)resp).Dispose();
						}
					}
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult(result2);
			}

			// Token: 0x0600003F RID: 63 RVA: 0x00002C9C File Offset: 0x00000E9C
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000043 RID: 67
			public int <>1__state;

			// Token: 0x04000044 RID: 68
			public AsyncTaskMethodBuilder<byte[]> <>t__builder;

			// Token: 0x04000045 RID: 69
			public HttpClient <>4__this;

			// Token: 0x04000046 RID: 70
			public string requestUri;

			// Token: 0x04000047 RID: 71
			private HttpResponseMessage <resp>5__1;

			// Token: 0x04000048 RID: 72
			private ConfiguredTaskAwaitable<HttpResponseMessage>.ConfiguredTaskAwaiter <>u__1;

			// Token: 0x04000049 RID: 73
			private ConfiguredTaskAwaitable<byte[]>.ConfiguredTaskAwaiter <>u__2;
		}

		// Token: 0x0200000A RID: 10
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <GetByteArrayAsync>d__49 : IAsyncStateMachine
		{
			// Token: 0x06000040 RID: 64 RVA: 0x00002CAC File Offset: 0x00000EAC
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				HttpClient httpClient = this;
				byte[] result2;
				try
				{
					ConfiguredTaskAwaitable<HttpResponseMessage>.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num != 0)
					{
						if (num == 1)
						{
							goto IL_8C;
						}
						configuredTaskAwaiter = httpClient.GetAsync(requestUri, HttpCompletionOption.ResponseContentRead).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num = (num2 = 0);
							ConfiguredTaskAwaitable<HttpResponseMessage>.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<HttpResponseMessage>.ConfiguredTaskAwaiter, HttpClient.<GetByteArrayAsync>d__49>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable<HttpResponseMessage>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<HttpResponseMessage>.ConfiguredTaskAwaiter);
						num = (num2 = -1);
					}
					HttpResponseMessage result = configuredTaskAwaiter.GetResult();
					resp = result;
					IL_8C:
					try
					{
						ConfiguredTaskAwaitable<byte[]>.ConfiguredTaskAwaiter configuredTaskAwaiter3;
						if (num != 1)
						{
							resp.EnsureSuccessStatusCode();
							configuredTaskAwaiter3 = resp.Content.ReadAsByteArrayAsync().ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter3.IsCompleted)
							{
								num = (num2 = 1);
								ConfiguredTaskAwaitable<byte[]>.ConfiguredTaskAwaiter configuredTaskAwaiter4 = configuredTaskAwaiter3;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<byte[]>.ConfiguredTaskAwaiter, HttpClient.<GetByteArrayAsync>d__49>(ref configuredTaskAwaiter3, ref this);
								return;
							}
						}
						else
						{
							ConfiguredTaskAwaitable<byte[]>.ConfiguredTaskAwaiter configuredTaskAwaiter4;
							configuredTaskAwaiter3 = configuredTaskAwaiter4;
							configuredTaskAwaiter4 = default(ConfiguredTaskAwaitable<byte[]>.ConfiguredTaskAwaiter);
							num = (num2 = -1);
						}
						result2 = configuredTaskAwaiter3.GetResult();
					}
					finally
					{
						if (num < 0 && resp != null)
						{
							((IDisposable)resp).Dispose();
						}
					}
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult(result2);
			}

			// Token: 0x06000041 RID: 65 RVA: 0x00002E44 File Offset: 0x00001044
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x0400004A RID: 74
			public int <>1__state;

			// Token: 0x0400004B RID: 75
			public AsyncTaskMethodBuilder<byte[]> <>t__builder;

			// Token: 0x0400004C RID: 76
			public HttpClient <>4__this;

			// Token: 0x0400004D RID: 77
			public Uri requestUri;

			// Token: 0x0400004E RID: 78
			private HttpResponseMessage <resp>5__1;

			// Token: 0x0400004F RID: 79
			private ConfiguredTaskAwaitable<HttpResponseMessage>.ConfiguredTaskAwaiter <>u__1;

			// Token: 0x04000050 RID: 80
			private ConfiguredTaskAwaitable<byte[]>.ConfiguredTaskAwaiter <>u__2;
		}

		// Token: 0x0200000B RID: 11
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <GetStreamAsync>d__50 : IAsyncStateMachine
		{
			// Token: 0x06000042 RID: 66 RVA: 0x00002E54 File Offset: 0x00001054
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				HttpClient httpClient = this;
				Stream result2;
				try
				{
					ConfiguredTaskAwaitable<Stream>.ConfiguredTaskAwaiter configuredTaskAwaiter;
					ConfiguredTaskAwaitable<HttpResponseMessage>.ConfiguredTaskAwaiter configuredTaskAwaiter3;
					if (num != 0)
					{
						if (num == 1)
						{
							ConfiguredTaskAwaitable<Stream>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<Stream>.ConfiguredTaskAwaiter);
							num2 = -1;
							goto IL_ED;
						}
						configuredTaskAwaiter3 = httpClient.GetAsync(requestUri, HttpCompletionOption.ResponseHeadersRead).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter3.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable<HttpResponseMessage>.ConfiguredTaskAwaiter configuredTaskAwaiter4 = configuredTaskAwaiter3;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<HttpResponseMessage>.ConfiguredTaskAwaiter, HttpClient.<GetStreamAsync>d__50>(ref configuredTaskAwaiter3, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable<HttpResponseMessage>.ConfiguredTaskAwaiter configuredTaskAwaiter4;
						configuredTaskAwaiter3 = configuredTaskAwaiter4;
						configuredTaskAwaiter4 = default(ConfiguredTaskAwaitable<HttpResponseMessage>.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					HttpResponseMessage result = configuredTaskAwaiter3.GetResult();
					result.EnsureSuccessStatusCode();
					configuredTaskAwaiter = result.Content.ReadAsStreamAsync().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						num2 = 1;
						ConfiguredTaskAwaitable<Stream>.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<Stream>.ConfiguredTaskAwaiter, HttpClient.<GetStreamAsync>d__50>(ref configuredTaskAwaiter, ref this);
						return;
					}
					IL_ED:
					result2 = configuredTaskAwaiter.GetResult();
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult(result2);
			}

			// Token: 0x06000043 RID: 67 RVA: 0x00002F98 File Offset: 0x00001198
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000051 RID: 81
			public int <>1__state;

			// Token: 0x04000052 RID: 82
			public AsyncTaskMethodBuilder<Stream> <>t__builder;

			// Token: 0x04000053 RID: 83
			public HttpClient <>4__this;

			// Token: 0x04000054 RID: 84
			public string requestUri;

			// Token: 0x04000055 RID: 85
			private ConfiguredTaskAwaitable<HttpResponseMessage>.ConfiguredTaskAwaiter <>u__1;

			// Token: 0x04000056 RID: 86
			private ConfiguredTaskAwaitable<Stream>.ConfiguredTaskAwaiter <>u__2;
		}

		// Token: 0x0200000C RID: 12
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <GetStreamAsync>d__51 : IAsyncStateMachine
		{
			// Token: 0x06000044 RID: 68 RVA: 0x00002FA8 File Offset: 0x000011A8
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				HttpClient httpClient = this;
				Stream result2;
				try
				{
					ConfiguredTaskAwaitable<Stream>.ConfiguredTaskAwaiter configuredTaskAwaiter;
					ConfiguredTaskAwaitable<HttpResponseMessage>.ConfiguredTaskAwaiter configuredTaskAwaiter3;
					if (num != 0)
					{
						if (num == 1)
						{
							ConfiguredTaskAwaitable<Stream>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<Stream>.ConfiguredTaskAwaiter);
							num2 = -1;
							goto IL_ED;
						}
						configuredTaskAwaiter3 = httpClient.GetAsync(requestUri, HttpCompletionOption.ResponseHeadersRead).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter3.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable<HttpResponseMessage>.ConfiguredTaskAwaiter configuredTaskAwaiter4 = configuredTaskAwaiter3;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<HttpResponseMessage>.ConfiguredTaskAwaiter, HttpClient.<GetStreamAsync>d__51>(ref configuredTaskAwaiter3, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable<HttpResponseMessage>.ConfiguredTaskAwaiter configuredTaskAwaiter4;
						configuredTaskAwaiter3 = configuredTaskAwaiter4;
						configuredTaskAwaiter4 = default(ConfiguredTaskAwaitable<HttpResponseMessage>.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					HttpResponseMessage result = configuredTaskAwaiter3.GetResult();
					result.EnsureSuccessStatusCode();
					configuredTaskAwaiter = result.Content.ReadAsStreamAsync().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						num2 = 1;
						ConfiguredTaskAwaitable<Stream>.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<Stream>.ConfiguredTaskAwaiter, HttpClient.<GetStreamAsync>d__51>(ref configuredTaskAwaiter, ref this);
						return;
					}
					IL_ED:
					result2 = configuredTaskAwaiter.GetResult();
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult(result2);
			}

			// Token: 0x06000045 RID: 69 RVA: 0x000030EC File Offset: 0x000012EC
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000057 RID: 87
			public int <>1__state;

			// Token: 0x04000058 RID: 88
			public AsyncTaskMethodBuilder<Stream> <>t__builder;

			// Token: 0x04000059 RID: 89
			public HttpClient <>4__this;

			// Token: 0x0400005A RID: 90
			public Uri requestUri;

			// Token: 0x0400005B RID: 91
			private ConfiguredTaskAwaitable<HttpResponseMessage>.ConfiguredTaskAwaiter <>u__1;

			// Token: 0x0400005C RID: 92
			private ConfiguredTaskAwaitable<Stream>.ConfiguredTaskAwaiter <>u__2;
		}

		// Token: 0x0200000D RID: 13
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <GetStringAsync>d__52 : IAsyncStateMachine
		{
			// Token: 0x06000046 RID: 70 RVA: 0x000030FC File Offset: 0x000012FC
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				HttpClient httpClient = this;
				string result2;
				try
				{
					ConfiguredTaskAwaitable<HttpResponseMessage>.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num != 0)
					{
						if (num == 1)
						{
							goto IL_8C;
						}
						configuredTaskAwaiter = httpClient.GetAsync(requestUri, HttpCompletionOption.ResponseContentRead).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num = (num2 = 0);
							ConfiguredTaskAwaitable<HttpResponseMessage>.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<HttpResponseMessage>.ConfiguredTaskAwaiter, HttpClient.<GetStringAsync>d__52>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable<HttpResponseMessage>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<HttpResponseMessage>.ConfiguredTaskAwaiter);
						num = (num2 = -1);
					}
					HttpResponseMessage result = configuredTaskAwaiter.GetResult();
					resp = result;
					IL_8C:
					try
					{
						ConfiguredTaskAwaitable<string>.ConfiguredTaskAwaiter configuredTaskAwaiter3;
						if (num != 1)
						{
							resp.EnsureSuccessStatusCode();
							configuredTaskAwaiter3 = resp.Content.ReadAsStringAsync().ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter3.IsCompleted)
							{
								num = (num2 = 1);
								ConfiguredTaskAwaitable<string>.ConfiguredTaskAwaiter configuredTaskAwaiter4 = configuredTaskAwaiter3;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<string>.ConfiguredTaskAwaiter, HttpClient.<GetStringAsync>d__52>(ref configuredTaskAwaiter3, ref this);
								return;
							}
						}
						else
						{
							ConfiguredTaskAwaitable<string>.ConfiguredTaskAwaiter configuredTaskAwaiter4;
							configuredTaskAwaiter3 = configuredTaskAwaiter4;
							configuredTaskAwaiter4 = default(ConfiguredTaskAwaitable<string>.ConfiguredTaskAwaiter);
							num = (num2 = -1);
						}
						result2 = configuredTaskAwaiter3.GetResult();
					}
					finally
					{
						if (num < 0 && resp != null)
						{
							((IDisposable)resp).Dispose();
						}
					}
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult(result2);
			}

			// Token: 0x06000047 RID: 71 RVA: 0x00003294 File Offset: 0x00001494
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x0400005D RID: 93
			public int <>1__state;

			// Token: 0x0400005E RID: 94
			public AsyncTaskMethodBuilder<string> <>t__builder;

			// Token: 0x0400005F RID: 95
			public HttpClient <>4__this;

			// Token: 0x04000060 RID: 96
			public string requestUri;

			// Token: 0x04000061 RID: 97
			private HttpResponseMessage <resp>5__1;

			// Token: 0x04000062 RID: 98
			private ConfiguredTaskAwaitable<HttpResponseMessage>.ConfiguredTaskAwaiter <>u__1;

			// Token: 0x04000063 RID: 99
			private ConfiguredTaskAwaitable<string>.ConfiguredTaskAwaiter <>u__2;
		}

		// Token: 0x0200000E RID: 14
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <GetStringAsync>d__53 : IAsyncStateMachine
		{
			// Token: 0x06000048 RID: 72 RVA: 0x000032A4 File Offset: 0x000014A4
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				HttpClient httpClient = this;
				string result2;
				try
				{
					ConfiguredTaskAwaitable<HttpResponseMessage>.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num != 0)
					{
						if (num == 1)
						{
							goto IL_8C;
						}
						configuredTaskAwaiter = httpClient.GetAsync(requestUri, HttpCompletionOption.ResponseContentRead).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num = (num2 = 0);
							ConfiguredTaskAwaitable<HttpResponseMessage>.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<HttpResponseMessage>.ConfiguredTaskAwaiter, HttpClient.<GetStringAsync>d__53>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable<HttpResponseMessage>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<HttpResponseMessage>.ConfiguredTaskAwaiter);
						num = (num2 = -1);
					}
					HttpResponseMessage result = configuredTaskAwaiter.GetResult();
					resp = result;
					IL_8C:
					try
					{
						ConfiguredTaskAwaitable<string>.ConfiguredTaskAwaiter configuredTaskAwaiter3;
						if (num != 1)
						{
							resp.EnsureSuccessStatusCode();
							configuredTaskAwaiter3 = resp.Content.ReadAsStringAsync().ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter3.IsCompleted)
							{
								num = (num2 = 1);
								ConfiguredTaskAwaitable<string>.ConfiguredTaskAwaiter configuredTaskAwaiter4 = configuredTaskAwaiter3;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<string>.ConfiguredTaskAwaiter, HttpClient.<GetStringAsync>d__53>(ref configuredTaskAwaiter3, ref this);
								return;
							}
						}
						else
						{
							ConfiguredTaskAwaitable<string>.ConfiguredTaskAwaiter configuredTaskAwaiter4;
							configuredTaskAwaiter3 = configuredTaskAwaiter4;
							configuredTaskAwaiter4 = default(ConfiguredTaskAwaitable<string>.ConfiguredTaskAwaiter);
							num = (num2 = -1);
						}
						result2 = configuredTaskAwaiter3.GetResult();
					}
					finally
					{
						if (num < 0 && resp != null)
						{
							((IDisposable)resp).Dispose();
						}
					}
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult(result2);
			}

			// Token: 0x06000049 RID: 73 RVA: 0x0000343C File Offset: 0x0000163C
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000064 RID: 100
			public int <>1__state;

			// Token: 0x04000065 RID: 101
			public AsyncTaskMethodBuilder<string> <>t__builder;

			// Token: 0x04000066 RID: 102
			public HttpClient <>4__this;

			// Token: 0x04000067 RID: 103
			public Uri requestUri;

			// Token: 0x04000068 RID: 104
			private HttpResponseMessage <resp>5__1;

			// Token: 0x04000069 RID: 105
			private ConfiguredTaskAwaitable<HttpResponseMessage>.ConfiguredTaskAwaiter <>u__1;

			// Token: 0x0400006A RID: 106
			private ConfiguredTaskAwaitable<string>.ConfiguredTaskAwaiter <>u__2;
		}
	}
}
