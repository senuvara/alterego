﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace System.Net.Http
{
	/// <summary>Provides a collection of <see cref="T:System.Net.Http.HttpContent" /> objects that get serialized using the multipart/* content type specification.</summary>
	// Token: 0x02000022 RID: 34
	public class MultipartContent : HttpContent, IEnumerable<HttpContent>, IEnumerable
	{
		/// <summary>Creates a new instance of the <see cref="T:System.Net.Http.MultipartContent" /> class.</summary>
		// Token: 0x060000E9 RID: 233 RVA: 0x000050E2 File Offset: 0x000032E2
		public MultipartContent() : this("mixed")
		{
		}

		/// <summary>Creates a new instance of the <see cref="T:System.Net.Http.MultipartContent" /> class.</summary>
		/// <param name="subtype">The subtype of the multipart content.</param>
		/// <exception cref="T:System.ArgumentException">The <paramref name="subtype" /> was <see langword="null" /> or contains only white space characters.</exception>
		// Token: 0x060000EA RID: 234 RVA: 0x000050F0 File Offset: 0x000032F0
		public MultipartContent(string subtype) : this(subtype, Guid.NewGuid().ToString("D", CultureInfo.InvariantCulture))
		{
		}

		/// <summary>Creates a new instance of the <see cref="T:System.Net.Http.MultipartContent" /> class.</summary>
		/// <param name="subtype">The subtype of the multipart content.</param>
		/// <param name="boundary">The boundary string for the multipart content.</param>
		/// <exception cref="T:System.ArgumentException">The <paramref name="subtype" /> was <see langword="null" /> or an empty string.The <paramref name="boundary" /> was <see langword="null" /> or contains only white space characters.-or-The <paramref name="boundary" /> ends with a space character.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The length of the <paramref name="boundary" /> was greater than 70.</exception>
		// Token: 0x060000EB RID: 235 RVA: 0x0000511C File Offset: 0x0000331C
		public MultipartContent(string subtype, string boundary)
		{
			if (string.IsNullOrWhiteSpace(subtype))
			{
				throw new ArgumentException("boundary");
			}
			if (string.IsNullOrWhiteSpace(boundary))
			{
				throw new ArgumentException("boundary");
			}
			if (boundary.Length > 70)
			{
				throw new ArgumentOutOfRangeException("boundary");
			}
			if (boundary.Last<char>() == ' ' || !MultipartContent.IsValidRFC2049(boundary))
			{
				throw new ArgumentException("boundary");
			}
			this.boundary = boundary;
			this.nested_content = new List<HttpContent>(2);
			base.Headers.ContentType = new MediaTypeHeaderValue("multipart/" + subtype)
			{
				Parameters = 
				{
					new NameValueHeaderValue("boundary", "\"" + boundary + "\"")
				}
			};
		}

		// Token: 0x060000EC RID: 236 RVA: 0x000051DC File Offset: 0x000033DC
		private static bool IsValidRFC2049(string s)
		{
			foreach (char c in s)
			{
				if ((c < 'a' || c > 'z') && (c < 'A' || c > 'Z') && (c < '0' || c > '9'))
				{
					if (c <= ':')
					{
						switch (c)
						{
						case '\'':
						case '(':
						case ')':
						case '+':
						case ',':
						case '-':
						case '.':
						case '/':
							goto IL_71;
						case '*':
							break;
						default:
							if (c == ':')
							{
								goto IL_71;
							}
							break;
						}
					}
					else if (c == '=' || c == '?')
					{
						goto IL_71;
					}
					return false;
				}
				IL_71:;
			}
			return true;
		}

		/// <summary>Add multipart HTTP content to a collection of <see cref="T:System.Net.Http.HttpContent" /> objects that get serialized using the multipart/* content type specification.</summary>
		/// <param name="content">The HTTP content to add to the collection.</param>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="content" /> was <see langword="null" />.</exception>
		// Token: 0x060000ED RID: 237 RVA: 0x00005268 File Offset: 0x00003468
		public virtual void Add(HttpContent content)
		{
			if (content == null)
			{
				throw new ArgumentNullException("content");
			}
			if (this.nested_content == null)
			{
				this.nested_content = new List<HttpContent>();
			}
			this.nested_content.Add(content);
		}

		/// <summary>Releases the unmanaged resources used by the <see cref="T:System.Net.Http.MultipartContent" /> and optionally disposes of the managed resources.</summary>
		/// <param name="disposing">
		///       <see langword="true" /> to release both managed and unmanaged resources; <see langword="false" /> to releases only unmanaged resources.</param>
		// Token: 0x060000EE RID: 238 RVA: 0x00005298 File Offset: 0x00003498
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				foreach (HttpContent httpContent in this.nested_content)
				{
					httpContent.Dispose();
				}
				this.nested_content = null;
			}
			base.Dispose(disposing);
		}

		/// <summary>Serialize the multipart HTTP content to a stream as an asynchronous operation.</summary>
		/// <param name="stream">The target stream.</param>
		/// <param name="context">Information about the transport (channel binding token, for example). This parameter may be <see langword="null" />.</param>
		/// <returns>The task object representing the asynchronous operation.</returns>
		// Token: 0x060000EF RID: 239 RVA: 0x000052FC File Offset: 0x000034FC
		protected internal override async Task SerializeToStreamAsync(Stream stream, TransportContext context)
		{
			StringBuilder sb = new StringBuilder();
			sb.Append('-').Append('-');
			sb.Append(this.boundary);
			sb.Append('\r').Append('\n');
			byte[] bytes;
			for (int i = 0; i < this.nested_content.Count; i++)
			{
				HttpContent c = this.nested_content[i];
				foreach (KeyValuePair<string, IEnumerable<string>> keyValuePair in c.Headers)
				{
					sb.Append(keyValuePair.Key);
					sb.Append(':').Append(' ');
					foreach (string value in keyValuePair.Value)
					{
						sb.Append(value);
					}
					sb.Append('\r').Append('\n');
				}
				sb.Append('\r').Append('\n');
				bytes = Encoding.ASCII.GetBytes(sb.ToString());
				sb.Clear();
				await stream.WriteAsync(bytes, 0, bytes.Length).ConfigureAwait(false);
				await c.SerializeToStreamAsync(stream, context).ConfigureAwait(false);
				if (i != this.nested_content.Count - 1)
				{
					sb.Append('\r').Append('\n');
					sb.Append('-').Append('-');
					sb.Append(this.boundary);
					sb.Append('\r').Append('\n');
				}
				c = null;
			}
			sb.Append('\r').Append('\n');
			sb.Append('-').Append('-');
			sb.Append(this.boundary);
			sb.Append('-').Append('-');
			sb.Append('\r').Append('\n');
			bytes = Encoding.ASCII.GetBytes(sb.ToString());
			await stream.WriteAsync(bytes, 0, bytes.Length).ConfigureAwait(false);
		}

		/// <summary>Determines whether the HTTP multipart content has a valid length in bytes.</summary>
		/// <param name="length">The length in bytes of the HHTP content.</param>
		/// <returns>
		///     <see langword="true" /> if <paramref name="length" /> is a valid length; otherwise, <see langword="false" />.</returns>
		// Token: 0x060000F0 RID: 240 RVA: 0x00005354 File Offset: 0x00003554
		protected internal override bool TryComputeLength(out long length)
		{
			length = (long)(12 + 2 * this.boundary.Length);
			for (int i = 0; i < this.nested_content.Count; i++)
			{
				HttpContent httpContent = this.nested_content[i];
				foreach (KeyValuePair<string, IEnumerable<string>> keyValuePair in httpContent.Headers)
				{
					length += (long)keyValuePair.Key.Length;
					length += 4L;
					foreach (string text in keyValuePair.Value)
					{
						length += (long)text.Length;
					}
				}
				long num;
				if (!httpContent.TryComputeLength(out num))
				{
					return false;
				}
				length += 2L;
				length += num;
				if (i != this.nested_content.Count - 1)
				{
					length += 6L;
					length += (long)this.boundary.Length;
				}
			}
			return true;
		}

		/// <summary>Returns an enumerator that iterates through the collection of <see cref="T:System.Net.Http.HttpContent" /> objects that get serialized using the multipart/* content type specification..</summary>
		/// <returns>An object that can be used to iterate through the collection.</returns>
		// Token: 0x060000F1 RID: 241 RVA: 0x0000547C File Offset: 0x0000367C
		public IEnumerator<HttpContent> GetEnumerator()
		{
			return this.nested_content.GetEnumerator();
		}

		/// <summary>The explicit implementation of the <see cref="M:System.Net.Http.MultipartContent.GetEnumerator" /> method.</summary>
		/// <returns>An object that can be used to iterate through the collection.</returns>
		// Token: 0x060000F2 RID: 242 RVA: 0x0000547C File Offset: 0x0000367C
		IEnumerator IEnumerable.GetEnumerator()
		{
			return this.nested_content.GetEnumerator();
		}

		// Token: 0x040000C8 RID: 200
		private List<HttpContent> nested_content;

		// Token: 0x040000C9 RID: 201
		private readonly string boundary;

		// Token: 0x02000023 RID: 35
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <SerializeToStreamAsync>d__8 : IAsyncStateMachine
		{
			// Token: 0x060000F3 RID: 243 RVA: 0x00005490 File Offset: 0x00003690
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				MultipartContent multipartContent = this;
				try
				{
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
					switch (num)
					{
					case 0:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num = (num2 = -1);
						break;
					}
					case 1:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num = (num2 = -1);
						goto IL_25D;
					}
					case 2:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num = (num2 = -1);
						goto IL_3DD;
					}
					default:
						sb = new StringBuilder();
						sb.Append('-').Append('-');
						sb.Append(multipartContent.boundary);
						sb.Append('\r').Append('\n');
						i = 0;
						goto IL_2E3;
					}
					IL_1E4:
					configuredTaskAwaiter.GetResult();
					configuredTaskAwaiter = c.SerializeToStreamAsync(stream, context).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						num = (num2 = 1);
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, MultipartContent.<SerializeToStreamAsync>d__8>(ref configuredTaskAwaiter, ref this);
						return;
					}
					IL_25D:
					configuredTaskAwaiter.GetResult();
					if (i != multipartContent.nested_content.Count - 1)
					{
						sb.Append('\r').Append('\n');
						sb.Append('-').Append('-');
						sb.Append(multipartContent.boundary);
						sb.Append('\r').Append('\n');
					}
					c = null;
					int num3 = i;
					i = num3 + 1;
					IL_2E3:
					if (i >= multipartContent.nested_content.Count)
					{
						sb.Append('\r').Append('\n');
						sb.Append('-').Append('-');
						sb.Append(multipartContent.boundary);
						sb.Append('-').Append('-');
						sb.Append('\r').Append('\n');
						byte[] bytes = Encoding.ASCII.GetBytes(sb.ToString());
						configuredTaskAwaiter = stream.WriteAsync(bytes, 0, bytes.Length).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num = (num2 = 2);
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, MultipartContent.<SerializeToStreamAsync>d__8>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						c = multipartContent.nested_content[i];
						IEnumerator<KeyValuePair<string, IEnumerable<string>>> enumerator = c.Headers.GetEnumerator();
						try
						{
							while (enumerator.MoveNext())
							{
								KeyValuePair<string, IEnumerable<string>> keyValuePair = enumerator.Current;
								sb.Append(keyValuePair.Key);
								sb.Append(':').Append(' ');
								IEnumerator<string> enumerator2 = keyValuePair.Value.GetEnumerator();
								try
								{
									while (enumerator2.MoveNext())
									{
										string value = enumerator2.Current;
										sb.Append(value);
									}
								}
								finally
								{
									if (num < 0 && enumerator2 != null)
									{
										enumerator2.Dispose();
									}
								}
								sb.Append('\r').Append('\n');
							}
						}
						finally
						{
							if (num < 0 && enumerator != null)
							{
								enumerator.Dispose();
							}
						}
						sb.Append('\r').Append('\n');
						byte[] bytes = Encoding.ASCII.GetBytes(sb.ToString());
						sb.Clear();
						configuredTaskAwaiter = stream.WriteAsync(bytes, 0, bytes.Length).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num = (num2 = 0);
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, MultipartContent.<SerializeToStreamAsync>d__8>(ref configuredTaskAwaiter, ref this);
							return;
						}
						goto IL_1E4;
					}
					IL_3DD:
					configuredTaskAwaiter.GetResult();
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x060000F4 RID: 244 RVA: 0x000058FC File Offset: 0x00003AFC
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x040000CA RID: 202
			public int <>1__state;

			// Token: 0x040000CB RID: 203
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x040000CC RID: 204
			public MultipartContent <>4__this;

			// Token: 0x040000CD RID: 205
			private StringBuilder <sb>5__1;

			// Token: 0x040000CE RID: 206
			public Stream stream;

			// Token: 0x040000CF RID: 207
			private HttpContent <c>5__2;

			// Token: 0x040000D0 RID: 208
			public TransportContext context;

			// Token: 0x040000D1 RID: 209
			private int <i>5__3;

			// Token: 0x040000D2 RID: 210
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}
	}
}
