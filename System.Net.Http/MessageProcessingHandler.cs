﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;

namespace System.Net.Http
{
	/// <summary>A base type for handlers which only do some small processing of request and/or response messages.</summary>
	// Token: 0x02000020 RID: 32
	public abstract class MessageProcessingHandler : DelegatingHandler
	{
		/// <summary>Creates an instance of a <see cref="T:System.Net.Http.MessageProcessingHandler" /> class.</summary>
		// Token: 0x060000E1 RID: 225 RVA: 0x00004F6B File Offset: 0x0000316B
		protected MessageProcessingHandler()
		{
		}

		/// <summary>Creates an instance of a <see cref="T:System.Net.Http.MessageProcessingHandler" /> class with a specific inner handler.</summary>
		/// <param name="innerHandler">The inner handler which is responsible for processing the HTTP response messages.</param>
		// Token: 0x060000E2 RID: 226 RVA: 0x00004F73 File Offset: 0x00003173
		protected MessageProcessingHandler(HttpMessageHandler innerHandler) : base(innerHandler)
		{
		}

		/// <summary>Performs processing on each request sent to the server.</summary>
		/// <param name="request">The HTTP request message to process.</param>
		/// <param name="cancellationToken">A cancellation token that can be used by other objects or threads to receive notice of cancellation.</param>
		/// <returns>The HTTP request message that was processed.</returns>
		// Token: 0x060000E3 RID: 227
		protected abstract HttpRequestMessage ProcessRequest(HttpRequestMessage request, CancellationToken cancellationToken);

		/// <summary>Perform processing on each response from the server.</summary>
		/// <param name="response">The HTTP response message to process.</param>
		/// <param name="cancellationToken">A cancellation token that can be used by other objects or threads to receive notice of cancellation.</param>
		/// <returns>The HTTP response message that was processed.</returns>
		// Token: 0x060000E4 RID: 228
		protected abstract HttpResponseMessage ProcessResponse(HttpResponseMessage response, CancellationToken cancellationToken);

		/// <summary>Sends an HTTP request to the inner handler to send to the server as an asynchronous operation.</summary>
		/// <param name="request">The HTTP request message to send to the server.</param>
		/// <param name="cancellationToken">A cancellation token that can be used by other objects or threads to receive notice of cancellation.</param>
		/// <returns>The task object representing the asynchronous operation.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="request" /> was <see langword="null" />.</exception>
		// Token: 0x060000E5 RID: 229 RVA: 0x00004F7C File Offset: 0x0000317C
		protected internal sealed override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
		{
			request = this.ProcessRequest(request, cancellationToken);
			HttpResponseMessage response = await base.SendAsync(request, cancellationToken).ConfigureAwait(false);
			return this.ProcessResponse(response, cancellationToken);
		}

		// Token: 0x060000E6 RID: 230 RVA: 0x00004FD1 File Offset: 0x000031D1
		[DebuggerHidden]
		[CompilerGenerated]
		private Task<HttpResponseMessage> <>n__0(HttpRequestMessage request, CancellationToken cancellationToken)
		{
			return base.SendAsync(request, cancellationToken);
		}

		// Token: 0x02000021 RID: 33
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <SendAsync>d__4 : IAsyncStateMachine
		{
			// Token: 0x060000E7 RID: 231 RVA: 0x00004FDC File Offset: 0x000031DC
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				MessageProcessingHandler messageProcessingHandler = this;
				HttpResponseMessage result2;
				try
				{
					ConfiguredTaskAwaitable<HttpResponseMessage>.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num != 0)
					{
						request = messageProcessingHandler.ProcessRequest(request, cancellationToken);
						configuredTaskAwaiter = messageProcessingHandler.<>n__0(request, cancellationToken).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable<HttpResponseMessage>.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<HttpResponseMessage>.ConfiguredTaskAwaiter, MessageProcessingHandler.<SendAsync>d__4>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable<HttpResponseMessage>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<HttpResponseMessage>.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					HttpResponseMessage result = configuredTaskAwaiter.GetResult();
					result2 = messageProcessingHandler.ProcessResponse(result, cancellationToken);
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult(result2);
			}

			// Token: 0x060000E8 RID: 232 RVA: 0x000050D4 File Offset: 0x000032D4
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x040000C2 RID: 194
			public int <>1__state;

			// Token: 0x040000C3 RID: 195
			public AsyncTaskMethodBuilder<HttpResponseMessage> <>t__builder;

			// Token: 0x040000C4 RID: 196
			public HttpRequestMessage request;

			// Token: 0x040000C5 RID: 197
			public MessageProcessingHandler <>4__this;

			// Token: 0x040000C6 RID: 198
			public CancellationToken cancellationToken;

			// Token: 0x040000C7 RID: 199
			private ConfiguredTaskAwaitable<HttpResponseMessage>.ConfiguredTaskAwaiter <>u__1;
		}
	}
}
