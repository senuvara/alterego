﻿using System;
using System.Collections.Generic;
using System.Net.Http.Headers;
using System.Runtime.CompilerServices;
using System.Text;

namespace System.Net.Http
{
	/// <summary>Represents a HTTP request message.</summary>
	// Token: 0x0200001E RID: 30
	public class HttpRequestMessage : IDisposable
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Net.Http.HttpRequestMessage" /> class.</summary>
		// Token: 0x060000BD RID: 189 RVA: 0x00004AA1 File Offset: 0x00002CA1
		public HttpRequestMessage()
		{
			this.method = HttpMethod.Get;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Net.Http.HttpRequestMessage" /> class with an HTTP method and a request <see cref="T:System.Uri" />.</summary>
		/// <param name="method">The HTTP method.</param>
		/// <param name="requestUri">A string that represents the request  <see cref="T:System.Uri" />.</param>
		// Token: 0x060000BE RID: 190 RVA: 0x00004AB4 File Offset: 0x00002CB4
		public HttpRequestMessage(HttpMethod method, string requestUri) : this(method, string.IsNullOrEmpty(requestUri) ? null : new Uri(requestUri, UriKind.RelativeOrAbsolute))
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Net.Http.HttpRequestMessage" /> class with an HTTP method and a request <see cref="T:System.Uri" />.</summary>
		/// <param name="method">The HTTP method.</param>
		/// <param name="requestUri">The <see cref="T:System.Uri" /> to request.</param>
		// Token: 0x060000BF RID: 191 RVA: 0x00004ACF File Offset: 0x00002CCF
		public HttpRequestMessage(HttpMethod method, Uri requestUri)
		{
			this.Method = method;
			this.RequestUri = requestUri;
		}

		/// <summary>Gets or sets the contents of the HTTP message. </summary>
		/// <returns>The content of a message</returns>
		// Token: 0x17000027 RID: 39
		// (get) Token: 0x060000C0 RID: 192 RVA: 0x00004AE5 File Offset: 0x00002CE5
		// (set) Token: 0x060000C1 RID: 193 RVA: 0x00004AED File Offset: 0x00002CED
		public HttpContent Content
		{
			[CompilerGenerated]
			get
			{
				return this.<Content>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<Content>k__BackingField = value;
			}
		}

		/// <summary>Gets the collection of HTTP request headers.</summary>
		/// <returns>The collection of HTTP request headers.</returns>
		// Token: 0x17000028 RID: 40
		// (get) Token: 0x060000C2 RID: 194 RVA: 0x00004AF8 File Offset: 0x00002CF8
		public HttpRequestHeaders Headers
		{
			get
			{
				HttpRequestHeaders result;
				if ((result = this.headers) == null)
				{
					result = (this.headers = new HttpRequestHeaders());
				}
				return result;
			}
		}

		/// <summary>Gets or sets the HTTP method used by the HTTP request message.</summary>
		/// <returns>The HTTP method used by the request message. The default is the GET method.</returns>
		// Token: 0x17000029 RID: 41
		// (get) Token: 0x060000C3 RID: 195 RVA: 0x00004B1D File Offset: 0x00002D1D
		// (set) Token: 0x060000C4 RID: 196 RVA: 0x00004B25 File Offset: 0x00002D25
		public HttpMethod Method
		{
			get
			{
				return this.method;
			}
			set
			{
				if (value == null)
				{
					throw new ArgumentNullException("method");
				}
				this.method = value;
			}
		}

		/// <summary>Gets a set of properties for the HTTP request.</summary>
		/// <returns>Returns <see cref="T:System.Collections.Generic.IDictionary`2" />.</returns>
		// Token: 0x1700002A RID: 42
		// (get) Token: 0x060000C5 RID: 197 RVA: 0x00004B44 File Offset: 0x00002D44
		public IDictionary<string, object> Properties
		{
			get
			{
				Dictionary<string, object> result;
				if ((result = this.properties) == null)
				{
					result = (this.properties = new Dictionary<string, object>());
				}
				return result;
			}
		}

		/// <summary>Gets or sets the <see cref="T:System.Uri" /> used for the HTTP request.</summary>
		/// <returns>The <see cref="T:System.Uri" /> used for the HTTP request.</returns>
		// Token: 0x1700002B RID: 43
		// (get) Token: 0x060000C6 RID: 198 RVA: 0x00004B69 File Offset: 0x00002D69
		// (set) Token: 0x060000C7 RID: 199 RVA: 0x00004B71 File Offset: 0x00002D71
		public Uri RequestUri
		{
			get
			{
				return this.uri;
			}
			set
			{
				if (value != null && value.IsAbsoluteUri && !HttpRequestMessage.IsAllowedAbsoluteUri(value))
				{
					throw new ArgumentException("Only http or https scheme is allowed");
				}
				this.uri = value;
			}
		}

		// Token: 0x060000C8 RID: 200 RVA: 0x00004BA0 File Offset: 0x00002DA0
		private static bool IsAllowedAbsoluteUri(Uri uri)
		{
			return uri.Scheme == Uri.UriSchemeHttp || uri.Scheme == Uri.UriSchemeHttps || (uri.Scheme == Uri.UriSchemeFile && uri.OriginalString.StartsWith("/", StringComparison.Ordinal));
		}

		/// <summary>Gets or sets the HTTP message version.</summary>
		/// <returns>The HTTP message version. The default is 1.1.</returns>
		// Token: 0x1700002C RID: 44
		// (get) Token: 0x060000C9 RID: 201 RVA: 0x00004BFB File Offset: 0x00002DFB
		// (set) Token: 0x060000CA RID: 202 RVA: 0x00004C0C File Offset: 0x00002E0C
		public Version Version
		{
			get
			{
				return this.version ?? HttpVersion.Version11;
			}
			set
			{
				if (value == null)
				{
					throw new ArgumentNullException("Version");
				}
				this.version = value;
			}
		}

		/// <summary>Releases the unmanaged resources and disposes of the managed resources used by the <see cref="T:System.Net.Http.HttpRequestMessage" />.</summary>
		// Token: 0x060000CB RID: 203 RVA: 0x00004C29 File Offset: 0x00002E29
		public void Dispose()
		{
			this.Dispose(true);
		}

		/// <summary>Releases the unmanaged resources used by the <see cref="T:System.Net.Http.HttpRequestMessage" /> and optionally disposes of the managed resources.</summary>
		/// <param name="disposing">
		///       <see langword="true" /> to release both managed and unmanaged resources; <see langword="false" /> to releases only unmanaged resources.</param>
		// Token: 0x060000CC RID: 204 RVA: 0x00004C32 File Offset: 0x00002E32
		protected virtual void Dispose(bool disposing)
		{
			if (disposing && !this.disposed)
			{
				this.disposed = true;
				if (this.Content != null)
				{
					this.Content.Dispose();
				}
			}
		}

		// Token: 0x060000CD RID: 205 RVA: 0x00004C59 File Offset: 0x00002E59
		internal bool SetIsUsed()
		{
			if (this.is_used)
			{
				return true;
			}
			this.is_used = true;
			return false;
		}

		/// <summary>Returns a string that represents the current object.</summary>
		/// <returns>A string representation of the current object.</returns>
		// Token: 0x060000CE RID: 206 RVA: 0x00004C70 File Offset: 0x00002E70
		public override string ToString()
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("Method: ").Append(this.method);
			stringBuilder.Append(", RequestUri: '").Append((this.RequestUri != null) ? this.RequestUri.ToString() : "<null>");
			stringBuilder.Append("', Version: ").Append(this.Version);
			stringBuilder.Append(", Content: ").Append((this.Content != null) ? this.Content.ToString() : "<null>");
			stringBuilder.Append(", Headers:\r\n{\r\n").Append(this.Headers);
			if (this.Content != null)
			{
				stringBuilder.Append(this.Content.Headers);
			}
			stringBuilder.Append("}");
			return stringBuilder.ToString();
		}

		// Token: 0x040000B3 RID: 179
		private HttpRequestHeaders headers;

		// Token: 0x040000B4 RID: 180
		private HttpMethod method;

		// Token: 0x040000B5 RID: 181
		private Version version;

		// Token: 0x040000B6 RID: 182
		private Dictionary<string, object> properties;

		// Token: 0x040000B7 RID: 183
		private Uri uri;

		// Token: 0x040000B8 RID: 184
		private bool is_used;

		// Token: 0x040000B9 RID: 185
		private bool disposed;

		// Token: 0x040000BA RID: 186
		[CompilerGenerated]
		private HttpContent <Content>k__BackingField;
	}
}
