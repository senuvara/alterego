﻿using System;

namespace System.Net.Http
{
	/// <summary>Specifies how client certificates are provided.</summary>
	// Token: 0x02000004 RID: 4
	public enum ClientCertificateOption
	{
		/// <summary>The application manually provides the client certificates to the <see cref="T:System.Net.Http.WebRequestHandler" />. This value is the default. </summary>
		// Token: 0x0400002E RID: 46
		Manual,
		/// <summary>The <see cref="T:System.Net.Http.HttpClientHandler" /> will attempt to provide  all available client certificates  automatically.</summary>
		// Token: 0x0400002F RID: 47
		Automatic
	}
}
