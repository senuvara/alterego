﻿using System;
using System.Globalization;
using System.Runtime.CompilerServices;

namespace System.Net.Http.Headers
{
	/// <summary>Represents an If-Range header value which can either be a date/time or an entity-tag value.</summary>
	// Token: 0x02000057 RID: 87
	public class RangeConditionHeaderValue : ICloneable
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Net.Http.Headers.RangeConditionHeaderValue" /> class.</summary>
		/// <param name="date">A date value used to initialize the new instance.</param>
		// Token: 0x060002E8 RID: 744 RVA: 0x0000B1A3 File Offset: 0x000093A3
		public RangeConditionHeaderValue(DateTimeOffset date)
		{
			this.Date = new DateTimeOffset?(date);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Net.Http.Headers.RangeConditionHeaderValue" /> class.</summary>
		/// <param name="entityTag">An <see cref="T:System.Net.Http.Headers.EntityTagHeaderValue" /> object used to initialize the new instance.</param>
		// Token: 0x060002E9 RID: 745 RVA: 0x0000B1B7 File Offset: 0x000093B7
		public RangeConditionHeaderValue(EntityTagHeaderValue entityTag)
		{
			if (entityTag == null)
			{
				throw new ArgumentNullException("entityTag");
			}
			this.EntityTag = entityTag;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Net.Http.Headers.RangeConditionHeaderValue" /> class.</summary>
		/// <param name="entityTag">An entity tag represented as a string used to initialize the new instance.</param>
		// Token: 0x060002EA RID: 746 RVA: 0x0000B1D4 File Offset: 0x000093D4
		public RangeConditionHeaderValue(string entityTag) : this(new EntityTagHeaderValue(entityTag))
		{
		}

		/// <summary>Gets the date from the <see cref="T:System.Net.Http.Headers.RangeConditionHeaderValue" /> object.</summary>
		/// <returns>The date from the <see cref="T:System.Net.Http.Headers.RangeConditionHeaderValue" /> object.</returns>
		// Token: 0x170000B0 RID: 176
		// (get) Token: 0x060002EB RID: 747 RVA: 0x0000B1E2 File Offset: 0x000093E2
		// (set) Token: 0x060002EC RID: 748 RVA: 0x0000B1EA File Offset: 0x000093EA
		public DateTimeOffset? Date
		{
			[CompilerGenerated]
			get
			{
				return this.<Date>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<Date>k__BackingField = value;
			}
		}

		/// <summary>Gets the entity tag from the <see cref="T:System.Net.Http.Headers.RangeConditionHeaderValue" /> object.</summary>
		/// <returns>The entity tag from the <see cref="T:System.Net.Http.Headers.RangeConditionHeaderValue" /> object.</returns>
		// Token: 0x170000B1 RID: 177
		// (get) Token: 0x060002ED RID: 749 RVA: 0x0000B1F3 File Offset: 0x000093F3
		// (set) Token: 0x060002EE RID: 750 RVA: 0x0000B1FB File Offset: 0x000093FB
		public EntityTagHeaderValue EntityTag
		{
			[CompilerGenerated]
			get
			{
				return this.<EntityTag>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<EntityTag>k__BackingField = value;
			}
		}

		/// <summary>Creates a new object that is a copy of the current <see cref="T:System.Net.Http.Headers.RangeConditionHeaderValue" /> instance.</summary>
		/// <returns>A copy of the current instance.</returns>
		// Token: 0x060002EF RID: 751 RVA: 0x00005BA5 File Offset: 0x00003DA5
		object ICloneable.Clone()
		{
			return base.MemberwiseClone();
		}

		/// <summary>Determines whether the specified <see cref="T:System.Object" /> is equal to the current <see cref="T:System.Net.Http.Headers.RangeConditionHeaderValue" /> object.</summary>
		/// <param name="obj">The object to compare with the current object.</param>
		/// <returns>
		///     <see langword="true" /> if the specified <see cref="T:System.Object" /> is equal to the current object; otherwise, <see langword="false" />.</returns>
		// Token: 0x060002F0 RID: 752 RVA: 0x0000B204 File Offset: 0x00009404
		public override bool Equals(object obj)
		{
			RangeConditionHeaderValue rangeConditionHeaderValue = obj as RangeConditionHeaderValue;
			if (rangeConditionHeaderValue == null)
			{
				return false;
			}
			if (this.EntityTag == null)
			{
				return this.Date == rangeConditionHeaderValue.Date;
			}
			return this.EntityTag.Equals(rangeConditionHeaderValue.EntityTag);
		}

		/// <summary>Serves as a hash function for an <see cref="T:System.Net.Http.Headers.RangeConditionHeaderValue" /> object.</summary>
		/// <returns>A hash code for the current object.</returns>
		// Token: 0x060002F1 RID: 753 RVA: 0x0000B278 File Offset: 0x00009478
		public override int GetHashCode()
		{
			if (this.EntityTag == null)
			{
				return this.Date.GetHashCode();
			}
			return this.EntityTag.GetHashCode();
		}

		/// <summary>Converts a string to an <see cref="T:System.Net.Http.Headers.RangeConditionHeaderValue" /> instance.</summary>
		/// <param name="input">A string that represents range condition header value information.</param>
		/// <returns>A <see cref="T:System.Net.Http.Headers.RangeConditionHeaderValue" /> instance.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="input" /> is a <see langword="null" /> reference.</exception>
		/// <exception cref="T:System.FormatException">
		///         <paramref name="input" /> is not valid range Condition header value information.</exception>
		// Token: 0x060002F2 RID: 754 RVA: 0x0000B2B0 File Offset: 0x000094B0
		public static RangeConditionHeaderValue Parse(string input)
		{
			RangeConditionHeaderValue result;
			if (RangeConditionHeaderValue.TryParse(input, out result))
			{
				return result;
			}
			throw new FormatException(input);
		}

		/// <summary>Determines whether a string is valid <see cref="T:System.Net.Http.Headers.RangeConditionHeaderValue" /> information.</summary>
		/// <param name="input">The string to validate.</param>
		/// <param name="parsedValue">The <see cref="T:System.Net.Http.Headers.RangeConditionHeaderValue" /> version of the string.</param>
		/// <returns>
		///     <see langword="true" /> if <paramref name="input" /> is valid <see cref="T:System.Net.Http.Headers.RangeConditionHeaderValue" /> information; otherwise, <see langword="false" />.</returns>
		// Token: 0x060002F3 RID: 755 RVA: 0x0000B2D0 File Offset: 0x000094D0
		public static bool TryParse(string input, out RangeConditionHeaderValue parsedValue)
		{
			parsedValue = null;
			Lexer lexer = new Lexer(input);
			Token token = lexer.Scan(false);
			bool isWeak;
			if (token == Token.Type.Token)
			{
				if (lexer.GetStringValue(token) != "W")
				{
					DateTimeOffset date;
					if (!Lexer.TryGetDateValue(input, out date))
					{
						return false;
					}
					parsedValue = new RangeConditionHeaderValue(date);
					return true;
				}
				else
				{
					if (lexer.PeekChar() != 47)
					{
						return false;
					}
					isWeak = true;
					lexer.EatChar();
					token = lexer.Scan(false);
				}
			}
			else
			{
				isWeak = false;
			}
			if (token != Token.Type.QuotedString)
			{
				return false;
			}
			if (lexer.Scan(false) != Token.Type.End)
			{
				return false;
			}
			parsedValue = new RangeConditionHeaderValue(new EntityTagHeaderValue
			{
				Tag = lexer.GetStringValue(token),
				IsWeak = isWeak
			});
			return true;
		}

		/// <summary>Returns a string that represents the current <see cref="T:System.Net.Http.Headers.RangeConditionHeaderValue" /> object.</summary>
		/// <returns>A string that represents the current object.</returns>
		// Token: 0x060002F4 RID: 756 RVA: 0x0000B380 File Offset: 0x00009580
		public override string ToString()
		{
			if (this.EntityTag != null)
			{
				return this.EntityTag.ToString();
			}
			return this.Date.Value.ToString("r", CultureInfo.InvariantCulture);
		}

		// Token: 0x04000143 RID: 323
		[CompilerGenerated]
		private DateTimeOffset? <Date>k__BackingField;

		// Token: 0x04000144 RID: 324
		[CompilerGenerated]
		private EntityTagHeaderValue <EntityTag>k__BackingField;
	}
}
