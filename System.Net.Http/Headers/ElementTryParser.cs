﻿using System;

namespace System.Net.Http.Headers
{
	// Token: 0x0200002A RID: 42
	// (Invoke) Token: 0x0600013E RID: 318
	internal delegate bool ElementTryParser<T>(Lexer lexer, out T parsedValue, out Token token);
}
