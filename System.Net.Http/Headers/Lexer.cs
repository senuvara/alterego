﻿using System;
using System.Globalization;

namespace System.Net.Http.Headers
{
	// Token: 0x02000041 RID: 65
	internal class Lexer
	{
		// Token: 0x0600025E RID: 606 RVA: 0x000099E8 File Offset: 0x00007BE8
		public Lexer(string stream)
		{
			this.s = stream;
		}

		// Token: 0x170000A4 RID: 164
		// (get) Token: 0x0600025F RID: 607 RVA: 0x000099F7 File Offset: 0x00007BF7
		// (set) Token: 0x06000260 RID: 608 RVA: 0x000099FF File Offset: 0x00007BFF
		public int Position
		{
			get
			{
				return this.pos;
			}
			set
			{
				this.pos = value;
			}
		}

		// Token: 0x06000261 RID: 609 RVA: 0x00009A08 File Offset: 0x00007C08
		public string GetStringValue(Token token)
		{
			return this.s.Substring(token.StartPosition, token.EndPosition - token.StartPosition);
		}

		// Token: 0x06000262 RID: 610 RVA: 0x00009A2B File Offset: 0x00007C2B
		public string GetStringValue(Token start, Token end)
		{
			return this.s.Substring(start.StartPosition, end.EndPosition - start.StartPosition);
		}

		// Token: 0x06000263 RID: 611 RVA: 0x00009A4E File Offset: 0x00007C4E
		public string GetQuotedStringValue(Token start)
		{
			return this.s.Substring(start.StartPosition + 1, start.EndPosition - start.StartPosition - 2);
		}

		// Token: 0x06000264 RID: 612 RVA: 0x00009A75 File Offset: 0x00007C75
		public string GetRemainingStringValue(int position)
		{
			if (position <= this.s.Length)
			{
				return this.s.Substring(position);
			}
			return null;
		}

		// Token: 0x06000265 RID: 613 RVA: 0x00009A93 File Offset: 0x00007C93
		public bool IsStarStringValue(Token token)
		{
			return token.EndPosition - token.StartPosition == 1 && this.s[token.StartPosition] == '*';
		}

		// Token: 0x06000266 RID: 614 RVA: 0x00009ABF File Offset: 0x00007CBF
		public bool TryGetNumericValue(Token token, out int value)
		{
			return int.TryParse(this.GetStringValue(token), NumberStyles.None, CultureInfo.InvariantCulture, out value);
		}

		// Token: 0x06000267 RID: 615 RVA: 0x00009AD4 File Offset: 0x00007CD4
		public bool TryGetNumericValue(Token token, out long value)
		{
			return long.TryParse(this.GetStringValue(token), NumberStyles.None, CultureInfo.InvariantCulture, out value);
		}

		// Token: 0x06000268 RID: 616 RVA: 0x00009AEC File Offset: 0x00007CEC
		public TimeSpan? TryGetTimeSpanValue(Token token)
		{
			int num;
			if (this.TryGetNumericValue(token, out num))
			{
				return new TimeSpan?(TimeSpan.FromSeconds((double)num));
			}
			return null;
		}

		// Token: 0x06000269 RID: 617 RVA: 0x00009B1A File Offset: 0x00007D1A
		public bool TryGetDateValue(Token token, out DateTimeOffset value)
		{
			return Lexer.TryGetDateValue((token == Token.Type.QuotedString) ? this.s.Substring(token.StartPosition + 1, token.EndPosition - token.StartPosition - 2) : this.GetStringValue(token), out value);
		}

		// Token: 0x0600026A RID: 618 RVA: 0x00009B59 File Offset: 0x00007D59
		public static bool TryGetDateValue(string text, out DateTimeOffset value)
		{
			return DateTimeOffset.TryParseExact(text, Lexer.dt_formats, DateTimeFormatInfo.InvariantInfo, DateTimeStyles.AllowLeadingWhite | DateTimeStyles.AllowTrailingWhite | DateTimeStyles.AllowInnerWhite | DateTimeStyles.AssumeUniversal, out value);
		}

		// Token: 0x0600026B RID: 619 RVA: 0x00009B6E File Offset: 0x00007D6E
		public bool TryGetDoubleValue(Token token, out double value)
		{
			return double.TryParse(this.GetStringValue(token), NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture, out value);
		}

		// Token: 0x0600026C RID: 620 RVA: 0x00009B84 File Offset: 0x00007D84
		public static bool IsValidToken(string input)
		{
			int i;
			for (i = 0; i < input.Length; i++)
			{
				if (!Lexer.IsValidCharacter(input[i]))
				{
					return false;
				}
			}
			return i > 0;
		}

		// Token: 0x0600026D RID: 621 RVA: 0x00009BB6 File Offset: 0x00007DB6
		public static bool IsValidCharacter(char input)
		{
			return (int)input < Lexer.last_token_char && Lexer.token_chars[(int)input];
		}

		// Token: 0x0600026E RID: 622 RVA: 0x00009BC9 File Offset: 0x00007DC9
		public void EatChar()
		{
			this.pos++;
		}

		// Token: 0x0600026F RID: 623 RVA: 0x00009BD9 File Offset: 0x00007DD9
		public int PeekChar()
		{
			if (this.pos >= this.s.Length)
			{
				return -1;
			}
			return (int)this.s[this.pos];
		}

		// Token: 0x06000270 RID: 624 RVA: 0x00009C04 File Offset: 0x00007E04
		public bool ScanCommentOptional(out string value)
		{
			Token token;
			return this.ScanCommentOptional(out value, out token) || token == Token.Type.End;
		}

		// Token: 0x06000271 RID: 625 RVA: 0x00009C28 File Offset: 0x00007E28
		public bool ScanCommentOptional(out string value, out Token readToken)
		{
			readToken = this.Scan(false);
			if (readToken != Token.Type.OpenParens)
			{
				value = null;
				return false;
			}
			int num = 1;
			while (this.pos < this.s.Length)
			{
				char c = this.s[this.pos];
				if (c == '(')
				{
					num++;
					this.pos++;
				}
				else if (c == ')')
				{
					this.pos++;
					if (--num <= 0)
					{
						int startPosition = readToken.StartPosition;
						value = this.s.Substring(startPosition, this.pos - startPosition);
						return true;
					}
				}
				else
				{
					if (c < ' ' || c > '~')
					{
						break;
					}
					this.pos++;
				}
			}
			value = null;
			return false;
		}

		// Token: 0x06000272 RID: 626 RVA: 0x00009CF4 File Offset: 0x00007EF4
		public Token Scan(bool recognizeDash = false)
		{
			int startPosition = this.pos;
			if (this.s == null)
			{
				return new Token(Token.Type.Error, 0, 0);
			}
			Token.Type type;
			if (this.pos >= this.s.Length)
			{
				type = Token.Type.End;
			}
			else
			{
				type = Token.Type.Error;
				char c;
				for (;;)
				{
					string text = this.s;
					int num = this.pos;
					this.pos = num + 1;
					c = text[num];
					if (c > '"')
					{
						goto IL_6D;
					}
					if (c != '\t' && c != ' ')
					{
						break;
					}
					if (this.pos == this.s.Length)
					{
						goto Block_12;
					}
				}
				if (c != '"')
				{
					goto IL_171;
				}
				startPosition = this.pos - 1;
				while (this.pos < this.s.Length)
				{
					string text2 = this.s;
					int num = this.pos;
					this.pos = num + 1;
					c = text2[num];
					if (c == '\\')
					{
						if (this.pos + 1 >= this.s.Length)
						{
							break;
						}
						this.pos++;
					}
					else if (c == '"')
					{
						type = Token.Type.QuotedString;
						break;
					}
				}
				goto IL_1D3;
				IL_6D:
				if (c <= '/')
				{
					if (c == '(')
					{
						startPosition = this.pos - 1;
						type = Token.Type.OpenParens;
						goto IL_1D3;
					}
					switch (c)
					{
					case ',':
						type = Token.Type.SeparatorComma;
						goto IL_1D3;
					case '-':
						if (recognizeDash)
						{
							type = Token.Type.SeparatorDash;
							goto IL_1D3;
						}
						goto IL_171;
					case '.':
						goto IL_171;
					case '/':
						type = Token.Type.SeparatorSlash;
						goto IL_1D3;
					default:
						goto IL_171;
					}
				}
				else
				{
					if (c == ';')
					{
						type = Token.Type.SeparatorSemicolon;
						goto IL_1D3;
					}
					if (c != '=')
					{
						goto IL_171;
					}
					type = Token.Type.SeparatorEqual;
					goto IL_1D3;
				}
				Block_12:
				type = Token.Type.End;
				goto IL_1D3;
				IL_171:
				if ((int)c < Lexer.last_token_char && Lexer.token_chars[(int)c])
				{
					startPosition = this.pos - 1;
					type = Token.Type.Token;
					while (this.pos < this.s.Length)
					{
						c = this.s[this.pos];
						if ((int)c >= Lexer.last_token_char || !Lexer.token_chars[(int)c])
						{
							break;
						}
						this.pos++;
					}
				}
			}
			IL_1D3:
			return new Token(type, startPosition, this.pos);
		}

		// Token: 0x06000273 RID: 627 RVA: 0x00009EE4 File Offset: 0x000080E4
		// Note: this type is marked as 'beforefieldinit'.
		static Lexer()
		{
		}

		// Token: 0x0400012D RID: 301
		private static readonly bool[] token_chars = new bool[]
		{
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			true,
			false,
			true,
			true,
			true,
			true,
			true,
			false,
			false,
			true,
			true,
			false,
			true,
			true,
			false,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			false,
			false,
			false,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			false,
			true,
			false,
			true
		};

		// Token: 0x0400012E RID: 302
		private static readonly int last_token_char = Lexer.token_chars.Length;

		// Token: 0x0400012F RID: 303
		private static readonly string[] dt_formats = new string[]
		{
			"r",
			"dddd, dd'-'MMM'-'yy HH:mm:ss 'GMT'",
			"ddd MMM d HH:mm:ss yyyy",
			"d MMM yy H:m:s",
			"ddd, d MMM yyyy H:m:s zzz"
		};

		// Token: 0x04000130 RID: 304
		private readonly string s;

		// Token: 0x04000131 RID: 305
		private int pos;
	}
}
