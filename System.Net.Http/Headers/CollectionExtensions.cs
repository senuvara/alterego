﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace System.Net.Http.Headers
{
	// Token: 0x02000029 RID: 41
	internal static class CollectionExtensions
	{
		// Token: 0x06000139 RID: 313 RVA: 0x00006A6E File Offset: 0x00004C6E
		public static bool SequenceEqual<TSource>(this List<TSource> first, List<TSource> second)
		{
			if (first == null)
			{
				return second == null || second.Count == 0;
			}
			if (second == null)
			{
				return first == null || first.Count == 0;
			}
			return first.SequenceEqual(second);
		}

		// Token: 0x0600013A RID: 314 RVA: 0x00006A9C File Offset: 0x00004C9C
		public static void SetValue(this List<NameValueHeaderValue> parameters, string key, string value)
		{
			int i = 0;
			while (i < parameters.Count)
			{
				if (string.Equals(parameters[i].Name, key, StringComparison.OrdinalIgnoreCase))
				{
					if (value == null)
					{
						parameters.RemoveAt(i);
						return;
					}
					parameters[i].Value = value;
					return;
				}
				else
				{
					i++;
				}
			}
			if (!string.IsNullOrEmpty(value))
			{
				parameters.Add(new NameValueHeaderValue(key, value));
			}
		}

		// Token: 0x0600013B RID: 315 RVA: 0x00006B00 File Offset: 0x00004D00
		public static string ToString<T>(this List<T> list)
		{
			if (list == null || list.Count == 0)
			{
				return null;
			}
			StringBuilder stringBuilder = new StringBuilder();
			for (int i = 0; i < list.Count; i++)
			{
				stringBuilder.Append("; ");
				stringBuilder.Append(list[i]);
			}
			return stringBuilder.ToString();
		}

		// Token: 0x0600013C RID: 316 RVA: 0x00006B58 File Offset: 0x00004D58
		public static void ToStringBuilder<T>(this List<T> list, StringBuilder sb)
		{
			if (list == null || list.Count == 0)
			{
				return;
			}
			for (int i = 0; i < list.Count; i++)
			{
				if (i > 0)
				{
					sb.Append(", ");
				}
				sb.Append(list[i]);
			}
		}
	}
}
