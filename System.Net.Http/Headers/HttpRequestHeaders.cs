﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace System.Net.Http.Headers
{
	/// <summary>Represents the collection of Request Headers as defined in RFC 2616.</summary>
	// Token: 0x0200003B RID: 59
	public sealed class HttpRequestHeaders : HttpHeaders
	{
		// Token: 0x060001F9 RID: 505 RVA: 0x00009043 File Offset: 0x00007243
		internal HttpRequestHeaders() : base(HttpHeaderKind.Request)
		{
		}

		/// <summary>Gets the value of the <see langword="Accept" /> header for an HTTP request.</summary>
		/// <returns>The value of the <see langword="Accept" /> header for an HTTP request.</returns>
		// Token: 0x1700006D RID: 109
		// (get) Token: 0x060001FA RID: 506 RVA: 0x0000904C File Offset: 0x0000724C
		public HttpHeaderValueCollection<MediaTypeWithQualityHeaderValue> Accept
		{
			get
			{
				return base.GetValues<MediaTypeWithQualityHeaderValue>("Accept");
			}
		}

		/// <summary>Gets the value of the <see langword="Accept-Charset" /> header for an HTTP request.</summary>
		/// <returns>The value of the <see langword="Accept-Charset" /> header for an HTTP request.</returns>
		// Token: 0x1700006E RID: 110
		// (get) Token: 0x060001FB RID: 507 RVA: 0x00009059 File Offset: 0x00007259
		public HttpHeaderValueCollection<StringWithQualityHeaderValue> AcceptCharset
		{
			get
			{
				return base.GetValues<StringWithQualityHeaderValue>("Accept-Charset");
			}
		}

		/// <summary>Gets the value of the <see langword="Accept-Encoding" /> header for an HTTP request.</summary>
		/// <returns>The value of the <see langword="Accept-Encoding" /> header for an HTTP request.</returns>
		// Token: 0x1700006F RID: 111
		// (get) Token: 0x060001FC RID: 508 RVA: 0x00009066 File Offset: 0x00007266
		public HttpHeaderValueCollection<StringWithQualityHeaderValue> AcceptEncoding
		{
			get
			{
				return base.GetValues<StringWithQualityHeaderValue>("Accept-Encoding");
			}
		}

		/// <summary>Gets the value of the <see langword="Accept-Language" /> header for an HTTP request.</summary>
		/// <returns>The value of the <see langword="Accept-Language" /> header for an HTTP request.</returns>
		// Token: 0x17000070 RID: 112
		// (get) Token: 0x060001FD RID: 509 RVA: 0x00009073 File Offset: 0x00007273
		public HttpHeaderValueCollection<StringWithQualityHeaderValue> AcceptLanguage
		{
			get
			{
				return base.GetValues<StringWithQualityHeaderValue>("Accept-Language");
			}
		}

		/// <summary>Gets or sets the value of the <see langword="Authorization" /> header for an HTTP request.</summary>
		/// <returns>The value of the <see langword="Authorization" /> header for an HTTP request.</returns>
		// Token: 0x17000071 RID: 113
		// (get) Token: 0x060001FE RID: 510 RVA: 0x00009080 File Offset: 0x00007280
		// (set) Token: 0x060001FF RID: 511 RVA: 0x0000908D File Offset: 0x0000728D
		public AuthenticationHeaderValue Authorization
		{
			get
			{
				return base.GetValue<AuthenticationHeaderValue>("Authorization");
			}
			set
			{
				base.AddOrRemove<AuthenticationHeaderValue>("Authorization", value, null);
			}
		}

		/// <summary>Gets or sets the value of the <see langword="Cache-Control" /> header for an HTTP request.</summary>
		/// <returns>The value of the <see langword="Cache-Control" /> header for an HTTP request.</returns>
		// Token: 0x17000072 RID: 114
		// (get) Token: 0x06000200 RID: 512 RVA: 0x0000909C File Offset: 0x0000729C
		// (set) Token: 0x06000201 RID: 513 RVA: 0x000090A9 File Offset: 0x000072A9
		public CacheControlHeaderValue CacheControl
		{
			get
			{
				return base.GetValue<CacheControlHeaderValue>("Cache-Control");
			}
			set
			{
				base.AddOrRemove<CacheControlHeaderValue>("Cache-Control", value, null);
			}
		}

		/// <summary>Gets the value of the <see langword="Connection" /> header for an HTTP request.</summary>
		/// <returns>The value of the <see langword="Connection" /> header for an HTTP request.</returns>
		// Token: 0x17000073 RID: 115
		// (get) Token: 0x06000202 RID: 514 RVA: 0x000090B8 File Offset: 0x000072B8
		public HttpHeaderValueCollection<string> Connection
		{
			get
			{
				return base.GetValues<string>("Connection");
			}
		}

		/// <summary>Gets or sets a value that indicates if the <see langword="Connection" /> header for an HTTP request contains Close.</summary>
		/// <returns>
		///     <see langword="true" /> if the <see langword="Connection" /> header contains Close, otherwise <see langword="false" />.</returns>
		// Token: 0x17000074 RID: 116
		// (get) Token: 0x06000203 RID: 515 RVA: 0x000090C8 File Offset: 0x000072C8
		// (set) Token: 0x06000204 RID: 516 RVA: 0x00009130 File Offset: 0x00007330
		public bool? ConnectionClose
		{
			get
			{
				if (!(this.connectionclose == true))
				{
					if (this.Connection.Find((string l) => string.Equals(l, "close", StringComparison.OrdinalIgnoreCase)) == null)
					{
						return this.connectionclose;
					}
				}
				return new bool?(true);
			}
			set
			{
				if (this.connectionclose == value)
				{
					return;
				}
				this.Connection.Remove("close");
				if (value == true)
				{
					this.Connection.Add("close");
				}
				this.connectionclose = value;
			}
		}

		// Token: 0x17000075 RID: 117
		// (get) Token: 0x06000205 RID: 517 RVA: 0x000091AE File Offset: 0x000073AE
		internal bool ConnectionKeepAlive
		{
			get
			{
				return this.Connection.Find((string l) => string.Equals(l, "Keep-Alive", StringComparison.OrdinalIgnoreCase)) != null;
			}
		}

		/// <summary>Gets or sets the value of the <see langword="Date" /> header for an HTTP request.</summary>
		/// <returns>The value of the <see langword="Date" /> header for an HTTP request.</returns>
		// Token: 0x17000076 RID: 118
		// (get) Token: 0x06000206 RID: 518 RVA: 0x000091DD File Offset: 0x000073DD
		// (set) Token: 0x06000207 RID: 519 RVA: 0x000091EA File Offset: 0x000073EA
		public DateTimeOffset? Date
		{
			get
			{
				return base.GetValue<DateTimeOffset?>("Date");
			}
			set
			{
				base.AddOrRemove<DateTimeOffset>("Date", value, Parser.DateTime.ToString);
			}
		}

		/// <summary>Gets the value of the <see langword="Expect" /> header for an HTTP request.</summary>
		/// <returns>The value of the <see langword="Expect" /> header for an HTTP request.</returns>
		// Token: 0x17000077 RID: 119
		// (get) Token: 0x06000208 RID: 520 RVA: 0x000091FD File Offset: 0x000073FD
		public HttpHeaderValueCollection<NameValueWithParametersHeaderValue> Expect
		{
			get
			{
				return base.GetValues<NameValueWithParametersHeaderValue>("Expect");
			}
		}

		/// <summary>Gets or sets a value that indicates if the <see langword="Expect" /> header for an HTTP request contains Continue.</summary>
		/// <returns>
		///     <see langword="true" /> if the <see langword="Expect" /> header contains Continue, otherwise <see langword="false" />.</returns>
		// Token: 0x17000078 RID: 120
		// (get) Token: 0x06000209 RID: 521 RVA: 0x0000920C File Offset: 0x0000740C
		// (set) Token: 0x0600020A RID: 522 RVA: 0x0000926C File Offset: 0x0000746C
		public bool? ExpectContinue
		{
			get
			{
				if (this.expectContinue != null)
				{
					return this.expectContinue;
				}
				if (this.TransferEncoding.Find((TransferCodingHeaderValue l) => string.Equals(l.Value, "100-continue", StringComparison.OrdinalIgnoreCase)) == null)
				{
					return null;
				}
				return new bool?(true);
			}
			set
			{
				if (this.expectContinue == value)
				{
					return;
				}
				this.Expect.Remove((NameValueWithParametersHeaderValue l) => l.Name == "100-continue");
				if (value == true)
				{
					this.Expect.Add(new NameValueWithParametersHeaderValue("100-continue"));
				}
				this.expectContinue = value;
			}
		}

		/// <summary>Gets or sets the value of the <see langword="From" /> header for an HTTP request.</summary>
		/// <returns>The value of the <see langword="From" /> header for an HTTP request.</returns>
		// Token: 0x17000079 RID: 121
		// (get) Token: 0x0600020B RID: 523 RVA: 0x00009308 File Offset: 0x00007508
		// (set) Token: 0x0600020C RID: 524 RVA: 0x00009315 File Offset: 0x00007515
		public string From
		{
			get
			{
				return base.GetValue<string>("From");
			}
			set
			{
				if (!string.IsNullOrEmpty(value) && !Parser.EmailAddress.TryParse(value, out value))
				{
					throw new FormatException();
				}
				base.AddOrRemove("From", value);
			}
		}

		/// <summary>Gets or sets the value of the <see langword="Host" /> header for an HTTP request.</summary>
		/// <returns>The value of the <see langword="Host" /> header for an HTTP request.</returns>
		// Token: 0x1700007A RID: 122
		// (get) Token: 0x0600020D RID: 525 RVA: 0x0000933B File Offset: 0x0000753B
		// (set) Token: 0x0600020E RID: 526 RVA: 0x00009348 File Offset: 0x00007548
		public string Host
		{
			get
			{
				return base.GetValue<string>("Host");
			}
			set
			{
				base.AddOrRemove("Host", value);
			}
		}

		/// <summary>Gets the value of the <see langword="If-Match" /> header for an HTTP request.</summary>
		/// <returns>Returns <see cref="T:System.Net.Http.Headers.HttpHeaderValueCollection`1" />.The value of the <see langword="If-Match" /> header for an HTTP request.</returns>
		// Token: 0x1700007B RID: 123
		// (get) Token: 0x0600020F RID: 527 RVA: 0x00009356 File Offset: 0x00007556
		public HttpHeaderValueCollection<EntityTagHeaderValue> IfMatch
		{
			get
			{
				return base.GetValues<EntityTagHeaderValue>("If-Match");
			}
		}

		/// <summary>Gets or sets the value of the <see langword="If-Modified-Since" /> header for an HTTP request.</summary>
		/// <returns>The value of the <see langword="If-Modified-Since" /> header for an HTTP request.</returns>
		// Token: 0x1700007C RID: 124
		// (get) Token: 0x06000210 RID: 528 RVA: 0x00009363 File Offset: 0x00007563
		// (set) Token: 0x06000211 RID: 529 RVA: 0x00009370 File Offset: 0x00007570
		public DateTimeOffset? IfModifiedSince
		{
			get
			{
				return base.GetValue<DateTimeOffset?>("If-Modified-Since");
			}
			set
			{
				base.AddOrRemove<DateTimeOffset>("If-Modified-Since", value, Parser.DateTime.ToString);
			}
		}

		/// <summary>Gets the value of the <see langword="If-None-Match" /> header for an HTTP request.</summary>
		/// <returns>Gets the value of the <see langword="If-None-Match" /> header for an HTTP request.</returns>
		// Token: 0x1700007D RID: 125
		// (get) Token: 0x06000212 RID: 530 RVA: 0x00009383 File Offset: 0x00007583
		public HttpHeaderValueCollection<EntityTagHeaderValue> IfNoneMatch
		{
			get
			{
				return base.GetValues<EntityTagHeaderValue>("If-None-Match");
			}
		}

		/// <summary>Gets or sets the value of the <see langword="If-Range" /> header for an HTTP request.</summary>
		/// <returns>The value of the <see langword="If-Range" /> header for an HTTP request.</returns>
		// Token: 0x1700007E RID: 126
		// (get) Token: 0x06000213 RID: 531 RVA: 0x00009390 File Offset: 0x00007590
		// (set) Token: 0x06000214 RID: 532 RVA: 0x0000939D File Offset: 0x0000759D
		public RangeConditionHeaderValue IfRange
		{
			get
			{
				return base.GetValue<RangeConditionHeaderValue>("If-Range");
			}
			set
			{
				base.AddOrRemove<RangeConditionHeaderValue>("If-Range", value, null);
			}
		}

		/// <summary>Gets or sets the value of the <see langword="If-Unmodified-Since" /> header for an HTTP request.</summary>
		/// <returns>The value of the <see langword="If-Unmodified-Since" /> header for an HTTP request.</returns>
		// Token: 0x1700007F RID: 127
		// (get) Token: 0x06000215 RID: 533 RVA: 0x000093AC File Offset: 0x000075AC
		// (set) Token: 0x06000216 RID: 534 RVA: 0x000093B9 File Offset: 0x000075B9
		public DateTimeOffset? IfUnmodifiedSince
		{
			get
			{
				return base.GetValue<DateTimeOffset?>("If-Unmodified-Since");
			}
			set
			{
				base.AddOrRemove<DateTimeOffset>("If-Unmodified-Since", value, Parser.DateTime.ToString);
			}
		}

		/// <summary>Gets or sets the value of the <see langword="Max-Forwards" /> header for an HTTP request.</summary>
		/// <returns>The value of the <see langword="Max-Forwards" /> header for an HTTP request.</returns>
		// Token: 0x17000080 RID: 128
		// (get) Token: 0x06000217 RID: 535 RVA: 0x000093CC File Offset: 0x000075CC
		// (set) Token: 0x06000218 RID: 536 RVA: 0x000093D9 File Offset: 0x000075D9
		public int? MaxForwards
		{
			get
			{
				return base.GetValue<int?>("Max-Forwards");
			}
			set
			{
				base.AddOrRemove<int>("Max-Forwards", value);
			}
		}

		/// <summary>Gets the value of the <see langword="Pragma" /> header for an HTTP request.</summary>
		/// <returns>The value of the <see langword="Pragma" /> header for an HTTP request.</returns>
		// Token: 0x17000081 RID: 129
		// (get) Token: 0x06000219 RID: 537 RVA: 0x000093E7 File Offset: 0x000075E7
		public HttpHeaderValueCollection<NameValueHeaderValue> Pragma
		{
			get
			{
				return base.GetValues<NameValueHeaderValue>("Pragma");
			}
		}

		/// <summary>Gets or sets the value of the <see langword="Proxy-Authorization" /> header for an HTTP request.</summary>
		/// <returns>The value of the <see langword="Proxy-Authorization" /> header for an HTTP request.</returns>
		// Token: 0x17000082 RID: 130
		// (get) Token: 0x0600021A RID: 538 RVA: 0x000093F4 File Offset: 0x000075F4
		// (set) Token: 0x0600021B RID: 539 RVA: 0x00009401 File Offset: 0x00007601
		public AuthenticationHeaderValue ProxyAuthorization
		{
			get
			{
				return base.GetValue<AuthenticationHeaderValue>("Proxy-Authorization");
			}
			set
			{
				base.AddOrRemove<AuthenticationHeaderValue>("Proxy-Authorization", value, null);
			}
		}

		/// <summary>Gets or sets the value of the <see langword="Range" /> header for an HTTP request.</summary>
		/// <returns>The value of the <see langword="Range" /> header for an HTTP request.</returns>
		// Token: 0x17000083 RID: 131
		// (get) Token: 0x0600021C RID: 540 RVA: 0x00009410 File Offset: 0x00007610
		// (set) Token: 0x0600021D RID: 541 RVA: 0x0000941D File Offset: 0x0000761D
		public RangeHeaderValue Range
		{
			get
			{
				return base.GetValue<RangeHeaderValue>("Range");
			}
			set
			{
				base.AddOrRemove<RangeHeaderValue>("Range", value, null);
			}
		}

		/// <summary>Gets or sets the value of the <see langword="Referer" /> header for an HTTP request.</summary>
		/// <returns>The value of the <see langword="Referer" /> header for an HTTP request.</returns>
		// Token: 0x17000084 RID: 132
		// (get) Token: 0x0600021E RID: 542 RVA: 0x0000942C File Offset: 0x0000762C
		// (set) Token: 0x0600021F RID: 543 RVA: 0x00009439 File Offset: 0x00007639
		public Uri Referrer
		{
			get
			{
				return base.GetValue<Uri>("Referer");
			}
			set
			{
				base.AddOrRemove<Uri>("Referer", value, null);
			}
		}

		/// <summary>Gets the value of the <see langword="TE" /> header for an HTTP request.</summary>
		/// <returns>The value of the <see langword="TE" /> header for an HTTP request.</returns>
		// Token: 0x17000085 RID: 133
		// (get) Token: 0x06000220 RID: 544 RVA: 0x00009448 File Offset: 0x00007648
		public HttpHeaderValueCollection<TransferCodingWithQualityHeaderValue> TE
		{
			get
			{
				return base.GetValues<TransferCodingWithQualityHeaderValue>("TE");
			}
		}

		/// <summary>Gets the value of the <see langword="Trailer" /> header for an HTTP request.</summary>
		/// <returns>The value of the <see langword="Trailer" /> header for an HTTP request.</returns>
		// Token: 0x17000086 RID: 134
		// (get) Token: 0x06000221 RID: 545 RVA: 0x00009455 File Offset: 0x00007655
		public HttpHeaderValueCollection<string> Trailer
		{
			get
			{
				return base.GetValues<string>("Trailer");
			}
		}

		/// <summary>Gets the value of the <see langword="Transfer-Encoding" /> header for an HTTP request.</summary>
		/// <returns>The value of the <see langword="Transfer-Encoding" /> header for an HTTP request.</returns>
		// Token: 0x17000087 RID: 135
		// (get) Token: 0x06000222 RID: 546 RVA: 0x00009462 File Offset: 0x00007662
		public HttpHeaderValueCollection<TransferCodingHeaderValue> TransferEncoding
		{
			get
			{
				return base.GetValues<TransferCodingHeaderValue>("Transfer-Encoding");
			}
		}

		/// <summary>Gets or sets a value that indicates if the <see langword="Transfer-Encoding" /> header for an HTTP request contains chunked.</summary>
		/// <returns>
		///     <see langword="true" /> if the <see langword="Transfer-Encoding" /> header contains chunked, otherwise <see langword="false" />.</returns>
		// Token: 0x17000088 RID: 136
		// (get) Token: 0x06000223 RID: 547 RVA: 0x00009470 File Offset: 0x00007670
		// (set) Token: 0x06000224 RID: 548 RVA: 0x000094D0 File Offset: 0x000076D0
		public bool? TransferEncodingChunked
		{
			get
			{
				if (this.transferEncodingChunked != null)
				{
					return this.transferEncodingChunked;
				}
				if (this.TransferEncoding.Find((TransferCodingHeaderValue l) => string.Equals(l.Value, "chunked", StringComparison.OrdinalIgnoreCase)) == null)
				{
					return null;
				}
				return new bool?(true);
			}
			set
			{
				if (value == this.transferEncodingChunked)
				{
					return;
				}
				this.TransferEncoding.Remove((TransferCodingHeaderValue l) => l.Value == "chunked");
				if (value == true)
				{
					this.TransferEncoding.Add(new TransferCodingHeaderValue("chunked"));
				}
				this.transferEncodingChunked = value;
			}
		}

		/// <summary>Gets the value of the <see langword="Upgrade" /> header for an HTTP request.</summary>
		/// <returns>The value of the <see langword="Upgrade" /> header for an HTTP request.</returns>
		// Token: 0x17000089 RID: 137
		// (get) Token: 0x06000225 RID: 549 RVA: 0x0000956C File Offset: 0x0000776C
		public HttpHeaderValueCollection<ProductHeaderValue> Upgrade
		{
			get
			{
				return base.GetValues<ProductHeaderValue>("Upgrade");
			}
		}

		/// <summary>Gets the value of the <see langword="User-Agent" /> header for an HTTP request.</summary>
		/// <returns>The value of the <see langword="User-Agent" /> header for an HTTP request.</returns>
		// Token: 0x1700008A RID: 138
		// (get) Token: 0x06000226 RID: 550 RVA: 0x00009579 File Offset: 0x00007779
		public HttpHeaderValueCollection<ProductInfoHeaderValue> UserAgent
		{
			get
			{
				return base.GetValues<ProductInfoHeaderValue>("User-Agent");
			}
		}

		/// <summary>Gets the value of the <see langword="Via" /> header for an HTTP request.</summary>
		/// <returns>The value of the <see langword="Via" /> header for an HTTP request.</returns>
		// Token: 0x1700008B RID: 139
		// (get) Token: 0x06000227 RID: 551 RVA: 0x00009586 File Offset: 0x00007786
		public HttpHeaderValueCollection<ViaHeaderValue> Via
		{
			get
			{
				return base.GetValues<ViaHeaderValue>("Via");
			}
		}

		/// <summary>Gets the value of the <see langword="Warning" /> header for an HTTP request.</summary>
		/// <returns>The value of the <see langword="Warning" /> header for an HTTP request.</returns>
		// Token: 0x1700008C RID: 140
		// (get) Token: 0x06000228 RID: 552 RVA: 0x00009593 File Offset: 0x00007793
		public HttpHeaderValueCollection<WarningHeaderValue> Warning
		{
			get
			{
				return base.GetValues<WarningHeaderValue>("Warning");
			}
		}

		// Token: 0x06000229 RID: 553 RVA: 0x000095A0 File Offset: 0x000077A0
		internal void AddHeaders(HttpRequestHeaders headers)
		{
			foreach (KeyValuePair<string, IEnumerable<string>> keyValuePair in headers)
			{
				base.TryAddWithoutValidation(keyValuePair.Key, keyValuePair.Value);
			}
		}

		// Token: 0x04000111 RID: 273
		private bool? expectContinue;

		// Token: 0x0200003C RID: 60
		[CompilerGenerated]
		[Serializable]
		private sealed class <>c
		{
			// Token: 0x0600022A RID: 554 RVA: 0x000095F8 File Offset: 0x000077F8
			// Note: this type is marked as 'beforefieldinit'.
			static <>c()
			{
			}

			// Token: 0x0600022B RID: 555 RVA: 0x000039D8 File Offset: 0x00001BD8
			public <>c()
			{
			}

			// Token: 0x0600022C RID: 556 RVA: 0x00009604 File Offset: 0x00007804
			internal bool <get_ConnectionClose>b__19_0(string l)
			{
				return string.Equals(l, "close", StringComparison.OrdinalIgnoreCase);
			}

			// Token: 0x0600022D RID: 557 RVA: 0x00009612 File Offset: 0x00007812
			internal bool <get_ConnectionKeepAlive>b__22_0(string l)
			{
				return string.Equals(l, "Keep-Alive", StringComparison.OrdinalIgnoreCase);
			}

			// Token: 0x0600022E RID: 558 RVA: 0x00009620 File Offset: 0x00007820
			internal bool <get_ExpectContinue>b__29_0(TransferCodingHeaderValue l)
			{
				return string.Equals(l.Value, "100-continue", StringComparison.OrdinalIgnoreCase);
			}

			// Token: 0x0600022F RID: 559 RVA: 0x00009633 File Offset: 0x00007833
			internal bool <set_ExpectContinue>b__30_0(NameValueWithParametersHeaderValue l)
			{
				return l.Name == "100-continue";
			}

			// Token: 0x06000230 RID: 560 RVA: 0x00009645 File Offset: 0x00007845
			internal bool <get_TransferEncodingChunked>b__71_0(TransferCodingHeaderValue l)
			{
				return string.Equals(l.Value, "chunked", StringComparison.OrdinalIgnoreCase);
			}

			// Token: 0x06000231 RID: 561 RVA: 0x00009658 File Offset: 0x00007858
			internal bool <set_TransferEncodingChunked>b__72_0(TransferCodingHeaderValue l)
			{
				return l.Value == "chunked";
			}

			// Token: 0x04000112 RID: 274
			public static readonly HttpRequestHeaders.<>c <>9 = new HttpRequestHeaders.<>c();

			// Token: 0x04000113 RID: 275
			public static Predicate<string> <>9__19_0;

			// Token: 0x04000114 RID: 276
			public static Predicate<string> <>9__22_0;

			// Token: 0x04000115 RID: 277
			public static Predicate<TransferCodingHeaderValue> <>9__29_0;

			// Token: 0x04000116 RID: 278
			public static Predicate<NameValueWithParametersHeaderValue> <>9__30_0;

			// Token: 0x04000117 RID: 279
			public static Predicate<TransferCodingHeaderValue> <>9__71_0;

			// Token: 0x04000118 RID: 280
			public static Predicate<TransferCodingHeaderValue> <>9__72_0;
		}
	}
}
