﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.CompilerServices;

namespace System.Net.Http.Headers
{
	/// <summary>Represents a warning value used by the Warning header.</summary>
	// Token: 0x0200005F RID: 95
	public class WarningHeaderValue : ICloneable
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Net.Http.Headers.WarningHeaderValue" /> class.</summary>
		/// <param name="code">The specific warning code.</param>
		/// <param name="agent">The host that attached the warning.</param>
		/// <param name="text">A quoted-string containing the warning text.</param>
		// Token: 0x0600034F RID: 847 RVA: 0x0000C5CF File Offset: 0x0000A7CF
		public WarningHeaderValue(int code, string agent, string text)
		{
			if (!WarningHeaderValue.IsCodeValid(code))
			{
				throw new ArgumentOutOfRangeException("code");
			}
			Parser.Uri.Check(agent);
			Parser.Token.CheckQuotedString(text);
			this.Code = code;
			this.Agent = agent;
			this.Text = text;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Net.Http.Headers.WarningHeaderValue" /> class.</summary>
		/// <param name="code">The specific warning code.</param>
		/// <param name="agent">The host that attached the warning.</param>
		/// <param name="text">A quoted-string containing the warning text.</param>
		/// <param name="date">The date/time stamp of the warning.</param>
		// Token: 0x06000350 RID: 848 RVA: 0x0000C60B File Offset: 0x0000A80B
		public WarningHeaderValue(int code, string agent, string text, DateTimeOffset date) : this(code, agent, text)
		{
			this.Date = new DateTimeOffset?(date);
		}

		// Token: 0x06000351 RID: 849 RVA: 0x000039D8 File Offset: 0x00001BD8
		private WarningHeaderValue()
		{
		}

		/// <summary>Gets the host that attached the warning.</summary>
		/// <returns>The host that attached the warning.</returns>
		// Token: 0x170000C1 RID: 193
		// (get) Token: 0x06000352 RID: 850 RVA: 0x0000C623 File Offset: 0x0000A823
		// (set) Token: 0x06000353 RID: 851 RVA: 0x0000C62B File Offset: 0x0000A82B
		public string Agent
		{
			[CompilerGenerated]
			get
			{
				return this.<Agent>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<Agent>k__BackingField = value;
			}
		}

		/// <summary>Gets the specific warning code.</summary>
		/// <returns>The specific warning code.</returns>
		// Token: 0x170000C2 RID: 194
		// (get) Token: 0x06000354 RID: 852 RVA: 0x0000C634 File Offset: 0x0000A834
		// (set) Token: 0x06000355 RID: 853 RVA: 0x0000C63C File Offset: 0x0000A83C
		public int Code
		{
			[CompilerGenerated]
			get
			{
				return this.<Code>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<Code>k__BackingField = value;
			}
		}

		/// <summary>Gets the date/time stamp of the warning.</summary>
		/// <returns>The date/time stamp of the warning.</returns>
		// Token: 0x170000C3 RID: 195
		// (get) Token: 0x06000356 RID: 854 RVA: 0x0000C645 File Offset: 0x0000A845
		// (set) Token: 0x06000357 RID: 855 RVA: 0x0000C64D File Offset: 0x0000A84D
		public DateTimeOffset? Date
		{
			[CompilerGenerated]
			get
			{
				return this.<Date>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<Date>k__BackingField = value;
			}
		}

		/// <summary>Gets a quoted-string containing the warning text.</summary>
		/// <returns>A quoted-string containing the warning text.</returns>
		// Token: 0x170000C4 RID: 196
		// (get) Token: 0x06000358 RID: 856 RVA: 0x0000C656 File Offset: 0x0000A856
		// (set) Token: 0x06000359 RID: 857 RVA: 0x0000C65E File Offset: 0x0000A85E
		public string Text
		{
			[CompilerGenerated]
			get
			{
				return this.<Text>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<Text>k__BackingField = value;
			}
		}

		// Token: 0x0600035A RID: 858 RVA: 0x0000C667 File Offset: 0x0000A867
		private static bool IsCodeValid(int code)
		{
			return code >= 0 && code < 1000;
		}

		/// <summary>Creates a new object that is a copy of the current <see cref="T:System.Net.Http.Headers.WarningHeaderValue" /> instance.</summary>
		/// <returns>Returns a copy of the current instance.</returns>
		// Token: 0x0600035B RID: 859 RVA: 0x00005BA5 File Offset: 0x00003DA5
		object ICloneable.Clone()
		{
			return base.MemberwiseClone();
		}

		/// <summary>Determines whether the specified <see cref="T:System.Object" /> is equal to the current <see cref="T:System.Net.Http.Headers.WarningHeaderValue" /> object.</summary>
		/// <param name="obj">The object to compare with the current object.</param>
		/// <returns>
		///     <see langword="true" /> if the specified <see cref="T:System.Object" /> is equal to the current object; otherwise, <see langword="false" />.</returns>
		// Token: 0x0600035C RID: 860 RVA: 0x0000C678 File Offset: 0x0000A878
		public override bool Equals(object obj)
		{
			WarningHeaderValue warningHeaderValue = obj as WarningHeaderValue;
			return warningHeaderValue != null && (this.Code == warningHeaderValue.Code && string.Equals(warningHeaderValue.Agent, this.Agent, StringComparison.OrdinalIgnoreCase) && this.Text == warningHeaderValue.Text) && this.Date == warningHeaderValue.Date;
		}

		/// <summary>Serves as a hash function for an <see cref="T:System.Net.Http.Headers.WarningHeaderValue" /> object.</summary>
		/// <returns>A hash code for the current object.</returns>
		// Token: 0x0600035D RID: 861 RVA: 0x0000C708 File Offset: 0x0000A908
		public override int GetHashCode()
		{
			return this.Code.GetHashCode() ^ this.Agent.ToLowerInvariant().GetHashCode() ^ this.Text.GetHashCode() ^ this.Date.GetHashCode();
		}

		/// <summary>Converts a string to an <see cref="T:System.Net.Http.Headers.WarningHeaderValue" /> instance.</summary>
		/// <param name="input">A string that represents authentication header value information.</param>
		/// <returns>Returns a <see cref="T:System.Net.Http.Headers.WarningHeaderValue" /> instance.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="input" /> is a <see langword="null" /> reference.</exception>
		/// <exception cref="T:System.FormatException">
		///         <paramref name="input" /> is not valid authentication header value information.</exception>
		// Token: 0x0600035E RID: 862 RVA: 0x0000C758 File Offset: 0x0000A958
		public static WarningHeaderValue Parse(string input)
		{
			WarningHeaderValue result;
			if (WarningHeaderValue.TryParse(input, out result))
			{
				return result;
			}
			throw new FormatException(input);
		}

		/// <summary>Determines whether a string is valid <see cref="T:System.Net.Http.Headers.WarningHeaderValue" /> information.</summary>
		/// <param name="input">The string to validate.</param>
		/// <param name="parsedValue">The <see cref="T:System.Net.Http.Headers.WarningHeaderValue" /> version of the string.</param>
		/// <returns>
		///     <see langword="true" /> if <paramref name="input" /> is valid <see cref="T:System.Net.Http.Headers.WarningHeaderValue" /> information; otherwise, <see langword="false" />.</returns>
		// Token: 0x0600035F RID: 863 RVA: 0x0000C778 File Offset: 0x0000A978
		public static bool TryParse(string input, out WarningHeaderValue parsedValue)
		{
			Token token;
			if (WarningHeaderValue.TryParseElement(new Lexer(input), out parsedValue, out token) && token == Token.Type.End)
			{
				return true;
			}
			parsedValue = null;
			return false;
		}

		// Token: 0x06000360 RID: 864 RVA: 0x0000C7A4 File Offset: 0x0000A9A4
		internal static bool TryParse(string input, int minimalCount, out List<WarningHeaderValue> result)
		{
			return CollectionParser.TryParse<WarningHeaderValue>(input, minimalCount, new ElementTryParser<WarningHeaderValue>(WarningHeaderValue.TryParseElement), out result);
		}

		// Token: 0x06000361 RID: 865 RVA: 0x0000C7BC File Offset: 0x0000A9BC
		private static bool TryParseElement(Lexer lexer, out WarningHeaderValue parsedValue, out Token t)
		{
			parsedValue = null;
			t = lexer.Scan(false);
			if (t != Token.Type.Token)
			{
				return false;
			}
			int code;
			if (!lexer.TryGetNumericValue(t, out code) || !WarningHeaderValue.IsCodeValid(code))
			{
				return false;
			}
			t = lexer.Scan(false);
			if (t != Token.Type.Token)
			{
				return false;
			}
			Token token = t;
			if (lexer.PeekChar() == 58)
			{
				lexer.EatChar();
				token = lexer.Scan(false);
				if (token != Token.Type.Token)
				{
					return false;
				}
			}
			WarningHeaderValue warningHeaderValue = new WarningHeaderValue();
			warningHeaderValue.Code = code;
			warningHeaderValue.Agent = lexer.GetStringValue(t, token);
			t = lexer.Scan(false);
			if (t != Token.Type.QuotedString)
			{
				return false;
			}
			warningHeaderValue.Text = lexer.GetStringValue(t);
			t = lexer.Scan(false);
			if (t == Token.Type.QuotedString)
			{
				DateTimeOffset value;
				if (!lexer.TryGetDateValue(t, out value))
				{
					return false;
				}
				warningHeaderValue.Date = new DateTimeOffset?(value);
				t = lexer.Scan(false);
			}
			parsedValue = warningHeaderValue;
			return true;
		}

		/// <summary>Returns a string that represents the current <see cref="T:System.Net.Http.Headers.WarningHeaderValue" /> object.</summary>
		/// <returns>A string that represents the current object.</returns>
		// Token: 0x06000362 RID: 866 RVA: 0x0000C8E4 File Offset: 0x0000AAE4
		public override string ToString()
		{
			string text = string.Concat(new string[]
			{
				this.Code.ToString("000"),
				" ",
				this.Agent,
				" ",
				this.Text
			});
			if (this.Date != null)
			{
				text = text + " \"" + this.Date.Value.ToString("r", CultureInfo.InvariantCulture) + "\"";
			}
			return text;
		}

		// Token: 0x04000153 RID: 339
		[CompilerGenerated]
		private string <Agent>k__BackingField;

		// Token: 0x04000154 RID: 340
		[CompilerGenerated]
		private int <Code>k__BackingField;

		// Token: 0x04000155 RID: 341
		[CompilerGenerated]
		private DateTimeOffset? <Date>k__BackingField;

		// Token: 0x04000156 RID: 342
		[CompilerGenerated]
		private string <Text>k__BackingField;
	}
}
