﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.CompilerServices;

namespace System.Net.Http.Headers
{
	/// <summary>Represents a string header value with an optional quality.</summary>
	// Token: 0x0200005B RID: 91
	public class StringWithQualityHeaderValue : ICloneable
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Net.Http.Headers.StringWithQualityHeaderValue" /> class.</summary>
		/// <param name="value">The string used to initialize the new instance.</param>
		// Token: 0x06000316 RID: 790 RVA: 0x0000BC2A File Offset: 0x00009E2A
		public StringWithQualityHeaderValue(string value)
		{
			Parser.Token.Check(value);
			this.Value = value;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Net.Http.Headers.StringWithQualityHeaderValue" /> class.</summary>
		/// <param name="value">A string used to initialize the new instance.</param>
		/// <param name="quality">A quality factor used to initialize the new instance.</param>
		// Token: 0x06000317 RID: 791 RVA: 0x0000BC3F File Offset: 0x00009E3F
		public StringWithQualityHeaderValue(string value, double quality) : this(value)
		{
			if (quality < 0.0 || quality > 1.0)
			{
				throw new ArgumentOutOfRangeException("quality");
			}
			this.Quality = new double?(quality);
		}

		// Token: 0x06000318 RID: 792 RVA: 0x000039D8 File Offset: 0x00001BD8
		private StringWithQualityHeaderValue()
		{
		}

		/// <summary>Gets the quality factor from the <see cref="T:System.Net.Http.Headers.StringWithQualityHeaderValue" /> object.</summary>
		/// <returns>The quality factor from the <see cref="T:System.Net.Http.Headers.StringWithQualityHeaderValue" /> object.</returns>
		// Token: 0x170000B8 RID: 184
		// (get) Token: 0x06000319 RID: 793 RVA: 0x0000BC77 File Offset: 0x00009E77
		// (set) Token: 0x0600031A RID: 794 RVA: 0x0000BC7F File Offset: 0x00009E7F
		public double? Quality
		{
			[CompilerGenerated]
			get
			{
				return this.<Quality>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<Quality>k__BackingField = value;
			}
		}

		/// <summary>Gets the string value from the <see cref="T:System.Net.Http.Headers.StringWithQualityHeaderValue" /> object.</summary>
		/// <returns>The string value from the <see cref="T:System.Net.Http.Headers.StringWithQualityHeaderValue" /> object.</returns>
		// Token: 0x170000B9 RID: 185
		// (get) Token: 0x0600031B RID: 795 RVA: 0x0000BC88 File Offset: 0x00009E88
		// (set) Token: 0x0600031C RID: 796 RVA: 0x0000BC90 File Offset: 0x00009E90
		public string Value
		{
			[CompilerGenerated]
			get
			{
				return this.<Value>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<Value>k__BackingField = value;
			}
		}

		/// <summary>Creates a new object that is a copy of the current <see cref="T:System.Net.Http.Headers.StringWithQualityHeaderValue" /> instance.</summary>
		/// <returns>A copy of the current instance.</returns>
		// Token: 0x0600031D RID: 797 RVA: 0x00005BA5 File Offset: 0x00003DA5
		object ICloneable.Clone()
		{
			return base.MemberwiseClone();
		}

		/// <summary>Determines whether the specified Object is equal to the current <see cref="T:System.Net.Http.Headers.StringWithQualityHeaderValue" /> object.</summary>
		/// <param name="obj">The object to compare with the current object.</param>
		/// <returns>
		///     <see langword="true" /> if the specified <see cref="T:System.Object" /> is equal to the current object; otherwise, <see langword="false" />.</returns>
		// Token: 0x0600031E RID: 798 RVA: 0x0000BC9C File Offset: 0x00009E9C
		public override bool Equals(object obj)
		{
			StringWithQualityHeaderValue stringWithQualityHeaderValue = obj as StringWithQualityHeaderValue;
			return stringWithQualityHeaderValue != null && string.Equals(stringWithQualityHeaderValue.Value, this.Value, StringComparison.OrdinalIgnoreCase) && stringWithQualityHeaderValue.Quality == this.Quality;
		}

		/// <summary>Serves as a hash function for an <see cref="T:System.Net.Http.Headers.StringWithQualityHeaderValue" /> object.</summary>
		/// <returns>A hash code for the current object.</returns>
		// Token: 0x0600031F RID: 799 RVA: 0x0000BCFC File Offset: 0x00009EFC
		public override int GetHashCode()
		{
			return this.Value.ToLowerInvariant().GetHashCode() ^ this.Quality.GetHashCode();
		}

		/// <summary>Converts a string to an <see cref="T:System.Net.Http.Headers.StringWithQualityHeaderValue" /> instance.</summary>
		/// <param name="input">A string that represents quality header value information.</param>
		/// <returns>A <see cref="T:System.Net.Http.Headers.StringWithQualityHeaderValue" /> instance.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="input" /> is a <see langword="null" /> reference.</exception>
		/// <exception cref="T:System.FormatException">
		///         <paramref name="input" /> is not valid string with quality header value information.</exception>
		// Token: 0x06000320 RID: 800 RVA: 0x0000BD30 File Offset: 0x00009F30
		public static StringWithQualityHeaderValue Parse(string input)
		{
			StringWithQualityHeaderValue result;
			if (StringWithQualityHeaderValue.TryParse(input, out result))
			{
				return result;
			}
			throw new FormatException(input);
		}

		/// <summary>Determines whether a string is valid <see cref="T:System.Net.Http.Headers.StringWithQualityHeaderValue" /> information.</summary>
		/// <param name="input">The string to validate.</param>
		/// <param name="parsedValue">The <see cref="T:System.Net.Http.Headers.StringWithQualityHeaderValue" /> version of the string.</param>
		/// <returns>
		///     <see langword="true" /> if <paramref name="input" /> is valid <see cref="T:System.Net.Http.Headers.StringWithQualityHeaderValue" /> information; otherwise, <see langword="false" />.</returns>
		// Token: 0x06000321 RID: 801 RVA: 0x0000BD50 File Offset: 0x00009F50
		public static bool TryParse(string input, out StringWithQualityHeaderValue parsedValue)
		{
			Token token;
			if (StringWithQualityHeaderValue.TryParseElement(new Lexer(input), out parsedValue, out token) && token == Token.Type.End)
			{
				return true;
			}
			parsedValue = null;
			return false;
		}

		// Token: 0x06000322 RID: 802 RVA: 0x0000BD7C File Offset: 0x00009F7C
		internal static bool TryParse(string input, int minimalCount, out List<StringWithQualityHeaderValue> result)
		{
			return CollectionParser.TryParse<StringWithQualityHeaderValue>(input, minimalCount, new ElementTryParser<StringWithQualityHeaderValue>(StringWithQualityHeaderValue.TryParseElement), out result);
		}

		// Token: 0x06000323 RID: 803 RVA: 0x0000BD94 File Offset: 0x00009F94
		private static bool TryParseElement(Lexer lexer, out StringWithQualityHeaderValue parsedValue, out Token t)
		{
			parsedValue = null;
			t = lexer.Scan(false);
			if (t != Token.Type.Token)
			{
				return false;
			}
			StringWithQualityHeaderValue stringWithQualityHeaderValue = new StringWithQualityHeaderValue();
			stringWithQualityHeaderValue.Value = lexer.GetStringValue(t);
			t = lexer.Scan(false);
			if (t == Token.Type.SeparatorSemicolon)
			{
				t = lexer.Scan(false);
				if (t != Token.Type.Token)
				{
					return false;
				}
				string stringValue = lexer.GetStringValue(t);
				if (stringValue != "q" && stringValue != "Q")
				{
					return false;
				}
				t = lexer.Scan(false);
				if (t != Token.Type.SeparatorEqual)
				{
					return false;
				}
				t = lexer.Scan(false);
				double num;
				if (!lexer.TryGetDoubleValue(t, out num))
				{
					return false;
				}
				if (num > 1.0)
				{
					return false;
				}
				stringWithQualityHeaderValue.Quality = new double?(num);
				t = lexer.Scan(false);
			}
			parsedValue = stringWithQualityHeaderValue;
			return true;
		}

		/// <summary>Returns a string that represents the current <see cref="T:System.Net.Http.Headers.StringWithQualityHeaderValue" /> object.</summary>
		/// <returns>A string that represents the current object.</returns>
		// Token: 0x06000324 RID: 804 RVA: 0x0000BEA4 File Offset: 0x0000A0A4
		public override string ToString()
		{
			if (this.Quality != null)
			{
				return this.Value + "; q=" + this.Quality.Value.ToString("0.0##", CultureInfo.InvariantCulture);
			}
			return this.Value;
		}

		// Token: 0x0400014B RID: 331
		[CompilerGenerated]
		private double? <Quality>k__BackingField;

		// Token: 0x0400014C RID: 332
		[CompilerGenerated]
		private string <Value>k__BackingField;
	}
}
