﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace System.Net.Http.Headers
{
	/// <summary>Represents an entity-tag header value.</summary>
	// Token: 0x0200002E RID: 46
	public class EntityTagHeaderValue : ICloneable
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Net.Http.Headers.EntityTagHeaderValue" /> class.</summary>
		/// <param name="tag">A string that contains an <see cref="T:System.Net.Http.Headers.EntityTagHeaderValue" />.</param>
		// Token: 0x0600017B RID: 379 RVA: 0x000079F2 File Offset: 0x00005BF2
		public EntityTagHeaderValue(string tag)
		{
			Parser.Token.CheckQuotedString(tag);
			this.Tag = tag;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Net.Http.Headers.EntityTagHeaderValue" /> class.</summary>
		/// <param name="tag">A string that contains an  <see cref="T:System.Net.Http.Headers.EntityTagHeaderValue" />.</param>
		/// <param name="isWeak">A value that indicates if this entity-tag header is a weak validator. If the entity-tag header is weak validator, then <paramref name="isWeak" /> should be set to <see langword="true" />. If the entity-tag header is a strong validator, then <paramref name="isWeak" /> should be set to <see langword="false" />.</param>
		// Token: 0x0600017C RID: 380 RVA: 0x00007A07 File Offset: 0x00005C07
		public EntityTagHeaderValue(string tag, bool isWeak) : this(tag)
		{
			this.IsWeak = isWeak;
		}

		// Token: 0x0600017D RID: 381 RVA: 0x000039D8 File Offset: 0x00001BD8
		internal EntityTagHeaderValue()
		{
		}

		/// <summary>Gets the entity-tag header value.</summary>
		/// <returns>Returns <see cref="T:System.Net.Http.Headers.EntityTagHeaderValue" />.</returns>
		// Token: 0x17000055 RID: 85
		// (get) Token: 0x0600017E RID: 382 RVA: 0x00007A17 File Offset: 0x00005C17
		public static EntityTagHeaderValue Any
		{
			get
			{
				return EntityTagHeaderValue.any;
			}
		}

		/// <summary>Gets whether the entity-tag is prefaced by a weakness indicator.</summary>
		/// <returns>
		///     <see langword="true" /> if the entity-tag is prefaced by a weakness indicator; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000056 RID: 86
		// (get) Token: 0x0600017F RID: 383 RVA: 0x00007A1E File Offset: 0x00005C1E
		// (set) Token: 0x06000180 RID: 384 RVA: 0x00007A26 File Offset: 0x00005C26
		public bool IsWeak
		{
			[CompilerGenerated]
			get
			{
				return this.<IsWeak>k__BackingField;
			}
			[CompilerGenerated]
			internal set
			{
				this.<IsWeak>k__BackingField = value;
			}
		}

		/// <summary>Gets the opaque quoted string. </summary>
		/// <returns>An opaque quoted string.</returns>
		// Token: 0x17000057 RID: 87
		// (get) Token: 0x06000181 RID: 385 RVA: 0x00007A2F File Offset: 0x00005C2F
		// (set) Token: 0x06000182 RID: 386 RVA: 0x00007A37 File Offset: 0x00005C37
		public string Tag
		{
			[CompilerGenerated]
			get
			{
				return this.<Tag>k__BackingField;
			}
			[CompilerGenerated]
			internal set
			{
				this.<Tag>k__BackingField = value;
			}
		}

		/// <summary>Creates a new object that is a copy of the current <see cref="T:System.Net.Http.Headers.EntityTagHeaderValue" /> instance.</summary>
		/// <returns>A copy of the current instance.</returns>
		// Token: 0x06000183 RID: 387 RVA: 0x00005BA5 File Offset: 0x00003DA5
		object ICloneable.Clone()
		{
			return base.MemberwiseClone();
		}

		/// <summary>Determines whether the specified <see cref="T:System.Object" /> is equal to the current <see cref="T:System.Net.Http.Headers.EntityTagHeaderValue" /> object.</summary>
		/// <param name="obj">The object to compare with the current object.</param>
		/// <returns>
		///     <see langword="true" /> if the specified <see cref="T:System.Object" /> is equal to the current object; otherwise, <see langword="false" />.</returns>
		// Token: 0x06000184 RID: 388 RVA: 0x00007A40 File Offset: 0x00005C40
		public override bool Equals(object obj)
		{
			EntityTagHeaderValue entityTagHeaderValue = obj as EntityTagHeaderValue;
			return entityTagHeaderValue != null && entityTagHeaderValue.Tag == this.Tag && string.Equals(entityTagHeaderValue.Tag, this.Tag, StringComparison.Ordinal);
		}

		/// <summary>Serves as a hash function for an <see cref="T:System.Net.Http.Headers.EntityTagHeaderValue" /> object.</summary>
		/// <returns>A hash code for the current object.</returns>
		// Token: 0x06000185 RID: 389 RVA: 0x00007A80 File Offset: 0x00005C80
		public override int GetHashCode()
		{
			return this.IsWeak.GetHashCode() ^ this.Tag.GetHashCode();
		}

		/// <summary>Converts a string to an <see cref="T:System.Net.Http.Headers.EntityTagHeaderValue" /> instance.</summary>
		/// <param name="input">A string that represents entity tag header value information.</param>
		/// <returns>An <see cref="T:System.Net.Http.Headers.EntityTagHeaderValue" /> instance.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="input" /> is a <see langword="null" /> reference.</exception>
		/// <exception cref="T:System.FormatException">
		///         <paramref name="input" /> is not valid entity tag header value information.</exception>
		// Token: 0x06000186 RID: 390 RVA: 0x00007AA8 File Offset: 0x00005CA8
		public static EntityTagHeaderValue Parse(string input)
		{
			EntityTagHeaderValue result;
			if (EntityTagHeaderValue.TryParse(input, out result))
			{
				return result;
			}
			throw new FormatException(input);
		}

		/// <summary>Determines whether a string is valid <see cref="T:System.Net.Http.Headers.EntityTagHeaderValue" /> information.</summary>
		/// <param name="input">The string to validate.</param>
		/// <param name="parsedValue">The <see cref="T:System.Net.Http.Headers.EntityTagHeaderValue" /> version of the string.</param>
		/// <returns>
		///     <see langword="true" /> if <paramref name="input" /> is valid <see cref="T:System.Net.Http.Headers.EntityTagHeaderValue" /> information; otherwise, <see langword="false" />.</returns>
		// Token: 0x06000187 RID: 391 RVA: 0x00007AC8 File Offset: 0x00005CC8
		public static bool TryParse(string input, out EntityTagHeaderValue parsedValue)
		{
			Token token;
			if (EntityTagHeaderValue.TryParseElement(new Lexer(input), out parsedValue, out token) && token == Token.Type.End)
			{
				return true;
			}
			parsedValue = null;
			return false;
		}

		// Token: 0x06000188 RID: 392 RVA: 0x00007AF4 File Offset: 0x00005CF4
		private static bool TryParseElement(Lexer lexer, out EntityTagHeaderValue parsedValue, out Token t)
		{
			parsedValue = null;
			t = lexer.Scan(false);
			bool isWeak = false;
			if (t == Token.Type.Token)
			{
				string stringValue = lexer.GetStringValue(t);
				if (stringValue == "*")
				{
					parsedValue = EntityTagHeaderValue.any;
					t = lexer.Scan(false);
					return true;
				}
				if (stringValue != "W" || lexer.PeekChar() != 47)
				{
					return false;
				}
				isWeak = true;
				lexer.EatChar();
				t = lexer.Scan(false);
			}
			if (t != Token.Type.QuotedString)
			{
				return false;
			}
			parsedValue = new EntityTagHeaderValue();
			parsedValue.Tag = lexer.GetStringValue(t);
			parsedValue.IsWeak = isWeak;
			t = lexer.Scan(false);
			return true;
		}

		// Token: 0x06000189 RID: 393 RVA: 0x00007BBF File Offset: 0x00005DBF
		internal static bool TryParse(string input, int minimalCount, out List<EntityTagHeaderValue> result)
		{
			return CollectionParser.TryParse<EntityTagHeaderValue>(input, minimalCount, new ElementTryParser<EntityTagHeaderValue>(EntityTagHeaderValue.TryParseElement), out result);
		}

		/// <summary>Returns a string that represents the current <see cref="T:System.Net.Http.Headers.EntityTagHeaderValue" /> object.</summary>
		/// <returns>A string that represents the current object.</returns>
		// Token: 0x0600018A RID: 394 RVA: 0x00007BD5 File Offset: 0x00005DD5
		public override string ToString()
		{
			if (!this.IsWeak)
			{
				return this.Tag;
			}
			return "W/" + this.Tag;
		}

		// Token: 0x0600018B RID: 395 RVA: 0x00007BF6 File Offset: 0x00005DF6
		// Note: this type is marked as 'beforefieldinit'.
		static EntityTagHeaderValue()
		{
		}

		// Token: 0x040000F0 RID: 240
		private static readonly EntityTagHeaderValue any = new EntityTagHeaderValue
		{
			Tag = "*"
		};

		// Token: 0x040000F1 RID: 241
		[CompilerGenerated]
		private bool <IsWeak>k__BackingField;

		// Token: 0x040000F2 RID: 242
		[CompilerGenerated]
		private string <Tag>k__BackingField;
	}
}
