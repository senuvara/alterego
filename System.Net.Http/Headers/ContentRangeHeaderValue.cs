﻿using System;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Text;

namespace System.Net.Http.Headers
{
	/// <summary>Represents the value of the Content-Range header.</summary>
	// Token: 0x0200002D RID: 45
	public class ContentRangeHeaderValue : ICloneable
	{
		// Token: 0x06000167 RID: 359 RVA: 0x00007507 File Offset: 0x00005707
		private ContentRangeHeaderValue()
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Net.Http.Headers.ContentRangeHeaderValue" /> class.</summary>
		/// <param name="length">The starting or ending point of the range, in bytes.</param>
		// Token: 0x06000168 RID: 360 RVA: 0x0000751A File Offset: 0x0000571A
		public ContentRangeHeaderValue(long length)
		{
			if (length < 0L)
			{
				throw new ArgumentOutOfRangeException("length");
			}
			this.Length = new long?(length);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Net.Http.Headers.ContentRangeHeaderValue" /> class.</summary>
		/// <param name="from">The position, in bytes, at which to start sending data.</param>
		/// <param name="to">The position, in bytes, at which to stop sending data.</param>
		// Token: 0x06000169 RID: 361 RVA: 0x00007549 File Offset: 0x00005749
		public ContentRangeHeaderValue(long from, long to)
		{
			if (from < 0L || from > to)
			{
				throw new ArgumentOutOfRangeException("from");
			}
			this.From = new long?(from);
			this.To = new long?(to);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Net.Http.Headers.ContentRangeHeaderValue" /> class.</summary>
		/// <param name="from">The position, in bytes, at which to start sending data.</param>
		/// <param name="to">The position, in bytes, at which to stop sending data.</param>
		/// <param name="length">The starting or ending point of the range, in bytes.</param>
		// Token: 0x0600016A RID: 362 RVA: 0x00007588 File Offset: 0x00005788
		public ContentRangeHeaderValue(long from, long to, long length) : this(from, to)
		{
			if (length < 0L)
			{
				throw new ArgumentOutOfRangeException("length");
			}
			if (to > length)
			{
				throw new ArgumentOutOfRangeException("to");
			}
			this.Length = new long?(length);
		}

		/// <summary>Gets the position at which to start sending data.</summary>
		/// <returns>The position, in bytes, at which to start sending data.</returns>
		// Token: 0x1700004F RID: 79
		// (get) Token: 0x0600016B RID: 363 RVA: 0x000075BD File Offset: 0x000057BD
		// (set) Token: 0x0600016C RID: 364 RVA: 0x000075C5 File Offset: 0x000057C5
		public long? From
		{
			[CompilerGenerated]
			get
			{
				return this.<From>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<From>k__BackingField = value;
			}
		}

		/// <summary>Gets whether the Content-Range header has a length specified.</summary>
		/// <returns>
		///     <see langword="true" /> if the Content-Range has a length specified; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000050 RID: 80
		// (get) Token: 0x0600016D RID: 365 RVA: 0x000075D0 File Offset: 0x000057D0
		public bool HasLength
		{
			get
			{
				return this.Length != null;
			}
		}

		/// <summary>Gets whether the Content-Range has a range specified. </summary>
		/// <returns>
		///     <see langword="true" /> if the Content-Range has a range specified; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000051 RID: 81
		// (get) Token: 0x0600016E RID: 366 RVA: 0x000075EC File Offset: 0x000057EC
		public bool HasRange
		{
			get
			{
				return this.From != null;
			}
		}

		/// <summary>Gets the length of the full entity-body.</summary>
		/// <returns>The length of the full entity-body.</returns>
		// Token: 0x17000052 RID: 82
		// (get) Token: 0x0600016F RID: 367 RVA: 0x00007607 File Offset: 0x00005807
		// (set) Token: 0x06000170 RID: 368 RVA: 0x0000760F File Offset: 0x0000580F
		public long? Length
		{
			[CompilerGenerated]
			get
			{
				return this.<Length>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<Length>k__BackingField = value;
			}
		}

		/// <summary>Gets the position at which to stop sending data.</summary>
		/// <returns>The position at which to stop sending data.</returns>
		// Token: 0x17000053 RID: 83
		// (get) Token: 0x06000171 RID: 369 RVA: 0x00007618 File Offset: 0x00005818
		// (set) Token: 0x06000172 RID: 370 RVA: 0x00007620 File Offset: 0x00005820
		public long? To
		{
			[CompilerGenerated]
			get
			{
				return this.<To>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<To>k__BackingField = value;
			}
		}

		/// <summary>The range units used.</summary>
		/// <returns>A <see cref="T:System.String" /> that contains range units. </returns>
		// Token: 0x17000054 RID: 84
		// (get) Token: 0x06000173 RID: 371 RVA: 0x00007629 File Offset: 0x00005829
		// (set) Token: 0x06000174 RID: 372 RVA: 0x00007631 File Offset: 0x00005831
		public string Unit
		{
			get
			{
				return this.unit;
			}
			set
			{
				if (value == null)
				{
					throw new ArgumentNullException("Unit");
				}
				Parser.Token.Check(value);
				this.unit = value;
			}
		}

		/// <summary>Creates a new object that is a copy of the current <see cref="T:System.Net.Http.Headers.ContentRangeHeaderValue" /> instance.</summary>
		/// <returns>A copy of the current instance.</returns>
		// Token: 0x06000175 RID: 373 RVA: 0x00005BA5 File Offset: 0x00003DA5
		object ICloneable.Clone()
		{
			return base.MemberwiseClone();
		}

		/// <summary>Determines whether the specified Object is equal to the current <see cref="T:System.Net.Http.Headers.ContentRangeHeaderValue" /> object.</summary>
		/// <param name="obj">The object to compare with the current object.</param>
		/// <returns>
		///     <see langword="true" /> if the specified <see cref="T:System.Object" /> is equal to the current object; otherwise, <see langword="false" />.</returns>
		// Token: 0x06000176 RID: 374 RVA: 0x00007650 File Offset: 0x00005850
		public override bool Equals(object obj)
		{
			ContentRangeHeaderValue contentRangeHeaderValue = obj as ContentRangeHeaderValue;
			return contentRangeHeaderValue != null && (contentRangeHeaderValue.Length == this.Length && contentRangeHeaderValue.From == this.From && contentRangeHeaderValue.To == this.To) && string.Equals(contentRangeHeaderValue.unit, this.unit, StringComparison.OrdinalIgnoreCase);
		}

		/// <summary>Serves as a hash function for an <see cref="T:System.Net.Http.Headers.ContentRangeHeaderValue" /> object.</summary>
		/// <returns>A hash code for the current object.</returns>
		// Token: 0x06000177 RID: 375 RVA: 0x00007718 File Offset: 0x00005918
		public override int GetHashCode()
		{
			return this.Unit.GetHashCode() ^ this.Length.GetHashCode() ^ this.From.GetHashCode() ^ this.To.GetHashCode() ^ this.unit.ToLowerInvariant().GetHashCode();
		}

		/// <summary>Converts a string to an <see cref="T:System.Net.Http.Headers.ContentRangeHeaderValue" /> instance.</summary>
		/// <param name="input">A string that represents content range header value information.</param>
		/// <returns>An <see cref="T:System.Net.Http.Headers.ContentRangeHeaderValue" /> instance.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="input" /> is a <see langword="null" /> reference.</exception>
		/// <exception cref="T:System.FormatException">
		///         <paramref name="input" /> is not valid content range header value information.</exception>
		// Token: 0x06000178 RID: 376 RVA: 0x00007780 File Offset: 0x00005980
		public static ContentRangeHeaderValue Parse(string input)
		{
			ContentRangeHeaderValue result;
			if (ContentRangeHeaderValue.TryParse(input, out result))
			{
				return result;
			}
			throw new FormatException(input);
		}

		/// <summary>Determines whether a string is valid <see cref="T:System.Net.Http.Headers.ContentRangeHeaderValue" /> information.</summary>
		/// <param name="input">The string to validate.</param>
		/// <param name="parsedValue">The <see cref="T:System.Net.Http.Headers.ContentRangeHeaderValue" /> version of the string.</param>
		/// <returns>
		///     <see langword="true" /> if <paramref name="input" /> is valid <see cref="T:System.Net.Http.Headers.ContentRangeHeaderValue" /> information; otherwise, <see langword="false" />.</returns>
		// Token: 0x06000179 RID: 377 RVA: 0x000077A0 File Offset: 0x000059A0
		public static bool TryParse(string input, out ContentRangeHeaderValue parsedValue)
		{
			parsedValue = null;
			Lexer lexer = new Lexer(input);
			Token token = lexer.Scan(false);
			if (token != Token.Type.Token)
			{
				return false;
			}
			ContentRangeHeaderValue contentRangeHeaderValue = new ContentRangeHeaderValue();
			contentRangeHeaderValue.unit = lexer.GetStringValue(token);
			token = lexer.Scan(false);
			if (token != Token.Type.Token)
			{
				return false;
			}
			if (!lexer.IsStarStringValue(token))
			{
				long value;
				if (!lexer.TryGetNumericValue(token, out value))
				{
					string stringValue = lexer.GetStringValue(token);
					if (stringValue.Length < 3)
					{
						return false;
					}
					string[] array = stringValue.Split(new char[]
					{
						'-'
					});
					if (array.Length != 2)
					{
						return false;
					}
					if (!long.TryParse(array[0], NumberStyles.None, CultureInfo.InvariantCulture, out value))
					{
						return false;
					}
					contentRangeHeaderValue.From = new long?(value);
					if (!long.TryParse(array[1], NumberStyles.None, CultureInfo.InvariantCulture, out value))
					{
						return false;
					}
					contentRangeHeaderValue.To = new long?(value);
				}
				else
				{
					contentRangeHeaderValue.From = new long?(value);
					token = lexer.Scan(true);
					if (token != Token.Type.SeparatorDash)
					{
						return false;
					}
					token = lexer.Scan(false);
					if (!lexer.TryGetNumericValue(token, out value))
					{
						return false;
					}
					contentRangeHeaderValue.To = new long?(value);
				}
			}
			token = lexer.Scan(false);
			if (token != Token.Type.SeparatorSlash)
			{
				return false;
			}
			token = lexer.Scan(false);
			if (!lexer.IsStarStringValue(token))
			{
				long value2;
				if (!lexer.TryGetNumericValue(token, out value2))
				{
					return false;
				}
				contentRangeHeaderValue.Length = new long?(value2);
			}
			token = lexer.Scan(false);
			if (token != Token.Type.End)
			{
				return false;
			}
			parsedValue = contentRangeHeaderValue;
			return true;
		}

		/// <summary>Returns a string that represents the current <see cref="T:System.Net.Http.Headers.ContentRangeHeaderValue" /> object.</summary>
		/// <returns>A string that represents the current object.</returns>
		// Token: 0x0600017A RID: 378 RVA: 0x00007914 File Offset: 0x00005B14
		public override string ToString()
		{
			StringBuilder stringBuilder = new StringBuilder(this.unit);
			stringBuilder.Append(" ");
			if (this.From == null)
			{
				stringBuilder.Append("*");
			}
			else
			{
				stringBuilder.Append(this.From.Value.ToString(CultureInfo.InvariantCulture));
				stringBuilder.Append("-");
				stringBuilder.Append(this.To.Value.ToString(CultureInfo.InvariantCulture));
			}
			stringBuilder.Append("/");
			stringBuilder.Append((this.Length == null) ? "*" : this.Length.Value.ToString(CultureInfo.InvariantCulture));
			return stringBuilder.ToString();
		}

		// Token: 0x040000EC RID: 236
		private string unit = "bytes";

		// Token: 0x040000ED RID: 237
		[CompilerGenerated]
		private long? <From>k__BackingField;

		// Token: 0x040000EE RID: 238
		[CompilerGenerated]
		private long? <Length>k__BackingField;

		// Token: 0x040000EF RID: 239
		[CompilerGenerated]
		private long? <To>k__BackingField;
	}
}
