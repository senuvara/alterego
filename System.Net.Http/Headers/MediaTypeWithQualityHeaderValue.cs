﻿using System;
using System.Collections.Generic;

namespace System.Net.Http.Headers
{
	/// <summary>Represents a media type with an additional quality factor used in a Content-Type header.</summary>
	// Token: 0x02000044 RID: 68
	public sealed class MediaTypeWithQualityHeaderValue : MediaTypeHeaderValue
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Net.Http.Headers.MediaTypeWithQualityHeaderValue" /> class.</summary>
		/// <param name="mediaType">A <see cref="T:System.Net.Http.Headers.MediaTypeWithQualityHeaderValue" /> represented as string to initialize the new instance. </param>
		// Token: 0x06000286 RID: 646 RVA: 0x0000A2A8 File Offset: 0x000084A8
		public MediaTypeWithQualityHeaderValue(string mediaType) : base(mediaType)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Net.Http.Headers.MediaTypeWithQualityHeaderValue" /> class.</summary>
		/// <param name="mediaType">A <see cref="T:System.Net.Http.Headers.MediaTypeWithQualityHeaderValue" /> represented as string to initialize the new instance.</param>
		/// <param name="quality">The quality associated with this header value.</param>
		// Token: 0x06000287 RID: 647 RVA: 0x0000A2B1 File Offset: 0x000084B1
		public MediaTypeWithQualityHeaderValue(string mediaType, double quality) : this(mediaType)
		{
			this.Quality = new double?(quality);
		}

		// Token: 0x06000288 RID: 648 RVA: 0x0000A2C6 File Offset: 0x000084C6
		private MediaTypeWithQualityHeaderValue()
		{
		}

		/// <summary>Get or set the quality value for the <see cref="T:System.Net.Http.Headers.MediaTypeWithQualityHeaderValue" />.</summary>
		/// <returns>The quality value for the <see cref="T:System.Net.Http.Headers.MediaTypeWithQualityHeaderValue" /> object.</returns>
		// Token: 0x170000A8 RID: 168
		// (get) Token: 0x06000289 RID: 649 RVA: 0x0000A2CE File Offset: 0x000084CE
		// (set) Token: 0x0600028A RID: 650 RVA: 0x0000A2DB File Offset: 0x000084DB
		public double? Quality
		{
			get
			{
				return QualityValue.GetValue(this.parameters);
			}
			set
			{
				QualityValue.SetValue(ref this.parameters, value);
			}
		}

		/// <summary>Converts a string to an <see cref="T:System.Net.Http.Headers.MediaTypeWithQualityHeaderValue" /> instance.</summary>
		/// <param name="input">A string that represents media type with quality header value information.</param>
		/// <returns>A <see cref="T:System.Net.Http.Headers.MediaTypeWithQualityHeaderValue" /> instance.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="input" /> is a <see langword="null" /> reference.</exception>
		/// <exception cref="T:System.FormatException">
		///         <paramref name="input" /> is not valid media type with quality header value information.</exception>
		// Token: 0x0600028B RID: 651 RVA: 0x0000A2EC File Offset: 0x000084EC
		public new static MediaTypeWithQualityHeaderValue Parse(string input)
		{
			MediaTypeWithQualityHeaderValue result;
			if (MediaTypeWithQualityHeaderValue.TryParse(input, out result))
			{
				return result;
			}
			throw new FormatException();
		}

		/// <summary>Determines whether a string is valid <see cref="T:System.Net.Http.Headers.MediaTypeWithQualityHeaderValue" /> information.</summary>
		/// <param name="input">The string to validate.</param>
		/// <param name="parsedValue">The <see cref="T:System.Net.Http.Headers.MediaTypeWithQualityHeaderValue" /> version of the string.</param>
		/// <returns>
		///     <see langword="true" /> if <paramref name="input" /> is valid <see cref="T:System.Net.Http.Headers.MediaTypeWithQualityHeaderValue" /> information; otherwise, <see langword="false" />.</returns>
		// Token: 0x0600028C RID: 652 RVA: 0x0000A30C File Offset: 0x0000850C
		public static bool TryParse(string input, out MediaTypeWithQualityHeaderValue parsedValue)
		{
			Token token;
			if (MediaTypeWithQualityHeaderValue.TryParseElement(new Lexer(input), out parsedValue, out token) && token == Token.Type.End)
			{
				return true;
			}
			parsedValue = null;
			return false;
		}

		// Token: 0x0600028D RID: 653 RVA: 0x0000A338 File Offset: 0x00008538
		private static bool TryParseElement(Lexer lexer, out MediaTypeWithQualityHeaderValue parsedValue, out Token t)
		{
			parsedValue = null;
			List<NameValueHeaderValue> parameters = null;
			string media_type;
			Token? token = MediaTypeHeaderValue.TryParseMediaType(lexer, out media_type);
			if (token == null)
			{
				t = Token.Empty;
				return false;
			}
			t = token.Value;
			if (t == Token.Type.SeparatorSemicolon && !NameValueHeaderValue.TryParseParameters(lexer, out parameters, out t))
			{
				return false;
			}
			parsedValue = new MediaTypeWithQualityHeaderValue();
			parsedValue.media_type = media_type;
			parsedValue.parameters = parameters;
			return true;
		}

		// Token: 0x0600028E RID: 654 RVA: 0x0000A3A9 File Offset: 0x000085A9
		internal static bool TryParse(string input, int minimalCount, out List<MediaTypeWithQualityHeaderValue> result)
		{
			return CollectionParser.TryParse<MediaTypeWithQualityHeaderValue>(input, minimalCount, new ElementTryParser<MediaTypeWithQualityHeaderValue>(MediaTypeWithQualityHeaderValue.TryParseElement), out result);
		}
	}
}
