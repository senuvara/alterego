﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace System.Net.Http.Headers
{
	/// <summary>Represents the value of a Via header.</summary>
	// Token: 0x0200005E RID: 94
	public class ViaHeaderValue : ICloneable
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Net.Http.Headers.ViaHeaderValue" /> class.</summary>
		/// <param name="protocolVersion">The protocol version of the received protocol.</param>
		/// <param name="receivedBy">The host and port that the request or response was received by.</param>
		// Token: 0x0600033B RID: 827 RVA: 0x0000C25F File Offset: 0x0000A45F
		public ViaHeaderValue(string protocolVersion, string receivedBy)
		{
			Parser.Token.Check(protocolVersion);
			Parser.Uri.Check(receivedBy);
			this.ProtocolVersion = protocolVersion;
			this.ReceivedBy = receivedBy;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Net.Http.Headers.ViaHeaderValue" /> class.</summary>
		/// <param name="protocolVersion">The protocol version of the received protocol.</param>
		/// <param name="receivedBy">The host and port that the request or response was received by.</param>
		/// <param name="protocolName">The protocol name of the received protocol.</param>
		// Token: 0x0600033C RID: 828 RVA: 0x0000C281 File Offset: 0x0000A481
		public ViaHeaderValue(string protocolVersion, string receivedBy, string protocolName) : this(protocolVersion, receivedBy)
		{
			if (!string.IsNullOrEmpty(protocolName))
			{
				Parser.Token.Check(protocolName);
				this.ProtocolName = protocolName;
			}
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Net.Http.Headers.ViaHeaderValue" /> class.</summary>
		/// <param name="protocolVersion">The protocol version of the received protocol.</param>
		/// <param name="receivedBy">The host and port that the request or response was received by.</param>
		/// <param name="protocolName">The protocol name of the received protocol.</param>
		/// <param name="comment">The comment field used to identify the software of the recipient proxy or gateway.</param>
		// Token: 0x0600033D RID: 829 RVA: 0x0000C2A0 File Offset: 0x0000A4A0
		public ViaHeaderValue(string protocolVersion, string receivedBy, string protocolName, string comment) : this(protocolVersion, receivedBy, protocolName)
		{
			if (!string.IsNullOrEmpty(comment))
			{
				Parser.Token.CheckComment(comment);
				this.Comment = comment;
			}
		}

		// Token: 0x0600033E RID: 830 RVA: 0x000039D8 File Offset: 0x00001BD8
		private ViaHeaderValue()
		{
		}

		/// <summary>Gets the comment field used to identify the software of the recipient proxy or gateway.</summary>
		/// <returns>The comment field used to identify the software of the recipient proxy or gateway.</returns>
		// Token: 0x170000BD RID: 189
		// (get) Token: 0x0600033F RID: 831 RVA: 0x0000C2C3 File Offset: 0x0000A4C3
		// (set) Token: 0x06000340 RID: 832 RVA: 0x0000C2CB File Offset: 0x0000A4CB
		public string Comment
		{
			[CompilerGenerated]
			get
			{
				return this.<Comment>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<Comment>k__BackingField = value;
			}
		}

		/// <summary>Gets the protocol name of the received protocol.</summary>
		/// <returns>The protocol name.</returns>
		// Token: 0x170000BE RID: 190
		// (get) Token: 0x06000341 RID: 833 RVA: 0x0000C2D4 File Offset: 0x0000A4D4
		// (set) Token: 0x06000342 RID: 834 RVA: 0x0000C2DC File Offset: 0x0000A4DC
		public string ProtocolName
		{
			[CompilerGenerated]
			get
			{
				return this.<ProtocolName>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<ProtocolName>k__BackingField = value;
			}
		}

		/// <summary>Gets the protocol version of the received protocol.</summary>
		/// <returns>The protocol version.</returns>
		// Token: 0x170000BF RID: 191
		// (get) Token: 0x06000343 RID: 835 RVA: 0x0000C2E5 File Offset: 0x0000A4E5
		// (set) Token: 0x06000344 RID: 836 RVA: 0x0000C2ED File Offset: 0x0000A4ED
		public string ProtocolVersion
		{
			[CompilerGenerated]
			get
			{
				return this.<ProtocolVersion>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<ProtocolVersion>k__BackingField = value;
			}
		}

		/// <summary>Gets the host and port that the request or response was received by.</summary>
		/// <returns>The host and port that the request or response was received by.</returns>
		// Token: 0x170000C0 RID: 192
		// (get) Token: 0x06000345 RID: 837 RVA: 0x0000C2F6 File Offset: 0x0000A4F6
		// (set) Token: 0x06000346 RID: 838 RVA: 0x0000C2FE File Offset: 0x0000A4FE
		public string ReceivedBy
		{
			[CompilerGenerated]
			get
			{
				return this.<ReceivedBy>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<ReceivedBy>k__BackingField = value;
			}
		}

		/// <summary>Creates a new object that is a copy of the current <see cref="T:System.Net.Http.Headers.ViaHeaderValue" /> instance.</summary>
		/// <returns>A copy of the current instance.</returns>
		// Token: 0x06000347 RID: 839 RVA: 0x00005BA5 File Offset: 0x00003DA5
		object ICloneable.Clone()
		{
			return base.MemberwiseClone();
		}

		/// <summary>Determines whether the specified <see cref="T:System.Object" /> is equal to the current <see cref="T:System.Net.Http.Headers.ViaHeaderValue" />object.</summary>
		/// <param name="obj">The object to compare with the current object.</param>
		/// <returns>
		///     <see langword="true" /> if the specified <see cref="T:System.Object" /> is equal to the current object; otherwise, <see langword="false" />.</returns>
		// Token: 0x06000348 RID: 840 RVA: 0x0000C308 File Offset: 0x0000A508
		public override bool Equals(object obj)
		{
			ViaHeaderValue viaHeaderValue = obj as ViaHeaderValue;
			return viaHeaderValue != null && (string.Equals(viaHeaderValue.Comment, this.Comment, StringComparison.Ordinal) && string.Equals(viaHeaderValue.ProtocolName, this.ProtocolName, StringComparison.OrdinalIgnoreCase) && string.Equals(viaHeaderValue.ProtocolVersion, this.ProtocolVersion, StringComparison.OrdinalIgnoreCase)) && string.Equals(viaHeaderValue.ReceivedBy, this.ReceivedBy, StringComparison.OrdinalIgnoreCase);
		}

		/// <summary>Serves as a hash function for an <see cref="T:System.Net.Http.Headers.ViaHeaderValue" /> object.</summary>
		/// <returns>Returns a hash code for the current object.</returns>
		// Token: 0x06000349 RID: 841 RVA: 0x0000C374 File Offset: 0x0000A574
		public override int GetHashCode()
		{
			int num = this.ProtocolVersion.ToLowerInvariant().GetHashCode();
			num ^= this.ReceivedBy.ToLowerInvariant().GetHashCode();
			if (!string.IsNullOrEmpty(this.ProtocolName))
			{
				num ^= this.ProtocolName.ToLowerInvariant().GetHashCode();
			}
			if (!string.IsNullOrEmpty(this.Comment))
			{
				num ^= this.Comment.GetHashCode();
			}
			return num;
		}

		/// <summary>Converts a string to an <see cref="T:System.Net.Http.Headers.ViaHeaderValue" /> instance.</summary>
		/// <param name="input">A string that represents via header value information.</param>
		/// <returns>A <see cref="T:System.Net.Http.Headers.ViaHeaderValue" /> instance.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="input" /> is a <see langword="null" /> reference.</exception>
		/// <exception cref="T:System.FormatException">
		///         <paramref name="input" /> is not valid via header value information.</exception>
		// Token: 0x0600034A RID: 842 RVA: 0x0000C3E4 File Offset: 0x0000A5E4
		public static ViaHeaderValue Parse(string input)
		{
			ViaHeaderValue result;
			if (ViaHeaderValue.TryParse(input, out result))
			{
				return result;
			}
			throw new FormatException(input);
		}

		/// <summary>Determines whether a string is valid <see cref="T:System.Net.Http.Headers.ViaHeaderValue" /> information.</summary>
		/// <param name="input">The string to validate.</param>
		/// <param name="parsedValue">The <see cref="T:System.Net.Http.Headers.ViaHeaderValue" /> version of the string.</param>
		/// <returns>
		///     <see langword="true" /> if <paramref name="input" /> is valid <see cref="T:System.Net.Http.Headers.ViaHeaderValue" /> information; otherwise, <see langword="false" />.</returns>
		// Token: 0x0600034B RID: 843 RVA: 0x0000C404 File Offset: 0x0000A604
		public static bool TryParse(string input, out ViaHeaderValue parsedValue)
		{
			Token token;
			if (ViaHeaderValue.TryParseElement(new Lexer(input), out parsedValue, out token) && token == Token.Type.End)
			{
				return true;
			}
			parsedValue = null;
			return false;
		}

		// Token: 0x0600034C RID: 844 RVA: 0x0000C430 File Offset: 0x0000A630
		internal static bool TryParse(string input, int minimalCount, out List<ViaHeaderValue> result)
		{
			return CollectionParser.TryParse<ViaHeaderValue>(input, minimalCount, new ElementTryParser<ViaHeaderValue>(ViaHeaderValue.TryParseElement), out result);
		}

		// Token: 0x0600034D RID: 845 RVA: 0x0000C448 File Offset: 0x0000A648
		private static bool TryParseElement(Lexer lexer, out ViaHeaderValue parsedValue, out Token t)
		{
			parsedValue = null;
			t = lexer.Scan(false);
			if (t != Token.Type.Token)
			{
				return false;
			}
			Token token = lexer.Scan(false);
			ViaHeaderValue viaHeaderValue = new ViaHeaderValue();
			if (token == Token.Type.SeparatorSlash)
			{
				token = lexer.Scan(false);
				if (token != Token.Type.Token)
				{
					return false;
				}
				viaHeaderValue.ProtocolName = lexer.GetStringValue(t);
				viaHeaderValue.ProtocolVersion = lexer.GetStringValue(token);
				token = lexer.Scan(false);
			}
			else
			{
				viaHeaderValue.ProtocolVersion = lexer.GetStringValue(t);
			}
			if (token != Token.Type.Token)
			{
				return false;
			}
			if (lexer.PeekChar() == 58)
			{
				lexer.EatChar();
				t = lexer.Scan(false);
				if (t != Token.Type.Token)
				{
					return false;
				}
			}
			else
			{
				t = token;
			}
			viaHeaderValue.ReceivedBy = lexer.GetStringValue(token, t);
			string comment;
			if (lexer.ScanCommentOptional(out comment, out t))
			{
				t = lexer.Scan(false);
			}
			viaHeaderValue.Comment = comment;
			parsedValue = viaHeaderValue;
			return true;
		}

		/// <summary>Returns a string that represents the current <see cref="T:System.Net.Http.Headers.ViaHeaderValue" /> object.</summary>
		/// <returns>A string that represents the current object.</returns>
		// Token: 0x0600034E RID: 846 RVA: 0x0000C550 File Offset: 0x0000A750
		public override string ToString()
		{
			string text = (this.ProtocolName != null) ? string.Concat(new string[]
			{
				this.ProtocolName,
				"/",
				this.ProtocolVersion,
				" ",
				this.ReceivedBy
			}) : (this.ProtocolVersion + " " + this.ReceivedBy);
			if (this.Comment == null)
			{
				return text;
			}
			return text + " " + this.Comment;
		}

		// Token: 0x0400014F RID: 335
		[CompilerGenerated]
		private string <Comment>k__BackingField;

		// Token: 0x04000150 RID: 336
		[CompilerGenerated]
		private string <ProtocolName>k__BackingField;

		// Token: 0x04000151 RID: 337
		[CompilerGenerated]
		private string <ProtocolVersion>k__BackingField;

		// Token: 0x04000152 RID: 338
		[CompilerGenerated]
		private string <ReceivedBy>k__BackingField;
	}
}
