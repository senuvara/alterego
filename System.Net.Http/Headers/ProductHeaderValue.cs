﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace System.Net.Http.Headers
{
	/// <summary>Represents a product token value in a User-Agent header.</summary>
	// Token: 0x02000053 RID: 83
	public class ProductHeaderValue : ICloneable
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Net.Http.Headers.ProductHeaderValue" /> class.</summary>
		/// <param name="name">The product name.</param>
		// Token: 0x060002C4 RID: 708 RVA: 0x0000AC25 File Offset: 0x00008E25
		public ProductHeaderValue(string name)
		{
			Parser.Token.Check(name);
			this.Name = name;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Net.Http.Headers.ProductHeaderValue" /> class.</summary>
		/// <param name="name">The product name value.</param>
		/// <param name="version">The product version value.</param>
		// Token: 0x060002C5 RID: 709 RVA: 0x0000AC3A File Offset: 0x00008E3A
		public ProductHeaderValue(string name, string version) : this(name)
		{
			if (version != null)
			{
				Parser.Token.Check(version);
			}
			this.Version = version;
		}

		// Token: 0x060002C6 RID: 710 RVA: 0x000039D8 File Offset: 0x00001BD8
		internal ProductHeaderValue()
		{
		}

		/// <summary>Gets the name of the product token.</summary>
		/// <returns>The name of the product token.</returns>
		// Token: 0x170000AC RID: 172
		// (get) Token: 0x060002C7 RID: 711 RVA: 0x0000AC53 File Offset: 0x00008E53
		// (set) Token: 0x060002C8 RID: 712 RVA: 0x0000AC5B File Offset: 0x00008E5B
		public string Name
		{
			[CompilerGenerated]
			get
			{
				return this.<Name>k__BackingField;
			}
			[CompilerGenerated]
			internal set
			{
				this.<Name>k__BackingField = value;
			}
		}

		/// <summary>Gets the version of the product token.</summary>
		/// <returns>The version of the product token. </returns>
		// Token: 0x170000AD RID: 173
		// (get) Token: 0x060002C9 RID: 713 RVA: 0x0000AC64 File Offset: 0x00008E64
		// (set) Token: 0x060002CA RID: 714 RVA: 0x0000AC6C File Offset: 0x00008E6C
		public string Version
		{
			[CompilerGenerated]
			get
			{
				return this.<Version>k__BackingField;
			}
			[CompilerGenerated]
			internal set
			{
				this.<Version>k__BackingField = value;
			}
		}

		/// <summary>Creates a new object that is a copy of the current <see cref="T:System.Net.Http.Headers.ProductHeaderValue" /> instance.</summary>
		/// <returns>A copy of the current instance.</returns>
		// Token: 0x060002CB RID: 715 RVA: 0x00005BA5 File Offset: 0x00003DA5
		object ICloneable.Clone()
		{
			return base.MemberwiseClone();
		}

		/// <summary>Determines whether the specified <see cref="T:System.Object" /> is equal to the current <see cref="T:System.Net.Http.Headers.ProductHeaderValue" /> object.</summary>
		/// <param name="obj">The object to compare with the current object.</param>
		/// <returns>
		///     <see langword="true" /> if the specified <see cref="T:System.Object" /> is equal to the current object; otherwise, <see langword="false" />.</returns>
		// Token: 0x060002CC RID: 716 RVA: 0x0000AC78 File Offset: 0x00008E78
		public override bool Equals(object obj)
		{
			ProductHeaderValue productHeaderValue = obj as ProductHeaderValue;
			return productHeaderValue != null && string.Equals(productHeaderValue.Name, this.Name, StringComparison.OrdinalIgnoreCase) && string.Equals(productHeaderValue.Version, this.Version, StringComparison.OrdinalIgnoreCase);
		}

		/// <summary>Serves as a hash function for an <see cref="T:System.Net.Http.Headers.ProductHeaderValue" /> object.</summary>
		/// <returns>A hash code for the current object.</returns>
		// Token: 0x060002CD RID: 717 RVA: 0x0000ACBC File Offset: 0x00008EBC
		public override int GetHashCode()
		{
			int num = this.Name.ToLowerInvariant().GetHashCode();
			if (this.Version != null)
			{
				num ^= this.Version.ToLowerInvariant().GetHashCode();
			}
			return num;
		}

		/// <summary>Converts a string to an <see cref="T:System.Net.Http.Headers.ProductHeaderValue" /> instance.</summary>
		/// <param name="input">A string that represents product header value information.</param>
		/// <returns>A <see cref="T:System.Net.Http.Headers.ProductHeaderValue" /> instance.</returns>
		// Token: 0x060002CE RID: 718 RVA: 0x0000ACF8 File Offset: 0x00008EF8
		public static ProductHeaderValue Parse(string input)
		{
			ProductHeaderValue result;
			if (ProductHeaderValue.TryParse(input, out result))
			{
				return result;
			}
			throw new FormatException(input);
		}

		/// <summary>Determines whether a string is valid <see cref="T:System.Net.Http.Headers.ProductHeaderValue" /> information.</summary>
		/// <param name="input">The string to validate.</param>
		/// <param name="parsedValue">The <see cref="T:System.Net.Http.Headers.ProductHeaderValue" /> version of the string.</param>
		/// <returns>
		///     <see langword="true" /> if <paramref name="input" /> is valid <see cref="T:System.Net.Http.Headers.ProductHeaderValue" /> information; otherwise, <see langword="false" />.</returns>
		// Token: 0x060002CF RID: 719 RVA: 0x0000AD18 File Offset: 0x00008F18
		public static bool TryParse(string input, out ProductHeaderValue parsedValue)
		{
			Token token;
			if (ProductHeaderValue.TryParseElement(new Lexer(input), out parsedValue, out token) && token == Token.Type.End)
			{
				return true;
			}
			parsedValue = null;
			return false;
		}

		// Token: 0x060002D0 RID: 720 RVA: 0x0000AD44 File Offset: 0x00008F44
		internal static bool TryParse(string input, int minimalCount, out List<ProductHeaderValue> result)
		{
			return CollectionParser.TryParse<ProductHeaderValue>(input, minimalCount, new ElementTryParser<ProductHeaderValue>(ProductHeaderValue.TryParseElement), out result);
		}

		// Token: 0x060002D1 RID: 721 RVA: 0x0000AD5C File Offset: 0x00008F5C
		private static bool TryParseElement(Lexer lexer, out ProductHeaderValue parsedValue, out Token t)
		{
			parsedValue = null;
			t = lexer.Scan(false);
			if (t != Token.Type.Token)
			{
				return false;
			}
			parsedValue = new ProductHeaderValue();
			parsedValue.Name = lexer.GetStringValue(t);
			t = lexer.Scan(false);
			if (t == Token.Type.SeparatorSlash)
			{
				t = lexer.Scan(false);
				if (t != Token.Type.Token)
				{
					return false;
				}
				parsedValue.Version = lexer.GetStringValue(t);
				t = lexer.Scan(false);
			}
			return true;
		}

		/// <summary>Returns a string that represents the current <see cref="T:System.Net.Http.Headers.ProductHeaderValue" /> object.</summary>
		/// <returns>A string that represents the current object.</returns>
		// Token: 0x060002D2 RID: 722 RVA: 0x0000ADFC File Offset: 0x00008FFC
		public override string ToString()
		{
			if (this.Version != null)
			{
				return this.Name + "/" + this.Version;
			}
			return this.Name;
		}

		// Token: 0x0400013D RID: 317
		[CompilerGenerated]
		private string <Name>k__BackingField;

		// Token: 0x0400013E RID: 318
		[CompilerGenerated]
		private string <Version>k__BackingField;
	}
}
