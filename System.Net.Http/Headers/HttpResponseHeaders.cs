﻿using System;
using System.Runtime.CompilerServices;

namespace System.Net.Http.Headers
{
	/// <summary>Represents the collection of Response Headers as defined in RFC 2616.</summary>
	// Token: 0x0200003D RID: 61
	public sealed class HttpResponseHeaders : HttpHeaders
	{
		// Token: 0x06000232 RID: 562 RVA: 0x0000966A File Offset: 0x0000786A
		internal HttpResponseHeaders() : base(HttpHeaderKind.Response)
		{
		}

		/// <summary>Gets the value of the <see langword="Accept-Ranges" /> header for an HTTP response.</summary>
		/// <returns>The value of the <see langword="Accept-Ranges" /> header for an HTTP response.</returns>
		// Token: 0x1700008D RID: 141
		// (get) Token: 0x06000233 RID: 563 RVA: 0x00009673 File Offset: 0x00007873
		public HttpHeaderValueCollection<string> AcceptRanges
		{
			get
			{
				return base.GetValues<string>("Accept-Ranges");
			}
		}

		/// <summary>Gets or sets the value of the <see langword="Age" /> header for an HTTP response.</summary>
		/// <returns>The value of the <see langword="Age" /> header for an HTTP response.</returns>
		// Token: 0x1700008E RID: 142
		// (get) Token: 0x06000234 RID: 564 RVA: 0x00009680 File Offset: 0x00007880
		// (set) Token: 0x06000235 RID: 565 RVA: 0x0000968D File Offset: 0x0000788D
		public TimeSpan? Age
		{
			get
			{
				return base.GetValue<TimeSpan?>("Age");
			}
			set
			{
				base.AddOrRemove<TimeSpan>("Age", value, (object l) => ((long)((TimeSpan)l).TotalSeconds).ToString());
			}
		}

		/// <summary>Gets or sets the value of the <see langword="Cache-Control" /> header for an HTTP response.</summary>
		/// <returns>The value of the <see langword="Cache-Control" /> header for an HTTP response.</returns>
		// Token: 0x1700008F RID: 143
		// (get) Token: 0x06000236 RID: 566 RVA: 0x0000909C File Offset: 0x0000729C
		// (set) Token: 0x06000237 RID: 567 RVA: 0x000090A9 File Offset: 0x000072A9
		public CacheControlHeaderValue CacheControl
		{
			get
			{
				return base.GetValue<CacheControlHeaderValue>("Cache-Control");
			}
			set
			{
				base.AddOrRemove<CacheControlHeaderValue>("Cache-Control", value, null);
			}
		}

		/// <summary>Gets the value of the <see langword="Connection" /> header for an HTTP response.</summary>
		/// <returns>The value of the <see langword="Connection" /> header for an HTTP response.</returns>
		// Token: 0x17000090 RID: 144
		// (get) Token: 0x06000238 RID: 568 RVA: 0x000090B8 File Offset: 0x000072B8
		public HttpHeaderValueCollection<string> Connection
		{
			get
			{
				return base.GetValues<string>("Connection");
			}
		}

		/// <summary>Gets or sets a value that indicates if the <see langword="Connection" /> header for an HTTP response contains Close.</summary>
		/// <returns>
		///     <see langword="true" /> if the <see langword="Connection" /> header contains Close, otherwise <see langword="false" />.</returns>
		// Token: 0x17000091 RID: 145
		// (get) Token: 0x06000239 RID: 569 RVA: 0x000096BC File Offset: 0x000078BC
		// (set) Token: 0x0600023A RID: 570 RVA: 0x00009724 File Offset: 0x00007924
		public bool? ConnectionClose
		{
			get
			{
				if (!(this.connectionclose == true))
				{
					if (this.Connection.Find((string l) => string.Equals(l, "close", StringComparison.OrdinalIgnoreCase)) == null)
					{
						return this.connectionclose;
					}
				}
				return new bool?(true);
			}
			set
			{
				if (this.connectionclose == value)
				{
					return;
				}
				this.Connection.Remove("close");
				if (value == true)
				{
					this.Connection.Add("close");
				}
				this.connectionclose = value;
			}
		}

		/// <summary>Gets or sets the value of the <see langword="Date" /> header for an HTTP response.</summary>
		/// <returns>The value of the <see langword="Date" /> header for an HTTP response.</returns>
		// Token: 0x17000092 RID: 146
		// (get) Token: 0x0600023B RID: 571 RVA: 0x000091DD File Offset: 0x000073DD
		// (set) Token: 0x0600023C RID: 572 RVA: 0x000091EA File Offset: 0x000073EA
		public DateTimeOffset? Date
		{
			get
			{
				return base.GetValue<DateTimeOffset?>("Date");
			}
			set
			{
				base.AddOrRemove<DateTimeOffset>("Date", value, Parser.DateTime.ToString);
			}
		}

		/// <summary>Gets or sets the value of the <see langword="ETag" /> header for an HTTP response.</summary>
		/// <returns>The value of the <see langword="ETag" /> header for an HTTP response.</returns>
		// Token: 0x17000093 RID: 147
		// (get) Token: 0x0600023D RID: 573 RVA: 0x000097A2 File Offset: 0x000079A2
		// (set) Token: 0x0600023E RID: 574 RVA: 0x000097AF File Offset: 0x000079AF
		public EntityTagHeaderValue ETag
		{
			get
			{
				return base.GetValue<EntityTagHeaderValue>("ETag");
			}
			set
			{
				base.AddOrRemove<EntityTagHeaderValue>("ETag", value, null);
			}
		}

		/// <summary>Gets or sets the value of the <see langword="Location" /> header for an HTTP response.</summary>
		/// <returns>The value of the <see langword="Location" /> header for an HTTP response.</returns>
		// Token: 0x17000094 RID: 148
		// (get) Token: 0x0600023F RID: 575 RVA: 0x000097BE File Offset: 0x000079BE
		// (set) Token: 0x06000240 RID: 576 RVA: 0x000097CB File Offset: 0x000079CB
		public Uri Location
		{
			get
			{
				return base.GetValue<Uri>("Location");
			}
			set
			{
				base.AddOrRemove<Uri>("Location", value, null);
			}
		}

		/// <summary>Gets the value of the <see langword="Pragma" /> header for an HTTP response.</summary>
		/// <returns>Returns <see cref="T:System.Net.Http.Headers.HttpHeaderValueCollection`1" />.The value of the <see langword="Pragma" /> header for an HTTP response.</returns>
		// Token: 0x17000095 RID: 149
		// (get) Token: 0x06000241 RID: 577 RVA: 0x000093E7 File Offset: 0x000075E7
		public HttpHeaderValueCollection<NameValueHeaderValue> Pragma
		{
			get
			{
				return base.GetValues<NameValueHeaderValue>("Pragma");
			}
		}

		/// <summary>Gets the value of the <see langword="Proxy-Authenticate" /> header for an HTTP response.</summary>
		/// <returns>The value of the <see langword="Proxy-Authenticate" /> header for an HTTP response.</returns>
		// Token: 0x17000096 RID: 150
		// (get) Token: 0x06000242 RID: 578 RVA: 0x000097DA File Offset: 0x000079DA
		public HttpHeaderValueCollection<AuthenticationHeaderValue> ProxyAuthenticate
		{
			get
			{
				return base.GetValues<AuthenticationHeaderValue>("Proxy-Authenticate");
			}
		}

		/// <summary>Gets or sets the value of the <see langword="Retry-After" /> header for an HTTP response.</summary>
		/// <returns>The value of the <see langword="Retry-After" /> header for an HTTP response.</returns>
		// Token: 0x17000097 RID: 151
		// (get) Token: 0x06000243 RID: 579 RVA: 0x000097E7 File Offset: 0x000079E7
		// (set) Token: 0x06000244 RID: 580 RVA: 0x000097F4 File Offset: 0x000079F4
		public RetryConditionHeaderValue RetryAfter
		{
			get
			{
				return base.GetValue<RetryConditionHeaderValue>("Retry-After");
			}
			set
			{
				base.AddOrRemove<RetryConditionHeaderValue>("Retry-After", value, null);
			}
		}

		/// <summary>Gets the value of the <see langword="Server" /> header for an HTTP response.</summary>
		/// <returns>The value of the <see langword="Server" /> header for an HTTP response.</returns>
		// Token: 0x17000098 RID: 152
		// (get) Token: 0x06000245 RID: 581 RVA: 0x00009803 File Offset: 0x00007A03
		public HttpHeaderValueCollection<ProductInfoHeaderValue> Server
		{
			get
			{
				return base.GetValues<ProductInfoHeaderValue>("Server");
			}
		}

		/// <summary>Gets the value of the <see langword="Trailer" /> header for an HTTP response.</summary>
		/// <returns>The value of the <see langword="Trailer" /> header for an HTTP response.</returns>
		// Token: 0x17000099 RID: 153
		// (get) Token: 0x06000246 RID: 582 RVA: 0x00009455 File Offset: 0x00007655
		public HttpHeaderValueCollection<string> Trailer
		{
			get
			{
				return base.GetValues<string>("Trailer");
			}
		}

		/// <summary>Gets the value of the <see langword="Transfer-Encoding" /> header for an HTTP response.</summary>
		/// <returns>The value of the <see langword="Transfer-Encoding" /> header for an HTTP response.</returns>
		// Token: 0x1700009A RID: 154
		// (get) Token: 0x06000247 RID: 583 RVA: 0x00009462 File Offset: 0x00007662
		public HttpHeaderValueCollection<TransferCodingHeaderValue> TransferEncoding
		{
			get
			{
				return base.GetValues<TransferCodingHeaderValue>("Transfer-Encoding");
			}
		}

		/// <summary>Gets or sets a value that indicates if the <see langword="Transfer-Encoding" /> header for an HTTP response contains chunked.</summary>
		/// <returns>
		///     <see langword="true" /> if the <see langword="Transfer-Encoding" /> header contains chunked, otherwise <see langword="false" />.</returns>
		// Token: 0x1700009B RID: 155
		// (get) Token: 0x06000248 RID: 584 RVA: 0x00009810 File Offset: 0x00007A10
		// (set) Token: 0x06000249 RID: 585 RVA: 0x00009870 File Offset: 0x00007A70
		public bool? TransferEncodingChunked
		{
			get
			{
				if (this.transferEncodingChunked != null)
				{
					return this.transferEncodingChunked;
				}
				if (this.TransferEncoding.Find((TransferCodingHeaderValue l) => StringComparer.OrdinalIgnoreCase.Equals(l.Value, "chunked")) == null)
				{
					return null;
				}
				return new bool?(true);
			}
			set
			{
				if (value == this.transferEncodingChunked)
				{
					return;
				}
				this.TransferEncoding.Remove((TransferCodingHeaderValue l) => l.Value == "chunked");
				if (value == true)
				{
					this.TransferEncoding.Add(new TransferCodingHeaderValue("chunked"));
				}
				this.transferEncodingChunked = value;
			}
		}

		/// <summary>Gets the value of the <see langword="Upgrade" /> header for an HTTP response.</summary>
		/// <returns>The value of the <see langword="Upgrade" /> header for an HTTP response.</returns>
		// Token: 0x1700009C RID: 156
		// (get) Token: 0x0600024A RID: 586 RVA: 0x0000956C File Offset: 0x0000776C
		public HttpHeaderValueCollection<ProductHeaderValue> Upgrade
		{
			get
			{
				return base.GetValues<ProductHeaderValue>("Upgrade");
			}
		}

		/// <summary>Gets the value of the <see langword="Vary" /> header for an HTTP response.</summary>
		/// <returns>The value of the <see langword="Vary" /> header for an HTTP response.</returns>
		// Token: 0x1700009D RID: 157
		// (get) Token: 0x0600024B RID: 587 RVA: 0x0000990C File Offset: 0x00007B0C
		public HttpHeaderValueCollection<string> Vary
		{
			get
			{
				return base.GetValues<string>("Vary");
			}
		}

		/// <summary>Gets the value of the <see langword="Via" /> header for an HTTP response.</summary>
		/// <returns>The value of the <see langword="Via" /> header for an HTTP response.</returns>
		// Token: 0x1700009E RID: 158
		// (get) Token: 0x0600024C RID: 588 RVA: 0x00009586 File Offset: 0x00007786
		public HttpHeaderValueCollection<ViaHeaderValue> Via
		{
			get
			{
				return base.GetValues<ViaHeaderValue>("Via");
			}
		}

		/// <summary>Gets the value of the <see langword="Warning" /> header for an HTTP response.</summary>
		/// <returns>The value of the <see langword="Warning" /> header for an HTTP response.</returns>
		// Token: 0x1700009F RID: 159
		// (get) Token: 0x0600024D RID: 589 RVA: 0x00009593 File Offset: 0x00007793
		public HttpHeaderValueCollection<WarningHeaderValue> Warning
		{
			get
			{
				return base.GetValues<WarningHeaderValue>("Warning");
			}
		}

		/// <summary>Gets the value of the <see langword="WWW-Authenticate" /> header for an HTTP response.</summary>
		/// <returns>The value of the <see langword="WWW-Authenticate" /> header for an HTTP response.</returns>
		// Token: 0x170000A0 RID: 160
		// (get) Token: 0x0600024E RID: 590 RVA: 0x00009919 File Offset: 0x00007B19
		public HttpHeaderValueCollection<AuthenticationHeaderValue> WwwAuthenticate
		{
			get
			{
				return base.GetValues<AuthenticationHeaderValue>("WWW-Authenticate");
			}
		}

		// Token: 0x0200003E RID: 62
		[CompilerGenerated]
		[Serializable]
		private sealed class <>c
		{
			// Token: 0x0600024F RID: 591 RVA: 0x00009926 File Offset: 0x00007B26
			// Note: this type is marked as 'beforefieldinit'.
			static <>c()
			{
			}

			// Token: 0x06000250 RID: 592 RVA: 0x000039D8 File Offset: 0x00001BD8
			public <>c()
			{
			}

			// Token: 0x06000251 RID: 593 RVA: 0x00009934 File Offset: 0x00007B34
			internal string <set_Age>b__5_0(object l)
			{
				return ((long)((TimeSpan)l).TotalSeconds).ToString();
			}

			// Token: 0x06000252 RID: 594 RVA: 0x00009604 File Offset: 0x00007804
			internal bool <get_ConnectionClose>b__12_0(string l)
			{
				return string.Equals(l, "close", StringComparison.OrdinalIgnoreCase);
			}

			// Token: 0x06000253 RID: 595 RVA: 0x00009958 File Offset: 0x00007B58
			internal bool <get_TransferEncodingChunked>b__37_0(TransferCodingHeaderValue l)
			{
				return StringComparer.OrdinalIgnoreCase.Equals(l.Value, "chunked");
			}

			// Token: 0x06000254 RID: 596 RVA: 0x00009658 File Offset: 0x00007858
			internal bool <set_TransferEncodingChunked>b__38_0(TransferCodingHeaderValue l)
			{
				return l.Value == "chunked";
			}

			// Token: 0x04000119 RID: 281
			public static readonly HttpResponseHeaders.<>c <>9 = new HttpResponseHeaders.<>c();

			// Token: 0x0400011A RID: 282
			public static Func<object, string> <>9__5_0;

			// Token: 0x0400011B RID: 283
			public static Predicate<string> <>9__12_0;

			// Token: 0x0400011C RID: 284
			public static Predicate<TransferCodingHeaderValue> <>9__37_0;

			// Token: 0x0400011D RID: 285
			public static Predicate<TransferCodingHeaderValue> <>9__38_0;
		}
	}
}
