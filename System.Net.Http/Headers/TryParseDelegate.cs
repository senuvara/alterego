﻿using System;

namespace System.Net.Http.Headers
{
	// Token: 0x02000030 RID: 48
	// (Invoke) Token: 0x0600018E RID: 398
	internal delegate bool TryParseDelegate<T>(string value, out T result);
}
