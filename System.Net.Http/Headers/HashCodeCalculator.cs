﻿using System;
using System.Collections.Generic;

namespace System.Net.Http.Headers
{
	// Token: 0x0200002F RID: 47
	internal static class HashCodeCalculator
	{
		// Token: 0x0600018C RID: 396 RVA: 0x00007C10 File Offset: 0x00005E10
		public static int Calculate<T>(ICollection<T> list)
		{
			if (list == null)
			{
				return 0;
			}
			int num = 17;
			foreach (T t in list)
			{
				num = num * 29 + t.GetHashCode();
			}
			return num;
		}
	}
}
