﻿using System;
using System.Runtime.CompilerServices;

namespace System.Net.Http.Headers
{
	/// <summary>Represents a byte range in a Range header value.</summary>
	// Token: 0x02000059 RID: 89
	public class RangeItemHeaderValue : ICloneable
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Net.Http.Headers.RangeItemHeaderValue" /> class.</summary>
		/// <param name="from">The position at which to start sending data.</param>
		/// <param name="to">The position at which to stop sending data.</param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="from" /> is greater than <paramref name="to" />-or- 
		///         <paramref name="from" /> or <paramref name="to" /> is less than 0. </exception>
		// Token: 0x06000301 RID: 769 RVA: 0x0000B7BC File Offset: 0x000099BC
		public RangeItemHeaderValue(long? from, long? to)
		{
			if (from == null && to == null)
			{
				throw new ArgumentException();
			}
			if (from != null && to != null && from > to)
			{
				throw new ArgumentOutOfRangeException("from");
			}
			if (from < 0L)
			{
				throw new ArgumentOutOfRangeException("from");
			}
			if (to < 0L)
			{
				throw new ArgumentOutOfRangeException("to");
			}
			this.From = from;
			this.To = to;
		}

		/// <summary>Gets the position at which to start sending data.</summary>
		/// <returns>The position at which to start sending data.</returns>
		// Token: 0x170000B4 RID: 180
		// (get) Token: 0x06000302 RID: 770 RVA: 0x0000B886 File Offset: 0x00009A86
		// (set) Token: 0x06000303 RID: 771 RVA: 0x0000B88E File Offset: 0x00009A8E
		public long? From
		{
			[CompilerGenerated]
			get
			{
				return this.<From>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<From>k__BackingField = value;
			}
		}

		/// <summary>Gets the position at which to stop sending data. </summary>
		/// <returns>The position at which to stop sending data. </returns>
		// Token: 0x170000B5 RID: 181
		// (get) Token: 0x06000304 RID: 772 RVA: 0x0000B897 File Offset: 0x00009A97
		// (set) Token: 0x06000305 RID: 773 RVA: 0x0000B89F File Offset: 0x00009A9F
		public long? To
		{
			[CompilerGenerated]
			get
			{
				return this.<To>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<To>k__BackingField = value;
			}
		}

		/// <summary>Creates a new object that is a copy of the current <see cref="T:System.Net.Http.Headers.RangeItemHeaderValue" /> instance.</summary>
		/// <returns>A copy of the current instance.</returns>
		// Token: 0x06000306 RID: 774 RVA: 0x00005BA5 File Offset: 0x00003DA5
		object ICloneable.Clone()
		{
			return base.MemberwiseClone();
		}

		/// <summary>Determines whether the specified <see cref="T:System.Object" /> is equal to the current <see cref="T:System.Net.Http.Headers.RangeItemHeaderValue" /> object.</summary>
		/// <param name="obj">The object to compare with the current object.</param>
		/// <returns>
		///     <see langword="true" /> if the specified <see cref="T:System.Object" /> is equal to the current object; otherwise, <see langword="false" />.</returns>
		// Token: 0x06000307 RID: 775 RVA: 0x0000B8A8 File Offset: 0x00009AA8
		public override bool Equals(object obj)
		{
			RangeItemHeaderValue rangeItemHeaderValue = obj as RangeItemHeaderValue;
			return rangeItemHeaderValue != null && rangeItemHeaderValue.From == this.From && rangeItemHeaderValue.To == this.To;
		}

		/// <summary>Serves as a hash function for an <see cref="T:System.Net.Http.Headers.RangeItemHeaderValue" /> object.</summary>
		/// <returns>A hash code for the current object.</returns>
		// Token: 0x06000308 RID: 776 RVA: 0x0000B924 File Offset: 0x00009B24
		public override int GetHashCode()
		{
			return this.From.GetHashCode() ^ this.To.GetHashCode();
		}

		/// <summary>Returns a string that represents the current <see cref="T:System.Net.Http.Headers.RangeItemHeaderValue" /> object.</summary>
		/// <returns>A string that represents the current object.</returns>
		// Token: 0x06000309 RID: 777 RVA: 0x0000B95C File Offset: 0x00009B5C
		public override string ToString()
		{
			if (this.From == null)
			{
				return "-" + this.To.Value;
			}
			if (this.To == null)
			{
				return this.From.Value + "-";
			}
			return this.From.Value + "-" + this.To.Value;
		}

		// Token: 0x04000147 RID: 327
		[CompilerGenerated]
		private long? <From>k__BackingField;

		// Token: 0x04000148 RID: 328
		[CompilerGenerated]
		private long? <To>k__BackingField;
	}
}
