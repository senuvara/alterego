﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Text;

namespace System.Net.Http.Headers
{
	/// <summary>Represents the value of the Cache-Control header.</summary>
	// Token: 0x02000028 RID: 40
	public class CacheControlHeaderValue : ICloneable
	{
		/// <summary>Cache-extension tokens, each with an optional assigned value.</summary>
		/// <returns>A collection of cache-extension tokens each with an optional assigned value.</returns>
		// Token: 0x17000036 RID: 54
		// (get) Token: 0x06000115 RID: 277 RVA: 0x00005D40 File Offset: 0x00003F40
		public ICollection<NameValueHeaderValue> Extensions
		{
			get
			{
				List<NameValueHeaderValue> result;
				if ((result = this.extensions) == null)
				{
					result = (this.extensions = new List<NameValueHeaderValue>());
				}
				return result;
			}
		}

		/// <summary>The maximum age, specified in seconds, that the HTTP client is willing to accept a response. </summary>
		/// <returns>The time in seconds. </returns>
		// Token: 0x17000037 RID: 55
		// (get) Token: 0x06000116 RID: 278 RVA: 0x00005D65 File Offset: 0x00003F65
		// (set) Token: 0x06000117 RID: 279 RVA: 0x00005D6D File Offset: 0x00003F6D
		public TimeSpan? MaxAge
		{
			[CompilerGenerated]
			get
			{
				return this.<MaxAge>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<MaxAge>k__BackingField = value;
			}
		}

		/// <summary>Whether an HTTP client is willing to accept a response that has exceeded its expiration time.</summary>
		/// <returns>
		///     <see langword="true" /> if the HTTP client is willing to accept a response that has exceed the expiration time; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000038 RID: 56
		// (get) Token: 0x06000118 RID: 280 RVA: 0x00005D76 File Offset: 0x00003F76
		// (set) Token: 0x06000119 RID: 281 RVA: 0x00005D7E File Offset: 0x00003F7E
		public bool MaxStale
		{
			[CompilerGenerated]
			get
			{
				return this.<MaxStale>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<MaxStale>k__BackingField = value;
			}
		}

		/// <summary>The maximum time, in seconds, an HTTP client is willing to accept a response that has exceeded its expiration time.</summary>
		/// <returns>The time in seconds.</returns>
		// Token: 0x17000039 RID: 57
		// (get) Token: 0x0600011A RID: 282 RVA: 0x00005D87 File Offset: 0x00003F87
		// (set) Token: 0x0600011B RID: 283 RVA: 0x00005D8F File Offset: 0x00003F8F
		public TimeSpan? MaxStaleLimit
		{
			[CompilerGenerated]
			get
			{
				return this.<MaxStaleLimit>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<MaxStaleLimit>k__BackingField = value;
			}
		}

		/// <summary>The freshness lifetime, in seconds, that an HTTP client is willing to accept a response.</summary>
		/// <returns>The time in seconds.</returns>
		// Token: 0x1700003A RID: 58
		// (get) Token: 0x0600011C RID: 284 RVA: 0x00005D98 File Offset: 0x00003F98
		// (set) Token: 0x0600011D RID: 285 RVA: 0x00005DA0 File Offset: 0x00003FA0
		public TimeSpan? MinFresh
		{
			[CompilerGenerated]
			get
			{
				return this.<MinFresh>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<MinFresh>k__BackingField = value;
			}
		}

		/// <summary>Whether the origin server require revalidation of a cache entry on any subsequent use when the cache entry becomes stale.</summary>
		/// <returns>
		///     <see langword="true" /> if the origin server requires revalidation of a cache entry on any subsequent use when the entry becomes stale; otherwise, <see langword="false" />.</returns>
		// Token: 0x1700003B RID: 59
		// (get) Token: 0x0600011E RID: 286 RVA: 0x00005DA9 File Offset: 0x00003FA9
		// (set) Token: 0x0600011F RID: 287 RVA: 0x00005DB1 File Offset: 0x00003FB1
		public bool MustRevalidate
		{
			[CompilerGenerated]
			get
			{
				return this.<MustRevalidate>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<MustRevalidate>k__BackingField = value;
			}
		}

		/// <summary>Whether an HTTP client is willing to accept a cached response.</summary>
		/// <returns>
		///     <see langword="true" /> if the HTTP client is willing to accept a cached response; otherwise, <see langword="false" />.</returns>
		// Token: 0x1700003C RID: 60
		// (get) Token: 0x06000120 RID: 288 RVA: 0x00005DBA File Offset: 0x00003FBA
		// (set) Token: 0x06000121 RID: 289 RVA: 0x00005DC2 File Offset: 0x00003FC2
		public bool NoCache
		{
			[CompilerGenerated]
			get
			{
				return this.<NoCache>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<NoCache>k__BackingField = value;
			}
		}

		/// <summary>A collection of fieldnames in the "no-cache" directive in a cache-control header field on an HTTP response.</summary>
		/// <returns>A collection of fieldnames.</returns>
		// Token: 0x1700003D RID: 61
		// (get) Token: 0x06000122 RID: 290 RVA: 0x00005DCC File Offset: 0x00003FCC
		public ICollection<string> NoCacheHeaders
		{
			get
			{
				List<string> result;
				if ((result = this.no_cache_headers) == null)
				{
					result = (this.no_cache_headers = new List<string>());
				}
				return result;
			}
		}

		/// <summary>Whether a cache must not store any part of either the HTTP request mressage or any response.</summary>
		/// <returns>
		///     <see langword="true" /> if a cache must not store any part of either the HTTP request mressage or any response; otherwise, <see langword="false" />.</returns>
		// Token: 0x1700003E RID: 62
		// (get) Token: 0x06000123 RID: 291 RVA: 0x00005DF1 File Offset: 0x00003FF1
		// (set) Token: 0x06000124 RID: 292 RVA: 0x00005DF9 File Offset: 0x00003FF9
		public bool NoStore
		{
			[CompilerGenerated]
			get
			{
				return this.<NoStore>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<NoStore>k__BackingField = value;
			}
		}

		/// <summary>Whether a cache or proxy must not change any aspect of the entity-body.</summary>
		/// <returns>
		///     <see langword="true" /> if a cache or proxy must not change any aspect of the entity-body; otherwise, <see langword="false" />.</returns>
		// Token: 0x1700003F RID: 63
		// (get) Token: 0x06000125 RID: 293 RVA: 0x00005E02 File Offset: 0x00004002
		// (set) Token: 0x06000126 RID: 294 RVA: 0x00005E0A File Offset: 0x0000400A
		public bool NoTransform
		{
			[CompilerGenerated]
			get
			{
				return this.<NoTransform>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<NoTransform>k__BackingField = value;
			}
		}

		/// <summary>Whether a cache should either respond using a cached entry that is consistent with the other constraints of the HTTP request, or respond with a 504 (Gateway Timeout) status.</summary>
		/// <returns>
		///     <see langword="true" /> if a cache should either respond using a cached entry that is consistent with the other constraints of the HTTP request, or respond with a 504 (Gateway Timeout) status; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000040 RID: 64
		// (get) Token: 0x06000127 RID: 295 RVA: 0x00005E13 File Offset: 0x00004013
		// (set) Token: 0x06000128 RID: 296 RVA: 0x00005E1B File Offset: 0x0000401B
		public bool OnlyIfCached
		{
			[CompilerGenerated]
			get
			{
				return this.<OnlyIfCached>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<OnlyIfCached>k__BackingField = value;
			}
		}

		/// <summary>Whether all or part of the HTTP response message is intended for a single user and must not be cached by a shared cache.</summary>
		/// <returns>
		///     <see langword="true" /> if the HTTP response message is intended for a single user and must not be cached by a shared cache; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000041 RID: 65
		// (get) Token: 0x06000129 RID: 297 RVA: 0x00005E24 File Offset: 0x00004024
		// (set) Token: 0x0600012A RID: 298 RVA: 0x00005E2C File Offset: 0x0000402C
		public bool Private
		{
			[CompilerGenerated]
			get
			{
				return this.<Private>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<Private>k__BackingField = value;
			}
		}

		/// <summary>A collection fieldnames in the "private" directive in a cache-control header field on an HTTP response.</summary>
		/// <returns>A collection of fieldnames.</returns>
		// Token: 0x17000042 RID: 66
		// (get) Token: 0x0600012B RID: 299 RVA: 0x00005E38 File Offset: 0x00004038
		public ICollection<string> PrivateHeaders
		{
			get
			{
				List<string> result;
				if ((result = this.private_headers) == null)
				{
					result = (this.private_headers = new List<string>());
				}
				return result;
			}
		}

		/// <summary>Whether the origin server require revalidation of a cache entry on any subsequent use when the cache entry becomes stale for shared user agent caches.</summary>
		/// <returns>
		///     <see langword="true" /> if the origin server requires revalidation of a cache entry on any subsequent use when the entry becomes stale for shared user agent caches; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000043 RID: 67
		// (get) Token: 0x0600012C RID: 300 RVA: 0x00005E5D File Offset: 0x0000405D
		// (set) Token: 0x0600012D RID: 301 RVA: 0x00005E65 File Offset: 0x00004065
		public bool ProxyRevalidate
		{
			[CompilerGenerated]
			get
			{
				return this.<ProxyRevalidate>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<ProxyRevalidate>k__BackingField = value;
			}
		}

		/// <summary>Whether an HTTP response may be cached by any cache, even if it would normally be non-cacheable or cacheable only within a non- shared cache.</summary>
		/// <returns>
		///     <see langword="true" /> if the HTTP response may be cached by any cache, even if it would normally be non-cacheable or cacheable only within a non- shared cache; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000044 RID: 68
		// (get) Token: 0x0600012E RID: 302 RVA: 0x00005E6E File Offset: 0x0000406E
		// (set) Token: 0x0600012F RID: 303 RVA: 0x00005E76 File Offset: 0x00004076
		public bool Public
		{
			[CompilerGenerated]
			get
			{
				return this.<Public>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<Public>k__BackingField = value;
			}
		}

		/// <summary>The shared maximum age, specified in seconds, in an HTTP response that overrides the "max-age" directive in a cache-control header or an Expires header for a shared cache.</summary>
		/// <returns>The time in seconds.</returns>
		// Token: 0x17000045 RID: 69
		// (get) Token: 0x06000130 RID: 304 RVA: 0x00005E7F File Offset: 0x0000407F
		// (set) Token: 0x06000131 RID: 305 RVA: 0x00005E87 File Offset: 0x00004087
		public TimeSpan? SharedMaxAge
		{
			[CompilerGenerated]
			get
			{
				return this.<SharedMaxAge>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<SharedMaxAge>k__BackingField = value;
			}
		}

		/// <summary>Creates a new object that is a copy of the current <see cref="T:System.Net.Http.Headers.CacheControlHeaderValue" /> instance.</summary>
		/// <returns>A copy of the current instance.</returns>
		// Token: 0x06000132 RID: 306 RVA: 0x00005E90 File Offset: 0x00004090
		object ICloneable.Clone()
		{
			CacheControlHeaderValue cacheControlHeaderValue = (CacheControlHeaderValue)base.MemberwiseClone();
			if (this.extensions != null)
			{
				cacheControlHeaderValue.extensions = new List<NameValueHeaderValue>();
				foreach (NameValueHeaderValue item in this.extensions)
				{
					cacheControlHeaderValue.extensions.Add(item);
				}
			}
			if (this.no_cache_headers != null)
			{
				cacheControlHeaderValue.no_cache_headers = new List<string>();
				foreach (string item2 in this.no_cache_headers)
				{
					cacheControlHeaderValue.no_cache_headers.Add(item2);
				}
			}
			if (this.private_headers != null)
			{
				cacheControlHeaderValue.private_headers = new List<string>();
				foreach (string item3 in this.private_headers)
				{
					cacheControlHeaderValue.private_headers.Add(item3);
				}
			}
			return cacheControlHeaderValue;
		}

		/// <summary>Determines whether the specified <see cref="T:System.Object" /> is equal to the current <see cref="T:System.Net.Http.Headers.CacheControlHeaderValue" /> object.</summary>
		/// <param name="obj">The object to compare with the current object.</param>
		/// <returns>
		///     <see langword="true" /> if the specified <see cref="T:System.Object" /> is equal to the current object; otherwise, <see langword="false" />.</returns>
		// Token: 0x06000133 RID: 307 RVA: 0x00005FC0 File Offset: 0x000041C0
		public override bool Equals(object obj)
		{
			CacheControlHeaderValue cacheControlHeaderValue = obj as CacheControlHeaderValue;
			return cacheControlHeaderValue != null && (!(this.MaxAge != cacheControlHeaderValue.MaxAge) && this.MaxStale == cacheControlHeaderValue.MaxStale) && !(this.MaxStaleLimit != cacheControlHeaderValue.MaxStaleLimit) && (!(this.MinFresh != cacheControlHeaderValue.MinFresh) && this.MustRevalidate == cacheControlHeaderValue.MustRevalidate && this.NoCache == cacheControlHeaderValue.NoCache && this.NoStore == cacheControlHeaderValue.NoStore && this.NoTransform == cacheControlHeaderValue.NoTransform && this.OnlyIfCached == cacheControlHeaderValue.OnlyIfCached && this.Private == cacheControlHeaderValue.Private && this.ProxyRevalidate == cacheControlHeaderValue.ProxyRevalidate && this.Public == cacheControlHeaderValue.Public) && !(this.SharedMaxAge != cacheControlHeaderValue.SharedMaxAge) && (this.extensions.SequenceEqual(cacheControlHeaderValue.extensions) && this.no_cache_headers.SequenceEqual(cacheControlHeaderValue.no_cache_headers)) && this.private_headers.SequenceEqual(cacheControlHeaderValue.private_headers);
		}

		/// <summary>Serves as a hash function for a  <see cref="T:System.Net.Http.Headers.CacheControlHeaderValue" /> object.</summary>
		/// <returns>A hash code for the current object.</returns>
		// Token: 0x06000134 RID: 308 RVA: 0x000061B0 File Offset: 0x000043B0
		public override int GetHashCode()
		{
			return (((((((((((((((29 * 29 + HashCodeCalculator.Calculate<NameValueHeaderValue>(this.extensions)) * 29 + this.MaxAge.GetHashCode()) * 29 + this.MaxStale.GetHashCode()) * 29 + this.MaxStaleLimit.GetHashCode()) * 29 + this.MinFresh.GetHashCode()) * 29 + this.MustRevalidate.GetHashCode()) * 29 + HashCodeCalculator.Calculate<string>(this.no_cache_headers)) * 29 + this.NoCache.GetHashCode()) * 29 + this.NoStore.GetHashCode()) * 29 + this.NoTransform.GetHashCode()) * 29 + this.OnlyIfCached.GetHashCode()) * 29 + this.Private.GetHashCode()) * 29 + HashCodeCalculator.Calculate<string>(this.private_headers)) * 29 + this.ProxyRevalidate.GetHashCode()) * 29 + this.Public.GetHashCode()) * 29 + this.SharedMaxAge.GetHashCode();
		}

		/// <summary>Converts a string to an <see cref="T:System.Net.Http.Headers.CacheControlHeaderValue" /> instance.</summary>
		/// <param name="input">A string that represents cache-control header value information.</param>
		/// <returns>A <see cref="T:System.Net.Http.Headers.CacheControlHeaderValue" /> instance.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="input" /> is a <see langword="null" /> reference.</exception>
		/// <exception cref="T:System.FormatException">
		///         <paramref name="input" /> is not valid cache-control header value information.</exception>
		// Token: 0x06000135 RID: 309 RVA: 0x000062F0 File Offset: 0x000044F0
		public static CacheControlHeaderValue Parse(string input)
		{
			CacheControlHeaderValue result;
			if (CacheControlHeaderValue.TryParse(input, out result))
			{
				return result;
			}
			throw new FormatException(input);
		}

		/// <summary>Determines whether a string is valid <see cref="T:System.Net.Http.Headers.CacheControlHeaderValue" /> information.</summary>
		/// <param name="input">The string to validate.</param>
		/// <param name="parsedValue">The <see cref="T:System.Net.Http.Headers.CacheControlHeaderValue" /> version of the string.</param>
		/// <returns>
		///     <see langword="true" /> if <paramref name="input" /> is valid <see cref="T:System.Net.Http.Headers.CacheControlHeaderValue" /> information; otherwise, <see langword="false" />.</returns>
		// Token: 0x06000136 RID: 310 RVA: 0x00006310 File Offset: 0x00004510
		public static bool TryParse(string input, out CacheControlHeaderValue parsedValue)
		{
			parsedValue = null;
			if (input == null)
			{
				return true;
			}
			CacheControlHeaderValue cacheControlHeaderValue = new CacheControlHeaderValue();
			Lexer lexer = new Lexer(input);
			Token token;
			for (;;)
			{
				token = lexer.Scan(false);
				if (token != Token.Type.Token)
				{
					break;
				}
				string stringValue = lexer.GetStringValue(token);
				bool flag = false;
				uint num = <PrivateImplementationDetails>.ComputeStringHash(stringValue);
				if (num <= 1922561311U)
				{
					TimeSpan? timeSpan;
					if (num <= 719568158U)
					{
						if (num != 129047354U)
						{
							if (num != 412259456U)
							{
								if (num != 719568158U)
								{
									goto IL_3B9;
								}
								if (!(stringValue == "no-store"))
								{
									goto IL_3B9;
								}
								cacheControlHeaderValue.NoStore = true;
								goto IL_412;
							}
							else if (!(stringValue == "s-maxage"))
							{
								goto IL_3B9;
							}
						}
						else if (!(stringValue == "min-fresh"))
						{
							goto IL_3B9;
						}
					}
					else if (num != 962188105U)
					{
						if (num != 1657474316U)
						{
							if (num != 1922561311U)
							{
								goto IL_3B9;
							}
							if (!(stringValue == "max-age"))
							{
								goto IL_3B9;
							}
						}
						else
						{
							if (!(stringValue == "private"))
							{
								goto IL_3B9;
							}
							goto IL_2FE;
						}
					}
					else
					{
						if (!(stringValue == "max-stale"))
						{
							goto IL_3B9;
						}
						cacheControlHeaderValue.MaxStale = true;
						token = lexer.Scan(false);
						if (token != Token.Type.SeparatorEqual)
						{
							flag = true;
							goto IL_412;
						}
						token = lexer.Scan(false);
						if (token != Token.Type.Token)
						{
							return false;
						}
						timeSpan = lexer.TryGetTimeSpanValue(token);
						if (timeSpan == null)
						{
							return false;
						}
						cacheControlHeaderValue.MaxStaleLimit = timeSpan;
						goto IL_412;
					}
					token = lexer.Scan(false);
					if (token != Token.Type.SeparatorEqual)
					{
						return false;
					}
					token = lexer.Scan(false);
					if (token != Token.Type.Token)
					{
						return false;
					}
					timeSpan = lexer.TryGetTimeSpanValue(token);
					if (timeSpan == null)
					{
						return false;
					}
					int i = stringValue.Length;
					if (i != 7)
					{
						if (i != 8)
						{
							cacheControlHeaderValue.MinFresh = timeSpan;
						}
						else
						{
							cacheControlHeaderValue.SharedMaxAge = timeSpan;
						}
					}
					else
					{
						cacheControlHeaderValue.MaxAge = timeSpan;
					}
				}
				else if (num <= 2802093227U)
				{
					if (num != 2033558065U)
					{
						if (num != 2154495528U)
						{
							if (num != 2802093227U)
							{
								goto IL_3B9;
							}
							if (!(stringValue == "no-transform"))
							{
								goto IL_3B9;
							}
							cacheControlHeaderValue.NoTransform = true;
						}
						else
						{
							if (!(stringValue == "must-revalidate"))
							{
								goto IL_3B9;
							}
							cacheControlHeaderValue.MustRevalidate = true;
						}
					}
					else
					{
						if (!(stringValue == "proxy-revalidate"))
						{
							goto IL_3B9;
						}
						cacheControlHeaderValue.ProxyRevalidate = true;
					}
				}
				else if (num != 2866772502U)
				{
					if (num != 3432027008U)
					{
						if (num != 3443516981U)
						{
							goto IL_3B9;
						}
						if (!(stringValue == "no-cache"))
						{
							goto IL_3B9;
						}
						goto IL_2FE;
					}
					else
					{
						if (!(stringValue == "public"))
						{
							goto IL_3B9;
						}
						cacheControlHeaderValue.Public = true;
					}
				}
				else
				{
					if (!(stringValue == "only-if-cached"))
					{
						goto IL_3B9;
					}
					cacheControlHeaderValue.OnlyIfCached = true;
				}
				IL_412:
				if (!flag)
				{
					token = lexer.Scan(false);
				}
				if (token != Token.Type.SeparatorComma)
				{
					goto Block_46;
				}
				continue;
				IL_2FE:
				if (stringValue.Length == 7)
				{
					cacheControlHeaderValue.Private = true;
				}
				else
				{
					cacheControlHeaderValue.NoCache = true;
				}
				token = lexer.Scan(false);
				if (token != Token.Type.SeparatorEqual)
				{
					flag = true;
					goto IL_412;
				}
				token = lexer.Scan(false);
				if (token != Token.Type.QuotedString)
				{
					return false;
				}
				string[] array = lexer.GetQuotedStringValue(token).Split(new char[]
				{
					','
				});
				for (int i = 0; i < array.Length; i++)
				{
					string item = array[i].Trim(new char[]
					{
						'\t',
						' '
					});
					if (stringValue.Length == 7)
					{
						cacheControlHeaderValue.PrivateHeaders.Add(item);
					}
					else
					{
						cacheControlHeaderValue.NoCache = true;
						cacheControlHeaderValue.NoCacheHeaders.Add(item);
					}
				}
				goto IL_412;
				IL_3B9:
				string stringValue2 = lexer.GetStringValue(token);
				string value = null;
				token = lexer.Scan(false);
				if (token == Token.Type.SeparatorEqual)
				{
					token = lexer.Scan(false);
					Token.Type kind = token.Kind;
					if (kind - Token.Type.Token > 1)
					{
						return false;
					}
					value = lexer.GetStringValue(token);
				}
				else
				{
					flag = true;
				}
				cacheControlHeaderValue.Extensions.Add(NameValueHeaderValue.Create(stringValue2, value));
				goto IL_412;
			}
			return false;
			Block_46:
			if (token != Token.Type.End)
			{
				return false;
			}
			parsedValue = cacheControlHeaderValue;
			return true;
		}

		/// <summary>Returns a string that represents the current <see cref="T:System.Net.Http.Headers.CacheControlHeaderValue" /> object.</summary>
		/// <returns>A string that represents the current object.</returns>
		// Token: 0x06000137 RID: 311 RVA: 0x00006758 File Offset: 0x00004958
		public override string ToString()
		{
			StringBuilder stringBuilder = new StringBuilder();
			if (this.NoStore)
			{
				stringBuilder.Append("no-store");
				stringBuilder.Append(", ");
			}
			if (this.NoTransform)
			{
				stringBuilder.Append("no-transform");
				stringBuilder.Append(", ");
			}
			if (this.OnlyIfCached)
			{
				stringBuilder.Append("only-if-cached");
				stringBuilder.Append(", ");
			}
			if (this.Public)
			{
				stringBuilder.Append("public");
				stringBuilder.Append(", ");
			}
			if (this.MustRevalidate)
			{
				stringBuilder.Append("must-revalidate");
				stringBuilder.Append(", ");
			}
			if (this.ProxyRevalidate)
			{
				stringBuilder.Append("proxy-revalidate");
				stringBuilder.Append(", ");
			}
			if (this.NoCache)
			{
				stringBuilder.Append("no-cache");
				if (this.no_cache_headers != null)
				{
					stringBuilder.Append("=\"");
					this.no_cache_headers.ToStringBuilder(stringBuilder);
					stringBuilder.Append("\"");
				}
				stringBuilder.Append(", ");
			}
			if (this.MaxAge != null)
			{
				stringBuilder.Append("max-age=");
				stringBuilder.Append(this.MaxAge.Value.TotalSeconds.ToString(CultureInfo.InvariantCulture));
				stringBuilder.Append(", ");
			}
			if (this.SharedMaxAge != null)
			{
				stringBuilder.Append("s-maxage=");
				stringBuilder.Append(this.SharedMaxAge.Value.TotalSeconds.ToString(CultureInfo.InvariantCulture));
				stringBuilder.Append(", ");
			}
			if (this.MaxStale)
			{
				stringBuilder.Append("max-stale");
				if (this.MaxStaleLimit != null)
				{
					stringBuilder.Append("=");
					stringBuilder.Append(this.MaxStaleLimit.Value.TotalSeconds.ToString(CultureInfo.InvariantCulture));
				}
				stringBuilder.Append(", ");
			}
			if (this.MinFresh != null)
			{
				stringBuilder.Append("min-fresh=");
				stringBuilder.Append(this.MinFresh.Value.TotalSeconds.ToString(CultureInfo.InvariantCulture));
				stringBuilder.Append(", ");
			}
			if (this.Private)
			{
				stringBuilder.Append("private");
				if (this.private_headers != null)
				{
					stringBuilder.Append("=\"");
					this.private_headers.ToStringBuilder(stringBuilder);
					stringBuilder.Append("\"");
				}
				stringBuilder.Append(", ");
			}
			this.extensions.ToStringBuilder(stringBuilder);
			if (stringBuilder.Length > 2 && stringBuilder[stringBuilder.Length - 2] == ',' && stringBuilder[stringBuilder.Length - 1] == ' ')
			{
				stringBuilder.Remove(stringBuilder.Length - 2, 2);
			}
			return stringBuilder.ToString();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Net.Http.Headers.CacheControlHeaderValue" /> class.</summary>
		// Token: 0x06000138 RID: 312 RVA: 0x000039D8 File Offset: 0x00001BD8
		public CacheControlHeaderValue()
		{
		}

		// Token: 0x040000DA RID: 218
		private List<NameValueHeaderValue> extensions;

		// Token: 0x040000DB RID: 219
		private List<string> no_cache_headers;

		// Token: 0x040000DC RID: 220
		private List<string> private_headers;

		// Token: 0x040000DD RID: 221
		[CompilerGenerated]
		private TimeSpan? <MaxAge>k__BackingField;

		// Token: 0x040000DE RID: 222
		[CompilerGenerated]
		private bool <MaxStale>k__BackingField;

		// Token: 0x040000DF RID: 223
		[CompilerGenerated]
		private TimeSpan? <MaxStaleLimit>k__BackingField;

		// Token: 0x040000E0 RID: 224
		[CompilerGenerated]
		private TimeSpan? <MinFresh>k__BackingField;

		// Token: 0x040000E1 RID: 225
		[CompilerGenerated]
		private bool <MustRevalidate>k__BackingField;

		// Token: 0x040000E2 RID: 226
		[CompilerGenerated]
		private bool <NoCache>k__BackingField;

		// Token: 0x040000E3 RID: 227
		[CompilerGenerated]
		private bool <NoStore>k__BackingField;

		// Token: 0x040000E4 RID: 228
		[CompilerGenerated]
		private bool <NoTransform>k__BackingField;

		// Token: 0x040000E5 RID: 229
		[CompilerGenerated]
		private bool <OnlyIfCached>k__BackingField;

		// Token: 0x040000E6 RID: 230
		[CompilerGenerated]
		private bool <Private>k__BackingField;

		// Token: 0x040000E7 RID: 231
		[CompilerGenerated]
		private bool <ProxyRevalidate>k__BackingField;

		// Token: 0x040000E8 RID: 232
		[CompilerGenerated]
		private bool <Public>k__BackingField;

		// Token: 0x040000E9 RID: 233
		[CompilerGenerated]
		private TimeSpan? <SharedMaxAge>k__BackingField;
	}
}
