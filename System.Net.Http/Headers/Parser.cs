﻿using System;
using System.Globalization;
using System.Net.Mail;
using System.Runtime.CompilerServices;

namespace System.Net.Http.Headers
{
	// Token: 0x02000047 RID: 71
	internal static class Parser
	{
		// Token: 0x02000048 RID: 72
		public static class Token
		{
			// Token: 0x060002AE RID: 686 RVA: 0x0000A9A6 File Offset: 0x00008BA6
			public static bool TryParse(string input, out string result)
			{
				if (input != null && Lexer.IsValidToken(input))
				{
					result = input;
					return true;
				}
				result = null;
				return false;
			}

			// Token: 0x060002AF RID: 687 RVA: 0x0000A9BC File Offset: 0x00008BBC
			public static void Check(string s)
			{
				if (s == null)
				{
					throw new ArgumentNullException();
				}
				if (Lexer.IsValidToken(s))
				{
					return;
				}
				if (s.Length == 0)
				{
					throw new ArgumentException();
				}
				throw new FormatException(s);
			}

			// Token: 0x060002B0 RID: 688 RVA: 0x0000A9E4 File Offset: 0x00008BE4
			public static bool TryCheck(string s)
			{
				return s != null && Lexer.IsValidToken(s);
			}

			// Token: 0x060002B1 RID: 689 RVA: 0x0000A9F4 File Offset: 0x00008BF4
			public static void CheckQuotedString(string s)
			{
				if (s == null)
				{
					throw new ArgumentNullException();
				}
				Lexer lexer = new Lexer(s);
				if (lexer.Scan(false) == System.Net.Http.Headers.Token.Type.QuotedString && lexer.Scan(false) == System.Net.Http.Headers.Token.Type.End)
				{
					return;
				}
				if (s.Length == 0)
				{
					throw new ArgumentException();
				}
				throw new FormatException(s);
			}

			// Token: 0x060002B2 RID: 690 RVA: 0x0000AA44 File Offset: 0x00008C44
			public static void CheckComment(string s)
			{
				if (s == null)
				{
					throw new ArgumentNullException();
				}
				string text;
				if (new Lexer(s).ScanCommentOptional(out text))
				{
					return;
				}
				if (s.Length == 0)
				{
					throw new ArgumentException();
				}
				throw new FormatException(s);
			}
		}

		// Token: 0x02000049 RID: 73
		public static class DateTime
		{
			// Token: 0x060002B3 RID: 691 RVA: 0x0000AA7E File Offset: 0x00008C7E
			public static bool TryParse(string input, out DateTimeOffset result)
			{
				return Lexer.TryGetDateValue(input, out result);
			}

			// Token: 0x060002B4 RID: 692 RVA: 0x0000AA87 File Offset: 0x00008C87
			// Note: this type is marked as 'beforefieldinit'.
			static DateTime()
			{
			}

			// Token: 0x04000139 RID: 313
			public new static readonly Func<object, string> ToString = (object l) => ((DateTimeOffset)l).ToString("r", CultureInfo.InvariantCulture);

			// Token: 0x0200004A RID: 74
			[CompilerGenerated]
			[Serializable]
			private sealed class <>c
			{
				// Token: 0x060002B5 RID: 693 RVA: 0x0000AA9E File Offset: 0x00008C9E
				// Note: this type is marked as 'beforefieldinit'.
				static <>c()
				{
				}

				// Token: 0x060002B6 RID: 694 RVA: 0x000039D8 File Offset: 0x00001BD8
				public <>c()
				{
				}

				// Token: 0x060002B7 RID: 695 RVA: 0x0000AAAC File Offset: 0x00008CAC
				internal string <.cctor>b__2_0(object l)
				{
					return ((DateTimeOffset)l).ToString("r", CultureInfo.InvariantCulture);
				}

				// Token: 0x0400013A RID: 314
				public static readonly Parser.DateTime.<>c <>9 = new Parser.DateTime.<>c();
			}
		}

		// Token: 0x0200004B RID: 75
		public static class EmailAddress
		{
			// Token: 0x060002B8 RID: 696 RVA: 0x0000AAD4 File Offset: 0x00008CD4
			public static bool TryParse(string input, out string result)
			{
				bool result2;
				try
				{
					new MailAddress(input);
					result = input;
					result2 = true;
				}
				catch
				{
					result = null;
					result2 = false;
				}
				return result2;
			}
		}

		// Token: 0x0200004C RID: 76
		public static class Host
		{
			// Token: 0x060002B9 RID: 697 RVA: 0x0000AB08 File Offset: 0x00008D08
			public static bool TryParse(string input, out string result)
			{
				result = input;
				System.Uri uri;
				return System.Uri.TryCreate("http://u@" + input + "/", UriKind.Absolute, out uri);
			}
		}

		// Token: 0x0200004D RID: 77
		public static class Int
		{
			// Token: 0x060002BA RID: 698 RVA: 0x0000AB30 File Offset: 0x00008D30
			public static bool TryParse(string input, out int result)
			{
				return int.TryParse(input, NumberStyles.None, CultureInfo.InvariantCulture, out result);
			}
		}

		// Token: 0x0200004E RID: 78
		public static class Long
		{
			// Token: 0x060002BB RID: 699 RVA: 0x0000AB3F File Offset: 0x00008D3F
			public static bool TryParse(string input, out long result)
			{
				return long.TryParse(input, NumberStyles.None, CultureInfo.InvariantCulture, out result);
			}
		}

		// Token: 0x0200004F RID: 79
		public static class MD5
		{
			// Token: 0x060002BC RID: 700 RVA: 0x0000AB50 File Offset: 0x00008D50
			public static bool TryParse(string input, out byte[] result)
			{
				bool result2;
				try
				{
					result = Convert.FromBase64String(input);
					result2 = true;
				}
				catch
				{
					result = null;
					result2 = false;
				}
				return result2;
			}

			// Token: 0x060002BD RID: 701 RVA: 0x0000AB84 File Offset: 0x00008D84
			// Note: this type is marked as 'beforefieldinit'.
			static MD5()
			{
			}

			// Token: 0x0400013B RID: 315
			public new static readonly Func<object, string> ToString = (object l) => Convert.ToBase64String((byte[])l);

			// Token: 0x02000050 RID: 80
			[CompilerGenerated]
			[Serializable]
			private sealed class <>c
			{
				// Token: 0x060002BE RID: 702 RVA: 0x0000AB9B File Offset: 0x00008D9B
				// Note: this type is marked as 'beforefieldinit'.
				static <>c()
				{
				}

				// Token: 0x060002BF RID: 703 RVA: 0x000039D8 File Offset: 0x00001BD8
				public <>c()
				{
				}

				// Token: 0x060002C0 RID: 704 RVA: 0x0000ABA7 File Offset: 0x00008DA7
				internal string <.cctor>b__2_0(object l)
				{
					return Convert.ToBase64String((byte[])l);
				}

				// Token: 0x0400013C RID: 316
				public static readonly Parser.MD5.<>c <>9 = new Parser.MD5.<>c();
			}
		}

		// Token: 0x02000051 RID: 81
		public static class TimeSpanSeconds
		{
			// Token: 0x060002C1 RID: 705 RVA: 0x0000ABB4 File Offset: 0x00008DB4
			public static bool TryParse(string input, out TimeSpan result)
			{
				int num;
				if (Parser.Int.TryParse(input, out num))
				{
					result = TimeSpan.FromSeconds((double)num);
					return true;
				}
				result = TimeSpan.Zero;
				return false;
			}
		}

		// Token: 0x02000052 RID: 82
		public static class Uri
		{
			// Token: 0x060002C2 RID: 706 RVA: 0x0000ABE6 File Offset: 0x00008DE6
			public static bool TryParse(string input, out System.Uri result)
			{
				return System.Uri.TryCreate(input, UriKind.RelativeOrAbsolute, out result);
			}

			// Token: 0x060002C3 RID: 707 RVA: 0x0000ABF0 File Offset: 0x00008DF0
			public static void Check(string s)
			{
				if (s == null)
				{
					throw new ArgumentNullException();
				}
				System.Uri uri;
				if (Parser.Uri.TryParse(s, out uri))
				{
					return;
				}
				if (s.Length == 0)
				{
					throw new ArgumentException();
				}
				throw new FormatException(s);
			}
		}
	}
}
