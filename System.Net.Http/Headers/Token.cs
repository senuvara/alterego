﻿using System;
using System.Runtime.CompilerServices;

namespace System.Net.Http.Headers
{
	// Token: 0x0200003F RID: 63
	internal struct Token
	{
		// Token: 0x06000255 RID: 597 RVA: 0x0000996F File Offset: 0x00007B6F
		public Token(Token.Type type, int startPosition, int endPosition)
		{
			this = default(Token);
			this.type = type;
			this.StartPosition = startPosition;
			this.EndPosition = endPosition;
		}

		// Token: 0x170000A1 RID: 161
		// (get) Token: 0x06000256 RID: 598 RVA: 0x0000998D File Offset: 0x00007B8D
		// (set) Token: 0x06000257 RID: 599 RVA: 0x00009995 File Offset: 0x00007B95
		public int StartPosition
		{
			[CompilerGenerated]
			get
			{
				return this.<StartPosition>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<StartPosition>k__BackingField = value;
			}
		}

		// Token: 0x170000A2 RID: 162
		// (get) Token: 0x06000258 RID: 600 RVA: 0x0000999E File Offset: 0x00007B9E
		// (set) Token: 0x06000259 RID: 601 RVA: 0x000099A6 File Offset: 0x00007BA6
		public int EndPosition
		{
			[CompilerGenerated]
			get
			{
				return this.<EndPosition>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<EndPosition>k__BackingField = value;
			}
		}

		// Token: 0x170000A3 RID: 163
		// (get) Token: 0x0600025A RID: 602 RVA: 0x000099AF File Offset: 0x00007BAF
		public Token.Type Kind
		{
			get
			{
				return this.type;
			}
		}

		// Token: 0x0600025B RID: 603 RVA: 0x000099AF File Offset: 0x00007BAF
		public static implicit operator Token.Type(Token token)
		{
			return token.type;
		}

		// Token: 0x0600025C RID: 604 RVA: 0x000099B8 File Offset: 0x00007BB8
		public override string ToString()
		{
			return this.type.ToString();
		}

		// Token: 0x0600025D RID: 605 RVA: 0x000099D9 File Offset: 0x00007BD9
		// Note: this type is marked as 'beforefieldinit'.
		static Token()
		{
		}

		// Token: 0x0400011E RID: 286
		public static readonly Token Empty = new Token(Token.Type.Token, 0, 0);

		// Token: 0x0400011F RID: 287
		private readonly Token.Type type;

		// Token: 0x04000120 RID: 288
		[CompilerGenerated]
		private int <StartPosition>k__BackingField;

		// Token: 0x04000121 RID: 289
		[CompilerGenerated]
		private int <EndPosition>k__BackingField;

		// Token: 0x02000040 RID: 64
		public enum Type
		{
			// Token: 0x04000123 RID: 291
			Error,
			// Token: 0x04000124 RID: 292
			End,
			// Token: 0x04000125 RID: 293
			Token,
			// Token: 0x04000126 RID: 294
			QuotedString,
			// Token: 0x04000127 RID: 295
			SeparatorEqual,
			// Token: 0x04000128 RID: 296
			SeparatorSemicolon,
			// Token: 0x04000129 RID: 297
			SeparatorSlash,
			// Token: 0x0400012A RID: 298
			SeparatorDash,
			// Token: 0x0400012B RID: 299
			SeparatorComma,
			// Token: 0x0400012C RID: 300
			OpenParens
		}
	}
}
