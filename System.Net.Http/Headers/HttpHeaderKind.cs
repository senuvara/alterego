﻿using System;

namespace System.Net.Http.Headers
{
	// Token: 0x02000036 RID: 54
	[Flags]
	internal enum HttpHeaderKind
	{
		// Token: 0x040000FD RID: 253
		None = 0,
		// Token: 0x040000FE RID: 254
		Request = 1,
		// Token: 0x040000FF RID: 255
		Response = 2,
		// Token: 0x04000100 RID: 256
		Content = 4
	}
}
