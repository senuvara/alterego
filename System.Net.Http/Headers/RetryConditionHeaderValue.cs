﻿using System;
using System.Globalization;
using System.Runtime.CompilerServices;

namespace System.Net.Http.Headers
{
	/// <summary>Represents a Retry-After header value which can either be a date/time or a timespan value.</summary>
	// Token: 0x0200005A RID: 90
	public class RetryConditionHeaderValue : ICloneable
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Net.Http.Headers.RetryConditionHeaderValue" /> class.</summary>
		/// <param name="date">The date and time offset used to initialize the new instance.</param>
		// Token: 0x0600030A RID: 778 RVA: 0x0000B9F5 File Offset: 0x00009BF5
		public RetryConditionHeaderValue(DateTimeOffset date)
		{
			this.Date = new DateTimeOffset?(date);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Net.Http.Headers.RetryConditionHeaderValue" /> class.</summary>
		/// <param name="delta">The delta, in seconds, used to initialize the new instance.</param>
		// Token: 0x0600030B RID: 779 RVA: 0x0000BA09 File Offset: 0x00009C09
		public RetryConditionHeaderValue(TimeSpan delta)
		{
			if (delta.TotalSeconds > 4294967295.0)
			{
				throw new ArgumentOutOfRangeException("delta");
			}
			this.Delta = new TimeSpan?(delta);
		}

		/// <summary>Gets the date and time offset from the <see cref="T:System.Net.Http.Headers.RetryConditionHeaderValue" /> object.</summary>
		/// <returns>The date and time offset from the <see cref="T:System.Net.Http.Headers.RetryConditionHeaderValue" /> object.</returns>
		// Token: 0x170000B6 RID: 182
		// (get) Token: 0x0600030C RID: 780 RVA: 0x0000BA3A File Offset: 0x00009C3A
		// (set) Token: 0x0600030D RID: 781 RVA: 0x0000BA42 File Offset: 0x00009C42
		public DateTimeOffset? Date
		{
			[CompilerGenerated]
			get
			{
				return this.<Date>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<Date>k__BackingField = value;
			}
		}

		/// <summary>Gets the delta in seconds from the <see cref="T:System.Net.Http.Headers.RetryConditionHeaderValue" /> object.</summary>
		/// <returns>The delta in seconds from the <see cref="T:System.Net.Http.Headers.RetryConditionHeaderValue" /> object.</returns>
		// Token: 0x170000B7 RID: 183
		// (get) Token: 0x0600030E RID: 782 RVA: 0x0000BA4B File Offset: 0x00009C4B
		// (set) Token: 0x0600030F RID: 783 RVA: 0x0000BA53 File Offset: 0x00009C53
		public TimeSpan? Delta
		{
			[CompilerGenerated]
			get
			{
				return this.<Delta>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<Delta>k__BackingField = value;
			}
		}

		/// <summary>Creates a new object that is a copy of the current <see cref="T:System.Net.Http.Headers.RetryConditionHeaderValue" /> instance.</summary>
		/// <returns>A copy of the current instance.</returns>
		// Token: 0x06000310 RID: 784 RVA: 0x00005BA5 File Offset: 0x00003DA5
		object ICloneable.Clone()
		{
			return base.MemberwiseClone();
		}

		/// <summary>Determines whether the specified <see cref="T:System.Object" /> is equal to the current <see cref="T:System.Net.Http.Headers.RetryConditionHeaderValue" /> object.</summary>
		/// <param name="obj">The object to compare with the current object.</param>
		/// <returns>
		///     <see langword="true" /> if the specified <see cref="T:System.Object" /> is equal to the current object; otherwise, <see langword="false" />.</returns>
		// Token: 0x06000311 RID: 785 RVA: 0x0000BA5C File Offset: 0x00009C5C
		public override bool Equals(object obj)
		{
			RetryConditionHeaderValue retryConditionHeaderValue = obj as RetryConditionHeaderValue;
			return retryConditionHeaderValue != null && retryConditionHeaderValue.Date == this.Date && retryConditionHeaderValue.Delta == this.Delta;
		}

		/// <summary>Serves as a hash function for an <see cref="T:System.Net.Http.Headers.RetryConditionHeaderValue" /> object.</summary>
		/// <returns>A hash code for the current object.</returns>
		// Token: 0x06000312 RID: 786 RVA: 0x0000BAFC File Offset: 0x00009CFC
		public override int GetHashCode()
		{
			return this.Date.GetHashCode() ^ this.Delta.GetHashCode();
		}

		/// <summary>Converts a string to an <see cref="T:System.Net.Http.Headers.RetryConditionHeaderValue" /> instance.</summary>
		/// <param name="input">A string that represents retry condition header value information.</param>
		/// <returns>A <see cref="T:System.Net.Http.Headers.RetryConditionHeaderValue" /> instance.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="input" /> is a <see langword="null" /> reference.</exception>
		/// <exception cref="T:System.FormatException">
		///         <paramref name="input" /> is not valid retry condition header value information.</exception>
		// Token: 0x06000313 RID: 787 RVA: 0x0000BB34 File Offset: 0x00009D34
		public static RetryConditionHeaderValue Parse(string input)
		{
			RetryConditionHeaderValue result;
			if (RetryConditionHeaderValue.TryParse(input, out result))
			{
				return result;
			}
			throw new FormatException(input);
		}

		/// <summary>Determines whether a string is valid <see cref="T:System.Net.Http.Headers.RetryConditionHeaderValue" /> information.</summary>
		/// <param name="input">The string to validate.</param>
		/// <param name="parsedValue">The <see cref="T:System.Net.Http.Headers.RetryConditionHeaderValue" /> version of the string.</param>
		/// <returns>
		///     <see langword="true" /> if <paramref name="input" /> is valid <see cref="T:System.Net.Http.Headers.RetryConditionHeaderValue" /> information; otherwise, <see langword="false" />.</returns>
		// Token: 0x06000314 RID: 788 RVA: 0x0000BB54 File Offset: 0x00009D54
		public static bool TryParse(string input, out RetryConditionHeaderValue parsedValue)
		{
			parsedValue = null;
			Lexer lexer = new Lexer(input);
			Token token = lexer.Scan(false);
			if (token != Token.Type.Token)
			{
				return false;
			}
			TimeSpan? timeSpan = lexer.TryGetTimeSpanValue(token);
			if (timeSpan != null)
			{
				if (lexer.Scan(false) != Token.Type.End)
				{
					return false;
				}
				parsedValue = new RetryConditionHeaderValue(timeSpan.Value);
			}
			else
			{
				DateTimeOffset date;
				if (!Lexer.TryGetDateValue(input, out date))
				{
					return false;
				}
				parsedValue = new RetryConditionHeaderValue(date);
			}
			return true;
		}

		/// <summary>Returns a string that represents the current <see cref="T:System.Net.Http.Headers.RetryConditionHeaderValue" /> object.</summary>
		/// <returns>A string that represents the current object.</returns>
		// Token: 0x06000315 RID: 789 RVA: 0x0000BBC8 File Offset: 0x00009DC8
		public override string ToString()
		{
			if (this.Delta == null)
			{
				return this.Date.Value.ToString("r", CultureInfo.InvariantCulture);
			}
			return this.Delta.Value.TotalSeconds.ToString(CultureInfo.InvariantCulture);
		}

		// Token: 0x04000149 RID: 329
		[CompilerGenerated]
		private DateTimeOffset? <Date>k__BackingField;

		// Token: 0x0400014A RID: 330
		[CompilerGenerated]
		private TimeSpan? <Delta>k__BackingField;
	}
}
