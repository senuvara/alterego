﻿using System;
using System.Collections.Generic;
using System.Text;

namespace System.Net.Http.Headers
{
	/// <summary>Represents a Range header value.</summary>
	// Token: 0x02000058 RID: 88
	public class RangeHeaderValue : ICloneable
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Net.Http.Headers.RangeHeaderValue" /> class.</summary>
		// Token: 0x060002F5 RID: 757 RVA: 0x0000B3C1 File Offset: 0x000095C1
		public RangeHeaderValue()
		{
			this.unit = "bytes";
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Net.Http.Headers.RangeHeaderValue" /> class with a byte range.</summary>
		/// <param name="from">The position at which to start sending data.</param>
		/// <param name="to">The position at which to stop sending data.</param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="from" /> is greater than <paramref name="to" />-or- 
		///         <paramref name="from" /> or <paramref name="to" /> is less than 0. </exception>
		// Token: 0x060002F6 RID: 758 RVA: 0x0000B3D4 File Offset: 0x000095D4
		public RangeHeaderValue(long? from, long? to) : this()
		{
			this.Ranges.Add(new RangeItemHeaderValue(from, to));
		}

		// Token: 0x060002F7 RID: 759 RVA: 0x0000B3F0 File Offset: 0x000095F0
		private RangeHeaderValue(RangeHeaderValue source) : this()
		{
			if (source.ranges != null)
			{
				foreach (RangeItemHeaderValue item in source.ranges)
				{
					this.Ranges.Add(item);
				}
			}
		}

		/// <summary>Gets the ranges specified from the <see cref="T:System.Net.Http.Headers.RangeHeaderValue" /> object.</summary>
		/// <returns>The ranges from the <see cref="T:System.Net.Http.Headers.RangeHeaderValue" /> object.</returns>
		// Token: 0x170000B2 RID: 178
		// (get) Token: 0x060002F8 RID: 760 RVA: 0x0000B458 File Offset: 0x00009658
		public ICollection<RangeItemHeaderValue> Ranges
		{
			get
			{
				List<RangeItemHeaderValue> result;
				if ((result = this.ranges) == null)
				{
					result = (this.ranges = new List<RangeItemHeaderValue>());
				}
				return result;
			}
		}

		/// <summary>Gets the unit from the <see cref="T:System.Net.Http.Headers.RangeHeaderValue" /> object.</summary>
		/// <returns>The unit from the <see cref="T:System.Net.Http.Headers.RangeHeaderValue" /> object.</returns>
		// Token: 0x170000B3 RID: 179
		// (get) Token: 0x060002F9 RID: 761 RVA: 0x0000B47D File Offset: 0x0000967D
		// (set) Token: 0x060002FA RID: 762 RVA: 0x0000B485 File Offset: 0x00009685
		public string Unit
		{
			get
			{
				return this.unit;
			}
			set
			{
				if (value == null)
				{
					throw new ArgumentNullException("Unit");
				}
				Parser.Token.Check(value);
				this.unit = value;
			}
		}

		/// <summary>Creates a new object that is a copy of the current <see cref="T:System.Net.Http.Headers.RangeHeaderValue" /> instance.</summary>
		/// <returns>A copy of the current instance.</returns>
		// Token: 0x060002FB RID: 763 RVA: 0x0000B4A2 File Offset: 0x000096A2
		object ICloneable.Clone()
		{
			return new RangeHeaderValue(this);
		}

		/// <summary>Determines whether the specified <see cref="T:System.Object" /> is equal to the current <see cref="T:System.Net.Http.Headers.RangeHeaderValue" /> object.</summary>
		/// <param name="obj">The object to compare with the current object.</param>
		/// <returns>
		///     <see langword="true" /> if the specified <see cref="T:System.Object" /> is equal to the current object; otherwise, <see langword="false" />.</returns>
		// Token: 0x060002FC RID: 764 RVA: 0x0000B4AC File Offset: 0x000096AC
		public override bool Equals(object obj)
		{
			RangeHeaderValue rangeHeaderValue = obj as RangeHeaderValue;
			return rangeHeaderValue != null && string.Equals(rangeHeaderValue.Unit, this.Unit, StringComparison.OrdinalIgnoreCase) && rangeHeaderValue.ranges.SequenceEqual(this.ranges);
		}

		/// <summary>Serves as a hash function for an <see cref="T:System.Net.Http.Headers.RangeHeaderValue" /> object.</summary>
		/// <returns>A hash code for the current object.</returns>
		// Token: 0x060002FD RID: 765 RVA: 0x0000B4EC File Offset: 0x000096EC
		public override int GetHashCode()
		{
			return this.Unit.ToLowerInvariant().GetHashCode() ^ HashCodeCalculator.Calculate<RangeItemHeaderValue>(this.ranges);
		}

		/// <summary>Converts a string to an <see cref="T:System.Net.Http.Headers.RangeHeaderValue" /> instance.</summary>
		/// <param name="input">A string that represents range header value information.</param>
		/// <returns>A <see cref="T:System.Net.Http.Headers.RangeHeaderValue" /> instance.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="input" /> is a <see langword="null" /> reference.</exception>
		/// <exception cref="T:System.FormatException">
		///         <paramref name="input" /> is not valid range header value information.</exception>
		// Token: 0x060002FE RID: 766 RVA: 0x0000B50C File Offset: 0x0000970C
		public static RangeHeaderValue Parse(string input)
		{
			RangeHeaderValue result;
			if (RangeHeaderValue.TryParse(input, out result))
			{
				return result;
			}
			throw new FormatException(input);
		}

		/// <summary>Determines whether a string is valid <see cref="T:System.Net.Http.Headers.RangeHeaderValue" /> information.</summary>
		/// <param name="input">he string to validate.</param>
		/// <param name="parsedValue">The <see cref="T:System.Net.Http.Headers.RangeHeaderValue" /> version of the string.</param>
		/// <returns>
		///     <see langword="true" /> if <paramref name="input" /> is valid <see cref="T:System.Net.Http.Headers.AuthenticationHeaderValue" /> information; otherwise, <see langword="false" />.</returns>
		// Token: 0x060002FF RID: 767 RVA: 0x0000B52C File Offset: 0x0000972C
		public static bool TryParse(string input, out RangeHeaderValue parsedValue)
		{
			parsedValue = null;
			Lexer lexer = new Lexer(input);
			Token token = lexer.Scan(false);
			if (token != Token.Type.Token)
			{
				return false;
			}
			RangeHeaderValue rangeHeaderValue = new RangeHeaderValue();
			rangeHeaderValue.unit = lexer.GetStringValue(token);
			token = lexer.Scan(false);
			if (token != Token.Type.SeparatorEqual)
			{
				return false;
			}
			for (;;)
			{
				long? num = null;
				long? num2 = null;
				bool flag = false;
				token = lexer.Scan(true);
				Token.Type kind = token.Kind;
				if (kind != Token.Type.Token)
				{
					if (kind != Token.Type.SeparatorDash)
					{
						return false;
					}
					token = lexer.Scan(false);
					long value;
					if (!lexer.TryGetNumericValue(token, out value))
					{
						break;
					}
					num2 = new long?(value);
				}
				else
				{
					string stringValue = lexer.GetStringValue(token);
					string[] array = stringValue.Split(new char[]
					{
						'-'
					}, StringSplitOptions.RemoveEmptyEntries);
					long value;
					if (!Parser.Long.TryParse(array[0], out value))
					{
						return false;
					}
					int num3 = array.Length;
					if (num3 != 1)
					{
						if (num3 != 2)
						{
							return false;
						}
						num = new long?(value);
						if (!Parser.Long.TryParse(array[1], out value))
						{
							return false;
						}
						num2 = new long?(value);
						if (num2 < num)
						{
							return false;
						}
					}
					else
					{
						token = lexer.Scan(true);
						num = new long?(value);
						kind = token.Kind;
						if (kind != Token.Type.End)
						{
							if (kind != Token.Type.SeparatorDash)
							{
								if (kind != Token.Type.SeparatorComma)
								{
									return false;
								}
								flag = true;
							}
							else
							{
								token = lexer.Scan(false);
								if (token != Token.Type.Token)
								{
									flag = true;
								}
								else
								{
									if (!lexer.TryGetNumericValue(token, out value))
									{
										return false;
									}
									num2 = new long?(value);
									if (num2 < num)
									{
										return false;
									}
								}
							}
						}
						else
						{
							if (stringValue.Length > 0 && stringValue[stringValue.Length - 1] != '-')
							{
								return false;
							}
							flag = true;
						}
					}
				}
				rangeHeaderValue.Ranges.Add(new RangeItemHeaderValue(num, num2));
				if (!flag)
				{
					token = lexer.Scan(false);
				}
				if (token != Token.Type.SeparatorComma)
				{
					goto Block_22;
				}
			}
			return false;
			Block_22:
			if (token != Token.Type.End)
			{
				return false;
			}
			parsedValue = rangeHeaderValue;
			return true;
		}

		/// <summary>Returns a string that represents the current <see cref="T:System.Net.Http.Headers.RangeHeaderValue" /> object.</summary>
		/// <returns>A string that represents the current object.</returns>
		// Token: 0x06000300 RID: 768 RVA: 0x0000B758 File Offset: 0x00009958
		public override string ToString()
		{
			StringBuilder stringBuilder = new StringBuilder(this.unit);
			stringBuilder.Append("=");
			for (int i = 0; i < this.Ranges.Count; i++)
			{
				if (i > 0)
				{
					stringBuilder.Append(", ");
				}
				stringBuilder.Append(this.ranges[i]);
			}
			return stringBuilder.ToString();
		}

		// Token: 0x04000145 RID: 325
		private List<RangeItemHeaderValue> ranges;

		// Token: 0x04000146 RID: 326
		private string unit;
	}
}
