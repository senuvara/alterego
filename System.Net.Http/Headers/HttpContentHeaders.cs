﻿using System;
using System.Collections.Generic;
using Unity;

namespace System.Net.Http.Headers
{
	/// <summary>Represents the collection of Content Headers as defined in RFC 2616.</summary>
	// Token: 0x02000035 RID: 53
	public sealed class HttpContentHeaders : HttpHeaders
	{
		// Token: 0x060001A8 RID: 424 RVA: 0x00007E2C File Offset: 0x0000602C
		internal HttpContentHeaders(HttpContent content) : base(HttpHeaderKind.Content)
		{
			this.content = content;
		}

		/// <summary>Gets the value of the <see langword="Allow" /> content header on an HTTP response. </summary>
		/// <returns>The value of the <see langword="Allow" /> header on an HTTP response.</returns>
		// Token: 0x1700005B RID: 91
		// (get) Token: 0x060001A9 RID: 425 RVA: 0x00007E3C File Offset: 0x0000603C
		public ICollection<string> Allow
		{
			get
			{
				return base.GetValues<string>("Allow");
			}
		}

		/// <summary>Gets the value of the <see langword="Content-Encoding" /> content header on an HTTP response.</summary>
		/// <returns>The value of the <see langword="Content-Encoding" /> content header on an HTTP response.</returns>
		// Token: 0x1700005C RID: 92
		// (get) Token: 0x060001AA RID: 426 RVA: 0x00007E49 File Offset: 0x00006049
		public ICollection<string> ContentEncoding
		{
			get
			{
				return base.GetValues<string>("Content-Encoding");
			}
		}

		/// <summary>Gets the value of the <see langword="Content-Disposition" /> content header on an HTTP response.</summary>
		/// <returns>The value of the <see langword="Content-Disposition" /> content header on an HTTP response.</returns>
		// Token: 0x1700005D RID: 93
		// (get) Token: 0x060001AB RID: 427 RVA: 0x00007E56 File Offset: 0x00006056
		// (set) Token: 0x060001AC RID: 428 RVA: 0x00007E63 File Offset: 0x00006063
		public ContentDispositionHeaderValue ContentDisposition
		{
			get
			{
				return base.GetValue<ContentDispositionHeaderValue>("Content-Disposition");
			}
			set
			{
				base.AddOrRemove<ContentDispositionHeaderValue>("Content-Disposition", value, null);
			}
		}

		/// <summary>Gets the value of the <see langword="Content-Language" /> content header on an HTTP response.</summary>
		/// <returns>The value of the <see langword="Content-Language" /> content header on an HTTP response.</returns>
		// Token: 0x1700005E RID: 94
		// (get) Token: 0x060001AD RID: 429 RVA: 0x00007E72 File Offset: 0x00006072
		public ICollection<string> ContentLanguage
		{
			get
			{
				return base.GetValues<string>("Content-Language");
			}
		}

		/// <summary>Gets or sets the value of the <see langword="Content-Length" /> content header on an HTTP response.</summary>
		/// <returns>The value of the <see langword="Content-Length" /> content header on an HTTP response.</returns>
		// Token: 0x1700005F RID: 95
		// (get) Token: 0x060001AE RID: 430 RVA: 0x00007E80 File Offset: 0x00006080
		// (set) Token: 0x060001AF RID: 431 RVA: 0x00007EE7 File Offset: 0x000060E7
		public long? ContentLength
		{
			get
			{
				long? result = base.GetValue<long?>("Content-Length");
				if (result != null)
				{
					return result;
				}
				result = this.content.LoadedBufferLength;
				if (result != null)
				{
					return result;
				}
				long value;
				if (this.content.TryComputeLength(out value))
				{
					base.SetValue<long>("Content-Length", value, null);
					return new long?(value);
				}
				return null;
			}
			set
			{
				base.AddOrRemove<long>("Content-Length", value);
			}
		}

		/// <summary>Gets or sets the value of the <see langword="Content-Location" /> content header on an HTTP response.</summary>
		/// <returns>The value of the <see langword="Content-Location" /> content header on an HTTP response.</returns>
		// Token: 0x17000060 RID: 96
		// (get) Token: 0x060001B0 RID: 432 RVA: 0x00007EF5 File Offset: 0x000060F5
		// (set) Token: 0x060001B1 RID: 433 RVA: 0x00007F02 File Offset: 0x00006102
		public Uri ContentLocation
		{
			get
			{
				return base.GetValue<Uri>("Content-Location");
			}
			set
			{
				base.AddOrRemove<Uri>("Content-Location", value, null);
			}
		}

		/// <summary>Gets or sets the value of the <see langword="Content-MD5" /> content header on an HTTP response.</summary>
		/// <returns>The value of the <see langword="Content-MD5" /> content header on an HTTP response.</returns>
		// Token: 0x17000061 RID: 97
		// (get) Token: 0x060001B2 RID: 434 RVA: 0x00007F11 File Offset: 0x00006111
		// (set) Token: 0x060001B3 RID: 435 RVA: 0x00007F1E File Offset: 0x0000611E
		public byte[] ContentMD5
		{
			get
			{
				return base.GetValue<byte[]>("Content-MD5");
			}
			set
			{
				base.AddOrRemove<byte[]>("Content-MD5", value, Parser.MD5.ToString);
			}
		}

		/// <summary>Gets or sets the value of the <see langword="Content-Range" /> content header on an HTTP response.</summary>
		/// <returns>The value of the <see langword="Content-Range" /> content header on an HTTP response.</returns>
		// Token: 0x17000062 RID: 98
		// (get) Token: 0x060001B4 RID: 436 RVA: 0x00007F31 File Offset: 0x00006131
		// (set) Token: 0x060001B5 RID: 437 RVA: 0x00007F3E File Offset: 0x0000613E
		public ContentRangeHeaderValue ContentRange
		{
			get
			{
				return base.GetValue<ContentRangeHeaderValue>("Content-Range");
			}
			set
			{
				base.AddOrRemove<ContentRangeHeaderValue>("Content-Range", value, null);
			}
		}

		/// <summary>Gets or sets the value of the <see langword="Content-Type" /> content header on an HTTP response.</summary>
		/// <returns>The value of the <see langword="Content-Type" /> content header on an HTTP response.</returns>
		// Token: 0x17000063 RID: 99
		// (get) Token: 0x060001B6 RID: 438 RVA: 0x00007F4D File Offset: 0x0000614D
		// (set) Token: 0x060001B7 RID: 439 RVA: 0x00007F5A File Offset: 0x0000615A
		public MediaTypeHeaderValue ContentType
		{
			get
			{
				return base.GetValue<MediaTypeHeaderValue>("Content-Type");
			}
			set
			{
				base.AddOrRemove<MediaTypeHeaderValue>("Content-Type", value, null);
			}
		}

		/// <summary>Gets or sets the value of the <see langword="Expires" /> content header on an HTTP response.</summary>
		/// <returns>The value of the <see langword="Expires" /> content header on an HTTP response.</returns>
		// Token: 0x17000064 RID: 100
		// (get) Token: 0x060001B8 RID: 440 RVA: 0x00007F69 File Offset: 0x00006169
		// (set) Token: 0x060001B9 RID: 441 RVA: 0x00007F76 File Offset: 0x00006176
		public DateTimeOffset? Expires
		{
			get
			{
				return base.GetValue<DateTimeOffset?>("Expires");
			}
			set
			{
				base.AddOrRemove<DateTimeOffset>("Expires", value, Parser.DateTime.ToString);
			}
		}

		/// <summary>Gets or sets the value of the <see langword="Last-Modified" /> content header on an HTTP response.</summary>
		/// <returns>The value of the <see langword="Last-Modified" /> content header on an HTTP response.</returns>
		// Token: 0x17000065 RID: 101
		// (get) Token: 0x060001BA RID: 442 RVA: 0x00007F89 File Offset: 0x00006189
		// (set) Token: 0x060001BB RID: 443 RVA: 0x00007F96 File Offset: 0x00006196
		public DateTimeOffset? LastModified
		{
			get
			{
				return base.GetValue<DateTimeOffset?>("Last-Modified");
			}
			set
			{
				base.AddOrRemove<DateTimeOffset>("Last-Modified", value, Parser.DateTime.ToString);
			}
		}

		// Token: 0x060001BC RID: 444 RVA: 0x00007FA9 File Offset: 0x000061A9
		internal HttpContentHeaders()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x040000FB RID: 251
		private readonly HttpContent content;
	}
}
