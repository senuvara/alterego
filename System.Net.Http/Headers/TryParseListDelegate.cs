﻿using System;
using System.Collections.Generic;

namespace System.Net.Http.Headers
{
	// Token: 0x02000031 RID: 49
	// (Invoke) Token: 0x06000192 RID: 402
	internal delegate bool TryParseListDelegate<T>(string value, int minimalCount, out List<T> result);
}
