﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace System.Net.Http.Headers
{
	/// <summary>Represents a media type used in a Content-Type header as defined in the RFC 2616.</summary>
	// Token: 0x02000042 RID: 66
	public class MediaTypeHeaderValue : ICloneable
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Net.Http.Headers.MediaTypeHeaderValue" /> class.</summary>
		/// <param name="mediaType">The source represented as a string to initialize the new instance. </param>
		// Token: 0x06000274 RID: 628 RVA: 0x00009F47 File Offset: 0x00008147
		public MediaTypeHeaderValue(string mediaType)
		{
			this.MediaType = mediaType;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Net.Http.Headers.MediaTypeHeaderValue" /> class.</summary>
		/// <param name="source"> A <see cref="T:System.Net.Http.Headers.MediaTypeHeaderValue" /> object used to initialize the new instance.</param>
		// Token: 0x06000275 RID: 629 RVA: 0x00009F58 File Offset: 0x00008158
		protected MediaTypeHeaderValue(MediaTypeHeaderValue source)
		{
			if (source == null)
			{
				throw new ArgumentNullException("source");
			}
			this.media_type = source.media_type;
			if (source.parameters != null)
			{
				foreach (NameValueHeaderValue source2 in source.parameters)
				{
					this.Parameters.Add(new NameValueHeaderValue(source2));
				}
			}
		}

		// Token: 0x06000276 RID: 630 RVA: 0x000039D8 File Offset: 0x00001BD8
		internal MediaTypeHeaderValue()
		{
		}

		/// <summary>Gets or sets the character set.</summary>
		/// <returns>The character set.</returns>
		// Token: 0x170000A5 RID: 165
		// (get) Token: 0x06000277 RID: 631 RVA: 0x00009FE0 File Offset: 0x000081E0
		// (set) Token: 0x06000278 RID: 632 RVA: 0x0000A02D File Offset: 0x0000822D
		public string CharSet
		{
			get
			{
				if (this.parameters == null)
				{
					return null;
				}
				NameValueHeaderValue nameValueHeaderValue = this.parameters.Find((NameValueHeaderValue l) => string.Equals(l.Name, "charset", StringComparison.OrdinalIgnoreCase));
				if (nameValueHeaderValue == null)
				{
					return null;
				}
				return nameValueHeaderValue.Value;
			}
			set
			{
				if (this.parameters == null)
				{
					this.parameters = new List<NameValueHeaderValue>();
				}
				this.parameters.SetValue("charset", value);
			}
		}

		/// <summary>Gets or sets the media-type header value.</summary>
		/// <returns>The media-type header value.</returns>
		// Token: 0x170000A6 RID: 166
		// (get) Token: 0x06000279 RID: 633 RVA: 0x0000A053 File Offset: 0x00008253
		// (set) Token: 0x0600027A RID: 634 RVA: 0x0000A05C File Offset: 0x0000825C
		public string MediaType
		{
			get
			{
				return this.media_type;
			}
			set
			{
				if (value == null)
				{
					throw new ArgumentNullException("MediaType");
				}
				string text;
				Token? token = MediaTypeHeaderValue.TryParseMediaType(new Lexer(value), out text);
				if (token == null || token.Value.Kind != Token.Type.End)
				{
					throw new FormatException();
				}
				this.media_type = text;
			}
		}

		/// <summary>Gets or sets the media-type header value parameters.</summary>
		/// <returns>The media-type header value parameters.</returns>
		// Token: 0x170000A7 RID: 167
		// (get) Token: 0x0600027B RID: 635 RVA: 0x0000A0B0 File Offset: 0x000082B0
		public ICollection<NameValueHeaderValue> Parameters
		{
			get
			{
				List<NameValueHeaderValue> result;
				if ((result = this.parameters) == null)
				{
					result = (this.parameters = new List<NameValueHeaderValue>());
				}
				return result;
			}
		}

		/// <summary>Creates a new object that is a copy of the current <see cref="T:System.Net.Http.Headers.MediaTypeHeaderValue" /> instance.</summary>
		/// <returns>A copy of the current instance.</returns>
		// Token: 0x0600027C RID: 636 RVA: 0x0000A0D5 File Offset: 0x000082D5
		object ICloneable.Clone()
		{
			return new MediaTypeHeaderValue(this);
		}

		/// <summary>Determines whether the specified <see cref="T:System.Object" /> is equal to the current <see cref="T:System.Net.Http.Headers.MediaTypeHeaderValue" /> object.</summary>
		/// <param name="obj">The object to compare with the current object.</param>
		/// <returns>
		///     <see langword="true" /> if the specified <see cref="T:System.Object" /> is equal to the current object; otherwise, <see langword="false" />.</returns>
		// Token: 0x0600027D RID: 637 RVA: 0x0000A0E0 File Offset: 0x000082E0
		public override bool Equals(object obj)
		{
			MediaTypeHeaderValue mediaTypeHeaderValue = obj as MediaTypeHeaderValue;
			return mediaTypeHeaderValue != null && string.Equals(mediaTypeHeaderValue.media_type, this.media_type, StringComparison.OrdinalIgnoreCase) && mediaTypeHeaderValue.parameters.SequenceEqual(this.parameters);
		}

		/// <summary>Serves as a hash function for an <see cref="T:System.Net.Http.Headers.MediaTypeHeaderValue" /> object.</summary>
		/// <returns>A hash code for the current object.</returns>
		// Token: 0x0600027E RID: 638 RVA: 0x0000A120 File Offset: 0x00008320
		public override int GetHashCode()
		{
			return this.media_type.ToLowerInvariant().GetHashCode() ^ HashCodeCalculator.Calculate<NameValueHeaderValue>(this.parameters);
		}

		/// <summary>Converts a string to an <see cref="T:System.Net.Http.Headers.MediaTypeHeaderValue" /> instance.</summary>
		/// <param name="input">A string that represents media type header value information.</param>
		/// <returns>A <see cref="T:System.Net.Http.Headers.MediaTypeHeaderValue" /> instance.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="input" /> is a <see langword="null" /> reference.</exception>
		/// <exception cref="T:System.FormatException">
		///         <paramref name="input" /> is not valid media type header value information.</exception>
		// Token: 0x0600027F RID: 639 RVA: 0x0000A140 File Offset: 0x00008340
		public static MediaTypeHeaderValue Parse(string input)
		{
			MediaTypeHeaderValue result;
			if (MediaTypeHeaderValue.TryParse(input, out result))
			{
				return result;
			}
			throw new FormatException(input);
		}

		/// <summary>Returns a string that represents the current <see cref="T:System.Net.Http.Headers.MediaTypeHeaderValue" /> object.</summary>
		/// <returns>A string that represents the current object.</returns>
		// Token: 0x06000280 RID: 640 RVA: 0x0000A15F File Offset: 0x0000835F
		public override string ToString()
		{
			if (this.parameters == null)
			{
				return this.media_type;
			}
			return this.media_type + this.parameters.ToString<NameValueHeaderValue>();
		}

		/// <summary>Determines whether a string is valid <see cref="T:System.Net.Http.Headers.MediaTypeHeaderValue" /> information.</summary>
		/// <param name="input">The string to validate.</param>
		/// <param name="parsedValue">The <see cref="T:System.Net.Http.Headers.MediaTypeHeaderValue" /> version of the string.</param>
		/// <returns>
		///     <see langword="true" /> if <paramref name="input" /> is valid <see cref="T:System.Net.Http.Headers.MediaTypeHeaderValue" /> information; otherwise, <see langword="false" />.</returns>
		// Token: 0x06000281 RID: 641 RVA: 0x0000A188 File Offset: 0x00008388
		public static bool TryParse(string input, out MediaTypeHeaderValue parsedValue)
		{
			parsedValue = null;
			Lexer lexer = new Lexer(input);
			List<NameValueHeaderValue> list = null;
			string text;
			Token? token = MediaTypeHeaderValue.TryParseMediaType(lexer, out text);
			if (token == null)
			{
				return false;
			}
			Token.Type kind = token.Value.Kind;
			if (kind != Token.Type.End)
			{
				if (kind != Token.Type.SeparatorSemicolon)
				{
					return false;
				}
				Token token2;
				if (!NameValueHeaderValue.TryParseParameters(lexer, out list, out token2) || token2 != Token.Type.End)
				{
					return false;
				}
			}
			parsedValue = new MediaTypeHeaderValue
			{
				media_type = text,
				parameters = list
			};
			return true;
		}

		// Token: 0x06000282 RID: 642 RVA: 0x0000A204 File Offset: 0x00008404
		internal static Token? TryParseMediaType(Lexer lexer, out string media)
		{
			media = null;
			Token token = lexer.Scan(false);
			if (token != Token.Type.Token)
			{
				return null;
			}
			if (lexer.Scan(false) != Token.Type.SeparatorSlash)
			{
				return null;
			}
			Token token2 = lexer.Scan(false);
			if (token2 != Token.Type.Token)
			{
				return null;
			}
			media = lexer.GetStringValue(token) + "/" + lexer.GetStringValue(token2);
			return new Token?(lexer.Scan(false));
		}

		// Token: 0x04000132 RID: 306
		internal List<NameValueHeaderValue> parameters;

		// Token: 0x04000133 RID: 307
		internal string media_type;

		// Token: 0x02000043 RID: 67
		[CompilerGenerated]
		[Serializable]
		private sealed class <>c
		{
			// Token: 0x06000283 RID: 643 RVA: 0x0000A289 File Offset: 0x00008489
			// Note: this type is marked as 'beforefieldinit'.
			static <>c()
			{
			}

			// Token: 0x06000284 RID: 644 RVA: 0x000039D8 File Offset: 0x00001BD8
			public <>c()
			{
			}

			// Token: 0x06000285 RID: 645 RVA: 0x0000A295 File Offset: 0x00008495
			internal bool <get_CharSet>b__6_0(NameValueHeaderValue l)
			{
				return string.Equals(l.Name, "charset", StringComparison.OrdinalIgnoreCase);
			}

			// Token: 0x04000134 RID: 308
			public static readonly MediaTypeHeaderValue.<>c <>9 = new MediaTypeHeaderValue.<>c();

			// Token: 0x04000135 RID: 309
			public static Predicate<NameValueHeaderValue> <>9__6_0;
		}
	}
}
