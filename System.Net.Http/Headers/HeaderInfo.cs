﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace System.Net.Http.Headers
{
	// Token: 0x02000032 RID: 50
	internal abstract class HeaderInfo
	{
		// Token: 0x06000195 RID: 405 RVA: 0x00007C70 File Offset: 0x00005E70
		protected HeaderInfo(string name, HttpHeaderKind headerKind)
		{
			this.Name = name;
			this.HeaderKind = headerKind;
		}

		// Token: 0x06000196 RID: 406 RVA: 0x00007C86 File Offset: 0x00005E86
		public static HeaderInfo CreateSingle<T>(string name, TryParseDelegate<T> parser, HttpHeaderKind headerKind, Func<object, string> toString = null)
		{
			return new HeaderInfo.HeaderTypeInfo<T, object>(name, parser, headerKind)
			{
				CustomToString = toString
			};
		}

		// Token: 0x06000197 RID: 407 RVA: 0x00007C97 File Offset: 0x00005E97
		public static HeaderInfo CreateMulti<T>(string name, TryParseListDelegate<T> elementParser, HttpHeaderKind headerKind, int minimalCount = 1, string separator = ", ") where T : class
		{
			return new HeaderInfo.CollectionHeaderTypeInfo<T, T>(name, elementParser, headerKind, minimalCount, separator);
		}

		// Token: 0x06000198 RID: 408 RVA: 0x00007CA4 File Offset: 0x00005EA4
		public object CreateCollection(HttpHeaders headers)
		{
			return this.CreateCollection(headers, this);
		}

		// Token: 0x17000058 RID: 88
		// (get) Token: 0x06000199 RID: 409 RVA: 0x00007CAE File Offset: 0x00005EAE
		// (set) Token: 0x0600019A RID: 410 RVA: 0x00007CB6 File Offset: 0x00005EB6
		public Func<object, string> CustomToString
		{
			[CompilerGenerated]
			get
			{
				return this.<CustomToString>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<CustomToString>k__BackingField = value;
			}
		}

		// Token: 0x17000059 RID: 89
		// (get) Token: 0x0600019B RID: 411 RVA: 0x00007CBF File Offset: 0x00005EBF
		public virtual string Separator
		{
			get
			{
				throw new NotSupportedException();
			}
		}

		// Token: 0x0600019C RID: 412
		public abstract void AddToCollection(object collection, object value);

		// Token: 0x0600019D RID: 413
		protected abstract object CreateCollection(HttpHeaders headers, HeaderInfo headerInfo);

		// Token: 0x0600019E RID: 414
		public abstract List<string> ToStringCollection(object collection);

		// Token: 0x0600019F RID: 415
		public abstract bool TryParse(string value, out object result);

		// Token: 0x040000F3 RID: 243
		public bool AllowsMany;

		// Token: 0x040000F4 RID: 244
		public readonly HttpHeaderKind HeaderKind;

		// Token: 0x040000F5 RID: 245
		public readonly string Name;

		// Token: 0x040000F6 RID: 246
		[CompilerGenerated]
		private Func<object, string> <CustomToString>k__BackingField;

		// Token: 0x02000033 RID: 51
		private class HeaderTypeInfo<T, U> : HeaderInfo where U : class
		{
			// Token: 0x060001A0 RID: 416 RVA: 0x00007CC6 File Offset: 0x00005EC6
			public HeaderTypeInfo(string name, TryParseDelegate<T> parser, HttpHeaderKind headerKind) : base(name, headerKind)
			{
				this.parser = parser;
			}

			// Token: 0x060001A1 RID: 417 RVA: 0x00007CD8 File Offset: 0x00005ED8
			public override void AddToCollection(object collection, object value)
			{
				HttpHeaderValueCollection<U> httpHeaderValueCollection = (HttpHeaderValueCollection<U>)collection;
				List<U> list = value as List<U>;
				if (list != null)
				{
					httpHeaderValueCollection.AddRange(list);
					return;
				}
				httpHeaderValueCollection.Add((U)((object)value));
			}

			// Token: 0x060001A2 RID: 418 RVA: 0x00007D0A File Offset: 0x00005F0A
			protected override object CreateCollection(HttpHeaders headers, HeaderInfo headerInfo)
			{
				return new HttpHeaderValueCollection<U>(headers, headerInfo);
			}

			// Token: 0x060001A3 RID: 419 RVA: 0x00007D14 File Offset: 0x00005F14
			public override List<string> ToStringCollection(object collection)
			{
				if (collection == null)
				{
					return null;
				}
				HttpHeaderValueCollection<U> httpHeaderValueCollection = (HttpHeaderValueCollection<U>)collection;
				if (httpHeaderValueCollection.Count != 0)
				{
					List<string> list = new List<string>();
					foreach (U u in httpHeaderValueCollection)
					{
						list.Add(u.ToString());
					}
					if (httpHeaderValueCollection.InvalidValues != null)
					{
						list.AddRange(httpHeaderValueCollection.InvalidValues);
					}
					return list;
				}
				if (httpHeaderValueCollection.InvalidValues == null)
				{
					return null;
				}
				return new List<string>(httpHeaderValueCollection.InvalidValues);
			}

			// Token: 0x060001A4 RID: 420 RVA: 0x00007DAC File Offset: 0x00005FAC
			public override bool TryParse(string value, out object result)
			{
				T t;
				bool result2 = this.parser(value, out t);
				result = t;
				return result2;
			}

			// Token: 0x040000F7 RID: 247
			private readonly TryParseDelegate<T> parser;
		}

		// Token: 0x02000034 RID: 52
		private class CollectionHeaderTypeInfo<T, U> : HeaderInfo.HeaderTypeInfo<T, U> where U : class
		{
			// Token: 0x060001A5 RID: 421 RVA: 0x00007DCF File Offset: 0x00005FCF
			public CollectionHeaderTypeInfo(string name, TryParseListDelegate<T> parser, HttpHeaderKind headerKind, int minimalCount, string separator) : base(name, null, headerKind)
			{
				this.parser = parser;
				this.minimalCount = minimalCount;
				this.AllowsMany = true;
				this.separator = separator;
			}

			// Token: 0x1700005A RID: 90
			// (get) Token: 0x060001A6 RID: 422 RVA: 0x00007DF8 File Offset: 0x00005FF8
			public override string Separator
			{
				get
				{
					return this.separator;
				}
			}

			// Token: 0x060001A7 RID: 423 RVA: 0x00007E00 File Offset: 0x00006000
			public override bool TryParse(string value, out object result)
			{
				List<T> list;
				if (!this.parser(value, this.minimalCount, out list))
				{
					result = null;
					return false;
				}
				result = list;
				return true;
			}

			// Token: 0x040000F8 RID: 248
			private readonly int minimalCount;

			// Token: 0x040000F9 RID: 249
			private readonly string separator;

			// Token: 0x040000FA RID: 250
			private readonly TryParseListDelegate<T> parser;
		}
	}
}
