﻿using System;
using System.Collections;
using System.Collections.Generic;
using Unity;

namespace System.Net.Http.Headers
{
	/// <summary>Represents a collection of header values.</summary>
	/// <typeparam name="T">The header collection type.</typeparam>
	// Token: 0x02000037 RID: 55
	public sealed class HttpHeaderValueCollection<T> : ICollection<T>, IEnumerable<T>, IEnumerable where T : class
	{
		// Token: 0x060001BD RID: 445 RVA: 0x00007FB0 File Offset: 0x000061B0
		internal HttpHeaderValueCollection(HttpHeaders headers, HeaderInfo headerInfo)
		{
			this.list = new List<T>();
			this.headers = headers;
			this.headerInfo = headerInfo;
		}

		/// <summary>Gets the number of headers in the <see cref="T:System.Net.Http.Headers.HttpHeaderValueCollection`1" />.</summary>
		/// <returns>The number of headers in a collection</returns>
		// Token: 0x17000066 RID: 102
		// (get) Token: 0x060001BE RID: 446 RVA: 0x00007FD1 File Offset: 0x000061D1
		public int Count
		{
			get
			{
				return this.list.Count;
			}
		}

		// Token: 0x17000067 RID: 103
		// (get) Token: 0x060001BF RID: 447 RVA: 0x00007FDE File Offset: 0x000061DE
		internal List<string> InvalidValues
		{
			get
			{
				return this.invalidValues;
			}
		}

		/// <summary>Gets a value indicating whether the <see cref="T:System.Net.Http.Headers.HttpHeaderValueCollection`1" /> instance is read-only.</summary>
		/// <returns>
		///     <see langword="true" /> if the <see cref="T:System.Net.Http.Headers.HttpHeaderValueCollection`1" /> instance is read-only; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000068 RID: 104
		// (get) Token: 0x060001C0 RID: 448 RVA: 0x00007FE6 File Offset: 0x000061E6
		public bool IsReadOnly
		{
			get
			{
				return false;
			}
		}

		/// <summary>Adds an entry to the <see cref="T:System.Net.Http.Headers.HttpHeaderValueCollection`1" />.</summary>
		/// <param name="item">The item to add to the header collection.</param>
		// Token: 0x060001C1 RID: 449 RVA: 0x00007FE9 File Offset: 0x000061E9
		public void Add(T item)
		{
			this.list.Add(item);
		}

		// Token: 0x060001C2 RID: 450 RVA: 0x00007FF7 File Offset: 0x000061F7
		internal void AddRange(List<T> values)
		{
			this.list.AddRange(values);
		}

		// Token: 0x060001C3 RID: 451 RVA: 0x00008005 File Offset: 0x00006205
		internal void AddInvalidValue(string invalidValue)
		{
			if (this.invalidValues == null)
			{
				this.invalidValues = new List<string>();
			}
			this.invalidValues.Add(invalidValue);
		}

		/// <summary>Removes all entries from the <see cref="T:System.Net.Http.Headers.HttpHeaderValueCollection`1" />.</summary>
		// Token: 0x060001C4 RID: 452 RVA: 0x00008026 File Offset: 0x00006226
		public void Clear()
		{
			this.list.Clear();
			this.invalidValues = null;
		}

		/// <summary>Determines if the <see cref="T:System.Net.Http.Headers.HttpHeaderValueCollection`1" /> contains an item.</summary>
		/// <param name="item">The item to find to the header collection.</param>
		/// <returns>
		///     <see langword="true" /> if the entry is contained in the <see cref="T:System.Net.Http.Headers.HttpHeaderValueCollection`1" /> instance; otherwise, <see langword="false" /></returns>
		// Token: 0x060001C5 RID: 453 RVA: 0x0000803A File Offset: 0x0000623A
		public bool Contains(T item)
		{
			return this.list.Contains(item);
		}

		/// <summary>Copies the entire <see cref="T:System.Net.Http.Headers.HttpHeaderValueCollection`1" /> to a compatible one-dimensional <see cref="T:System.Array" />, starting at the specified index of the target array.</summary>
		/// <param name="array">The one-dimensional <see cref="T:System.Array" /> that is the destination of the elements copied from <see cref="T:System.Net.Http.Headers.HttpHeaderValueCollection`1" />. The <see cref="T:System.Array" /> must have zero-based indexing.</param>
		/// <param name="arrayIndex">The zero-based index in <paramref name="array" /> at which copying begins.</param>
		// Token: 0x060001C6 RID: 454 RVA: 0x00008048 File Offset: 0x00006248
		public void CopyTo(T[] array, int arrayIndex)
		{
			this.list.CopyTo(array, arrayIndex);
		}

		/// <summary>Parses and adds an entry to the <see cref="T:System.Net.Http.Headers.HttpHeaderValueCollection`1" />.</summary>
		/// <param name="input">The entry to add.</param>
		// Token: 0x060001C7 RID: 455 RVA: 0x00008057 File Offset: 0x00006257
		public void ParseAdd(string input)
		{
			this.headers.AddValue(input, this.headerInfo, false);
		}

		/// <summary>Removes the specified item from the <see cref="T:System.Net.Http.Headers.HttpHeaderValueCollection`1" />.</summary>
		/// <param name="item">The item to remove.</param>
		/// <returns>
		///     <see langword="true" /> if the <paramref name="item" /> was removed from the <see cref="T:System.Net.Http.Headers.HttpHeaderValueCollection`1" /> instance; otherwise, <see langword="false" /></returns>
		// Token: 0x060001C8 RID: 456 RVA: 0x0000806D File Offset: 0x0000626D
		public bool Remove(T item)
		{
			return this.list.Remove(item);
		}

		/// <summary>Returns a string that represents the current <see cref="T:System.Net.Http.Headers.HttpHeaderValueCollection`1" /> object. object.</summary>
		/// <returns>A string that represents the current object.</returns>
		// Token: 0x060001C9 RID: 457 RVA: 0x0000807C File Offset: 0x0000627C
		public override string ToString()
		{
			string text = string.Join<T>(this.headerInfo.Separator, this.list);
			if (this.invalidValues != null)
			{
				text += string.Join(this.headerInfo.Separator, this.invalidValues);
			}
			return text;
		}

		/// <summary>Determines whether the input could be parsed and added to the <see cref="T:System.Net.Http.Headers.HttpHeaderValueCollection`1" />.</summary>
		/// <param name="input">The entry to validate.</param>
		/// <returns>
		///     <see langword="true" /> if the <paramref name="input" /> could be parsed and added to the <see cref="T:System.Net.Http.Headers.HttpHeaderValueCollection`1" /> instance; otherwise, <see langword="false" /></returns>
		// Token: 0x060001CA RID: 458 RVA: 0x000080C6 File Offset: 0x000062C6
		public bool TryParseAdd(string input)
		{
			return this.headers.AddValue(input, this.headerInfo, true);
		}

		/// <summary>Returns an enumerator that iterates through the <see cref="T:System.Net.Http.Headers.HttpHeaderValueCollection`1" />.</summary>
		/// <returns>An enumerator for the <see cref="T:System.Net.Http.Headers.HttpHeaderValueCollection`1" /> instance.</returns>
		// Token: 0x060001CB RID: 459 RVA: 0x000080DB File Offset: 0x000062DB
		public IEnumerator<T> GetEnumerator()
		{
			return this.list.GetEnumerator();
		}

		/// <summary>Returns an enumerator that iterates through the <see cref="T:System.Net.Http.Headers.HttpHeaderValueCollection`1" />.</summary>
		/// <returns>An enumerator for the <see cref="T:System.Net.Http.Headers.HttpHeaderValueCollection`1" /> instance.</returns>
		// Token: 0x060001CC RID: 460 RVA: 0x000080ED File Offset: 0x000062ED
		IEnumerator IEnumerable.GetEnumerator()
		{
			return this.GetEnumerator();
		}

		// Token: 0x060001CD RID: 461 RVA: 0x000080F5 File Offset: 0x000062F5
		internal T Find(Predicate<T> predicate)
		{
			return this.list.Find(predicate);
		}

		// Token: 0x060001CE RID: 462 RVA: 0x00008104 File Offset: 0x00006304
		internal void Remove(Predicate<T> predicate)
		{
			T t = this.Find(predicate);
			if (t != null)
			{
				this.Remove(t);
			}
		}

		// Token: 0x060001CF RID: 463 RVA: 0x00007FA9 File Offset: 0x000061A9
		internal HttpHeaderValueCollection()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x04000101 RID: 257
		private readonly List<T> list;

		// Token: 0x04000102 RID: 258
		private readonly HttpHeaders headers;

		// Token: 0x04000103 RID: 259
		private readonly HeaderInfo headerInfo;

		// Token: 0x04000104 RID: 260
		private List<string> invalidValues;
	}
}
