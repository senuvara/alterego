﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.CompilerServices;

namespace System.Net.Http.Headers
{
	// Token: 0x02000055 RID: 85
	internal static class QualityValue
	{
		// Token: 0x060002E3 RID: 739 RVA: 0x0000B084 File Offset: 0x00009284
		public static double? GetValue(List<NameValueHeaderValue> parameters)
		{
			if (parameters == null)
			{
				return null;
			}
			NameValueHeaderValue nameValueHeaderValue = parameters.Find((NameValueHeaderValue l) => string.Equals(l.Name, "q", StringComparison.OrdinalIgnoreCase));
			if (nameValueHeaderValue == null)
			{
				return null;
			}
			double value;
			if (!double.TryParse(nameValueHeaderValue.Value, NumberStyles.Number, NumberFormatInfo.InvariantInfo, out value))
			{
				return null;
			}
			return new double?(value);
		}

		// Token: 0x060002E4 RID: 740 RVA: 0x0000B0F8 File Offset: 0x000092F8
		public static void SetValue(ref List<NameValueHeaderValue> parameters, double? value)
		{
			if (value < 0.0 || value > (double)1)
			{
				throw new ArgumentOutOfRangeException("Quality");
			}
			if (parameters == null)
			{
				parameters = new List<NameValueHeaderValue>();
			}
			parameters.SetValue("q", (value == null) ? null : value.Value.ToString(NumberFormatInfo.InvariantInfo));
		}

		// Token: 0x02000056 RID: 86
		[CompilerGenerated]
		[Serializable]
		private sealed class <>c
		{
			// Token: 0x060002E5 RID: 741 RVA: 0x0000B184 File Offset: 0x00009384
			// Note: this type is marked as 'beforefieldinit'.
			static <>c()
			{
			}

			// Token: 0x060002E6 RID: 742 RVA: 0x000039D8 File Offset: 0x00001BD8
			public <>c()
			{
			}

			// Token: 0x060002E7 RID: 743 RVA: 0x0000B190 File Offset: 0x00009390
			internal bool <GetValue>b__0_0(NameValueHeaderValue l)
			{
				return string.Equals(l.Name, "q", StringComparison.OrdinalIgnoreCase);
			}

			// Token: 0x04000141 RID: 321
			public static readonly QualityValue.<>c <>9 = new QualityValue.<>c();

			// Token: 0x04000142 RID: 322
			public static Predicate<NameValueHeaderValue> <>9__0_0;
		}
	}
}
