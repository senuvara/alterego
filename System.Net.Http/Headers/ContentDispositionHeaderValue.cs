﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace System.Net.Http.Headers
{
	/// <summary>Represents the value of the Content-Disposition header.</summary>
	// Token: 0x0200002C RID: 44
	public class ContentDispositionHeaderValue : ICloneable
	{
		// Token: 0x06000146 RID: 326 RVA: 0x000039D8 File Offset: 0x00001BD8
		private ContentDispositionHeaderValue()
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Net.Http.Headers.ContentDispositionHeaderValue" /> class.</summary>
		/// <param name="dispositionType">A string that contains a <see cref="T:System.Net.Http.Headers.ContentDispositionHeaderValue" />.</param>
		// Token: 0x06000147 RID: 327 RVA: 0x00006CE0 File Offset: 0x00004EE0
		public ContentDispositionHeaderValue(string dispositionType)
		{
			this.DispositionType = dispositionType;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Net.Http.Headers.ContentDispositionHeaderValue" /> class.</summary>
		/// <param name="source">A <see cref="T:System.Net.Http.Headers.ContentDispositionHeaderValue" />. </param>
		// Token: 0x06000148 RID: 328 RVA: 0x00006CF0 File Offset: 0x00004EF0
		protected ContentDispositionHeaderValue(ContentDispositionHeaderValue source)
		{
			if (source == null)
			{
				throw new ArgumentNullException("source");
			}
			this.dispositionType = source.dispositionType;
			if (source.parameters != null)
			{
				foreach (NameValueHeaderValue source2 in source.parameters)
				{
					this.Parameters.Add(new NameValueHeaderValue(source2));
				}
			}
		}

		/// <summary>The date at which   the file was created.</summary>
		/// <returns>The file creation date.</returns>
		// Token: 0x17000046 RID: 70
		// (get) Token: 0x06000149 RID: 329 RVA: 0x00006D78 File Offset: 0x00004F78
		// (set) Token: 0x0600014A RID: 330 RVA: 0x00006D85 File Offset: 0x00004F85
		public DateTimeOffset? CreationDate
		{
			get
			{
				return this.GetDateValue("creation-date");
			}
			set
			{
				this.SetDateValue("creation-date", value);
			}
		}

		/// <summary>The disposition type for a content body part.</summary>
		/// <returns>The disposition type. </returns>
		// Token: 0x17000047 RID: 71
		// (get) Token: 0x0600014B RID: 331 RVA: 0x00006D93 File Offset: 0x00004F93
		// (set) Token: 0x0600014C RID: 332 RVA: 0x00006D9B File Offset: 0x00004F9B
		public string DispositionType
		{
			get
			{
				return this.dispositionType;
			}
			set
			{
				Parser.Token.Check(value);
				this.dispositionType = value;
			}
		}

		/// <summary>A suggestion for how to construct a filename for   storing the message payload to be used if the entity is   detached and stored in a separate file.</summary>
		/// <returns>A suggested filename.</returns>
		// Token: 0x17000048 RID: 72
		// (get) Token: 0x0600014D RID: 333 RVA: 0x00006DAC File Offset: 0x00004FAC
		// (set) Token: 0x0600014E RID: 334 RVA: 0x00006DD1 File Offset: 0x00004FD1
		public string FileName
		{
			get
			{
				string text = this.FindParameter("filename");
				if (text == null)
				{
					return null;
				}
				return ContentDispositionHeaderValue.DecodeValue(text, false);
			}
			set
			{
				if (value != null)
				{
					value = ContentDispositionHeaderValue.EncodeBase64Value(value);
				}
				this.SetValue("filename", value);
			}
		}

		/// <summary>A suggestion for how to construct filenames for   storing message payloads to be used if the entities are    detached and stored in a separate files.</summary>
		/// <returns>A suggested filename of the form filename*.</returns>
		// Token: 0x17000049 RID: 73
		// (get) Token: 0x0600014F RID: 335 RVA: 0x00006DEC File Offset: 0x00004FEC
		// (set) Token: 0x06000150 RID: 336 RVA: 0x00006E11 File Offset: 0x00005011
		public string FileNameStar
		{
			get
			{
				string text = this.FindParameter("filename*");
				if (text == null)
				{
					return null;
				}
				return ContentDispositionHeaderValue.DecodeValue(text, true);
			}
			set
			{
				if (value != null)
				{
					value = ContentDispositionHeaderValue.EncodeRFC5987(value);
				}
				this.SetValue("filename*", value);
			}
		}

		/// <summary>The date at   which the file was last modified. </summary>
		/// <returns>The file modification date.</returns>
		// Token: 0x1700004A RID: 74
		// (get) Token: 0x06000151 RID: 337 RVA: 0x00006E2A File Offset: 0x0000502A
		// (set) Token: 0x06000152 RID: 338 RVA: 0x00006E37 File Offset: 0x00005037
		public DateTimeOffset? ModificationDate
		{
			get
			{
				return this.GetDateValue("modification-date");
			}
			set
			{
				this.SetDateValue("modification-date", value);
			}
		}

		/// <summary>The name for a content body part.</summary>
		/// <returns>The name for the content body part.</returns>
		// Token: 0x1700004B RID: 75
		// (get) Token: 0x06000153 RID: 339 RVA: 0x00006E48 File Offset: 0x00005048
		// (set) Token: 0x06000154 RID: 340 RVA: 0x00006E6D File Offset: 0x0000506D
		public string Name
		{
			get
			{
				string text = this.FindParameter("name");
				if (text == null)
				{
					return null;
				}
				return ContentDispositionHeaderValue.DecodeValue(text, false);
			}
			set
			{
				if (value != null)
				{
					value = ContentDispositionHeaderValue.EncodeBase64Value(value);
				}
				this.SetValue("name", value);
			}
		}

		/// <summary>A set of parameters included the Content-Disposition header.</summary>
		/// <returns>A collection of parameters. </returns>
		// Token: 0x1700004C RID: 76
		// (get) Token: 0x06000155 RID: 341 RVA: 0x00006E88 File Offset: 0x00005088
		public ICollection<NameValueHeaderValue> Parameters
		{
			get
			{
				List<NameValueHeaderValue> result;
				if ((result = this.parameters) == null)
				{
					result = (this.parameters = new List<NameValueHeaderValue>());
				}
				return result;
			}
		}

		/// <summary>The date the file was last read.</summary>
		/// <returns>The last read date.</returns>
		// Token: 0x1700004D RID: 77
		// (get) Token: 0x06000156 RID: 342 RVA: 0x00006EAD File Offset: 0x000050AD
		// (set) Token: 0x06000157 RID: 343 RVA: 0x00006EBA File Offset: 0x000050BA
		public DateTimeOffset? ReadDate
		{
			get
			{
				return this.GetDateValue("read-date");
			}
			set
			{
				this.SetDateValue("read-date", value);
			}
		}

		/// <summary>The approximate size, in bytes, of the file. </summary>
		/// <returns>The approximate size, in bytes.</returns>
		// Token: 0x1700004E RID: 78
		// (get) Token: 0x06000158 RID: 344 RVA: 0x00006EC8 File Offset: 0x000050C8
		// (set) Token: 0x06000159 RID: 345 RVA: 0x00006EFC File Offset: 0x000050FC
		public long? Size
		{
			get
			{
				long value;
				if (Parser.Long.TryParse(this.FindParameter("size"), out value))
				{
					return new long?(value);
				}
				return null;
			}
			set
			{
				if (value == null)
				{
					this.SetValue("size", null);
					return;
				}
				if (value < 0L)
				{
					throw new ArgumentOutOfRangeException("value");
				}
				this.SetValue("size", value.Value.ToString(CultureInfo.InvariantCulture));
			}
		}

		/// <summary>Creates a new object that is a copy of the current <see cref="T:System.Net.Http.Headers.ContentDispositionHeaderValue" />  instance.</summary>
		/// <returns>A copy of the current instance.</returns>
		// Token: 0x0600015A RID: 346 RVA: 0x00006F64 File Offset: 0x00005164
		object ICloneable.Clone()
		{
			return new ContentDispositionHeaderValue(this);
		}

		/// <summary>Determines whether the specified <see cref="T:System.Object" /> is equal to the current <see cref="T:System.Net.Http.Headers.ContentDispositionHeaderValue" />  object.</summary>
		/// <param name="obj">The object to compare with the current object.</param>
		/// <returns>
		///     <see langword="true" /> if the specified <see cref="T:System.Object" /> is equal to the current object; otherwise, <see langword="false" />.</returns>
		// Token: 0x0600015B RID: 347 RVA: 0x00006F6C File Offset: 0x0000516C
		public override bool Equals(object obj)
		{
			ContentDispositionHeaderValue contentDispositionHeaderValue = obj as ContentDispositionHeaderValue;
			return contentDispositionHeaderValue != null && string.Equals(contentDispositionHeaderValue.dispositionType, this.dispositionType, StringComparison.OrdinalIgnoreCase) && contentDispositionHeaderValue.parameters.SequenceEqual(this.parameters);
		}

		// Token: 0x0600015C RID: 348 RVA: 0x00006FAC File Offset: 0x000051AC
		private string FindParameter(string name)
		{
			if (this.parameters == null)
			{
				return null;
			}
			foreach (NameValueHeaderValue nameValueHeaderValue in this.parameters)
			{
				if (string.Equals(nameValueHeaderValue.Name, name, StringComparison.OrdinalIgnoreCase))
				{
					return nameValueHeaderValue.Value;
				}
			}
			return null;
		}

		// Token: 0x0600015D RID: 349 RVA: 0x00007020 File Offset: 0x00005220
		private DateTimeOffset? GetDateValue(string name)
		{
			string text = this.FindParameter(name);
			if (text == null || text == null)
			{
				return null;
			}
			if (text.Length < 3)
			{
				return null;
			}
			if (text[0] == '"')
			{
				text = text.Substring(1, text.Length - 2);
			}
			DateTimeOffset value;
			if (Lexer.TryGetDateValue(text, out value))
			{
				return new DateTimeOffset?(value);
			}
			return null;
		}

		// Token: 0x0600015E RID: 350 RVA: 0x00007090 File Offset: 0x00005290
		private static string EncodeBase64Value(string value)
		{
			bool flag = value.Length > 1 && value[0] == '"' && value[value.Length - 1] == '"';
			if (flag)
			{
				value = value.Substring(1, value.Length - 2);
			}
			for (int i = 0; i < value.Length; i++)
			{
				if (value[i] > '\u007f')
				{
					Encoding utf = Encoding.UTF8;
					return string.Format("\"=?{0}?B?{1}?=\"", utf.WebName, Convert.ToBase64String(utf.GetBytes(value)));
				}
			}
			if (flag || !Lexer.IsValidToken(value))
			{
				return "\"" + value + "\"";
			}
			return value;
		}

		// Token: 0x0600015F RID: 351 RVA: 0x00007138 File Offset: 0x00005338
		private static string EncodeRFC5987(string value)
		{
			Encoding utf = Encoding.UTF8;
			StringBuilder stringBuilder = new StringBuilder(value.Length + 11);
			stringBuilder.Append(utf.WebName);
			stringBuilder.Append('\'');
			stringBuilder.Append('\'');
			foreach (char c in value)
			{
				if (c > '\u007f')
				{
					foreach (byte b in utf.GetBytes(new char[]
					{
						c
					}))
					{
						stringBuilder.Append('%');
						stringBuilder.Append(b.ToString("X2"));
					}
				}
				else if (!Lexer.IsValidCharacter(c) || c == '*' || c == '?' || c == '%')
				{
					stringBuilder.Append(Uri.HexEscape(c));
				}
				else
				{
					stringBuilder.Append(c);
				}
			}
			return stringBuilder.ToString();
		}

		// Token: 0x06000160 RID: 352 RVA: 0x0000721C File Offset: 0x0000541C
		private static string DecodeValue(string value, bool extendedNotation)
		{
			if (value.Length < 2)
			{
				return value;
			}
			string[] array;
			Encoding encoding;
			if (value[0] == '"')
			{
				array = value.Split(new char[]
				{
					'?'
				});
				if (array.Length != 5 || array[0] != "\"=" || array[4] != "=\"" || (array[2] != "B" && array[2] != "b"))
				{
					return value;
				}
				try
				{
					encoding = Encoding.GetEncoding(array[1]);
					return encoding.GetString(Convert.FromBase64String(array[3]));
				}
				catch
				{
					return value;
				}
			}
			if (!extendedNotation)
			{
				return value;
			}
			array = value.Split(new char[]
			{
				'\''
			});
			if (array.Length != 3)
			{
				return null;
			}
			try
			{
				encoding = Encoding.GetEncoding(array[0]);
			}
			catch
			{
				return null;
			}
			value = array[2];
			if (value.IndexOf('%') < 0)
			{
				return value;
			}
			StringBuilder stringBuilder = new StringBuilder();
			byte[] array2 = null;
			int num = 0;
			int i = 0;
			while (i < value.Length)
			{
				char c = value[i];
				if (c == '%')
				{
					char c2 = c;
					c = Uri.HexUnescape(value, ref i);
					if (c != c2)
					{
						if (array2 == null)
						{
							array2 = new byte[value.Length - i + 1];
						}
						array2[num++] = (byte)c;
						continue;
					}
				}
				else
				{
					i++;
				}
				if (num != 0)
				{
					stringBuilder.Append(encoding.GetChars(array2, 0, num));
					num = 0;
				}
				stringBuilder.Append(c);
			}
			if (num != 0)
			{
				stringBuilder.Append(encoding.GetChars(array2, 0, num));
			}
			return stringBuilder.ToString();
		}

		/// <summary>Serves as a hash function for an  <see cref="T:System.Net.Http.Headers.ContentDispositionHeaderValue" /> object.</summary>
		/// <returns>A hash code for the current object.</returns>
		// Token: 0x06000161 RID: 353 RVA: 0x000073C4 File Offset: 0x000055C4
		public override int GetHashCode()
		{
			return this.dispositionType.ToLowerInvariant().GetHashCode() ^ HashCodeCalculator.Calculate<NameValueHeaderValue>(this.parameters);
		}

		/// <summary>Converts a string to an <see cref="T:System.Net.Http.Headers.ContentDispositionHeaderValue" />  instance.</summary>
		/// <param name="input">A string that represents content disposition header value information.</param>
		/// <returns>An <see cref="T:System.Net.Http.Headers.ContentDispositionHeaderValue" />  instance.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="input" /> is a <see langword="null" /> reference.</exception>
		/// <exception cref="T:System.FormatException">
		///         <paramref name="input" /> is not valid content disposition header value information.</exception>
		// Token: 0x06000162 RID: 354 RVA: 0x000073E4 File Offset: 0x000055E4
		public static ContentDispositionHeaderValue Parse(string input)
		{
			ContentDispositionHeaderValue result;
			if (ContentDispositionHeaderValue.TryParse(input, out result))
			{
				return result;
			}
			throw new FormatException(input);
		}

		// Token: 0x06000163 RID: 355 RVA: 0x00007404 File Offset: 0x00005604
		private void SetDateValue(string key, DateTimeOffset? value)
		{
			this.SetValue(key, (value == null) ? null : ("\"" + value.Value.ToString("r", CultureInfo.InvariantCulture) + "\""));
		}

		// Token: 0x06000164 RID: 356 RVA: 0x0000744C File Offset: 0x0000564C
		private void SetValue(string key, string value)
		{
			if (this.parameters == null)
			{
				this.parameters = new List<NameValueHeaderValue>();
			}
			this.parameters.SetValue(key, value);
		}

		/// <summary>Returns a string that represents the current <see cref="T:System.Net.Http.Headers.ContentDispositionHeaderValue" /> object.</summary>
		/// <returns>A string that represents the current object.</returns>
		// Token: 0x06000165 RID: 357 RVA: 0x0000746E File Offset: 0x0000566E
		public override string ToString()
		{
			return this.dispositionType + this.parameters.ToString<NameValueHeaderValue>();
		}

		/// <summary>Determines whether a string is valid <see cref="T:System.Net.Http.Headers.ContentDispositionHeaderValue" /> information.</summary>
		/// <param name="input">The string to validate.</param>
		/// <param name="parsedValue">The <see cref="T:System.Net.Http.Headers.ContentDispositionHeaderValue" /> version of the string.</param>
		/// <returns>
		///     <see langword="true" /> if <paramref name="input" /> is valid <see cref="T:System.Net.Http.Headers.ContentDispositionHeaderValue" /> information; otherwise, <see langword="false" />.</returns>
		// Token: 0x06000166 RID: 358 RVA: 0x00007488 File Offset: 0x00005688
		public static bool TryParse(string input, out ContentDispositionHeaderValue parsedValue)
		{
			parsedValue = null;
			Lexer lexer = new Lexer(input);
			Token token = lexer.Scan(false);
			if (token.Kind != Token.Type.Token)
			{
				return false;
			}
			List<NameValueHeaderValue> list = null;
			string stringValue = lexer.GetStringValue(token);
			token = lexer.Scan(false);
			Token.Type kind = token.Kind;
			if (kind != Token.Type.End)
			{
				if (kind != Token.Type.SeparatorSemicolon)
				{
					return false;
				}
				if (!NameValueHeaderValue.TryParseParameters(lexer, out list, out token) || token != Token.Type.End)
				{
					return false;
				}
			}
			parsedValue = new ContentDispositionHeaderValue
			{
				dispositionType = stringValue,
				parameters = list
			};
			return true;
		}

		// Token: 0x040000EA RID: 234
		private string dispositionType;

		// Token: 0x040000EB RID: 235
		private List<NameValueHeaderValue> parameters;
	}
}
