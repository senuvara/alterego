﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace System.Net.Http.Headers
{
	/// <summary>Represents a name/value pair used in various headers as defined in RFC 2616.</summary>
	// Token: 0x02000045 RID: 69
	public class NameValueHeaderValue : ICloneable
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Net.Http.Headers.NameValueHeaderValue" /> class.</summary>
		/// <param name="name">The header name.</param>
		// Token: 0x0600028F RID: 655 RVA: 0x0000A3BF File Offset: 0x000085BF
		public NameValueHeaderValue(string name) : this(name, null)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Net.Http.Headers.NameValueHeaderValue" /> class.</summary>
		/// <param name="name">The header name.</param>
		/// <param name="value">The header value.</param>
		// Token: 0x06000290 RID: 656 RVA: 0x0000A3C9 File Offset: 0x000085C9
		public NameValueHeaderValue(string name, string value)
		{
			Parser.Token.Check(name);
			this.Name = name;
			this.Value = value;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Net.Http.Headers.NameValueHeaderValue" /> class.</summary>
		/// <param name="source">A <see cref="T:System.Net.Http.Headers.NameValueHeaderValue" /> object used to initialize the new instance.</param>
		// Token: 0x06000291 RID: 657 RVA: 0x0000A3E5 File Offset: 0x000085E5
		protected internal NameValueHeaderValue(NameValueHeaderValue source)
		{
			this.Name = source.Name;
			this.value = source.value;
		}

		// Token: 0x06000292 RID: 658 RVA: 0x000039D8 File Offset: 0x00001BD8
		internal NameValueHeaderValue()
		{
		}

		/// <summary>Gets the header name.</summary>
		/// <returns>The header name.</returns>
		// Token: 0x170000A9 RID: 169
		// (get) Token: 0x06000293 RID: 659 RVA: 0x0000A405 File Offset: 0x00008605
		// (set) Token: 0x06000294 RID: 660 RVA: 0x0000A40D File Offset: 0x0000860D
		public string Name
		{
			[CompilerGenerated]
			get
			{
				return this.<Name>k__BackingField;
			}
			[CompilerGenerated]
			internal set
			{
				this.<Name>k__BackingField = value;
			}
		}

		/// <summary>Gets the header value.</summary>
		/// <returns>The header value.</returns>
		// Token: 0x170000AA RID: 170
		// (get) Token: 0x06000295 RID: 661 RVA: 0x0000A416 File Offset: 0x00008616
		// (set) Token: 0x06000296 RID: 662 RVA: 0x0000A420 File Offset: 0x00008620
		public string Value
		{
			get
			{
				return this.value;
			}
			set
			{
				if (!string.IsNullOrEmpty(value))
				{
					Lexer lexer = new Lexer(value);
					Token token = lexer.Scan(false);
					if (lexer.Scan(false) != Token.Type.End || (token != Token.Type.Token && token != Token.Type.QuotedString))
					{
						throw new FormatException();
					}
					value = lexer.GetStringValue(token);
				}
				this.value = value;
			}
		}

		// Token: 0x06000297 RID: 663 RVA: 0x0000A479 File Offset: 0x00008679
		internal static NameValueHeaderValue Create(string name, string value)
		{
			return new NameValueHeaderValue
			{
				Name = name,
				value = value
			};
		}

		/// <summary>Creates a new object that is a copy of the current <see cref="T:System.Net.Http.Headers.NameValueHeaderValue" /> instance.</summary>
		/// <returns>A copy of the current instance.</returns>
		// Token: 0x06000298 RID: 664 RVA: 0x0000A48E File Offset: 0x0000868E
		object ICloneable.Clone()
		{
			return new NameValueHeaderValue(this);
		}

		/// <summary>Serves as a hash function for an <see cref="T:System.Net.Http.Headers.NameValueHeaderValue" /> object.</summary>
		/// <returns>A hash code for the current object.</returns>
		// Token: 0x06000299 RID: 665 RVA: 0x0000A498 File Offset: 0x00008698
		public override int GetHashCode()
		{
			int num = this.Name.ToLowerInvariant().GetHashCode();
			if (!string.IsNullOrEmpty(this.value))
			{
				num ^= this.value.ToLowerInvariant().GetHashCode();
			}
			return num;
		}

		/// <summary>Determines whether the specified <see cref="T:System.Object" /> is equal to the current <see cref="T:System.Net.Http.Headers.NameValueHeaderValue" /> object.</summary>
		/// <param name="obj">The object to compare with the current object.</param>
		/// <returns>
		///     <see langword="true" /> if the specified <see cref="T:System.Object" /> is equal to the current object; otherwise, <see langword="false" />.</returns>
		// Token: 0x0600029A RID: 666 RVA: 0x0000A4D8 File Offset: 0x000086D8
		public override bool Equals(object obj)
		{
			NameValueHeaderValue nameValueHeaderValue = obj as NameValueHeaderValue;
			if (nameValueHeaderValue == null || !string.Equals(nameValueHeaderValue.Name, this.Name, StringComparison.OrdinalIgnoreCase))
			{
				return false;
			}
			if (string.IsNullOrEmpty(this.value))
			{
				return string.IsNullOrEmpty(nameValueHeaderValue.value);
			}
			return string.Equals(nameValueHeaderValue.value, this.value, StringComparison.OrdinalIgnoreCase);
		}

		/// <summary>Converts a string to an <see cref="T:System.Net.Http.Headers.NameValueHeaderValue" /> instance.</summary>
		/// <param name="input">A string that represents name value header value information.</param>
		/// <returns>A <see cref="T:System.Net.Http.Headers.NameValueHeaderValue" /> instance.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="input" /> is a <see langword="null" /> reference.</exception>
		/// <exception cref="T:System.FormatException">
		///         <paramref name="input" /> is not valid name value header value information.</exception>
		// Token: 0x0600029B RID: 667 RVA: 0x0000A530 File Offset: 0x00008730
		public static NameValueHeaderValue Parse(string input)
		{
			NameValueHeaderValue result;
			if (NameValueHeaderValue.TryParse(input, out result))
			{
				return result;
			}
			throw new FormatException(input);
		}

		// Token: 0x0600029C RID: 668 RVA: 0x0000A54F File Offset: 0x0000874F
		internal static bool TryParsePragma(string input, int minimalCount, out List<NameValueHeaderValue> result)
		{
			return CollectionParser.TryParse<NameValueHeaderValue>(input, minimalCount, new ElementTryParser<NameValueHeaderValue>(NameValueHeaderValue.TryParseElement), out result);
		}

		// Token: 0x0600029D RID: 669 RVA: 0x0000A568 File Offset: 0x00008768
		internal static bool TryParseParameters(Lexer lexer, out List<NameValueHeaderValue> result, out Token t)
		{
			List<NameValueHeaderValue> list = new List<NameValueHeaderValue>();
			result = null;
			for (;;)
			{
				Token token = lexer.Scan(false);
				if (token != Token.Type.Token)
				{
					break;
				}
				string text = null;
				t = lexer.Scan(false);
				if (t == Token.Type.SeparatorEqual)
				{
					t = lexer.Scan(false);
					if (t != Token.Type.Token && t != Token.Type.QuotedString)
					{
						return false;
					}
					text = lexer.GetStringValue(t);
					t = lexer.Scan(false);
				}
				list.Add(new NameValueHeaderValue
				{
					Name = lexer.GetStringValue(token),
					value = text
				});
				if (t != Token.Type.SeparatorSemicolon)
				{
					goto Block_5;
				}
			}
			t = Token.Empty;
			return false;
			Block_5:
			result = list;
			return true;
		}

		/// <summary>Returns a string that represents the current <see cref="T:System.Net.Http.Headers.NameValueHeaderValue" /> object.</summary>
		/// <returns>A string that represents the current object.</returns>
		// Token: 0x0600029E RID: 670 RVA: 0x0000A632 File Offset: 0x00008832
		public override string ToString()
		{
			if (string.IsNullOrEmpty(this.value))
			{
				return this.Name;
			}
			return this.Name + "=" + this.value;
		}

		/// <summary>Determines whether a string is valid <see cref="T:System.Net.Http.Headers.NameValueHeaderValue" /> information.</summary>
		/// <param name="input">The string to validate.</param>
		/// <param name="parsedValue">The <see cref="T:System.Net.Http.Headers.NameValueHeaderValue" /> version of the string.</param>
		/// <returns>
		///     <see langword="true" /> if <paramref name="input" /> is valid <see cref="T:System.Net.Http.Headers.NameValueHeaderValue" /> information; otherwise, <see langword="false" />.</returns>
		// Token: 0x0600029F RID: 671 RVA: 0x0000A660 File Offset: 0x00008860
		public static bool TryParse(string input, out NameValueHeaderValue parsedValue)
		{
			Token token;
			if (NameValueHeaderValue.TryParseElement(new Lexer(input), out parsedValue, out token) && token == Token.Type.End)
			{
				return true;
			}
			parsedValue = null;
			return false;
		}

		// Token: 0x060002A0 RID: 672 RVA: 0x0000A68C File Offset: 0x0000888C
		private static bool TryParseElement(Lexer lexer, out NameValueHeaderValue parsedValue, out Token t)
		{
			parsedValue = null;
			t = lexer.Scan(false);
			if (t != Token.Type.Token)
			{
				return false;
			}
			parsedValue = new NameValueHeaderValue
			{
				Name = lexer.GetStringValue(t)
			};
			t = lexer.Scan(false);
			if (t == Token.Type.SeparatorEqual)
			{
				t = lexer.Scan(false);
				if (t != Token.Type.Token && t != Token.Type.QuotedString)
				{
					return false;
				}
				parsedValue.value = lexer.GetStringValue(t);
				t = lexer.Scan(false);
			}
			return true;
		}

		// Token: 0x04000136 RID: 310
		internal string value;

		// Token: 0x04000137 RID: 311
		[CompilerGenerated]
		private string <Name>k__BackingField;
	}
}
