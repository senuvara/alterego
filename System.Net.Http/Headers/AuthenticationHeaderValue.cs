﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace System.Net.Http.Headers
{
	/// <summary>Represents authentication information in Authorization, ProxyAuthorization, WWW-Authenticate, and Proxy-Authenticate header values.</summary>
	// Token: 0x02000027 RID: 39
	public class AuthenticationHeaderValue : ICloneable
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Net.Http.Headers.AuthenticationHeaderValue" /> class.</summary>
		/// <param name="scheme">The scheme to use for authorization.</param>
		// Token: 0x06000106 RID: 262 RVA: 0x00005B5D File Offset: 0x00003D5D
		public AuthenticationHeaderValue(string scheme) : this(scheme, null)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Net.Http.Headers.AuthenticationHeaderValue" /> class.</summary>
		/// <param name="scheme">The scheme to use for authorization.</param>
		/// <param name="parameter">The credentials containing the authentication information of the user agent for the resource being requested.</param>
		// Token: 0x06000107 RID: 263 RVA: 0x00005B67 File Offset: 0x00003D67
		public AuthenticationHeaderValue(string scheme, string parameter)
		{
			Parser.Token.Check(scheme);
			this.Scheme = scheme;
			this.Parameter = parameter;
		}

		// Token: 0x06000108 RID: 264 RVA: 0x000039D8 File Offset: 0x00001BD8
		private AuthenticationHeaderValue()
		{
		}

		/// <summary>Gets the credentials containing the authentication information of the user agent for the resource being requested.</summary>
		/// <returns>The credentials containing the authentication information.</returns>
		// Token: 0x17000034 RID: 52
		// (get) Token: 0x06000109 RID: 265 RVA: 0x00005B83 File Offset: 0x00003D83
		// (set) Token: 0x0600010A RID: 266 RVA: 0x00005B8B File Offset: 0x00003D8B
		public string Parameter
		{
			[CompilerGenerated]
			get
			{
				return this.<Parameter>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<Parameter>k__BackingField = value;
			}
		}

		/// <summary>Gets the scheme to use for authorization.</summary>
		/// <returns>The scheme to use for authorization.</returns>
		// Token: 0x17000035 RID: 53
		// (get) Token: 0x0600010B RID: 267 RVA: 0x00005B94 File Offset: 0x00003D94
		// (set) Token: 0x0600010C RID: 268 RVA: 0x00005B9C File Offset: 0x00003D9C
		public string Scheme
		{
			[CompilerGenerated]
			get
			{
				return this.<Scheme>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<Scheme>k__BackingField = value;
			}
		}

		/// <summary>Creates a new object that is a copy of the current <see cref="T:System.Net.Http.Headers.AuthenticationHeaderValue" /> instance.</summary>
		/// <returns>A copy of the current instance.</returns>
		// Token: 0x0600010D RID: 269 RVA: 0x00005BA5 File Offset: 0x00003DA5
		object ICloneable.Clone()
		{
			return base.MemberwiseClone();
		}

		/// <summary>Determines whether the specified <see cref="T:System.Object" /> is equal to the current <see cref="T:System.Net.Http.Headers.AuthenticationHeaderValue" /> object.</summary>
		/// <param name="obj">The object to compare with the current object. </param>
		/// <returns>
		///     <see langword="true" /> if the specified <see cref="T:System.Object" /> is equal to the current object; otherwise, <see langword="false" />.</returns>
		// Token: 0x0600010E RID: 270 RVA: 0x00005BB0 File Offset: 0x00003DB0
		public override bool Equals(object obj)
		{
			AuthenticationHeaderValue authenticationHeaderValue = obj as AuthenticationHeaderValue;
			return authenticationHeaderValue != null && string.Equals(authenticationHeaderValue.Scheme, this.Scheme, StringComparison.OrdinalIgnoreCase) && authenticationHeaderValue.Parameter == this.Parameter;
		}

		/// <summary>Serves as a hash function for an  <see cref="T:System.Net.Http.Headers.AuthenticationHeaderValue" /> object.</summary>
		/// <returns>A hash code for the current object.</returns>
		// Token: 0x0600010F RID: 271 RVA: 0x00005BF0 File Offset: 0x00003DF0
		public override int GetHashCode()
		{
			int num = this.Scheme.ToLowerInvariant().GetHashCode();
			if (!string.IsNullOrEmpty(this.Parameter))
			{
				num ^= this.Parameter.ToLowerInvariant().GetHashCode();
			}
			return num;
		}

		/// <summary>Converts a string to an <see cref="T:System.Net.Http.Headers.AuthenticationHeaderValue" /> instance.</summary>
		/// <param name="input">A string that represents authentication header value information.</param>
		/// <returns>An <see cref="T:System.Net.Http.Headers.AuthenticationHeaderValue" /> instance.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="input" /> is a <see langword="null" /> reference.</exception>
		/// <exception cref="T:System.FormatException">
		///         <paramref name="input" /> is not valid authentication header value information.</exception>
		// Token: 0x06000110 RID: 272 RVA: 0x00005C30 File Offset: 0x00003E30
		public static AuthenticationHeaderValue Parse(string input)
		{
			AuthenticationHeaderValue result;
			if (AuthenticationHeaderValue.TryParse(input, out result))
			{
				return result;
			}
			throw new FormatException(input);
		}

		/// <summary>Determines whether a string is valid <see cref="T:System.Net.Http.Headers.AuthenticationHeaderValue" /> information.</summary>
		/// <param name="input">The string to validate.</param>
		/// <param name="parsedValue">The <see cref="T:System.Net.Http.Headers.AuthenticationHeaderValue" /> version of the string.</param>
		/// <returns>
		///     <see langword="true" /> if <paramref name="input" /> is valid <see cref="T:System.Net.Http.Headers.AuthenticationHeaderValue" /> information; otherwise, <see langword="false" />.</returns>
		// Token: 0x06000111 RID: 273 RVA: 0x00005C50 File Offset: 0x00003E50
		public static bool TryParse(string input, out AuthenticationHeaderValue parsedValue)
		{
			Token token;
			if (AuthenticationHeaderValue.TryParseElement(new Lexer(input), out parsedValue, out token) && token == Token.Type.End)
			{
				return true;
			}
			parsedValue = null;
			return false;
		}

		// Token: 0x06000112 RID: 274 RVA: 0x00005C7C File Offset: 0x00003E7C
		internal static bool TryParse(string input, int minimalCount, out List<AuthenticationHeaderValue> result)
		{
			return CollectionParser.TryParse<AuthenticationHeaderValue>(input, minimalCount, new ElementTryParser<AuthenticationHeaderValue>(AuthenticationHeaderValue.TryParseElement), out result);
		}

		// Token: 0x06000113 RID: 275 RVA: 0x00005C94 File Offset: 0x00003E94
		private static bool TryParseElement(Lexer lexer, out AuthenticationHeaderValue parsedValue, out Token t)
		{
			t = lexer.Scan(false);
			if (t != Token.Type.Token)
			{
				parsedValue = null;
				return false;
			}
			parsedValue = new AuthenticationHeaderValue();
			parsedValue.Scheme = lexer.GetStringValue(t);
			t = lexer.Scan(false);
			if (t == Token.Type.Token)
			{
				parsedValue.Parameter = lexer.GetRemainingStringValue(t.StartPosition);
				t = new Token(Token.Type.End, 0, 0);
			}
			return true;
		}

		/// <summary>Returns a string that represents the current <see cref="T:System.Net.Http.Headers.AuthenticationHeaderValue" /> object.</summary>
		/// <returns>A string that represents the current object.</returns>
		// Token: 0x06000114 RID: 276 RVA: 0x00005D18 File Offset: 0x00003F18
		public override string ToString()
		{
			if (this.Parameter == null)
			{
				return this.Scheme;
			}
			return this.Scheme + " " + this.Parameter;
		}

		// Token: 0x040000D8 RID: 216
		[CompilerGenerated]
		private string <Parameter>k__BackingField;

		// Token: 0x040000D9 RID: 217
		[CompilerGenerated]
		private string <Scheme>k__BackingField;
	}
}
