﻿using System;
using System.Collections.Generic;

namespace System.Net.Http.Headers
{
	// Token: 0x0200002B RID: 43
	internal static class CollectionParser
	{
		// Token: 0x06000141 RID: 321 RVA: 0x00006BA8 File Offset: 0x00004DA8
		public static bool TryParse<T>(string input, int minimalCount, ElementTryParser<T> parser, out List<T> result) where T : class
		{
			Lexer lexer = new Lexer(input);
			result = new List<T>();
			T t;
			Token token;
			while (parser(lexer, out t, out token))
			{
				if (t != null)
				{
					result.Add(t);
				}
				if (token != Token.Type.SeparatorComma)
				{
					if (token != Token.Type.End)
					{
						result = null;
						return false;
					}
					if (minimalCount > result.Count)
					{
						result = null;
						return false;
					}
					return true;
				}
			}
			return false;
		}

		// Token: 0x06000142 RID: 322 RVA: 0x00006C09 File Offset: 0x00004E09
		public static bool TryParse(string input, int minimalCount, out List<string> result)
		{
			return CollectionParser.TryParse<string>(input, minimalCount, new ElementTryParser<string>(CollectionParser.TryParseStringElement), out result);
		}

		// Token: 0x06000143 RID: 323 RVA: 0x00006C1F File Offset: 0x00004E1F
		public static bool TryParseRepetition(string input, int minimalCount, out List<string> result)
		{
			return CollectionParser.TryParseRepetition<string>(input, minimalCount, new ElementTryParser<string>(CollectionParser.TryParseStringElement), out result);
		}

		// Token: 0x06000144 RID: 324 RVA: 0x00006C38 File Offset: 0x00004E38
		private static bool TryParseStringElement(Lexer lexer, out string parsedValue, out Token t)
		{
			t = lexer.Scan(false);
			if (t == Token.Type.Token)
			{
				parsedValue = lexer.GetStringValue(t);
				if (parsedValue.Length == 0)
				{
					parsedValue = null;
				}
				t = lexer.Scan(false);
			}
			else
			{
				parsedValue = null;
			}
			return true;
		}

		// Token: 0x06000145 RID: 325 RVA: 0x00006C90 File Offset: 0x00004E90
		public static bool TryParseRepetition<T>(string input, int minimalCount, ElementTryParser<T> parser, out List<T> result) where T : class
		{
			Lexer lexer = new Lexer(input);
			result = new List<T>();
			T t;
			Token token;
			while (parser(lexer, out t, out token))
			{
				if (t != null)
				{
					result.Add(t);
				}
				if (token == Token.Type.End)
				{
					return minimalCount <= result.Count;
				}
			}
			return false;
		}
	}
}
