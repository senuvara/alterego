﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000002 RID: 2
	internal interface INativeFacebookStore : INativeStore
	{
		// Token: 0x06000001 RID: 1
		bool Check();

		// Token: 0x06000002 RID: 2
		void Init();

		// Token: 0x06000003 RID: 3
		void SetUnityPurchasingCallback(UnityPurchasingCallback AsyncCallback);

		// Token: 0x06000004 RID: 4
		bool ConsumeItem(string purchaseToken);
	}
}
