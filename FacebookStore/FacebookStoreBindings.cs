﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000003 RID: 3
	internal class FacebookStoreBindings : INativeFacebookStore, INativeStore
	{
		// Token: 0x06000005 RID: 5 RVA: 0x00002050 File Offset: 0x00000250
		public bool Check()
		{
			return false;
		}

		// Token: 0x06000006 RID: 6 RVA: 0x00002063 File Offset: 0x00000263
		public void Init()
		{
			throw new NotImplementedException();
		}

		// Token: 0x06000007 RID: 7 RVA: 0x00002063 File Offset: 0x00000263
		public void SetUnityPurchasingCallback(UnityPurchasingCallback AsyncCallback)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06000008 RID: 8 RVA: 0x00002063 File Offset: 0x00000263
		public void RetrieveProducts(string json)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06000009 RID: 9 RVA: 0x00002063 File Offset: 0x00000263
		public void Purchase(string productJSON, string developerPayload)
		{
			throw new NotImplementedException();
		}

		// Token: 0x0600000A RID: 10 RVA: 0x00002063 File Offset: 0x00000263
		public void FinishTransaction(string productJSON, string transactionID)
		{
			throw new NotImplementedException();
		}

		// Token: 0x0600000B RID: 11 RVA: 0x00002063 File Offset: 0x00000263
		public bool ConsumeItem(string purchaseToken)
		{
			throw new NotImplementedException();
		}

		// Token: 0x0600000C RID: 12 RVA: 0x0000206B File Offset: 0x0000026B
		public FacebookStoreBindings()
		{
		}
	}
}
