﻿using System;

namespace System.Runtime
{
	// Token: 0x02000013 RID: 19
	internal class CompletedAsyncResult : AsyncResult
	{
		// Token: 0x06000073 RID: 115 RVA: 0x00003282 File Offset: 0x00001482
		public CompletedAsyncResult(AsyncCallback callback, object state) : base(callback, state)
		{
			base.Complete(true);
		}

		// Token: 0x06000074 RID: 116 RVA: 0x00003293 File Offset: 0x00001493
		public static void End(IAsyncResult result)
		{
			Fx.AssertAndThrowFatal(result.IsCompleted, "CompletedAsyncResult was not completed!");
			AsyncResult.End<CompletedAsyncResult>(result);
		}
	}
}
