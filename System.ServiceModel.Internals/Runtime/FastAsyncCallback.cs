﻿using System;

namespace System.Runtime
{
	// Token: 0x0200001A RID: 26
	// (Invoke) Token: 0x06000095 RID: 149
	internal delegate void FastAsyncCallback(object state, Exception asyncException);
}
