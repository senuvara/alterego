﻿using System;
using System.Runtime.CompilerServices;

namespace System.Runtime
{
	// Token: 0x0200003A RID: 58
	internal static class WaitCallbackActionItem
	{
		// Token: 0x1700003C RID: 60
		// (get) Token: 0x060001BB RID: 443 RVA: 0x00007BE3 File Offset: 0x00005DE3
		// (set) Token: 0x060001BC RID: 444 RVA: 0x00007BEA File Offset: 0x00005DEA
		internal static bool ShouldUseActivity
		{
			[CompilerGenerated]
			get
			{
				return WaitCallbackActionItem.<ShouldUseActivity>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				WaitCallbackActionItem.<ShouldUseActivity>k__BackingField = value;
			}
		}

		// Token: 0x04000119 RID: 281
		[CompilerGenerated]
		private static bool <ShouldUseActivity>k__BackingField;
	}
}
