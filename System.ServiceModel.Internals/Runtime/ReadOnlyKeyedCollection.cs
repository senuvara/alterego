﻿using System;
using System.Collections.ObjectModel;

namespace System.Runtime
{
	// Token: 0x02000029 RID: 41
	internal class ReadOnlyKeyedCollection<TKey, TValue> : ReadOnlyCollection<TValue>
	{
		// Token: 0x06000134 RID: 308 RVA: 0x00005830 File Offset: 0x00003A30
		public ReadOnlyKeyedCollection(KeyedCollection<TKey, TValue> innerCollection) : base(innerCollection)
		{
			this.innerCollection = innerCollection;
		}

		// Token: 0x1700002E RID: 46
		public TValue this[TKey key]
		{
			get
			{
				return this.innerCollection[key];
			}
		}

		// Token: 0x040000CE RID: 206
		private KeyedCollection<TKey, TValue> innerCollection;
	}
}
