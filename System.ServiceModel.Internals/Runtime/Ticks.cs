﻿using System;
using System.Runtime.Interop;
using System.Security;

namespace System.Runtime
{
	// Token: 0x02000030 RID: 48
	internal static class Ticks
	{
		// Token: 0x17000034 RID: 52
		// (get) Token: 0x0600016D RID: 365 RVA: 0x0000652C File Offset: 0x0000472C
		public static long Now
		{
			[SecuritySafeCritical]
			get
			{
				long result;
				UnsafeNativeMethods.GetSystemTimeAsFileTime(out result);
				return result;
			}
		}

		// Token: 0x0600016E RID: 366 RVA: 0x00006541 File Offset: 0x00004741
		public static long FromMilliseconds(int milliseconds)
		{
			return checked(unchecked((long)milliseconds) * 10000L);
		}

		// Token: 0x0600016F RID: 367 RVA: 0x0000654C File Offset: 0x0000474C
		public static int ToMilliseconds(long ticks)
		{
			return checked((int)(ticks / 10000L));
		}

		// Token: 0x06000170 RID: 368 RVA: 0x00006557 File Offset: 0x00004757
		public static long FromTimeSpan(TimeSpan duration)
		{
			return duration.Ticks;
		}

		// Token: 0x06000171 RID: 369 RVA: 0x00006560 File Offset: 0x00004760
		public static TimeSpan ToTimeSpan(long ticks)
		{
			return new TimeSpan(ticks);
		}

		// Token: 0x06000172 RID: 370 RVA: 0x00006568 File Offset: 0x00004768
		public static long Add(long firstTicks, long secondTicks)
		{
			if (firstTicks == 9223372036854775807L || firstTicks == -9223372036854775808L)
			{
				return firstTicks;
			}
			if (secondTicks == 9223372036854775807L || secondTicks == -9223372036854775808L)
			{
				return secondTicks;
			}
			if (firstTicks >= 0L && 9223372036854775807L - firstTicks <= secondTicks)
			{
				return 9223372036854775806L;
			}
			if (firstTicks <= 0L && -9223372036854775808L - firstTicks >= secondTicks)
			{
				return -9223372036854775807L;
			}
			return checked(firstTicks + secondTicks);
		}
	}
}
