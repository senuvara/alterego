﻿using System;
using System.Threading;

namespace System.Runtime
{
	// Token: 0x02000020 RID: 32
	internal class IOThreadCancellationTokenSource : IDisposable
	{
		// Token: 0x060000C0 RID: 192 RVA: 0x000040D4 File Offset: 0x000022D4
		public IOThreadCancellationTokenSource(TimeSpan timeout)
		{
			TimeoutHelper.ThrowIfNegativeArgument(timeout);
			this.timeout = timeout;
		}

		// Token: 0x060000C1 RID: 193 RVA: 0x000040E9 File Offset: 0x000022E9
		public IOThreadCancellationTokenSource(int timeout) : this(TimeSpan.FromMilliseconds((double)timeout))
		{
		}

		// Token: 0x1700001E RID: 30
		// (get) Token: 0x060000C2 RID: 194 RVA: 0x000040F8 File Offset: 0x000022F8
		public CancellationToken Token
		{
			get
			{
				if (this.token == null)
				{
					if (this.timeout >= TimeoutHelper.MaxWait)
					{
						this.token = new CancellationToken?(CancellationToken.None);
					}
					else
					{
						this.timer = new IOThreadTimer(IOThreadCancellationTokenSource.onCancel, this, true);
						this.source = new CancellationTokenSource();
						this.timer.Set(this.timeout);
						this.token = new CancellationToken?(this.source.Token);
					}
				}
				return this.token.Value;
			}
		}

		// Token: 0x060000C3 RID: 195 RVA: 0x00004185 File Offset: 0x00002385
		public void Dispose()
		{
			if (this.source != null && this.timer.Cancel())
			{
				this.source.Dispose();
				this.source = null;
			}
		}

		// Token: 0x060000C4 RID: 196 RVA: 0x000041AE File Offset: 0x000023AE
		private static void OnCancel(object obj)
		{
			((IOThreadCancellationTokenSource)obj).Cancel();
		}

		// Token: 0x060000C5 RID: 197 RVA: 0x000041BB File Offset: 0x000023BB
		private void Cancel()
		{
			this.source.Cancel();
			this.source.Dispose();
			this.source = null;
		}

		// Token: 0x060000C6 RID: 198 RVA: 0x000041DA File Offset: 0x000023DA
		// Note: this type is marked as 'beforefieldinit'.
		static IOThreadCancellationTokenSource()
		{
		}

		// Token: 0x040000A3 RID: 163
		private static readonly Action<object> onCancel = Fx.ThunkCallback<object>(new Action<object>(IOThreadCancellationTokenSource.OnCancel));

		// Token: 0x040000A4 RID: 164
		private readonly TimeSpan timeout;

		// Token: 0x040000A5 RID: 165
		private CancellationTokenSource source;

		// Token: 0x040000A6 RID: 166
		private CancellationToken? token;

		// Token: 0x040000A7 RID: 167
		private IOThreadTimer timer;
	}
}
