﻿using System;
using System.Threading;

namespace System.Runtime
{
	// Token: 0x0200002B RID: 43
	internal class SignalGate
	{
		// Token: 0x0600013C RID: 316 RVA: 0x000058C8 File Offset: 0x00003AC8
		public SignalGate()
		{
		}

		// Token: 0x1700002F RID: 47
		// (get) Token: 0x0600013D RID: 317 RVA: 0x000058D0 File Offset: 0x00003AD0
		internal bool IsLocked
		{
			get
			{
				return this.state == 0;
			}
		}

		// Token: 0x17000030 RID: 48
		// (get) Token: 0x0600013E RID: 318 RVA: 0x000058DB File Offset: 0x00003ADB
		internal bool IsSignalled
		{
			get
			{
				return this.state == 3;
			}
		}

		// Token: 0x0600013F RID: 319 RVA: 0x000058E8 File Offset: 0x00003AE8
		public bool Signal()
		{
			int num = this.state;
			if (num == 0)
			{
				num = Interlocked.CompareExchange(ref this.state, 1, 0);
			}
			if (num == 2)
			{
				this.state = 3;
				return true;
			}
			if (num != 0)
			{
				this.ThrowInvalidSignalGateState();
			}
			return false;
		}

		// Token: 0x06000140 RID: 320 RVA: 0x00005924 File Offset: 0x00003B24
		public bool Unlock()
		{
			int num = this.state;
			if (num == 0)
			{
				num = Interlocked.CompareExchange(ref this.state, 2, 0);
			}
			if (num == 1)
			{
				this.state = 3;
				return true;
			}
			if (num != 0)
			{
				this.ThrowInvalidSignalGateState();
			}
			return false;
		}

		// Token: 0x06000141 RID: 321 RVA: 0x00005960 File Offset: 0x00003B60
		private void ThrowInvalidSignalGateState()
		{
			throw Fx.Exception.AsError(new InvalidOperationException("Invalid Semaphore Exit"));
		}

		// Token: 0x040000D0 RID: 208
		private int state;

		// Token: 0x02000081 RID: 129
		private static class GateState
		{
			// Token: 0x040002BD RID: 701
			public const int Locked = 0;

			// Token: 0x040002BE RID: 702
			public const int SignalPending = 1;

			// Token: 0x040002BF RID: 703
			public const int Unlocked = 2;

			// Token: 0x040002C0 RID: 704
			public const int Signalled = 3;
		}
	}
}
