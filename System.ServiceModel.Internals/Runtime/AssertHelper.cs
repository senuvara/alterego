﻿using System;
using System.Diagnostics;

namespace System.Runtime
{
	// Token: 0x02000008 RID: 8
	internal static class AssertHelper
	{
		// Token: 0x06000014 RID: 20 RVA: 0x0000218C File Offset: 0x0000038C
		internal static void FireAssert(string message)
		{
			try
			{
			}
			finally
			{
				Debug.Assert(false, message);
			}
		}
	}
}
