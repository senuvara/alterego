﻿using System;

namespace System.Runtime
{
	// Token: 0x0200002C RID: 44
	internal class SignalGate<T> : SignalGate
	{
		// Token: 0x06000142 RID: 322 RVA: 0x00005976 File Offset: 0x00003B76
		public SignalGate()
		{
		}

		// Token: 0x06000143 RID: 323 RVA: 0x0000597E File Offset: 0x00003B7E
		public bool Signal(T result)
		{
			this.result = result;
			return base.Signal();
		}

		// Token: 0x06000144 RID: 324 RVA: 0x0000598D File Offset: 0x00003B8D
		public bool Unlock(out T result)
		{
			if (base.Unlock())
			{
				result = this.result;
				return true;
			}
			result = default(T);
			return false;
		}

		// Token: 0x040000D1 RID: 209
		private T result;
	}
}
