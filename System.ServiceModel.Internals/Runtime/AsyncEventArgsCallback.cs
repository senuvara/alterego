﻿using System;

namespace System.Runtime
{
	// Token: 0x0200000D RID: 13
	// (Invoke) Token: 0x06000024 RID: 36
	internal delegate void AsyncEventArgsCallback(IAsyncEventArgs eventArgs);
}
