﻿using System;

namespace System.Runtime
{
	// Token: 0x02000014 RID: 20
	internal class CompletedAsyncResult<T> : AsyncResult
	{
		// Token: 0x06000075 RID: 117 RVA: 0x000032AC File Offset: 0x000014AC
		public CompletedAsyncResult(T data, AsyncCallback callback, object state) : base(callback, state)
		{
			this.data = data;
			base.Complete(true);
		}

		// Token: 0x06000076 RID: 118 RVA: 0x000032C4 File Offset: 0x000014C4
		public static T End(IAsyncResult result)
		{
			Fx.AssertAndThrowFatal(result.IsCompleted, "CompletedAsyncResult<T> was not completed!");
			return AsyncResult.End<CompletedAsyncResult<T>>(result).data;
		}

		// Token: 0x04000075 RID: 117
		private T data;
	}
}
