﻿using System;

namespace System.Runtime
{
	// Token: 0x02000034 RID: 52
	internal enum TraceEventOpcode
	{
		// Token: 0x040000F5 RID: 245
		Info,
		// Token: 0x040000F6 RID: 246
		Start,
		// Token: 0x040000F7 RID: 247
		Stop,
		// Token: 0x040000F8 RID: 248
		Reply = 6,
		// Token: 0x040000F9 RID: 249
		Resume,
		// Token: 0x040000FA RID: 250
		Suspend,
		// Token: 0x040000FB RID: 251
		Send,
		// Token: 0x040000FC RID: 252
		Receive = 240
	}
}
