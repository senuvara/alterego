﻿using System;
using System.Collections.Generic;

namespace System.Runtime
{
	// Token: 0x02000018 RID: 24
	internal class DuplicateDetector<T> where T : class
	{
		// Token: 0x06000079 RID: 121 RVA: 0x0000333A File Offset: 0x0000153A
		public DuplicateDetector(int capacity)
		{
			this.capacity = capacity;
			this.items = new Dictionary<T, LinkedListNode<T>>();
			this.fifoList = new LinkedList<T>();
			this.thisLock = new object();
		}

		// Token: 0x0600007A RID: 122 RVA: 0x0000336C File Offset: 0x0000156C
		public bool AddIfNotDuplicate(T value)
		{
			bool result = false;
			object obj = this.thisLock;
			lock (obj)
			{
				if (!this.items.ContainsKey(value))
				{
					this.Add(value);
					result = true;
				}
			}
			return result;
		}

		// Token: 0x0600007B RID: 123 RVA: 0x000033C0 File Offset: 0x000015C0
		private void Add(T value)
		{
			if (this.items.Count == this.capacity)
			{
				LinkedListNode<T> last = this.fifoList.Last;
				this.items.Remove(last.Value);
				this.fifoList.Remove(last);
			}
			this.items.Add(value, this.fifoList.AddFirst(value));
		}

		// Token: 0x0600007C RID: 124 RVA: 0x00003424 File Offset: 0x00001624
		public bool Remove(T value)
		{
			bool result = false;
			object obj = this.thisLock;
			lock (obj)
			{
				LinkedListNode<T> node;
				if (this.items.TryGetValue(value, out node))
				{
					this.items.Remove(value);
					this.fifoList.Remove(node);
					result = true;
				}
			}
			return result;
		}

		// Token: 0x0600007D RID: 125 RVA: 0x0000348C File Offset: 0x0000168C
		public void Clear()
		{
			object obj = this.thisLock;
			lock (obj)
			{
				this.fifoList.Clear();
				this.items.Clear();
			}
		}

		// Token: 0x04000098 RID: 152
		private LinkedList<T> fifoList;

		// Token: 0x04000099 RID: 153
		private Dictionary<T, LinkedListNode<T>> items;

		// Token: 0x0400009A RID: 154
		private int capacity;

		// Token: 0x0400009B RID: 155
		private object thisLock;
	}
}
