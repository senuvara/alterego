﻿using System;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Security;
using System.Security.Permissions;

namespace System.Runtime
{
	// Token: 0x02000027 RID: 39
	internal static class PartialTrustHelpers
	{
		// Token: 0x17000027 RID: 39
		// (get) Token: 0x06000117 RID: 279 RVA: 0x00005680 File Offset: 0x00003880
		internal static bool ShouldFlowSecurityContext
		{
			[SecurityCritical]
			get
			{
				return SecurityManager.CurrentThreadRequiresSecurityContextCapture();
			}
		}

		// Token: 0x06000118 RID: 280 RVA: 0x00005687 File Offset: 0x00003887
		[SecurityCritical]
		internal static bool IsInFullTrust()
		{
			return true;
		}

		// Token: 0x06000119 RID: 281 RVA: 0x0000568C File Offset: 0x0000388C
		[SecurityCritical]
		internal static bool IsTypeAptca(Type type)
		{
			Assembly assembly = type.Assembly;
			return PartialTrustHelpers.IsAssemblyAptca(assembly) || !PartialTrustHelpers.IsAssemblySigned(assembly);
		}

		// Token: 0x0600011A RID: 282 RVA: 0x000056B3 File Offset: 0x000038B3
		[SecuritySafeCritical]
		[PermissionSet(SecurityAction.Demand, Unrestricted = true)]
		[MethodImpl(MethodImplOptions.NoInlining)]
		internal static void DemandForFullTrust()
		{
		}

		// Token: 0x0600011B RID: 283 RVA: 0x000056B5 File Offset: 0x000038B5
		[SecurityCritical]
		private static bool IsAssemblyAptca(Assembly assembly)
		{
			if (PartialTrustHelpers.aptca == null)
			{
				PartialTrustHelpers.aptca = typeof(AllowPartiallyTrustedCallersAttribute);
			}
			return assembly.GetCustomAttributes(PartialTrustHelpers.aptca, false).Length != 0;
		}

		// Token: 0x0600011C RID: 284 RVA: 0x000056E4 File Offset: 0x000038E4
		[SecurityCritical]
		[FileIOPermission(SecurityAction.Assert, Unrestricted = true)]
		private static bool IsAssemblySigned(Assembly assembly)
		{
			byte[] publicKeyToken = assembly.GetName().GetPublicKeyToken();
			return publicKeyToken != null & publicKeyToken.Length != 0;
		}

		// Token: 0x0600011D RID: 285 RVA: 0x00005707 File Offset: 0x00003907
		[SecurityCritical]
		internal static bool CheckAppDomainPermissions(PermissionSet permissions)
		{
			return true;
		}

		// Token: 0x0600011E RID: 286 RVA: 0x0000570A File Offset: 0x0000390A
		[SecurityCritical]
		internal static bool HasEtwPermissions()
		{
			return true;
		}

		// Token: 0x17000028 RID: 40
		// (get) Token: 0x0600011F RID: 287 RVA: 0x0000570D File Offset: 0x0000390D
		internal static bool AppDomainFullyTrusted
		{
			[SecuritySafeCritical]
			get
			{
				return true;
			}
		}

		// Token: 0x040000CA RID: 202
		[SecurityCritical]
		private static Type aptca;

		// Token: 0x040000CB RID: 203
		[SecurityCritical]
		private static volatile bool checkedForFullTrust;

		// Token: 0x040000CC RID: 204
		[SecurityCritical]
		private static bool inFullTrust;
	}
}
