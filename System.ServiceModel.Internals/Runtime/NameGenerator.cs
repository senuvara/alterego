﻿using System;
using System.Globalization;
using System.Threading;

namespace System.Runtime
{
	// Token: 0x02000026 RID: 38
	internal class NameGenerator
	{
		// Token: 0x06000114 RID: 276 RVA: 0x000055F8 File Offset: 0x000037F8
		private NameGenerator()
		{
			this.prefix = "_" + Guid.NewGuid().ToString().Replace('-', '_') + "_";
		}

		// Token: 0x06000115 RID: 277 RVA: 0x0000563C File Offset: 0x0000383C
		public static string Next()
		{
			long num = Interlocked.Increment(ref NameGenerator.nameGenerator.id);
			return NameGenerator.nameGenerator.prefix + num.ToString(CultureInfo.InvariantCulture);
		}

		// Token: 0x06000116 RID: 278 RVA: 0x00005674 File Offset: 0x00003874
		// Note: this type is marked as 'beforefieldinit'.
		static NameGenerator()
		{
		}

		// Token: 0x040000C7 RID: 199
		private static NameGenerator nameGenerator = new NameGenerator();

		// Token: 0x040000C8 RID: 200
		private long id;

		// Token: 0x040000C9 RID: 201
		private string prefix;
	}
}
