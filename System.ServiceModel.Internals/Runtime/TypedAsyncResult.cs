﻿using System;

namespace System.Runtime
{
	// Token: 0x02000038 RID: 56
	internal abstract class TypedAsyncResult<T> : AsyncResult
	{
		// Token: 0x060001A5 RID: 421 RVA: 0x000075DC File Offset: 0x000057DC
		public TypedAsyncResult(AsyncCallback callback, object state) : base(callback, state)
		{
		}

		// Token: 0x1700003B RID: 59
		// (get) Token: 0x060001A6 RID: 422 RVA: 0x000075E6 File Offset: 0x000057E6
		public T Data
		{
			get
			{
				return this.data;
			}
		}

		// Token: 0x060001A7 RID: 423 RVA: 0x000075EE File Offset: 0x000057EE
		protected void Complete(T data, bool completedSynchronously)
		{
			this.data = data;
			base.Complete(completedSynchronously);
		}

		// Token: 0x060001A8 RID: 424 RVA: 0x000075FE File Offset: 0x000057FE
		public static T End(IAsyncResult result)
		{
			return AsyncResult.End<TypedAsyncResult<T>>(result).Data;
		}

		// Token: 0x04000118 RID: 280
		private T data;
	}
}
