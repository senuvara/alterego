﻿using System;

namespace System.Runtime.Collections
{
	// Token: 0x02000055 RID: 85
	internal class ObjectCacheSettings
	{
		// Token: 0x06000310 RID: 784 RVA: 0x000105E4 File Offset: 0x0000E7E4
		public ObjectCacheSettings()
		{
			this.CacheLimit = 64;
			this.IdleTimeout = ObjectCacheSettings.DefaultIdleTimeout;
			this.LeaseTimeout = ObjectCacheSettings.DefaultLeaseTimeout;
			this.PurgeFrequency = 32;
		}

		// Token: 0x06000311 RID: 785 RVA: 0x00010612 File Offset: 0x0000E812
		private ObjectCacheSettings(ObjectCacheSettings other)
		{
			this.CacheLimit = other.CacheLimit;
			this.IdleTimeout = other.IdleTimeout;
			this.LeaseTimeout = other.LeaseTimeout;
			this.PurgeFrequency = other.PurgeFrequency;
		}

		// Token: 0x06000312 RID: 786 RVA: 0x0001064A File Offset: 0x0000E84A
		internal ObjectCacheSettings Clone()
		{
			return new ObjectCacheSettings(this);
		}

		// Token: 0x17000068 RID: 104
		// (get) Token: 0x06000313 RID: 787 RVA: 0x00010652 File Offset: 0x0000E852
		// (set) Token: 0x06000314 RID: 788 RVA: 0x0001065A File Offset: 0x0000E85A
		public int CacheLimit
		{
			get
			{
				return this.cacheLimit;
			}
			set
			{
				this.cacheLimit = value;
			}
		}

		// Token: 0x17000069 RID: 105
		// (get) Token: 0x06000315 RID: 789 RVA: 0x00010663 File Offset: 0x0000E863
		// (set) Token: 0x06000316 RID: 790 RVA: 0x0001066B File Offset: 0x0000E86B
		public TimeSpan IdleTimeout
		{
			get
			{
				return this.idleTimeout;
			}
			set
			{
				this.idleTimeout = value;
			}
		}

		// Token: 0x1700006A RID: 106
		// (get) Token: 0x06000317 RID: 791 RVA: 0x00010674 File Offset: 0x0000E874
		// (set) Token: 0x06000318 RID: 792 RVA: 0x0001067C File Offset: 0x0000E87C
		public TimeSpan LeaseTimeout
		{
			get
			{
				return this.leaseTimeout;
			}
			set
			{
				this.leaseTimeout = value;
			}
		}

		// Token: 0x1700006B RID: 107
		// (get) Token: 0x06000319 RID: 793 RVA: 0x00010685 File Offset: 0x0000E885
		// (set) Token: 0x0600031A RID: 794 RVA: 0x0001068D File Offset: 0x0000E88D
		public int PurgeFrequency
		{
			get
			{
				return this.purgeFrequency;
			}
			set
			{
				this.purgeFrequency = value;
			}
		}

		// Token: 0x0600031B RID: 795 RVA: 0x00010696 File Offset: 0x0000E896
		// Note: this type is marked as 'beforefieldinit'.
		static ObjectCacheSettings()
		{
		}

		// Token: 0x04000202 RID: 514
		private int cacheLimit;

		// Token: 0x04000203 RID: 515
		private TimeSpan idleTimeout;

		// Token: 0x04000204 RID: 516
		private TimeSpan leaseTimeout;

		// Token: 0x04000205 RID: 517
		private int purgeFrequency;

		// Token: 0x04000206 RID: 518
		private const int DefaultCacheLimit = 64;

		// Token: 0x04000207 RID: 519
		private const int DefaultPurgeFrequency = 32;

		// Token: 0x04000208 RID: 520
		private static TimeSpan DefaultIdleTimeout = TimeSpan.FromMinutes(2.0);

		// Token: 0x04000209 RID: 521
		private static TimeSpan DefaultLeaseTimeout = TimeSpan.FromMinutes(5.0);
	}
}
