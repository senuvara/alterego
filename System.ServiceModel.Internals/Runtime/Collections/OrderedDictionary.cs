﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace System.Runtime.Collections
{
	// Token: 0x02000056 RID: 86
	internal class OrderedDictionary<TKey, TValue> : IDictionary<TKey, TValue>, ICollection<KeyValuePair<TKey, TValue>>, IEnumerable<KeyValuePair<TKey, TValue>>, IEnumerable, IDictionary, ICollection
	{
		// Token: 0x0600031C RID: 796 RVA: 0x000106BE File Offset: 0x0000E8BE
		public OrderedDictionary()
		{
			this.privateDictionary = new OrderedDictionary();
		}

		// Token: 0x0600031D RID: 797 RVA: 0x000106D4 File Offset: 0x0000E8D4
		public OrderedDictionary(IDictionary<TKey, TValue> dictionary)
		{
			if (dictionary != null)
			{
				this.privateDictionary = new OrderedDictionary();
				foreach (KeyValuePair<TKey, TValue> keyValuePair in dictionary)
				{
					this.privateDictionary.Add(keyValuePair.Key, keyValuePair.Value);
				}
			}
		}

		// Token: 0x1700006C RID: 108
		// (get) Token: 0x0600031E RID: 798 RVA: 0x0001074C File Offset: 0x0000E94C
		public int Count
		{
			get
			{
				return this.privateDictionary.Count;
			}
		}

		// Token: 0x1700006D RID: 109
		// (get) Token: 0x0600031F RID: 799 RVA: 0x00010759 File Offset: 0x0000E959
		public bool IsReadOnly
		{
			get
			{
				return false;
			}
		}

		// Token: 0x1700006E RID: 110
		public TValue this[TKey key]
		{
			get
			{
				if (key == null)
				{
					throw Fx.Exception.ArgumentNull("key");
				}
				if (this.privateDictionary.Contains(key))
				{
					return (TValue)((object)this.privateDictionary[key]);
				}
				throw Fx.Exception.AsError(new KeyNotFoundException("Key Not Found In Dictionary"));
			}
			set
			{
				if (key == null)
				{
					throw Fx.Exception.ArgumentNull("key");
				}
				this.privateDictionary[key] = value;
			}
		}

		// Token: 0x1700006F RID: 111
		// (get) Token: 0x06000322 RID: 802 RVA: 0x000107F0 File Offset: 0x0000E9F0
		public ICollection<TKey> Keys
		{
			get
			{
				List<TKey> list = new List<TKey>(this.privateDictionary.Count);
				foreach (object obj in this.privateDictionary.Keys)
				{
					TKey item = (TKey)((object)obj);
					list.Add(item);
				}
				return list;
			}
		}

		// Token: 0x17000070 RID: 112
		// (get) Token: 0x06000323 RID: 803 RVA: 0x00010860 File Offset: 0x0000EA60
		public ICollection<TValue> Values
		{
			get
			{
				List<TValue> list = new List<TValue>(this.privateDictionary.Count);
				foreach (object obj in this.privateDictionary.Values)
				{
					TValue item = (TValue)((object)obj);
					list.Add(item);
				}
				return list;
			}
		}

		// Token: 0x06000324 RID: 804 RVA: 0x000108D0 File Offset: 0x0000EAD0
		public void Add(KeyValuePair<TKey, TValue> item)
		{
			this.Add(item.Key, item.Value);
		}

		// Token: 0x06000325 RID: 805 RVA: 0x000108E6 File Offset: 0x0000EAE6
		public void Add(TKey key, TValue value)
		{
			if (key == null)
			{
				throw Fx.Exception.ArgumentNull("key");
			}
			this.privateDictionary.Add(key, value);
		}

		// Token: 0x06000326 RID: 806 RVA: 0x00010917 File Offset: 0x0000EB17
		public void Clear()
		{
			this.privateDictionary.Clear();
		}

		// Token: 0x06000327 RID: 807 RVA: 0x00010924 File Offset: 0x0000EB24
		public bool Contains(KeyValuePair<TKey, TValue> item)
		{
			return item.Key != null && this.privateDictionary.Contains(item.Key) && this.privateDictionary[item.Key].Equals(item.Value);
		}

		// Token: 0x06000328 RID: 808 RVA: 0x00010982 File Offset: 0x0000EB82
		public bool ContainsKey(TKey key)
		{
			if (key == null)
			{
				throw Fx.Exception.ArgumentNull("key");
			}
			return this.privateDictionary.Contains(key);
		}

		// Token: 0x06000329 RID: 809 RVA: 0x000109B0 File Offset: 0x0000EBB0
		public void CopyTo(KeyValuePair<TKey, TValue>[] array, int arrayIndex)
		{
			if (array == null)
			{
				throw Fx.Exception.ArgumentNull("array");
			}
			if (arrayIndex < 0)
			{
				throw Fx.Exception.AsError(new ArgumentOutOfRangeException("arrayIndex"));
			}
			if (array.Rank > 1 || arrayIndex >= array.Length || array.Length - arrayIndex < this.privateDictionary.Count)
			{
				throw Fx.Exception.Argument("array", "Bad Copy To Array");
			}
			int num = arrayIndex;
			foreach (object obj in this.privateDictionary)
			{
				DictionaryEntry dictionaryEntry = (DictionaryEntry)obj;
				array[num] = new KeyValuePair<TKey, TValue>((TKey)((object)dictionaryEntry.Key), (TValue)((object)dictionaryEntry.Value));
				num++;
			}
		}

		// Token: 0x0600032A RID: 810 RVA: 0x00010A90 File Offset: 0x0000EC90
		public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
		{
			foreach (object obj in this.privateDictionary)
			{
				DictionaryEntry dictionaryEntry = (DictionaryEntry)obj;
				yield return new KeyValuePair<TKey, TValue>((TKey)((object)dictionaryEntry.Key), (TValue)((object)dictionaryEntry.Value));
			}
			IDictionaryEnumerator dictionaryEnumerator = null;
			yield break;
			yield break;
		}

		// Token: 0x0600032B RID: 811 RVA: 0x00010A9F File Offset: 0x0000EC9F
		IEnumerator IEnumerable.GetEnumerator()
		{
			return this.GetEnumerator();
		}

		// Token: 0x0600032C RID: 812 RVA: 0x00010AA7 File Offset: 0x0000ECA7
		public bool Remove(KeyValuePair<TKey, TValue> item)
		{
			if (this.Contains(item))
			{
				this.privateDictionary.Remove(item.Key);
				return true;
			}
			return false;
		}

		// Token: 0x0600032D RID: 813 RVA: 0x00010ACC File Offset: 0x0000ECCC
		public bool Remove(TKey key)
		{
			if (key == null)
			{
				throw Fx.Exception.ArgumentNull("key");
			}
			if (this.privateDictionary.Contains(key))
			{
				this.privateDictionary.Remove(key);
				return true;
			}
			return false;
		}

		// Token: 0x0600032E RID: 814 RVA: 0x00010B18 File Offset: 0x0000ED18
		public bool TryGetValue(TKey key, out TValue value)
		{
			if (key == null)
			{
				throw Fx.Exception.ArgumentNull("key");
			}
			bool flag = this.privateDictionary.Contains(key);
			value = (flag ? ((TValue)((object)this.privateDictionary[key])) : default(TValue));
			return flag;
		}

		// Token: 0x0600032F RID: 815 RVA: 0x00010B7A File Offset: 0x0000ED7A
		void IDictionary.Add(object key, object value)
		{
			this.privateDictionary.Add(key, value);
		}

		// Token: 0x06000330 RID: 816 RVA: 0x00010B89 File Offset: 0x0000ED89
		void IDictionary.Clear()
		{
			this.privateDictionary.Clear();
		}

		// Token: 0x06000331 RID: 817 RVA: 0x00010B96 File Offset: 0x0000ED96
		bool IDictionary.Contains(object key)
		{
			return this.privateDictionary.Contains(key);
		}

		// Token: 0x06000332 RID: 818 RVA: 0x00010BA4 File Offset: 0x0000EDA4
		IDictionaryEnumerator IDictionary.GetEnumerator()
		{
			return this.privateDictionary.GetEnumerator();
		}

		// Token: 0x17000071 RID: 113
		// (get) Token: 0x06000333 RID: 819 RVA: 0x00010BB1 File Offset: 0x0000EDB1
		bool IDictionary.IsFixedSize
		{
			get
			{
				return ((IDictionary)this.privateDictionary).IsFixedSize;
			}
		}

		// Token: 0x17000072 RID: 114
		// (get) Token: 0x06000334 RID: 820 RVA: 0x00010BBE File Offset: 0x0000EDBE
		bool IDictionary.IsReadOnly
		{
			get
			{
				return this.privateDictionary.IsReadOnly;
			}
		}

		// Token: 0x17000073 RID: 115
		// (get) Token: 0x06000335 RID: 821 RVA: 0x00010BCB File Offset: 0x0000EDCB
		ICollection IDictionary.Keys
		{
			get
			{
				return this.privateDictionary.Keys;
			}
		}

		// Token: 0x06000336 RID: 822 RVA: 0x00010BD8 File Offset: 0x0000EDD8
		void IDictionary.Remove(object key)
		{
			this.privateDictionary.Remove(key);
		}

		// Token: 0x17000074 RID: 116
		// (get) Token: 0x06000337 RID: 823 RVA: 0x00010BE6 File Offset: 0x0000EDE6
		ICollection IDictionary.Values
		{
			get
			{
				return this.privateDictionary.Values;
			}
		}

		// Token: 0x17000075 RID: 117
		object IDictionary.this[object key]
		{
			get
			{
				return this.privateDictionary[key];
			}
			set
			{
				this.privateDictionary[key] = value;
			}
		}

		// Token: 0x0600033A RID: 826 RVA: 0x00010C10 File Offset: 0x0000EE10
		void ICollection.CopyTo(Array array, int index)
		{
			this.privateDictionary.CopyTo(array, index);
		}

		// Token: 0x17000076 RID: 118
		// (get) Token: 0x0600033B RID: 827 RVA: 0x00010C1F File Offset: 0x0000EE1F
		int ICollection.Count
		{
			get
			{
				return this.privateDictionary.Count;
			}
		}

		// Token: 0x17000077 RID: 119
		// (get) Token: 0x0600033C RID: 828 RVA: 0x00010C2C File Offset: 0x0000EE2C
		bool ICollection.IsSynchronized
		{
			get
			{
				return ((ICollection)this.privateDictionary).IsSynchronized;
			}
		}

		// Token: 0x17000078 RID: 120
		// (get) Token: 0x0600033D RID: 829 RVA: 0x00010C39 File Offset: 0x0000EE39
		object ICollection.SyncRoot
		{
			get
			{
				return ((ICollection)this.privateDictionary).SyncRoot;
			}
		}

		// Token: 0x0400020A RID: 522
		private OrderedDictionary privateDictionary;

		// Token: 0x02000099 RID: 153
		[CompilerGenerated]
		private sealed class <GetEnumerator>d__20 : IEnumerator<KeyValuePair<!0, !1>>, IDisposable, IEnumerator
		{
			// Token: 0x0600043E RID: 1086 RVA: 0x00013598 File Offset: 0x00011798
			[DebuggerHidden]
			public <GetEnumerator>d__20(int <>1__state)
			{
				this.<>1__state = <>1__state;
			}

			// Token: 0x0600043F RID: 1087 RVA: 0x000135A8 File Offset: 0x000117A8
			[DebuggerHidden]
			void IDisposable.Dispose()
			{
				int num = this.<>1__state;
				if (num == -3 || num == 1)
				{
					try
					{
					}
					finally
					{
						this.<>m__Finally1();
					}
				}
			}

			// Token: 0x06000440 RID: 1088 RVA: 0x000135E0 File Offset: 0x000117E0
			bool IEnumerator.MoveNext()
			{
				bool result;
				try
				{
					int num = this.<>1__state;
					OrderedDictionary<TKey, TValue> orderedDictionary = this;
					if (num != 0)
					{
						if (num != 1)
						{
							return false;
						}
						this.<>1__state = -3;
					}
					else
					{
						this.<>1__state = -1;
						dictionaryEnumerator = orderedDictionary.privateDictionary.GetEnumerator();
						this.<>1__state = -3;
					}
					if (!dictionaryEnumerator.MoveNext())
					{
						this.<>m__Finally1();
						dictionaryEnumerator = null;
						result = false;
					}
					else
					{
						DictionaryEntry dictionaryEntry = (DictionaryEntry)dictionaryEnumerator.Current;
						this.<>2__current = new KeyValuePair<TKey, TValue>((TKey)((object)dictionaryEntry.Key), (TValue)((object)dictionaryEntry.Value));
						this.<>1__state = 1;
						result = true;
					}
				}
				catch
				{
					this.System.IDisposable.Dispose();
					throw;
				}
				return result;
			}

			// Token: 0x06000441 RID: 1089 RVA: 0x000136A8 File Offset: 0x000118A8
			private void <>m__Finally1()
			{
				this.<>1__state = -1;
				IDisposable disposable = dictionaryEnumerator as IDisposable;
				if (disposable != null)
				{
					disposable.Dispose();
				}
			}

			// Token: 0x170000AC RID: 172
			// (get) Token: 0x06000442 RID: 1090 RVA: 0x000136D1 File Offset: 0x000118D1
			KeyValuePair<TKey, TValue> IEnumerator<KeyValuePair<!0, !1>>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x06000443 RID: 1091 RVA: 0x000136D9 File Offset: 0x000118D9
			[DebuggerHidden]
			void IEnumerator.Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x170000AD RID: 173
			// (get) Token: 0x06000444 RID: 1092 RVA: 0x000136E0 File Offset: 0x000118E0
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x0400030C RID: 780
			private int <>1__state;

			// Token: 0x0400030D RID: 781
			private KeyValuePair<TKey, TValue> <>2__current;

			// Token: 0x0400030E RID: 782
			public OrderedDictionary<TKey, TValue> <>4__this;

			// Token: 0x0400030F RID: 783
			private IDictionaryEnumerator <>7__wrap1;
		}
	}
}
