﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Security;
using System.Threading;

namespace System.Runtime
{
	// Token: 0x0200000F RID: 15
	internal class AsyncWaitHandle
	{
		// Token: 0x06000040 RID: 64 RVA: 0x0000278A File Offset: 0x0000098A
		public AsyncWaitHandle() : this(EventResetMode.AutoReset)
		{
		}

		// Token: 0x06000041 RID: 65 RVA: 0x00002793 File Offset: 0x00000993
		public AsyncWaitHandle(EventResetMode resetMode)
		{
			this.resetMode = resetMode;
			this.syncObject = new object();
		}

		// Token: 0x06000042 RID: 66 RVA: 0x000027B0 File Offset: 0x000009B0
		public bool WaitAsync(Action<object, TimeoutException> callback, object state, TimeSpan timeout)
		{
			if (!this.isSignaled || (this.isSignaled && this.resetMode == EventResetMode.AutoReset))
			{
				object obj = this.syncObject;
				lock (obj)
				{
					if (this.isSignaled && this.resetMode == EventResetMode.AutoReset)
					{
						this.isSignaled = false;
					}
					else if (!this.isSignaled)
					{
						AsyncWaitHandle.AsyncWaiter asyncWaiter = new AsyncWaitHandle.AsyncWaiter(this, callback, state);
						if (this.asyncWaiters == null)
						{
							this.asyncWaiters = new List<AsyncWaitHandle.AsyncWaiter>();
						}
						this.asyncWaiters.Add(asyncWaiter);
						if (timeout != TimeSpan.MaxValue)
						{
							if (AsyncWaitHandle.timerCompleteCallback == null)
							{
								AsyncWaitHandle.timerCompleteCallback = new Action<object>(AsyncWaitHandle.OnTimerComplete);
							}
							asyncWaiter.SetTimer(AsyncWaitHandle.timerCompleteCallback, asyncWaiter, timeout);
						}
						return false;
					}
				}
				return true;
			}
			return true;
		}

		// Token: 0x06000043 RID: 67 RVA: 0x0000288C File Offset: 0x00000A8C
		private static void OnTimerComplete(object state)
		{
			AsyncWaitHandle.AsyncWaiter asyncWaiter = (AsyncWaitHandle.AsyncWaiter)state;
			AsyncWaitHandle parent = asyncWaiter.Parent;
			bool flag = false;
			object obj = parent.syncObject;
			lock (obj)
			{
				if (parent.asyncWaiters != null && parent.asyncWaiters.Remove(asyncWaiter))
				{
					asyncWaiter.TimedOut = true;
					flag = true;
				}
			}
			asyncWaiter.CancelTimer();
			if (flag)
			{
				asyncWaiter.Call();
			}
		}

		// Token: 0x06000044 RID: 68 RVA: 0x00002908 File Offset: 0x00000B08
		public bool Wait(TimeSpan timeout)
		{
			if (!this.isSignaled || (this.isSignaled && this.resetMode == EventResetMode.AutoReset))
			{
				object obj = this.syncObject;
				lock (obj)
				{
					if (this.isSignaled && this.resetMode == EventResetMode.AutoReset)
					{
						this.isSignaled = false;
					}
					else if (!this.isSignaled)
					{
						bool flag2 = false;
						try
						{
							try
							{
							}
							finally
							{
								this.syncWaiterCount++;
								flag2 = true;
							}
							if (timeout == TimeSpan.MaxValue)
							{
								if (!Monitor.Wait(this.syncObject, -1))
								{
									return false;
								}
							}
							else if (!Monitor.Wait(this.syncObject, timeout))
							{
								return false;
							}
						}
						finally
						{
							if (flag2)
							{
								this.syncWaiterCount--;
							}
						}
					}
				}
				return true;
			}
			return true;
		}

		// Token: 0x06000045 RID: 69 RVA: 0x000029F8 File Offset: 0x00000BF8
		public void Set()
		{
			List<AsyncWaitHandle.AsyncWaiter> list = null;
			AsyncWaitHandle.AsyncWaiter asyncWaiter = null;
			if (!this.isSignaled)
			{
				object obj = this.syncObject;
				lock (obj)
				{
					if (!this.isSignaled)
					{
						if (this.resetMode == EventResetMode.ManualReset)
						{
							this.isSignaled = true;
							Monitor.PulseAll(this.syncObject);
							list = this.asyncWaiters;
							this.asyncWaiters = null;
						}
						else if (this.syncWaiterCount > 0)
						{
							Monitor.Pulse(this.syncObject);
						}
						else if (this.asyncWaiters != null && this.asyncWaiters.Count > 0)
						{
							asyncWaiter = this.asyncWaiters[0];
							this.asyncWaiters.RemoveAt(0);
						}
						else
						{
							this.isSignaled = true;
						}
					}
				}
			}
			if (list != null)
			{
				foreach (AsyncWaitHandle.AsyncWaiter asyncWaiter2 in list)
				{
					asyncWaiter2.CancelTimer();
					asyncWaiter2.Call();
				}
			}
			if (asyncWaiter != null)
			{
				asyncWaiter.CancelTimer();
				asyncWaiter.Call();
			}
		}

		// Token: 0x06000046 RID: 70 RVA: 0x00002B18 File Offset: 0x00000D18
		public void Reset()
		{
			this.isSignaled = false;
		}

		// Token: 0x04000058 RID: 88
		private static Action<object> timerCompleteCallback;

		// Token: 0x04000059 RID: 89
		private List<AsyncWaitHandle.AsyncWaiter> asyncWaiters;

		// Token: 0x0400005A RID: 90
		private bool isSignaled;

		// Token: 0x0400005B RID: 91
		private EventResetMode resetMode;

		// Token: 0x0400005C RID: 92
		private object syncObject;

		// Token: 0x0400005D RID: 93
		private int syncWaiterCount;

		// Token: 0x0200005E RID: 94
		private class AsyncWaiter : ActionItem
		{
			// Token: 0x06000355 RID: 853 RVA: 0x00010EC8 File Offset: 0x0000F0C8
			[SecuritySafeCritical]
			public AsyncWaiter(AsyncWaitHandle parent, Action<object, TimeoutException> callback, object state)
			{
				this.Parent = parent;
				this.callback = callback;
				this.state = state;
			}

			// Token: 0x1700007D RID: 125
			// (get) Token: 0x06000356 RID: 854 RVA: 0x00010EE5 File Offset: 0x0000F0E5
			// (set) Token: 0x06000357 RID: 855 RVA: 0x00010EED File Offset: 0x0000F0ED
			public AsyncWaitHandle Parent
			{
				[CompilerGenerated]
				get
				{
					return this.<Parent>k__BackingField;
				}
				[CompilerGenerated]
				private set
				{
					this.<Parent>k__BackingField = value;
				}
			}

			// Token: 0x1700007E RID: 126
			// (get) Token: 0x06000358 RID: 856 RVA: 0x00010EF6 File Offset: 0x0000F0F6
			// (set) Token: 0x06000359 RID: 857 RVA: 0x00010EFE File Offset: 0x0000F0FE
			public bool TimedOut
			{
				[CompilerGenerated]
				get
				{
					return this.<TimedOut>k__BackingField;
				}
				[CompilerGenerated]
				set
				{
					this.<TimedOut>k__BackingField = value;
				}
			}

			// Token: 0x0600035A RID: 858 RVA: 0x00010F07 File Offset: 0x0000F107
			[SecuritySafeCritical]
			public void Call()
			{
				base.Schedule();
			}

			// Token: 0x0600035B RID: 859 RVA: 0x00010F0F File Offset: 0x0000F10F
			[SecurityCritical]
			protected override void Invoke()
			{
				this.callback(this.state, this.TimedOut ? new TimeoutException(InternalSR.TimeoutOnOperation(this.originalTimeout)) : null);
			}

			// Token: 0x0600035C RID: 860 RVA: 0x00010F42 File Offset: 0x0000F142
			public void SetTimer(Action<object> callback, object state, TimeSpan timeout)
			{
				if (this.timer != null)
				{
					throw Fx.Exception.AsError(new InvalidOperationException("Must Cancel Old Timer"));
				}
				this.originalTimeout = timeout;
				this.timer = new IOThreadTimer(callback, state, false);
				this.timer.Set(timeout);
			}

			// Token: 0x0600035D RID: 861 RVA: 0x00010F82 File Offset: 0x0000F182
			public void CancelTimer()
			{
				if (this.timer != null)
				{
					this.timer.Cancel();
					this.timer = null;
				}
			}

			// Token: 0x0400021C RID: 540
			[SecurityCritical]
			private Action<object, TimeoutException> callback;

			// Token: 0x0400021D RID: 541
			[SecurityCritical]
			private object state;

			// Token: 0x0400021E RID: 542
			private IOThreadTimer timer;

			// Token: 0x0400021F RID: 543
			private TimeSpan originalTimeout;

			// Token: 0x04000220 RID: 544
			[CompilerGenerated]
			private AsyncWaitHandle <Parent>k__BackingField;

			// Token: 0x04000221 RID: 545
			[CompilerGenerated]
			private bool <TimedOut>k__BackingField;
		}
	}
}
