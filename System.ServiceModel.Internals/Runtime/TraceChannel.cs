﻿using System;

namespace System.Runtime
{
	// Token: 0x02000032 RID: 50
	internal enum TraceChannel
	{
		// Token: 0x040000E7 RID: 231
		Admin = 16,
		// Token: 0x040000E8 RID: 232
		Operational,
		// Token: 0x040000E9 RID: 233
		Analytic,
		// Token: 0x040000EA RID: 234
		Debug,
		// Token: 0x040000EB RID: 235
		Perf,
		// Token: 0x040000EC RID: 236
		Application = 9
	}
}
