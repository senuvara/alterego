﻿using System;

namespace System.Runtime
{
	// Token: 0x02000033 RID: 51
	internal enum TraceEventLevel
	{
		// Token: 0x040000EE RID: 238
		LogAlways,
		// Token: 0x040000EF RID: 239
		Critical,
		// Token: 0x040000F0 RID: 240
		Error,
		// Token: 0x040000F1 RID: 241
		Warning,
		// Token: 0x040000F2 RID: 242
		Informational,
		// Token: 0x040000F3 RID: 243
		Verbose
	}
}
