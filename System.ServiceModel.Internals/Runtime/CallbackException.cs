﻿using System;
using System.Runtime.Serialization;

namespace System.Runtime
{
	// Token: 0x02000012 RID: 18
	[Serializable]
	internal class CallbackException : FatalException
	{
		// Token: 0x06000070 RID: 112 RVA: 0x00003266 File Offset: 0x00001466
		public CallbackException()
		{
		}

		// Token: 0x06000071 RID: 113 RVA: 0x0000326E File Offset: 0x0000146E
		public CallbackException(string message, Exception innerException) : base(message, innerException)
		{
		}

		// Token: 0x06000072 RID: 114 RVA: 0x00003278 File Offset: 0x00001478
		protected CallbackException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
}
