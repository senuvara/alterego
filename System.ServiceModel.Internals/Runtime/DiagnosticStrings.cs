﻿using System;

namespace System.Runtime
{
	// Token: 0x02000017 RID: 23
	internal static class DiagnosticStrings
	{
		// Token: 0x04000081 RID: 129
		internal const string AppDomain = "AppDomain";

		// Token: 0x04000082 RID: 130
		internal const string ChannelTag = "Channel";

		// Token: 0x04000083 RID: 131
		internal const string Description = "Description";

		// Token: 0x04000084 RID: 132
		internal const string DataTag = "Data";

		// Token: 0x04000085 RID: 133
		internal const string DataItemsTag = "DataItems";

		// Token: 0x04000086 RID: 134
		internal const string DescriptionTag = "Description";

		// Token: 0x04000087 RID: 135
		internal const string ExceptionTag = "Exception";

		// Token: 0x04000088 RID: 136
		internal const string ExceptionTypeTag = "ExceptionType";

		// Token: 0x04000089 RID: 137
		internal const string ExceptionStringTag = "ExceptionString";

		// Token: 0x0400008A RID: 138
		internal const string ExtendedDataTag = "ExtendedData";

		// Token: 0x0400008B RID: 139
		internal const string InnerExceptionTag = "InnerException";

		// Token: 0x0400008C RID: 140
		internal const string KeyTag = "Key";

		// Token: 0x0400008D RID: 141
		internal const string MessageTag = "Message";

		// Token: 0x0400008E RID: 142
		internal const string NamespaceTag = "xmlns";

		// Token: 0x0400008F RID: 143
		internal const string NativeErrorCodeTag = "NativeErrorCode";

		// Token: 0x04000090 RID: 144
		internal const string Separator = ":";

		// Token: 0x04000091 RID: 145
		internal const string SeverityTag = "Severity";

		// Token: 0x04000092 RID: 146
		internal const string SourceTag = "Source";

		// Token: 0x04000093 RID: 147
		internal const string StackTraceTag = "StackTrace";

		// Token: 0x04000094 RID: 148
		internal const string Task = "Task";

		// Token: 0x04000095 RID: 149
		internal const string TraceCodeTag = "TraceIdentifier";

		// Token: 0x04000096 RID: 150
		internal const string TraceRecordTag = "TraceRecord";

		// Token: 0x04000097 RID: 151
		internal const string ValueTag = "Value";
	}
}
