﻿using System;
using System.Threading;

namespace System.Runtime
{
	// Token: 0x02000010 RID: 16
	internal sealed class BackoffTimeoutHelper
	{
		// Token: 0x06000047 RID: 71 RVA: 0x00002B21 File Offset: 0x00000D21
		internal BackoffTimeoutHelper(TimeSpan timeout) : this(timeout, BackoffTimeoutHelper.defaultMaxWaitTime)
		{
		}

		// Token: 0x06000048 RID: 72 RVA: 0x00002B2F File Offset: 0x00000D2F
		internal BackoffTimeoutHelper(TimeSpan timeout, TimeSpan maxWaitTime) : this(timeout, maxWaitTime, BackoffTimeoutHelper.defaultInitialWaitTime)
		{
		}

		// Token: 0x06000049 RID: 73 RVA: 0x00002B3E File Offset: 0x00000D3E
		internal BackoffTimeoutHelper(TimeSpan timeout, TimeSpan maxWaitTime, TimeSpan initialWaitTime)
		{
			this.random = new Random(this.GetHashCode());
			this.maxWaitTime = maxWaitTime;
			this.originalTimeout = timeout;
			this.Reset(timeout, initialWaitTime);
		}

		// Token: 0x1700000F RID: 15
		// (get) Token: 0x0600004A RID: 74 RVA: 0x00002B6D File Offset: 0x00000D6D
		public TimeSpan OriginalTimeout
		{
			get
			{
				return this.originalTimeout;
			}
		}

		// Token: 0x0600004B RID: 75 RVA: 0x00002B75 File Offset: 0x00000D75
		private void Reset(TimeSpan timeout, TimeSpan initialWaitTime)
		{
			if (timeout == TimeSpan.MaxValue)
			{
				this.deadline = DateTime.MaxValue;
			}
			else
			{
				this.deadline = DateTime.UtcNow + timeout;
			}
			this.waitTime = initialWaitTime;
		}

		// Token: 0x0600004C RID: 76 RVA: 0x00002BA9 File Offset: 0x00000DA9
		public bool IsExpired()
		{
			return !(this.deadline == DateTime.MaxValue) && DateTime.UtcNow >= this.deadline;
		}

		// Token: 0x0600004D RID: 77 RVA: 0x00002BD0 File Offset: 0x00000DD0
		public void WaitAndBackoff(Action<object> callback, object state)
		{
			if (this.backoffCallback != callback || this.backoffState != state)
			{
				if (this.backoffTimer != null)
				{
					this.backoffTimer.Cancel();
				}
				this.backoffCallback = callback;
				this.backoffState = state;
				this.backoffTimer = new IOThreadTimer(callback, state, false, BackoffTimeoutHelper.maxSkewMilliseconds);
			}
			TimeSpan timeFromNow = this.WaitTimeWithDrift();
			this.Backoff();
			this.backoffTimer.Set(timeFromNow);
		}

		// Token: 0x0600004E RID: 78 RVA: 0x00002C42 File Offset: 0x00000E42
		public void WaitAndBackoff()
		{
			Thread.Sleep(this.WaitTimeWithDrift());
			this.Backoff();
		}

		// Token: 0x0600004F RID: 79 RVA: 0x00002C58 File Offset: 0x00000E58
		private TimeSpan WaitTimeWithDrift()
		{
			return Ticks.ToTimeSpan(Math.Max(Ticks.FromTimeSpan(BackoffTimeoutHelper.defaultInitialWaitTime), Ticks.Add(Ticks.FromTimeSpan(this.waitTime), (long)((ulong)this.random.Next() % (ulong)(2L * BackoffTimeoutHelper.maxDriftTicks + 1L) - (ulong)BackoffTimeoutHelper.maxDriftTicks))));
		}

		// Token: 0x06000050 RID: 80 RVA: 0x00002CA8 File Offset: 0x00000EA8
		private void Backoff()
		{
			if (this.waitTime.Ticks >= this.maxWaitTime.Ticks / 2L)
			{
				this.waitTime = this.maxWaitTime;
			}
			else
			{
				this.waitTime = TimeSpan.FromTicks(this.waitTime.Ticks * 2L);
			}
			if (this.deadline != DateTime.MaxValue)
			{
				TimeSpan t = this.deadline - DateTime.UtcNow;
				if (this.waitTime > t)
				{
					this.waitTime = t;
					if (this.waitTime < TimeSpan.Zero)
					{
						this.waitTime = TimeSpan.Zero;
					}
				}
			}
		}

		// Token: 0x06000051 RID: 81 RVA: 0x00002D4C File Offset: 0x00000F4C
		// Note: this type is marked as 'beforefieldinit'.
		static BackoffTimeoutHelper()
		{
		}

		// Token: 0x0400005E RID: 94
		private static readonly int maxSkewMilliseconds = (int)(IOThreadTimer.SystemTimeResolutionTicks / 10000L);

		// Token: 0x0400005F RID: 95
		private static readonly long maxDriftTicks = IOThreadTimer.SystemTimeResolutionTicks * 2L;

		// Token: 0x04000060 RID: 96
		private static readonly TimeSpan defaultInitialWaitTime = TimeSpan.FromMilliseconds(1.0);

		// Token: 0x04000061 RID: 97
		private static readonly TimeSpan defaultMaxWaitTime = TimeSpan.FromMinutes(1.0);

		// Token: 0x04000062 RID: 98
		private DateTime deadline;

		// Token: 0x04000063 RID: 99
		private TimeSpan maxWaitTime;

		// Token: 0x04000064 RID: 100
		private TimeSpan waitTime;

		// Token: 0x04000065 RID: 101
		private IOThreadTimer backoffTimer;

		// Token: 0x04000066 RID: 102
		private Action<object> backoffCallback;

		// Token: 0x04000067 RID: 103
		private object backoffState;

		// Token: 0x04000068 RID: 104
		private Random random;

		// Token: 0x04000069 RID: 105
		private TimeSpan originalTimeout;
	}
}
