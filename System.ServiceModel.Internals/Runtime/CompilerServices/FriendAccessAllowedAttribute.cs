﻿using System;

namespace System.Runtime.CompilerServices
{
	// Token: 0x02000058 RID: 88
	internal class FriendAccessAllowedAttribute : Attribute
	{
		// Token: 0x06000349 RID: 841 RVA: 0x00010CE0 File Offset: 0x0000EEE0
		public FriendAccessAllowedAttribute()
		{
		}
	}
}
