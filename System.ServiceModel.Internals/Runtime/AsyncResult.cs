﻿using System;
using System.Runtime.CompilerServices;
using System.Threading;

namespace System.Runtime
{
	// Token: 0x0200000E RID: 14
	internal abstract class AsyncResult : IAsyncResult
	{
		// Token: 0x06000027 RID: 39 RVA: 0x000022D2 File Offset: 0x000004D2
		protected AsyncResult(AsyncCallback callback, object state)
		{
			this.callback = callback;
			this.state = state;
			this.thisLock = new object();
		}

		// Token: 0x17000007 RID: 7
		// (get) Token: 0x06000028 RID: 40 RVA: 0x000022F3 File Offset: 0x000004F3
		public object AsyncState
		{
			get
			{
				return this.state;
			}
		}

		// Token: 0x17000008 RID: 8
		// (get) Token: 0x06000029 RID: 41 RVA: 0x000022FC File Offset: 0x000004FC
		public WaitHandle AsyncWaitHandle
		{
			get
			{
				if (this.manualResetEvent != null)
				{
					return this.manualResetEvent;
				}
				object obj = this.ThisLock;
				lock (obj)
				{
					if (this.manualResetEvent == null)
					{
						this.manualResetEvent = new ManualResetEvent(this.isCompleted);
					}
				}
				return this.manualResetEvent;
			}
		}

		// Token: 0x17000009 RID: 9
		// (get) Token: 0x0600002A RID: 42 RVA: 0x00002364 File Offset: 0x00000564
		public bool CompletedSynchronously
		{
			get
			{
				return this.completedSynchronously;
			}
		}

		// Token: 0x1700000A RID: 10
		// (get) Token: 0x0600002B RID: 43 RVA: 0x0000236C File Offset: 0x0000056C
		public bool HasCallback
		{
			get
			{
				return this.callback != null;
			}
		}

		// Token: 0x1700000B RID: 11
		// (get) Token: 0x0600002C RID: 44 RVA: 0x00002377 File Offset: 0x00000577
		public bool IsCompleted
		{
			get
			{
				return this.isCompleted;
			}
		}

		// Token: 0x1700000C RID: 12
		// (get) Token: 0x0600002D RID: 45 RVA: 0x0000237F File Offset: 0x0000057F
		// (set) Token: 0x0600002E RID: 46 RVA: 0x00002387 File Offset: 0x00000587
		protected Action<AsyncResult, Exception> OnCompleting
		{
			[CompilerGenerated]
			get
			{
				return this.<OnCompleting>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<OnCompleting>k__BackingField = value;
			}
		}

		// Token: 0x1700000D RID: 13
		// (get) Token: 0x0600002F RID: 47 RVA: 0x00002390 File Offset: 0x00000590
		private object ThisLock
		{
			get
			{
				return this.thisLock;
			}
		}

		// Token: 0x1700000E RID: 14
		// (get) Token: 0x06000030 RID: 48 RVA: 0x00002398 File Offset: 0x00000598
		// (set) Token: 0x06000031 RID: 49 RVA: 0x000023A0 File Offset: 0x000005A0
		protected Action<AsyncCallback, IAsyncResult> VirtualCallback
		{
			[CompilerGenerated]
			get
			{
				return this.<VirtualCallback>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<VirtualCallback>k__BackingField = value;
			}
		}

		// Token: 0x06000032 RID: 50 RVA: 0x000023AC File Offset: 0x000005AC
		protected void Complete(bool completedSynchronously)
		{
			if (this.isCompleted)
			{
				throw Fx.Exception.AsError(new InvalidOperationException(InternalSR.AsyncResultCompletedTwice(base.GetType())));
			}
			this.completedSynchronously = completedSynchronously;
			if (this.OnCompleting != null)
			{
				try
				{
					this.OnCompleting(this, this.exception);
				}
				catch (Exception ex)
				{
					if (Fx.IsFatal(ex))
					{
						throw;
					}
					this.exception = ex;
				}
			}
			if (completedSynchronously)
			{
				this.isCompleted = true;
			}
			else
			{
				object obj = this.ThisLock;
				lock (obj)
				{
					this.isCompleted = true;
					if (this.manualResetEvent != null)
					{
						this.manualResetEvent.Set();
					}
				}
			}
			if (this.callback != null)
			{
				try
				{
					if (this.VirtualCallback != null)
					{
						this.VirtualCallback(this.callback, this);
					}
					else
					{
						this.callback(this);
					}
				}
				catch (Exception innerException)
				{
					if (Fx.IsFatal(innerException))
					{
						throw;
					}
					throw Fx.Exception.AsError(new CallbackException("Async Callback Threw Exception", innerException));
				}
			}
		}

		// Token: 0x06000033 RID: 51 RVA: 0x000024D4 File Offset: 0x000006D4
		protected void Complete(bool completedSynchronously, Exception exception)
		{
			this.exception = exception;
			this.Complete(completedSynchronously);
		}

		// Token: 0x06000034 RID: 52 RVA: 0x000024E4 File Offset: 0x000006E4
		private static void AsyncCompletionWrapperCallback(IAsyncResult result)
		{
			if (result == null)
			{
				throw Fx.Exception.AsError(new InvalidOperationException("Invalid Null Async Result"));
			}
			if (result.CompletedSynchronously)
			{
				return;
			}
			AsyncResult asyncResult = (AsyncResult)result.AsyncState;
			if (!asyncResult.OnContinueAsyncCompletion(result))
			{
				return;
			}
			AsyncResult.AsyncCompletion nextCompletion = asyncResult.GetNextCompletion();
			if (nextCompletion == null)
			{
				AsyncResult.ThrowInvalidAsyncResult(result);
			}
			bool flag = false;
			Exception ex = null;
			try
			{
				flag = nextCompletion(result);
			}
			catch (Exception ex2)
			{
				if (Fx.IsFatal(ex2))
				{
					throw;
				}
				flag = true;
				ex = ex2;
			}
			if (flag)
			{
				asyncResult.Complete(false, ex);
			}
		}

		// Token: 0x06000035 RID: 53 RVA: 0x00002570 File Offset: 0x00000770
		protected virtual bool OnContinueAsyncCompletion(IAsyncResult result)
		{
			return true;
		}

		// Token: 0x06000036 RID: 54 RVA: 0x00002573 File Offset: 0x00000773
		protected void SetBeforePrepareAsyncCompletionAction(Action beforePrepareAsyncCompletionAction)
		{
			this.beforePrepareAsyncCompletionAction = beforePrepareAsyncCompletionAction;
		}

		// Token: 0x06000037 RID: 55 RVA: 0x0000257C File Offset: 0x0000077C
		protected void SetCheckSyncValidationFunc(Func<IAsyncResult, bool> checkSyncValidationFunc)
		{
			this.checkSyncValidationFunc = checkSyncValidationFunc;
		}

		// Token: 0x06000038 RID: 56 RVA: 0x00002585 File Offset: 0x00000785
		protected AsyncCallback PrepareAsyncCompletion(AsyncResult.AsyncCompletion callback)
		{
			if (this.beforePrepareAsyncCompletionAction != null)
			{
				this.beforePrepareAsyncCompletionAction();
			}
			this.nextAsyncCompletion = callback;
			if (AsyncResult.asyncCompletionWrapperCallback == null)
			{
				AsyncResult.asyncCompletionWrapperCallback = Fx.ThunkCallback(new AsyncCallback(AsyncResult.AsyncCompletionWrapperCallback));
			}
			return AsyncResult.asyncCompletionWrapperCallback;
		}

		// Token: 0x06000039 RID: 57 RVA: 0x000025C4 File Offset: 0x000007C4
		protected bool CheckSyncContinue(IAsyncResult result)
		{
			AsyncResult.AsyncCompletion asyncCompletion;
			return this.TryContinueHelper(result, out asyncCompletion);
		}

		// Token: 0x0600003A RID: 58 RVA: 0x000025DC File Offset: 0x000007DC
		protected bool SyncContinue(IAsyncResult result)
		{
			AsyncResult.AsyncCompletion asyncCompletion;
			return this.TryContinueHelper(result, out asyncCompletion) && asyncCompletion(result);
		}

		// Token: 0x0600003B RID: 59 RVA: 0x00002600 File Offset: 0x00000800
		private bool TryContinueHelper(IAsyncResult result, out AsyncResult.AsyncCompletion callback)
		{
			if (result == null)
			{
				throw Fx.Exception.AsError(new InvalidOperationException("Invalid Null Async Result"));
			}
			callback = null;
			if (this.checkSyncValidationFunc != null)
			{
				if (!this.checkSyncValidationFunc(result))
				{
					return false;
				}
			}
			else if (!result.CompletedSynchronously)
			{
				return false;
			}
			callback = this.GetNextCompletion();
			if (callback == null)
			{
				AsyncResult.ThrowInvalidAsyncResult("Only call Check/SyncContinue once per async operation (once per PrepareAsyncCompletion).");
			}
			return true;
		}

		// Token: 0x0600003C RID: 60 RVA: 0x00002661 File Offset: 0x00000861
		private AsyncResult.AsyncCompletion GetNextCompletion()
		{
			AsyncResult.AsyncCompletion result = this.nextAsyncCompletion;
			this.nextAsyncCompletion = null;
			return result;
		}

		// Token: 0x0600003D RID: 61 RVA: 0x00002670 File Offset: 0x00000870
		protected static void ThrowInvalidAsyncResult(IAsyncResult result)
		{
			throw Fx.Exception.AsError(new InvalidOperationException(InternalSR.InvalidAsyncResultImplementation(result.GetType())));
		}

		// Token: 0x0600003E RID: 62 RVA: 0x0000268C File Offset: 0x0000088C
		protected static void ThrowInvalidAsyncResult(string debugText)
		{
			string message = "Invalid Async Result Implementation Generic";
			throw Fx.Exception.AsError(new InvalidOperationException(message));
		}

		// Token: 0x0600003F RID: 63 RVA: 0x000026B4 File Offset: 0x000008B4
		protected static TAsyncResult End<TAsyncResult>(IAsyncResult result) where TAsyncResult : AsyncResult
		{
			if (result == null)
			{
				throw Fx.Exception.ArgumentNull("result");
			}
			TAsyncResult tasyncResult = result as TAsyncResult;
			if (tasyncResult == null)
			{
				throw Fx.Exception.Argument("result", "Invalid Async Result");
			}
			if (tasyncResult.endCalled)
			{
				throw Fx.Exception.AsError(new InvalidOperationException("Async Result Already Ended"));
			}
			tasyncResult.endCalled = true;
			if (!tasyncResult.isCompleted)
			{
				tasyncResult.AsyncWaitHandle.WaitOne();
			}
			if (tasyncResult.manualResetEvent != null)
			{
				tasyncResult.manualResetEvent.Close();
			}
			if (tasyncResult.exception != null)
			{
				throw Fx.Exception.AsError(tasyncResult.exception);
			}
			return tasyncResult;
		}

		// Token: 0x0400004A RID: 74
		private static AsyncCallback asyncCompletionWrapperCallback;

		// Token: 0x0400004B RID: 75
		private AsyncCallback callback;

		// Token: 0x0400004C RID: 76
		private bool completedSynchronously;

		// Token: 0x0400004D RID: 77
		private bool endCalled;

		// Token: 0x0400004E RID: 78
		private Exception exception;

		// Token: 0x0400004F RID: 79
		private bool isCompleted;

		// Token: 0x04000050 RID: 80
		private AsyncResult.AsyncCompletion nextAsyncCompletion;

		// Token: 0x04000051 RID: 81
		private object state;

		// Token: 0x04000052 RID: 82
		private Action beforePrepareAsyncCompletionAction;

		// Token: 0x04000053 RID: 83
		private Func<IAsyncResult, bool> checkSyncValidationFunc;

		// Token: 0x04000054 RID: 84
		private ManualResetEvent manualResetEvent;

		// Token: 0x04000055 RID: 85
		private object thisLock;

		// Token: 0x04000056 RID: 86
		[CompilerGenerated]
		private Action<AsyncResult, Exception> <OnCompleting>k__BackingField;

		// Token: 0x04000057 RID: 87
		[CompilerGenerated]
		private Action<AsyncCallback, IAsyncResult> <VirtualCallback>k__BackingField;

		// Token: 0x0200005D RID: 93
		// (Invoke) Token: 0x06000352 RID: 850
		protected delegate bool AsyncCompletion(IAsyncResult result);
	}
}
