﻿using System;

namespace System.Runtime
{
	// Token: 0x0200002A RID: 42
	internal abstract class ScheduleActionItemAsyncResult : AsyncResult
	{
		// Token: 0x06000136 RID: 310 RVA: 0x0000584E File Offset: 0x00003A4E
		protected ScheduleActionItemAsyncResult(AsyncCallback callback, object state) : base(callback, state)
		{
		}

		// Token: 0x06000137 RID: 311 RVA: 0x00005858 File Offset: 0x00003A58
		protected void Schedule()
		{
			ActionItem.Schedule(ScheduleActionItemAsyncResult.doWork, this);
		}

		// Token: 0x06000138 RID: 312 RVA: 0x00005868 File Offset: 0x00003A68
		private static void DoWork(object state)
		{
			ScheduleActionItemAsyncResult scheduleActionItemAsyncResult = (ScheduleActionItemAsyncResult)state;
			Exception exception = null;
			try
			{
				scheduleActionItemAsyncResult.OnDoWork();
			}
			catch (Exception ex)
			{
				if (Fx.IsFatal(ex))
				{
					throw;
				}
				exception = ex;
			}
			scheduleActionItemAsyncResult.Complete(false, exception);
		}

		// Token: 0x06000139 RID: 313
		protected abstract void OnDoWork();

		// Token: 0x0600013A RID: 314 RVA: 0x000058AC File Offset: 0x00003AAC
		public static void End(IAsyncResult result)
		{
			AsyncResult.End<ScheduleActionItemAsyncResult>(result);
		}

		// Token: 0x0600013B RID: 315 RVA: 0x000058B5 File Offset: 0x00003AB5
		// Note: this type is marked as 'beforefieldinit'.
		static ScheduleActionItemAsyncResult()
		{
		}

		// Token: 0x040000CF RID: 207
		private static Action<object> doWork = new Action<object>(ScheduleActionItemAsyncResult.DoWork);
	}
}
