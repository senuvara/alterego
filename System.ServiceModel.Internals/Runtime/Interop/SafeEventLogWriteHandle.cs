﻿using System;
using System.Runtime.InteropServices;
using System.Security;
using Microsoft.Win32.SafeHandles;

namespace System.Runtime.Interop
{
	// Token: 0x0200003D RID: 61
	[SecurityCritical]
	internal sealed class SafeEventLogWriteHandle : SafeHandleZeroOrMinusOneIsInvalid
	{
		// Token: 0x06000209 RID: 521 RVA: 0x00008A93 File Offset: 0x00006C93
		[SecurityCritical]
		private SafeEventLogWriteHandle() : base(true)
		{
		}

		// Token: 0x0600020A RID: 522 RVA: 0x00008A9C File Offset: 0x00006C9C
		[SecurityCritical]
		public static SafeEventLogWriteHandle RegisterEventSource(string uncServerName, string sourceName)
		{
			SafeEventLogWriteHandle safeEventLogWriteHandle = UnsafeNativeMethods.RegisterEventSource(uncServerName, sourceName);
			Marshal.GetLastWin32Error();
			bool isInvalid = safeEventLogWriteHandle.IsInvalid;
			return safeEventLogWriteHandle;
		}

		// Token: 0x0600020B RID: 523
		[DllImport("advapi32", SetLastError = true)]
		private static extern bool DeregisterEventSource(IntPtr hEventLog);

		// Token: 0x0600020C RID: 524 RVA: 0x00008AB2 File Offset: 0x00006CB2
		[SecurityCritical]
		protected override bool ReleaseHandle()
		{
			return SafeEventLogWriteHandle.DeregisterEventSource(this.handle);
		}
	}
}
