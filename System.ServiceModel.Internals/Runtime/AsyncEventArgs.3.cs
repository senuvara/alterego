﻿using System;
using System.Runtime.CompilerServices;

namespace System.Runtime
{
	// Token: 0x0200000C RID: 12
	internal class AsyncEventArgs<TArgument, TResult> : AsyncEventArgs<TArgument>
	{
		// Token: 0x17000006 RID: 6
		// (get) Token: 0x06000020 RID: 32 RVA: 0x000022B9 File Offset: 0x000004B9
		// (set) Token: 0x06000021 RID: 33 RVA: 0x000022C1 File Offset: 0x000004C1
		public TResult Result
		{
			[CompilerGenerated]
			get
			{
				return this.<Result>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<Result>k__BackingField = value;
			}
		}

		// Token: 0x06000022 RID: 34 RVA: 0x000022CA File Offset: 0x000004CA
		public AsyncEventArgs()
		{
		}

		// Token: 0x04000049 RID: 73
		[CompilerGenerated]
		private TResult <Result>k__BackingField;
	}
}
