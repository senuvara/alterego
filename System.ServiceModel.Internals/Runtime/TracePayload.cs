﻿using System;

namespace System.Runtime
{
	// Token: 0x02000036 RID: 54
	internal struct TracePayload
	{
		// Token: 0x0600018C RID: 396 RVA: 0x0000698E File Offset: 0x00004B8E
		public TracePayload(string serializedException, string eventSource, string appDomainFriendlyName, string extendedData, string hostReference)
		{
			this.serializedException = serializedException;
			this.eventSource = eventSource;
			this.appDomainFriendlyName = appDomainFriendlyName;
			this.extendedData = extendedData;
			this.hostReference = hostReference;
		}

		// Token: 0x17000036 RID: 54
		// (get) Token: 0x0600018D RID: 397 RVA: 0x000069B5 File Offset: 0x00004BB5
		public string SerializedException
		{
			get
			{
				return this.serializedException;
			}
		}

		// Token: 0x17000037 RID: 55
		// (get) Token: 0x0600018E RID: 398 RVA: 0x000069BD File Offset: 0x00004BBD
		public string EventSource
		{
			get
			{
				return this.eventSource;
			}
		}

		// Token: 0x17000038 RID: 56
		// (get) Token: 0x0600018F RID: 399 RVA: 0x000069C5 File Offset: 0x00004BC5
		public string AppDomainFriendlyName
		{
			get
			{
				return this.appDomainFriendlyName;
			}
		}

		// Token: 0x17000039 RID: 57
		// (get) Token: 0x06000190 RID: 400 RVA: 0x000069CD File Offset: 0x00004BCD
		public string ExtendedData
		{
			get
			{
				return this.extendedData;
			}
		}

		// Token: 0x1700003A RID: 58
		// (get) Token: 0x06000191 RID: 401 RVA: 0x000069D5 File Offset: 0x00004BD5
		public string HostReference
		{
			get
			{
				return this.hostReference;
			}
		}

		// Token: 0x040000FE RID: 254
		private string serializedException;

		// Token: 0x040000FF RID: 255
		private string eventSource;

		// Token: 0x04000100 RID: 256
		private string appDomainFriendlyName;

		// Token: 0x04000101 RID: 257
		private string extendedData;

		// Token: 0x04000102 RID: 258
		private string hostReference;
	}
}
