﻿using System;

namespace System.Runtime
{
	// Token: 0x0200000A RID: 10
	internal abstract class AsyncEventArgs : IAsyncEventArgs
	{
		// Token: 0x17000002 RID: 2
		// (get) Token: 0x06000015 RID: 21 RVA: 0x000021B4 File Offset: 0x000003B4
		public Exception Exception
		{
			get
			{
				return this.exception;
			}
		}

		// Token: 0x17000003 RID: 3
		// (get) Token: 0x06000016 RID: 22 RVA: 0x000021BC File Offset: 0x000003BC
		public object AsyncState
		{
			get
			{
				return this.asyncState;
			}
		}

		// Token: 0x17000004 RID: 4
		// (set) Token: 0x06000017 RID: 23 RVA: 0x000021C4 File Offset: 0x000003C4
		private AsyncEventArgs.OperationState State
		{
			set
			{
				if (value != AsyncEventArgs.OperationState.PendingCompletion)
				{
					if (value - AsyncEventArgs.OperationState.CompletedSynchronously <= 1)
					{
						if (this.state != AsyncEventArgs.OperationState.PendingCompletion)
						{
							throw Fx.Exception.AsError(new InvalidOperationException(InternalSR.AsyncEventArgsCompletedTwice(base.GetType())));
						}
					}
				}
				else if (this.state == AsyncEventArgs.OperationState.PendingCompletion)
				{
					throw Fx.Exception.AsError(new InvalidOperationException(InternalSR.AsyncEventArgsCompletionPending(base.GetType())));
				}
				this.state = value;
			}
		}

		// Token: 0x06000018 RID: 24 RVA: 0x0000222C File Offset: 0x0000042C
		public void Complete(bool completedSynchronously)
		{
			this.Complete(completedSynchronously, null);
		}

		// Token: 0x06000019 RID: 25 RVA: 0x00002236 File Offset: 0x00000436
		public virtual void Complete(bool completedSynchronously, Exception exception)
		{
			this.exception = exception;
			if (completedSynchronously)
			{
				this.State = AsyncEventArgs.OperationState.CompletedSynchronously;
				return;
			}
			this.State = AsyncEventArgs.OperationState.CompletedAsynchronously;
			this.callback(this);
		}

		// Token: 0x0600001A RID: 26 RVA: 0x0000225D File Offset: 0x0000045D
		protected void SetAsyncState(AsyncEventArgsCallback callback, object state)
		{
			if (callback == null)
			{
				throw Fx.Exception.ArgumentNull("callback");
			}
			this.State = AsyncEventArgs.OperationState.PendingCompletion;
			this.asyncState = state;
			this.callback = callback;
		}

		// Token: 0x0600001B RID: 27 RVA: 0x00002287 File Offset: 0x00000487
		protected AsyncEventArgs()
		{
		}

		// Token: 0x04000044 RID: 68
		private AsyncEventArgs.OperationState state;

		// Token: 0x04000045 RID: 69
		private object asyncState;

		// Token: 0x04000046 RID: 70
		private AsyncEventArgsCallback callback;

		// Token: 0x04000047 RID: 71
		private Exception exception;

		// Token: 0x0200005C RID: 92
		private enum OperationState
		{
			// Token: 0x04000218 RID: 536
			Created,
			// Token: 0x04000219 RID: 537
			PendingCompletion,
			// Token: 0x0400021A RID: 538
			CompletedSynchronously,
			// Token: 0x0400021B RID: 539
			CompletedAsynchronously
		}
	}
}
