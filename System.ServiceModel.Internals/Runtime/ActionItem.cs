﻿using System;
using System.Diagnostics;
using System.Runtime.Diagnostics;
using System.Security;
using System.Threading;

namespace System.Runtime
{
	// Token: 0x02000007 RID: 7
	internal abstract class ActionItem
	{
		// Token: 0x0600000A RID: 10 RVA: 0x000020B0 File Offset: 0x000002B0
		protected ActionItem()
		{
		}

		// Token: 0x17000001 RID: 1
		// (get) Token: 0x0600000B RID: 11 RVA: 0x000020B8 File Offset: 0x000002B8
		// (set) Token: 0x0600000C RID: 12 RVA: 0x000020C0 File Offset: 0x000002C0
		public bool LowPriority
		{
			get
			{
				return this.lowPriority;
			}
			protected set
			{
				this.lowPriority = value;
			}
		}

		// Token: 0x0600000D RID: 13 RVA: 0x000020C9 File Offset: 0x000002C9
		public static void Schedule(Action<object> callback, object state)
		{
			ActionItem.Schedule(callback, state, false);
		}

		// Token: 0x0600000E RID: 14 RVA: 0x000020D3 File Offset: 0x000002D3
		[SecuritySafeCritical]
		public static void Schedule(Action<object> callback, object state, bool lowPriority)
		{
			if (PartialTrustHelpers.ShouldFlowSecurityContext || WaitCallbackActionItem.ShouldUseActivity || Fx.Trace.IsEnd2EndActivityTracingEnabled)
			{
				new ActionItem.DefaultActionItem(callback, state, lowPriority).Schedule();
				return;
			}
			ActionItem.ScheduleCallback(callback, state, lowPriority);
		}

		// Token: 0x0600000F RID: 15
		[SecurityCritical]
		protected abstract void Invoke();

		// Token: 0x06000010 RID: 16 RVA: 0x00002105 File Offset: 0x00000305
		[SecurityCritical]
		protected void Schedule()
		{
			if (this.isScheduled)
			{
				throw Fx.Exception.AsError(new InvalidOperationException("Action Item Is Already Scheduled"));
			}
			this.isScheduled = true;
			this.ScheduleCallback(ActionItem.CallbackHelper.InvokeWithoutContextCallback);
		}

		// Token: 0x06000011 RID: 17 RVA: 0x00002136 File Offset: 0x00000336
		[SecurityCritical]
		protected void ScheduleWithoutContext()
		{
			if (this.isScheduled)
			{
				throw Fx.Exception.AsError(new InvalidOperationException("Action Item Is Already Scheduled"));
			}
			this.isScheduled = true;
			this.ScheduleCallback(ActionItem.CallbackHelper.InvokeWithoutContextCallback);
		}

		// Token: 0x06000012 RID: 18 RVA: 0x00002167 File Offset: 0x00000367
		[SecurityCritical]
		private static void ScheduleCallback(Action<object> callback, object state, bool lowPriority)
		{
			if (lowPriority)
			{
				IOThreadScheduler.ScheduleCallbackLowPriNoFlow(callback, state);
				return;
			}
			IOThreadScheduler.ScheduleCallbackNoFlow(callback, state);
		}

		// Token: 0x06000013 RID: 19 RVA: 0x0000217B File Offset: 0x0000037B
		[SecurityCritical]
		private void ScheduleCallback(Action<object> callback)
		{
			ActionItem.ScheduleCallback(callback, this, this.lowPriority);
		}

		// Token: 0x0400003F RID: 63
		private bool isScheduled;

		// Token: 0x04000040 RID: 64
		private bool lowPriority;

		// Token: 0x0200005A RID: 90
		[SecurityCritical]
		private static class CallbackHelper
		{
			// Token: 0x1700007B RID: 123
			// (get) Token: 0x0600034A RID: 842 RVA: 0x00010CE8 File Offset: 0x0000EEE8
			public static Action<object> InvokeWithoutContextCallback
			{
				get
				{
					if (ActionItem.CallbackHelper.invokeWithoutContextCallback == null)
					{
						ActionItem.CallbackHelper.invokeWithoutContextCallback = new Action<object>(ActionItem.CallbackHelper.InvokeWithoutContext);
					}
					return ActionItem.CallbackHelper.invokeWithoutContextCallback;
				}
			}

			// Token: 0x1700007C RID: 124
			// (get) Token: 0x0600034B RID: 843 RVA: 0x00010D07 File Offset: 0x0000EF07
			public static ContextCallback OnContextAppliedCallback
			{
				get
				{
					if (ActionItem.CallbackHelper.onContextAppliedCallback == null)
					{
						ActionItem.CallbackHelper.onContextAppliedCallback = new ContextCallback(ActionItem.CallbackHelper.OnContextApplied);
					}
					return ActionItem.CallbackHelper.onContextAppliedCallback;
				}
			}

			// Token: 0x0600034C RID: 844 RVA: 0x00010D26 File Offset: 0x0000EF26
			private static void InvokeWithoutContext(object state)
			{
				((ActionItem)state).Invoke();
				((ActionItem)state).isScheduled = false;
			}

			// Token: 0x0600034D RID: 845 RVA: 0x00010D3F File Offset: 0x0000EF3F
			private static void OnContextApplied(object o)
			{
				((ActionItem)o).Invoke();
				((ActionItem)o).isScheduled = false;
			}

			// Token: 0x04000210 RID: 528
			private static Action<object> invokeWithoutContextCallback;

			// Token: 0x04000211 RID: 529
			private static ContextCallback onContextAppliedCallback;
		}

		// Token: 0x0200005B RID: 91
		private class DefaultActionItem : ActionItem
		{
			// Token: 0x0600034E RID: 846 RVA: 0x00010D58 File Offset: 0x0000EF58
			[SecuritySafeCritical]
			public DefaultActionItem(Action<object> callback, object state, bool isLowPriority)
			{
				base.LowPriority = isLowPriority;
				this.callback = callback;
				this.state = state;
				if (WaitCallbackActionItem.ShouldUseActivity)
				{
					this.flowLegacyActivityId = true;
					this.activityId = DiagnosticTraceBase.ActivityId;
				}
				if (Fx.Trace.IsEnd2EndActivityTracingEnabled)
				{
					this.eventTraceActivity = EventTraceActivity.GetFromThreadOrCreate(false);
					if (TraceCore.ActionItemScheduledIsEnabled(Fx.Trace))
					{
						TraceCore.ActionItemScheduled(Fx.Trace, this.eventTraceActivity);
					}
				}
			}

			// Token: 0x0600034F RID: 847 RVA: 0x00010DCD File Offset: 0x0000EFCD
			[SecurityCritical]
			protected override void Invoke()
			{
				if (this.flowLegacyActivityId || Fx.Trace.IsEnd2EndActivityTracingEnabled)
				{
					this.TraceAndInvoke();
					return;
				}
				this.callback(this.state);
			}

			// Token: 0x06000350 RID: 848 RVA: 0x00010DFC File Offset: 0x0000EFFC
			[SecurityCritical]
			private void TraceAndInvoke()
			{
				if (this.flowLegacyActivityId)
				{
					Guid guid = DiagnosticTraceBase.ActivityId;
					try
					{
						DiagnosticTraceBase.ActivityId = this.activityId;
						this.callback(this.state);
						return;
					}
					finally
					{
						DiagnosticTraceBase.ActivityId = guid;
					}
				}
				Guid empty = Guid.Empty;
				bool flag = false;
				try
				{
					if (this.eventTraceActivity != null)
					{
						empty = Trace.CorrelationManager.ActivityId;
						flag = true;
						Trace.CorrelationManager.ActivityId = this.eventTraceActivity.ActivityId;
						if (TraceCore.ActionItemCallbackInvokedIsEnabled(Fx.Trace))
						{
							TraceCore.ActionItemCallbackInvoked(Fx.Trace, this.eventTraceActivity);
						}
					}
					this.callback(this.state);
				}
				finally
				{
					if (flag)
					{
						Trace.CorrelationManager.ActivityId = empty;
					}
				}
			}

			// Token: 0x04000212 RID: 530
			[SecurityCritical]
			private Action<object> callback;

			// Token: 0x04000213 RID: 531
			[SecurityCritical]
			private object state;

			// Token: 0x04000214 RID: 532
			private bool flowLegacyActivityId;

			// Token: 0x04000215 RID: 533
			private Guid activityId;

			// Token: 0x04000216 RID: 534
			private EventTraceActivity eventTraceActivity;
		}
	}
}
