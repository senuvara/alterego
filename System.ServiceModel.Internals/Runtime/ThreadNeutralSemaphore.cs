﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Threading;

namespace System.Runtime
{
	// Token: 0x0200002F RID: 47
	internal class ThreadNeutralSemaphore
	{
		// Token: 0x0600015E RID: 350 RVA: 0x000060A9 File Offset: 0x000042A9
		public ThreadNeutralSemaphore(int maxCount) : this(maxCount, null)
		{
		}

		// Token: 0x0600015F RID: 351 RVA: 0x000060B3 File Offset: 0x000042B3
		public ThreadNeutralSemaphore(int maxCount, Func<Exception> abortedExceptionGenerator)
		{
			this.maxCount = maxCount;
			this.abortedExceptionGenerator = abortedExceptionGenerator;
		}

		// Token: 0x17000032 RID: 50
		// (get) Token: 0x06000160 RID: 352 RVA: 0x000060D4 File Offset: 0x000042D4
		private static Action<object, TimeoutException> EnteredAsyncCallback
		{
			get
			{
				if (ThreadNeutralSemaphore.enteredAsyncCallback == null)
				{
					ThreadNeutralSemaphore.enteredAsyncCallback = new Action<object, TimeoutException>(ThreadNeutralSemaphore.OnEnteredAsync);
				}
				return ThreadNeutralSemaphore.enteredAsyncCallback;
			}
		}

		// Token: 0x17000033 RID: 51
		// (get) Token: 0x06000161 RID: 353 RVA: 0x000060F3 File Offset: 0x000042F3
		private Queue<AsyncWaitHandle> Waiters
		{
			get
			{
				if (this.waiters == null)
				{
					this.waiters = new Queue<AsyncWaitHandle>();
				}
				return this.waiters;
			}
		}

		// Token: 0x06000162 RID: 354 RVA: 0x00006110 File Offset: 0x00004310
		public bool EnterAsync(TimeSpan timeout, FastAsyncCallback callback, object state)
		{
			AsyncWaitHandle asyncWaitHandle = null;
			object thisLock = this.ThisLock;
			lock (thisLock)
			{
				if (this.aborted)
				{
					throw Fx.Exception.AsError(this.CreateObjectAbortedException());
				}
				if (this.count < this.maxCount)
				{
					this.count++;
					return true;
				}
				asyncWaitHandle = new AsyncWaitHandle();
				this.Waiters.Enqueue(asyncWaitHandle);
			}
			return asyncWaitHandle.WaitAsync(ThreadNeutralSemaphore.EnteredAsyncCallback, new ThreadNeutralSemaphore.EnterAsyncData(this, asyncWaitHandle, callback, state), timeout);
		}

		// Token: 0x06000163 RID: 355 RVA: 0x000061B0 File Offset: 0x000043B0
		private static void OnEnteredAsync(object state, TimeoutException exception)
		{
			ThreadNeutralSemaphore.EnterAsyncData enterAsyncData = (ThreadNeutralSemaphore.EnterAsyncData)state;
			ThreadNeutralSemaphore semaphore = enterAsyncData.Semaphore;
			Exception asyncException = exception;
			if (exception != null && !semaphore.RemoveWaiter(enterAsyncData.Waiter))
			{
				asyncException = null;
			}
			if (semaphore.aborted)
			{
				asyncException = semaphore.CreateObjectAbortedException();
			}
			enterAsyncData.Callback(enterAsyncData.State, asyncException);
		}

		// Token: 0x06000164 RID: 356 RVA: 0x00006204 File Offset: 0x00004404
		public bool TryEnter()
		{
			object thisLock = this.ThisLock;
			bool result;
			lock (thisLock)
			{
				if (this.count < this.maxCount)
				{
					this.count++;
					result = true;
				}
				else
				{
					result = false;
				}
			}
			return result;
		}

		// Token: 0x06000165 RID: 357 RVA: 0x00006264 File Offset: 0x00004464
		public void Enter(TimeSpan timeout)
		{
			if (!this.TryEnter(timeout))
			{
				throw Fx.Exception.AsError(ThreadNeutralSemaphore.CreateEnterTimedOutException(timeout));
			}
		}

		// Token: 0x06000166 RID: 358 RVA: 0x00006280 File Offset: 0x00004480
		public bool TryEnter(TimeSpan timeout)
		{
			AsyncWaitHandle asyncWaitHandle = this.EnterCore();
			if (asyncWaitHandle == null)
			{
				return true;
			}
			bool flag = !asyncWaitHandle.Wait(timeout);
			if (this.aborted)
			{
				throw Fx.Exception.AsError(this.CreateObjectAbortedException());
			}
			if (flag && !this.RemoveWaiter(asyncWaitHandle))
			{
				flag = false;
			}
			return !flag;
		}

		// Token: 0x06000167 RID: 359 RVA: 0x000062CF File Offset: 0x000044CF
		internal static TimeoutException CreateEnterTimedOutException(TimeSpan timeout)
		{
			return new TimeoutException(InternalSR.LockTimeoutExceptionMessage(timeout));
		}

		// Token: 0x06000168 RID: 360 RVA: 0x000062E1 File Offset: 0x000044E1
		private Exception CreateObjectAbortedException()
		{
			if (this.abortedExceptionGenerator != null)
			{
				return this.abortedExceptionGenerator();
			}
			return new OperationCanceledException("Thread Neutral Semaphore Aborted");
		}

		// Token: 0x06000169 RID: 361 RVA: 0x00006304 File Offset: 0x00004504
		private bool RemoveWaiter(AsyncWaitHandle waiter)
		{
			bool result = false;
			object thisLock = this.ThisLock;
			lock (thisLock)
			{
				for (int i = this.Waiters.Count; i > 0; i--)
				{
					AsyncWaitHandle asyncWaitHandle = this.Waiters.Dequeue();
					if (asyncWaitHandle == waiter)
					{
						result = true;
					}
					else
					{
						this.Waiters.Enqueue(asyncWaitHandle);
					}
				}
			}
			return result;
		}

		// Token: 0x0600016A RID: 362 RVA: 0x0000637C File Offset: 0x0000457C
		private AsyncWaitHandle EnterCore()
		{
			object thisLock = this.ThisLock;
			AsyncWaitHandle asyncWaitHandle;
			lock (thisLock)
			{
				if (this.aborted)
				{
					throw Fx.Exception.AsError(this.CreateObjectAbortedException());
				}
				if (this.count < this.maxCount)
				{
					this.count++;
					return null;
				}
				asyncWaitHandle = new AsyncWaitHandle();
				this.Waiters.Enqueue(asyncWaitHandle);
			}
			return asyncWaitHandle;
		}

		// Token: 0x0600016B RID: 363 RVA: 0x00006404 File Offset: 0x00004604
		public int Exit()
		{
			int result = -1;
			object thisLock = this.ThisLock;
			AsyncWaitHandle asyncWaitHandle;
			lock (thisLock)
			{
				if (this.aborted)
				{
					return result;
				}
				if (this.count == 0)
				{
					string message = "Invalid Semaphore Exit";
					throw Fx.Exception.AsError(new SynchronizationLockException(message));
				}
				if (this.waiters == null || this.waiters.Count == 0)
				{
					this.count--;
					return this.count;
				}
				asyncWaitHandle = this.waiters.Dequeue();
				result = this.count;
			}
			asyncWaitHandle.Set();
			return result;
		}

		// Token: 0x0600016C RID: 364 RVA: 0x000064B8 File Offset: 0x000046B8
		public void Abort()
		{
			object thisLock = this.ThisLock;
			lock (thisLock)
			{
				if (!this.aborted)
				{
					this.aborted = true;
					if (this.waiters != null)
					{
						while (this.waiters.Count > 0)
						{
							this.waiters.Dequeue().Set();
						}
					}
				}
			}
		}

		// Token: 0x040000DB RID: 219
		private static Action<object, TimeoutException> enteredAsyncCallback;

		// Token: 0x040000DC RID: 220
		private bool aborted;

		// Token: 0x040000DD RID: 221
		private Func<Exception> abortedExceptionGenerator;

		// Token: 0x040000DE RID: 222
		private int count;

		// Token: 0x040000DF RID: 223
		private int maxCount;

		// Token: 0x040000E0 RID: 224
		private object ThisLock = new object();

		// Token: 0x040000E1 RID: 225
		private Queue<AsyncWaitHandle> waiters;

		// Token: 0x02000089 RID: 137
		private class EnterAsyncData
		{
			// Token: 0x060003EE RID: 1006 RVA: 0x00012A16 File Offset: 0x00010C16
			public EnterAsyncData(ThreadNeutralSemaphore semaphore, AsyncWaitHandle waiter, FastAsyncCallback callback, object state)
			{
				this.Waiter = waiter;
				this.Semaphore = semaphore;
				this.Callback = callback;
				this.State = state;
			}

			// Token: 0x1700009A RID: 154
			// (get) Token: 0x060003EF RID: 1007 RVA: 0x00012A3B File Offset: 0x00010C3B
			// (set) Token: 0x060003F0 RID: 1008 RVA: 0x00012A43 File Offset: 0x00010C43
			public ThreadNeutralSemaphore Semaphore
			{
				[CompilerGenerated]
				get
				{
					return this.<Semaphore>k__BackingField;
				}
				[CompilerGenerated]
				set
				{
					this.<Semaphore>k__BackingField = value;
				}
			}

			// Token: 0x1700009B RID: 155
			// (get) Token: 0x060003F1 RID: 1009 RVA: 0x00012A4C File Offset: 0x00010C4C
			// (set) Token: 0x060003F2 RID: 1010 RVA: 0x00012A54 File Offset: 0x00010C54
			public AsyncWaitHandle Waiter
			{
				[CompilerGenerated]
				get
				{
					return this.<Waiter>k__BackingField;
				}
				[CompilerGenerated]
				set
				{
					this.<Waiter>k__BackingField = value;
				}
			}

			// Token: 0x1700009C RID: 156
			// (get) Token: 0x060003F3 RID: 1011 RVA: 0x00012A5D File Offset: 0x00010C5D
			// (set) Token: 0x060003F4 RID: 1012 RVA: 0x00012A65 File Offset: 0x00010C65
			public FastAsyncCallback Callback
			{
				[CompilerGenerated]
				get
				{
					return this.<Callback>k__BackingField;
				}
				[CompilerGenerated]
				set
				{
					this.<Callback>k__BackingField = value;
				}
			}

			// Token: 0x1700009D RID: 157
			// (get) Token: 0x060003F5 RID: 1013 RVA: 0x00012A6E File Offset: 0x00010C6E
			// (set) Token: 0x060003F6 RID: 1014 RVA: 0x00012A76 File Offset: 0x00010C76
			public object State
			{
				[CompilerGenerated]
				get
				{
					return this.<State>k__BackingField;
				}
				[CompilerGenerated]
				set
				{
					this.<State>k__BackingField = value;
				}
			}

			// Token: 0x040002D0 RID: 720
			[CompilerGenerated]
			private ThreadNeutralSemaphore <Semaphore>k__BackingField;

			// Token: 0x040002D1 RID: 721
			[CompilerGenerated]
			private AsyncWaitHandle <Waiter>k__BackingField;

			// Token: 0x040002D2 RID: 722
			[CompilerGenerated]
			private FastAsyncCallback <Callback>k__BackingField;

			// Token: 0x040002D3 RID: 723
			[CompilerGenerated]
			private object <State>k__BackingField;
		}
	}
}
