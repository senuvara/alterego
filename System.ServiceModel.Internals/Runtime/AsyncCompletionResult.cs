﻿using System;

namespace System.Runtime
{
	// Token: 0x02000009 RID: 9
	internal enum AsyncCompletionResult
	{
		// Token: 0x04000042 RID: 66
		Queued,
		// Token: 0x04000043 RID: 67
		Completed
	}
}
