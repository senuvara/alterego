﻿using System;

namespace System.Runtime
{
	// Token: 0x02000016 RID: 22
	internal enum ComputerNameFormat
	{
		// Token: 0x04000079 RID: 121
		NetBIOS,
		// Token: 0x0400007A RID: 122
		DnsHostName,
		// Token: 0x0400007B RID: 123
		Dns,
		// Token: 0x0400007C RID: 124
		DnsFullyQualified,
		// Token: 0x0400007D RID: 125
		PhysicalNetBIOS,
		// Token: 0x0400007E RID: 126
		PhysicalDnsHostName,
		// Token: 0x0400007F RID: 127
		PhysicalDnsDomain,
		// Token: 0x04000080 RID: 128
		PhysicalDnsFullyQualified
	}
}
