﻿using System;

namespace System.Runtime
{
	// Token: 0x02000015 RID: 21
	internal class CompletedAsyncResult<TResult, TParameter> : AsyncResult
	{
		// Token: 0x06000077 RID: 119 RVA: 0x000032E1 File Offset: 0x000014E1
		public CompletedAsyncResult(TResult resultData, TParameter parameter, AsyncCallback callback, object state) : base(callback, state)
		{
			this.resultData = resultData;
			this.parameter = parameter;
			base.Complete(true);
		}

		// Token: 0x06000078 RID: 120 RVA: 0x00003304 File Offset: 0x00001504
		public static TResult End(IAsyncResult result, out TParameter parameter)
		{
			Fx.AssertAndThrowFatal(result.IsCompleted, "CompletedAsyncResult<T> was not completed!");
			CompletedAsyncResult<TResult, TParameter> completedAsyncResult = AsyncResult.End<CompletedAsyncResult<TResult, TParameter>>(result);
			parameter = completedAsyncResult.parameter;
			return completedAsyncResult.resultData;
		}

		// Token: 0x04000076 RID: 118
		private TResult resultData;

		// Token: 0x04000077 RID: 119
		private TParameter parameter;
	}
}
