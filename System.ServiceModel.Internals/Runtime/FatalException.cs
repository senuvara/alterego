﻿using System;
using System.Runtime.Serialization;

namespace System.Runtime
{
	// Token: 0x0200001B RID: 27
	[Serializable]
	internal class FatalException : SystemException
	{
		// Token: 0x06000098 RID: 152 RVA: 0x000039E0 File Offset: 0x00001BE0
		public FatalException()
		{
		}

		// Token: 0x06000099 RID: 153 RVA: 0x000039E8 File Offset: 0x00001BE8
		public FatalException(string message) : base(message)
		{
		}

		// Token: 0x0600009A RID: 154 RVA: 0x000039F1 File Offset: 0x00001BF1
		public FatalException(string message, Exception innerException) : base(message, innerException)
		{
		}

		// Token: 0x0600009B RID: 155 RVA: 0x000039FB File Offset: 0x00001BFB
		protected FatalException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
}
