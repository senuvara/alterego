﻿using System;
using System.Diagnostics;

namespace System.Runtime.Diagnostics
{
	// Token: 0x02000042 RID: 66
	internal class DiagnosticTraceSource : TraceSource
	{
		// Token: 0x06000261 RID: 609 RVA: 0x0000A225 File Offset: 0x00008425
		internal DiagnosticTraceSource(string name) : base(name)
		{
		}

		// Token: 0x06000262 RID: 610 RVA: 0x0000A22E File Offset: 0x0000842E
		protected override string[] GetSupportedAttributes()
		{
			return new string[]
			{
				"propagateActivity"
			};
		}

		// Token: 0x17000049 RID: 73
		// (get) Token: 0x06000263 RID: 611 RVA: 0x0000A240 File Offset: 0x00008440
		// (set) Token: 0x06000264 RID: 612 RVA: 0x0000A275 File Offset: 0x00008475
		internal bool PropagateActivity
		{
			get
			{
				bool result = false;
				string value = base.Attributes["propagateActivity"];
				if (!string.IsNullOrEmpty(value) && !bool.TryParse(value, out result))
				{
					result = false;
				}
				return result;
			}
			set
			{
				base.Attributes["propagateActivity"] = value.ToString();
			}
		}

		// Token: 0x0400015C RID: 348
		private const string PropagateActivityValue = "propagateActivity";
	}
}
