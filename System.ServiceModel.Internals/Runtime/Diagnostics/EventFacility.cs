﻿using System;

namespace System.Runtime.Diagnostics
{
	// Token: 0x0200004A RID: 74
	internal enum EventFacility : uint
	{
		// Token: 0x040001DC RID: 476
		Tracing = 65536U,
		// Token: 0x040001DD RID: 477
		ServiceModel = 131072U,
		// Token: 0x040001DE RID: 478
		TransactionBridge = 196608U,
		// Token: 0x040001DF RID: 479
		SMSvcHost = 262144U,
		// Token: 0x040001E0 RID: 480
		InfoCards = 327680U,
		// Token: 0x040001E1 RID: 481
		SecurityAudit = 393216U
	}
}
