﻿using System;
using System.Runtime.CompilerServices;

namespace System.Runtime.Diagnostics
{
	// Token: 0x0200004E RID: 78
	[AttributeUsage(AttributeTargets.Field, Inherited = false)]
	internal sealed class PerformanceCounterNameAttribute : Attribute
	{
		// Token: 0x060002DA RID: 730 RVA: 0x0000FA99 File Offset: 0x0000DC99
		public PerformanceCounterNameAttribute(string name)
		{
			this.Name = name;
		}

		// Token: 0x1700005C RID: 92
		// (get) Token: 0x060002DB RID: 731 RVA: 0x0000FAA8 File Offset: 0x0000DCA8
		// (set) Token: 0x060002DC RID: 732 RVA: 0x0000FAB0 File Offset: 0x0000DCB0
		public string Name
		{
			[CompilerGenerated]
			get
			{
				return this.<Name>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<Name>k__BackingField = value;
			}
		}

		// Token: 0x040001EA RID: 490
		[CompilerGenerated]
		private string <Name>k__BackingField;
	}
}
