﻿using System;
using System.Diagnostics;
using System.Security;

namespace System.Runtime.Diagnostics
{
	// Token: 0x0200004C RID: 76
	internal class EventTraceActivity
	{
		// Token: 0x060002D1 RID: 721 RVA: 0x0000F9DF File Offset: 0x0000DBDF
		public EventTraceActivity(bool setOnThread = false) : this(Guid.NewGuid(), setOnThread)
		{
		}

		// Token: 0x060002D2 RID: 722 RVA: 0x0000F9ED File Offset: 0x0000DBED
		public EventTraceActivity(Guid guid, bool setOnThread = false)
		{
			this.ActivityId = guid;
			if (setOnThread)
			{
				this.SetActivityIdOnThread();
			}
		}

		// Token: 0x1700005A RID: 90
		// (get) Token: 0x060002D3 RID: 723 RVA: 0x0000FA05 File Offset: 0x0000DC05
		public static EventTraceActivity Empty
		{
			get
			{
				if (EventTraceActivity.empty == null)
				{
					EventTraceActivity.empty = new EventTraceActivity(Guid.Empty, false);
				}
				return EventTraceActivity.empty;
			}
		}

		// Token: 0x1700005B RID: 91
		// (get) Token: 0x060002D4 RID: 724 RVA: 0x0000FA23 File Offset: 0x0000DC23
		public static string Name
		{
			get
			{
				return "E2EActivity";
			}
		}

		// Token: 0x060002D5 RID: 725 RVA: 0x0000FA2C File Offset: 0x0000DC2C
		[SecuritySafeCritical]
		public static EventTraceActivity GetFromThreadOrCreate(bool clearIdOnThread = false)
		{
			Guid guid = Trace.CorrelationManager.ActivityId;
			if (guid == Guid.Empty)
			{
				guid = Guid.NewGuid();
			}
			else if (clearIdOnThread)
			{
				Trace.CorrelationManager.ActivityId = Guid.Empty;
			}
			return new EventTraceActivity(guid, false);
		}

		// Token: 0x060002D6 RID: 726 RVA: 0x0000FA72 File Offset: 0x0000DC72
		[SecuritySafeCritical]
		public static Guid GetActivityIdFromThread()
		{
			return Trace.CorrelationManager.ActivityId;
		}

		// Token: 0x060002D7 RID: 727 RVA: 0x0000FA7E File Offset: 0x0000DC7E
		public void SetActivityId(Guid guid)
		{
			this.ActivityId = guid;
		}

		// Token: 0x060002D8 RID: 728 RVA: 0x0000FA87 File Offset: 0x0000DC87
		[SecuritySafeCritical]
		private void SetActivityIdOnThread()
		{
			Trace.CorrelationManager.ActivityId = this.ActivityId;
		}

		// Token: 0x040001E8 RID: 488
		public Guid ActivityId;

		// Token: 0x040001E9 RID: 489
		private static EventTraceActivity empty;
	}
}
