﻿using System;

namespace System.Runtime.Diagnostics
{
	// Token: 0x02000047 RID: 71
	internal enum EventLogCategory : ushort
	{
		// Token: 0x04000178 RID: 376
		ServiceAuthorization = 1,
		// Token: 0x04000179 RID: 377
		MessageAuthentication,
		// Token: 0x0400017A RID: 378
		ObjectAccess,
		// Token: 0x0400017B RID: 379
		Tracing,
		// Token: 0x0400017C RID: 380
		WebHost,
		// Token: 0x0400017D RID: 381
		FailFast,
		// Token: 0x0400017E RID: 382
		MessageLogging,
		// Token: 0x0400017F RID: 383
		PerformanceCounter,
		// Token: 0x04000180 RID: 384
		Wmi,
		// Token: 0x04000181 RID: 385
		ComPlus,
		// Token: 0x04000182 RID: 386
		StateMachine,
		// Token: 0x04000183 RID: 387
		Wsat,
		// Token: 0x04000184 RID: 388
		SharingService,
		// Token: 0x04000185 RID: 389
		ListenerAdapter
	}
}
