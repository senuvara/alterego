﻿using System;
using System.Xml;

namespace System.Runtime.Diagnostics
{
	// Token: 0x0200004F RID: 79
	internal class StringTraceRecord : TraceRecord
	{
		// Token: 0x060002DD RID: 733 RVA: 0x0000FAB9 File Offset: 0x0000DCB9
		internal StringTraceRecord(string elementName, string content)
		{
			this.elementName = elementName;
			this.content = content;
		}

		// Token: 0x1700005D RID: 93
		// (get) Token: 0x060002DE RID: 734 RVA: 0x0000FACF File Offset: 0x0000DCCF
		internal override string EventId
		{
			get
			{
				return base.BuildEventId("String");
			}
		}

		// Token: 0x060002DF RID: 735 RVA: 0x0000FADC File Offset: 0x0000DCDC
		internal override void WriteTo(XmlWriter writer)
		{
			writer.WriteElementString(this.elementName, this.content);
		}

		// Token: 0x040001EB RID: 491
		private string elementName;

		// Token: 0x040001EC RID: 492
		private string content;
	}
}
