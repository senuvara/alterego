﻿using System;
using System.Collections;
using System.Xml;

namespace System.Runtime.Diagnostics
{
	// Token: 0x02000043 RID: 67
	internal class DictionaryTraceRecord : TraceRecord
	{
		// Token: 0x06000265 RID: 613 RVA: 0x0000A28E File Offset: 0x0000848E
		internal DictionaryTraceRecord(IDictionary dictionary)
		{
			this.dictionary = dictionary;
		}

		// Token: 0x1700004A RID: 74
		// (get) Token: 0x06000266 RID: 614 RVA: 0x0000A29D File Offset: 0x0000849D
		internal override string EventId
		{
			get
			{
				return "http://schemas.microsoft.com/2006/08/ServiceModel/DictionaryTraceRecord";
			}
		}

		// Token: 0x06000267 RID: 615 RVA: 0x0000A2A4 File Offset: 0x000084A4
		internal override void WriteTo(XmlWriter xml)
		{
			if (this.dictionary != null)
			{
				foreach (object obj in this.dictionary.Keys)
				{
					object obj2 = this.dictionary[obj];
					xml.WriteElementString(obj.ToString(), (obj2 == null) ? string.Empty : obj2.ToString());
				}
			}
		}

		// Token: 0x0400015D RID: 349
		private IDictionary dictionary;
	}
}
