﻿using System;

namespace System.Runtime.Diagnostics
{
	// Token: 0x0200004D RID: 77
	internal interface ITraceSourceStringProvider
	{
		// Token: 0x060002D9 RID: 729
		string GetSourceString();
	}
}
