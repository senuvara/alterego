﻿using System;

namespace System.Runtime.Diagnostics
{
	// Token: 0x0200003F RID: 63
	internal enum ActivityControl : uint
	{
		// Token: 0x0400013A RID: 314
		EVENT_ACTIVITY_CTRL_GET_ID = 1U,
		// Token: 0x0400013B RID: 315
		EVENT_ACTIVITY_CTRL_SET_ID,
		// Token: 0x0400013C RID: 316
		EVENT_ACTIVITY_CTRL_CREATE_ID,
		// Token: 0x0400013D RID: 317
		EVENT_ACTIVITY_CTRL_GET_SET_ID,
		// Token: 0x0400013E RID: 318
		EVENT_ACTIVITY_CTRL_CREATE_SET_ID
	}
}
