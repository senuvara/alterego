﻿using System;

namespace System.Runtime.Diagnostics
{
	// Token: 0x02000049 RID: 73
	internal enum EventSeverity : uint
	{
		// Token: 0x040001D7 RID: 471
		Success,
		// Token: 0x040001D8 RID: 472
		Informational = 1073741824U,
		// Token: 0x040001D9 RID: 473
		Warning = 2147483648U,
		// Token: 0x040001DA RID: 474
		Error = 3221225472U
	}
}
