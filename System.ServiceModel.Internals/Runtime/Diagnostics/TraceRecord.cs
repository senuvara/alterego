﻿using System;
using System.Xml;

namespace System.Runtime.Diagnostics
{
	// Token: 0x02000050 RID: 80
	[Serializable]
	internal class TraceRecord
	{
		// Token: 0x1700005E RID: 94
		// (get) Token: 0x060002E0 RID: 736 RVA: 0x0000FAF0 File Offset: 0x0000DCF0
		internal virtual string EventId
		{
			get
			{
				return this.BuildEventId("Empty");
			}
		}

		// Token: 0x060002E1 RID: 737 RVA: 0x0000FAFD File Offset: 0x0000DCFD
		internal virtual void WriteTo(XmlWriter writer)
		{
		}

		// Token: 0x060002E2 RID: 738 RVA: 0x0000FAFF File Offset: 0x0000DCFF
		protected string BuildEventId(string eventId)
		{
			return "http://schemas.microsoft.com/2006/08/ServiceModel/" + eventId + "TraceRecord";
		}

		// Token: 0x060002E3 RID: 739 RVA: 0x0000FB11 File Offset: 0x0000DD11
		protected string XmlEncode(string text)
		{
			return DiagnosticTraceBase.XmlEncode(text);
		}

		// Token: 0x060002E4 RID: 740 RVA: 0x0000FB19 File Offset: 0x0000DD19
		public TraceRecord()
		{
		}

		// Token: 0x040001ED RID: 493
		protected const string EventIdBase = "http://schemas.microsoft.com/2006/08/ServiceModel/";

		// Token: 0x040001EE RID: 494
		protected const string NamespaceSuffix = "TraceRecord";
	}
}
