﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Threading.Tasks;

namespace System.Runtime
{
	// Token: 0x0200002E RID: 46
	internal static class TaskExtensions
	{
		// Token: 0x06000152 RID: 338 RVA: 0x00005DF0 File Offset: 0x00003FF0
		public static IAsyncResult AsAsyncResult<T>(this Task<T> task, AsyncCallback callback, object state)
		{
			if (task == null)
			{
				throw Fx.Exception.ArgumentNull("task");
			}
			if (task.Status == TaskStatus.Created)
			{
				throw Fx.Exception.AsError(new InvalidOperationException("SFx Task Not Started"));
			}
			TaskCompletionSource<T> tcs = new TaskCompletionSource<T>(state);
			task.ContinueWith(delegate(Task<T> t)
			{
				if (t.IsFaulted)
				{
					tcs.TrySetException(t.Exception.InnerExceptions);
				}
				else if (t.IsCanceled)
				{
					tcs.TrySetCanceled();
				}
				else
				{
					tcs.TrySetResult(t.Result);
				}
				if (callback != null)
				{
					callback(tcs.Task);
				}
			}, TaskContinuationOptions.ExecuteSynchronously);
			return tcs.Task;
		}

		// Token: 0x06000153 RID: 339 RVA: 0x00005E6C File Offset: 0x0000406C
		public static IAsyncResult AsAsyncResult(this Task task, AsyncCallback callback, object state)
		{
			if (task == null)
			{
				throw Fx.Exception.ArgumentNull("task");
			}
			if (task.Status == TaskStatus.Created)
			{
				throw Fx.Exception.AsError(new InvalidOperationException("SFx Task Not Started"));
			}
			TaskCompletionSource<object> tcs = new TaskCompletionSource<object>(state);
			task.ContinueWith(delegate(Task t)
			{
				if (t.IsFaulted)
				{
					tcs.TrySetException(t.Exception.InnerExceptions);
				}
				else if (t.IsCanceled)
				{
					tcs.TrySetCanceled();
				}
				else
				{
					tcs.TrySetResult(null);
				}
				if (callback != null)
				{
					callback(tcs.Task);
				}
			}, TaskContinuationOptions.ExecuteSynchronously);
			return tcs.Task;
		}

		// Token: 0x06000154 RID: 340 RVA: 0x00005EE5 File Offset: 0x000040E5
		public static ConfiguredTaskAwaitable SuppressContextFlow(this Task task)
		{
			return task.ConfigureAwait(false);
		}

		// Token: 0x06000155 RID: 341 RVA: 0x00005EEE File Offset: 0x000040EE
		public static ConfiguredTaskAwaitable<T> SuppressContextFlow<T>(this Task<T> task)
		{
			return task.ConfigureAwait(false);
		}

		// Token: 0x06000156 RID: 342 RVA: 0x00005EF7 File Offset: 0x000040F7
		public static ConfiguredTaskAwaitable ContinueOnCapturedContextFlow(this Task task)
		{
			return task.ConfigureAwait(true);
		}

		// Token: 0x06000157 RID: 343 RVA: 0x00005F00 File Offset: 0x00004100
		public static ConfiguredTaskAwaitable<T> ContinueOnCapturedContextFlow<T>(this Task<T> task)
		{
			return task.ConfigureAwait(true);
		}

		// Token: 0x06000158 RID: 344 RVA: 0x00005F0C File Offset: 0x0000410C
		public static void Wait<TException>(this Task task)
		{
			try
			{
				task.Wait();
			}
			catch (AggregateException aggregateException)
			{
				throw Fx.Exception.AsError<TException>(aggregateException);
			}
		}

		// Token: 0x06000159 RID: 345 RVA: 0x00005F40 File Offset: 0x00004140
		public static bool Wait<TException>(this Task task, int millisecondsTimeout)
		{
			bool result;
			try
			{
				result = task.Wait(millisecondsTimeout);
			}
			catch (AggregateException aggregateException)
			{
				throw Fx.Exception.AsError<TException>(aggregateException);
			}
			return result;
		}

		// Token: 0x0600015A RID: 346 RVA: 0x00005F78 File Offset: 0x00004178
		public static bool Wait<TException>(this Task task, TimeSpan timeout)
		{
			bool result;
			try
			{
				if (timeout == TimeSpan.MaxValue)
				{
					result = task.Wait(-1);
				}
				else
				{
					result = task.Wait(timeout);
				}
			}
			catch (AggregateException aggregateException)
			{
				throw Fx.Exception.AsError<TException>(aggregateException);
			}
			return result;
		}

		// Token: 0x0600015B RID: 347 RVA: 0x00005FC4 File Offset: 0x000041C4
		public static void Wait(this Task task, TimeSpan timeout, Action<Exception, TimeSpan, string> exceptionConverter, string operationType)
		{
			bool flag = false;
			try
			{
				if (timeout > TimeoutHelper.MaxWait)
				{
					task.Wait();
				}
				else
				{
					flag = !task.Wait(timeout);
				}
			}
			catch (Exception ex)
			{
				if (Fx.IsFatal(ex) || exceptionConverter == null)
				{
					throw;
				}
				exceptionConverter(ex, timeout, operationType);
			}
			if (flag)
			{
				throw Fx.Exception.AsError(new TimeoutException(InternalSR.TaskTimedOutError(timeout)));
			}
		}

		// Token: 0x0600015C RID: 348 RVA: 0x0000603C File Offset: 0x0000423C
		public static Task<TBase> Upcast<TDerived, TBase>(this Task<TDerived> task) where TDerived : TBase
		{
			if (task.Status != TaskStatus.RanToCompletion)
			{
				return task.UpcastPrivate<TDerived, TBase>();
			}
			return Task.FromResult<TBase>((TBase)((object)task.Result));
		}

		// Token: 0x0600015D RID: 349 RVA: 0x00006064 File Offset: 0x00004264
		private static async Task<TBase> UpcastPrivate<TDerived, TBase>(this Task<TDerived> task) where TDerived : TBase
		{
			ConfiguredTaskAwaitable<TDerived>.ConfiguredTaskAwaiter configuredTaskAwaiter = task.ConfigureAwait(false).GetAwaiter();
			if (!configuredTaskAwaiter.IsCompleted)
			{
				await configuredTaskAwaiter;
				ConfiguredTaskAwaitable<TDerived>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
				configuredTaskAwaiter = configuredTaskAwaiter2;
				configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<TDerived>.ConfiguredTaskAwaiter);
			}
			return (TBase)((object)configuredTaskAwaiter.GetResult());
		}

		// Token: 0x02000086 RID: 134
		[CompilerGenerated]
		private sealed class <>c__DisplayClass0_0<T>
		{
			// Token: 0x060003E8 RID: 1000 RVA: 0x00012850 File Offset: 0x00010A50
			public <>c__DisplayClass0_0()
			{
			}

			// Token: 0x060003E9 RID: 1001 RVA: 0x00012858 File Offset: 0x00010A58
			internal void <AsAsyncResult>b__0(Task<T> t)
			{
				if (t.IsFaulted)
				{
					this.tcs.TrySetException(t.Exception.InnerExceptions);
				}
				else if (t.IsCanceled)
				{
					this.tcs.TrySetCanceled();
				}
				else
				{
					this.tcs.TrySetResult(t.Result);
				}
				if (this.callback != null)
				{
					this.callback(this.tcs.Task);
				}
			}

			// Token: 0x040002C8 RID: 712
			public TaskCompletionSource<T> tcs;

			// Token: 0x040002C9 RID: 713
			public AsyncCallback callback;
		}

		// Token: 0x02000087 RID: 135
		[CompilerGenerated]
		private sealed class <>c__DisplayClass1_0
		{
			// Token: 0x060003EA RID: 1002 RVA: 0x000128CC File Offset: 0x00010ACC
			public <>c__DisplayClass1_0()
			{
			}

			// Token: 0x060003EB RID: 1003 RVA: 0x000128D4 File Offset: 0x00010AD4
			internal void <AsAsyncResult>b__0(Task t)
			{
				if (t.IsFaulted)
				{
					this.tcs.TrySetException(t.Exception.InnerExceptions);
				}
				else if (t.IsCanceled)
				{
					this.tcs.TrySetCanceled();
				}
				else
				{
					this.tcs.TrySetResult(null);
				}
				if (this.callback != null)
				{
					this.callback(this.tcs.Task);
				}
			}

			// Token: 0x040002CA RID: 714
			public TaskCompletionSource<object> tcs;

			// Token: 0x040002CB RID: 715
			public AsyncCallback callback;
		}

		// Token: 0x02000088 RID: 136
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <UpcastPrivate>d__11<TDerived, TBase> : IAsyncStateMachine where TDerived : TBase
		{
			// Token: 0x060003EC RID: 1004 RVA: 0x00012944 File Offset: 0x00010B44
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				TBase result;
				try
				{
					ConfiguredTaskAwaitable<TDerived>.ConfiguredTaskAwaiter configuredTaskAwaiter3;
					if (num != 0)
					{
						configuredTaskAwaiter3 = task.ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter3.IsCompleted)
						{
							num2 = 0;
							configuredTaskAwaiter2 = configuredTaskAwaiter3;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<TDerived>.ConfiguredTaskAwaiter, TaskExtensions.<UpcastPrivate>d__11<TDerived, TBase>>(ref configuredTaskAwaiter3, ref this);
							return;
						}
					}
					else
					{
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<TDerived>.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					result = (TBase)((object)configuredTaskAwaiter3.GetResult());
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x060003ED RID: 1005 RVA: 0x00012A08 File Offset: 0x00010C08
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x040002CC RID: 716
			public int <>1__state;

			// Token: 0x040002CD RID: 717
			public AsyncTaskMethodBuilder<TBase> <>t__builder;

			// Token: 0x040002CE RID: 718
			public Task<TDerived> task;

			// Token: 0x040002CF RID: 719
			private ConfiguredTaskAwaitable<TDerived>.ConfiguredTaskAwaiter <>u__1;
		}
	}
}
