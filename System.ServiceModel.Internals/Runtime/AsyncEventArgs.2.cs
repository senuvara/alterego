﻿using System;
using System.Runtime.CompilerServices;

namespace System.Runtime
{
	// Token: 0x0200000B RID: 11
	internal class AsyncEventArgs<TArgument> : AsyncEventArgs
	{
		// Token: 0x17000005 RID: 5
		// (get) Token: 0x0600001C RID: 28 RVA: 0x0000228F File Offset: 0x0000048F
		// (set) Token: 0x0600001D RID: 29 RVA: 0x00002297 File Offset: 0x00000497
		public TArgument Arguments
		{
			[CompilerGenerated]
			get
			{
				return this.<Arguments>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<Arguments>k__BackingField = value;
			}
		}

		// Token: 0x0600001E RID: 30 RVA: 0x000022A0 File Offset: 0x000004A0
		public virtual void Set(AsyncEventArgsCallback callback, TArgument arguments, object state)
		{
			base.SetAsyncState(callback, state);
			this.Arguments = arguments;
		}

		// Token: 0x0600001F RID: 31 RVA: 0x000022B1 File Offset: 0x000004B1
		public AsyncEventArgs()
		{
		}

		// Token: 0x04000048 RID: 72
		[CompilerGenerated]
		private TArgument <Arguments>k__BackingField;
	}
}
