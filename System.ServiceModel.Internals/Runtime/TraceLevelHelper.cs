﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace System.Runtime
{
	// Token: 0x02000035 RID: 53
	internal class TraceLevelHelper
	{
		// Token: 0x06000186 RID: 390 RVA: 0x00006888 File Offset: 0x00004A88
		internal static TraceEventType GetTraceEventType(byte level, byte opcode)
		{
			if (opcode <= 2)
			{
				if (opcode == 1)
				{
					return TraceEventType.Start;
				}
				if (opcode == 2)
				{
					return TraceEventType.Stop;
				}
			}
			else
			{
				if (opcode == 7)
				{
					return TraceEventType.Resume;
				}
				if (opcode == 8)
				{
					return TraceEventType.Suspend;
				}
			}
			return TraceLevelHelper.EtwLevelToTraceEventType[(int)level];
		}

		// Token: 0x06000187 RID: 391 RVA: 0x000068C1 File Offset: 0x00004AC1
		internal static TraceEventType GetTraceEventType(TraceEventLevel level)
		{
			return TraceLevelHelper.EtwLevelToTraceEventType[(int)level];
		}

		// Token: 0x06000188 RID: 392 RVA: 0x000068CA File Offset: 0x00004ACA
		internal static TraceEventType GetTraceEventType(byte level)
		{
			return TraceLevelHelper.EtwLevelToTraceEventType[(int)level];
		}

		// Token: 0x06000189 RID: 393 RVA: 0x000068D4 File Offset: 0x00004AD4
		internal static string LookupSeverity(TraceEventLevel level, TraceEventOpcode opcode)
		{
			if (opcode <= TraceEventOpcode.Stop)
			{
				if (opcode == TraceEventOpcode.Start)
				{
					return "Start";
				}
				if (opcode == TraceEventOpcode.Stop)
				{
					return "Stop";
				}
			}
			else
			{
				if (opcode == TraceEventOpcode.Resume)
				{
					return "Resume";
				}
				if (opcode == TraceEventOpcode.Suspend)
				{
					return "Suspend";
				}
			}
			string result;
			switch (level)
			{
			case TraceEventLevel.Critical:
				result = "Critical";
				break;
			case TraceEventLevel.Error:
				result = "Error";
				break;
			case TraceEventLevel.Warning:
				result = "Warning";
				break;
			case TraceEventLevel.Informational:
				result = "Information";
				break;
			case TraceEventLevel.Verbose:
				result = "Verbose";
				break;
			default:
				result = level.ToString();
				break;
			}
			return result;
		}

		// Token: 0x0600018A RID: 394 RVA: 0x0000696E File Offset: 0x00004B6E
		public TraceLevelHelper()
		{
		}

		// Token: 0x0600018B RID: 395 RVA: 0x00006976 File Offset: 0x00004B76
		// Note: this type is marked as 'beforefieldinit'.
		static TraceLevelHelper()
		{
			TraceEventType[] array = new TraceEventType[6];
			RuntimeHelpers.InitializeArray(array, fieldof(<PrivateImplementationDetails>.BED87A2CCF180E558D235309068F42A19229A3A2).FieldHandle);
			TraceLevelHelper.EtwLevelToTraceEventType = array;
		}

		// Token: 0x040000FD RID: 253
		private static TraceEventType[] EtwLevelToTraceEventType;
	}
}
