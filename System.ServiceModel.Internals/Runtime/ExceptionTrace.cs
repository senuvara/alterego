﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.Diagnostics;
using System.Security;

namespace System.Runtime
{
	// Token: 0x02000019 RID: 25
	internal class ExceptionTrace
	{
		// Token: 0x0600007E RID: 126 RVA: 0x000034DC File Offset: 0x000016DC
		public ExceptionTrace(string eventSourceName, EtwDiagnosticTrace diagnosticTrace)
		{
			this.eventSourceName = eventSourceName;
			this.diagnosticTrace = diagnosticTrace;
		}

		// Token: 0x0600007F RID: 127 RVA: 0x000034F2 File Offset: 0x000016F2
		public void AsInformation(Exception exception)
		{
			TraceCore.HandledException(this.diagnosticTrace, (exception != null) ? exception.ToString() : string.Empty, exception);
		}

		// Token: 0x06000080 RID: 128 RVA: 0x00003510 File Offset: 0x00001710
		public void AsWarning(Exception exception)
		{
			TraceCore.HandledExceptionWarning(this.diagnosticTrace, (exception != null) ? exception.ToString() : string.Empty, exception);
		}

		// Token: 0x06000081 RID: 129 RVA: 0x00003530 File Offset: 0x00001730
		public Exception AsError(Exception exception)
		{
			AggregateException ex = exception as AggregateException;
			if (ex != null)
			{
				return this.AsError<Exception>(ex);
			}
			TargetInvocationException ex2 = exception as TargetInvocationException;
			if (ex2 != null && ex2.InnerException != null)
			{
				return this.AsError(ex2.InnerException);
			}
			return this.TraceException<Exception>(exception);
		}

		// Token: 0x06000082 RID: 130 RVA: 0x00003578 File Offset: 0x00001778
		public Exception AsError(Exception exception, string eventSource)
		{
			AggregateException ex = exception as AggregateException;
			if (ex != null)
			{
				return this.AsError<Exception>(ex, eventSource);
			}
			TargetInvocationException ex2 = exception as TargetInvocationException;
			if (ex2 != null && ex2.InnerException != null)
			{
				return this.AsError(ex2.InnerException, eventSource);
			}
			return this.TraceException<Exception>(exception, eventSource);
		}

		// Token: 0x06000083 RID: 131 RVA: 0x000035C0 File Offset: 0x000017C0
		public Exception AsError(TargetInvocationException targetInvocationException, string eventSource)
		{
			if (Fx.IsFatal(targetInvocationException))
			{
				return targetInvocationException;
			}
			Exception innerException = targetInvocationException.InnerException;
			if (innerException != null)
			{
				return this.AsError(innerException, eventSource);
			}
			return this.TraceException<Exception>(targetInvocationException, eventSource);
		}

		// Token: 0x06000084 RID: 132 RVA: 0x000035F2 File Offset: 0x000017F2
		public Exception AsError<TPreferredException>(AggregateException aggregateException)
		{
			return this.AsError<TPreferredException>(aggregateException, this.eventSourceName);
		}

		// Token: 0x06000085 RID: 133 RVA: 0x00003604 File Offset: 0x00001804
		public Exception AsError<TPreferredException>(AggregateException aggregateException, string eventSource)
		{
			if (Fx.IsFatal(aggregateException))
			{
				return aggregateException;
			}
			ReadOnlyCollection<Exception> innerExceptions = aggregateException.Flatten().InnerExceptions;
			if (innerExceptions.Count == 0)
			{
				return this.TraceException<AggregateException>(aggregateException, eventSource);
			}
			Exception ex = null;
			foreach (Exception ex2 in innerExceptions)
			{
				TargetInvocationException ex3 = ex2 as TargetInvocationException;
				Exception ex4 = (ex3 != null && ex3.InnerException != null) ? ex3.InnerException : ex2;
				if (ex4 is TPreferredException && ex == null)
				{
					ex = ex4;
				}
				this.TraceException<Exception>(ex4, eventSource);
			}
			if (ex == null)
			{
				ex = innerExceptions[0];
			}
			return ex;
		}

		// Token: 0x06000086 RID: 134 RVA: 0x000036B4 File Offset: 0x000018B4
		public ArgumentException Argument(string paramName, string message)
		{
			return this.TraceException<ArgumentException>(new ArgumentException(message, paramName));
		}

		// Token: 0x06000087 RID: 135 RVA: 0x000036C3 File Offset: 0x000018C3
		public ArgumentNullException ArgumentNull(string paramName)
		{
			return this.TraceException<ArgumentNullException>(new ArgumentNullException(paramName));
		}

		// Token: 0x06000088 RID: 136 RVA: 0x000036D1 File Offset: 0x000018D1
		public ArgumentNullException ArgumentNull(string paramName, string message)
		{
			return this.TraceException<ArgumentNullException>(new ArgumentNullException(paramName, message));
		}

		// Token: 0x06000089 RID: 137 RVA: 0x000036E0 File Offset: 0x000018E0
		public ArgumentException ArgumentNullOrEmpty(string paramName)
		{
			return this.Argument(paramName, InternalSR.ArgumentNullOrEmpty(paramName));
		}

		// Token: 0x0600008A RID: 138 RVA: 0x000036EF File Offset: 0x000018EF
		public ArgumentOutOfRangeException ArgumentOutOfRange(string paramName, object actualValue, string message)
		{
			return this.TraceException<ArgumentOutOfRangeException>(new ArgumentOutOfRangeException(paramName, actualValue, message));
		}

		// Token: 0x0600008B RID: 139 RVA: 0x000036FF File Offset: 0x000018FF
		public ObjectDisposedException ObjectDisposed(string message)
		{
			return this.TraceException<ObjectDisposedException>(new ObjectDisposedException(null, message));
		}

		// Token: 0x0600008C RID: 140 RVA: 0x0000370E File Offset: 0x0000190E
		public void TraceUnhandledException(Exception exception)
		{
			TraceCore.UnhandledException(this.diagnosticTrace, (exception != null) ? exception.ToString() : string.Empty, exception);
		}

		// Token: 0x0600008D RID: 141 RVA: 0x0000372C File Offset: 0x0000192C
		public void TraceHandledException(Exception exception, TraceEventType traceEventType)
		{
			if (traceEventType != TraceEventType.Error)
			{
				if (traceEventType != TraceEventType.Warning)
				{
					if (traceEventType != TraceEventType.Verbose)
					{
						if (TraceCore.HandledExceptionIsEnabled(this.diagnosticTrace))
						{
							TraceCore.HandledException(this.diagnosticTrace, (exception != null) ? exception.ToString() : string.Empty, exception);
						}
					}
					else if (TraceCore.HandledExceptionVerboseIsEnabled(this.diagnosticTrace))
					{
						TraceCore.HandledExceptionVerbose(this.diagnosticTrace, (exception != null) ? exception.ToString() : string.Empty, exception);
						return;
					}
				}
				else if (TraceCore.HandledExceptionWarningIsEnabled(this.diagnosticTrace))
				{
					TraceCore.HandledExceptionWarning(this.diagnosticTrace, (exception != null) ? exception.ToString() : string.Empty, exception);
					return;
				}
			}
			else if (TraceCore.HandledExceptionErrorIsEnabled(this.diagnosticTrace))
			{
				TraceCore.HandledExceptionError(this.diagnosticTrace, (exception != null) ? exception.ToString() : string.Empty, exception);
				return;
			}
		}

		// Token: 0x0600008E RID: 142 RVA: 0x000037F8 File Offset: 0x000019F8
		public void TraceEtwException(Exception exception, TraceEventType eventType)
		{
			switch (eventType)
			{
			case TraceEventType.Critical:
				if (TraceCore.EtwUnhandledExceptionIsEnabled(this.diagnosticTrace))
				{
					TraceCore.EtwUnhandledException(this.diagnosticTrace, (exception != null) ? exception.ToString() : string.Empty, exception);
					return;
				}
				return;
			case TraceEventType.Error:
			case TraceEventType.Warning:
				if (TraceCore.ThrowingEtwExceptionIsEnabled(this.diagnosticTrace))
				{
					TraceCore.ThrowingEtwException(this.diagnosticTrace, this.eventSourceName, (exception != null) ? exception.ToString() : string.Empty, exception);
					return;
				}
				return;
			}
			if (TraceCore.ThrowingEtwExceptionVerboseIsEnabled(this.diagnosticTrace))
			{
				TraceCore.ThrowingEtwExceptionVerbose(this.diagnosticTrace, this.eventSourceName, (exception != null) ? exception.ToString() : string.Empty, exception);
			}
		}

		// Token: 0x0600008F RID: 143 RVA: 0x000038A8 File Offset: 0x00001AA8
		private TException TraceException<TException>(TException exception) where TException : Exception
		{
			return this.TraceException<TException>(exception, this.eventSourceName);
		}

		// Token: 0x06000090 RID: 144 RVA: 0x000038B8 File Offset: 0x00001AB8
		[SecuritySafeCritical]
		private TException TraceException<TException>(TException exception, string eventSource) where TException : Exception
		{
			if (TraceCore.ThrowingExceptionIsEnabled(this.diagnosticTrace))
			{
				TraceCore.ThrowingException(this.diagnosticTrace, eventSource, (exception != null) ? exception.ToString() : string.Empty, exception);
			}
			this.BreakOnException(exception);
			return exception;
		}

		// Token: 0x06000091 RID: 145 RVA: 0x0000390B File Offset: 0x00001B0B
		[SecuritySafeCritical]
		private void BreakOnException(Exception exception)
		{
		}

		// Token: 0x06000092 RID: 146 RVA: 0x00003910 File Offset: 0x00001B10
		[MethodImpl(MethodImplOptions.NoInlining)]
		internal void TraceFailFast(string message)
		{
			EventLogger logger = new EventLogger(this.eventSourceName, this.diagnosticTrace);
			this.TraceFailFast(message, logger);
		}

		// Token: 0x06000093 RID: 147 RVA: 0x0000393C File Offset: 0x00001B3C
		[MethodImpl(MethodImplOptions.NoInlining)]
		internal void TraceFailFast(string message, EventLogger logger)
		{
			if (logger != null)
			{
				try
				{
					string text = null;
					try
					{
						text = new StackTrace().ToString();
					}
					catch (Exception ex)
					{
						text = ex.Message;
						if (Fx.IsFatal(ex))
						{
							throw;
						}
					}
					finally
					{
						logger.LogEvent(TraceEventType.Critical, 6, 3221291110U, new string[]
						{
							message,
							text
						});
					}
				}
				catch (Exception ex2)
				{
					logger.LogEvent(TraceEventType.Critical, 6, 3221291111U, new string[]
					{
						ex2.ToString()
					});
					if (Fx.IsFatal(ex2))
					{
						throw;
					}
				}
			}
		}

		// Token: 0x0400009C RID: 156
		private const ushort FailFastEventLogCategory = 6;

		// Token: 0x0400009D RID: 157
		private string eventSourceName;

		// Token: 0x0400009E RID: 158
		private readonly EtwDiagnosticTrace diagnosticTrace;
	}
}
