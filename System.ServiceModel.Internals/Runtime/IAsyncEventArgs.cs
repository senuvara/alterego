﻿using System;

namespace System.Runtime
{
	// Token: 0x0200001F RID: 31
	internal interface IAsyncEventArgs
	{
		// Token: 0x1700001C RID: 28
		// (get) Token: 0x060000BE RID: 190
		object AsyncState { get; }

		// Token: 0x1700001D RID: 29
		// (get) Token: 0x060000BF RID: 191
		Exception Exception { get; }
	}
}
