﻿using System;

namespace System.Diagnostics
{
	// Token: 0x02000006 RID: 6
	internal enum EventLogEntryType
	{
		// Token: 0x0400003A RID: 58
		Error = 1,
		// Token: 0x0400003B RID: 59
		Warning,
		// Token: 0x0400003C RID: 60
		Information = 4,
		// Token: 0x0400003D RID: 61
		SuccessAudit = 8,
		// Token: 0x0400003E RID: 62
		FailureAudit = 16
	}
}
