﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// Token: 0x02000059 RID: 89
[CompilerGenerated]
internal sealed class <PrivateImplementationDetails>
{
	// Token: 0x0400020D RID: 525 RVA: 0x00036A14 File Offset: 0x00034C14
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=64 B9EF176A54F91ED30BF0775494A7BCF7243E156C;

	// Token: 0x0400020E RID: 526 RVA: 0x00036A54 File Offset: 0x00034C54
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=24 BED87A2CCF180E558D235309068F42A19229A3A2;

	// Token: 0x0400020F RID: 527 RVA: 0x00036A6C File Offset: 0x00034C6C
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=256 D002CBBE1FF33721AF7C4D1D3ECAD1B7DB5258B7;

	// Token: 0x0200009A RID: 154
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 24)]
	private struct __StaticArrayInitTypeSize=24
	{
	}

	// Token: 0x0200009B RID: 155
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 64)]
	private struct __StaticArrayInitTypeSize=64
	{
	}

	// Token: 0x0200009C RID: 156
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 256)]
	private struct __StaticArrayInitTypeSize=256
	{
	}
}
