﻿using System;

namespace System.ServiceModel.Internals
{
	// Token: 0x02000005 RID: 5
	internal static class LocalAppContextSwitches
	{
		// Token: 0x06000009 RID: 9 RVA: 0x000020AE File Offset: 0x000002AE
		// Note: this type is marked as 'beforefieldinit'.
		static LocalAppContextSwitches()
		{
		}

		// Token: 0x04000038 RID: 56
		public static readonly bool IncludeNullExceptionMessageInETWTrace;
	}
}
