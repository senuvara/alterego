﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;

// Token: 0x0200000C RID: 12
[CompilerGenerated]
internal sealed class <>f__AnonymousType10<<OperationId>j__TPar, <Operation>j__TPar, <IsolationLevel>j__TPar, <Connection>j__TPar, <TransactionName>j__TPar, <Exception>j__TPar, <Timestamp>j__TPar>
{
	// Token: 0x17000039 RID: 57
	// (get) Token: 0x06000061 RID: 97 RVA: 0x00003ED0 File Offset: 0x000020D0
	public <OperationId>j__TPar OperationId
	{
		get
		{
			return this.<OperationId>i__Field;
		}
	}

	// Token: 0x1700003A RID: 58
	// (get) Token: 0x06000062 RID: 98 RVA: 0x00003ED8 File Offset: 0x000020D8
	public <Operation>j__TPar Operation
	{
		get
		{
			return this.<Operation>i__Field;
		}
	}

	// Token: 0x1700003B RID: 59
	// (get) Token: 0x06000063 RID: 99 RVA: 0x00003EE0 File Offset: 0x000020E0
	public <IsolationLevel>j__TPar IsolationLevel
	{
		get
		{
			return this.<IsolationLevel>i__Field;
		}
	}

	// Token: 0x1700003C RID: 60
	// (get) Token: 0x06000064 RID: 100 RVA: 0x00003EE8 File Offset: 0x000020E8
	public <Connection>j__TPar Connection
	{
		get
		{
			return this.<Connection>i__Field;
		}
	}

	// Token: 0x1700003D RID: 61
	// (get) Token: 0x06000065 RID: 101 RVA: 0x00003EF0 File Offset: 0x000020F0
	public <TransactionName>j__TPar TransactionName
	{
		get
		{
			return this.<TransactionName>i__Field;
		}
	}

	// Token: 0x1700003E RID: 62
	// (get) Token: 0x06000066 RID: 102 RVA: 0x00003EF8 File Offset: 0x000020F8
	public <Exception>j__TPar Exception
	{
		get
		{
			return this.<Exception>i__Field;
		}
	}

	// Token: 0x1700003F RID: 63
	// (get) Token: 0x06000067 RID: 103 RVA: 0x00003F00 File Offset: 0x00002100
	public <Timestamp>j__TPar Timestamp
	{
		get
		{
			return this.<Timestamp>i__Field;
		}
	}

	// Token: 0x06000068 RID: 104 RVA: 0x00003F08 File Offset: 0x00002108
	[DebuggerHidden]
	public <>f__AnonymousType10(<OperationId>j__TPar OperationId, <Operation>j__TPar Operation, <IsolationLevel>j__TPar IsolationLevel, <Connection>j__TPar Connection, <TransactionName>j__TPar TransactionName, <Exception>j__TPar Exception, <Timestamp>j__TPar Timestamp)
	{
		this.<OperationId>i__Field = OperationId;
		this.<Operation>i__Field = Operation;
		this.<IsolationLevel>i__Field = IsolationLevel;
		this.<Connection>i__Field = Connection;
		this.<TransactionName>i__Field = TransactionName;
		this.<Exception>i__Field = Exception;
		this.<Timestamp>i__Field = Timestamp;
	}

	// Token: 0x06000069 RID: 105 RVA: 0x00003F48 File Offset: 0x00002148
	[DebuggerHidden]
	public override bool Equals(object value)
	{
		var <>f__AnonymousType = value as <>f__AnonymousType10<<OperationId>j__TPar, <Operation>j__TPar, <IsolationLevel>j__TPar, <Connection>j__TPar, <TransactionName>j__TPar, <Exception>j__TPar, <Timestamp>j__TPar>;
		return <>f__AnonymousType != null && EqualityComparer<<OperationId>j__TPar>.Default.Equals(this.<OperationId>i__Field, <>f__AnonymousType.<OperationId>i__Field) && EqualityComparer<<Operation>j__TPar>.Default.Equals(this.<Operation>i__Field, <>f__AnonymousType.<Operation>i__Field) && EqualityComparer<<IsolationLevel>j__TPar>.Default.Equals(this.<IsolationLevel>i__Field, <>f__AnonymousType.<IsolationLevel>i__Field) && EqualityComparer<<Connection>j__TPar>.Default.Equals(this.<Connection>i__Field, <>f__AnonymousType.<Connection>i__Field) && EqualityComparer<<TransactionName>j__TPar>.Default.Equals(this.<TransactionName>i__Field, <>f__AnonymousType.<TransactionName>i__Field) && EqualityComparer<<Exception>j__TPar>.Default.Equals(this.<Exception>i__Field, <>f__AnonymousType.<Exception>i__Field) && EqualityComparer<<Timestamp>j__TPar>.Default.Equals(this.<Timestamp>i__Field, <>f__AnonymousType.<Timestamp>i__Field);
	}

	// Token: 0x0600006A RID: 106 RVA: 0x00004010 File Offset: 0x00002210
	[DebuggerHidden]
	public override int GetHashCode()
	{
		return ((((((-260334980 * -1521134295 + EqualityComparer<<OperationId>j__TPar>.Default.GetHashCode(this.<OperationId>i__Field)) * -1521134295 + EqualityComparer<<Operation>j__TPar>.Default.GetHashCode(this.<Operation>i__Field)) * -1521134295 + EqualityComparer<<IsolationLevel>j__TPar>.Default.GetHashCode(this.<IsolationLevel>i__Field)) * -1521134295 + EqualityComparer<<Connection>j__TPar>.Default.GetHashCode(this.<Connection>i__Field)) * -1521134295 + EqualityComparer<<TransactionName>j__TPar>.Default.GetHashCode(this.<TransactionName>i__Field)) * -1521134295 + EqualityComparer<<Exception>j__TPar>.Default.GetHashCode(this.<Exception>i__Field)) * -1521134295 + EqualityComparer<<Timestamp>j__TPar>.Default.GetHashCode(this.<Timestamp>i__Field);
	}

	// Token: 0x0600006B RID: 107 RVA: 0x000040C4 File Offset: 0x000022C4
	[DebuggerHidden]
	public override string ToString()
	{
		IFormatProvider provider = null;
		string format = "{{ OperationId = {0}, Operation = {1}, IsolationLevel = {2}, Connection = {3}, TransactionName = {4}, Exception = {5}, Timestamp = {6} }}";
		object[] array = new object[7];
		int num = 0;
		<OperationId>j__TPar <OperationId>j__TPar = this.<OperationId>i__Field;
		ref <OperationId>j__TPar ptr = ref <OperationId>j__TPar;
		<OperationId>j__TPar <OperationId>j__TPar2 = default(<OperationId>j__TPar);
		object obj;
		if (<OperationId>j__TPar2 == null)
		{
			<OperationId>j__TPar2 = <OperationId>j__TPar;
			ptr = ref <OperationId>j__TPar2;
			if (<OperationId>j__TPar2 == null)
			{
				obj = null;
				goto IL_46;
			}
		}
		obj = ptr.ToString();
		IL_46:
		array[num] = obj;
		int num2 = 1;
		<Operation>j__TPar <Operation>j__TPar = this.<Operation>i__Field;
		ref <Operation>j__TPar ptr2 = ref <Operation>j__TPar;
		<Operation>j__TPar <Operation>j__TPar2 = default(<Operation>j__TPar);
		object obj2;
		if (<Operation>j__TPar2 == null)
		{
			<Operation>j__TPar2 = <Operation>j__TPar;
			ptr2 = ref <Operation>j__TPar2;
			if (<Operation>j__TPar2 == null)
			{
				obj2 = null;
				goto IL_81;
			}
		}
		obj2 = ptr2.ToString();
		IL_81:
		array[num2] = obj2;
		int num3 = 2;
		<IsolationLevel>j__TPar <IsolationLevel>j__TPar = this.<IsolationLevel>i__Field;
		ref <IsolationLevel>j__TPar ptr3 = ref <IsolationLevel>j__TPar;
		<IsolationLevel>j__TPar <IsolationLevel>j__TPar2 = default(<IsolationLevel>j__TPar);
		object obj3;
		if (<IsolationLevel>j__TPar2 == null)
		{
			<IsolationLevel>j__TPar2 = <IsolationLevel>j__TPar;
			ptr3 = ref <IsolationLevel>j__TPar2;
			if (<IsolationLevel>j__TPar2 == null)
			{
				obj3 = null;
				goto IL_C0;
			}
		}
		obj3 = ptr3.ToString();
		IL_C0:
		array[num3] = obj3;
		int num4 = 3;
		<Connection>j__TPar <Connection>j__TPar = this.<Connection>i__Field;
		ref <Connection>j__TPar ptr4 = ref <Connection>j__TPar;
		<Connection>j__TPar <Connection>j__TPar2 = default(<Connection>j__TPar);
		object obj4;
		if (<Connection>j__TPar2 == null)
		{
			<Connection>j__TPar2 = <Connection>j__TPar;
			ptr4 = ref <Connection>j__TPar2;
			if (<Connection>j__TPar2 == null)
			{
				obj4 = null;
				goto IL_FF;
			}
		}
		obj4 = ptr4.ToString();
		IL_FF:
		array[num4] = obj4;
		int num5 = 4;
		<TransactionName>j__TPar <TransactionName>j__TPar = this.<TransactionName>i__Field;
		ref <TransactionName>j__TPar ptr5 = ref <TransactionName>j__TPar;
		<TransactionName>j__TPar <TransactionName>j__TPar2 = default(<TransactionName>j__TPar);
		object obj5;
		if (<TransactionName>j__TPar2 == null)
		{
			<TransactionName>j__TPar2 = <TransactionName>j__TPar;
			ptr5 = ref <TransactionName>j__TPar2;
			if (<TransactionName>j__TPar2 == null)
			{
				obj5 = null;
				goto IL_13E;
			}
		}
		obj5 = ptr5.ToString();
		IL_13E:
		array[num5] = obj5;
		int num6 = 5;
		<Exception>j__TPar <Exception>j__TPar = this.<Exception>i__Field;
		ref <Exception>j__TPar ptr6 = ref <Exception>j__TPar;
		<Exception>j__TPar <Exception>j__TPar2 = default(<Exception>j__TPar);
		object obj6;
		if (<Exception>j__TPar2 == null)
		{
			<Exception>j__TPar2 = <Exception>j__TPar;
			ptr6 = ref <Exception>j__TPar2;
			if (<Exception>j__TPar2 == null)
			{
				obj6 = null;
				goto IL_17D;
			}
		}
		obj6 = ptr6.ToString();
		IL_17D:
		array[num6] = obj6;
		int num7 = 6;
		<Timestamp>j__TPar <Timestamp>j__TPar = this.<Timestamp>i__Field;
		ref <Timestamp>j__TPar ptr7 = ref <Timestamp>j__TPar;
		<Timestamp>j__TPar <Timestamp>j__TPar2 = default(<Timestamp>j__TPar);
		object obj7;
		if (<Timestamp>j__TPar2 == null)
		{
			<Timestamp>j__TPar2 = <Timestamp>j__TPar;
			ptr7 = ref <Timestamp>j__TPar2;
			if (<Timestamp>j__TPar2 == null)
			{
				obj7 = null;
				goto IL_1BC;
			}
		}
		obj7 = ptr7.ToString();
		IL_1BC:
		array[num7] = obj7;
		return string.Format(provider, format, array);
	}

	// Token: 0x04000039 RID: 57
	[DebuggerBrowsable(DebuggerBrowsableState.Never)]
	private readonly <OperationId>j__TPar <OperationId>i__Field;

	// Token: 0x0400003A RID: 58
	[DebuggerBrowsable(DebuggerBrowsableState.Never)]
	private readonly <Operation>j__TPar <Operation>i__Field;

	// Token: 0x0400003B RID: 59
	[DebuggerBrowsable(DebuggerBrowsableState.Never)]
	private readonly <IsolationLevel>j__TPar <IsolationLevel>i__Field;

	// Token: 0x0400003C RID: 60
	[DebuggerBrowsable(DebuggerBrowsableState.Never)]
	private readonly <Connection>j__TPar <Connection>i__Field;

	// Token: 0x0400003D RID: 61
	[DebuggerBrowsable(DebuggerBrowsableState.Never)]
	private readonly <TransactionName>j__TPar <TransactionName>i__Field;

	// Token: 0x0400003E RID: 62
	[DebuggerBrowsable(DebuggerBrowsableState.Never)]
	private readonly <Exception>j__TPar <Exception>i__Field;

	// Token: 0x0400003F RID: 63
	[DebuggerBrowsable(DebuggerBrowsableState.Never)]
	private readonly <Timestamp>j__TPar <Timestamp>i__Field;
}
