﻿using System;

namespace System.Xml
{
	// Token: 0x02000032 RID: 50
	internal enum ElementState
	{
		// Token: 0x04000428 RID: 1064
		None,
		// Token: 0x04000429 RID: 1065
		Defoliated,
		// Token: 0x0400042A RID: 1066
		WeakFoliation,
		// Token: 0x0400042B RID: 1067
		StrongFoliation,
		// Token: 0x0400042C RID: 1068
		Foliating,
		// Token: 0x0400042D RID: 1069
		Defoliating
	}
}
