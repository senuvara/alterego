﻿using System;

namespace System.Xml
{
	// Token: 0x02000029 RID: 41
	internal abstract class BaseTreeIterator
	{
		// Token: 0x060000C7 RID: 199 RVA: 0x00005E3C File Offset: 0x0000403C
		internal BaseTreeIterator(DataSetMapper mapper)
		{
			this.mapper = mapper;
		}

		// Token: 0x1700004C RID: 76
		// (get) Token: 0x060000C8 RID: 200
		internal abstract XmlNode CurrentNode { get; }

		// Token: 0x060000C9 RID: 201
		internal abstract bool Next();

		// Token: 0x060000CA RID: 202
		internal abstract bool NextRight();

		// Token: 0x060000CB RID: 203 RVA: 0x00005E4B File Offset: 0x0000404B
		internal bool NextRowElement()
		{
			while (this.Next())
			{
				if (this.OnRowElement())
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x060000CC RID: 204 RVA: 0x00005E62 File Offset: 0x00004062
		internal bool NextRightRowElement()
		{
			return this.NextRight() && (this.OnRowElement() || this.NextRowElement());
		}

		// Token: 0x060000CD RID: 205 RVA: 0x00005E80 File Offset: 0x00004080
		internal bool OnRowElement()
		{
			XmlBoundElement xmlBoundElement = this.CurrentNode as XmlBoundElement;
			return xmlBoundElement != null && xmlBoundElement.Row != null;
		}

		// Token: 0x04000409 RID: 1033
		protected DataSetMapper mapper;
	}
}
