﻿using System;

namespace System.Xml
{
	// Token: 0x0200002E RID: 46
	internal abstract class BaseRegionIterator : BaseTreeIterator
	{
		// Token: 0x06000131 RID: 305 RVA: 0x000073DE File Offset: 0x000055DE
		internal BaseRegionIterator(DataSetMapper mapper) : base(mapper)
		{
		}
	}
}
