﻿using System;

namespace System.Xml
{
	// Token: 0x02000030 RID: 48
	internal sealed class TreeIterator : BaseTreeIterator
	{
		// Token: 0x06000138 RID: 312 RVA: 0x00007611 File Offset: 0x00005811
		internal TreeIterator(XmlNode nodeTop) : base(((XmlDataDocument)nodeTop.OwnerDocument).Mapper)
		{
			this._nodeTop = nodeTop;
			this._currentNode = nodeTop;
		}

		// Token: 0x1700006D RID: 109
		// (get) Token: 0x06000139 RID: 313 RVA: 0x00007637 File Offset: 0x00005837
		internal override XmlNode CurrentNode
		{
			get
			{
				return this._currentNode;
			}
		}

		// Token: 0x0600013A RID: 314 RVA: 0x00007640 File Offset: 0x00005840
		internal override bool Next()
		{
			XmlNode firstChild = this._currentNode.FirstChild;
			if (firstChild != null)
			{
				this._currentNode = firstChild;
				return true;
			}
			return this.NextRight();
		}

		// Token: 0x0600013B RID: 315 RVA: 0x0000766C File Offset: 0x0000586C
		internal override bool NextRight()
		{
			if (this._currentNode == this._nodeTop)
			{
				this._currentNode = null;
				return false;
			}
			XmlNode xmlNode = this._currentNode.NextSibling;
			if (xmlNode != null)
			{
				this._currentNode = xmlNode;
				return true;
			}
			xmlNode = this._currentNode;
			while (xmlNode != this._nodeTop && xmlNode.NextSibling == null)
			{
				xmlNode = xmlNode.ParentNode;
			}
			if (xmlNode == this._nodeTop)
			{
				this._currentNode = null;
				return false;
			}
			this._currentNode = xmlNode.NextSibling;
			return true;
		}

		// Token: 0x0400041A RID: 1050
		private readonly XmlNode _nodeTop;

		// Token: 0x0400041B RID: 1051
		private XmlNode _currentNode;
	}
}
