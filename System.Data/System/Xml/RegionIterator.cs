﻿using System;
using System.Text;

namespace System.Xml
{
	// Token: 0x0200002F RID: 47
	internal sealed class RegionIterator : BaseRegionIterator
	{
		// Token: 0x06000132 RID: 306 RVA: 0x000073E7 File Offset: 0x000055E7
		internal RegionIterator(XmlBoundElement rowElement) : base(((XmlDataDocument)rowElement.OwnerDocument).Mapper)
		{
			this._rowElement = rowElement;
			this._currentNode = rowElement;
		}

		// Token: 0x1700006C RID: 108
		// (get) Token: 0x06000133 RID: 307 RVA: 0x0000740D File Offset: 0x0000560D
		internal override XmlNode CurrentNode
		{
			get
			{
				return this._currentNode;
			}
		}

		// Token: 0x06000134 RID: 308 RVA: 0x00007418 File Offset: 0x00005618
		internal override bool Next()
		{
			ElementState elementState = this._rowElement.ElementState;
			XmlNode firstChild = this._currentNode.FirstChild;
			if (firstChild != null)
			{
				this._currentNode = firstChild;
				this._rowElement.ElementState = elementState;
				return true;
			}
			return this.NextRight();
		}

		// Token: 0x06000135 RID: 309 RVA: 0x0000745C File Offset: 0x0000565C
		internal override bool NextRight()
		{
			if (this._currentNode == this._rowElement)
			{
				this._currentNode = null;
				return false;
			}
			ElementState elementState = this._rowElement.ElementState;
			XmlNode xmlNode = this._currentNode.NextSibling;
			if (xmlNode != null)
			{
				this._currentNode = xmlNode;
				this._rowElement.ElementState = elementState;
				return true;
			}
			xmlNode = this._currentNode;
			while (xmlNode != this._rowElement && xmlNode.NextSibling == null)
			{
				xmlNode = xmlNode.ParentNode;
			}
			if (xmlNode == this._rowElement)
			{
				this._currentNode = null;
				this._rowElement.ElementState = elementState;
				return false;
			}
			this._currentNode = xmlNode.NextSibling;
			this._rowElement.ElementState = elementState;
			return true;
		}

		// Token: 0x06000136 RID: 310 RVA: 0x00007508 File Offset: 0x00005708
		internal bool NextInitialTextLikeNodes(out string value)
		{
			ElementState elementState = this._rowElement.ElementState;
			XmlNode firstChild = this.CurrentNode.FirstChild;
			value = RegionIterator.GetInitialTextFromNodes(ref firstChild);
			if (firstChild == null)
			{
				this._rowElement.ElementState = elementState;
				return this.NextRight();
			}
			this._currentNode = firstChild;
			this._rowElement.ElementState = elementState;
			return true;
		}

		// Token: 0x06000137 RID: 311 RVA: 0x00007560 File Offset: 0x00005760
		private static string GetInitialTextFromNodes(ref XmlNode n)
		{
			string text = null;
			if (n != null)
			{
				while (n.NodeType == XmlNodeType.Whitespace)
				{
					n = n.NextSibling;
					if (n == null)
					{
						return string.Empty;
					}
				}
				if (XmlDataDocument.IsTextLikeNode(n) && (n.NextSibling == null || !XmlDataDocument.IsTextLikeNode(n.NextSibling)))
				{
					text = n.Value;
					n = n.NextSibling;
				}
				else
				{
					StringBuilder stringBuilder = new StringBuilder();
					while (n != null && XmlDataDocument.IsTextLikeNode(n))
					{
						if (n.NodeType != XmlNodeType.Whitespace)
						{
							stringBuilder.Append(n.Value);
						}
						n = n.NextSibling;
					}
					text = stringBuilder.ToString();
				}
			}
			return text ?? string.Empty;
		}

		// Token: 0x04000418 RID: 1048
		private XmlBoundElement _rowElement;

		// Token: 0x04000419 RID: 1049
		private XmlNode _currentNode;
	}
}
