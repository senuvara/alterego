﻿using System;
using System.Data;

namespace System.Xml
{
	// Token: 0x0200002D RID: 45
	internal interface IXmlDataVirtualNode
	{
		// Token: 0x0600012D RID: 301
		bool IsOnNode(XmlNode nodeToCheck);

		// Token: 0x0600012E RID: 302
		bool IsOnColumn(DataColumn col);

		// Token: 0x0600012F RID: 303
		bool IsInUse();

		// Token: 0x06000130 RID: 304
		void OnFoliated(XmlNode foliatedNode);
	}
}
