﻿using System;

namespace System.Xml
{
	// Token: 0x02000035 RID: 53
	internal sealed class XmlDataImplementation : XmlImplementation
	{
		// Token: 0x0600021F RID: 543 RVA: 0x0000CFEC File Offset: 0x0000B1EC
		public XmlDataImplementation()
		{
		}

		// Token: 0x06000220 RID: 544 RVA: 0x0000CFF4 File Offset: 0x0000B1F4
		public override XmlDocument CreateDocument()
		{
			return new XmlDataDocument(this);
		}
	}
}
