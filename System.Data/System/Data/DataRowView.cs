﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading;
using Unity;

namespace System.Data
{
	/// <summary>Represents a customized view of a <see cref="T:System.Data.DataRow" />.</summary>
	// Token: 0x02000085 RID: 133
	public class DataRowView : ICustomTypeDescriptor, IEditableObject, IDataErrorInfo, INotifyPropertyChanged
	{
		// Token: 0x0600066A RID: 1642 RVA: 0x00019BDB File Offset: 0x00017DDB
		internal DataRowView(DataView dataView, DataRow row)
		{
			this._dataView = dataView;
			this._row = row;
		}

		/// <summary>Gets a value indicating whether the current <see cref="T:System.Data.DataRowView" /> is identical to the specified object.</summary>
		/// <param name="other">An <see cref="T:System.Object" /> to be compared. </param>
		/// <returns>
		///     <see langword="true" /> if <paramref name="object" /> is a <see cref="T:System.Data.DataRowView" /> and it returns the same row as the current <see cref="T:System.Data.DataRowView" />; otherwise <see langword="false" />.</returns>
		// Token: 0x0600066B RID: 1643 RVA: 0x00019BF1 File Offset: 0x00017DF1
		public override bool Equals(object other)
		{
			return this == other;
		}

		/// <summary>Returns the hash code of the <see cref="T:System.Data.DataRow" /> object.</summary>
		/// <returns>A 32-bit signed integer hash code 1, which represents Boolean <see langword="true" /> if the value of this instance is nonzero; otherwise the integer zero, which represents Boolean <see langword="false" />.</returns>
		// Token: 0x0600066C RID: 1644 RVA: 0x00019BF7 File Offset: 0x00017DF7
		public override int GetHashCode()
		{
			return this.Row.GetHashCode();
		}

		/// <summary>Gets the <see cref="T:System.Data.DataView" /> to which this row belongs.</summary>
		/// <returns>The <see langword="DataView" /> to which this row belongs.</returns>
		// Token: 0x17000132 RID: 306
		// (get) Token: 0x0600066D RID: 1645 RVA: 0x00019C04 File Offset: 0x00017E04
		public DataView DataView
		{
			get
			{
				return this._dataView;
			}
		}

		// Token: 0x17000133 RID: 307
		// (get) Token: 0x0600066E RID: 1646 RVA: 0x00019C0C File Offset: 0x00017E0C
		internal int ObjectID
		{
			get
			{
				return this._row._objectID;
			}
		}

		/// <summary>Gets or sets a value in a specified column.</summary>
		/// <param name="ndx">The specified column. </param>
		/// <returns>The value of the column.</returns>
		// Token: 0x17000134 RID: 308
		public object this[int ndx]
		{
			get
			{
				return this.Row[ndx, this.RowVersionDefault];
			}
			set
			{
				if (!this._dataView.AllowEdit && !this.IsNew)
				{
					throw ExceptionBuilder.CanNotEdit();
				}
				this.SetColumnValue(this._dataView.Table.Columns[ndx], value);
			}
		}

		/// <summary>Gets or sets a value in a specified column.</summary>
		/// <param name="property">String that contains the specified column. </param>
		/// <returns>The value of the column.</returns>
		// Token: 0x17000135 RID: 309
		public object this[string property]
		{
			get
			{
				DataColumn dataColumn = this._dataView.Table.Columns[property];
				if (dataColumn != null)
				{
					return this.Row[dataColumn, this.RowVersionDefault];
				}
				if (this._dataView.Table.DataSet != null && this._dataView.Table.DataSet.Relations.Contains(property))
				{
					return this.CreateChildView(property);
				}
				throw ExceptionBuilder.PropertyNotFound(property, this._dataView.Table.TableName);
			}
			set
			{
				DataColumn dataColumn = this._dataView.Table.Columns[property];
				if (dataColumn == null)
				{
					throw ExceptionBuilder.SetFailed(property);
				}
				if (!this._dataView.AllowEdit && !this.IsNew)
				{
					throw ExceptionBuilder.CanNotEdit();
				}
				this.SetColumnValue(dataColumn, value);
			}
		}

		/// <summary>Gets the error message for the property with the given name.</summary>
		/// <param name="colName">The name of the property whose error message to get. </param>
		/// <returns>The error message for the property. The default is an empty string ("").</returns>
		// Token: 0x17000136 RID: 310
		string IDataErrorInfo.this[string colName]
		{
			get
			{
				return this.Row.GetColumnError(colName);
			}
		}

		/// <summary>Gets a message that describes any validation errors for the object.</summary>
		/// <returns>The validation error on the object.</returns>
		// Token: 0x17000137 RID: 311
		// (get) Token: 0x06000674 RID: 1652 RVA: 0x00019D4F File Offset: 0x00017F4F
		string IDataErrorInfo.Error
		{
			get
			{
				return this.Row.RowError;
			}
		}

		/// <summary>Gets the current version description of the <see cref="T:System.Data.DataRow" />.</summary>
		/// <returns>One of the <see cref="T:System.Data.DataRowVersion" /> values. Possible values for the <see cref="P:System.Data.DataRowView.RowVersion" /> property are <see langword="Default" />, <see langword="Original" />, <see langword="Current" />, and <see langword="Proposed" />.</returns>
		// Token: 0x17000138 RID: 312
		// (get) Token: 0x06000675 RID: 1653 RVA: 0x00019D5C File Offset: 0x00017F5C
		public DataRowVersion RowVersion
		{
			get
			{
				return this.RowVersionDefault & (DataRowVersion)(-1025);
			}
		}

		// Token: 0x17000139 RID: 313
		// (get) Token: 0x06000676 RID: 1654 RVA: 0x00019D6A File Offset: 0x00017F6A
		private DataRowVersion RowVersionDefault
		{
			get
			{
				return this.Row.GetDefaultRowVersion(this._dataView.RowStateFilter);
			}
		}

		// Token: 0x06000677 RID: 1655 RVA: 0x00019D82 File Offset: 0x00017F82
		internal int GetRecord()
		{
			return this.Row.GetRecordFromVersion(this.RowVersionDefault);
		}

		// Token: 0x06000678 RID: 1656 RVA: 0x00019D95 File Offset: 0x00017F95
		internal bool HasRecord()
		{
			return this.Row.HasVersion(this.RowVersionDefault);
		}

		// Token: 0x06000679 RID: 1657 RVA: 0x00019DA8 File Offset: 0x00017FA8
		internal object GetColumnValue(DataColumn column)
		{
			return this.Row[column, this.RowVersionDefault];
		}

		// Token: 0x0600067A RID: 1658 RVA: 0x00019DBC File Offset: 0x00017FBC
		internal void SetColumnValue(DataColumn column, object value)
		{
			if (this._delayBeginEdit)
			{
				this._delayBeginEdit = false;
				this.Row.BeginEdit();
			}
			if (DataRowVersion.Original == this.RowVersionDefault)
			{
				throw ExceptionBuilder.SetFailed(column.ColumnName);
			}
			this.Row[column] = value;
		}

		/// <summary>Returns a <see cref="T:System.Data.DataView" /> for the child <see cref="T:System.Data.DataTable" /> with the specified <see cref="T:System.Data.DataRelation" /> and parent..</summary>
		/// <param name="relation">The <see cref="T:System.Data.DataRelation" /> object.</param>
		/// <param name="followParent">The parent object.</param>
		/// <returns>A <see cref="T:System.Data.DataView" /> for the child <see cref="T:System.Data.DataTable" />.</returns>
		// Token: 0x0600067B RID: 1659 RVA: 0x00019E0C File Offset: 0x0001800C
		public DataView CreateChildView(DataRelation relation, bool followParent)
		{
			if (relation == null || relation.ParentKey.Table != this.DataView.Table)
			{
				throw ExceptionBuilder.CreateChildView();
			}
			RelatedView relatedView;
			if (!followParent)
			{
				int record = this.GetRecord();
				object[] keyValues = relation.ParentKey.GetKeyValues(record);
				relatedView = new RelatedView(relation.ChildColumnsReference, keyValues);
			}
			else
			{
				relatedView = new RelatedView(this, relation.ParentKey, relation.ChildColumnsReference);
			}
			relatedView.SetIndex("", DataViewRowState.CurrentRows, null);
			relatedView.SetDataViewManager(this.DataView.DataViewManager);
			return relatedView;
		}

		/// <summary>Returns a <see cref="T:System.Data.DataView" /> for the child <see cref="T:System.Data.DataTable" /> with the specified child <see cref="T:System.Data.DataRelation" />.</summary>
		/// <param name="relation">The <see cref="T:System.Data.DataRelation" /> object. </param>
		/// <returns>a <see cref="T:System.Data.DataView" /> for the child <see cref="T:System.Data.DataTable" />.</returns>
		// Token: 0x0600067C RID: 1660 RVA: 0x00019E99 File Offset: 0x00018099
		public DataView CreateChildView(DataRelation relation)
		{
			return this.CreateChildView(relation, false);
		}

		/// <summary>Returns a <see cref="T:System.Data.DataView" /> for the child <see cref="T:System.Data.DataTable" /> with the specified <see cref="T:System.Data.DataRelation" /> name and parent.</summary>
		/// <param name="relationName">A string containing the <see cref="T:System.Data.DataRelation" /> name.</param>
		/// <param name="followParent">The parent</param>
		/// <returns>a <see cref="T:System.Data.DataView" /> for the child <see cref="T:System.Data.DataTable" />.</returns>
		// Token: 0x0600067D RID: 1661 RVA: 0x00019EA3 File Offset: 0x000180A3
		public DataView CreateChildView(string relationName, bool followParent)
		{
			return this.CreateChildView(this.DataView.Table.ChildRelations[relationName], followParent);
		}

		/// <summary>Returns a <see cref="T:System.Data.DataView" /> for the child <see cref="T:System.Data.DataTable" /> with the specified child <see cref="T:System.Data.DataRelation" /> name.</summary>
		/// <param name="relationName">A string containing the <see cref="T:System.Data.DataRelation" /> name. </param>
		/// <returns>a <see cref="T:System.Data.DataView" /> for the child <see cref="T:System.Data.DataTable" />.</returns>
		// Token: 0x0600067E RID: 1662 RVA: 0x00019EC2 File Offset: 0x000180C2
		public DataView CreateChildView(string relationName)
		{
			return this.CreateChildView(relationName, false);
		}

		/// <summary>Gets the <see cref="T:System.Data.DataRow" /> being viewed.</summary>
		/// <returns>The <see cref="T:System.Data.DataRow" /> being viewed by the <see cref="T:System.Data.DataRowView" />.</returns>
		// Token: 0x1700013A RID: 314
		// (get) Token: 0x0600067F RID: 1663 RVA: 0x00019ECC File Offset: 0x000180CC
		public DataRow Row
		{
			get
			{
				return this._row;
			}
		}

		/// <summary>Begins an edit procedure.</summary>
		// Token: 0x06000680 RID: 1664 RVA: 0x00019ED4 File Offset: 0x000180D4
		public void BeginEdit()
		{
			this._delayBeginEdit = true;
		}

		/// <summary>Cancels an edit procedure.</summary>
		// Token: 0x06000681 RID: 1665 RVA: 0x00019EE0 File Offset: 0x000180E0
		public void CancelEdit()
		{
			DataRow row = this.Row;
			if (this.IsNew)
			{
				this._dataView.FinishAddNew(false);
			}
			else
			{
				row.CancelEdit();
			}
			this._delayBeginEdit = false;
		}

		/// <summary>Commits changes to the underlying <see cref="T:System.Data.DataRow" /> and ends the editing session that was begun with <see cref="M:System.Data.DataRowView.BeginEdit" />.  Use <see cref="M:System.Data.DataRowView.CancelEdit" /> to discard the changes made to the <see cref="T:System.Data.DataRow" />.</summary>
		// Token: 0x06000682 RID: 1666 RVA: 0x00019F17 File Offset: 0x00018117
		public void EndEdit()
		{
			if (this.IsNew)
			{
				this._dataView.FinishAddNew(true);
			}
			else
			{
				this.Row.EndEdit();
			}
			this._delayBeginEdit = false;
		}

		/// <summary>Indicates whether a <see cref="T:System.Data.DataRowView" /> is new.</summary>
		/// <returns>
		///     <see langword="true" /> if the row is new; otherwise <see langword="false" />.</returns>
		// Token: 0x1700013B RID: 315
		// (get) Token: 0x06000683 RID: 1667 RVA: 0x00019F41 File Offset: 0x00018141
		public bool IsNew
		{
			get
			{
				return this._row == this._dataView._addNewRow;
			}
		}

		/// <summary>Indicates whether the row is in edit mode.</summary>
		/// <returns>
		///     <see langword="true" /> if the row is in edit mode; otherwise <see langword="false" />.</returns>
		// Token: 0x1700013C RID: 316
		// (get) Token: 0x06000684 RID: 1668 RVA: 0x00019F56 File Offset: 0x00018156
		public bool IsEdit
		{
			get
			{
				return this.Row.HasVersion(DataRowVersion.Proposed) || this._delayBeginEdit;
			}
		}

		/// <summary>Deletes a row.</summary>
		// Token: 0x06000685 RID: 1669 RVA: 0x00019F72 File Offset: 0x00018172
		public void Delete()
		{
			this._dataView.Delete(this.Row);
		}

		/// <summary>Event that is raised when a <see cref="T:System.Data.DataRowView" /> property is changed.</summary>
		// Token: 0x1400000A RID: 10
		// (add) Token: 0x06000686 RID: 1670 RVA: 0x00019F88 File Offset: 0x00018188
		// (remove) Token: 0x06000687 RID: 1671 RVA: 0x00019FC0 File Offset: 0x000181C0
		public event PropertyChangedEventHandler PropertyChanged
		{
			[CompilerGenerated]
			add
			{
				PropertyChangedEventHandler propertyChangedEventHandler = this.PropertyChanged;
				PropertyChangedEventHandler propertyChangedEventHandler2;
				do
				{
					propertyChangedEventHandler2 = propertyChangedEventHandler;
					PropertyChangedEventHandler value2 = (PropertyChangedEventHandler)Delegate.Combine(propertyChangedEventHandler2, value);
					propertyChangedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler>(ref this.PropertyChanged, value2, propertyChangedEventHandler2);
				}
				while (propertyChangedEventHandler != propertyChangedEventHandler2);
			}
			[CompilerGenerated]
			remove
			{
				PropertyChangedEventHandler propertyChangedEventHandler = this.PropertyChanged;
				PropertyChangedEventHandler propertyChangedEventHandler2;
				do
				{
					propertyChangedEventHandler2 = propertyChangedEventHandler;
					PropertyChangedEventHandler value2 = (PropertyChangedEventHandler)Delegate.Remove(propertyChangedEventHandler2, value);
					propertyChangedEventHandler = Interlocked.CompareExchange<PropertyChangedEventHandler>(ref this.PropertyChanged, value2, propertyChangedEventHandler2);
				}
				while (propertyChangedEventHandler != propertyChangedEventHandler2);
			}
		}

		// Token: 0x06000688 RID: 1672 RVA: 0x00019FF5 File Offset: 0x000181F5
		internal void RaisePropertyChangedEvent(string propName)
		{
			PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
			if (propertyChanged == null)
			{
				return;
			}
			propertyChanged(this, new PropertyChangedEventArgs(propName));
		}

		/// <summary>Returns a collection of custom attributes for this instance of a component.</summary>
		/// <returns>An AttributeCollection containing the attributes for this object.</returns>
		// Token: 0x06000689 RID: 1673 RVA: 0x0001A00E File Offset: 0x0001820E
		AttributeCollection ICustomTypeDescriptor.GetAttributes()
		{
			return new AttributeCollection(null);
		}

		/// <summary>Returns the class name of this instance of a component.</summary>
		/// <returns>The class name of this instance of a component.</returns>
		// Token: 0x0600068A RID: 1674 RVA: 0x00004526 File Offset: 0x00002726
		string ICustomTypeDescriptor.GetClassName()
		{
			return null;
		}

		/// <summary>Returns the name of this instance of a component.</summary>
		/// <returns>The name of this instance of a component.</returns>
		// Token: 0x0600068B RID: 1675 RVA: 0x00004526 File Offset: 0x00002726
		string ICustomTypeDescriptor.GetComponentName()
		{
			return null;
		}

		/// <summary>Returns a type converter for this instance of a component.</summary>
		/// <returns>The type converter for this instance of a component.</returns>
		// Token: 0x0600068C RID: 1676 RVA: 0x00004526 File Offset: 0x00002726
		TypeConverter ICustomTypeDescriptor.GetConverter()
		{
			return null;
		}

		/// <summary>Returns the default event for this instance of a component.</summary>
		/// <returns>The default event for this instance of a component.</returns>
		// Token: 0x0600068D RID: 1677 RVA: 0x00004526 File Offset: 0x00002726
		EventDescriptor ICustomTypeDescriptor.GetDefaultEvent()
		{
			return null;
		}

		/// <summary>Returns the default property for this instance of a component.</summary>
		/// <returns>The default property for this instance of a component.</returns>
		// Token: 0x0600068E RID: 1678 RVA: 0x00004526 File Offset: 0x00002726
		PropertyDescriptor ICustomTypeDescriptor.GetDefaultProperty()
		{
			return null;
		}

		/// <summary>Returns an editor of the specified type for this instance of a component.</summary>
		/// <param name="editorBaseType">A <see cref="T:System.Type" /> that represents the editor for this object. </param>
		/// <returns>An <see cref="T:System.Object" /> of the specified type that is the editor for this object, or <see langword="null" /> if the editor cannot be found.</returns>
		// Token: 0x0600068F RID: 1679 RVA: 0x00004526 File Offset: 0x00002726
		object ICustomTypeDescriptor.GetEditor(Type editorBaseType)
		{
			return null;
		}

		/// <summary>Returns the events for this instance of a component.</summary>
		/// <returns>The events for this instance of a component.</returns>
		// Token: 0x06000690 RID: 1680 RVA: 0x0001A016 File Offset: 0x00018216
		EventDescriptorCollection ICustomTypeDescriptor.GetEvents()
		{
			return new EventDescriptorCollection(null);
		}

		/// <summary>Returns the events for this instance of a component with specified attributes.</summary>
		/// <param name="attributes">The attributes</param>
		/// <returns>The events for this instance of a component.</returns>
		// Token: 0x06000691 RID: 1681 RVA: 0x0001A016 File Offset: 0x00018216
		EventDescriptorCollection ICustomTypeDescriptor.GetEvents(Attribute[] attributes)
		{
			return new EventDescriptorCollection(null);
		}

		/// <summary>Returns the properties for this instance of a component.</summary>
		/// <returns>The properties for this instance of a component.</returns>
		// Token: 0x06000692 RID: 1682 RVA: 0x0001A01E File Offset: 0x0001821E
		PropertyDescriptorCollection ICustomTypeDescriptor.GetProperties()
		{
			return ((ICustomTypeDescriptor)this).GetProperties(null);
		}

		/// <summary>Returns the properties for this instance of a component with specified attributes.</summary>
		/// <param name="attributes">The attributes.</param>
		/// <returns>The properties for this instance of a component.</returns>
		// Token: 0x06000693 RID: 1683 RVA: 0x0001A027 File Offset: 0x00018227
		PropertyDescriptorCollection ICustomTypeDescriptor.GetProperties(Attribute[] attributes)
		{
			if (this._dataView.Table == null)
			{
				return DataRowView.s_zeroPropertyDescriptorCollection;
			}
			return this._dataView.Table.GetPropertyDescriptorCollection(attributes);
		}

		/// <summary>Returns an object that contains the property described by the specified property descriptor.</summary>
		/// <param name="pd">A <see cref="T:System.ComponentModel.PropertyDescriptor" /> that represents the property whose owner is to be found. </param>
		/// <returns>An <see cref="T:System.Object" /> that represents the owner of the specified property.</returns>
		// Token: 0x06000694 RID: 1684 RVA: 0x00005D82 File Offset: 0x00003F82
		object ICustomTypeDescriptor.GetPropertyOwner(PropertyDescriptor pd)
		{
			return this;
		}

		// Token: 0x06000695 RID: 1685 RVA: 0x0001A04D File Offset: 0x0001824D
		// Note: this type is marked as 'beforefieldinit'.
		static DataRowView()
		{
		}

		// Token: 0x06000696 RID: 1686 RVA: 0x00010458 File Offset: 0x0000E658
		internal DataRowView()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x040005A4 RID: 1444
		private readonly DataView _dataView;

		// Token: 0x040005A5 RID: 1445
		private readonly DataRow _row;

		// Token: 0x040005A6 RID: 1446
		private bool _delayBeginEdit;

		// Token: 0x040005A7 RID: 1447
		private static readonly PropertyDescriptorCollection s_zeroPropertyDescriptorCollection = new PropertyDescriptorCollection(null);

		// Token: 0x040005A8 RID: 1448
		[CompilerGenerated]
		private PropertyChangedEventHandler PropertyChanged;
	}
}
