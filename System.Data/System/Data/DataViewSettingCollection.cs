﻿using System;
using System.Collections;
using System.ComponentModel;
using Unity;

namespace System.Data
{
	/// <summary>Contains a read-only collection of <see cref="T:System.Data.DataViewSetting" /> objects for each <see cref="T:System.Data.DataTable" /> in a <see cref="T:System.Data.DataSet" />.</summary>
	// Token: 0x0200009F RID: 159
	public class DataViewSettingCollection : ICollection, IEnumerable
	{
		// Token: 0x060009F0 RID: 2544 RVA: 0x0002CDD3 File Offset: 0x0002AFD3
		internal DataViewSettingCollection(DataViewManager dataViewManager)
		{
			this._list = new Hashtable();
			base..ctor();
			if (dataViewManager == null)
			{
				throw ExceptionBuilder.ArgumentNull("dataViewManager");
			}
			this._dataViewManager = dataViewManager;
		}

		/// <summary>Gets the <see cref="T:System.Data.DataViewSetting" /> objects of the specified <see cref="T:System.Data.DataTable" /> from the collection. </summary>
		/// <param name="table">The <see cref="T:System.Data.DataTable" /> to find. </param>
		/// <returns>A collection of <see cref="T:System.Data.DataViewSetting" /> objects.</returns>
		// Token: 0x170001D2 RID: 466
		public virtual DataViewSetting this[DataTable table]
		{
			get
			{
				if (table == null)
				{
					throw ExceptionBuilder.ArgumentNull("table");
				}
				DataViewSetting dataViewSetting = (DataViewSetting)this._list[table];
				if (dataViewSetting == null)
				{
					dataViewSetting = new DataViewSetting();
					this[table] = dataViewSetting;
				}
				return dataViewSetting;
			}
			set
			{
				if (table == null)
				{
					throw ExceptionBuilder.ArgumentNull("table");
				}
				value.SetDataViewManager(this._dataViewManager);
				value.SetDataTable(table);
				this._list[table] = value;
			}
		}

		// Token: 0x060009F3 RID: 2547 RVA: 0x0002CE6C File Offset: 0x0002B06C
		private DataTable GetTable(string tableName)
		{
			DataTable result = null;
			DataSet dataSet = this._dataViewManager.DataSet;
			if (dataSet != null)
			{
				result = dataSet.Tables[tableName];
			}
			return result;
		}

		// Token: 0x060009F4 RID: 2548 RVA: 0x0002CE98 File Offset: 0x0002B098
		private DataTable GetTable(int index)
		{
			DataTable result = null;
			DataSet dataSet = this._dataViewManager.DataSet;
			if (dataSet != null)
			{
				result = dataSet.Tables[index];
			}
			return result;
		}

		/// <summary>Gets the <see cref="T:System.Data.DataViewSetting" /> of the <see cref="T:System.Data.DataTable" /> specified by its name.</summary>
		/// <param name="tableName">The name of the <see cref="T:System.Data.DataTable" /> to find. </param>
		/// <returns>A collection of <see cref="T:System.Data.DataViewSetting" /> objects.</returns>
		// Token: 0x170001D3 RID: 467
		public virtual DataViewSetting this[string tableName]
		{
			get
			{
				DataTable table = this.GetTable(tableName);
				if (table != null)
				{
					return this[table];
				}
				return null;
			}
		}

		/// <summary>Gets the <see cref="T:System.Data.DataViewSetting" /> objects of the <see cref="T:System.Data.DataTable" /> specified by its index.</summary>
		/// <param name="index">The zero-based index of the <see cref="T:System.Data.DataTable" /> to find. </param>
		/// <returns>A collection of <see cref="T:System.Data.DataViewSetting" /> objects.</returns>
		// Token: 0x170001D4 RID: 468
		public virtual DataViewSetting this[int index]
		{
			get
			{
				DataTable table = this.GetTable(index);
				if (table != null)
				{
					return this[table];
				}
				return null;
			}
			set
			{
				DataTable table = this.GetTable(index);
				if (table != null)
				{
					this[table] = value;
				}
			}
		}

		/// <summary>Copies the collection objects to a one-dimensional <see cref="T:System.Array" /> instance starting at the specified index.</summary>
		/// <param name="ar">The one-dimensional <see cref="T:System.Array" /> that is the destination of the values copied from the collection. </param>
		/// <param name="index">The index of the array at which to start inserting. </param>
		// Token: 0x060009F8 RID: 2552 RVA: 0x0002CF2C File Offset: 0x0002B12C
		public void CopyTo(Array ar, int index)
		{
			foreach (object value in this)
			{
				ar.SetValue(value, index++);
			}
		}

		/// <summary>Copies the collection objects to a one-dimensional <see cref="T:System.Array" /> instance starting at the specified index.</summary>
		/// <param name="ar">The one-dimensional <see cref="T:System.Array" /> that is the destination of the values copied from the collection. </param>
		/// <param name="index">The index of the array at which to start inserting. </param>
		// Token: 0x060009F9 RID: 2553 RVA: 0x0002CF5C File Offset: 0x0002B15C
		public void CopyTo(DataViewSetting[] ar, int index)
		{
			foreach (object value in this)
			{
				ar.SetValue(value, index++);
			}
		}

		/// <summary>Gets the number of <see cref="T:System.Data.DataViewSetting" /> objects in the <see cref="T:System.Data.DataViewSettingCollection" />.</summary>
		/// <returns>The number of <see cref="T:System.Data.DataViewSetting" /> objects in the collection.</returns>
		// Token: 0x170001D5 RID: 469
		// (get) Token: 0x060009FA RID: 2554 RVA: 0x0002CF8C File Offset: 0x0002B18C
		[Browsable(false)]
		public virtual int Count
		{
			get
			{
				DataSet dataSet = this._dataViewManager.DataSet;
				if (dataSet != null)
				{
					return dataSet.Tables.Count;
				}
				return 0;
			}
		}

		/// <summary>Gets an <see cref="T:System.Collections.IEnumerator" /> for the collection.</summary>
		/// <returns>An <see cref="T:System.Collections.IEnumerator" /> object.</returns>
		// Token: 0x060009FB RID: 2555 RVA: 0x0002CFB5 File Offset: 0x0002B1B5
		public IEnumerator GetEnumerator()
		{
			return new DataViewSettingCollection.DataViewSettingsEnumerator(this._dataViewManager);
		}

		/// <summary>Gets a value that indicates whether the <see cref="T:System.Data.DataViewSettingCollection" /> is read-only.</summary>
		/// <returns>Returns <see langword="true" />.</returns>
		// Token: 0x170001D6 RID: 470
		// (get) Token: 0x060009FC RID: 2556 RVA: 0x0000EF1B File Offset: 0x0000D11B
		[Browsable(false)]
		public bool IsReadOnly
		{
			get
			{
				return true;
			}
		}

		/// <summary>Gets a value that indicates whether access to the <see cref="T:System.Data.DataViewSettingCollection" /> is synchronized (thread-safe).</summary>
		/// <returns>This property is always <see langword="false" />, unless overridden by a derived class.</returns>
		// Token: 0x170001D7 RID: 471
		// (get) Token: 0x060009FD RID: 2557 RVA: 0x000061C5 File Offset: 0x000043C5
		[Browsable(false)]
		public bool IsSynchronized
		{
			get
			{
				return false;
			}
		}

		/// <summary>Gets an object that can be used to synchronize access to the <see cref="T:System.Data.DataViewSettingCollection" />.</summary>
		/// <returns>An object that can be used to synchronize access to the <see cref="T:System.Data.DataViewSettingCollection" />.</returns>
		// Token: 0x170001D8 RID: 472
		// (get) Token: 0x060009FE RID: 2558 RVA: 0x00005D82 File Offset: 0x00003F82
		[Browsable(false)]
		public object SyncRoot
		{
			get
			{
				return this;
			}
		}

		// Token: 0x060009FF RID: 2559 RVA: 0x0002CFC2 File Offset: 0x0002B1C2
		internal void Remove(DataTable table)
		{
			this._list.Remove(table);
		}

		// Token: 0x06000A00 RID: 2560 RVA: 0x00010458 File Offset: 0x0000E658
		internal DataViewSettingCollection()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x04000681 RID: 1665
		private readonly DataViewManager _dataViewManager;

		// Token: 0x04000682 RID: 1666
		private readonly Hashtable _list;

		// Token: 0x020000A0 RID: 160
		private sealed class DataViewSettingsEnumerator : IEnumerator
		{
			// Token: 0x06000A01 RID: 2561 RVA: 0x0002CFD0 File Offset: 0x0002B1D0
			public DataViewSettingsEnumerator(DataViewManager dvm)
			{
				if (dvm.DataSet != null)
				{
					this._dataViewSettings = dvm.DataViewSettings;
					this._tableEnumerator = dvm.DataSet.Tables.GetEnumerator();
					return;
				}
				this._dataViewSettings = null;
				this._tableEnumerator = Array.Empty<DataTable>().GetEnumerator();
			}

			// Token: 0x06000A02 RID: 2562 RVA: 0x0002D025 File Offset: 0x0002B225
			public bool MoveNext()
			{
				return this._tableEnumerator.MoveNext();
			}

			// Token: 0x06000A03 RID: 2563 RVA: 0x0002D032 File Offset: 0x0002B232
			public void Reset()
			{
				this._tableEnumerator.Reset();
			}

			// Token: 0x170001D9 RID: 473
			// (get) Token: 0x06000A04 RID: 2564 RVA: 0x0002D03F File Offset: 0x0002B23F
			public object Current
			{
				get
				{
					return this._dataViewSettings[(DataTable)this._tableEnumerator.Current];
				}
			}

			// Token: 0x04000683 RID: 1667
			private DataViewSettingCollection _dataViewSettings;

			// Token: 0x04000684 RID: 1668
			private IEnumerator _tableEnumerator;
		}
	}
}
