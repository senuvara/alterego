﻿using System;

namespace System.Data
{
	// Token: 0x020000B2 RID: 178
	internal sealed class OperatorInfo
	{
		// Token: 0x06000A8C RID: 2700 RVA: 0x00031EE2 File Offset: 0x000300E2
		internal OperatorInfo(Nodes type, int op, int pri)
		{
			this._type = type;
			this._op = op;
			this._priority = pri;
		}

		// Token: 0x04000732 RID: 1842
		internal Nodes _type;

		// Token: 0x04000733 RID: 1843
		internal int _op;

		// Token: 0x04000734 RID: 1844
		internal int _priority;
	}
}
