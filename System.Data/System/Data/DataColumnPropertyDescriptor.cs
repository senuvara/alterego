﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data.Common;
using System.Runtime.CompilerServices;

namespace System.Data
{
	// Token: 0x02000065 RID: 101
	internal sealed class DataColumnPropertyDescriptor : PropertyDescriptor
	{
		// Token: 0x060003F2 RID: 1010 RVA: 0x00013F46 File Offset: 0x00012146
		internal DataColumnPropertyDescriptor(DataColumn dataColumn) : base(dataColumn.ColumnName, null)
		{
			this.Column = dataColumn;
		}

		// Token: 0x170000F4 RID: 244
		// (get) Token: 0x060003F3 RID: 1011 RVA: 0x00013F5C File Offset: 0x0001215C
		public override AttributeCollection Attributes
		{
			get
			{
				if (typeof(IList).IsAssignableFrom(this.PropertyType))
				{
					Attribute[] array = new Attribute[base.Attributes.Count + 1];
					base.Attributes.CopyTo(array, 0);
					array[array.Length - 1] = new ListBindableAttribute(false);
					return new AttributeCollection(array);
				}
				return base.Attributes;
			}
		}

		// Token: 0x170000F5 RID: 245
		// (get) Token: 0x060003F4 RID: 1012 RVA: 0x00013FBA File Offset: 0x000121BA
		internal DataColumn Column
		{
			[CompilerGenerated]
			get
			{
				return this.<Column>k__BackingField;
			}
		}

		// Token: 0x170000F6 RID: 246
		// (get) Token: 0x060003F5 RID: 1013 RVA: 0x00013FC2 File Offset: 0x000121C2
		public override Type ComponentType
		{
			get
			{
				return typeof(DataRowView);
			}
		}

		// Token: 0x170000F7 RID: 247
		// (get) Token: 0x060003F6 RID: 1014 RVA: 0x00013FCE File Offset: 0x000121CE
		public override bool IsReadOnly
		{
			get
			{
				return this.Column.ReadOnly;
			}
		}

		// Token: 0x170000F8 RID: 248
		// (get) Token: 0x060003F7 RID: 1015 RVA: 0x00013FDB File Offset: 0x000121DB
		public override Type PropertyType
		{
			get
			{
				return this.Column.DataType;
			}
		}

		// Token: 0x060003F8 RID: 1016 RVA: 0x00013FE8 File Offset: 0x000121E8
		public override bool Equals(object other)
		{
			return other is DataColumnPropertyDescriptor && ((DataColumnPropertyDescriptor)other).Column == this.Column;
		}

		// Token: 0x060003F9 RID: 1017 RVA: 0x00014007 File Offset: 0x00012207
		public override int GetHashCode()
		{
			return this.Column.GetHashCode();
		}

		// Token: 0x060003FA RID: 1018 RVA: 0x00014014 File Offset: 0x00012214
		public override bool CanResetValue(object component)
		{
			DataRowView dataRowView = (DataRowView)component;
			if (!this.Column.IsSqlType)
			{
				return dataRowView.GetColumnValue(this.Column) != DBNull.Value;
			}
			return !DataStorage.IsObjectNull(dataRowView.GetColumnValue(this.Column));
		}

		// Token: 0x060003FB RID: 1019 RVA: 0x00014060 File Offset: 0x00012260
		public override object GetValue(object component)
		{
			return ((DataRowView)component).GetColumnValue(this.Column);
		}

		// Token: 0x060003FC RID: 1020 RVA: 0x00014073 File Offset: 0x00012273
		public override void ResetValue(object component)
		{
			((DataRowView)component).SetColumnValue(this.Column, DBNull.Value);
		}

		// Token: 0x060003FD RID: 1021 RVA: 0x0001408B File Offset: 0x0001228B
		public override void SetValue(object component, object value)
		{
			((DataRowView)component).SetColumnValue(this.Column, value);
			this.OnValueChanged(component, EventArgs.Empty);
		}

		// Token: 0x060003FE RID: 1022 RVA: 0x000061C5 File Offset: 0x000043C5
		public override bool ShouldSerializeValue(object component)
		{
			return false;
		}

		// Token: 0x170000F9 RID: 249
		// (get) Token: 0x060003FF RID: 1023 RVA: 0x000140AB File Offset: 0x000122AB
		public override bool IsBrowsable
		{
			get
			{
				return this.Column.ColumnMapping != MappingType.Hidden && base.IsBrowsable;
			}
		}

		// Token: 0x0400054E RID: 1358
		[CompilerGenerated]
		private readonly DataColumn <Column>k__BackingField;
	}
}
