﻿using System;
using System.Runtime.Serialization;

namespace System.Data
{
	/// <summary>Represents the exception that is thrown when errors are generated using ADO.NET components.</summary>
	// Token: 0x02000068 RID: 104
	[Serializable]
	public class DataException : SystemException
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Data.DataException" /> class with the specified serialization information and context.</summary>
		/// <param name="info">The data necessary to serialize or deserialize an object. </param>
		/// <param name="context">Description of the source and destination of the specified serialized stream. </param>
		// Token: 0x0600040C RID: 1036 RVA: 0x00014394 File Offset: 0x00012594
		protected DataException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Data.DataException" /> class. This is the default constructor.</summary>
		// Token: 0x0600040D RID: 1037 RVA: 0x000143A3 File Offset: 0x000125A3
		public DataException() : base("Data Exception.")
		{
			base.HResult = -2146232032;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Data.DataException" /> class with the specified string.</summary>
		/// <param name="s">The string to display when the exception is thrown. </param>
		// Token: 0x0600040E RID: 1038 RVA: 0x000143BB File Offset: 0x000125BB
		public DataException(string s) : base(s)
		{
			base.HResult = -2146232032;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Data.DataException" /> class with the specified string and inner exception.</summary>
		/// <param name="s">The string to display when the exception is thrown. </param>
		/// <param name="innerException">A reference to an inner exception. </param>
		// Token: 0x0600040F RID: 1039 RVA: 0x000143CF File Offset: 0x000125CF
		public DataException(string s, Exception innerException) : base(s, innerException)
		{
		}
	}
}
