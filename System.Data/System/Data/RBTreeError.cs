﻿using System;

namespace System.Data
{
	// Token: 0x020000DC RID: 220
	internal enum RBTreeError
	{
		// Token: 0x040007F6 RID: 2038
		InvalidPageSize = 1,
		// Token: 0x040007F7 RID: 2039
		PagePositionInSlotInUse = 3,
		// Token: 0x040007F8 RID: 2040
		NoFreeSlots,
		// Token: 0x040007F9 RID: 2041
		InvalidStateinInsert,
		// Token: 0x040007FA RID: 2042
		InvalidNextSizeInDelete = 7,
		// Token: 0x040007FB RID: 2043
		InvalidStateinDelete,
		// Token: 0x040007FC RID: 2044
		InvalidNodeSizeinDelete,
		// Token: 0x040007FD RID: 2045
		InvalidStateinEndDelete,
		// Token: 0x040007FE RID: 2046
		CannotRotateInvalidsuccessorNodeinDelete,
		// Token: 0x040007FF RID: 2047
		IndexOutOFRangeinGetNodeByIndex = 13,
		// Token: 0x04000800 RID: 2048
		RBDeleteFixup,
		// Token: 0x04000801 RID: 2049
		UnsupportedAccessMethod1,
		// Token: 0x04000802 RID: 2050
		UnsupportedAccessMethod2,
		// Token: 0x04000803 RID: 2051
		UnsupportedAccessMethodInNonNillRootSubtree,
		// Token: 0x04000804 RID: 2052
		AttachedNodeWithZerorbTreeNodeId,
		// Token: 0x04000805 RID: 2053
		CompareNodeInDataRowTree,
		// Token: 0x04000806 RID: 2054
		CompareSateliteTreeNodeInDataRowTree,
		// Token: 0x04000807 RID: 2055
		NestedSatelliteTreeEnumerator
	}
}
