﻿using System;
using Unity;

namespace System.Data
{
	/// <summary>The <see langword="DataRowBuilder" /> type supports the .NET Framework infrastructure and is not intended to be used directly from your code.</summary>
	// Token: 0x0200007B RID: 123
	public sealed class DataRowBuilder
	{
		// Token: 0x0600063E RID: 1598 RVA: 0x00019830 File Offset: 0x00017A30
		internal DataRowBuilder(DataTable table, int record)
		{
			this._table = table;
			this._record = record;
		}

		// Token: 0x0600063F RID: 1599 RVA: 0x00010458 File Offset: 0x0000E658
		internal DataRowBuilder()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x04000589 RID: 1417
		internal readonly DataTable _table;

		// Token: 0x0400058A RID: 1418
		internal int _record;
	}
}
