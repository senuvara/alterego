﻿using System;

namespace System.Data
{
	// Token: 0x020000EC RID: 236
	internal struct IndexField
	{
		// Token: 0x06000C72 RID: 3186 RVA: 0x0003A4AE File Offset: 0x000386AE
		internal IndexField(DataColumn column, bool isDescending)
		{
			this.Column = column;
			this.IsDescending = isDescending;
		}

		// Token: 0x06000C73 RID: 3187 RVA: 0x0003A4BE File Offset: 0x000386BE
		public static bool operator ==(IndexField if1, IndexField if2)
		{
			return if1.Column == if2.Column && if1.IsDescending == if2.IsDescending;
		}

		// Token: 0x06000C74 RID: 3188 RVA: 0x0003A4DE File Offset: 0x000386DE
		public static bool operator !=(IndexField if1, IndexField if2)
		{
			return !(if1 == if2);
		}

		// Token: 0x06000C75 RID: 3189 RVA: 0x0003A4EA File Offset: 0x000386EA
		public override bool Equals(object obj)
		{
			return obj is IndexField && this == (IndexField)obj;
		}

		// Token: 0x06000C76 RID: 3190 RVA: 0x0003A508 File Offset: 0x00038708
		public override int GetHashCode()
		{
			return this.Column.GetHashCode() ^ this.IsDescending.GetHashCode();
		}

		// Token: 0x04000853 RID: 2131
		public readonly DataColumn Column;

		// Token: 0x04000854 RID: 2132
		public readonly bool IsDescending;
	}
}
