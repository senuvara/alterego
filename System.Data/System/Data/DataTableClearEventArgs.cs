﻿using System;
using System.Runtime.CompilerServices;

namespace System.Data
{
	/// <summary>Provides data for the <see cref="M:System.Data.DataTable.Clear" /> method.</summary>
	// Token: 0x0200008E RID: 142
	public sealed class DataTableClearEventArgs : EventArgs
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Data.DataTableClearEventArgs" /> class.</summary>
		/// <param name="dataTable">The <see cref="T:System.Data.DataTable" /> whose rows are being cleared.</param>
		// Token: 0x06000888 RID: 2184 RVA: 0x00027B0B File Offset: 0x00025D0B
		public DataTableClearEventArgs(DataTable dataTable)
		{
			this.Table = dataTable;
		}

		/// <summary>Gets the table whose rows are being cleared.</summary>
		/// <returns>The <see cref="T:System.Data.DataTable" /> whose rows are being cleared.</returns>
		// Token: 0x17000180 RID: 384
		// (get) Token: 0x06000889 RID: 2185 RVA: 0x00027B1A File Offset: 0x00025D1A
		public DataTable Table
		{
			[CompilerGenerated]
			get
			{
				return this.<Table>k__BackingField;
			}
		}

		/// <summary>Gets the table name whose rows are being cleared.</summary>
		/// <returns>A <see cref="T:System.String" /> indicating the table name.</returns>
		// Token: 0x17000181 RID: 385
		// (get) Token: 0x0600088A RID: 2186 RVA: 0x00027B22 File Offset: 0x00025D22
		public string TableName
		{
			get
			{
				return this.Table.TableName;
			}
		}

		/// <summary>Gets the namespace of the table whose rows are being cleared.</summary>
		/// <returns>A <see cref="T:System.String" /> indicating the namespace name.</returns>
		// Token: 0x17000182 RID: 386
		// (get) Token: 0x0600088B RID: 2187 RVA: 0x00027B2F File Offset: 0x00025D2F
		public string TableNamespace
		{
			get
			{
				return this.Table.Namespace;
			}
		}

		// Token: 0x04000627 RID: 1575
		[CompilerGenerated]
		private readonly DataTable <Table>k__BackingField;
	}
}
