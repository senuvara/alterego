﻿using System;

namespace System.Data
{
	/// <summary>Represents the method that will handle the <see cref="E:System.Data.Common.DbConnection.StateChange" /> event.</summary>
	/// <param name="sender">The source of the event. </param>
	/// <param name="e">The <see cref="T:System.Data.StateChangeEventArgs" /> that contains the event data. </param>
	// Token: 0x020000F7 RID: 247
	// (Invoke) Token: 0x06000CEE RID: 3310
	public delegate void StateChangeEventHandler(object sender, StateChangeEventArgs e);
}
