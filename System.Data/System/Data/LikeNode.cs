﻿using System;
using System.Data.Common;
using System.Data.SqlTypes;

namespace System.Data
{
	// Token: 0x020000A9 RID: 169
	internal sealed class LikeNode : BinaryNode
	{
		// Token: 0x06000A37 RID: 2615 RVA: 0x0002F9C7 File Offset: 0x0002DBC7
		internal LikeNode(DataTable table, int op, ExpressionNode left, ExpressionNode right) : base(table, op, left, right)
		{
		}

		// Token: 0x06000A38 RID: 2616 RVA: 0x0002F9D4 File Offset: 0x0002DBD4
		internal override object Eval(DataRow row, DataRowVersion version)
		{
			object obj = this._left.Eval(row, version);
			if (obj == DBNull.Value || (this._left.IsSqlColumn && DataStorage.IsObjectSqlNull(obj)))
			{
				return DBNull.Value;
			}
			string text;
			if (this._pattern == null)
			{
				object obj2 = this._right.Eval(row, version);
				if (!(obj2 is string) && !(obj2 is SqlString))
				{
					base.SetTypeMismatchError(this._op, obj.GetType(), obj2.GetType());
				}
				if (obj2 == DBNull.Value || DataStorage.IsObjectSqlNull(obj2))
				{
					return DBNull.Value;
				}
				string pat = (string)SqlConvert.ChangeType2(obj2, StorageType.String, typeof(string), base.FormatProvider);
				text = this.AnalyzePattern(pat);
				if (this._right.IsConstant())
				{
					this._pattern = text;
				}
			}
			else
			{
				text = this._pattern;
			}
			if (!(obj is string) && !(obj is SqlString))
			{
				base.SetTypeMismatchError(this._op, obj.GetType(), typeof(string));
			}
			char[] trimChars = new char[]
			{
				' ',
				'\u3000'
			};
			string text2;
			if (obj is SqlString)
			{
				text2 = ((SqlString)obj).Value;
			}
			else
			{
				text2 = (string)obj;
			}
			string s = text2.TrimEnd(trimChars);
			switch (this._kind)
			{
			case 1:
				return base.table.IndexOf(s, text) == 0;
			case 2:
			{
				string s2 = text.TrimEnd(trimChars);
				return base.table.IsSuffix(s, s2);
			}
			case 3:
				return 0 <= base.table.IndexOf(s, text);
			case 4:
				return base.table.Compare(s, text) == 0;
			case 5:
				return true;
			default:
				return DBNull.Value;
			}
		}

		// Token: 0x06000A39 RID: 2617 RVA: 0x0002FBB4 File Offset: 0x0002DDB4
		internal string AnalyzePattern(string pat)
		{
			int length = pat.Length;
			char[] array = new char[length + 1];
			pat.CopyTo(0, array, 0, length);
			array[length] = '\0';
			char[] array2 = new char[length + 1];
			int num = 0;
			int num2 = 0;
			int i = 0;
			while (i < length)
			{
				if (array[i] != '*')
				{
					if (array[i] != '%')
					{
						if (array[i] != '[')
						{
							array2[num++] = array[i];
							i++;
							continue;
						}
						i++;
						if (i >= length)
						{
							throw ExprException.InvalidPattern(pat);
						}
						array2[num++] = array[i++];
						if (i >= length)
						{
							throw ExprException.InvalidPattern(pat);
						}
						if (array[i] != ']')
						{
							throw ExprException.InvalidPattern(pat);
						}
						i++;
						continue;
					}
				}
				while ((array[i] == '*' || array[i] == '%') && i < length)
				{
					i++;
				}
				if ((i < length && num > 0) || num2 >= 2)
				{
					throw ExprException.InvalidPattern(pat);
				}
				num2++;
			}
			string result = new string(array2, 0, num);
			if (num2 == 0)
			{
				this._kind = 4;
				return result;
			}
			if (num <= 0)
			{
				this._kind = 5;
				return result;
			}
			if (array[0] != '*' && array[0] != '%')
			{
				this._kind = 1;
				return result;
			}
			if (array[length - 1] == '*' || array[length - 1] == '%')
			{
				this._kind = 3;
				return result;
			}
			this._kind = 2;
			return result;
		}

		// Token: 0x040006DE RID: 1758
		internal const int match_left = 1;

		// Token: 0x040006DF RID: 1759
		internal const int match_right = 2;

		// Token: 0x040006E0 RID: 1760
		internal const int match_middle = 3;

		// Token: 0x040006E1 RID: 1761
		internal const int match_exact = 4;

		// Token: 0x040006E2 RID: 1762
		internal const int match_all = 5;

		// Token: 0x040006E3 RID: 1763
		private int _kind;

		// Token: 0x040006E4 RID: 1764
		private string _pattern;
	}
}
