﻿using System;
using System.Runtime.InteropServices;
using System.Text;

namespace System.Data
{
	// Token: 0x02000118 RID: 280
	internal static class LocalDBAPI
	{
		// Token: 0x06000E53 RID: 3667 RVA: 0x0004B969 File Offset: 0x00049B69
		internal static string GetLocalDBMessage(int hrCode)
		{
			throw new PlatformNotSupportedException("LocalDB is not supported on this platform.");
		}

		// Token: 0x06000E54 RID: 3668 RVA: 0x0004B978 File Offset: 0x00049B78
		internal static string GetLocalDbInstanceNameFromServerName(string serverName)
		{
			if (serverName == null)
			{
				return null;
			}
			serverName = serverName.TrimStart(Array.Empty<char>());
			if (!serverName.StartsWith("(localdb)\\", StringComparison.OrdinalIgnoreCase))
			{
				return null;
			}
			string text = serverName.Substring("(localdb)\\".Length).Trim();
			if (text.Length == 0)
			{
				return null;
			}
			return text;
		}

		// Token: 0x040009EF RID: 2543
		private const string const_localDbPrefix = "(localdb)\\";

		// Token: 0x02000119 RID: 281
		// (Invoke) Token: 0x06000E56 RID: 3670
		[UnmanagedFunctionPointer(CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
		private delegate int LocalDBFormatMessageDelegate(int hrLocalDB, uint dwFlags, uint dwLanguageId, StringBuilder buffer, ref uint buflen);
	}
}
