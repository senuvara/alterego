﻿using System;
using System.ComponentModel;
using System.Data.Common;
using Unity;

namespace System.Data.Odbc
{
	/// <summary>Represents an SQL statement or stored procedure to execute against a data source. This class cannot be inherited.</summary>
	// Token: 0x02000371 RID: 881
	[DefaultEvent("RecordsAffected")]
	[Designer("Microsoft.VSDesigner.Data.VS.OdbcCommandDesigner, Microsoft.VSDesigner, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")]
	[ToolboxItem(true)]
	public sealed class OdbcCommand : DbCommand, ICloneable
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Data.Odbc.OdbcCommand" /> class.</summary>
		// Token: 0x06002A6D RID: 10861 RVA: 0x00010458 File Offset: 0x0000E658
		public OdbcCommand()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Data.Odbc.OdbcCommand" /> class with the text of the query.</summary>
		/// <param name="cmdText">The text of the query. </param>
		// Token: 0x06002A6E RID: 10862 RVA: 0x00010458 File Offset: 0x0000E658
		public OdbcCommand(string cmdText)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Data.Odbc.OdbcCommand" /> class with the text of the query and an <see cref="T:System.Data.Odbc.OdbcConnection" /> object.</summary>
		/// <param name="cmdText">The text of the query. </param>
		/// <param name="connection">An <see cref="T:System.Data.Odbc.OdbcConnection" /> object that represents the connection to a data source. </param>
		// Token: 0x06002A6F RID: 10863 RVA: 0x00010458 File Offset: 0x0000E658
		public OdbcCommand(string cmdText, OdbcConnection connection)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Data.Odbc.OdbcCommand" /> class with the text of the query, an <see cref="T:System.Data.Odbc.OdbcConnection" /> object, and the <see cref="P:System.Data.Odbc.OdbcCommand.Transaction" />.</summary>
		/// <param name="cmdText">The text of the query. </param>
		/// <param name="connection">An <see cref="T:System.Data.Odbc.OdbcConnection" /> object that represents the connection to a data source. </param>
		/// <param name="transaction">The transaction in which the <see cref="T:System.Data.Odbc.OdbcCommand" /> executes. </param>
		// Token: 0x06002A70 RID: 10864 RVA: 0x00010458 File Offset: 0x0000E658
		public OdbcCommand(string cmdText, OdbcConnection connection, OdbcTransaction transaction)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the SQL statement or stored procedure to execute against the data source.</summary>
		/// <returns>The SQL statement or stored procedure to execute. The default value is an empty string ("").</returns>
		// Token: 0x170006FD RID: 1789
		// (get) Token: 0x06002A71 RID: 10865 RVA: 0x00051759 File Offset: 0x0004F959
		// (set) Token: 0x06002A72 RID: 10866 RVA: 0x00010458 File Offset: 0x0000E658
		public override string CommandText
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the wait time before terminating an attempt to execute a command and generating an error.</summary>
		/// <returns>The time in seconds to wait for the command to execute. The default is 30 seconds.</returns>
		// Token: 0x170006FE RID: 1790
		// (get) Token: 0x06002A73 RID: 10867 RVA: 0x000B7230 File Offset: 0x000B5430
		// (set) Token: 0x06002A74 RID: 10868 RVA: 0x00010458 File Offset: 0x0000E658
		public override int CommandTimeout
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets a value that indicates how the <see cref="P:System.Data.Odbc.OdbcCommand.CommandText" /> property is interpreted.</summary>
		/// <returns>One of the <see cref="T:System.Data.CommandType" /> values. The default is <see langword="Text" />.</returns>
		/// <exception cref="T:System.ArgumentException">The value was not a valid <see cref="T:System.Data.CommandType" />. </exception>
		// Token: 0x170006FF RID: 1791
		// (get) Token: 0x06002A75 RID: 10869 RVA: 0x000B724C File Offset: 0x000B544C
		// (set) Token: 0x06002A76 RID: 10870 RVA: 0x00010458 File Offset: 0x0000E658
		public override CommandType CommandType
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return (CommandType)0;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the <see cref="T:System.Data.Odbc.OdbcConnection" /> used by this instance of the <see cref="T:System.Data.Odbc.OdbcCommand" />.</summary>
		/// <returns>The connection to a data source. The default is a null value.</returns>
		/// <exception cref="T:System.InvalidOperationException">The <see cref="P:System.Data.Odbc.OdbcCommand.Connection" /> property was changed while a transaction was in progress. </exception>
		// Token: 0x17000700 RID: 1792
		// (set) Token: 0x06002A77 RID: 10871 RVA: 0x00010458 File Offset: 0x0000E658
		public new OdbcConnection Connection
		{
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		// Token: 0x17000701 RID: 1793
		// (get) Token: 0x06002A78 RID: 10872 RVA: 0x00051759 File Offset: 0x0004F959
		// (set) Token: 0x06002A79 RID: 10873 RVA: 0x00010458 File Offset: 0x0000E658
		protected override DbConnection DbConnection
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		// Token: 0x17000702 RID: 1794
		// (get) Token: 0x06002A7A RID: 10874 RVA: 0x00051759 File Offset: 0x0004F959
		protected override DbParameterCollection DbParameterCollection
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		// Token: 0x17000703 RID: 1795
		// (get) Token: 0x06002A7B RID: 10875 RVA: 0x00051759 File Offset: 0x0004F959
		// (set) Token: 0x06002A7C RID: 10876 RVA: 0x00010458 File Offset: 0x0000E658
		protected override DbTransaction DbTransaction
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets a value that indicates whether the command object should be visible in a customized interface control.</summary>
		/// <returns>
		///     true, if the command object should be visible in a control; otherwise false. The default is true.</returns>
		// Token: 0x17000704 RID: 1796
		// (get) Token: 0x06002A7D RID: 10877 RVA: 0x000B7268 File Offset: 0x000B5468
		// (set) Token: 0x06002A7E RID: 10878 RVA: 0x00010458 File Offset: 0x0000E658
		public override bool DesignTimeVisible
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the <see cref="T:System.Data.Odbc.OdbcTransaction" /> within which the <see cref="T:System.Data.Odbc.OdbcCommand" /> executes.</summary>
		/// <returns>An <see cref="T:System.Data.Odbc.OdbcTransaction" />. The default is a null value.</returns>
		// Token: 0x17000705 RID: 1797
		// (set) Token: 0x06002A7F RID: 10879 RVA: 0x00010458 File Offset: 0x0000E658
		public new OdbcTransaction Transaction
		{
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets a value that specifies how the Update method should apply command results to the DataRow.</summary>
		/// <returns>One of the <see cref="T:System.Data.UpdateRowSource" /> values.</returns>
		// Token: 0x17000706 RID: 1798
		// (get) Token: 0x06002A80 RID: 10880 RVA: 0x000B7284 File Offset: 0x000B5484
		// (set) Token: 0x06002A81 RID: 10881 RVA: 0x00010458 File Offset: 0x0000E658
		public override UpdateRowSource UpdatedRowSource
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return UpdateRowSource.None;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Tries to cancel the execution of an <see cref="T:System.Data.Odbc.OdbcCommand" />.</summary>
		// Token: 0x06002A82 RID: 10882 RVA: 0x00010458 File Offset: 0x0000E658
		public override void Cancel()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x06002A83 RID: 10883 RVA: 0x00051759 File Offset: 0x0004F959
		protected override DbParameter CreateDbParameter()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		// Token: 0x06002A84 RID: 10884 RVA: 0x00051759 File Offset: 0x0004F959
		protected override DbDataReader ExecuteDbDataReader(CommandBehavior behavior)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Executes an SQL statement against the <see cref="P:System.Data.Odbc.OdbcCommand.Connection" /> and returns the number of rows affected.</summary>
		/// <returns>For UPDATE, INSERT, and DELETE statements, the return value is the number of rows affected by the command. For all other types of statements, the return value is -1.</returns>
		/// <exception cref="T:System.InvalidOperationException">The connection does not exist.-or- The connection is not open. </exception>
		// Token: 0x06002A85 RID: 10885 RVA: 0x000B72A0 File Offset: 0x000B54A0
		public override int ExecuteNonQuery()
		{
			ThrowStub.ThrowNotSupportedException();
			return 0;
		}

		/// <summary>Executes the query, and returns the first column of the first row in the result set returned by the query. Additional columns or rows are ignored.</summary>
		/// <returns>The first column of the first row in the result set, or a null reference if the result set is empty.</returns>
		// Token: 0x06002A86 RID: 10886 RVA: 0x00051759 File Offset: 0x0004F959
		public override object ExecuteScalar()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Creates a prepared or compiled version of the command at the data source.</summary>
		/// <exception cref="T:System.InvalidOperationException">The <see cref="P:System.Data.Odbc.OdbcCommand.Connection" /> is not set.-or- The <see cref="P:System.Data.Odbc.OdbcCommand.Connection" /> is not <see cref="M:System.Data.Odbc.OdbcConnection.Open" />. </exception>
		// Token: 0x06002A87 RID: 10887 RVA: 0x00010458 File Offset: 0x0000E658
		public override void Prepare()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Resets the <see cref="P:System.Data.Odbc.OdbcCommand.CommandTimeout" /> property to the default value.</summary>
		// Token: 0x06002A88 RID: 10888 RVA: 0x00010458 File Offset: 0x0000E658
		public void ResetCommandTimeout()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>For a description of this member, see <see cref="M:System.ICloneable.Clone" />.</summary>
		/// <returns>A new <see cref="T;System.Object" /> that is a copy of this instance.</returns>
		// Token: 0x06002A89 RID: 10889 RVA: 0x00051759 File Offset: 0x0004F959
		object ICloneable.Clone()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}
	}
}
