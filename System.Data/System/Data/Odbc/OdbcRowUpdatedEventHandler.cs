﻿using System;
using Unity;

namespace System.Data.Odbc
{
	/// <summary>Represents the method that will handle the <see cref="E:System.Data.Odbc.OdbcDataAdapter.RowUpdated" /> event of an <see cref="T:System.Data.Odbc.OdbcDataAdapter" />.</summary>
	/// <param name="sender">The source of the event. </param>
	/// <param name="e">The <see cref="T:System.Data.Odbc.OdbcRowUpdatedEventArgs" /> that contains the event data. </param>
	// Token: 0x0200037A RID: 890
	public sealed class OdbcRowUpdatedEventHandler : MulticastDelegate
	{
		// Token: 0x06002ACE RID: 10958 RVA: 0x00010458 File Offset: 0x0000E658
		public OdbcRowUpdatedEventHandler(object @object, IntPtr method)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x06002ACF RID: 10959 RVA: 0x00010458 File Offset: 0x0000E658
		public void Invoke(object sender, OdbcRowUpdatedEventArgs e)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x06002AD0 RID: 10960 RVA: 0x00051759 File Offset: 0x0004F959
		public IAsyncResult BeginInvoke(object sender, OdbcRowUpdatedEventArgs e, AsyncCallback callback, object @object)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		// Token: 0x06002AD1 RID: 10961 RVA: 0x00010458 File Offset: 0x0000E658
		public void EndInvoke(IAsyncResult result)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
