﻿using System;
using System.Data.Common;
using Unity;

namespace System.Data.Odbc
{
	/// <summary>Represents an SQL transaction to be made at a data source. This class cannot be inherited.</summary>
	// Token: 0x02000377 RID: 887
	public sealed class OdbcTransaction : DbTransaction
	{
		// Token: 0x06002AB2 RID: 10930 RVA: 0x00010458 File Offset: 0x0000E658
		internal OdbcTransaction()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x17000716 RID: 1814
		// (get) Token: 0x06002AB3 RID: 10931 RVA: 0x00051759 File Offset: 0x0004F959
		protected override DbConnection DbConnection
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Specifies the <see cref="T:System.Data.IsolationLevel" /> for this transaction.</summary>
		/// <returns>The <see cref="T:System.Data.IsolationLevel" /> for this transaction. The default depends on the underlying ODBC driver.</returns>
		// Token: 0x17000717 RID: 1815
		// (get) Token: 0x06002AB4 RID: 10932 RVA: 0x000B732C File Offset: 0x000B552C
		public override IsolationLevel IsolationLevel
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return (IsolationLevel)0;
			}
		}

		/// <summary>Commits the database transaction.</summary>
		/// <exception cref="T:System.Exception">An error occurred while trying to commit the transaction. </exception>
		/// <exception cref="T:System.InvalidOperationException">The transaction has already been committed or rolled back.-or- The connection is broken. </exception>
		// Token: 0x06002AB5 RID: 10933 RVA: 0x00010458 File Offset: 0x0000E658
		public override void Commit()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Rolls back a transaction from a pending state.</summary>
		/// <exception cref="T:System.Exception">An error occurred while trying to commit the transaction. </exception>
		/// <exception cref="T:System.InvalidOperationException">The transaction has already been committed or rolled back.-or- The connection is broken.</exception>
		// Token: 0x06002AB6 RID: 10934 RVA: 0x00010458 File Offset: 0x0000E658
		public override void Rollback()
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
