﻿using System;
using System.Data.Common;
using Unity;

namespace System.Data.Odbc
{
	/// <summary>Provides data for the <see cref="E:System.Data.Odbc.OdbcDataAdapter.RowUpdating" /> event.</summary>
	// Token: 0x0200037D RID: 893
	public sealed class OdbcRowUpdatingEventArgs : RowUpdatingEventArgs
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Data.Odbc.OdbcRowUpdatingEventArgs" /> class.</summary>
		/// <param name="row">The <see cref="T:System.Data.DataRow" /> to update. </param>
		/// <param name="command">The <see cref="T:System.Data.Odbc.OdbcCommand" /> to execute during the update operation. </param>
		/// <param name="statementType">One of the <see cref="T:System.Data.StatementType" /> values that specifies the type of query executed. </param>
		/// <param name="tableMapping">The <see cref="T:System.Data.Common.DataTableMapping" /> sent through <see cref="M:System.Data.Common.DbDataAdapter.Update(System.Data.DataSet)" />. </param>
		// Token: 0x06002AD7 RID: 10967 RVA: 0x00010458 File Offset: 0x0000E658
		public OdbcRowUpdatingEventArgs(DataRow row, IDbCommand command, StatementType statementType, DataTableMapping tableMapping)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the <see cref="T:System.Data.Odbc.OdbcCommand" /> to execute when <see cref="M:System.Data.Common.DbDataAdapter.Update(System.Data.DataSet)" /> is called.</summary>
		/// <returns>The <see cref="T:System.Data.Odbc.OdbcCommand" /> to execute when <see cref="M:System.Data.Common.DbDataAdapter.Update(System.Data.DataSet)" /> is called.</returns>
		// Token: 0x1700071D RID: 1821
		// (set) Token: 0x06002AD8 RID: 10968 RVA: 0x00010458 File Offset: 0x0000E658
		public new OdbcCommand Command
		{
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}
	}
}
