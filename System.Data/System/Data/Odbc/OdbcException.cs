﻿using System;
using System.Data.Common;
using Unity;

namespace System.Data.Odbc
{
	/// <summary>The exception that is generated when a warning or error is returned by an ODBC data source. This class cannot be inherited.</summary>
	// Token: 0x02000380 RID: 896
	[Serializable]
	public sealed class OdbcException : DbException
	{
		// Token: 0x06002B01 RID: 11009 RVA: 0x00010458 File Offset: 0x0000E658
		internal OdbcException()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets a collection of one or more <see cref="T:System.Data.Odbc.OdbcError" /> objects that give detailed information about exceptions generated by the .NET Framework Data Provider for ODBC.</summary>
		/// <returns>The collected instances of the <see cref="T:System.Data.Odbc.OdbcError" /> class.</returns>
		// Token: 0x17000726 RID: 1830
		// (get) Token: 0x06002B02 RID: 11010 RVA: 0x00051759 File Offset: 0x0004F959
		public OdbcErrorCollection Errors
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}
	}
}
