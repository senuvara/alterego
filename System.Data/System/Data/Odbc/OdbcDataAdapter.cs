﻿using System;
using System.ComponentModel;
using System.Data.Common;
using Unity;

namespace System.Data.Odbc
{
	/// <summary>Represents a set of data commands and a connection to a data source that are used to fill the <see cref="T:System.Data.DataSet" /> and update the data source. This class cannot be inherited.</summary>
	// Token: 0x02000379 RID: 889
	[DefaultEvent("RowUpdated")]
	[Designer("Microsoft.VSDesigner.Data.VS.OdbcDataAdapterDesigner, Microsoft.VSDesigner, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")]
	[ToolboxItem("Microsoft.VSDesigner.Data.VS.OdbcDataAdapterToolboxItem, Microsoft.VSDesigner, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")]
	public sealed class OdbcDataAdapter : DbDataAdapter
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Data.Odbc.OdbcDataAdapter" /> class.</summary>
		// Token: 0x06002AC2 RID: 10946 RVA: 0x00010458 File Offset: 0x0000E658
		public OdbcDataAdapter()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Data.Odbc.OdbcDataAdapter" /> class with the specified SQL SELECT statement.</summary>
		/// <param name="selectCommand">An <see cref="T:System.Data.Odbc.OdbcCommand" /> that is an SQL SELECT statement or stored procedure, and is set as the <see cref="P:System.Data.Odbc.OdbcDataAdapter.SelectCommand" /> property of the <see cref="T:System.Data.Odbc.OdbcDataAdapter" />. </param>
		// Token: 0x06002AC3 RID: 10947 RVA: 0x00010458 File Offset: 0x0000E658
		public OdbcDataAdapter(OdbcCommand selectCommand)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Data.Odbc.OdbcDataAdapter" /> class with an SQL SELECT statement and an <see cref="T:System.Data.Odbc.OdbcConnection" />.</summary>
		/// <param name="selectCommandText">A string that is a SQL SELECT statement or stored procedure to be used by the <see cref="P:System.Data.Odbc.OdbcDataAdapter.SelectCommand" /> property of the <see cref="T:System.Data.Odbc.OdbcDataAdapter" />. </param>
		/// <param name="selectConnection">An <see cref="T:System.Data.Odbc.OdbcConnection" /> that represents the connection. </param>
		// Token: 0x06002AC4 RID: 10948 RVA: 0x00010458 File Offset: 0x0000E658
		public OdbcDataAdapter(string selectCommandText, OdbcConnection selectConnection)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Data.Odbc.OdbcDataAdapter" /> class with an SQL SELECT statement and a connection string.</summary>
		/// <param name="selectCommandText">A string that is a SQL SELECT statement or stored procedure to be used by the <see cref="P:System.Data.Odbc.OdbcDataAdapter.SelectCommand" /> property of the <see cref="T:System.Data.Odbc.OdbcDataAdapter" />. </param>
		/// <param name="selectConnectionString">The connection string. </param>
		// Token: 0x06002AC5 RID: 10949 RVA: 0x00010458 File Offset: 0x0000E658
		public OdbcDataAdapter(string selectCommandText, string selectConnectionString)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets an SQL statement or stored procedure used to delete records in the data source.</summary>
		/// <returns>An <see cref="T:System.Data.Odbc.OdbcCommand" /> used during an update operation to delete records in the data source that correspond to deleted rows in the <see cref="T:System.Data.DataSet" />.</returns>
		// Token: 0x17000719 RID: 1817
		// (set) Token: 0x06002AC6 RID: 10950 RVA: 0x00010458 File Offset: 0x0000E658
		public new OdbcCommand DeleteCommand
		{
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets an SQL statement or stored procedure used to insert new records into the data source.</summary>
		/// <returns>An <see cref="T:System.Data.Odbc.OdbcCommand" /> used during an update operation to insert records in the data source that correspond to new rows in the <see cref="T:System.Data.DataSet" />.</returns>
		// Token: 0x1700071A RID: 1818
		// (set) Token: 0x06002AC7 RID: 10951 RVA: 0x00010458 File Offset: 0x0000E658
		public new OdbcCommand InsertCommand
		{
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets an SQL statement or stored procedure used to select records in the data source.</summary>
		/// <returns>An <see cref="T:System.Data.Odbc.OdbcCommand" /> that is used during a fill operation to select records from data source for placement in the <see cref="T:System.Data.DataSet" />.</returns>
		// Token: 0x1700071B RID: 1819
		// (set) Token: 0x06002AC8 RID: 10952 RVA: 0x00010458 File Offset: 0x0000E658
		public new OdbcCommand SelectCommand
		{
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets an SQL statement or stored procedure used to update records in the data source.</summary>
		/// <returns>An <see cref="T:System.Data.Odbc.OdbcCommand" /> used during an update operation to update records in the data source that correspond to modified rows in the <see cref="T:System.Data.DataSet" />.</returns>
		// Token: 0x1700071C RID: 1820
		// (set) Token: 0x06002AC9 RID: 10953 RVA: 0x00010458 File Offset: 0x0000E658
		public new OdbcCommand UpdateCommand
		{
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Occurs during an update operation after a command is executed against the data source.</summary>
		// Token: 0x1400002C RID: 44
		// (add) Token: 0x06002ACA RID: 10954 RVA: 0x00010458 File Offset: 0x0000E658
		// (remove) Token: 0x06002ACB RID: 10955 RVA: 0x00010458 File Offset: 0x0000E658
		public event OdbcRowUpdatedEventHandler RowUpdated
		{
			add
			{
				ThrowStub.ThrowNotSupportedException();
			}
			remove
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Occurs during <see cref="M:System.Data.Common.DbDataAdapter.Update(System.Data.DataSet)" /> before a command is executed against the data source.</summary>
		// Token: 0x1400002D RID: 45
		// (add) Token: 0x06002ACC RID: 10956 RVA: 0x00010458 File Offset: 0x0000E658
		// (remove) Token: 0x06002ACD RID: 10957 RVA: 0x00010458 File Offset: 0x0000E658
		public event OdbcRowUpdatingEventHandler RowUpdating
		{
			add
			{
				ThrowStub.ThrowNotSupportedException();
			}
			remove
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}
	}
}
