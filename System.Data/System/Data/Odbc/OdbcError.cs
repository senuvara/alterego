﻿using System;
using Unity;

namespace System.Data.Odbc
{
	/// <summary>Collects information relevant to a warning or error returned by the data source.</summary>
	// Token: 0x02000376 RID: 886
	[Serializable]
	public sealed class OdbcError
	{
		// Token: 0x06002AAD RID: 10925 RVA: 0x00010458 File Offset: 0x0000E658
		internal OdbcError()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets a short description of the error.</summary>
		/// <returns>A description of the error.</returns>
		// Token: 0x17000712 RID: 1810
		// (get) Token: 0x06002AAE RID: 10926 RVA: 0x00051759 File Offset: 0x0004F959
		public string Message
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the data source-specific error information.</summary>
		/// <returns>The data source-specific error information.</returns>
		// Token: 0x17000713 RID: 1811
		// (get) Token: 0x06002AAF RID: 10927 RVA: 0x000B7310 File Offset: 0x000B5510
		public int NativeError
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
		}

		/// <summary>Gets the name of the driver that generated the error.</summary>
		/// <returns>The name of the driver that generated the error.</returns>
		// Token: 0x17000714 RID: 1812
		// (get) Token: 0x06002AB0 RID: 10928 RVA: 0x00051759 File Offset: 0x0004F959
		public string Source
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the five-character error code that follows the ANSI SQL standard for the database.</summary>
		/// <returns>The five-character error code, which identifies the source of the error if the error can be issued from more than one place.</returns>
		// Token: 0x17000715 RID: 1813
		// (get) Token: 0x06002AB1 RID: 10929 RVA: 0x00051759 File Offset: 0x0004F959
		public string SQLState
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}
	}
}
