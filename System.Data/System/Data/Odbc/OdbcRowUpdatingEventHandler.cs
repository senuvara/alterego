﻿using System;
using Unity;

namespace System.Data.Odbc
{
	/// <summary>Represents the method that will handle the <see cref="E:System.Data.Odbc.OdbcDataAdapter.RowUpdating" /> event of an <see cref="T:System.Data.Odbc.OdbcDataAdapter" />.</summary>
	/// <param name="sender">The source of the event. </param>
	/// <param name="e">The <see cref="T:System.Data.Odbc.OdbcRowUpdatingEventArgs" /> that contains the event data. </param>
	// Token: 0x0200037C RID: 892
	public sealed class OdbcRowUpdatingEventHandler : MulticastDelegate
	{
		// Token: 0x06002AD3 RID: 10963 RVA: 0x00010458 File Offset: 0x0000E658
		public OdbcRowUpdatingEventHandler(object @object, IntPtr method)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x06002AD4 RID: 10964 RVA: 0x00010458 File Offset: 0x0000E658
		public void Invoke(object sender, OdbcRowUpdatingEventArgs e)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x06002AD5 RID: 10965 RVA: 0x00051759 File Offset: 0x0004F959
		public IAsyncResult BeginInvoke(object sender, OdbcRowUpdatingEventArgs e, AsyncCallback callback, object @object)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		// Token: 0x06002AD6 RID: 10966 RVA: 0x00010458 File Offset: 0x0000E658
		public void EndInvoke(IAsyncResult result)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
