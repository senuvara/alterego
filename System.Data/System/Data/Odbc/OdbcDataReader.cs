﻿using System;
using System.Collections;
using System.Data.Common;
using Unity;

namespace System.Data.Odbc
{
	/// <summary>Provides a way of reading a forward-only stream of data rows from a data source. This class cannot be inherited. </summary>
	// Token: 0x0200037F RID: 895
	public sealed class OdbcDataReader : DbDataReader
	{
		// Token: 0x06002ADF RID: 10975 RVA: 0x00010458 File Offset: 0x0000E658
		internal OdbcDataReader()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets a value that indicates the depth of nesting for the current row.</summary>
		/// <returns>The depth of nesting for the current row.</returns>
		// Token: 0x17000720 RID: 1824
		// (get) Token: 0x06002AE0 RID: 10976 RVA: 0x000B7348 File Offset: 0x000B5548
		public override int Depth
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
		}

		/// <summary>Gets the number of columns in the current row.</summary>
		/// <returns>When not positioned in a valid record set, 0; otherwise the number of columns in the current record. The default is -1.</returns>
		/// <exception cref="T:System.NotSupportedException">There is no current connection to a data source. </exception>
		// Token: 0x17000721 RID: 1825
		// (get) Token: 0x06002AE1 RID: 10977 RVA: 0x000B7364 File Offset: 0x000B5564
		public override int FieldCount
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
		}

		/// <summary>Gets a value that indicates whether the <see cref="T:System.Data.Odbc.OdbcDataReader" /> contains one or more rows.</summary>
		/// <returns>
		///     <see langword="true" /> if the <see cref="T:System.Data.Odbc.OdbcDataReader" /> contains one or more rows; otherwise <see langword="false" />.</returns>
		// Token: 0x17000722 RID: 1826
		// (get) Token: 0x06002AE2 RID: 10978 RVA: 0x000B7380 File Offset: 0x000B5580
		public override bool HasRows
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
		}

		/// <summary>Indicates whether the <see cref="T:System.Data.Odbc.OdbcDataReader" /> is closed.</summary>
		/// <returns>
		///     <see langword="true" /> if the <see cref="T:System.Data.Odbc.OdbcDataReader" /> is closed; otherwise <see langword="false" />.</returns>
		// Token: 0x17000723 RID: 1827
		// (get) Token: 0x06002AE3 RID: 10979 RVA: 0x000B739C File Offset: 0x000B559C
		public override bool IsClosed
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
		}

		// Token: 0x06002AE4 RID: 10980 RVA: 0x00051759 File Offset: 0x0004F959
		public override object get_Item(int i)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Gets the value of the specified column in its native format given the column name.</summary>
		/// <param name="value">The column name. </param>
		/// <returns>The value of the specified column in its native format.</returns>
		/// <exception cref="T:System.IndexOutOfRangeException">No column with the specified name was found. </exception>
		// Token: 0x17000724 RID: 1828
		public override object this[string value]
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the number of rows changed, inserted, or deleted by execution of the SQL statement.</summary>
		/// <returns>The number of rows changed, inserted, or deleted. -1 for SELECT statements; 0 if no rows were affected, or the statement failed.</returns>
		// Token: 0x17000725 RID: 1829
		// (get) Token: 0x06002AE6 RID: 10982 RVA: 0x000B73B8 File Offset: 0x000B55B8
		public override int RecordsAffected
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
		}

		/// <summary>Gets the value of the specified column as a Boolean.</summary>
		/// <param name="i">The zero-based column ordinal. </param>
		/// <returns>A Boolean that is the value of the column.</returns>
		/// <exception cref="T:System.InvalidCastException">The specified cast is not valid. </exception>
		// Token: 0x06002AE7 RID: 10983 RVA: 0x000B73D4 File Offset: 0x000B55D4
		public override bool GetBoolean(int i)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Gets the value of the specified column as a byte.</summary>
		/// <param name="i">The zero-based column ordinal. </param>
		/// <returns>The value of the specified column as a byte.</returns>
		/// <exception cref="T:System.InvalidCastException">The specified cast is not valid. </exception>
		// Token: 0x06002AE8 RID: 10984 RVA: 0x000B73F0 File Offset: 0x000B55F0
		public override byte GetByte(int i)
		{
			ThrowStub.ThrowNotSupportedException();
			return 0;
		}

		/// <summary>Reads a stream of bytes from the specified column offset into the buffer as an array, starting at the particular buffer offset.</summary>
		/// <param name="i">The zero-based column ordinal. </param>
		/// <param name="dataIndex">The index within the field where the read operation is to start. </param>
		/// <param name="buffer">The buffer into which to read the stream of bytes. </param>
		/// <param name="bufferIndex">The index within the <paramref name="buffer" /> where the write operation is to start. </param>
		/// <param name="length">The number of bytes to read. </param>
		/// <returns>The actual number of bytes read.</returns>
		// Token: 0x06002AE9 RID: 10985 RVA: 0x000B740C File Offset: 0x000B560C
		public override long GetBytes(int i, long dataIndex, byte[] buffer, int bufferIndex, int length)
		{
			ThrowStub.ThrowNotSupportedException();
			return 0L;
		}

		/// <summary>Gets the value of the specified column as a character.</summary>
		/// <param name="i">The zero-based column ordinal. </param>
		/// <returns>The value of the specified column as a character.</returns>
		/// <exception cref="T:System.InvalidCastException">The specified cast is not valid. </exception>
		// Token: 0x06002AEA RID: 10986 RVA: 0x000B7428 File Offset: 0x000B5628
		public override char GetChar(int i)
		{
			ThrowStub.ThrowNotSupportedException();
			return '\0';
		}

		/// <summary>Reads a stream of characters from the specified column offset into the buffer as an array, starting at the particular buffer offset.</summary>
		/// <param name="i">The zero-based column ordinal. </param>
		/// <param name="dataIndex">The index within the row where the read operation is to start. </param>
		/// <param name="buffer">The buffer into which to copy data. </param>
		/// <param name="bufferIndex">The index within the <paramref name="buffer" /> where the write operation is to start. </param>
		/// <param name="length">The number of characters to read. </param>
		/// <returns>The actual number of characters read.</returns>
		// Token: 0x06002AEB RID: 10987 RVA: 0x000B7444 File Offset: 0x000B5644
		public override long GetChars(int i, long dataIndex, char[] buffer, int bufferIndex, int length)
		{
			ThrowStub.ThrowNotSupportedException();
			return 0L;
		}

		/// <summary>Gets the name of the source data type.</summary>
		/// <param name="i">The zero-based column ordinal. </param>
		/// <returns>The name of the source data type.</returns>
		// Token: 0x06002AEC RID: 10988 RVA: 0x00051759 File Offset: 0x0004F959
		public override string GetDataTypeName(int i)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Gets the value of the specified column as a <see cref="T:System.DateTime" /> object.</summary>
		/// <param name="i">The zero-based column ordinal. </param>
		/// <returns>The value of the specified column as a <see cref="T:System.DateTime" /> object.</returns>
		// Token: 0x06002AED RID: 10989 RVA: 0x000B7460 File Offset: 0x000B5660
		public DateTime GetDate(int i)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(DateTime);
		}

		/// <summary>Gets the value of the specified column as a <see cref="T:System.DateTime" /> object.</summary>
		/// <param name="i">The zero-based column ordinal. </param>
		/// <returns>The value of the specified column as a <see cref="T:System.DateTime" /> object.</returns>
		/// <exception cref="T:System.InvalidCastException">The specified cast is not valid. </exception>
		// Token: 0x06002AEE RID: 10990 RVA: 0x000B747C File Offset: 0x000B567C
		public override DateTime GetDateTime(int i)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(DateTime);
		}

		/// <summary>Gets the value of the specified column as a <see cref="T:System.Decimal" /> object.</summary>
		/// <param name="i">The zero-based column ordinal. </param>
		/// <returns>The value of the specified column as a <see cref="T:System.Decimal" /> object.</returns>
		/// <exception cref="T:System.InvalidCastException">The specified cast is not valid. </exception>
		// Token: 0x06002AEF RID: 10991 RVA: 0x000B7498 File Offset: 0x000B5698
		public override decimal GetDecimal(int i)
		{
			ThrowStub.ThrowNotSupportedException();
			return 0m;
		}

		/// <summary>Gets the value of the specified column as a double-precision floating-point number.</summary>
		/// <param name="i">The zero-based column ordinal. </param>
		/// <returns>The value of the specified column as a double-precision floating-point number.</returns>
		/// <exception cref="T:System.InvalidCastException">The specified cast is not valid. </exception>
		// Token: 0x06002AF0 RID: 10992 RVA: 0x000B74B4 File Offset: 0x000B56B4
		public override double GetDouble(int i)
		{
			ThrowStub.ThrowNotSupportedException();
			return 0.0;
		}

		/// <summary>Returns an <see cref="T:System.Collections.IEnumerator" /> that can be used to iterate through the rows in the data reader.</summary>
		/// <returns>An <see cref="T:System.Collections.IEnumerator" /> that can be used to iterate through the rows in the data reader.</returns>
		// Token: 0x06002AF1 RID: 10993 RVA: 0x00051759 File Offset: 0x0004F959
		public override IEnumerator GetEnumerator()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Gets the <see cref="T:System.Type" /> that is the data type of the object.</summary>
		/// <param name="i">The zero-based column ordinal. </param>
		/// <returns>The <see cref="T:System.Type" /> that is the data type of the object.</returns>
		// Token: 0x06002AF2 RID: 10994 RVA: 0x00051759 File Offset: 0x0004F959
		public override Type GetFieldType(int i)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Gets the value of the specified column as a single-precision floating-point number.</summary>
		/// <param name="i">The zero-based column ordinal. </param>
		/// <returns>The value of the specified column as a single-precision floating-point number.</returns>
		/// <exception cref="T:System.InvalidCastException">The specified cast is not valid. </exception>
		// Token: 0x06002AF3 RID: 10995 RVA: 0x000B74D0 File Offset: 0x000B56D0
		public override float GetFloat(int i)
		{
			ThrowStub.ThrowNotSupportedException();
			return 0f;
		}

		/// <summary>Gets the value of the specified column as a globally unique identifier (GUID).</summary>
		/// <param name="i">The zero-based column ordinal. </param>
		/// <returns>The value of the specified column as a GUID.</returns>
		/// <exception cref="T:System.InvalidCastException">The specified cast is not valid. </exception>
		// Token: 0x06002AF4 RID: 10996 RVA: 0x000B74EC File Offset: 0x000B56EC
		public override Guid GetGuid(int i)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(Guid);
		}

		/// <summary>Gets the value of the specified column as a 16-bit signed integer.</summary>
		/// <param name="i">The zero-based column ordinal. </param>
		/// <returns>The value of the specified column as a 16-bit signed integer.</returns>
		/// <exception cref="T:System.InvalidCastException">The specified cast is not valid. </exception>
		// Token: 0x06002AF5 RID: 10997 RVA: 0x000B7508 File Offset: 0x000B5708
		public override short GetInt16(int i)
		{
			ThrowStub.ThrowNotSupportedException();
			return 0;
		}

		/// <summary>Gets the value of the specified column as a 32-bit signed integer.</summary>
		/// <param name="i">The zero-based column ordinal. </param>
		/// <returns>The value of the specified column as a 32-bit signed integer.</returns>
		/// <exception cref="T:System.InvalidCastException">The specified cast is not valid. </exception>
		// Token: 0x06002AF6 RID: 10998 RVA: 0x000B7524 File Offset: 0x000B5724
		public override int GetInt32(int i)
		{
			ThrowStub.ThrowNotSupportedException();
			return 0;
		}

		/// <summary>Gets the value of the specified column as a 64-bit signed integer.</summary>
		/// <param name="i">The zero-based column ordinal. </param>
		/// <returns>The value of the specified column as a 64-bit signed integer.</returns>
		/// <exception cref="T:System.InvalidCastException">The specified cast is not valid. </exception>
		// Token: 0x06002AF7 RID: 10999 RVA: 0x000B7540 File Offset: 0x000B5740
		public override long GetInt64(int i)
		{
			ThrowStub.ThrowNotSupportedException();
			return 0L;
		}

		/// <summary>Gets the name of the specified column.</summary>
		/// <param name="i">The zero-based column ordinal. </param>
		/// <returns>A string that is the name of the specified column.</returns>
		// Token: 0x06002AF8 RID: 11000 RVA: 0x00051759 File Offset: 0x0004F959
		public override string GetName(int i)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Gets the column ordinal, given the name of the column.</summary>
		/// <param name="value">The name of the column. </param>
		/// <returns>The zero-based column ordinal.</returns>
		// Token: 0x06002AF9 RID: 11001 RVA: 0x000B755C File Offset: 0x000B575C
		public override int GetOrdinal(string value)
		{
			ThrowStub.ThrowNotSupportedException();
			return 0;
		}

		/// <summary>Gets the value of the specified column as a <see cref="T:System.String" />.</summary>
		/// <param name="i">The zero-based column ordinal. </param>
		/// <returns>The value of the specified column as a <see cref="T:System.String" />.</returns>
		/// <exception cref="T:System.InvalidCastException">The specified cast is not valid. </exception>
		// Token: 0x06002AFA RID: 11002 RVA: 0x00051759 File Offset: 0x0004F959
		public override string GetString(int i)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Gets the value of the specified column as a <see cref="T:System.TimeSpan" /> object.</summary>
		/// <param name="i">The zero-based column ordinal. </param>
		/// <returns>The value of the specified column as a <see cref="T:System.TimeSpan" /> object.</returns>
		// Token: 0x06002AFB RID: 11003 RVA: 0x000B7578 File Offset: 0x000B5778
		public TimeSpan GetTime(int i)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(TimeSpan);
		}

		/// <summary>Gets the value of the column at the specified ordinal in its native format.</summary>
		/// <param name="i">The zero-based column ordinal. </param>
		/// <returns>The value to return.</returns>
		// Token: 0x06002AFC RID: 11004 RVA: 0x00051759 File Offset: 0x0004F959
		public override object GetValue(int i)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Populates an array of objects with the column values of the current row.</summary>
		/// <param name="values">An array of type <see cref="T:System.Object" /> into which to copy the attribute columns. </param>
		/// <returns>The number of instances of <see cref="T:System.Object" /> in the array.</returns>
		// Token: 0x06002AFD RID: 11005 RVA: 0x000B7594 File Offset: 0x000B5794
		public override int GetValues(object[] values)
		{
			ThrowStub.ThrowNotSupportedException();
			return 0;
		}

		/// <summary>Gets a value that indicates whether the column contains nonexistent or missing values.</summary>
		/// <param name="i">The zero-based column ordinal. </param>
		/// <returns>
		///     <see langword="true" /> if the specified column value is equivalent to <see cref="T:System.DBNull" />; otherwise <see langword="false" />.</returns>
		// Token: 0x06002AFE RID: 11006 RVA: 0x000B75B0 File Offset: 0x000B57B0
		public override bool IsDBNull(int i)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Advances the <see cref="T:System.Data.Odbc.OdbcDataReader" /> to the next result when reading the results of batch SQL statements.</summary>
		/// <returns>
		///     <see langword="true" /> if there are more result sets; otherwise <see langword="false" />.</returns>
		// Token: 0x06002AFF RID: 11007 RVA: 0x000B75CC File Offset: 0x000B57CC
		public override bool NextResult()
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Advances the <see cref="T:System.Data.Odbc.OdbcDataReader" /> to the next record.</summary>
		/// <returns>
		///     <see langword="true" /> if there are more rows; otherwise <see langword="false" />.</returns>
		// Token: 0x06002B00 RID: 11008 RVA: 0x000B75E8 File Offset: 0x000B57E8
		public override bool Read()
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}
	}
}
