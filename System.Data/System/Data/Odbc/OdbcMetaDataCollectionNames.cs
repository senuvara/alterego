﻿using System;

namespace System.Data.Odbc
{
	/// <summary>Provides a list of constants for use with the GetSchema method to retrieve metadata collections.</summary>
	// Token: 0x02000382 RID: 898
	public static class OdbcMetaDataCollectionNames
	{
		/// <summary>A constant for use with the GetSchema method that represents the Columns collection.</summary>
		// Token: 0x04001972 RID: 6514
		public static readonly string Columns;

		/// <summary>A constant for use with the GetSchema method that represents the Indexes collection.</summary>
		// Token: 0x04001973 RID: 6515
		public static readonly string Indexes;

		/// <summary>A constant for use with the GetSchema method that represents the ProcedureColumns collection.</summary>
		// Token: 0x04001974 RID: 6516
		public static readonly string ProcedureColumns;

		/// <summary>A constant for use with the GetSchema method that represents the ProcedureParameters collection.</summary>
		// Token: 0x04001975 RID: 6517
		public static readonly string ProcedureParameters;

		/// <summary>A constant for use with the GetSchema method that represents the Procedures collection. </summary>
		// Token: 0x04001976 RID: 6518
		public static readonly string Procedures;

		/// <summary>A constant for use with the GetSchema method that represents the Tables collection.</summary>
		// Token: 0x04001977 RID: 6519
		public static readonly string Tables;

		/// <summary>A constant for use with the GetSchema method that represents the Views collection.</summary>
		// Token: 0x04001978 RID: 6520
		public static readonly string Views;
	}
}
