﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data.Common;
using Unity;

namespace System.Data.Odbc
{
	/// <summary>Represents a collection of parameters relevant to an <see cref="T:System.Data.Odbc.OdbcCommand" /> and their respective mappings to columns in a <see cref="T:System.Data.DataSet" />. This class cannot be inherited.</summary>
	// Token: 0x02000386 RID: 902
	[Editor("Microsoft.VSDesigner.Data.Design.DBParametersEditor, Microsoft.VSDesigner, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a", "System.Drawing.Design.UITypeEditor, System.Drawing, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")]
	[ListBindable(false)]
	public sealed class OdbcParameterCollection : DbParameterCollection
	{
		// Token: 0x06002B20 RID: 11040 RVA: 0x00010458 File Offset: 0x0000E658
		internal OdbcParameterCollection()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Returns an Integer that contains the number of elements in the <see cref="T:System.Data.Odbc.OdbcParameterCollection" />. Read-only.</summary>
		/// <returns>The number of elements in the <see cref="T:System.Data.Odbc.OdbcParameterCollection" /> as an Integer.</returns>
		// Token: 0x17000730 RID: 1840
		// (get) Token: 0x06002B21 RID: 11041 RVA: 0x000B76AC File Offset: 0x000B58AC
		public override int Count
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
		}

		// Token: 0x06002B22 RID: 11042 RVA: 0x00010458 File Offset: 0x0000E658
		public void set_Item(int index, OdbcParameter value)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the <see cref="T:System.Data.Odbc.OdbcParameter" /> with the specified name.</summary>
		/// <param name="parameterName">The name of the parameter to retrieve. </param>
		/// <returns>The <see cref="T:System.Data.Odbc.OdbcParameter" /> with the specified name.</returns>
		/// <exception cref="T:System.IndexOutOfRangeException">The name specified does not exist. </exception>
		// Token: 0x17000731 RID: 1841
		public string this[string parameterName]
		{
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets an object that can be used to synchronize access to the <see cref="T:System.Data.Odbc.OdbcParameterCollection" />. Read-only.</summary>
		/// <returns>An object that can be used to synchronize access to the <see cref="T:System.Data.Odbc.OdbcParameterCollection" />.</returns>
		// Token: 0x17000732 RID: 1842
		// (get) Token: 0x06002B24 RID: 11044 RVA: 0x00051759 File Offset: 0x0004F959
		public override object SyncRoot
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Adds the specified <see cref="T:System.Data.Odbc.OdbcParameter" /> to the <see cref="T:System.Data.Odbc.OdbcParameterCollection" />.</summary>
		/// <param name="value">The <see cref="T:System.Data.Odbc.OdbcParameter" /> to add to the collection. </param>
		/// <returns>The index of the new <see cref="T:System.Data.Odbc.OdbcParameter" /> object.</returns>
		/// <exception cref="T:System.ArgumentException">The <see cref="T:System.Data.Odbc.OdbcParameter" /> specified in the <paramref name="value" /> parameter is already added to this or another <see cref="T:System.Data.Odbc.OdbcParameterCollection" />. </exception>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="value" /> parameter is null.</exception>
		// Token: 0x06002B25 RID: 11045 RVA: 0x00051759 File Offset: 0x0004F959
		public OdbcParameter Add(OdbcParameter value)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Adds the specified <see cref="T:System.Data.Odbc.OdbcParameter" /> object to the <see cref="T:System.Data.Odbc.OdbcParameterCollection" />.</summary>
		/// <param name="value">A <see cref="T:System.Object" />.</param>
		/// <returns>The index of the new <see cref="T:System.Data.Odbc.OdbcParameter" /> object in the collection.</returns>
		// Token: 0x06002B26 RID: 11046 RVA: 0x000B76C8 File Offset: 0x000B58C8
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override int Add(object value)
		{
			ThrowStub.ThrowNotSupportedException();
			return 0;
		}

		/// <summary>Adds an <see cref="T:System.Data.Odbc.OdbcParameter" /> to the <see cref="T:System.Data.Odbc.OdbcParameterCollection" />, given the parameter name and data type.</summary>
		/// <param name="parameterName">The name of the parameter. </param>
		/// <param name="odbcType">One of the <see cref="T:System.Data.Odbc.OdbcType" /> values. </param>
		/// <returns>The index of the new <see cref="T:System.Data.Odbc.OdbcParameter" /> object.</returns>
		// Token: 0x06002B27 RID: 11047 RVA: 0x00051759 File Offset: 0x0004F959
		public OdbcParameter Add(string parameterName, OdbcType odbcType)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Adds an <see cref="T:System.Data.Odbc.OdbcParameter" /> to the <see cref="T:System.Data.Odbc.OdbcParameterCollection" />, given the parameter name, data type, and column length.</summary>
		/// <param name="parameterName">The name of the parameter. </param>
		/// <param name="odbcType">One of the <see cref="T:System.Data.Odbc.OdbcType" /> values. </param>
		/// <param name="size">The length of the column. </param>
		/// <returns>The index of the new <see cref="T:System.Data.Odbc.OdbcParameter" /> object.</returns>
		// Token: 0x06002B28 RID: 11048 RVA: 0x00051759 File Offset: 0x0004F959
		public OdbcParameter Add(string parameterName, OdbcType odbcType, int size)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Adds an <see cref="T:System.Data.Odbc.OdbcParameter" /> to the <see cref="T:System.Data.Odbc.OdbcParameterCollection" /> given the parameter name, data type, column length, and source column name.</summary>
		/// <param name="parameterName">The name of the parameter. </param>
		/// <param name="odbcType">One of the <see cref="T:System.Data.Odbc.OdbcType" /> values. </param>
		/// <param name="size">The length of the column. </param>
		/// <param name="sourceColumn">The name of the source column. </param>
		/// <returns>The index of the new <see cref="T:System.Data.Odbc.OdbcParameter" /> object.</returns>
		// Token: 0x06002B29 RID: 11049 RVA: 0x00051759 File Offset: 0x0004F959
		public OdbcParameter Add(string parameterName, OdbcType odbcType, int size, string sourceColumn)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Adds an <see cref="T:System.Data.Odbc.OdbcParameter" /> to the <see cref="T:System.Data.Odbc.OdbcParameterCollection" /> given the parameter name and value.</summary>
		/// <param name="parameterName">The name of the parameter. </param>
		/// <param name="value">The <see cref="P:System.Data.OleDb.OleDbParameter.Value" /> of the <see cref="T:System.Data.Odbc.OdbcParameter" /> to add to the collection. </param>
		/// <returns>The index of the new <see cref="T:System.Data.Odbc.OdbcParameter" /> object.</returns>
		/// <exception cref="T:System.InvalidCastException">The <paramref name="value" /> parameter is not an <see cref="T:System.Data.Odbc.OdbcParameter" />. </exception>
		// Token: 0x06002B2A RID: 11050 RVA: 0x00051759 File Offset: 0x0004F959
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Obsolete("Add(String parameterName, Object value) has been deprecated.  Use AddWithValue(String parameterName, Object value).  http://go.microsoft.com/fwlink/?linkid=14202", false)]
		public OdbcParameter Add(string parameterName, object value)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Adds an array of values to the end of the <see cref="T:System.Data.Odbc.OdbcParameterCollection" />.</summary>
		/// <param name="values">The <see cref="T:System.Array" /> values to add.</param>
		// Token: 0x06002B2B RID: 11051 RVA: 0x00010458 File Offset: 0x0000E658
		public override void AddRange(Array values)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Adds an array of <see cref="T:System.Data.Odbc.OdbcParameter" /> values to the end of the <see cref="T:System.Data.Odbc.OdbcParameterCollection" />.</summary>
		/// <param name="values">An array of <see cref="T:System.Data.Odbc.OdbcParameter" /> objects to add to the collection.</param>
		// Token: 0x06002B2C RID: 11052 RVA: 0x00010458 File Offset: 0x0000E658
		public void AddRange(OdbcParameter[] values)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Adds a value to the end of the <see cref="T:System.Data.Odbc.OdbcParameterCollection" />. </summary>
		/// <param name="parameterName">The name of the parameter.</param>
		/// <param name="value">The value to be added.</param>
		/// <returns>An <see cref="T:System.Data.Odbc.OdbcParameter" /> object.</returns>
		// Token: 0x06002B2D RID: 11053 RVA: 0x00051759 File Offset: 0x0004F959
		public OdbcParameter AddWithValue(string parameterName, object value)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Removes all <see cref="T:System.Data.Odbc.OdbcParameter" /> objects from the <see cref="T:System.Data.Odbc.OdbcParameterCollection" />.</summary>
		// Token: 0x06002B2E RID: 11054 RVA: 0x00010458 File Offset: 0x0000E658
		public override void Clear()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Determines whether the specified <see cref="T:System.Data.Odbc.OdbcParameter" /> is in this <see cref="T:System.Data.Odbc.OdbcParameterCollection" />.</summary>
		/// <param name="value">The <see cref="T:System.Data.Odbc.OdbcParameter" /> value.</param>
		/// <returns>
		///     <see langword="true" /> if the <see cref="T:System.Data.Odbc.OdbcParameter" /> is in the collection; otherwise, <see langword="false" />.</returns>
		// Token: 0x06002B2F RID: 11055 RVA: 0x000B76E4 File Offset: 0x000B58E4
		public bool Contains(OdbcParameter value)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Determines whether the specified <see cref="T:System.Object" /> is in this <see cref="T:System.Data.Odbc.OdbcParameterCollection" />.</summary>
		/// <param name="value">The <see cref="T:System.Object" /> value.</param>
		/// <returns>
		///     <see langword="true" /> if the <see cref="T:System.Data.Odbc.OdbcParameterCollection" /> contains the value otherwise <see langword="false" />.</returns>
		// Token: 0x06002B30 RID: 11056 RVA: 0x000B7700 File Offset: 0x000B5900
		public override bool Contains(object value)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Gets a value indicating whether an <see cref="T:System.Data.Odbc.OdbcParameter" /> object with the specified parameter name exists in the collection.</summary>
		/// <param name="value">The name of the <see cref="T:System.Data.Odbc.OdbcParameter" /> object to find. </param>
		/// <returns>
		///     <see langword="true" /> if the collection contains the parameter; otherwise, <see langword="false" />.</returns>
		// Token: 0x06002B31 RID: 11057 RVA: 0x000B771C File Offset: 0x000B591C
		public override bool Contains(string value)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Copies all the elements of the current <see cref="T:System.Data.Odbc.OdbcParameterCollection" /> to the specified one-dimensional <see cref="T:System.Array" /> starting at the specified destination <see cref="T:System.Array" /> index.</summary>
		/// <param name="array">The one-dimensional <see cref="T:System.Array" /> that is the destination of the elements copied from the current <see cref="T:System.Data.Odbc.OdbcParameterCollection" />.</param>
		/// <param name="index">A 32-bit integer that represents the index in the <see cref="T:System.Array" /> at which copying starts.</param>
		// Token: 0x06002B32 RID: 11058 RVA: 0x00010458 File Offset: 0x0000E658
		public override void CopyTo(Array array, int index)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Copies all the elements of the current <see cref="T:System.Data.Odbc.OdbcParameterCollection" /> to the specified <see cref="T:System.Data.Odbc.OdbcParameterCollection" /> starting at the specified destination index.</summary>
		/// <param name="array">The <see cref="T:System.Data.Odbc.OdbcParameterCollection" /> that is the destination of the elements copied from the current <see cref="T:System.Data.Odbc.OdbcParameterCollection" />.</param>
		/// <param name="index">A 32-bit integer that represents the index in the <see cref="T:System.Data.Odbc.OdbcParameterCollection" /> at which copying starts.</param>
		// Token: 0x06002B33 RID: 11059 RVA: 0x00010458 File Offset: 0x0000E658
		public void CopyTo(OdbcParameter[] array, int index)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Returns an enumerator that iterates through the <see cref="T:System.Data.Odbc.OdbcParameterCollection" />.</summary>
		/// <returns>An <see cref="T:System.Collections.Generic.IEnumerator" /> for the <see cref="T:System.Data.Odbc.OdbcParameterCollection" />.</returns>
		// Token: 0x06002B34 RID: 11060 RVA: 0x00051759 File Offset: 0x0004F959
		public override IEnumerator GetEnumerator()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		// Token: 0x06002B35 RID: 11061 RVA: 0x00051759 File Offset: 0x0004F959
		protected override DbParameter GetParameter(int index)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		// Token: 0x06002B36 RID: 11062 RVA: 0x00051759 File Offset: 0x0004F959
		protected override DbParameter GetParameter(string parameterName)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Gets the location of the specified <see cref="T:System.Data.Odbc.OdbcParameter" /> within the collection.</summary>
		/// <param name="value">The <see cref="T:System.Data.Odbc.OdbcParameter" /> object in the collection to find.</param>
		/// <returns>The zero-based location of the specified <see cref="T:System.Data.Odbc.OdbcParameter" /> within the collection.</returns>
		// Token: 0x06002B37 RID: 11063 RVA: 0x000B7738 File Offset: 0x000B5938
		public int IndexOf(OdbcParameter value)
		{
			ThrowStub.ThrowNotSupportedException();
			return 0;
		}

		/// <summary>Gets the location of the specified <see cref="T:System.Object" /> within the collection.</summary>
		/// <param name="value">The <see cref="T:System.Object" /> to find.</param>
		/// <returns>The zero-based location of the specified <see cref="T:System.Object" /> that is a <see cref="T:System.Data.Odbc.OdbcParameter" /> within the collection.</returns>
		// Token: 0x06002B38 RID: 11064 RVA: 0x000B7754 File Offset: 0x000B5954
		public override int IndexOf(object value)
		{
			ThrowStub.ThrowNotSupportedException();
			return 0;
		}

		/// <summary>Gets the location of the specified <see cref="T:System.Data.Odbc.OdbcParameter" /> with the specified name.</summary>
		/// <param name="parameterName">The case-sensitive name of the <see cref="T:System.Data.Odbc.OdbcParameter" /> to find.</param>
		/// <returns>The zero-based location of the specified <see cref="T:System.Data.Odbc.OdbcParameter" /> with the specified case-sensitive name.</returns>
		// Token: 0x06002B39 RID: 11065 RVA: 0x000B7770 File Offset: 0x000B5970
		public override int IndexOf(string parameterName)
		{
			ThrowStub.ThrowNotSupportedException();
			return 0;
		}

		/// <summary>Inserts a <see cref="T:System.Data.Odbc.OdbcParameter" /> object into the <see cref="T:System.Data.Odbc.OdbcParameterCollection" /> at the specified index.</summary>
		/// <param name="index">The zero-based index at which the object should be inserted.</param>
		/// <param name="value">A <see cref="T:System.Data.Odbc.OdbcParameter" /> object to be inserted in the <see cref="T:System.Data.Odbc.OdbcParameterCollection" />.</param>
		// Token: 0x06002B3A RID: 11066 RVA: 0x00010458 File Offset: 0x0000E658
		public void Insert(int index, OdbcParameter value)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Inserts a <see cref="T:System.Object" /> into the <see cref="T:System.Data.Odbc.OdbcParameterCollection" /> at the specified index.</summary>
		/// <param name="index">The zero-based index at which the object should be inserted.</param>
		/// <param name="value">A <see cref="T:System.Object" /> to be inserted in the <see cref="T:System.Data.Odbc.OdbcParameterCollection" />.</param>
		// Token: 0x06002B3B RID: 11067 RVA: 0x00010458 File Offset: 0x0000E658
		public override void Insert(int index, object value)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Removes the <see cref="T:System.Data.Odbc.OdbcParameter" /> from the <see cref="T:System.Data.Odbc.OdbcParameterCollection" />.</summary>
		/// <param name="value">A <see cref="T:System.Data.Odbc.OdbcParameter" /> object to remove from the collection.</param>
		/// <exception cref="T:System.InvalidCastException">The parameter is not a <see cref="T:System.Data.Odbc.OdbcParameter" />.</exception>
		/// <exception cref="T:System.SystemException">The parameter does not exist in the collection.</exception>
		// Token: 0x06002B3C RID: 11068 RVA: 0x00010458 File Offset: 0x0000E658
		public void Remove(OdbcParameter value)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Removes the <see cref="T:System.Object" /> object from the <see cref="T:System.Data.Odbc.OdbcParameterCollection" />.</summary>
		/// <param name="value">A <see cref="T:System.Object" /> to be removed from the <see cref="T:System.Data.Odbc.OdbcParameterCollection" />.</param>
		// Token: 0x06002B3D RID: 11069 RVA: 0x00010458 File Offset: 0x0000E658
		public override void Remove(object value)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Removes the <see cref="T:System.Data.Odbc.OdbcParameter" /> from the <see cref="T:System.Data.Odbc.OdbcParameterCollection" /> at the specified index.</summary>
		/// <param name="index">The zero-based index of the <see cref="T:System.Data.Odbc.OdbcParameter" /> object to remove.</param>
		// Token: 0x06002B3E RID: 11070 RVA: 0x00010458 File Offset: 0x0000E658
		public override void RemoveAt(int index)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Removes the <see cref="T:System.Data.Odbc.OdbcParameter" /> from the <see cref="T:System.Data.Odbc.OdbcParameterCollection" /> with the specified parameter name.</summary>
		/// <param name="parameterName">The name of the <see cref="T:System.Data.Odbc.OdbcParameter" /> object to remove.</param>
		// Token: 0x06002B3F RID: 11071 RVA: 0x00010458 File Offset: 0x0000E658
		public override void RemoveAt(string parameterName)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x06002B40 RID: 11072 RVA: 0x00010458 File Offset: 0x0000E658
		protected override void SetParameter(int index, DbParameter value)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x06002B41 RID: 11073 RVA: 0x00010458 File Offset: 0x0000E658
		protected override void SetParameter(string parameterName, DbParameter value)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
