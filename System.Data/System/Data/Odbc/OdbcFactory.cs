﻿using System;
using System.Data.Common;
using Unity;

namespace System.Data.Odbc
{
	/// <summary>Represents a set of methods for creating instances of the ODBC provider's implementation of the data source classes.</summary>
	// Token: 0x02000381 RID: 897
	public sealed class OdbcFactory : DbProviderFactory
	{
		// Token: 0x06002B03 RID: 11011 RVA: 0x00010458 File Offset: 0x0000E658
		internal OdbcFactory()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets an instance of the <see cref="T:System.Data.Odbc.OdbcFactory" />, which can be used to retrieve strongly-typed data objects.</summary>
		// Token: 0x04001971 RID: 6513
		public static readonly OdbcFactory Instance;
	}
}
