﻿using System;
using System.ComponentModel;
using System.Data.Common;
using System.Reflection;
using Unity;

namespace System.Data.Odbc
{
	/// <summary>Provides a simple way to create and manage the contents of connection strings used by the <see cref="T:System.Data.Odbc.OdbcConnection" /> class.</summary>
	// Token: 0x0200037E RID: 894
	[DefaultMember("Item")]
	[TypeConverter("System.Data.Odbc.OdbcConnectionStringBuilder.OdbcConnectionStringBuilderConverter")]
	[DefaultProperty("Driver")]
	public sealed class OdbcConnectionStringBuilder : DbConnectionStringBuilder
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Data.Odbc.OdbcConnectionStringBuilder" /> class.</summary>
		// Token: 0x06002AD9 RID: 10969 RVA: 0x00010458 File Offset: 0x0000E658
		public OdbcConnectionStringBuilder()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Data.Odbc.OdbcConnectionStringBuilder" /> class. The provided connection string provides the data for the instance's internal connection information.</summary>
		/// <param name="connectionString">The basis for the object's internal connection information. Parsed into key/value pairs.</param>
		/// <exception cref="T:System.ArgumentException">The connection string is incorrectly formatted (perhaps missing the required "=" within a key/value pair).</exception>
		// Token: 0x06002ADA RID: 10970 RVA: 0x00010458 File Offset: 0x0000E658
		public OdbcConnectionStringBuilder(string connectionString)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the name of the ODBC driver associated with the connection.</summary>
		/// <returns>The value of the <see cref="P:System.Data.Odbc.OdbcConnectionStringBuilder.Driver" /> property, or <see langword="String.Empty" /> if none has been supplied.</returns>
		// Token: 0x1700071E RID: 1822
		// (get) Token: 0x06002ADB RID: 10971 RVA: 0x00051759 File Offset: 0x0004F959
		// (set) Token: 0x06002ADC RID: 10972 RVA: 0x00010458 File Offset: 0x0000E658
		public string Driver
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the name of the data source name (DSN) associated with the connection.</summary>
		/// <returns>The value of the <see cref="P:System.Data.Odbc.OdbcConnectionStringBuilder.Dsn" /> property, or <see langword="String.Empty" /> if none has been supplied.</returns>
		// Token: 0x1700071F RID: 1823
		// (get) Token: 0x06002ADD RID: 10973 RVA: 0x00051759 File Offset: 0x0004F959
		// (set) Token: 0x06002ADE RID: 10974 RVA: 0x00010458 File Offset: 0x0000E658
		public string Dsn
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}
	}
}
