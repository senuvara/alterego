﻿using System;
using System.ComponentModel;
using System.Data.Common;
using Unity;

namespace System.Data.Odbc
{
	/// <summary>Represents a parameter to an <see cref="T:System.Data.Odbc.OdbcCommand" /> and optionally, its mapping to a <see cref="T:System.Data.DataColumn" />. This class cannot be inherited.</summary>
	// Token: 0x02000384 RID: 900
	[TypeConverter("System.Data.Odbc.OdbcParameter.OdbcParameterConverter")]
	public sealed class OdbcParameter : DbParameter, ICloneable
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Data.Odbc.OdbcParameter" /> class.</summary>
		// Token: 0x06002B04 RID: 11012 RVA: 0x00010458 File Offset: 0x0000E658
		public OdbcParameter()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Data.Odbc.OdbcParameter" /> class that uses the parameter name and data type.</summary>
		/// <param name="name">The name of the parameter. </param>
		/// <param name="type">One of the <see cref="T:System.Data.Odbc.OdbcType" /> values. </param>
		/// <exception cref="T:System.ArgumentException">The value supplied in the <paramref name="type" /> parameter is an invalid back-end data type. </exception>
		// Token: 0x06002B05 RID: 11013 RVA: 0x00010458 File Offset: 0x0000E658
		public OdbcParameter(string name, OdbcType type)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Data.Odbc.OdbcParameter" /> class that uses the parameter name, data type, and length.</summary>
		/// <param name="name">The name of the parameter. </param>
		/// <param name="type">One of the <see cref="T:System.Data.Odbc.OdbcType" /> values. </param>
		/// <param name="size">The length of the parameter. </param>
		/// <exception cref="T:System.ArgumentException">The value supplied in the <paramref name="type" /> parameter is an invalid back-end data type. </exception>
		// Token: 0x06002B06 RID: 11014 RVA: 0x00010458 File Offset: 0x0000E658
		public OdbcParameter(string name, OdbcType type, int size)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Data.Odbc.OdbcParameter" /> class that uses the parameter name, data type, length, source column name, parameter direction, numeric precision, and other properties.</summary>
		/// <param name="parameterName">The name of the parameter. </param>
		/// <param name="odbcType">One of the <see cref="T:System.Data.Odbc.OdbcType" /> values. </param>
		/// <param name="size">The length of the parameter. </param>
		/// <param name="parameterDirection">One of the <see cref="T:System.Data.ParameterDirection" /> values. </param>
		/// <param name="isNullable">
		///       <see langword="true" /> if the value of the field can be null; otherwise <see langword="false" />. </param>
		/// <param name="precision">The total number of digits to the left and right of the decimal point to which <see cref="P:System.Data.Odbc.OdbcParameter.Value" /> is resolved. </param>
		/// <param name="scale">The total number of decimal places to which <see cref="P:System.Data.Odbc.OdbcParameter.Value" /> is resolved. </param>
		/// <param name="srcColumn">The name of the source column. </param>
		/// <param name="srcVersion">One of the <see cref="T:System.Data.DataRowVersion" /> values. </param>
		/// <param name="value">An <see cref="T:System.Object" /> that is the value of the <see cref="T:System.Data.Odbc.OdbcParameter" />. </param>
		/// <exception cref="T:System.ArgumentException">The value supplied in the <paramref name="type" /> parameter is an invalid back-end data type. </exception>
		// Token: 0x06002B07 RID: 11015 RVA: 0x00010458 File Offset: 0x0000E658
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public OdbcParameter(string parameterName, OdbcType odbcType, int size, ParameterDirection parameterDirection, bool isNullable, byte precision, byte scale, string srcColumn, DataRowVersion srcVersion, object value)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Data.Odbc.OdbcParameter" /> class that uses the parameter name, data type, length, source column name, parameter direction, numeric precision, and other properties.</summary>
		/// <param name="parameterName">The name of the parameter. </param>
		/// <param name="odbcType">One of the <see cref="P:System.Data.Odbc.OdbcParameter.OdbcType" /> values. </param>
		/// <param name="size">The length of the parameter. </param>
		/// <param name="parameterDirection">One of the <see cref="T:System.Data.ParameterDirection" /> values. </param>
		/// <param name="precision">The total number of digits to the left and right of the decimal point to which <see cref="P:System.Data.Odbc.OdbcParameter.Value" /> is resolved. </param>
		/// <param name="scale">The total number of decimal places to which <see cref="P:System.Data.Odbc.OdbcParameter.Value" /> is resolved. </param>
		/// <param name="sourceColumn">The name of the source column. </param>
		/// <param name="sourceVersion">One of the <see cref="T:System.Data.DataRowVersion" /> values. </param>
		/// <param name="sourceColumnNullMapping">
		///       <see langword="true" /> if the corresponding source column is nullable; <see langword="false" /> if it is not.</param>
		/// <param name="value">An <see cref="T:System.Object" /> that is the value of the <see cref="T:System.Data.Odbc.OdbcParameter" />. </param>
		/// <exception cref="T:System.ArgumentException">The value supplied in the <paramref name="type" /> parameter is an invalid back-end data type. </exception>
		// Token: 0x06002B08 RID: 11016 RVA: 0x00010458 File Offset: 0x0000E658
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public OdbcParameter(string parameterName, OdbcType odbcType, int size, ParameterDirection parameterDirection, byte precision, byte scale, string sourceColumn, DataRowVersion sourceVersion, bool sourceColumnNullMapping, object value)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Data.Odbc.OdbcParameter" /> class that uses the parameter name, data type, length, and source column name.</summary>
		/// <param name="name">The name of the parameter. </param>
		/// <param name="type">One of the <see cref="T:System.Data.Odbc.OdbcType" /> values. </param>
		/// <param name="size">The length of the parameter. </param>
		/// <param name="sourcecolumn">The name of the source column. </param>
		/// <exception cref="T:System.ArgumentException">The value supplied in the <paramref name="type" /> parameter is an invalid back-end data type. </exception>
		// Token: 0x06002B09 RID: 11017 RVA: 0x00010458 File Offset: 0x0000E658
		public OdbcParameter(string name, OdbcType type, int size, string sourcecolumn)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Data.Odbc.OdbcParameter" /> class that uses the parameter name and an <see cref="T:System.Data.Odbc.OdbcParameter" /> object.</summary>
		/// <param name="name">The name of the parameter. </param>
		/// <param name="value">An <see cref="T:System.Data.Odbc.OdbcParameter" /> object. </param>
		// Token: 0x06002B0A RID: 11018 RVA: 0x00010458 File Offset: 0x0000E658
		public OdbcParameter(string name, object value)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the <see cref="T:System.Data.DbType" /> of the parameter.</summary>
		/// <returns>One of the <see cref="T:System.Data.DbType" /> values. The default is <see cref="T:System.String" />.</returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The property was not set to a valid <see cref="T:System.Data.DbType" />. </exception>
		// Token: 0x17000727 RID: 1831
		// (get) Token: 0x06002B0B RID: 11019 RVA: 0x000B7604 File Offset: 0x000B5804
		// (set) Token: 0x06002B0C RID: 11020 RVA: 0x00010458 File Offset: 0x0000E658
		public override DbType DbType
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return DbType.AnsiString;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets a value that indicates whether the parameter is input-only, output-only, bidirectional, or a stored procedure return value parameter.</summary>
		/// <returns>One of the <see cref="T:System.Data.ParameterDirection" /> values. The default is <see langword="Input" />.</returns>
		/// <exception cref="T:System.ArgumentException">The property was not set to one of the valid <see cref="T:System.Data.ParameterDirection" /> values.</exception>
		// Token: 0x17000728 RID: 1832
		// (get) Token: 0x06002B0D RID: 11021 RVA: 0x000B7620 File Offset: 0x000B5820
		// (set) Token: 0x06002B0E RID: 11022 RVA: 0x00010458 File Offset: 0x0000E658
		public override ParameterDirection Direction
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return (ParameterDirection)0;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets a value that indicates whether the parameter accepts null values.</summary>
		/// <returns>
		///     <see langword="true" /> if null values are accepted; otherwise <see langword="false" />. The default is <see langword="false" />.</returns>
		// Token: 0x17000729 RID: 1833
		// (get) Token: 0x06002B0F RID: 11023 RVA: 0x000B763C File Offset: 0x000B583C
		// (set) Token: 0x06002B10 RID: 11024 RVA: 0x00010458 File Offset: 0x0000E658
		public override bool IsNullable
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the <see cref="T:System.Data.Odbc.OdbcType" /> of the parameter.</summary>
		/// <returns>An <see cref="T:System.Data.Odbc.OdbcType" /> value that is the <see cref="T:System.Data.Odbc.OdbcType" /> of the parameter. The default is <see langword="Nchar" />.</returns>
		// Token: 0x1700072A RID: 1834
		// (get) Token: 0x06002B11 RID: 11025 RVA: 0x000B7658 File Offset: 0x000B5858
		// (set) Token: 0x06002B12 RID: 11026 RVA: 0x00010458 File Offset: 0x0000E658
		public OdbcType OdbcType
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return (OdbcType)0;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the name of the <see cref="T:System.Data.Odbc.OdbcParameter" />.</summary>
		/// <returns>The name of the <see cref="T:System.Data.Odbc.OdbcParameter" />. The default is an empty string ("").</returns>
		// Token: 0x1700072B RID: 1835
		// (get) Token: 0x06002B13 RID: 11027 RVA: 0x00051759 File Offset: 0x0004F959
		// (set) Token: 0x06002B14 RID: 11028 RVA: 0x00010458 File Offset: 0x0000E658
		public override string ParameterName
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the maximum size of the data within the column.</summary>
		/// <returns>The maximum size of the data within the column. The default value is inferred from the parameter value.</returns>
		// Token: 0x1700072C RID: 1836
		// (get) Token: 0x06002B15 RID: 11029 RVA: 0x000B7674 File Offset: 0x000B5874
		// (set) Token: 0x06002B16 RID: 11030 RVA: 0x00010458 File Offset: 0x0000E658
		public override int Size
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the name of the source column mapped to the <see cref="T:System.Data.DataSet" /> and used for loading or returning the <see cref="P:System.Data.Odbc.OdbcParameter.Value" />.</summary>
		/// <returns>The name of the source column that will be used to set the value of this parameter. The default is an empty string ("").</returns>
		// Token: 0x1700072D RID: 1837
		// (get) Token: 0x06002B17 RID: 11031 RVA: 0x00051759 File Offset: 0x0004F959
		// (set) Token: 0x06002B18 RID: 11032 RVA: 0x00010458 File Offset: 0x0000E658
		public override string SourceColumn
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Sets or gets a value which indicates whether the source column is nullable. This lets <see cref="T:System.Data.Common.DbCommandBuilder" /> correctly generate Update statements for nullable columns.</summary>
		/// <returns>
		///     <see langword="true" /> if the source column is nullable; <see langword="false" /> if it is not.</returns>
		// Token: 0x1700072E RID: 1838
		// (get) Token: 0x06002B19 RID: 11033 RVA: 0x000B7690 File Offset: 0x000B5890
		// (set) Token: 0x06002B1A RID: 11034 RVA: 0x00010458 File Offset: 0x0000E658
		public override bool SourceColumnNullMapping
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the value of the parameter.</summary>
		/// <returns>An <see cref="T:System.Object" /> that is the value of the parameter. The default value is null.</returns>
		// Token: 0x1700072F RID: 1839
		// (get) Token: 0x06002B1B RID: 11035 RVA: 0x00051759 File Offset: 0x0004F959
		// (set) Token: 0x06002B1C RID: 11036 RVA: 0x00010458 File Offset: 0x0000E658
		public override object Value
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Resets the type associated with this <see cref="T:System.Data.Odbc.OdbcParameter" />.</summary>
		// Token: 0x06002B1D RID: 11037 RVA: 0x00010458 File Offset: 0x0000E658
		public override void ResetDbType()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Resets the type associated with this <see cref="T:System.Data.Odbc.OdbcParameter" />.</summary>
		// Token: 0x06002B1E RID: 11038 RVA: 0x00010458 File Offset: 0x0000E658
		public void ResetOdbcType()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>For a description of this member, see <see cref="M:System.ICloneable.Clone" />.</summary>
		/// <returns>A new <see cref="T:System.Object" /> that is a copy of this instance.</returns>
		// Token: 0x06002B1F RID: 11039 RVA: 0x00051759 File Offset: 0x0004F959
		object ICloneable.Clone()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}
	}
}
