﻿using System;
using System.ComponentModel;
using System.Data.Common;
using System.EnterpriseServices;
using Unity;

namespace System.Data.Odbc
{
	/// <summary>Represents an open connection to a data source. </summary>
	// Token: 0x02000372 RID: 882
	[DefaultEvent("InfoMessage")]
	public sealed class OdbcConnection : DbConnection, ICloneable
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Data.Odbc.OdbcConnection" /> class.</summary>
		// Token: 0x06002A8A RID: 10890 RVA: 0x00010458 File Offset: 0x0000E658
		public OdbcConnection()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Data.Odbc.OdbcConnection" /> class with the specified connection string.</summary>
		/// <param name="connectionString">The connection used to open the data source. </param>
		// Token: 0x06002A8B RID: 10891 RVA: 0x00010458 File Offset: 0x0000E658
		public OdbcConnection(string connectionString)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the string used to open a data source.</summary>
		/// <returns>The ODBC driver connection string that includes settings, such as the data source name, needed to establish the initial connection. The default value is an empty string (""). The maximum length is 1024 characters.</returns>
		// Token: 0x17000707 RID: 1799
		// (get) Token: 0x06002A8C RID: 10892 RVA: 0x00051759 File Offset: 0x0004F959
		// (set) Token: 0x06002A8D RID: 10893 RVA: 0x00010458 File Offset: 0x0000E658
		public override string ConnectionString
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the time to wait while trying to establish a connection before terminating the attempt and generating an error.</summary>
		/// <returns>The time in seconds to wait for a connection to open. The default value is 15 seconds.</returns>
		/// <exception cref="T:System.ArgumentException">The value set is less than 0. </exception>
		// Token: 0x17000708 RID: 1800
		// (set) Token: 0x06002A8E RID: 10894 RVA: 0x00010458 File Offset: 0x0000E658
		public new int ConnectionTimeout
		{
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets the name of the current database or the database to be used after a connection is opened.</summary>
		/// <returns>The name of the current database. The default value is an empty string ("") until the connection is opened.</returns>
		// Token: 0x17000709 RID: 1801
		// (get) Token: 0x06002A8F RID: 10895 RVA: 0x00051759 File Offset: 0x0004F959
		public override string Database
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the server name or file name of the data source.</summary>
		/// <returns>The server name or file name of the data source. The default value is an empty string ("") until the connection is opened.</returns>
		// Token: 0x1700070A RID: 1802
		// (get) Token: 0x06002A90 RID: 10896 RVA: 0x00051759 File Offset: 0x0004F959
		public override string DataSource
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the name of the ODBC driver specified for the current connection.</summary>
		/// <returns>The name of the ODBC driver. This typically is the DLL name (for example, Sqlsrv32.dll). The default value is an empty string ("") until the connection is opened.</returns>
		// Token: 0x1700070B RID: 1803
		// (get) Token: 0x06002A91 RID: 10897 RVA: 0x00051759 File Offset: 0x0004F959
		public string Driver
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a string that contains the version of the server to which the client is connected.</summary>
		/// <returns>The version of the connected server.</returns>
		/// <exception cref="T:System.InvalidOperationException">The connection is closed. </exception>
		// Token: 0x1700070C RID: 1804
		// (get) Token: 0x06002A92 RID: 10898 RVA: 0x00051759 File Offset: 0x0004F959
		public override string ServerVersion
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the current state of the connection.</summary>
		/// <returns>A bitwise combination of the <see cref="T:System.Data.ConnectionState" /> values. The default is <see langword="Closed" />.</returns>
		// Token: 0x1700070D RID: 1805
		// (get) Token: 0x06002A93 RID: 10899 RVA: 0x000B72BC File Offset: 0x000B54BC
		public override ConnectionState State
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return ConnectionState.Closed;
			}
		}

		/// <summary>Occurs when the ODBC driver sends a warning or an informational message.</summary>
		// Token: 0x1400002B RID: 43
		// (add) Token: 0x06002A94 RID: 10900 RVA: 0x00010458 File Offset: 0x0000E658
		// (remove) Token: 0x06002A95 RID: 10901 RVA: 0x00010458 File Offset: 0x0000E658
		public event OdbcInfoMessageEventHandler InfoMessage
		{
			add
			{
				ThrowStub.ThrowNotSupportedException();
			}
			remove
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		// Token: 0x06002A96 RID: 10902 RVA: 0x00051759 File Offset: 0x0004F959
		protected override DbTransaction BeginDbTransaction(IsolationLevel isolationLevel)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Changes the current database associated with an open <see cref="T:System.Data.Odbc.OdbcConnection" />.</summary>
		/// <param name="value">The database name. </param>
		/// <exception cref="T:System.ArgumentException">The database name is not valid. </exception>
		/// <exception cref="T:System.InvalidOperationException">The connection is not open. </exception>
		/// <exception cref="T:System.Data.Odbc.OdbcException">Cannot change the database. </exception>
		// Token: 0x06002A97 RID: 10903 RVA: 0x00010458 File Offset: 0x0000E658
		public override void ChangeDatabase(string value)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Closes the connection to the data source. </summary>
		// Token: 0x06002A98 RID: 10904 RVA: 0x00010458 File Offset: 0x0000E658
		public override void Close()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x06002A99 RID: 10905 RVA: 0x00051759 File Offset: 0x0004F959
		protected override DbCommand CreateDbCommand()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Enlists in the specified transaction as a distributed transaction.</summary>
		/// <param name="transaction">A reference to an existing <see cref="T:System.EnterpriseServices.ITransaction" /> in which to enlist.</param>
		// Token: 0x06002A9A RID: 10906 RVA: 0x00010458 File Offset: 0x0000E658
		public void EnlistDistributedTransaction(ITransaction transaction)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Opens a connection to a data source with the property settings specified by the <see cref="P:System.Data.Odbc.OdbcConnection.ConnectionString" />.</summary>
		/// <exception cref="T:System.NotSupportedException">The functionality of this method is unsupported in the base class and must be implemented in a derived class instead. </exception>
		// Token: 0x06002A9B RID: 10907 RVA: 0x00010458 File Offset: 0x0000E658
		public override void Open()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Indicates that the ODBC Driver Manager environment handle can be released when the last underlying connection is released.</summary>
		// Token: 0x06002A9C RID: 10908 RVA: 0x00010458 File Offset: 0x0000E658
		public static void ReleaseObjectPool()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>For a description of this member, see <see cref="M:System.ICloneable.Clone" />.</summary>
		/// <returns>A new <see cref="T:System.Object" /> that is a copy of this instance.</returns>
		// Token: 0x06002A9D RID: 10909 RVA: 0x00051759 File Offset: 0x0004F959
		object ICloneable.Clone()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}
	}
}
