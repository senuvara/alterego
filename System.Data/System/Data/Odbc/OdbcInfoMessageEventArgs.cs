﻿using System;
using Unity;

namespace System.Data.Odbc
{
	/// <summary>Provides data for the <see cref="E:System.Data.Odbc.OdbcConnection.InfoMessage" /> event.</summary>
	// Token: 0x02000374 RID: 884
	public sealed class OdbcInfoMessageEventArgs : EventArgs
	{
		// Token: 0x06002AA2 RID: 10914 RVA: 0x00010458 File Offset: 0x0000E658
		internal OdbcInfoMessageEventArgs()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the collection of warnings sent from the data source.</summary>
		/// <returns>The collection of warnings sent from the data source.</returns>
		// Token: 0x1700070E RID: 1806
		// (get) Token: 0x06002AA3 RID: 10915 RVA: 0x00051759 File Offset: 0x0004F959
		public OdbcErrorCollection Errors
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the full text of the error sent from the database.</summary>
		/// <returns>The full text of the error.</returns>
		// Token: 0x1700070F RID: 1807
		// (get) Token: 0x06002AA4 RID: 10916 RVA: 0x00051759 File Offset: 0x0004F959
		public string Message
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}
	}
}
