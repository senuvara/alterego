﻿using System;

namespace System.Data.Odbc
{
	/// <summary>Provides static values that are used for the column names in the <see cref="T:System.Data.Odbc.OdbcMetaDataCollectionNames" /> objects contained in the <see cref="T:System.Data.DataTable" />. The <see cref="T:System.Data.DataTable" /> is created by the GetSchema method.</summary>
	// Token: 0x02000383 RID: 899
	public static class OdbcMetaDataColumnNames
	{
		/// <summary>Used by the GetSchema method to create the BooleanFalseLiteral column.</summary>
		// Token: 0x04001979 RID: 6521
		public static readonly string BooleanFalseLiteral;

		/// <summary>Used by the GetSchema method to create the BooleanTrueLiteral column.</summary>
		// Token: 0x0400197A RID: 6522
		public static readonly string BooleanTrueLiteral;

		/// <summary>Used by the GetSchema method to create the SQLType column. </summary>
		// Token: 0x0400197B RID: 6523
		public static readonly string SQLType;
	}
}
