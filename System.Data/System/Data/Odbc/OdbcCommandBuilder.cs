﻿using System;
using System.Data.Common;
using Unity;

namespace System.Data.Odbc
{
	/// <summary>Automatically generates single-table commands that are used to reconcile changes made to a <see cref="T:System.Data.DataSet" /> with the associated data source. This class cannot be inherited.</summary>
	// Token: 0x02000378 RID: 888
	public sealed class OdbcCommandBuilder : DbCommandBuilder
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Data.Odbc.OdbcCommandBuilder" /> class.</summary>
		// Token: 0x06002AB7 RID: 10935 RVA: 0x00010458 File Offset: 0x0000E658
		public OdbcCommandBuilder()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Data.Odbc.OdbcCommandBuilder" /> class with the associated <see cref="T:System.Data.Odbc.OdbcDataAdapter" /> object.</summary>
		/// <param name="adapter">An <see cref="T:System.Data.Odbc.OdbcDataAdapter" /> object to associate with this <see cref="T:System.Data.Odbc.OdbcCommandBuilder" />.</param>
		// Token: 0x06002AB8 RID: 10936 RVA: 0x00010458 File Offset: 0x0000E658
		public OdbcCommandBuilder(OdbcDataAdapter adapter)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets an <see cref="T:System.Data.Odbc.OdbcDataAdapter" /> object for which this <see cref="T:System.Data.Odbc.OdbcCommandBuilder" /> object will generate SQL statements.</summary>
		/// <returns>An <see cref="T:System.Data.Odbc.OdbcDataAdapter" /> object that is associated with this <see cref="T:System.Data.Odbc.OdbcCommandBuilder" />.</returns>
		// Token: 0x17000718 RID: 1816
		// (set) Token: 0x06002AB9 RID: 10937 RVA: 0x00010458 File Offset: 0x0000E658
		public new OdbcDataAdapter DataAdapter
		{
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		// Token: 0x06002ABA RID: 10938 RVA: 0x00010458 File Offset: 0x0000E658
		protected override void ApplyParameterInfo(DbParameter parameter, DataRow datarow, StatementType statementType, bool whereClause)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Retrieves parameter information from the stored procedure specified in the <see cref="T:System.Data.Odbc.OdbcCommand" /> and populates the <see cref="P:System.Data.Odbc.OdbcCommand.Parameters" /> collection of the specified <see cref="T:System.Data.Odbc.OdbcCommand" /> object.</summary>
		/// <param name="command">The <see cref="T:System.Data.Odbc.OdbcCommand" /> referencing the stored procedure from which the parameter information is to be derived. The derived parameters are added to the <see cref="P:System.Data.Odbc.OdbcCommand.Parameters" /> collection of the <see cref="T:System.Data.Odbc.OdbcCommand" />. </param>
		/// <exception cref="T:System.InvalidOperationException">The underlying ODBC driver does not support returning stored procedure parameter information, or the command text is not a valid stored procedure name, or the <see cref="T:System.Data.CommandType" /> specified was not <see langword="CommandType.StoredProcedure" />. </exception>
		// Token: 0x06002ABB RID: 10939 RVA: 0x00010458 File Offset: 0x0000E658
		public static void DeriveParameters(OdbcCommand command)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x06002ABC RID: 10940 RVA: 0x00051759 File Offset: 0x0004F959
		protected override string GetParameterName(int parameterOrdinal)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		// Token: 0x06002ABD RID: 10941 RVA: 0x00051759 File Offset: 0x0004F959
		protected override string GetParameterName(string parameterName)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		// Token: 0x06002ABE RID: 10942 RVA: 0x00051759 File Offset: 0x0004F959
		protected override string GetParameterPlaceholder(int parameterOrdinal)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Given an unquoted identifier in the correct catalog case, returns the correct quoted form of that identifier. This includes correctly escaping any embedded quotes in the identifier.</summary>
		/// <param name="unquotedIdentifier">The original unquoted identifier.</param>
		/// <param name="connection">When a connection is passed, causes the managed wrapper to get the quote character from the ODBC driver, calling SQLGetInfo(SQL_IDENTIFIER_QUOTE_CHAR). When no connection is passed, the string is quoted using values from <see cref="P:System.Data.Common.DbCommandBuilder.QuotePrefix" /> and <see cref="P:System.Data.Common.DbCommandBuilder.QuoteSuffix" />.</param>
		/// <returns>The quoted version of the identifier. Embedded quotes within the identifier are correctly escaped.</returns>
		// Token: 0x06002ABF RID: 10943 RVA: 0x00051759 File Offset: 0x0004F959
		public string QuoteIdentifier(string unquotedIdentifier, OdbcConnection connection)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		// Token: 0x06002AC0 RID: 10944 RVA: 0x00010458 File Offset: 0x0000E658
		protected override void SetRowUpdatingHandler(DbDataAdapter adapter)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Given a quoted identifier, returns the correct unquoted form of that identifier, including correctly unescaping any embedded quotes in the identifier.</summary>
		/// <param name="quotedIdentifier">The identifier that will have its embedded quotes removed.</param>
		/// <param name="connection">The <see cref="T:System.Data.Odbc.OdbcConnection" />.</param>
		/// <returns>The unquoted identifier, with embedded quotes correctly unescaped.</returns>
		// Token: 0x06002AC1 RID: 10945 RVA: 0x00051759 File Offset: 0x0004F959
		public string UnquoteIdentifier(string quotedIdentifier, OdbcConnection connection)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}
	}
}
