﻿using System;
using System.Data.Common;
using System.Security;
using System.Security.Permissions;
using Unity;

namespace System.Data.Odbc
{
	/// <summary>Associates a security action with a custom security attribute.</summary>
	// Token: 0x02000388 RID: 904
	[AttributeUsage(AttributeTargets.Assembly | AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Constructor | AttributeTargets.Method, AllowMultiple = true, Inherited = false)]
	[Serializable]
	public sealed class OdbcPermissionAttribute : DBDataPermissionAttribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Data.Odbc.OdbcPermissionAttribute" /> class with one of the <see cref="T:System.Security.Permissions.SecurityAction" /> values.</summary>
		/// <param name="action">One of the <see cref="T:System.Security.Permissions.SecurityAction" /> values representing an action that can be performed by using declarative security. </param>
		// Token: 0x06002B45 RID: 11077 RVA: 0x00005E03 File Offset: 0x00004003
		public OdbcPermissionAttribute(SecurityAction action)
		{
		}

		/// <summary>Returns an <see cref="T:System.Data.Odbc.OdbcPermission" /> object that is configured according to the attribute properties.</summary>
		/// <returns>An <see cref="T:System.Data.Odbc.OdbcPermission" /> object.</returns>
		// Token: 0x06002B46 RID: 11078 RVA: 0x00051759 File Offset: 0x0004F959
		public override IPermission CreatePermission()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}
	}
}
