﻿using System;
using System.Data.Common;
using System.Globalization;
using System.Numerics;

namespace System.Data
{
	// Token: 0x02000060 RID: 96
	internal sealed class AutoIncrementInt64 : AutoIncrementValue
	{
		// Token: 0x170000E2 RID: 226
		// (get) Token: 0x06000399 RID: 921 RVA: 0x00012A41 File Offset: 0x00010C41
		// (set) Token: 0x0600039A RID: 922 RVA: 0x00012A4E File Offset: 0x00010C4E
		internal override object Current
		{
			get
			{
				return this._current;
			}
			set
			{
				this._current = (long)value;
			}
		}

		// Token: 0x170000E3 RID: 227
		// (get) Token: 0x0600039B RID: 923 RVA: 0x00012A5C File Offset: 0x00010C5C
		internal override Type DataType
		{
			get
			{
				return typeof(long);
			}
		}

		// Token: 0x170000E4 RID: 228
		// (get) Token: 0x0600039C RID: 924 RVA: 0x00012A68 File Offset: 0x00010C68
		// (set) Token: 0x0600039D RID: 925 RVA: 0x00012A70 File Offset: 0x00010C70
		internal override long Seed
		{
			get
			{
				return this._seed;
			}
			set
			{
				if (this._current == this._seed || this.BoundaryCheck(value))
				{
					this._current = value;
				}
				this._seed = value;
			}
		}

		// Token: 0x170000E5 RID: 229
		// (get) Token: 0x0600039E RID: 926 RVA: 0x00012A9C File Offset: 0x00010C9C
		// (set) Token: 0x0600039F RID: 927 RVA: 0x00012AA4 File Offset: 0x00010CA4
		internal override long Step
		{
			get
			{
				return this._step;
			}
			set
			{
				if (value == 0L)
				{
					throw ExceptionBuilder.AutoIncrementSeed();
				}
				if (this._step != value)
				{
					if (this._current != this.Seed)
					{
						this._current = this._current - this._step + value;
					}
					this._step = value;
				}
			}
		}

		// Token: 0x060003A0 RID: 928 RVA: 0x00012AE2 File Offset: 0x00010CE2
		internal override void MoveAfter()
		{
			this._current += this._step;
		}

		// Token: 0x060003A1 RID: 929 RVA: 0x00012AF7 File Offset: 0x00010CF7
		internal override void SetCurrent(object value, IFormatProvider formatProvider)
		{
			this._current = Convert.ToInt64(value, formatProvider);
		}

		// Token: 0x060003A2 RID: 930 RVA: 0x00012B08 File Offset: 0x00010D08
		internal override void SetCurrentAndIncrement(object value)
		{
			long num = (long)SqlConvert.ChangeType2(value, StorageType.Int64, typeof(long), CultureInfo.InvariantCulture);
			if (this.BoundaryCheck(num))
			{
				this._current = num + this._step;
			}
		}

		// Token: 0x060003A3 RID: 931 RVA: 0x00012B4E File Offset: 0x00010D4E
		private bool BoundaryCheck(BigInteger value)
		{
			return (this._step < 0L && value <= this._current) || (0L < this._step && this._current <= value);
		}

		// Token: 0x060003A4 RID: 932 RVA: 0x00012B82 File Offset: 0x00010D82
		public AutoIncrementInt64()
		{
		}

		// Token: 0x04000539 RID: 1337
		private long _current;

		// Token: 0x0400053A RID: 1338
		private long _seed;

		// Token: 0x0400053B RID: 1339
		private long _step = 1L;
	}
}
