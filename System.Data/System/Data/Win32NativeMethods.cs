﻿using System;

namespace System.Data
{
	// Token: 0x02000116 RID: 278
	internal static class Win32NativeMethods
	{
		// Token: 0x06000E4F RID: 3663 RVA: 0x0004B911 File Offset: 0x00049B11
		internal static bool IsTokenRestrictedWrapper(IntPtr token)
		{
			throw new PlatformNotSupportedException("Win32NativeMethods.IsTokenRestrictedWrapper is not supported on non-Windows platforms");
		}
	}
}
