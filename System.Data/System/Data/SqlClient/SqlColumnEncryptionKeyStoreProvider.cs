﻿using System;
using Unity;

namespace System.Data.SqlClient
{
	/// <summary>Base class for all key store providers. A custom provider must derive from this class and override its member functions and then register it using SqlConnection.RegisterColumnEncryptionKeyStoreProviders(). For details see, Always Encrypted.</summary>
	// Token: 0x02000338 RID: 824
	public abstract class SqlColumnEncryptionKeyStoreProvider
	{
		/// <summary>Initializes a new instance of the SqlColumnEncryptionKeyStoreProviderClass.</summary>
		// Token: 0x06002940 RID: 10560 RVA: 0x00010458 File Offset: 0x0000E658
		protected SqlColumnEncryptionKeyStoreProvider()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Decrypts the specified encrypted value of a column encryption key. The encrypted value is expected to be encrypted using the column master key with the specified key path and using the specified algorithm.</summary>
		/// <param name="masterKeyPath">The master key path.</param>
		/// <param name="encryptionAlgorithm">The encryption algorithm.</param>
		/// <param name="encryptedColumnEncryptionKey">The encrypted column encryption key.</param>
		/// <returns>Returns <see cref="T:System.Byte" />. The decrypted column encryption key.</returns>
		// Token: 0x06002941 RID: 10561
		public abstract byte[] DecryptColumnEncryptionKey(string masterKeyPath, string encryptionAlgorithm, byte[] encryptedColumnEncryptionKey);

		/// <summary>Encrypts a column encryption key using the column master key with the specified key path and using the specified algorithm.</summary>
		/// <param name="masterKeyPath">The master key path.</param>
		/// <param name="encryptionAlgorithm">The encryption algorithm.</param>
		/// <param name="columnEncryptionKey">The encrypted column encryption key.</param>
		/// <returns>Returns <see cref="T:System.Byte" />. The encrypted column encryption key.</returns>
		// Token: 0x06002942 RID: 10562
		public abstract byte[] EncryptColumnEncryptionKey(string masterKeyPath, string encryptionAlgorithm, byte[] columnEncryptionKey);
	}
}
