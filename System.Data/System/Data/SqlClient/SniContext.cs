﻿using System;

namespace System.Data.SqlClient
{
	// Token: 0x020001E1 RID: 481
	internal enum SniContext
	{
		// Token: 0x0400104A RID: 4170
		Undefined,
		// Token: 0x0400104B RID: 4171
		Snix_Connect,
		// Token: 0x0400104C RID: 4172
		Snix_PreLoginBeforeSuccessfulWrite,
		// Token: 0x0400104D RID: 4173
		Snix_PreLogin,
		// Token: 0x0400104E RID: 4174
		Snix_LoginSspi,
		// Token: 0x0400104F RID: 4175
		Snix_ProcessSspi,
		// Token: 0x04001050 RID: 4176
		Snix_Login,
		// Token: 0x04001051 RID: 4177
		Snix_EnableMars,
		// Token: 0x04001052 RID: 4178
		Snix_AutoEnlist,
		// Token: 0x04001053 RID: 4179
		Snix_GetMarsSession,
		// Token: 0x04001054 RID: 4180
		Snix_Execute,
		// Token: 0x04001055 RID: 4181
		Snix_Read,
		// Token: 0x04001056 RID: 4182
		Snix_Close,
		// Token: 0x04001057 RID: 4183
		Snix_SendRows
	}
}
