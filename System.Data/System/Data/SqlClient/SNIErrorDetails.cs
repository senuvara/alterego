﻿using System;

namespace System.Data.SqlClient
{
	// Token: 0x020001F4 RID: 500
	internal struct SNIErrorDetails
	{
		// Token: 0x040010CB RID: 4299
		public string errorMessage;

		// Token: 0x040010CC RID: 4300
		public uint nativeError;

		// Token: 0x040010CD RID: 4301
		public uint sniErrorNumber;

		// Token: 0x040010CE RID: 4302
		public int provider;

		// Token: 0x040010CF RID: 4303
		public uint lineNumber;

		// Token: 0x040010D0 RID: 4304
		public string function;

		// Token: 0x040010D1 RID: 4305
		public Exception exception;
	}
}
