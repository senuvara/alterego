﻿using System;
using System.Text;

namespace System.Data.SqlClient
{
	// Token: 0x020001D8 RID: 472
	internal static class SqlServerEscapeHelper
	{
		// Token: 0x060015CD RID: 5581 RVA: 0x0006CC69 File Offset: 0x0006AE69
		internal static string EscapeIdentifier(string name)
		{
			return "[" + name.Replace("]", "]]") + "]";
		}

		// Token: 0x060015CE RID: 5582 RVA: 0x0006CC8A File Offset: 0x0006AE8A
		internal static void EscapeIdentifier(StringBuilder builder, string name)
		{
			builder.Append("[");
			builder.Append(name.Replace("]", "]]"));
			builder.Append("]");
		}

		// Token: 0x060015CF RID: 5583 RVA: 0x0006CCBB File Offset: 0x0006AEBB
		internal static string EscapeStringAsLiteral(string input)
		{
			return input.Replace("'", "''");
		}

		// Token: 0x060015D0 RID: 5584 RVA: 0x0006CCCD File Offset: 0x0006AECD
		internal static string MakeStringLiteral(string input)
		{
			if (string.IsNullOrEmpty(input))
			{
				return "''";
			}
			return "'" + SqlServerEscapeHelper.EscapeStringAsLiteral(input) + "'";
		}
	}
}
