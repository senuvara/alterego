﻿using System;

namespace System.Data.SqlClient
{
	// Token: 0x020001A8 RID: 424
	internal class SessionStateRecord
	{
		// Token: 0x0600137F RID: 4991 RVA: 0x00005C14 File Offset: 0x00003E14
		public SessionStateRecord()
		{
		}

		// Token: 0x04000DDD RID: 3549
		internal bool _recoverable;

		// Token: 0x04000DDE RID: 3550
		internal uint _version;

		// Token: 0x04000DDF RID: 3551
		internal int _dataLength;

		// Token: 0x04000DE0 RID: 3552
		internal byte[] _data;
	}
}
