﻿using System;
using System.Data.Common;

namespace System.Data.SqlClient
{
	/// <summary>Defines the mapping between a column in a <see cref="T:System.Data.SqlClient.SqlBulkCopy" /> instance's data source and a column in the instance's destination table. </summary>
	// Token: 0x02000145 RID: 325
	public sealed class SqlBulkCopyColumnMapping
	{
		/// <summary>Name of the column being mapped in the destination database table.</summary>
		/// <returns>The string value of the <see cref="P:System.Data.SqlClient.SqlBulkCopyColumnMapping.DestinationColumn" /> property.</returns>
		// Token: 0x170002A6 RID: 678
		// (get) Token: 0x06000F4C RID: 3916 RVA: 0x00050E6E File Offset: 0x0004F06E
		// (set) Token: 0x06000F4D RID: 3917 RVA: 0x00050E84 File Offset: 0x0004F084
		public string DestinationColumn
		{
			get
			{
				if (this._destinationColumnName != null)
				{
					return this._destinationColumnName;
				}
				return string.Empty;
			}
			set
			{
				this._destinationColumnOrdinal = (this._internalDestinationColumnOrdinal = -1);
				this._destinationColumnName = value;
			}
		}

		/// <summary>Ordinal value of the destination column within the destination table.</summary>
		/// <returns>The integer value of the <see cref="P:System.Data.SqlClient.SqlBulkCopyColumnMapping.DestinationOrdinal" /> property, or -1 if the property has not been set.</returns>
		// Token: 0x170002A7 RID: 679
		// (get) Token: 0x06000F4E RID: 3918 RVA: 0x00050EA8 File Offset: 0x0004F0A8
		// (set) Token: 0x06000F4F RID: 3919 RVA: 0x00050EB0 File Offset: 0x0004F0B0
		public int DestinationOrdinal
		{
			get
			{
				return this._destinationColumnOrdinal;
			}
			set
			{
				if (value >= 0)
				{
					this._destinationColumnName = null;
					this._internalDestinationColumnOrdinal = value;
					this._destinationColumnOrdinal = value;
					return;
				}
				throw ADP.IndexOutOfRange(value);
			}
		}

		/// <summary>Name of the column being mapped in the data source.</summary>
		/// <returns>The string value of the <see cref="P:System.Data.SqlClient.SqlBulkCopyColumnMapping.SourceColumn" /> property.</returns>
		// Token: 0x170002A8 RID: 680
		// (get) Token: 0x06000F50 RID: 3920 RVA: 0x00050EDF File Offset: 0x0004F0DF
		// (set) Token: 0x06000F51 RID: 3921 RVA: 0x00050EF8 File Offset: 0x0004F0F8
		public string SourceColumn
		{
			get
			{
				if (this._sourceColumnName != null)
				{
					return this._sourceColumnName;
				}
				return string.Empty;
			}
			set
			{
				this._sourceColumnOrdinal = (this._internalSourceColumnOrdinal = -1);
				this._sourceColumnName = value;
			}
		}

		/// <summary>The ordinal position of the source column within the data source.</summary>
		/// <returns>The integer value of the <see cref="P:System.Data.SqlClient.SqlBulkCopyColumnMapping.SourceOrdinal" /> property.</returns>
		// Token: 0x170002A9 RID: 681
		// (get) Token: 0x06000F52 RID: 3922 RVA: 0x00050F1C File Offset: 0x0004F11C
		// (set) Token: 0x06000F53 RID: 3923 RVA: 0x00050F24 File Offset: 0x0004F124
		public int SourceOrdinal
		{
			get
			{
				return this._sourceColumnOrdinal;
			}
			set
			{
				if (value >= 0)
				{
					this._sourceColumnName = null;
					this._internalSourceColumnOrdinal = value;
					this._sourceColumnOrdinal = value;
					return;
				}
				throw ADP.IndexOutOfRange(value);
			}
		}

		/// <summary>Default constructor that initializes a new <see cref="T:System.Data.SqlClient.SqlBulkCopyColumnMapping" /> object.</summary>
		// Token: 0x06000F54 RID: 3924 RVA: 0x00050F53 File Offset: 0x0004F153
		public SqlBulkCopyColumnMapping()
		{
			this._internalSourceColumnOrdinal = -1;
		}

		/// <summary>Creates a new column mapping, using column names to refer to source and destination columns.</summary>
		/// <param name="sourceColumn">The name of the source column within the data source.</param>
		/// <param name="destinationColumn">The name of the destination column within the destination table.</param>
		// Token: 0x06000F55 RID: 3925 RVA: 0x00050F62 File Offset: 0x0004F162
		public SqlBulkCopyColumnMapping(string sourceColumn, string destinationColumn)
		{
			this.SourceColumn = sourceColumn;
			this.DestinationColumn = destinationColumn;
		}

		/// <summary>Creates a new column mapping, using a column ordinal to refer to the source column and a column name for the target column.</summary>
		/// <param name="sourceColumnOrdinal">The ordinal position of the source column within the data source.</param>
		/// <param name="destinationColumn">The name of the destination column within the destination table.</param>
		// Token: 0x06000F56 RID: 3926 RVA: 0x00050F78 File Offset: 0x0004F178
		public SqlBulkCopyColumnMapping(int sourceColumnOrdinal, string destinationColumn)
		{
			this.SourceOrdinal = sourceColumnOrdinal;
			this.DestinationColumn = destinationColumn;
		}

		/// <summary>Creates a new column mapping, using a column name to refer to the source column and a column ordinal for the target column.</summary>
		/// <param name="sourceColumn">The name of the source column within the data source.</param>
		/// <param name="destinationOrdinal">The ordinal position of the destination column within the destination table.</param>
		// Token: 0x06000F57 RID: 3927 RVA: 0x00050F8E File Offset: 0x0004F18E
		public SqlBulkCopyColumnMapping(string sourceColumn, int destinationOrdinal)
		{
			this.SourceColumn = sourceColumn;
			this.DestinationOrdinal = destinationOrdinal;
		}

		/// <summary>Creates a new column mapping, using column ordinals to refer to source and destination columns.</summary>
		/// <param name="sourceColumnOrdinal">The ordinal position of the source column within the data source.</param>
		/// <param name="destinationOrdinal">The ordinal position of the destination column within the destination table.</param>
		// Token: 0x06000F58 RID: 3928 RVA: 0x00050FA4 File Offset: 0x0004F1A4
		public SqlBulkCopyColumnMapping(int sourceColumnOrdinal, int destinationOrdinal)
		{
			this.SourceOrdinal = sourceColumnOrdinal;
			this.DestinationOrdinal = destinationOrdinal;
		}

		// Token: 0x04000AE7 RID: 2791
		internal string _destinationColumnName;

		// Token: 0x04000AE8 RID: 2792
		internal int _destinationColumnOrdinal;

		// Token: 0x04000AE9 RID: 2793
		internal string _sourceColumnName;

		// Token: 0x04000AEA RID: 2794
		internal int _sourceColumnOrdinal;

		// Token: 0x04000AEB RID: 2795
		internal int _internalDestinationColumnOrdinal;

		// Token: 0x04000AEC RID: 2796
		internal int _internalSourceColumnOrdinal;
	}
}
