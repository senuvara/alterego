﻿using System;

namespace System.Data.SqlClient
{
	/// <summary>Describes the different SQL authentication methods that can be used by a client connecting to Azure SQL Database. For details, see Connecting to SQL Database By Using Azure Active Directory Authentication.</summary>
	// Token: 0x0200021E RID: 542
	public enum SqlAuthenticationMethod
	{
		/// <summary>The authentication method is not specified.</summary>
		// Token: 0x040011F2 RID: 4594
		NotSpecified,
		/// <summary>The authentication method is Sql Password.</summary>
		// Token: 0x040011F3 RID: 4595
		SqlPassword,
		/// <summary>The authentication method uses Active Directory Password. Use Active Directory Password to connect to a SQL Database using an Azure AD principal name and password. </summary>
		// Token: 0x040011F4 RID: 4596
		ActiveDirectoryPassword,
		/// <summary>The authentication method uses Active Directory Integrated. Use Active Directory Integrated to connect to a SQL Database using integrated Windows authentication.</summary>
		// Token: 0x040011F5 RID: 4597
		ActiveDirectoryIntegrated
	}
}
