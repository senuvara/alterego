﻿using System;

namespace System.Data.SqlClient
{
	// Token: 0x020001F5 RID: 501
	internal enum CallbackType
	{
		// Token: 0x040010D3 RID: 4307
		Read,
		// Token: 0x040010D4 RID: 4308
		Write
	}
}
