﻿using System;
using Unity;

namespace System.Data.SqlClient
{
	/// <summary>The CMK Store provider implementation for using  Microsoft CAPI based Cryptographic Service Providers (CSP) with Always Encrypted.</summary>
	// Token: 0x02000356 RID: 854
	public class SqlColumnEncryptionCspProvider : SqlColumnEncryptionKeyStoreProvider
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Data.SqlClient.SqlColumnEncryptionCspProvider" /> class.</summary>
		// Token: 0x0600297A RID: 10618 RVA: 0x00010458 File Offset: 0x0000E658
		public SqlColumnEncryptionCspProvider()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Decrypts the given encrypted value using an asymmetric key specified by the key path and algorithm. The key path will be in the format of [ProviderName]/KeyIdentifier and should be an asymmetric key stored in the specified CSP provider. The valid algorithm used to encrypt/decrypt the CEK is 'RSA_OAEP'.</summary>
		/// <param name="masterKeyPath">The master key path.</param>
		/// <param name="encryptionAlgorithm">The encryption algorithm. </param>
		/// <param name="encryptedColumnEncryptionKey">The encrypted column encryption key.</param>
		/// <returns>The decrypted column encryption key.</returns>
		// Token: 0x0600297B RID: 10619 RVA: 0x00051759 File Offset: 0x0004F959
		public override byte[] DecryptColumnEncryptionKey(string masterKeyPath, string encryptionAlgorithm, byte[] encryptedColumnEncryptionKey)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Encrypts the given plain text column encryption key using an asymmetric key specified by the key path and the specified algorithm. The key path will be in the format of [ProviderName]/KeyIdentifier and should be an asymmetric key stored in the specified CSP provider. The valid algorithm used to encrypt/decrypt the CEK is 'RSA_OAEP'.</summary>
		/// <param name="masterKeyPath">The master key path.</param>
		/// <param name="encryptionAlgorithm">The encryption algorithm. </param>
		/// <param name="columnEncryptionKey">The encrypted column encryption key.</param>
		/// <returns>The encrypted column encryption key.</returns>
		// Token: 0x0600297C RID: 10620 RVA: 0x00051759 File Offset: 0x0004F959
		public override byte[] EncryptColumnEncryptionKey(string masterKeyPath, string encryptionAlgorithm, byte[] columnEncryptionKey)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>A constant string for the provider name 'MSSQL_CSP_PROVIDER'.</summary>
		/// <returns>The provider name.</returns>
		// Token: 0x0400191D RID: 6429
		public const string ProviderName = "MSSQL_CSP_PROVIDER";
	}
}
