﻿using System;
using System.Data.Common;
using System.Data.ProviderBase;
using System.IO;
using System.Reflection;

namespace System.Data.SqlClient
{
	// Token: 0x02000170 RID: 368
	internal sealed class SqlConnectionFactory : DbConnectionFactory
	{
		// Token: 0x0600110C RID: 4364 RVA: 0x00058150 File Offset: 0x00056350
		private SqlConnectionFactory()
		{
		}

		// Token: 0x170002FC RID: 764
		// (get) Token: 0x0600110D RID: 4365 RVA: 0x00056B70 File Offset: 0x00054D70
		public override DbProviderFactory ProviderFactory
		{
			get
			{
				return SqlClientFactory.Instance;
			}
		}

		// Token: 0x0600110E RID: 4366 RVA: 0x00058158 File Offset: 0x00056358
		protected override DbConnectionInternal CreateConnection(DbConnectionOptions options, DbConnectionPoolKey poolKey, object poolGroupProviderInfo, DbConnectionPool pool, DbConnection owningConnection)
		{
			return this.CreateConnection(options, poolKey, poolGroupProviderInfo, pool, owningConnection, null);
		}

		// Token: 0x0600110F RID: 4367 RVA: 0x00058168 File Offset: 0x00056368
		protected override DbConnectionInternal CreateConnection(DbConnectionOptions options, DbConnectionPoolKey poolKey, object poolGroupProviderInfo, DbConnectionPool pool, DbConnection owningConnection, DbConnectionOptions userOptions)
		{
			SqlConnectionString sqlConnectionString = (SqlConnectionString)options;
			SqlConnectionPoolKey sqlConnectionPoolKey = (SqlConnectionPoolKey)poolKey;
			SessionData reconnectSessionData = null;
			SqlConnection sqlConnection = (SqlConnection)owningConnection;
			bool applyTransientFaultHandling = sqlConnection != null && sqlConnection._applyTransientFaultHandling;
			SqlConnectionString userConnectionOptions = null;
			if (userOptions != null)
			{
				userConnectionOptions = (SqlConnectionString)userOptions;
			}
			else if (sqlConnection != null)
			{
				userConnectionOptions = (SqlConnectionString)sqlConnection.UserConnectionOptions;
			}
			if (sqlConnection != null)
			{
				reconnectSessionData = sqlConnection._recoverySessionData;
			}
			bool redirectedUserInstance = false;
			DbConnectionPoolIdentity identity = null;
			if (sqlConnectionString.IntegratedSecurity)
			{
				if (pool != null)
				{
					identity = pool.Identity;
				}
				else
				{
					identity = DbConnectionPoolIdentity.GetCurrent();
				}
			}
			if (sqlConnectionString.UserInstance)
			{
				redirectedUserInstance = true;
				string instanceName;
				if (pool == null || (pool != null && pool.Count <= 0))
				{
					SqlInternalConnectionTds sqlInternalConnectionTds = null;
					try
					{
						SqlConnectionString connectionOptions = new SqlConnectionString(sqlConnectionString, sqlConnectionString.DataSource, true, new bool?(false));
						sqlInternalConnectionTds = new SqlInternalConnectionTds(identity, connectionOptions, null, false, null, null, applyTransientFaultHandling);
						instanceName = sqlInternalConnectionTds.InstanceName;
						if (!instanceName.StartsWith("\\\\.\\", StringComparison.Ordinal))
						{
							throw SQL.NonLocalSSEInstance();
						}
						if (pool != null)
						{
							((SqlConnectionPoolProviderInfo)pool.ProviderInfo).InstanceName = instanceName;
						}
						goto IL_113;
					}
					finally
					{
						if (sqlInternalConnectionTds != null)
						{
							sqlInternalConnectionTds.Dispose();
						}
					}
				}
				instanceName = ((SqlConnectionPoolProviderInfo)pool.ProviderInfo).InstanceName;
				IL_113:
				sqlConnectionString = new SqlConnectionString(sqlConnectionString, instanceName, false, null);
				poolGroupProviderInfo = null;
			}
			return new SqlInternalConnectionTds(identity, sqlConnectionString, poolGroupProviderInfo, redirectedUserInstance, userConnectionOptions, reconnectSessionData, applyTransientFaultHandling);
		}

		// Token: 0x06001110 RID: 4368 RVA: 0x000582C0 File Offset: 0x000564C0
		protected override DbConnectionOptions CreateConnectionOptions(string connectionString, DbConnectionOptions previous)
		{
			return new SqlConnectionString(connectionString);
		}

		// Token: 0x06001111 RID: 4369 RVA: 0x000582C8 File Offset: 0x000564C8
		internal override DbConnectionPoolProviderInfo CreateConnectionPoolProviderInfo(DbConnectionOptions connectionOptions)
		{
			DbConnectionPoolProviderInfo result = null;
			if (((SqlConnectionString)connectionOptions).UserInstance)
			{
				result = new SqlConnectionPoolProviderInfo();
			}
			return result;
		}

		// Token: 0x06001112 RID: 4370 RVA: 0x000582EC File Offset: 0x000564EC
		protected override DbConnectionPoolGroupOptions CreateConnectionPoolGroupOptions(DbConnectionOptions connectionOptions)
		{
			SqlConnectionString sqlConnectionString = (SqlConnectionString)connectionOptions;
			DbConnectionPoolGroupOptions result = null;
			if (sqlConnectionString.Pooling)
			{
				int num = sqlConnectionString.ConnectTimeout;
				if (0 < num && num < 2147483)
				{
					num *= 1000;
				}
				else if (num >= 2147483)
				{
					num = int.MaxValue;
				}
				result = new DbConnectionPoolGroupOptions(sqlConnectionString.IntegratedSecurity, sqlConnectionString.MinPoolSize, sqlConnectionString.MaxPoolSize, num, sqlConnectionString.LoadBalanceTimeout, sqlConnectionString.Enlist);
			}
			return result;
		}

		// Token: 0x06001113 RID: 4371 RVA: 0x0005835B File Offset: 0x0005655B
		internal override DbConnectionPoolGroupProviderInfo CreateConnectionPoolGroupProviderInfo(DbConnectionOptions connectionOptions)
		{
			return new SqlConnectionPoolGroupProviderInfo((SqlConnectionString)connectionOptions);
		}

		// Token: 0x06001114 RID: 4372 RVA: 0x00058368 File Offset: 0x00056568
		internal static SqlConnectionString FindSqlConnectionOptions(SqlConnectionPoolKey key)
		{
			SqlConnectionString sqlConnectionString = (SqlConnectionString)SqlConnectionFactory.SingletonInstance.FindConnectionOptions(key);
			if (sqlConnectionString == null)
			{
				sqlConnectionString = new SqlConnectionString(key.ConnectionString);
			}
			if (sqlConnectionString.IsEmpty)
			{
				throw ADP.NoConnectionString();
			}
			return sqlConnectionString;
		}

		// Token: 0x06001115 RID: 4373 RVA: 0x000583A4 File Offset: 0x000565A4
		internal override DbConnectionPoolGroup GetConnectionPoolGroup(DbConnection connection)
		{
			SqlConnection sqlConnection = connection as SqlConnection;
			if (sqlConnection != null)
			{
				return sqlConnection.PoolGroup;
			}
			return null;
		}

		// Token: 0x06001116 RID: 4374 RVA: 0x000583C4 File Offset: 0x000565C4
		internal override DbConnectionInternal GetInnerConnection(DbConnection connection)
		{
			SqlConnection sqlConnection = connection as SqlConnection;
			if (sqlConnection != null)
			{
				return sqlConnection.InnerConnection;
			}
			return null;
		}

		// Token: 0x06001117 RID: 4375 RVA: 0x000583E4 File Offset: 0x000565E4
		internal override void PermissionDemand(DbConnection outerConnection)
		{
			SqlConnection sqlConnection = outerConnection as SqlConnection;
			if (sqlConnection != null)
			{
				sqlConnection.PermissionDemand();
			}
		}

		// Token: 0x06001118 RID: 4376 RVA: 0x00058404 File Offset: 0x00056604
		internal override void SetConnectionPoolGroup(DbConnection outerConnection, DbConnectionPoolGroup poolGroup)
		{
			SqlConnection sqlConnection = outerConnection as SqlConnection;
			if (sqlConnection != null)
			{
				sqlConnection.PoolGroup = poolGroup;
			}
		}

		// Token: 0x06001119 RID: 4377 RVA: 0x00058424 File Offset: 0x00056624
		internal override void SetInnerConnectionEvent(DbConnection owningObject, DbConnectionInternal to)
		{
			SqlConnection sqlConnection = owningObject as SqlConnection;
			if (sqlConnection != null)
			{
				sqlConnection.SetInnerConnectionEvent(to);
			}
		}

		// Token: 0x0600111A RID: 4378 RVA: 0x00058444 File Offset: 0x00056644
		internal override bool SetInnerConnectionFrom(DbConnection owningObject, DbConnectionInternal to, DbConnectionInternal from)
		{
			SqlConnection sqlConnection = owningObject as SqlConnection;
			return sqlConnection != null && sqlConnection.SetInnerConnectionFrom(to, from);
		}

		// Token: 0x0600111B RID: 4379 RVA: 0x00058468 File Offset: 0x00056668
		internal override void SetInnerConnectionTo(DbConnection owningObject, DbConnectionInternal to)
		{
			SqlConnection sqlConnection = owningObject as SqlConnection;
			if (sqlConnection != null)
			{
				sqlConnection.SetInnerConnectionTo(to);
			}
		}

		// Token: 0x0600111C RID: 4380 RVA: 0x00058486 File Offset: 0x00056686
		protected override DbMetaDataFactory CreateMetaDataFactory(DbConnectionInternal internalConnection, out bool cacheMetaDataFactory)
		{
			Stream manifestResourceStream = Assembly.GetExecutingAssembly().GetManifestResourceStream("System.Data.SqlClient.SqlMetaData.xml");
			cacheMetaDataFactory = true;
			return new SqlMetaDataFactory(manifestResourceStream, internalConnection.ServerVersion, internalConnection.ServerVersion);
		}

		// Token: 0x0600111D RID: 4381 RVA: 0x000584AB File Offset: 0x000566AB
		// Note: this type is marked as 'beforefieldinit'.
		static SqlConnectionFactory()
		{
		}

		// Token: 0x04000BD5 RID: 3029
		private const string _metaDataXml = "MetaDataXml";

		// Token: 0x04000BD6 RID: 3030
		public static readonly SqlConnectionFactory SingletonInstance = new SqlConnectionFactory();
	}
}
