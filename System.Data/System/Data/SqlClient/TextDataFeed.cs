﻿using System;
using System.IO;

namespace System.Data.SqlClient
{
	// Token: 0x020001B5 RID: 437
	internal class TextDataFeed : DataFeed
	{
		// Token: 0x060013DF RID: 5087 RVA: 0x00066C88 File Offset: 0x00064E88
		internal TextDataFeed(TextReader source)
		{
			this._source = source;
		}

		// Token: 0x04000E3C RID: 3644
		internal TextReader _source;
	}
}
