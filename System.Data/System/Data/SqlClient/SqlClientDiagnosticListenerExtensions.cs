﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace System.Data.SqlClient
{
	// Token: 0x0200014A RID: 330
	internal static class SqlClientDiagnosticListenerExtensions
	{
		// Token: 0x06000F78 RID: 3960 RVA: 0x00051330 File Offset: 0x0004F530
		public static Guid WriteCommandBefore(this DiagnosticListener @this, SqlCommand sqlCommand, [CallerMemberName] string operation = "")
		{
			if (@this.IsEnabled("System.Data.SqlClient.WriteCommandBefore"))
			{
				Guid guid = Guid.NewGuid();
				string s = "System.Data.SqlClient.WriteCommandBefore";
				Guid operationId = guid;
				SqlConnection connection = sqlCommand.Connection;
				@this.Write(s, new
				{
					OperationId = operationId,
					Operation = operation,
					ConnectionId = ((connection != null) ? new Guid?(connection.ClientConnectionId) : null),
					Command = sqlCommand
				});
				return guid;
			}
			return Guid.Empty;
		}

		// Token: 0x06000F79 RID: 3961 RVA: 0x0005138C File Offset: 0x0004F58C
		public static void WriteCommandAfter(this DiagnosticListener @this, Guid operationId, SqlCommand sqlCommand, [CallerMemberName] string operation = "")
		{
			if (@this.IsEnabled("System.Data.SqlClient.WriteCommandAfter"))
			{
				string s = "System.Data.SqlClient.WriteCommandAfter";
				SqlConnection connection = sqlCommand.Connection;
				Guid? connectionId = (connection != null) ? new Guid?(connection.ClientConnectionId) : null;
				SqlStatistics statistics = sqlCommand.Statistics;
				@this.Write(s, new
				{
					OperationId = operationId,
					Operation = operation,
					ConnectionId = connectionId,
					Command = sqlCommand,
					Statistics = ((statistics != null) ? statistics.GetDictionary() : null),
					Timestamp = Stopwatch.GetTimestamp()
				});
			}
		}

		// Token: 0x06000F7A RID: 3962 RVA: 0x000513F0 File Offset: 0x0004F5F0
		public static void WriteCommandError(this DiagnosticListener @this, Guid operationId, SqlCommand sqlCommand, Exception ex, [CallerMemberName] string operation = "")
		{
			if (@this.IsEnabled("System.Data.SqlClient.WriteCommandError"))
			{
				string s = "System.Data.SqlClient.WriteCommandError";
				SqlConnection connection = sqlCommand.Connection;
				@this.Write(s, new
				{
					OperationId = operationId,
					Operation = operation,
					ConnectionId = ((connection != null) ? new Guid?(connection.ClientConnectionId) : null),
					Command = sqlCommand,
					Exception = ex,
					Timestamp = Stopwatch.GetTimestamp()
				});
			}
		}

		// Token: 0x06000F7B RID: 3963 RVA: 0x00051444 File Offset: 0x0004F644
		public static Guid WriteConnectionOpenBefore(this DiagnosticListener @this, SqlConnection sqlConnection, [CallerMemberName] string operation = "")
		{
			if (@this.IsEnabled("System.Data.SqlClient.WriteConnectionOpenBefore"))
			{
				Guid guid = Guid.NewGuid();
				@this.Write("System.Data.SqlClient.WriteConnectionOpenBefore", new
				{
					OperationId = guid,
					Operation = operation,
					Connection = sqlConnection,
					Timestamp = Stopwatch.GetTimestamp()
				});
				return guid;
			}
			return Guid.Empty;
		}

		// Token: 0x06000F7C RID: 3964 RVA: 0x00051483 File Offset: 0x0004F683
		public static void WriteConnectionOpenAfter(this DiagnosticListener @this, Guid operationId, SqlConnection sqlConnection, [CallerMemberName] string operation = "")
		{
			if (@this.IsEnabled("System.Data.SqlClient.WriteConnectionOpenAfter"))
			{
				string s = "System.Data.SqlClient.WriteConnectionOpenAfter";
				Guid clientConnectionId = sqlConnection.ClientConnectionId;
				SqlStatistics statistics = sqlConnection.Statistics;
				@this.Write(s, new
				{
					OperationId = operationId,
					Operation = operation,
					ConnectionId = clientConnectionId,
					Connection = sqlConnection,
					Statistics = ((statistics != null) ? statistics.GetDictionary() : null),
					Timestamp = Stopwatch.GetTimestamp()
				});
			}
		}

		// Token: 0x06000F7D RID: 3965 RVA: 0x000514C2 File Offset: 0x0004F6C2
		public static void WriteConnectionOpenError(this DiagnosticListener @this, Guid operationId, SqlConnection sqlConnection, Exception ex, [CallerMemberName] string operation = "")
		{
			if (@this.IsEnabled("System.Data.SqlClient.WriteConnectionOpenError"))
			{
				@this.Write("System.Data.SqlClient.WriteConnectionOpenError", new
				{
					OperationId = operationId,
					Operation = operation,
					ConnectionId = sqlConnection.ClientConnectionId,
					Connection = sqlConnection,
					Exception = ex,
					Timestamp = Stopwatch.GetTimestamp()
				});
			}
		}

		// Token: 0x06000F7E RID: 3966 RVA: 0x000514F4 File Offset: 0x0004F6F4
		public static Guid WriteConnectionCloseBefore(this DiagnosticListener @this, SqlConnection sqlConnection, [CallerMemberName] string operation = "")
		{
			if (@this.IsEnabled("System.Data.SqlClient.WriteConnectionCloseBefore"))
			{
				Guid guid = Guid.NewGuid();
				string s = "System.Data.SqlClient.WriteConnectionCloseBefore";
				Guid operationId = guid;
				Guid clientConnectionId = sqlConnection.ClientConnectionId;
				SqlStatistics statistics = sqlConnection.Statistics;
				@this.Write(s, new
				{
					OperationId = operationId,
					Operation = operation,
					ConnectionId = clientConnectionId,
					Connection = sqlConnection,
					Statistics = ((statistics != null) ? statistics.GetDictionary() : null),
					Timestamp = Stopwatch.GetTimestamp()
				});
				return guid;
			}
			return Guid.Empty;
		}

		// Token: 0x06000F7F RID: 3967 RVA: 0x0005154B File Offset: 0x0004F74B
		public static void WriteConnectionCloseAfter(this DiagnosticListener @this, Guid operationId, Guid clientConnectionId, SqlConnection sqlConnection, [CallerMemberName] string operation = "")
		{
			if (@this.IsEnabled("System.Data.SqlClient.WriteConnectionCloseAfter"))
			{
				string s = "System.Data.SqlClient.WriteConnectionCloseAfter";
				SqlStatistics statistics = sqlConnection.Statistics;
				@this.Write(s, new
				{
					OperationId = operationId,
					Operation = operation,
					ConnectionId = clientConnectionId,
					Connection = sqlConnection,
					Statistics = ((statistics != null) ? statistics.GetDictionary() : null),
					Timestamp = Stopwatch.GetTimestamp()
				});
			}
		}

		// Token: 0x06000F80 RID: 3968 RVA: 0x00051588 File Offset: 0x0004F788
		public static void WriteConnectionCloseError(this DiagnosticListener @this, Guid operationId, Guid clientConnectionId, SqlConnection sqlConnection, Exception ex, [CallerMemberName] string operation = "")
		{
			if (@this.IsEnabled("System.Data.SqlClient.WriteConnectionCloseError"))
			{
				string s = "System.Data.SqlClient.WriteConnectionCloseError";
				SqlStatistics statistics = sqlConnection.Statistics;
				@this.Write(s, new
				{
					OperationId = operationId,
					Operation = operation,
					ConnectionId = clientConnectionId,
					Connection = sqlConnection,
					Statistics = ((statistics != null) ? statistics.GetDictionary() : null),
					Exception = ex,
					Timestamp = Stopwatch.GetTimestamp()
				});
			}
		}

		// Token: 0x06000F81 RID: 3969 RVA: 0x000515D0 File Offset: 0x0004F7D0
		public static Guid WriteTransactionCommitBefore(this DiagnosticListener @this, IsolationLevel isolationLevel, SqlConnection connection, [CallerMemberName] string operation = "")
		{
			if (@this.IsEnabled("System.Data.SqlClient.WriteTransactionCommitBefore"))
			{
				Guid guid = Guid.NewGuid();
				@this.Write("System.Data.SqlClient.WriteTransactionCommitBefore", new
				{
					OperationId = guid,
					Operation = operation,
					IsolationLevel = isolationLevel,
					Connection = connection,
					Timestamp = Stopwatch.GetTimestamp()
				});
				return guid;
			}
			return Guid.Empty;
		}

		// Token: 0x06000F82 RID: 3970 RVA: 0x00051610 File Offset: 0x0004F810
		public static void WriteTransactionCommitAfter(this DiagnosticListener @this, Guid operationId, IsolationLevel isolationLevel, SqlConnection connection, [CallerMemberName] string operation = "")
		{
			if (@this.IsEnabled("System.Data.SqlClient.WriteTransactionCommitAfter"))
			{
				@this.Write("System.Data.SqlClient.WriteTransactionCommitAfter", new
				{
					OperationId = operationId,
					Operation = operation,
					IsolationLevel = isolationLevel,
					Connection = connection,
					Timestamp = Stopwatch.GetTimestamp()
				});
			}
		}

		// Token: 0x06000F83 RID: 3971 RVA: 0x00051639 File Offset: 0x0004F839
		public static void WriteTransactionCommitError(this DiagnosticListener @this, Guid operationId, IsolationLevel isolationLevel, SqlConnection connection, Exception ex, [CallerMemberName] string operation = "")
		{
			if (@this.IsEnabled("System.Data.SqlClient.WriteTransactionCommitError"))
			{
				@this.Write("System.Data.SqlClient.WriteTransactionCommitError", new
				{
					OperationId = operationId,
					Operation = operation,
					IsolationLevel = isolationLevel,
					Connection = connection,
					Exception = ex,
					Timestamp = Stopwatch.GetTimestamp()
				});
			}
		}

		// Token: 0x06000F84 RID: 3972 RVA: 0x00051664 File Offset: 0x0004F864
		public static Guid WriteTransactionRollbackBefore(this DiagnosticListener @this, IsolationLevel isolationLevel, SqlConnection connection, string transactionName, [CallerMemberName] string operation = "")
		{
			if (@this.IsEnabled("System.Data.SqlClient.WriteTransactionRollbackBefore"))
			{
				Guid guid = Guid.NewGuid();
				@this.Write("System.Data.SqlClient.WriteTransactionRollbackBefore", new
				{
					OperationId = guid,
					Operation = operation,
					IsolationLevel = isolationLevel,
					Connection = connection,
					TransactionName = transactionName,
					Timestamp = Stopwatch.GetTimestamp()
				});
				return guid;
			}
			return Guid.Empty;
		}

		// Token: 0x06000F85 RID: 3973 RVA: 0x000516A6 File Offset: 0x0004F8A6
		public static void WriteTransactionRollbackAfter(this DiagnosticListener @this, Guid operationId, IsolationLevel isolationLevel, SqlConnection connection, string transactionName, [CallerMemberName] string operation = "")
		{
			if (@this.IsEnabled("System.Data.SqlClient.WriteTransactionRollbackAfter"))
			{
				@this.Write("System.Data.SqlClient.WriteTransactionRollbackAfter", new
				{
					OperationId = operationId,
					Operation = operation,
					IsolationLevel = isolationLevel,
					Connection = connection,
					TransactionName = transactionName,
					Timestamp = Stopwatch.GetTimestamp()
				});
			}
		}

		// Token: 0x06000F86 RID: 3974 RVA: 0x000516D4 File Offset: 0x0004F8D4
		public static void WriteTransactionRollbackError(this DiagnosticListener @this, Guid operationId, IsolationLevel isolationLevel, SqlConnection connection, string transactionName, Exception ex, [CallerMemberName] string operation = "")
		{
			if (@this.IsEnabled("System.Data.SqlClient.WriteTransactionRollbackError"))
			{
				@this.Write("System.Data.SqlClient.WriteTransactionRollbackError", new
				{
					OperationId = operationId,
					Operation = operation,
					IsolationLevel = isolationLevel,
					Connection = connection,
					TransactionName = transactionName,
					Exception = ex,
					Timestamp = Stopwatch.GetTimestamp()
				});
			}
		}

		// Token: 0x04000B01 RID: 2817
		public const string DiagnosticListenerName = "SqlClientDiagnosticListener";

		// Token: 0x04000B02 RID: 2818
		private const string SqlClientPrefix = "System.Data.SqlClient.";

		// Token: 0x04000B03 RID: 2819
		public const string SqlBeforeExecuteCommand = "System.Data.SqlClient.WriteCommandBefore";

		// Token: 0x04000B04 RID: 2820
		public const string SqlAfterExecuteCommand = "System.Data.SqlClient.WriteCommandAfter";

		// Token: 0x04000B05 RID: 2821
		public const string SqlErrorExecuteCommand = "System.Data.SqlClient.WriteCommandError";

		// Token: 0x04000B06 RID: 2822
		public const string SqlBeforeOpenConnection = "System.Data.SqlClient.WriteConnectionOpenBefore";

		// Token: 0x04000B07 RID: 2823
		public const string SqlAfterOpenConnection = "System.Data.SqlClient.WriteConnectionOpenAfter";

		// Token: 0x04000B08 RID: 2824
		public const string SqlErrorOpenConnection = "System.Data.SqlClient.WriteConnectionOpenError";

		// Token: 0x04000B09 RID: 2825
		public const string SqlBeforeCloseConnection = "System.Data.SqlClient.WriteConnectionCloseBefore";

		// Token: 0x04000B0A RID: 2826
		public const string SqlAfterCloseConnection = "System.Data.SqlClient.WriteConnectionCloseAfter";

		// Token: 0x04000B0B RID: 2827
		public const string SqlErrorCloseConnection = "System.Data.SqlClient.WriteConnectionCloseError";

		// Token: 0x04000B0C RID: 2828
		public const string SqlBeforeCommitTransaction = "System.Data.SqlClient.WriteTransactionCommitBefore";

		// Token: 0x04000B0D RID: 2829
		public const string SqlAfterCommitTransaction = "System.Data.SqlClient.WriteTransactionCommitAfter";

		// Token: 0x04000B0E RID: 2830
		public const string SqlErrorCommitTransaction = "System.Data.SqlClient.WriteTransactionCommitError";

		// Token: 0x04000B0F RID: 2831
		public const string SqlBeforeRollbackTransaction = "System.Data.SqlClient.WriteTransactionRollbackBefore";

		// Token: 0x04000B10 RID: 2832
		public const string SqlAfterRollbackTransaction = "System.Data.SqlClient.WriteTransactionRollbackAfter";

		// Token: 0x04000B11 RID: 2833
		public const string SqlErrorRollbackTransaction = "System.Data.SqlClient.WriteTransactionRollbackError";
	}
}
