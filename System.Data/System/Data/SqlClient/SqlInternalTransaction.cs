﻿using System;
using System.Data.Common;
using System.Runtime.CompilerServices;
using System.Threading;

namespace System.Data.SqlClient
{
	// Token: 0x02000217 RID: 535
	internal sealed class SqlInternalTransaction
	{
		// Token: 0x17000457 RID: 1111
		// (get) Token: 0x060017DF RID: 6111 RVA: 0x0007CDBA File Offset: 0x0007AFBA
		// (set) Token: 0x060017E0 RID: 6112 RVA: 0x0007CDC2 File Offset: 0x0007AFC2
		internal bool RestoreBrokenConnection
		{
			[CompilerGenerated]
			get
			{
				return this.<RestoreBrokenConnection>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<RestoreBrokenConnection>k__BackingField = value;
			}
		}

		// Token: 0x17000458 RID: 1112
		// (get) Token: 0x060017E1 RID: 6113 RVA: 0x0007CDCB File Offset: 0x0007AFCB
		// (set) Token: 0x060017E2 RID: 6114 RVA: 0x0007CDD3 File Offset: 0x0007AFD3
		internal bool ConnectionHasBeenRestored
		{
			[CompilerGenerated]
			get
			{
				return this.<ConnectionHasBeenRestored>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<ConnectionHasBeenRestored>k__BackingField = value;
			}
		}

		// Token: 0x060017E3 RID: 6115 RVA: 0x0007CDDC File Offset: 0x0007AFDC
		internal SqlInternalTransaction(SqlInternalConnection innerConnection, TransactionType type, SqlTransaction outerTransaction) : this(innerConnection, type, outerTransaction, 0L)
		{
		}

		// Token: 0x060017E4 RID: 6116 RVA: 0x0007CDE9 File Offset: 0x0007AFE9
		internal SqlInternalTransaction(SqlInternalConnection innerConnection, TransactionType type, SqlTransaction outerTransaction, long transactionId)
		{
			this._innerConnection = innerConnection;
			this._transactionType = type;
			if (outerTransaction != null)
			{
				this._parent = new WeakReference(outerTransaction);
			}
			this._transactionId = transactionId;
			this.RestoreBrokenConnection = false;
			this.ConnectionHasBeenRestored = false;
		}

		// Token: 0x17000459 RID: 1113
		// (get) Token: 0x060017E5 RID: 6117 RVA: 0x0007CE24 File Offset: 0x0007B024
		internal bool HasParentTransaction
		{
			get
			{
				return TransactionType.LocalFromAPI == this._transactionType || (TransactionType.LocalFromTSQL == this._transactionType && this._parent != null);
			}
		}

		// Token: 0x1700045A RID: 1114
		// (get) Token: 0x060017E6 RID: 6118 RVA: 0x0007CE45 File Offset: 0x0007B045
		internal bool IsAborted
		{
			get
			{
				return TransactionState.Aborted == this._transactionState;
			}
		}

		// Token: 0x1700045B RID: 1115
		// (get) Token: 0x060017E7 RID: 6119 RVA: 0x0007CE50 File Offset: 0x0007B050
		internal bool IsActive
		{
			get
			{
				return TransactionState.Active == this._transactionState;
			}
		}

		// Token: 0x1700045C RID: 1116
		// (get) Token: 0x060017E8 RID: 6120 RVA: 0x0007CE5B File Offset: 0x0007B05B
		internal bool IsCommitted
		{
			get
			{
				return TransactionState.Committed == this._transactionState;
			}
		}

		// Token: 0x1700045D RID: 1117
		// (get) Token: 0x060017E9 RID: 6121 RVA: 0x0007CE66 File Offset: 0x0007B066
		internal bool IsCompleted
		{
			get
			{
				return TransactionState.Aborted == this._transactionState || TransactionState.Committed == this._transactionState || TransactionState.Unknown == this._transactionState;
			}
		}

		// Token: 0x1700045E RID: 1118
		// (get) Token: 0x060017EA RID: 6122 RVA: 0x0007CE85 File Offset: 0x0007B085
		internal bool IsDelegated
		{
			get
			{
				return TransactionType.Delegated == this._transactionType;
			}
		}

		// Token: 0x1700045F RID: 1119
		// (get) Token: 0x060017EB RID: 6123 RVA: 0x0007CE90 File Offset: 0x0007B090
		internal bool IsDistributed
		{
			get
			{
				return TransactionType.Distributed == this._transactionType;
			}
		}

		// Token: 0x17000460 RID: 1120
		// (get) Token: 0x060017EC RID: 6124 RVA: 0x0007CE9B File Offset: 0x0007B09B
		internal bool IsLocal
		{
			get
			{
				return TransactionType.LocalFromTSQL == this._transactionType || TransactionType.LocalFromAPI == this._transactionType;
			}
		}

		// Token: 0x17000461 RID: 1121
		// (get) Token: 0x060017ED RID: 6125 RVA: 0x0007CEB4 File Offset: 0x0007B0B4
		internal bool IsOrphaned
		{
			get
			{
				return this._parent != null && this._parent.Target == null;
			}
		}

		// Token: 0x17000462 RID: 1122
		// (get) Token: 0x060017EE RID: 6126 RVA: 0x0007CEE1 File Offset: 0x0007B0E1
		internal bool IsZombied
		{
			get
			{
				return this._innerConnection == null;
			}
		}

		// Token: 0x17000463 RID: 1123
		// (get) Token: 0x060017EF RID: 6127 RVA: 0x0007CEEC File Offset: 0x0007B0EC
		internal int OpenResultsCount
		{
			get
			{
				return this._openResultCount;
			}
		}

		// Token: 0x17000464 RID: 1124
		// (get) Token: 0x060017F0 RID: 6128 RVA: 0x0007CEF4 File Offset: 0x0007B0F4
		internal SqlTransaction Parent
		{
			get
			{
				SqlTransaction result = null;
				if (this._parent != null)
				{
					result = (SqlTransaction)this._parent.Target;
				}
				return result;
			}
		}

		// Token: 0x17000465 RID: 1125
		// (get) Token: 0x060017F1 RID: 6129 RVA: 0x0007CF1D File Offset: 0x0007B11D
		// (set) Token: 0x060017F2 RID: 6130 RVA: 0x0007CF25 File Offset: 0x0007B125
		internal long TransactionId
		{
			get
			{
				return this._transactionId;
			}
			set
			{
				this._transactionId = value;
			}
		}

		// Token: 0x060017F3 RID: 6131 RVA: 0x0007CF2E File Offset: 0x0007B12E
		internal void Activate()
		{
			this._transactionState = TransactionState.Active;
		}

		// Token: 0x060017F4 RID: 6132 RVA: 0x0007CF38 File Offset: 0x0007B138
		private void CheckTransactionLevelAndZombie()
		{
			try
			{
				if (!this.IsZombied && this.GetServerTransactionLevel() == 0)
				{
					this.Zombie();
				}
			}
			catch (Exception e)
			{
				if (!ADP.IsCatchableExceptionType(e))
				{
					throw;
				}
				this.Zombie();
			}
		}

		// Token: 0x060017F5 RID: 6133 RVA: 0x0007CF80 File Offset: 0x0007B180
		internal void CloseFromConnection()
		{
			SqlInternalConnection innerConnection = this._innerConnection;
			bool flag = true;
			try
			{
				innerConnection.ExecuteTransaction(SqlInternalConnection.TransactionRequest.IfRollback, null, IsolationLevel.Unspecified, null, false);
			}
			catch (Exception e)
			{
				flag = ADP.IsCatchableExceptionType(e);
				throw;
			}
			finally
			{
				if (flag)
				{
					this.Zombie();
				}
			}
		}

		// Token: 0x060017F6 RID: 6134 RVA: 0x0007CFD4 File Offset: 0x0007B1D4
		internal void Commit()
		{
			if (this._innerConnection.IsLockedForBulkCopy)
			{
				throw SQL.ConnectionLockedForBcpEvent();
			}
			this._innerConnection.ValidateConnectionForExecute(null);
			try
			{
				this._innerConnection.ExecuteTransaction(SqlInternalConnection.TransactionRequest.Commit, null, IsolationLevel.Unspecified, null, false);
				this.ZombieParent();
			}
			catch (Exception e)
			{
				if (ADP.IsCatchableExceptionType(e))
				{
					this.CheckTransactionLevelAndZombie();
				}
				throw;
			}
		}

		// Token: 0x060017F7 RID: 6135 RVA: 0x0007D038 File Offset: 0x0007B238
		internal void Completed(TransactionState transactionState)
		{
			this._transactionState = transactionState;
			this.Zombie();
		}

		// Token: 0x060017F8 RID: 6136 RVA: 0x0007D047 File Offset: 0x0007B247
		internal int DecrementAndObtainOpenResultCount()
		{
			int num = Interlocked.Decrement(ref this._openResultCount);
			if (num < 0)
			{
				throw SQL.OpenResultCountExceeded();
			}
			return num;
		}

		// Token: 0x060017F9 RID: 6137 RVA: 0x0007D05E File Offset: 0x0007B25E
		internal void Dispose()
		{
			this.Dispose(true);
			GC.SuppressFinalize(this);
		}

		// Token: 0x060017FA RID: 6138 RVA: 0x0007D06D File Offset: 0x0007B26D
		private void Dispose(bool disposing)
		{
			if (disposing && this._innerConnection != null)
			{
				this._disposing = true;
				this.Rollback();
			}
		}

		// Token: 0x060017FB RID: 6139 RVA: 0x0007D088 File Offset: 0x0007B288
		private int GetServerTransactionLevel()
		{
			int result;
			using (SqlCommand sqlCommand = new SqlCommand("set @out = @@trancount", (SqlConnection)this._innerConnection.Owner))
			{
				sqlCommand.Transaction = this.Parent;
				SqlParameter sqlParameter = new SqlParameter("@out", SqlDbType.Int);
				sqlParameter.Direction = ParameterDirection.Output;
				sqlCommand.Parameters.Add(sqlParameter);
				sqlCommand.RunExecuteReader(CommandBehavior.Default, RunBehavior.UntilDone, false, "GetServerTransactionLevel");
				result = (int)sqlParameter.Value;
			}
			return result;
		}

		// Token: 0x060017FC RID: 6140 RVA: 0x0007D114 File Offset: 0x0007B314
		internal int IncrementAndObtainOpenResultCount()
		{
			int num = Interlocked.Increment(ref this._openResultCount);
			if (num < 0)
			{
				throw SQL.OpenResultCountExceeded();
			}
			return num;
		}

		// Token: 0x060017FD RID: 6141 RVA: 0x0007D12B File Offset: 0x0007B32B
		internal void InitParent(SqlTransaction transaction)
		{
			this._parent = new WeakReference(transaction);
		}

		// Token: 0x060017FE RID: 6142 RVA: 0x0007D13C File Offset: 0x0007B33C
		internal void Rollback()
		{
			if (this._innerConnection.IsLockedForBulkCopy)
			{
				throw SQL.ConnectionLockedForBcpEvent();
			}
			this._innerConnection.ValidateConnectionForExecute(null);
			try
			{
				this._innerConnection.ExecuteTransaction(SqlInternalConnection.TransactionRequest.IfRollback, null, IsolationLevel.Unspecified, null, false);
				this.Zombie();
			}
			catch (Exception e)
			{
				if (!ADP.IsCatchableExceptionType(e))
				{
					throw;
				}
				this.CheckTransactionLevelAndZombie();
				if (!this._disposing)
				{
					throw;
				}
			}
		}

		// Token: 0x060017FF RID: 6143 RVA: 0x0007D1AC File Offset: 0x0007B3AC
		internal void Rollback(string transactionName)
		{
			if (this._innerConnection.IsLockedForBulkCopy)
			{
				throw SQL.ConnectionLockedForBcpEvent();
			}
			this._innerConnection.ValidateConnectionForExecute(null);
			if (string.IsNullOrEmpty(transactionName))
			{
				throw SQL.NullEmptyTransactionName();
			}
			try
			{
				this._innerConnection.ExecuteTransaction(SqlInternalConnection.TransactionRequest.Rollback, transactionName, IsolationLevel.Unspecified, null, false);
			}
			catch (Exception e)
			{
				if (ADP.IsCatchableExceptionType(e))
				{
					this.CheckTransactionLevelAndZombie();
				}
				throw;
			}
		}

		// Token: 0x06001800 RID: 6144 RVA: 0x0007D218 File Offset: 0x0007B418
		internal void Save(string savePointName)
		{
			this._innerConnection.ValidateConnectionForExecute(null);
			if (string.IsNullOrEmpty(savePointName))
			{
				throw SQL.NullEmptyTransactionName();
			}
			try
			{
				this._innerConnection.ExecuteTransaction(SqlInternalConnection.TransactionRequest.Save, savePointName, IsolationLevel.Unspecified, null, false);
			}
			catch (Exception e)
			{
				if (ADP.IsCatchableExceptionType(e))
				{
					this.CheckTransactionLevelAndZombie();
				}
				throw;
			}
		}

		// Token: 0x06001801 RID: 6145 RVA: 0x0007D274 File Offset: 0x0007B474
		internal void Zombie()
		{
			this.ZombieParent();
			SqlInternalConnection innerConnection = this._innerConnection;
			this._innerConnection = null;
			if (innerConnection != null)
			{
				innerConnection.DisconnectTransaction(this);
			}
		}

		// Token: 0x06001802 RID: 6146 RVA: 0x0007D2A0 File Offset: 0x0007B4A0
		private void ZombieParent()
		{
			if (this._parent != null)
			{
				SqlTransaction sqlTransaction = (SqlTransaction)this._parent.Target;
				if (sqlTransaction != null)
				{
					sqlTransaction.Zombie();
				}
				this._parent = null;
			}
		}

		// Token: 0x040011DF RID: 4575
		internal const long NullTransactionId = 0L;

		// Token: 0x040011E0 RID: 4576
		private TransactionState _transactionState;

		// Token: 0x040011E1 RID: 4577
		private TransactionType _transactionType;

		// Token: 0x040011E2 RID: 4578
		private long _transactionId;

		// Token: 0x040011E3 RID: 4579
		private int _openResultCount;

		// Token: 0x040011E4 RID: 4580
		private SqlInternalConnection _innerConnection;

		// Token: 0x040011E5 RID: 4581
		private bool _disposing;

		// Token: 0x040011E6 RID: 4582
		private WeakReference _parent;

		// Token: 0x040011E7 RID: 4583
		[CompilerGenerated]
		private bool <RestoreBrokenConnection>k__BackingField;

		// Token: 0x040011E8 RID: 4584
		[CompilerGenerated]
		private bool <ConnectionHasBeenRestored>k__BackingField;
	}
}
