﻿using System;

namespace System.Data.SqlClient
{
	// Token: 0x02000215 RID: 533
	internal enum TransactionState
	{
		// Token: 0x040011D4 RID: 4564
		Pending,
		// Token: 0x040011D5 RID: 4565
		Active,
		// Token: 0x040011D6 RID: 4566
		Aborted,
		// Token: 0x040011D7 RID: 4567
		Committed,
		// Token: 0x040011D8 RID: 4568
		Unknown
	}
}
