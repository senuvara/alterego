﻿using System;

namespace System.Data.SqlClient
{
	// Token: 0x02000120 RID: 288
	internal static class SNINativeMethodWrapper
	{
		// Token: 0x02000121 RID: 289
		internal enum SniSpecialErrors : uint
		{
			// Token: 0x04000A2D RID: 2605
			LocalDBErrorCode = 50U,
			// Token: 0x04000A2E RID: 2606
			MultiSubnetFailoverWithMoreThan64IPs = 47U,
			// Token: 0x04000A2F RID: 2607
			MultiSubnetFailoverWithInstanceSpecified,
			// Token: 0x04000A30 RID: 2608
			MultiSubnetFailoverWithNonTcpProtocol,
			// Token: 0x04000A31 RID: 2609
			MaxErrorValue = 50157U
		}
	}
}
