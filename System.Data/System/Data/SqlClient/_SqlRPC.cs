﻿using System;

namespace System.Data.SqlClient
{
	// Token: 0x02000204 RID: 516
	internal sealed class _SqlRPC
	{
		// Token: 0x060016E7 RID: 5863 RVA: 0x000781C5 File Offset: 0x000763C5
		internal string GetCommandTextOrRpcName()
		{
			if (10 == this.ProcID)
			{
				return (string)this.parameters[0].Value;
			}
			return this.rpcName;
		}

		// Token: 0x060016E8 RID: 5864 RVA: 0x00005C14 File Offset: 0x00003E14
		public _SqlRPC()
		{
		}

		// Token: 0x04001145 RID: 4421
		internal string rpcName;

		// Token: 0x04001146 RID: 4422
		internal ushort ProcID;

		// Token: 0x04001147 RID: 4423
		internal ushort options;

		// Token: 0x04001148 RID: 4424
		internal SqlParameter[] parameters;

		// Token: 0x04001149 RID: 4425
		internal byte[] paramoptions;

		// Token: 0x0400114A RID: 4426
		internal int? recordsAffected;

		// Token: 0x0400114B RID: 4427
		internal int cumulativeRecordsAffected;

		// Token: 0x0400114C RID: 4428
		internal int errorsIndexStart;

		// Token: 0x0400114D RID: 4429
		internal int errorsIndexEnd;

		// Token: 0x0400114E RID: 4430
		internal SqlErrorCollection errors;

		// Token: 0x0400114F RID: 4431
		internal int warningsIndexStart;

		// Token: 0x04001150 RID: 4432
		internal int warningsIndexEnd;

		// Token: 0x04001151 RID: 4433
		internal SqlErrorCollection warnings;
	}
}
