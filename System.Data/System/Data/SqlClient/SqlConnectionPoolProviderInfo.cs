﻿using System;
using System.Data.ProviderBase;

namespace System.Data.SqlClient
{
	// Token: 0x02000173 RID: 371
	internal sealed class SqlConnectionPoolProviderInfo : DbConnectionPoolProviderInfo
	{
		// Token: 0x17000300 RID: 768
		// (get) Token: 0x0600112B RID: 4395 RVA: 0x00058668 File Offset: 0x00056868
		// (set) Token: 0x0600112C RID: 4396 RVA: 0x00058670 File Offset: 0x00056870
		internal string InstanceName
		{
			get
			{
				return this._instanceName;
			}
			set
			{
				this._instanceName = value;
			}
		}

		// Token: 0x0600112D RID: 4397 RVA: 0x00058679 File Offset: 0x00056879
		public SqlConnectionPoolProviderInfo()
		{
		}

		// Token: 0x04000BDB RID: 3035
		private string _instanceName;
	}
}
