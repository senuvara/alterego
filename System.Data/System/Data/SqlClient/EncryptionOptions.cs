﻿using System;

namespace System.Data.SqlClient
{
	// Token: 0x020001F6 RID: 502
	internal enum EncryptionOptions
	{
		// Token: 0x040010D6 RID: 4310
		OFF,
		// Token: 0x040010D7 RID: 4311
		ON,
		// Token: 0x040010D8 RID: 4312
		NOT_SUP,
		// Token: 0x040010D9 RID: 4313
		REQ,
		// Token: 0x040010DA RID: 4314
		LOGIN
	}
}
