﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.IO;
using System.Runtime.CompilerServices;
using System.Xml;

namespace System.Data.SqlClient
{
	// Token: 0x02000149 RID: 329
	internal sealed class SqlCachedBuffer : INullable
	{
		// Token: 0x06000F6C RID: 3948 RVA: 0x00005C14 File Offset: 0x00003E14
		private SqlCachedBuffer()
		{
		}

		// Token: 0x06000F6D RID: 3949 RVA: 0x000511D8 File Offset: 0x0004F3D8
		private SqlCachedBuffer(List<byte[]> cachedBytes)
		{
			this._cachedBytes = cachedBytes;
		}

		// Token: 0x170002AC RID: 684
		// (get) Token: 0x06000F6E RID: 3950 RVA: 0x000511E7 File Offset: 0x0004F3E7
		internal List<byte[]> CachedBytes
		{
			get
			{
				return this._cachedBytes;
			}
		}

		// Token: 0x06000F6F RID: 3951 RVA: 0x000511F0 File Offset: 0x0004F3F0
		internal static bool TryCreate(SqlMetaDataPriv metadata, TdsParser parser, TdsParserStateObject stateObj, out SqlCachedBuffer buffer)
		{
			int num = 0;
			List<byte[]> list = new List<byte[]>();
			buffer = null;
			ulong num2;
			if (!parser.TryPlpBytesLeft(stateObj, out num2))
			{
				return false;
			}
			while (num2 != 0UL)
			{
				do
				{
					num = ((num2 > 2048UL) ? 2048 : ((int)num2));
					byte[] array = new byte[num];
					if (!stateObj.TryReadPlpBytes(ref array, 0, num, out num))
					{
						return false;
					}
					if (list.Count == 0)
					{
						SqlCachedBuffer.AddByteOrderMark(array, list);
					}
					list.Add(array);
					num2 -= (ulong)((long)num);
				}
				while (num2 > 0UL);
				if (!parser.TryPlpBytesLeft(stateObj, out num2))
				{
					return false;
				}
				if (num2 <= 0UL)
				{
					break;
				}
			}
			buffer = new SqlCachedBuffer(list);
			return true;
		}

		// Token: 0x06000F70 RID: 3952 RVA: 0x0005127D File Offset: 0x0004F47D
		private static void AddByteOrderMark(byte[] byteArr, List<byte[]> cachedBytes)
		{
			if (byteArr.Length < 2 || byteArr[0] != 223 || byteArr[1] != 255)
			{
				cachedBytes.Add(TdsEnums.XMLUNICODEBOMBYTES);
			}
		}

		// Token: 0x06000F71 RID: 3953 RVA: 0x000512A4 File Offset: 0x0004F4A4
		internal Stream ToStream()
		{
			return new SqlCachedStream(this);
		}

		// Token: 0x06000F72 RID: 3954 RVA: 0x000512AC File Offset: 0x0004F4AC
		public override string ToString()
		{
			if (this.IsNull)
			{
				throw new SqlNullValueException();
			}
			if (this._cachedBytes.Count == 0)
			{
				return string.Empty;
			}
			return new SqlXml(this.ToStream()).Value;
		}

		// Token: 0x06000F73 RID: 3955 RVA: 0x000512DF File Offset: 0x0004F4DF
		internal SqlString ToSqlString()
		{
			if (this.IsNull)
			{
				return SqlString.Null;
			}
			return new SqlString(this.ToString());
		}

		// Token: 0x06000F74 RID: 3956 RVA: 0x000512FA File Offset: 0x0004F4FA
		internal SqlXml ToSqlXml()
		{
			return new SqlXml(this.ToStream());
		}

		// Token: 0x06000F75 RID: 3957 RVA: 0x00051307 File Offset: 0x0004F507
		[MethodImpl(MethodImplOptions.NoInlining)]
		internal XmlReader ToXmlReader()
		{
			return SqlTypeWorkarounds.SqlXmlCreateSqlXmlReader(this.ToStream(), false, false);
		}

		// Token: 0x170002AD RID: 685
		// (get) Token: 0x06000F76 RID: 3958 RVA: 0x00051316 File Offset: 0x0004F516
		public bool IsNull
		{
			get
			{
				return this._cachedBytes == null;
			}
		}

		// Token: 0x06000F77 RID: 3959 RVA: 0x00051323 File Offset: 0x0004F523
		// Note: this type is marked as 'beforefieldinit'.
		static SqlCachedBuffer()
		{
		}

		// Token: 0x04000AFE RID: 2814
		public static readonly SqlCachedBuffer Null = new SqlCachedBuffer();

		// Token: 0x04000AFF RID: 2815
		private const int _maxChunkSize = 2048;

		// Token: 0x04000B00 RID: 2816
		private List<byte[]> _cachedBytes;
	}
}
