﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.ExceptionServices;
using System.Threading;
using System.Threading.Tasks;

namespace System.Data.SqlClient
{
	// Token: 0x020001D0 RID: 464
	internal static class AsyncHelper
	{
		// Token: 0x06001524 RID: 5412 RVA: 0x0006B9A8 File Offset: 0x00069BA8
		internal static Task CreateContinuationTask(Task task, Action onSuccess, SqlInternalConnectionTds connectionToDoom = null, Action<Exception> onFailure = null)
		{
			AsyncHelper.<>c__DisplayClass0_0 CS$<>8__locals1 = new AsyncHelper.<>c__DisplayClass0_0();
			CS$<>8__locals1.onSuccess = onSuccess;
			if (task == null)
			{
				CS$<>8__locals1.onSuccess();
				return null;
			}
			TaskCompletionSource<object> completion = new TaskCompletionSource<object>();
			AsyncHelper.ContinueTask(task, completion, delegate
			{
				CS$<>8__locals1.onSuccess();
				completion.SetResult(null);
			}, connectionToDoom, onFailure, null, null, null);
			return completion.Task;
		}

		// Token: 0x06001525 RID: 5413 RVA: 0x0006BA14 File Offset: 0x00069C14
		internal static Task CreateContinuationTask<T1, T2>(Task task, Action<T1, T2> onSuccess, T1 arg1, T2 arg2, SqlInternalConnectionTds connectionToDoom = null, Action<Exception> onFailure = null)
		{
			return AsyncHelper.CreateContinuationTask(task, delegate()
			{
				onSuccess(arg1, arg2);
			}, connectionToDoom, onFailure);
		}

		// Token: 0x06001526 RID: 5414 RVA: 0x0006BA54 File Offset: 0x00069C54
		internal static void ContinueTask(Task task, TaskCompletionSource<object> completion, Action onSuccess, SqlInternalConnectionTds connectionToDoom = null, Action<Exception> onFailure = null, Action onCancellation = null, Func<Exception, Exception> exceptionConverter = null, SqlConnection connectionToAbort = null)
		{
			task.ContinueWith(delegate(Task tsk)
			{
				if (tsk.Exception != null)
				{
					Exception ex = tsk.Exception.InnerException;
					if (exceptionConverter != null)
					{
						ex = exceptionConverter(ex);
					}
					try
					{
						if (onFailure != null)
						{
							onFailure(ex);
						}
						return;
					}
					finally
					{
						completion.TrySetException(ex);
					}
				}
				if (tsk.IsCanceled)
				{
					try
					{
						if (onCancellation != null)
						{
							onCancellation();
						}
						return;
					}
					finally
					{
						completion.TrySetCanceled();
					}
				}
				try
				{
					onSuccess();
				}
				catch (Exception exception)
				{
					completion.SetException(exception);
				}
			}, TaskScheduler.Default);
		}

		// Token: 0x06001527 RID: 5415 RVA: 0x0006BAA8 File Offset: 0x00069CA8
		internal static void WaitForCompletion(Task task, int timeout, Action onTimeout = null, bool rethrowExceptions = true)
		{
			try
			{
				task.Wait((timeout > 0) ? (1000 * timeout) : -1);
			}
			catch (AggregateException ex)
			{
				if (rethrowExceptions)
				{
					ExceptionDispatchInfo.Capture(ex.InnerException).Throw();
				}
			}
			if (!task.IsCompleted && onTimeout != null)
			{
				onTimeout();
			}
		}

		// Token: 0x06001528 RID: 5416 RVA: 0x0006BB04 File Offset: 0x00069D04
		internal static void SetTimeoutException(TaskCompletionSource<object> completion, int timeout, Func<Exception> exc, CancellationToken ctoken)
		{
			if (timeout > 0)
			{
				Task.Delay(timeout * 1000, ctoken).ContinueWith(delegate(Task tsk)
				{
					if (!tsk.IsCanceled && !completion.Task.IsCompleted)
					{
						completion.TrySetException(exc());
					}
				});
			}
		}

		// Token: 0x020001D1 RID: 465
		[CompilerGenerated]
		private sealed class <>c__DisplayClass0_0
		{
			// Token: 0x06001529 RID: 5417 RVA: 0x00005C14 File Offset: 0x00003E14
			public <>c__DisplayClass0_0()
			{
			}

			// Token: 0x04000EB4 RID: 3764
			public Action onSuccess;
		}

		// Token: 0x020001D2 RID: 466
		[CompilerGenerated]
		private sealed class <>c__DisplayClass0_1
		{
			// Token: 0x0600152A RID: 5418 RVA: 0x00005C14 File Offset: 0x00003E14
			public <>c__DisplayClass0_1()
			{
			}

			// Token: 0x0600152B RID: 5419 RVA: 0x0006BB48 File Offset: 0x00069D48
			internal void <CreateContinuationTask>b__0()
			{
				this.CS$<>8__locals1.onSuccess();
				this.completion.SetResult(null);
			}

			// Token: 0x04000EB5 RID: 3765
			public TaskCompletionSource<object> completion;

			// Token: 0x04000EB6 RID: 3766
			public AsyncHelper.<>c__DisplayClass0_0 CS$<>8__locals1;
		}

		// Token: 0x020001D3 RID: 467
		[CompilerGenerated]
		private sealed class <>c__DisplayClass1_0<T1, T2>
		{
			// Token: 0x0600152C RID: 5420 RVA: 0x00005C14 File Offset: 0x00003E14
			public <>c__DisplayClass1_0()
			{
			}

			// Token: 0x0600152D RID: 5421 RVA: 0x0006BB66 File Offset: 0x00069D66
			internal void <CreateContinuationTask>b__0()
			{
				this.onSuccess(this.arg1, this.arg2);
			}

			// Token: 0x04000EB7 RID: 3767
			public Action<T1, T2> onSuccess;

			// Token: 0x04000EB8 RID: 3768
			public T1 arg1;

			// Token: 0x04000EB9 RID: 3769
			public T2 arg2;
		}

		// Token: 0x020001D4 RID: 468
		[CompilerGenerated]
		private sealed class <>c__DisplayClass2_0
		{
			// Token: 0x0600152E RID: 5422 RVA: 0x00005C14 File Offset: 0x00003E14
			public <>c__DisplayClass2_0()
			{
			}

			// Token: 0x0600152F RID: 5423 RVA: 0x0006BB80 File Offset: 0x00069D80
			internal void <ContinueTask>b__0(Task tsk)
			{
				if (tsk.Exception != null)
				{
					Exception ex = tsk.Exception.InnerException;
					if (this.exceptionConverter != null)
					{
						ex = this.exceptionConverter(ex);
					}
					try
					{
						if (this.onFailure != null)
						{
							this.onFailure(ex);
						}
						return;
					}
					finally
					{
						this.completion.TrySetException(ex);
					}
				}
				if (tsk.IsCanceled)
				{
					try
					{
						if (this.onCancellation != null)
						{
							this.onCancellation();
						}
						return;
					}
					finally
					{
						this.completion.TrySetCanceled();
					}
				}
				try
				{
					this.onSuccess();
				}
				catch (Exception exception)
				{
					this.completion.SetException(exception);
				}
			}

			// Token: 0x04000EBA RID: 3770
			public Func<Exception, Exception> exceptionConverter;

			// Token: 0x04000EBB RID: 3771
			public Action<Exception> onFailure;

			// Token: 0x04000EBC RID: 3772
			public TaskCompletionSource<object> completion;

			// Token: 0x04000EBD RID: 3773
			public Action onCancellation;

			// Token: 0x04000EBE RID: 3774
			public Action onSuccess;
		}

		// Token: 0x020001D5 RID: 469
		[CompilerGenerated]
		private sealed class <>c__DisplayClass4_0
		{
			// Token: 0x06001530 RID: 5424 RVA: 0x00005C14 File Offset: 0x00003E14
			public <>c__DisplayClass4_0()
			{
			}

			// Token: 0x06001531 RID: 5425 RVA: 0x0006BC4C File Offset: 0x00069E4C
			internal void <SetTimeoutException>b__0(Task tsk)
			{
				if (!tsk.IsCanceled && !this.completion.Task.IsCompleted)
				{
					this.completion.TrySetException(this.exc());
				}
			}

			// Token: 0x04000EBF RID: 3775
			public TaskCompletionSource<object> completion;

			// Token: 0x04000EC0 RID: 3776
			public Func<Exception> exc;
		}
	}
}
