﻿using System;

namespace System.Data.SqlClient
{
	// Token: 0x02000200 RID: 512
	internal sealed class _SqlMetaData : SqlMetaDataPriv
	{
		// Token: 0x060016D3 RID: 5843 RVA: 0x00077D36 File Offset: 0x00075F36
		internal _SqlMetaData(int ordinal)
		{
			this.ordinal = ordinal;
		}

		// Token: 0x17000435 RID: 1077
		// (get) Token: 0x060016D4 RID: 5844 RVA: 0x00077D45 File Offset: 0x00075F45
		internal string serverName
		{
			get
			{
				return this.multiPartTableName.ServerName;
			}
		}

		// Token: 0x17000436 RID: 1078
		// (get) Token: 0x060016D5 RID: 5845 RVA: 0x00077D52 File Offset: 0x00075F52
		internal string catalogName
		{
			get
			{
				return this.multiPartTableName.CatalogName;
			}
		}

		// Token: 0x17000437 RID: 1079
		// (get) Token: 0x060016D6 RID: 5846 RVA: 0x00077D5F File Offset: 0x00075F5F
		internal string schemaName
		{
			get
			{
				return this.multiPartTableName.SchemaName;
			}
		}

		// Token: 0x17000438 RID: 1080
		// (get) Token: 0x060016D7 RID: 5847 RVA: 0x00077D6C File Offset: 0x00075F6C
		internal string tableName
		{
			get
			{
				return this.multiPartTableName.TableName;
			}
		}

		// Token: 0x17000439 RID: 1081
		// (get) Token: 0x060016D8 RID: 5848 RVA: 0x00077D79 File Offset: 0x00075F79
		internal bool IsNewKatmaiDateTimeType
		{
			get
			{
				return SqlDbType.Date == this.type || SqlDbType.Time == this.type || SqlDbType.DateTime2 == this.type || SqlDbType.DateTimeOffset == this.type;
			}
		}

		// Token: 0x1700043A RID: 1082
		// (get) Token: 0x060016D9 RID: 5849 RVA: 0x00077DA5 File Offset: 0x00075FA5
		internal bool IsLargeUdt
		{
			get
			{
				return this.type == SqlDbType.Udt && this.length == int.MaxValue;
			}
		}

		// Token: 0x060016DA RID: 5850 RVA: 0x00077DC0 File Offset: 0x00075FC0
		public object Clone()
		{
			_SqlMetaData sqlMetaData = new _SqlMetaData(this.ordinal);
			sqlMetaData.CopyFrom(this);
			sqlMetaData.column = this.column;
			sqlMetaData.baseColumn = this.baseColumn;
			sqlMetaData.multiPartTableName = this.multiPartTableName;
			sqlMetaData.updatability = this.updatability;
			sqlMetaData.tableNum = this.tableNum;
			sqlMetaData.isDifferentName = this.isDifferentName;
			sqlMetaData.isKey = this.isKey;
			sqlMetaData.isHidden = this.isHidden;
			sqlMetaData.isExpression = this.isExpression;
			sqlMetaData.isIdentity = this.isIdentity;
			sqlMetaData.isColumnSet = this.isColumnSet;
			sqlMetaData.op = this.op;
			sqlMetaData.operand = this.operand;
			return sqlMetaData;
		}

		// Token: 0x0400111E RID: 4382
		internal string column;

		// Token: 0x0400111F RID: 4383
		internal string baseColumn;

		// Token: 0x04001120 RID: 4384
		internal MultiPartTableName multiPartTableName;

		// Token: 0x04001121 RID: 4385
		internal readonly int ordinal;

		// Token: 0x04001122 RID: 4386
		internal byte updatability;

		// Token: 0x04001123 RID: 4387
		internal byte tableNum;

		// Token: 0x04001124 RID: 4388
		internal bool isDifferentName;

		// Token: 0x04001125 RID: 4389
		internal bool isKey;

		// Token: 0x04001126 RID: 4390
		internal bool isHidden;

		// Token: 0x04001127 RID: 4391
		internal bool isExpression;

		// Token: 0x04001128 RID: 4392
		internal bool isIdentity;

		// Token: 0x04001129 RID: 4393
		internal bool isColumnSet;

		// Token: 0x0400112A RID: 4394
		internal byte op;

		// Token: 0x0400112B RID: 4395
		internal ushort operand;
	}
}
