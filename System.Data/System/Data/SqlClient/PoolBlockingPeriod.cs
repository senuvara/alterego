﻿using System;

namespace System.Data.SqlClient
{
	/// <summary>Specifies a value for PoolBlockingPeriod Property. </summary>
	// Token: 0x02000220 RID: 544
	public enum PoolBlockingPeriod
	{
		/// <summary>Blocking period OFF for Azure SQL servers, but ON for all other SQL servers.</summary>
		// Token: 0x040011FC RID: 4604
		Auto,
		/// <summary>Blocking period ON for all SQL servers including Azure SQL servers.</summary>
		// Token: 0x040011FD RID: 4605
		AlwaysBlock,
		/// <summary>Blocking period OFF for all SQL servers including Azure SQL servers.</summary>
		// Token: 0x040011FE RID: 4606
		NeverBlock
	}
}
