﻿using System;
using Unity;

namespace System.Data.SqlClient
{
	/// <summary>The CMK Store provider implementation for using the Microsoft Cryptography API: Next Generation (CNG) with Always Encrypted.   </summary>
	// Token: 0x02000355 RID: 853
	public class SqlColumnEncryptionCngProvider : SqlColumnEncryptionKeyStoreProvider
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Data.SqlClient.SqlColumnEncryptionCngProvider" /> class.</summary>
		// Token: 0x06002977 RID: 10615 RVA: 0x00010458 File Offset: 0x0000E658
		public SqlColumnEncryptionCngProvider()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Decrypts the given encrypted value using an asymmetric key specified by the key path and the specified algorithm. The key path will be in the format of [ProviderName]/KeyIdentifier and should be an asymmetric key stored in the specified CNG key store provider. The valid algorithm used to encrypt/decrypt the CEK is 'RSA_OAEP'.</summary>
		/// <param name="masterKeyPath">The master key path.</param>
		/// <param name="encryptionAlgorithm">The encryption algorithm.</param>
		/// <param name="encryptedColumnEncryptionKey">The encrypted column encryption key.</param>
		/// <returns>The decrypted column encryption key.</returns>
		// Token: 0x06002978 RID: 10616 RVA: 0x00051759 File Offset: 0x0004F959
		public override byte[] DecryptColumnEncryptionKey(string masterKeyPath, string encryptionAlgorithm, byte[] encryptedColumnEncryptionKey)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Encrypts the given plain text column encryption key using an asymmetric key specified by the key path and the specified algorithm. The key path will be in the format of [ProviderName]/KeyIdentifier and should be an asymmetric key stored in the specified CNG key store provider. The valid algorithm used to encrypt/decrypt the CEK is 'RSA_OAEP'.</summary>
		/// <param name="masterKeyPath">The master key path.</param>
		/// <param name="encryptionAlgorithm">The encryption algorithm.</param>
		/// <param name="columnEncryptionKey">The encrypted column encryption key.</param>
		/// <returns>The encrypted column encryption key.</returns>
		// Token: 0x06002979 RID: 10617 RVA: 0x00051759 File Offset: 0x0004F959
		public override byte[] EncryptColumnEncryptionKey(string masterKeyPath, string encryptionAlgorithm, byte[] columnEncryptionKey)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>A constant string for the provider name 'MSSQL_CNG_STORE'.</summary>
		/// <returns>The provider name.</returns>
		// Token: 0x0400191C RID: 6428
		public const string ProviderName = "MSSQL_CNG_STORE";
	}
}
