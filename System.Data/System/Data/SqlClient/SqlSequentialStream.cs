﻿using System;
using System.Data.Common;
using System.IO;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;

namespace System.Data.SqlClient
{
	// Token: 0x020001C1 RID: 449
	internal sealed class SqlSequentialStream : Stream
	{
		// Token: 0x0600149D RID: 5277 RVA: 0x00069784 File Offset: 0x00067984
		internal SqlSequentialStream(SqlDataReader reader, int columnIndex)
		{
			this._reader = reader;
			this._columnIndex = columnIndex;
			this._currentTask = null;
			this._disposalTokenSource = new CancellationTokenSource();
			if (reader.Command != null && reader.Command.CommandTimeout != 0)
			{
				this._readTimeout = (int)Math.Min((long)reader.Command.CommandTimeout * 1000L, 2147483647L);
				return;
			}
			this._readTimeout = -1;
		}

		// Token: 0x170003F4 RID: 1012
		// (get) Token: 0x0600149E RID: 5278 RVA: 0x000697F9 File Offset: 0x000679F9
		public override bool CanRead
		{
			get
			{
				return this._reader != null && !this._reader.IsClosed;
			}
		}

		// Token: 0x170003F5 RID: 1013
		// (get) Token: 0x0600149F RID: 5279 RVA: 0x000061C5 File Offset: 0x000043C5
		public override bool CanSeek
		{
			get
			{
				return false;
			}
		}

		// Token: 0x170003F6 RID: 1014
		// (get) Token: 0x060014A0 RID: 5280 RVA: 0x0000EF1B File Offset: 0x0000D11B
		public override bool CanTimeout
		{
			get
			{
				return true;
			}
		}

		// Token: 0x170003F7 RID: 1015
		// (get) Token: 0x060014A1 RID: 5281 RVA: 0x000061C5 File Offset: 0x000043C5
		public override bool CanWrite
		{
			get
			{
				return false;
			}
		}

		// Token: 0x060014A2 RID: 5282 RVA: 0x00005E03 File Offset: 0x00004003
		public override void Flush()
		{
		}

		// Token: 0x170003F8 RID: 1016
		// (get) Token: 0x060014A3 RID: 5283 RVA: 0x0005CDBE File Offset: 0x0005AFBE
		public override long Length
		{
			get
			{
				throw ADP.NotSupported();
			}
		}

		// Token: 0x170003F9 RID: 1017
		// (get) Token: 0x060014A4 RID: 5284 RVA: 0x0005CDBE File Offset: 0x0005AFBE
		// (set) Token: 0x060014A5 RID: 5285 RVA: 0x0005CDBE File Offset: 0x0005AFBE
		public override long Position
		{
			get
			{
				throw ADP.NotSupported();
			}
			set
			{
				throw ADP.NotSupported();
			}
		}

		// Token: 0x170003FA RID: 1018
		// (get) Token: 0x060014A6 RID: 5286 RVA: 0x00069813 File Offset: 0x00067A13
		// (set) Token: 0x060014A7 RID: 5287 RVA: 0x0006981B File Offset: 0x00067A1B
		public override int ReadTimeout
		{
			get
			{
				return this._readTimeout;
			}
			set
			{
				if (value > 0 || value == -1)
				{
					this._readTimeout = value;
					return;
				}
				throw ADP.ArgumentOutOfRange("value");
			}
		}

		// Token: 0x170003FB RID: 1019
		// (get) Token: 0x060014A8 RID: 5288 RVA: 0x00069837 File Offset: 0x00067A37
		internal int ColumnIndex
		{
			get
			{
				return this._columnIndex;
			}
		}

		// Token: 0x060014A9 RID: 5289 RVA: 0x00069840 File Offset: 0x00067A40
		public override int Read(byte[] buffer, int offset, int count)
		{
			SqlSequentialStream.ValidateReadParameters(buffer, offset, count);
			if (!this.CanRead)
			{
				throw ADP.ObjectDisposed(this);
			}
			if (this._currentTask != null)
			{
				throw ADP.AsyncOperationPending();
			}
			int bytesInternalSequential;
			try
			{
				bytesInternalSequential = this._reader.GetBytesInternalSequential(this._columnIndex, buffer, offset, count, new long?((long)this._readTimeout));
			}
			catch (SqlException internalException)
			{
				throw ADP.ErrorReadingFromStream(internalException);
			}
			return bytesInternalSequential;
		}

		// Token: 0x060014AA RID: 5290 RVA: 0x000698B0 File Offset: 0x00067AB0
		public override Task<int> ReadAsync(byte[] buffer, int offset, int count, CancellationToken cancellationToken)
		{
			SqlSequentialStream.ValidateReadParameters(buffer, offset, count);
			TaskCompletionSource<int> completion = new TaskCompletionSource<int>();
			if (!this.CanRead)
			{
				completion.SetException(ADP.ExceptionWithStackTrace(ADP.ObjectDisposed(this)));
			}
			else
			{
				try
				{
					if (Interlocked.CompareExchange<Task>(ref this._currentTask, completion.Task, null) != null)
					{
						completion.SetException(ADP.ExceptionWithStackTrace(ADP.AsyncOperationPending()));
					}
					else
					{
						CancellationTokenSource combinedTokenSource;
						if (!cancellationToken.CanBeCanceled)
						{
							combinedTokenSource = this._disposalTokenSource;
						}
						else
						{
							combinedTokenSource = CancellationTokenSource.CreateLinkedTokenSource(cancellationToken, this._disposalTokenSource.Token);
						}
						int result = 0;
						Task<int> task = null;
						SqlDataReader reader = this._reader;
						if (reader != null && !cancellationToken.IsCancellationRequested && !this._disposalTokenSource.Token.IsCancellationRequested)
						{
							task = reader.GetBytesAsync(this._columnIndex, buffer, offset, count, this._readTimeout, combinedTokenSource.Token, out result);
						}
						if (task == null)
						{
							this._currentTask = null;
							if (cancellationToken.IsCancellationRequested)
							{
								completion.SetCanceled();
							}
							else if (!this.CanRead)
							{
								completion.SetException(ADP.ExceptionWithStackTrace(ADP.ObjectDisposed(this)));
							}
							else
							{
								completion.SetResult(result);
							}
							if (combinedTokenSource != this._disposalTokenSource)
							{
								combinedTokenSource.Dispose();
							}
						}
						else
						{
							task.ContinueWith(delegate(Task<int> t)
							{
								this._currentTask = null;
								if (t.Status == TaskStatus.RanToCompletion && this.CanRead)
								{
									completion.SetResult(t.Result);
								}
								else if (t.Status == TaskStatus.Faulted)
								{
									if (t.Exception.InnerException is SqlException)
									{
										completion.SetException(ADP.ExceptionWithStackTrace(ADP.ErrorReadingFromStream(t.Exception.InnerException)));
									}
									else
									{
										completion.SetException(t.Exception.InnerException);
									}
								}
								else if (!this.CanRead)
								{
									completion.SetException(ADP.ExceptionWithStackTrace(ADP.ObjectDisposed(this)));
								}
								else
								{
									completion.SetCanceled();
								}
								if (combinedTokenSource != this._disposalTokenSource)
								{
									combinedTokenSource.Dispose();
								}
							}, TaskScheduler.Default);
						}
					}
				}
				catch (Exception exception)
				{
					completion.TrySetException(exception);
					Interlocked.CompareExchange<Task>(ref this._currentTask, null, completion.Task);
					throw;
				}
			}
			return completion.Task;
		}

		// Token: 0x060014AB RID: 5291 RVA: 0x00069AA4 File Offset: 0x00067CA4
		public override IAsyncResult BeginRead(byte[] array, int offset, int count, AsyncCallback asyncCallback, object asyncState)
		{
			return TaskToApm.Begin(this.ReadAsync(array, offset, count, CancellationToken.None), asyncCallback, asyncState);
		}

		// Token: 0x060014AC RID: 5292 RVA: 0x00069ABD File Offset: 0x00067CBD
		public override int EndRead(IAsyncResult asyncResult)
		{
			return TaskToApm.End<int>(asyncResult);
		}

		// Token: 0x060014AD RID: 5293 RVA: 0x0005CDBE File Offset: 0x0005AFBE
		public override long Seek(long offset, SeekOrigin origin)
		{
			throw ADP.NotSupported();
		}

		// Token: 0x060014AE RID: 5294 RVA: 0x0005CDBE File Offset: 0x0005AFBE
		public override void SetLength(long value)
		{
			throw ADP.NotSupported();
		}

		// Token: 0x060014AF RID: 5295 RVA: 0x0005CDBE File Offset: 0x0005AFBE
		public override void Write(byte[] buffer, int offset, int count)
		{
			throw ADP.NotSupported();
		}

		// Token: 0x060014B0 RID: 5296 RVA: 0x00069AC8 File Offset: 0x00067CC8
		internal void SetClosed()
		{
			this._disposalTokenSource.Cancel();
			this._reader = null;
			Task currentTask = this._currentTask;
			if (currentTask != null)
			{
				((IAsyncResult)currentTask).AsyncWaitHandle.WaitOne();
			}
		}

		// Token: 0x060014B1 RID: 5297 RVA: 0x00069AFD File Offset: 0x00067CFD
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				this.SetClosed();
			}
			base.Dispose(disposing);
		}

		// Token: 0x060014B2 RID: 5298 RVA: 0x00069B10 File Offset: 0x00067D10
		internal static void ValidateReadParameters(byte[] buffer, int offset, int count)
		{
			if (buffer == null)
			{
				throw ADP.ArgumentNull("buffer");
			}
			if (offset < 0)
			{
				throw ADP.ArgumentOutOfRange("offset");
			}
			if (count < 0)
			{
				throw ADP.ArgumentOutOfRange("count");
			}
			try
			{
				if (checked(offset + count) > buffer.Length)
				{
					throw ExceptionBuilder.InvalidOffsetLength();
				}
			}
			catch (OverflowException)
			{
				throw ExceptionBuilder.InvalidOffsetLength();
			}
		}

		// Token: 0x04000E64 RID: 3684
		private SqlDataReader _reader;

		// Token: 0x04000E65 RID: 3685
		private int _columnIndex;

		// Token: 0x04000E66 RID: 3686
		private Task _currentTask;

		// Token: 0x04000E67 RID: 3687
		private int _readTimeout;

		// Token: 0x04000E68 RID: 3688
		private CancellationTokenSource _disposalTokenSource;

		// Token: 0x020001C2 RID: 450
		[CompilerGenerated]
		private sealed class <>c__DisplayClass26_0
		{
			// Token: 0x060014B3 RID: 5299 RVA: 0x00005C14 File Offset: 0x00003E14
			public <>c__DisplayClass26_0()
			{
			}

			// Token: 0x04000E69 RID: 3689
			public SqlSequentialStream <>4__this;

			// Token: 0x04000E6A RID: 3690
			public TaskCompletionSource<int> completion;
		}

		// Token: 0x020001C3 RID: 451
		[CompilerGenerated]
		private sealed class <>c__DisplayClass26_1
		{
			// Token: 0x060014B4 RID: 5300 RVA: 0x00005C14 File Offset: 0x00003E14
			public <>c__DisplayClass26_1()
			{
			}

			// Token: 0x060014B5 RID: 5301 RVA: 0x00069B74 File Offset: 0x00067D74
			internal void <ReadAsync>b__0(Task<int> t)
			{
				this.CS$<>8__locals1.<>4__this._currentTask = null;
				if (t.Status == TaskStatus.RanToCompletion && this.CS$<>8__locals1.<>4__this.CanRead)
				{
					this.CS$<>8__locals1.completion.SetResult(t.Result);
				}
				else if (t.Status == TaskStatus.Faulted)
				{
					if (t.Exception.InnerException is SqlException)
					{
						this.CS$<>8__locals1.completion.SetException(ADP.ExceptionWithStackTrace(ADP.ErrorReadingFromStream(t.Exception.InnerException)));
					}
					else
					{
						this.CS$<>8__locals1.completion.SetException(t.Exception.InnerException);
					}
				}
				else if (!this.CS$<>8__locals1.<>4__this.CanRead)
				{
					this.CS$<>8__locals1.completion.SetException(ADP.ExceptionWithStackTrace(ADP.ObjectDisposed(this.CS$<>8__locals1.<>4__this)));
				}
				else
				{
					this.CS$<>8__locals1.completion.SetCanceled();
				}
				if (this.combinedTokenSource != this.CS$<>8__locals1.<>4__this._disposalTokenSource)
				{
					this.combinedTokenSource.Dispose();
				}
			}

			// Token: 0x04000E6B RID: 3691
			public CancellationTokenSource combinedTokenSource;

			// Token: 0x04000E6C RID: 3692
			public SqlSequentialStream.<>c__DisplayClass26_0 CS$<>8__locals1;
		}
	}
}
