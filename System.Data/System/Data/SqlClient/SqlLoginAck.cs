﻿using System;

namespace System.Data.SqlClient
{
	// Token: 0x020001FF RID: 511
	internal sealed class SqlLoginAck
	{
		// Token: 0x060016D2 RID: 5842 RVA: 0x00005C14 File Offset: 0x00003E14
		public SqlLoginAck()
		{
		}

		// Token: 0x0400111A RID: 4378
		internal byte majorVersion;

		// Token: 0x0400111B RID: 4379
		internal byte minorVersion;

		// Token: 0x0400111C RID: 4380
		internal short buildNum;

		// Token: 0x0400111D RID: 4381
		internal uint tdsVersion;
	}
}
