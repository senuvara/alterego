﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data.Common;
using System.Linq;
using System.Runtime.CompilerServices;
using Unity;

namespace System.Data.SqlClient
{
	/// <summary>Provides a simple way to create and manage the contents of connection strings used by the <see cref="T:System.Data.SqlClient.SqlConnection" /> class. </summary>
	// Token: 0x0200017C RID: 380
	public sealed class SqlConnectionStringBuilder : DbConnectionStringBuilder
	{
		// Token: 0x06001155 RID: 4437 RVA: 0x000592D4 File Offset: 0x000574D4
		private static string[] CreateValidKeywords()
		{
			string[] array = new string[29];
			array[25] = "ApplicationIntent";
			array[20] = "Application Name";
			array[2] = "AttachDbFilename";
			array[14] = "Connect Timeout";
			array[21] = "Current Language";
			array[0] = "Data Source";
			array[15] = "Encrypt";
			array[8] = "Enlist";
			array[1] = "Failover Partner";
			array[3] = "Initial Catalog";
			array[4] = "Integrated Security";
			array[17] = "Load Balance Timeout";
			array[11] = "Max Pool Size";
			array[10] = "Min Pool Size";
			array[12] = "MultipleActiveResultSets";
			array[26] = "MultiSubnetFailover";
			array[18] = "Packet Size";
			array[7] = "Password";
			array[5] = "Persist Security Info";
			array[9] = "Pooling";
			array[13] = "Replication";
			array[24] = "Transaction Binding";
			array[16] = "TrustServerCertificate";
			array[19] = "Type System Version";
			array[6] = "User ID";
			array[23] = "User Instance";
			array[22] = "Workstation ID";
			array[27] = "ConnectRetryCount";
			array[28] = "ConnectRetryInterval";
			return array;
		}

		// Token: 0x06001156 RID: 4438 RVA: 0x000593E4 File Offset: 0x000575E4
		private static Dictionary<string, SqlConnectionStringBuilder.Keywords> CreateKeywordsDictionary()
		{
			return new Dictionary<string, SqlConnectionStringBuilder.Keywords>(47, StringComparer.OrdinalIgnoreCase)
			{
				{
					"ApplicationIntent",
					SqlConnectionStringBuilder.Keywords.ApplicationIntent
				},
				{
					"Application Name",
					SqlConnectionStringBuilder.Keywords.ApplicationName
				},
				{
					"AttachDbFilename",
					SqlConnectionStringBuilder.Keywords.AttachDBFilename
				},
				{
					"Connect Timeout",
					SqlConnectionStringBuilder.Keywords.ConnectTimeout
				},
				{
					"Current Language",
					SqlConnectionStringBuilder.Keywords.CurrentLanguage
				},
				{
					"Data Source",
					SqlConnectionStringBuilder.Keywords.DataSource
				},
				{
					"Encrypt",
					SqlConnectionStringBuilder.Keywords.Encrypt
				},
				{
					"Enlist",
					SqlConnectionStringBuilder.Keywords.Enlist
				},
				{
					"Failover Partner",
					SqlConnectionStringBuilder.Keywords.FailoverPartner
				},
				{
					"Initial Catalog",
					SqlConnectionStringBuilder.Keywords.InitialCatalog
				},
				{
					"Integrated Security",
					SqlConnectionStringBuilder.Keywords.IntegratedSecurity
				},
				{
					"Load Balance Timeout",
					SqlConnectionStringBuilder.Keywords.LoadBalanceTimeout
				},
				{
					"MultipleActiveResultSets",
					SqlConnectionStringBuilder.Keywords.MultipleActiveResultSets
				},
				{
					"Max Pool Size",
					SqlConnectionStringBuilder.Keywords.MaxPoolSize
				},
				{
					"Min Pool Size",
					SqlConnectionStringBuilder.Keywords.MinPoolSize
				},
				{
					"MultiSubnetFailover",
					SqlConnectionStringBuilder.Keywords.MultiSubnetFailover
				},
				{
					"Packet Size",
					SqlConnectionStringBuilder.Keywords.PacketSize
				},
				{
					"Password",
					SqlConnectionStringBuilder.Keywords.Password
				},
				{
					"Persist Security Info",
					SqlConnectionStringBuilder.Keywords.PersistSecurityInfo
				},
				{
					"Pooling",
					SqlConnectionStringBuilder.Keywords.Pooling
				},
				{
					"Replication",
					SqlConnectionStringBuilder.Keywords.Replication
				},
				{
					"Transaction Binding",
					SqlConnectionStringBuilder.Keywords.TransactionBinding
				},
				{
					"TrustServerCertificate",
					SqlConnectionStringBuilder.Keywords.TrustServerCertificate
				},
				{
					"Type System Version",
					SqlConnectionStringBuilder.Keywords.TypeSystemVersion
				},
				{
					"User ID",
					SqlConnectionStringBuilder.Keywords.UserID
				},
				{
					"User Instance",
					SqlConnectionStringBuilder.Keywords.UserInstance
				},
				{
					"Workstation ID",
					SqlConnectionStringBuilder.Keywords.WorkstationID
				},
				{
					"ConnectRetryCount",
					SqlConnectionStringBuilder.Keywords.ConnectRetryCount
				},
				{
					"ConnectRetryInterval",
					SqlConnectionStringBuilder.Keywords.ConnectRetryInterval
				},
				{
					"app",
					SqlConnectionStringBuilder.Keywords.ApplicationName
				},
				{
					"extended properties",
					SqlConnectionStringBuilder.Keywords.AttachDBFilename
				},
				{
					"initial file name",
					SqlConnectionStringBuilder.Keywords.AttachDBFilename
				},
				{
					"connection timeout",
					SqlConnectionStringBuilder.Keywords.ConnectTimeout
				},
				{
					"timeout",
					SqlConnectionStringBuilder.Keywords.ConnectTimeout
				},
				{
					"language",
					SqlConnectionStringBuilder.Keywords.CurrentLanguage
				},
				{
					"addr",
					SqlConnectionStringBuilder.Keywords.DataSource
				},
				{
					"address",
					SqlConnectionStringBuilder.Keywords.DataSource
				},
				{
					"network address",
					SqlConnectionStringBuilder.Keywords.DataSource
				},
				{
					"server",
					SqlConnectionStringBuilder.Keywords.DataSource
				},
				{
					"database",
					SqlConnectionStringBuilder.Keywords.InitialCatalog
				},
				{
					"trusted_connection",
					SqlConnectionStringBuilder.Keywords.IntegratedSecurity
				},
				{
					"connection lifetime",
					SqlConnectionStringBuilder.Keywords.LoadBalanceTimeout
				},
				{
					"pwd",
					SqlConnectionStringBuilder.Keywords.Password
				},
				{
					"persistsecurityinfo",
					SqlConnectionStringBuilder.Keywords.PersistSecurityInfo
				},
				{
					"uid",
					SqlConnectionStringBuilder.Keywords.UserID
				},
				{
					"user",
					SqlConnectionStringBuilder.Keywords.UserID
				},
				{
					"wsid",
					SqlConnectionStringBuilder.Keywords.WorkstationID
				}
			};
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Data.SqlClient.SqlConnectionStringBuilder" /> class.</summary>
		// Token: 0x06001157 RID: 4439 RVA: 0x0005964B File Offset: 0x0005784B
		public SqlConnectionStringBuilder() : this(null)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Data.SqlClient.SqlConnectionStringBuilder" /> class. The provided connection string provides the data for the instance's internal connection information.</summary>
		/// <param name="connectionString">The basis for the object's internal connection information. Parsed into name/value pairs. Invalid key names raise <see cref="T:System.Collections.Generic.KeyNotFoundException" />.</param>
		/// <exception cref="T:System.Collections.Generic.KeyNotFoundException">Invalid key name within the connection string.</exception>
		/// <exception cref="T:System.FormatException">Invalid value within the connection string (specifically, when a Boolean or numeric value was expected but not supplied).</exception>
		/// <exception cref="T:System.ArgumentException">The supplied <paramref name="connectionString" /> is not valid.</exception>
		// Token: 0x06001158 RID: 4440 RVA: 0x00059654 File Offset: 0x00057854
		public SqlConnectionStringBuilder(string connectionString)
		{
			if (!string.IsNullOrEmpty(connectionString))
			{
				base.ConnectionString = connectionString;
			}
		}

		/// <summary>Gets or sets the value associated with the specified key. In C#, this property is the indexer.</summary>
		/// <param name="keyword">The key of the item to get or set.</param>
		/// <returns>The value associated with the specified key. </returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="keyword" /> is a null reference (<see langword="Nothing" /> in Visual Basic).</exception>
		/// <exception cref="T:System.Collections.Generic.KeyNotFoundException">Tried to add a key that does not exist within the available keys.</exception>
		/// <exception cref="T:System.FormatException">Invalid value within the connection string (specifically, a Boolean or numeric value was expected but not supplied).</exception>
		// Token: 0x17000321 RID: 801
		public override object this[string keyword]
		{
			get
			{
				SqlConnectionStringBuilder.Keywords index = this.GetIndex(keyword);
				return this.GetAt(index);
			}
			set
			{
				if (value == null)
				{
					this.Remove(keyword);
					return;
				}
				switch (this.GetIndex(keyword))
				{
				case SqlConnectionStringBuilder.Keywords.DataSource:
					this.DataSource = SqlConnectionStringBuilder.ConvertToString(value);
					return;
				case SqlConnectionStringBuilder.Keywords.FailoverPartner:
					this.FailoverPartner = SqlConnectionStringBuilder.ConvertToString(value);
					return;
				case SqlConnectionStringBuilder.Keywords.AttachDBFilename:
					this.AttachDBFilename = SqlConnectionStringBuilder.ConvertToString(value);
					return;
				case SqlConnectionStringBuilder.Keywords.InitialCatalog:
					this.InitialCatalog = SqlConnectionStringBuilder.ConvertToString(value);
					return;
				case SqlConnectionStringBuilder.Keywords.IntegratedSecurity:
					this.IntegratedSecurity = SqlConnectionStringBuilder.ConvertToIntegratedSecurity(value);
					return;
				case SqlConnectionStringBuilder.Keywords.PersistSecurityInfo:
					this.PersistSecurityInfo = SqlConnectionStringBuilder.ConvertToBoolean(value);
					return;
				case SqlConnectionStringBuilder.Keywords.UserID:
					this.UserID = SqlConnectionStringBuilder.ConvertToString(value);
					return;
				case SqlConnectionStringBuilder.Keywords.Password:
					this.Password = SqlConnectionStringBuilder.ConvertToString(value);
					return;
				case SqlConnectionStringBuilder.Keywords.Enlist:
					this.Enlist = SqlConnectionStringBuilder.ConvertToBoolean(value);
					return;
				case SqlConnectionStringBuilder.Keywords.Pooling:
					this.Pooling = SqlConnectionStringBuilder.ConvertToBoolean(value);
					return;
				case SqlConnectionStringBuilder.Keywords.MinPoolSize:
					this.MinPoolSize = SqlConnectionStringBuilder.ConvertToInt32(value);
					return;
				case SqlConnectionStringBuilder.Keywords.MaxPoolSize:
					this.MaxPoolSize = SqlConnectionStringBuilder.ConvertToInt32(value);
					return;
				case SqlConnectionStringBuilder.Keywords.MultipleActiveResultSets:
					this.MultipleActiveResultSets = SqlConnectionStringBuilder.ConvertToBoolean(value);
					return;
				case SqlConnectionStringBuilder.Keywords.Replication:
					this.Replication = SqlConnectionStringBuilder.ConvertToBoolean(value);
					return;
				case SqlConnectionStringBuilder.Keywords.ConnectTimeout:
					this.ConnectTimeout = SqlConnectionStringBuilder.ConvertToInt32(value);
					return;
				case SqlConnectionStringBuilder.Keywords.Encrypt:
					this.Encrypt = SqlConnectionStringBuilder.ConvertToBoolean(value);
					return;
				case SqlConnectionStringBuilder.Keywords.TrustServerCertificate:
					this.TrustServerCertificate = SqlConnectionStringBuilder.ConvertToBoolean(value);
					return;
				case SqlConnectionStringBuilder.Keywords.LoadBalanceTimeout:
					this.LoadBalanceTimeout = SqlConnectionStringBuilder.ConvertToInt32(value);
					return;
				case SqlConnectionStringBuilder.Keywords.PacketSize:
					this.PacketSize = SqlConnectionStringBuilder.ConvertToInt32(value);
					return;
				case SqlConnectionStringBuilder.Keywords.TypeSystemVersion:
					this.TypeSystemVersion = SqlConnectionStringBuilder.ConvertToString(value);
					return;
				case SqlConnectionStringBuilder.Keywords.ApplicationName:
					this.ApplicationName = SqlConnectionStringBuilder.ConvertToString(value);
					return;
				case SqlConnectionStringBuilder.Keywords.CurrentLanguage:
					this.CurrentLanguage = SqlConnectionStringBuilder.ConvertToString(value);
					return;
				case SqlConnectionStringBuilder.Keywords.WorkstationID:
					this.WorkstationID = SqlConnectionStringBuilder.ConvertToString(value);
					return;
				case SqlConnectionStringBuilder.Keywords.UserInstance:
					this.UserInstance = SqlConnectionStringBuilder.ConvertToBoolean(value);
					return;
				case SqlConnectionStringBuilder.Keywords.TransactionBinding:
					this.TransactionBinding = SqlConnectionStringBuilder.ConvertToString(value);
					return;
				case SqlConnectionStringBuilder.Keywords.ApplicationIntent:
					this.ApplicationIntent = SqlConnectionStringBuilder.ConvertToApplicationIntent(keyword, value);
					return;
				case SqlConnectionStringBuilder.Keywords.MultiSubnetFailover:
					this.MultiSubnetFailover = SqlConnectionStringBuilder.ConvertToBoolean(value);
					return;
				case SqlConnectionStringBuilder.Keywords.ConnectRetryCount:
					this.ConnectRetryCount = SqlConnectionStringBuilder.ConvertToInt32(value);
					return;
				case SqlConnectionStringBuilder.Keywords.ConnectRetryInterval:
					this.ConnectRetryInterval = SqlConnectionStringBuilder.ConvertToInt32(value);
					return;
				default:
					throw this.UnsupportedKeyword(keyword);
				}
			}
		}

		/// <summary>Declares the application workload type when connecting to a database in an SQL Server Availability Group. You can set the value of this property with <see cref="T:System.Data.SqlClient.ApplicationIntent" />. For more information about SqlClient support for Always On Availability Groups, see SqlClient Support for High Availability, Disaster Recovery.</summary>
		/// <returns>Returns the current value of the property (a value of type <see cref="T:System.Data.SqlClient.ApplicationIntent" />).</returns>
		// Token: 0x17000322 RID: 802
		// (get) Token: 0x0600115B RID: 4443 RVA: 0x00059968 File Offset: 0x00057B68
		// (set) Token: 0x0600115C RID: 4444 RVA: 0x00059970 File Offset: 0x00057B70
		public ApplicationIntent ApplicationIntent
		{
			get
			{
				return this._applicationIntent;
			}
			set
			{
				if (!DbConnectionStringBuilderUtil.IsValidApplicationIntentValue(value))
				{
					throw ADP.InvalidEnumerationValue(typeof(ApplicationIntent), (int)value);
				}
				this.SetApplicationIntentValue(value);
				this._applicationIntent = value;
			}
		}

		/// <summary>Gets or sets the name of the application associated with the connection string.</summary>
		/// <returns>The name of the application, or ".NET SqlClient Data Provider" if no name has been supplied.</returns>
		/// <exception cref="T:System.ArgumentNullException">To set the value to null, use <see cref="F:System.DBNull.Value" />.</exception>
		// Token: 0x17000323 RID: 803
		// (get) Token: 0x0600115D RID: 4445 RVA: 0x00059999 File Offset: 0x00057B99
		// (set) Token: 0x0600115E RID: 4446 RVA: 0x000599A1 File Offset: 0x00057BA1
		public string ApplicationName
		{
			get
			{
				return this._applicationName;
			}
			set
			{
				this.SetValue("Application Name", value);
				this._applicationName = value;
			}
		}

		/// <summary>Gets or sets a string that contains the name of the primary data file. This includes the full path name of an attachable database.</summary>
		/// <returns>The value of the <see langword="AttachDBFilename" /> property, or <see langword="String.Empty" /> if no value has been supplied.</returns>
		/// <exception cref="T:System.ArgumentNullException">To set the value to null, use <see cref="F:System.DBNull.Value" />.</exception>
		// Token: 0x17000324 RID: 804
		// (get) Token: 0x0600115F RID: 4447 RVA: 0x000599B6 File Offset: 0x00057BB6
		// (set) Token: 0x06001160 RID: 4448 RVA: 0x000599BE File Offset: 0x00057BBE
		public string AttachDBFilename
		{
			get
			{
				return this._attachDBFilename;
			}
			set
			{
				this.SetValue("AttachDbFilename", value);
				this._attachDBFilename = value;
			}
		}

		/// <summary>Gets or sets the length of time (in seconds) to wait for a connection to the server before terminating the attempt and generating an error.</summary>
		/// <returns>The value of the <see cref="P:System.Data.SqlClient.SqlConnectionStringBuilder.ConnectTimeout" /> property, or 15 seconds if no value has been supplied.</returns>
		// Token: 0x17000325 RID: 805
		// (get) Token: 0x06001161 RID: 4449 RVA: 0x000599D3 File Offset: 0x00057BD3
		// (set) Token: 0x06001162 RID: 4450 RVA: 0x000599DB File Offset: 0x00057BDB
		public int ConnectTimeout
		{
			get
			{
				return this._connectTimeout;
			}
			set
			{
				if (value < 0)
				{
					throw ADP.InvalidConnectionOptionValue("Connect Timeout");
				}
				this.SetValue("Connect Timeout", value);
				this._connectTimeout = value;
			}
		}

		/// <summary>Gets or sets the SQL Server Language record name.</summary>
		/// <returns>The value of the <see cref="P:System.Data.SqlClient.SqlConnectionStringBuilder.CurrentLanguage" /> property, or <see langword="String.Empty" /> if no value has been supplied.</returns>
		/// <exception cref="T:System.ArgumentNullException">To set the value to null, use <see cref="F:System.DBNull.Value" />.</exception>
		// Token: 0x17000326 RID: 806
		// (get) Token: 0x06001163 RID: 4451 RVA: 0x000599FF File Offset: 0x00057BFF
		// (set) Token: 0x06001164 RID: 4452 RVA: 0x00059A07 File Offset: 0x00057C07
		public string CurrentLanguage
		{
			get
			{
				return this._currentLanguage;
			}
			set
			{
				this.SetValue("Current Language", value);
				this._currentLanguage = value;
			}
		}

		/// <summary>Gets or sets the name or network address of the instance of SQL Server to connect to.</summary>
		/// <returns>The value of the <see cref="P:System.Data.SqlClient.SqlConnectionStringBuilder.DataSource" /> property, or <see langword="String.Empty" /> if none has been supplied.</returns>
		/// <exception cref="T:System.ArgumentNullException">To set the value to null, use <see cref="F:System.DBNull.Value" />.</exception>
		// Token: 0x17000327 RID: 807
		// (get) Token: 0x06001165 RID: 4453 RVA: 0x00059A1C File Offset: 0x00057C1C
		// (set) Token: 0x06001166 RID: 4454 RVA: 0x00059A24 File Offset: 0x00057C24
		public string DataSource
		{
			get
			{
				return this._dataSource;
			}
			set
			{
				this.SetValue("Data Source", value);
				this._dataSource = value;
			}
		}

		/// <summary>Gets or sets a Boolean value that indicates whether SQL Server uses SSL encryption for all data sent between the client and server if the server has a certificate installed.</summary>
		/// <returns>The value of the <see cref="P:System.Data.SqlClient.SqlConnectionStringBuilder.Encrypt" /> property, or <see langword="false" /> if none has been supplied.</returns>
		// Token: 0x17000328 RID: 808
		// (get) Token: 0x06001167 RID: 4455 RVA: 0x00059A39 File Offset: 0x00057C39
		// (set) Token: 0x06001168 RID: 4456 RVA: 0x00059A41 File Offset: 0x00057C41
		public bool Encrypt
		{
			get
			{
				return this._encrypt;
			}
			set
			{
				this.SetValue("Encrypt", value);
				this._encrypt = value;
			}
		}

		/// <summary>Gets or sets a value that indicates whether the channel will be encrypted while bypassing walking the certificate chain to validate trust.</summary>
		/// <returns>A <see langword="Boolean" />. Recognized values are <see langword="true" />, <see langword="false" />, <see langword="yes" />, and <see langword="no" />. </returns>
		// Token: 0x17000329 RID: 809
		// (get) Token: 0x06001169 RID: 4457 RVA: 0x00059A56 File Offset: 0x00057C56
		// (set) Token: 0x0600116A RID: 4458 RVA: 0x00059A5E File Offset: 0x00057C5E
		public bool TrustServerCertificate
		{
			get
			{
				return this._trustServerCertificate;
			}
			set
			{
				this.SetValue("TrustServerCertificate", value);
				this._trustServerCertificate = value;
			}
		}

		/// <summary>Gets or sets a Boolean value that indicates whether the SQL Server connection pooler automatically enlists the connection in the creation thread's current transaction context.</summary>
		/// <returns>The value of the <see cref="P:System.Data.SqlClient.SqlConnectionStringBuilder.Enlist" /> property, or <see langword="true " />if none has been supplied.</returns>
		// Token: 0x1700032A RID: 810
		// (get) Token: 0x0600116B RID: 4459 RVA: 0x00059A73 File Offset: 0x00057C73
		// (set) Token: 0x0600116C RID: 4460 RVA: 0x00059A7B File Offset: 0x00057C7B
		public bool Enlist
		{
			get
			{
				return this._enlist;
			}
			set
			{
				this.SetValue("Enlist", value);
				this._enlist = value;
			}
		}

		/// <summary>Gets or sets the name or address of the partner server to connect to if the primary server is down.</summary>
		/// <returns>The value of the <see cref="P:System.Data.SqlClient.SqlConnectionStringBuilder.FailoverPartner" /> property, or <see langword="String.Empty" /> if none has been supplied.</returns>
		/// <exception cref="T:System.ArgumentNullException">To set the value to null, use <see cref="F:System.DBNull.Value" />.</exception>
		// Token: 0x1700032B RID: 811
		// (get) Token: 0x0600116D RID: 4461 RVA: 0x00059A90 File Offset: 0x00057C90
		// (set) Token: 0x0600116E RID: 4462 RVA: 0x00059A98 File Offset: 0x00057C98
		public string FailoverPartner
		{
			get
			{
				return this._failoverPartner;
			}
			set
			{
				this.SetValue("Failover Partner", value);
				this._failoverPartner = value;
			}
		}

		/// <summary>Gets or sets the name of the database associated with the connection.</summary>
		/// <returns>The value of the <see cref="P:System.Data.SqlClient.SqlConnectionStringBuilder.InitialCatalog" /> property, or <see langword="String.Empty" /> if none has been supplied.</returns>
		/// <exception cref="T:System.ArgumentNullException">To set the value to null, use <see cref="F:System.DBNull.Value" />.</exception>
		// Token: 0x1700032C RID: 812
		// (get) Token: 0x0600116F RID: 4463 RVA: 0x00059AAD File Offset: 0x00057CAD
		// (set) Token: 0x06001170 RID: 4464 RVA: 0x00059AB5 File Offset: 0x00057CB5
		[TypeConverter(typeof(SqlConnectionStringBuilder.SqlInitialCatalogConverter))]
		public string InitialCatalog
		{
			get
			{
				return this._initialCatalog;
			}
			set
			{
				this.SetValue("Initial Catalog", value);
				this._initialCatalog = value;
			}
		}

		/// <summary>Gets or sets a Boolean value that indicates whether User ID and Password are specified in the connection (when <see langword="false" />) or whether the current Windows account credentials are used for authentication (when <see langword="true" />).</summary>
		/// <returns>The value of the <see cref="P:System.Data.SqlClient.SqlConnectionStringBuilder.IntegratedSecurity" /> property, or <see langword="false" /> if none has been supplied.</returns>
		// Token: 0x1700032D RID: 813
		// (get) Token: 0x06001171 RID: 4465 RVA: 0x00059ACA File Offset: 0x00057CCA
		// (set) Token: 0x06001172 RID: 4466 RVA: 0x00059AD2 File Offset: 0x00057CD2
		public bool IntegratedSecurity
		{
			get
			{
				return this._integratedSecurity;
			}
			set
			{
				this.SetValue("Integrated Security", value);
				this._integratedSecurity = value;
			}
		}

		/// <summary>Gets or sets the minimum time, in seconds, for the connection to live in the connection pool before being destroyed.</summary>
		/// <returns>The value of the <see cref="P:System.Data.SqlClient.SqlConnectionStringBuilder.LoadBalanceTimeout" /> property, or 0 if none has been supplied.</returns>
		// Token: 0x1700032E RID: 814
		// (get) Token: 0x06001173 RID: 4467 RVA: 0x00059AE7 File Offset: 0x00057CE7
		// (set) Token: 0x06001174 RID: 4468 RVA: 0x00059AEF File Offset: 0x00057CEF
		public int LoadBalanceTimeout
		{
			get
			{
				return this._loadBalanceTimeout;
			}
			set
			{
				if (value < 0)
				{
					throw ADP.InvalidConnectionOptionValue("Load Balance Timeout");
				}
				this.SetValue("Load Balance Timeout", value);
				this._loadBalanceTimeout = value;
			}
		}

		/// <summary>Gets or sets the maximum number of connections allowed in the connection pool for this specific connection string.</summary>
		/// <returns>The value of the <see cref="P:System.Data.SqlClient.SqlConnectionStringBuilder.MaxPoolSize" /> property, or 100 if none has been supplied.</returns>
		// Token: 0x1700032F RID: 815
		// (get) Token: 0x06001175 RID: 4469 RVA: 0x00059B13 File Offset: 0x00057D13
		// (set) Token: 0x06001176 RID: 4470 RVA: 0x00059B1B File Offset: 0x00057D1B
		public int MaxPoolSize
		{
			get
			{
				return this._maxPoolSize;
			}
			set
			{
				if (value < 1)
				{
					throw ADP.InvalidConnectionOptionValue("Max Pool Size");
				}
				this.SetValue("Max Pool Size", value);
				this._maxPoolSize = value;
			}
		}

		/// <summary>[Supported in the .NET Framework 4.5.1 and later versions] The number of reconnections attempted after identifying that there was an idle connection failure. This must be an integer between 0 and 255. Default is 1. Set to 0 to disable reconnecting on idle connection failures. An <see cref="T:System.ArgumentException" /> will be thrown if set to a value outside of the allowed range.</summary>
		/// <returns>The number of reconnections attempted after identifying that there was an idle connection failure.</returns>
		// Token: 0x17000330 RID: 816
		// (get) Token: 0x06001177 RID: 4471 RVA: 0x00059B3F File Offset: 0x00057D3F
		// (set) Token: 0x06001178 RID: 4472 RVA: 0x00059B47 File Offset: 0x00057D47
		public int ConnectRetryCount
		{
			get
			{
				return this._connectRetryCount;
			}
			set
			{
				if (value < 0 || value > 255)
				{
					throw ADP.InvalidConnectionOptionValue("ConnectRetryCount");
				}
				this.SetValue("ConnectRetryCount", value);
				this._connectRetryCount = value;
			}
		}

		/// <summary>[Supported in the .NET Framework 4.5.1 and later versions] Amount of time (in seconds) between each reconnection attempt after identifying that there was an idle connection failure. This must be an integer between 1 and 60. The default is 10 seconds. An <see cref="T:System.ArgumentException" /> will be thrown if set to a value outside of the allowed range.</summary>
		/// <returns>Amount of time (in seconds) between each reconnection attempt after identifying that there was an idle connection failure.</returns>
		// Token: 0x17000331 RID: 817
		// (get) Token: 0x06001179 RID: 4473 RVA: 0x00059B73 File Offset: 0x00057D73
		// (set) Token: 0x0600117A RID: 4474 RVA: 0x00059B7B File Offset: 0x00057D7B
		public int ConnectRetryInterval
		{
			get
			{
				return this._connectRetryInterval;
			}
			set
			{
				if (value < 1 || value > 60)
				{
					throw ADP.InvalidConnectionOptionValue("ConnectRetryInterval");
				}
				this.SetValue("ConnectRetryInterval", value);
				this._connectRetryInterval = value;
			}
		}

		/// <summary>Gets or sets the minimum number of connections allowed in the connection pool for this specific connection string.</summary>
		/// <returns>The value of the <see cref="P:System.Data.SqlClient.SqlConnectionStringBuilder.MinPoolSize" /> property, or 0 if none has been supplied.</returns>
		// Token: 0x17000332 RID: 818
		// (get) Token: 0x0600117B RID: 4475 RVA: 0x00059BA4 File Offset: 0x00057DA4
		// (set) Token: 0x0600117C RID: 4476 RVA: 0x00059BAC File Offset: 0x00057DAC
		public int MinPoolSize
		{
			get
			{
				return this._minPoolSize;
			}
			set
			{
				if (value < 0)
				{
					throw ADP.InvalidConnectionOptionValue("Min Pool Size");
				}
				this.SetValue("Min Pool Size", value);
				this._minPoolSize = value;
			}
		}

		/// <summary>When true, an application can maintain multiple active result sets (MARS). When false, an application must process or cancel all result sets from one batch before it can execute any other batch on that connection.For more information, see Multiple Active Result Sets (MARS).</summary>
		/// <returns>The value of the <see cref="P:System.Data.SqlClient.SqlConnectionStringBuilder.MultipleActiveResultSets" /> property, or <see langword="false" /> if none has been supplied.</returns>
		// Token: 0x17000333 RID: 819
		// (get) Token: 0x0600117D RID: 4477 RVA: 0x00059BD0 File Offset: 0x00057DD0
		// (set) Token: 0x0600117E RID: 4478 RVA: 0x00059BD8 File Offset: 0x00057DD8
		public bool MultipleActiveResultSets
		{
			get
			{
				return this._multipleActiveResultSets;
			}
			set
			{
				this.SetValue("MultipleActiveResultSets", value);
				this._multipleActiveResultSets = value;
			}
		}

		/// <summary>If your application is connecting to an AlwaysOn availability group (AG) on different subnets, setting MultiSubnetFailover=true provides faster detection of and connection to the (currently) active server. For more information about SqlClient support for Always On Availability Groups, see SqlClient Support for High Availability, Disaster Recovery.</summary>
		/// <returns>Returns <see cref="T:System.Boolean" /> indicating the current value of the property.</returns>
		// Token: 0x17000334 RID: 820
		// (get) Token: 0x0600117F RID: 4479 RVA: 0x00059BED File Offset: 0x00057DED
		// (set) Token: 0x06001180 RID: 4480 RVA: 0x00059BF5 File Offset: 0x00057DF5
		public bool MultiSubnetFailover
		{
			get
			{
				return this._multiSubnetFailover;
			}
			set
			{
				this.SetValue("MultiSubnetFailover", value);
				this._multiSubnetFailover = value;
			}
		}

		/// <summary>Gets or sets the size in bytes of the network packets used to communicate with an instance of SQL Server.</summary>
		/// <returns>The value of the <see cref="P:System.Data.SqlClient.SqlConnectionStringBuilder.PacketSize" /> property, or 8000 if none has been supplied.</returns>
		// Token: 0x17000335 RID: 821
		// (get) Token: 0x06001181 RID: 4481 RVA: 0x00059C0A File Offset: 0x00057E0A
		// (set) Token: 0x06001182 RID: 4482 RVA: 0x00059C12 File Offset: 0x00057E12
		public int PacketSize
		{
			get
			{
				return this._packetSize;
			}
			set
			{
				if (value < 512 || 32768 < value)
				{
					throw SQL.InvalidPacketSizeValue();
				}
				this.SetValue("Packet Size", value);
				this._packetSize = value;
			}
		}

		/// <summary>Gets or sets the password for the SQL Server account.</summary>
		/// <returns>The value of the <see cref="P:System.Data.SqlClient.SqlConnectionStringBuilder.Password" /> property, or <see langword="String.Empty" /> if none has been supplied.</returns>
		/// <exception cref="T:System.ArgumentNullException">The password was incorrectly set to null.  See code sample below.</exception>
		// Token: 0x17000336 RID: 822
		// (get) Token: 0x06001183 RID: 4483 RVA: 0x00059C3D File Offset: 0x00057E3D
		// (set) Token: 0x06001184 RID: 4484 RVA: 0x00059C45 File Offset: 0x00057E45
		public string Password
		{
			get
			{
				return this._password;
			}
			set
			{
				this.SetValue("Password", value);
				this._password = value;
			}
		}

		/// <summary>Gets or sets a Boolean value that indicates if security-sensitive information, such as the password, is not returned as part of the connection if the connection is open or has ever been in an open state.</summary>
		/// <returns>The value of the <see cref="P:System.Data.SqlClient.SqlConnectionStringBuilder.PersistSecurityInfo" /> property, or <see langword="false" /> if none has been supplied.</returns>
		// Token: 0x17000337 RID: 823
		// (get) Token: 0x06001185 RID: 4485 RVA: 0x00059C5A File Offset: 0x00057E5A
		// (set) Token: 0x06001186 RID: 4486 RVA: 0x00059C62 File Offset: 0x00057E62
		public bool PersistSecurityInfo
		{
			get
			{
				return this._persistSecurityInfo;
			}
			set
			{
				this.SetValue("Persist Security Info", value);
				this._persistSecurityInfo = value;
			}
		}

		/// <summary>Gets or sets a Boolean value that indicates whether the connection will be pooled or explicitly opened every time that the connection is requested.</summary>
		/// <returns>The value of the <see cref="P:System.Data.SqlClient.SqlConnectionStringBuilder.Pooling" /> property, or <see langword="true" /> if none has been supplied.</returns>
		// Token: 0x17000338 RID: 824
		// (get) Token: 0x06001187 RID: 4487 RVA: 0x00059C77 File Offset: 0x00057E77
		// (set) Token: 0x06001188 RID: 4488 RVA: 0x00059C7F File Offset: 0x00057E7F
		public bool Pooling
		{
			get
			{
				return this._pooling;
			}
			set
			{
				this.SetValue("Pooling", value);
				this._pooling = value;
			}
		}

		/// <summary>Gets or sets a Boolean value that indicates whether replication is supported using the connection.</summary>
		/// <returns>The value of the <see cref="P:System.Data.SqlClient.SqlConnectionStringBuilder.Replication" /> property, or false if none has been supplied.</returns>
		// Token: 0x17000339 RID: 825
		// (get) Token: 0x06001189 RID: 4489 RVA: 0x00059C94 File Offset: 0x00057E94
		// (set) Token: 0x0600118A RID: 4490 RVA: 0x00059C9C File Offset: 0x00057E9C
		public bool Replication
		{
			get
			{
				return this._replication;
			}
			set
			{
				this.SetValue("Replication", value);
				this._replication = value;
			}
		}

		/// <summary>Gets or sets a string value that indicates how the connection maintains its association with an enlisted <see langword="System.Transactions" /> transaction.</summary>
		/// <returns>The value of the <see cref="P:System.Data.SqlClient.SqlConnectionStringBuilder.TransactionBinding" /> property, or <see langword="String.Empty" /> if none has been supplied.</returns>
		// Token: 0x1700033A RID: 826
		// (get) Token: 0x0600118B RID: 4491 RVA: 0x00059CB1 File Offset: 0x00057EB1
		// (set) Token: 0x0600118C RID: 4492 RVA: 0x00059CB9 File Offset: 0x00057EB9
		public string TransactionBinding
		{
			get
			{
				return this._transactionBinding;
			}
			set
			{
				this.SetValue("Transaction Binding", value);
				this._transactionBinding = value;
			}
		}

		/// <summary>Gets or sets a string value that indicates the type system the application expects.</summary>
		/// <returns>The following table shows the possible values for the <see cref="P:System.Data.SqlClient.SqlConnectionStringBuilder.TypeSystemVersion" /> property:ValueDescription
		///             SQL Server 2005Uses the SQL Server 2005 type system. No conversions are made for the current version of ADO.NET.SQL Server 2008Uses the SQL Server 2008 type system.LatestUse the latest version than this client-server pair can handle. This will automatically move forward as the client and server components are upgraded.</returns>
		// Token: 0x1700033B RID: 827
		// (get) Token: 0x0600118D RID: 4493 RVA: 0x00059CCE File Offset: 0x00057ECE
		// (set) Token: 0x0600118E RID: 4494 RVA: 0x00059CD6 File Offset: 0x00057ED6
		public string TypeSystemVersion
		{
			get
			{
				return this._typeSystemVersion;
			}
			set
			{
				this.SetValue("Type System Version", value);
				this._typeSystemVersion = value;
			}
		}

		/// <summary>Gets or sets the user ID to be used when connecting to SQL Server.</summary>
		/// <returns>The value of the <see cref="P:System.Data.SqlClient.SqlConnectionStringBuilder.UserID" /> property, or <see langword="String.Empty" /> if none has been supplied.</returns>
		/// <exception cref="T:System.ArgumentNullException">To set the value to null, use <see cref="F:System.DBNull.Value" />.</exception>
		// Token: 0x1700033C RID: 828
		// (get) Token: 0x0600118F RID: 4495 RVA: 0x00059CEB File Offset: 0x00057EEB
		// (set) Token: 0x06001190 RID: 4496 RVA: 0x00059CF3 File Offset: 0x00057EF3
		public string UserID
		{
			get
			{
				return this._userID;
			}
			set
			{
				this.SetValue("User ID", value);
				this._userID = value;
			}
		}

		/// <summary>Gets or sets a value that indicates whether to redirect the connection from the default SQL Server Express instance to a runtime-initiated instance running under the account of the caller.</summary>
		/// <returns>The value of the <see cref="P:System.Data.SqlClient.SqlConnectionStringBuilder.UserInstance" /> property, or <see langword="False" /> if none has been supplied.</returns>
		/// <exception cref="T:System.ArgumentNullException">To set the value to null, use <see cref="F:System.DBNull.Value" />.</exception>
		// Token: 0x1700033D RID: 829
		// (get) Token: 0x06001191 RID: 4497 RVA: 0x00059D08 File Offset: 0x00057F08
		// (set) Token: 0x06001192 RID: 4498 RVA: 0x00059D10 File Offset: 0x00057F10
		public bool UserInstance
		{
			get
			{
				return this._userInstance;
			}
			set
			{
				this.SetValue("User Instance", value);
				this._userInstance = value;
			}
		}

		/// <summary>Gets or sets the name of the workstation connecting to SQL Server.</summary>
		/// <returns>The value of the <see cref="P:System.Data.SqlClient.SqlConnectionStringBuilder.WorkstationID" /> property, or <see langword="String.Empty" /> if none has been supplied.</returns>
		/// <exception cref="T:System.ArgumentNullException">To set the value to null, use <see cref="F:System.DBNull.Value" />.</exception>
		// Token: 0x1700033E RID: 830
		// (get) Token: 0x06001193 RID: 4499 RVA: 0x00059D25 File Offset: 0x00057F25
		// (set) Token: 0x06001194 RID: 4500 RVA: 0x00059D2D File Offset: 0x00057F2D
		public string WorkstationID
		{
			get
			{
				return this._workstationID;
			}
			set
			{
				this.SetValue("Workstation ID", value);
				this._workstationID = value;
			}
		}

		/// <summary>Gets an <see cref="T:System.Collections.ICollection" /> that contains the keys in the <see cref="T:System.Data.SqlClient.SqlConnectionStringBuilder" />.</summary>
		/// <returns>An <see cref="T:System.Collections.ICollection" /> that contains the keys in the <see cref="T:System.Data.SqlClient.SqlConnectionStringBuilder" />.</returns>
		// Token: 0x1700033F RID: 831
		// (get) Token: 0x06001195 RID: 4501 RVA: 0x00059D42 File Offset: 0x00057F42
		public override ICollection Keys
		{
			get
			{
				return new ReadOnlyCollection<string>(SqlConnectionStringBuilder.s_validKeywords);
			}
		}

		/// <summary>Gets an <see cref="T:System.Collections.ICollection" /> that contains the values in the <see cref="T:System.Data.SqlClient.SqlConnectionStringBuilder" />.</summary>
		/// <returns>An <see cref="T:System.Collections.ICollection" /> that contains the values in the <see cref="T:System.Data.SqlClient.SqlConnectionStringBuilder" />.</returns>
		// Token: 0x17000340 RID: 832
		// (get) Token: 0x06001196 RID: 4502 RVA: 0x00059D50 File Offset: 0x00057F50
		public override ICollection Values
		{
			get
			{
				object[] array = new object[SqlConnectionStringBuilder.s_validKeywords.Length];
				for (int i = 0; i < array.Length; i++)
				{
					array[i] = this.GetAt((SqlConnectionStringBuilder.Keywords)i);
				}
				return new ReadOnlyCollection<object>(array);
			}
		}

		/// <summary>Clears the contents of the <see cref="T:System.Data.SqlClient.SqlConnectionStringBuilder" /> instance.</summary>
		// Token: 0x06001197 RID: 4503 RVA: 0x00059D88 File Offset: 0x00057F88
		public override void Clear()
		{
			base.Clear();
			for (int i = 0; i < SqlConnectionStringBuilder.s_validKeywords.Length; i++)
			{
				this.Reset((SqlConnectionStringBuilder.Keywords)i);
			}
		}

		/// <summary>Determines whether the <see cref="T:System.Data.SqlClient.SqlConnectionStringBuilder" /> contains a specific key.</summary>
		/// <param name="keyword">The key to locate in the <see cref="T:System.Data.SqlClient.SqlConnectionStringBuilder" />.</param>
		/// <returns>true if the <see cref="T:System.Data.SqlClient.SqlConnectionStringBuilder" /> contains an element that has the specified key; otherwise, false.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="keyword" /> is null (<see langword="Nothing" /> in Visual Basic)</exception>
		// Token: 0x06001198 RID: 4504 RVA: 0x00059DB4 File Offset: 0x00057FB4
		public override bool ContainsKey(string keyword)
		{
			ADP.CheckArgumentNull(keyword, "keyword");
			return SqlConnectionStringBuilder.s_keywords.ContainsKey(keyword);
		}

		// Token: 0x06001199 RID: 4505 RVA: 0x00059DCC File Offset: 0x00057FCC
		private static bool ConvertToBoolean(object value)
		{
			return DbConnectionStringBuilderUtil.ConvertToBoolean(value);
		}

		// Token: 0x0600119A RID: 4506 RVA: 0x00059DD4 File Offset: 0x00057FD4
		private static int ConvertToInt32(object value)
		{
			return DbConnectionStringBuilderUtil.ConvertToInt32(value);
		}

		// Token: 0x0600119B RID: 4507 RVA: 0x00059DDC File Offset: 0x00057FDC
		private static bool ConvertToIntegratedSecurity(object value)
		{
			return DbConnectionStringBuilderUtil.ConvertToIntegratedSecurity(value);
		}

		// Token: 0x0600119C RID: 4508 RVA: 0x00059DE4 File Offset: 0x00057FE4
		private static string ConvertToString(object value)
		{
			return DbConnectionStringBuilderUtil.ConvertToString(value);
		}

		// Token: 0x0600119D RID: 4509 RVA: 0x00059DEC File Offset: 0x00057FEC
		private static ApplicationIntent ConvertToApplicationIntent(string keyword, object value)
		{
			return DbConnectionStringBuilderUtil.ConvertToApplicationIntent(keyword, value);
		}

		// Token: 0x0600119E RID: 4510 RVA: 0x00059DF8 File Offset: 0x00057FF8
		private object GetAt(SqlConnectionStringBuilder.Keywords index)
		{
			switch (index)
			{
			case SqlConnectionStringBuilder.Keywords.DataSource:
				return this.DataSource;
			case SqlConnectionStringBuilder.Keywords.FailoverPartner:
				return this.FailoverPartner;
			case SqlConnectionStringBuilder.Keywords.AttachDBFilename:
				return this.AttachDBFilename;
			case SqlConnectionStringBuilder.Keywords.InitialCatalog:
				return this.InitialCatalog;
			case SqlConnectionStringBuilder.Keywords.IntegratedSecurity:
				return this.IntegratedSecurity;
			case SqlConnectionStringBuilder.Keywords.PersistSecurityInfo:
				return this.PersistSecurityInfo;
			case SqlConnectionStringBuilder.Keywords.UserID:
				return this.UserID;
			case SqlConnectionStringBuilder.Keywords.Password:
				return this.Password;
			case SqlConnectionStringBuilder.Keywords.Enlist:
				return this.Enlist;
			case SqlConnectionStringBuilder.Keywords.Pooling:
				return this.Pooling;
			case SqlConnectionStringBuilder.Keywords.MinPoolSize:
				return this.MinPoolSize;
			case SqlConnectionStringBuilder.Keywords.MaxPoolSize:
				return this.MaxPoolSize;
			case SqlConnectionStringBuilder.Keywords.MultipleActiveResultSets:
				return this.MultipleActiveResultSets;
			case SqlConnectionStringBuilder.Keywords.Replication:
				return this.Replication;
			case SqlConnectionStringBuilder.Keywords.ConnectTimeout:
				return this.ConnectTimeout;
			case SqlConnectionStringBuilder.Keywords.Encrypt:
				return this.Encrypt;
			case SqlConnectionStringBuilder.Keywords.TrustServerCertificate:
				return this.TrustServerCertificate;
			case SqlConnectionStringBuilder.Keywords.LoadBalanceTimeout:
				return this.LoadBalanceTimeout;
			case SqlConnectionStringBuilder.Keywords.PacketSize:
				return this.PacketSize;
			case SqlConnectionStringBuilder.Keywords.TypeSystemVersion:
				return this.TypeSystemVersion;
			case SqlConnectionStringBuilder.Keywords.ApplicationName:
				return this.ApplicationName;
			case SqlConnectionStringBuilder.Keywords.CurrentLanguage:
				return this.CurrentLanguage;
			case SqlConnectionStringBuilder.Keywords.WorkstationID:
				return this.WorkstationID;
			case SqlConnectionStringBuilder.Keywords.UserInstance:
				return this.UserInstance;
			case SqlConnectionStringBuilder.Keywords.TransactionBinding:
				return this.TransactionBinding;
			case SqlConnectionStringBuilder.Keywords.ApplicationIntent:
				return this.ApplicationIntent;
			case SqlConnectionStringBuilder.Keywords.MultiSubnetFailover:
				return this.MultiSubnetFailover;
			case SqlConnectionStringBuilder.Keywords.ConnectRetryCount:
				return this.ConnectRetryCount;
			case SqlConnectionStringBuilder.Keywords.ConnectRetryInterval:
				return this.ConnectRetryInterval;
			default:
				throw this.UnsupportedKeyword(SqlConnectionStringBuilder.s_validKeywords[(int)index]);
			}
		}

		// Token: 0x0600119F RID: 4511 RVA: 0x00059FB8 File Offset: 0x000581B8
		private SqlConnectionStringBuilder.Keywords GetIndex(string keyword)
		{
			ADP.CheckArgumentNull(keyword, "keyword");
			SqlConnectionStringBuilder.Keywords result;
			if (SqlConnectionStringBuilder.s_keywords.TryGetValue(keyword, out result))
			{
				return result;
			}
			throw this.UnsupportedKeyword(keyword);
		}

		/// <summary>Removes the entry with the specified key from the <see cref="T:System.Data.SqlClient.SqlConnectionStringBuilder" /> instance.</summary>
		/// <param name="keyword">The key of the key/value pair to be removed from the connection string in this <see cref="T:System.Data.SqlClient.SqlConnectionStringBuilder" />.</param>
		/// <returns>
		///     <see langword="true" /> if the key existed within the connection string and was removed; <see langword="false" /> if the key did not exist.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="keyword" /> is null (<see langword="Nothing" /> in Visual Basic)</exception>
		// Token: 0x060011A0 RID: 4512 RVA: 0x00059FE8 File Offset: 0x000581E8
		public override bool Remove(string keyword)
		{
			ADP.CheckArgumentNull(keyword, "keyword");
			SqlConnectionStringBuilder.Keywords keywords;
			if (SqlConnectionStringBuilder.s_keywords.TryGetValue(keyword, out keywords) && base.Remove(SqlConnectionStringBuilder.s_validKeywords[(int)keywords]))
			{
				this.Reset(keywords);
				return true;
			}
			return false;
		}

		// Token: 0x060011A1 RID: 4513 RVA: 0x0005A028 File Offset: 0x00058228
		private void Reset(SqlConnectionStringBuilder.Keywords index)
		{
			switch (index)
			{
			case SqlConnectionStringBuilder.Keywords.DataSource:
				this._dataSource = "";
				return;
			case SqlConnectionStringBuilder.Keywords.FailoverPartner:
				this._failoverPartner = "";
				return;
			case SqlConnectionStringBuilder.Keywords.AttachDBFilename:
				this._attachDBFilename = "";
				return;
			case SqlConnectionStringBuilder.Keywords.InitialCatalog:
				this._initialCatalog = "";
				return;
			case SqlConnectionStringBuilder.Keywords.IntegratedSecurity:
				this._integratedSecurity = false;
				return;
			case SqlConnectionStringBuilder.Keywords.PersistSecurityInfo:
				this._persistSecurityInfo = false;
				return;
			case SqlConnectionStringBuilder.Keywords.UserID:
				this._userID = "";
				return;
			case SqlConnectionStringBuilder.Keywords.Password:
				this._password = "";
				return;
			case SqlConnectionStringBuilder.Keywords.Enlist:
				this._enlist = true;
				return;
			case SqlConnectionStringBuilder.Keywords.Pooling:
				this._pooling = true;
				return;
			case SqlConnectionStringBuilder.Keywords.MinPoolSize:
				this._minPoolSize = 0;
				return;
			case SqlConnectionStringBuilder.Keywords.MaxPoolSize:
				this._maxPoolSize = 100;
				return;
			case SqlConnectionStringBuilder.Keywords.MultipleActiveResultSets:
				this._multipleActiveResultSets = false;
				return;
			case SqlConnectionStringBuilder.Keywords.Replication:
				this._replication = false;
				return;
			case SqlConnectionStringBuilder.Keywords.ConnectTimeout:
				this._connectTimeout = 15;
				return;
			case SqlConnectionStringBuilder.Keywords.Encrypt:
				this._encrypt = false;
				return;
			case SqlConnectionStringBuilder.Keywords.TrustServerCertificate:
				this._trustServerCertificate = false;
				return;
			case SqlConnectionStringBuilder.Keywords.LoadBalanceTimeout:
				this._loadBalanceTimeout = 0;
				return;
			case SqlConnectionStringBuilder.Keywords.PacketSize:
				this._packetSize = 8000;
				return;
			case SqlConnectionStringBuilder.Keywords.TypeSystemVersion:
				this._typeSystemVersion = "Latest";
				return;
			case SqlConnectionStringBuilder.Keywords.ApplicationName:
				this._applicationName = "Core .Net SqlClient Data Provider";
				return;
			case SqlConnectionStringBuilder.Keywords.CurrentLanguage:
				this._currentLanguage = "";
				return;
			case SqlConnectionStringBuilder.Keywords.WorkstationID:
				this._workstationID = "";
				return;
			case SqlConnectionStringBuilder.Keywords.UserInstance:
				this._userInstance = false;
				return;
			case SqlConnectionStringBuilder.Keywords.TransactionBinding:
				this._transactionBinding = "Implicit Unbind";
				return;
			case SqlConnectionStringBuilder.Keywords.ApplicationIntent:
				this._applicationIntent = ApplicationIntent.ReadWrite;
				return;
			case SqlConnectionStringBuilder.Keywords.MultiSubnetFailover:
				this._multiSubnetFailover = false;
				return;
			case SqlConnectionStringBuilder.Keywords.ConnectRetryCount:
				this._connectRetryCount = 1;
				return;
			case SqlConnectionStringBuilder.Keywords.ConnectRetryInterval:
				this._connectRetryInterval = 10;
				return;
			default:
				throw this.UnsupportedKeyword(SqlConnectionStringBuilder.s_validKeywords[(int)index]);
			}
		}

		// Token: 0x060011A2 RID: 4514 RVA: 0x0005A1DC File Offset: 0x000583DC
		private void SetValue(string keyword, bool value)
		{
			base[keyword] = value.ToString();
		}

		// Token: 0x060011A3 RID: 4515 RVA: 0x0005A1EC File Offset: 0x000583EC
		private void SetValue(string keyword, int value)
		{
			base[keyword] = value.ToString(null);
		}

		// Token: 0x060011A4 RID: 4516 RVA: 0x0005A1FD File Offset: 0x000583FD
		private void SetValue(string keyword, string value)
		{
			ADP.CheckArgumentNull(value, keyword);
			base[keyword] = value;
		}

		// Token: 0x060011A5 RID: 4517 RVA: 0x0005A20E File Offset: 0x0005840E
		private void SetApplicationIntentValue(ApplicationIntent value)
		{
			base["ApplicationIntent"] = DbConnectionStringBuilderUtil.ApplicationIntentToString(value);
		}

		/// <summary>Indicates whether the specified key exists in this <see cref="T:System.Data.SqlClient.SqlConnectionStringBuilder" /> instance.</summary>
		/// <param name="keyword">The key to locate in the <see cref="T:System.Data.SqlClient.SqlConnectionStringBuilder" />.</param>
		/// <returns>
		///     <see langword="true" /> if the <see cref="T:System.Data.SqlClient.SqlConnectionStringBuilder" /> contains an entry with the specified key; otherwise, <see langword="false" />.</returns>
		// Token: 0x060011A6 RID: 4518 RVA: 0x0005A224 File Offset: 0x00058424
		public override bool ShouldSerialize(string keyword)
		{
			ADP.CheckArgumentNull(keyword, "keyword");
			SqlConnectionStringBuilder.Keywords keywords;
			return SqlConnectionStringBuilder.s_keywords.TryGetValue(keyword, out keywords) && base.ShouldSerialize(SqlConnectionStringBuilder.s_validKeywords[(int)keywords]);
		}

		/// <summary>Retrieves a value corresponding to the supplied key from this <see cref="T:System.Data.SqlClient.SqlConnectionStringBuilder" />.</summary>
		/// <param name="keyword">The key of the item to retrieve.</param>
		/// <param name="value">The value corresponding to <paramref name="keyword." /></param>
		/// <returns>
		///     <see langword="true" /> if <paramref name="keyword" /> was found within the connection string; otherwise, <see langword="false" />.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="keyword" /> contains a null value (<see langword="Nothing" /> in Visual Basic).</exception>
		// Token: 0x060011A7 RID: 4519 RVA: 0x0005A25C File Offset: 0x0005845C
		public override bool TryGetValue(string keyword, out object value)
		{
			SqlConnectionStringBuilder.Keywords index;
			if (SqlConnectionStringBuilder.s_keywords.TryGetValue(keyword, out index))
			{
				value = this.GetAt(index);
				return true;
			}
			value = null;
			return false;
		}

		// Token: 0x060011A8 RID: 4520 RVA: 0x0005A287 File Offset: 0x00058487
		private Exception UnsupportedKeyword(string keyword)
		{
			if (SqlConnectionStringBuilder.s_notSupportedKeywords.Contains(keyword, StringComparer.OrdinalIgnoreCase))
			{
				return SQL.UnsupportedKeyword(keyword);
			}
			if (SqlConnectionStringBuilder.s_notSupportedNetworkLibraryKeywords.Contains(keyword, StringComparer.OrdinalIgnoreCase))
			{
				return SQL.NetworkLibraryKeywordNotSupported();
			}
			return ADP.KeywordNotSupported(keyword);
		}

		/// <summary>Gets or sets a Boolean value that indicates whether asynchronous processing is allowed by the connection created by using this connection string.</summary>
		/// <returns>This property is ignored beginning in .NET Framework 4.5. For more information about SqlClient support for asynchronous programming, see Asynchronous Programming.The value of the <see cref="P:System.Data.SqlClient.SqlConnectionStringBuilder.AsynchronousProcessing" /> property, or <see langword="false" /> if no value has been supplied.</returns>
		// Token: 0x17000341 RID: 833
		// (get) Token: 0x060011A9 RID: 4521 RVA: 0x0005A2C0 File Offset: 0x000584C0
		// (set) Token: 0x060011AA RID: 4522 RVA: 0x0005A2C8 File Offset: 0x000584C8
		[Obsolete("This property is ignored beginning in .NET Framework 4.5.For more information about SqlClient support for asynchronous programming, seehttps://docs.microsoft.com/en-us/dotnet/framework/data/adonet/asynchronous-programming")]
		public bool AsynchronousProcessing
		{
			[CompilerGenerated]
			get
			{
				return this.<AsynchronousProcessing>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<AsynchronousProcessing>k__BackingField = value;
			}
		}

		/// <summary>Obsolete. Gets or sets a Boolean value that indicates whether the connection is reset when drawn from the connection pool.</summary>
		/// <returns>The value of the <see cref="P:System.Data.SqlClient.SqlConnectionStringBuilder.ConnectionReset" /> property, or true if no value has been supplied.</returns>
		// Token: 0x17000342 RID: 834
		// (get) Token: 0x060011AB RID: 4523 RVA: 0x0005A2D1 File Offset: 0x000584D1
		// (set) Token: 0x060011AC RID: 4524 RVA: 0x0005A2D9 File Offset: 0x000584D9
		[Obsolete("ConnectionReset has been deprecated.  SqlConnection will ignore the 'connection reset'keyword and always reset the connection")]
		public bool ConnectionReset
		{
			[CompilerGenerated]
			get
			{
				return this.<ConnectionReset>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<ConnectionReset>k__BackingField = value;
			}
		}

		/// <summary>Gets or sets a value that indicates whether a client/server or in-process connection to SQL Server should be made.</summary>
		/// <returns>The value of the <see cref="P:System.Data.SqlClient.SqlConnectionStringBuilder.ContextConnection" /> property, or <see langword="False" /> if none has been supplied.</returns>
		// Token: 0x17000343 RID: 835
		// (get) Token: 0x060011AD RID: 4525 RVA: 0x000554AE File Offset: 0x000536AE
		// (set) Token: 0x060011AE RID: 4526 RVA: 0x000554AE File Offset: 0x000536AE
		[MonoTODO("Not implemented in corefx: https://github.com/dotnet/corefx/issues/22474")]
		public bool ContextConnection
		{
			get
			{
				throw new NotImplementedException();
			}
			set
			{
				throw new NotImplementedException();
			}
		}

		/// <summary>Gets or sets a string that contains the name of the network library used to establish a connection to the SQL Server.</summary>
		/// <returns>The value of the <see cref="P:System.Data.SqlClient.SqlConnectionStringBuilder.NetworkLibrary" /> property, or <see langword="String.Empty" /> if none has been supplied.</returns>
		/// <exception cref="T:System.ArgumentNullException">To set the value to null, use <see cref="F:System.DBNull.Value" />.</exception>
		// Token: 0x17000344 RID: 836
		// (get) Token: 0x060011AF RID: 4527 RVA: 0x000554AE File Offset: 0x000536AE
		// (set) Token: 0x060011B0 RID: 4528 RVA: 0x000554AE File Offset: 0x000536AE
		[MonoTODO("Not implemented in corefx: https://github.com/dotnet/corefx/issues/22474")]
		public string NetworkLibrary
		{
			get
			{
				throw new NotImplementedException();
			}
			set
			{
				throw new NotImplementedException();
			}
		}

		/// <summary>The blocking period behavior for a connection pool.</summary>
		/// <returns>The available blocking period settings.</returns>
		// Token: 0x17000345 RID: 837
		// (get) Token: 0x060011B1 RID: 4529 RVA: 0x000554AE File Offset: 0x000536AE
		// (set) Token: 0x060011B2 RID: 4530 RVA: 0x000554AE File Offset: 0x000536AE
		[MonoTODO("Not implemented in corefx: https://github.com/dotnet/corefx/issues/22474")]
		public PoolBlockingPeriod PoolBlockingPeriod
		{
			get
			{
				throw new NotImplementedException();
			}
			set
			{
				throw new NotImplementedException();
			}
		}

		/// <summary>When the value of this key is set to <see langword="true" />, the application is required to retrieve all IP addresses for a particular DNS entry and attempt to connect with the first one in the list. If the connection is not established within 0.5 seconds, the application will try to connect to all others in parallel. When the first answers, the application will establish the connection with the respondent IP address.</summary>
		/// <returns>A boolean value.</returns>
		// Token: 0x17000346 RID: 838
		// (get) Token: 0x060011B3 RID: 4531 RVA: 0x000554AE File Offset: 0x000536AE
		// (set) Token: 0x060011B4 RID: 4532 RVA: 0x000554AE File Offset: 0x000536AE
		[MonoTODO("Not implemented in corefx: https://github.com/dotnet/corefx/issues/22474")]
		public bool TransparentNetworkIPResolution
		{
			get
			{
				throw new NotImplementedException();
			}
			set
			{
				throw new NotImplementedException();
			}
		}

		/// <summary>Gets and sets the column encryption settings for the connection string builder.</summary>
		/// <returns>The column encryption settings for the connection string builder.</returns>
		// Token: 0x17000347 RID: 839
		// (get) Token: 0x060011B5 RID: 4533 RVA: 0x000554AE File Offset: 0x000536AE
		// (set) Token: 0x060011B6 RID: 4534 RVA: 0x000554AE File Offset: 0x000536AE
		[MonoTODO("Not implemented in corefx: https://github.com/dotnet/corefx/issues/22474")]
		public SqlConnectionColumnEncryptionSetting ColumnEncryptionSetting
		{
			get
			{
				throw new NotImplementedException();
			}
			set
			{
				throw new NotImplementedException();
			}
		}

		// Token: 0x060011B7 RID: 4535 RVA: 0x0005A2E4 File Offset: 0x000584E4
		// Note: this type is marked as 'beforefieldinit'.
		static SqlConnectionStringBuilder()
		{
		}

		/// <summary>Gets the authentication of the connection string.</summary>
		/// <returns>The authentication of the connection string.</returns>
		// Token: 0x17000348 RID: 840
		// (get) Token: 0x060011B8 RID: 4536 RVA: 0x0005A35C File Offset: 0x0005855C
		// (set) Token: 0x060011B9 RID: 4537 RVA: 0x00010458 File Offset: 0x0000E658
		public SqlAuthenticationMethod Authentication
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return SqlAuthenticationMethod.NotSpecified;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		// Token: 0x04000C5E RID: 3166
		internal const int KeywordsCount = 29;

		// Token: 0x04000C5F RID: 3167
		internal const int DeprecatedKeywordsCount = 4;

		// Token: 0x04000C60 RID: 3168
		private static readonly string[] s_validKeywords = SqlConnectionStringBuilder.CreateValidKeywords();

		// Token: 0x04000C61 RID: 3169
		private static readonly Dictionary<string, SqlConnectionStringBuilder.Keywords> s_keywords = SqlConnectionStringBuilder.CreateKeywordsDictionary();

		// Token: 0x04000C62 RID: 3170
		private ApplicationIntent _applicationIntent;

		// Token: 0x04000C63 RID: 3171
		private string _applicationName = "Core .Net SqlClient Data Provider";

		// Token: 0x04000C64 RID: 3172
		private string _attachDBFilename = "";

		// Token: 0x04000C65 RID: 3173
		private string _currentLanguage = "";

		// Token: 0x04000C66 RID: 3174
		private string _dataSource = "";

		// Token: 0x04000C67 RID: 3175
		private string _failoverPartner = "";

		// Token: 0x04000C68 RID: 3176
		private string _initialCatalog = "";

		// Token: 0x04000C69 RID: 3177
		private string _password = "";

		// Token: 0x04000C6A RID: 3178
		private string _transactionBinding = "Implicit Unbind";

		// Token: 0x04000C6B RID: 3179
		private string _typeSystemVersion = "Latest";

		// Token: 0x04000C6C RID: 3180
		private string _userID = "";

		// Token: 0x04000C6D RID: 3181
		private string _workstationID = "";

		// Token: 0x04000C6E RID: 3182
		private int _connectTimeout = 15;

		// Token: 0x04000C6F RID: 3183
		private int _loadBalanceTimeout;

		// Token: 0x04000C70 RID: 3184
		private int _maxPoolSize = 100;

		// Token: 0x04000C71 RID: 3185
		private int _minPoolSize;

		// Token: 0x04000C72 RID: 3186
		private int _packetSize = 8000;

		// Token: 0x04000C73 RID: 3187
		private int _connectRetryCount = 1;

		// Token: 0x04000C74 RID: 3188
		private int _connectRetryInterval = 10;

		// Token: 0x04000C75 RID: 3189
		private bool _encrypt;

		// Token: 0x04000C76 RID: 3190
		private bool _trustServerCertificate;

		// Token: 0x04000C77 RID: 3191
		private bool _enlist = true;

		// Token: 0x04000C78 RID: 3192
		private bool _integratedSecurity;

		// Token: 0x04000C79 RID: 3193
		private bool _multipleActiveResultSets;

		// Token: 0x04000C7A RID: 3194
		private bool _multiSubnetFailover;

		// Token: 0x04000C7B RID: 3195
		private bool _persistSecurityInfo;

		// Token: 0x04000C7C RID: 3196
		private bool _pooling = true;

		// Token: 0x04000C7D RID: 3197
		private bool _replication;

		// Token: 0x04000C7E RID: 3198
		private bool _userInstance;

		// Token: 0x04000C7F RID: 3199
		private static readonly string[] s_notSupportedKeywords = new string[]
		{
			"Asynchronous Processing",
			"Connection Reset",
			"Context Connection",
			"Transaction Binding",
			"async"
		};

		// Token: 0x04000C80 RID: 3200
		private static readonly string[] s_notSupportedNetworkLibraryKeywords = new string[]
		{
			"Network Library",
			"net",
			"network"
		};

		// Token: 0x04000C81 RID: 3201
		[CompilerGenerated]
		private bool <AsynchronousProcessing>k__BackingField;

		// Token: 0x04000C82 RID: 3202
		[CompilerGenerated]
		private bool <ConnectionReset>k__BackingField;

		// Token: 0x0200017D RID: 381
		private enum Keywords
		{
			// Token: 0x04000C84 RID: 3204
			DataSource,
			// Token: 0x04000C85 RID: 3205
			FailoverPartner,
			// Token: 0x04000C86 RID: 3206
			AttachDBFilename,
			// Token: 0x04000C87 RID: 3207
			InitialCatalog,
			// Token: 0x04000C88 RID: 3208
			IntegratedSecurity,
			// Token: 0x04000C89 RID: 3209
			PersistSecurityInfo,
			// Token: 0x04000C8A RID: 3210
			UserID,
			// Token: 0x04000C8B RID: 3211
			Password,
			// Token: 0x04000C8C RID: 3212
			Enlist,
			// Token: 0x04000C8D RID: 3213
			Pooling,
			// Token: 0x04000C8E RID: 3214
			MinPoolSize,
			// Token: 0x04000C8F RID: 3215
			MaxPoolSize,
			// Token: 0x04000C90 RID: 3216
			MultipleActiveResultSets,
			// Token: 0x04000C91 RID: 3217
			Replication,
			// Token: 0x04000C92 RID: 3218
			ConnectTimeout,
			// Token: 0x04000C93 RID: 3219
			Encrypt,
			// Token: 0x04000C94 RID: 3220
			TrustServerCertificate,
			// Token: 0x04000C95 RID: 3221
			LoadBalanceTimeout,
			// Token: 0x04000C96 RID: 3222
			PacketSize,
			// Token: 0x04000C97 RID: 3223
			TypeSystemVersion,
			// Token: 0x04000C98 RID: 3224
			ApplicationName,
			// Token: 0x04000C99 RID: 3225
			CurrentLanguage,
			// Token: 0x04000C9A RID: 3226
			WorkstationID,
			// Token: 0x04000C9B RID: 3227
			UserInstance,
			// Token: 0x04000C9C RID: 3228
			TransactionBinding,
			// Token: 0x04000C9D RID: 3229
			ApplicationIntent,
			// Token: 0x04000C9E RID: 3230
			MultiSubnetFailover,
			// Token: 0x04000C9F RID: 3231
			ConnectRetryCount,
			// Token: 0x04000CA0 RID: 3232
			ConnectRetryInterval,
			// Token: 0x04000CA1 RID: 3233
			KeywordsCount
		}

		// Token: 0x0200017E RID: 382
		private sealed class SqlInitialCatalogConverter : StringConverter
		{
			// Token: 0x060011BA RID: 4538 RVA: 0x0002D05C File Offset: 0x0002B25C
			public SqlInitialCatalogConverter()
			{
			}

			// Token: 0x060011BB RID: 4539 RVA: 0x0005A377 File Offset: 0x00058577
			public override bool GetStandardValuesSupported(ITypeDescriptorContext context)
			{
				return this.GetStandardValuesSupportedInternal(context);
			}

			// Token: 0x060011BC RID: 4540 RVA: 0x0005A380 File Offset: 0x00058580
			private bool GetStandardValuesSupportedInternal(ITypeDescriptorContext context)
			{
				bool result = false;
				if (context != null)
				{
					SqlConnectionStringBuilder sqlConnectionStringBuilder = context.Instance as SqlConnectionStringBuilder;
					if (sqlConnectionStringBuilder != null && 0 < sqlConnectionStringBuilder.DataSource.Length && (sqlConnectionStringBuilder.IntegratedSecurity || 0 < sqlConnectionStringBuilder.UserID.Length))
					{
						result = true;
					}
				}
				return result;
			}

			// Token: 0x060011BD RID: 4541 RVA: 0x000061C5 File Offset: 0x000043C5
			public override bool GetStandardValuesExclusive(ITypeDescriptorContext context)
			{
				return false;
			}

			// Token: 0x060011BE RID: 4542 RVA: 0x0005A3C8 File Offset: 0x000585C8
			public override TypeConverter.StandardValuesCollection GetStandardValues(ITypeDescriptorContext context)
			{
				if (this.GetStandardValuesSupportedInternal(context))
				{
					List<string> list = new List<string>();
					try
					{
						SqlConnectionStringBuilder sqlConnectionStringBuilder = (SqlConnectionStringBuilder)context.Instance;
						using (SqlConnection sqlConnection = new SqlConnection())
						{
							sqlConnection.ConnectionString = sqlConnectionStringBuilder.ConnectionString;
							sqlConnection.Open();
							foreach (object obj in sqlConnection.GetSchema("DATABASES").Rows)
							{
								string item = (string)((DataRow)obj)["database_name"];
								list.Add(item);
							}
						}
					}
					catch (SqlException e)
					{
						ADP.TraceExceptionWithoutRethrow(e);
					}
					return new TypeConverter.StandardValuesCollection(list);
				}
				return null;
			}
		}
	}
}
