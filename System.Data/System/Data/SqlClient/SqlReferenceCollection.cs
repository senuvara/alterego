﻿using System;
using System.Data.ProviderBase;
using System.Runtime.CompilerServices;

namespace System.Data.SqlClient
{
	// Token: 0x020001B9 RID: 441
	internal sealed class SqlReferenceCollection : DbReferenceCollection
	{
		// Token: 0x06001480 RID: 5248 RVA: 0x000695F6 File Offset: 0x000677F6
		public override void Add(object value, int tag)
		{
			base.AddItem(value, tag);
		}

		// Token: 0x06001481 RID: 5249 RVA: 0x00069600 File Offset: 0x00067800
		internal void Deactivate()
		{
			base.Notify(0);
		}

		// Token: 0x06001482 RID: 5250 RVA: 0x0006960C File Offset: 0x0006780C
		internal SqlDataReader FindLiveReader(SqlCommand command)
		{
			if (command == null)
			{
				return base.FindItem<SqlDataReader>(1, (SqlDataReader dataReader) => !dataReader.IsClosed);
			}
			return base.FindItem<SqlDataReader>(1, (SqlDataReader dataReader) => !dataReader.IsClosed && command == dataReader.Command);
		}

		// Token: 0x06001483 RID: 5251 RVA: 0x00069668 File Offset: 0x00067868
		internal SqlCommand FindLiveCommand(TdsParserStateObject stateObj)
		{
			return base.FindItem<SqlCommand>(2, (SqlCommand command) => command.StateObject == stateObj);
		}

		// Token: 0x06001484 RID: 5252 RVA: 0x00069698 File Offset: 0x00067898
		protected override void NotifyItem(int message, int tag, object value)
		{
			if (tag == 1)
			{
				SqlDataReader sqlDataReader = (SqlDataReader)value;
				if (!sqlDataReader.IsClosed)
				{
					sqlDataReader.CloseReaderFromConnection();
					return;
				}
			}
			else
			{
				if (tag == 2)
				{
					((SqlCommand)value).OnConnectionClosed();
					return;
				}
				if (tag == 3)
				{
					((SqlBulkCopy)value).OnConnectionClosed();
				}
			}
		}

		// Token: 0x06001485 RID: 5253 RVA: 0x000696DE File Offset: 0x000678DE
		public override void Remove(object value)
		{
			base.RemoveItem(value);
		}

		// Token: 0x06001486 RID: 5254 RVA: 0x000696E7 File Offset: 0x000678E7
		public SqlReferenceCollection()
		{
		}

		// Token: 0x04000E5D RID: 3677
		internal const int DataReaderTag = 1;

		// Token: 0x04000E5E RID: 3678
		internal const int CommandTag = 2;

		// Token: 0x04000E5F RID: 3679
		internal const int BulkCopyTag = 3;

		// Token: 0x020001BA RID: 442
		[CompilerGenerated]
		private sealed class <>c__DisplayClass5_0
		{
			// Token: 0x06001487 RID: 5255 RVA: 0x00005C14 File Offset: 0x00003E14
			public <>c__DisplayClass5_0()
			{
			}

			// Token: 0x06001488 RID: 5256 RVA: 0x000696EF File Offset: 0x000678EF
			internal bool <FindLiveReader>b__1(SqlDataReader dataReader)
			{
				return !dataReader.IsClosed && this.command == dataReader.Command;
			}

			// Token: 0x04000E60 RID: 3680
			public SqlCommand command;
		}

		// Token: 0x020001BB RID: 443
		[CompilerGenerated]
		[Serializable]
		private sealed class <>c
		{
			// Token: 0x06001489 RID: 5257 RVA: 0x00069709 File Offset: 0x00067909
			// Note: this type is marked as 'beforefieldinit'.
			static <>c()
			{
			}

			// Token: 0x0600148A RID: 5258 RVA: 0x00005C14 File Offset: 0x00003E14
			public <>c()
			{
			}

			// Token: 0x0600148B RID: 5259 RVA: 0x00069715 File Offset: 0x00067915
			internal bool <FindLiveReader>b__5_0(SqlDataReader dataReader)
			{
				return !dataReader.IsClosed;
			}

			// Token: 0x04000E61 RID: 3681
			public static readonly SqlReferenceCollection.<>c <>9 = new SqlReferenceCollection.<>c();

			// Token: 0x04000E62 RID: 3682
			public static Func<SqlDataReader, bool> <>9__5_0;
		}

		// Token: 0x020001BC RID: 444
		[CompilerGenerated]
		private sealed class <>c__DisplayClass6_0
		{
			// Token: 0x0600148C RID: 5260 RVA: 0x00005C14 File Offset: 0x00003E14
			public <>c__DisplayClass6_0()
			{
			}

			// Token: 0x0600148D RID: 5261 RVA: 0x00069720 File Offset: 0x00067920
			internal bool <FindLiveCommand>b__0(SqlCommand command)
			{
				return command.StateObject == this.stateObj;
			}

			// Token: 0x04000E63 RID: 3683
			public TdsParserStateObject stateObj;
		}
	}
}
