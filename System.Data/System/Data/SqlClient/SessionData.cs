﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace System.Data.SqlClient
{
	// Token: 0x020001A9 RID: 425
	internal class SessionData
	{
		// Token: 0x06001380 RID: 4992 RVA: 0x00065150 File Offset: 0x00063350
		public SessionData(SessionData recoveryData)
		{
			this._initialDatabase = recoveryData._initialDatabase;
			this._initialCollation = recoveryData._initialCollation;
			this._initialLanguage = recoveryData._initialLanguage;
			this._resolvedAliases = recoveryData._resolvedAliases;
			for (int i = 0; i < 256; i++)
			{
				if (recoveryData._initialState[i] != null)
				{
					this._initialState[i] = (byte[])recoveryData._initialState[i].Clone();
				}
			}
		}

		// Token: 0x06001381 RID: 4993 RVA: 0x000651E7 File Offset: 0x000633E7
		public SessionData()
		{
			this._resolvedAliases = new Dictionary<string, Tuple<string, string>>(2);
		}

		// Token: 0x06001382 RID: 4994 RVA: 0x0006521B File Offset: 0x0006341B
		public void Reset()
		{
			this._database = null;
			this._collation = null;
			this._language = null;
			if (this._deltaDirty)
			{
				this._delta = new SessionStateRecord[256];
				this._deltaDirty = false;
			}
			this._unrecoverableStatesCount = 0;
		}

		// Token: 0x06001383 RID: 4995 RVA: 0x00065258 File Offset: 0x00063458
		[Conditional("DEBUG")]
		public void AssertUnrecoverableStateCountIsCorrect()
		{
			byte b = 0;
			foreach (SessionStateRecord sessionStateRecord in this._delta)
			{
				if (sessionStateRecord != null && !sessionStateRecord._recoverable)
				{
					b += 1;
				}
			}
		}

		// Token: 0x04000DE1 RID: 3553
		internal const int _maxNumberOfSessionStates = 256;

		// Token: 0x04000DE2 RID: 3554
		internal uint _tdsVersion;

		// Token: 0x04000DE3 RID: 3555
		internal bool _encrypted;

		// Token: 0x04000DE4 RID: 3556
		internal string _database;

		// Token: 0x04000DE5 RID: 3557
		internal SqlCollation _collation;

		// Token: 0x04000DE6 RID: 3558
		internal string _language;

		// Token: 0x04000DE7 RID: 3559
		internal string _initialDatabase;

		// Token: 0x04000DE8 RID: 3560
		internal SqlCollation _initialCollation;

		// Token: 0x04000DE9 RID: 3561
		internal string _initialLanguage;

		// Token: 0x04000DEA RID: 3562
		internal byte _unrecoverableStatesCount;

		// Token: 0x04000DEB RID: 3563
		internal Dictionary<string, Tuple<string, string>> _resolvedAliases;

		// Token: 0x04000DEC RID: 3564
		internal SessionStateRecord[] _delta = new SessionStateRecord[256];

		// Token: 0x04000DED RID: 3565
		internal bool _deltaDirty;

		// Token: 0x04000DEE RID: 3566
		internal byte[][] _initialState = new byte[256][];
	}
}
