﻿using System;

namespace System.Data.SqlClient
{
	// Token: 0x020001F8 RID: 504
	internal enum PreLoginOptions
	{
		// Token: 0x040010DF RID: 4319
		VERSION,
		// Token: 0x040010E0 RID: 4320
		ENCRYPT,
		// Token: 0x040010E1 RID: 4321
		INSTANCE,
		// Token: 0x040010E2 RID: 4322
		THREADID,
		// Token: 0x040010E3 RID: 4323
		MARS,
		// Token: 0x040010E4 RID: 4324
		TRACEID,
		// Token: 0x040010E5 RID: 4325
		NUMOPT,
		// Token: 0x040010E6 RID: 4326
		LASTOPT = 255
	}
}
