﻿using System;

namespace System.Data.SqlClient
{
	/// <summary>Specifies that Always Encrypted functionality is enabled in a connection. Note that these settings cannot be used to bypass encryption and gain access to plaintext data. For details, see Always Encrypted (Database Engine).</summary>
	// Token: 0x0200021D RID: 541
	public enum SqlConnectionColumnEncryptionSetting
	{
		/// <summary>Specifies the connection does not use Always Encrypted. Should be used if no queries sent over the connection access encrypted columns.</summary>
		// Token: 0x040011EF RID: 4591
		Disabled,
		/// <summary>Enables Always Encrypted functionality for the connection. Query parameters that correspond to encrypted columns will be transparently encrypted and query results from encrypted columns will be transparently decrypted.</summary>
		// Token: 0x040011F0 RID: 4592
		Enabled
	}
}
