﻿using System;

namespace System.Data.SqlClient
{
	// Token: 0x0200017F RID: 383
	internal enum SqlConnectionTimeoutErrorPhase
	{
		// Token: 0x04000CA3 RID: 3235
		Undefined,
		// Token: 0x04000CA4 RID: 3236
		PreLoginBegin,
		// Token: 0x04000CA5 RID: 3237
		InitializeConnection,
		// Token: 0x04000CA6 RID: 3238
		SendPreLoginHandshake,
		// Token: 0x04000CA7 RID: 3239
		ConsumePreLoginHandshake,
		// Token: 0x04000CA8 RID: 3240
		LoginBegin,
		// Token: 0x04000CA9 RID: 3241
		ProcessConnectionAuth,
		// Token: 0x04000CAA RID: 3242
		PostLogin,
		// Token: 0x04000CAB RID: 3243
		Complete,
		// Token: 0x04000CAC RID: 3244
		Count
	}
}
