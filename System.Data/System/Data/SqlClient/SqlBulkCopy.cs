﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlTypes;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;

namespace System.Data.SqlClient
{
	/// <summary>Lets you efficiently bulk load a SQL Server table with data from another source.</summary>
	// Token: 0x02000134 RID: 308
	public sealed class SqlBulkCopy : IDisposable
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Data.SqlClient.SqlBulkCopy" /> class using the specified open instance of <see cref="T:System.Data.SqlClient.SqlConnection" />. </summary>
		/// <param name="connection">The already open <see cref="T:System.Data.SqlClient.SqlConnection" /> instance that will be used to perform the bulk copy operation. If your connection string does not use <see langword="Integrated Security = true" />, you can use <see cref="T:System.Data.SqlClient.SqlCredential" /> to pass the user ID and password more securely than by specifying the user ID and password as text in the connection string.</param>
		// Token: 0x06000ED3 RID: 3795 RVA: 0x0004D5D5 File Offset: 0x0004B7D5
		public SqlBulkCopy(SqlConnection connection)
		{
			if (connection == null)
			{
				throw ADP.ArgumentNull("connection");
			}
			this._connection = connection;
			this._columnMappings = new SqlBulkCopyColumnMappingCollection();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Data.SqlClient.SqlBulkCopy" /> class using the supplied existing open instance of <see cref="T:System.Data.SqlClient.SqlConnection" />. The <see cref="T:System.Data.SqlClient.SqlBulkCopy" /> instance behaves according to options supplied in the <paramref name="copyOptions" /> parameter. If a non-null <see cref="T:System.Data.SqlClient.SqlTransaction" /> is supplied, the copy operations will be performed within that transaction.</summary>
		/// <param name="connection">The already open <see cref="T:System.Data.SqlClient.SqlConnection" /> instance that will be used to perform the bulk copy. If your connection string does not use <see langword="Integrated Security = true" />, you can use <see cref="T:System.Data.SqlClient.SqlCredential" /> to pass the user ID and password more securely than by specifying the user ID and password as text in the connection string.</param>
		/// <param name="copyOptions">A combination of values from the <see cref="T:System.Data.SqlClient.SqlBulkCopyOptions" />  enumeration that determines which data source rows are copied to the destination table.</param>
		/// <param name="externalTransaction">An existing <see cref="T:System.Data.SqlClient.SqlTransaction" /> instance under which the bulk copy will occur.</param>
		// Token: 0x06000ED4 RID: 3796 RVA: 0x0004D605 File Offset: 0x0004B805
		public SqlBulkCopy(SqlConnection connection, SqlBulkCopyOptions copyOptions, SqlTransaction externalTransaction) : this(connection)
		{
			this._copyOptions = copyOptions;
			if (externalTransaction != null && this.IsCopyOption(SqlBulkCopyOptions.UseInternalTransaction))
			{
				throw SQL.BulkLoadConflictingTransactionOption();
			}
			if (!this.IsCopyOption(SqlBulkCopyOptions.UseInternalTransaction))
			{
				this._externalTransaction = externalTransaction;
			}
		}

		/// <summary>Initializes and opens a new instance of <see cref="T:System.Data.SqlClient.SqlConnection" /> based on the supplied <paramref name="connectionString" />. The constructor uses the <see cref="T:System.Data.SqlClient.SqlConnection" /> to initialize a new instance of the <see cref="T:System.Data.SqlClient.SqlBulkCopy" /> class.</summary>
		/// <param name="connectionString">The string defining the connection that will be opened for use by the <see cref="T:System.Data.SqlClient.SqlBulkCopy" /> instance. If your connection string does not use <see langword="Integrated Security = true" />, you can use <see cref="M:System.Data.SqlClient.SqlBulkCopy.#ctor(System.Data.SqlClient.SqlConnection)" /> or <see cref="M:System.Data.SqlClient.SqlBulkCopy.#ctor(System.Data.SqlClient.SqlConnection,System.Data.SqlClient.SqlBulkCopyOptions,System.Data.SqlClient.SqlTransaction)" /> and <see cref="T:System.Data.SqlClient.SqlCredential" /> to pass the user ID and password more securely than by specifying the user ID and password as text in the connection string.</param>
		// Token: 0x06000ED5 RID: 3797 RVA: 0x0004D639 File Offset: 0x0004B839
		public SqlBulkCopy(string connectionString) : this(new SqlConnection(connectionString))
		{
			if (connectionString == null)
			{
				throw ADP.ArgumentNull("connectionString");
			}
			this._connection = new SqlConnection(connectionString);
			this._columnMappings = new SqlBulkCopyColumnMappingCollection();
			this._ownConnection = true;
		}

		/// <summary>Initializes and opens a new instance of <see cref="T:System.Data.SqlClient.SqlConnection" /> based on the supplied <paramref name="connectionString" />. The constructor uses that <see cref="T:System.Data.SqlClient.SqlConnection" /> to initialize a new instance of the <see cref="T:System.Data.SqlClient.SqlBulkCopy" /> class. The <see cref="T:System.Data.SqlClient.SqlConnection" /> instance behaves according to options supplied in the <paramref name="copyOptions" /> parameter.</summary>
		/// <param name="connectionString">The string defining the connection that will be opened for use by the <see cref="T:System.Data.SqlClient.SqlBulkCopy" /> instance. If your connection string does not use <see langword="Integrated Security = true" />, you can use <see cref="M:System.Data.SqlClient.SqlBulkCopy.#ctor(System.Data.SqlClient.SqlConnection)" /> or <see cref="M:System.Data.SqlClient.SqlBulkCopy.#ctor(System.Data.SqlClient.SqlConnection,System.Data.SqlClient.SqlBulkCopyOptions,System.Data.SqlClient.SqlTransaction)" /> and <see cref="T:System.Data.SqlClient.SqlCredential" /> to pass the user ID and password more securely than by specifying the user ID and password as text in the connection string.</param>
		/// <param name="copyOptions">A combination of values from the <see cref="T:System.Data.SqlClient.SqlBulkCopyOptions" />  enumeration that determines which data source rows are copied to the destination table.</param>
		// Token: 0x06000ED6 RID: 3798 RVA: 0x0004D673 File Offset: 0x0004B873
		public SqlBulkCopy(string connectionString, SqlBulkCopyOptions copyOptions) : this(connectionString)
		{
			this._copyOptions = copyOptions;
		}

		/// <summary>Number of rows in each batch. At the end of each batch, the rows in the batch are sent to the server.</summary>
		/// <returns>The integer value of the <see cref="P:System.Data.SqlClient.SqlBulkCopy.BatchSize" /> property, or zero if no value has been set.</returns>
		// Token: 0x1700029F RID: 671
		// (get) Token: 0x06000ED7 RID: 3799 RVA: 0x0004D683 File Offset: 0x0004B883
		// (set) Token: 0x06000ED8 RID: 3800 RVA: 0x0004D68B File Offset: 0x0004B88B
		public int BatchSize
		{
			get
			{
				return this._batchSize;
			}
			set
			{
				if (value >= 0)
				{
					this._batchSize = value;
					return;
				}
				throw ADP.ArgumentOutOfRange("BatchSize");
			}
		}

		/// <summary>Number of seconds for the operation to complete before it times out.</summary>
		/// <returns>The integer value of the <see cref="P:System.Data.SqlClient.SqlBulkCopy.BulkCopyTimeout" /> property. The default is 30 seconds. A value of 0 indicates no limit; the bulk copy will wait indefinitely.</returns>
		// Token: 0x170002A0 RID: 672
		// (get) Token: 0x06000ED9 RID: 3801 RVA: 0x0004D6A3 File Offset: 0x0004B8A3
		// (set) Token: 0x06000EDA RID: 3802 RVA: 0x0004D6AB File Offset: 0x0004B8AB
		public int BulkCopyTimeout
		{
			get
			{
				return this._timeout;
			}
			set
			{
				if (value < 0)
				{
					throw SQL.BulkLoadInvalidTimeout(value);
				}
				this._timeout = value;
			}
		}

		/// <summary>Enables or disables a <see cref="T:System.Data.SqlClient.SqlBulkCopy" /> object to stream data from an <see cref="T:System.Data.IDataReader" /> object</summary>
		/// <returns>
		///     <see langword="true" /> if a <see cref="T:System.Data.SqlClient.SqlBulkCopy" /> object can stream data from an <see cref="T:System.Data.IDataReader" /> object; otherwise, false. The default is <see langword="false" />.</returns>
		// Token: 0x170002A1 RID: 673
		// (get) Token: 0x06000EDB RID: 3803 RVA: 0x0004D6BF File Offset: 0x0004B8BF
		// (set) Token: 0x06000EDC RID: 3804 RVA: 0x0004D6C7 File Offset: 0x0004B8C7
		public bool EnableStreaming
		{
			get
			{
				return this._enableStreaming;
			}
			set
			{
				this._enableStreaming = value;
			}
		}

		/// <summary>Returns a collection of <see cref="T:System.Data.SqlClient.SqlBulkCopyColumnMapping" /> items. Column mappings define the relationships between columns in the data source and columns in the destination.</summary>
		/// <returns>A collection of column mappings. By default, it is an empty collection.</returns>
		// Token: 0x170002A2 RID: 674
		// (get) Token: 0x06000EDD RID: 3805 RVA: 0x0004D6D0 File Offset: 0x0004B8D0
		public SqlBulkCopyColumnMappingCollection ColumnMappings
		{
			get
			{
				return this._columnMappings;
			}
		}

		/// <summary>Name of the destination table on the server. </summary>
		/// <returns>The string value of the <see cref="P:System.Data.SqlClient.SqlBulkCopy.DestinationTableName" /> property, or null if none as been supplied.</returns>
		// Token: 0x170002A3 RID: 675
		// (get) Token: 0x06000EDE RID: 3806 RVA: 0x0004D6D8 File Offset: 0x0004B8D8
		// (set) Token: 0x06000EDF RID: 3807 RVA: 0x0004D6E0 File Offset: 0x0004B8E0
		public string DestinationTableName
		{
			get
			{
				return this._destinationTableName;
			}
			set
			{
				if (value == null)
				{
					throw ADP.ArgumentNull("DestinationTableName");
				}
				if (value.Length == 0)
				{
					throw ADP.ArgumentOutOfRange("DestinationTableName");
				}
				this._destinationTableName = value;
			}
		}

		/// <summary>Defines the number of rows to be processed before generating a notification event.</summary>
		/// <returns>The integer value of the <see cref="P:System.Data.SqlClient.SqlBulkCopy.NotifyAfter" /> property, or zero if the property has not been set.</returns>
		// Token: 0x170002A4 RID: 676
		// (get) Token: 0x06000EE0 RID: 3808 RVA: 0x0004D70A File Offset: 0x0004B90A
		// (set) Token: 0x06000EE1 RID: 3809 RVA: 0x0004D712 File Offset: 0x0004B912
		public int NotifyAfter
		{
			get
			{
				return this._notifyAfter;
			}
			set
			{
				if (value >= 0)
				{
					this._notifyAfter = value;
					return;
				}
				throw ADP.ArgumentOutOfRange("NotifyAfter");
			}
		}

		/// <summary>Occurs every time that the number of rows specified by the <see cref="P:System.Data.SqlClient.SqlBulkCopy.NotifyAfter" /> property have been processed.</summary>
		// Token: 0x14000020 RID: 32
		// (add) Token: 0x06000EE2 RID: 3810 RVA: 0x0004D72A File Offset: 0x0004B92A
		// (remove) Token: 0x06000EE3 RID: 3811 RVA: 0x0004D743 File Offset: 0x0004B943
		public event SqlRowsCopiedEventHandler SqlRowsCopied
		{
			add
			{
				this._rowsCopiedEventHandler = (SqlRowsCopiedEventHandler)Delegate.Combine(this._rowsCopiedEventHandler, value);
			}
			remove
			{
				this._rowsCopiedEventHandler = (SqlRowsCopiedEventHandler)Delegate.Remove(this._rowsCopiedEventHandler, value);
			}
		}

		// Token: 0x170002A5 RID: 677
		// (get) Token: 0x06000EE4 RID: 3812 RVA: 0x0004D75C File Offset: 0x0004B95C
		internal SqlStatistics Statistics
		{
			get
			{
				if (this._connection != null && this._connection.StatisticsEnabled)
				{
					return this._connection.Statistics;
				}
				return null;
			}
		}

		/// <summary>Releases all resources used by the current instance of the <see cref="T:System.Data.SqlClient.SqlBulkCopy" /> class.</summary>
		// Token: 0x06000EE5 RID: 3813 RVA: 0x0004D780 File Offset: 0x0004B980
		void IDisposable.Dispose()
		{
			this.Dispose(true);
			GC.SuppressFinalize(this);
		}

		// Token: 0x06000EE6 RID: 3814 RVA: 0x0004D78F File Offset: 0x0004B98F
		private bool IsCopyOption(SqlBulkCopyOptions copyOption)
		{
			return (this._copyOptions & copyOption) == copyOption;
		}

		// Token: 0x06000EE7 RID: 3815 RVA: 0x0004D79C File Offset: 0x0004B99C
		private string CreateInitialQuery()
		{
			string[] array;
			try
			{
				array = MultipartIdentifier.ParseMultipartIdentifier(this.DestinationTableName, "[\"", "]\"", "SqlBulkCopy.WriteToServer failed because the SqlBulkCopy.DestinationTableName is an invalid multipart name", true);
			}
			catch (Exception inner)
			{
				throw SQL.BulkLoadInvalidDestinationTable(this.DestinationTableName, inner);
			}
			if (string.IsNullOrEmpty(array[3]))
			{
				throw SQL.BulkLoadInvalidDestinationTable(this.DestinationTableName, null);
			}
			string text = "select @@trancount; SET FMTONLY ON select * from " + this.DestinationTableName + " SET FMTONLY OFF ";
			string text2;
			if (this._connection.IsKatmaiOrNewer)
			{
				text2 = "sp_tablecollations_100";
			}
			else
			{
				text2 = "sp_tablecollations_90";
			}
			string text3 = array[3];
			bool flag = text3.Length > 0 && '#' == text3[0];
			if (!string.IsNullOrEmpty(text3))
			{
				text3 = SqlServerEscapeHelper.EscapeStringAsLiteral(text3);
				text3 = SqlServerEscapeHelper.EscapeIdentifier(text3);
			}
			string text4 = array[2];
			if (!string.IsNullOrEmpty(text4))
			{
				text4 = SqlServerEscapeHelper.EscapeStringAsLiteral(text4);
				text4 = SqlServerEscapeHelper.EscapeIdentifier(text4);
			}
			string text5 = array[1];
			if (flag && string.IsNullOrEmpty(text5))
			{
				text += string.Format(null, "exec tempdb..{0} N'{1}.{2}'", text2, text4, text3);
			}
			else
			{
				if (!string.IsNullOrEmpty(text5))
				{
					text5 = SqlServerEscapeHelper.EscapeIdentifier(text5);
				}
				text += string.Format(null, "exec {0}..{1} N'{2}.{3}'", new object[]
				{
					text5,
					text2,
					text4,
					text3
				});
			}
			return text;
		}

		// Token: 0x06000EE8 RID: 3816 RVA: 0x0004D8E8 File Offset: 0x0004BAE8
		private Task<BulkCopySimpleResultSet> CreateAndExecuteInitialQueryAsync(out BulkCopySimpleResultSet result)
		{
			string text = this.CreateInitialQuery();
			Task task = this._parser.TdsExecuteSQLBatch(text, this.BulkCopyTimeout, null, this._stateObj, !this._isAsyncBulkCopy, true);
			if (task == null)
			{
				result = new BulkCopySimpleResultSet();
				this.RunParser(result);
				return null;
			}
			result = null;
			return task.ContinueWith<BulkCopySimpleResultSet>(delegate(Task t)
			{
				if (t.IsFaulted)
				{
					throw t.Exception.InnerException;
				}
				BulkCopySimpleResultSet bulkCopySimpleResultSet = new BulkCopySimpleResultSet();
				this.RunParserReliably(bulkCopySimpleResultSet);
				return bulkCopySimpleResultSet;
			}, TaskScheduler.Default);
		}

		// Token: 0x06000EE9 RID: 3817 RVA: 0x0004D950 File Offset: 0x0004BB50
		private string AnalyzeTargetAndCreateUpdateBulkCommand(BulkCopySimpleResultSet internalResults)
		{
			StringBuilder stringBuilder = new StringBuilder();
			if (internalResults[2].Count == 0)
			{
				throw SQL.BulkLoadNoCollation();
			}
			stringBuilder.AppendFormat("insert bulk {0} (", this.DestinationTableName);
			int num = 0;
			int num2 = 0;
			if (this._connection.HasLocalTransaction && this._externalTransaction == null && this._internalTransaction == null && this._connection.Parser != null && this._connection.Parser.CurrentTransaction != null && this._connection.Parser.CurrentTransaction.IsLocal)
			{
				throw SQL.BulkLoadExistingTransaction();
			}
			_SqlMetaDataSet metaData = internalResults[1].MetaData;
			this._sortedColumnMappings = new List<_ColumnMapping>(metaData.Length);
			for (int i = 0; i < metaData.Length; i++)
			{
				_SqlMetaData sqlMetaData = metaData[i];
				bool flag = false;
				if (sqlMetaData.type == SqlDbType.Timestamp || (sqlMetaData.isIdentity && !this.IsCopyOption(SqlBulkCopyOptions.KeepIdentity)))
				{
					metaData[i] = null;
					flag = true;
				}
				int j = 0;
				while (j < this._localColumnMappings.Count)
				{
					if (this._localColumnMappings[j]._destinationColumnOrdinal == sqlMetaData.ordinal || this.UnquotedName(this._localColumnMappings[j]._destinationColumnName) == sqlMetaData.column)
					{
						if (flag)
						{
							num2++;
							break;
						}
						this._sortedColumnMappings.Add(new _ColumnMapping(this._localColumnMappings[j]._internalSourceColumnOrdinal, sqlMetaData));
						num++;
						if (num > 1)
						{
							stringBuilder.Append(", ");
						}
						if (sqlMetaData.type == SqlDbType.Variant)
						{
							this.AppendColumnNameAndTypeName(stringBuilder, sqlMetaData.column, "sql_variant");
						}
						else
						{
							if (sqlMetaData.type == SqlDbType.Udt)
							{
								throw ADP.DbTypeNotSupported(SqlDbType.Udt.ToString());
							}
							this.AppendColumnNameAndTypeName(stringBuilder, sqlMetaData.column, sqlMetaData.type.ToString());
						}
						byte nullableType = sqlMetaData.metaType.NullableType;
						if (nullableType <= 106)
						{
							if (nullableType - 41 > 2)
							{
								if (nullableType != 106)
								{
									goto IL_29B;
								}
								goto IL_217;
							}
							else
							{
								stringBuilder.AppendFormat(null, "({0})", sqlMetaData.scale);
							}
						}
						else
						{
							if (nullableType == 108)
							{
								goto IL_217;
							}
							if (nullableType != 240)
							{
								goto IL_29B;
							}
							if (sqlMetaData.IsLargeUdt)
							{
								stringBuilder.Append("(max)");
							}
							else
							{
								int length = sqlMetaData.length;
								stringBuilder.AppendFormat(null, "({0})", length);
							}
						}
						IL_32C:
						object obj = internalResults[2][i][3];
						SqlDbType type = sqlMetaData.type;
						if (type <= SqlDbType.NVarChar)
						{
							if (type != SqlDbType.Char && type - SqlDbType.NChar > 2)
							{
								goto IL_371;
							}
							goto IL_36C;
						}
						else
						{
							if (type == SqlDbType.Text || type == SqlDbType.VarChar)
							{
								goto IL_36C;
							}
							goto IL_371;
						}
						IL_374:
						bool flag2;
						if (obj == null || !flag2)
						{
							break;
						}
						SqlString sqlString = (SqlString)obj;
						if (sqlString.IsNull)
						{
							break;
						}
						stringBuilder.Append(" COLLATE " + sqlString.Value);
						if (this._SqlDataReaderRowSource == null || sqlMetaData.collation == null)
						{
							break;
						}
						int internalSourceColumnOrdinal = this._localColumnMappings[j]._internalSourceColumnOrdinal;
						int lcid = sqlMetaData.collation.LCID;
						int localeId = this._SqlDataReaderRowSource.GetLocaleId(internalSourceColumnOrdinal);
						if (localeId != lcid)
						{
							throw SQL.BulkLoadLcidMismatch(localeId, this._SqlDataReaderRowSource.GetName(internalSourceColumnOrdinal), lcid, sqlMetaData.column);
						}
						break;
						IL_371:
						flag2 = false;
						goto IL_374;
						IL_36C:
						flag2 = true;
						goto IL_374;
						IL_217:
						stringBuilder.AppendFormat(null, "({0},{1})", sqlMetaData.precision, sqlMetaData.scale);
						goto IL_32C;
						IL_29B:
						if (!sqlMetaData.metaType.IsFixed && !sqlMetaData.metaType.IsLong)
						{
							int num3 = sqlMetaData.length;
							nullableType = sqlMetaData.metaType.NullableType;
							if (nullableType == 99 || nullableType == 231 || nullableType == 239)
							{
								num3 /= 2;
							}
							stringBuilder.AppendFormat(null, "({0})", num3);
							goto IL_32C;
						}
						if (sqlMetaData.metaType.IsPlp && sqlMetaData.metaType.SqlDbType != SqlDbType.Xml)
						{
							stringBuilder.Append("(max)");
							goto IL_32C;
						}
						goto IL_32C;
					}
					else
					{
						j++;
					}
				}
				if (j == this._localColumnMappings.Count)
				{
					metaData[i] = null;
				}
			}
			if (num + num2 != this._localColumnMappings.Count)
			{
				throw SQL.BulkLoadNonMatchingColumnMapping();
			}
			stringBuilder.Append(")");
			if ((this._copyOptions & (SqlBulkCopyOptions.CheckConstraints | SqlBulkCopyOptions.TableLock | SqlBulkCopyOptions.KeepNulls | SqlBulkCopyOptions.FireTriggers)) != SqlBulkCopyOptions.Default)
			{
				bool flag3 = false;
				stringBuilder.Append(" with (");
				if (this.IsCopyOption(SqlBulkCopyOptions.KeepNulls))
				{
					stringBuilder.Append("KEEP_NULLS");
					flag3 = true;
				}
				if (this.IsCopyOption(SqlBulkCopyOptions.TableLock))
				{
					stringBuilder.Append((flag3 ? ", " : "") + "TABLOCK");
					flag3 = true;
				}
				if (this.IsCopyOption(SqlBulkCopyOptions.CheckConstraints))
				{
					stringBuilder.Append((flag3 ? ", " : "") + "CHECK_CONSTRAINTS");
					flag3 = true;
				}
				if (this.IsCopyOption(SqlBulkCopyOptions.FireTriggers))
				{
					stringBuilder.Append((flag3 ? ", " : "") + "FIRE_TRIGGERS");
				}
				stringBuilder.Append(")");
			}
			return stringBuilder.ToString();
		}

		// Token: 0x06000EEA RID: 3818 RVA: 0x0004DEA8 File Offset: 0x0004C0A8
		private Task SubmitUpdateBulkCommand(string TDSCommand)
		{
			Task task = this._parser.TdsExecuteSQLBatch(TDSCommand, this.BulkCopyTimeout, null, this._stateObj, !this._isAsyncBulkCopy, true);
			if (task == null)
			{
				this.RunParser(null);
				return null;
			}
			return task.ContinueWith(delegate(Task t)
			{
				if (t.IsFaulted)
				{
					throw t.Exception.InnerException;
				}
				this.RunParserReliably(null);
			}, TaskScheduler.Default);
		}

		// Token: 0x06000EEB RID: 3819 RVA: 0x0004DEFC File Offset: 0x0004C0FC
		private void WriteMetaData(BulkCopySimpleResultSet internalResults)
		{
			this._stateObj.SetTimeoutSeconds(this.BulkCopyTimeout);
			_SqlMetaDataSet metaData = internalResults[1].MetaData;
			this._stateObj._outputMessageType = 7;
			this._parser.WriteBulkCopyMetaData(metaData, this._sortedColumnMappings.Count, this._stateObj);
		}

		/// <summary>Closes the <see cref="T:System.Data.SqlClient.SqlBulkCopy" /> instance.</summary>
		// Token: 0x06000EEC RID: 3820 RVA: 0x0004DF50 File Offset: 0x0004C150
		public void Close()
		{
			if (this._insideRowsCopiedEvent)
			{
				throw SQL.InvalidOperationInsideEvent();
			}
			this.Dispose(true);
			GC.SuppressFinalize(this);
		}

		// Token: 0x06000EED RID: 3821 RVA: 0x0004DF70 File Offset: 0x0004C170
		private void Dispose(bool disposing)
		{
			if (disposing)
			{
				this._columnMappings = null;
				this._parser = null;
				try
				{
					if (this._internalTransaction != null)
					{
						this._internalTransaction.Rollback();
						this._internalTransaction.Dispose();
						this._internalTransaction = null;
					}
				}
				catch (Exception e)
				{
					if (!ADP.IsCatchableExceptionType(e))
					{
						throw;
					}
				}
				finally
				{
					if (this._connection != null)
					{
						if (this._ownConnection)
						{
							this._connection.Dispose();
						}
						this._connection = null;
					}
				}
			}
		}

		// Token: 0x06000EEE RID: 3822 RVA: 0x0004E000 File Offset: 0x0004C200
		private object GetValueFromSourceRow(int destRowIndex, out bool isSqlType, out bool isDataFeed, out bool isNull)
		{
			_SqlMetaData metadata = this._sortedColumnMappings[destRowIndex]._metadata;
			int sourceColumnOrdinal = this._sortedColumnMappings[destRowIndex]._sourceColumnOrdinal;
			switch (this._rowSourceType)
			{
			case SqlBulkCopy.ValueSourceType.IDataReader:
			case SqlBulkCopy.ValueSourceType.DbDataReader:
				if (this._currentRowMetadata[destRowIndex].IsDataFeed)
				{
					if (this._DbDataReaderRowSource.IsDBNull(sourceColumnOrdinal))
					{
						isSqlType = false;
						isDataFeed = false;
						isNull = true;
						return DBNull.Value;
					}
					isSqlType = false;
					isDataFeed = true;
					isNull = false;
					switch (this._currentRowMetadata[destRowIndex].Method)
					{
					case SqlBulkCopy.ValueMethod.DataFeedStream:
						return new StreamDataFeed(this._DbDataReaderRowSource.GetStream(sourceColumnOrdinal));
					case SqlBulkCopy.ValueMethod.DataFeedText:
						return new TextDataFeed(this._DbDataReaderRowSource.GetTextReader(sourceColumnOrdinal));
					case SqlBulkCopy.ValueMethod.DataFeedXml:
						return new XmlDataFeed(this._SqlDataReaderRowSource.GetXmlReader(sourceColumnOrdinal));
					default:
					{
						isDataFeed = false;
						object value = this._DbDataReaderRowSource.GetValue(sourceColumnOrdinal);
						ADP.IsNullOrSqlType(value, out isNull, out isSqlType);
						return value;
					}
					}
				}
				else if (this._SqlDataReaderRowSource != null)
				{
					if (this._currentRowMetadata[destRowIndex].IsSqlType)
					{
						isSqlType = true;
						isDataFeed = false;
						INullable nullable;
						switch (this._currentRowMetadata[destRowIndex].Method)
						{
						case SqlBulkCopy.ValueMethod.SqlTypeSqlDecimal:
							nullable = this._SqlDataReaderRowSource.GetSqlDecimal(sourceColumnOrdinal);
							break;
						case SqlBulkCopy.ValueMethod.SqlTypeSqlDouble:
							nullable = new SqlDecimal(this._SqlDataReaderRowSource.GetSqlDouble(sourceColumnOrdinal).Value);
							break;
						case SqlBulkCopy.ValueMethod.SqlTypeSqlSingle:
							nullable = new SqlDecimal((double)this._SqlDataReaderRowSource.GetSqlSingle(sourceColumnOrdinal).Value);
							break;
						default:
							nullable = (INullable)this._SqlDataReaderRowSource.GetSqlValue(sourceColumnOrdinal);
							break;
						}
						isNull = nullable.IsNull;
						return nullable;
					}
					isSqlType = false;
					isDataFeed = false;
					object value2 = this._SqlDataReaderRowSource.GetValue(sourceColumnOrdinal);
					isNull = (value2 == null || value2 == DBNull.Value);
					if (!isNull && metadata.type == SqlDbType.Udt)
					{
						INullable nullable2 = value2 as INullable;
						isNull = (nullable2 != null && nullable2.IsNull);
					}
					return value2;
				}
				else
				{
					isDataFeed = false;
					IDataReader dataReader = (IDataReader)this._rowSource;
					if (this._enableStreaming && this._SqlDataReaderRowSource == null && dataReader.IsDBNull(sourceColumnOrdinal))
					{
						isSqlType = false;
						isNull = true;
						return DBNull.Value;
					}
					object value3 = dataReader.GetValue(sourceColumnOrdinal);
					ADP.IsNullOrSqlType(value3, out isNull, out isSqlType);
					return value3;
				}
				break;
			case SqlBulkCopy.ValueSourceType.DataTable:
			case SqlBulkCopy.ValueSourceType.RowArray:
			{
				isDataFeed = false;
				object obj = this._currentRow[sourceColumnOrdinal];
				ADP.IsNullOrSqlType(obj, out isNull, out isSqlType);
				if (!isNull && this._currentRowMetadata[destRowIndex].IsSqlType)
				{
					switch (this._currentRowMetadata[destRowIndex].Method)
					{
					case SqlBulkCopy.ValueMethod.SqlTypeSqlDecimal:
						if (isSqlType)
						{
							return (SqlDecimal)obj;
						}
						isSqlType = true;
						return new SqlDecimal((decimal)obj);
					case SqlBulkCopy.ValueMethod.SqlTypeSqlDouble:
					{
						if (isSqlType)
						{
							return new SqlDecimal(((SqlDouble)obj).Value);
						}
						double num = (double)obj;
						if (!double.IsNaN(num))
						{
							isSqlType = true;
							return new SqlDecimal(num);
						}
						break;
					}
					case SqlBulkCopy.ValueMethod.SqlTypeSqlSingle:
					{
						if (isSqlType)
						{
							return new SqlDecimal((double)((SqlSingle)obj).Value);
						}
						float num2 = (float)obj;
						if (!float.IsNaN(num2))
						{
							isSqlType = true;
							return new SqlDecimal((double)num2);
						}
						break;
					}
					}
				}
				return obj;
			}
			default:
				throw ADP.NotSupported();
			}
		}

		// Token: 0x06000EEF RID: 3823 RVA: 0x0004E388 File Offset: 0x0004C588
		private Task ReadFromRowSourceAsync(CancellationToken cts)
		{
			if (this._isAsyncBulkCopy && this._DbDataReaderRowSource != null)
			{
				return this._DbDataReaderRowSource.ReadAsync(cts).ContinueWith<Task<bool>>(delegate(Task<bool> t)
				{
					if (t.Status == TaskStatus.RanToCompletion)
					{
						this._hasMoreRowToCopy = t.Result;
					}
					return t;
				}, TaskScheduler.Default).Unwrap<bool>();
			}
			this._hasMoreRowToCopy = false;
			try
			{
				this._hasMoreRowToCopy = this.ReadFromRowSource();
			}
			catch (Exception exception)
			{
				if (this._isAsyncBulkCopy)
				{
					return Task.FromException<bool>(exception);
				}
				throw;
			}
			return null;
		}

		// Token: 0x06000EF0 RID: 3824 RVA: 0x0004E40C File Offset: 0x0004C60C
		private bool ReadFromRowSource()
		{
			switch (this._rowSourceType)
			{
			case SqlBulkCopy.ValueSourceType.IDataReader:
			case SqlBulkCopy.ValueSourceType.DbDataReader:
				return ((IDataReader)this._rowSource).Read();
			case SqlBulkCopy.ValueSourceType.DataTable:
			case SqlBulkCopy.ValueSourceType.RowArray:
				while (this._rowEnumerator.MoveNext())
				{
					this._currentRow = (DataRow)this._rowEnumerator.Current;
					if ((this._currentRow.RowState & this._rowStateToSkip) == (DataRowState)0)
					{
						this._currentRowLength = this._currentRow.ItemArray.Length;
						return true;
					}
				}
				return false;
			default:
				throw ADP.NotSupported();
			}
		}

		// Token: 0x06000EF1 RID: 3825 RVA: 0x0004E4A0 File Offset: 0x0004C6A0
		private SqlBulkCopy.SourceColumnMetadata GetColumnMetadata(int ordinal)
		{
			int sourceColumnOrdinal = this._sortedColumnMappings[ordinal]._sourceColumnOrdinal;
			_SqlMetaData metadata = this._sortedColumnMappings[ordinal]._metadata;
			bool isDataFeed;
			bool isSqlType;
			SqlBulkCopy.ValueMethod method;
			if ((this._SqlDataReaderRowSource != null || this._dataTableSource != null) && (metadata.metaType.NullableType == 106 || metadata.metaType.NullableType == 108))
			{
				isDataFeed = false;
				Type right;
				switch (this._rowSourceType)
				{
				case SqlBulkCopy.ValueSourceType.IDataReader:
				case SqlBulkCopy.ValueSourceType.DbDataReader:
					right = this._SqlDataReaderRowSource.GetFieldType(sourceColumnOrdinal);
					break;
				case SqlBulkCopy.ValueSourceType.DataTable:
				case SqlBulkCopy.ValueSourceType.RowArray:
					right = this._dataTableSource.Columns[sourceColumnOrdinal].DataType;
					break;
				default:
					right = null;
					break;
				}
				if (typeof(SqlDecimal) == right || typeof(decimal) == right)
				{
					isSqlType = true;
					method = SqlBulkCopy.ValueMethod.SqlTypeSqlDecimal;
				}
				else if (typeof(SqlDouble) == right || typeof(double) == right)
				{
					isSqlType = true;
					method = SqlBulkCopy.ValueMethod.SqlTypeSqlDouble;
				}
				else if (typeof(SqlSingle) == right || typeof(float) == right)
				{
					isSqlType = true;
					method = SqlBulkCopy.ValueMethod.SqlTypeSqlSingle;
				}
				else
				{
					isSqlType = false;
					method = SqlBulkCopy.ValueMethod.GetValue;
				}
			}
			else if (this._enableStreaming && metadata.length == 2147483647)
			{
				isSqlType = false;
				if (this._SqlDataReaderRowSource != null)
				{
					MetaType metaType = this._SqlDataReaderRowSource.MetaData[sourceColumnOrdinal].metaType;
					if (metadata.type == SqlDbType.VarBinary && metaType.IsBinType && metaType.SqlDbType != SqlDbType.Timestamp && this._SqlDataReaderRowSource.IsCommandBehavior(CommandBehavior.SequentialAccess))
					{
						isDataFeed = true;
						method = SqlBulkCopy.ValueMethod.DataFeedStream;
					}
					else if ((metadata.type == SqlDbType.VarChar || metadata.type == SqlDbType.NVarChar) && metaType.IsCharType && metaType.SqlDbType != SqlDbType.Xml)
					{
						isDataFeed = true;
						method = SqlBulkCopy.ValueMethod.DataFeedText;
					}
					else if (metadata.type == SqlDbType.Xml && metaType.SqlDbType == SqlDbType.Xml)
					{
						isDataFeed = true;
						method = SqlBulkCopy.ValueMethod.DataFeedXml;
					}
					else
					{
						isDataFeed = false;
						method = SqlBulkCopy.ValueMethod.GetValue;
					}
				}
				else if (this._DbDataReaderRowSource != null)
				{
					if (metadata.type == SqlDbType.VarBinary)
					{
						isDataFeed = true;
						method = SqlBulkCopy.ValueMethod.DataFeedStream;
					}
					else if (metadata.type == SqlDbType.VarChar || metadata.type == SqlDbType.NVarChar)
					{
						isDataFeed = true;
						method = SqlBulkCopy.ValueMethod.DataFeedText;
					}
					else
					{
						isDataFeed = false;
						method = SqlBulkCopy.ValueMethod.GetValue;
					}
				}
				else
				{
					isDataFeed = false;
					method = SqlBulkCopy.ValueMethod.GetValue;
				}
			}
			else
			{
				isSqlType = false;
				isDataFeed = false;
				method = SqlBulkCopy.ValueMethod.GetValue;
			}
			return new SqlBulkCopy.SourceColumnMetadata(method, isSqlType, isDataFeed);
		}

		// Token: 0x06000EF2 RID: 3826 RVA: 0x0004E70C File Offset: 0x0004C90C
		private void CreateOrValidateConnection(string method)
		{
			if (this._connection == null)
			{
				throw ADP.ConnectionRequired(method);
			}
			if (this._ownConnection && this._connection.State != ConnectionState.Open)
			{
				this._connection.Open();
			}
			this._connection.ValidateConnectionForExecute(method, null);
			if (this._externalTransaction != null && this._connection != this._externalTransaction.Connection)
			{
				throw ADP.TransactionConnectionMismatch();
			}
		}

		// Token: 0x06000EF3 RID: 3827 RVA: 0x0004E778 File Offset: 0x0004C978
		private void RunParser(BulkCopySimpleResultSet bulkCopyHandler = null)
		{
			SqlInternalConnectionTds openTdsConnection = this._connection.GetOpenTdsConnection();
			openTdsConnection.ThreadHasParserLockForClose = true;
			try
			{
				this._parser.Run(RunBehavior.UntilDone, null, null, bulkCopyHandler, this._stateObj);
			}
			finally
			{
				openTdsConnection.ThreadHasParserLockForClose = false;
			}
		}

		// Token: 0x06000EF4 RID: 3828 RVA: 0x0004E7C8 File Offset: 0x0004C9C8
		private void RunParserReliably(BulkCopySimpleResultSet bulkCopyHandler = null)
		{
			SqlInternalConnectionTds openTdsConnection = this._connection.GetOpenTdsConnection();
			openTdsConnection.ThreadHasParserLockForClose = true;
			try
			{
				this._parser.Run(RunBehavior.UntilDone, null, null, bulkCopyHandler, this._stateObj);
			}
			finally
			{
				openTdsConnection.ThreadHasParserLockForClose = false;
			}
		}

		// Token: 0x06000EF5 RID: 3829 RVA: 0x0004E818 File Offset: 0x0004CA18
		private void CommitTransaction()
		{
			if (this._internalTransaction != null)
			{
				SqlInternalConnectionTds openTdsConnection = this._connection.GetOpenTdsConnection();
				openTdsConnection.ThreadHasParserLockForClose = true;
				try
				{
					this._internalTransaction.Commit();
					this._internalTransaction.Dispose();
					this._internalTransaction = null;
				}
				finally
				{
					openTdsConnection.ThreadHasParserLockForClose = false;
				}
			}
		}

		// Token: 0x06000EF6 RID: 3830 RVA: 0x0004E878 File Offset: 0x0004CA78
		private void AbortTransaction()
		{
			if (this._internalTransaction != null)
			{
				if (!this._internalTransaction.IsZombied)
				{
					SqlInternalConnectionTds openTdsConnection = this._connection.GetOpenTdsConnection();
					openTdsConnection.ThreadHasParserLockForClose = true;
					try
					{
						this._internalTransaction.Rollback();
					}
					finally
					{
						openTdsConnection.ThreadHasParserLockForClose = false;
					}
				}
				this._internalTransaction.Dispose();
				this._internalTransaction = null;
			}
		}

		// Token: 0x06000EF7 RID: 3831 RVA: 0x0004E8E4 File Offset: 0x0004CAE4
		private void AppendColumnNameAndTypeName(StringBuilder query, string columnName, string typeName)
		{
			SqlServerEscapeHelper.EscapeIdentifier(query, columnName);
			query.Append(" ");
			query.Append(typeName);
		}

		// Token: 0x06000EF8 RID: 3832 RVA: 0x0004E904 File Offset: 0x0004CB04
		private string UnquotedName(string name)
		{
			if (string.IsNullOrEmpty(name))
			{
				return null;
			}
			if (name[0] == '[')
			{
				int length = name.Length;
				name = name.Substring(1, length - 2);
			}
			return name;
		}

		// Token: 0x06000EF9 RID: 3833 RVA: 0x0004E93C File Offset: 0x0004CB3C
		private object ValidateBulkCopyVariant(object value)
		{
			byte tdstype = MetaType.GetMetaTypeFromValue(value, true, true).TDSType;
			if (tdstype <= 108)
			{
				if (tdstype <= 43)
				{
					if (tdstype != 36 && tdstype - 40 > 3)
					{
						goto IL_AD;
					}
				}
				else
				{
					switch (tdstype)
					{
					case 48:
					case 50:
					case 52:
					case 56:
					case 59:
					case 60:
					case 61:
					case 62:
						break;
					case 49:
					case 51:
					case 53:
					case 54:
					case 55:
					case 57:
					case 58:
						goto IL_AD;
					default:
						if (tdstype != 108)
						{
							goto IL_AD;
						}
						break;
					}
				}
			}
			else if (tdstype <= 165)
			{
				if (tdstype != 127 && tdstype != 165)
				{
					goto IL_AD;
				}
			}
			else if (tdstype != 167 && tdstype != 231)
			{
				goto IL_AD;
			}
			if (value is INullable)
			{
				return MetaType.GetComValueFromSqlVariant(value);
			}
			return value;
			IL_AD:
			throw SQL.BulkLoadInvalidVariantValue();
		}

		// Token: 0x06000EFA RID: 3834 RVA: 0x0004E9FC File Offset: 0x0004CBFC
		private object ConvertValue(object value, _SqlMetaData metadata, bool isNull, ref bool isSqlType, out bool coercedToDataFeed)
		{
			coercedToDataFeed = false;
			if (!isNull)
			{
				MetaType metaType = metadata.metaType;
				bool flag = false;
				object result;
				try
				{
					byte nullableType = metaType.NullableType;
					MetaType metaTypeFromSqlDbType;
					if (nullableType <= 165)
					{
						if (nullableType <= 59)
						{
							switch (nullableType)
							{
							case 34:
							case 35:
							case 36:
							case 38:
							case 40:
							case 41:
							case 42:
							case 43:
							case 50:
								break;
							case 37:
							case 39:
							case 44:
							case 45:
							case 46:
							case 47:
							case 48:
							case 49:
								goto IL_2A3;
							default:
								if (nullableType - 58 > 1)
								{
									goto IL_2A3;
								}
								break;
							}
						}
						else if (nullableType - 61 > 1)
						{
							switch (nullableType)
							{
							case 98:
								value = this.ValidateBulkCopyVariant(value);
								flag = true;
								goto IL_2B6;
							case 99:
								goto IL_212;
							case 100:
							case 101:
							case 102:
							case 103:
							case 105:
							case 107:
								goto IL_2A3;
							case 104:
							case 109:
							case 110:
							case 111:
								break;
							case 106:
							case 108:
							{
								metaTypeFromSqlDbType = MetaType.GetMetaTypeFromSqlDbType(metaType.SqlDbType, false);
								value = SqlParameter.CoerceValue(value, metaTypeFromSqlDbType, out coercedToDataFeed, out flag, false);
								SqlDecimal sqlDecimal;
								if (isSqlType && !flag)
								{
									sqlDecimal = (SqlDecimal)value;
								}
								else
								{
									sqlDecimal = new SqlDecimal((decimal)value);
								}
								if (sqlDecimal.Scale != metadata.scale)
								{
									sqlDecimal = TdsParser.AdjustSqlDecimalScale(sqlDecimal, (int)metadata.scale);
								}
								if (sqlDecimal.Precision > metadata.precision)
								{
									try
									{
										sqlDecimal = SqlDecimal.ConvertToPrecScale(sqlDecimal, (int)metadata.precision, (int)sqlDecimal.Scale);
									}
									catch (SqlTruncateException)
									{
										throw SQL.BulkLoadCannotConvertValue(value.GetType(), metaTypeFromSqlDbType, ADP.ParameterValueOutOfRange(sqlDecimal));
									}
								}
								value = sqlDecimal;
								isSqlType = true;
								flag = false;
								goto IL_2B6;
							}
							default:
								if (nullableType != 165)
								{
									goto IL_2A3;
								}
								break;
							}
						}
					}
					else if (nullableType <= 173)
					{
						if (nullableType != 167 && nullableType != 173)
						{
							goto IL_2A3;
						}
					}
					else if (nullableType != 175)
					{
						if (nullableType == 231)
						{
							goto IL_212;
						}
						switch (nullableType)
						{
						case 239:
							goto IL_212;
						case 240:
							throw ADP.DbTypeNotSupported("UDT");
						case 241:
							if (value is XmlReader)
							{
								value = new XmlDataFeed((XmlReader)value);
								flag = true;
								coercedToDataFeed = true;
								goto IL_2B6;
							}
							goto IL_2B6;
						default:
							goto IL_2A3;
						}
					}
					metaTypeFromSqlDbType = MetaType.GetMetaTypeFromSqlDbType(metaType.SqlDbType, false);
					value = SqlParameter.CoerceValue(value, metaTypeFromSqlDbType, out coercedToDataFeed, out flag, false);
					goto IL_2B6;
					IL_212:
					metaTypeFromSqlDbType = MetaType.GetMetaTypeFromSqlDbType(metaType.SqlDbType, false);
					value = SqlParameter.CoerceValue(value, metaTypeFromSqlDbType, out coercedToDataFeed, out flag, false);
					if (!coercedToDataFeed && ((isSqlType && !flag) ? ((SqlString)value).Value.Length : ((string)value).Length) > metadata.length / 2)
					{
						throw SQL.BulkLoadStringTooLong();
					}
					goto IL_2B6;
					IL_2A3:
					throw SQL.BulkLoadCannotConvertValue(value.GetType(), metadata.metaType, null);
					IL_2B6:
					if (flag)
					{
						isSqlType = false;
					}
					result = value;
				}
				catch (Exception e)
				{
					if (!ADP.IsCatchableExceptionType(e))
					{
						throw;
					}
					throw SQL.BulkLoadCannotConvertValue(value.GetType(), metadata.metaType, e);
				}
				return result;
			}
			if (!metadata.isNullable)
			{
				throw SQL.BulkLoadBulkLoadNotAllowDBNull(metadata.column);
			}
			return value;
		}

		/// <summary>Copies all rows from the supplied <see cref="T:System.Data.Common.DbDataReader" /> array to a destination table specified by the <see cref="P:System.Data.SqlClient.SqlBulkCopy.DestinationTableName" /> property of the <see cref="T:System.Data.SqlClient.SqlBulkCopy" /> object.</summary>
		/// <param name="reader">A <see cref="T:System.Data.Common.DbDataReader" /> whose rows will be copied to the destination table.</param>
		// Token: 0x06000EFB RID: 3835 RVA: 0x0004ED24 File Offset: 0x0004CF24
		public void WriteToServer(DbDataReader reader)
		{
			if (reader == null)
			{
				throw new ArgumentNullException("reader");
			}
			if (this._isBulkCopyingInProgress)
			{
				throw SQL.BulkLoadPendingOperation();
			}
			SqlStatistics statistics = this.Statistics;
			try
			{
				statistics = SqlStatistics.StartTimer(this.Statistics);
				this._rowSource = reader;
				this._DbDataReaderRowSource = reader;
				this._SqlDataReaderRowSource = (reader as SqlDataReader);
				this._dataTableSource = null;
				this._rowSourceType = SqlBulkCopy.ValueSourceType.DbDataReader;
				this._isAsyncBulkCopy = false;
				this.WriteRowSourceToServerAsync(reader.FieldCount, CancellationToken.None);
			}
			finally
			{
				SqlStatistics.StopTimer(statistics);
			}
		}

		/// <summary>Copies all rows in the supplied <see cref="T:System.Data.IDataReader" /> to a destination table specified by the <see cref="P:System.Data.SqlClient.SqlBulkCopy.DestinationTableName" /> property of the <see cref="T:System.Data.SqlClient.SqlBulkCopy" /> object.</summary>
		/// <param name="reader">A <see cref="T:System.Data.IDataReader" /> whose rows will be copied to the destination table.</param>
		// Token: 0x06000EFC RID: 3836 RVA: 0x0004EDBC File Offset: 0x0004CFBC
		public void WriteToServer(IDataReader reader)
		{
			if (reader == null)
			{
				throw new ArgumentNullException("reader");
			}
			if (this._isBulkCopyingInProgress)
			{
				throw SQL.BulkLoadPendingOperation();
			}
			SqlStatistics statistics = this.Statistics;
			try
			{
				statistics = SqlStatistics.StartTimer(this.Statistics);
				this._rowSource = reader;
				this._SqlDataReaderRowSource = (this._rowSource as SqlDataReader);
				this._DbDataReaderRowSource = (this._rowSource as DbDataReader);
				this._dataTableSource = null;
				this._rowSourceType = SqlBulkCopy.ValueSourceType.IDataReader;
				this._isAsyncBulkCopy = false;
				this.WriteRowSourceToServerAsync(reader.FieldCount, CancellationToken.None);
			}
			finally
			{
				SqlStatistics.StopTimer(statistics);
			}
		}

		/// <summary>Copies all rows in the supplied <see cref="T:System.Data.DataTable" /> to a destination table specified by the <see cref="P:System.Data.SqlClient.SqlBulkCopy.DestinationTableName" /> property of the <see cref="T:System.Data.SqlClient.SqlBulkCopy" /> object.</summary>
		/// <param name="table">A <see cref="T:System.Data.DataTable" /> whose rows will be copied to the destination table.</param>
		// Token: 0x06000EFD RID: 3837 RVA: 0x0004EE64 File Offset: 0x0004D064
		public void WriteToServer(DataTable table)
		{
			this.WriteToServer(table, (DataRowState)0);
		}

		/// <summary>Copies only rows that match the supplied row state in the supplied <see cref="T:System.Data.DataTable" /> to a destination table specified by the <see cref="P:System.Data.SqlClient.SqlBulkCopy.DestinationTableName" /> property of the <see cref="T:System.Data.SqlClient.SqlBulkCopy" /> object.</summary>
		/// <param name="table">A <see cref="T:System.Data.DataTable" /> whose rows will be copied to the destination table.</param>
		/// <param name="rowState">A value from the <see cref="T:System.Data.DataRowState" /> enumeration. Only rows matching the row state are copied to the destination.</param>
		// Token: 0x06000EFE RID: 3838 RVA: 0x0004EE70 File Offset: 0x0004D070
		public void WriteToServer(DataTable table, DataRowState rowState)
		{
			if (table == null)
			{
				throw new ArgumentNullException("table");
			}
			if (this._isBulkCopyingInProgress)
			{
				throw SQL.BulkLoadPendingOperation();
			}
			SqlStatistics statistics = this.Statistics;
			try
			{
				statistics = SqlStatistics.StartTimer(this.Statistics);
				this._rowStateToSkip = ((rowState == (DataRowState)0 || rowState == DataRowState.Deleted) ? DataRowState.Deleted : (~rowState | DataRowState.Deleted));
				this._rowSource = table;
				this._dataTableSource = table;
				this._SqlDataReaderRowSource = null;
				this._rowSourceType = SqlBulkCopy.ValueSourceType.DataTable;
				this._rowEnumerator = table.Rows.GetEnumerator();
				this._isAsyncBulkCopy = false;
				this.WriteRowSourceToServerAsync(table.Columns.Count, CancellationToken.None);
			}
			finally
			{
				SqlStatistics.StopTimer(statistics);
			}
		}

		/// <summary>Copies all rows from the supplied <see cref="T:System.Data.DataRow" /> array to a destination table specified by the <see cref="P:System.Data.SqlClient.SqlBulkCopy.DestinationTableName" /> property of the <see cref="T:System.Data.SqlClient.SqlBulkCopy" /> object.</summary>
		/// <param name="rows">An array of <see cref="T:System.Data.DataRow" /> objects that will be copied to the destination table.</param>
		// Token: 0x06000EFF RID: 3839 RVA: 0x0004EF24 File Offset: 0x0004D124
		public void WriteToServer(DataRow[] rows)
		{
			SqlStatistics statistics = this.Statistics;
			if (rows == null)
			{
				throw new ArgumentNullException("rows");
			}
			if (this._isBulkCopyingInProgress)
			{
				throw SQL.BulkLoadPendingOperation();
			}
			if (rows.Length == 0)
			{
				return;
			}
			try
			{
				statistics = SqlStatistics.StartTimer(this.Statistics);
				DataTable table = rows[0].Table;
				this._rowStateToSkip = DataRowState.Deleted;
				this._rowSource = rows;
				this._dataTableSource = table;
				this._SqlDataReaderRowSource = null;
				this._rowSourceType = SqlBulkCopy.ValueSourceType.RowArray;
				this._rowEnumerator = rows.GetEnumerator();
				this._isAsyncBulkCopy = false;
				this.WriteRowSourceToServerAsync(table.Columns.Count, CancellationToken.None);
			}
			finally
			{
				SqlStatistics.StopTimer(statistics);
			}
		}

		/// <summary>The asynchronous version of <see cref="M:System.Data.SqlClient.SqlBulkCopy.WriteToServer(System.Data.DataRow[])" />, which copies all rows from the supplied <see cref="T:System.Data.DataRow" /> array to a destination table specified by the <see cref="P:System.Data.SqlClient.SqlBulkCopy.DestinationTableName" /> property of the <see cref="T:System.Data.SqlClient.SqlBulkCopy" /> object.</summary>
		/// <param name="rows">An array of <see cref="T:System.Data.DataRow" /> objects that will be copied to the destination table.</param>
		/// <returns>A task representing the asynchronous operation.</returns>
		/// <exception cref="T:System.InvalidOperationException">Calling <see cref="M:System.Data.SqlClient.SqlBulkCopy.WriteToServerAsync(System.Data.DataRow[])" /> multiple times for the same instance before task completion.Calling <see cref="M:System.Data.SqlClient.SqlBulkCopy.WriteToServerAsync(System.Data.DataRow[])" /> and <see cref="M:System.Data.SqlClient.SqlBulkCopy.WriteToServer(System.Data.DataRow[])" /> for the same instance before task completion.The connection drops or is closed during <see cref="M:System.Data.SqlClient.SqlBulkCopy.WriteToServerAsync(System.Data.DataRow[])" /> execution.Returned in the task object, the <see cref="T:System.Data.SqlClient.SqlBulkCopy" /> object was closed during the method execution.Returned in the task object, there was a connection pool timeout.Returned in the task object, the <see cref="T:System.Data.SqlClient.SqlConnection" /> object is closed before method execution.
		///         <see langword="Context Connection=true" /> is specified in the connection string.</exception>
		/// <exception cref="T:System.Data.SqlClient.SqlException">Returned in the task object, any error returned by SQL Server that occurred while opening the connection.</exception>
		// Token: 0x06000F00 RID: 3840 RVA: 0x0004EFD8 File Offset: 0x0004D1D8
		public Task WriteToServerAsync(DataRow[] rows)
		{
			return this.WriteToServerAsync(rows, CancellationToken.None);
		}

		/// <summary>The asynchronous version of <see cref="M:System.Data.SqlClient.SqlBulkCopy.WriteToServer(System.Data.DataRow[])" />, which copies all rows from the supplied <see cref="T:System.Data.DataRow" /> array to a destination table specified by the <see cref="P:System.Data.SqlClient.SqlBulkCopy.DestinationTableName" /> property of the <see cref="T:System.Data.SqlClient.SqlBulkCopy" /> object.The cancellation token can be used to request that the operation be abandoned before the command timeout elapses.  Exceptions will be reported via the returned Task object.</summary>
		/// <param name="rows">An array of <see cref="T:System.Data.DataRow" /> objects that will be copied to the destination table.</param>
		/// <param name="cancellationToken">The cancellation instruction. A <see cref="P:System.Threading.CancellationToken.None" /> value in this parameter makes this method equivalent to <see cref="M:System.Data.SqlClient.SqlBulkCopy.WriteToServerAsync(System.Data.DataTable)" />.</param>
		/// <returns>A task representing the asynchronous operation.</returns>
		/// <exception cref="T:System.InvalidOperationException">Calling <see cref="M:System.Data.SqlClient.SqlBulkCopy.WriteToServerAsync(System.Data.DataRow[])" /> multiple times for the same instance before task completion.Calling <see cref="M:System.Data.SqlClient.SqlBulkCopy.WriteToServerAsync(System.Data.DataRow[])" /> and <see cref="M:System.Data.SqlClient.SqlBulkCopy.WriteToServer(System.Data.DataRow[])" /> for the same instance before task completion.The connection drops or is closed during <see cref="M:System.Data.SqlClient.SqlBulkCopy.WriteToServerAsync(System.Data.DataRow[])" /> execution.Returned in the task object, the <see cref="T:System.Data.SqlClient.SqlBulkCopy" /> object was closed during the method execution.Returned in the task object, there was a connection pool timeout.Returned in the task object, the <see cref="T:System.Data.SqlClient.SqlConnection" /> object is closed before method execution.
		///         <see langword="Context Connection=true" /> is specified in the connection string.</exception>
		/// <exception cref="T:System.Data.SqlClient.SqlException">Returned in the task object, any error returned by SQL Server that occurred while opening the connection.</exception>
		// Token: 0x06000F01 RID: 3841 RVA: 0x0004EFE8 File Offset: 0x0004D1E8
		public Task WriteToServerAsync(DataRow[] rows, CancellationToken cancellationToken)
		{
			Task result = null;
			if (rows == null)
			{
				throw new ArgumentNullException("rows");
			}
			if (this._isBulkCopyingInProgress)
			{
				throw SQL.BulkLoadPendingOperation();
			}
			SqlStatistics statistics = this.Statistics;
			try
			{
				statistics = SqlStatistics.StartTimer(this.Statistics);
				if (rows.Length == 0)
				{
					return cancellationToken.IsCancellationRequested ? Task.FromCanceled(cancellationToken) : Task.CompletedTask;
				}
				DataTable table = rows[0].Table;
				this._rowStateToSkip = DataRowState.Deleted;
				this._rowSource = rows;
				this._dataTableSource = table;
				this._SqlDataReaderRowSource = null;
				this._rowSourceType = SqlBulkCopy.ValueSourceType.RowArray;
				this._rowEnumerator = rows.GetEnumerator();
				this._isAsyncBulkCopy = true;
				result = this.WriteRowSourceToServerAsync(table.Columns.Count, cancellationToken);
			}
			finally
			{
				SqlStatistics.StopTimer(statistics);
			}
			return result;
		}

		/// <summary>The asynchronous version of <see cref="M:System.Data.SqlClient.SqlBulkCopy.WriteToServer(System.Data.Common.DbDataReader)" />, which copies all rows from the supplied <see cref="T:System.Data.Common.DbDataReader" /> array to a destination table specified by the <see cref="P:System.Data.SqlClient.SqlBulkCopy.DestinationTableName" /> property of the <see cref="T:System.Data.SqlClient.SqlBulkCopy" /> object.</summary>
		/// <param name="reader">A <see cref="T:System.Data.Common.DbDataReader" /> whose rows will be copied to the destination table.</param>
		/// <returns>A task representing the asynchronous operation.</returns>
		// Token: 0x06000F02 RID: 3842 RVA: 0x0004F0B4 File Offset: 0x0004D2B4
		public Task WriteToServerAsync(DbDataReader reader)
		{
			return this.WriteToServerAsync(reader, CancellationToken.None);
		}

		/// <summary>The asynchronous version of <see cref="M:System.Data.SqlClient.SqlBulkCopy.WriteToServer(System.Data.Common.DbDataReader)" />, which copies all rows from the supplied <see cref="T:System.Data.Common.DbDataReader" /> array to a destination table specified by the <see cref="P:System.Data.SqlClient.SqlBulkCopy.DestinationTableName" /> property of the <see cref="T:System.Data.SqlClient.SqlBulkCopy" /> object.</summary>
		/// <param name="reader">A <see cref="T:System.Data.Common.DbDataReader" /> whose rows will be copied to the destination table.</param>
		/// <param name="cancellationToken">The cancellation instruction. A <see cref="P:System.Threading.CancellationToken.None" /> value in this parameter makes this method equivalent to <see cref="M:System.Data.SqlClient.SqlBulkCopy.WriteToServerAsync(System.Data.Common.DbDataReader)" />.</param>
		/// <returns>Returns <see cref="T:System.Threading.Tasks.Task" />.</returns>
		// Token: 0x06000F03 RID: 3843 RVA: 0x0004F0C4 File Offset: 0x0004D2C4
		public Task WriteToServerAsync(DbDataReader reader, CancellationToken cancellationToken)
		{
			Task result = null;
			if (reader == null)
			{
				throw new ArgumentNullException("reader");
			}
			if (this._isBulkCopyingInProgress)
			{
				throw SQL.BulkLoadPendingOperation();
			}
			SqlStatistics statistics = this.Statistics;
			try
			{
				statistics = SqlStatistics.StartTimer(this.Statistics);
				this._rowSource = reader;
				this._SqlDataReaderRowSource = (reader as SqlDataReader);
				this._DbDataReaderRowSource = reader;
				this._dataTableSource = null;
				this._rowSourceType = SqlBulkCopy.ValueSourceType.DbDataReader;
				this._isAsyncBulkCopy = true;
				result = this.WriteRowSourceToServerAsync(reader.FieldCount, cancellationToken);
			}
			finally
			{
				SqlStatistics.StopTimer(statistics);
			}
			return result;
		}

		/// <summary>The asynchronous version of <see cref="M:System.Data.SqlClient.SqlBulkCopy.WriteToServer(System.Data.IDataReader)" />, which copies all rows in the supplied <see cref="T:System.Data.IDataReader" /> to a destination table specified by the <see cref="P:System.Data.SqlClient.SqlBulkCopy.DestinationTableName" /> property of the <see cref="T:System.Data.SqlClient.SqlBulkCopy" /> object.</summary>
		/// <param name="reader">A <see cref="T:System.Data.IDataReader" /> whose rows will be copied to the destination table.</param>
		/// <returns>A task representing the asynchronous operation.</returns>
		/// <exception cref="T:System.InvalidOperationException">Calling <see cref="M:System.Data.SqlClient.SqlBulkCopy.WriteToServerAsync(System.Data.IDataReader)" /> multiple times for the same instance before task completion.Calling <see cref="M:System.Data.SqlClient.SqlBulkCopy.WriteToServerAsync(System.Data.IDataReader)" /> and <see cref="M:System.Data.SqlClient.SqlBulkCopy.WriteToServer(System.Data.IDataReader)" /> for the same instance before task completion.The connection drops or is closed during <see cref="M:System.Data.SqlClient.SqlBulkCopy.WriteToServerAsync(System.Data.IDataReader)" /> execution.Returned in the task object, the <see cref="T:System.Data.SqlClient.SqlBulkCopy" /> object was closed during the method execution.Returned in the task object, there was a connection pool timeout.Returned in the task object, the <see cref="T:System.Data.SqlClient.SqlConnection" /> object is closed before method execution.The <see cref="T:System.Data.IDataReader" /> was closed before the completed <see cref="T:System.Threading.Tasks.Task" /> returned.The <see cref="T:System.Data.IDataReader" />'s associated connection was closed before the completed <see cref="T:System.Threading.Tasks.Task" /> returned.
		///         <see langword="Context Connection=true" /> is specified in the connection string.</exception>
		/// <exception cref="T:System.Data.SqlClient.SqlException">Returned in the task object, any error returned by SQL Server that occurred while opening the connection.</exception>
		// Token: 0x06000F04 RID: 3844 RVA: 0x0004F15C File Offset: 0x0004D35C
		public Task WriteToServerAsync(IDataReader reader)
		{
			return this.WriteToServerAsync(reader, CancellationToken.None);
		}

		/// <summary>The asynchronous version of <see cref="M:System.Data.SqlClient.SqlBulkCopy.WriteToServer(System.Data.IDataReader)" />, which copies all rows in the supplied <see cref="T:System.Data.IDataReader" /> to a destination table specified by the <see cref="P:System.Data.SqlClient.SqlBulkCopy.DestinationTableName" /> property of the <see cref="T:System.Data.SqlClient.SqlBulkCopy" /> object.The cancellation token can be used to request that the operation be abandoned before the command timeout elapses.  Exceptions will be reported via the returned Task object.</summary>
		/// <param name="reader">A <see cref="T:System.Data.IDataReader" /> whose rows will be copied to the destination table.</param>
		/// <param name="cancellationToken">The cancellation instruction. A <see cref="P:System.Threading.CancellationToken.None" /> value in this parameter makes this method equivalent to <see cref="M:System.Data.SqlClient.SqlBulkCopy.WriteToServerAsync(System.Data.DataTable)" />.</param>
		/// <returns>A task representing the asynchronous operation.</returns>
		/// <exception cref="T:System.InvalidOperationException">Calling <see cref="M:System.Data.SqlClient.SqlBulkCopy.WriteToServerAsync(System.Data.IDataReader)" /> multiple times for the same instance before task completion.Calling <see cref="M:System.Data.SqlClient.SqlBulkCopy.WriteToServerAsync(System.Data.IDataReader)" /> and <see cref="M:System.Data.SqlClient.SqlBulkCopy.WriteToServer(System.Data.IDataReader)" /> for the same instance before task completion.The connection drops or is closed during <see cref="M:System.Data.SqlClient.SqlBulkCopy.WriteToServerAsync(System.Data.IDataReader)" /> execution.Returned in the task object, the <see cref="T:System.Data.SqlClient.SqlBulkCopy" /> object was closed during the method execution.Returned in the task object, there was a connection pool timeout.Returned in the task object, the <see cref="T:System.Data.SqlClient.SqlConnection" /> object is closed before method execution.The <see cref="T:System.Data.IDataReader" /> was closed before the completed <see cref="T:System.Threading.Tasks.Task" /> returned.The <see cref="T:System.Data.IDataReader" />'s associated connection was closed before the completed <see cref="T:System.Threading.Tasks.Task" /> returned.
		///         <see langword="Context Connection=true" /> is specified in the connection string.</exception>
		/// <exception cref="T:System.Data.SqlClient.SqlException">Returned in the task object, any error returned by SQL Server that occurred while opening the connection.</exception>
		// Token: 0x06000F05 RID: 3845 RVA: 0x0004F16C File Offset: 0x0004D36C
		public Task WriteToServerAsync(IDataReader reader, CancellationToken cancellationToken)
		{
			Task result = null;
			if (reader == null)
			{
				throw new ArgumentNullException("reader");
			}
			if (this._isBulkCopyingInProgress)
			{
				throw SQL.BulkLoadPendingOperation();
			}
			SqlStatistics statistics = this.Statistics;
			try
			{
				statistics = SqlStatistics.StartTimer(this.Statistics);
				this._rowSource = reader;
				this._SqlDataReaderRowSource = (this._rowSource as SqlDataReader);
				this._DbDataReaderRowSource = (this._rowSource as DbDataReader);
				this._dataTableSource = null;
				this._rowSourceType = SqlBulkCopy.ValueSourceType.IDataReader;
				this._isAsyncBulkCopy = true;
				result = this.WriteRowSourceToServerAsync(reader.FieldCount, cancellationToken);
			}
			finally
			{
				SqlStatistics.StopTimer(statistics);
			}
			return result;
		}

		/// <summary>The asynchronous version of <see cref="M:System.Data.SqlClient.SqlBulkCopy.WriteToServer(System.Data.DataTable)" />, which copies all rows in the supplied <see cref="T:System.Data.DataTable" /> to a destination table specified by the <see cref="P:System.Data.SqlClient.SqlBulkCopy.DestinationTableName" /> property of the <see cref="T:System.Data.SqlClient.SqlBulkCopy" /> object.</summary>
		/// <param name="table">A <see cref="T:System.Data.DataTable" /> whose rows will be copied to the destination table.</param>
		/// <returns>A task representing the asynchronous operation.</returns>
		/// <exception cref="T:System.InvalidOperationException">Calling <see cref="M:System.Data.SqlClient.SqlBulkCopy.WriteToServerAsync(System.Data.DataTable)" /> multiple times for the same instance before task completion.Calling <see cref="M:System.Data.SqlClient.SqlBulkCopy.WriteToServerAsync(System.Data.DataTable)" /> and <see cref="M:System.Data.SqlClient.SqlBulkCopy.WriteToServer(System.Data.DataTable)" /> for the same instance before task completion.The connection drops or is closed during <see cref="M:System.Data.SqlClient.SqlBulkCopy.WriteToServerAsync(System.Data.DataTable)" /> execution.Returned in the task object, the <see cref="T:System.Data.SqlClient.SqlBulkCopy" /> object was closed during the method execution.Returned in the task object, there was a connection pool timeout.Returned in the task object, the <see cref="T:System.Data.SqlClient.SqlConnection" /> object is closed before method execution.
		///         <see langword="Context Connection=true" /> is specified in the connection string.</exception>
		/// <exception cref="T:System.Data.SqlClient.SqlException">Returned in the task object, any error returned by SQL Server that occurred while opening the connection.</exception>
		// Token: 0x06000F06 RID: 3846 RVA: 0x0004F210 File Offset: 0x0004D410
		public Task WriteToServerAsync(DataTable table)
		{
			return this.WriteToServerAsync(table, (DataRowState)0, CancellationToken.None);
		}

		/// <summary>The asynchronous version of <see cref="M:System.Data.SqlClient.SqlBulkCopy.WriteToServer(System.Data.DataTable)" />, which copies all rows in the supplied <see cref="T:System.Data.DataTable" /> to a destination table specified by the <see cref="P:System.Data.SqlClient.SqlBulkCopy.DestinationTableName" /> property of the <see cref="T:System.Data.SqlClient.SqlBulkCopy" /> object.The cancellation token can be used to request that the operation be abandoned before the command timeout elapses.  Exceptions will be reported via the returned Task object.</summary>
		/// <param name="table">A <see cref="T:System.Data.DataTable" /> whose rows will be copied to the destination table.</param>
		/// <param name="cancellationToken">The cancellation instruction. A <see cref="P:System.Threading.CancellationToken.None" /> value in this parameter makes this method equivalent to <see cref="M:System.Data.SqlClient.SqlBulkCopy.WriteToServerAsync(System.Data.DataTable)" />.</param>
		/// <returns>A task representing the asynchronous operation.</returns>
		/// <exception cref="T:System.InvalidOperationException">Calling <see cref="M:System.Data.SqlClient.SqlBulkCopy.WriteToServerAsync(System.Data.DataTable)" /> multiple times for the same instance before task completion.Calling <see cref="M:System.Data.SqlClient.SqlBulkCopy.WriteToServerAsync(System.Data.DataTable)" /> and <see cref="M:System.Data.SqlClient.SqlBulkCopy.WriteToServer(System.Data.DataTable)" /> for the same instance before task completion.The connection drops or is closed during <see cref="M:System.Data.SqlClient.SqlBulkCopy.WriteToServerAsync(System.Data.DataTable)" /> execution.Returned in the task object, the <see cref="T:System.Data.SqlClient.SqlBulkCopy" /> object was closed during the method execution.Returned in the task object, there was a connection pool timeout.Returned in the task object, the <see cref="T:System.Data.SqlClient.SqlConnection" /> object is closed before method execution.
		///         <see langword="Context Connection=true" /> is specified in the connection string.</exception>
		/// <exception cref="T:System.Data.SqlClient.SqlException">Returned in the task object, any error returned by SQL Server that occurred while opening the connection.</exception>
		// Token: 0x06000F07 RID: 3847 RVA: 0x0004F21F File Offset: 0x0004D41F
		public Task WriteToServerAsync(DataTable table, CancellationToken cancellationToken)
		{
			return this.WriteToServerAsync(table, (DataRowState)0, cancellationToken);
		}

		/// <summary>The asynchronous version of <see cref="M:System.Data.SqlClient.SqlBulkCopy.WriteToServer(System.Data.DataTable,System.Data.DataRowState)" />, which copies only rows that match the supplied row state in the supplied <see cref="T:System.Data.DataTable" /> to a destination table specified by the <see cref="P:System.Data.SqlClient.SqlBulkCopy.DestinationTableName" /> property of the <see cref="T:System.Data.SqlClient.SqlBulkCopy" /> object.</summary>
		/// <param name="table">A <see cref="T:System.Data.DataTable" /> whose rows will be copied to the destination table.</param>
		/// <param name="rowState">A value from the <see cref="T:System.Data.DataRowState" /> enumeration. Only rows matching the row state are copied to the destination.</param>
		/// <returns>A task representing the asynchronous operation.</returns>
		/// <exception cref="T:System.InvalidOperationException">Calling <see cref="M:System.Data.SqlClient.SqlBulkCopy.WriteToServerAsync(System.Data.DataTable,System.Data.DataRowState)" /> multiple times for the same instance before task completion.Calling <see cref="M:System.Data.SqlClient.SqlBulkCopy.WriteToServerAsync(System.Data.DataTable,System.Data.DataRowState)" /> and <see cref="M:System.Data.SqlClient.SqlBulkCopy.WriteToServer(System.Data.DataTable,System.Data.DataRowState)" /> for the same instance before task completion.The connection drops or is closed during <see cref="M:System.Data.SqlClient.SqlBulkCopy.WriteToServerAsync(System.Data.DataTable,System.Data.DataRowState)" /> execution.Returned in the task object, the <see cref="T:System.Data.SqlClient.SqlBulkCopy" /> object was closed during the method execution.Returned in the task object, there was a connection pool timeout.Returned in the task object, the <see cref="T:System.Data.SqlClient.SqlConnection" /> object is closed before method execution.
		///         <see langword="Context Connection=true" /> is specified in the connection string.</exception>
		/// <exception cref="T:System.Data.SqlClient.SqlException">Returned in the task object, any error returned by SQL Server that occurred while opening the connection.</exception>
		// Token: 0x06000F08 RID: 3848 RVA: 0x0004F22A File Offset: 0x0004D42A
		public Task WriteToServerAsync(DataTable table, DataRowState rowState)
		{
			return this.WriteToServerAsync(table, rowState, CancellationToken.None);
		}

		/// <summary>The asynchronous version of <see cref="M:System.Data.SqlClient.SqlBulkCopy.WriteToServer(System.Data.DataTable,System.Data.DataRowState)" />, which copies only rows that match the supplied row state in the supplied <see cref="T:System.Data.DataTable" /> to a destination table specified by the <see cref="P:System.Data.SqlClient.SqlBulkCopy.DestinationTableName" /> property of the <see cref="T:System.Data.SqlClient.SqlBulkCopy" /> object.The cancellation token can be used to request that the operation be abandoned before the command timeout elapses.  Exceptions will be reported via the returned Task object.</summary>
		/// <param name="table">A <see cref="T:System.Data.DataTable" /> whose rows will be copied to the destination table.</param>
		/// <param name="rowState">A value from the <see cref="T:System.Data.DataRowState" /> enumeration. Only rows matching the row state are copied to the destination.</param>
		/// <param name="cancellationToken">The cancellation instruction. A <see cref="P:System.Threading.CancellationToken.None" /> value in this parameter makes this method equivalent to <see cref="M:System.Data.SqlClient.SqlBulkCopy.WriteToServerAsync(System.Data.DataTable)" />.</param>
		/// <returns>A task representing the asynchronous operation.</returns>
		/// <exception cref="T:System.InvalidOperationException">Calling <see cref="M:System.Data.SqlClient.SqlBulkCopy.WriteToServerAsync(System.Data.DataTable,System.Data.DataRowState)" /> multiple times for the same instance before task completion.Calling <see cref="M:System.Data.SqlClient.SqlBulkCopy.WriteToServerAsync(System.Data.DataTable,System.Data.DataRowState)" /> and <see cref="M:System.Data.SqlClient.SqlBulkCopy.WriteToServer(System.Data.DataTable,System.Data.DataRowState)" /> for the same instance before task completion.The connection drops or is closed during <see cref="M:System.Data.SqlClient.SqlBulkCopy.WriteToServerAsync(System.Data.DataTable,System.Data.DataRowState)" /> execution.Returned in the task object, the <see cref="T:System.Data.SqlClient.SqlBulkCopy" /> object was closed during the method execution.Returned in the task object, there was a connection pool timeout.Returned in the task object, the <see cref="T:System.Data.SqlClient.SqlConnection" /> object is closed before method execution.
		///         <see langword="Context Connection=true" /> is specified in the connection string.</exception>
		/// <exception cref="T:System.Data.SqlClient.SqlException">Returned in the task object, any error returned by SQL Server that occurred while opening the connection.</exception>
		// Token: 0x06000F09 RID: 3849 RVA: 0x0004F23C File Offset: 0x0004D43C
		public Task WriteToServerAsync(DataTable table, DataRowState rowState, CancellationToken cancellationToken)
		{
			Task result = null;
			if (table == null)
			{
				throw new ArgumentNullException("table");
			}
			if (this._isBulkCopyingInProgress)
			{
				throw SQL.BulkLoadPendingOperation();
			}
			SqlStatistics statistics = this.Statistics;
			try
			{
				statistics = SqlStatistics.StartTimer(this.Statistics);
				this._rowStateToSkip = ((rowState == (DataRowState)0 || rowState == DataRowState.Deleted) ? DataRowState.Deleted : (~rowState | DataRowState.Deleted));
				this._rowSource = table;
				this._SqlDataReaderRowSource = null;
				this._dataTableSource = table;
				this._rowSourceType = SqlBulkCopy.ValueSourceType.DataTable;
				this._rowEnumerator = table.Rows.GetEnumerator();
				this._isAsyncBulkCopy = true;
				result = this.WriteRowSourceToServerAsync(table.Columns.Count, cancellationToken);
			}
			finally
			{
				SqlStatistics.StopTimer(statistics);
			}
			return result;
		}

		// Token: 0x06000F0A RID: 3850 RVA: 0x0004F2F0 File Offset: 0x0004D4F0
		private Task WriteRowSourceToServerAsync(int columnCount, CancellationToken ctoken)
		{
			Task currentReconnectionTask = this._connection._currentReconnectionTask;
			if (currentReconnectionTask != null && !currentReconnectionTask.IsCompleted)
			{
				if (this._isAsyncBulkCopy)
				{
					TaskCompletionSource<object> tcs = new TaskCompletionSource<object>();
					Action <>9__2;
					currentReconnectionTask.ContinueWith(delegate(Task t)
					{
						Task task2 = this.WriteRowSourceToServerAsync(columnCount, ctoken);
						TaskCompletionSource<object> tcs;
						if (task2 == null)
						{
							tcs.SetResult(null);
							return;
						}
						Task task3 = task2;
						tcs = tcs;
						Action onSuccess;
						if ((onSuccess = <>9__2) == null)
						{
							onSuccess = (<>9__2 = delegate()
							{
								tcs.SetResult(null);
							});
						}
						AsyncHelper.ContinueTask(task3, tcs, onSuccess, null, null, null, null, null);
					}, ctoken);
					return tcs.Task;
				}
				AsyncHelper.WaitForCompletion(currentReconnectionTask, this.BulkCopyTimeout, delegate
				{
					throw SQL.CR_ReconnectTimeout();
				}, false);
			}
			bool flag = true;
			this._isBulkCopyingInProgress = true;
			this.CreateOrValidateConnection("WriteToServer");
			SqlInternalConnectionTds openTdsConnection = this._connection.GetOpenTdsConnection();
			this._parserLock = openTdsConnection._parserLock;
			this._parserLock.Wait(this._isAsyncBulkCopy);
			Task result;
			try
			{
				this.WriteRowSourceToServerCommon(columnCount);
				Task task = this.WriteToServerInternalAsync(ctoken);
				if (task != null)
				{
					flag = false;
					result = task.ContinueWith<Task>(delegate(Task t)
					{
						this.AbortTransaction();
						this._isBulkCopyingInProgress = false;
						if (this._parser != null)
						{
							this._parser._asyncWrite = false;
						}
						if (this._parserLock != null)
						{
							this._parserLock.Release();
							this._parserLock = null;
						}
						return t;
					}, TaskScheduler.Default).Unwrap();
				}
				else
				{
					result = null;
				}
			}
			catch (OutOfMemoryException e)
			{
				this._connection.Abort(e);
				throw;
			}
			catch (StackOverflowException e2)
			{
				this._connection.Abort(e2);
				throw;
			}
			catch (ThreadAbortException e3)
			{
				this._connection.Abort(e3);
				throw;
			}
			finally
			{
				this._columnMappings.ReadOnly = false;
				if (flag)
				{
					this.AbortTransaction();
					this._isBulkCopyingInProgress = false;
					if (this._parser != null)
					{
						this._parser._asyncWrite = false;
					}
					if (this._parserLock != null)
					{
						this._parserLock.Release();
						this._parserLock = null;
					}
				}
			}
			return result;
		}

		// Token: 0x06000F0B RID: 3851 RVA: 0x0004F4E8 File Offset: 0x0004D6E8
		private void WriteRowSourceToServerCommon(int columnCount)
		{
			bool flag = false;
			this._columnMappings.ReadOnly = true;
			this._localColumnMappings = this._columnMappings;
			if (this._localColumnMappings.Count > 0)
			{
				this._localColumnMappings.ValidateCollection();
				using (IEnumerator enumerator = this._localColumnMappings.GetEnumerator())
				{
					while (enumerator.MoveNext())
					{
						if (((SqlBulkCopyColumnMapping)enumerator.Current)._internalSourceColumnOrdinal == -1)
						{
							flag = true;
							break;
						}
					}
					goto IL_8A;
				}
			}
			this._localColumnMappings = new SqlBulkCopyColumnMappingCollection();
			this._localColumnMappings.CreateDefaultMapping(columnCount);
			IL_8A:
			if (flag)
			{
				int num = -1;
				flag = false;
				if (this._localColumnMappings.Count > 0)
				{
					foreach (object obj in this._localColumnMappings)
					{
						SqlBulkCopyColumnMapping sqlBulkCopyColumnMapping = (SqlBulkCopyColumnMapping)obj;
						if (sqlBulkCopyColumnMapping._internalSourceColumnOrdinal == -1)
						{
							string text = this.UnquotedName(sqlBulkCopyColumnMapping.SourceColumn);
							switch (this._rowSourceType)
							{
							case SqlBulkCopy.ValueSourceType.IDataReader:
							case SqlBulkCopy.ValueSourceType.DbDataReader:
								try
								{
									num = this._DbDataReaderRowSource.GetOrdinal(text);
								}
								catch (IndexOutOfRangeException e)
								{
									throw SQL.BulkLoadNonMatchingColumnName(text, e);
								}
								break;
							case SqlBulkCopy.ValueSourceType.DataTable:
								num = ((DataTable)this._rowSource).Columns.IndexOf(text);
								break;
							case SqlBulkCopy.ValueSourceType.RowArray:
								num = ((DataRow[])this._rowSource)[0].Table.Columns.IndexOf(text);
								break;
							}
							if (num == -1)
							{
								throw SQL.BulkLoadNonMatchingColumnName(text);
							}
							sqlBulkCopyColumnMapping._internalSourceColumnOrdinal = num;
						}
					}
				}
			}
		}

		// Token: 0x06000F0C RID: 3852 RVA: 0x0004F6AC File Offset: 0x0004D8AC
		internal void OnConnectionClosed()
		{
			TdsParserStateObject stateObj = this._stateObj;
			if (stateObj != null)
			{
				stateObj.OnConnectionClosed();
			}
		}

		// Token: 0x06000F0D RID: 3853 RVA: 0x0004F6CC File Offset: 0x0004D8CC
		private void OnRowsCopied(SqlRowsCopiedEventArgs value)
		{
			SqlRowsCopiedEventHandler rowsCopiedEventHandler = this._rowsCopiedEventHandler;
			if (rowsCopiedEventHandler != null)
			{
				rowsCopiedEventHandler(this, value);
			}
		}

		// Token: 0x06000F0E RID: 3854 RVA: 0x0004F6EC File Offset: 0x0004D8EC
		private bool FireRowsCopiedEvent(long rowsCopied)
		{
			SqlInternalConnectionTds openTdsConnection = this._connection.GetOpenTdsConnection();
			bool canBeReleasedFromAnyThread = openTdsConnection._parserLock.CanBeReleasedFromAnyThread;
			openTdsConnection._parserLock.Release();
			SqlRowsCopiedEventArgs sqlRowsCopiedEventArgs = new SqlRowsCopiedEventArgs(rowsCopied);
			try
			{
				this._insideRowsCopiedEvent = true;
				this.OnRowsCopied(sqlRowsCopiedEventArgs);
			}
			finally
			{
				this._insideRowsCopiedEvent = false;
				openTdsConnection._parserLock.Wait(canBeReleasedFromAnyThread);
			}
			return sqlRowsCopiedEventArgs.Abort;
		}

		// Token: 0x06000F0F RID: 3855 RVA: 0x0004F760 File Offset: 0x0004D960
		private Task ReadWriteColumnValueAsync(int col)
		{
			bool isSqlType;
			bool flag;
			bool isNull;
			object obj = this.GetValueFromSourceRow(col, out isSqlType, out flag, out isNull);
			_SqlMetaData metadata = this._sortedColumnMappings[col]._metadata;
			if (!flag)
			{
				obj = this.ConvertValue(obj, metadata, isNull, ref isSqlType, out flag);
			}
			Task result = null;
			if (metadata.type != SqlDbType.Variant)
			{
				result = this._parser.WriteBulkCopyValue(obj, metadata, this._stateObj, isSqlType, flag, isNull);
			}
			else
			{
				SqlBuffer.StorageType storageType = SqlBuffer.StorageType.Empty;
				if (this._SqlDataReaderRowSource != null && this._connection.IsKatmaiOrNewer)
				{
					storageType = this._SqlDataReaderRowSource.GetVariantInternalStorageType(this._sortedColumnMappings[col]._sourceColumnOrdinal);
				}
				if (storageType == SqlBuffer.StorageType.DateTime2)
				{
					this._parser.WriteSqlVariantDateTime2((DateTime)obj, this._stateObj);
				}
				else if (storageType == SqlBuffer.StorageType.Date)
				{
					this._parser.WriteSqlVariantDate((DateTime)obj, this._stateObj);
				}
				else
				{
					result = this._parser.WriteSqlVariantDataRowValue(obj, this._stateObj, true);
				}
			}
			return result;
		}

		// Token: 0x06000F10 RID: 3856 RVA: 0x0004F857 File Offset: 0x0004DA57
		private void RegisterForConnectionCloseNotification<T>(ref Task<T> outerTask)
		{
			SqlConnection connection = this._connection;
			if (connection == null)
			{
				throw ADP.ClosedConnectionError();
			}
			connection.RegisterForConnectionCloseNotification<T>(ref outerTask, this, 3);
		}

		// Token: 0x06000F11 RID: 3857 RVA: 0x0004F870 File Offset: 0x0004DA70
		private Task CopyColumnsAsync(int col, TaskCompletionSource<object> source = null)
		{
			Task result = null;
			Task task = null;
			try
			{
				int i;
				for (i = col; i < this._sortedColumnMappings.Count; i++)
				{
					task = this.ReadWriteColumnValueAsync(i);
					if (task != null)
					{
						break;
					}
				}
				if (task != null)
				{
					if (source == null)
					{
						source = new TaskCompletionSource<object>();
						result = source.Task;
					}
					this.CopyColumnsAsyncSetupContinuation(source, task, i);
					return result;
				}
				if (source != null)
				{
					source.SetResult(null);
				}
			}
			catch (Exception exception)
			{
				if (source == null)
				{
					throw;
				}
				source.TrySetException(exception);
			}
			return result;
		}

		// Token: 0x06000F12 RID: 3858 RVA: 0x0004F8F8 File Offset: 0x0004DAF8
		private void CopyColumnsAsyncSetupContinuation(TaskCompletionSource<object> source, Task task, int i)
		{
			AsyncHelper.ContinueTask(task, source, delegate
			{
				if (i + 1 < this._sortedColumnMappings.Count)
				{
					this.CopyColumnsAsync(i + 1, source);
					return;
				}
				source.SetResult(null);
			}, this._connection.GetOpenTdsConnection(), null, null, null, null);
		}

		// Token: 0x06000F13 RID: 3859 RVA: 0x0004F948 File Offset: 0x0004DB48
		private void CheckAndRaiseNotification()
		{
			bool flag = false;
			Exception ex = null;
			this._rowsCopied++;
			if (this._notifyAfter > 0 && this._rowsUntilNotification > 0)
			{
				int num = this._rowsUntilNotification - 1;
				this._rowsUntilNotification = num;
				if (num == 0)
				{
					try
					{
						this._stateObj.BcpLock = true;
						flag = this.FireRowsCopiedEvent((long)this._rowsCopied);
						if (ConnectionState.Open != this._connection.State)
						{
							ex = ADP.OpenConnectionRequired("CheckAndRaiseNotification", this._connection.State);
						}
					}
					catch (Exception ex2)
					{
						if (!ADP.IsCatchableExceptionType(ex2))
						{
							ex = ex2;
						}
						else
						{
							ex = OperationAbortedException.Aborted(ex2);
						}
					}
					finally
					{
						this._stateObj.BcpLock = false;
					}
					if (!flag)
					{
						this._rowsUntilNotification = this._notifyAfter;
					}
				}
			}
			if (!flag && this._rowsUntilNotification > this._notifyAfter)
			{
				this._rowsUntilNotification = this._notifyAfter;
			}
			if (ex == null && flag)
			{
				ex = OperationAbortedException.Aborted(null);
			}
			if (this._connection.State != ConnectionState.Open)
			{
				throw ADP.OpenConnectionRequired("WriteToServer", this._connection.State);
			}
			if (ex != null)
			{
				this._parser._asyncWrite = false;
				this._parser.WriteBulkCopyDone(this._stateObj);
				this.RunParser(null);
				this.AbortTransaction();
				throw ex;
			}
		}

		// Token: 0x06000F14 RID: 3860 RVA: 0x0004FAA0 File Offset: 0x0004DCA0
		private Task CheckForCancellation(CancellationToken cts, TaskCompletionSource<object> tcs)
		{
			if (cts.IsCancellationRequested)
			{
				if (tcs == null)
				{
					tcs = new TaskCompletionSource<object>();
				}
				tcs.SetCanceled();
				return tcs.Task;
			}
			return null;
		}

		// Token: 0x06000F15 RID: 3861 RVA: 0x0004FAC4 File Offset: 0x0004DCC4
		private TaskCompletionSource<object> ContinueTaskPend(Task task, TaskCompletionSource<object> source, Func<TaskCompletionSource<object>> action)
		{
			if (task == null)
			{
				return action();
			}
			AsyncHelper.ContinueTask(task, source, delegate
			{
				action();
			}, null, null, null, null, null);
			return null;
		}

		// Token: 0x06000F16 RID: 3862 RVA: 0x0004FB08 File Offset: 0x0004DD08
		private Task CopyRowsAsync(int rowsSoFar, int totalRows, CancellationToken cts, TaskCompletionSource<object> source = null)
		{
			Task task = null;
			try
			{
				int i = rowsSoFar;
				Action <>9__1;
				Action <>9__2;
				while ((totalRows <= 0 || i < totalRows) && this._hasMoreRowToCopy)
				{
					if (this._isAsyncBulkCopy)
					{
						task = this.CheckForCancellation(cts, source);
						if (task != null)
						{
							return task;
						}
					}
					this._stateObj.WriteByte(209);
					Task task2 = this.CopyColumnsAsync(0, null);
					if (task2 != null)
					{
						source = (source ?? new TaskCompletionSource<object>());
						task = source.Task;
						AsyncHelper.ContinueTask(task2, source, delegate
						{
							this.CheckAndRaiseNotification();
							Task task5 = this.ReadFromRowSourceAsync(cts);
							if (task5 == null)
							{
								this.CopyRowsAsync(i + 1, totalRows, cts, source);
								return;
							}
							Task task6 = task5;
							TaskCompletionSource<object> source3 = source;
							Action onSuccess2;
							if ((onSuccess2 = <>9__2) == null)
							{
								onSuccess2 = (<>9__2 = delegate()
								{
									this.CopyRowsAsync(i + 1, totalRows, cts, source);
								});
							}
							AsyncHelper.ContinueTask(task6, source3, onSuccess2, this._connection.GetOpenTdsConnection(), null, null, null, null);
						}, this._connection.GetOpenTdsConnection(), null, null, null, null);
						return task;
					}
					this.CheckAndRaiseNotification();
					Task task3 = this.ReadFromRowSourceAsync(cts);
					if (task3 != null)
					{
						if (source == null)
						{
							source = new TaskCompletionSource<object>();
						}
						task = source.Task;
						Task task4 = task3;
						TaskCompletionSource<object> source2 = source;
						Action onSuccess;
						if ((onSuccess = <>9__1) == null)
						{
							onSuccess = (<>9__1 = delegate()
							{
								this.CopyRowsAsync(i + 1, totalRows, cts, source);
							});
						}
						AsyncHelper.ContinueTask(task4, source2, onSuccess, this._connection.GetOpenTdsConnection(), null, null, null, null);
						return task;
					}
					int j = i;
					i = j + 1;
				}
				if (source != null)
				{
					source.TrySetResult(null);
				}
			}
			catch (Exception exception)
			{
				if (source == null)
				{
					throw;
				}
				source.TrySetException(exception);
			}
			return task;
		}

		// Token: 0x06000F17 RID: 3863 RVA: 0x0004FCDC File Offset: 0x0004DEDC
		private Task CopyBatchesAsync(BulkCopySimpleResultSet internalResults, string updateBulkCommandText, CancellationToken cts, TaskCompletionSource<object> source = null)
		{
			try
			{
				Action <>9__0;
				while (this._hasMoreRowToCopy)
				{
					SqlInternalConnectionTds openTdsConnection = this._connection.GetOpenTdsConnection();
					if (this.IsCopyOption(SqlBulkCopyOptions.UseInternalTransaction))
					{
						openTdsConnection.ThreadHasParserLockForClose = true;
						try
						{
							this._internalTransaction = this._connection.BeginTransaction();
						}
						finally
						{
							openTdsConnection.ThreadHasParserLockForClose = false;
						}
					}
					Task task = this.SubmitUpdateBulkCommand(updateBulkCommandText);
					if (task != null)
					{
						if (source == null)
						{
							source = new TaskCompletionSource<object>();
						}
						Task task2 = task;
						TaskCompletionSource<object> source2 = source;
						Action onSuccess;
						if ((onSuccess = <>9__0) == null)
						{
							onSuccess = (<>9__0 = delegate()
							{
								if (this.CopyBatchesAsyncContinued(internalResults, updateBulkCommandText, cts, source) == null)
								{
									this.CopyBatchesAsync(internalResults, updateBulkCommandText, cts, source);
								}
							});
						}
						AsyncHelper.ContinueTask(task2, source2, onSuccess, this._connection.GetOpenTdsConnection(), null, null, null, null);
						return source.Task;
					}
					Task task3 = this.CopyBatchesAsyncContinued(internalResults, updateBulkCommandText, cts, source);
					if (task3 != null)
					{
						return task3;
					}
				}
			}
			catch (Exception exception)
			{
				if (source != null)
				{
					source.TrySetException(exception);
					return source.Task;
				}
				throw;
			}
			if (source != null)
			{
				source.SetResult(null);
				return source.Task;
			}
			return null;
		}

		// Token: 0x06000F18 RID: 3864 RVA: 0x0004FE60 File Offset: 0x0004E060
		private Task CopyBatchesAsyncContinued(BulkCopySimpleResultSet internalResults, string updateBulkCommandText, CancellationToken cts, TaskCompletionSource<object> source)
		{
			Task result;
			try
			{
				this.WriteMetaData(internalResults);
				Task task = this.CopyRowsAsync(0, this._savedBatchSize, cts, null);
				if (task != null)
				{
					if (source == null)
					{
						source = new TaskCompletionSource<object>();
					}
					AsyncHelper.ContinueTask(task, source, delegate
					{
						if (this.CopyBatchesAsyncContinuedOnSuccess(internalResults, updateBulkCommandText, cts, source) == null)
						{
							this.CopyBatchesAsync(internalResults, updateBulkCommandText, cts, source);
						}
					}, this._connection.GetOpenTdsConnection(), delegate(Exception _)
					{
						this.CopyBatchesAsyncContinuedOnError(false);
					}, delegate
					{
						this.CopyBatchesAsyncContinuedOnError(true);
					}, null, null);
					result = source.Task;
				}
				else
				{
					result = this.CopyBatchesAsyncContinuedOnSuccess(internalResults, updateBulkCommandText, cts, source);
				}
			}
			catch (Exception exception)
			{
				if (source == null)
				{
					throw;
				}
				source.TrySetException(exception);
				result = source.Task;
			}
			return result;
		}

		// Token: 0x06000F19 RID: 3865 RVA: 0x0004FF74 File Offset: 0x0004E174
		private Task CopyBatchesAsyncContinuedOnSuccess(BulkCopySimpleResultSet internalResults, string updateBulkCommandText, CancellationToken cts, TaskCompletionSource<object> source)
		{
			Task result;
			try
			{
				Task task = this._parser.WriteBulkCopyDone(this._stateObj);
				if (task == null)
				{
					this.RunParser(null);
					this.CommitTransaction();
					result = null;
				}
				else
				{
					if (source == null)
					{
						source = new TaskCompletionSource<object>();
					}
					AsyncHelper.ContinueTask(task, source, delegate
					{
						try
						{
							this.RunParser(null);
							this.CommitTransaction();
						}
						catch (Exception)
						{
							this.CopyBatchesAsyncContinuedOnError(false);
							throw;
						}
						this.CopyBatchesAsync(internalResults, updateBulkCommandText, cts, source);
					}, this._connection.GetOpenTdsConnection(), delegate(Exception _)
					{
						this.CopyBatchesAsyncContinuedOnError(false);
					}, null, null, null);
					result = source.Task;
				}
			}
			catch (Exception exception)
			{
				if (source == null)
				{
					throw;
				}
				source.TrySetException(exception);
				result = source.Task;
			}
			return result;
		}

		// Token: 0x06000F1A RID: 3866 RVA: 0x0005005C File Offset: 0x0004E25C
		private void CopyBatchesAsyncContinuedOnError(bool cleanupParser)
		{
			SqlInternalConnectionTds openTdsConnection = this._connection.GetOpenTdsConnection();
			try
			{
				if (cleanupParser && this._parser != null && this._stateObj != null)
				{
					this._parser._asyncWrite = false;
					this._parser.WriteBulkCopyDone(this._stateObj);
					this.RunParser(null);
				}
				if (this._stateObj != null)
				{
					this.CleanUpStateObjectOnError();
				}
			}
			catch (OutOfMemoryException)
			{
				openTdsConnection.DoomThisConnection();
				throw;
			}
			catch (StackOverflowException)
			{
				openTdsConnection.DoomThisConnection();
				throw;
			}
			catch (ThreadAbortException)
			{
				openTdsConnection.DoomThisConnection();
				throw;
			}
			this.AbortTransaction();
		}

		// Token: 0x06000F1B RID: 3867 RVA: 0x00050108 File Offset: 0x0004E308
		private void CleanUpStateObjectOnError()
		{
			if (this._stateObj != null)
			{
				this._parser.Connection.ThreadHasParserLockForClose = true;
				try
				{
					this._stateObj.ResetBuffer();
					this._stateObj._outputPacketNumber = 1;
					if (this._parser.State == TdsParserState.OpenNotLoggedIn || this._parser.State == TdsParserState.OpenLoggedIn)
					{
						this._stateObj.CancelRequest();
					}
					this._stateObj._internalTimeout = false;
					this._stateObj.CloseSession();
					this._stateObj._bulkCopyOpperationInProgress = false;
					this._stateObj._bulkCopyWriteTimeout = false;
					this._stateObj = null;
				}
				finally
				{
					this._parser.Connection.ThreadHasParserLockForClose = false;
				}
			}
		}

		// Token: 0x06000F1C RID: 3868 RVA: 0x000501CC File Offset: 0x0004E3CC
		private void WriteToServerInternalRestContinuedAsync(BulkCopySimpleResultSet internalResults, CancellationToken cts, TaskCompletionSource<object> source)
		{
			Task task = null;
			try
			{
				string updateBulkCommandText = this.AnalyzeTargetAndCreateUpdateBulkCommand(internalResults);
				if (this._sortedColumnMappings.Count != 0)
				{
					this._stateObj.SniContext = SniContext.Snix_SendRows;
					this._savedBatchSize = this._batchSize;
					this._rowsUntilNotification = this._notifyAfter;
					this._rowsCopied = 0;
					this._currentRowMetadata = new SqlBulkCopy.SourceColumnMetadata[this._sortedColumnMappings.Count];
					for (int i = 0; i < this._currentRowMetadata.Length; i++)
					{
						this._currentRowMetadata[i] = this.GetColumnMetadata(i);
					}
					task = this.CopyBatchesAsync(internalResults, updateBulkCommandText, cts, null);
				}
				if (task != null)
				{
					if (source == null)
					{
						source = new TaskCompletionSource<object>();
					}
					AsyncHelper.ContinueTask(task, source, delegate
					{
						if (task.IsCanceled)
						{
							this._localColumnMappings = null;
							try
							{
								this.CleanUpStateObjectOnError();
								return;
							}
							finally
							{
								source.SetCanceled();
							}
						}
						if (task.Exception != null)
						{
							source.SetException(task.Exception.InnerException);
							return;
						}
						this._localColumnMappings = null;
						try
						{
							this.CleanUpStateObjectOnError();
						}
						finally
						{
							if (source != null)
							{
								if (cts.IsCancellationRequested)
								{
									source.SetCanceled();
								}
								else
								{
									source.SetResult(null);
								}
							}
						}
					}, this._connection.GetOpenTdsConnection(), null, null, null, null);
				}
				else
				{
					this._localColumnMappings = null;
					try
					{
						this.CleanUpStateObjectOnError();
					}
					catch (Exception)
					{
					}
					if (source != null)
					{
						source.SetResult(null);
					}
				}
			}
			catch (Exception exception)
			{
				this._localColumnMappings = null;
				try
				{
					this.CleanUpStateObjectOnError();
				}
				catch (Exception)
				{
				}
				if (source == null)
				{
					throw;
				}
				source.TrySetException(exception);
			}
		}

		// Token: 0x06000F1D RID: 3869 RVA: 0x0005037C File Offset: 0x0004E57C
		private void WriteToServerInternalRestAsync(CancellationToken cts, TaskCompletionSource<object> source)
		{
			this._hasMoreRowToCopy = true;
			Task<BulkCopySimpleResultSet> internalResultsTask = null;
			BulkCopySimpleResultSet internalResults = new BulkCopySimpleResultSet();
			SqlInternalConnectionTds openTdsConnection = this._connection.GetOpenTdsConnection();
			try
			{
				this._parser = this._connection.Parser;
				this._parser._asyncWrite = this._isAsyncBulkCopy;
				Task task;
				try
				{
					task = this._connection.ValidateAndReconnect(delegate
					{
						if (this._parserLock != null)
						{
							this._parserLock.Release();
							this._parserLock = null;
						}
					}, this.BulkCopyTimeout);
				}
				catch (SqlException inner)
				{
					throw SQL.BulkLoadInvalidDestinationTable(this._destinationTableName, inner);
				}
				if (task != null)
				{
					if (this._isAsyncBulkCopy)
					{
						CancellationTokenRegistration regReconnectCancel = default(CancellationTokenRegistration);
						TaskCompletionSource<object> cancellableReconnectTS = new TaskCompletionSource<object>();
						if (cts.CanBeCanceled)
						{
							regReconnectCancel = cts.Register(delegate(object s)
							{
								((TaskCompletionSource<object>)s).TrySetCanceled();
							}, cancellableReconnectTS);
						}
						AsyncHelper.ContinueTask(task, cancellableReconnectTS, delegate
						{
							cancellableReconnectTS.SetResult(null);
						}, null, null, null, null, null);
						AsyncHelper.SetTimeoutException(cancellableReconnectTS, this.BulkCopyTimeout, () => SQL.BulkLoadInvalidDestinationTable(this._destinationTableName, SQL.CR_ReconnectTimeout()), CancellationToken.None);
						AsyncHelper.ContinueTask(cancellableReconnectTS.Task, source, delegate
						{
							regReconnectCancel.Dispose();
							if (this._parserLock != null)
							{
								this._parserLock.Release();
								this._parserLock = null;
							}
							this._parserLock = this._connection.GetOpenTdsConnection()._parserLock;
							this._parserLock.Wait(true);
							this.WriteToServerInternalRestAsync(cts, source);
						}, null, delegate(Exception e)
						{
							regReconnectCancel.Dispose();
						}, delegate
						{
							regReconnectCancel.Dispose();
						}, (Exception ex) => SQL.BulkLoadInvalidDestinationTable(this._destinationTableName, ex), this._connection);
					}
					else
					{
						try
						{
							AsyncHelper.WaitForCompletion(task, this.BulkCopyTimeout, delegate
							{
								throw SQL.CR_ReconnectTimeout();
							}, true);
						}
						catch (SqlException inner2)
						{
							throw SQL.BulkLoadInvalidDestinationTable(this._destinationTableName, inner2);
						}
						this._parserLock = this._connection.GetOpenTdsConnection()._parserLock;
						this._parserLock.Wait(false);
						this.WriteToServerInternalRestAsync(cts, source);
					}
				}
				else
				{
					if (this._isAsyncBulkCopy)
					{
						this._connection.AddWeakReference(this, 3);
					}
					openTdsConnection.ThreadHasParserLockForClose = true;
					try
					{
						this._stateObj = this._parser.GetSession(this);
						this._stateObj._bulkCopyOpperationInProgress = true;
						this._stateObj.StartSession(this);
					}
					finally
					{
						openTdsConnection.ThreadHasParserLockForClose = false;
					}
					try
					{
						internalResultsTask = this.CreateAndExecuteInitialQueryAsync(out internalResults);
					}
					catch (SqlException inner3)
					{
						throw SQL.BulkLoadInvalidDestinationTable(this._destinationTableName, inner3);
					}
					if (internalResultsTask != null)
					{
						AsyncHelper.ContinueTask(internalResultsTask, source, delegate
						{
							this.WriteToServerInternalRestContinuedAsync(internalResultsTask.Result, cts, source);
						}, this._connection.GetOpenTdsConnection(), null, null, null, null);
					}
					else
					{
						this.WriteToServerInternalRestContinuedAsync(internalResults, cts, source);
					}
				}
			}
			catch (Exception exception)
			{
				if (source == null)
				{
					throw;
				}
				source.TrySetException(exception);
			}
		}

		// Token: 0x06000F1E RID: 3870 RVA: 0x00050718 File Offset: 0x0004E918
		private Task WriteToServerInternalAsync(CancellationToken ctoken)
		{
			TaskCompletionSource<object> source = null;
			Task<object> result = null;
			if (this._isAsyncBulkCopy)
			{
				source = new TaskCompletionSource<object>();
				result = source.Task;
				this.RegisterForConnectionCloseNotification<object>(ref result);
			}
			if (this._destinationTableName != null)
			{
				try
				{
					Task task = this.ReadFromRowSourceAsync(ctoken);
					if (task != null)
					{
						AsyncHelper.ContinueTask(task, source, delegate
						{
							if (!this._hasMoreRowToCopy)
							{
								source.SetResult(null);
								return;
							}
							this.WriteToServerInternalRestAsync(ctoken, source);
						}, this._connection.GetOpenTdsConnection(), null, null, null, null);
						return result;
					}
					if (!this._hasMoreRowToCopy)
					{
						if (source != null)
						{
							source.SetResult(null);
						}
						return result;
					}
					this.WriteToServerInternalRestAsync(ctoken, source);
					return result;
				}
				catch (Exception exception)
				{
					if (source == null)
					{
						throw;
					}
					source.TrySetException(exception);
				}
				return result;
			}
			if (source != null)
			{
				source.SetException(SQL.BulkLoadMissingDestinationTable());
				return result;
			}
			throw SQL.BulkLoadMissingDestinationTable();
		}

		// Token: 0x06000F1F RID: 3871 RVA: 0x00050838 File Offset: 0x0004EA38
		[CompilerGenerated]
		private BulkCopySimpleResultSet <CreateAndExecuteInitialQueryAsync>b__71_0(Task t)
		{
			if (t.IsFaulted)
			{
				throw t.Exception.InnerException;
			}
			BulkCopySimpleResultSet bulkCopySimpleResultSet = new BulkCopySimpleResultSet();
			this.RunParserReliably(bulkCopySimpleResultSet);
			return bulkCopySimpleResultSet;
		}

		// Token: 0x06000F20 RID: 3872 RVA: 0x00050867 File Offset: 0x0004EA67
		[CompilerGenerated]
		private void <SubmitUpdateBulkCommand>b__73_0(Task t)
		{
			if (t.IsFaulted)
			{
				throw t.Exception.InnerException;
			}
			this.RunParserReliably(null);
		}

		// Token: 0x06000F21 RID: 3873 RVA: 0x00050884 File Offset: 0x0004EA84
		[CompilerGenerated]
		private Task<bool> <ReadFromRowSourceAsync>b__78_0(Task<bool> t)
		{
			if (t.Status == TaskStatus.RanToCompletion)
			{
				this._hasMoreRowToCopy = t.Result;
			}
			return t;
		}

		// Token: 0x04000A7C RID: 2684
		private const int MetaDataResultId = 1;

		// Token: 0x04000A7D RID: 2685
		private const int CollationResultId = 2;

		// Token: 0x04000A7E RID: 2686
		private const int CollationId = 3;

		// Token: 0x04000A7F RID: 2687
		private const int MAX_LENGTH = 2147483647;

		// Token: 0x04000A80 RID: 2688
		private const int DefaultCommandTimeout = 30;

		// Token: 0x04000A81 RID: 2689
		private bool _enableStreaming;

		// Token: 0x04000A82 RID: 2690
		private int _batchSize;

		// Token: 0x04000A83 RID: 2691
		private bool _ownConnection;

		// Token: 0x04000A84 RID: 2692
		private SqlBulkCopyOptions _copyOptions;

		// Token: 0x04000A85 RID: 2693
		private int _timeout = 30;

		// Token: 0x04000A86 RID: 2694
		private string _destinationTableName;

		// Token: 0x04000A87 RID: 2695
		private int _rowsCopied;

		// Token: 0x04000A88 RID: 2696
		private int _notifyAfter;

		// Token: 0x04000A89 RID: 2697
		private int _rowsUntilNotification;

		// Token: 0x04000A8A RID: 2698
		private bool _insideRowsCopiedEvent;

		// Token: 0x04000A8B RID: 2699
		private object _rowSource;

		// Token: 0x04000A8C RID: 2700
		private SqlDataReader _SqlDataReaderRowSource;

		// Token: 0x04000A8D RID: 2701
		private DbDataReader _DbDataReaderRowSource;

		// Token: 0x04000A8E RID: 2702
		private DataTable _dataTableSource;

		// Token: 0x04000A8F RID: 2703
		private SqlBulkCopyColumnMappingCollection _columnMappings;

		// Token: 0x04000A90 RID: 2704
		private SqlBulkCopyColumnMappingCollection _localColumnMappings;

		// Token: 0x04000A91 RID: 2705
		private SqlConnection _connection;

		// Token: 0x04000A92 RID: 2706
		private SqlTransaction _internalTransaction;

		// Token: 0x04000A93 RID: 2707
		private SqlTransaction _externalTransaction;

		// Token: 0x04000A94 RID: 2708
		private SqlBulkCopy.ValueSourceType _rowSourceType;

		// Token: 0x04000A95 RID: 2709
		private DataRow _currentRow;

		// Token: 0x04000A96 RID: 2710
		private int _currentRowLength;

		// Token: 0x04000A97 RID: 2711
		private DataRowState _rowStateToSkip;

		// Token: 0x04000A98 RID: 2712
		private IEnumerator _rowEnumerator;

		// Token: 0x04000A99 RID: 2713
		private TdsParser _parser;

		// Token: 0x04000A9A RID: 2714
		private TdsParserStateObject _stateObj;

		// Token: 0x04000A9B RID: 2715
		private List<_ColumnMapping> _sortedColumnMappings;

		// Token: 0x04000A9C RID: 2716
		private SqlRowsCopiedEventHandler _rowsCopiedEventHandler;

		// Token: 0x04000A9D RID: 2717
		private int _savedBatchSize;

		// Token: 0x04000A9E RID: 2718
		private bool _hasMoreRowToCopy;

		// Token: 0x04000A9F RID: 2719
		private bool _isAsyncBulkCopy;

		// Token: 0x04000AA0 RID: 2720
		private bool _isBulkCopyingInProgress;

		// Token: 0x04000AA1 RID: 2721
		private SqlInternalConnectionTds.SyncAsyncLock _parserLock;

		// Token: 0x04000AA2 RID: 2722
		private SqlBulkCopy.SourceColumnMetadata[] _currentRowMetadata;

		// Token: 0x02000135 RID: 309
		private enum ValueSourceType
		{
			// Token: 0x04000AA4 RID: 2724
			Unspecified,
			// Token: 0x04000AA5 RID: 2725
			IDataReader,
			// Token: 0x04000AA6 RID: 2726
			DataTable,
			// Token: 0x04000AA7 RID: 2727
			RowArray,
			// Token: 0x04000AA8 RID: 2728
			DbDataReader
		}

		// Token: 0x02000136 RID: 310
		private enum ValueMethod : byte
		{
			// Token: 0x04000AAA RID: 2730
			GetValue,
			// Token: 0x04000AAB RID: 2731
			SqlTypeSqlDecimal,
			// Token: 0x04000AAC RID: 2732
			SqlTypeSqlDouble,
			// Token: 0x04000AAD RID: 2733
			SqlTypeSqlSingle,
			// Token: 0x04000AAE RID: 2734
			DataFeedStream,
			// Token: 0x04000AAF RID: 2735
			DataFeedText,
			// Token: 0x04000AB0 RID: 2736
			DataFeedXml
		}

		// Token: 0x02000137 RID: 311
		private struct SourceColumnMetadata
		{
			// Token: 0x06000F22 RID: 3874 RVA: 0x0005089C File Offset: 0x0004EA9C
			public SourceColumnMetadata(SqlBulkCopy.ValueMethod method, bool isSqlType, bool isDataFeed)
			{
				this.Method = method;
				this.IsSqlType = isSqlType;
				this.IsDataFeed = isDataFeed;
			}

			// Token: 0x04000AB1 RID: 2737
			public readonly SqlBulkCopy.ValueMethod Method;

			// Token: 0x04000AB2 RID: 2738
			public readonly bool IsSqlType;

			// Token: 0x04000AB3 RID: 2739
			public readonly bool IsDataFeed;
		}

		// Token: 0x02000138 RID: 312
		[CompilerGenerated]
		private sealed class <>c__DisplayClass105_0
		{
			// Token: 0x06000F23 RID: 3875 RVA: 0x00005C14 File Offset: 0x00003E14
			public <>c__DisplayClass105_0()
			{
			}

			// Token: 0x06000F24 RID: 3876 RVA: 0x000508B4 File Offset: 0x0004EAB4
			internal Task <WriteRowSourceToServerAsync>b__3(Task t)
			{
				this.<>4__this.AbortTransaction();
				this.<>4__this._isBulkCopyingInProgress = false;
				if (this.<>4__this._parser != null)
				{
					this.<>4__this._parser._asyncWrite = false;
				}
				if (this.<>4__this._parserLock != null)
				{
					this.<>4__this._parserLock.Release();
					this.<>4__this._parserLock = null;
				}
				return t;
			}

			// Token: 0x04000AB4 RID: 2740
			public SqlBulkCopy <>4__this;

			// Token: 0x04000AB5 RID: 2741
			public int columnCount;

			// Token: 0x04000AB6 RID: 2742
			public CancellationToken ctoken;
		}

		// Token: 0x02000139 RID: 313
		[CompilerGenerated]
		private sealed class <>c__DisplayClass105_1
		{
			// Token: 0x06000F25 RID: 3877 RVA: 0x00005C14 File Offset: 0x00003E14
			public <>c__DisplayClass105_1()
			{
			}

			// Token: 0x06000F26 RID: 3878 RVA: 0x00050920 File Offset: 0x0004EB20
			internal void <WriteRowSourceToServerAsync>b__1(Task t)
			{
				Task task = this.CS$<>8__locals1.<>4__this.WriteRowSourceToServerAsync(this.CS$<>8__locals1.columnCount, this.CS$<>8__locals1.ctoken);
				if (task == null)
				{
					this.tcs.SetResult(null);
					return;
				}
				Task task2 = task;
				TaskCompletionSource<object> completion = this.tcs;
				Action onSuccess;
				if ((onSuccess = this.<>9__2) == null)
				{
					onSuccess = (this.<>9__2 = delegate()
					{
						this.tcs.SetResult(null);
					});
				}
				AsyncHelper.ContinueTask(task2, completion, onSuccess, null, null, null, null, null);
			}

			// Token: 0x06000F27 RID: 3879 RVA: 0x00050994 File Offset: 0x0004EB94
			internal void <WriteRowSourceToServerAsync>b__2()
			{
				this.tcs.SetResult(null);
			}

			// Token: 0x04000AB7 RID: 2743
			public TaskCompletionSource<object> tcs;

			// Token: 0x04000AB8 RID: 2744
			public SqlBulkCopy.<>c__DisplayClass105_0 CS$<>8__locals1;

			// Token: 0x04000AB9 RID: 2745
			public Action <>9__2;
		}

		// Token: 0x0200013A RID: 314
		[CompilerGenerated]
		[Serializable]
		private sealed class <>c
		{
			// Token: 0x06000F28 RID: 3880 RVA: 0x000509A2 File Offset: 0x0004EBA2
			// Note: this type is marked as 'beforefieldinit'.
			static <>c()
			{
			}

			// Token: 0x06000F29 RID: 3881 RVA: 0x00005C14 File Offset: 0x00003E14
			public <>c()
			{
			}

			// Token: 0x06000F2A RID: 3882 RVA: 0x000509AE File Offset: 0x0004EBAE
			internal void <WriteRowSourceToServerAsync>b__105_0()
			{
				throw SQL.CR_ReconnectTimeout();
			}

			// Token: 0x06000F2B RID: 3883 RVA: 0x000509B5 File Offset: 0x0004EBB5
			internal void <WriteToServerInternalRestAsync>b__124_3(object s)
			{
				((TaskCompletionSource<object>)s).TrySetCanceled();
			}

			// Token: 0x06000F2C RID: 3884 RVA: 0x000509AE File Offset: 0x0004EBAE
			internal void <WriteToServerInternalRestAsync>b__124_1()
			{
				throw SQL.CR_ReconnectTimeout();
			}

			// Token: 0x04000ABA RID: 2746
			public static readonly SqlBulkCopy.<>c <>9 = new SqlBulkCopy.<>c();

			// Token: 0x04000ABB RID: 2747
			public static Action <>9__105_0;

			// Token: 0x04000ABC RID: 2748
			public static Action<object> <>9__124_3;

			// Token: 0x04000ABD RID: 2749
			public static Action <>9__124_1;
		}

		// Token: 0x0200013B RID: 315
		[CompilerGenerated]
		private sealed class <>c__DisplayClass113_0
		{
			// Token: 0x06000F2D RID: 3885 RVA: 0x00005C14 File Offset: 0x00003E14
			public <>c__DisplayClass113_0()
			{
			}

			// Token: 0x06000F2E RID: 3886 RVA: 0x000509C4 File Offset: 0x0004EBC4
			internal void <CopyColumnsAsyncSetupContinuation>b__0()
			{
				if (this.i + 1 < this.<>4__this._sortedColumnMappings.Count)
				{
					this.<>4__this.CopyColumnsAsync(this.i + 1, this.source);
					return;
				}
				this.source.SetResult(null);
			}

			// Token: 0x04000ABE RID: 2750
			public int i;

			// Token: 0x04000ABF RID: 2751
			public SqlBulkCopy <>4__this;

			// Token: 0x04000AC0 RID: 2752
			public TaskCompletionSource<object> source;
		}

		// Token: 0x0200013C RID: 316
		[CompilerGenerated]
		private sealed class <>c__DisplayClass116_0
		{
			// Token: 0x06000F2F RID: 3887 RVA: 0x00005C14 File Offset: 0x00003E14
			public <>c__DisplayClass116_0()
			{
			}

			// Token: 0x06000F30 RID: 3888 RVA: 0x00050A12 File Offset: 0x0004EC12
			internal void <ContinueTaskPend>b__0()
			{
				this.action();
			}

			// Token: 0x04000AC1 RID: 2753
			public Func<TaskCompletionSource<object>> action;
		}

		// Token: 0x0200013D RID: 317
		[CompilerGenerated]
		private sealed class <>c__DisplayClass117_0
		{
			// Token: 0x06000F31 RID: 3889 RVA: 0x00005C14 File Offset: 0x00003E14
			public <>c__DisplayClass117_0()
			{
			}

			// Token: 0x06000F32 RID: 3890 RVA: 0x00050A20 File Offset: 0x0004EC20
			internal void <CopyRowsAsync>b__1()
			{
				this.<>4__this.CopyRowsAsync(this.i + 1, this.totalRows, this.cts, this.source);
			}

			// Token: 0x06000F33 RID: 3891 RVA: 0x00050A48 File Offset: 0x0004EC48
			internal void <CopyRowsAsync>b__0()
			{
				this.<>4__this.CheckAndRaiseNotification();
				Task task = this.<>4__this.ReadFromRowSourceAsync(this.cts);
				if (task == null)
				{
					this.<>4__this.CopyRowsAsync(this.i + 1, this.totalRows, this.cts, this.source);
					return;
				}
				Task task2 = task;
				TaskCompletionSource<object> completion = this.source;
				Action onSuccess;
				if ((onSuccess = this.<>9__2) == null)
				{
					onSuccess = (this.<>9__2 = delegate()
					{
						this.<>4__this.CopyRowsAsync(this.i + 1, this.totalRows, this.cts, this.source);
					});
				}
				AsyncHelper.ContinueTask(task2, completion, onSuccess, this.<>4__this._connection.GetOpenTdsConnection(), null, null, null, null);
			}

			// Token: 0x06000F34 RID: 3892 RVA: 0x00050A20 File Offset: 0x0004EC20
			internal void <CopyRowsAsync>b__2()
			{
				this.<>4__this.CopyRowsAsync(this.i + 1, this.totalRows, this.cts, this.source);
			}

			// Token: 0x04000AC2 RID: 2754
			public SqlBulkCopy <>4__this;

			// Token: 0x04000AC3 RID: 2755
			public int i;

			// Token: 0x04000AC4 RID: 2756
			public int totalRows;

			// Token: 0x04000AC5 RID: 2757
			public CancellationToken cts;

			// Token: 0x04000AC6 RID: 2758
			public TaskCompletionSource<object> source;

			// Token: 0x04000AC7 RID: 2759
			public Action <>9__1;

			// Token: 0x04000AC8 RID: 2760
			public Action <>9__2;
		}

		// Token: 0x0200013E RID: 318
		[CompilerGenerated]
		private sealed class <>c__DisplayClass118_0
		{
			// Token: 0x06000F35 RID: 3893 RVA: 0x00005C14 File Offset: 0x00003E14
			public <>c__DisplayClass118_0()
			{
			}

			// Token: 0x06000F36 RID: 3894 RVA: 0x00050ADC File Offset: 0x0004ECDC
			internal void <CopyBatchesAsync>b__0()
			{
				if (this.<>4__this.CopyBatchesAsyncContinued(this.internalResults, this.updateBulkCommandText, this.cts, this.source) == null)
				{
					this.<>4__this.CopyBatchesAsync(this.internalResults, this.updateBulkCommandText, this.cts, this.source);
				}
			}

			// Token: 0x04000AC9 RID: 2761
			public SqlBulkCopy <>4__this;

			// Token: 0x04000ACA RID: 2762
			public BulkCopySimpleResultSet internalResults;

			// Token: 0x04000ACB RID: 2763
			public string updateBulkCommandText;

			// Token: 0x04000ACC RID: 2764
			public CancellationToken cts;

			// Token: 0x04000ACD RID: 2765
			public TaskCompletionSource<object> source;

			// Token: 0x04000ACE RID: 2766
			public Action <>9__0;
		}

		// Token: 0x0200013F RID: 319
		[CompilerGenerated]
		private sealed class <>c__DisplayClass119_0
		{
			// Token: 0x06000F37 RID: 3895 RVA: 0x00005C14 File Offset: 0x00003E14
			public <>c__DisplayClass119_0()
			{
			}

			// Token: 0x06000F38 RID: 3896 RVA: 0x00050B34 File Offset: 0x0004ED34
			internal void <CopyBatchesAsyncContinued>b__0()
			{
				if (this.<>4__this.CopyBatchesAsyncContinuedOnSuccess(this.internalResults, this.updateBulkCommandText, this.cts, this.source) == null)
				{
					this.<>4__this.CopyBatchesAsync(this.internalResults, this.updateBulkCommandText, this.cts, this.source);
				}
			}

			// Token: 0x06000F39 RID: 3897 RVA: 0x00050B8A File Offset: 0x0004ED8A
			internal void <CopyBatchesAsyncContinued>b__1(Exception _)
			{
				this.<>4__this.CopyBatchesAsyncContinuedOnError(false);
			}

			// Token: 0x06000F3A RID: 3898 RVA: 0x00050B98 File Offset: 0x0004ED98
			internal void <CopyBatchesAsyncContinued>b__2()
			{
				this.<>4__this.CopyBatchesAsyncContinuedOnError(true);
			}

			// Token: 0x04000ACF RID: 2767
			public SqlBulkCopy <>4__this;

			// Token: 0x04000AD0 RID: 2768
			public BulkCopySimpleResultSet internalResults;

			// Token: 0x04000AD1 RID: 2769
			public string updateBulkCommandText;

			// Token: 0x04000AD2 RID: 2770
			public CancellationToken cts;

			// Token: 0x04000AD3 RID: 2771
			public TaskCompletionSource<object> source;
		}

		// Token: 0x02000140 RID: 320
		[CompilerGenerated]
		private sealed class <>c__DisplayClass120_0
		{
			// Token: 0x06000F3B RID: 3899 RVA: 0x00005C14 File Offset: 0x00003E14
			public <>c__DisplayClass120_0()
			{
			}

			// Token: 0x06000F3C RID: 3900 RVA: 0x00050BA8 File Offset: 0x0004EDA8
			internal void <CopyBatchesAsyncContinuedOnSuccess>b__0()
			{
				try
				{
					this.<>4__this.RunParser(null);
					this.<>4__this.CommitTransaction();
				}
				catch (Exception)
				{
					this.<>4__this.CopyBatchesAsyncContinuedOnError(false);
					throw;
				}
				this.<>4__this.CopyBatchesAsync(this.internalResults, this.updateBulkCommandText, this.cts, this.source);
			}

			// Token: 0x06000F3D RID: 3901 RVA: 0x00050C14 File Offset: 0x0004EE14
			internal void <CopyBatchesAsyncContinuedOnSuccess>b__1(Exception _)
			{
				this.<>4__this.CopyBatchesAsyncContinuedOnError(false);
			}

			// Token: 0x04000AD4 RID: 2772
			public SqlBulkCopy <>4__this;

			// Token: 0x04000AD5 RID: 2773
			public BulkCopySimpleResultSet internalResults;

			// Token: 0x04000AD6 RID: 2774
			public string updateBulkCommandText;

			// Token: 0x04000AD7 RID: 2775
			public CancellationToken cts;

			// Token: 0x04000AD8 RID: 2776
			public TaskCompletionSource<object> source;
		}

		// Token: 0x02000141 RID: 321
		[CompilerGenerated]
		private sealed class <>c__DisplayClass123_0
		{
			// Token: 0x06000F3E RID: 3902 RVA: 0x00005C14 File Offset: 0x00003E14
			public <>c__DisplayClass123_0()
			{
			}

			// Token: 0x06000F3F RID: 3903 RVA: 0x00050C24 File Offset: 0x0004EE24
			internal void <WriteToServerInternalRestContinuedAsync>b__0()
			{
				if (this.task.IsCanceled)
				{
					this.<>4__this._localColumnMappings = null;
					try
					{
						this.<>4__this.CleanUpStateObjectOnError();
						return;
					}
					finally
					{
						this.source.SetCanceled();
					}
				}
				if (this.task.Exception != null)
				{
					this.source.SetException(this.task.Exception.InnerException);
					return;
				}
				this.<>4__this._localColumnMappings = null;
				try
				{
					this.<>4__this.CleanUpStateObjectOnError();
				}
				finally
				{
					if (this.source != null)
					{
						if (this.cts.IsCancellationRequested)
						{
							this.source.SetCanceled();
						}
						else
						{
							this.source.SetResult(null);
						}
					}
				}
			}

			// Token: 0x04000AD9 RID: 2777
			public Task task;

			// Token: 0x04000ADA RID: 2778
			public SqlBulkCopy <>4__this;

			// Token: 0x04000ADB RID: 2779
			public TaskCompletionSource<object> source;

			// Token: 0x04000ADC RID: 2780
			public CancellationToken cts;
		}

		// Token: 0x02000142 RID: 322
		[CompilerGenerated]
		private sealed class <>c__DisplayClass124_0
		{
			// Token: 0x06000F40 RID: 3904 RVA: 0x00005C14 File Offset: 0x00003E14
			public <>c__DisplayClass124_0()
			{
			}

			// Token: 0x06000F41 RID: 3905 RVA: 0x00050CF0 File Offset: 0x0004EEF0
			internal void <WriteToServerInternalRestAsync>b__0()
			{
				if (this.<>4__this._parserLock != null)
				{
					this.<>4__this._parserLock.Release();
					this.<>4__this._parserLock = null;
				}
			}

			// Token: 0x06000F42 RID: 3906 RVA: 0x00050D1B File Offset: 0x0004EF1B
			internal Exception <WriteToServerInternalRestAsync>b__5()
			{
				return SQL.BulkLoadInvalidDestinationTable(this.<>4__this._destinationTableName, SQL.CR_ReconnectTimeout());
			}

			// Token: 0x06000F43 RID: 3907 RVA: 0x00050D32 File Offset: 0x0004EF32
			internal Exception <WriteToServerInternalRestAsync>b__9(Exception ex)
			{
				return SQL.BulkLoadInvalidDestinationTable(this.<>4__this._destinationTableName, ex);
			}

			// Token: 0x06000F44 RID: 3908 RVA: 0x00050D45 File Offset: 0x0004EF45
			internal void <WriteToServerInternalRestAsync>b__2()
			{
				this.<>4__this.WriteToServerInternalRestContinuedAsync(this.internalResultsTask.Result, this.cts, this.source);
			}

			// Token: 0x04000ADD RID: 2781
			public SqlBulkCopy <>4__this;

			// Token: 0x04000ADE RID: 2782
			public CancellationToken cts;

			// Token: 0x04000ADF RID: 2783
			public TaskCompletionSource<object> source;

			// Token: 0x04000AE0 RID: 2784
			public Task<BulkCopySimpleResultSet> internalResultsTask;
		}

		// Token: 0x02000143 RID: 323
		[CompilerGenerated]
		private sealed class <>c__DisplayClass124_1
		{
			// Token: 0x06000F45 RID: 3909 RVA: 0x00005C14 File Offset: 0x00003E14
			public <>c__DisplayClass124_1()
			{
			}

			// Token: 0x06000F46 RID: 3910 RVA: 0x00050D69 File Offset: 0x0004EF69
			internal void <WriteToServerInternalRestAsync>b__4()
			{
				this.cancellableReconnectTS.SetResult(null);
			}

			// Token: 0x06000F47 RID: 3911 RVA: 0x00050D78 File Offset: 0x0004EF78
			internal void <WriteToServerInternalRestAsync>b__6()
			{
				this.regReconnectCancel.Dispose();
				if (this.CS$<>8__locals1.<>4__this._parserLock != null)
				{
					this.CS$<>8__locals1.<>4__this._parserLock.Release();
					this.CS$<>8__locals1.<>4__this._parserLock = null;
				}
				this.CS$<>8__locals1.<>4__this._parserLock = this.CS$<>8__locals1.<>4__this._connection.GetOpenTdsConnection()._parserLock;
				this.CS$<>8__locals1.<>4__this._parserLock.Wait(true);
				this.CS$<>8__locals1.<>4__this.WriteToServerInternalRestAsync(this.CS$<>8__locals1.cts, this.CS$<>8__locals1.source);
			}

			// Token: 0x06000F48 RID: 3912 RVA: 0x00050E2E File Offset: 0x0004F02E
			internal void <WriteToServerInternalRestAsync>b__7(Exception e)
			{
				this.regReconnectCancel.Dispose();
			}

			// Token: 0x06000F49 RID: 3913 RVA: 0x00050E2E File Offset: 0x0004F02E
			internal void <WriteToServerInternalRestAsync>b__8()
			{
				this.regReconnectCancel.Dispose();
			}

			// Token: 0x04000AE1 RID: 2785
			public TaskCompletionSource<object> cancellableReconnectTS;

			// Token: 0x04000AE2 RID: 2786
			public CancellationTokenRegistration regReconnectCancel;

			// Token: 0x04000AE3 RID: 2787
			public SqlBulkCopy.<>c__DisplayClass124_0 CS$<>8__locals1;
		}

		// Token: 0x02000144 RID: 324
		[CompilerGenerated]
		private sealed class <>c__DisplayClass125_0
		{
			// Token: 0x06000F4A RID: 3914 RVA: 0x00005C14 File Offset: 0x00003E14
			public <>c__DisplayClass125_0()
			{
			}

			// Token: 0x06000F4B RID: 3915 RVA: 0x00050E3B File Offset: 0x0004F03B
			internal void <WriteToServerInternalAsync>b__0()
			{
				if (!this.<>4__this._hasMoreRowToCopy)
				{
					this.source.SetResult(null);
					return;
				}
				this.<>4__this.WriteToServerInternalRestAsync(this.ctoken, this.source);
			}

			// Token: 0x04000AE4 RID: 2788
			public SqlBulkCopy <>4__this;

			// Token: 0x04000AE5 RID: 2789
			public TaskCompletionSource<object> source;

			// Token: 0x04000AE6 RID: 2790
			public CancellationToken ctoken;
		}
	}
}
