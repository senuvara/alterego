﻿using System;
using System.Collections.Generic;
using Microsoft.SqlServer.Server;

namespace System.Data.SqlClient
{
	// Token: 0x02000124 RID: 292
	internal class ParameterPeekAheadValue
	{
		// Token: 0x06000E74 RID: 3700 RVA: 0x00005C14 File Offset: 0x00003E14
		public ParameterPeekAheadValue()
		{
		}

		// Token: 0x04000A35 RID: 2613
		internal IEnumerator<SqlDataRecord> Enumerator;

		// Token: 0x04000A36 RID: 2614
		internal SqlDataRecord FirstRecord;
	}
}
