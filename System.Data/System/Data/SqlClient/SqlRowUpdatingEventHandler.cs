﻿using System;

namespace System.Data.SqlClient
{
	/// <summary>Represents the method that will handle the <see cref="E:System.Data.SqlClient.SqlDataAdapter.RowUpdating" /> event of a <see cref="T:System.Data.SqlClient.SqlDataAdapter" />.</summary>
	/// <param name="sender">The source of the event. </param>
	/// <param name="e">The <see cref="T:System.Data.SqlClient.SqlRowUpdatingEventArgs" /> that contains the event data. </param>
	// Token: 0x020001C0 RID: 448
	// (Invoke) Token: 0x0600149A RID: 5274
	public delegate void SqlRowUpdatingEventHandler(object sender, SqlRowUpdatingEventArgs e);
}
