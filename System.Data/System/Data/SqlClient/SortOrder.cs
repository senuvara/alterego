﻿using System;

namespace System.Data.SqlClient
{
	/// <summary>Specifies how rows of data are sorted.</summary>
	// Token: 0x02000127 RID: 295
	public enum SortOrder
	{
		/// <summary>The default. No sort order is specified.</summary>
		// Token: 0x04000A3A RID: 2618
		Unspecified = -1,
		/// <summary>Rows are sorted in ascending order.</summary>
		// Token: 0x04000A3B RID: 2619
		Ascending,
		/// <summary>Rows are sorted in descending order.</summary>
		// Token: 0x04000A3C RID: 2620
		Descending
	}
}
