﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.ProviderBase;
using System.Diagnostics;
using System.EnterpriseServices;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;
using Unity;

namespace System.Data.SqlClient
{
	/// <summary>Represents an open connection to a SQL Server database. This class cannot be inherited.</summary>
	// Token: 0x02000168 RID: 360
	public sealed class SqlConnection : DbConnection, ICloneable, IDbConnection, IDisposable
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Data.SqlClient.SqlConnection" /> class when given a string that contains the connection string.</summary>
		/// <param name="connectionString">The connection used to open the SQL Server database.</param>
		// Token: 0x0600108F RID: 4239 RVA: 0x0005685D File Offset: 0x00054A5D
		public SqlConnection(string connectionString) : this()
		{
			this.ConnectionString = connectionString;
			this.CacheConnectionStringProperties();
		}

		// Token: 0x06001090 RID: 4240 RVA: 0x00056872 File Offset: 0x00054A72
		private SqlConnection(SqlConnection connection)
		{
			this._reconnectLock = new object();
			this._originalConnectionId = Guid.Empty;
			base..ctor();
			GC.SuppressFinalize(this);
			this.CopyFrom(connection);
			this._connectionString = connection._connectionString;
			this.CacheConnectionStringProperties();
		}

		// Token: 0x06001091 RID: 4241 RVA: 0x000568B0 File Offset: 0x00054AB0
		private void CacheConnectionStringProperties()
		{
			SqlConnectionString sqlConnectionString = this.ConnectionOptions as SqlConnectionString;
			if (sqlConnectionString != null)
			{
				this._connectRetryCount = sqlConnectionString.ConnectRetryCount;
			}
		}

		/// <summary>When set to <see langword="true" />, enables statistics gathering for the current connection.</summary>
		/// <returns>Returns <see langword="true" /> if statistics gathering is enabled; otherwise <see langword="false" />. <see langword="false" /> is the default.</returns>
		// Token: 0x170002D9 RID: 729
		// (get) Token: 0x06001092 RID: 4242 RVA: 0x000568D8 File Offset: 0x00054AD8
		// (set) Token: 0x06001093 RID: 4243 RVA: 0x000568E0 File Offset: 0x00054AE0
		public bool StatisticsEnabled
		{
			get
			{
				return this._collectstats;
			}
			set
			{
				if (value)
				{
					if (ConnectionState.Open == this.State)
					{
						if (this._statistics == null)
						{
							this._statistics = new SqlStatistics();
							ADP.TimerCurrent(out this._statistics._openTimestamp);
						}
						this.Parser.Statistics = this._statistics;
					}
				}
				else if (this._statistics != null && ConnectionState.Open == this.State)
				{
					this.Parser.Statistics = null;
					ADP.TimerCurrent(out this._statistics._closeTimestamp);
				}
				this._collectstats = value;
			}
		}

		// Token: 0x170002DA RID: 730
		// (get) Token: 0x06001094 RID: 4244 RVA: 0x00056963 File Offset: 0x00054B63
		// (set) Token: 0x06001095 RID: 4245 RVA: 0x0005696B File Offset: 0x00054B6B
		internal bool AsyncCommandInProgress
		{
			get
			{
				return this._AsyncCommandInProgress;
			}
			set
			{
				this._AsyncCommandInProgress = value;
			}
		}

		// Token: 0x170002DB RID: 731
		// (get) Token: 0x06001096 RID: 4246 RVA: 0x00056974 File Offset: 0x00054B74
		internal SqlConnectionString.TransactionBindingEnum TransactionBinding
		{
			get
			{
				return ((SqlConnectionString)this.ConnectionOptions).TransactionBinding;
			}
		}

		// Token: 0x170002DC RID: 732
		// (get) Token: 0x06001097 RID: 4247 RVA: 0x00056986 File Offset: 0x00054B86
		internal SqlConnectionString.TypeSystem TypeSystem
		{
			get
			{
				return ((SqlConnectionString)this.ConnectionOptions).TypeSystemVersion;
			}
		}

		// Token: 0x170002DD RID: 733
		// (get) Token: 0x06001098 RID: 4248 RVA: 0x00056998 File Offset: 0x00054B98
		internal int ConnectRetryInterval
		{
			get
			{
				return ((SqlConnectionString)this.ConnectionOptions).ConnectRetryInterval;
			}
		}

		/// <summary>Gets or sets the string used to open a SQL Server database.</summary>
		/// <returns>The connection string that includes the source database name, and other parameters needed to establish the initial connection. The default value is an empty string.</returns>
		/// <exception cref="T:System.ArgumentException">An invalid connection string argument has been supplied, or a required connection string argument has not been supplied. </exception>
		// Token: 0x170002DE RID: 734
		// (get) Token: 0x06001099 RID: 4249 RVA: 0x000569AA File Offset: 0x00054BAA
		// (set) Token: 0x0600109A RID: 4250 RVA: 0x000569B2 File Offset: 0x00054BB2
		public override string ConnectionString
		{
			get
			{
				return this.ConnectionString_Get();
			}
			set
			{
				this.ConnectionString_Set(new SqlConnectionPoolKey(value));
				this._connectionString = value;
				this.CacheConnectionStringProperties();
			}
		}

		/// <summary>Gets the time to wait while trying to establish a connection before terminating the attempt and generating an error.</summary>
		/// <returns>The time (in seconds) to wait for a connection to open. The default value is 15 seconds.</returns>
		/// <exception cref="T:System.ArgumentException">The value set is less than 0. </exception>
		// Token: 0x170002DF RID: 735
		// (get) Token: 0x0600109B RID: 4251 RVA: 0x000569D0 File Offset: 0x00054BD0
		public override int ConnectionTimeout
		{
			get
			{
				SqlConnectionString sqlConnectionString = (SqlConnectionString)this.ConnectionOptions;
				if (sqlConnectionString == null)
				{
					return 15;
				}
				return sqlConnectionString.ConnectTimeout;
			}
		}

		/// <summary>Gets the name of the current database or the database to be used after a connection is opened.</summary>
		/// <returns>The name of the current database or the name of the database to be used after a connection is opened. The default value is an empty string.</returns>
		// Token: 0x170002E0 RID: 736
		// (get) Token: 0x0600109C RID: 4252 RVA: 0x000569F8 File Offset: 0x00054BF8
		public override string Database
		{
			get
			{
				SqlInternalConnection sqlInternalConnection = this.InnerConnection as SqlInternalConnection;
				string result;
				if (sqlInternalConnection != null)
				{
					result = sqlInternalConnection.CurrentDatabase;
				}
				else
				{
					SqlConnectionString sqlConnectionString = (SqlConnectionString)this.ConnectionOptions;
					result = ((sqlConnectionString != null) ? sqlConnectionString.InitialCatalog : "");
				}
				return result;
			}
		}

		/// <summary>Gets the name of the instance of SQL Server to which to connect.</summary>
		/// <returns>The name of the instance of SQL Server to which to connect. The default value is an empty string.</returns>
		// Token: 0x170002E1 RID: 737
		// (get) Token: 0x0600109D RID: 4253 RVA: 0x00056A3C File Offset: 0x00054C3C
		public override string DataSource
		{
			get
			{
				SqlInternalConnection sqlInternalConnection = this.InnerConnection as SqlInternalConnection;
				string result;
				if (sqlInternalConnection != null)
				{
					result = sqlInternalConnection.CurrentDataSource;
				}
				else
				{
					SqlConnectionString sqlConnectionString = (SqlConnectionString)this.ConnectionOptions;
					result = ((sqlConnectionString != null) ? sqlConnectionString.DataSource : "");
				}
				return result;
			}
		}

		/// <summary>Gets the size (in bytes) of network packets used to communicate with an instance of SQL Server.</summary>
		/// <returns>The size (in bytes) of network packets. The default value is 8000.</returns>
		// Token: 0x170002E2 RID: 738
		// (get) Token: 0x0600109E RID: 4254 RVA: 0x00056A80 File Offset: 0x00054C80
		public int PacketSize
		{
			get
			{
				SqlInternalConnectionTds sqlInternalConnectionTds = this.InnerConnection as SqlInternalConnectionTds;
				int result;
				if (sqlInternalConnectionTds != null)
				{
					result = sqlInternalConnectionTds.PacketSize;
				}
				else
				{
					SqlConnectionString sqlConnectionString = (SqlConnectionString)this.ConnectionOptions;
					result = ((sqlConnectionString != null) ? sqlConnectionString.PacketSize : 8000);
				}
				return result;
			}
		}

		/// <summary>The connection ID of the most recent connection attempt, regardless of whether the attempt succeeded or failed.</summary>
		/// <returns>The connection ID of the most recent connection attempt.</returns>
		// Token: 0x170002E3 RID: 739
		// (get) Token: 0x0600109F RID: 4255 RVA: 0x00056AC4 File Offset: 0x00054CC4
		public Guid ClientConnectionId
		{
			get
			{
				SqlInternalConnectionTds sqlInternalConnectionTds = this.InnerConnection as SqlInternalConnectionTds;
				if (sqlInternalConnectionTds != null)
				{
					return sqlInternalConnectionTds.ClientConnectionId;
				}
				Task currentReconnectionTask = this._currentReconnectionTask;
				if (currentReconnectionTask != null && !currentReconnectionTask.IsCompleted)
				{
					return this._originalConnectionId;
				}
				return Guid.Empty;
			}
		}

		/// <summary>Gets a string that contains the version of the instance of SQL Server to which the client is connected.</summary>
		/// <returns>The version of the instance of SQL Server.</returns>
		/// <exception cref="T:System.InvalidOperationException">The connection is closed. 
		///         <see cref="P:System.Data.SqlClient.SqlConnection.ServerVersion" /> was called while the returned Task was not completed and the connection was not opened after a call to <see cref="M:System.Data.SqlClient.SqlConnection.OpenAsync(System.Threading.CancellationToken)" />.</exception>
		// Token: 0x170002E4 RID: 740
		// (get) Token: 0x060010A0 RID: 4256 RVA: 0x00056B05 File Offset: 0x00054D05
		public override string ServerVersion
		{
			get
			{
				return this.GetOpenTdsConnection().ServerVersion;
			}
		}

		/// <summary>Indicates the state of the <see cref="T:System.Data.SqlClient.SqlConnection" /> during the most recent network operation performed on the connection.</summary>
		/// <returns>An <see cref="T:System.Data.ConnectionState" /> enumeration.</returns>
		// Token: 0x170002E5 RID: 741
		// (get) Token: 0x060010A1 RID: 4257 RVA: 0x00056B14 File Offset: 0x00054D14
		public override ConnectionState State
		{
			get
			{
				Task currentReconnectionTask = this._currentReconnectionTask;
				if (currentReconnectionTask != null && !currentReconnectionTask.IsCompleted)
				{
					return ConnectionState.Open;
				}
				return this.InnerConnection.State;
			}
		}

		// Token: 0x170002E6 RID: 742
		// (get) Token: 0x060010A2 RID: 4258 RVA: 0x00056B40 File Offset: 0x00054D40
		internal SqlStatistics Statistics
		{
			get
			{
				return this._statistics;
			}
		}

		/// <summary>Gets a string that identifies the database client.</summary>
		/// <returns>A string that identifies the database client. If not specified, the name of the client computer. If neither is specified, the value is an empty string.</returns>
		// Token: 0x170002E7 RID: 743
		// (get) Token: 0x060010A3 RID: 4259 RVA: 0x00056B48 File Offset: 0x00054D48
		public string WorkstationId
		{
			get
			{
				SqlConnectionString sqlConnectionString = (SqlConnectionString)this.ConnectionOptions;
				if (sqlConnectionString == null)
				{
					return string.Empty;
				}
				return sqlConnectionString.WorkstationId;
			}
		}

		// Token: 0x170002E8 RID: 744
		// (get) Token: 0x060010A4 RID: 4260 RVA: 0x00056B70 File Offset: 0x00054D70
		protected override DbProviderFactory DbProviderFactory
		{
			get
			{
				return SqlClientFactory.Instance;
			}
		}

		/// <summary>Occurs when SQL Server returns a warning or informational message.</summary>
		// Token: 0x14000022 RID: 34
		// (add) Token: 0x060010A5 RID: 4261 RVA: 0x00056B78 File Offset: 0x00054D78
		// (remove) Token: 0x060010A6 RID: 4262 RVA: 0x00056BB0 File Offset: 0x00054DB0
		public event SqlInfoMessageEventHandler InfoMessage
		{
			[CompilerGenerated]
			add
			{
				SqlInfoMessageEventHandler sqlInfoMessageEventHandler = this.InfoMessage;
				SqlInfoMessageEventHandler sqlInfoMessageEventHandler2;
				do
				{
					sqlInfoMessageEventHandler2 = sqlInfoMessageEventHandler;
					SqlInfoMessageEventHandler value2 = (SqlInfoMessageEventHandler)Delegate.Combine(sqlInfoMessageEventHandler2, value);
					sqlInfoMessageEventHandler = Interlocked.CompareExchange<SqlInfoMessageEventHandler>(ref this.InfoMessage, value2, sqlInfoMessageEventHandler2);
				}
				while (sqlInfoMessageEventHandler != sqlInfoMessageEventHandler2);
			}
			[CompilerGenerated]
			remove
			{
				SqlInfoMessageEventHandler sqlInfoMessageEventHandler = this.InfoMessage;
				SqlInfoMessageEventHandler sqlInfoMessageEventHandler2;
				do
				{
					sqlInfoMessageEventHandler2 = sqlInfoMessageEventHandler;
					SqlInfoMessageEventHandler value2 = (SqlInfoMessageEventHandler)Delegate.Remove(sqlInfoMessageEventHandler2, value);
					sqlInfoMessageEventHandler = Interlocked.CompareExchange<SqlInfoMessageEventHandler>(ref this.InfoMessage, value2, sqlInfoMessageEventHandler2);
				}
				while (sqlInfoMessageEventHandler != sqlInfoMessageEventHandler2);
			}
		}

		/// <summary>Gets or sets the <see cref="P:System.Data.SqlClient.SqlConnection.FireInfoMessageEventOnUserErrors" /> property.</summary>
		/// <returns>
		///     <see langword="true" /> if the <see cref="P:System.Data.SqlClient.SqlConnection.FireInfoMessageEventOnUserErrors" /> property has been set; otherwise <see langword="false" />.</returns>
		// Token: 0x170002E9 RID: 745
		// (get) Token: 0x060010A7 RID: 4263 RVA: 0x00056BE5 File Offset: 0x00054DE5
		// (set) Token: 0x060010A8 RID: 4264 RVA: 0x00056BED File Offset: 0x00054DED
		public bool FireInfoMessageEventOnUserErrors
		{
			get
			{
				return this._fireInfoMessageEventOnUserErrors;
			}
			set
			{
				this._fireInfoMessageEventOnUserErrors = value;
			}
		}

		// Token: 0x170002EA RID: 746
		// (get) Token: 0x060010A9 RID: 4265 RVA: 0x00056BF6 File Offset: 0x00054DF6
		internal int ReconnectCount
		{
			get
			{
				return this._reconnectCount;
			}
		}

		// Token: 0x170002EB RID: 747
		// (get) Token: 0x060010AA RID: 4266 RVA: 0x00056BFE File Offset: 0x00054DFE
		// (set) Token: 0x060010AB RID: 4267 RVA: 0x00056C06 File Offset: 0x00054E06
		internal bool ForceNewConnection
		{
			[CompilerGenerated]
			get
			{
				return this.<ForceNewConnection>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<ForceNewConnection>k__BackingField = value;
			}
		}

		// Token: 0x060010AC RID: 4268 RVA: 0x00056C0F File Offset: 0x00054E0F
		protected override void OnStateChange(StateChangeEventArgs stateChange)
		{
			if (!this._suppressStateChangeForReconnection)
			{
				base.OnStateChange(stateChange);
			}
		}

		/// <summary>Starts a database transaction.</summary>
		/// <returns>An object representing the new transaction.</returns>
		/// <exception cref="T:System.Data.SqlClient.SqlException">Parallel transactions are not allowed when using Multiple Active Result Sets (MARS).</exception>
		/// <exception cref="T:System.InvalidOperationException">Parallel transactions are not supported. </exception>
		// Token: 0x060010AD RID: 4269 RVA: 0x00056C20 File Offset: 0x00054E20
		public new SqlTransaction BeginTransaction()
		{
			return this.BeginTransaction(IsolationLevel.Unspecified, null);
		}

		/// <summary>Starts a database transaction with the specified isolation level.</summary>
		/// <param name="iso">The isolation level under which the transaction should run. </param>
		/// <returns>An object representing the new transaction.</returns>
		/// <exception cref="T:System.Data.SqlClient.SqlException">Parallel transactions are not allowed when using Multiple Active Result Sets (MARS).</exception>
		/// <exception cref="T:System.InvalidOperationException">Parallel transactions are not supported. </exception>
		// Token: 0x060010AE RID: 4270 RVA: 0x00056C2A File Offset: 0x00054E2A
		public new SqlTransaction BeginTransaction(IsolationLevel iso)
		{
			return this.BeginTransaction(iso, null);
		}

		/// <summary>Starts a database transaction with the specified transaction name.</summary>
		/// <param name="transactionName">The name of the transaction. </param>
		/// <returns>An object representing the new transaction.</returns>
		/// <exception cref="T:System.Data.SqlClient.SqlException">Parallel transactions are not allowed when using Multiple Active Result Sets (MARS).</exception>
		/// <exception cref="T:System.InvalidOperationException">Parallel transactions are not supported. </exception>
		// Token: 0x060010AF RID: 4271 RVA: 0x00056C34 File Offset: 0x00054E34
		public SqlTransaction BeginTransaction(string transactionName)
		{
			return this.BeginTransaction(IsolationLevel.Unspecified, transactionName);
		}

		// Token: 0x060010B0 RID: 4272 RVA: 0x00056C3E File Offset: 0x00054E3E
		protected override DbTransaction BeginDbTransaction(IsolationLevel isolationLevel)
		{
			DbTransaction result = this.BeginTransaction(isolationLevel);
			GC.KeepAlive(this);
			return result;
		}

		/// <summary>Starts a database transaction with the specified isolation level and transaction name.</summary>
		/// <param name="iso">The isolation level under which the transaction should run. </param>
		/// <param name="transactionName">The name of the transaction. </param>
		/// <returns>An object representing the new transaction.</returns>
		/// <exception cref="T:System.Data.SqlClient.SqlException">Parallel transactions are not allowed when using Multiple Active Result Sets (MARS).</exception>
		/// <exception cref="T:System.InvalidOperationException">Parallel transactions are not supported. </exception>
		// Token: 0x060010B1 RID: 4273 RVA: 0x00056C50 File Offset: 0x00054E50
		public SqlTransaction BeginTransaction(IsolationLevel iso, string transactionName)
		{
			this.WaitForPendingReconnection();
			SqlStatistics statistics = null;
			SqlTransaction result;
			try
			{
				statistics = SqlStatistics.StartTimer(this.Statistics);
				bool shouldReconnect = true;
				SqlTransaction sqlTransaction;
				do
				{
					sqlTransaction = this.GetOpenTdsConnection().BeginSqlTransaction(iso, transactionName, shouldReconnect);
					shouldReconnect = false;
				}
				while (sqlTransaction.InternalTransaction.ConnectionHasBeenRestored);
				GC.KeepAlive(this);
				result = sqlTransaction;
			}
			finally
			{
				SqlStatistics.StopTimer(statistics);
			}
			return result;
		}

		/// <summary>Changes the current database for an open <see cref="T:System.Data.SqlClient.SqlConnection" />.</summary>
		/// <param name="database">The name of the database to use instead of the current database. </param>
		/// <exception cref="T:System.ArgumentException">The database name is not valid.</exception>
		/// <exception cref="T:System.InvalidOperationException">The connection is not open. </exception>
		/// <exception cref="T:System.Data.SqlClient.SqlException">Cannot change the database. </exception>
		// Token: 0x060010B2 RID: 4274 RVA: 0x00056CB4 File Offset: 0x00054EB4
		public override void ChangeDatabase(string database)
		{
			SqlStatistics statistics = null;
			this.RepairInnerConnection();
			try
			{
				statistics = SqlStatistics.StartTimer(this.Statistics);
				this.InnerConnection.ChangeDatabase(database);
			}
			finally
			{
				SqlStatistics.StopTimer(statistics);
			}
		}

		/// <summary>Empties the connection pool.</summary>
		// Token: 0x060010B3 RID: 4275 RVA: 0x00056CFC File Offset: 0x00054EFC
		public static void ClearAllPools()
		{
			SqlConnectionFactory.SingletonInstance.ClearAllPools();
		}

		/// <summary>Empties the connection pool associated with the specified connection.</summary>
		/// <param name="connection">The <see cref="T:System.Data.SqlClient.SqlConnection" /> to be cleared from the pool.</param>
		// Token: 0x060010B4 RID: 4276 RVA: 0x00056D08 File Offset: 0x00054F08
		public static void ClearPool(SqlConnection connection)
		{
			ADP.CheckArgumentNull(connection, "connection");
			DbConnectionOptions userConnectionOptions = connection.UserConnectionOptions;
			if (userConnectionOptions != null)
			{
				SqlConnectionFactory.SingletonInstance.ClearPool(connection);
			}
		}

		// Token: 0x060010B5 RID: 4277 RVA: 0x00056D35 File Offset: 0x00054F35
		private void CloseInnerConnection()
		{
			this.InnerConnection.CloseConnection(this, this.ConnectionFactory);
		}

		/// <summary>Closes the connection to the database. This is the preferred method of closing any open connection.</summary>
		/// <exception cref="T:System.Data.SqlClient.SqlException">The connection-level error that occurred while opening the connection. </exception>
		// Token: 0x060010B6 RID: 4278 RVA: 0x00056D4C File Offset: 0x00054F4C
		public override void Close()
		{
			ConnectionState state = this.State;
			Guid operationId = default(Guid);
			Guid clientConnectionId = default(Guid);
			if (state != ConnectionState.Closed)
			{
				operationId = SqlConnection.s_diagnosticListener.WriteConnectionCloseBefore(this, "Close");
				clientConnectionId = this.ClientConnectionId;
			}
			SqlStatistics statistics = null;
			Exception ex = null;
			try
			{
				statistics = SqlStatistics.StartTimer(this.Statistics);
				Task currentReconnectionTask = this._currentReconnectionTask;
				if (currentReconnectionTask != null && !currentReconnectionTask.IsCompleted)
				{
					CancellationTokenSource reconnectionCancellationSource = this._reconnectionCancellationSource;
					if (reconnectionCancellationSource != null)
					{
						reconnectionCancellationSource.Cancel();
					}
					AsyncHelper.WaitForCompletion(currentReconnectionTask, 0, null, false);
					if (this.State != ConnectionState.Open)
					{
						this.OnStateChange(DbConnectionInternal.StateChangeClosed);
					}
				}
				this.CancelOpenAndWait();
				this.CloseInnerConnection();
				GC.SuppressFinalize(this);
				if (this.Statistics != null)
				{
					ADP.TimerCurrent(out this._statistics._closeTimestamp);
				}
			}
			catch (Exception ex)
			{
				throw;
			}
			finally
			{
				SqlStatistics.StopTimer(statistics);
				if (state != ConnectionState.Closed)
				{
					if (ex != null)
					{
						SqlConnection.s_diagnosticListener.WriteConnectionCloseError(operationId, clientConnectionId, this, ex, "Close");
					}
					else
					{
						SqlConnection.s_diagnosticListener.WriteConnectionCloseAfter(operationId, clientConnectionId, this, "Close");
					}
				}
			}
		}

		/// <summary>Creates and returns a <see cref="T:System.Data.SqlClient.SqlCommand" /> object associated with the <see cref="T:System.Data.SqlClient.SqlConnection" />.</summary>
		/// <returns>A <see cref="T:System.Data.SqlClient.SqlCommand" /> object.</returns>
		// Token: 0x060010B7 RID: 4279 RVA: 0x00056E64 File Offset: 0x00055064
		public new SqlCommand CreateCommand()
		{
			return new SqlCommand(null, this);
		}

		// Token: 0x060010B8 RID: 4280 RVA: 0x00056E70 File Offset: 0x00055070
		private void DisposeMe(bool disposing)
		{
			if (!disposing)
			{
				SqlInternalConnectionTds sqlInternalConnectionTds = this.InnerConnection as SqlInternalConnectionTds;
				if (sqlInternalConnectionTds != null && !sqlInternalConnectionTds.ConnectionOptions.Pooling)
				{
					TdsParser parser = sqlInternalConnectionTds.Parser;
					if (parser != null && parser._physicalStateObj != null)
					{
						parser._physicalStateObj.DecrementPendingCallbacks(false);
					}
				}
			}
		}

		/// <summary>Opens a database connection with the property settings specified by the <see cref="P:System.Data.SqlClient.SqlConnection.ConnectionString" />.</summary>
		/// <exception cref="T:System.InvalidOperationException">Cannot open a connection without specifying a data source or server.orThe connection is already open.</exception>
		/// <exception cref="T:System.Data.SqlClient.SqlException">A connection-level error occurred while opening the connection. If the <see cref="P:System.Data.SqlClient.SqlException.Number" /> property contains the value 18487 or 18488, this indicates that the specified password has expired or must be reset. See the <see cref="M:System.Data.SqlClient.SqlConnection.ChangePassword(System.String,System.String)" /> method for more information.The <see langword="&lt;system.data.localdb&gt;" /> tag in the app.config file has invalid or unknown elements.</exception>
		/// <exception cref="T:System.Configuration.ConfigurationErrorsException">There are two entries with the same name in the <see langword="&lt;localdbinstances&gt;" /> section.</exception>
		// Token: 0x060010B9 RID: 4281 RVA: 0x00056EBC File Offset: 0x000550BC
		public override void Open()
		{
			Guid operationId = SqlConnection.s_diagnosticListener.WriteConnectionOpenBefore(this, "Open");
			this.PrepareStatisticsForNewConnection();
			SqlStatistics statistics = null;
			Exception ex = null;
			try
			{
				statistics = SqlStatistics.StartTimer(this.Statistics);
				if (!this.TryOpen(null))
				{
					throw ADP.InternalError(ADP.InternalErrorCode.SynchronousConnectReturnedPending);
				}
			}
			catch (Exception ex)
			{
				throw;
			}
			finally
			{
				SqlStatistics.StopTimer(statistics);
				if (ex != null)
				{
					SqlConnection.s_diagnosticListener.WriteConnectionOpenError(operationId, this, ex, "Open");
				}
				else
				{
					SqlConnection.s_diagnosticListener.WriteConnectionOpenAfter(operationId, this, "Open");
				}
			}
		}

		// Token: 0x060010BA RID: 4282 RVA: 0x00056F54 File Offset: 0x00055154
		internal void RegisterWaitingForReconnect(Task waitingTask)
		{
			if (((SqlConnectionString)this.ConnectionOptions).MARS)
			{
				return;
			}
			Interlocked.CompareExchange<Task>(ref this._asyncWaitingForReconnection, waitingTask, null);
			if (this._asyncWaitingForReconnection != waitingTask)
			{
				throw SQL.MARSUnspportedOnConnection();
			}
		}

		// Token: 0x060010BB RID: 4283 RVA: 0x00056F88 File Offset: 0x00055188
		private async Task ReconnectAsync(int timeout)
		{
			try
			{
				long commandTimeoutExpiration = 0L;
				if (timeout > 0)
				{
					commandTimeoutExpiration = ADP.TimerCurrent() + ADP.TimerFromSeconds(timeout);
				}
				CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();
				this._reconnectionCancellationSource = cancellationTokenSource;
				CancellationToken ctoken = cancellationTokenSource.Token;
				int retryCount = this._connectRetryCount;
				for (int attempt = 0; attempt < retryCount; attempt++)
				{
					if (ctoken.IsCancellationRequested)
					{
						return;
					}
					try
					{
						try
						{
							this.ForceNewConnection = true;
							await this.OpenAsync(ctoken).ConfigureAwait(false);
							this._reconnectCount++;
						}
						finally
						{
							this.ForceNewConnection = false;
						}
						return;
					}
					catch (SqlException innerException)
					{
						if (attempt == retryCount - 1)
						{
							throw SQL.CR_AllAttemptsFailed(innerException, this._originalConnectionId);
						}
						if (timeout > 0 && ADP.TimerRemaining(commandTimeoutExpiration) < ADP.TimerFromSeconds(this.ConnectRetryInterval))
						{
							throw SQL.CR_NextAttemptWillExceedQueryTimeout(innerException, this._originalConnectionId);
						}
					}
					await Task.Delay(1000 * this.ConnectRetryInterval, ctoken).ConfigureAwait(false);
				}
				ctoken = default(CancellationToken);
			}
			finally
			{
				this._recoverySessionData = null;
				this._suppressStateChangeForReconnection = false;
			}
		}

		// Token: 0x060010BC RID: 4284 RVA: 0x00056FD8 File Offset: 0x000551D8
		internal Task ValidateAndReconnect(Action beforeDisconnect, int timeout)
		{
			Task task = this._currentReconnectionTask;
			while (task != null && task.IsCompleted)
			{
				Interlocked.CompareExchange<Task>(ref this._currentReconnectionTask, null, task);
				task = this._currentReconnectionTask;
			}
			if (task == null)
			{
				if (this._connectRetryCount > 0)
				{
					SqlInternalConnectionTds openTdsConnection = this.GetOpenTdsConnection();
					if (openTdsConnection._sessionRecoveryAcknowledged && !openTdsConnection.Parser._physicalStateObj.ValidateSNIConnection())
					{
						if (openTdsConnection.Parser._sessionPool != null && openTdsConnection.Parser._sessionPool.ActiveSessionsCount > 0)
						{
							if (beforeDisconnect != null)
							{
								beforeDisconnect();
							}
							this.OnError(SQL.CR_UnrecoverableClient(this.ClientConnectionId), true, null);
						}
						SessionData currentSessionData = openTdsConnection.CurrentSessionData;
						if (currentSessionData._unrecoverableStatesCount == 0)
						{
							bool flag = false;
							object reconnectLock = this._reconnectLock;
							lock (reconnectLock)
							{
								openTdsConnection.CheckEnlistedTransactionBinding();
								task = this._currentReconnectionTask;
								if (task == null)
								{
									if (currentSessionData._unrecoverableStatesCount == 0)
									{
										this._originalConnectionId = this.ClientConnectionId;
										this._recoverySessionData = currentSessionData;
										if (beforeDisconnect != null)
										{
											beforeDisconnect();
										}
										try
										{
											this._suppressStateChangeForReconnection = true;
											openTdsConnection.DoomThisConnection();
										}
										catch (SqlException)
										{
										}
										task = Task.Run(() => this.ReconnectAsync(timeout));
										this._currentReconnectionTask = task;
									}
								}
								else
								{
									flag = true;
								}
							}
							if (flag && beforeDisconnect != null)
							{
								beforeDisconnect();
							}
						}
						else
						{
							if (beforeDisconnect != null)
							{
								beforeDisconnect();
							}
							this.OnError(SQL.CR_UnrecoverableServer(this.ClientConnectionId), true, null);
						}
					}
				}
			}
			else if (beforeDisconnect != null)
			{
				beforeDisconnect();
			}
			return task;
		}

		// Token: 0x060010BD RID: 4285 RVA: 0x00057188 File Offset: 0x00055388
		private void WaitForPendingReconnection()
		{
			Task currentReconnectionTask = this._currentReconnectionTask;
			if (currentReconnectionTask != null && !currentReconnectionTask.IsCompleted)
			{
				AsyncHelper.WaitForCompletion(currentReconnectionTask, 0, null, false);
			}
		}

		// Token: 0x060010BE RID: 4286 RVA: 0x000571B0 File Offset: 0x000553B0
		private void CancelOpenAndWait()
		{
			Tuple<TaskCompletionSource<DbConnectionInternal>, Task> currentCompletion = this._currentCompletion;
			if (currentCompletion != null)
			{
				currentCompletion.Item1.TrySetCanceled();
				((IAsyncResult)currentCompletion.Item2).AsyncWaitHandle.WaitOne();
			}
		}

		/// <summary>An asynchronous version of <see cref="M:System.Data.SqlClient.SqlConnection.Open" />, which opens a database connection with the property settings specified by the <see cref="P:System.Data.SqlClient.SqlConnection.ConnectionString" />. The cancellation token can be used to request that the operation be abandoned before the connection timeout elapses.  Exceptions will be propagated via the returned Task. If the connection timeout time elapses without successfully connecting, the returned Task will be marked as faulted with an Exception. The implementation returns a Task without blocking the calling thread for both pooled and non-pooled connections.</summary>
		/// <param name="cancellationToken">The cancellation instruction.</param>
		/// <returns>A task representing the asynchronous operation.</returns>
		/// <exception cref="T:System.InvalidOperationException">Calling <see cref="M:System.Data.SqlClient.SqlConnection.OpenAsync(System.Threading.CancellationToken)" /> more than once for the same instance before task completion.
		///         <see langword="Context Connection=true" /> is specified in the connection string.A connection was not available from the connection pool before the connection time out elapsed.</exception>
		/// <exception cref="T:System.Data.SqlClient.SqlException">Any error returned by SQL Server that occurred while opening the connection.</exception>
		// Token: 0x060010BF RID: 4287 RVA: 0x000571E4 File Offset: 0x000553E4
		public override Task OpenAsync(CancellationToken cancellationToken)
		{
			Guid operationId = SqlConnection.s_diagnosticListener.WriteConnectionOpenBefore(this, "OpenAsync");
			this.PrepareStatisticsForNewConnection();
			SqlStatistics statistics = null;
			Task task;
			try
			{
				statistics = SqlStatistics.StartTimer(this.Statistics);
				TaskCompletionSource<DbConnectionInternal> taskCompletionSource = new TaskCompletionSource<DbConnectionInternal>(ADP.GetCurrentTransaction());
				TaskCompletionSource<object> taskCompletionSource2 = new TaskCompletionSource<object>();
				if (SqlConnection.s_diagnosticListener.IsEnabled("System.Data.SqlClient.WriteConnectionOpenAfter") || SqlConnection.s_diagnosticListener.IsEnabled("System.Data.SqlClient.WriteConnectionOpenError"))
				{
					taskCompletionSource2.Task.ContinueWith(delegate(Task<object> t)
					{
						if (t.Exception != null)
						{
							SqlConnection.s_diagnosticListener.WriteConnectionOpenError(operationId, this, t.Exception, "OpenAsync");
							return;
						}
						SqlConnection.s_diagnosticListener.WriteConnectionOpenAfter(operationId, this, "OpenAsync");
					}, TaskScheduler.Default);
				}
				if (cancellationToken.IsCancellationRequested)
				{
					taskCompletionSource2.SetCanceled();
					task = taskCompletionSource2.Task;
				}
				else
				{
					bool flag;
					try
					{
						flag = this.TryOpen(taskCompletionSource);
					}
					catch (Exception ex)
					{
						SqlConnection.s_diagnosticListener.WriteConnectionOpenError(operationId, this, ex, "OpenAsync");
						taskCompletionSource2.SetException(ex);
						return taskCompletionSource2.Task;
					}
					if (flag)
					{
						taskCompletionSource2.SetResult(null);
						task = taskCompletionSource2.Task;
					}
					else
					{
						CancellationTokenRegistration registration = default(CancellationTokenRegistration);
						if (cancellationToken.CanBeCanceled)
						{
							registration = cancellationToken.Register(delegate(object s)
							{
								((TaskCompletionSource<DbConnectionInternal>)s).TrySetCanceled();
							}, taskCompletionSource);
						}
						SqlConnection.OpenAsyncRetry @object = new SqlConnection.OpenAsyncRetry(this, taskCompletionSource, taskCompletionSource2, registration);
						this._currentCompletion = new Tuple<TaskCompletionSource<DbConnectionInternal>, Task>(taskCompletionSource, taskCompletionSource2.Task);
						taskCompletionSource.Task.ContinueWith(new Action<Task<DbConnectionInternal>>(@object.Retry), TaskScheduler.Default);
						task = taskCompletionSource2.Task;
					}
				}
			}
			catch (Exception ex2)
			{
				SqlConnection.s_diagnosticListener.WriteConnectionOpenError(operationId, this, ex2, "OpenAsync");
				throw;
			}
			finally
			{
				SqlStatistics.StopTimer(statistics);
			}
			return task;
		}

		/// <summary>Returns schema information for the data source of this <see cref="T:System.Data.SqlClient.SqlConnection" />. For more information about scheme, see SQL Server Schema Collections.</summary>
		/// <returns>A <see cref="T:System.Data.DataTable" /> that contains schema information.</returns>
		// Token: 0x060010C0 RID: 4288 RVA: 0x000573DC File Offset: 0x000555DC
		public override DataTable GetSchema()
		{
			return this.GetSchema(DbMetaDataCollectionNames.MetaDataCollections, null);
		}

		/// <summary>Returns schema information for the data source of this <see cref="T:System.Data.SqlClient.SqlConnection" /> using the specified string for the schema name.</summary>
		/// <param name="collectionName">Specifies the name of the schema to return.</param>
		/// <returns>A <see cref="T:System.Data.DataTable" /> that contains schema information. </returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="collectionName" /> is specified as null.</exception>
		// Token: 0x060010C1 RID: 4289 RVA: 0x000573EA File Offset: 0x000555EA
		public override DataTable GetSchema(string collectionName)
		{
			return this.GetSchema(collectionName, null);
		}

		/// <summary>Returns schema information for the data source of this <see cref="T:System.Data.SqlClient.SqlConnection" /> using the specified string for the schema name and the specified string array for the restriction values.</summary>
		/// <param name="collectionName">Specifies the name of the schema to return.</param>
		/// <param name="restrictionValues">A set of restriction values for the requested schema.</param>
		/// <returns>A <see cref="T:System.Data.DataTable" /> that contains schema information. </returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="collectionName" /> is specified as null.</exception>
		// Token: 0x060010C2 RID: 4290 RVA: 0x000573F4 File Offset: 0x000555F4
		public override DataTable GetSchema(string collectionName, string[] restrictionValues)
		{
			return this.InnerConnection.GetSchema(this.ConnectionFactory, this.PoolGroup, this, collectionName, restrictionValues);
		}

		// Token: 0x060010C3 RID: 4291 RVA: 0x00057410 File Offset: 0x00055610
		private void PrepareStatisticsForNewConnection()
		{
			if (this.StatisticsEnabled || SqlConnection.s_diagnosticListener.IsEnabled("System.Data.SqlClient.WriteCommandAfter") || SqlConnection.s_diagnosticListener.IsEnabled("System.Data.SqlClient.WriteConnectionOpenAfter"))
			{
				if (this._statistics == null)
				{
					this._statistics = new SqlStatistics();
					return;
				}
				this._statistics.ContinueOnNewConnection();
			}
		}

		// Token: 0x060010C4 RID: 4292 RVA: 0x00057468 File Offset: 0x00055668
		private bool TryOpen(TaskCompletionSource<DbConnectionInternal> retry)
		{
			SqlConnectionString sqlConnectionString = (SqlConnectionString)this.ConnectionOptions;
			this._applyTransientFaultHandling = (retry == null && sqlConnectionString != null && sqlConnectionString.ConnectRetryCount > 0);
			if (this.ForceNewConnection)
			{
				if (!this.InnerConnection.TryReplaceConnection(this, this.ConnectionFactory, retry, this.UserConnectionOptions))
				{
					return false;
				}
			}
			else if (!this.InnerConnection.TryOpenConnection(this, this.ConnectionFactory, retry, this.UserConnectionOptions))
			{
				return false;
			}
			SqlInternalConnectionTds sqlInternalConnectionTds = (SqlInternalConnectionTds)this.InnerConnection;
			if (!sqlInternalConnectionTds.ConnectionOptions.Pooling)
			{
				GC.ReRegisterForFinalize(this);
			}
			SqlStatistics statistics = this._statistics;
			if (this.StatisticsEnabled || (SqlConnection.s_diagnosticListener.IsEnabled("System.Data.SqlClient.WriteCommandAfter") && statistics != null))
			{
				ADP.TimerCurrent(out this._statistics._openTimestamp);
				sqlInternalConnectionTds.Parser.Statistics = this._statistics;
			}
			else
			{
				sqlInternalConnectionTds.Parser.Statistics = null;
				this._statistics = null;
			}
			return true;
		}

		// Token: 0x170002EC RID: 748
		// (get) Token: 0x060010C5 RID: 4293 RVA: 0x00057554 File Offset: 0x00055754
		internal bool HasLocalTransaction
		{
			get
			{
				return this.GetOpenTdsConnection().HasLocalTransaction;
			}
		}

		// Token: 0x170002ED RID: 749
		// (get) Token: 0x060010C6 RID: 4294 RVA: 0x00057564 File Offset: 0x00055764
		internal bool HasLocalTransactionFromAPI
		{
			get
			{
				Task currentReconnectionTask = this._currentReconnectionTask;
				return (currentReconnectionTask == null || currentReconnectionTask.IsCompleted) && this.GetOpenTdsConnection().HasLocalTransactionFromAPI;
			}
		}

		// Token: 0x170002EE RID: 750
		// (get) Token: 0x060010C7 RID: 4295 RVA: 0x00057590 File Offset: 0x00055790
		internal bool IsKatmaiOrNewer
		{
			get
			{
				return this._currentReconnectionTask != null || this.GetOpenTdsConnection().IsKatmaiOrNewer;
			}
		}

		// Token: 0x170002EF RID: 751
		// (get) Token: 0x060010C8 RID: 4296 RVA: 0x000575A7 File Offset: 0x000557A7
		internal TdsParser Parser
		{
			get
			{
				return this.GetOpenTdsConnection().Parser;
			}
		}

		// Token: 0x060010C9 RID: 4297 RVA: 0x000575B4 File Offset: 0x000557B4
		internal void ValidateConnectionForExecute(string method, SqlCommand command)
		{
			Task asyncWaitingForReconnection = this._asyncWaitingForReconnection;
			if (asyncWaitingForReconnection != null)
			{
				if (!asyncWaitingForReconnection.IsCompleted)
				{
					throw SQL.MARSUnspportedOnConnection();
				}
				Interlocked.CompareExchange<Task>(ref this._asyncWaitingForReconnection, null, asyncWaitingForReconnection);
			}
			if (this._currentReconnectionTask != null)
			{
				Task currentReconnectionTask = this._currentReconnectionTask;
				if (currentReconnectionTask != null && !currentReconnectionTask.IsCompleted)
				{
					return;
				}
			}
			this.GetOpenTdsConnection(method).ValidateConnectionForExecute(command);
		}

		// Token: 0x060010CA RID: 4298 RVA: 0x0005760F File Offset: 0x0005580F
		internal static string FixupDatabaseTransactionName(string name)
		{
			if (!string.IsNullOrEmpty(name))
			{
				return SqlServerEscapeHelper.EscapeIdentifier(name);
			}
			return name;
		}

		// Token: 0x060010CB RID: 4299 RVA: 0x00057624 File Offset: 0x00055824
		internal void OnError(SqlException exception, bool breakConnection, Action<Action> wrapCloseInAction)
		{
			if (breakConnection && ConnectionState.Open == this.State)
			{
				if (wrapCloseInAction != null)
				{
					int capturedCloseCount = this._closeCount;
					Action obj = delegate()
					{
						if (capturedCloseCount == this._closeCount)
						{
							this.Close();
						}
					};
					wrapCloseInAction(obj);
				}
				else
				{
					this.Close();
				}
			}
			if (exception.Class >= 11)
			{
				throw exception;
			}
			this.OnInfoMessage(new SqlInfoMessageEventArgs(exception));
		}

		// Token: 0x060010CC RID: 4300 RVA: 0x0005768C File Offset: 0x0005588C
		internal SqlInternalConnectionTds GetOpenTdsConnection()
		{
			SqlInternalConnectionTds sqlInternalConnectionTds = this.InnerConnection as SqlInternalConnectionTds;
			if (sqlInternalConnectionTds == null)
			{
				throw ADP.ClosedConnectionError();
			}
			return sqlInternalConnectionTds;
		}

		// Token: 0x060010CD RID: 4301 RVA: 0x000576B0 File Offset: 0x000558B0
		internal SqlInternalConnectionTds GetOpenTdsConnection(string method)
		{
			SqlInternalConnectionTds sqlInternalConnectionTds = this.InnerConnection as SqlInternalConnectionTds;
			if (sqlInternalConnectionTds == null)
			{
				throw ADP.OpenConnectionRequired(method, this.InnerConnection.State);
			}
			return sqlInternalConnectionTds;
		}

		// Token: 0x060010CE RID: 4302 RVA: 0x000576E0 File Offset: 0x000558E0
		internal void OnInfoMessage(SqlInfoMessageEventArgs imevent)
		{
			bool flag;
			this.OnInfoMessage(imevent, out flag);
		}

		// Token: 0x060010CF RID: 4303 RVA: 0x000576F8 File Offset: 0x000558F8
		internal void OnInfoMessage(SqlInfoMessageEventArgs imevent, out bool notified)
		{
			SqlInfoMessageEventHandler infoMessage = this.InfoMessage;
			if (infoMessage != null)
			{
				notified = true;
				try
				{
					infoMessage(this, imevent);
					return;
				}
				catch (Exception e)
				{
					if (!ADP.IsCatchableOrSecurityExceptionType(e))
					{
						throw;
					}
					return;
				}
			}
			notified = false;
		}

		// Token: 0x060010D0 RID: 4304 RVA: 0x0005773C File Offset: 0x0005593C
		internal void RegisterForConnectionCloseNotification<T>(ref Task<T> outerTask, object value, int tag)
		{
			outerTask = outerTask.ContinueWith<Task<T>>(delegate(Task<T> task)
			{
				this.RemoveWeakReference(value);
				return task;
			}, TaskScheduler.Default).Unwrap<T>();
		}

		/// <summary>If statistics gathering is enabled, all values are reset to zero.</summary>
		// Token: 0x060010D1 RID: 4305 RVA: 0x0005777C File Offset: 0x0005597C
		public void ResetStatistics()
		{
			if (this.Statistics != null)
			{
				this.Statistics.Reset();
				if (ConnectionState.Open == this.State)
				{
					ADP.TimerCurrent(out this._statistics._openTimestamp);
				}
			}
		}

		/// <summary>Returns a name value pair collection of statistics at the point in time the method is called.</summary>
		/// <returns>Returns a reference of type <see cref="T:System.Collections.IDictionary" /> of <see cref="T:System.Collections.DictionaryEntry" /> items.</returns>
		// Token: 0x060010D2 RID: 4306 RVA: 0x000577AA File Offset: 0x000559AA
		public IDictionary RetrieveStatistics()
		{
			if (this.Statistics != null)
			{
				this.UpdateStatistics();
				return this.Statistics.GetDictionary();
			}
			return new SqlStatistics().GetDictionary();
		}

		// Token: 0x060010D3 RID: 4307 RVA: 0x000577D0 File Offset: 0x000559D0
		private void UpdateStatistics()
		{
			if (ConnectionState.Open == this.State)
			{
				ADP.TimerCurrent(out this._statistics._closeTimestamp);
			}
			this.Statistics.UpdateStatistics();
		}

		/// <summary>Creates a new object that is a copy of the current instance.</summary>
		/// <returns>A new object that is a copy of this instance.</returns>
		// Token: 0x060010D4 RID: 4308 RVA: 0x000577F6 File Offset: 0x000559F6
		object ICloneable.Clone()
		{
			return new SqlConnection(this);
		}

		// Token: 0x060010D5 RID: 4309 RVA: 0x00057800 File Offset: 0x00055A00
		private void CopyFrom(SqlConnection connection)
		{
			ADP.CheckArgumentNull(connection, "connection");
			this._userConnectionOptions = connection.UserConnectionOptions;
			this._poolGroup = connection.PoolGroup;
			if (DbConnectionClosedNeverOpened.SingletonInstance == connection._innerConnection)
			{
				this._innerConnection = DbConnectionClosedNeverOpened.SingletonInstance;
				return;
			}
			this._innerConnection = DbConnectionClosedPreviouslyOpened.SingletonInstance;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Data.SqlClient.SqlConnection" /> class.</summary>
		// Token: 0x060010D6 RID: 4310 RVA: 0x00057854 File Offset: 0x00055A54
		public SqlConnection()
		{
			this._reconnectLock = new object();
			this._originalConnectionId = Guid.Empty;
			base..ctor();
			GC.SuppressFinalize(this);
			this._innerConnection = DbConnectionClosedNeverOpened.SingletonInstance;
		}

		// Token: 0x170002F0 RID: 752
		// (get) Token: 0x060010D7 RID: 4311 RVA: 0x00057883 File Offset: 0x00055A83
		internal int CloseCount
		{
			get
			{
				return this._closeCount;
			}
		}

		// Token: 0x170002F1 RID: 753
		// (get) Token: 0x060010D8 RID: 4312 RVA: 0x0005788B File Offset: 0x00055A8B
		internal DbConnectionFactory ConnectionFactory
		{
			get
			{
				return SqlConnection.s_connectionFactory;
			}
		}

		// Token: 0x170002F2 RID: 754
		// (get) Token: 0x060010D9 RID: 4313 RVA: 0x00057894 File Offset: 0x00055A94
		internal DbConnectionOptions ConnectionOptions
		{
			get
			{
				DbConnectionPoolGroup poolGroup = this.PoolGroup;
				if (poolGroup == null)
				{
					return null;
				}
				return poolGroup.ConnectionOptions;
			}
		}

		// Token: 0x060010DA RID: 4314 RVA: 0x000578B4 File Offset: 0x00055AB4
		private string ConnectionString_Get()
		{
			bool shouldHidePassword = this.InnerConnection.ShouldHidePassword;
			DbConnectionOptions userConnectionOptions = this.UserConnectionOptions;
			if (userConnectionOptions == null)
			{
				return "";
			}
			return userConnectionOptions.UsersConnectionString(shouldHidePassword);
		}

		// Token: 0x060010DB RID: 4315 RVA: 0x000578E4 File Offset: 0x00055AE4
		private void ConnectionString_Set(DbConnectionPoolKey key)
		{
			DbConnectionOptions userConnectionOptions = null;
			DbConnectionPoolGroup connectionPoolGroup = this.ConnectionFactory.GetConnectionPoolGroup(key, null, ref userConnectionOptions);
			DbConnectionInternal innerConnection = this.InnerConnection;
			bool flag = innerConnection.AllowSetConnectionString;
			if (flag)
			{
				flag = this.SetInnerConnectionFrom(DbConnectionClosedBusy.SingletonInstance, innerConnection);
				if (flag)
				{
					this._userConnectionOptions = userConnectionOptions;
					this._poolGroup = connectionPoolGroup;
					this._innerConnection = DbConnectionClosedNeverOpened.SingletonInstance;
				}
			}
			if (!flag)
			{
				throw ADP.OpenConnectionPropertySet("ConnectionString", innerConnection.State);
			}
		}

		// Token: 0x170002F3 RID: 755
		// (get) Token: 0x060010DC RID: 4316 RVA: 0x00057951 File Offset: 0x00055B51
		internal DbConnectionInternal InnerConnection
		{
			get
			{
				return this._innerConnection;
			}
		}

		// Token: 0x170002F4 RID: 756
		// (get) Token: 0x060010DD RID: 4317 RVA: 0x00057959 File Offset: 0x00055B59
		// (set) Token: 0x060010DE RID: 4318 RVA: 0x00057961 File Offset: 0x00055B61
		internal DbConnectionPoolGroup PoolGroup
		{
			get
			{
				return this._poolGroup;
			}
			set
			{
				this._poolGroup = value;
			}
		}

		// Token: 0x170002F5 RID: 757
		// (get) Token: 0x060010DF RID: 4319 RVA: 0x0005796A File Offset: 0x00055B6A
		internal DbConnectionOptions UserConnectionOptions
		{
			get
			{
				return this._userConnectionOptions;
			}
		}

		// Token: 0x060010E0 RID: 4320 RVA: 0x00057974 File Offset: 0x00055B74
		internal void Abort(Exception e)
		{
			DbConnectionInternal innerConnection = this._innerConnection;
			if (ConnectionState.Open == innerConnection.State)
			{
				Interlocked.CompareExchange<DbConnectionInternal>(ref this._innerConnection, DbConnectionClosedPreviouslyOpened.SingletonInstance, innerConnection);
				innerConnection.DoomThisConnection();
			}
		}

		// Token: 0x060010E1 RID: 4321 RVA: 0x000579A9 File Offset: 0x00055BA9
		internal void AddWeakReference(object value, int tag)
		{
			this.InnerConnection.AddWeakReference(value, tag);
		}

		// Token: 0x060010E2 RID: 4322 RVA: 0x000579B8 File Offset: 0x00055BB8
		protected override DbCommand CreateDbCommand()
		{
			DbCommand dbCommand = this.ConnectionFactory.ProviderFactory.CreateCommand();
			dbCommand.Connection = this;
			return dbCommand;
		}

		// Token: 0x060010E3 RID: 4323 RVA: 0x000579D1 File Offset: 0x00055BD1
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				this._userConnectionOptions = null;
				this._poolGroup = null;
				this.Close();
			}
			this.DisposeMe(disposing);
			base.Dispose(disposing);
		}

		// Token: 0x060010E4 RID: 4324 RVA: 0x000579F8 File Offset: 0x00055BF8
		private void RepairInnerConnection()
		{
			this.WaitForPendingReconnection();
			if (this._connectRetryCount == 0)
			{
				return;
			}
			SqlInternalConnectionTds sqlInternalConnectionTds = this.InnerConnection as SqlInternalConnectionTds;
			if (sqlInternalConnectionTds != null)
			{
				sqlInternalConnectionTds.ValidateConnectionForExecute(null);
				sqlInternalConnectionTds.GetSessionAndReconnectIfNeeded(this, 0);
			}
		}

		/// <summary>Enlists in the specified transaction as a distributed transaction.</summary>
		/// <param name="transaction">A reference to an existing <see cref="T:System.Transactions.Transaction" /> in which to enlist.</param>
		// Token: 0x060010E5 RID: 4325 RVA: 0x00057A34 File Offset: 0x00055C34
		public override void EnlistTransaction(Transaction transaction)
		{
			Transaction enlistedTransaction = this.InnerConnection.EnlistedTransaction;
			if (enlistedTransaction != null)
			{
				if (enlistedTransaction.Equals(transaction))
				{
					return;
				}
				if (enlistedTransaction.TransactionInformation.Status == System.Transactions.TransactionStatus.Active)
				{
					throw ADP.TransactionPresent();
				}
			}
			this.RepairInnerConnection();
			this.InnerConnection.EnlistTransaction(transaction);
			GC.KeepAlive(this);
		}

		// Token: 0x060010E6 RID: 4326 RVA: 0x00057A8B File Offset: 0x00055C8B
		internal void NotifyWeakReference(int message)
		{
			this.InnerConnection.NotifyWeakReference(message);
		}

		// Token: 0x060010E7 RID: 4327 RVA: 0x00057A9C File Offset: 0x00055C9C
		internal void PermissionDemand()
		{
			DbConnectionPoolGroup poolGroup = this.PoolGroup;
			DbConnectionOptions dbConnectionOptions = (poolGroup != null) ? poolGroup.ConnectionOptions : null;
			if (dbConnectionOptions == null || dbConnectionOptions.IsEmpty)
			{
				throw ADP.NoConnectionString();
			}
			DbConnectionOptions userConnectionOptions = this.UserConnectionOptions;
		}

		// Token: 0x060010E8 RID: 4328 RVA: 0x00057AD5 File Offset: 0x00055CD5
		internal void RemoveWeakReference(object value)
		{
			this.InnerConnection.RemoveWeakReference(value);
		}

		// Token: 0x060010E9 RID: 4329 RVA: 0x00057AE4 File Offset: 0x00055CE4
		internal void SetInnerConnectionEvent(DbConnectionInternal to)
		{
			ConnectionState connectionState = this._innerConnection.State & ConnectionState.Open;
			ConnectionState connectionState2 = to.State & ConnectionState.Open;
			if (connectionState != connectionState2 && connectionState2 == ConnectionState.Closed)
			{
				this._closeCount++;
			}
			this._innerConnection = to;
			if (connectionState == ConnectionState.Closed && ConnectionState.Open == connectionState2)
			{
				this.OnStateChange(DbConnectionInternal.StateChangeOpen);
				return;
			}
			if (ConnectionState.Open == connectionState && connectionState2 == ConnectionState.Closed)
			{
				this.OnStateChange(DbConnectionInternal.StateChangeClosed);
				return;
			}
			if (connectionState != connectionState2)
			{
				this.OnStateChange(new StateChangeEventArgs(connectionState, connectionState2));
			}
		}

		// Token: 0x060010EA RID: 4330 RVA: 0x00057B5B File Offset: 0x00055D5B
		internal bool SetInnerConnectionFrom(DbConnectionInternal to, DbConnectionInternal from)
		{
			return from == Interlocked.CompareExchange<DbConnectionInternal>(ref this._innerConnection, to, from);
		}

		// Token: 0x060010EB RID: 4331 RVA: 0x00057B6D File Offset: 0x00055D6D
		internal void SetInnerConnectionTo(DbConnectionInternal to)
		{
			this._innerConnection = to;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Data.SqlClient.SqlConnection" /> class given a connection string, that does not use <see langword="Integrated Security = true" /> and a <see cref="T:System.Data.SqlClient.SqlCredential" /> object that contains the user ID and password.</summary>
		/// <param name="connectionString">A connection string that does not use any of the following connection string keywords: <see langword="Integrated Security = true" />, <see langword="UserId" />, or <see langword="Password" />; or that does not use <see langword="ContextConnection = true" />.</param>
		/// <param name="credential">A <see cref="T:System.Data.SqlClient.SqlCredential" /> object. If <paramref name="credential" /> is null, <see cref="M:System.Data.SqlClient.SqlConnection.#ctor(System.String,System.Data.SqlClient.SqlCredential)" /> is functionally equivalent to <see cref="M:System.Data.SqlClient.SqlConnection.#ctor(System.String)" />.</param>
		// Token: 0x060010EC RID: 4332 RVA: 0x00057B76 File Offset: 0x00055D76
		public SqlConnection(string connectionString, SqlCredential credential)
		{
			this._reconnectLock = new object();
			this._originalConnectionId = Guid.Empty;
			base..ctor();
			this.ConnectionString = connectionString;
			this.Credentials = credential;
		}

		/// <summary>Changes the SQL Server password for the user indicated in the connection string to the supplied new password.</summary>
		/// <param name="connectionString">The connection string that contains enough information to connect to the server that you want. The connection string must contain the user ID and the current password.</param>
		/// <param name="newPassword">The new password to set. This password must comply with any password security policy set on the server, including minimum length, requirements for specific characters, and so on.</param>
		/// <exception cref="T:System.ArgumentException">The connection string includes the option to use integrated security. Or The <paramref name="newPassword" /> exceeds 128 characters.</exception>
		/// <exception cref="T:System.ArgumentNullException">Either the <paramref name="connectionString" /> or the <paramref name="newPassword" /> parameter is null.</exception>
		// Token: 0x060010ED RID: 4333 RVA: 0x000554AE File Offset: 0x000536AE
		[MonoTODO]
		public static void ChangePassword(string connectionString, string newPassword)
		{
			throw new NotImplementedException();
		}

		// Token: 0x170002F6 RID: 758
		// (get) Token: 0x060010EE RID: 4334 RVA: 0x000554AE File Offset: 0x000536AE
		// (set) Token: 0x060010EF RID: 4335 RVA: 0x000554AE File Offset: 0x000536AE
		[MonoTODO]
		public SqlCredential Credentials
		{
			get
			{
				throw new NotImplementedException();
			}
			set
			{
				throw new NotImplementedException();
			}
		}

		// Token: 0x060010F0 RID: 4336 RVA: 0x00057BA2 File Offset: 0x00055DA2
		// Note: this type is marked as 'beforefieldinit'.
		static SqlConnection()
		{
		}

		/// <summary>Gets or sets the access token for the connection.</summary>
		/// <returns>The access token for the connection.</returns>
		// Token: 0x170002F7 RID: 759
		// (get) Token: 0x060010F1 RID: 4337 RVA: 0x00051759 File Offset: 0x0004F959
		// (set) Token: 0x060010F2 RID: 4338 RVA: 0x00010458 File Offset: 0x0000E658
		public string AccessToken
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the time-to-live for column encryption key entries in the column encryption key cache for the Always Encrypted feature. The default value is 2 hours. 0 means no caching at all.</summary>
		/// <returns>The time interval.</returns>
		// Token: 0x170002F8 RID: 760
		// (get) Token: 0x060010F3 RID: 4339 RVA: 0x00057BC0 File Offset: 0x00055DC0
		// (set) Token: 0x060010F4 RID: 4340 RVA: 0x00010458 File Offset: 0x0000E658
		public static TimeSpan ColumnEncryptionKeyCacheTtl
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(TimeSpan);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets a value that indicates whether query metadata caching is enabled (true) or not (false) for parameterized queries running against Always Encrypted enabled databases. The default value is true.</summary>
		/// <returns>Returns true if query metadata caching is enabled; otherwise false. true is the default.</returns>
		// Token: 0x170002F9 RID: 761
		// (get) Token: 0x060010F5 RID: 4341 RVA: 0x00057BDC File Offset: 0x00055DDC
		// (set) Token: 0x060010F6 RID: 4342 RVA: 0x00010458 File Offset: 0x0000E658
		public static bool ColumnEncryptionQueryMetadataCacheEnabled
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Allows you to set a list of trusted key paths for a database server. If while processing an application query the driver receives a key path that is not on the list, the query will fail. This property provides additional protection against security attacks that involve a compromised SQL Server providing fake key paths, which may lead to leaking key store credentials.</summary>
		/// <returns>The list of trusted master key paths for the column encryption.</returns>
		// Token: 0x170002FA RID: 762
		// (get) Token: 0x060010F7 RID: 4343 RVA: 0x00057BF7 File Offset: 0x00055DF7
		public static IDictionary<string, IList<string>> ColumnEncryptionTrustedMasterKeyPaths
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
		}

		/// <summary>Gets or sets the <see cref="T:System.Data.SqlClient.SqlCredential" /> object for this connection.</summary>
		/// <returns>The <see cref="T:System.Data.SqlClient.SqlCredential" /> object for this connection.</returns>
		// Token: 0x170002FB RID: 763
		// (get) Token: 0x060010F8 RID: 4344 RVA: 0x00051759 File Offset: 0x0004F959
		// (set) Token: 0x060010F9 RID: 4345 RVA: 0x00010458 File Offset: 0x0000E658
		public SqlCredential Credential
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Changes the SQL Server password for the user indicated in the <see cref="T:System.Data.SqlClient.SqlCredential" /> object.</summary>
		/// <param name="connectionString">The connection string that contains enough information to connect to a server. The connection string should not use any of the following connection string keywords: <see langword="Integrated Security = true" />, <see langword="UserId" />, or <see langword="Password" />; or <see langword="ContextConnection = true" />.</param>
		/// <param name="credential">A <see cref="T:System.Data.SqlClient.SqlCredential" /> object.</param>
		/// <param name="newSecurePassword">The new password. <paramref name="newSecurePassword" /> must be read only. The password must also comply with any password security policy set on the server (for example, minimum length and requirements for specific characters).</param>
		/// <exception cref="T:System.ArgumentException">The connection string contains any combination of <see langword="UserId" />, <see langword="Password" />, or <see langword="Integrated Security=true" />.The connection string contains <see langword="Context Connection=true" />.
		///             <paramref name="newSecurePassword" /> is greater than 128 characters.
		///             <paramref name="newSecurePassword" /> is not read only.
		///             <paramref name="newSecurePassword" /> is an empty string.</exception>
		/// <exception cref="T:System.ArgumentNullException">One of the parameters (<paramref name="connectionString" />, <paramref name="credential" />, or <paramref name="newSecurePassword" />) is null.</exception>
		// Token: 0x060010FA RID: 4346 RVA: 0x00010458 File Offset: 0x0000E658
		public static void ChangePassword(string connectionString, SqlCredential credential, SecureString newSecurePassword)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Enlists in the specified transaction as a distributed transaction.</summary>
		/// <param name="transaction">A reference to an existing <see cref="T:System.EnterpriseServices.ITransaction" /> in which to enlist.</param>
		// Token: 0x060010FB RID: 4347 RVA: 0x00010458 File Offset: 0x0000E658
		public void EnlistDistributedTransaction(ITransaction transaction)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Registers the column encryption key store providers.</summary>
		/// <param name="customProviders">The custom providers</param>
		// Token: 0x060010FC RID: 4348 RVA: 0x00010458 File Offset: 0x0000E658
		public static void RegisterColumnEncryptionKeyStoreProviders(IDictionary<string, SqlColumnEncryptionKeyStoreProvider> customProviders)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x04000BA6 RID: 2982
		private bool _AsyncCommandInProgress;

		// Token: 0x04000BA7 RID: 2983
		internal SqlStatistics _statistics;

		// Token: 0x04000BA8 RID: 2984
		private bool _collectstats;

		// Token: 0x04000BA9 RID: 2985
		private bool _fireInfoMessageEventOnUserErrors;

		// Token: 0x04000BAA RID: 2986
		private Tuple<TaskCompletionSource<DbConnectionInternal>, Task> _currentCompletion;

		// Token: 0x04000BAB RID: 2987
		private string _connectionString;

		// Token: 0x04000BAC RID: 2988
		private int _connectRetryCount;

		// Token: 0x04000BAD RID: 2989
		private object _reconnectLock;

		// Token: 0x04000BAE RID: 2990
		internal Task _currentReconnectionTask;

		// Token: 0x04000BAF RID: 2991
		private Task _asyncWaitingForReconnection;

		// Token: 0x04000BB0 RID: 2992
		private Guid _originalConnectionId;

		// Token: 0x04000BB1 RID: 2993
		private CancellationTokenSource _reconnectionCancellationSource;

		// Token: 0x04000BB2 RID: 2994
		internal SessionData _recoverySessionData;

		// Token: 0x04000BB3 RID: 2995
		internal new bool _suppressStateChangeForReconnection;

		// Token: 0x04000BB4 RID: 2996
		private int _reconnectCount;

		// Token: 0x04000BB5 RID: 2997
		private static readonly DiagnosticListener s_diagnosticListener = new DiagnosticListener("SqlClientDiagnosticListener");

		// Token: 0x04000BB6 RID: 2998
		internal bool _applyTransientFaultHandling;

		// Token: 0x04000BB7 RID: 2999
		[CompilerGenerated]
		private SqlInfoMessageEventHandler InfoMessage;

		// Token: 0x04000BB8 RID: 3000
		[CompilerGenerated]
		private bool <ForceNewConnection>k__BackingField;

		// Token: 0x04000BB9 RID: 3001
		private static readonly DbConnectionFactory s_connectionFactory = SqlConnectionFactory.SingletonInstance;

		// Token: 0x04000BBA RID: 3002
		private DbConnectionOptions _userConnectionOptions;

		// Token: 0x04000BBB RID: 3003
		private DbConnectionPoolGroup _poolGroup;

		// Token: 0x04000BBC RID: 3004
		private DbConnectionInternal _innerConnection;

		// Token: 0x04000BBD RID: 3005
		private int _closeCount;

		// Token: 0x02000169 RID: 361
		private class OpenAsyncRetry
		{
			// Token: 0x060010FD RID: 4349 RVA: 0x00057BFF File Offset: 0x00055DFF
			public OpenAsyncRetry(SqlConnection parent, TaskCompletionSource<DbConnectionInternal> retry, TaskCompletionSource<object> result, CancellationTokenRegistration registration)
			{
				this._parent = parent;
				this._retry = retry;
				this._result = result;
				this._registration = registration;
			}

			// Token: 0x060010FE RID: 4350 RVA: 0x00057C24 File Offset: 0x00055E24
			internal void Retry(Task<DbConnectionInternal> retryTask)
			{
				this._registration.Dispose();
				try
				{
					SqlStatistics statistics = null;
					try
					{
						statistics = SqlStatistics.StartTimer(this._parent.Statistics);
						if (retryTask.IsFaulted)
						{
							Exception innerException = retryTask.Exception.InnerException;
							this._parent.CloseInnerConnection();
							this._parent._currentCompletion = null;
							this._result.SetException(retryTask.Exception.InnerException);
						}
						else if (retryTask.IsCanceled)
						{
							this._parent.CloseInnerConnection();
							this._parent._currentCompletion = null;
							this._result.SetCanceled();
						}
						else
						{
							DbConnectionInternal innerConnection = this._parent.InnerConnection;
							bool flag2;
							lock (innerConnection)
							{
								flag2 = this._parent.TryOpen(this._retry);
							}
							if (flag2)
							{
								this._parent._currentCompletion = null;
								this._result.SetResult(null);
							}
							else
							{
								this._parent.CloseInnerConnection();
								this._parent._currentCompletion = null;
								this._result.SetException(ADP.ExceptionWithStackTrace(ADP.InternalError(ADP.InternalErrorCode.CompletedConnectReturnedPending)));
							}
						}
					}
					finally
					{
						SqlStatistics.StopTimer(statistics);
					}
				}
				catch (Exception exception)
				{
					this._parent.CloseInnerConnection();
					this._parent._currentCompletion = null;
					this._result.SetException(exception);
				}
			}

			// Token: 0x04000BBE RID: 3006
			private SqlConnection _parent;

			// Token: 0x04000BBF RID: 3007
			private TaskCompletionSource<DbConnectionInternal> _retry;

			// Token: 0x04000BC0 RID: 3008
			private TaskCompletionSource<object> _result;

			// Token: 0x04000BC1 RID: 3009
			private CancellationTokenRegistration _registration;
		}

		// Token: 0x0200016A RID: 362
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ReconnectAsync>d__82 : IAsyncStateMachine
		{
			// Token: 0x060010FF RID: 4351 RVA: 0x00057DC0 File Offset: 0x00055FC0
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				SqlConnection sqlConnection = this;
				try
				{
					try
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
						if (num != 0)
						{
							if (num != 1)
							{
								commandTimeoutExpiration = 0L;
								if (timeout > 0)
								{
									commandTimeoutExpiration = ADP.TimerCurrent() + ADP.TimerFromSeconds(timeout);
								}
								CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();
								sqlConnection._reconnectionCancellationSource = cancellationTokenSource;
								ctoken = cancellationTokenSource.Token;
								retryCount = sqlConnection._connectRetryCount;
								attempt = 0;
								goto IL_1FB;
							}
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num = (num2 = -1);
							goto IL_1E2;
						}
						IL_88:
						try
						{
							try
							{
								if (num != 0)
								{
									sqlConnection.ForceNewConnection = true;
									configuredTaskAwaiter = sqlConnection.OpenAsync(ctoken).ConfigureAwait(false).GetAwaiter();
									if (!configuredTaskAwaiter.IsCompleted)
									{
										num = (num2 = 0);
										configuredTaskAwaiter2 = configuredTaskAwaiter;
										this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, SqlConnection.<ReconnectAsync>d__82>(ref configuredTaskAwaiter, ref this);
										return;
									}
								}
								else
								{
									configuredTaskAwaiter = configuredTaskAwaiter2;
									configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
									num = (num2 = -1);
								}
								configuredTaskAwaiter.GetResult();
								sqlConnection._reconnectCount++;
							}
							finally
							{
								if (num < 0)
								{
									sqlConnection.ForceNewConnection = false;
								}
							}
							goto IL_248;
						}
						catch (SqlException innerException)
						{
							if (attempt == retryCount - 1)
							{
								throw SQL.CR_AllAttemptsFailed(innerException, sqlConnection._originalConnectionId);
							}
							if (timeout > 0 && ADP.TimerRemaining(commandTimeoutExpiration) < ADP.TimerFromSeconds(sqlConnection.ConnectRetryInterval))
							{
								throw SQL.CR_NextAttemptWillExceedQueryTimeout(innerException, sqlConnection._originalConnectionId);
							}
						}
						configuredTaskAwaiter = Task.Delay(1000 * sqlConnection.ConnectRetryInterval, ctoken).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num = (num2 = 1);
							configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, SqlConnection.<ReconnectAsync>d__82>(ref configuredTaskAwaiter, ref this);
							return;
						}
						IL_1E2:
						configuredTaskAwaiter.GetResult();
						int num3 = attempt;
						attempt = num3 + 1;
						IL_1FB:
						if (attempt >= retryCount)
						{
							ctoken = default(CancellationToken);
						}
						else if (!ctoken.IsCancellationRequested)
						{
							goto IL_88;
						}
					}
					finally
					{
						if (num < 0)
						{
							sqlConnection._recoverySessionData = null;
							sqlConnection._suppressStateChangeForReconnection = false;
						}
					}
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_248:
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06001100 RID: 4352 RVA: 0x0005808C File Offset: 0x0005628C
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000BC2 RID: 3010
			public int <>1__state;

			// Token: 0x04000BC3 RID: 3011
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x04000BC4 RID: 3012
			public int timeout;

			// Token: 0x04000BC5 RID: 3013
			public SqlConnection <>4__this;

			// Token: 0x04000BC6 RID: 3014
			private CancellationToken <ctoken>5__1;

			// Token: 0x04000BC7 RID: 3015
			private int <attempt>5__2;

			// Token: 0x04000BC8 RID: 3016
			private int <retryCount>5__3;

			// Token: 0x04000BC9 RID: 3017
			private long <commandTimeoutExpiration>5__4;

			// Token: 0x04000BCA RID: 3018
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x0200016B RID: 363
		[CompilerGenerated]
		private sealed class <>c__DisplayClass83_0
		{
			// Token: 0x06001101 RID: 4353 RVA: 0x00005C14 File Offset: 0x00003E14
			public <>c__DisplayClass83_0()
			{
			}

			// Token: 0x06001102 RID: 4354 RVA: 0x0005809A File Offset: 0x0005629A
			internal Task <ValidateAndReconnect>b__0()
			{
				return this.<>4__this.ReconnectAsync(this.timeout);
			}

			// Token: 0x04000BCB RID: 3019
			public SqlConnection <>4__this;

			// Token: 0x04000BCC RID: 3020
			public int timeout;
		}

		// Token: 0x0200016C RID: 364
		[CompilerGenerated]
		private sealed class <>c__DisplayClass86_0
		{
			// Token: 0x06001103 RID: 4355 RVA: 0x00005C14 File Offset: 0x00003E14
			public <>c__DisplayClass86_0()
			{
			}

			// Token: 0x06001104 RID: 4356 RVA: 0x000580B0 File Offset: 0x000562B0
			internal void <OpenAsync>b__0(Task<object> t)
			{
				if (t.Exception != null)
				{
					SqlConnection.s_diagnosticListener.WriteConnectionOpenError(this.operationId, this.<>4__this, t.Exception, "OpenAsync");
					return;
				}
				SqlConnection.s_diagnosticListener.WriteConnectionOpenAfter(this.operationId, this.<>4__this, "OpenAsync");
			}

			// Token: 0x04000BCD RID: 3021
			public Guid operationId;

			// Token: 0x04000BCE RID: 3022
			public SqlConnection <>4__this;
		}

		// Token: 0x0200016D RID: 365
		[CompilerGenerated]
		[Serializable]
		private sealed class <>c
		{
			// Token: 0x06001105 RID: 4357 RVA: 0x00058102 File Offset: 0x00056302
			// Note: this type is marked as 'beforefieldinit'.
			static <>c()
			{
			}

			// Token: 0x06001106 RID: 4358 RVA: 0x00005C14 File Offset: 0x00003E14
			public <>c()
			{
			}

			// Token: 0x06001107 RID: 4359 RVA: 0x0005810E File Offset: 0x0005630E
			internal void <OpenAsync>b__86_1(object s)
			{
				((TaskCompletionSource<DbConnectionInternal>)s).TrySetCanceled();
			}

			// Token: 0x04000BCF RID: 3023
			public static readonly SqlConnection.<>c <>9 = new SqlConnection.<>c();

			// Token: 0x04000BD0 RID: 3024
			public static Action<object> <>9__86_1;
		}

		// Token: 0x0200016E RID: 366
		[CompilerGenerated]
		private sealed class <>c__DisplayClass103_0
		{
			// Token: 0x06001108 RID: 4360 RVA: 0x00005C14 File Offset: 0x00003E14
			public <>c__DisplayClass103_0()
			{
			}

			// Token: 0x06001109 RID: 4361 RVA: 0x0005811C File Offset: 0x0005631C
			internal void <OnError>b__0()
			{
				if (this.capturedCloseCount == this.<>4__this._closeCount)
				{
					this.<>4__this.Close();
				}
			}

			// Token: 0x04000BD1 RID: 3025
			public int capturedCloseCount;

			// Token: 0x04000BD2 RID: 3026
			public SqlConnection <>4__this;
		}

		// Token: 0x0200016F RID: 367
		[CompilerGenerated]
		private sealed class <>c__DisplayClass108_0<T>
		{
			// Token: 0x0600110A RID: 4362 RVA: 0x00005C14 File Offset: 0x00003E14
			public <>c__DisplayClass108_0()
			{
			}

			// Token: 0x0600110B RID: 4363 RVA: 0x0005813C File Offset: 0x0005633C
			internal Task<T> <RegisterForConnectionCloseNotification>b__0(Task<T> task)
			{
				this.<>4__this.RemoveWeakReference(this.value);
				return task;
			}

			// Token: 0x04000BD3 RID: 3027
			public SqlConnection <>4__this;

			// Token: 0x04000BD4 RID: 3028
			public object value;
		}
	}
}
