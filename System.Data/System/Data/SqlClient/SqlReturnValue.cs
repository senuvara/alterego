﻿using System;

namespace System.Data.SqlClient
{
	// Token: 0x02000205 RID: 517
	internal sealed class SqlReturnValue : SqlMetaDataPriv
	{
		// Token: 0x060016E9 RID: 5865 RVA: 0x000781EA File Offset: 0x000763EA
		internal SqlReturnValue()
		{
			this.value = new SqlBuffer();
		}

		// Token: 0x04001152 RID: 4434
		internal string parameter;

		// Token: 0x04001153 RID: 4435
		internal readonly SqlBuffer value;
	}
}
