﻿using System;

namespace System.Data.SqlClient
{
	// Token: 0x02000130 RID: 304
	internal sealed class _ColumnMapping
	{
		// Token: 0x06000EC5 RID: 3781 RVA: 0x0004D48A File Offset: 0x0004B68A
		internal _ColumnMapping(int columnId, _SqlMetaData metadata)
		{
			this._sourceColumnOrdinal = columnId;
			this._metadata = metadata;
		}

		// Token: 0x04000A74 RID: 2676
		internal int _sourceColumnOrdinal;

		// Token: 0x04000A75 RID: 2677
		internal _SqlMetaData _metadata;
	}
}
