﻿using System;

namespace System.Data.SqlClient
{
	// Token: 0x02000131 RID: 305
	internal sealed class Row
	{
		// Token: 0x06000EC6 RID: 3782 RVA: 0x0004D4A0 File Offset: 0x0004B6A0
		internal Row(int rowCount)
		{
			this._dataFields = new object[rowCount];
		}

		// Token: 0x17000299 RID: 665
		// (get) Token: 0x06000EC7 RID: 3783 RVA: 0x0004D4B4 File Offset: 0x0004B6B4
		internal object[] DataFields
		{
			get
			{
				return this._dataFields;
			}
		}

		// Token: 0x1700029A RID: 666
		internal object this[int index]
		{
			get
			{
				return this._dataFields[index];
			}
		}

		// Token: 0x04000A76 RID: 2678
		private object[] _dataFields;
	}
}
