﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.IO;

namespace System.Data.SqlClient
{
	// Token: 0x020001CD RID: 461
	internal sealed class SqlCachedStream : Stream
	{
		// Token: 0x060014FF RID: 5375 RVA: 0x0006AE4E File Offset: 0x0006904E
		internal SqlCachedStream(SqlCachedBuffer sqlBuf)
		{
			this._cachedBytes = sqlBuf.CachedBytes;
		}

		// Token: 0x1700040C RID: 1036
		// (get) Token: 0x06001500 RID: 5376 RVA: 0x0000EF1B File Offset: 0x0000D11B
		public override bool CanRead
		{
			get
			{
				return true;
			}
		}

		// Token: 0x1700040D RID: 1037
		// (get) Token: 0x06001501 RID: 5377 RVA: 0x0000EF1B File Offset: 0x0000D11B
		public override bool CanSeek
		{
			get
			{
				return true;
			}
		}

		// Token: 0x1700040E RID: 1038
		// (get) Token: 0x06001502 RID: 5378 RVA: 0x000061C5 File Offset: 0x000043C5
		public override bool CanWrite
		{
			get
			{
				return false;
			}
		}

		// Token: 0x1700040F RID: 1039
		// (get) Token: 0x06001503 RID: 5379 RVA: 0x0006AE62 File Offset: 0x00069062
		public override long Length
		{
			get
			{
				return this.TotalLength;
			}
		}

		// Token: 0x17000410 RID: 1040
		// (get) Token: 0x06001504 RID: 5380 RVA: 0x0006AE6C File Offset: 0x0006906C
		// (set) Token: 0x06001505 RID: 5381 RVA: 0x0006AEB3 File Offset: 0x000690B3
		public override long Position
		{
			get
			{
				long num = 0L;
				if (this._currentArrayIndex > 0)
				{
					for (int i = 0; i < this._currentArrayIndex; i++)
					{
						num += (long)this._cachedBytes[i].Length;
					}
				}
				return num + (long)this._currentPosition;
			}
			set
			{
				if (this._cachedBytes == null)
				{
					throw ADP.StreamClosed("set_Position");
				}
				this.SetInternalPosition(value, "set_Position");
			}
		}

		// Token: 0x06001506 RID: 5382 RVA: 0x0006AED4 File Offset: 0x000690D4
		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing && this._cachedBytes != null)
				{
					this._cachedBytes.Clear();
				}
				this._cachedBytes = null;
				this._currentPosition = 0;
				this._currentArrayIndex = 0;
				this._totalLength = 0L;
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		// Token: 0x06001507 RID: 5383 RVA: 0x0005CDBE File Offset: 0x0005AFBE
		public override void Flush()
		{
			throw ADP.NotSupported();
		}

		// Token: 0x06001508 RID: 5384 RVA: 0x0006AF30 File Offset: 0x00069130
		public override int Read(byte[] buffer, int offset, int count)
		{
			int num = 0;
			if (this._cachedBytes == null)
			{
				throw ADP.StreamClosed("Read");
			}
			if (buffer == null)
			{
				throw ADP.ArgumentNull("buffer");
			}
			if (offset < 0 || count < 0)
			{
				throw ADP.ArgumentOutOfRange(string.Empty, (offset < 0) ? "offset" : "count");
			}
			if (buffer.Length - offset < count)
			{
				throw ADP.ArgumentOutOfRange("count");
			}
			if (this._cachedBytes.Count <= this._currentArrayIndex)
			{
				return 0;
			}
			while (count > 0)
			{
				if (this._cachedBytes[this._currentArrayIndex].Length <= this._currentPosition)
				{
					this._currentArrayIndex++;
					if (this._cachedBytes.Count <= this._currentArrayIndex)
					{
						break;
					}
					this._currentPosition = 0;
				}
				int num2 = this._cachedBytes[this._currentArrayIndex].Length - this._currentPosition;
				if (num2 > count)
				{
					num2 = count;
				}
				Buffer.BlockCopy(this._cachedBytes[this._currentArrayIndex], this._currentPosition, buffer, offset, num2);
				this._currentPosition += num2;
				count -= num2;
				offset += num2;
				num += num2;
			}
			return num;
		}

		// Token: 0x06001509 RID: 5385 RVA: 0x0006B058 File Offset: 0x00069258
		public override long Seek(long offset, SeekOrigin origin)
		{
			long num = 0L;
			if (this._cachedBytes == null)
			{
				throw ADP.StreamClosed("Seek");
			}
			switch (origin)
			{
			case SeekOrigin.Begin:
				this.SetInternalPosition(offset, "offset");
				break;
			case SeekOrigin.Current:
				num = offset + this.Position;
				this.SetInternalPosition(num, "offset");
				break;
			case SeekOrigin.End:
				num = this.TotalLength + offset;
				this.SetInternalPosition(num, "offset");
				break;
			default:
				throw ADP.InvalidSeekOrigin("offset");
			}
			return num;
		}

		// Token: 0x0600150A RID: 5386 RVA: 0x0005CDBE File Offset: 0x0005AFBE
		public override void SetLength(long value)
		{
			throw ADP.NotSupported();
		}

		// Token: 0x0600150B RID: 5387 RVA: 0x0005CDBE File Offset: 0x0005AFBE
		public override void Write(byte[] buffer, int offset, int count)
		{
			throw ADP.NotSupported();
		}

		// Token: 0x0600150C RID: 5388 RVA: 0x0006B0D8 File Offset: 0x000692D8
		private void SetInternalPosition(long lPos, string argumentName)
		{
			long num = lPos;
			if (num < 0L)
			{
				throw new ArgumentOutOfRangeException(argumentName);
			}
			for (int i = 0; i < this._cachedBytes.Count; i++)
			{
				if (num <= (long)this._cachedBytes[i].Length)
				{
					this._currentArrayIndex = i;
					this._currentPosition = (int)num;
					return;
				}
				num -= (long)this._cachedBytes[i].Length;
			}
			if (num > 0L)
			{
				throw new ArgumentOutOfRangeException(argumentName);
			}
		}

		// Token: 0x17000411 RID: 1041
		// (get) Token: 0x0600150D RID: 5389 RVA: 0x0006B14C File Offset: 0x0006934C
		private long TotalLength
		{
			get
			{
				if (this._totalLength == 0L && this._cachedBytes != null)
				{
					long num = 0L;
					for (int i = 0; i < this._cachedBytes.Count; i++)
					{
						num += (long)this._cachedBytes[i].Length;
					}
					this._totalLength = num;
				}
				return this._totalLength;
			}
		}

		// Token: 0x04000EA5 RID: 3749
		private int _currentPosition;

		// Token: 0x04000EA6 RID: 3750
		private int _currentArrayIndex;

		// Token: 0x04000EA7 RID: 3751
		private List<byte[]> _cachedBytes;

		// Token: 0x04000EA8 RID: 3752
		private long _totalLength;
	}
}
