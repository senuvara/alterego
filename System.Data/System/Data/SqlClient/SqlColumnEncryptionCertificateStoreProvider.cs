﻿using System;
using Unity;

namespace System.Data.SqlClient
{
	/// <summary>The implementation of the key store provider for Windows Certificate Store. This class enables using certificates stored in the Windows Certificate Store as column master keys. For details, see Always Encrypted.</summary>
	// Token: 0x02000354 RID: 852
	public class SqlColumnEncryptionCertificateStoreProvider : SqlColumnEncryptionKeyStoreProvider
	{
		/// <summary>Key store provider for Windows Certificate Store.</summary>
		// Token: 0x06002974 RID: 10612 RVA: 0x00010458 File Offset: 0x0000E658
		public SqlColumnEncryptionCertificateStoreProvider()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Decrypts the specified encrypted value of a column encryption key. The encrypted value is expected to be encrypted using the certificate with the specified key path and using the specified algorithm. The format of the key path should be “Local Machine/My/&lt;certificate_thumbrint&gt;” or “Current User/My/&lt;certificate_thumbprint&gt;”.</summary>
		/// <param name="masterKeyPath">The master key path.</param>
		/// <param name="encryptionAlgorithm">The encryption algorithm. Currently, the only valid value is: RSA_OAEP</param>
		/// <param name="encryptedColumnEncryptionKey">The encrypted column encryption key.</param>
		/// <returns>Returns <see cref="T:System.Byte" />. The decrypted column encryption key.</returns>
		// Token: 0x06002975 RID: 10613 RVA: 0x00051759 File Offset: 0x0004F959
		public override byte[] DecryptColumnEncryptionKey(string masterKeyPath, string encryptionAlgorithm, byte[] encryptedColumnEncryptionKey)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Encrypts a column encryption key using the certificate with the specified key path and using the specified algorithm. The format of the key path should be “Local Machine/My/&lt;certificate_thumbrint&gt;” or “Current User/My/&lt;certificate_thumbprint&gt;”.</summary>
		/// <param name="masterKeyPath">The master key path.</param>
		/// <param name="encryptionAlgorithm">The encryption algorithm. Currently, the only valid value is: RSA_OAEP</param>
		/// <param name="columnEncryptionKey">The encrypted column encryption key.</param>
		/// <returns>Returns <see cref="T:System.Byte" />. The encrypted column encryption key.</returns>
		// Token: 0x06002976 RID: 10614 RVA: 0x00051759 File Offset: 0x0004F959
		public override byte[] EncryptColumnEncryptionKey(string masterKeyPath, string encryptionAlgorithm, byte[] columnEncryptionKey)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>The provider name.</summary>
		/// <returns>The provider name.</returns>
		// Token: 0x0400191B RID: 6427
		public const string ProviderName = "MSSQL_CERTIFICATE_STORE";
	}
}
