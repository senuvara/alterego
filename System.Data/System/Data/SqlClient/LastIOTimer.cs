﻿using System;

namespace System.Data.SqlClient
{
	// Token: 0x02000208 RID: 520
	internal sealed class LastIOTimer
	{
		// Token: 0x060016FD RID: 5885 RVA: 0x00005C14 File Offset: 0x00003E14
		public LastIOTimer()
		{
		}

		// Token: 0x04001160 RID: 4448
		internal long _value;
	}
}
