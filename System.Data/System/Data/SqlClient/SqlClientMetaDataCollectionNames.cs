﻿using System;

namespace System.Data.SqlClient
{
	/// <summary>Provides a list of constants for use with the GetSchema method to retrieve metadata collections.</summary>
	// Token: 0x0200014C RID: 332
	public static class SqlClientMetaDataCollectionNames
	{
		// Token: 0x06000F93 RID: 3987 RVA: 0x00051764 File Offset: 0x0004F964
		// Note: this type is marked as 'beforefieldinit'.
		static SqlClientMetaDataCollectionNames()
		{
		}

		/// <summary>A constant for use with the GetSchema method that represents the Columns collection.</summary>
		// Token: 0x04000B13 RID: 2835
		public static readonly string Columns = "Columns";

		/// <summary>A constant for use with the GetSchema method that represents the Databases collection.</summary>
		// Token: 0x04000B14 RID: 2836
		public static readonly string Databases = "Databases";

		/// <summary>A constant for use with the GetSchema method that represents the ForeignKeys collection.</summary>
		// Token: 0x04000B15 RID: 2837
		public static readonly string ForeignKeys = "ForeignKeys";

		/// <summary>A constant for use with the GetSchema method that represents the IndexColumns collection.</summary>
		// Token: 0x04000B16 RID: 2838
		public static readonly string IndexColumns = "IndexColumns";

		/// <summary>A constant for use with the GetSchema method that represents the Indexes collection.</summary>
		// Token: 0x04000B17 RID: 2839
		public static readonly string Indexes = "Indexes";

		/// <summary>A constant for use with the GetSchema method that represents the Parameters collection.</summary>
		// Token: 0x04000B18 RID: 2840
		public static readonly string Parameters = "Parameters";

		/// <summary>A constant for use with the GetSchema method that represents the ProcedureColumns collection.</summary>
		// Token: 0x04000B19 RID: 2841
		public static readonly string ProcedureColumns = "ProcedureColumns";

		/// <summary>A constant for use with the GetSchema method that represents the Procedures collection.</summary>
		// Token: 0x04000B1A RID: 2842
		public static readonly string Procedures = "Procedures";

		/// <summary>A constant for use with the GetSchema method that represents the Tables collection.</summary>
		// Token: 0x04000B1B RID: 2843
		public static readonly string Tables = "Tables";

		/// <summary>A constant for use with the GetSchema method that represents the UserDefinedTypes collection.</summary>
		// Token: 0x04000B1C RID: 2844
		public static readonly string UserDefinedTypes = "UserDefinedTypes";

		/// <summary>A constant for use with the GetSchema method that represents the Users collection.</summary>
		// Token: 0x04000B1D RID: 2845
		public static readonly string Users = "Users";

		/// <summary>A constant for use with the GetSchema method that represents the ViewColumns collection.</summary>
		// Token: 0x04000B1E RID: 2846
		public static readonly string ViewColumns = "ViewColumns";

		/// <summary>A constant for use with the GetSchema method that represents the Views collection. </summary>
		// Token: 0x04000B1F RID: 2847
		public static readonly string Views = "Views";
	}
}
