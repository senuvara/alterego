﻿using System;
using System.Text;

namespace System.Data.SqlClient
{
	// Token: 0x02000203 RID: 515
	internal class SqlMetaDataPriv
	{
		// Token: 0x060016E5 RID: 5861 RVA: 0x000780CC File Offset: 0x000762CC
		internal SqlMetaDataPriv()
		{
		}

		// Token: 0x060016E6 RID: 5862 RVA: 0x000780EC File Offset: 0x000762EC
		internal virtual void CopyFrom(SqlMetaDataPriv original)
		{
			this.type = original.type;
			this.tdsType = original.tdsType;
			this.precision = original.precision;
			this.scale = original.scale;
			this.length = original.length;
			this.collation = original.collation;
			this.codePage = original.codePage;
			this.encoding = original.encoding;
			this.isNullable = original.isNullable;
			this.udtDatabaseName = original.udtDatabaseName;
			this.udtSchemaName = original.udtSchemaName;
			this.udtTypeName = original.udtTypeName;
			this.udtAssemblyQualifiedName = original.udtAssemblyQualifiedName;
			this.xmlSchemaCollectionDatabase = original.xmlSchemaCollectionDatabase;
			this.xmlSchemaCollectionOwningSchema = original.xmlSchemaCollectionOwningSchema;
			this.xmlSchemaCollectionName = original.xmlSchemaCollectionName;
			this.metaType = original.metaType;
		}

		// Token: 0x04001134 RID: 4404
		internal SqlDbType type;

		// Token: 0x04001135 RID: 4405
		internal byte tdsType;

		// Token: 0x04001136 RID: 4406
		internal byte precision = byte.MaxValue;

		// Token: 0x04001137 RID: 4407
		internal byte scale = byte.MaxValue;

		// Token: 0x04001138 RID: 4408
		internal int length;

		// Token: 0x04001139 RID: 4409
		internal SqlCollation collation;

		// Token: 0x0400113A RID: 4410
		internal int codePage;

		// Token: 0x0400113B RID: 4411
		internal Encoding encoding;

		// Token: 0x0400113C RID: 4412
		internal bool isNullable;

		// Token: 0x0400113D RID: 4413
		internal string udtDatabaseName;

		// Token: 0x0400113E RID: 4414
		internal string udtSchemaName;

		// Token: 0x0400113F RID: 4415
		internal string udtTypeName;

		// Token: 0x04001140 RID: 4416
		internal string udtAssemblyQualifiedName;

		// Token: 0x04001141 RID: 4417
		internal string xmlSchemaCollectionDatabase;

		// Token: 0x04001142 RID: 4418
		internal string xmlSchemaCollectionOwningSchema;

		// Token: 0x04001143 RID: 4419
		internal string xmlSchemaCollectionName;

		// Token: 0x04001144 RID: 4420
		internal MetaType metaType;
	}
}
