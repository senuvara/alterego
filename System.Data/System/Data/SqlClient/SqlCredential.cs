﻿using System;
using System.Security;

namespace System.Data.SqlClient
{
	/// <summary>
	///     <see cref="T:System.Data.SqlClient.SqlCredential" /> provides a more secure way to specify the password for a login attempt using SQL Server Authentication.
	///     <see cref="T:System.Data.SqlClient.SqlCredential" /> is comprised of a user id and a password that will be used for SQL Server Authentication. The password in a <see cref="T:System.Data.SqlClient.SqlCredential" /> object is of type <see cref="T:System.Security.SecureString" />.
	///     <see cref="T:System.Data.SqlClient.SqlCredential" /> cannot be inherited.Windows Authentication (<see langword="Integrated Security = true" />) remains the most secure way to log in to a SQL Server database.</summary>
	// Token: 0x0200021C RID: 540
	[Serializable]
	public sealed class SqlCredential
	{
		/// <summary>Creates an object of type <see cref="T:System.Data.SqlClient.SqlCredential" />.</summary>
		/// <param name="userId">The user id.</param>
		/// <param name="password">The password; a <see cref="T:System.Security.SecureString" /> value marked as read-only.  Passing a read/write <see cref="T:System.Security.SecureString" /> parameter will raise an <see cref="T:System.ArgumentException" />.</param>
		// Token: 0x06001813 RID: 6163 RVA: 0x0007D909 File Offset: 0x0007BB09
		public SqlCredential(string userId, SecureString password)
		{
			if (userId == null)
			{
				throw new ArgumentNullException("userId");
			}
			if (password == null)
			{
				throw new ArgumentNullException("password");
			}
			this.uid = userId;
			this.pwd = password;
		}

		/// <summary>Returns the user ID component of the <see cref="T:System.Data.SqlClient.SqlCredential" /> object.</summary>
		/// <returns>Returns the user ID component of the <see cref="T:System.Data.SqlClient.SqlCredential" /> object..</returns>
		// Token: 0x17000466 RID: 1126
		// (get) Token: 0x06001814 RID: 6164 RVA: 0x0007D946 File Offset: 0x0007BB46
		public string UserId
		{
			get
			{
				return this.uid;
			}
		}

		/// <summary>Returns the password component of the <see cref="T:System.Data.SqlClient.SqlCredential" /> object.</summary>
		/// <returns>Returns the password component of the <see cref="T:System.Data.SqlClient.SqlCredential" /> object.</returns>
		// Token: 0x17000467 RID: 1127
		// (get) Token: 0x06001815 RID: 6165 RVA: 0x0007D94E File Offset: 0x0007BB4E
		public SecureString Password
		{
			get
			{
				return this.pwd;
			}
		}

		// Token: 0x040011EC RID: 4588
		private string uid = "";

		// Token: 0x040011ED RID: 4589
		private SecureString pwd;
	}
}
