﻿using System;
using System.Globalization;
using System.Runtime.CompilerServices;

namespace System.Data.SqlClient
{
	// Token: 0x020001AE RID: 430
	internal sealed class ServerInfo
	{
		// Token: 0x170003C1 RID: 961
		// (get) Token: 0x060013CA RID: 5066 RVA: 0x00066AEC File Offset: 0x00064CEC
		// (set) Token: 0x060013CB RID: 5067 RVA: 0x00066AF4 File Offset: 0x00064CF4
		internal string ExtendedServerName
		{
			[CompilerGenerated]
			get
			{
				return this.<ExtendedServerName>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<ExtendedServerName>k__BackingField = value;
			}
		}

		// Token: 0x170003C2 RID: 962
		// (get) Token: 0x060013CC RID: 5068 RVA: 0x00066AFD File Offset: 0x00064CFD
		// (set) Token: 0x060013CD RID: 5069 RVA: 0x00066B05 File Offset: 0x00064D05
		internal string ResolvedServerName
		{
			[CompilerGenerated]
			get
			{
				return this.<ResolvedServerName>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<ResolvedServerName>k__BackingField = value;
			}
		}

		// Token: 0x170003C3 RID: 963
		// (get) Token: 0x060013CE RID: 5070 RVA: 0x00066B0E File Offset: 0x00064D0E
		// (set) Token: 0x060013CF RID: 5071 RVA: 0x00066B16 File Offset: 0x00064D16
		internal string ResolvedDatabaseName
		{
			[CompilerGenerated]
			get
			{
				return this.<ResolvedDatabaseName>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<ResolvedDatabaseName>k__BackingField = value;
			}
		}

		// Token: 0x170003C4 RID: 964
		// (get) Token: 0x060013D0 RID: 5072 RVA: 0x00066B1F File Offset: 0x00064D1F
		// (set) Token: 0x060013D1 RID: 5073 RVA: 0x00066B27 File Offset: 0x00064D27
		internal string UserProtocol
		{
			[CompilerGenerated]
			get
			{
				return this.<UserProtocol>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<UserProtocol>k__BackingField = value;
			}
		}

		// Token: 0x170003C5 RID: 965
		// (get) Token: 0x060013D2 RID: 5074 RVA: 0x00066B30 File Offset: 0x00064D30
		// (set) Token: 0x060013D3 RID: 5075 RVA: 0x00066B38 File Offset: 0x00064D38
		internal string UserServerName
		{
			get
			{
				return this._userServerName;
			}
			private set
			{
				this._userServerName = value;
			}
		}

		// Token: 0x060013D4 RID: 5076 RVA: 0x00066B41 File Offset: 0x00064D41
		internal ServerInfo(SqlConnectionString userOptions) : this(userOptions, userOptions.DataSource)
		{
		}

		// Token: 0x060013D5 RID: 5077 RVA: 0x00066B50 File Offset: 0x00064D50
		internal ServerInfo(SqlConnectionString userOptions, string serverName)
		{
			this.UserServerName = (serverName ?? string.Empty);
			this.UserProtocol = string.Empty;
			this.ResolvedDatabaseName = userOptions.InitialCatalog;
			this.PreRoutingServerName = null;
		}

		// Token: 0x060013D6 RID: 5078 RVA: 0x00066B88 File Offset: 0x00064D88
		internal ServerInfo(SqlConnectionString userOptions, RoutingInfo routing, string preRoutingServerName)
		{
			if (routing == null || routing.ServerName == null)
			{
				this.UserServerName = string.Empty;
			}
			else
			{
				this.UserServerName = string.Format(CultureInfo.InvariantCulture, "{0},{1}", routing.ServerName, routing.Port);
			}
			this.PreRoutingServerName = preRoutingServerName;
			this.UserProtocol = "tcp";
			this.SetDerivedNames(this.UserProtocol, this.UserServerName);
			this.ResolvedDatabaseName = userOptions.InitialCatalog;
		}

		// Token: 0x060013D7 RID: 5079 RVA: 0x00066C09 File Offset: 0x00064E09
		internal void SetDerivedNames(string protocol, string serverName)
		{
			if (!string.IsNullOrEmpty(protocol))
			{
				this.ExtendedServerName = protocol + ":" + serverName;
			}
			else
			{
				this.ExtendedServerName = serverName;
			}
			this.ResolvedServerName = serverName;
		}

		// Token: 0x04000E0D RID: 3597
		[CompilerGenerated]
		private string <ExtendedServerName>k__BackingField;

		// Token: 0x04000E0E RID: 3598
		[CompilerGenerated]
		private string <ResolvedServerName>k__BackingField;

		// Token: 0x04000E0F RID: 3599
		[CompilerGenerated]
		private string <ResolvedDatabaseName>k__BackingField;

		// Token: 0x04000E10 RID: 3600
		[CompilerGenerated]
		private string <UserProtocol>k__BackingField;

		// Token: 0x04000E11 RID: 3601
		private string _userServerName;

		// Token: 0x04000E12 RID: 3602
		internal readonly string PreRoutingServerName;
	}
}
