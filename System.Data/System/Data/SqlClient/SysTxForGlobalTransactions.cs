﻿using System;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Transactions;

namespace System.Data.SqlClient
{
	// Token: 0x020001D9 RID: 473
	internal static class SysTxForGlobalTransactions
	{
		// Token: 0x1700041A RID: 1050
		// (get) Token: 0x060015D1 RID: 5585 RVA: 0x0006CCF2 File Offset: 0x0006AEF2
		public static MethodInfo EnlistPromotableSinglePhase
		{
			get
			{
				return SysTxForGlobalTransactions._enlistPromotableSinglePhase.Value;
			}
		}

		// Token: 0x1700041B RID: 1051
		// (get) Token: 0x060015D2 RID: 5586 RVA: 0x0006CCFE File Offset: 0x0006AEFE
		public static MethodInfo SetDistributedTransactionIdentifier
		{
			get
			{
				return SysTxForGlobalTransactions._setDistributedTransactionIdentifier.Value;
			}
		}

		// Token: 0x1700041C RID: 1052
		// (get) Token: 0x060015D3 RID: 5587 RVA: 0x0006CD0A File Offset: 0x0006AF0A
		public static MethodInfo GetPromotedToken
		{
			get
			{
				return SysTxForGlobalTransactions._getPromotedToken.Value;
			}
		}

		// Token: 0x060015D4 RID: 5588 RVA: 0x0006CD18 File Offset: 0x0006AF18
		// Note: this type is marked as 'beforefieldinit'.
		static SysTxForGlobalTransactions()
		{
		}

		// Token: 0x04000EC6 RID: 3782
		private static readonly Lazy<MethodInfo> _enlistPromotableSinglePhase = new Lazy<MethodInfo>(() => typeof(Transaction).GetMethod("EnlistPromotableSinglePhase", new Type[]
		{
			typeof(IPromotableSinglePhaseNotification),
			typeof(Guid)
		}));

		// Token: 0x04000EC7 RID: 3783
		private static readonly Lazy<MethodInfo> _setDistributedTransactionIdentifier = new Lazy<MethodInfo>(() => typeof(Transaction).GetMethod("SetDistributedTransactionIdentifier", new Type[]
		{
			typeof(IPromotableSinglePhaseNotification),
			typeof(Guid)
		}));

		// Token: 0x04000EC8 RID: 3784
		private static readonly Lazy<MethodInfo> _getPromotedToken = new Lazy<MethodInfo>(() => typeof(Transaction).GetMethod("GetPromotedToken"));

		// Token: 0x020001DA RID: 474
		[CompilerGenerated]
		[Serializable]
		private sealed class <>c
		{
			// Token: 0x060015D5 RID: 5589 RVA: 0x0006CD73 File Offset: 0x0006AF73
			// Note: this type is marked as 'beforefieldinit'.
			static <>c()
			{
			}

			// Token: 0x060015D6 RID: 5590 RVA: 0x00005C14 File Offset: 0x00003E14
			public <>c()
			{
			}

			// Token: 0x060015D7 RID: 5591 RVA: 0x0006CD7F File Offset: 0x0006AF7F
			internal MethodInfo <.cctor>b__9_0()
			{
				return typeof(Transaction).GetMethod("EnlistPromotableSinglePhase", new Type[]
				{
					typeof(IPromotableSinglePhaseNotification),
					typeof(Guid)
				});
			}

			// Token: 0x060015D8 RID: 5592 RVA: 0x0006CDB5 File Offset: 0x0006AFB5
			internal MethodInfo <.cctor>b__9_1()
			{
				return typeof(Transaction).GetMethod("SetDistributedTransactionIdentifier", new Type[]
				{
					typeof(IPromotableSinglePhaseNotification),
					typeof(Guid)
				});
			}

			// Token: 0x060015D9 RID: 5593 RVA: 0x0006CDEB File Offset: 0x0006AFEB
			internal MethodInfo <.cctor>b__9_2()
			{
				return typeof(Transaction).GetMethod("GetPromotedToken");
			}

			// Token: 0x04000EC9 RID: 3785
			public static readonly SysTxForGlobalTransactions.<>c <>9 = new SysTxForGlobalTransactions.<>c();
		}
	}
}
