﻿using System;
using System.Runtime.CompilerServices;

namespace System.Data.SqlClient
{
	// Token: 0x020001FC RID: 508
	internal class RoutingInfo
	{
		// Token: 0x17000432 RID: 1074
		// (get) Token: 0x060016C9 RID: 5833 RVA: 0x00077C70 File Offset: 0x00075E70
		// (set) Token: 0x060016CA RID: 5834 RVA: 0x00077C78 File Offset: 0x00075E78
		internal byte Protocol
		{
			[CompilerGenerated]
			get
			{
				return this.<Protocol>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<Protocol>k__BackingField = value;
			}
		}

		// Token: 0x17000433 RID: 1075
		// (get) Token: 0x060016CB RID: 5835 RVA: 0x00077C81 File Offset: 0x00075E81
		// (set) Token: 0x060016CC RID: 5836 RVA: 0x00077C89 File Offset: 0x00075E89
		internal ushort Port
		{
			[CompilerGenerated]
			get
			{
				return this.<Port>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<Port>k__BackingField = value;
			}
		}

		// Token: 0x17000434 RID: 1076
		// (get) Token: 0x060016CD RID: 5837 RVA: 0x00077C92 File Offset: 0x00075E92
		// (set) Token: 0x060016CE RID: 5838 RVA: 0x00077C9A File Offset: 0x00075E9A
		internal string ServerName
		{
			[CompilerGenerated]
			get
			{
				return this.<ServerName>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<ServerName>k__BackingField = value;
			}
		}

		// Token: 0x060016CF RID: 5839 RVA: 0x00077CA3 File Offset: 0x00075EA3
		internal RoutingInfo(byte protocol, ushort port, string servername)
		{
			this.Protocol = protocol;
			this.Port = port;
			this.ServerName = servername;
		}

		// Token: 0x040010FC RID: 4348
		[CompilerGenerated]
		private byte <Protocol>k__BackingField;

		// Token: 0x040010FD RID: 4349
		[CompilerGenerated]
		private ushort <Port>k__BackingField;

		// Token: 0x040010FE RID: 4350
		[CompilerGenerated]
		private string <ServerName>k__BackingField;
	}
}
