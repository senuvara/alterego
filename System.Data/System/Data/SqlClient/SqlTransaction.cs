﻿using System;
using System.Data.Common;
using Unity;

namespace System.Data.SqlClient
{
	/// <summary>Represents a Transact-SQL transaction to be made in a SQL Server database. This class cannot be inherited. </summary>
	// Token: 0x020001CF RID: 463
	public sealed class SqlTransaction : DbTransaction
	{
		// Token: 0x06001513 RID: 5395 RVA: 0x0006B5B4 File Offset: 0x000697B4
		internal SqlTransaction(SqlInternalConnection internalConnection, SqlConnection con, IsolationLevel iso, SqlInternalTransaction internalTransaction)
		{
			this._isolationLevel = IsolationLevel.ReadCommitted;
			base..ctor();
			this._isolationLevel = iso;
			this._connection = con;
			if (internalTransaction == null)
			{
				this._internalTransaction = new SqlInternalTransaction(internalConnection, TransactionType.LocalFromAPI, this);
				return;
			}
			this._internalTransaction = internalTransaction;
			this._internalTransaction.InitParent(this);
		}

		/// <summary>Gets the <see cref="T:System.Data.SqlClient.SqlConnection" /> object associated with the transaction, or <see langword="null" /> if the transaction is no longer valid.</summary>
		/// <returns>The <see cref="T:System.Data.SqlClient.SqlConnection" /> object associated with the transaction.</returns>
		// Token: 0x17000413 RID: 1043
		// (get) Token: 0x06001514 RID: 5396 RVA: 0x0006B607 File Offset: 0x00069807
		public new SqlConnection Connection
		{
			get
			{
				if (this.IsZombied)
				{
					return null;
				}
				return this._connection;
			}
		}

		// Token: 0x17000414 RID: 1044
		// (get) Token: 0x06001515 RID: 5397 RVA: 0x0006B619 File Offset: 0x00069819
		protected override DbConnection DbConnection
		{
			get
			{
				return this.Connection;
			}
		}

		// Token: 0x17000415 RID: 1045
		// (get) Token: 0x06001516 RID: 5398 RVA: 0x0006B621 File Offset: 0x00069821
		internal SqlInternalTransaction InternalTransaction
		{
			get
			{
				return this._internalTransaction;
			}
		}

		/// <summary>Specifies the <see cref="T:System.Data.IsolationLevel" /> for this transaction.</summary>
		/// <returns>The <see cref="T:System.Data.IsolationLevel" /> for this transaction. The default is <see langword="ReadCommitted" />.</returns>
		// Token: 0x17000416 RID: 1046
		// (get) Token: 0x06001517 RID: 5399 RVA: 0x0006B629 File Offset: 0x00069829
		public override IsolationLevel IsolationLevel
		{
			get
			{
				this.ZombieCheck();
				return this._isolationLevel;
			}
		}

		// Token: 0x17000417 RID: 1047
		// (get) Token: 0x06001518 RID: 5400 RVA: 0x0006B637 File Offset: 0x00069837
		private bool IsYukonPartialZombie
		{
			get
			{
				return this._internalTransaction != null && this._internalTransaction.IsCompleted;
			}
		}

		// Token: 0x17000418 RID: 1048
		// (get) Token: 0x06001519 RID: 5401 RVA: 0x0006B64E File Offset: 0x0006984E
		internal bool IsZombied
		{
			get
			{
				return this._internalTransaction == null || this._internalTransaction.IsCompleted;
			}
		}

		// Token: 0x17000419 RID: 1049
		// (get) Token: 0x0600151A RID: 5402 RVA: 0x0006B665 File Offset: 0x00069865
		internal SqlStatistics Statistics
		{
			get
			{
				if (this._connection != null && this._connection.StatisticsEnabled)
				{
					return this._connection.Statistics;
				}
				return null;
			}
		}

		/// <summary>Commits the database transaction.</summary>
		/// <exception cref="T:System.Exception">An error occurred while trying to commit the transaction. </exception>
		/// <exception cref="T:System.InvalidOperationException">The transaction has already been committed or rolled back.-or- The connection is broken. </exception>
		// Token: 0x0600151B RID: 5403 RVA: 0x0006B68C File Offset: 0x0006988C
		public override void Commit()
		{
			Exception ex = null;
			Guid operationId = SqlTransaction.s_diagnosticListener.WriteTransactionCommitBefore(this._isolationLevel, this._connection, "Commit");
			this.ZombieCheck();
			SqlStatistics statistics = null;
			try
			{
				statistics = SqlStatistics.StartTimer(this.Statistics);
				this._isFromAPI = true;
				this._internalTransaction.Commit();
			}
			catch (Exception ex)
			{
				throw;
			}
			finally
			{
				if (ex != null)
				{
					SqlTransaction.s_diagnosticListener.WriteTransactionCommitError(operationId, this._isolationLevel, this._connection, ex, "Commit");
				}
				else
				{
					SqlTransaction.s_diagnosticListener.WriteTransactionCommitAfter(operationId, this._isolationLevel, this._connection, "Commit");
				}
				this._isFromAPI = false;
				SqlStatistics.StopTimer(statistics);
			}
		}

		// Token: 0x0600151C RID: 5404 RVA: 0x0006B74C File Offset: 0x0006994C
		protected override void Dispose(bool disposing)
		{
			if (disposing && !this.IsZombied && !this.IsYukonPartialZombie)
			{
				this._internalTransaction.Dispose();
			}
			base.Dispose(disposing);
		}

		/// <summary>Rolls back a transaction from a pending state.</summary>
		/// <exception cref="T:System.Exception">An error occurred while trying to commit the transaction. </exception>
		/// <exception cref="T:System.InvalidOperationException">The transaction has already been committed or rolled back.-or- The connection is broken. </exception>
		// Token: 0x0600151D RID: 5405 RVA: 0x0006B774 File Offset: 0x00069974
		public override void Rollback()
		{
			Exception ex = null;
			Guid operationId = SqlTransaction.s_diagnosticListener.WriteTransactionRollbackBefore(this._isolationLevel, this._connection, null, "Rollback");
			if (this.IsYukonPartialZombie)
			{
				this._internalTransaction = null;
				return;
			}
			this.ZombieCheck();
			SqlStatistics statistics = null;
			try
			{
				statistics = SqlStatistics.StartTimer(this.Statistics);
				this._isFromAPI = true;
				this._internalTransaction.Rollback();
			}
			catch (Exception ex)
			{
				throw;
			}
			finally
			{
				if (ex != null)
				{
					SqlTransaction.s_diagnosticListener.WriteTransactionRollbackError(operationId, this._isolationLevel, this._connection, null, ex, "Rollback");
				}
				else
				{
					SqlTransaction.s_diagnosticListener.WriteTransactionRollbackAfter(operationId, this._isolationLevel, this._connection, null, "Rollback");
				}
				this._isFromAPI = false;
				SqlStatistics.StopTimer(statistics);
			}
		}

		/// <summary>Rolls back a transaction from a pending state, and specifies the transaction or savepoint name.</summary>
		/// <param name="transactionName">The name of the transaction to roll back, or the savepoint to which to roll back. </param>
		/// <exception cref="T:System.ArgumentException">No transaction name was specified. </exception>
		/// <exception cref="T:System.InvalidOperationException">The transaction has already been committed or rolled back.-or- The connection is broken. </exception>
		// Token: 0x0600151E RID: 5406 RVA: 0x0006B848 File Offset: 0x00069A48
		public void Rollback(string transactionName)
		{
			Exception ex = null;
			Guid operationId = SqlTransaction.s_diagnosticListener.WriteTransactionRollbackBefore(this._isolationLevel, this._connection, transactionName, "Rollback");
			this.ZombieCheck();
			SqlStatistics statistics = null;
			try
			{
				statistics = SqlStatistics.StartTimer(this.Statistics);
				this._isFromAPI = true;
				this._internalTransaction.Rollback(transactionName);
			}
			catch (Exception ex)
			{
				throw;
			}
			finally
			{
				if (ex != null)
				{
					SqlTransaction.s_diagnosticListener.WriteTransactionRollbackError(operationId, this._isolationLevel, this._connection, transactionName, ex, "Rollback");
				}
				else
				{
					SqlTransaction.s_diagnosticListener.WriteTransactionRollbackAfter(operationId, this._isolationLevel, this._connection, transactionName, "Rollback");
				}
				this._isFromAPI = false;
				SqlStatistics.StopTimer(statistics);
			}
		}

		/// <summary>Creates a savepoint in the transaction that can be used to roll back a part of the transaction, and specifies the savepoint name.</summary>
		/// <param name="savePointName">The name of the savepoint. </param>
		/// <exception cref="T:System.Exception">An error occurred while trying to commit the transaction. </exception>
		/// <exception cref="T:System.InvalidOperationException">The transaction has already been committed or rolled back.-or- The connection is broken. </exception>
		// Token: 0x0600151F RID: 5407 RVA: 0x0006B90C File Offset: 0x00069B0C
		public void Save(string savePointName)
		{
			this.ZombieCheck();
			SqlStatistics statistics = null;
			try
			{
				statistics = SqlStatistics.StartTimer(this.Statistics);
				this._internalTransaction.Save(savePointName);
			}
			finally
			{
				SqlStatistics.StopTimer(statistics);
			}
		}

		// Token: 0x06001520 RID: 5408 RVA: 0x0006B954 File Offset: 0x00069B54
		internal void Zombie()
		{
			if (!(this._connection.InnerConnection is SqlInternalConnection) || this._isFromAPI)
			{
				this._internalTransaction = null;
			}
		}

		// Token: 0x06001521 RID: 5409 RVA: 0x0006B977 File Offset: 0x00069B77
		private void ZombieCheck()
		{
			if (this.IsZombied)
			{
				if (this.IsYukonPartialZombie)
				{
					this._internalTransaction = null;
				}
				throw ADP.TransactionZombied(this);
			}
		}

		// Token: 0x06001522 RID: 5410 RVA: 0x0006B997 File Offset: 0x00069B97
		// Note: this type is marked as 'beforefieldinit'.
		static SqlTransaction()
		{
		}

		// Token: 0x06001523 RID: 5411 RVA: 0x00010458 File Offset: 0x0000E658
		internal SqlTransaction()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x04000EAF RID: 3759
		private static readonly DiagnosticListener s_diagnosticListener = new DiagnosticListener("SqlClientDiagnosticListener");

		// Token: 0x04000EB0 RID: 3760
		internal readonly IsolationLevel _isolationLevel;

		// Token: 0x04000EB1 RID: 3761
		private SqlInternalTransaction _internalTransaction;

		// Token: 0x04000EB2 RID: 3762
		private SqlConnection _connection;

		// Token: 0x04000EB3 RID: 3763
		private bool _isFromAPI;
	}
}
