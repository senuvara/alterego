﻿using System;
using System.Data.Common;

namespace System.Data.SqlClient
{
	// Token: 0x02000206 RID: 518
	internal struct MultiPartTableName
	{
		// Token: 0x060016EA RID: 5866 RVA: 0x000781FD File Offset: 0x000763FD
		internal MultiPartTableName(string[] parts)
		{
			this._multipartName = null;
			this._serverName = parts[0];
			this._catalogName = parts[1];
			this._schemaName = parts[2];
			this._tableName = parts[3];
		}

		// Token: 0x060016EB RID: 5867 RVA: 0x0007822A File Offset: 0x0007642A
		internal MultiPartTableName(string multipartName)
		{
			this._multipartName = multipartName;
			this._serverName = null;
			this._catalogName = null;
			this._schemaName = null;
			this._tableName = null;
		}

		// Token: 0x1700043D RID: 1085
		// (get) Token: 0x060016EC RID: 5868 RVA: 0x0007824F File Offset: 0x0007644F
		// (set) Token: 0x060016ED RID: 5869 RVA: 0x0007825D File Offset: 0x0007645D
		internal string ServerName
		{
			get
			{
				this.ParseMultipartName();
				return this._serverName;
			}
			set
			{
				this._serverName = value;
			}
		}

		// Token: 0x1700043E RID: 1086
		// (get) Token: 0x060016EE RID: 5870 RVA: 0x00078266 File Offset: 0x00076466
		// (set) Token: 0x060016EF RID: 5871 RVA: 0x00078274 File Offset: 0x00076474
		internal string CatalogName
		{
			get
			{
				this.ParseMultipartName();
				return this._catalogName;
			}
			set
			{
				this._catalogName = value;
			}
		}

		// Token: 0x1700043F RID: 1087
		// (get) Token: 0x060016F0 RID: 5872 RVA: 0x0007827D File Offset: 0x0007647D
		// (set) Token: 0x060016F1 RID: 5873 RVA: 0x0007828B File Offset: 0x0007648B
		internal string SchemaName
		{
			get
			{
				this.ParseMultipartName();
				return this._schemaName;
			}
			set
			{
				this._schemaName = value;
			}
		}

		// Token: 0x17000440 RID: 1088
		// (get) Token: 0x060016F2 RID: 5874 RVA: 0x00078294 File Offset: 0x00076494
		// (set) Token: 0x060016F3 RID: 5875 RVA: 0x000782A2 File Offset: 0x000764A2
		internal string TableName
		{
			get
			{
				this.ParseMultipartName();
				return this._tableName;
			}
			set
			{
				this._tableName = value;
			}
		}

		// Token: 0x060016F4 RID: 5876 RVA: 0x000782AC File Offset: 0x000764AC
		private void ParseMultipartName()
		{
			if (this._multipartName != null)
			{
				string[] array = MultipartIdentifier.ParseMultipartIdentifier(this._multipartName, "[\"", "]\"", "Processing of results from SQL Server failed because of an invalid multipart name", false);
				this._serverName = array[0];
				this._catalogName = array[1];
				this._schemaName = array[2];
				this._tableName = array[3];
				this._multipartName = null;
			}
		}

		// Token: 0x060016F5 RID: 5877 RVA: 0x00078308 File Offset: 0x00076508
		// Note: this type is marked as 'beforefieldinit'.
		static MultiPartTableName()
		{
		}

		// Token: 0x04001154 RID: 4436
		private string _multipartName;

		// Token: 0x04001155 RID: 4437
		private string _serverName;

		// Token: 0x04001156 RID: 4438
		private string _catalogName;

		// Token: 0x04001157 RID: 4439
		private string _schemaName;

		// Token: 0x04001158 RID: 4440
		private string _tableName;

		// Token: 0x04001159 RID: 4441
		internal static readonly MultiPartTableName Null = new MultiPartTableName(new string[4]);
	}
}
