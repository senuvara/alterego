﻿using System;

namespace System.Data.SqlClient
{
	// Token: 0x020001F9 RID: 505
	internal enum RunBehavior
	{
		// Token: 0x040010E8 RID: 4328
		UntilDone = 1,
		// Token: 0x040010E9 RID: 4329
		ReturnImmediately,
		// Token: 0x040010EA RID: 4330
		Clean = 5,
		// Token: 0x040010EB RID: 4331
		Attention = 13
	}
}
