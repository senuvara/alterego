﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Diagnostics;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace System.Data.SqlClient
{
	// Token: 0x02000209 RID: 521
	internal abstract class TdsParserStateObject
	{
		// Token: 0x060016FE RID: 5886 RVA: 0x00078610 File Offset: 0x00076810
		internal TdsParserStateObject(TdsParser parser)
		{
			this._parser = parser;
			this.SetPacketSize(4096);
			this.IncrementPendingCallbacks();
			this._lastSuccessfulIOTimer = new LastIOTimer();
		}

		// Token: 0x060016FF RID: 5887 RVA: 0x000786B4 File Offset: 0x000768B4
		internal TdsParserStateObject(TdsParser parser, TdsParserStateObject physicalConnection, bool async)
		{
			this._parser = parser;
			this.SniContext = SniContext.Snix_GetMarsSession;
			this.SetPacketSize(this._parser._physicalStateObj._outBuff.Length);
			this.CreateSessionHandle(physicalConnection, async);
			if (this.IsFailedHandle())
			{
				this.AddError(parser.ProcessSNIError(this));
				this.ThrowExceptionAndWarning(false, false);
			}
			this.IncrementPendingCallbacks();
			this._lastSuccessfulIOTimer = parser._physicalStateObj._lastSuccessfulIOTimer;
		}

		// Token: 0x17000443 RID: 1091
		// (get) Token: 0x06001700 RID: 5888 RVA: 0x00078796 File Offset: 0x00076996
		// (set) Token: 0x06001701 RID: 5889 RVA: 0x0007879E File Offset: 0x0007699E
		internal bool BcpLock
		{
			get
			{
				return this._bcpLock;
			}
			set
			{
				this._bcpLock = value;
			}
		}

		// Token: 0x17000444 RID: 1092
		// (get) Token: 0x06001702 RID: 5890 RVA: 0x000787A7 File Offset: 0x000769A7
		internal bool HasOpenResult
		{
			get
			{
				return this._hasOpenResult;
			}
		}

		// Token: 0x17000445 RID: 1093
		// (get) Token: 0x06001703 RID: 5891 RVA: 0x000787AF File Offset: 0x000769AF
		internal bool IsOrphaned
		{
			get
			{
				return this._activateCount != 0 && !this._owner.IsAlive;
			}
		}

		// Token: 0x17000446 RID: 1094
		// (set) Token: 0x06001704 RID: 5892 RVA: 0x000787CC File Offset: 0x000769CC
		internal object Owner
		{
			set
			{
				SqlDataReader sqlDataReader = value as SqlDataReader;
				if (sqlDataReader == null)
				{
					this._readerState = null;
				}
				else
				{
					this._readerState = sqlDataReader._sharedState;
				}
				this._owner.Target = value;
			}
		}

		// Token: 0x06001705 RID: 5893
		internal abstract uint DisabeSsl();

		// Token: 0x17000447 RID: 1095
		// (get) Token: 0x06001706 RID: 5894 RVA: 0x00078804 File Offset: 0x00076A04
		internal bool HasOwner
		{
			get
			{
				return this._owner.IsAlive;
			}
		}

		// Token: 0x17000448 RID: 1096
		// (get) Token: 0x06001707 RID: 5895 RVA: 0x00078811 File Offset: 0x00076A11
		internal TdsParser Parser
		{
			get
			{
				return this._parser;
			}
		}

		// Token: 0x06001708 RID: 5896
		internal abstract uint EnableMars(ref uint info);

		// Token: 0x17000449 RID: 1097
		// (get) Token: 0x06001709 RID: 5897 RVA: 0x00078819 File Offset: 0x00076A19
		// (set) Token: 0x0600170A RID: 5898 RVA: 0x00078821 File Offset: 0x00076A21
		internal SniContext SniContext
		{
			get
			{
				return this._sniContext;
			}
			set
			{
				this._sniContext = value;
			}
		}

		// Token: 0x1700044A RID: 1098
		// (get) Token: 0x0600170B RID: 5899
		internal abstract uint Status { get; }

		// Token: 0x1700044B RID: 1099
		// (get) Token: 0x0600170C RID: 5900
		internal abstract object SessionHandle { get; }

		// Token: 0x1700044C RID: 1100
		// (get) Token: 0x0600170D RID: 5901 RVA: 0x0007882A File Offset: 0x00076A2A
		internal bool TimeoutHasExpired
		{
			get
			{
				return TdsParserStaticMethods.TimeoutHasExpired(this._timeoutTime);
			}
		}

		// Token: 0x1700044D RID: 1101
		// (get) Token: 0x0600170E RID: 5902 RVA: 0x00078837 File Offset: 0x00076A37
		// (set) Token: 0x0600170F RID: 5903 RVA: 0x00078860 File Offset: 0x00076A60
		internal long TimeoutTime
		{
			get
			{
				if (this._timeoutMilliseconds != 0L)
				{
					this._timeoutTime = TdsParserStaticMethods.GetTimeout(this._timeoutMilliseconds);
					this._timeoutMilliseconds = 0L;
				}
				return this._timeoutTime;
			}
			set
			{
				this._timeoutMilliseconds = 0L;
				this._timeoutTime = value;
			}
		}

		// Token: 0x06001710 RID: 5904 RVA: 0x00078874 File Offset: 0x00076A74
		internal int GetTimeoutRemaining()
		{
			int result;
			if (this._timeoutMilliseconds != 0L)
			{
				result = (int)Math.Min(2147483647L, this._timeoutMilliseconds);
				this._timeoutTime = TdsParserStaticMethods.GetTimeout(this._timeoutMilliseconds);
				this._timeoutMilliseconds = 0L;
			}
			else
			{
				result = TdsParserStaticMethods.GetTimeoutMilliseconds(this._timeoutTime);
			}
			return result;
		}

		// Token: 0x06001711 RID: 5905 RVA: 0x000788C4 File Offset: 0x00076AC4
		internal bool TryStartNewRow(bool isNullCompressed, int nullBitmapColumnsCount = 0)
		{
			if (this._snapshot != null)
			{
				this._snapshot.CloneNullBitmapInfo();
			}
			if (isNullCompressed)
			{
				if (!this._nullBitmapInfo.TryInitialize(this, nullBitmapColumnsCount))
				{
					return false;
				}
			}
			else
			{
				this._nullBitmapInfo.Clean();
			}
			return true;
		}

		// Token: 0x06001712 RID: 5906 RVA: 0x000788FC File Offset: 0x00076AFC
		internal bool IsRowTokenReady()
		{
			int num = Math.Min(this._inBytesPacket, this._inBytesRead - this._inBytesUsed) - 1;
			if (num > 0)
			{
				if (this._inBuff[this._inBytesUsed] == 209)
				{
					return true;
				}
				if (this._inBuff[this._inBytesUsed] == 210)
				{
					return 1 + (this._cleanupMetaData.Length + 7) / 8 <= num;
				}
			}
			return false;
		}

		// Token: 0x06001713 RID: 5907 RVA: 0x0007896B File Offset: 0x00076B6B
		internal bool IsNullCompressionBitSet(int columnOrdinal)
		{
			return this._nullBitmapInfo.IsGuaranteedNull(columnOrdinal);
		}

		// Token: 0x06001714 RID: 5908 RVA: 0x00078979 File Offset: 0x00076B79
		internal void Activate(object owner)
		{
			this.Owner = owner;
			Interlocked.Increment(ref this._activateCount);
		}

		// Token: 0x06001715 RID: 5909 RVA: 0x00078990 File Offset: 0x00076B90
		internal void Cancel(object caller)
		{
			bool flag = false;
			try
			{
				while (!flag && this._parser.State != TdsParserState.Closed && this._parser.State != TdsParserState.Broken)
				{
					Monitor.TryEnter(this, 100, ref flag);
					if (flag && !this._cancelled && this._cancellationOwner.Target == caller)
					{
						this._cancelled = true;
						if (this._pendingData && !this._attentionSent)
						{
							bool flag2 = false;
							while (!flag2 && this._parser.State != TdsParserState.Closed && this._parser.State != TdsParserState.Broken)
							{
								try
								{
									this._parser.Connection._parserLock.Wait(false, 100, ref flag2);
									if (flag2)
									{
										this._parser.Connection.ThreadHasParserLockForClose = true;
										this.SendAttention(false);
									}
								}
								finally
								{
									if (flag2)
									{
										if (this._parser.Connection.ThreadHasParserLockForClose)
										{
											this._parser.Connection.ThreadHasParserLockForClose = false;
										}
										this._parser.Connection._parserLock.Release();
									}
								}
							}
						}
					}
				}
			}
			finally
			{
				if (flag)
				{
					Monitor.Exit(this);
				}
			}
		}

		// Token: 0x06001716 RID: 5910 RVA: 0x00078AEC File Offset: 0x00076CEC
		internal void CancelRequest()
		{
			this.ResetBuffer();
			this._outputPacketNumber = 1;
			if (!this._bulkCopyWriteTimeout)
			{
				this.SendAttention(false);
				this.Parser.ProcessPendingAck(this);
			}
		}

		// Token: 0x06001717 RID: 5911 RVA: 0x00078B18 File Offset: 0x00076D18
		public void CheckSetResetConnectionState(uint error, CallbackType callbackType)
		{
			if (this._fResetEventOwned)
			{
				if (callbackType == CallbackType.Read && error == 0U)
				{
					this._parser._fResetConnection = false;
					this._fResetConnectionSent = false;
					this._fResetEventOwned = !this._parser._resetConnectionEvent.Set();
				}
				if (error != 0U)
				{
					this._fResetConnectionSent = false;
					this._fResetEventOwned = !this._parser._resetConnectionEvent.Set();
				}
			}
		}

		// Token: 0x06001718 RID: 5912 RVA: 0x00078B8E File Offset: 0x00076D8E
		internal void CloseSession()
		{
			this.ResetCancelAndProcessAttention();
			this.Parser.PutSession(this);
		}

		// Token: 0x06001719 RID: 5913 RVA: 0x00078BA4 File Offset: 0x00076DA4
		private void ResetCancelAndProcessAttention()
		{
			lock (this)
			{
				this._cancelled = false;
				this._cancellationOwner.Target = null;
				if (this._attentionSent)
				{
					this.Parser.ProcessPendingAck(this);
				}
				this._internalTimeout = false;
			}
		}

		// Token: 0x0600171A RID: 5914
		internal abstract void CreatePhysicalSNIHandle(string serverName, bool ignoreSniOpenTimeout, long timerExpire, out byte[] instanceName, ref byte[] spnBuffer, bool flushCache, bool async, bool fParallel, bool isIntegratedSecurity = false);

		// Token: 0x0600171B RID: 5915
		internal abstract uint SniGetConnectionId(ref Guid clientConnectionId);

		// Token: 0x0600171C RID: 5916
		internal abstract bool IsFailedHandle();

		// Token: 0x0600171D RID: 5917
		protected abstract void CreateSessionHandle(TdsParserStateObject physicalConnection, bool async);

		// Token: 0x0600171E RID: 5918
		protected abstract void FreeGcHandle(int remaining, bool release);

		// Token: 0x0600171F RID: 5919
		internal abstract uint EnableSsl(ref uint info);

		// Token: 0x06001720 RID: 5920
		internal abstract uint WaitForSSLHandShakeToComplete();

		// Token: 0x06001721 RID: 5921
		internal abstract void Dispose();

		// Token: 0x06001722 RID: 5922
		internal abstract void DisposePacketCache();

		// Token: 0x06001723 RID: 5923
		internal abstract bool IsPacketEmpty(object readPacket);

		// Token: 0x06001724 RID: 5924
		internal abstract object ReadSyncOverAsync(int timeoutRemaining, bool isMarsOn, out uint error);

		// Token: 0x06001725 RID: 5925
		internal abstract object ReadAsync(out uint error, ref object handle);

		// Token: 0x06001726 RID: 5926
		internal abstract uint CheckConnection();

		// Token: 0x06001727 RID: 5927
		internal abstract uint SetConnectionBufferSize(ref uint unsignedPacketSize);

		// Token: 0x06001728 RID: 5928
		internal abstract void ReleasePacket(object syncReadPacket);

		// Token: 0x06001729 RID: 5929
		protected abstract uint SNIPacketGetData(object packet, byte[] _inBuff, ref uint dataSize);

		// Token: 0x0600172A RID: 5930
		internal abstract object GetResetWritePacket();

		// Token: 0x0600172B RID: 5931
		internal abstract void ClearAllWritePackets();

		// Token: 0x0600172C RID: 5932
		internal abstract object AddPacketToPendingList(object packet);

		// Token: 0x0600172D RID: 5933
		protected abstract void RemovePacketFromPendingList(object pointer);

		// Token: 0x0600172E RID: 5934
		internal abstract uint GenerateSspiClientContext(byte[] receivedBuff, uint receivedLength, ref byte[] sendBuff, ref uint sendLength, byte[] _sniSpnBuffer);

		// Token: 0x0600172F RID: 5935 RVA: 0x00078C0C File Offset: 0x00076E0C
		internal bool Deactivate()
		{
			bool result = false;
			try
			{
				TdsParserState state = this.Parser.State;
				if (state != TdsParserState.Broken && state != TdsParserState.Closed)
				{
					if (this._pendingData)
					{
						this.Parser.DrainData(this);
					}
					if (this.HasOpenResult)
					{
						this.DecrementOpenResultCount();
					}
					this.ResetCancelAndProcessAttention();
					result = true;
				}
			}
			catch (Exception e)
			{
				if (!ADP.IsCatchableExceptionType(e))
				{
					throw;
				}
			}
			return result;
		}

		// Token: 0x06001730 RID: 5936 RVA: 0x00078C78 File Offset: 0x00076E78
		internal void RemoveOwner()
		{
			if (this._parser.MARSOn)
			{
				Interlocked.Decrement(ref this._activateCount);
			}
			this.Owner = null;
		}

		// Token: 0x06001731 RID: 5937 RVA: 0x00078C9A File Offset: 0x00076E9A
		internal void DecrementOpenResultCount()
		{
			if (this._executedUnderTransaction == null)
			{
				this._parser.DecrementNonTransactedOpenResultCount();
			}
			else
			{
				this._executedUnderTransaction.DecrementAndObtainOpenResultCount();
				this._executedUnderTransaction = null;
			}
			this._hasOpenResult = false;
		}

		// Token: 0x06001732 RID: 5938 RVA: 0x00078CCC File Offset: 0x00076ECC
		internal int DecrementPendingCallbacks(bool release)
		{
			int num = Interlocked.Decrement(ref this._pendingCallbacks);
			this.FreeGcHandle(num, release);
			return num;
		}

		// Token: 0x06001733 RID: 5939 RVA: 0x00078CF0 File Offset: 0x00076EF0
		internal void DisposeCounters()
		{
			Timer networkPacketTimeout = this._networkPacketTimeout;
			if (networkPacketTimeout != null)
			{
				this._networkPacketTimeout = null;
				networkPacketTimeout.Dispose();
			}
			if (Volatile.Read(ref this._readingCount) > 0)
			{
				SpinWait.SpinUntil(() => Volatile.Read(ref this._readingCount) == 0);
			}
		}

		// Token: 0x06001734 RID: 5940 RVA: 0x00078D33 File Offset: 0x00076F33
		internal int IncrementAndObtainOpenResultCount(SqlInternalTransaction transaction)
		{
			this._hasOpenResult = true;
			if (transaction == null)
			{
				return this._parser.IncrementNonTransactedOpenResultCount();
			}
			this._executedUnderTransaction = transaction;
			return transaction.IncrementAndObtainOpenResultCount();
		}

		// Token: 0x06001735 RID: 5941 RVA: 0x00078D58 File Offset: 0x00076F58
		internal int IncrementPendingCallbacks()
		{
			return Interlocked.Increment(ref this._pendingCallbacks);
		}

		// Token: 0x06001736 RID: 5942 RVA: 0x00078D65 File Offset: 0x00076F65
		internal void SetTimeoutSeconds(int timeout)
		{
			this.SetTimeoutMilliseconds((long)timeout * 1000L);
		}

		// Token: 0x06001737 RID: 5943 RVA: 0x00078D76 File Offset: 0x00076F76
		internal void SetTimeoutMilliseconds(long timeout)
		{
			if (timeout <= 0L)
			{
				this._timeoutMilliseconds = 0L;
				this._timeoutTime = long.MaxValue;
				return;
			}
			this._timeoutMilliseconds = timeout;
			this._timeoutTime = 0L;
		}

		// Token: 0x06001738 RID: 5944 RVA: 0x00078DA4 File Offset: 0x00076FA4
		internal void StartSession(object cancellationOwner)
		{
			this._cancellationOwner.Target = cancellationOwner;
		}

		// Token: 0x06001739 RID: 5945 RVA: 0x00078DB2 File Offset: 0x00076FB2
		internal void ThrowExceptionAndWarning(bool callerHasConnectionLock = false, bool asyncClose = false)
		{
			this._parser.ThrowExceptionAndWarning(this, callerHasConnectionLock, asyncClose);
		}

		// Token: 0x0600173A RID: 5946 RVA: 0x00078DC4 File Offset: 0x00076FC4
		internal Task ExecuteFlush()
		{
			Task result;
			lock (this)
			{
				if (this._cancelled && 1 == this._outputPacketNumber)
				{
					this.ResetBuffer();
					this._cancelled = false;
					throw SQL.OperationCancelled();
				}
				Task task = this.WritePacket(1, false);
				if (task == null)
				{
					this._pendingData = true;
					this._messageStatus = 0;
					result = null;
				}
				else
				{
					result = AsyncHelper.CreateContinuationTask(task, delegate()
					{
						this._pendingData = true;
						this._messageStatus = 0;
					}, null, null);
				}
			}
			return result;
		}

		// Token: 0x0600173B RID: 5947 RVA: 0x00078E54 File Offset: 0x00077054
		internal bool TryProcessHeader()
		{
			if (this._partialHeaderBytesRead > 0 || this._inBytesUsed + this._inputHeaderLen > this._inBytesRead)
			{
				for (;;)
				{
					int num = Math.Min(this._inBytesRead - this._inBytesUsed, this._inputHeaderLen - this._partialHeaderBytesRead);
					Buffer.BlockCopy(this._inBuff, this._inBytesUsed, this._partialHeaderBuffer, this._partialHeaderBytesRead, num);
					this._partialHeaderBytesRead += num;
					this._inBytesUsed += num;
					if (this._partialHeaderBytesRead == this._inputHeaderLen)
					{
						this._partialHeaderBytesRead = 0;
						this._inBytesPacket = ((int)this._partialHeaderBuffer[2] << 8 | (int)this._partialHeaderBuffer[3]) - this._inputHeaderLen;
						this._messageStatus = this._partialHeaderBuffer[1];
					}
					else
					{
						if (this._parser.State == TdsParserState.Broken || this._parser.State == TdsParserState.Closed)
						{
							break;
						}
						if (!this.TryReadNetworkPacket())
						{
							return false;
						}
						if (this._internalTimeout)
						{
							goto Block_5;
						}
					}
					if (this._partialHeaderBytesRead == 0)
					{
						goto Block_6;
					}
				}
				this.ThrowExceptionAndWarning(false, false);
				return true;
				Block_5:
				this.ThrowExceptionAndWarning(false, false);
				return true;
				Block_6:;
			}
			else
			{
				this._messageStatus = this._inBuff[this._inBytesUsed + 1];
				this._inBytesPacket = ((int)this._inBuff[this._inBytesUsed + 2] << 8 | (int)this._inBuff[this._inBytesUsed + 2 + 1]) - this._inputHeaderLen;
				this._inBytesUsed += this._inputHeaderLen;
			}
			if (this._inBytesPacket < 0)
			{
				throw SQL.ParsingError();
			}
			return true;
		}

		// Token: 0x0600173C RID: 5948 RVA: 0x00078FD8 File Offset: 0x000771D8
		internal bool TryPrepareBuffer()
		{
			if (this._inBytesPacket == 0 && this._inBytesUsed < this._inBytesRead && !this.TryProcessHeader())
			{
				return false;
			}
			if (this._inBytesUsed == this._inBytesRead)
			{
				if (this._inBytesPacket > 0)
				{
					if (!this.TryReadNetworkPacket())
					{
						return false;
					}
				}
				else if (this._inBytesPacket == 0)
				{
					if (!this.TryReadNetworkPacket())
					{
						return false;
					}
					if (!this.TryProcessHeader())
					{
						return false;
					}
					if (this._inBytesUsed == this._inBytesRead && !this.TryReadNetworkPacket())
					{
						return false;
					}
				}
			}
			return true;
		}

		// Token: 0x0600173D RID: 5949 RVA: 0x0007905B File Offset: 0x0007725B
		internal void ResetBuffer()
		{
			this._outBytesUsed = this._outputHeaderLen;
		}

		// Token: 0x0600173E RID: 5950 RVA: 0x0007906C File Offset: 0x0007726C
		internal bool SetPacketSize(int size)
		{
			if (size > 32768)
			{
				throw SQL.InvalidPacketSize();
			}
			if (this._inBuff == null || this._inBuff.Length != size)
			{
				if (this._inBuff == null)
				{
					this._inBuff = new byte[size];
					this._inBytesRead = 0;
					this._inBytesUsed = 0;
				}
				else if (size != this._inBuff.Length)
				{
					if (this._inBytesRead > this._inBytesUsed)
					{
						byte[] inBuff = this._inBuff;
						this._inBuff = new byte[size];
						int num = this._inBytesRead - this._inBytesUsed;
						if (inBuff.Length < this._inBytesUsed + num || this._inBuff.Length < num)
						{
							throw SQL.InvalidInternalPacketSize(string.Concat(new object[]
							{
								SR.GetString("Invalid internal packet size:"),
								" ",
								inBuff.Length,
								", ",
								this._inBytesUsed,
								", ",
								num,
								", ",
								this._inBuff.Length
							}));
						}
						Buffer.BlockCopy(inBuff, this._inBytesUsed, this._inBuff, 0, num);
						this._inBytesRead -= this._inBytesUsed;
						this._inBytesUsed = 0;
					}
					else
					{
						this._inBuff = new byte[size];
						this._inBytesRead = 0;
						this._inBytesUsed = 0;
					}
				}
				this._outBuff = new byte[size];
				this._outBytesUsed = this._outputHeaderLen;
				return true;
			}
			return false;
		}

		// Token: 0x0600173F RID: 5951 RVA: 0x000791F2 File Offset: 0x000773F2
		internal bool TryPeekByte(out byte value)
		{
			if (!this.TryReadByte(out value))
			{
				return false;
			}
			this._inBytesPacket++;
			this._inBytesUsed--;
			return true;
		}

		// Token: 0x06001740 RID: 5952 RVA: 0x0007921C File Offset: 0x0007741C
		public bool TryReadByteArray(byte[] buff, int offset, int len)
		{
			int num;
			return this.TryReadByteArray(buff, offset, len, out num);
		}

		// Token: 0x06001741 RID: 5953 RVA: 0x00079234 File Offset: 0x00077434
		public bool TryReadByteArray(byte[] buff, int offset, int len, out int totalRead)
		{
			totalRead = 0;
			while (len > 0)
			{
				if ((this._inBytesPacket == 0 || this._inBytesUsed == this._inBytesRead) && !this.TryPrepareBuffer())
				{
					return false;
				}
				int num = Math.Min(len, Math.Min(this._inBytesPacket, this._inBytesRead - this._inBytesUsed));
				if (buff != null)
				{
					Buffer.BlockCopy(this._inBuff, this._inBytesUsed, buff, offset + totalRead, num);
				}
				totalRead += num;
				this._inBytesUsed += num;
				this._inBytesPacket -= num;
				len -= num;
			}
			return this._messageStatus == 1 || (this._inBytesPacket != 0 && this._inBytesUsed != this._inBytesRead) || this.TryPrepareBuffer();
		}

		// Token: 0x06001742 RID: 5954 RVA: 0x00079300 File Offset: 0x00077500
		internal bool TryReadByte(out byte value)
		{
			value = 0;
			if ((this._inBytesPacket == 0 || this._inBytesUsed == this._inBytesRead) && !this.TryPrepareBuffer())
			{
				return false;
			}
			this._inBytesPacket--;
			byte[] inBuff = this._inBuff;
			int inBytesUsed = this._inBytesUsed;
			this._inBytesUsed = inBytesUsed + 1;
			value = inBuff[inBytesUsed];
			return true;
		}

		// Token: 0x06001743 RID: 5955 RVA: 0x0007935C File Offset: 0x0007755C
		internal bool TryReadChar(out char value)
		{
			byte[] array;
			int num;
			if (this._inBytesUsed + 2 > this._inBytesRead || this._inBytesPacket < 2)
			{
				if (!this.TryReadByteArray(this._bTmp, 0, 2))
				{
					value = '\0';
					return false;
				}
				array = this._bTmp;
				num = 0;
			}
			else
			{
				array = this._inBuff;
				num = this._inBytesUsed;
				this._inBytesUsed += 2;
				this._inBytesPacket -= 2;
			}
			value = (char)(((int)array[num + 1] << 8) + (int)array[num]);
			return true;
		}

		// Token: 0x06001744 RID: 5956 RVA: 0x000793DC File Offset: 0x000775DC
		internal bool TryReadInt16(out short value)
		{
			byte[] array;
			int num;
			if (this._inBytesUsed + 2 > this._inBytesRead || this._inBytesPacket < 2)
			{
				if (!this.TryReadByteArray(this._bTmp, 0, 2))
				{
					value = 0;
					return false;
				}
				array = this._bTmp;
				num = 0;
			}
			else
			{
				array = this._inBuff;
				num = this._inBytesUsed;
				this._inBytesUsed += 2;
				this._inBytesPacket -= 2;
			}
			value = (short)(((int)array[num + 1] << 8) + (int)array[num]);
			return true;
		}

		// Token: 0x06001745 RID: 5957 RVA: 0x0007945C File Offset: 0x0007765C
		internal bool TryReadInt32(out int value)
		{
			if (this._inBytesUsed + 4 <= this._inBytesRead && this._inBytesPacket >= 4)
			{
				value = BitConverter.ToInt32(this._inBuff, this._inBytesUsed);
				this._inBytesUsed += 4;
				this._inBytesPacket -= 4;
				return true;
			}
			if (!this.TryReadByteArray(this._bTmp, 0, 4))
			{
				value = 0;
				return false;
			}
			value = BitConverter.ToInt32(this._bTmp, 0);
			return true;
		}

		// Token: 0x06001746 RID: 5958 RVA: 0x000794D8 File Offset: 0x000776D8
		internal bool TryReadInt64(out long value)
		{
			if ((this._inBytesPacket == 0 || this._inBytesUsed == this._inBytesRead) && !this.TryPrepareBuffer())
			{
				value = 0L;
				return false;
			}
			if (this._bTmpRead <= 0 && this._inBytesUsed + 8 <= this._inBytesRead && this._inBytesPacket >= 8)
			{
				value = BitConverter.ToInt64(this._inBuff, this._inBytesUsed);
				this._inBytesUsed += 8;
				this._inBytesPacket -= 8;
				return true;
			}
			int num = 0;
			if (!this.TryReadByteArray(this._bTmp, this._bTmpRead, 8 - this._bTmpRead, out num))
			{
				this._bTmpRead += num;
				value = 0L;
				return false;
			}
			this._bTmpRead = 0;
			value = BitConverter.ToInt64(this._bTmp, 0);
			return true;
		}

		// Token: 0x06001747 RID: 5959 RVA: 0x000795A8 File Offset: 0x000777A8
		internal bool TryReadUInt16(out ushort value)
		{
			byte[] array;
			int num;
			if (this._inBytesUsed + 2 > this._inBytesRead || this._inBytesPacket < 2)
			{
				if (!this.TryReadByteArray(this._bTmp, 0, 2))
				{
					value = 0;
					return false;
				}
				array = this._bTmp;
				num = 0;
			}
			else
			{
				array = this._inBuff;
				num = this._inBytesUsed;
				this._inBytesUsed += 2;
				this._inBytesPacket -= 2;
			}
			value = (ushort)(((int)array[num + 1] << 8) + (int)array[num]);
			return true;
		}

		// Token: 0x06001748 RID: 5960 RVA: 0x00079628 File Offset: 0x00077828
		internal bool TryReadUInt32(out uint value)
		{
			if ((this._inBytesPacket == 0 || this._inBytesUsed == this._inBytesRead) && !this.TryPrepareBuffer())
			{
				value = 0U;
				return false;
			}
			if (this._bTmpRead <= 0 && this._inBytesUsed + 4 <= this._inBytesRead && this._inBytesPacket >= 4)
			{
				value = BitConverter.ToUInt32(this._inBuff, this._inBytesUsed);
				this._inBytesUsed += 4;
				this._inBytesPacket -= 4;
				return true;
			}
			int num = 0;
			if (!this.TryReadByteArray(this._bTmp, this._bTmpRead, 4 - this._bTmpRead, out num))
			{
				this._bTmpRead += num;
				value = 0U;
				return false;
			}
			this._bTmpRead = 0;
			value = BitConverter.ToUInt32(this._bTmp, 0);
			return true;
		}

		// Token: 0x06001749 RID: 5961 RVA: 0x000796F4 File Offset: 0x000778F4
		internal bool TryReadSingle(out float value)
		{
			if (this._inBytesUsed + 4 <= this._inBytesRead && this._inBytesPacket >= 4)
			{
				value = BitConverter.ToSingle(this._inBuff, this._inBytesUsed);
				this._inBytesUsed += 4;
				this._inBytesPacket -= 4;
				return true;
			}
			if (!this.TryReadByteArray(this._bTmp, 0, 4))
			{
				value = 0f;
				return false;
			}
			value = BitConverter.ToSingle(this._bTmp, 0);
			return true;
		}

		// Token: 0x0600174A RID: 5962 RVA: 0x00079774 File Offset: 0x00077974
		internal bool TryReadDouble(out double value)
		{
			if (this._inBytesUsed + 8 <= this._inBytesRead && this._inBytesPacket >= 8)
			{
				value = BitConverter.ToDouble(this._inBuff, this._inBytesUsed);
				this._inBytesUsed += 8;
				this._inBytesPacket -= 8;
				return true;
			}
			if (!this.TryReadByteArray(this._bTmp, 0, 8))
			{
				value = 0.0;
				return false;
			}
			value = BitConverter.ToDouble(this._bTmp, 0);
			return true;
		}

		// Token: 0x0600174B RID: 5963 RVA: 0x000797F8 File Offset: 0x000779F8
		internal bool TryReadString(int length, out string value)
		{
			int num = length << 1;
			int index = 0;
			byte[] bytes;
			if (this._inBytesUsed + num > this._inBytesRead || this._inBytesPacket < num)
			{
				if (this._bTmp == null || this._bTmp.Length < num)
				{
					this._bTmp = new byte[num];
				}
				if (!this.TryReadByteArray(this._bTmp, 0, num))
				{
					value = null;
					return false;
				}
				bytes = this._bTmp;
			}
			else
			{
				bytes = this._inBuff;
				index = this._inBytesUsed;
				this._inBytesUsed += num;
				this._inBytesPacket -= num;
			}
			value = Encoding.Unicode.GetString(bytes, index, num);
			return true;
		}

		// Token: 0x0600174C RID: 5964 RVA: 0x0007989C File Offset: 0x00077A9C
		internal bool TryReadStringWithEncoding(int length, Encoding encoding, bool isPlp, out string value)
		{
			if (encoding == null)
			{
				if (isPlp)
				{
					ulong num;
					if (!this._parser.TrySkipPlpValue((ulong)((long)length), this, out num))
					{
						value = null;
						return false;
					}
				}
				else if (!this.TrySkipBytes(length))
				{
					value = null;
					return false;
				}
				this._parser.ThrowUnsupportedCollationEncountered(this);
			}
			byte[] bytes = null;
			int index = 0;
			if (isPlp)
			{
				if (!this.TryReadPlpBytes(ref bytes, 0, 2147483647, out length))
				{
					value = null;
					return false;
				}
			}
			else if (this._inBytesUsed + length > this._inBytesRead || this._inBytesPacket < length)
			{
				if (this._bTmp == null || this._bTmp.Length < length)
				{
					this._bTmp = new byte[length];
				}
				if (!this.TryReadByteArray(this._bTmp, 0, length))
				{
					value = null;
					return false;
				}
				bytes = this._bTmp;
			}
			else
			{
				bytes = this._inBuff;
				index = this._inBytesUsed;
				this._inBytesUsed += length;
				this._inBytesPacket -= length;
			}
			value = encoding.GetString(bytes, index, length);
			return true;
		}

		// Token: 0x0600174D RID: 5965 RVA: 0x00079994 File Offset: 0x00077B94
		internal ulong ReadPlpLength(bool returnPlpNullIfNull)
		{
			ulong result;
			if (!this.TryReadPlpLength(returnPlpNullIfNull, out result))
			{
				throw SQL.SynchronousCallMayNotPend();
			}
			return result;
		}

		// Token: 0x0600174E RID: 5966 RVA: 0x000799B4 File Offset: 0x00077BB4
		internal bool TryReadPlpLength(bool returnPlpNullIfNull, out ulong lengthLeft)
		{
			bool flag = false;
			if (this._longlen == 0UL)
			{
				long longlen;
				if (!this.TryReadInt64(out longlen))
				{
					lengthLeft = 0UL;
					return false;
				}
				this._longlen = (ulong)longlen;
			}
			if (this._longlen == 18446744073709551615UL)
			{
				this._longlen = 0UL;
				this._longlenleft = 0UL;
				flag = true;
			}
			else
			{
				uint num;
				if (!this.TryReadUInt32(out num))
				{
					lengthLeft = 0UL;
					return false;
				}
				if (num == 0U)
				{
					this._longlenleft = 0UL;
					this._longlen = 0UL;
				}
				else
				{
					this._longlenleft = (ulong)num;
				}
			}
			if (flag && returnPlpNullIfNull)
			{
				lengthLeft = ulong.MaxValue;
				return true;
			}
			lengthLeft = this._longlenleft;
			return true;
		}

		// Token: 0x0600174F RID: 5967 RVA: 0x00079A44 File Offset: 0x00077C44
		internal int ReadPlpBytesChunk(byte[] buff, int offset, int len)
		{
			int num = (int)Math.Min(this._longlenleft, (ulong)((long)len));
			int result;
			bool flag = this.TryReadByteArray(buff, offset, num, out result);
			this._longlenleft -= (ulong)((long)num);
			if (!flag)
			{
				throw SQL.SynchronousCallMayNotPend();
			}
			return result;
		}

		// Token: 0x06001750 RID: 5968 RVA: 0x00079A84 File Offset: 0x00077C84
		internal bool TryReadPlpBytes(ref byte[] buff, int offset, int len, out int totalBytesRead)
		{
			int num = 0;
			if (this._longlen == 0UL)
			{
				if (buff == null)
				{
					buff = Array.Empty<byte>();
				}
				totalBytesRead = 0;
				return true;
			}
			int i = len;
			if (buff == null && this._longlen != 18446744073709551614UL)
			{
				buff = new byte[Math.Min((int)this._longlen, len)];
			}
			if (this._longlenleft == 0UL)
			{
				ulong num2;
				if (!this.TryReadPlpLength(false, out num2))
				{
					totalBytesRead = 0;
					return false;
				}
				if (this._longlenleft == 0UL)
				{
					totalBytesRead = 0;
					return true;
				}
			}
			if (buff == null)
			{
				buff = new byte[this._longlenleft];
			}
			totalBytesRead = 0;
			while (i > 0)
			{
				int num3 = (int)Math.Min(this._longlenleft, (ulong)((long)i));
				if (buff.Length < offset + num3)
				{
					byte[] array = new byte[offset + num3];
					Buffer.BlockCopy(buff, 0, array, 0, offset);
					buff = array;
				}
				bool flag = this.TryReadByteArray(buff, offset, num3, out num);
				i -= num;
				offset += num;
				totalBytesRead += num;
				this._longlenleft -= (ulong)((long)num);
				if (!flag)
				{
					return false;
				}
				ulong num2;
				if (this._longlenleft == 0UL && !this.TryReadPlpLength(false, out num2))
				{
					return false;
				}
				if (this._longlenleft == 0UL)
				{
					break;
				}
			}
			return true;
		}

		// Token: 0x06001751 RID: 5969 RVA: 0x00079B9C File Offset: 0x00077D9C
		internal bool TrySkipLongBytes(long num)
		{
			while (num > 0L)
			{
				int num2 = (int)Math.Min(2147483647L, num);
				if (!this.TryReadByteArray(null, 0, num2))
				{
					return false;
				}
				num -= (long)num2;
			}
			return true;
		}

		// Token: 0x06001752 RID: 5970 RVA: 0x00079BD4 File Offset: 0x00077DD4
		internal bool TrySkipBytes(int num)
		{
			return this.TryReadByteArray(null, 0, num);
		}

		// Token: 0x06001753 RID: 5971 RVA: 0x00079BDF File Offset: 0x00077DDF
		internal void SetSnapshot()
		{
			this._snapshot = new TdsParserStateObject.StateSnapshot(this);
			this._snapshot.Snap();
			this._snapshotReplay = false;
		}

		// Token: 0x06001754 RID: 5972 RVA: 0x00079BFF File Offset: 0x00077DFF
		internal void ResetSnapshot()
		{
			this._snapshot = null;
			this._snapshotReplay = false;
		}

		// Token: 0x06001755 RID: 5973 RVA: 0x00079C10 File Offset: 0x00077E10
		internal bool TryReadNetworkPacket()
		{
			if (this._snapshot != null)
			{
				if (this._snapshotReplay && this._snapshot.Replay())
				{
					return true;
				}
				this._inBuff = new byte[this._inBuff.Length];
			}
			if (this._syncOverAsync)
			{
				this.ReadSniSyncOverAsync();
				return true;
			}
			this.ReadSni(new TaskCompletionSource<object>());
			return false;
		}

		// Token: 0x06001756 RID: 5974 RVA: 0x00079C6B File Offset: 0x00077E6B
		internal void PrepareReplaySnapshot()
		{
			this._networkPacketTaskSource = null;
			this._snapshot.PrepareReplay();
		}

		// Token: 0x06001757 RID: 5975 RVA: 0x00079C80 File Offset: 0x00077E80
		internal void ReadSniSyncOverAsync()
		{
			if (this._parser.State == TdsParserState.Broken || this._parser.State == TdsParserState.Closed)
			{
				throw ADP.ClosedConnectionError();
			}
			object obj = null;
			bool flag = false;
			try
			{
				Interlocked.Increment(ref this._readingCount);
				flag = true;
				uint num;
				obj = this.ReadSyncOverAsync(this.GetTimeoutRemaining(), false, out num);
				Interlocked.Decrement(ref this._readingCount);
				flag = false;
				if (this._parser.MARSOn)
				{
					this.CheckSetResetConnectionState(num, CallbackType.Read);
				}
				if (num == 0U)
				{
					this.ProcessSniPacket(obj, 0U);
				}
				else
				{
					this.ReadSniError(this, num);
				}
			}
			finally
			{
				if (flag)
				{
					Interlocked.Decrement(ref this._readingCount);
				}
				if (!this.IsPacketEmpty(obj))
				{
					this.ReleasePacket(obj);
				}
			}
		}

		// Token: 0x06001758 RID: 5976 RVA: 0x00079D3C File Offset: 0x00077F3C
		internal void OnConnectionClosed()
		{
			this.Parser.State = TdsParserState.Broken;
			this.Parser.Connection.BreakConnection();
			Interlocked.MemoryBarrier();
			TaskCompletionSource<object> taskCompletionSource = this._networkPacketTaskSource;
			if (taskCompletionSource != null)
			{
				taskCompletionSource.TrySetException(ADP.ExceptionWithStackTrace(ADP.ClosedConnectionError()));
			}
			taskCompletionSource = this._writeCompletionSource;
			if (taskCompletionSource != null)
			{
				taskCompletionSource.TrySetException(ADP.ExceptionWithStackTrace(ADP.ClosedConnectionError()));
			}
		}

		// Token: 0x06001759 RID: 5977 RVA: 0x00079DA4 File Offset: 0x00077FA4
		private void OnTimeout(object state)
		{
			if (!this._internalTimeout)
			{
				this._internalTimeout = true;
				lock (this)
				{
					if (!this._attentionSent)
					{
						this.AddError(new SqlError(-2, 0, 11, this._parser.Server, this._parser.Connection.TimeoutErrorInternal.GetErrorMessage(), "", 0, 258U, null));
						TaskCompletionSource<object> source = this._networkPacketTaskSource;
						if (this._parser.Connection.IsInPool)
						{
							this._parser.State = TdsParserState.Broken;
							this._parser.Connection.BreakConnection();
							if (source != null)
							{
								source.TrySetCanceled();
							}
						}
						else if (this._parser.State == TdsParserState.OpenLoggedIn)
						{
							try
							{
								this.SendAttention(true);
							}
							catch (Exception e)
							{
								if (!ADP.IsCatchableExceptionType(e))
								{
									throw;
								}
								if (source != null)
								{
									source.TrySetCanceled();
								}
							}
						}
						if (source != null)
						{
							Task.Delay(5000).ContinueWith(delegate(Task _)
							{
								if (!source.Task.IsCompleted)
								{
									int num = this.IncrementPendingCallbacks();
									try
									{
										if (num == 3 && !source.Task.IsCompleted)
										{
											bool flag2 = false;
											try
											{
												this.CheckThrowSNIException();
											}
											catch (Exception exception)
											{
												if (source.TrySetException(exception))
												{
													flag2 = true;
												}
											}
											this._parser.State = TdsParserState.Broken;
											this._parser.Connection.BreakConnection();
											if (!flag2)
											{
												source.TrySetCanceled();
											}
										}
									}
									finally
									{
										this.DecrementPendingCallbacks(false);
									}
								}
							});
						}
					}
				}
			}
		}

		// Token: 0x0600175A RID: 5978 RVA: 0x00079F10 File Offset: 0x00078110
		internal void ReadSni(TaskCompletionSource<object> completion)
		{
			this._networkPacketTaskSource = completion;
			Interlocked.MemoryBarrier();
			if (this._parser.State == TdsParserState.Broken || this._parser.State == TdsParserState.Closed)
			{
				throw ADP.ClosedConnectionError();
			}
			object obj = null;
			uint num = 0U;
			try
			{
				if (this._networkPacketTimeout == null)
				{
					this._networkPacketTimeout = new Timer(new TimerCallback(this.OnTimeout), null, -1, -1);
				}
				int timeoutRemaining = this.GetTimeoutRemaining();
				if (timeoutRemaining > 0)
				{
					this.ChangeNetworkPacketTimeout(timeoutRemaining, -1);
				}
				object obj2 = null;
				Interlocked.Increment(ref this._readingCount);
				obj2 = this.SessionHandle;
				if (obj2 != null)
				{
					this.IncrementPendingCallbacks();
					obj = this.ReadAsync(out num, ref obj2);
					if (num != 0U && 997U != num)
					{
						this.DecrementPendingCallbacks(false);
					}
				}
				Interlocked.Decrement(ref this._readingCount);
				if (obj2 == null)
				{
					throw ADP.ClosedConnectionError();
				}
				if (num == 0U)
				{
					this.ReadAsyncCallback<object>(IntPtr.Zero, obj, 0U);
				}
				else if (997U != num)
				{
					this.ReadSniError(this, num);
					this._networkPacketTaskSource.TrySetResult(null);
					this.ChangeNetworkPacketTimeout(-1, -1);
				}
				else if (timeoutRemaining == 0)
				{
					this.ChangeNetworkPacketTimeout(0, -1);
				}
			}
			finally
			{
				if (!TdsParserStateObjectFactory.UseManagedSNI && !this.IsPacketEmpty(obj))
				{
					this.ReleasePacket(obj);
				}
			}
		}

		// Token: 0x0600175B RID: 5979 RVA: 0x0007A044 File Offset: 0x00078244
		internal bool IsConnectionAlive(bool throwOnException)
		{
			bool result = true;
			if (DateTime.UtcNow.Ticks - this._lastSuccessfulIOTimer._value > 50000L)
			{
				if (this._parser == null || this._parser.State == TdsParserState.Broken || this._parser.State == TdsParserState.Closed)
				{
					result = false;
					if (throwOnException)
					{
						throw SQL.ConnectionDoomed();
					}
				}
				else if (this._pendingCallbacks <= 1 && (this._parser.Connection == null || this._parser.Connection.IsInPool))
				{
					object emptyReadPacket = this.EmptyReadPacket;
					try
					{
						this.SniContext = SniContext.Snix_Connect;
						uint num = this.CheckConnection();
						if (num != 0U && num != 258U)
						{
							result = false;
							if (throwOnException)
							{
								this.AddError(this._parser.ProcessSNIError(this));
								this.ThrowExceptionAndWarning(false, false);
							}
						}
						else
						{
							this._lastSuccessfulIOTimer._value = DateTime.UtcNow.Ticks;
						}
					}
					finally
					{
						if (!this.IsPacketEmpty(emptyReadPacket))
						{
							this.ReleasePacket(emptyReadPacket);
						}
					}
				}
			}
			return result;
		}

		// Token: 0x0600175C RID: 5980 RVA: 0x0007A150 File Offset: 0x00078350
		internal bool ValidateSNIConnection()
		{
			if (this._parser == null || this._parser.State == TdsParserState.Broken || this._parser.State == TdsParserState.Closed)
			{
				return false;
			}
			if (DateTime.UtcNow.Ticks - this._lastSuccessfulIOTimer._value <= 50000L)
			{
				return true;
			}
			uint num = 0U;
			this.SniContext = SniContext.Snix_Connect;
			try
			{
				Interlocked.Increment(ref this._readingCount);
				num = this.CheckConnection();
			}
			finally
			{
				Interlocked.Decrement(ref this._readingCount);
			}
			return num == 0U || num == 258U;
		}

		// Token: 0x0600175D RID: 5981 RVA: 0x0007A1F0 File Offset: 0x000783F0
		private void ReadSniError(TdsParserStateObject stateObj, uint error)
		{
			if (258U == error)
			{
				bool flag = false;
				if (this._internalTimeout)
				{
					flag = true;
				}
				else
				{
					stateObj._internalTimeout = true;
					this.AddError(new SqlError(-2, 0, 11, this._parser.Server, this._parser.Connection.TimeoutErrorInternal.GetErrorMessage(), "", 0, 258U, null));
					if (!stateObj._attentionSent)
					{
						if (stateObj.Parser.State == TdsParserState.OpenLoggedIn)
						{
							stateObj.SendAttention(true);
							object obj = null;
							bool flag2 = false;
							try
							{
								Interlocked.Increment(ref this._readingCount);
								flag2 = true;
								obj = this.ReadSyncOverAsync(stateObj.GetTimeoutRemaining(), this._parser.MARSOn, out error);
								Interlocked.Decrement(ref this._readingCount);
								flag2 = false;
								if (error == 0U)
								{
									stateObj.ProcessSniPacket(obj, 0U);
									return;
								}
								flag = true;
								goto IL_13D;
							}
							finally
							{
								if (flag2)
								{
									Interlocked.Decrement(ref this._readingCount);
								}
								if (!this.IsPacketEmpty(obj))
								{
									this.ReleasePacket(obj);
								}
							}
						}
						if (this._parser._loginWithFailover)
						{
							this._parser.Disconnect();
						}
						else if (this._parser.State == TdsParserState.OpenNotLoggedIn && this._parser.Connection.ConnectionOptions.MultiSubnetFailover)
						{
							this._parser.Disconnect();
						}
						else
						{
							flag = true;
						}
					}
				}
				IL_13D:
				if (flag)
				{
					this._parser.State = TdsParserState.Broken;
					this._parser.Connection.BreakConnection();
				}
			}
			else
			{
				this.AddError(this._parser.ProcessSNIError(stateObj));
			}
			this.ThrowExceptionAndWarning(false, false);
		}

		// Token: 0x0600175E RID: 5982 RVA: 0x0007A388 File Offset: 0x00078588
		public void ProcessSniPacket(object packet, uint error)
		{
			if (error != 0U)
			{
				if (this._parser.State == TdsParserState.Closed || this._parser.State == TdsParserState.Broken)
				{
					return;
				}
				this.AddError(this._parser.ProcessSNIError(this));
				return;
			}
			else
			{
				uint num = 0U;
				if (this.SNIPacketGetData(packet, this._inBuff, ref num) != 0U)
				{
					throw SQL.ParsingError();
				}
				if ((long)this._inBuff.Length < (long)((ulong)num))
				{
					throw SQL.InvalidInternalPacketSize(SR.GetString("Invalid array size."));
				}
				this._lastSuccessfulIOTimer._value = DateTime.UtcNow.Ticks;
				this._inBytesRead = (int)num;
				this._inBytesUsed = 0;
				if (this._snapshot != null)
				{
					this._snapshot.PushBuffer(this._inBuff, this._inBytesRead);
					if (this._snapshotReplay)
					{
						this._snapshot.Replay();
					}
				}
				this.SniReadStatisticsAndTracing();
				return;
			}
		}

		// Token: 0x0600175F RID: 5983 RVA: 0x0007A45C File Offset: 0x0007865C
		private void ChangeNetworkPacketTimeout(int dueTime, int period)
		{
			Timer networkPacketTimeout = this._networkPacketTimeout;
			if (networkPacketTimeout != null)
			{
				try
				{
					networkPacketTimeout.Change(dueTime, period);
				}
				catch (ObjectDisposedException)
				{
				}
			}
		}

		// Token: 0x06001760 RID: 5984 RVA: 0x0007A494 File Offset: 0x00078694
		public void ReadAsyncCallback<T>(T packet, uint error)
		{
			this.ReadAsyncCallback<T>(IntPtr.Zero, packet, error);
		}

		// Token: 0x06001761 RID: 5985 RVA: 0x0007A4A4 File Offset: 0x000786A4
		public void ReadAsyncCallback<T>(IntPtr key, T packet, uint error)
		{
			TaskCompletionSource<object> source = this._networkPacketTaskSource;
			if (source == null && this._parser._pMarsPhysicalConObj == this)
			{
				return;
			}
			bool flag = true;
			try
			{
				if (this._parser.MARSOn)
				{
					this.CheckSetResetConnectionState(error, CallbackType.Read);
				}
				this.ChangeNetworkPacketTimeout(-1, -1);
				this.ProcessSniPacket(packet, error);
			}
			catch (Exception e)
			{
				flag = ADP.IsCatchableExceptionType(e);
				throw;
			}
			finally
			{
				int num = this.DecrementPendingCallbacks(false);
				if (flag && source != null && num < 2)
				{
					if (error == 0U)
					{
						if (this._executionContext != null)
						{
							ExecutionContext.Run(this._executionContext, delegate(object state)
							{
								source.TrySetResult(null);
							}, null);
						}
						else
						{
							source.TrySetResult(null);
						}
					}
					else if (this._executionContext != null)
					{
						ExecutionContext.Run(this._executionContext, delegate(object state)
						{
							this.ReadAsyncCallbackCaptureException(source);
						}, null);
					}
					else
					{
						this.ReadAsyncCallbackCaptureException(source);
					}
				}
			}
		}

		// Token: 0x06001762 RID: 5986
		protected abstract bool CheckPacket(object packet, TaskCompletionSource<object> source);

		// Token: 0x06001763 RID: 5987 RVA: 0x0007A5B0 File Offset: 0x000787B0
		private void ReadAsyncCallbackCaptureException(TaskCompletionSource<object> source)
		{
			bool flag = false;
			try
			{
				if (this._hasErrorOrWarning)
				{
					this.ThrowExceptionAndWarning(false, true);
				}
				else if (this._parser.State == TdsParserState.Closed || this._parser.State == TdsParserState.Broken)
				{
					throw ADP.ClosedConnectionError();
				}
			}
			catch (Exception exception)
			{
				if (source.TrySetException(exception))
				{
					flag = true;
				}
			}
			if (!flag)
			{
				Task.Factory.StartNew(delegate()
				{
					this._parser.State = TdsParserState.Broken;
					this._parser.Connection.BreakConnection();
					source.TrySetCanceled();
				});
			}
		}

		// Token: 0x06001764 RID: 5988 RVA: 0x0007A648 File Offset: 0x00078848
		public void WriteAsyncCallback<T>(T packet, uint sniError)
		{
			this.WriteAsyncCallback<T>(IntPtr.Zero, packet, sniError);
		}

		// Token: 0x06001765 RID: 5989 RVA: 0x0007A658 File Offset: 0x00078858
		public void WriteAsyncCallback<T>(IntPtr key, T packet, uint sniError)
		{
			this.RemovePacketFromPendingList(packet);
			try
			{
				if (sniError != 0U)
				{
					try
					{
						this.AddError(this._parser.ProcessSNIError(this));
						this.ThrowExceptionAndWarning(false, true);
						goto IL_9E;
					}
					catch (Exception ex)
					{
						TaskCompletionSource<object> writeCompletionSource = this._writeCompletionSource;
						if (writeCompletionSource != null)
						{
							writeCompletionSource.TrySetException(ex);
						}
						else
						{
							this._delayedWriteAsyncCallbackException = ex;
							Interlocked.MemoryBarrier();
							writeCompletionSource = this._writeCompletionSource;
							if (writeCompletionSource != null)
							{
								Exception ex2 = Interlocked.Exchange<Exception>(ref this._delayedWriteAsyncCallbackException, null);
								if (ex2 != null)
								{
									writeCompletionSource.TrySetException(ex2);
								}
							}
						}
						return;
					}
				}
				this._lastSuccessfulIOTimer._value = DateTime.UtcNow.Ticks;
			}
			finally
			{
				Interlocked.Decrement(ref this._asyncWriteCount);
			}
			IL_9E:
			TaskCompletionSource<object> writeCompletionSource2 = this._writeCompletionSource;
			if (this._asyncWriteCount == 0 && writeCompletionSource2 != null)
			{
				writeCompletionSource2.TrySetResult(null);
			}
		}

		// Token: 0x06001766 RID: 5990 RVA: 0x0007A740 File Offset: 0x00078940
		internal Task WaitForAccumulatedWrites()
		{
			Exception ex = Interlocked.Exchange<Exception>(ref this._delayedWriteAsyncCallbackException, null);
			if (ex != null)
			{
				throw ex;
			}
			if (this._asyncWriteCount == 0)
			{
				return null;
			}
			this._writeCompletionSource = new TaskCompletionSource<object>();
			Task task = this._writeCompletionSource.Task;
			Interlocked.MemoryBarrier();
			if (this._parser.State == TdsParserState.Closed || this._parser.State == TdsParserState.Broken)
			{
				throw ADP.ClosedConnectionError();
			}
			ex = Interlocked.Exchange<Exception>(ref this._delayedWriteAsyncCallbackException, null);
			if (ex != null)
			{
				throw ex;
			}
			if (this._asyncWriteCount == 0 && (!task.IsCompleted || task.Exception == null))
			{
				task = null;
			}
			return task;
		}

		// Token: 0x06001767 RID: 5991 RVA: 0x0007A7DC File Offset: 0x000789DC
		internal void WriteByte(byte b)
		{
			if (this._outBytesUsed == this._outBuff.Length)
			{
				this.WritePacket(0, true);
			}
			byte[] outBuff = this._outBuff;
			int outBytesUsed = this._outBytesUsed;
			this._outBytesUsed = outBytesUsed + 1;
			outBuff[outBytesUsed] = b;
		}

		// Token: 0x06001768 RID: 5992 RVA: 0x0007A81C File Offset: 0x00078A1C
		internal Task WriteByteArray(byte[] b, int len, int offsetBuffer, bool canAccumulate = true, TaskCompletionSource<object> completion = null)
		{
			Task result2;
			try
			{
				bool asyncWrite = this._parser._asyncWrite;
				int num = offsetBuffer;
				while (this._outBytesUsed + len > this._outBuff.Length)
				{
					int num2 = this._outBuff.Length - this._outBytesUsed;
					Buffer.BlockCopy(b, num, this._outBuff, this._outBytesUsed, num2);
					num += num2;
					this._outBytesUsed += num2;
					len -= num2;
					Task task = this.WritePacket(0, canAccumulate);
					if (task != null)
					{
						Task result = null;
						if (completion == null)
						{
							completion = new TaskCompletionSource<object>();
							result = completion.Task;
						}
						this.WriteByteArraySetupContinuation(b, len, completion, num, task);
						return result;
					}
					if (len <= 0)
					{
						IL_B9:
						if (completion != null)
						{
							completion.SetResult(null);
						}
						return null;
					}
				}
				Buffer.BlockCopy(b, num, this._outBuff, this._outBytesUsed, len);
				this._outBytesUsed += len;
				goto IL_B9;
			}
			catch (Exception exception)
			{
				if (completion == null)
				{
					throw;
				}
				completion.SetException(exception);
				result2 = null;
			}
			return result2;
		}

		// Token: 0x06001769 RID: 5993 RVA: 0x0007A91C File Offset: 0x00078B1C
		private void WriteByteArraySetupContinuation(byte[] b, int len, TaskCompletionSource<object> completion, int offset, Task packetTask)
		{
			AsyncHelper.ContinueTask(packetTask, completion, delegate
			{
				this.WriteByteArray(b, len, offset, false, completion);
			}, this._parser.Connection, null, null, null, null);
		}

		// Token: 0x0600176A RID: 5994 RVA: 0x0007A97C File Offset: 0x00078B7C
		internal Task WritePacket(byte flushMode, bool canAccumulate = false)
		{
			if (this._parser.State == TdsParserState.Closed || this._parser.State == TdsParserState.Broken)
			{
				throw ADP.ClosedConnectionError();
			}
			if ((this._parser.State == TdsParserState.OpenLoggedIn && !this._bulkCopyOpperationInProgress && this._outBytesUsed == this._outputHeaderLen + BitConverter.ToInt32(this._outBuff, this._outputHeaderLen) && this._outputPacketNumber == 1) || (this._outBytesUsed == this._outputHeaderLen && this._outputPacketNumber == 1))
			{
				return null;
			}
			byte outputPacketNumber = this._outputPacketNumber;
			bool flag = this._cancelled && this._parser._asyncWrite;
			byte b;
			if (flag)
			{
				b = 3;
				this._outputPacketNumber = 1;
			}
			else if (1 == flushMode)
			{
				b = 1;
				this._outputPacketNumber = 1;
			}
			else if (flushMode == 0)
			{
				b = 4;
				this._outputPacketNumber += 1;
			}
			else
			{
				b = 1;
			}
			this._outBuff[0] = this._outputMessageType;
			this._outBuff[1] = b;
			this._outBuff[2] = (byte)(this._outBytesUsed >> 8);
			this._outBuff[3] = (byte)(this._outBytesUsed & 255);
			this._outBuff[4] = 0;
			this._outBuff[5] = 0;
			this._outBuff[6] = outputPacketNumber;
			this._outBuff[7] = 0;
			this._parser.CheckResetConnection(this);
			Task task = this.WriteSni(canAccumulate);
			if (flag)
			{
				task = AsyncHelper.CreateContinuationTask(task, new Action(this.CancelWritePacket), this._parser.Connection, null);
			}
			return task;
		}

		// Token: 0x0600176B RID: 5995 RVA: 0x0007AAF0 File Offset: 0x00078CF0
		private void CancelWritePacket()
		{
			this._parser.Connection.ThreadHasParserLockForClose = true;
			try
			{
				this.SendAttention(false);
				this.ResetCancelAndProcessAttention();
				throw SQL.OperationCancelled();
			}
			finally
			{
				this._parser.Connection.ThreadHasParserLockForClose = false;
			}
		}

		// Token: 0x0600176C RID: 5996 RVA: 0x0007AB44 File Offset: 0x00078D44
		private Task SNIWritePacket(object packet, out uint sniError, bool canAccumulate, bool callerHasConnectionLock)
		{
			Exception ex = Interlocked.Exchange<Exception>(ref this._delayedWriteAsyncCallbackException, null);
			if (ex != null)
			{
				throw ex;
			}
			Task task = null;
			this._writeCompletionSource = null;
			object pointer = this.EmptyReadPacket;
			bool flag = !this._parser._asyncWrite;
			if (flag && this._asyncWriteCount > 0)
			{
				Task task2 = this.WaitForAccumulatedWrites();
				if (task2 != null)
				{
					try
					{
						task2.Wait();
					}
					catch (AggregateException ex2)
					{
						throw ex2.InnerException;
					}
				}
			}
			if (!flag)
			{
				pointer = this.AddPacketToPendingList(packet);
			}
			try
			{
			}
			finally
			{
				sniError = this.WritePacket(packet, flag);
			}
			if (sniError == 997U)
			{
				Interlocked.Increment(ref this._asyncWriteCount);
				if (!canAccumulate)
				{
					this._writeCompletionSource = new TaskCompletionSource<object>();
					task = this._writeCompletionSource.Task;
					Interlocked.MemoryBarrier();
					ex = Interlocked.Exchange<Exception>(ref this._delayedWriteAsyncCallbackException, null);
					if (ex != null)
					{
						throw ex;
					}
					if (this._asyncWriteCount == 0 && (!task.IsCompleted || task.Exception == null))
					{
						task = null;
					}
				}
			}
			else
			{
				if (this._parser.MARSOn)
				{
					this.CheckSetResetConnectionState(sniError, CallbackType.Write);
				}
				if (sniError == 0U)
				{
					this._lastSuccessfulIOTimer._value = DateTime.UtcNow.Ticks;
					if (!flag)
					{
						this.RemovePacketFromPendingList(pointer);
					}
				}
				else
				{
					this.AddError(this._parser.ProcessSNIError(this));
					this.ThrowExceptionAndWarning(callerHasConnectionLock, false);
				}
			}
			return task;
		}

		// Token: 0x0600176D RID: 5997
		internal abstract bool IsValidPacket(object packetPointer);

		// Token: 0x0600176E RID: 5998
		internal abstract uint WritePacket(object packet, bool sync);

		// Token: 0x0600176F RID: 5999 RVA: 0x0007ACA8 File Offset: 0x00078EA8
		internal void SendAttention(bool mustTakeWriteLock = false)
		{
			if (!this._attentionSent)
			{
				if (this._parser.State == TdsParserState.Closed || this._parser.State == TdsParserState.Broken)
				{
					return;
				}
				object packet = this.CreateAndSetAttentionPacket();
				try
				{
					this._attentionSending = true;
					bool flag = false;
					if (mustTakeWriteLock && !this._parser.Connection.ThreadHasParserLockForClose)
					{
						flag = true;
						this._parser.Connection._parserLock.Wait(false);
						this._parser.Connection.ThreadHasParserLockForClose = true;
					}
					try
					{
						if (this._parser.State == TdsParserState.Closed || this._parser.State == TdsParserState.Broken)
						{
							return;
						}
						this._parser._asyncWrite = false;
						uint num;
						this.SNIWritePacket(packet, out num, false, false);
					}
					finally
					{
						if (flag)
						{
							this._parser.Connection.ThreadHasParserLockForClose = false;
							this._parser.Connection._parserLock.Release();
						}
					}
					this.SetTimeoutSeconds(5);
					this._attentionSent = true;
				}
				finally
				{
					this._attentionSending = false;
				}
			}
		}

		// Token: 0x06001770 RID: 6000
		internal abstract object CreateAndSetAttentionPacket();

		// Token: 0x06001771 RID: 6001
		internal abstract void SetPacketData(object packet, byte[] buffer, int bytesUsed);

		// Token: 0x06001772 RID: 6002 RVA: 0x0007ADC8 File Offset: 0x00078FC8
		private Task WriteSni(bool canAccumulate)
		{
			object resetWritePacket = this.GetResetWritePacket();
			this.SetPacketData(resetWritePacket, this._outBuff, this._outBytesUsed);
			uint num;
			Task result = this.SNIWritePacket(resetWritePacket, out num, canAccumulate, true);
			if (this._bulkCopyOpperationInProgress && this.GetTimeoutRemaining() == 0)
			{
				this._parser.Connection.ThreadHasParserLockForClose = true;
				try
				{
					this.AddError(new SqlError(-2, 0, 11, this._parser.Server, this._parser.Connection.TimeoutErrorInternal.GetErrorMessage(), "", 0, 258U, null));
					this._bulkCopyWriteTimeout = true;
					this.SendAttention(false);
					this._parser.ProcessPendingAck(this);
					this.ThrowExceptionAndWarning(false, false);
				}
				finally
				{
					this._parser.Connection.ThreadHasParserLockForClose = false;
				}
			}
			if (this._parser.State == TdsParserState.OpenNotLoggedIn && this._parser.EncryptionOptions == EncryptionOptions.LOGIN)
			{
				this._parser.RemoveEncryption();
				this._parser.EncryptionOptions = EncryptionOptions.OFF;
				this.ClearAllWritePackets();
			}
			this.SniWriteStatisticsAndTracing();
			this.ResetBuffer();
			return result;
		}

		// Token: 0x06001773 RID: 6003 RVA: 0x0007AEEC File Offset: 0x000790EC
		private void SniReadStatisticsAndTracing()
		{
			SqlStatistics statistics = this.Parser.Statistics;
			if (statistics != null)
			{
				if (statistics.WaitForReply)
				{
					statistics.SafeIncrement(ref statistics._serverRoundtrips);
					statistics.ReleaseAndUpdateNetworkServerTimer();
				}
				statistics.SafeAdd(ref statistics._bytesReceived, (long)this._inBytesRead);
				statistics.SafeIncrement(ref statistics._buffersReceived);
			}
		}

		// Token: 0x06001774 RID: 6004 RVA: 0x0007AF44 File Offset: 0x00079144
		private void SniWriteStatisticsAndTracing()
		{
			SqlStatistics statistics = this._parser.Statistics;
			if (statistics != null)
			{
				statistics.SafeIncrement(ref statistics._buffersSent);
				statistics.SafeAdd(ref statistics._bytesSent, (long)this._outBytesUsed);
				statistics.RequestNetworkServerTimer();
			}
		}

		// Token: 0x06001775 RID: 6005 RVA: 0x0007AF88 File Offset: 0x00079188
		[Conditional("DEBUG")]
		private void AssertValidState()
		{
			if (this._inBytesUsed < 0 || this._inBytesRead < 0)
			{
				string text = string.Format(CultureInfo.InvariantCulture, "either _inBytesUsed or _inBytesRead is negative: {0}, {1}", this._inBytesUsed, this._inBytesRead);
			}
			else if (this._inBytesUsed > this._inBytesRead)
			{
				string text = string.Format(CultureInfo.InvariantCulture, "_inBytesUsed > _inBytesRead: {0} > {1}", this._inBytesUsed, this._inBytesRead);
			}
		}

		// Token: 0x1700044E RID: 1102
		// (get) Token: 0x06001776 RID: 6006 RVA: 0x0007B007 File Offset: 0x00079207
		internal bool HasErrorOrWarning
		{
			get
			{
				return this._hasErrorOrWarning;
			}
		}

		// Token: 0x06001777 RID: 6007 RVA: 0x0007B010 File Offset: 0x00079210
		internal void AddError(SqlError error)
		{
			this._syncOverAsync = true;
			object errorAndWarningsLock = this._errorAndWarningsLock;
			lock (errorAndWarningsLock)
			{
				this._hasErrorOrWarning = true;
				if (this._errors == null)
				{
					this._errors = new SqlErrorCollection();
				}
				this._errors.Add(error);
			}
		}

		// Token: 0x1700044F RID: 1103
		// (get) Token: 0x06001778 RID: 6008 RVA: 0x0007B078 File Offset: 0x00079278
		internal int ErrorCount
		{
			get
			{
				int result = 0;
				object errorAndWarningsLock = this._errorAndWarningsLock;
				lock (errorAndWarningsLock)
				{
					if (this._errors != null)
					{
						result = this._errors.Count;
					}
				}
				return result;
			}
		}

		// Token: 0x06001779 RID: 6009 RVA: 0x0007B0CC File Offset: 0x000792CC
		internal void AddWarning(SqlError error)
		{
			this._syncOverAsync = true;
			object errorAndWarningsLock = this._errorAndWarningsLock;
			lock (errorAndWarningsLock)
			{
				this._hasErrorOrWarning = true;
				if (this._warnings == null)
				{
					this._warnings = new SqlErrorCollection();
				}
				this._warnings.Add(error);
			}
		}

		// Token: 0x17000450 RID: 1104
		// (get) Token: 0x0600177A RID: 6010 RVA: 0x0007B134 File Offset: 0x00079334
		internal int WarningCount
		{
			get
			{
				int result = 0;
				object errorAndWarningsLock = this._errorAndWarningsLock;
				lock (errorAndWarningsLock)
				{
					if (this._warnings != null)
					{
						result = this._warnings.Count;
					}
				}
				return result;
			}
		}

		// Token: 0x17000451 RID: 1105
		// (get) Token: 0x0600177B RID: 6011
		protected abstract object EmptyReadPacket { get; }

		// Token: 0x0600177C RID: 6012 RVA: 0x0007B188 File Offset: 0x00079388
		internal SqlErrorCollection GetFullErrorAndWarningCollection(out bool broken)
		{
			SqlErrorCollection result = new SqlErrorCollection();
			broken = false;
			object errorAndWarningsLock = this._errorAndWarningsLock;
			lock (errorAndWarningsLock)
			{
				this._hasErrorOrWarning = false;
				this.AddErrorsToCollection(this._errors, ref result, ref broken);
				this.AddErrorsToCollection(this._warnings, ref result, ref broken);
				this._errors = null;
				this._warnings = null;
				this.AddErrorsToCollection(this._preAttentionErrors, ref result, ref broken);
				this.AddErrorsToCollection(this._preAttentionWarnings, ref result, ref broken);
				this._preAttentionErrors = null;
				this._preAttentionWarnings = null;
			}
			return result;
		}

		// Token: 0x0600177D RID: 6013 RVA: 0x0007B22C File Offset: 0x0007942C
		private void AddErrorsToCollection(SqlErrorCollection inCollection, ref SqlErrorCollection collectionToAddTo, ref bool broken)
		{
			if (inCollection != null)
			{
				foreach (object obj in inCollection)
				{
					SqlError sqlError = (SqlError)obj;
					collectionToAddTo.Add(sqlError);
					broken |= (sqlError.Class >= 20);
				}
			}
		}

		// Token: 0x0600177E RID: 6014 RVA: 0x0007B298 File Offset: 0x00079498
		internal void StoreErrorAndWarningForAttention()
		{
			object errorAndWarningsLock = this._errorAndWarningsLock;
			lock (errorAndWarningsLock)
			{
				this._hasErrorOrWarning = false;
				this._preAttentionErrors = this._errors;
				this._preAttentionWarnings = this._warnings;
				this._errors = null;
				this._warnings = null;
			}
		}

		// Token: 0x0600177F RID: 6015 RVA: 0x0007B300 File Offset: 0x00079500
		internal void RestoreErrorAndWarningAfterAttention()
		{
			object errorAndWarningsLock = this._errorAndWarningsLock;
			lock (errorAndWarningsLock)
			{
				this._hasErrorOrWarning = ((this._preAttentionErrors != null && this._preAttentionErrors.Count > 0) || (this._preAttentionWarnings != null && this._preAttentionWarnings.Count > 0));
				this._errors = this._preAttentionErrors;
				this._warnings = this._preAttentionWarnings;
				this._preAttentionErrors = null;
				this._preAttentionWarnings = null;
			}
		}

		// Token: 0x06001780 RID: 6016 RVA: 0x0007B398 File Offset: 0x00079598
		internal void CheckThrowSNIException()
		{
			if (this.HasErrorOrWarning)
			{
				this.ThrowExceptionAndWarning(false, false);
			}
		}

		// Token: 0x06001781 RID: 6017 RVA: 0x0007B3AC File Offset: 0x000795AC
		[Conditional("DEBUG")]
		internal void AssertStateIsClean()
		{
			TdsParser parser = this._parser;
			if (parser != null && parser.State != TdsParserState.Closed)
			{
				TdsParserState state = parser.State;
			}
		}

		// Token: 0x06001782 RID: 6018 RVA: 0x0007B3D4 File Offset: 0x000795D4
		internal void CloneCleanupAltMetaDataSetArray()
		{
			if (this._snapshot != null)
			{
				this._snapshot.CloneCleanupAltMetaDataSetArray();
			}
		}

		// Token: 0x06001783 RID: 6019 RVA: 0x0007B3E9 File Offset: 0x000795E9
		[CompilerGenerated]
		private bool <DisposeCounters>b__135_0()
		{
			return Volatile.Read(ref this._readingCount) == 0;
		}

		// Token: 0x06001784 RID: 6020 RVA: 0x0007B3F9 File Offset: 0x000795F9
		[CompilerGenerated]
		private void <ExecuteFlush>b__142_0()
		{
			this._pendingData = true;
			this._messageStatus = 0;
		}

		// Token: 0x04001161 RID: 4449
		private const int AttentionTimeoutSeconds = 5;

		// Token: 0x04001162 RID: 4450
		private const long CheckConnectionWindow = 50000L;

		// Token: 0x04001163 RID: 4451
		protected readonly TdsParser _parser;

		// Token: 0x04001164 RID: 4452
		private readonly WeakReference _owner = new WeakReference(null);

		// Token: 0x04001165 RID: 4453
		internal SqlDataReader.SharedState _readerState;

		// Token: 0x04001166 RID: 4454
		private int _activateCount;

		// Token: 0x04001167 RID: 4455
		internal readonly int _inputHeaderLen = 8;

		// Token: 0x04001168 RID: 4456
		internal readonly int _outputHeaderLen = 8;

		// Token: 0x04001169 RID: 4457
		internal byte[] _outBuff;

		// Token: 0x0400116A RID: 4458
		internal int _outBytesUsed = 8;

		// Token: 0x0400116B RID: 4459
		protected byte[] _inBuff;

		// Token: 0x0400116C RID: 4460
		internal int _inBytesUsed;

		// Token: 0x0400116D RID: 4461
		internal int _inBytesRead;

		// Token: 0x0400116E RID: 4462
		internal int _inBytesPacket;

		// Token: 0x0400116F RID: 4463
		internal byte _outputMessageType;

		// Token: 0x04001170 RID: 4464
		internal byte _messageStatus;

		// Token: 0x04001171 RID: 4465
		internal byte _outputPacketNumber = 1;

		// Token: 0x04001172 RID: 4466
		internal bool _pendingData;

		// Token: 0x04001173 RID: 4467
		internal volatile bool _fResetEventOwned;

		// Token: 0x04001174 RID: 4468
		internal volatile bool _fResetConnectionSent;

		// Token: 0x04001175 RID: 4469
		internal bool _errorTokenReceived;

		// Token: 0x04001176 RID: 4470
		internal bool _bulkCopyOpperationInProgress;

		// Token: 0x04001177 RID: 4471
		internal bool _bulkCopyWriteTimeout;

		// Token: 0x04001178 RID: 4472
		protected readonly object _writePacketLockObject = new object();

		// Token: 0x04001179 RID: 4473
		private int _pendingCallbacks;

		// Token: 0x0400117A RID: 4474
		private long _timeoutMilliseconds;

		// Token: 0x0400117B RID: 4475
		private long _timeoutTime;

		// Token: 0x0400117C RID: 4476
		internal volatile bool _attentionSent;

		// Token: 0x0400117D RID: 4477
		internal bool _attentionReceived;

		// Token: 0x0400117E RID: 4478
		internal volatile bool _attentionSending;

		// Token: 0x0400117F RID: 4479
		internal bool _internalTimeout;

		// Token: 0x04001180 RID: 4480
		private readonly LastIOTimer _lastSuccessfulIOTimer;

		// Token: 0x04001181 RID: 4481
		private bool _cancelled;

		// Token: 0x04001182 RID: 4482
		private const int _waitForCancellationLockPollTimeout = 100;

		// Token: 0x04001183 RID: 4483
		private WeakReference _cancellationOwner = new WeakReference(null);

		// Token: 0x04001184 RID: 4484
		internal bool _hasOpenResult;

		// Token: 0x04001185 RID: 4485
		internal SqlInternalTransaction _executedUnderTransaction;

		// Token: 0x04001186 RID: 4486
		internal ulong _longlen;

		// Token: 0x04001187 RID: 4487
		internal ulong _longlenleft;

		// Token: 0x04001188 RID: 4488
		internal int[] _decimalBits;

		// Token: 0x04001189 RID: 4489
		internal byte[] _bTmp = new byte[12];

		// Token: 0x0400118A RID: 4490
		internal int _bTmpRead;

		// Token: 0x0400118B RID: 4491
		internal Decoder _plpdecoder;

		// Token: 0x0400118C RID: 4492
		internal bool _accumulateInfoEvents;

		// Token: 0x0400118D RID: 4493
		internal List<SqlError> _pendingInfoEvents;

		// Token: 0x0400118E RID: 4494
		private byte[] _partialHeaderBuffer = new byte[8];

		// Token: 0x0400118F RID: 4495
		internal int _partialHeaderBytesRead;

		// Token: 0x04001190 RID: 4496
		internal _SqlMetaDataSet _cleanupMetaData;

		// Token: 0x04001191 RID: 4497
		internal _SqlMetaDataSetCollection _cleanupAltMetaDataSetArray;

		// Token: 0x04001192 RID: 4498
		internal bool _receivedColMetaData;

		// Token: 0x04001193 RID: 4499
		private SniContext _sniContext;

		// Token: 0x04001194 RID: 4500
		private bool _bcpLock;

		// Token: 0x04001195 RID: 4501
		private TdsParserStateObject.NullBitmap _nullBitmapInfo;

		// Token: 0x04001196 RID: 4502
		internal TaskCompletionSource<object> _networkPacketTaskSource;

		// Token: 0x04001197 RID: 4503
		private Timer _networkPacketTimeout;

		// Token: 0x04001198 RID: 4504
		internal bool _syncOverAsync = true;

		// Token: 0x04001199 RID: 4505
		private bool _snapshotReplay;

		// Token: 0x0400119A RID: 4506
		private TdsParserStateObject.StateSnapshot _snapshot;

		// Token: 0x0400119B RID: 4507
		internal ExecutionContext _executionContext;

		// Token: 0x0400119C RID: 4508
		internal bool _asyncReadWithoutSnapshot;

		// Token: 0x0400119D RID: 4509
		internal SqlErrorCollection _errors;

		// Token: 0x0400119E RID: 4510
		internal SqlErrorCollection _warnings;

		// Token: 0x0400119F RID: 4511
		internal object _errorAndWarningsLock = new object();

		// Token: 0x040011A0 RID: 4512
		private bool _hasErrorOrWarning;

		// Token: 0x040011A1 RID: 4513
		internal SqlErrorCollection _preAttentionErrors;

		// Token: 0x040011A2 RID: 4514
		internal SqlErrorCollection _preAttentionWarnings;

		// Token: 0x040011A3 RID: 4515
		private volatile TaskCompletionSource<object> _writeCompletionSource;

		// Token: 0x040011A4 RID: 4516
		protected volatile int _asyncWriteCount;

		// Token: 0x040011A5 RID: 4517
		private volatile Exception _delayedWriteAsyncCallbackException;

		// Token: 0x040011A6 RID: 4518
		private int _readingCount;

		// Token: 0x0200020A RID: 522
		private struct NullBitmap
		{
			// Token: 0x06001785 RID: 6021 RVA: 0x0007B40C File Offset: 0x0007960C
			internal bool TryInitialize(TdsParserStateObject stateObj, int columnsCount)
			{
				this._columnsCount = columnsCount;
				int num = (columnsCount + 7) / 8;
				if (this._nullBitmap == null || this._nullBitmap.Length != num)
				{
					this._nullBitmap = new byte[num];
				}
				return stateObj.TryReadByteArray(this._nullBitmap, 0, this._nullBitmap.Length);
			}

			// Token: 0x06001786 RID: 6022 RVA: 0x0007B45F File Offset: 0x0007965F
			internal bool ReferenceEquals(TdsParserStateObject.NullBitmap obj)
			{
				return this._nullBitmap == obj._nullBitmap;
			}

			// Token: 0x06001787 RID: 6023 RVA: 0x0007B470 File Offset: 0x00079670
			internal TdsParserStateObject.NullBitmap Clone()
			{
				return new TdsParserStateObject.NullBitmap
				{
					_nullBitmap = ((this._nullBitmap == null) ? null : ((byte[])this._nullBitmap.Clone())),
					_columnsCount = this._columnsCount
				};
			}

			// Token: 0x06001788 RID: 6024 RVA: 0x0007B4B5 File Offset: 0x000796B5
			internal void Clean()
			{
				this._columnsCount = 0;
			}

			// Token: 0x06001789 RID: 6025 RVA: 0x0007B4C0 File Offset: 0x000796C0
			internal bool IsGuaranteedNull(int columnOrdinal)
			{
				if (this._columnsCount == 0)
				{
					return false;
				}
				byte b = (byte)(1 << (columnOrdinal & 7));
				byte b2 = this._nullBitmap[columnOrdinal >> 3];
				return (b & b2) > 0;
			}

			// Token: 0x040011A7 RID: 4519
			private byte[] _nullBitmap;

			// Token: 0x040011A8 RID: 4520
			private int _columnsCount;
		}

		// Token: 0x0200020B RID: 523
		private class PacketData
		{
			// Token: 0x0600178A RID: 6026 RVA: 0x00005C14 File Offset: 0x00003E14
			public PacketData()
			{
			}

			// Token: 0x040011A9 RID: 4521
			public byte[] Buffer;

			// Token: 0x040011AA RID: 4522
			public int Read;
		}

		// Token: 0x0200020C RID: 524
		private class StateSnapshot
		{
			// Token: 0x0600178B RID: 6027 RVA: 0x0007B4F0 File Offset: 0x000796F0
			public StateSnapshot(TdsParserStateObject state)
			{
				this._snapshotInBuffs = new List<TdsParserStateObject.PacketData>();
				this._stateObj = state;
			}

			// Token: 0x0600178C RID: 6028 RVA: 0x0007B50A File Offset: 0x0007970A
			internal void CloneNullBitmapInfo()
			{
				if (this._stateObj._nullBitmapInfo.ReferenceEquals(this._snapshotNullBitmapInfo))
				{
					this._stateObj._nullBitmapInfo = this._stateObj._nullBitmapInfo.Clone();
				}
			}

			// Token: 0x0600178D RID: 6029 RVA: 0x0007B540 File Offset: 0x00079740
			internal void CloneCleanupAltMetaDataSetArray()
			{
				if (this._stateObj._cleanupAltMetaDataSetArray != null && this._snapshotCleanupAltMetaDataSetArray == this._stateObj._cleanupAltMetaDataSetArray)
				{
					this._stateObj._cleanupAltMetaDataSetArray = (_SqlMetaDataSetCollection)this._stateObj._cleanupAltMetaDataSetArray.Clone();
				}
			}

			// Token: 0x0600178E RID: 6030 RVA: 0x0007B590 File Offset: 0x00079790
			internal void PushBuffer(byte[] buffer, int read)
			{
				TdsParserStateObject.PacketData packetData = new TdsParserStateObject.PacketData();
				packetData.Buffer = buffer;
				packetData.Read = read;
				this._snapshotInBuffs.Add(packetData);
			}

			// Token: 0x0600178F RID: 6031 RVA: 0x0007B5C0 File Offset: 0x000797C0
			internal bool Replay()
			{
				if (this._snapshotInBuffCurrent < this._snapshotInBuffs.Count)
				{
					TdsParserStateObject.PacketData packetData = this._snapshotInBuffs[this._snapshotInBuffCurrent];
					this._stateObj._inBuff = packetData.Buffer;
					this._stateObj._inBytesUsed = 0;
					this._stateObj._inBytesRead = packetData.Read;
					this._snapshotInBuffCurrent++;
					return true;
				}
				return false;
			}

			// Token: 0x06001790 RID: 6032 RVA: 0x0007B634 File Offset: 0x00079834
			internal void Snap()
			{
				this._snapshotInBuffs.Clear();
				this._snapshotInBuffCurrent = 0;
				this._snapshotInBytesUsed = this._stateObj._inBytesUsed;
				this._snapshotInBytesPacket = this._stateObj._inBytesPacket;
				this._snapshotPendingData = this._stateObj._pendingData;
				this._snapshotErrorTokenReceived = this._stateObj._errorTokenReceived;
				this._snapshotMessageStatus = this._stateObj._messageStatus;
				this._snapshotNullBitmapInfo = this._stateObj._nullBitmapInfo;
				this._snapshotLongLen = this._stateObj._longlen;
				this._snapshotLongLenLeft = this._stateObj._longlenleft;
				this._snapshotCleanupMetaData = this._stateObj._cleanupMetaData;
				this._snapshotCleanupAltMetaDataSetArray = this._stateObj._cleanupAltMetaDataSetArray;
				this._snapshotHasOpenResult = this._stateObj._hasOpenResult;
				this._snapshotReceivedColumnMetadata = this._stateObj._receivedColMetaData;
				this._snapshotAttentionReceived = this._stateObj._attentionReceived;
				this.PushBuffer(this._stateObj._inBuff, this._stateObj._inBytesRead);
			}

			// Token: 0x06001791 RID: 6033 RVA: 0x0007B74C File Offset: 0x0007994C
			internal void ResetSnapshotState()
			{
				this._snapshotInBuffCurrent = 0;
				this.Replay();
				this._stateObj._inBytesUsed = this._snapshotInBytesUsed;
				this._stateObj._inBytesPacket = this._snapshotInBytesPacket;
				this._stateObj._pendingData = this._snapshotPendingData;
				this._stateObj._errorTokenReceived = this._snapshotErrorTokenReceived;
				this._stateObj._messageStatus = this._snapshotMessageStatus;
				this._stateObj._nullBitmapInfo = this._snapshotNullBitmapInfo;
				this._stateObj._cleanupMetaData = this._snapshotCleanupMetaData;
				this._stateObj._cleanupAltMetaDataSetArray = this._snapshotCleanupAltMetaDataSetArray;
				this._stateObj._hasOpenResult = this._snapshotHasOpenResult;
				this._stateObj._receivedColMetaData = this._snapshotReceivedColumnMetadata;
				this._stateObj._attentionReceived = this._snapshotAttentionReceived;
				this._stateObj._bTmpRead = 0;
				this._stateObj._partialHeaderBytesRead = 0;
				this._stateObj._longlen = this._snapshotLongLen;
				this._stateObj._longlenleft = this._snapshotLongLenLeft;
				this._stateObj._snapshotReplay = true;
			}

			// Token: 0x06001792 RID: 6034 RVA: 0x0007B868 File Offset: 0x00079A68
			internal void PrepareReplay()
			{
				this.ResetSnapshotState();
			}

			// Token: 0x040011AB RID: 4523
			private List<TdsParserStateObject.PacketData> _snapshotInBuffs;

			// Token: 0x040011AC RID: 4524
			private int _snapshotInBuffCurrent;

			// Token: 0x040011AD RID: 4525
			private int _snapshotInBytesUsed;

			// Token: 0x040011AE RID: 4526
			private int _snapshotInBytesPacket;

			// Token: 0x040011AF RID: 4527
			private bool _snapshotPendingData;

			// Token: 0x040011B0 RID: 4528
			private bool _snapshotErrorTokenReceived;

			// Token: 0x040011B1 RID: 4529
			private bool _snapshotHasOpenResult;

			// Token: 0x040011B2 RID: 4530
			private bool _snapshotReceivedColumnMetadata;

			// Token: 0x040011B3 RID: 4531
			private bool _snapshotAttentionReceived;

			// Token: 0x040011B4 RID: 4532
			private byte _snapshotMessageStatus;

			// Token: 0x040011B5 RID: 4533
			private TdsParserStateObject.NullBitmap _snapshotNullBitmapInfo;

			// Token: 0x040011B6 RID: 4534
			private ulong _snapshotLongLen;

			// Token: 0x040011B7 RID: 4535
			private ulong _snapshotLongLenLeft;

			// Token: 0x040011B8 RID: 4536
			private _SqlMetaDataSet _snapshotCleanupMetaData;

			// Token: 0x040011B9 RID: 4537
			private _SqlMetaDataSetCollection _snapshotCleanupAltMetaDataSetArray;

			// Token: 0x040011BA RID: 4538
			private readonly TdsParserStateObject _stateObj;
		}

		// Token: 0x0200020D RID: 525
		[CompilerGenerated]
		private sealed class <>c__DisplayClass173_0
		{
			// Token: 0x06001793 RID: 6035 RVA: 0x00005C14 File Offset: 0x00003E14
			public <>c__DisplayClass173_0()
			{
			}

			// Token: 0x06001794 RID: 6036 RVA: 0x0007B870 File Offset: 0x00079A70
			internal void <OnTimeout>b__0(Task _)
			{
				if (!this.source.Task.IsCompleted)
				{
					int num = this.<>4__this.IncrementPendingCallbacks();
					try
					{
						if (num == 3 && !this.source.Task.IsCompleted)
						{
							bool flag = false;
							try
							{
								this.<>4__this.CheckThrowSNIException();
							}
							catch (Exception exception)
							{
								if (this.source.TrySetException(exception))
								{
									flag = true;
								}
							}
							this.<>4__this._parser.State = TdsParserState.Broken;
							this.<>4__this._parser.Connection.BreakConnection();
							if (!flag)
							{
								this.source.TrySetCanceled();
							}
						}
					}
					finally
					{
						this.<>4__this.DecrementPendingCallbacks(false);
					}
				}
			}

			// Token: 0x040011BB RID: 4539
			public TaskCompletionSource<object> source;

			// Token: 0x040011BC RID: 4540
			public TdsParserStateObject <>4__this;
		}

		// Token: 0x0200020E RID: 526
		[CompilerGenerated]
		private sealed class <>c__DisplayClass181_0<T>
		{
			// Token: 0x06001795 RID: 6037 RVA: 0x00005C14 File Offset: 0x00003E14
			public <>c__DisplayClass181_0()
			{
			}

			// Token: 0x06001796 RID: 6038 RVA: 0x0007B938 File Offset: 0x00079B38
			internal void <ReadAsyncCallback>b__0(object state)
			{
				this.source.TrySetResult(null);
			}

			// Token: 0x06001797 RID: 6039 RVA: 0x0007B947 File Offset: 0x00079B47
			internal void <ReadAsyncCallback>b__1(object state)
			{
				this.<>4__this.ReadAsyncCallbackCaptureException(this.source);
			}

			// Token: 0x040011BD RID: 4541
			public TaskCompletionSource<object> source;

			// Token: 0x040011BE RID: 4542
			public TdsParserStateObject <>4__this;
		}

		// Token: 0x0200020F RID: 527
		[CompilerGenerated]
		private sealed class <>c__DisplayClass183_0
		{
			// Token: 0x06001798 RID: 6040 RVA: 0x00005C14 File Offset: 0x00003E14
			public <>c__DisplayClass183_0()
			{
			}

			// Token: 0x06001799 RID: 6041 RVA: 0x0007B95A File Offset: 0x00079B5A
			internal void <ReadAsyncCallbackCaptureException>b__0()
			{
				this.<>4__this._parser.State = TdsParserState.Broken;
				this.<>4__this._parser.Connection.BreakConnection();
				this.source.TrySetCanceled();
			}

			// Token: 0x040011BF RID: 4543
			public TdsParserStateObject <>4__this;

			// Token: 0x040011C0 RID: 4544
			public TaskCompletionSource<object> source;
		}

		// Token: 0x02000210 RID: 528
		[CompilerGenerated]
		private sealed class <>c__DisplayClass189_0
		{
			// Token: 0x0600179A RID: 6042 RVA: 0x00005C14 File Offset: 0x00003E14
			public <>c__DisplayClass189_0()
			{
			}

			// Token: 0x0600179B RID: 6043 RVA: 0x0007B98E File Offset: 0x00079B8E
			internal void <WriteByteArraySetupContinuation>b__0()
			{
				this.<>4__this.WriteByteArray(this.b, this.len, this.offset, false, this.completion);
			}

			// Token: 0x040011C1 RID: 4545
			public TdsParserStateObject <>4__this;

			// Token: 0x040011C2 RID: 4546
			public byte[] b;

			// Token: 0x040011C3 RID: 4547
			public int len;

			// Token: 0x040011C4 RID: 4548
			public int offset;

			// Token: 0x040011C5 RID: 4549
			public TaskCompletionSource<object> completion;
		}
	}
}
