﻿using System;
using System.Collections.Generic;
using System.Data.Common;

namespace System.Data.SqlClient
{
	// Token: 0x02000174 RID: 372
	internal sealed class SqlConnectionString : DbConnectionOptions
	{
		// Token: 0x0600112E RID: 4398 RVA: 0x00058684 File Offset: 0x00056884
		internal SqlConnectionString(string connectionString) : base(connectionString, SqlConnectionString.GetParseSynonyms())
		{
			this.ThrowUnsupportedIfKeywordSet("asynchronous processing");
			this.ThrowUnsupportedIfKeywordSet("connection reset");
			this.ThrowUnsupportedIfKeywordSet("context connection");
			if (base.ContainsKey("network library"))
			{
				throw SQL.NetworkLibraryKeywordNotSupported();
			}
			this._integratedSecurity = base.ConvertValueToIntegratedSecurity();
			this._encrypt = base.ConvertValueToBoolean("encrypt", false);
			this._enlist = base.ConvertValueToBoolean("enlist", true);
			this._mars = base.ConvertValueToBoolean("multipleactiveresultsets", false);
			this._persistSecurityInfo = base.ConvertValueToBoolean("persist security info", false);
			this._pooling = base.ConvertValueToBoolean("pooling", true);
			this._replication = base.ConvertValueToBoolean("replication", false);
			this._userInstance = base.ConvertValueToBoolean("user instance", false);
			this._multiSubnetFailover = base.ConvertValueToBoolean("multisubnetfailover", false);
			this._connectTimeout = base.ConvertValueToInt32("connect timeout", 15);
			this._loadBalanceTimeout = base.ConvertValueToInt32("load balance timeout", 0);
			this._maxPoolSize = base.ConvertValueToInt32("max pool size", 100);
			this._minPoolSize = base.ConvertValueToInt32("min pool size", 0);
			this._packetSize = base.ConvertValueToInt32("packet size", 8000);
			this._connectRetryCount = base.ConvertValueToInt32("connectretrycount", 1);
			this._connectRetryInterval = base.ConvertValueToInt32("connectretryinterval", 10);
			this._applicationIntent = this.ConvertValueToApplicationIntent();
			this._applicationName = base.ConvertValueToString("application name", "Core .Net SqlClient Data Provider");
			this._attachDBFileName = base.ConvertValueToString("attachdbfilename", "");
			this._currentLanguage = base.ConvertValueToString("current language", "");
			this._dataSource = base.ConvertValueToString("data source", "");
			this._localDBInstance = LocalDBAPI.GetLocalDbInstanceNameFromServerName(this._dataSource);
			this._failoverPartner = base.ConvertValueToString("failover partner", "");
			this._initialCatalog = base.ConvertValueToString("initial catalog", "");
			this._password = base.ConvertValueToString("password", "");
			this._trustServerCertificate = base.ConvertValueToBoolean("trustservercertificate", false);
			string text = base.ConvertValueToString("type system version", null);
			string text2 = base.ConvertValueToString("transaction binding", null);
			this._userID = base.ConvertValueToString("user id", "");
			this._workstationId = base.ConvertValueToString("workstation id", null);
			if (this._loadBalanceTimeout < 0)
			{
				throw ADP.InvalidConnectionOptionValue("load balance timeout");
			}
			if (this._connectTimeout < 0)
			{
				throw ADP.InvalidConnectionOptionValue("connect timeout");
			}
			if (this._maxPoolSize < 1)
			{
				throw ADP.InvalidConnectionOptionValue("max pool size");
			}
			if (this._minPoolSize < 0)
			{
				throw ADP.InvalidConnectionOptionValue("min pool size");
			}
			if (this._maxPoolSize < this._minPoolSize)
			{
				throw ADP.InvalidMinMaxPoolSizeValues();
			}
			if (this._packetSize < 512 || 32768 < this._packetSize)
			{
				throw SQL.InvalidPacketSizeValue();
			}
			this.ValidateValueLength(this._applicationName, 128, "application name");
			this.ValidateValueLength(this._currentLanguage, 128, "current language");
			this.ValidateValueLength(this._dataSource, 128, "data source");
			this.ValidateValueLength(this._failoverPartner, 128, "failover partner");
			this.ValidateValueLength(this._initialCatalog, 128, "initial catalog");
			this.ValidateValueLength(this._password, 128, "password");
			this.ValidateValueLength(this._userID, 128, "user id");
			if (this._workstationId != null)
			{
				this.ValidateValueLength(this._workstationId, 128, "workstation id");
			}
			if (!string.Equals("", this._failoverPartner, StringComparison.OrdinalIgnoreCase))
			{
				if (this._multiSubnetFailover)
				{
					throw SQL.MultiSubnetFailoverWithFailoverPartner(false, null);
				}
				if (string.Equals("", this._initialCatalog, StringComparison.OrdinalIgnoreCase))
				{
					throw ADP.MissingConnectionOptionValue("failover partner", "initial catalog");
				}
			}
			if (0 <= this._attachDBFileName.IndexOf('|'))
			{
				throw ADP.InvalidConnectionOptionValue("attachdbfilename");
			}
			this.ValidateValueLength(this._attachDBFileName, 260, "attachdbfilename");
			if (this._userInstance && !string.IsNullOrEmpty(this._failoverPartner))
			{
				throw SQL.UserInstanceFailoverNotCompatible();
			}
			if (string.IsNullOrEmpty(text))
			{
				text = "Latest";
			}
			if (text.Equals("Latest", StringComparison.OrdinalIgnoreCase))
			{
				this._typeSystemVersion = SqlConnectionString.TypeSystem.Latest;
			}
			else if (text.Equals("SQL Server 2000", StringComparison.OrdinalIgnoreCase))
			{
				this._typeSystemVersion = SqlConnectionString.TypeSystem.SQLServer2000;
			}
			else if (text.Equals("SQL Server 2005", StringComparison.OrdinalIgnoreCase))
			{
				this._typeSystemVersion = SqlConnectionString.TypeSystem.SQLServer2005;
			}
			else if (text.Equals("SQL Server 2008", StringComparison.OrdinalIgnoreCase))
			{
				this._typeSystemVersion = SqlConnectionString.TypeSystem.Latest;
			}
			else
			{
				if (!text.Equals("SQL Server 2012", StringComparison.OrdinalIgnoreCase))
				{
					throw ADP.InvalidConnectionOptionValue("type system version");
				}
				this._typeSystemVersion = SqlConnectionString.TypeSystem.SQLServer2012;
			}
			if (string.IsNullOrEmpty(text2))
			{
				text2 = "Implicit Unbind";
			}
			if (text2.Equals("Implicit Unbind", StringComparison.OrdinalIgnoreCase))
			{
				this._transactionBinding = SqlConnectionString.TransactionBindingEnum.ImplicitUnbind;
			}
			else
			{
				if (!text2.Equals("Explicit Unbind", StringComparison.OrdinalIgnoreCase))
				{
					throw ADP.InvalidConnectionOptionValue("transaction binding");
				}
				this._transactionBinding = SqlConnectionString.TransactionBindingEnum.ExplicitUnbind;
			}
			if (this._applicationIntent == ApplicationIntent.ReadOnly && !string.IsNullOrEmpty(this._failoverPartner))
			{
				throw SQL.ROR_FailoverNotSupportedConnString();
			}
			if (this._connectRetryCount < 0 || this._connectRetryCount > 255)
			{
				throw ADP.InvalidConnectRetryCountValue();
			}
			if (this._connectRetryInterval < 1 || this._connectRetryInterval > 60)
			{
				throw ADP.InvalidConnectRetryIntervalValue();
			}
		}

		// Token: 0x0600112F RID: 4399 RVA: 0x00058C04 File Offset: 0x00056E04
		internal SqlConnectionString(SqlConnectionString connectionOptions, string dataSource, bool userInstance, bool? setEnlistValue) : base(connectionOptions)
		{
			this._integratedSecurity = connectionOptions._integratedSecurity;
			this._encrypt = connectionOptions._encrypt;
			if (setEnlistValue != null)
			{
				this._enlist = setEnlistValue.Value;
			}
			else
			{
				this._enlist = connectionOptions._enlist;
			}
			this._mars = connectionOptions._mars;
			this._persistSecurityInfo = connectionOptions._persistSecurityInfo;
			this._pooling = connectionOptions._pooling;
			this._replication = connectionOptions._replication;
			this._userInstance = userInstance;
			this._connectTimeout = connectionOptions._connectTimeout;
			this._loadBalanceTimeout = connectionOptions._loadBalanceTimeout;
			this._maxPoolSize = connectionOptions._maxPoolSize;
			this._minPoolSize = connectionOptions._minPoolSize;
			this._multiSubnetFailover = connectionOptions._multiSubnetFailover;
			this._packetSize = connectionOptions._packetSize;
			this._applicationName = connectionOptions._applicationName;
			this._attachDBFileName = connectionOptions._attachDBFileName;
			this._currentLanguage = connectionOptions._currentLanguage;
			this._dataSource = dataSource;
			this._localDBInstance = LocalDBAPI.GetLocalDbInstanceNameFromServerName(this._dataSource);
			this._failoverPartner = connectionOptions._failoverPartner;
			this._initialCatalog = connectionOptions._initialCatalog;
			this._password = connectionOptions._password;
			this._userID = connectionOptions._userID;
			this._workstationId = connectionOptions._workstationId;
			this._typeSystemVersion = connectionOptions._typeSystemVersion;
			this._transactionBinding = connectionOptions._transactionBinding;
			this._applicationIntent = connectionOptions._applicationIntent;
			this._connectRetryCount = connectionOptions._connectRetryCount;
			this._connectRetryInterval = connectionOptions._connectRetryInterval;
			this.ValidateValueLength(this._dataSource, 128, "data source");
		}

		// Token: 0x17000301 RID: 769
		// (get) Token: 0x06001130 RID: 4400 RVA: 0x00058D9D File Offset: 0x00056F9D
		internal bool IntegratedSecurity
		{
			get
			{
				return this._integratedSecurity;
			}
		}

		// Token: 0x17000302 RID: 770
		// (get) Token: 0x06001131 RID: 4401 RVA: 0x0000EF1B File Offset: 0x0000D11B
		internal bool Asynchronous
		{
			get
			{
				return true;
			}
		}

		// Token: 0x17000303 RID: 771
		// (get) Token: 0x06001132 RID: 4402 RVA: 0x0000EF1B File Offset: 0x0000D11B
		internal bool ConnectionReset
		{
			get
			{
				return true;
			}
		}

		// Token: 0x17000304 RID: 772
		// (get) Token: 0x06001133 RID: 4403 RVA: 0x00058DA5 File Offset: 0x00056FA5
		internal bool Encrypt
		{
			get
			{
				return this._encrypt;
			}
		}

		// Token: 0x17000305 RID: 773
		// (get) Token: 0x06001134 RID: 4404 RVA: 0x00058DAD File Offset: 0x00056FAD
		internal bool TrustServerCertificate
		{
			get
			{
				return this._trustServerCertificate;
			}
		}

		// Token: 0x17000306 RID: 774
		// (get) Token: 0x06001135 RID: 4405 RVA: 0x00058DB5 File Offset: 0x00056FB5
		internal bool Enlist
		{
			get
			{
				return this._enlist;
			}
		}

		// Token: 0x17000307 RID: 775
		// (get) Token: 0x06001136 RID: 4406 RVA: 0x00058DBD File Offset: 0x00056FBD
		internal bool MARS
		{
			get
			{
				return this._mars;
			}
		}

		// Token: 0x17000308 RID: 776
		// (get) Token: 0x06001137 RID: 4407 RVA: 0x00058DC5 File Offset: 0x00056FC5
		internal bool MultiSubnetFailover
		{
			get
			{
				return this._multiSubnetFailover;
			}
		}

		// Token: 0x17000309 RID: 777
		// (get) Token: 0x06001138 RID: 4408 RVA: 0x00058DCD File Offset: 0x00056FCD
		internal bool PersistSecurityInfo
		{
			get
			{
				return this._persistSecurityInfo;
			}
		}

		// Token: 0x1700030A RID: 778
		// (get) Token: 0x06001139 RID: 4409 RVA: 0x00058DD5 File Offset: 0x00056FD5
		internal bool Pooling
		{
			get
			{
				return this._pooling;
			}
		}

		// Token: 0x1700030B RID: 779
		// (get) Token: 0x0600113A RID: 4410 RVA: 0x00058DDD File Offset: 0x00056FDD
		internal bool Replication
		{
			get
			{
				return this._replication;
			}
		}

		// Token: 0x1700030C RID: 780
		// (get) Token: 0x0600113B RID: 4411 RVA: 0x00058DE5 File Offset: 0x00056FE5
		internal bool UserInstance
		{
			get
			{
				return this._userInstance;
			}
		}

		// Token: 0x1700030D RID: 781
		// (get) Token: 0x0600113C RID: 4412 RVA: 0x00058DED File Offset: 0x00056FED
		internal int ConnectTimeout
		{
			get
			{
				return this._connectTimeout;
			}
		}

		// Token: 0x1700030E RID: 782
		// (get) Token: 0x0600113D RID: 4413 RVA: 0x00058DF5 File Offset: 0x00056FF5
		internal int LoadBalanceTimeout
		{
			get
			{
				return this._loadBalanceTimeout;
			}
		}

		// Token: 0x1700030F RID: 783
		// (get) Token: 0x0600113E RID: 4414 RVA: 0x00058DFD File Offset: 0x00056FFD
		internal int MaxPoolSize
		{
			get
			{
				return this._maxPoolSize;
			}
		}

		// Token: 0x17000310 RID: 784
		// (get) Token: 0x0600113F RID: 4415 RVA: 0x00058E05 File Offset: 0x00057005
		internal int MinPoolSize
		{
			get
			{
				return this._minPoolSize;
			}
		}

		// Token: 0x17000311 RID: 785
		// (get) Token: 0x06001140 RID: 4416 RVA: 0x00058E0D File Offset: 0x0005700D
		internal int PacketSize
		{
			get
			{
				return this._packetSize;
			}
		}

		// Token: 0x17000312 RID: 786
		// (get) Token: 0x06001141 RID: 4417 RVA: 0x00058E15 File Offset: 0x00057015
		internal int ConnectRetryCount
		{
			get
			{
				return this._connectRetryCount;
			}
		}

		// Token: 0x17000313 RID: 787
		// (get) Token: 0x06001142 RID: 4418 RVA: 0x00058E1D File Offset: 0x0005701D
		internal int ConnectRetryInterval
		{
			get
			{
				return this._connectRetryInterval;
			}
		}

		// Token: 0x17000314 RID: 788
		// (get) Token: 0x06001143 RID: 4419 RVA: 0x00058E25 File Offset: 0x00057025
		internal ApplicationIntent ApplicationIntent
		{
			get
			{
				return this._applicationIntent;
			}
		}

		// Token: 0x17000315 RID: 789
		// (get) Token: 0x06001144 RID: 4420 RVA: 0x00058E2D File Offset: 0x0005702D
		internal string ApplicationName
		{
			get
			{
				return this._applicationName;
			}
		}

		// Token: 0x17000316 RID: 790
		// (get) Token: 0x06001145 RID: 4421 RVA: 0x00058E35 File Offset: 0x00057035
		internal string AttachDBFilename
		{
			get
			{
				return this._attachDBFileName;
			}
		}

		// Token: 0x17000317 RID: 791
		// (get) Token: 0x06001146 RID: 4422 RVA: 0x00058E3D File Offset: 0x0005703D
		internal string CurrentLanguage
		{
			get
			{
				return this._currentLanguage;
			}
		}

		// Token: 0x17000318 RID: 792
		// (get) Token: 0x06001147 RID: 4423 RVA: 0x00058E45 File Offset: 0x00057045
		internal string DataSource
		{
			get
			{
				return this._dataSource;
			}
		}

		// Token: 0x17000319 RID: 793
		// (get) Token: 0x06001148 RID: 4424 RVA: 0x00058E4D File Offset: 0x0005704D
		internal string LocalDBInstance
		{
			get
			{
				return this._localDBInstance;
			}
		}

		// Token: 0x1700031A RID: 794
		// (get) Token: 0x06001149 RID: 4425 RVA: 0x00058E55 File Offset: 0x00057055
		internal string FailoverPartner
		{
			get
			{
				return this._failoverPartner;
			}
		}

		// Token: 0x1700031B RID: 795
		// (get) Token: 0x0600114A RID: 4426 RVA: 0x00058E5D File Offset: 0x0005705D
		internal string InitialCatalog
		{
			get
			{
				return this._initialCatalog;
			}
		}

		// Token: 0x1700031C RID: 796
		// (get) Token: 0x0600114B RID: 4427 RVA: 0x00058E65 File Offset: 0x00057065
		internal string Password
		{
			get
			{
				return this._password;
			}
		}

		// Token: 0x1700031D RID: 797
		// (get) Token: 0x0600114C RID: 4428 RVA: 0x00058E6D File Offset: 0x0005706D
		internal string UserID
		{
			get
			{
				return this._userID;
			}
		}

		// Token: 0x1700031E RID: 798
		// (get) Token: 0x0600114D RID: 4429 RVA: 0x00058E75 File Offset: 0x00057075
		internal string WorkstationId
		{
			get
			{
				return this._workstationId;
			}
		}

		// Token: 0x1700031F RID: 799
		// (get) Token: 0x0600114E RID: 4430 RVA: 0x00058E7D File Offset: 0x0005707D
		internal SqlConnectionString.TypeSystem TypeSystemVersion
		{
			get
			{
				return this._typeSystemVersion;
			}
		}

		// Token: 0x17000320 RID: 800
		// (get) Token: 0x0600114F RID: 4431 RVA: 0x00058E85 File Offset: 0x00057085
		internal SqlConnectionString.TransactionBindingEnum TransactionBinding
		{
			get
			{
				return this._transactionBinding;
			}
		}

		// Token: 0x06001150 RID: 4432 RVA: 0x00058E90 File Offset: 0x00057090
		internal static Dictionary<string, string> GetParseSynonyms()
		{
			Dictionary<string, string> dictionary = SqlConnectionString.s_sqlClientSynonyms;
			if (dictionary == null)
			{
				dictionary = new Dictionary<string, string>(54)
				{
					{
						"applicationintent",
						"applicationintent"
					},
					{
						"application name",
						"application name"
					},
					{
						"asynchronous processing",
						"asynchronous processing"
					},
					{
						"attachdbfilename",
						"attachdbfilename"
					},
					{
						"connect timeout",
						"connect timeout"
					},
					{
						"connection reset",
						"connection reset"
					},
					{
						"context connection",
						"context connection"
					},
					{
						"current language",
						"current language"
					},
					{
						"data source",
						"data source"
					},
					{
						"encrypt",
						"encrypt"
					},
					{
						"enlist",
						"enlist"
					},
					{
						"failover partner",
						"failover partner"
					},
					{
						"initial catalog",
						"initial catalog"
					},
					{
						"integrated security",
						"integrated security"
					},
					{
						"load balance timeout",
						"load balance timeout"
					},
					{
						"multipleactiveresultsets",
						"multipleactiveresultsets"
					},
					{
						"max pool size",
						"max pool size"
					},
					{
						"min pool size",
						"min pool size"
					},
					{
						"multisubnetfailover",
						"multisubnetfailover"
					},
					{
						"network library",
						"network library"
					},
					{
						"packet size",
						"packet size"
					},
					{
						"password",
						"password"
					},
					{
						"persist security info",
						"persist security info"
					},
					{
						"pooling",
						"pooling"
					},
					{
						"replication",
						"replication"
					},
					{
						"trustservercertificate",
						"trustservercertificate"
					},
					{
						"transaction binding",
						"transaction binding"
					},
					{
						"type system version",
						"type system version"
					},
					{
						"user id",
						"user id"
					},
					{
						"user instance",
						"user instance"
					},
					{
						"workstation id",
						"workstation id"
					},
					{
						"connectretrycount",
						"connectretrycount"
					},
					{
						"connectretryinterval",
						"connectretryinterval"
					},
					{
						"app",
						"application name"
					},
					{
						"async",
						"asynchronous processing"
					},
					{
						"extended properties",
						"attachdbfilename"
					},
					{
						"initial file name",
						"attachdbfilename"
					},
					{
						"connection timeout",
						"connect timeout"
					},
					{
						"timeout",
						"connect timeout"
					},
					{
						"language",
						"current language"
					},
					{
						"addr",
						"data source"
					},
					{
						"address",
						"data source"
					},
					{
						"network address",
						"data source"
					},
					{
						"server",
						"data source"
					},
					{
						"database",
						"initial catalog"
					},
					{
						"trusted_connection",
						"integrated security"
					},
					{
						"connection lifetime",
						"load balance timeout"
					},
					{
						"net",
						"network library"
					},
					{
						"network",
						"network library"
					},
					{
						"pwd",
						"password"
					},
					{
						"persistsecurityinfo",
						"persist security info"
					},
					{
						"uid",
						"user id"
					},
					{
						"user",
						"user id"
					},
					{
						"wsid",
						"workstation id"
					}
				};
				SqlConnectionString.s_sqlClientSynonyms = dictionary;
			}
			return dictionary;
		}

		// Token: 0x06001151 RID: 4433 RVA: 0x00059218 File Offset: 0x00057418
		internal string ObtainWorkstationId()
		{
			string text = this.WorkstationId;
			if (text == null)
			{
				text = ADP.MachineName();
				this.ValidateValueLength(text, 128, "workstation id");
			}
			return text;
		}

		// Token: 0x06001152 RID: 4434 RVA: 0x00059247 File Offset: 0x00057447
		private void ValidateValueLength(string value, int limit, string key)
		{
			if (limit < value.Length)
			{
				throw ADP.InvalidConnectionOptionValueLength(key, limit);
			}
		}

		// Token: 0x06001153 RID: 4435 RVA: 0x0005925C File Offset: 0x0005745C
		internal ApplicationIntent ConvertValueToApplicationIntent()
		{
			string value;
			if (!base.TryGetParsetableValue("applicationintent", out value))
			{
				return ApplicationIntent.ReadWrite;
			}
			ApplicationIntent result;
			try
			{
				result = DbConnectionStringBuilderUtil.ConvertToApplicationIntent("applicationintent", value);
			}
			catch (FormatException inner)
			{
				throw ADP.InvalidConnectionOptionValue("applicationintent", inner);
			}
			catch (OverflowException inner2)
			{
				throw ADP.InvalidConnectionOptionValue("applicationintent", inner2);
			}
			return result;
		}

		// Token: 0x06001154 RID: 4436 RVA: 0x000592C0 File Offset: 0x000574C0
		internal void ThrowUnsupportedIfKeywordSet(string keyword)
		{
			if (base.ContainsKey(keyword))
			{
				throw SQL.UnsupportedKeyword(keyword);
			}
		}

		// Token: 0x04000BDC RID: 3036
		internal const int SynonymCount = 18;

		// Token: 0x04000BDD RID: 3037
		internal const int DeprecatedSynonymCount = 3;

		// Token: 0x04000BDE RID: 3038
		private static Dictionary<string, string> s_sqlClientSynonyms;

		// Token: 0x04000BDF RID: 3039
		private readonly bool _integratedSecurity;

		// Token: 0x04000BE0 RID: 3040
		private readonly bool _encrypt;

		// Token: 0x04000BE1 RID: 3041
		private readonly bool _trustServerCertificate;

		// Token: 0x04000BE2 RID: 3042
		private readonly bool _enlist;

		// Token: 0x04000BE3 RID: 3043
		private readonly bool _mars;

		// Token: 0x04000BE4 RID: 3044
		private readonly bool _persistSecurityInfo;

		// Token: 0x04000BE5 RID: 3045
		private readonly bool _pooling;

		// Token: 0x04000BE6 RID: 3046
		private readonly bool _replication;

		// Token: 0x04000BE7 RID: 3047
		private readonly bool _userInstance;

		// Token: 0x04000BE8 RID: 3048
		private readonly bool _multiSubnetFailover;

		// Token: 0x04000BE9 RID: 3049
		private readonly int _connectTimeout;

		// Token: 0x04000BEA RID: 3050
		private readonly int _loadBalanceTimeout;

		// Token: 0x04000BEB RID: 3051
		private readonly int _maxPoolSize;

		// Token: 0x04000BEC RID: 3052
		private readonly int _minPoolSize;

		// Token: 0x04000BED RID: 3053
		private readonly int _packetSize;

		// Token: 0x04000BEE RID: 3054
		private readonly int _connectRetryCount;

		// Token: 0x04000BEF RID: 3055
		private readonly int _connectRetryInterval;

		// Token: 0x04000BF0 RID: 3056
		private readonly ApplicationIntent _applicationIntent;

		// Token: 0x04000BF1 RID: 3057
		private readonly string _applicationName;

		// Token: 0x04000BF2 RID: 3058
		private readonly string _attachDBFileName;

		// Token: 0x04000BF3 RID: 3059
		private readonly string _currentLanguage;

		// Token: 0x04000BF4 RID: 3060
		private readonly string _dataSource;

		// Token: 0x04000BF5 RID: 3061
		private readonly string _localDBInstance;

		// Token: 0x04000BF6 RID: 3062
		private readonly string _failoverPartner;

		// Token: 0x04000BF7 RID: 3063
		private readonly string _initialCatalog;

		// Token: 0x04000BF8 RID: 3064
		private readonly string _password;

		// Token: 0x04000BF9 RID: 3065
		private readonly string _userID;

		// Token: 0x04000BFA RID: 3066
		private readonly string _workstationId;

		// Token: 0x04000BFB RID: 3067
		private readonly SqlConnectionString.TypeSystem _typeSystemVersion;

		// Token: 0x04000BFC RID: 3068
		private readonly SqlConnectionString.TransactionBindingEnum _transactionBinding;

		// Token: 0x02000175 RID: 373
		internal static class DEFAULT
		{
			// Token: 0x04000BFD RID: 3069
			internal const ApplicationIntent ApplicationIntent = ApplicationIntent.ReadWrite;

			// Token: 0x04000BFE RID: 3070
			internal const string Application_Name = "Core .Net SqlClient Data Provider";

			// Token: 0x04000BFF RID: 3071
			internal const string AttachDBFilename = "";

			// Token: 0x04000C00 RID: 3072
			internal const int Connect_Timeout = 15;

			// Token: 0x04000C01 RID: 3073
			internal const string Current_Language = "";

			// Token: 0x04000C02 RID: 3074
			internal const string Data_Source = "";

			// Token: 0x04000C03 RID: 3075
			internal const bool Encrypt = false;

			// Token: 0x04000C04 RID: 3076
			internal const bool Enlist = true;

			// Token: 0x04000C05 RID: 3077
			internal const string FailoverPartner = "";

			// Token: 0x04000C06 RID: 3078
			internal const string Initial_Catalog = "";

			// Token: 0x04000C07 RID: 3079
			internal const bool Integrated_Security = false;

			// Token: 0x04000C08 RID: 3080
			internal const int Load_Balance_Timeout = 0;

			// Token: 0x04000C09 RID: 3081
			internal const bool MARS = false;

			// Token: 0x04000C0A RID: 3082
			internal const int Max_Pool_Size = 100;

			// Token: 0x04000C0B RID: 3083
			internal const int Min_Pool_Size = 0;

			// Token: 0x04000C0C RID: 3084
			internal const bool MultiSubnetFailover = false;

			// Token: 0x04000C0D RID: 3085
			internal const int Packet_Size = 8000;

			// Token: 0x04000C0E RID: 3086
			internal const string Password = "";

			// Token: 0x04000C0F RID: 3087
			internal const bool Persist_Security_Info = false;

			// Token: 0x04000C10 RID: 3088
			internal const bool Pooling = true;

			// Token: 0x04000C11 RID: 3089
			internal const bool TrustServerCertificate = false;

			// Token: 0x04000C12 RID: 3090
			internal const string Type_System_Version = "";

			// Token: 0x04000C13 RID: 3091
			internal const string User_ID = "";

			// Token: 0x04000C14 RID: 3092
			internal const bool User_Instance = false;

			// Token: 0x04000C15 RID: 3093
			internal const bool Replication = false;

			// Token: 0x04000C16 RID: 3094
			internal const int Connect_Retry_Count = 1;

			// Token: 0x04000C17 RID: 3095
			internal const int Connect_Retry_Interval = 10;
		}

		// Token: 0x02000176 RID: 374
		internal static class KEY
		{
			// Token: 0x04000C18 RID: 3096
			internal const string ApplicationIntent = "applicationintent";

			// Token: 0x04000C19 RID: 3097
			internal const string Application_Name = "application name";

			// Token: 0x04000C1A RID: 3098
			internal const string AsynchronousProcessing = "asynchronous processing";

			// Token: 0x04000C1B RID: 3099
			internal const string AttachDBFilename = "attachdbfilename";

			// Token: 0x04000C1C RID: 3100
			internal const string Connect_Timeout = "connect timeout";

			// Token: 0x04000C1D RID: 3101
			internal const string Connection_Reset = "connection reset";

			// Token: 0x04000C1E RID: 3102
			internal const string Context_Connection = "context connection";

			// Token: 0x04000C1F RID: 3103
			internal const string Current_Language = "current language";

			// Token: 0x04000C20 RID: 3104
			internal const string Data_Source = "data source";

			// Token: 0x04000C21 RID: 3105
			internal const string Encrypt = "encrypt";

			// Token: 0x04000C22 RID: 3106
			internal const string Enlist = "enlist";

			// Token: 0x04000C23 RID: 3107
			internal const string FailoverPartner = "failover partner";

			// Token: 0x04000C24 RID: 3108
			internal const string Initial_Catalog = "initial catalog";

			// Token: 0x04000C25 RID: 3109
			internal const string Integrated_Security = "integrated security";

			// Token: 0x04000C26 RID: 3110
			internal const string Load_Balance_Timeout = "load balance timeout";

			// Token: 0x04000C27 RID: 3111
			internal const string MARS = "multipleactiveresultsets";

			// Token: 0x04000C28 RID: 3112
			internal const string Max_Pool_Size = "max pool size";

			// Token: 0x04000C29 RID: 3113
			internal const string Min_Pool_Size = "min pool size";

			// Token: 0x04000C2A RID: 3114
			internal const string MultiSubnetFailover = "multisubnetfailover";

			// Token: 0x04000C2B RID: 3115
			internal const string Network_Library = "network library";

			// Token: 0x04000C2C RID: 3116
			internal const string Packet_Size = "packet size";

			// Token: 0x04000C2D RID: 3117
			internal const string Password = "password";

			// Token: 0x04000C2E RID: 3118
			internal const string Persist_Security_Info = "persist security info";

			// Token: 0x04000C2F RID: 3119
			internal const string Pooling = "pooling";

			// Token: 0x04000C30 RID: 3120
			internal const string TransactionBinding = "transaction binding";

			// Token: 0x04000C31 RID: 3121
			internal const string TrustServerCertificate = "trustservercertificate";

			// Token: 0x04000C32 RID: 3122
			internal const string Type_System_Version = "type system version";

			// Token: 0x04000C33 RID: 3123
			internal const string User_ID = "user id";

			// Token: 0x04000C34 RID: 3124
			internal const string User_Instance = "user instance";

			// Token: 0x04000C35 RID: 3125
			internal const string Workstation_Id = "workstation id";

			// Token: 0x04000C36 RID: 3126
			internal const string Replication = "replication";

			// Token: 0x04000C37 RID: 3127
			internal const string Connect_Retry_Count = "connectretrycount";

			// Token: 0x04000C38 RID: 3128
			internal const string Connect_Retry_Interval = "connectretryinterval";
		}

		// Token: 0x02000177 RID: 375
		private static class SYNONYM
		{
			// Token: 0x04000C39 RID: 3129
			internal const string APP = "app";

			// Token: 0x04000C3A RID: 3130
			internal const string Async = "async";

			// Token: 0x04000C3B RID: 3131
			internal const string EXTENDED_PROPERTIES = "extended properties";

			// Token: 0x04000C3C RID: 3132
			internal const string INITIAL_FILE_NAME = "initial file name";

			// Token: 0x04000C3D RID: 3133
			internal const string CONNECTION_TIMEOUT = "connection timeout";

			// Token: 0x04000C3E RID: 3134
			internal const string TIMEOUT = "timeout";

			// Token: 0x04000C3F RID: 3135
			internal const string LANGUAGE = "language";

			// Token: 0x04000C40 RID: 3136
			internal const string ADDR = "addr";

			// Token: 0x04000C41 RID: 3137
			internal const string ADDRESS = "address";

			// Token: 0x04000C42 RID: 3138
			internal const string SERVER = "server";

			// Token: 0x04000C43 RID: 3139
			internal const string NETWORK_ADDRESS = "network address";

			// Token: 0x04000C44 RID: 3140
			internal const string DATABASE = "database";

			// Token: 0x04000C45 RID: 3141
			internal const string TRUSTED_CONNECTION = "trusted_connection";

			// Token: 0x04000C46 RID: 3142
			internal const string Connection_Lifetime = "connection lifetime";

			// Token: 0x04000C47 RID: 3143
			internal const string NET = "net";

			// Token: 0x04000C48 RID: 3144
			internal const string NETWORK = "network";

			// Token: 0x04000C49 RID: 3145
			internal const string Pwd = "pwd";

			// Token: 0x04000C4A RID: 3146
			internal const string PERSISTSECURITYINFO = "persistsecurityinfo";

			// Token: 0x04000C4B RID: 3147
			internal const string UID = "uid";

			// Token: 0x04000C4C RID: 3148
			internal const string User = "user";

			// Token: 0x04000C4D RID: 3149
			internal const string WSID = "wsid";
		}

		// Token: 0x02000178 RID: 376
		internal enum TypeSystem
		{
			// Token: 0x04000C4F RID: 3151
			Latest = 2008,
			// Token: 0x04000C50 RID: 3152
			SQLServer2000 = 2000,
			// Token: 0x04000C51 RID: 3153
			SQLServer2005 = 2005,
			// Token: 0x04000C52 RID: 3154
			SQLServer2008 = 2008,
			// Token: 0x04000C53 RID: 3155
			SQLServer2012 = 2012
		}

		// Token: 0x02000179 RID: 377
		internal static class TYPESYSTEMVERSION
		{
			// Token: 0x04000C54 RID: 3156
			internal const string Latest = "Latest";

			// Token: 0x04000C55 RID: 3157
			internal const string SQL_Server_2000 = "SQL Server 2000";

			// Token: 0x04000C56 RID: 3158
			internal const string SQL_Server_2005 = "SQL Server 2005";

			// Token: 0x04000C57 RID: 3159
			internal const string SQL_Server_2008 = "SQL Server 2008";

			// Token: 0x04000C58 RID: 3160
			internal const string SQL_Server_2012 = "SQL Server 2012";
		}

		// Token: 0x0200017A RID: 378
		internal enum TransactionBindingEnum
		{
			// Token: 0x04000C5A RID: 3162
			ImplicitUnbind,
			// Token: 0x04000C5B RID: 3163
			ExplicitUnbind
		}

		// Token: 0x0200017B RID: 379
		internal static class TRANSACTIONBINDING
		{
			// Token: 0x04000C5C RID: 3164
			internal const string ImplicitUnbind = "Implicit Unbind";

			// Token: 0x04000C5D RID: 3165
			internal const string ExplicitUnbind = "Explicit Unbind";
		}
	}
}
