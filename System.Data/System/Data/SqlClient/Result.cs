﻿using System;
using System.Collections.Generic;

namespace System.Data.SqlClient
{
	// Token: 0x02000132 RID: 306
	internal sealed class Result
	{
		// Token: 0x06000EC9 RID: 3785 RVA: 0x0004D4C6 File Offset: 0x0004B6C6
		internal Result(_SqlMetaDataSet metadata)
		{
			this._metadata = metadata;
			this._rowset = new List<Row>();
		}

		// Token: 0x1700029B RID: 667
		// (get) Token: 0x06000ECA RID: 3786 RVA: 0x0004D4E0 File Offset: 0x0004B6E0
		internal int Count
		{
			get
			{
				return this._rowset.Count;
			}
		}

		// Token: 0x1700029C RID: 668
		// (get) Token: 0x06000ECB RID: 3787 RVA: 0x0004D4ED File Offset: 0x0004B6ED
		internal _SqlMetaDataSet MetaData
		{
			get
			{
				return this._metadata;
			}
		}

		// Token: 0x1700029D RID: 669
		internal Row this[int index]
		{
			get
			{
				return this._rowset[index];
			}
		}

		// Token: 0x06000ECD RID: 3789 RVA: 0x0004D503 File Offset: 0x0004B703
		internal void AddRow(Row row)
		{
			this._rowset.Add(row);
		}

		// Token: 0x04000A77 RID: 2679
		private readonly _SqlMetaDataSet _metadata;

		// Token: 0x04000A78 RID: 2680
		private readonly List<Row> _rowset;
	}
}
