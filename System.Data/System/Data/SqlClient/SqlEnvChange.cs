﻿using System;

namespace System.Data.SqlClient
{
	// Token: 0x020001FD RID: 509
	internal sealed class SqlEnvChange
	{
		// Token: 0x060016D0 RID: 5840 RVA: 0x00005C14 File Offset: 0x00003E14
		public SqlEnvChange()
		{
		}

		// Token: 0x040010FF RID: 4351
		internal byte type;

		// Token: 0x04001100 RID: 4352
		internal byte oldLength;

		// Token: 0x04001101 RID: 4353
		internal int newLength;

		// Token: 0x04001102 RID: 4354
		internal int length;

		// Token: 0x04001103 RID: 4355
		internal string newValue;

		// Token: 0x04001104 RID: 4356
		internal string oldValue;

		// Token: 0x04001105 RID: 4357
		internal byte[] newBinValue;

		// Token: 0x04001106 RID: 4358
		internal byte[] oldBinValue;

		// Token: 0x04001107 RID: 4359
		internal long newLongValue;

		// Token: 0x04001108 RID: 4360
		internal long oldLongValue;

		// Token: 0x04001109 RID: 4361
		internal SqlCollation newCollation;

		// Token: 0x0400110A RID: 4362
		internal SqlCollation oldCollation;

		// Token: 0x0400110B RID: 4363
		internal RoutingInfo newRoutingInfo;
	}
}
