﻿using System;

namespace System.Data.SqlClient
{
	/// <summary>Represents the method that will handle the <see cref="E:System.Data.SqlClient.SqlDataAdapter.RowUpdated" /> event of a <see cref="T:System.Data.SqlClient.SqlDataAdapter" />.</summary>
	/// <param name="sender">The source of the event. </param>
	/// <param name="e">The <see cref="T:System.Data.SqlClient.SqlRowUpdatedEventArgs" /> that contains the event data. </param>
	// Token: 0x020001BE RID: 446
	// (Invoke) Token: 0x06001491 RID: 5265
	public delegate void SqlRowUpdatedEventHandler(object sender, SqlRowUpdatedEventArgs e);
}
