﻿using System;
using System.Data.ProviderBase;

namespace System.Data.SqlClient
{
	// Token: 0x02000171 RID: 369
	internal sealed class SqlConnectionPoolGroupProviderInfo : DbConnectionPoolGroupProviderInfo
	{
		// Token: 0x0600111E RID: 4382 RVA: 0x000584B7 File Offset: 0x000566B7
		internal SqlConnectionPoolGroupProviderInfo(SqlConnectionString connectionOptions)
		{
			this._failoverPartner = connectionOptions.FailoverPartner;
			if (string.IsNullOrEmpty(this._failoverPartner))
			{
				this._failoverPartner = null;
			}
		}

		// Token: 0x170002FD RID: 765
		// (get) Token: 0x0600111F RID: 4383 RVA: 0x000584DF File Offset: 0x000566DF
		internal string FailoverPartner
		{
			get
			{
				return this._failoverPartner;
			}
		}

		// Token: 0x170002FE RID: 766
		// (get) Token: 0x06001120 RID: 4384 RVA: 0x000584E7 File Offset: 0x000566E7
		internal bool UseFailoverPartner
		{
			get
			{
				return this._useFailoverPartner;
			}
		}

		// Token: 0x06001121 RID: 4385 RVA: 0x000584F0 File Offset: 0x000566F0
		internal void AliasCheck(string server)
		{
			if (this._alias != server)
			{
				lock (this)
				{
					if (this._alias == null)
					{
						this._alias = server;
					}
					else if (this._alias != server)
					{
						base.PoolGroup.Clear();
						this._alias = server;
					}
				}
			}
		}

		// Token: 0x06001122 RID: 4386 RVA: 0x00058568 File Offset: 0x00056768
		internal void FailoverCheck(SqlInternalConnection connection, bool actualUseFailoverPartner, SqlConnectionString userConnectionOptions, string actualFailoverPartner)
		{
			if (this.UseFailoverPartner != actualUseFailoverPartner)
			{
				base.PoolGroup.Clear();
				this._useFailoverPartner = actualUseFailoverPartner;
			}
			if (!this._useFailoverPartner && this._failoverPartner != actualFailoverPartner)
			{
				lock (this)
				{
					if (this._failoverPartner != actualFailoverPartner)
					{
						this._failoverPartner = actualFailoverPartner;
					}
				}
			}
		}

		// Token: 0x04000BD7 RID: 3031
		private string _alias;

		// Token: 0x04000BD8 RID: 3032
		private string _failoverPartner;

		// Token: 0x04000BD9 RID: 3033
		private bool _useFailoverPartner;
	}
}
