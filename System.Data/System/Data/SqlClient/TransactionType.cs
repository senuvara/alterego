﻿using System;

namespace System.Data.SqlClient
{
	// Token: 0x02000216 RID: 534
	internal enum TransactionType
	{
		// Token: 0x040011DA RID: 4570
		LocalFromTSQL = 1,
		// Token: 0x040011DB RID: 4571
		LocalFromAPI,
		// Token: 0x040011DC RID: 4572
		Delegated,
		// Token: 0x040011DD RID: 4573
		Distributed,
		// Token: 0x040011DE RID: 4574
		Context
	}
}
