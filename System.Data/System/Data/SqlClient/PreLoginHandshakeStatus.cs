﻿using System;

namespace System.Data.SqlClient
{
	// Token: 0x020001F7 RID: 503
	internal enum PreLoginHandshakeStatus
	{
		// Token: 0x040010DC RID: 4316
		Successful,
		// Token: 0x040010DD RID: 4317
		InstanceFailure
	}
}
