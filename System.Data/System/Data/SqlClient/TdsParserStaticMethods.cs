﻿using System;
using System.Data.Common;
using System.Threading;

namespace System.Data.SqlClient
{
	// Token: 0x02000212 RID: 530
	internal sealed class TdsParserStaticMethods
	{
		// Token: 0x060017A3 RID: 6051 RVA: 0x0007B9EC File Offset: 0x00079BEC
		internal static byte[] ObfuscatePassword(string password)
		{
			byte[] array = new byte[password.Length << 1];
			for (int i = 0; i < password.Length; i++)
			{
				char c = password[i];
				byte b = (byte)(c & 'ÿ');
				byte b2 = (byte)(c >> 8 & 'ÿ');
				array[i << 1] = (byte)(((int)(b & 15) << 4 | b >> 4) ^ 165);
				array[(i << 1) + 1] = (byte)(((int)(b2 & 15) << 4 | b2 >> 4) ^ 165);
			}
			return array;
		}

		// Token: 0x060017A4 RID: 6052 RVA: 0x0007BA64 File Offset: 0x00079C64
		internal static int GetCurrentProcessIdForTdsLoginOnly()
		{
			if (TdsParserStaticMethods.s_currentProcessId == -1)
			{
				int value = new Random().Next();
				Interlocked.CompareExchange(ref TdsParserStaticMethods.s_currentProcessId, value, -1);
			}
			return TdsParserStaticMethods.s_currentProcessId;
		}

		// Token: 0x060017A5 RID: 6053 RVA: 0x0007BA96 File Offset: 0x00079C96
		internal static int GetCurrentThreadIdForTdsLoginOnly()
		{
			return Environment.CurrentManagedThreadId;
		}

		// Token: 0x060017A6 RID: 6054 RVA: 0x0007BAA0 File Offset: 0x00079CA0
		internal static byte[] GetNetworkPhysicalAddressForTdsLoginOnly()
		{
			if (TdsParserStaticMethods.s_nicAddress == null)
			{
				byte[] array = new byte[6];
				new Random().NextBytes(array);
				Interlocked.CompareExchange<byte[]>(ref TdsParserStaticMethods.s_nicAddress, array, null);
			}
			return TdsParserStaticMethods.s_nicAddress;
		}

		// Token: 0x060017A7 RID: 6055 RVA: 0x0007BAD8 File Offset: 0x00079CD8
		internal static int GetTimeoutMilliseconds(long timeoutTime)
		{
			if (9223372036854775807L == timeoutTime)
			{
				return -1;
			}
			long num = ADP.TimerRemainingMilliseconds(timeoutTime);
			if (num < 0L)
			{
				return 0;
			}
			if (num > 2147483647L)
			{
				return int.MaxValue;
			}
			return (int)num;
		}

		// Token: 0x060017A8 RID: 6056 RVA: 0x0007BB14 File Offset: 0x00079D14
		internal static long GetTimeout(long timeoutMilliseconds)
		{
			long result;
			if (timeoutMilliseconds <= 0L)
			{
				result = long.MaxValue;
			}
			else
			{
				try
				{
					result = checked(ADP.TimerCurrent() + ADP.TimerFromMilliseconds(timeoutMilliseconds));
				}
				catch (OverflowException)
				{
					result = long.MaxValue;
				}
			}
			return result;
		}

		// Token: 0x060017A9 RID: 6057 RVA: 0x0007BB60 File Offset: 0x00079D60
		internal static bool TimeoutHasExpired(long timeoutTime)
		{
			bool result = false;
			if (timeoutTime != 0L && 9223372036854775807L != timeoutTime)
			{
				result = ADP.TimerHasExpired(timeoutTime);
			}
			return result;
		}

		// Token: 0x060017AA RID: 6058 RVA: 0x0007BB86 File Offset: 0x00079D86
		internal static int NullAwareStringLength(string str)
		{
			if (str == null)
			{
				return 0;
			}
			return str.Length;
		}

		// Token: 0x060017AB RID: 6059 RVA: 0x0007BB94 File Offset: 0x00079D94
		internal static int GetRemainingTimeout(int timeout, long start)
		{
			if (timeout <= 0)
			{
				return timeout;
			}
			long num = ADP.TimerRemainingSeconds(start + ADP.TimerFromSeconds(timeout));
			if (num <= 0L)
			{
				return 1;
			}
			return checked((int)num);
		}

		// Token: 0x060017AC RID: 6060 RVA: 0x00005C14 File Offset: 0x00003E14
		public TdsParserStaticMethods()
		{
		}

		// Token: 0x060017AD RID: 6061 RVA: 0x0007BBBE File Offset: 0x00079DBE
		// Note: this type is marked as 'beforefieldinit'.
		static TdsParserStaticMethods()
		{
		}

		// Token: 0x040011C7 RID: 4551
		private const int NoProcessId = -1;

		// Token: 0x040011C8 RID: 4552
		private static int s_currentProcessId = -1;

		// Token: 0x040011C9 RID: 4553
		private static byte[] s_nicAddress = null;
	}
}
