﻿using System;
using System.Data.Common;
using System.Security;
using System.Security.Permissions;

namespace System.Data.SqlClient
{
	/// <summary>Enables the .NET Framework Data Provider for SQL Server to help make sure that a user has a security level sufficient to access a data source. </summary>
	// Token: 0x02000219 RID: 537
	[Serializable]
	public sealed class SqlClientPermission : DBDataPermission
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Data.SqlClient.SqlClientPermission" /> class.</summary>
		// Token: 0x06001808 RID: 6152 RVA: 0x0007D865 File Offset: 0x0007BA65
		[Obsolete("SqlClientPermission() has been deprecated.  Use the SqlClientPermission(PermissionState.None) constructor.  http://go.microsoft.com/fwlink/?linkid=14202", true)]
		public SqlClientPermission() : this(PermissionState.None)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Data.SqlClient.SqlClientPermission" /> class.</summary>
		/// <param name="state">One of the <see cref="T:System.Security.Permissions.PermissionState" /> values. </param>
		// Token: 0x06001809 RID: 6153 RVA: 0x0007D86E File Offset: 0x0007BA6E
		public SqlClientPermission(PermissionState state) : base(state)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Data.SqlClient.SqlClientPermission" /> class.</summary>
		/// <param name="state">One of the <see cref="T:System.Security.Permissions.PermissionState" /> values. </param>
		/// <param name="allowBlankPassword">Indicates whether a blank password is allowed. </param>
		// Token: 0x0600180A RID: 6154 RVA: 0x0007D877 File Offset: 0x0007BA77
		[Obsolete("SqlClientPermission(PermissionState state, Boolean allowBlankPassword) has been deprecated.  Use the SqlClientPermission(PermissionState.None) constructor.  http://go.microsoft.com/fwlink/?linkid=14202", true)]
		public SqlClientPermission(PermissionState state, bool allowBlankPassword) : this(state)
		{
			base.AllowBlankPassword = allowBlankPassword;
		}

		// Token: 0x0600180B RID: 6155 RVA: 0x0007D887 File Offset: 0x0007BA87
		private SqlClientPermission(SqlClientPermission permission) : base(permission)
		{
		}

		// Token: 0x0600180C RID: 6156 RVA: 0x0007D890 File Offset: 0x0007BA90
		internal SqlClientPermission(SqlClientPermissionAttribute permissionAttribute) : base(permissionAttribute)
		{
		}

		// Token: 0x0600180D RID: 6157 RVA: 0x0007D899 File Offset: 0x0007BA99
		internal SqlClientPermission(SqlConnectionString constr) : base(constr)
		{
			if (constr == null || constr.IsEmpty)
			{
				base.Add(ADP.StrEmpty, ADP.StrEmpty, KeyRestrictionBehavior.AllowOnly);
			}
		}

		/// <summary>Adds a new connection string and a set of restricted keywords to the <see cref="T:System.Data.SqlClient.SqlClientPermission" /> object.</summary>
		/// <param name="connectionString">The connection string.</param>
		/// <param name="restrictions">The key restrictions.</param>
		/// <param name="behavior">One of the <see cref="T:System.Data.KeyRestrictionBehavior" /> enumerations.</param>
		// Token: 0x0600180E RID: 6158 RVA: 0x0007D8C0 File Offset: 0x0007BAC0
		public override void Add(string connectionString, string restrictions, KeyRestrictionBehavior behavior)
		{
			DBConnectionString entry = new DBConnectionString(connectionString, restrictions, behavior, SqlConnectionString.GetParseSynonyms(), false);
			base.AddPermissionEntry(entry);
		}

		/// <summary>Returns the <see cref="T:System.Data.SqlClient.SqlClientPermission" /> as an <see cref="T:System.Security.IPermission" />.</summary>
		/// <returns>A copy of the current permission object.</returns>
		// Token: 0x0600180F RID: 6159 RVA: 0x0007D8E3 File Offset: 0x0007BAE3
		public override IPermission Copy()
		{
			return new SqlClientPermission(this);
		}
	}
}
