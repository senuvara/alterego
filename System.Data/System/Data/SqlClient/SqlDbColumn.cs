﻿using System;
using System.Data.Common;

namespace System.Data.SqlClient
{
	// Token: 0x02000195 RID: 405
	internal class SqlDbColumn : DbColumn
	{
		// Token: 0x060012A5 RID: 4773 RVA: 0x000607B2 File Offset: 0x0005E9B2
		internal SqlDbColumn(_SqlMetaData md)
		{
			this._metadata = md;
			this.Populate();
		}

		// Token: 0x060012A6 RID: 4774 RVA: 0x000607C8 File Offset: 0x0005E9C8
		private void Populate()
		{
			base.AllowDBNull = new bool?(this._metadata.isNullable);
			base.BaseCatalogName = this._metadata.catalogName;
			base.BaseColumnName = this._metadata.baseColumn;
			base.BaseSchemaName = this._metadata.schemaName;
			base.BaseServerName = this._metadata.serverName;
			base.BaseTableName = this._metadata.tableName;
			base.ColumnName = this._metadata.column;
			base.ColumnOrdinal = new int?(this._metadata.ordinal);
			base.ColumnSize = new int?((this._metadata.metaType.IsSizeInCharacters && this._metadata.length != int.MaxValue) ? (this._metadata.length / 2) : this._metadata.length);
			base.IsAutoIncrement = new bool?(this._metadata.isIdentity);
			base.IsIdentity = new bool?(this._metadata.isIdentity);
			base.IsLong = new bool?(this._metadata.metaType.IsLong);
			if (SqlDbType.Timestamp == this._metadata.type)
			{
				base.IsUnique = new bool?(true);
			}
			else
			{
				base.IsUnique = new bool?(false);
			}
			if (255 != this._metadata.precision)
			{
				base.NumericPrecision = new int?((int)this._metadata.precision);
			}
			else
			{
				base.NumericPrecision = new int?((int)this._metadata.metaType.Precision);
			}
			base.IsReadOnly = new bool?(this._metadata.updatability == 0);
			base.UdtAssemblyQualifiedName = this._metadata.udtAssemblyQualifiedName;
		}

		// Token: 0x17000363 RID: 867
		// (set) Token: 0x060012A7 RID: 4775 RVA: 0x0006098F File Offset: 0x0005EB8F
		internal bool? SqlIsAliased
		{
			set
			{
				base.IsAliased = value;
			}
		}

		// Token: 0x17000364 RID: 868
		// (set) Token: 0x060012A8 RID: 4776 RVA: 0x00060998 File Offset: 0x0005EB98
		internal bool? SqlIsKey
		{
			set
			{
				base.IsKey = value;
			}
		}

		// Token: 0x17000365 RID: 869
		// (set) Token: 0x060012A9 RID: 4777 RVA: 0x000609A1 File Offset: 0x0005EBA1
		internal bool? SqlIsHidden
		{
			set
			{
				base.IsHidden = value;
			}
		}

		// Token: 0x17000366 RID: 870
		// (set) Token: 0x060012AA RID: 4778 RVA: 0x000609AA File Offset: 0x0005EBAA
		internal bool? SqlIsExpression
		{
			set
			{
				base.IsExpression = value;
			}
		}

		// Token: 0x17000367 RID: 871
		// (set) Token: 0x060012AB RID: 4779 RVA: 0x000609B3 File Offset: 0x0005EBB3
		internal Type SqlDataType
		{
			set
			{
				base.DataType = value;
			}
		}

		// Token: 0x17000368 RID: 872
		// (set) Token: 0x060012AC RID: 4780 RVA: 0x000609BC File Offset: 0x0005EBBC
		internal string SqlDataTypeName
		{
			set
			{
				base.DataTypeName = value;
			}
		}

		// Token: 0x17000369 RID: 873
		// (set) Token: 0x060012AD RID: 4781 RVA: 0x000609C5 File Offset: 0x0005EBC5
		internal int? SqlNumericScale
		{
			set
			{
				base.NumericScale = value;
			}
		}

		// Token: 0x04000D2D RID: 3373
		private readonly _SqlMetaData _metadata;
	}
}
