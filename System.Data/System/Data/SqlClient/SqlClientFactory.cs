﻿using System;
using System.Data.Common;
using System.Data.Sql;
using System.Security;
using System.Security.Permissions;
using Unity;

namespace System.Data.SqlClient
{
	/// <summary>Represents a set of methods for creating instances of the <see cref="N:System.Data.SqlClient" /> provider's implementation of the data source classes.</summary>
	// Token: 0x0200014B RID: 331
	public sealed class SqlClientFactory : DbProviderFactory, IServiceProvider
	{
		// Token: 0x06000F87 RID: 3975 RVA: 0x0005170C File Offset: 0x0004F90C
		private SqlClientFactory()
		{
		}

		/// <summary>Returns a strongly typed <see cref="T:System.Data.Common.DbCommand" /> instance.</summary>
		/// <returns>A new strongly typed instance of <see cref="T:System.Data.Common.DbCommand" />.</returns>
		// Token: 0x06000F88 RID: 3976 RVA: 0x00051714 File Offset: 0x0004F914
		public override DbCommand CreateCommand()
		{
			return new SqlCommand();
		}

		/// <summary>Returns a strongly typed <see cref="T:System.Data.Common.DbCommandBuilder" /> instance.</summary>
		/// <returns>A new strongly typed instance of <see cref="T:System.Data.Common.DbCommandBuilder" />.</returns>
		// Token: 0x06000F89 RID: 3977 RVA: 0x0005171B File Offset: 0x0004F91B
		public override DbCommandBuilder CreateCommandBuilder()
		{
			return new SqlCommandBuilder();
		}

		/// <summary>Returns a strongly typed <see cref="T:System.Data.Common.DbConnection" /> instance.</summary>
		/// <returns>A new strongly typed instance of <see cref="T:System.Data.Common.DbConnection" />.</returns>
		// Token: 0x06000F8A RID: 3978 RVA: 0x00051722 File Offset: 0x0004F922
		public override DbConnection CreateConnection()
		{
			return new SqlConnection();
		}

		/// <summary>Returns a strongly typed <see cref="T:System.Data.Common.DbConnectionStringBuilder" /> instance.</summary>
		/// <returns>A new strongly typed instance of <see cref="T:System.Data.Common.DbConnectionStringBuilder" />.</returns>
		// Token: 0x06000F8B RID: 3979 RVA: 0x00051729 File Offset: 0x0004F929
		public override DbConnectionStringBuilder CreateConnectionStringBuilder()
		{
			return new SqlConnectionStringBuilder();
		}

		/// <summary>Returns a strongly typed <see cref="T:System.Data.Common.DbDataAdapter" /> instance.</summary>
		/// <returns>A new strongly typed instance of <see cref="T:System.Data.Common.DbDataAdapter" />.</returns>
		// Token: 0x06000F8C RID: 3980 RVA: 0x00051730 File Offset: 0x0004F930
		public override DbDataAdapter CreateDataAdapter()
		{
			return new SqlDataAdapter();
		}

		/// <summary>Returns a strongly typed <see cref="T:System.Data.Common.DbParameter" /> instance.</summary>
		/// <returns>A new strongly typed instance of <see cref="T:System.Data.Common.DbParameter" />.</returns>
		// Token: 0x06000F8D RID: 3981 RVA: 0x00051737 File Offset: 0x0004F937
		public override DbParameter CreateParameter()
		{
			return new SqlParameter();
		}

		/// <summary>Returns <see langword="true" /> if a <see cref="T:System.Data.Sql.SqlDataSourceEnumerator" /> can be created; otherwise <see langword="false" /> .</summary>
		/// <returns>
		///     <see langword="true" /> if a <see cref="T:System.Data.Sql.SqlDataSourceEnumerator" /> can be created; otherwise <see langword="false" />.</returns>
		// Token: 0x170002AE RID: 686
		// (get) Token: 0x06000F8E RID: 3982 RVA: 0x0000EF1B File Offset: 0x0000D11B
		public override bool CanCreateDataSourceEnumerator
		{
			get
			{
				return true;
			}
		}

		/// <summary>Returns a new <see cref="T:System.Data.Sql.SqlDataSourceEnumerator" />.</summary>
		/// <returns>A new data source enumerator.</returns>
		// Token: 0x06000F8F RID: 3983 RVA: 0x0005173E File Offset: 0x0004F93E
		public override DbDataSourceEnumerator CreateDataSourceEnumerator()
		{
			return SqlDataSourceEnumerator.Instance;
		}

		/// <summary>Returns a new <see cref="T:System.Security.CodeAccessPermission" />.</summary>
		/// <param name="state">A member of the <see cref="T:System.Security.Permissions.PermissionState" /> enumeration.</param>
		/// <returns>A strongly typed instance of <see cref="T:System.Security.CodeAccessPermission" />.</returns>
		// Token: 0x06000F90 RID: 3984 RVA: 0x00051745 File Offset: 0x0004F945
		public override CodeAccessPermission CreatePermission(PermissionState state)
		{
			return new SqlClientPermission(state);
		}

		// Token: 0x06000F91 RID: 3985 RVA: 0x0005174D File Offset: 0x0004F94D
		// Note: this type is marked as 'beforefieldinit'.
		static SqlClientFactory()
		{
		}

		/// <summary>For a description of this member, see <see cref="M:System.IServiceProvider.GetService(System.Type)" />.</summary>
		/// <param name="serviceType">An object that specifies the type of service object to get. </param>
		/// <returns>A service object.</returns>
		// Token: 0x06000F92 RID: 3986 RVA: 0x00051759 File Offset: 0x0004F959
		object IServiceProvider.GetService(Type serviceType)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Gets an instance of the <see cref="T:System.Data.SqlClient.SqlClientFactory" />. This can be used to retrieve strongly typed data objects.</summary>
		// Token: 0x04000B12 RID: 2834
		public static readonly SqlClientFactory Instance = new SqlClientFactory();
	}
}
