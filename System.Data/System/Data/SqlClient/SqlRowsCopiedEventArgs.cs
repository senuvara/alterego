﻿using System;

namespace System.Data.SqlClient
{
	/// <summary>Represents the set of arguments passed to the <see cref="T:System.Data.SqlClient.SqlRowsCopiedEventHandler" />.</summary>
	// Token: 0x02000125 RID: 293
	public class SqlRowsCopiedEventArgs : EventArgs
	{
		/// <summary>Creates a new instance of the <see cref="T:System.Data.SqlClient.SqlRowsCopiedEventArgs" /> object.</summary>
		/// <param name="rowsCopied">An <see cref="T:System.Int64" /> that indicates the number of rows copied during the current bulk copy operation. </param>
		// Token: 0x06000E75 RID: 3701 RVA: 0x0004BEF0 File Offset: 0x0004A0F0
		public SqlRowsCopiedEventArgs(long rowsCopied)
		{
			this._rowsCopied = rowsCopied;
		}

		/// <summary>Gets or sets a value that indicates whether the bulk copy operation should be aborted.</summary>
		/// <returns>
		///     <see langword="true" /> if the bulk copy operation should be aborted; otherwise <see langword="false" />.</returns>
		// Token: 0x17000273 RID: 627
		// (get) Token: 0x06000E76 RID: 3702 RVA: 0x0004BEFF File Offset: 0x0004A0FF
		// (set) Token: 0x06000E77 RID: 3703 RVA: 0x0004BF07 File Offset: 0x0004A107
		public bool Abort
		{
			get
			{
				return this._abort;
			}
			set
			{
				this._abort = value;
			}
		}

		/// <summary>Gets a value that returns the number of rows copied during the current bulk copy operation.</summary>
		/// <returns>
		///     <see langword="int" /> that returns the number of rows copied.</returns>
		// Token: 0x17000274 RID: 628
		// (get) Token: 0x06000E78 RID: 3704 RVA: 0x0004BF10 File Offset: 0x0004A110
		public long RowsCopied
		{
			get
			{
				return this._rowsCopied;
			}
		}

		// Token: 0x04000A37 RID: 2615
		private bool _abort;

		// Token: 0x04000A38 RID: 2616
		private long _rowsCopied;
	}
}
