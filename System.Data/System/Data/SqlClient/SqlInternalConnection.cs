﻿using System;
using System.Data.Common;
using System.Data.ProviderBase;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Transactions;

namespace System.Data.SqlClient
{
	// Token: 0x020001A6 RID: 422
	internal abstract class SqlInternalConnection : DbConnectionInternal
	{
		// Token: 0x17000398 RID: 920
		// (get) Token: 0x0600134F RID: 4943 RVA: 0x00064C01 File Offset: 0x00062E01
		// (set) Token: 0x06001350 RID: 4944 RVA: 0x00064C09 File Offset: 0x00062E09
		internal string CurrentDatabase
		{
			[CompilerGenerated]
			get
			{
				return this.<CurrentDatabase>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<CurrentDatabase>k__BackingField = value;
			}
		}

		// Token: 0x17000399 RID: 921
		// (get) Token: 0x06001351 RID: 4945 RVA: 0x00064C12 File Offset: 0x00062E12
		// (set) Token: 0x06001352 RID: 4946 RVA: 0x00064C1A File Offset: 0x00062E1A
		internal string CurrentDataSource
		{
			[CompilerGenerated]
			get
			{
				return this.<CurrentDataSource>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<CurrentDataSource>k__BackingField = value;
			}
		}

		// Token: 0x1700039A RID: 922
		// (get) Token: 0x06001353 RID: 4947 RVA: 0x00064C23 File Offset: 0x00062E23
		// (set) Token: 0x06001354 RID: 4948 RVA: 0x00064C2B File Offset: 0x00062E2B
		internal SqlDelegatedTransaction DelegatedTransaction
		{
			[CompilerGenerated]
			get
			{
				return this.<DelegatedTransaction>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<DelegatedTransaction>k__BackingField = value;
			}
		}

		// Token: 0x06001355 RID: 4949 RVA: 0x00064C34 File Offset: 0x00062E34
		internal SqlInternalConnection(SqlConnectionString connectionOptions)
		{
			this._connectionOptions = connectionOptions;
		}

		// Token: 0x1700039B RID: 923
		// (get) Token: 0x06001356 RID: 4950 RVA: 0x00064C43 File Offset: 0x00062E43
		internal SqlConnection Connection
		{
			get
			{
				return (SqlConnection)base.Owner;
			}
		}

		// Token: 0x1700039C RID: 924
		// (get) Token: 0x06001357 RID: 4951 RVA: 0x00064C50 File Offset: 0x00062E50
		internal SqlConnectionString ConnectionOptions
		{
			get
			{
				return this._connectionOptions;
			}
		}

		// Token: 0x1700039D RID: 925
		// (get) Token: 0x06001358 RID: 4952
		internal abstract SqlInternalTransaction CurrentTransaction { get; }

		// Token: 0x1700039E RID: 926
		// (get) Token: 0x06001359 RID: 4953 RVA: 0x00064C58 File Offset: 0x00062E58
		internal virtual SqlInternalTransaction AvailableInternalTransaction
		{
			get
			{
				return this.CurrentTransaction;
			}
		}

		// Token: 0x1700039F RID: 927
		// (get) Token: 0x0600135A RID: 4954
		internal abstract SqlInternalTransaction PendingTransaction { get; }

		// Token: 0x170003A0 RID: 928
		// (get) Token: 0x0600135B RID: 4955 RVA: 0x00064C60 File Offset: 0x00062E60
		protected internal override bool IsNonPoolableTransactionRoot
		{
			get
			{
				return this.IsTransactionRoot;
			}
		}

		// Token: 0x170003A1 RID: 929
		// (get) Token: 0x0600135C RID: 4956 RVA: 0x00064C68 File Offset: 0x00062E68
		internal override bool IsTransactionRoot
		{
			get
			{
				SqlDelegatedTransaction delegatedTransaction = this.DelegatedTransaction;
				return delegatedTransaction != null && delegatedTransaction.IsActive;
			}
		}

		// Token: 0x170003A2 RID: 930
		// (get) Token: 0x0600135D RID: 4957 RVA: 0x00064C88 File Offset: 0x00062E88
		internal bool HasLocalTransaction
		{
			get
			{
				SqlInternalTransaction currentTransaction = this.CurrentTransaction;
				return currentTransaction != null && currentTransaction.IsLocal;
			}
		}

		// Token: 0x170003A3 RID: 931
		// (get) Token: 0x0600135E RID: 4958 RVA: 0x00064CA8 File Offset: 0x00062EA8
		internal bool HasLocalTransactionFromAPI
		{
			get
			{
				SqlInternalTransaction currentTransaction = this.CurrentTransaction;
				return currentTransaction != null && currentTransaction.HasParentTransaction;
			}
		}

		// Token: 0x170003A4 RID: 932
		// (get) Token: 0x0600135F RID: 4959 RVA: 0x00064CC7 File Offset: 0x00062EC7
		internal bool IsEnlistedInTransaction
		{
			get
			{
				return this._isEnlistedInTransaction;
			}
		}

		// Token: 0x170003A5 RID: 933
		// (get) Token: 0x06001360 RID: 4960
		internal abstract bool IsLockedForBulkCopy { get; }

		// Token: 0x170003A6 RID: 934
		// (get) Token: 0x06001361 RID: 4961
		internal abstract bool IsKatmaiOrNewer { get; }

		// Token: 0x170003A7 RID: 935
		// (get) Token: 0x06001362 RID: 4962 RVA: 0x00064CCF File Offset: 0x00062ECF
		// (set) Token: 0x06001363 RID: 4963 RVA: 0x00064CD7 File Offset: 0x00062ED7
		internal byte[] PromotedDTCToken
		{
			get
			{
				return this._promotedDTCToken;
			}
			set
			{
				this._promotedDTCToken = value;
			}
		}

		// Token: 0x170003A8 RID: 936
		// (get) Token: 0x06001364 RID: 4964 RVA: 0x00064CE0 File Offset: 0x00062EE0
		// (set) Token: 0x06001365 RID: 4965 RVA: 0x00064CE8 File Offset: 0x00062EE8
		internal bool IsGlobalTransaction
		{
			get
			{
				return this._isGlobalTransaction;
			}
			set
			{
				this._isGlobalTransaction = value;
			}
		}

		// Token: 0x170003A9 RID: 937
		// (get) Token: 0x06001366 RID: 4966 RVA: 0x00064CF1 File Offset: 0x00062EF1
		// (set) Token: 0x06001367 RID: 4967 RVA: 0x00064CF9 File Offset: 0x00062EF9
		internal bool IsGlobalTransactionsEnabledForServer
		{
			get
			{
				return this._isGlobalTransactionEnabledForServer;
			}
			set
			{
				this._isGlobalTransactionEnabledForServer = value;
			}
		}

		// Token: 0x06001368 RID: 4968 RVA: 0x00064D02 File Offset: 0x00062F02
		public override DbTransaction BeginTransaction(IsolationLevel iso)
		{
			return this.BeginSqlTransaction(iso, null, false);
		}

		// Token: 0x06001369 RID: 4969 RVA: 0x00064D10 File Offset: 0x00062F10
		internal virtual SqlTransaction BeginSqlTransaction(IsolationLevel iso, string transactionName, bool shouldReconnect)
		{
			SqlStatistics statistics = null;
			SqlTransaction result;
			try
			{
				statistics = SqlStatistics.StartTimer(this.Connection.Statistics);
				this.ValidateConnectionForExecute(null);
				if (this.HasLocalTransactionFromAPI)
				{
					throw ADP.ParallelTransactionsNotSupported(this.Connection);
				}
				if (iso == IsolationLevel.Unspecified)
				{
					iso = IsolationLevel.ReadCommitted;
				}
				SqlTransaction sqlTransaction = new SqlTransaction(this, this.Connection, iso, this.AvailableInternalTransaction);
				sqlTransaction.InternalTransaction.RestoreBrokenConnection = shouldReconnect;
				this.ExecuteTransaction(SqlInternalConnection.TransactionRequest.Begin, transactionName, iso, sqlTransaction.InternalTransaction, false);
				sqlTransaction.InternalTransaction.RestoreBrokenConnection = false;
				result = sqlTransaction;
			}
			finally
			{
				SqlStatistics.StopTimer(statistics);
			}
			return result;
		}

		// Token: 0x0600136A RID: 4970 RVA: 0x00064DB0 File Offset: 0x00062FB0
		public override void ChangeDatabase(string database)
		{
			if (string.IsNullOrEmpty(database))
			{
				throw ADP.EmptyDatabaseName();
			}
			this.ValidateConnectionForExecute(null);
			this.ChangeDatabaseInternal(database);
		}

		// Token: 0x0600136B RID: 4971
		protected abstract void ChangeDatabaseInternal(string database);

		// Token: 0x0600136C RID: 4972 RVA: 0x00064DD0 File Offset: 0x00062FD0
		protected override void CleanupTransactionOnCompletion(Transaction transaction)
		{
			SqlDelegatedTransaction delegatedTransaction = this.DelegatedTransaction;
			if (delegatedTransaction != null)
			{
				delegatedTransaction.TransactionEnded(transaction);
			}
		}

		// Token: 0x0600136D RID: 4973 RVA: 0x00064DEE File Offset: 0x00062FEE
		protected override DbReferenceCollection CreateReferenceCollection()
		{
			return new SqlReferenceCollection();
		}

		// Token: 0x0600136E RID: 4974 RVA: 0x00064DF8 File Offset: 0x00062FF8
		protected override void Deactivate()
		{
			try
			{
				SqlReferenceCollection sqlReferenceCollection = (SqlReferenceCollection)base.ReferenceCollection;
				if (sqlReferenceCollection != null)
				{
					sqlReferenceCollection.Deactivate();
				}
				this.InternalDeactivate();
			}
			catch (Exception e)
			{
				if (!ADP.IsCatchableExceptionType(e))
				{
					throw;
				}
				base.DoomThisConnection();
			}
		}

		// Token: 0x0600136F RID: 4975
		internal abstract void DisconnectTransaction(SqlInternalTransaction internalTransaction);

		// Token: 0x06001370 RID: 4976 RVA: 0x00064E44 File Offset: 0x00063044
		public override void Dispose()
		{
			this._whereAbouts = null;
			base.Dispose();
		}

		// Token: 0x06001371 RID: 4977 RVA: 0x00064E54 File Offset: 0x00063054
		protected void Enlist(Transaction tx)
		{
			if (null == tx)
			{
				if (this.IsEnlistedInTransaction)
				{
					this.EnlistNull();
					return;
				}
				Transaction enlistedTransaction = base.EnlistedTransaction;
				if (enlistedTransaction != null && enlistedTransaction.TransactionInformation.Status != TransactionStatus.Active)
				{
					this.EnlistNull();
					return;
				}
			}
			else if (!tx.Equals(base.EnlistedTransaction))
			{
				this.EnlistNonNull(tx);
			}
		}

		// Token: 0x06001372 RID: 4978 RVA: 0x00064EB4 File Offset: 0x000630B4
		private void EnlistNonNull(Transaction tx)
		{
			bool flag = false;
			SqlDelegatedTransaction sqlDelegatedTransaction = new SqlDelegatedTransaction(this, tx);
			try
			{
				if (this._isGlobalTransaction)
				{
					if (SysTxForGlobalTransactions.EnlistPromotableSinglePhase == null)
					{
						flag = tx.EnlistPromotableSinglePhase(sqlDelegatedTransaction);
					}
					else
					{
						flag = (bool)SysTxForGlobalTransactions.EnlistPromotableSinglePhase.Invoke(tx, new object[]
						{
							sqlDelegatedTransaction,
							SqlInternalConnection._globalTransactionTMID
						});
					}
				}
				else
				{
					flag = tx.EnlistPromotableSinglePhase(sqlDelegatedTransaction);
				}
				if (flag)
				{
					this.DelegatedTransaction = sqlDelegatedTransaction;
				}
			}
			catch (SqlException ex)
			{
				if (ex.Class >= 20)
				{
					throw;
				}
				SqlInternalConnectionTds sqlInternalConnectionTds = this as SqlInternalConnectionTds;
				if (sqlInternalConnectionTds != null)
				{
					TdsParser parser = sqlInternalConnectionTds.Parser;
					if (parser == null || parser.State != TdsParserState.OpenLoggedIn)
					{
						throw;
					}
				}
			}
			if (!flag)
			{
				byte[] transactionCookie;
				if (this._isGlobalTransaction)
				{
					if (SysTxForGlobalTransactions.GetPromotedToken == null)
					{
						throw SQL.UnsupportedSysTxForGlobalTransactions();
					}
					transactionCookie = (byte[])SysTxForGlobalTransactions.GetPromotedToken.Invoke(tx, null);
				}
				else
				{
					if (this._whereAbouts == null)
					{
						byte[] dtcaddress = this.GetDTCAddress();
						if (dtcaddress == null)
						{
							throw SQL.CannotGetDTCAddress();
						}
						this._whereAbouts = dtcaddress;
					}
					transactionCookie = SqlInternalConnection.GetTransactionCookie(tx, this._whereAbouts);
				}
				this.PropagateTransactionCookie(transactionCookie);
				this._isEnlistedInTransaction = true;
			}
			base.EnlistedTransaction = tx;
		}

		// Token: 0x06001373 RID: 4979 RVA: 0x00064FE0 File Offset: 0x000631E0
		internal void EnlistNull()
		{
			this.PropagateTransactionCookie(null);
			this._isEnlistedInTransaction = false;
			base.EnlistedTransaction = null;
		}

		// Token: 0x06001374 RID: 4980 RVA: 0x00064FF8 File Offset: 0x000631F8
		public override void EnlistTransaction(Transaction transaction)
		{
			this.ValidateConnectionForExecute(null);
			if (this.HasLocalTransaction)
			{
				throw ADP.LocalTransactionPresent();
			}
			if (null != transaction && transaction.Equals(base.EnlistedTransaction))
			{
				return;
			}
			try
			{
				this.Enlist(transaction);
			}
			catch (OutOfMemoryException e)
			{
				this.Connection.Abort(e);
				throw;
			}
			catch (StackOverflowException e2)
			{
				this.Connection.Abort(e2);
				throw;
			}
			catch (ThreadAbortException e3)
			{
				this.Connection.Abort(e3);
				throw;
			}
		}

		// Token: 0x06001375 RID: 4981
		internal abstract void ExecuteTransaction(SqlInternalConnection.TransactionRequest transactionRequest, string name, IsolationLevel iso, SqlInternalTransaction internalTransaction, bool isDelegateControlRequest);

		// Token: 0x06001376 RID: 4982 RVA: 0x00065094 File Offset: 0x00063294
		internal SqlDataReader FindLiveReader(SqlCommand command)
		{
			SqlDataReader result = null;
			SqlReferenceCollection sqlReferenceCollection = (SqlReferenceCollection)base.ReferenceCollection;
			if (sqlReferenceCollection != null)
			{
				result = sqlReferenceCollection.FindLiveReader(command);
			}
			return result;
		}

		// Token: 0x06001377 RID: 4983 RVA: 0x000650BC File Offset: 0x000632BC
		internal SqlCommand FindLiveCommand(TdsParserStateObject stateObj)
		{
			SqlCommand result = null;
			SqlReferenceCollection sqlReferenceCollection = (SqlReferenceCollection)base.ReferenceCollection;
			if (sqlReferenceCollection != null)
			{
				result = sqlReferenceCollection.FindLiveCommand(stateObj);
			}
			return result;
		}

		// Token: 0x06001378 RID: 4984
		protected abstract byte[] GetDTCAddress();

		// Token: 0x06001379 RID: 4985 RVA: 0x000650E4 File Offset: 0x000632E4
		private static byte[] GetTransactionCookie(Transaction transaction, byte[] whereAbouts)
		{
			byte[] result = null;
			if (null != transaction)
			{
				result = TransactionInterop.GetExportCookie(transaction, whereAbouts);
			}
			return result;
		}

		// Token: 0x0600137A RID: 4986 RVA: 0x00005E03 File Offset: 0x00004003
		protected virtual void InternalDeactivate()
		{
		}

		// Token: 0x0600137B RID: 4987 RVA: 0x00065108 File Offset: 0x00063308
		internal void OnError(SqlException exception, bool breakConnection, Action<Action> wrapCloseInAction = null)
		{
			if (breakConnection)
			{
				base.DoomThisConnection();
			}
			SqlConnection connection = this.Connection;
			if (connection != null)
			{
				connection.OnError(exception, breakConnection, wrapCloseInAction);
				return;
			}
			if (exception.Class >= 11)
			{
				throw exception;
			}
		}

		// Token: 0x0600137C RID: 4988
		protected abstract void PropagateTransactionCookie(byte[] transactionCookie);

		// Token: 0x0600137D RID: 4989
		internal abstract void ValidateConnectionForExecute(SqlCommand command);

		// Token: 0x0600137E RID: 4990 RVA: 0x0006513E File Offset: 0x0006333E
		// Note: this type is marked as 'beforefieldinit'.
		static SqlInternalConnection()
		{
		}

		// Token: 0x04000DCC RID: 3532
		private readonly SqlConnectionString _connectionOptions;

		// Token: 0x04000DCD RID: 3533
		private bool _isEnlistedInTransaction;

		// Token: 0x04000DCE RID: 3534
		private byte[] _promotedDTCToken;

		// Token: 0x04000DCF RID: 3535
		private byte[] _whereAbouts;

		// Token: 0x04000DD0 RID: 3536
		private bool _isGlobalTransaction;

		// Token: 0x04000DD1 RID: 3537
		private bool _isGlobalTransactionEnabledForServer;

		// Token: 0x04000DD2 RID: 3538
		private static readonly Guid _globalTransactionTMID = new Guid("1c742caf-6680-40ea-9c26-6b6846079764");

		// Token: 0x04000DD3 RID: 3539
		[CompilerGenerated]
		private string <CurrentDatabase>k__BackingField;

		// Token: 0x04000DD4 RID: 3540
		[CompilerGenerated]
		private string <CurrentDataSource>k__BackingField;

		// Token: 0x04000DD5 RID: 3541
		[CompilerGenerated]
		private SqlDelegatedTransaction <DelegatedTransaction>k__BackingField;

		// Token: 0x020001A7 RID: 423
		internal enum TransactionRequest
		{
			// Token: 0x04000DD7 RID: 3543
			Begin,
			// Token: 0x04000DD8 RID: 3544
			Promote,
			// Token: 0x04000DD9 RID: 3545
			Commit,
			// Token: 0x04000DDA RID: 3546
			Rollback,
			// Token: 0x04000DDB RID: 3547
			IfRollback,
			// Token: 0x04000DDC RID: 3548
			Save
		}
	}
}
