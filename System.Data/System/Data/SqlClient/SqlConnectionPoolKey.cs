﻿using System;
using System.Data.Common;

namespace System.Data.SqlClient
{
	// Token: 0x02000172 RID: 370
	internal class SqlConnectionPoolKey : DbConnectionPoolKey
	{
		// Token: 0x06001123 RID: 4387 RVA: 0x000585E8 File Offset: 0x000567E8
		internal SqlConnectionPoolKey(string connectionString) : base(connectionString)
		{
			this.CalculateHashCode();
		}

		// Token: 0x06001124 RID: 4388 RVA: 0x000585F7 File Offset: 0x000567F7
		private SqlConnectionPoolKey(SqlConnectionPoolKey key) : base(key)
		{
			this.CalculateHashCode();
		}

		// Token: 0x06001125 RID: 4389 RVA: 0x00058606 File Offset: 0x00056806
		public override object Clone()
		{
			return new SqlConnectionPoolKey(this);
		}

		// Token: 0x170002FF RID: 767
		// (get) Token: 0x06001126 RID: 4390 RVA: 0x0005860E File Offset: 0x0005680E
		// (set) Token: 0x06001127 RID: 4391 RVA: 0x00058616 File Offset: 0x00056816
		internal override string ConnectionString
		{
			get
			{
				return base.ConnectionString;
			}
			set
			{
				base.ConnectionString = value;
				this.CalculateHashCode();
			}
		}

		// Token: 0x06001128 RID: 4392 RVA: 0x00058628 File Offset: 0x00056828
		public override bool Equals(object obj)
		{
			SqlConnectionPoolKey sqlConnectionPoolKey = obj as SqlConnectionPoolKey;
			return sqlConnectionPoolKey != null && this.ConnectionString == sqlConnectionPoolKey.ConnectionString;
		}

		// Token: 0x06001129 RID: 4393 RVA: 0x00058652 File Offset: 0x00056852
		public override int GetHashCode()
		{
			return this._hashValue;
		}

		// Token: 0x0600112A RID: 4394 RVA: 0x0005865A File Offset: 0x0005685A
		private void CalculateHashCode()
		{
			this._hashValue = base.GetHashCode();
		}

		// Token: 0x04000BDA RID: 3034
		private int _hashValue;
	}
}
