﻿using System;

namespace System.Data.SqlClient
{
	// Token: 0x0200019D RID: 413
	internal class SqlNotification : MarshalByRefObject
	{
		// Token: 0x060012F8 RID: 4856 RVA: 0x000626D3 File Offset: 0x000608D3
		internal SqlNotification(SqlNotificationInfo info, SqlNotificationSource source, SqlNotificationType type, string key)
		{
			this._info = info;
			this._source = source;
			this._type = type;
			this._key = key;
		}

		// Token: 0x17000378 RID: 888
		// (get) Token: 0x060012F9 RID: 4857 RVA: 0x000626F8 File Offset: 0x000608F8
		internal SqlNotificationInfo Info
		{
			get
			{
				return this._info;
			}
		}

		// Token: 0x17000379 RID: 889
		// (get) Token: 0x060012FA RID: 4858 RVA: 0x00062700 File Offset: 0x00060900
		internal string Key
		{
			get
			{
				return this._key;
			}
		}

		// Token: 0x1700037A RID: 890
		// (get) Token: 0x060012FB RID: 4859 RVA: 0x00062708 File Offset: 0x00060908
		internal SqlNotificationSource Source
		{
			get
			{
				return this._source;
			}
		}

		// Token: 0x1700037B RID: 891
		// (get) Token: 0x060012FC RID: 4860 RVA: 0x00062710 File Offset: 0x00060910
		internal SqlNotificationType Type
		{
			get
			{
				return this._type;
			}
		}

		// Token: 0x04000D56 RID: 3414
		private readonly SqlNotificationInfo _info;

		// Token: 0x04000D57 RID: 3415
		private readonly SqlNotificationSource _source;

		// Token: 0x04000D58 RID: 3416
		private readonly SqlNotificationType _type;

		// Token: 0x04000D59 RID: 3417
		private readonly string _key;
	}
}
