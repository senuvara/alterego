﻿using System;
using System.Collections.Generic;

namespace System.Data.SqlClient
{
	// Token: 0x02000133 RID: 307
	internal sealed class BulkCopySimpleResultSet
	{
		// Token: 0x06000ECE RID: 3790 RVA: 0x0004D511 File Offset: 0x0004B711
		internal BulkCopySimpleResultSet()
		{
			this._results = new List<Result>();
		}

		// Token: 0x1700029E RID: 670
		internal Result this[int idx]
		{
			get
			{
				return this._results[idx];
			}
		}

		// Token: 0x06000ED0 RID: 3792 RVA: 0x0004D534 File Offset: 0x0004B734
		internal void SetMetaData(_SqlMetaDataSet metadata)
		{
			this._resultSet = new Result(metadata);
			this._results.Add(this._resultSet);
			this._indexmap = new int[this._resultSet.MetaData.Length];
			for (int i = 0; i < this._indexmap.Length; i++)
			{
				this._indexmap[i] = i;
			}
		}

		// Token: 0x06000ED1 RID: 3793 RVA: 0x0004D595 File Offset: 0x0004B795
		internal int[] CreateIndexMap()
		{
			return this._indexmap;
		}

		// Token: 0x06000ED2 RID: 3794 RVA: 0x0004D5A0 File Offset: 0x0004B7A0
		internal object[] CreateRowBuffer()
		{
			Row row = new Row(this._resultSet.MetaData.Length);
			this._resultSet.AddRow(row);
			return row.DataFields;
		}

		// Token: 0x04000A79 RID: 2681
		private readonly List<Result> _results;

		// Token: 0x04000A7A RID: 2682
		private Result _resultSet;

		// Token: 0x04000A7B RID: 2683
		private int[] _indexmap;
	}
}
