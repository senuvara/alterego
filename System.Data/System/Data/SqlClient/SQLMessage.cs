﻿using System;

namespace System.Data.SqlClient
{
	// Token: 0x020001D7 RID: 471
	internal sealed class SQLMessage
	{
		// Token: 0x060015AE RID: 5550 RVA: 0x00005C14 File Offset: 0x00003E14
		private SQLMessage()
		{
		}

		// Token: 0x060015AF RID: 5551 RVA: 0x0006CA5B File Offset: 0x0006AC5B
		internal static string CultureIdError()
		{
			return SR.GetString("The Collation specified by SQL Server is not supported.");
		}

		// Token: 0x060015B0 RID: 5552 RVA: 0x0006CA67 File Offset: 0x0006AC67
		internal static string EncryptionNotSupportedByClient()
		{
			return SR.GetString("The instance of SQL Server you attempted to connect to requires encryption but this machine does not support it.");
		}

		// Token: 0x060015B1 RID: 5553 RVA: 0x0006CA73 File Offset: 0x0006AC73
		internal static string EncryptionNotSupportedByServer()
		{
			return SR.GetString("The instance of SQL Server you attempted to connect to does not support encryption.");
		}

		// Token: 0x060015B2 RID: 5554 RVA: 0x0006CA7F File Offset: 0x0006AC7F
		internal static string OperationCancelled()
		{
			return SR.GetString("Operation cancelled by user.");
		}

		// Token: 0x060015B3 RID: 5555 RVA: 0x0006CA8B File Offset: 0x0006AC8B
		internal static string SevereError()
		{
			return SR.GetString("A severe error occurred on the current command.  The results, if any, should be discarded.");
		}

		// Token: 0x060015B4 RID: 5556 RVA: 0x0006CA97 File Offset: 0x0006AC97
		internal static string SSPIInitializeError()
		{
			return SR.GetString("Cannot initialize SSPI package.");
		}

		// Token: 0x060015B5 RID: 5557 RVA: 0x0006CAA3 File Offset: 0x0006ACA3
		internal static string SSPIGenerateError()
		{
			return SR.GetString("Failed to generate SSPI context.");
		}

		// Token: 0x060015B6 RID: 5558 RVA: 0x0006CAAF File Offset: 0x0006ACAF
		internal static string SqlServerBrowserNotAccessible()
		{
			return SR.GetString("Cannot connect to SQL Server Browser. Ensure SQL Server Browser has been started.");
		}

		// Token: 0x060015B7 RID: 5559 RVA: 0x0006CABB File Offset: 0x0006ACBB
		internal static string KerberosTicketMissingError()
		{
			return SR.GetString("Cannot access Kerberos ticket. Ensure Kerberos has been initialized with 'kinit'.");
		}

		// Token: 0x060015B8 RID: 5560 RVA: 0x0006CAC7 File Offset: 0x0006ACC7
		internal static string Timeout()
		{
			return SR.GetString("Timeout expired.  The timeout period elapsed prior to completion of the operation or the server is not responding.");
		}

		// Token: 0x060015B9 RID: 5561 RVA: 0x0006CAD3 File Offset: 0x0006ACD3
		internal static string Timeout_PreLogin_Begin()
		{
			return SR.GetString("Connection Timeout Expired.  The timeout period elapsed at the start of the pre-login phase.  This could be because of insufficient time provided for connection timeout.");
		}

		// Token: 0x060015BA RID: 5562 RVA: 0x0006CADF File Offset: 0x0006ACDF
		internal static string Timeout_PreLogin_InitializeConnection()
		{
			return SR.GetString("Connection Timeout Expired.  The timeout period elapsed while attempting to create and initialize a socket to the server.  This could be either because the server was unreachable or unable to respond back in time.");
		}

		// Token: 0x060015BB RID: 5563 RVA: 0x0006CAEB File Offset: 0x0006ACEB
		internal static string Timeout_PreLogin_SendHandshake()
		{
			return SR.GetString("Connection Timeout Expired.  The timeout period elapsed while making a pre-login handshake request.  This could be because the server was unable to respond back in time.");
		}

		// Token: 0x060015BC RID: 5564 RVA: 0x0006CAF7 File Offset: 0x0006ACF7
		internal static string Timeout_PreLogin_ConsumeHandshake()
		{
			return SR.GetString("Connection Timeout Expired.  The timeout period elapsed while attempting to consume the pre-login handshake acknowledgement.  This could be because the pre-login handshake failed or the server was unable to respond back in time.");
		}

		// Token: 0x060015BD RID: 5565 RVA: 0x0006CB03 File Offset: 0x0006AD03
		internal static string Timeout_Login_Begin()
		{
			return SR.GetString("Connection Timeout Expired.  The timeout period elapsed at the start of the login phase.  This could be because of insufficient time provided for connection timeout.");
		}

		// Token: 0x060015BE RID: 5566 RVA: 0x0006CB0F File Offset: 0x0006AD0F
		internal static string Timeout_Login_ProcessConnectionAuth()
		{
			return SR.GetString("Connection Timeout Expired.  The timeout period elapsed while attempting to authenticate the login.  This could be because the server failed to authenticate the user or the server was unable to respond back in time.");
		}

		// Token: 0x060015BF RID: 5567 RVA: 0x0006CB1B File Offset: 0x0006AD1B
		internal static string Timeout_PostLogin()
		{
			return SR.GetString("Connection Timeout Expired.  The timeout period elapsed during the post-login phase.  The connection could have timed out while waiting for server to complete the login process and respond; Or it could have timed out while attempting to create multiple active connections.");
		}

		// Token: 0x060015C0 RID: 5568 RVA: 0x0006CB27 File Offset: 0x0006AD27
		internal static string Timeout_FailoverInfo()
		{
			return SR.GetString("This failure occurred while attempting to connect to the {0} server.");
		}

		// Token: 0x060015C1 RID: 5569 RVA: 0x0006CB33 File Offset: 0x0006AD33
		internal static string Timeout_RoutingDestination()
		{
			return SR.GetString("This failure occurred while attempting to connect to the routing destination. The duration spent while attempting to connect to the original server was - [Pre-Login] initialization={0}; handshake={1}; [Login] initialization={2}; authentication={3}; [Post-Login] complete={4};  ");
		}

		// Token: 0x060015C2 RID: 5570 RVA: 0x0006CB3F File Offset: 0x0006AD3F
		internal static string Duration_PreLogin_Begin(long PreLoginBeginDuration)
		{
			return SR.GetString("The duration spent while attempting to connect to this server was - [Pre-Login] initialization={0};", new object[]
			{
				PreLoginBeginDuration
			});
		}

		// Token: 0x060015C3 RID: 5571 RVA: 0x0006CB5A File Offset: 0x0006AD5A
		internal static string Duration_PreLoginHandshake(long PreLoginBeginDuration, long PreLoginHandshakeDuration)
		{
			return SR.GetString("The duration spent while attempting to connect to this server was - [Pre-Login] initialization={0}; handshake={1}; ", new object[]
			{
				PreLoginBeginDuration,
				PreLoginHandshakeDuration
			});
		}

		// Token: 0x060015C4 RID: 5572 RVA: 0x0006CB7E File Offset: 0x0006AD7E
		internal static string Duration_Login_Begin(long PreLoginBeginDuration, long PreLoginHandshakeDuration, long LoginBeginDuration)
		{
			return SR.GetString("The duration spent while attempting to connect to this server was - [Pre-Login] initialization={0}; handshake={1}; [Login] initialization={2}; ", new object[]
			{
				PreLoginBeginDuration,
				PreLoginHandshakeDuration,
				LoginBeginDuration
			});
		}

		// Token: 0x060015C5 RID: 5573 RVA: 0x0006CBAB File Offset: 0x0006ADAB
		internal static string Duration_Login_ProcessConnectionAuth(long PreLoginBeginDuration, long PreLoginHandshakeDuration, long LoginBeginDuration, long LoginAuthDuration)
		{
			return SR.GetString("The duration spent while attempting to connect to this server was - [Pre-Login] initialization={0}; handshake={1}; [Login] initialization={2}; authentication={3}; ", new object[]
			{
				PreLoginBeginDuration,
				PreLoginHandshakeDuration,
				LoginBeginDuration,
				LoginAuthDuration
			});
		}

		// Token: 0x060015C6 RID: 5574 RVA: 0x0006CBE1 File Offset: 0x0006ADE1
		internal static string Duration_PostLogin(long PreLoginBeginDuration, long PreLoginHandshakeDuration, long LoginBeginDuration, long LoginAuthDuration, long PostLoginDuration)
		{
			return SR.GetString("The duration spent while attempting to connect to this server was - [Pre-Login] initialization={0}; handshake={1}; [Login] initialization={2}; authentication={3}; [Post-Login] complete={4}; ", new object[]
			{
				PreLoginBeginDuration,
				PreLoginHandshakeDuration,
				LoginBeginDuration,
				LoginAuthDuration,
				PostLoginDuration
			});
		}

		// Token: 0x060015C7 RID: 5575 RVA: 0x0006CC21 File Offset: 0x0006AE21
		internal static string UserInstanceFailure()
		{
			return SR.GetString("A user instance was requested in the connection string but the server specified does not support this option.");
		}

		// Token: 0x060015C8 RID: 5576 RVA: 0x0006CC2D File Offset: 0x0006AE2D
		internal static string PreloginError()
		{
			return SR.GetString("A connection was successfully established with the server, but then an error occurred during the pre-login handshake.");
		}

		// Token: 0x060015C9 RID: 5577 RVA: 0x0006CC39 File Offset: 0x0006AE39
		internal static string ExClientConnectionId()
		{
			return SR.GetString("ClientConnectionId:{0}");
		}

		// Token: 0x060015CA RID: 5578 RVA: 0x0006CC45 File Offset: 0x0006AE45
		internal static string ExErrorNumberStateClass()
		{
			return SR.GetString("Error Number:{0},State:{1},Class:{2}");
		}

		// Token: 0x060015CB RID: 5579 RVA: 0x0006CC51 File Offset: 0x0006AE51
		internal static string ExOriginalClientConnectionId()
		{
			return SR.GetString("ClientConnectionId before routing:{0}");
		}

		// Token: 0x060015CC RID: 5580 RVA: 0x0006CC5D File Offset: 0x0006AE5D
		internal static string ExRoutingDestination()
		{
			return SR.GetString("Routing Destination:{0}");
		}
	}
}
