﻿using System;
using System.Collections.Generic;
using System.Data.Common;

namespace System.Data.SqlClient
{
	// Token: 0x02000207 RID: 519
	internal class TdsParserSessionPool
	{
		// Token: 0x060016F6 RID: 5878 RVA: 0x0007831A File Offset: 0x0007651A
		internal TdsParserSessionPool(TdsParser parser)
		{
			this._parser = parser;
			this._cache = new List<TdsParserStateObject>();
			this._freeStateObjects = new TdsParserStateObject[10];
			this._freeStateObjectCount = 0;
		}

		// Token: 0x17000441 RID: 1089
		// (get) Token: 0x060016F7 RID: 5879 RVA: 0x00078348 File Offset: 0x00076548
		private bool IsDisposed
		{
			get
			{
				return this._freeStateObjects == null;
			}
		}

		// Token: 0x060016F8 RID: 5880 RVA: 0x00078354 File Offset: 0x00076554
		internal void Deactivate()
		{
			List<TdsParserStateObject> cache = this._cache;
			lock (cache)
			{
				for (int i = this._cache.Count - 1; i >= 0; i--)
				{
					TdsParserStateObject tdsParserStateObject = this._cache[i];
					if (tdsParserStateObject != null && tdsParserStateObject.IsOrphaned)
					{
						this.PutSession(tdsParserStateObject);
					}
				}
			}
		}

		// Token: 0x060016F9 RID: 5881 RVA: 0x000783C8 File Offset: 0x000765C8
		internal void Dispose()
		{
			List<TdsParserStateObject> cache = this._cache;
			lock (cache)
			{
				for (int i = 0; i < this._freeStateObjectCount; i++)
				{
					if (this._freeStateObjects[i] != null)
					{
						this._freeStateObjects[i].Dispose();
					}
				}
				this._freeStateObjects = null;
				this._freeStateObjectCount = 0;
				for (int j = 0; j < this._cache.Count; j++)
				{
					if (this._cache[j] != null)
					{
						if (this._cache[j].IsOrphaned)
						{
							this._cache[j].Dispose();
						}
						else
						{
							this._cache[j].DecrementPendingCallbacks(false);
						}
					}
				}
				this._cache.Clear();
				this._cachedCount = 0;
			}
		}

		// Token: 0x060016FA RID: 5882 RVA: 0x000784A8 File Offset: 0x000766A8
		internal TdsParserStateObject GetSession(object owner)
		{
			List<TdsParserStateObject> cache = this._cache;
			TdsParserStateObject tdsParserStateObject;
			lock (cache)
			{
				if (this.IsDisposed)
				{
					throw ADP.ClosedConnectionError();
				}
				if (this._freeStateObjectCount > 0)
				{
					this._freeStateObjectCount--;
					tdsParserStateObject = this._freeStateObjects[this._freeStateObjectCount];
					this._freeStateObjects[this._freeStateObjectCount] = null;
				}
				else
				{
					tdsParserStateObject = this._parser.CreateSession();
					this._cache.Add(tdsParserStateObject);
					this._cachedCount = this._cache.Count;
				}
				tdsParserStateObject.Activate(owner);
			}
			return tdsParserStateObject;
		}

		// Token: 0x060016FB RID: 5883 RVA: 0x00078558 File Offset: 0x00076758
		internal void PutSession(TdsParserStateObject session)
		{
			bool flag = session.Deactivate();
			List<TdsParserStateObject> cache = this._cache;
			lock (cache)
			{
				if (this.IsDisposed)
				{
					session.Dispose();
				}
				else if (flag && this._freeStateObjectCount < 10)
				{
					this._freeStateObjects[this._freeStateObjectCount] = session;
					this._freeStateObjectCount++;
				}
				else
				{
					this._cache.Remove(session);
					this._cachedCount = this._cache.Count;
					session.Dispose();
				}
				session.RemoveOwner();
			}
		}

		// Token: 0x17000442 RID: 1090
		// (get) Token: 0x060016FC RID: 5884 RVA: 0x00078600 File Offset: 0x00076800
		internal int ActiveSessionsCount
		{
			get
			{
				return this._cachedCount - this._freeStateObjectCount;
			}
		}

		// Token: 0x0400115A RID: 4442
		private const int MaxInactiveCount = 10;

		// Token: 0x0400115B RID: 4443
		private readonly TdsParser _parser;

		// Token: 0x0400115C RID: 4444
		private readonly List<TdsParserStateObject> _cache;

		// Token: 0x0400115D RID: 4445
		private int _cachedCount;

		// Token: 0x0400115E RID: 4446
		private TdsParserStateObject[] _freeStateObjects;

		// Token: 0x0400115F RID: 4447
		private int _freeStateObjectCount;
	}
}
