﻿using System;

namespace System.Data.SqlClient
{
	/// <summary>This enumeration provides additional information about the different notifications that can be received by the dependency event handler. </summary>
	// Token: 0x020001B0 RID: 432
	public enum SqlNotificationInfo
	{
		/// <summary>One or more tables were truncated.</summary>
		// Token: 0x04000E18 RID: 3608
		Truncate,
		/// <summary>Data was changed by an INSERT statement.</summary>
		// Token: 0x04000E19 RID: 3609
		Insert,
		/// <summary>Data was changed by an UPDATE statement.</summary>
		// Token: 0x04000E1A RID: 3610
		Update,
		/// <summary>Data was changed by a DELETE statement.</summary>
		// Token: 0x04000E1B RID: 3611
		Delete,
		/// <summary>An underlying object related to the query was dropped.</summary>
		// Token: 0x04000E1C RID: 3612
		Drop,
		/// <summary>An underlying server object related to the query was modified.</summary>
		// Token: 0x04000E1D RID: 3613
		Alter,
		/// <summary>The server was restarted (notifications are sent during restart.).</summary>
		// Token: 0x04000E1E RID: 3614
		Restart,
		/// <summary>An internal server error occurred.</summary>
		// Token: 0x04000E1F RID: 3615
		Error,
		/// <summary>A SELECT statement that cannot be notified or was provided.</summary>
		// Token: 0x04000E20 RID: 3616
		Query,
		/// <summary>A statement was provided that cannot be notified (for example, an UPDATE statement).</summary>
		// Token: 0x04000E21 RID: 3617
		Invalid,
		/// <summary>The SET options were not set appropriately at subscription time.</summary>
		// Token: 0x04000E22 RID: 3618
		Options,
		/// <summary>The statement was executed under an isolation mode that was not valid (for example, Snapshot).</summary>
		// Token: 0x04000E23 RID: 3619
		Isolation,
		/// <summary>The <see langword="SqlDependency" /> object has expired.</summary>
		// Token: 0x04000E24 RID: 3620
		Expired,
		/// <summary>Fires as a result of server resource pressure.</summary>
		// Token: 0x04000E25 RID: 3621
		Resource,
		/// <summary>A previous statement has caused query notifications to fire under the current transaction.</summary>
		// Token: 0x04000E26 RID: 3622
		PreviousFire,
		/// <summary>The subscribing query causes the number of templates on one of the target tables to exceed the maximum allowable limit.</summary>
		// Token: 0x04000E27 RID: 3623
		TemplateLimit,
		/// <summary>Used to distinguish the server-side cause for a query notification firing.</summary>
		// Token: 0x04000E28 RID: 3624
		Merge,
		/// <summary>Used when the info option sent by the server was not recognized by the client.</summary>
		// Token: 0x04000E29 RID: 3625
		Unknown = -1,
		/// <summary>The <see langword="SqlDependency" /> object already fired, and new commands cannot be added to it.</summary>
		// Token: 0x04000E2A RID: 3626
		AlreadyChanged = -2
	}
}
