﻿using System;
using System.Data.Common;
using System.Security;
using System.Security.Permissions;

namespace System.Data.SqlClient
{
	/// <summary>Associates a security action with a custom security attribute.</summary>
	// Token: 0x0200021A RID: 538
	[AttributeUsage(AttributeTargets.Assembly | AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Constructor | AttributeTargets.Method, AllowMultiple = true, Inherited = false)]
	[Serializable]
	public sealed class SqlClientPermissionAttribute : DBDataPermissionAttribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Data.SqlClient.SqlClientPermissionAttribute" /> class. </summary>
		/// <param name="action">One of the <see cref="T:System.Security.Permissions.SecurityAction" /> values representing an action that can be performed by using declarative security. </param>
		// Token: 0x06001810 RID: 6160 RVA: 0x0007D8EB File Offset: 0x0007BAEB
		public SqlClientPermissionAttribute(SecurityAction action) : base(action)
		{
		}

		/// <summary>Returns a <see cref="T:System.Data.SqlClient.SqlClientPermission" /> object that is configured according to the attribute properties.</summary>
		/// <returns>A <see cref="T:System.Data.SqlClient.SqlClientPermission" /> object.</returns>
		// Token: 0x06001811 RID: 6161 RVA: 0x0007D8F4 File Offset: 0x0007BAF4
		public override IPermission CreatePermission()
		{
			return new SqlClientPermission(this);
		}
	}
}
