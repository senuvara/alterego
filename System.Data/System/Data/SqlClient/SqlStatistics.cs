﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Common;

namespace System.Data.SqlClient
{
	// Token: 0x020001C9 RID: 457
	internal sealed class SqlStatistics
	{
		// Token: 0x060014D0 RID: 5328 RVA: 0x0006A4D5 File Offset: 0x000686D5
		internal static SqlStatistics StartTimer(SqlStatistics statistics)
		{
			if (statistics != null && !statistics.RequestExecutionTimer())
			{
				statistics = null;
			}
			return statistics;
		}

		// Token: 0x060014D1 RID: 5329 RVA: 0x0006A4E6 File Offset: 0x000686E6
		internal static void StopTimer(SqlStatistics statistics)
		{
			if (statistics != null)
			{
				statistics.ReleaseAndUpdateExecutionTimer();
			}
		}

		// Token: 0x17000400 RID: 1024
		// (get) Token: 0x060014D2 RID: 5330 RVA: 0x0006A4F1 File Offset: 0x000686F1
		// (set) Token: 0x060014D3 RID: 5331 RVA: 0x0006A4F9 File Offset: 0x000686F9
		internal bool WaitForDoneAfterRow
		{
			get
			{
				return this._waitForDoneAfterRow;
			}
			set
			{
				this._waitForDoneAfterRow = value;
			}
		}

		// Token: 0x17000401 RID: 1025
		// (get) Token: 0x060014D4 RID: 5332 RVA: 0x0006A502 File Offset: 0x00068702
		internal bool WaitForReply
		{
			get
			{
				return this._waitForReply;
			}
		}

		// Token: 0x060014D5 RID: 5333 RVA: 0x00005C14 File Offset: 0x00003E14
		internal SqlStatistics()
		{
		}

		// Token: 0x060014D6 RID: 5334 RVA: 0x0006A50A File Offset: 0x0006870A
		internal void ContinueOnNewConnection()
		{
			this._startExecutionTimestamp = 0L;
			this._startFetchTimestamp = 0L;
			this._waitForDoneAfterRow = false;
			this._waitForReply = false;
		}

		// Token: 0x060014D7 RID: 5335 RVA: 0x0006A52C File Offset: 0x0006872C
		internal IDictionary GetDictionary()
		{
			return new SqlStatistics.StatisticsDictionary(18)
			{
				{
					"BuffersReceived",
					this._buffersReceived
				},
				{
					"BuffersSent",
					this._buffersSent
				},
				{
					"BytesReceived",
					this._bytesReceived
				},
				{
					"BytesSent",
					this._bytesSent
				},
				{
					"CursorOpens",
					this._cursorOpens
				},
				{
					"IduCount",
					this._iduCount
				},
				{
					"IduRows",
					this._iduRows
				},
				{
					"PreparedExecs",
					this._preparedExecs
				},
				{
					"Prepares",
					this._prepares
				},
				{
					"SelectCount",
					this._selectCount
				},
				{
					"SelectRows",
					this._selectRows
				},
				{
					"ServerRoundtrips",
					this._serverRoundtrips
				},
				{
					"SumResultSets",
					this._sumResultSets
				},
				{
					"Transactions",
					this._transactions
				},
				{
					"UnpreparedExecs",
					this._unpreparedExecs
				},
				{
					"ConnectionTime",
					ADP.TimerToMilliseconds(this._connectionTime)
				},
				{
					"ExecutionTime",
					ADP.TimerToMilliseconds(this._executionTime)
				},
				{
					"NetworkServerTime",
					ADP.TimerToMilliseconds(this._networkServerTime)
				}
			};
		}

		// Token: 0x060014D8 RID: 5336 RVA: 0x0006A6DB File Offset: 0x000688DB
		internal bool RequestExecutionTimer()
		{
			if (this._startExecutionTimestamp == 0L)
			{
				ADP.TimerCurrent(out this._startExecutionTimestamp);
				return true;
			}
			return false;
		}

		// Token: 0x060014D9 RID: 5337 RVA: 0x0006A6F3 File Offset: 0x000688F3
		internal void RequestNetworkServerTimer()
		{
			if (this._startNetworkServerTimestamp == 0L)
			{
				ADP.TimerCurrent(out this._startNetworkServerTimestamp);
			}
			this._waitForReply = true;
		}

		// Token: 0x060014DA RID: 5338 RVA: 0x0006A70F File Offset: 0x0006890F
		internal void ReleaseAndUpdateExecutionTimer()
		{
			if (this._startExecutionTimestamp > 0L)
			{
				this._executionTime += ADP.TimerCurrent() - this._startExecutionTimestamp;
				this._startExecutionTimestamp = 0L;
			}
		}

		// Token: 0x060014DB RID: 5339 RVA: 0x0006A73C File Offset: 0x0006893C
		internal void ReleaseAndUpdateNetworkServerTimer()
		{
			if (this._waitForReply && this._startNetworkServerTimestamp > 0L)
			{
				this._networkServerTime += ADP.TimerCurrent() - this._startNetworkServerTimestamp;
				this._startNetworkServerTimestamp = 0L;
			}
			this._waitForReply = false;
		}

		// Token: 0x060014DC RID: 5340 RVA: 0x0006A778 File Offset: 0x00068978
		internal void Reset()
		{
			this._buffersReceived = 0L;
			this._buffersSent = 0L;
			this._bytesReceived = 0L;
			this._bytesSent = 0L;
			this._connectionTime = 0L;
			this._cursorOpens = 0L;
			this._executionTime = 0L;
			this._iduCount = 0L;
			this._iduRows = 0L;
			this._networkServerTime = 0L;
			this._preparedExecs = 0L;
			this._prepares = 0L;
			this._selectCount = 0L;
			this._selectRows = 0L;
			this._serverRoundtrips = 0L;
			this._sumResultSets = 0L;
			this._transactions = 0L;
			this._unpreparedExecs = 0L;
			this._waitForDoneAfterRow = false;
			this._waitForReply = false;
			this._startExecutionTimestamp = 0L;
			this._startNetworkServerTimestamp = 0L;
		}

		// Token: 0x060014DD RID: 5341 RVA: 0x0006A833 File Offset: 0x00068A33
		internal void SafeAdd(ref long value, long summand)
		{
			if (9223372036854775807L - value > summand)
			{
				value += summand;
				return;
			}
			value = long.MaxValue;
		}

		// Token: 0x060014DE RID: 5342 RVA: 0x0006A856 File Offset: 0x00068A56
		internal long SafeIncrement(ref long value)
		{
			if (value < 9223372036854775807L)
			{
				value += 1L;
			}
			return value;
		}

		// Token: 0x060014DF RID: 5343 RVA: 0x0006A86E File Offset: 0x00068A6E
		internal void UpdateStatistics()
		{
			if (this._closeTimestamp >= this._openTimestamp)
			{
				this.SafeAdd(ref this._connectionTime, this._closeTimestamp - this._openTimestamp);
				return;
			}
			this._connectionTime = long.MaxValue;
		}

		// Token: 0x04000E7F RID: 3711
		internal long _closeTimestamp;

		// Token: 0x04000E80 RID: 3712
		internal long _openTimestamp;

		// Token: 0x04000E81 RID: 3713
		internal long _startExecutionTimestamp;

		// Token: 0x04000E82 RID: 3714
		internal long _startFetchTimestamp;

		// Token: 0x04000E83 RID: 3715
		internal long _startNetworkServerTimestamp;

		// Token: 0x04000E84 RID: 3716
		internal long _buffersReceived;

		// Token: 0x04000E85 RID: 3717
		internal long _buffersSent;

		// Token: 0x04000E86 RID: 3718
		internal long _bytesReceived;

		// Token: 0x04000E87 RID: 3719
		internal long _bytesSent;

		// Token: 0x04000E88 RID: 3720
		internal long _connectionTime;

		// Token: 0x04000E89 RID: 3721
		internal long _cursorOpens;

		// Token: 0x04000E8A RID: 3722
		internal long _executionTime;

		// Token: 0x04000E8B RID: 3723
		internal long _iduCount;

		// Token: 0x04000E8C RID: 3724
		internal long _iduRows;

		// Token: 0x04000E8D RID: 3725
		internal long _networkServerTime;

		// Token: 0x04000E8E RID: 3726
		internal long _preparedExecs;

		// Token: 0x04000E8F RID: 3727
		internal long _prepares;

		// Token: 0x04000E90 RID: 3728
		internal long _selectCount;

		// Token: 0x04000E91 RID: 3729
		internal long _selectRows;

		// Token: 0x04000E92 RID: 3730
		internal long _serverRoundtrips;

		// Token: 0x04000E93 RID: 3731
		internal long _sumResultSets;

		// Token: 0x04000E94 RID: 3732
		internal long _transactions;

		// Token: 0x04000E95 RID: 3733
		internal long _unpreparedExecs;

		// Token: 0x04000E96 RID: 3734
		private bool _waitForDoneAfterRow;

		// Token: 0x04000E97 RID: 3735
		private bool _waitForReply;

		// Token: 0x020001CA RID: 458
		private sealed class StatisticsDictionary : Dictionary<object, object>, IDictionary, ICollection, IEnumerable
		{
			// Token: 0x060014E0 RID: 5344 RVA: 0x0006A8A7 File Offset: 0x00068AA7
			public StatisticsDictionary(int capacity) : base(capacity)
			{
			}

			// Token: 0x17000402 RID: 1026
			// (get) Token: 0x060014E1 RID: 5345 RVA: 0x0006A8B0 File Offset: 0x00068AB0
			ICollection IDictionary.Keys
			{
				get
				{
					SqlStatistics.StatisticsDictionary.Collection result;
					if ((result = this._keys) == null)
					{
						result = (this._keys = new SqlStatistics.StatisticsDictionary.Collection(this, base.Keys));
					}
					return result;
				}
			}

			// Token: 0x17000403 RID: 1027
			// (get) Token: 0x060014E2 RID: 5346 RVA: 0x0006A8DC File Offset: 0x00068ADC
			ICollection IDictionary.Values
			{
				get
				{
					SqlStatistics.StatisticsDictionary.Collection result;
					if ((result = this._values) == null)
					{
						result = (this._values = new SqlStatistics.StatisticsDictionary.Collection(this, base.Values));
					}
					return result;
				}
			}

			// Token: 0x060014E3 RID: 5347 RVA: 0x0006A908 File Offset: 0x00068B08
			IEnumerator IEnumerable.GetEnumerator()
			{
				return ((IDictionary)this).GetEnumerator();
			}

			// Token: 0x060014E4 RID: 5348 RVA: 0x0006A910 File Offset: 0x00068B10
			void ICollection.CopyTo(Array array, int arrayIndex)
			{
				this.ValidateCopyToArguments(array, arrayIndex);
				foreach (KeyValuePair<object, object> keyValuePair in this)
				{
					DictionaryEntry dictionaryEntry = new DictionaryEntry(keyValuePair.Key, keyValuePair.Value);
					array.SetValue(dictionaryEntry, arrayIndex++);
				}
			}

			// Token: 0x060014E5 RID: 5349 RVA: 0x0006A988 File Offset: 0x00068B88
			private void CopyKeys(Array array, int arrayIndex)
			{
				this.ValidateCopyToArguments(array, arrayIndex);
				foreach (KeyValuePair<object, object> keyValuePair in this)
				{
					array.SetValue(keyValuePair.Key, arrayIndex++);
				}
			}

			// Token: 0x060014E6 RID: 5350 RVA: 0x0006A9EC File Offset: 0x00068BEC
			private void CopyValues(Array array, int arrayIndex)
			{
				this.ValidateCopyToArguments(array, arrayIndex);
				foreach (KeyValuePair<object, object> keyValuePair in this)
				{
					array.SetValue(keyValuePair.Value, arrayIndex++);
				}
			}

			// Token: 0x060014E7 RID: 5351 RVA: 0x0006AA50 File Offset: 0x00068C50
			private void ValidateCopyToArguments(Array array, int arrayIndex)
			{
				if (array == null)
				{
					throw new ArgumentNullException("array");
				}
				if (array.Rank != 1)
				{
					throw new ArgumentException("Only single dimensional arrays are supported for the requested action.");
				}
				if (arrayIndex < 0)
				{
					throw new ArgumentOutOfRangeException("arrayIndex", "Non-negative number required.");
				}
				if (array.Length - arrayIndex < base.Count)
				{
					throw new ArgumentException("Destination array is not long enough to copy all the items in the collection. Check array index and length.");
				}
			}

			// Token: 0x04000E98 RID: 3736
			private SqlStatistics.StatisticsDictionary.Collection _keys;

			// Token: 0x04000E99 RID: 3737
			private SqlStatistics.StatisticsDictionary.Collection _values;

			// Token: 0x020001CB RID: 459
			private sealed class Collection : ICollection, IEnumerable
			{
				// Token: 0x060014E8 RID: 5352 RVA: 0x0006AAAE File Offset: 0x00068CAE
				public Collection(SqlStatistics.StatisticsDictionary dictionary, ICollection collection)
				{
					this._dictionary = dictionary;
					this._collection = collection;
				}

				// Token: 0x17000404 RID: 1028
				// (get) Token: 0x060014E9 RID: 5353 RVA: 0x0006AAC4 File Offset: 0x00068CC4
				int ICollection.Count
				{
					get
					{
						return this._collection.Count;
					}
				}

				// Token: 0x17000405 RID: 1029
				// (get) Token: 0x060014EA RID: 5354 RVA: 0x0006AAD1 File Offset: 0x00068CD1
				bool ICollection.IsSynchronized
				{
					get
					{
						return this._collection.IsSynchronized;
					}
				}

				// Token: 0x17000406 RID: 1030
				// (get) Token: 0x060014EB RID: 5355 RVA: 0x0006AADE File Offset: 0x00068CDE
				object ICollection.SyncRoot
				{
					get
					{
						return this._collection.SyncRoot;
					}
				}

				// Token: 0x060014EC RID: 5356 RVA: 0x0006AAEB File Offset: 0x00068CEB
				void ICollection.CopyTo(Array array, int arrayIndex)
				{
					if (this._collection is Dictionary<object, object>.KeyCollection)
					{
						this._dictionary.CopyKeys(array, arrayIndex);
						return;
					}
					this._dictionary.CopyValues(array, arrayIndex);
				}

				// Token: 0x060014ED RID: 5357 RVA: 0x0006AB15 File Offset: 0x00068D15
				IEnumerator IEnumerable.GetEnumerator()
				{
					return this._collection.GetEnumerator();
				}

				// Token: 0x04000E9A RID: 3738
				private readonly SqlStatistics.StatisticsDictionary _dictionary;

				// Token: 0x04000E9B RID: 3739
				private readonly ICollection _collection;
			}
		}
	}
}
