﻿using System;
using System.Xml;

namespace System.Data.SqlClient
{
	// Token: 0x020001B6 RID: 438
	internal class XmlDataFeed : DataFeed
	{
		// Token: 0x060013E0 RID: 5088 RVA: 0x00066C97 File Offset: 0x00064E97
		internal XmlDataFeed(XmlReader source)
		{
			this._source = source;
		}

		// Token: 0x04000E3D RID: 3645
		internal XmlReader _source;
	}
}
