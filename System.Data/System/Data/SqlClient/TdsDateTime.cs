﻿using System;

namespace System.Data.SqlClient
{
	// Token: 0x020001A0 RID: 416
	internal struct TdsDateTime
	{
		// Token: 0x04000DB7 RID: 3511
		public int days;

		// Token: 0x04000DB8 RID: 3512
		public int time;
	}
}
