﻿using System;
using System.Data.SqlClient.SNI;

namespace System.Data.SqlClient
{
	// Token: 0x02000211 RID: 529
	internal sealed class TdsParserStateObjectFactory
	{
		// Token: 0x17000452 RID: 1106
		// (get) Token: 0x0600179C RID: 6044 RVA: 0x0000EF1B File Offset: 0x0000D11B
		public static bool UseManagedSNI
		{
			get
			{
				return true;
			}
		}

		// Token: 0x17000453 RID: 1107
		// (get) Token: 0x0600179D RID: 6045 RVA: 0x0007B9B5 File Offset: 0x00079BB5
		public EncryptionOptions EncryptionOptions
		{
			get
			{
				return SNILoadHandle.SingletonInstance.Options;
			}
		}

		// Token: 0x17000454 RID: 1108
		// (get) Token: 0x0600179E RID: 6046 RVA: 0x0007B9C1 File Offset: 0x00079BC1
		public uint SNIStatus
		{
			get
			{
				return SNILoadHandle.SingletonInstance.Status;
			}
		}

		// Token: 0x0600179F RID: 6047 RVA: 0x0007B9CD File Offset: 0x00079BCD
		public TdsParserStateObject CreateTdsParserStateObject(TdsParser parser)
		{
			return new TdsParserStateObjectManaged(parser);
		}

		// Token: 0x060017A0 RID: 6048 RVA: 0x0007B9D5 File Offset: 0x00079BD5
		internal TdsParserStateObject CreateSessionObject(TdsParser tdsParser, TdsParserStateObject _pMarsPhysicalConObj, bool v)
		{
			return new TdsParserStateObjectManaged(tdsParser, _pMarsPhysicalConObj, true);
		}

		// Token: 0x060017A1 RID: 6049 RVA: 0x00005C14 File Offset: 0x00003E14
		public TdsParserStateObjectFactory()
		{
		}

		// Token: 0x060017A2 RID: 6050 RVA: 0x0007B9DF File Offset: 0x00079BDF
		// Note: this type is marked as 'beforefieldinit'.
		static TdsParserStateObjectFactory()
		{
		}

		// Token: 0x040011C6 RID: 4550
		public static readonly TdsParserStateObjectFactory Singleton = new TdsParserStateObjectFactory();
	}
}
