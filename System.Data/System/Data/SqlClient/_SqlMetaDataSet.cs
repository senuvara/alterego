﻿using System;
using System.Collections.ObjectModel;
using System.Data.Common;

namespace System.Data.SqlClient
{
	// Token: 0x02000201 RID: 513
	internal sealed class _SqlMetaDataSet
	{
		// Token: 0x060016DB RID: 5851 RVA: 0x00077E7C File Offset: 0x0007607C
		internal _SqlMetaDataSet(int count)
		{
			this._metaDataArray = new _SqlMetaData[count];
			for (int i = 0; i < this._metaDataArray.Length; i++)
			{
				this._metaDataArray[i] = new _SqlMetaData(i);
			}
		}

		// Token: 0x060016DC RID: 5852 RVA: 0x00077EBC File Offset: 0x000760BC
		private _SqlMetaDataSet(_SqlMetaDataSet original)
		{
			this.id = original.id;
			this.indexMap = original.indexMap;
			this.visibleColumns = original.visibleColumns;
			this.dbColumnSchema = original.dbColumnSchema;
			if (original._metaDataArray == null)
			{
				this._metaDataArray = null;
				return;
			}
			this._metaDataArray = new _SqlMetaData[original._metaDataArray.Length];
			for (int i = 0; i < this._metaDataArray.Length; i++)
			{
				this._metaDataArray[i] = (_SqlMetaData)original._metaDataArray[i].Clone();
			}
		}

		// Token: 0x1700043B RID: 1083
		// (get) Token: 0x060016DD RID: 5853 RVA: 0x00077F4F File Offset: 0x0007614F
		internal int Length
		{
			get
			{
				return this._metaDataArray.Length;
			}
		}

		// Token: 0x1700043C RID: 1084
		internal _SqlMetaData this[int index]
		{
			get
			{
				return this._metaDataArray[index];
			}
			set
			{
				this._metaDataArray[index] = value;
			}
		}

		// Token: 0x060016E0 RID: 5856 RVA: 0x00077F6E File Offset: 0x0007616E
		public object Clone()
		{
			return new _SqlMetaDataSet(this);
		}

		// Token: 0x0400112C RID: 4396
		internal ushort id;

		// Token: 0x0400112D RID: 4397
		internal int[] indexMap;

		// Token: 0x0400112E RID: 4398
		internal int visibleColumns;

		// Token: 0x0400112F RID: 4399
		internal DataTable schemaTable;

		// Token: 0x04001130 RID: 4400
		private readonly _SqlMetaData[] _metaDataArray;

		// Token: 0x04001131 RID: 4401
		internal ReadOnlyCollection<DbColumn> dbColumnSchema;
	}
}
