﻿using System;
using Unity;

namespace System.Data.SqlClient
{
	/// <summary>Collects information relevant to a warning or error returned by SQL Server.</summary>
	// Token: 0x020001A1 RID: 417
	[Serializable]
	public sealed class SqlError
	{
		// Token: 0x0600131B RID: 4891 RVA: 0x00064564 File Offset: 0x00062764
		internal SqlError(int infoNumber, byte errorState, byte errorClass, string server, string errorMessage, string procedure, int lineNumber, uint win32ErrorCode, Exception exception = null) : this(infoNumber, errorState, errorClass, server, errorMessage, procedure, lineNumber, exception)
		{
			this._win32ErrorCode = (int)win32ErrorCode;
		}

		// Token: 0x0600131C RID: 4892 RVA: 0x0006458C File Offset: 0x0006278C
		internal SqlError(int infoNumber, byte errorState, byte errorClass, string server, string errorMessage, string procedure, int lineNumber, Exception exception = null)
		{
			this._source = "Core .Net SqlClient Data Provider";
			base..ctor();
			this._number = infoNumber;
			this._state = errorState;
			this._errorClass = errorClass;
			this._server = server;
			this._message = errorMessage;
			this._procedure = procedure;
			this._lineNumber = lineNumber;
			this._win32ErrorCode = 0;
			this._exception = exception;
		}

		/// <summary>Gets the complete text of the error message.</summary>
		/// <returns>The complete text of the error.</returns>
		// Token: 0x0600131D RID: 4893 RVA: 0x000645EE File Offset: 0x000627EE
		public override string ToString()
		{
			return typeof(SqlError).ToString() + ": " + this._message;
		}

		/// <summary>Gets the name of the provider that generated the error.</summary>
		/// <returns>The name of the provider that generated the error.</returns>
		// Token: 0x1700037D RID: 893
		// (get) Token: 0x0600131E RID: 4894 RVA: 0x0006460F File Offset: 0x0006280F
		public string Source
		{
			get
			{
				return this._source;
			}
		}

		/// <summary>Gets a number that identifies the type of error.</summary>
		/// <returns>The number that identifies the type of error.</returns>
		// Token: 0x1700037E RID: 894
		// (get) Token: 0x0600131F RID: 4895 RVA: 0x00064617 File Offset: 0x00062817
		public int Number
		{
			get
			{
				return this._number;
			}
		}

		/// <summary>Some error messages can be raised at multiple points in the code for the Database Engine. For example, an 1105 error can be raised for several different conditions. Each specific condition that raises an error assigns a unique state code.</summary>
		/// <returns>The state code.</returns>
		// Token: 0x1700037F RID: 895
		// (get) Token: 0x06001320 RID: 4896 RVA: 0x0006461F File Offset: 0x0006281F
		public byte State
		{
			get
			{
				return this._state;
			}
		}

		/// <summary>Gets the severity level of the error returned from SQL Server.</summary>
		/// <returns>A value from 1 to 25 that indicates the severity level of the error. The default is 0.</returns>
		// Token: 0x17000380 RID: 896
		// (get) Token: 0x06001321 RID: 4897 RVA: 0x00064627 File Offset: 0x00062827
		public byte Class
		{
			get
			{
				return this._errorClass;
			}
		}

		/// <summary>Gets the name of the instance of SQL Server that generated the error.</summary>
		/// <returns>The name of the instance of SQL Server.</returns>
		// Token: 0x17000381 RID: 897
		// (get) Token: 0x06001322 RID: 4898 RVA: 0x0006462F File Offset: 0x0006282F
		public string Server
		{
			get
			{
				return this._server;
			}
		}

		/// <summary>Gets the text describing the error.</summary>
		/// <returns>The text describing the error.For more information on errors generated by SQL Server, see SQL Server Books Online.</returns>
		// Token: 0x17000382 RID: 898
		// (get) Token: 0x06001323 RID: 4899 RVA: 0x00064637 File Offset: 0x00062837
		public string Message
		{
			get
			{
				return this._message;
			}
		}

		/// <summary>Gets the name of the stored procedure or remote procedure call (RPC) that generated the error.</summary>
		/// <returns>The name of the stored procedure or RPC.For more information on errors generated by SQL Server, see SQL Server Books Online.</returns>
		// Token: 0x17000383 RID: 899
		// (get) Token: 0x06001324 RID: 4900 RVA: 0x0006463F File Offset: 0x0006283F
		public string Procedure
		{
			get
			{
				return this._procedure;
			}
		}

		/// <summary>Gets the line number within the Transact-SQL command batch or stored procedure that contains the error.</summary>
		/// <returns>The line number within the Transact-SQL command batch or stored procedure that contains the error.</returns>
		// Token: 0x17000384 RID: 900
		// (get) Token: 0x06001325 RID: 4901 RVA: 0x00064647 File Offset: 0x00062847
		public int LineNumber
		{
			get
			{
				return this._lineNumber;
			}
		}

		// Token: 0x17000385 RID: 901
		// (get) Token: 0x06001326 RID: 4902 RVA: 0x0006464F File Offset: 0x0006284F
		internal int Win32ErrorCode
		{
			get
			{
				return this._win32ErrorCode;
			}
		}

		// Token: 0x17000386 RID: 902
		// (get) Token: 0x06001327 RID: 4903 RVA: 0x00064657 File Offset: 0x00062857
		internal Exception Exception
		{
			get
			{
				return this._exception;
			}
		}

		// Token: 0x06001328 RID: 4904 RVA: 0x00010458 File Offset: 0x0000E658
		internal SqlError()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x04000DB9 RID: 3513
		private string _source;

		// Token: 0x04000DBA RID: 3514
		private int _number;

		// Token: 0x04000DBB RID: 3515
		private byte _state;

		// Token: 0x04000DBC RID: 3516
		private byte _errorClass;

		// Token: 0x04000DBD RID: 3517
		private string _server;

		// Token: 0x04000DBE RID: 3518
		private string _message;

		// Token: 0x04000DBF RID: 3519
		private string _procedure;

		// Token: 0x04000DC0 RID: 3520
		private int _lineNumber;

		// Token: 0x04000DC1 RID: 3521
		private int _win32ErrorCode;

		// Token: 0x04000DC2 RID: 3522
		private Exception _exception;
	}
}
