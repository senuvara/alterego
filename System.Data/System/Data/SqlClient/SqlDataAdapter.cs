﻿using System;
using System.Data.Common;

namespace System.Data.SqlClient
{
	/// <summary>Represents a set of data commands and a database connection that are used to fill the <see cref="T:System.Data.DataSet" /> and update a SQL Server database. This class cannot be inherited.</summary>
	// Token: 0x02000183 RID: 387
	public sealed class SqlDataAdapter : DbDataAdapter, IDbDataAdapter, IDataAdapter, ICloneable
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Data.SqlClient.SqlDataAdapter" /> class.</summary>
		// Token: 0x060011CC RID: 4556 RVA: 0x0005A955 File Offset: 0x00058B55
		public SqlDataAdapter()
		{
			GC.SuppressFinalize(this);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Data.SqlClient.SqlDataAdapter" /> class with the specified <see cref="T:System.Data.SqlClient.SqlCommand" /> as the <see cref="P:System.Data.SqlClient.SqlDataAdapter.SelectCommand" /> property.</summary>
		/// <param name="selectCommand">A <see cref="T:System.Data.SqlClient.SqlCommand" /> that is a Transact-SQL SELECT statement or stored procedure and is set as the <see cref="P:System.Data.SqlClient.SqlDataAdapter.SelectCommand" /> property of the <see cref="T:System.Data.SqlClient.SqlDataAdapter" />. </param>
		// Token: 0x060011CD RID: 4557 RVA: 0x0005A96A File Offset: 0x00058B6A
		public SqlDataAdapter(SqlCommand selectCommand) : this()
		{
			this.SelectCommand = selectCommand;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Data.SqlClient.SqlDataAdapter" /> class with a <see cref="P:System.Data.SqlClient.SqlDataAdapter.SelectCommand" /> and a connection string.</summary>
		/// <param name="selectCommandText">A <see cref="T:System.String" /> that is a Transact-SQL SELECT statement or stored procedure to be used by the <see cref="P:System.Data.SqlClient.SqlDataAdapter.SelectCommand" /> property of the <see cref="T:System.Data.SqlClient.SqlDataAdapter" />. </param>
		/// <param name="selectConnectionString">The connection string. If your connection string does not use <see langword="Integrated Security = true" />, you can use <see cref="M:System.Data.SqlClient.SqlDataAdapter.#ctor(System.String,System.Data.SqlClient.SqlConnection)" /> and <see cref="T:System.Data.SqlClient.SqlCredential" /> to pass the user ID and password more securely than by specifying the user ID and password as text in the connection string.</param>
		// Token: 0x060011CE RID: 4558 RVA: 0x0005A97C File Offset: 0x00058B7C
		public SqlDataAdapter(string selectCommandText, string selectConnectionString) : this()
		{
			SqlConnection connection = new SqlConnection(selectConnectionString);
			this.SelectCommand = new SqlCommand(selectCommandText, connection);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Data.SqlClient.SqlDataAdapter" /> class with a <see cref="P:System.Data.SqlClient.SqlDataAdapter.SelectCommand" /> and a <see cref="T:System.Data.SqlClient.SqlConnection" /> object.</summary>
		/// <param name="selectCommandText">A <see cref="T:System.String" /> that is a Transact-SQL SELECT statement or stored procedure to be used by the <see cref="P:System.Data.SqlClient.SqlDataAdapter.SelectCommand" /> property of the <see cref="T:System.Data.SqlClient.SqlDataAdapter" />. </param>
		/// <param name="selectConnection">A <see cref="T:System.Data.SqlClient.SqlConnection" /> that represents the connection. If your connection string does not use <see langword="Integrated Security = true" />, you can use <see cref="T:System.Data.SqlClient.SqlCredential" /> to pass the user ID and password more securely than by specifying the user ID and password as text in the connection string.</param>
		// Token: 0x060011CF RID: 4559 RVA: 0x0005A9A3 File Offset: 0x00058BA3
		public SqlDataAdapter(string selectCommandText, SqlConnection selectConnection) : this()
		{
			this.SelectCommand = new SqlCommand(selectCommandText, selectConnection);
		}

		// Token: 0x060011D0 RID: 4560 RVA: 0x0005A9B8 File Offset: 0x00058BB8
		private SqlDataAdapter(SqlDataAdapter from) : base(from)
		{
			GC.SuppressFinalize(this);
		}

		/// <summary>Gets or sets a Transact-SQL statement or stored procedure to delete records from the data set.</summary>
		/// <returns>A <see cref="T:System.Data.SqlClient.SqlCommand" /> used during <see cref="M:System.Data.Common.DbDataAdapter.Update(System.Data.DataSet)" /> to delete records in the database that correspond to deleted rows in the <see cref="T:System.Data.DataSet" />.</returns>
		// Token: 0x1700034A RID: 842
		// (get) Token: 0x060011D1 RID: 4561 RVA: 0x0005A9CE File Offset: 0x00058BCE
		// (set) Token: 0x060011D2 RID: 4562 RVA: 0x0005A9D6 File Offset: 0x00058BD6
		public new SqlCommand DeleteCommand
		{
			get
			{
				return this._deleteCommand;
			}
			set
			{
				this._deleteCommand = value;
			}
		}

		/// <summary>For a description of this member, see <see cref="P:System.Data.IDbDataAdapter.DeleteCommand" />.</summary>
		/// <returns>An <see cref="T:System.Data.IDbCommand" /> that is used during <see cref="M:System.Data.Common.DbDataAdapter.Update(System.Data.DataSet)" /> to delete records in the data source for deleted rows in the data set.</returns>
		// Token: 0x1700034B RID: 843
		// (get) Token: 0x060011D3 RID: 4563 RVA: 0x0005A9CE File Offset: 0x00058BCE
		// (set) Token: 0x060011D4 RID: 4564 RVA: 0x0005A9DF File Offset: 0x00058BDF
		IDbCommand IDbDataAdapter.DeleteCommand
		{
			get
			{
				return this._deleteCommand;
			}
			set
			{
				this._deleteCommand = (SqlCommand)value;
			}
		}

		/// <summary>Gets or sets a Transact-SQL statement or stored procedure to insert new records into the data source.</summary>
		/// <returns>A <see cref="T:System.Data.SqlClient.SqlCommand" /> used during <see cref="M:System.Data.Common.DbDataAdapter.Update(System.Data.DataSet)" /> to insert records into the database that correspond to new rows in the <see cref="T:System.Data.DataSet" />.</returns>
		// Token: 0x1700034C RID: 844
		// (get) Token: 0x060011D5 RID: 4565 RVA: 0x0005A9ED File Offset: 0x00058BED
		// (set) Token: 0x060011D6 RID: 4566 RVA: 0x0005A9F5 File Offset: 0x00058BF5
		public new SqlCommand InsertCommand
		{
			get
			{
				return this._insertCommand;
			}
			set
			{
				this._insertCommand = value;
			}
		}

		/// <summary>For a description of this member, see <see cref="P:System.Data.IDbDataAdapter.InsertCommand" />.</summary>
		/// <returns>An <see cref="T:System.Data.IDbCommand" /> that is used during <see cref="M:System.Data.Common.DbDataAdapter.Update(System.Data.DataSet)" /> to insert records in the data source for new rows in the data set.</returns>
		// Token: 0x1700034D RID: 845
		// (get) Token: 0x060011D7 RID: 4567 RVA: 0x0005A9ED File Offset: 0x00058BED
		// (set) Token: 0x060011D8 RID: 4568 RVA: 0x0005A9FE File Offset: 0x00058BFE
		IDbCommand IDbDataAdapter.InsertCommand
		{
			get
			{
				return this._insertCommand;
			}
			set
			{
				this._insertCommand = (SqlCommand)value;
			}
		}

		/// <summary>Gets or sets a Transact-SQL statement or stored procedure used to select records in the data source.</summary>
		/// <returns>A <see cref="T:System.Data.SqlClient.SqlCommand" /> used during <see cref="M:System.Data.Common.DbDataAdapter.Fill(System.Data.DataSet)" /> to select records from the database for placement in the <see cref="T:System.Data.DataSet" />.</returns>
		// Token: 0x1700034E RID: 846
		// (get) Token: 0x060011D9 RID: 4569 RVA: 0x0005AA0C File Offset: 0x00058C0C
		// (set) Token: 0x060011DA RID: 4570 RVA: 0x0005AA14 File Offset: 0x00058C14
		public new SqlCommand SelectCommand
		{
			get
			{
				return this._selectCommand;
			}
			set
			{
				this._selectCommand = value;
			}
		}

		/// <summary>For a description of this member, see <see cref="P:System.Data.IDbDataAdapter.SelectCommand" />.</summary>
		/// <returns>An <see cref="T:System.Data.IDbCommand" /> that is used during <see cref="M:System.Data.Common.DbDataAdapter.Update(System.Data.DataSet)" /> to select records from data source for placement in the data set.</returns>
		// Token: 0x1700034F RID: 847
		// (get) Token: 0x060011DB RID: 4571 RVA: 0x0005AA0C File Offset: 0x00058C0C
		// (set) Token: 0x060011DC RID: 4572 RVA: 0x0005AA1D File Offset: 0x00058C1D
		IDbCommand IDbDataAdapter.SelectCommand
		{
			get
			{
				return this._selectCommand;
			}
			set
			{
				this._selectCommand = (SqlCommand)value;
			}
		}

		/// <summary>Gets or sets a Transact-SQL statement or stored procedure used to update records in the data source.</summary>
		/// <returns>A <see cref="T:System.Data.SqlClient.SqlCommand" /> used during <see cref="M:System.Data.Common.DbDataAdapter.Update(System.Data.DataSet)" /> to update records in the database that correspond to modified rows in the <see cref="T:System.Data.DataSet" />.</returns>
		// Token: 0x17000350 RID: 848
		// (get) Token: 0x060011DD RID: 4573 RVA: 0x0005AA2B File Offset: 0x00058C2B
		// (set) Token: 0x060011DE RID: 4574 RVA: 0x0005AA33 File Offset: 0x00058C33
		public new SqlCommand UpdateCommand
		{
			get
			{
				return this._updateCommand;
			}
			set
			{
				this._updateCommand = value;
			}
		}

		/// <summary>For a description of this member, see <see cref="P:System.Data.IDbDataAdapter.UpdateCommand" />.</summary>
		/// <returns>An <see cref="T:System.Data.IdbCommand" /> that is used during <see cref="M:System.Data.Common.DbDataAdapter.Update(System.Data.DataSet)" /> to update records in the data source for modified rows in the data set.</returns>
		// Token: 0x17000351 RID: 849
		// (get) Token: 0x060011DF RID: 4575 RVA: 0x0005AA2B File Offset: 0x00058C2B
		// (set) Token: 0x060011E0 RID: 4576 RVA: 0x0005AA3C File Offset: 0x00058C3C
		IDbCommand IDbDataAdapter.UpdateCommand
		{
			get
			{
				return this._updateCommand;
			}
			set
			{
				this._updateCommand = (SqlCommand)value;
			}
		}

		/// <summary>Gets or sets the number of rows that are processed in each round-trip to the server.</summary>
		/// <returns>The number of rows to process per-batch. Value isEffect0There is no limit on the batch size..1Disables batch updating.&gt;1Changes are sent using batches of <see cref="P:System.Data.SqlClient.SqlDataAdapter.UpdateBatchSize" /> operations at a time.When setting this to a value other than 1, all the commands associated with the <see cref="T:System.Data.SqlClient.SqlDataAdapter" /> have to have their UpdatedRowSource property set to <see langword="None" /> or <see langword="OutputParameters" />. An exception is thrown otherwise.</returns>
		// Token: 0x17000352 RID: 850
		// (get) Token: 0x060011E1 RID: 4577 RVA: 0x0005AA4A File Offset: 0x00058C4A
		// (set) Token: 0x060011E2 RID: 4578 RVA: 0x0005AA52 File Offset: 0x00058C52
		public override int UpdateBatchSize
		{
			get
			{
				return this._updateBatchSize;
			}
			set
			{
				if (0 > value)
				{
					throw ADP.ArgumentOutOfRange("UpdateBatchSize");
				}
				this._updateBatchSize = value;
			}
		}

		// Token: 0x060011E3 RID: 4579 RVA: 0x0005AA6A File Offset: 0x00058C6A
		protected override int AddToBatch(IDbCommand command)
		{
			int commandCount = this._commandSet.CommandCount;
			this._commandSet.Append((SqlCommand)command);
			return commandCount;
		}

		// Token: 0x060011E4 RID: 4580 RVA: 0x0005AA88 File Offset: 0x00058C88
		protected override void ClearBatch()
		{
			this._commandSet.Clear();
		}

		// Token: 0x060011E5 RID: 4581 RVA: 0x0005AA95 File Offset: 0x00058C95
		protected override int ExecuteBatch()
		{
			return this._commandSet.ExecuteNonQuery();
		}

		// Token: 0x060011E6 RID: 4582 RVA: 0x0005AAA2 File Offset: 0x00058CA2
		protected override IDataParameter GetBatchedParameter(int commandIdentifier, int parameterIndex)
		{
			return this._commandSet.GetParameter(commandIdentifier, parameterIndex);
		}

		// Token: 0x060011E7 RID: 4583 RVA: 0x0005AAB1 File Offset: 0x00058CB1
		protected override bool GetBatchedRecordsAffected(int commandIdentifier, out int recordsAffected, out Exception error)
		{
			return this._commandSet.GetBatchedAffected(commandIdentifier, out recordsAffected, out error);
		}

		// Token: 0x060011E8 RID: 4584 RVA: 0x0005AAC4 File Offset: 0x00058CC4
		protected override void InitializeBatching()
		{
			this._commandSet = new SqlCommandSet();
			SqlCommand sqlCommand = this.SelectCommand;
			if (sqlCommand == null)
			{
				sqlCommand = this.InsertCommand;
				if (sqlCommand == null)
				{
					sqlCommand = this.UpdateCommand;
					if (sqlCommand == null)
					{
						sqlCommand = this.DeleteCommand;
					}
				}
			}
			if (sqlCommand != null)
			{
				this._commandSet.Connection = sqlCommand.Connection;
				this._commandSet.Transaction = sqlCommand.Transaction;
				this._commandSet.CommandTimeout = sqlCommand.CommandTimeout;
			}
		}

		// Token: 0x060011E9 RID: 4585 RVA: 0x0005AB37 File Offset: 0x00058D37
		protected override void TerminateBatching()
		{
			if (this._commandSet != null)
			{
				this._commandSet.Dispose();
				this._commandSet = null;
			}
		}

		/// <summary>For a description of this member, see <see cref="M:System.ICloneable.Clone" />.</summary>
		/// <returns>A new object that is a copy of the current instance.</returns>
		// Token: 0x060011EA RID: 4586 RVA: 0x0005AB53 File Offset: 0x00058D53
		object ICloneable.Clone()
		{
			return new SqlDataAdapter(this);
		}

		// Token: 0x060011EB RID: 4587 RVA: 0x0005AB5B File Offset: 0x00058D5B
		protected override RowUpdatedEventArgs CreateRowUpdatedEvent(DataRow dataRow, IDbCommand command, StatementType statementType, DataTableMapping tableMapping)
		{
			return new SqlRowUpdatedEventArgs(dataRow, command, statementType, tableMapping);
		}

		// Token: 0x060011EC RID: 4588 RVA: 0x0005AB67 File Offset: 0x00058D67
		protected override RowUpdatingEventArgs CreateRowUpdatingEvent(DataRow dataRow, IDbCommand command, StatementType statementType, DataTableMapping tableMapping)
		{
			return new SqlRowUpdatingEventArgs(dataRow, command, statementType, tableMapping);
		}

		/// <summary>Occurs during <see cref="M:System.Data.Common.DbDataAdapter.Update(System.Data.DataSet)" /> after a command is executed against the data source. The attempt to update is made, so the event fires.</summary>
		// Token: 0x14000023 RID: 35
		// (add) Token: 0x060011ED RID: 4589 RVA: 0x0005AB73 File Offset: 0x00058D73
		// (remove) Token: 0x060011EE RID: 4590 RVA: 0x0005AB86 File Offset: 0x00058D86
		public event SqlRowUpdatedEventHandler RowUpdated
		{
			add
			{
				base.Events.AddHandler(SqlDataAdapter.EventRowUpdated, value);
			}
			remove
			{
				base.Events.RemoveHandler(SqlDataAdapter.EventRowUpdated, value);
			}
		}

		/// <summary>Occurs during <see cref="M:System.Data.Common.DbDataAdapter.Update(System.Data.DataSet)" /> before a command is executed against the data source. The attempt to update is made, so the event fires.</summary>
		// Token: 0x14000024 RID: 36
		// (add) Token: 0x060011EF RID: 4591 RVA: 0x0005AB9C File Offset: 0x00058D9C
		// (remove) Token: 0x060011F0 RID: 4592 RVA: 0x0005AC00 File Offset: 0x00058E00
		public event SqlRowUpdatingEventHandler RowUpdating
		{
			add
			{
				SqlRowUpdatingEventHandler sqlRowUpdatingEventHandler = (SqlRowUpdatingEventHandler)base.Events[SqlDataAdapter.EventRowUpdating];
				if (sqlRowUpdatingEventHandler != null && value.Target is DbCommandBuilder)
				{
					SqlRowUpdatingEventHandler sqlRowUpdatingEventHandler2 = (SqlRowUpdatingEventHandler)ADP.FindBuilder(sqlRowUpdatingEventHandler);
					if (sqlRowUpdatingEventHandler2 != null)
					{
						base.Events.RemoveHandler(SqlDataAdapter.EventRowUpdating, sqlRowUpdatingEventHandler2);
					}
				}
				base.Events.AddHandler(SqlDataAdapter.EventRowUpdating, value);
			}
			remove
			{
				base.Events.RemoveHandler(SqlDataAdapter.EventRowUpdating, value);
			}
		}

		// Token: 0x060011F1 RID: 4593 RVA: 0x0005AC14 File Offset: 0x00058E14
		protected override void OnRowUpdated(RowUpdatedEventArgs value)
		{
			SqlRowUpdatedEventHandler sqlRowUpdatedEventHandler = (SqlRowUpdatedEventHandler)base.Events[SqlDataAdapter.EventRowUpdated];
			if (sqlRowUpdatedEventHandler != null && value is SqlRowUpdatedEventArgs)
			{
				sqlRowUpdatedEventHandler(this, (SqlRowUpdatedEventArgs)value);
			}
			base.OnRowUpdated(value);
		}

		// Token: 0x060011F2 RID: 4594 RVA: 0x0005AC58 File Offset: 0x00058E58
		protected override void OnRowUpdating(RowUpdatingEventArgs value)
		{
			SqlRowUpdatingEventHandler sqlRowUpdatingEventHandler = (SqlRowUpdatingEventHandler)base.Events[SqlDataAdapter.EventRowUpdating];
			if (sqlRowUpdatingEventHandler != null && value is SqlRowUpdatingEventArgs)
			{
				sqlRowUpdatingEventHandler(this, (SqlRowUpdatingEventArgs)value);
			}
			base.OnRowUpdating(value);
		}

		// Token: 0x060011F3 RID: 4595 RVA: 0x0005AC9A File Offset: 0x00058E9A
		// Note: this type is marked as 'beforefieldinit'.
		static SqlDataAdapter()
		{
		}

		// Token: 0x04000CB7 RID: 3255
		private static readonly object EventRowUpdated = new object();

		// Token: 0x04000CB8 RID: 3256
		private static readonly object EventRowUpdating = new object();

		// Token: 0x04000CB9 RID: 3257
		private SqlCommand _deleteCommand;

		// Token: 0x04000CBA RID: 3258
		private SqlCommand _insertCommand;

		// Token: 0x04000CBB RID: 3259
		private SqlCommand _selectCommand;

		// Token: 0x04000CBC RID: 3260
		private SqlCommand _updateCommand;

		// Token: 0x04000CBD RID: 3261
		private SqlCommandSet _commandSet;

		// Token: 0x04000CBE RID: 3262
		private int _updateBatchSize = 1;
	}
}
