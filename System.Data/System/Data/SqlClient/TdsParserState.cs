﻿using System;

namespace System.Data.SqlClient
{
	// Token: 0x020001FA RID: 506
	internal enum TdsParserState
	{
		// Token: 0x040010ED RID: 4333
		Closed,
		// Token: 0x040010EE RID: 4334
		OpenNotLoggedIn,
		// Token: 0x040010EF RID: 4335
		OpenLoggedIn,
		// Token: 0x040010F0 RID: 4336
		Broken
	}
}
