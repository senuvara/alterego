﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.ProviderBase;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;

namespace System.Data.SqlClient
{
	// Token: 0x020001AA RID: 426
	internal sealed class SqlInternalConnectionTds : SqlInternalConnection, IDisposable
	{
		// Token: 0x170003AA RID: 938
		// (get) Token: 0x06001384 RID: 4996 RVA: 0x00065290 File Offset: 0x00063490
		internal SessionData CurrentSessionData
		{
			get
			{
				if (this._currentSessionData != null)
				{
					this._currentSessionData._database = base.CurrentDatabase;
					this._currentSessionData._language = this._currentLanguage;
				}
				return this._currentSessionData;
			}
		}

		// Token: 0x170003AB RID: 939
		// (get) Token: 0x06001385 RID: 4997 RVA: 0x000652C2 File Offset: 0x000634C2
		internal SqlConnectionTimeoutErrorInternal TimeoutErrorInternal
		{
			get
			{
				return this._timeoutErrorInternal;
			}
		}

		// Token: 0x06001386 RID: 4998 RVA: 0x000652CC File Offset: 0x000634CC
		internal SqlInternalConnectionTds(DbConnectionPoolIdentity identity, SqlConnectionString connectionOptions, object providerInfo, bool redirectedUserInstance, SqlConnectionString userConnectionOptions = null, SessionData reconnectSessionData = null, bool applyTransientFaultHandling = false) : base(connectionOptions)
		{
			if (connectionOptions.ConnectRetryCount > 0)
			{
				this._recoverySessionData = reconnectSessionData;
				if (reconnectSessionData == null)
				{
					this._currentSessionData = new SessionData();
				}
				else
				{
					this._currentSessionData = new SessionData(this._recoverySessionData);
					this._originalDatabase = this._recoverySessionData._initialDatabase;
					this._originalLanguage = this._recoverySessionData._initialLanguage;
				}
			}
			this._identity = identity;
			this._poolGroupProviderInfo = (SqlConnectionPoolGroupProviderInfo)providerInfo;
			this._fResetConnection = connectionOptions.ConnectionReset;
			if (this._fResetConnection && this._recoverySessionData == null)
			{
				this._originalDatabase = connectionOptions.InitialCatalog;
				this._originalLanguage = connectionOptions.CurrentLanguage;
			}
			this._timeoutErrorInternal = new SqlConnectionTimeoutErrorInternal();
			this._parserLock.Wait(false);
			this.ThreadHasParserLockForClose = true;
			try
			{
				TimeoutTimer timeoutTimer = TimeoutTimer.StartSecondsTimeout(connectionOptions.ConnectTimeout);
				int num = applyTransientFaultHandling ? (connectionOptions.ConnectRetryCount + 1) : 1;
				int num2 = connectionOptions.ConnectRetryInterval * 1000;
				for (int i = 0; i < num; i++)
				{
					try
					{
						this.OpenLoginEnlist(timeoutTimer, connectionOptions, redirectedUserInstance);
						break;
					}
					catch (SqlException ex)
					{
						if (i + 1 == num || !applyTransientFaultHandling || timeoutTimer.IsExpired || timeoutTimer.MillisecondsRemaining < (long)num2 || !this.IsTransientError(ex))
						{
							throw ex;
						}
						Thread.Sleep(num2);
					}
				}
			}
			finally
			{
				this.ThreadHasParserLockForClose = false;
				this._parserLock.Release();
			}
		}

		// Token: 0x06001387 RID: 4999 RVA: 0x00065474 File Offset: 0x00063674
		private bool IsTransientError(SqlException exc)
		{
			if (exc == null)
			{
				return false;
			}
			foreach (object obj in exc.Errors)
			{
				SqlError sqlError = (SqlError)obj;
				if (SqlInternalConnectionTds.s_transientErrors.Contains(sqlError.Number))
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x170003AC RID: 940
		// (get) Token: 0x06001388 RID: 5000 RVA: 0x000654E4 File Offset: 0x000636E4
		internal Guid ClientConnectionId
		{
			get
			{
				return this._clientConnectionId;
			}
		}

		// Token: 0x170003AD RID: 941
		// (get) Token: 0x06001389 RID: 5001 RVA: 0x000654EC File Offset: 0x000636EC
		internal Guid OriginalClientConnectionId
		{
			get
			{
				return this._originalClientConnectionId;
			}
		}

		// Token: 0x170003AE RID: 942
		// (get) Token: 0x0600138A RID: 5002 RVA: 0x000654F4 File Offset: 0x000636F4
		internal string RoutingDestination
		{
			get
			{
				return this._routingDestination;
			}
		}

		// Token: 0x170003AF RID: 943
		// (get) Token: 0x0600138B RID: 5003 RVA: 0x000654FC File Offset: 0x000636FC
		internal override SqlInternalTransaction CurrentTransaction
		{
			get
			{
				return this._parser.CurrentTransaction;
			}
		}

		// Token: 0x170003B0 RID: 944
		// (get) Token: 0x0600138C RID: 5004 RVA: 0x00065509 File Offset: 0x00063709
		internal override SqlInternalTransaction AvailableInternalTransaction
		{
			get
			{
				if (!this._parser._fResetConnection)
				{
					return this.CurrentTransaction;
				}
				return null;
			}
		}

		// Token: 0x170003B1 RID: 945
		// (get) Token: 0x0600138D RID: 5005 RVA: 0x00065522 File Offset: 0x00063722
		internal override SqlInternalTransaction PendingTransaction
		{
			get
			{
				return this._parser.PendingTransaction;
			}
		}

		// Token: 0x170003B2 RID: 946
		// (get) Token: 0x0600138E RID: 5006 RVA: 0x0006552F File Offset: 0x0006372F
		internal DbConnectionPoolIdentity Identity
		{
			get
			{
				return this._identity;
			}
		}

		// Token: 0x170003B3 RID: 947
		// (get) Token: 0x0600138F RID: 5007 RVA: 0x00065537 File Offset: 0x00063737
		internal string InstanceName
		{
			get
			{
				return this._instanceName;
			}
		}

		// Token: 0x170003B4 RID: 948
		// (get) Token: 0x06001390 RID: 5008 RVA: 0x0006553F File Offset: 0x0006373F
		internal override bool IsLockedForBulkCopy
		{
			get
			{
				return !this.Parser.MARSOn && this.Parser._physicalStateObj.BcpLock;
			}
		}

		// Token: 0x170003B5 RID: 949
		// (get) Token: 0x06001391 RID: 5009 RVA: 0x00065560 File Offset: 0x00063760
		protected internal override bool IsNonPoolableTransactionRoot
		{
			get
			{
				return this.IsTransactionRoot && (!this.IsKatmaiOrNewer || base.Pool == null);
			}
		}

		// Token: 0x170003B6 RID: 950
		// (get) Token: 0x06001392 RID: 5010 RVA: 0x0006557F File Offset: 0x0006377F
		internal override bool IsKatmaiOrNewer
		{
			get
			{
				return this._parser.IsKatmaiOrNewer;
			}
		}

		// Token: 0x170003B7 RID: 951
		// (get) Token: 0x06001393 RID: 5011 RVA: 0x0006558C File Offset: 0x0006378C
		internal int PacketSize
		{
			get
			{
				return this._currentPacketSize;
			}
		}

		// Token: 0x170003B8 RID: 952
		// (get) Token: 0x06001394 RID: 5012 RVA: 0x00065594 File Offset: 0x00063794
		internal TdsParser Parser
		{
			get
			{
				return this._parser;
			}
		}

		// Token: 0x170003B9 RID: 953
		// (get) Token: 0x06001395 RID: 5013 RVA: 0x0006559C File Offset: 0x0006379C
		internal string ServerProvidedFailOverPartner
		{
			get
			{
				return this._currentFailoverPartner;
			}
		}

		// Token: 0x170003BA RID: 954
		// (get) Token: 0x06001396 RID: 5014 RVA: 0x000655A4 File Offset: 0x000637A4
		internal SqlConnectionPoolGroupProviderInfo PoolGroupProviderInfo
		{
			get
			{
				return this._poolGroupProviderInfo;
			}
		}

		// Token: 0x170003BB RID: 955
		// (get) Token: 0x06001397 RID: 5015 RVA: 0x000655AC File Offset: 0x000637AC
		protected override bool ReadyToPrepareTransaction
		{
			get
			{
				return base.FindLiveReader(null) == null;
			}
		}

		// Token: 0x170003BC RID: 956
		// (get) Token: 0x06001398 RID: 5016 RVA: 0x000655B8 File Offset: 0x000637B8
		public override string ServerVersion
		{
			get
			{
				return string.Format(null, "{0:00}.{1:00}.{2:0000}", this._loginAck.majorVersion, (short)this._loginAck.minorVersion, this._loginAck.buildNum);
			}
		}

		// Token: 0x170003BD RID: 957
		// (get) Token: 0x06001399 RID: 5017 RVA: 0x000061C5 File Offset: 0x000043C5
		protected override bool UnbindOnTransactionCompletion
		{
			get
			{
				return false;
			}
		}

		// Token: 0x0600139A RID: 5018 RVA: 0x000655F8 File Offset: 0x000637F8
		protected override void ChangeDatabaseInternal(string database)
		{
			database = SqlConnection.FixupDatabaseTransactionName(database);
			this._parser.TdsExecuteSQLBatch("use " + database, base.ConnectionOptions.ConnectTimeout, null, this._parser._physicalStateObj, true, false);
			this._parser.Run(RunBehavior.UntilDone, null, null, null, this._parser._physicalStateObj);
		}

		// Token: 0x0600139B RID: 5019 RVA: 0x00065658 File Offset: 0x00063858
		public override void Dispose()
		{
			try
			{
				TdsParser tdsParser = Interlocked.Exchange<TdsParser>(ref this._parser, null);
				if (tdsParser != null)
				{
					tdsParser.Disconnect();
				}
			}
			finally
			{
				this._loginAck = null;
				this._fConnectionOpen = false;
			}
			base.Dispose();
		}

		// Token: 0x0600139C RID: 5020 RVA: 0x000656A4 File Offset: 0x000638A4
		internal override void ValidateConnectionForExecute(SqlCommand command)
		{
			TdsParser parser = this._parser;
			if (parser == null || parser.State == TdsParserState.Broken || parser.State == TdsParserState.Closed)
			{
				throw ADP.ClosedConnectionError();
			}
			SqlDataReader sqlDataReader = null;
			if (parser.MARSOn)
			{
				if (command != null)
				{
					sqlDataReader = base.FindLiveReader(command);
				}
			}
			else
			{
				if (this._asyncCommandCount > 0)
				{
					throw SQL.MARSUnspportedOnConnection();
				}
				sqlDataReader = base.FindLiveReader(null);
			}
			if (sqlDataReader != null)
			{
				throw ADP.OpenReaderExists();
			}
			if (!parser.MARSOn && parser._physicalStateObj._pendingData)
			{
				parser.DrainData(parser._physicalStateObj);
			}
			parser.RollbackOrphanedAPITransactions();
		}

		// Token: 0x0600139D RID: 5021 RVA: 0x00065730 File Offset: 0x00063930
		internal void CheckEnlistedTransactionBinding()
		{
			Transaction enlistedTransaction = base.EnlistedTransaction;
			if (enlistedTransaction != null)
			{
				if (base.ConnectionOptions.TransactionBinding == SqlConnectionString.TransactionBindingEnum.ExplicitUnbind)
				{
					Transaction obj = Transaction.Current;
					if (enlistedTransaction.TransactionInformation.Status != TransactionStatus.Active || !enlistedTransaction.Equals(obj))
					{
						throw ADP.TransactionConnectionMismatch();
					}
				}
				else if (enlistedTransaction.TransactionInformation.Status != TransactionStatus.Active)
				{
					if (base.EnlistedTransactionDisposed)
					{
						base.DetachTransaction(enlistedTransaction, true);
						return;
					}
					throw ADP.TransactionCompletedButNotDisposed();
				}
			}
		}

		// Token: 0x0600139E RID: 5022 RVA: 0x000657A3 File Offset: 0x000639A3
		internal override bool IsConnectionAlive(bool throwOnException)
		{
			return this._parser._physicalStateObj.IsConnectionAlive(throwOnException);
		}

		// Token: 0x0600139F RID: 5023 RVA: 0x000657B6 File Offset: 0x000639B6
		protected override void Activate(Transaction transaction)
		{
			if (null != transaction)
			{
				if (base.ConnectionOptions.Enlist)
				{
					base.Enlist(transaction);
					return;
				}
			}
			else
			{
				base.Enlist(null);
			}
		}

		// Token: 0x060013A0 RID: 5024 RVA: 0x000657DD File Offset: 0x000639DD
		protected override void InternalDeactivate()
		{
			if (this._asyncCommandCount != 0)
			{
				base.DoomThisConnection();
			}
			if (!this.IsNonPoolableTransactionRoot && this._parser != null)
			{
				this._parser.Deactivate(base.IsConnectionDoomed);
				if (!base.IsConnectionDoomed)
				{
					this.ResetConnection();
				}
			}
		}

		// Token: 0x060013A1 RID: 5025 RVA: 0x0006581C File Offset: 0x00063A1C
		private void ResetConnection()
		{
			if (this._fResetConnection)
			{
				this._parser.PrepareResetConnection(this.IsTransactionRoot && !this.IsNonPoolableTransactionRoot);
				base.CurrentDatabase = this._originalDatabase;
				this._currentLanguage = this._originalLanguage;
			}
		}

		// Token: 0x060013A2 RID: 5026 RVA: 0x00065868 File Offset: 0x00063A68
		internal void DecrementAsyncCount()
		{
			Interlocked.Decrement(ref this._asyncCommandCount);
		}

		// Token: 0x060013A3 RID: 5027 RVA: 0x00065876 File Offset: 0x00063A76
		internal void IncrementAsyncCount()
		{
			Interlocked.Increment(ref this._asyncCommandCount);
		}

		// Token: 0x060013A4 RID: 5028 RVA: 0x00065884 File Offset: 0x00063A84
		internal override void DisconnectTransaction(SqlInternalTransaction internalTransaction)
		{
			TdsParser parser = this.Parser;
			if (parser != null)
			{
				parser.DisconnectTransaction(internalTransaction);
			}
		}

		// Token: 0x060013A5 RID: 5029 RVA: 0x000658A2 File Offset: 0x00063AA2
		internal void ExecuteTransaction(SqlInternalConnection.TransactionRequest transactionRequest, string name, IsolationLevel iso)
		{
			this.ExecuteTransaction(transactionRequest, name, iso, null, false);
		}

		// Token: 0x060013A6 RID: 5030 RVA: 0x000658B0 File Offset: 0x00063AB0
		internal override void ExecuteTransaction(SqlInternalConnection.TransactionRequest transactionRequest, string name, IsolationLevel iso, SqlInternalTransaction internalTransaction, bool isDelegateControlRequest)
		{
			if (base.IsConnectionDoomed)
			{
				if (transactionRequest == SqlInternalConnection.TransactionRequest.Rollback || transactionRequest == SqlInternalConnection.TransactionRequest.IfRollback)
				{
					return;
				}
				throw SQL.ConnectionDoomed();
			}
			else
			{
				if ((transactionRequest == SqlInternalConnection.TransactionRequest.Commit || transactionRequest == SqlInternalConnection.TransactionRequest.Rollback || transactionRequest == SqlInternalConnection.TransactionRequest.IfRollback) && !this.Parser.MARSOn && this.Parser._physicalStateObj.BcpLock)
				{
					throw SQL.ConnectionLockedForBcpEvent();
				}
				string transactionName = (name == null) ? string.Empty : name;
				this.ExecuteTransactionYukon(transactionRequest, transactionName, iso, internalTransaction, isDelegateControlRequest);
				return;
			}
		}

		// Token: 0x060013A7 RID: 5031 RVA: 0x00065920 File Offset: 0x00063B20
		internal void ExecuteTransactionYukon(SqlInternalConnection.TransactionRequest transactionRequest, string transactionName, IsolationLevel iso, SqlInternalTransaction internalTransaction, bool isDelegateControlRequest)
		{
			TdsEnums.TransactionManagerRequestType request = TdsEnums.TransactionManagerRequestType.Begin;
			if (iso <= IsolationLevel.ReadUncommitted)
			{
				if (iso == IsolationLevel.Unspecified)
				{
					TdsEnums.TransactionManagerIsolationLevel isoLevel = TdsEnums.TransactionManagerIsolationLevel.Unspecified;
					goto IL_7E;
				}
				if (iso == IsolationLevel.Chaos)
				{
					throw SQL.NotSupportedIsolationLevel(iso);
				}
				if (iso == IsolationLevel.ReadUncommitted)
				{
					TdsEnums.TransactionManagerIsolationLevel isoLevel = TdsEnums.TransactionManagerIsolationLevel.ReadUncommitted;
					goto IL_7E;
				}
			}
			else if (iso <= IsolationLevel.RepeatableRead)
			{
				if (iso == IsolationLevel.ReadCommitted)
				{
					TdsEnums.TransactionManagerIsolationLevel isoLevel = TdsEnums.TransactionManagerIsolationLevel.ReadCommitted;
					goto IL_7E;
				}
				if (iso == IsolationLevel.RepeatableRead)
				{
					TdsEnums.TransactionManagerIsolationLevel isoLevel = TdsEnums.TransactionManagerIsolationLevel.RepeatableRead;
					goto IL_7E;
				}
			}
			else
			{
				if (iso == IsolationLevel.Serializable)
				{
					TdsEnums.TransactionManagerIsolationLevel isoLevel = TdsEnums.TransactionManagerIsolationLevel.Serializable;
					goto IL_7E;
				}
				if (iso == IsolationLevel.Snapshot)
				{
					TdsEnums.TransactionManagerIsolationLevel isoLevel = TdsEnums.TransactionManagerIsolationLevel.Snapshot;
					goto IL_7E;
				}
			}
			throw ADP.InvalidIsolationLevel(iso);
			IL_7E:
			TdsParserStateObject tdsParserStateObject = this._parser._physicalStateObj;
			TdsParser parser = this._parser;
			bool flag = false;
			bool releaseConnectionLock = false;
			if (!this.ThreadHasParserLockForClose)
			{
				this._parserLock.Wait(false);
				this.ThreadHasParserLockForClose = true;
				releaseConnectionLock = true;
			}
			try
			{
				switch (transactionRequest)
				{
				case SqlInternalConnection.TransactionRequest.Begin:
					request = TdsEnums.TransactionManagerRequestType.Begin;
					break;
				case SqlInternalConnection.TransactionRequest.Promote:
					request = TdsEnums.TransactionManagerRequestType.Promote;
					break;
				case SqlInternalConnection.TransactionRequest.Commit:
					request = TdsEnums.TransactionManagerRequestType.Commit;
					break;
				case SqlInternalConnection.TransactionRequest.Rollback:
				case SqlInternalConnection.TransactionRequest.IfRollback:
					request = TdsEnums.TransactionManagerRequestType.Rollback;
					break;
				case SqlInternalConnection.TransactionRequest.Save:
					request = TdsEnums.TransactionManagerRequestType.Save;
					break;
				}
				if ((internalTransaction != null && internalTransaction.RestoreBrokenConnection) & releaseConnectionLock)
				{
					Task task = internalTransaction.Parent.Connection.ValidateAndReconnect(delegate
					{
						this.ThreadHasParserLockForClose = false;
						this._parserLock.Release();
						releaseConnectionLock = false;
					}, 0);
					if (task != null)
					{
						AsyncHelper.WaitForCompletion(task, 0, null, true);
						internalTransaction.ConnectionHasBeenRestored = true;
						return;
					}
				}
				if (internalTransaction != null && internalTransaction.IsDelegated)
				{
					if (this._parser.MARSOn)
					{
						tdsParserStateObject = this._parser.GetSession(this);
						flag = true;
					}
					else
					{
						int openResultsCount = internalTransaction.OpenResultsCount;
					}
				}
				TdsEnums.TransactionManagerIsolationLevel isoLevel;
				this._parser.TdsExecuteTransactionManagerRequest(null, request, transactionName, isoLevel, base.ConnectionOptions.ConnectTimeout, internalTransaction, tdsParserStateObject, isDelegateControlRequest);
			}
			finally
			{
				if (flag)
				{
					parser.PutSession(tdsParserStateObject);
				}
				if (releaseConnectionLock)
				{
					this.ThreadHasParserLockForClose = false;
					this._parserLock.Release();
				}
			}
		}

		// Token: 0x060013A8 RID: 5032 RVA: 0x00065AFC File Offset: 0x00063CFC
		internal override void DelegatedTransactionEnded()
		{
			base.DelegatedTransactionEnded();
		}

		// Token: 0x060013A9 RID: 5033 RVA: 0x00065B04 File Offset: 0x00063D04
		protected override byte[] GetDTCAddress()
		{
			return this._parser.GetDTCAddress(base.ConnectionOptions.ConnectTimeout, this._parser.GetSession(this));
		}

		// Token: 0x060013AA RID: 5034 RVA: 0x00065B28 File Offset: 0x00063D28
		protected override void PropagateTransactionCookie(byte[] cookie)
		{
			this._parser.PropagateDistributedTransaction(cookie, base.ConnectionOptions.ConnectTimeout, this._parser._physicalStateObj);
		}

		// Token: 0x060013AB RID: 5035 RVA: 0x00065B4C File Offset: 0x00063D4C
		private void CompleteLogin(bool enlistOK)
		{
			this._parser.Run(RunBehavior.UntilDone, null, null, null, this._parser._physicalStateObj);
			if (this._routingInfo == null)
			{
				if (!this._sessionRecoveryAcknowledged)
				{
					this._currentSessionData = null;
					if (this._recoverySessionData != null)
					{
						throw SQL.CR_NoCRAckAtReconnection(this);
					}
				}
				if (this._currentSessionData != null && this._recoverySessionData == null)
				{
					this._currentSessionData._initialDatabase = base.CurrentDatabase;
					this._currentSessionData._initialCollation = this._currentSessionData._collation;
					this._currentSessionData._initialLanguage = this._currentLanguage;
				}
				bool flag = this._parser.EncryptionOptions == EncryptionOptions.ON;
				if (this._recoverySessionData != null && this._recoverySessionData._encrypted != flag)
				{
					throw SQL.CR_EncryptionChanged(this);
				}
				if (this._currentSessionData != null)
				{
					this._currentSessionData._encrypted = flag;
				}
				this._recoverySessionData = null;
			}
			this._parser._physicalStateObj.SniContext = SniContext.Snix_EnableMars;
			this._parser.EnableMars();
			this._fConnectionOpen = true;
			if (enlistOK && base.ConnectionOptions.Enlist)
			{
				this._parser._physicalStateObj.SniContext = SniContext.Snix_AutoEnlist;
				Transaction currentTransaction = ADP.GetCurrentTransaction();
				base.Enlist(currentTransaction);
			}
			this._parser._physicalStateObj.SniContext = SniContext.Snix_Login;
		}

		// Token: 0x060013AC RID: 5036 RVA: 0x00065C90 File Offset: 0x00063E90
		private void Login(ServerInfo server, TimeoutTimer timeout)
		{
			SqlLogin sqlLogin = new SqlLogin();
			base.CurrentDatabase = server.ResolvedDatabaseName;
			this._currentPacketSize = base.ConnectionOptions.PacketSize;
			this._currentLanguage = base.ConnectionOptions.CurrentLanguage;
			int timeout2 = 0;
			if (!timeout.IsInfinite)
			{
				long num = timeout.MillisecondsRemaining / 1000L;
				if (2147483647L > num)
				{
					timeout2 = (int)num;
				}
			}
			sqlLogin.timeout = timeout2;
			sqlLogin.userInstance = base.ConnectionOptions.UserInstance;
			sqlLogin.hostName = base.ConnectionOptions.ObtainWorkstationId();
			sqlLogin.userName = base.ConnectionOptions.UserID;
			sqlLogin.password = base.ConnectionOptions.Password;
			sqlLogin.applicationName = base.ConnectionOptions.ApplicationName;
			sqlLogin.language = this._currentLanguage;
			if (!sqlLogin.userInstance)
			{
				sqlLogin.database = base.CurrentDatabase;
				sqlLogin.attachDBFilename = base.ConnectionOptions.AttachDBFilename;
			}
			sqlLogin.serverName = server.UserServerName;
			sqlLogin.useReplication = base.ConnectionOptions.Replication;
			sqlLogin.useSSPI = base.ConnectionOptions.IntegratedSecurity;
			sqlLogin.packetSize = this._currentPacketSize;
			sqlLogin.readOnlyIntent = (base.ConnectionOptions.ApplicationIntent == ApplicationIntent.ReadOnly);
			TdsEnums.FeatureExtension featureExtension = TdsEnums.FeatureExtension.None;
			if (base.ConnectionOptions.ConnectRetryCount > 0)
			{
				featureExtension |= TdsEnums.FeatureExtension.SessionRecovery;
				this._sessionRecoveryRequested = true;
			}
			featureExtension |= TdsEnums.FeatureExtension.GlobalTransactions;
			this._parser.TdsLogin(sqlLogin, featureExtension, this._recoverySessionData);
		}

		// Token: 0x060013AD RID: 5037 RVA: 0x00065E02 File Offset: 0x00064002
		private void LoginFailure()
		{
			if (this._parser != null)
			{
				this._parser.Disconnect();
			}
		}

		// Token: 0x060013AE RID: 5038 RVA: 0x00065E18 File Offset: 0x00064018
		private void OpenLoginEnlist(TimeoutTimer timeout, SqlConnectionString connectionOptions, bool redirectedUserInstance)
		{
			ServerInfo serverInfo = new ServerInfo(connectionOptions);
			bool flag;
			string failoverPartner;
			if (this.PoolGroupProviderInfo != null)
			{
				flag = this.PoolGroupProviderInfo.UseFailoverPartner;
				failoverPartner = this.PoolGroupProviderInfo.FailoverPartner;
			}
			else
			{
				flag = false;
				failoverPartner = base.ConnectionOptions.FailoverPartner;
			}
			this._timeoutErrorInternal.SetInternalSourceType(flag ? SqlConnectionInternalSourceType.Failover : SqlConnectionInternalSourceType.Principle);
			bool flag2 = !string.IsNullOrEmpty(failoverPartner);
			try
			{
				this._timeoutErrorInternal.SetAndBeginPhase(SqlConnectionTimeoutErrorPhase.PreLoginBegin);
				if (flag2)
				{
					this._timeoutErrorInternal.SetFailoverScenario(true);
					this.LoginWithFailover(flag, serverInfo, failoverPartner, redirectedUserInstance, connectionOptions, timeout);
				}
				else
				{
					this._timeoutErrorInternal.SetFailoverScenario(false);
					this.LoginNoFailover(serverInfo, redirectedUserInstance, connectionOptions, timeout);
				}
				this._timeoutErrorInternal.EndPhase(SqlConnectionTimeoutErrorPhase.PostLogin);
			}
			catch (Exception e)
			{
				if (ADP.IsCatchableExceptionType(e))
				{
					this.LoginFailure();
				}
				throw;
			}
			this._timeoutErrorInternal.SetAllCompleteMarker();
		}

		// Token: 0x060013AF RID: 5039 RVA: 0x00065EF0 File Offset: 0x000640F0
		private bool IsDoNotRetryConnectError(SqlException exc)
		{
			return 18456 == exc.Number || 18488 == exc.Number || 1346 == exc.Number || exc._doNotReconnect;
		}

		// Token: 0x060013B0 RID: 5040 RVA: 0x00065F24 File Offset: 0x00064124
		private void LoginNoFailover(ServerInfo serverInfo, bool redirectedUserInstance, SqlConnectionString connectionOptions, TimeoutTimer timeout)
		{
			int num = 0;
			ServerInfo serverInfo2 = serverInfo;
			int num2 = 100;
			this.ResolveExtendedServerName(serverInfo, !redirectedUserInstance, connectionOptions);
			long num3 = 0L;
			if (connectionOptions.MultiSubnetFailover)
			{
				if (timeout.IsInfinite)
				{
					num3 = 1200L;
				}
				else
				{
					num3 = checked((long)(unchecked(0.08f * (float)timeout.MillisecondsRemaining)));
				}
			}
			int num4 = 0;
			TimeoutTimer timeoutTimer = null;
			for (;;)
			{
				if (connectionOptions.MultiSubnetFailover)
				{
					num4++;
					long num5 = checked(num3 * unchecked((long)num4));
					long millisecondsRemaining = timeout.MillisecondsRemaining;
					if (num5 > millisecondsRemaining)
					{
						num5 = millisecondsRemaining;
					}
					timeoutTimer = TimeoutTimer.StartMillisecondsTimeout(num5);
				}
				if (this._parser != null)
				{
					this._parser.Disconnect();
				}
				this._parser = new TdsParser(base.ConnectionOptions.MARS, base.ConnectionOptions.Asynchronous);
				try
				{
					this.AttemptOneLogin(serverInfo, !connectionOptions.MultiSubnetFailover, connectionOptions.MultiSubnetFailover ? timeoutTimer : timeout, false);
					if (connectionOptions.MultiSubnetFailover && this.ServerProvidedFailOverPartner != null)
					{
						throw SQL.MultiSubnetFailoverWithFailoverPartner(true, this);
					}
					if (this._routingInfo == null)
					{
						goto IL_261;
					}
					if (num > 0)
					{
						throw SQL.ROR_RecursiveRoutingNotSupported(this);
					}
					if (timeout.IsExpired)
					{
						throw SQL.ROR_TimeoutAfterRoutingInfo(this);
					}
					serverInfo = new ServerInfo(base.ConnectionOptions, this._routingInfo, serverInfo.ResolvedServerName);
					this._timeoutErrorInternal.SetInternalSourceType(SqlConnectionInternalSourceType.RoutingDestination);
					this._originalClientConnectionId = this._clientConnectionId;
					this._routingDestination = serverInfo.UserServerName;
					this._currentPacketSize = base.ConnectionOptions.PacketSize;
					this._currentLanguage = (this._originalLanguage = base.ConnectionOptions.CurrentLanguage);
					base.CurrentDatabase = (this._originalDatabase = base.ConnectionOptions.InitialCatalog);
					this._currentFailoverPartner = null;
					this._instanceName = string.Empty;
					num++;
					continue;
				}
				catch (SqlException exc)
				{
					if (this._parser == null || this._parser.State != TdsParserState.Closed || this.IsDoNotRetryConnectError(exc) || timeout.IsExpired)
					{
						throw;
					}
					if (timeout.MillisecondsRemaining <= (long)num2)
					{
						throw;
					}
				}
				if (this.ServerProvidedFailOverPartner != null)
				{
					break;
				}
				Thread.Sleep(num2);
				num2 = ((num2 < 500) ? (num2 * 2) : 1000);
			}
			if (connectionOptions.MultiSubnetFailover)
			{
				throw SQL.MultiSubnetFailoverWithFailoverPartner(true, this);
			}
			this._timeoutErrorInternal.ResetAndRestartPhase();
			this._timeoutErrorInternal.SetAndBeginPhase(SqlConnectionTimeoutErrorPhase.PreLoginBegin);
			this._timeoutErrorInternal.SetInternalSourceType(SqlConnectionInternalSourceType.Failover);
			this._timeoutErrorInternal.SetFailoverScenario(true);
			this.LoginWithFailover(true, serverInfo, this.ServerProvidedFailOverPartner, redirectedUserInstance, connectionOptions, timeout);
			return;
			IL_261:
			if (this.PoolGroupProviderInfo != null)
			{
				this.PoolGroupProviderInfo.FailoverCheck(this, false, connectionOptions, this.ServerProvidedFailOverPartner);
			}
			base.CurrentDataSource = serverInfo2.UserServerName;
		}

		// Token: 0x060013B1 RID: 5041 RVA: 0x000661D8 File Offset: 0x000643D8
		private void LoginWithFailover(bool useFailoverHost, ServerInfo primaryServerInfo, string failoverHost, bool redirectedUserInstance, SqlConnectionString connectionOptions, TimeoutTimer timeout)
		{
			int num = 100;
			ServerInfo serverInfo = new ServerInfo(connectionOptions, failoverHost);
			this.ResolveExtendedServerName(primaryServerInfo, !redirectedUserInstance, connectionOptions);
			if (this.ServerProvidedFailOverPartner == null)
			{
				this.ResolveExtendedServerName(serverInfo, !redirectedUserInstance && failoverHost != primaryServerInfo.UserServerName, connectionOptions);
			}
			long num2;
			int num3;
			checked
			{
				if (timeout.IsInfinite)
				{
					num2 = (long)(unchecked(0.08f * (float)ADP.TimerFromSeconds(15)));
				}
				else
				{
					num2 = (long)(unchecked(0.08f * (float)timeout.MillisecondsRemaining));
				}
				num3 = 0;
			}
			for (;;)
			{
				long num4 = checked(num2 * unchecked((long)(checked(num3 / 2 + 1))));
				long millisecondsRemaining = timeout.MillisecondsRemaining;
				if (num4 > millisecondsRemaining)
				{
					num4 = millisecondsRemaining;
				}
				TimeoutTimer timeout2 = TimeoutTimer.StartMillisecondsTimeout(num4);
				if (this._parser != null)
				{
					this._parser.Disconnect();
				}
				this._parser = new TdsParser(base.ConnectionOptions.MARS, base.ConnectionOptions.Asynchronous);
				ServerInfo serverInfo2;
				if (useFailoverHost)
				{
					if (this.ServerProvidedFailOverPartner != null && serverInfo.ResolvedServerName != this.ServerProvidedFailOverPartner)
					{
						serverInfo.SetDerivedNames(string.Empty, this.ServerProvidedFailOverPartner);
					}
					serverInfo2 = serverInfo;
					this._timeoutErrorInternal.SetInternalSourceType(SqlConnectionInternalSourceType.Failover);
				}
				else
				{
					serverInfo2 = primaryServerInfo;
					this._timeoutErrorInternal.SetInternalSourceType(SqlConnectionInternalSourceType.Principle);
				}
				try
				{
					this.AttemptOneLogin(serverInfo2, false, timeout2, true);
					if (this._routingInfo != null)
					{
						throw SQL.ROR_UnexpectedRoutingInfo(this);
					}
					break;
				}
				catch (SqlException exc)
				{
					if (this.IsDoNotRetryConnectError(exc) || timeout.IsExpired)
					{
						throw;
					}
					if (base.IsConnectionDoomed)
					{
						throw;
					}
					if (1 == num3 % 2 && timeout.MillisecondsRemaining <= (long)num)
					{
						throw;
					}
				}
				if (1 == num3 % 2)
				{
					Thread.Sleep(num);
					num = ((num < 500) ? (num * 2) : 1000);
				}
				num3++;
				useFailoverHost = !useFailoverHost;
			}
			if (useFailoverHost && this.ServerProvidedFailOverPartner == null)
			{
				throw SQL.InvalidPartnerConfiguration(failoverHost, base.CurrentDatabase);
			}
			if (this.PoolGroupProviderInfo != null)
			{
				this.PoolGroupProviderInfo.FailoverCheck(this, useFailoverHost, connectionOptions, this.ServerProvidedFailOverPartner);
			}
			base.CurrentDataSource = (useFailoverHost ? failoverHost : primaryServerInfo.UserServerName);
		}

		// Token: 0x060013B2 RID: 5042 RVA: 0x000663D4 File Offset: 0x000645D4
		private void ResolveExtendedServerName(ServerInfo serverInfo, bool aliasLookup, SqlConnectionString options)
		{
			if (serverInfo.ExtendedServerName == null)
			{
				string userServerName = serverInfo.UserServerName;
				string userProtocol = serverInfo.UserProtocol;
				serverInfo.SetDerivedNames(userProtocol, userServerName);
			}
		}

		// Token: 0x060013B3 RID: 5043 RVA: 0x00066400 File Offset: 0x00064600
		private void AttemptOneLogin(ServerInfo serverInfo, bool ignoreSniOpenTimeout, TimeoutTimer timeout, bool withFailover = false)
		{
			this._routingInfo = null;
			this._parser._physicalStateObj.SniContext = SniContext.Snix_Connect;
			this._parser.Connect(serverInfo, this, ignoreSniOpenTimeout, timeout.LegacyTimerExpire, base.ConnectionOptions.Encrypt, base.ConnectionOptions.TrustServerCertificate, base.ConnectionOptions.IntegratedSecurity, withFailover);
			this._timeoutErrorInternal.EndPhase(SqlConnectionTimeoutErrorPhase.ConsumePreLoginHandshake);
			this._timeoutErrorInternal.SetAndBeginPhase(SqlConnectionTimeoutErrorPhase.LoginBegin);
			this._parser._physicalStateObj.SniContext = SniContext.Snix_Login;
			this.Login(serverInfo, timeout);
			this._timeoutErrorInternal.EndPhase(SqlConnectionTimeoutErrorPhase.ProcessConnectionAuth);
			this._timeoutErrorInternal.SetAndBeginPhase(SqlConnectionTimeoutErrorPhase.PostLogin);
			this.CompleteLogin(!base.ConnectionOptions.Pooling);
			this._timeoutErrorInternal.EndPhase(SqlConnectionTimeoutErrorPhase.PostLogin);
		}

		// Token: 0x060013B4 RID: 5044 RVA: 0x000664C5 File Offset: 0x000646C5
		protected override object ObtainAdditionalLocksForClose()
		{
			bool flag = !this.ThreadHasParserLockForClose;
			if (flag)
			{
				this._parserLock.Wait(false);
				this.ThreadHasParserLockForClose = true;
			}
			return flag;
		}

		// Token: 0x060013B5 RID: 5045 RVA: 0x000664EB File Offset: 0x000646EB
		protected override void ReleaseAdditionalLocksForClose(object lockToken)
		{
			if ((bool)lockToken)
			{
				this.ThreadHasParserLockForClose = false;
				this._parserLock.Release();
			}
		}

		// Token: 0x060013B6 RID: 5046 RVA: 0x00066508 File Offset: 0x00064708
		internal bool GetSessionAndReconnectIfNeeded(SqlConnection parent, int timeout = 0)
		{
			if (this.ThreadHasParserLockForClose)
			{
				return false;
			}
			this._parserLock.Wait(false);
			this.ThreadHasParserLockForClose = true;
			bool releaseConnectionLock = true;
			bool result;
			try
			{
				Task task = parent.ValidateAndReconnect(delegate
				{
					this.ThreadHasParserLockForClose = false;
					this._parserLock.Release();
					releaseConnectionLock = false;
				}, timeout);
				if (task != null)
				{
					AsyncHelper.WaitForCompletion(task, timeout, null, true);
					result = true;
				}
				else
				{
					result = false;
				}
			}
			finally
			{
				if (releaseConnectionLock)
				{
					this.ThreadHasParserLockForClose = false;
					this._parserLock.Release();
				}
			}
			return result;
		}

		// Token: 0x060013B7 RID: 5047 RVA: 0x0006659C File Offset: 0x0006479C
		internal void BreakConnection()
		{
			SqlConnection connection = base.Connection;
			base.DoomThisConnection();
			if (connection != null)
			{
				connection.Close();
			}
		}

		// Token: 0x170003BE RID: 958
		// (get) Token: 0x060013B8 RID: 5048 RVA: 0x000665BF File Offset: 0x000647BF
		internal bool IgnoreEnvChange
		{
			get
			{
				return this._routingInfo != null;
			}
		}

		// Token: 0x060013B9 RID: 5049 RVA: 0x000665CC File Offset: 0x000647CC
		internal void OnEnvChange(SqlEnvChange rec)
		{
			switch (rec.type)
			{
			case 1:
				if (!this._fConnectionOpen && this._recoverySessionData == null)
				{
					this._originalDatabase = rec.newValue;
				}
				base.CurrentDatabase = rec.newValue;
				return;
			case 2:
				if (!this._fConnectionOpen && this._recoverySessionData == null)
				{
					this._originalLanguage = rec.newValue;
				}
				this._currentLanguage = rec.newValue;
				return;
			case 3:
			case 5:
			case 6:
			case 8:
			case 9:
			case 10:
			case 11:
			case 12:
			case 14:
			case 16:
			case 17:
				break;
			case 4:
				this._currentPacketSize = int.Parse(rec.newValue, CultureInfo.InvariantCulture);
				return;
			case 7:
				if (this._currentSessionData != null)
				{
					this._currentSessionData._collation = rec.newCollation;
					return;
				}
				break;
			case 13:
				if (base.ConnectionOptions.ApplicationIntent == ApplicationIntent.ReadOnly)
				{
					throw SQL.ROR_FailoverNotSupportedServer(this);
				}
				this._currentFailoverPartner = rec.newValue;
				return;
			case 15:
				base.PromotedDTCToken = rec.newBinValue;
				return;
			case 18:
				if (this._currentSessionData != null)
				{
					this._currentSessionData.Reset();
					return;
				}
				break;
			case 19:
				this._instanceName = rec.newValue;
				return;
			case 20:
				if (string.IsNullOrEmpty(rec.newRoutingInfo.ServerName) || rec.newRoutingInfo.Protocol != 0 || rec.newRoutingInfo.Port == 0)
				{
					throw SQL.ROR_InvalidRoutingInfo(this);
				}
				this._routingInfo = rec.newRoutingInfo;
				break;
			default:
				return;
			}
		}

		// Token: 0x060013BA RID: 5050 RVA: 0x00066750 File Offset: 0x00064950
		internal void OnLoginAck(SqlLoginAck rec)
		{
			this._loginAck = rec;
			if (this._recoverySessionData != null && this._recoverySessionData._tdsVersion != rec.tdsVersion)
			{
				throw SQL.CR_TDSVersionNotPreserved(this);
			}
			if (this._currentSessionData != null)
			{
				this._currentSessionData._tdsVersion = rec.tdsVersion;
			}
		}

		// Token: 0x060013BB RID: 5051 RVA: 0x000667A0 File Offset: 0x000649A0
		internal void OnFeatureExtAck(int featureId, byte[] data)
		{
			if (this._routingInfo != null)
			{
				return;
			}
			if (featureId != 1)
			{
				if (featureId != 5)
				{
					throw SQL.ParsingError();
				}
				if (data.Length < 1)
				{
					throw SQL.ParsingError();
				}
				base.IsGlobalTransaction = true;
				if (1 == data[0])
				{
					base.IsGlobalTransactionsEnabledForServer = true;
					return;
				}
				return;
			}
			else
			{
				if (!this._sessionRecoveryRequested)
				{
					throw SQL.ParsingError();
				}
				this._sessionRecoveryAcknowledged = true;
				int i = 0;
				while (i < data.Length)
				{
					byte b = data[i];
					i++;
					byte b2 = data[i];
					i++;
					int num;
					if (b2 == 255)
					{
						num = BitConverter.ToInt32(data, i);
						i += 4;
					}
					else
					{
						num = (int)b2;
					}
					byte[] array = new byte[num];
					Buffer.BlockCopy(data, i, array, 0, num);
					i += num;
					if (this._recoverySessionData == null)
					{
						this._currentSessionData._initialState[(int)b] = array;
					}
					else
					{
						this._currentSessionData._delta[(int)b] = new SessionStateRecord
						{
							_data = array,
							_dataLength = num,
							_recoverable = true,
							_version = 0U
						};
						this._currentSessionData._deltaDirty = true;
					}
				}
				return;
			}
		}

		// Token: 0x170003BF RID: 959
		// (get) Token: 0x060013BC RID: 5052 RVA: 0x000668A6 File Offset: 0x00064AA6
		// (set) Token: 0x060013BD RID: 5053 RVA: 0x000668BA File Offset: 0x00064ABA
		internal bool ThreadHasParserLockForClose
		{
			get
			{
				return this._threadIdOwningParserLock == Thread.CurrentThread.ManagedThreadId;
			}
			set
			{
				if (value)
				{
					this._threadIdOwningParserLock = Thread.CurrentThread.ManagedThreadId;
					return;
				}
				if (this._threadIdOwningParserLock == Thread.CurrentThread.ManagedThreadId)
				{
					this._threadIdOwningParserLock = -1;
				}
			}
		}

		// Token: 0x060013BE RID: 5054 RVA: 0x000668E9 File Offset: 0x00064AE9
		internal override bool TryReplaceConnection(DbConnection outerConnection, DbConnectionFactory connectionFactory, TaskCompletionSource<DbConnectionInternal> retry, DbConnectionOptions userOptions)
		{
			return base.TryOpenConnectionInternal(outerConnection, connectionFactory, retry, userOptions);
		}

		// Token: 0x060013BF RID: 5055 RVA: 0x000668F8 File Offset: 0x00064AF8
		// Note: this type is marked as 'beforefieldinit'.
		static SqlInternalConnectionTds()
		{
		}

		// Token: 0x04000DEF RID: 3567
		private readonly SqlConnectionPoolGroupProviderInfo _poolGroupProviderInfo;

		// Token: 0x04000DF0 RID: 3568
		private TdsParser _parser;

		// Token: 0x04000DF1 RID: 3569
		private SqlLoginAck _loginAck;

		// Token: 0x04000DF2 RID: 3570
		private bool _sessionRecoveryRequested;

		// Token: 0x04000DF3 RID: 3571
		internal bool _sessionRecoveryAcknowledged;

		// Token: 0x04000DF4 RID: 3572
		internal SessionData _currentSessionData;

		// Token: 0x04000DF5 RID: 3573
		private SessionData _recoverySessionData;

		// Token: 0x04000DF6 RID: 3574
		private static readonly HashSet<int> s_transientErrors = new HashSet<int>
		{
			4060,
			10928,
			10929,
			40197,
			40501,
			40613
		};

		// Token: 0x04000DF7 RID: 3575
		private bool _fConnectionOpen;

		// Token: 0x04000DF8 RID: 3576
		private bool _fResetConnection;

		// Token: 0x04000DF9 RID: 3577
		private string _originalDatabase;

		// Token: 0x04000DFA RID: 3578
		private string _currentFailoverPartner;

		// Token: 0x04000DFB RID: 3579
		private string _originalLanguage;

		// Token: 0x04000DFC RID: 3580
		private string _currentLanguage;

		// Token: 0x04000DFD RID: 3581
		private int _currentPacketSize;

		// Token: 0x04000DFE RID: 3582
		private int _asyncCommandCount;

		// Token: 0x04000DFF RID: 3583
		private string _instanceName = string.Empty;

		// Token: 0x04000E00 RID: 3584
		private DbConnectionPoolIdentity _identity;

		// Token: 0x04000E01 RID: 3585
		internal SqlInternalConnectionTds.SyncAsyncLock _parserLock = new SqlInternalConnectionTds.SyncAsyncLock();

		// Token: 0x04000E02 RID: 3586
		private int _threadIdOwningParserLock = -1;

		// Token: 0x04000E03 RID: 3587
		private SqlConnectionTimeoutErrorInternal _timeoutErrorInternal;

		// Token: 0x04000E04 RID: 3588
		internal Guid _clientConnectionId = Guid.Empty;

		// Token: 0x04000E05 RID: 3589
		private RoutingInfo _routingInfo;

		// Token: 0x04000E06 RID: 3590
		private Guid _originalClientConnectionId = Guid.Empty;

		// Token: 0x04000E07 RID: 3591
		private string _routingDestination;

		// Token: 0x020001AB RID: 427
		internal class SyncAsyncLock
		{
			// Token: 0x060013C0 RID: 5056 RVA: 0x00066958 File Offset: 0x00064B58
			internal void Wait(bool canReleaseFromAnyThread)
			{
				Monitor.Enter(this._semaphore);
				if (canReleaseFromAnyThread || this._semaphore.CurrentCount == 0)
				{
					this._semaphore.Wait();
					if (canReleaseFromAnyThread)
					{
						Monitor.Exit(this._semaphore);
						return;
					}
					this._semaphore.Release();
				}
			}

			// Token: 0x060013C1 RID: 5057 RVA: 0x000669A8 File Offset: 0x00064BA8
			internal void Wait(bool canReleaseFromAnyThread, int timeout, ref bool lockTaken)
			{
				lockTaken = false;
				bool flag = false;
				try
				{
					Monitor.TryEnter(this._semaphore, timeout, ref flag);
					if (flag)
					{
						if (canReleaseFromAnyThread || this._semaphore.CurrentCount == 0)
						{
							if (this._semaphore.Wait(timeout))
							{
								if (canReleaseFromAnyThread)
								{
									Monitor.Exit(this._semaphore);
									flag = false;
								}
								else
								{
									this._semaphore.Release();
								}
								lockTaken = true;
							}
						}
						else
						{
							lockTaken = true;
						}
					}
				}
				finally
				{
					if (!lockTaken && flag)
					{
						Monitor.Exit(this._semaphore);
					}
				}
			}

			// Token: 0x060013C2 RID: 5058 RVA: 0x00066A38 File Offset: 0x00064C38
			internal void Release()
			{
				if (this._semaphore.CurrentCount == 0)
				{
					this._semaphore.Release();
					return;
				}
				Monitor.Exit(this._semaphore);
			}

			// Token: 0x170003C0 RID: 960
			// (get) Token: 0x060013C3 RID: 5059 RVA: 0x00066A5F File Offset: 0x00064C5F
			internal bool CanBeReleasedFromAnyThread
			{
				get
				{
					return this._semaphore.CurrentCount == 0;
				}
			}

			// Token: 0x060013C4 RID: 5060 RVA: 0x00066A6F File Offset: 0x00064C6F
			internal bool ThreadMayHaveLock()
			{
				return Monitor.IsEntered(this._semaphore) || this._semaphore.CurrentCount == 0;
			}

			// Token: 0x060013C5 RID: 5061 RVA: 0x00066A8E File Offset: 0x00064C8E
			public SyncAsyncLock()
			{
			}

			// Token: 0x04000E08 RID: 3592
			private SemaphoreSlim _semaphore = new SemaphoreSlim(1);
		}

		// Token: 0x020001AC RID: 428
		[CompilerGenerated]
		private sealed class <>c__DisplayClass81_0
		{
			// Token: 0x060013C6 RID: 5062 RVA: 0x00005C14 File Offset: 0x00003E14
			public <>c__DisplayClass81_0()
			{
			}

			// Token: 0x060013C7 RID: 5063 RVA: 0x00066AA2 File Offset: 0x00064CA2
			internal void <ExecuteTransactionYukon>b__0()
			{
				this.<>4__this.ThreadHasParserLockForClose = false;
				this.<>4__this._parserLock.Release();
				this.releaseConnectionLock = false;
			}

			// Token: 0x04000E09 RID: 3593
			public SqlInternalConnectionTds <>4__this;

			// Token: 0x04000E0A RID: 3594
			public bool releaseConnectionLock;
		}

		// Token: 0x020001AD RID: 429
		[CompilerGenerated]
		private sealed class <>c__DisplayClass96_0
		{
			// Token: 0x060013C8 RID: 5064 RVA: 0x00005C14 File Offset: 0x00003E14
			public <>c__DisplayClass96_0()
			{
			}

			// Token: 0x060013C9 RID: 5065 RVA: 0x00066AC7 File Offset: 0x00064CC7
			internal void <GetSessionAndReconnectIfNeeded>b__0()
			{
				this.<>4__this.ThreadHasParserLockForClose = false;
				this.<>4__this._parserLock.Release();
				this.releaseConnectionLock = false;
			}

			// Token: 0x04000E0B RID: 3595
			public SqlInternalConnectionTds <>4__this;

			// Token: 0x04000E0C RID: 3596
			public bool releaseConnectionLock;
		}
	}
}
