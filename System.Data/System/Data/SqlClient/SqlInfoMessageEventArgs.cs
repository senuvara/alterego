﻿using System;
using Unity;

namespace System.Data.SqlClient
{
	/// <summary>Provides data for the <see cref="E:System.Data.SqlClient.SqlConnection.InfoMessage" /> event.</summary>
	// Token: 0x020001A4 RID: 420
	public sealed class SqlInfoMessageEventArgs : EventArgs
	{
		// Token: 0x06001344 RID: 4932 RVA: 0x00064BA4 File Offset: 0x00062DA4
		internal SqlInfoMessageEventArgs(SqlException exception)
		{
			this._exception = exception;
		}

		/// <summary>Gets the collection of warnings sent from the server.</summary>
		/// <returns>The collection of warnings sent from the server.</returns>
		// Token: 0x17000395 RID: 917
		// (get) Token: 0x06001345 RID: 4933 RVA: 0x00064BB3 File Offset: 0x00062DB3
		public SqlErrorCollection Errors
		{
			get
			{
				return this._exception.Errors;
			}
		}

		// Token: 0x06001346 RID: 4934 RVA: 0x00064BC0 File Offset: 0x00062DC0
		private bool ShouldSerializeErrors()
		{
			return this._exception != null && 0 < this._exception.Errors.Count;
		}

		/// <summary>Gets the full text of the error sent from the database.</summary>
		/// <returns>The full text of the error.</returns>
		// Token: 0x17000396 RID: 918
		// (get) Token: 0x06001347 RID: 4935 RVA: 0x00064BDF File Offset: 0x00062DDF
		public string Message
		{
			get
			{
				return this._exception.Message;
			}
		}

		/// <summary>Gets the name of the object that generated the error.</summary>
		/// <returns>The name of the object that generated the error.</returns>
		// Token: 0x17000397 RID: 919
		// (get) Token: 0x06001348 RID: 4936 RVA: 0x00064BEC File Offset: 0x00062DEC
		public string Source
		{
			get
			{
				return this._exception.Source;
			}
		}

		/// <summary>Retrieves a string representation of the <see cref="E:System.Data.SqlClient.SqlConnection.InfoMessage" /> event.</summary>
		/// <returns>A string representing the <see cref="E:System.Data.SqlClient.SqlConnection.InfoMessage" /> event.</returns>
		// Token: 0x06001349 RID: 4937 RVA: 0x00064BF9 File Offset: 0x00062DF9
		public override string ToString()
		{
			return this.Message;
		}

		// Token: 0x0600134A RID: 4938 RVA: 0x00010458 File Offset: 0x0000E658
		internal SqlInfoMessageEventArgs()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x04000DCB RID: 3531
		private SqlException _exception;
	}
}
