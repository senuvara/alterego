﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Text;
using System.Text.RegularExpressions;

namespace System.Data.SqlClient
{
	// Token: 0x02000166 RID: 358
	internal sealed class SqlCommandSet
	{
		// Token: 0x0600107C RID: 4220 RVA: 0x00056346 File Offset: 0x00054546
		internal SqlCommandSet()
		{
			this._batchCommand = new SqlCommand();
		}

		// Token: 0x170002D3 RID: 723
		// (get) Token: 0x0600107D RID: 4221 RVA: 0x00056364 File Offset: 0x00054564
		private SqlCommand BatchCommand
		{
			get
			{
				SqlCommand batchCommand = this._batchCommand;
				if (batchCommand == null)
				{
					throw ADP.ObjectDisposed(this);
				}
				return batchCommand;
			}
		}

		// Token: 0x170002D4 RID: 724
		// (get) Token: 0x0600107E RID: 4222 RVA: 0x00056383 File Offset: 0x00054583
		internal int CommandCount
		{
			get
			{
				return this.CommandList.Count;
			}
		}

		// Token: 0x170002D5 RID: 725
		// (get) Token: 0x0600107F RID: 4223 RVA: 0x00056390 File Offset: 0x00054590
		private List<SqlCommandSet.LocalCommand> CommandList
		{
			get
			{
				List<SqlCommandSet.LocalCommand> commandList = this._commandList;
				if (commandList == null)
				{
					throw ADP.ObjectDisposed(this);
				}
				return commandList;
			}
		}

		// Token: 0x170002D6 RID: 726
		// (set) Token: 0x06001080 RID: 4224 RVA: 0x000563AF File Offset: 0x000545AF
		internal int CommandTimeout
		{
			set
			{
				this.BatchCommand.CommandTimeout = value;
			}
		}

		// Token: 0x170002D7 RID: 727
		// (get) Token: 0x06001081 RID: 4225 RVA: 0x000563BD File Offset: 0x000545BD
		// (set) Token: 0x06001082 RID: 4226 RVA: 0x000563CA File Offset: 0x000545CA
		internal SqlConnection Connection
		{
			get
			{
				return this.BatchCommand.Connection;
			}
			set
			{
				this.BatchCommand.Connection = value;
			}
		}

		// Token: 0x170002D8 RID: 728
		// (set) Token: 0x06001083 RID: 4227 RVA: 0x000563D8 File Offset: 0x000545D8
		internal SqlTransaction Transaction
		{
			set
			{
				this.BatchCommand.Transaction = value;
			}
		}

		// Token: 0x06001084 RID: 4228 RVA: 0x000563E8 File Offset: 0x000545E8
		internal void Append(SqlCommand command)
		{
			ADP.CheckArgumentNull(command, "command");
			string commandText = command.CommandText;
			if (string.IsNullOrEmpty(commandText))
			{
				throw ADP.CommandTextRequired("Append");
			}
			CommandType commandType = command.CommandType;
			if (commandType == CommandType.Text || commandType == CommandType.StoredProcedure)
			{
				SqlParameterCollection sqlParameterCollection = null;
				SqlParameterCollection parameters = command.Parameters;
				if (0 < parameters.Count)
				{
					sqlParameterCollection = new SqlParameterCollection();
					for (int i = 0; i < parameters.Count; i++)
					{
						SqlParameter sqlParameter = new SqlParameter();
						parameters[i].CopyTo(sqlParameter);
						sqlParameterCollection.Add(sqlParameter);
						if (!SqlCommandSet.s_sqlIdentifierParser.IsMatch(sqlParameter.ParameterName))
						{
							throw ADP.BadParameterName(sqlParameter.ParameterName);
						}
					}
					foreach (object obj in sqlParameterCollection)
					{
						SqlParameter sqlParameter2 = (SqlParameter)obj;
						object value = sqlParameter2.Value;
						byte[] array = value as byte[];
						if (array != null)
						{
							int offset = sqlParameter2.Offset;
							int size = sqlParameter2.Size;
							int num = array.Length - offset;
							if (size != 0 && size < num)
							{
								num = size;
							}
							byte[] array2 = new byte[Math.Max(num, 0)];
							Buffer.BlockCopy(array, offset, array2, 0, array2.Length);
							sqlParameter2.Offset = 0;
							sqlParameter2.Value = array2;
						}
						else
						{
							char[] array3 = value as char[];
							if (array3 != null)
							{
								int offset2 = sqlParameter2.Offset;
								int size2 = sqlParameter2.Size;
								int num2 = array3.Length - offset2;
								if (size2 != 0 && size2 < num2)
								{
									num2 = size2;
								}
								char[] array4 = new char[Math.Max(num2, 0)];
								Buffer.BlockCopy(array3, offset2, array4, 0, array4.Length * 2);
								sqlParameter2.Offset = 0;
								sqlParameter2.Value = array4;
							}
							else
							{
								ICloneable cloneable = value as ICloneable;
								if (cloneable != null)
								{
									sqlParameter2.Value = cloneable.Clone();
								}
							}
						}
					}
				}
				int returnParameterIndex = -1;
				if (sqlParameterCollection != null)
				{
					for (int j = 0; j < sqlParameterCollection.Count; j++)
					{
						if (ParameterDirection.ReturnValue == sqlParameterCollection[j].Direction)
						{
							returnParameterIndex = j;
							break;
						}
					}
				}
				SqlCommandSet.LocalCommand item = new SqlCommandSet.LocalCommand(commandText, sqlParameterCollection, returnParameterIndex, command.CommandType);
				this.CommandList.Add(item);
				return;
			}
			if (commandType == CommandType.TableDirect)
			{
				throw SQL.NotSupportedCommandType(commandType);
			}
			throw ADP.InvalidCommandType(commandType);
		}

		// Token: 0x06001085 RID: 4229 RVA: 0x00056650 File Offset: 0x00054850
		internal static void BuildStoredProcedureName(StringBuilder builder, string part)
		{
			if (part != null && 0 < part.Length)
			{
				if ('[' == part[0])
				{
					int num = 0;
					foreach (char c in part)
					{
						if (']' == c)
						{
							num++;
						}
					}
					if (1 == num % 2)
					{
						builder.Append(part);
						return;
					}
				}
				SqlServerEscapeHelper.EscapeIdentifier(builder, part);
			}
		}

		// Token: 0x06001086 RID: 4230 RVA: 0x000566B0 File Offset: 0x000548B0
		internal void Clear()
		{
			DbCommand batchCommand = this.BatchCommand;
			if (batchCommand != null)
			{
				batchCommand.Parameters.Clear();
				batchCommand.CommandText = null;
			}
			List<SqlCommandSet.LocalCommand> commandList = this._commandList;
			if (commandList != null)
			{
				commandList.Clear();
			}
		}

		// Token: 0x06001087 RID: 4231 RVA: 0x000566EC File Offset: 0x000548EC
		internal void Dispose()
		{
			SqlCommand batchCommand = this._batchCommand;
			this._commandList = null;
			this._batchCommand = null;
			if (batchCommand != null)
			{
				batchCommand.Dispose();
			}
		}

		// Token: 0x06001088 RID: 4232 RVA: 0x00056718 File Offset: 0x00054918
		internal int ExecuteNonQuery()
		{
			this.ValidateCommandBehavior("ExecuteNonQuery", CommandBehavior.Default);
			this.BatchCommand.BatchRPCMode = true;
			this.BatchCommand.ClearBatchCommand();
			this.BatchCommand.Parameters.Clear();
			for (int i = 0; i < this._commandList.Count; i++)
			{
				SqlCommandSet.LocalCommand localCommand = this._commandList[i];
				this.BatchCommand.AddBatchCommand(localCommand.CommandText, localCommand.Parameters, localCommand.CmdType);
			}
			return this.BatchCommand.ExecuteBatchRPCCommand();
		}

		// Token: 0x06001089 RID: 4233 RVA: 0x000567A3 File Offset: 0x000549A3
		internal SqlParameter GetParameter(int commandIndex, int parameterIndex)
		{
			return this.CommandList[commandIndex].Parameters[parameterIndex];
		}

		// Token: 0x0600108A RID: 4234 RVA: 0x000567BC File Offset: 0x000549BC
		internal bool GetBatchedAffected(int commandIdentifier, out int recordsAffected, out Exception error)
		{
			error = this.BatchCommand.GetErrors(commandIdentifier);
			int? recordsAffected2 = this.BatchCommand.GetRecordsAffected(commandIdentifier);
			recordsAffected = recordsAffected2.GetValueOrDefault();
			return recordsAffected2 != null;
		}

		// Token: 0x0600108B RID: 4235 RVA: 0x000567F4 File Offset: 0x000549F4
		internal int GetParameterCount(int commandIndex)
		{
			return this.CommandList[commandIndex].Parameters.Count;
		}

		// Token: 0x0600108C RID: 4236 RVA: 0x0005680C File Offset: 0x00054A0C
		private void ValidateCommandBehavior(string method, CommandBehavior behavior)
		{
			if ((behavior & ~(CommandBehavior.SequentialAccess | CommandBehavior.CloseConnection)) != CommandBehavior.Default)
			{
				ADP.ValidateCommandBehavior(behavior);
				throw ADP.NotSupportedCommandBehavior(behavior & ~(CommandBehavior.SequentialAccess | CommandBehavior.CloseConnection), method);
			}
		}

		// Token: 0x0600108D RID: 4237 RVA: 0x00056825 File Offset: 0x00054A25
		// Note: this type is marked as 'beforefieldinit'.
		static SqlCommandSet()
		{
		}

		// Token: 0x04000B9E RID: 2974
		private const string SqlIdentifierPattern = "^@[\\p{Lo}\\p{Lu}\\p{Ll}\\p{Lm}_@#][\\p{Lo}\\p{Lu}\\p{Ll}\\p{Lm}\\p{Nd}＿_@#\\$]*$";

		// Token: 0x04000B9F RID: 2975
		private static readonly Regex s_sqlIdentifierParser = new Regex("^@[\\p{Lo}\\p{Lu}\\p{Ll}\\p{Lm}_@#][\\p{Lo}\\p{Lu}\\p{Ll}\\p{Lm}\\p{Nd}＿_@#\\$]*$", RegexOptions.ExplicitCapture | RegexOptions.Singleline);

		// Token: 0x04000BA0 RID: 2976
		private List<SqlCommandSet.LocalCommand> _commandList = new List<SqlCommandSet.LocalCommand>();

		// Token: 0x04000BA1 RID: 2977
		private SqlCommand _batchCommand;

		// Token: 0x02000167 RID: 359
		private sealed class LocalCommand
		{
			// Token: 0x0600108E RID: 4238 RVA: 0x00056838 File Offset: 0x00054A38
			internal LocalCommand(string commandText, SqlParameterCollection parameters, int returnParameterIndex, CommandType cmdType)
			{
				this.CommandText = commandText;
				this.Parameters = parameters;
				this.ReturnParameterIndex = returnParameterIndex;
				this.CmdType = cmdType;
			}

			// Token: 0x04000BA2 RID: 2978
			internal readonly string CommandText;

			// Token: 0x04000BA3 RID: 2979
			internal readonly SqlParameterCollection Parameters;

			// Token: 0x04000BA4 RID: 2980
			internal readonly int ReturnParameterIndex;

			// Token: 0x04000BA5 RID: 2981
			internal readonly CommandType CmdType;
		}
	}
}
