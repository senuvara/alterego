﻿using System;

namespace System.Data.SqlClient
{
	// Token: 0x020001FE RID: 510
	internal sealed class SqlLogin
	{
		// Token: 0x060016D1 RID: 5841 RVA: 0x00077CC0 File Offset: 0x00075EC0
		public SqlLogin()
		{
		}

		// Token: 0x0400110C RID: 4364
		internal int timeout;

		// Token: 0x0400110D RID: 4365
		internal bool userInstance;

		// Token: 0x0400110E RID: 4366
		internal string hostName = "";

		// Token: 0x0400110F RID: 4367
		internal string userName = "";

		// Token: 0x04001110 RID: 4368
		internal string password = "";

		// Token: 0x04001111 RID: 4369
		internal string applicationName = "";

		// Token: 0x04001112 RID: 4370
		internal string serverName = "";

		// Token: 0x04001113 RID: 4371
		internal string language = "";

		// Token: 0x04001114 RID: 4372
		internal string database = "";

		// Token: 0x04001115 RID: 4373
		internal string attachDBFilename = "";

		// Token: 0x04001116 RID: 4374
		internal bool useReplication;

		// Token: 0x04001117 RID: 4375
		internal bool useSSPI;

		// Token: 0x04001118 RID: 4376
		internal int packetSize = 8000;

		// Token: 0x04001119 RID: 4377
		internal bool readOnlyIntent;
	}
}
