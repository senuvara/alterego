﻿using System;
using System.Data.Common;
using System.Data.SqlTypes;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Transactions;

namespace System.Data.SqlClient
{
	// Token: 0x020001D6 RID: 470
	internal static class SQL
	{
		// Token: 0x06001532 RID: 5426 RVA: 0x0006BC7F File Offset: 0x00069E7F
		internal static Exception CannotGetDTCAddress()
		{
			return ADP.InvalidOperation(SR.GetString("Unable to get the address of the distributed transaction coordinator for the server, from the server.  Is DTC enabled on the server?"));
		}

		// Token: 0x06001533 RID: 5427 RVA: 0x0006BC90 File Offset: 0x00069E90
		internal static Exception InvalidInternalPacketSize(string str)
		{
			return ADP.ArgumentOutOfRange(str);
		}

		// Token: 0x06001534 RID: 5428 RVA: 0x0006BC98 File Offset: 0x00069E98
		internal static Exception InvalidPacketSize()
		{
			return ADP.ArgumentOutOfRange(SR.GetString("Invalid Packet Size."));
		}

		// Token: 0x06001535 RID: 5429 RVA: 0x0006BCA9 File Offset: 0x00069EA9
		internal static Exception InvalidPacketSizeValue()
		{
			return ADP.Argument(SR.GetString("Invalid 'Packet Size'.  The value must be an integer >= 512 and <= 32768."));
		}

		// Token: 0x06001536 RID: 5430 RVA: 0x0006BCBA File Offset: 0x00069EBA
		internal static Exception InvalidSSPIPacketSize()
		{
			return ADP.Argument(SR.GetString("Invalid SSPI packet size."));
		}

		// Token: 0x06001537 RID: 5431 RVA: 0x0006BCCB File Offset: 0x00069ECB
		internal static Exception NullEmptyTransactionName()
		{
			return ADP.Argument(SR.GetString("Invalid transaction or invalid name for a point at which to save within the transaction."));
		}

		// Token: 0x06001538 RID: 5432 RVA: 0x0006BCDC File Offset: 0x00069EDC
		internal static Exception UserInstanceFailoverNotCompatible()
		{
			return ADP.Argument(SR.GetString("User Instance and Failover are not compatible options.  Please choose only one of the two in the connection string."));
		}

		// Token: 0x06001539 RID: 5433 RVA: 0x0006BCED File Offset: 0x00069EED
		internal static Exception InvalidSQLServerVersionUnknown()
		{
			return ADP.DataAdapter(SR.GetString("Unsupported SQL Server version.  The .Net Framework SqlClient Data Provider can only be used with SQL Server versions 7.0 and later."));
		}

		// Token: 0x0600153A RID: 5434 RVA: 0x0006BCFE File Offset: 0x00069EFE
		internal static Exception SynchronousCallMayNotPend()
		{
			return new Exception(SR.GetString("Internal Error"));
		}

		// Token: 0x0600153B RID: 5435 RVA: 0x0006BD0F File Offset: 0x00069F0F
		internal static Exception ConnectionLockedForBcpEvent()
		{
			return ADP.InvalidOperation(SR.GetString("The connection cannot be used because there is an ongoing operation that must be finished."));
		}

		// Token: 0x0600153C RID: 5436 RVA: 0x0006BD20 File Offset: 0x00069F20
		internal static Exception InstanceFailure()
		{
			return ADP.InvalidOperation(SR.GetString("Instance failure."));
		}

		// Token: 0x0600153D RID: 5437 RVA: 0x0006BD31 File Offset: 0x00069F31
		internal static Exception GlobalTransactionsNotEnabled()
		{
			return ADP.InvalidOperation(SR.GetString("Global Transactions are not enabled for this Azure SQL Database. Please contact Azure SQL Database support for assistance."));
		}

		// Token: 0x0600153E RID: 5438 RVA: 0x0006BD42 File Offset: 0x00069F42
		internal static Exception UnknownSysTxIsolationLevel(IsolationLevel isolationLevel)
		{
			return ADP.InvalidOperation(SR.GetString("Unrecognized System.Transactions.IsolationLevel enumeration value: {0}.", new object[]
			{
				isolationLevel.ToString()
			}));
		}

		// Token: 0x0600153F RID: 5439 RVA: 0x0006BD69 File Offset: 0x00069F69
		internal static Exception InvalidPartnerConfiguration(string server, string database)
		{
			return ADP.InvalidOperation(SR.GetString("Server {0}, database {1} is not configured for database mirroring.", new object[]
			{
				server,
				database
			}));
		}

		// Token: 0x06001540 RID: 5440 RVA: 0x0006BD88 File Offset: 0x00069F88
		internal static Exception MARSUnspportedOnConnection()
		{
			return ADP.InvalidOperation(SR.GetString("The connection does not support MultipleActiveResultSets."));
		}

		// Token: 0x06001541 RID: 5441 RVA: 0x0006BD99 File Offset: 0x00069F99
		internal static Exception CannotModifyPropertyAsyncOperationInProgress([CallerMemberName] string property = "")
		{
			return ADP.InvalidOperation(SR.GetString("{0} cannot be changed while async operation is in progress.", new object[]
			{
				property
			}));
		}

		// Token: 0x06001542 RID: 5442 RVA: 0x0006BDB4 File Offset: 0x00069FB4
		internal static Exception NonLocalSSEInstance()
		{
			return ADP.NotSupported(SR.GetString("SSE Instance re-direction is not supported for non-local user instances."));
		}

		// Token: 0x06001543 RID: 5443 RVA: 0x0006BDC5 File Offset: 0x00069FC5
		internal static ArgumentOutOfRangeException NotSupportedEnumerationValue(Type type, int value)
		{
			return ADP.ArgumentOutOfRange(SR.GetString("The {0} enumeration value, {1}, is not supported by the .Net Framework SqlClient Data Provider.", new object[]
			{
				type.Name,
				value.ToString(CultureInfo.InvariantCulture)
			}), type.Name);
		}

		// Token: 0x06001544 RID: 5444 RVA: 0x0006BDFA File Offset: 0x00069FFA
		internal static ArgumentOutOfRangeException NotSupportedCommandType(CommandType value)
		{
			return SQL.NotSupportedEnumerationValue(typeof(CommandType), (int)value);
		}

		// Token: 0x06001545 RID: 5445 RVA: 0x0006BE0C File Offset: 0x0006A00C
		internal static ArgumentOutOfRangeException NotSupportedIsolationLevel(IsolationLevel value)
		{
			return SQL.NotSupportedEnumerationValue(typeof(IsolationLevel), (int)value);
		}

		// Token: 0x06001546 RID: 5446 RVA: 0x0006BE1E File Offset: 0x0006A01E
		internal static Exception OperationCancelled()
		{
			return ADP.InvalidOperation(SR.GetString("Operation cancelled by user."));
		}

		// Token: 0x06001547 RID: 5447 RVA: 0x0006BE2F File Offset: 0x0006A02F
		internal static Exception PendingBeginXXXExists()
		{
			return ADP.InvalidOperation(SR.GetString("The command execution cannot proceed due to a pending asynchronous operation already in progress."));
		}

		// Token: 0x06001548 RID: 5448 RVA: 0x0006BE40 File Offset: 0x0006A040
		internal static ArgumentOutOfRangeException InvalidSqlDependencyTimeout(string param)
		{
			return ADP.ArgumentOutOfRange(SR.GetString("Timeout specified is invalid. Timeout cannot be < 0."), param);
		}

		// Token: 0x06001549 RID: 5449 RVA: 0x0006BE52 File Offset: 0x0006A052
		internal static Exception NonXmlResult()
		{
			return ADP.InvalidOperation(SR.GetString("Invalid command sent to ExecuteXmlReader.  The command must return an Xml result."));
		}

		// Token: 0x0600154A RID: 5450 RVA: 0x0006BE63 File Offset: 0x0006A063
		internal static Exception InvalidParameterTypeNameFormat()
		{
			return ADP.Argument(SR.GetString("Invalid 3 part name format for TypeName."));
		}

		// Token: 0x0600154B RID: 5451 RVA: 0x0006BE74 File Offset: 0x0006A074
		internal static Exception InvalidParameterNameLength(string value)
		{
			return ADP.Argument(SR.GetString("The length of the parameter '{0}' exceeds the limit of 128 characters.", new object[]
			{
				value
			}));
		}

		// Token: 0x0600154C RID: 5452 RVA: 0x0006BE8F File Offset: 0x0006A08F
		internal static Exception PrecisionValueOutOfRange(byte precision)
		{
			return ADP.Argument(SR.GetString("Precision value '{0}' is either less than 0 or greater than the maximum allowed precision of 38.", new object[]
			{
				precision.ToString(CultureInfo.InvariantCulture)
			}));
		}

		// Token: 0x0600154D RID: 5453 RVA: 0x0006BEB5 File Offset: 0x0006A0B5
		internal static Exception ScaleValueOutOfRange(byte scale)
		{
			return ADP.Argument(SR.GetString("Scale value '{0}' is either less than 0 or greater than the maximum allowed scale of 38.", new object[]
			{
				scale.ToString(CultureInfo.InvariantCulture)
			}));
		}

		// Token: 0x0600154E RID: 5454 RVA: 0x0006BEDB File Offset: 0x0006A0DB
		internal static Exception TimeScaleValueOutOfRange(byte scale)
		{
			return ADP.Argument(SR.GetString("Scale value '{0}' is either less than 0 or greater than the maximum allowed scale of 7.", new object[]
			{
				scale.ToString(CultureInfo.InvariantCulture)
			}));
		}

		// Token: 0x0600154F RID: 5455 RVA: 0x0006BF01 File Offset: 0x0006A101
		internal static Exception InvalidSqlDbType(SqlDbType value)
		{
			return ADP.InvalidEnumerationValue(typeof(SqlDbType), (int)value);
		}

		// Token: 0x06001550 RID: 5456 RVA: 0x0006BF13 File Offset: 0x0006A113
		internal static Exception UnsupportedTVPOutputParameter(ParameterDirection direction, string paramName)
		{
			return ADP.NotSupported(SR.GetString("ParameterDirection '{0}' specified for parameter '{1}' is not supported. Table-valued parameters only support ParameterDirection.Input.", new object[]
			{
				direction.ToString(),
				paramName
			}));
		}

		// Token: 0x06001551 RID: 5457 RVA: 0x0006BF3E File Offset: 0x0006A13E
		internal static Exception DBNullNotSupportedForTVPValues(string paramName)
		{
			return ADP.NotSupported(SR.GetString("DBNull value for parameter '{0}' is not supported. Table-valued parameters cannot be DBNull.", new object[]
			{
				paramName
			}));
		}

		// Token: 0x06001552 RID: 5458 RVA: 0x0006BF59 File Offset: 0x0006A159
		internal static Exception UnexpectedTypeNameForNonStructParams(string paramName)
		{
			return ADP.NotSupported(SR.GetString("TypeName specified for parameter '{0}'.  TypeName must only be set for Structured parameters.", new object[]
			{
				paramName
			}));
		}

		// Token: 0x06001553 RID: 5459 RVA: 0x0006BF74 File Offset: 0x0006A174
		internal static Exception ParameterInvalidVariant(string paramName)
		{
			return ADP.InvalidOperation(SR.GetString("Parameter '{0}' exceeds the size limit for the sql_variant datatype.", new object[]
			{
				paramName
			}));
		}

		// Token: 0x06001554 RID: 5460 RVA: 0x0006BF8F File Offset: 0x0006A18F
		internal static Exception MustSetTypeNameForParam(string paramType, string paramName)
		{
			return ADP.Argument(SR.GetString("The {0} type parameter '{1}' must have a valid type name.", new object[]
			{
				paramType,
				paramName
			}));
		}

		// Token: 0x06001555 RID: 5461 RVA: 0x0006BFAE File Offset: 0x0006A1AE
		internal static Exception NullSchemaTableDataTypeNotSupported(string columnName)
		{
			return ADP.Argument(SR.GetString("DateType column for field '{0}' in schema table is null.  DataType must be non-null.", new object[]
			{
				columnName
			}));
		}

		// Token: 0x06001556 RID: 5462 RVA: 0x0006BFC9 File Offset: 0x0006A1C9
		internal static Exception InvalidSchemaTableOrdinals()
		{
			return ADP.Argument(SR.GetString("Invalid column ordinals in schema table.  ColumnOrdinals, if present, must not have duplicates or gaps."));
		}

		// Token: 0x06001557 RID: 5463 RVA: 0x0006BFDA File Offset: 0x0006A1DA
		internal static Exception EnumeratedRecordMetaDataChanged(string fieldName, int recordNumber)
		{
			return ADP.Argument(SR.GetString("Metadata for field '{0}' of record '{1}' did not match the original record's metadata.", new object[]
			{
				fieldName,
				recordNumber
			}));
		}

		// Token: 0x06001558 RID: 5464 RVA: 0x0006BFFE File Offset: 0x0006A1FE
		internal static Exception EnumeratedRecordFieldCountChanged(int recordNumber)
		{
			return ADP.Argument(SR.GetString("Number of fields in record '{0}' does not match the number in the original record.", new object[]
			{
				recordNumber
			}));
		}

		// Token: 0x06001559 RID: 5465 RVA: 0x0006C01E File Offset: 0x0006A21E
		internal static Exception InvalidTDSVersion()
		{
			return ADP.InvalidOperation(SR.GetString("The SQL Server instance returned an invalid or unsupported protocol version during login negotiation."));
		}

		// Token: 0x0600155A RID: 5466 RVA: 0x0006C02F File Offset: 0x0006A22F
		internal static Exception ParsingError()
		{
			return ADP.InvalidOperation(SR.GetString("Internal connection fatal error."));
		}

		// Token: 0x0600155B RID: 5467 RVA: 0x0006C040 File Offset: 0x0006A240
		internal static Exception MoneyOverflow(string moneyValue)
		{
			return ADP.Overflow(SR.GetString("SqlDbType.SmallMoney overflow.  Value '{0}' is out of range.  Must be between -214,748.3648 and 214,748.3647.", new object[]
			{
				moneyValue
			}));
		}

		// Token: 0x0600155C RID: 5468 RVA: 0x0006C05B File Offset: 0x0006A25B
		internal static Exception SmallDateTimeOverflow(string datetime)
		{
			return ADP.Overflow(SR.GetString("SqlDbType.SmallDateTime overflow.  Value '{0}' is out of range.  Must be between 1/1/1900 12:00:00 AM and 6/6/2079 11:59:59 PM.", new object[]
			{
				datetime
			}));
		}

		// Token: 0x0600155D RID: 5469 RVA: 0x0006C076 File Offset: 0x0006A276
		internal static Exception SNIPacketAllocationFailure()
		{
			return ADP.InvalidOperation(SR.GetString("Memory allocation for internal connection failed."));
		}

		// Token: 0x0600155E RID: 5470 RVA: 0x0006C087 File Offset: 0x0006A287
		internal static Exception TimeOverflow(string time)
		{
			return ADP.Overflow(SR.GetString("SqlDbType.Time overflow.  Value '{0}' is out of range.  Must be between 00:00:00.0000000 and 23:59:59.9999999.", new object[]
			{
				time
			}));
		}

		// Token: 0x0600155F RID: 5471 RVA: 0x0006C0A2 File Offset: 0x0006A2A2
		internal static Exception InvalidRead()
		{
			return ADP.InvalidOperation(SR.GetString("Invalid attempt to read when no data is present."));
		}

		// Token: 0x06001560 RID: 5472 RVA: 0x0006C0B3 File Offset: 0x0006A2B3
		internal static Exception NonBlobColumn(string columnName)
		{
			return ADP.InvalidCast(SR.GetString("Invalid attempt to GetBytes on column '{0}'.  The GetBytes function can only be used on columns of type Text, NText, or Image.", new object[]
			{
				columnName
			}));
		}

		// Token: 0x06001561 RID: 5473 RVA: 0x0006C0CE File Offset: 0x0006A2CE
		internal static Exception NonCharColumn(string columnName)
		{
			return ADP.InvalidCast(SR.GetString("Invalid attempt to GetChars on column '{0}'.  The GetChars function can only be used on columns of type Text, NText, Xml, VarChar or NVarChar.", new object[]
			{
				columnName
			}));
		}

		// Token: 0x06001562 RID: 5474 RVA: 0x0006C0E9 File Offset: 0x0006A2E9
		internal static Exception StreamNotSupportOnColumnType(string columnName)
		{
			return ADP.InvalidCast(SR.GetString("Invalid attempt to GetStream on column '{0}'. The GetStream function can only be used on columns of type Binary, Image, Udt or VarBinary.", new object[]
			{
				columnName
			}));
		}

		// Token: 0x06001563 RID: 5475 RVA: 0x0006C104 File Offset: 0x0006A304
		internal static Exception TextReaderNotSupportOnColumnType(string columnName)
		{
			return ADP.InvalidCast(SR.GetString("Invalid attempt to GetTextReader on column '{0}'. The GetTextReader function can only be used on columns of type Char, NChar, NText, NVarChar, Text or VarChar.", new object[]
			{
				columnName
			}));
		}

		// Token: 0x06001564 RID: 5476 RVA: 0x0006C11F File Offset: 0x0006A31F
		internal static Exception XmlReaderNotSupportOnColumnType(string columnName)
		{
			return ADP.InvalidCast(SR.GetString("Invalid attempt to GetXmlReader on column '{0}'. The GetXmlReader function can only be used on columns of type Xml.", new object[]
			{
				columnName
			}));
		}

		// Token: 0x06001565 RID: 5477 RVA: 0x0006C13A File Offset: 0x0006A33A
		internal static Exception SqlCommandHasExistingSqlNotificationRequest()
		{
			return ADP.InvalidOperation(SR.GetString("This SqlCommand object is already associated with another SqlDependency object."));
		}

		// Token: 0x06001566 RID: 5478 RVA: 0x0006C14B File Offset: 0x0006A34B
		internal static Exception SqlDepDefaultOptionsButNoStart()
		{
			return ADP.InvalidOperation(SR.GetString("When using SqlDependency without providing an options value, SqlDependency.Start() must be called prior to execution of a command added to the SqlDependency instance."));
		}

		// Token: 0x06001567 RID: 5479 RVA: 0x0006C15C File Offset: 0x0006A35C
		internal static Exception SqlDependencyDatabaseBrokerDisabled()
		{
			return ADP.InvalidOperation(SR.GetString("The SQL Server Service Broker for the current database is not enabled, and as a result query notifications are not supported.  Please enable the Service Broker for this database if you wish to use notifications."));
		}

		// Token: 0x06001568 RID: 5480 RVA: 0x0006C16D File Offset: 0x0006A36D
		internal static Exception SqlDependencyEventNoDuplicate()
		{
			return ADP.InvalidOperation(SR.GetString("SqlDependency.OnChange does not support multiple event registrations for the same delegate."));
		}

		// Token: 0x06001569 RID: 5481 RVA: 0x0006C17E File Offset: 0x0006A37E
		internal static Exception SqlDependencyDuplicateStart()
		{
			return ADP.InvalidOperation(SR.GetString("SqlDependency does not support calling Start() with different connection strings having the same server, user, and database in the same app domain."));
		}

		// Token: 0x0600156A RID: 5482 RVA: 0x0006C18F File Offset: 0x0006A38F
		internal static Exception SqlDependencyIdMismatch()
		{
			return ADP.InvalidOperation(SR.GetString("No SqlDependency exists for the key."));
		}

		// Token: 0x0600156B RID: 5483 RVA: 0x0006C1A0 File Offset: 0x0006A3A0
		internal static Exception SqlDependencyNoMatchingServerStart()
		{
			return ADP.InvalidOperation(SR.GetString("When using SqlDependency without providing an options value, SqlDependency.Start() must be called for each server that is being executed against."));
		}

		// Token: 0x0600156C RID: 5484 RVA: 0x0006C1B1 File Offset: 0x0006A3B1
		internal static Exception SqlDependencyNoMatchingServerDatabaseStart()
		{
			return ADP.InvalidOperation(SR.GetString("SqlDependency.Start has been called for the server the command is executing against more than once, but there is no matching server/user/database Start() call for current command."));
		}

		// Token: 0x0600156D RID: 5485 RVA: 0x0006C1C2 File Offset: 0x0006A3C2
		internal static TransactionPromotionException PromotionFailed(Exception inner)
		{
			TransactionPromotionException ex = new TransactionPromotionException(SR.GetString("Failure while attempting to promote transaction."), inner);
			ADP.TraceExceptionAsReturnValue(ex);
			return ex;
		}

		// Token: 0x0600156E RID: 5486 RVA: 0x0006C1DA File Offset: 0x0006A3DA
		internal static Exception InvalidSqlDbTypeForConstructor(SqlDbType type)
		{
			return ADP.Argument(SR.GetString("The dbType {0} is invalid for this constructor.", new object[]
			{
				type.ToString()
			}));
		}

		// Token: 0x0600156F RID: 5487 RVA: 0x0006C201 File Offset: 0x0006A401
		internal static Exception NameTooLong(string parameterName)
		{
			return ADP.Argument(SR.GetString("The name is too long."), parameterName);
		}

		// Token: 0x06001570 RID: 5488 RVA: 0x0006C213 File Offset: 0x0006A413
		internal static Exception InvalidSortOrder(SortOrder order)
		{
			return ADP.InvalidEnumerationValue(typeof(SortOrder), (int)order);
		}

		// Token: 0x06001571 RID: 5489 RVA: 0x0006C225 File Offset: 0x0006A425
		internal static Exception MustSpecifyBothSortOrderAndOrdinal(SortOrder order, int ordinal)
		{
			return ADP.InvalidOperation(SR.GetString("The sort order and ordinal must either both be specified, or neither should be specified (SortOrder.Unspecified and -1).  The values given were: order = {0}, ordinal = {1}.", new object[]
			{
				order.ToString(),
				ordinal
			}));
		}

		// Token: 0x06001572 RID: 5490 RVA: 0x0006C255 File Offset: 0x0006A455
		internal static Exception UnsupportedColumnTypeForSqlProvider(string columnName, string typeName)
		{
			return ADP.Argument(SR.GetString("The type of column '{0}' is not supported.  The type is '{1}'", new object[]
			{
				columnName,
				typeName
			}));
		}

		// Token: 0x06001573 RID: 5491 RVA: 0x0006C274 File Offset: 0x0006A474
		internal static Exception InvalidColumnMaxLength(string columnName, long maxLength)
		{
			return ADP.Argument(SR.GetString("The size of column '{0}' is not supported. The size is {1}.", new object[]
			{
				columnName,
				maxLength
			}));
		}

		// Token: 0x06001574 RID: 5492 RVA: 0x0006C298 File Offset: 0x0006A498
		internal static Exception InvalidColumnPrecScale()
		{
			return ADP.Argument(SR.GetString("Invalid numeric precision/scale."));
		}

		// Token: 0x06001575 RID: 5493 RVA: 0x0006C2A9 File Offset: 0x0006A4A9
		internal static Exception NotEnoughColumnsInStructuredType()
		{
			return ADP.Argument(SR.GetString("There are not enough fields in the Structured type.  Structured types must have at least one field."));
		}

		// Token: 0x06001576 RID: 5494 RVA: 0x0006C2BA File Offset: 0x0006A4BA
		internal static Exception DuplicateSortOrdinal(int sortOrdinal)
		{
			return ADP.InvalidOperation(SR.GetString("The sort ordinal {0} was specified twice.", new object[]
			{
				sortOrdinal
			}));
		}

		// Token: 0x06001577 RID: 5495 RVA: 0x0006C2DA File Offset: 0x0006A4DA
		internal static Exception MissingSortOrdinal(int sortOrdinal)
		{
			return ADP.InvalidOperation(SR.GetString("The sort ordinal {0} was not specified.", new object[]
			{
				sortOrdinal
			}));
		}

		// Token: 0x06001578 RID: 5496 RVA: 0x0006C2FA File Offset: 0x0006A4FA
		internal static Exception SortOrdinalGreaterThanFieldCount(int columnOrdinal, int sortOrdinal)
		{
			return ADP.InvalidOperation(SR.GetString("The sort ordinal {0} on field {1} exceeds the total number of fields.", new object[]
			{
				sortOrdinal,
				columnOrdinal
			}));
		}

		// Token: 0x06001579 RID: 5497 RVA: 0x0006C323 File Offset: 0x0006A523
		internal static Exception IEnumerableOfSqlDataRecordHasNoRows()
		{
			return ADP.Argument(SR.GetString("There are no records in the SqlDataRecord enumeration. To send a table-valued parameter with no rows, use a null reference for the value instead."));
		}

		// Token: 0x0600157A RID: 5498 RVA: 0x0006C334 File Offset: 0x0006A534
		internal static Exception BulkLoadMappingInaccessible()
		{
			return ADP.InvalidOperation(SR.GetString("The mapped collection is in use and cannot be accessed at this time;"));
		}

		// Token: 0x0600157B RID: 5499 RVA: 0x0006C345 File Offset: 0x0006A545
		internal static Exception BulkLoadMappingsNamesOrOrdinalsOnly()
		{
			return ADP.InvalidOperation(SR.GetString("Mappings must be either all name or all ordinal based."));
		}

		// Token: 0x0600157C RID: 5500 RVA: 0x0006C356 File Offset: 0x0006A556
		internal static Exception BulkLoadCannotConvertValue(Type sourcetype, MetaType metatype, Exception e)
		{
			return ADP.InvalidOperation(SR.GetString("The given value of type {0} from the data source cannot be converted to type {1} of the specified target column.", new object[]
			{
				sourcetype.Name,
				metatype.TypeName
			}), e);
		}

		// Token: 0x0600157D RID: 5501 RVA: 0x0006C380 File Offset: 0x0006A580
		internal static Exception BulkLoadNonMatchingColumnMapping()
		{
			return ADP.InvalidOperation(SR.GetString("The given ColumnMapping does not match up with any column in the source or destination."));
		}

		// Token: 0x0600157E RID: 5502 RVA: 0x0006C391 File Offset: 0x0006A591
		internal static Exception BulkLoadNonMatchingColumnName(string columnName)
		{
			return SQL.BulkLoadNonMatchingColumnName(columnName, null);
		}

		// Token: 0x0600157F RID: 5503 RVA: 0x0006C39A File Offset: 0x0006A59A
		internal static Exception BulkLoadNonMatchingColumnName(string columnName, Exception e)
		{
			return ADP.InvalidOperation(SR.GetString("The given ColumnName '{0}' does not match up with any column in data source.", new object[]
			{
				columnName
			}), e);
		}

		// Token: 0x06001580 RID: 5504 RVA: 0x0006C3B6 File Offset: 0x0006A5B6
		internal static Exception BulkLoadStringTooLong()
		{
			return ADP.InvalidOperation(SR.GetString("String or binary data would be truncated."));
		}

		// Token: 0x06001581 RID: 5505 RVA: 0x0006C3C7 File Offset: 0x0006A5C7
		internal static Exception BulkLoadInvalidVariantValue()
		{
			return ADP.InvalidOperation(SR.GetString("Value cannot be converted to SqlVariant."));
		}

		// Token: 0x06001582 RID: 5506 RVA: 0x0006C3D8 File Offset: 0x0006A5D8
		internal static Exception BulkLoadInvalidTimeout(int timeout)
		{
			return ADP.Argument(SR.GetString("Timeout Value '{0}' is less than 0.", new object[]
			{
				timeout.ToString(CultureInfo.InvariantCulture)
			}));
		}

		// Token: 0x06001583 RID: 5507 RVA: 0x0006C3FE File Offset: 0x0006A5FE
		internal static Exception BulkLoadExistingTransaction()
		{
			return ADP.InvalidOperation(SR.GetString("Unexpected existing transaction."));
		}

		// Token: 0x06001584 RID: 5508 RVA: 0x0006C40F File Offset: 0x0006A60F
		internal static Exception BulkLoadNoCollation()
		{
			return ADP.InvalidOperation(SR.GetString("Failed to obtain column collation information for the destination table. If the table is not in the current database the name must be qualified using the database name (e.g. [mydb]..[mytable](e.g. [mydb]..[mytable]); this also applies to temporary-tables (e.g. #mytable would be specified as tempdb..#mytable)."));
		}

		// Token: 0x06001585 RID: 5509 RVA: 0x0006C420 File Offset: 0x0006A620
		internal static Exception BulkLoadConflictingTransactionOption()
		{
			return ADP.Argument(SR.GetString("Must not specify SqlBulkCopyOption.UseInternalTransaction and pass an external Transaction at the same time."));
		}

		// Token: 0x06001586 RID: 5510 RVA: 0x0006C431 File Offset: 0x0006A631
		internal static Exception BulkLoadLcidMismatch(int sourceLcid, string sourceColumnName, int destinationLcid, string destinationColumnName)
		{
			return ADP.InvalidOperation(SR.GetString("The locale id '{0}' of the source column '{1}' and the locale id '{2}' of the destination column '{3}' do not match.", new object[]
			{
				sourceLcid,
				sourceColumnName,
				destinationLcid,
				destinationColumnName
			}));
		}

		// Token: 0x06001587 RID: 5511 RVA: 0x0006C462 File Offset: 0x0006A662
		internal static Exception InvalidOperationInsideEvent()
		{
			return ADP.InvalidOperation(SR.GetString("Function must not be called during event."));
		}

		// Token: 0x06001588 RID: 5512 RVA: 0x0006C473 File Offset: 0x0006A673
		internal static Exception BulkLoadMissingDestinationTable()
		{
			return ADP.InvalidOperation(SR.GetString("The DestinationTableName property must be set before calling this method."));
		}

		// Token: 0x06001589 RID: 5513 RVA: 0x0006C484 File Offset: 0x0006A684
		internal static Exception BulkLoadInvalidDestinationTable(string tableName, Exception inner)
		{
			return ADP.InvalidOperation(SR.GetString("Cannot access destination table '{0}'.", new object[]
			{
				tableName
			}), inner);
		}

		// Token: 0x0600158A RID: 5514 RVA: 0x0006C4A0 File Offset: 0x0006A6A0
		internal static Exception BulkLoadBulkLoadNotAllowDBNull(string columnName)
		{
			return ADP.InvalidOperation(SR.GetString("Column '{0}' does not allow DBNull.Value.", new object[]
			{
				columnName
			}));
		}

		// Token: 0x0600158B RID: 5515 RVA: 0x0006C4BB File Offset: 0x0006A6BB
		internal static Exception BulkLoadPendingOperation()
		{
			return ADP.InvalidOperation(SR.GetString("Attempt to invoke bulk copy on an object that has a pending operation."));
		}

		// Token: 0x0600158C RID: 5516 RVA: 0x0006C4CC File Offset: 0x0006A6CC
		internal static Exception InvalidTableDerivedPrecisionForTvp(string columnName, byte precision)
		{
			return ADP.InvalidOperation(SR.GetString("Precision '{0}' required to send all values in column '{1}' exceeds the maximum supported precision '{2}'. The values must all fit in a single precision.", new object[]
			{
				precision,
				columnName,
				SqlDecimal.MaxPrecision
			}));
		}

		// Token: 0x0600158D RID: 5517 RVA: 0x0006C4FD File Offset: 0x0006A6FD
		internal static Exception ConnectionDoomed()
		{
			return ADP.InvalidOperation(SR.GetString("The requested operation cannot be completed because the connection has been broken."));
		}

		// Token: 0x0600158E RID: 5518 RVA: 0x0006C50E File Offset: 0x0006A70E
		internal static Exception OpenResultCountExceeded()
		{
			return ADP.InvalidOperation(SR.GetString("Open result count exceeded."));
		}

		// Token: 0x0600158F RID: 5519 RVA: 0x0006C51F File Offset: 0x0006A71F
		internal static Exception UnsupportedSysTxForGlobalTransactions()
		{
			return ADP.InvalidOperation(SR.GetString("The currently loaded System.Transactions.dll does not support Global Transactions."));
		}

		// Token: 0x06001590 RID: 5520 RVA: 0x0006C530 File Offset: 0x0006A730
		internal static Exception MultiSubnetFailoverWithFailoverPartner(bool serverProvidedFailoverPartner, SqlInternalConnectionTds internalConnection)
		{
			string @string = SR.GetString("Connecting to a mirrored SQL Server instance using the MultiSubnetFailover connection option is not supported.");
			if (serverProvidedFailoverPartner)
			{
				SqlException ex = SqlException.CreateException(new SqlErrorCollection
				{
					new SqlError(0, 0, 20, null, @string, "", 0, null)
				}, null, internalConnection, null);
				ex._doNotReconnect = true;
				return ex;
			}
			return ADP.Argument(@string);
		}

		// Token: 0x06001591 RID: 5521 RVA: 0x0006C57E File Offset: 0x0006A77E
		internal static Exception MultiSubnetFailoverWithMoreThan64IPs()
		{
			return ADP.InvalidOperation(SQL.GetSNIErrorMessage(47));
		}

		// Token: 0x06001592 RID: 5522 RVA: 0x0006C58C File Offset: 0x0006A78C
		internal static Exception MultiSubnetFailoverWithInstanceSpecified()
		{
			return ADP.Argument(SQL.GetSNIErrorMessage(48));
		}

		// Token: 0x06001593 RID: 5523 RVA: 0x0006C59A File Offset: 0x0006A79A
		internal static Exception MultiSubnetFailoverWithNonTcpProtocol()
		{
			return ADP.Argument(SQL.GetSNIErrorMessage(49));
		}

		// Token: 0x06001594 RID: 5524 RVA: 0x0006C5A8 File Offset: 0x0006A7A8
		internal static Exception ROR_FailoverNotSupportedConnString()
		{
			return ADP.Argument(SR.GetString("Connecting to a mirrored SQL Server instance using the ApplicationIntent ReadOnly connection option is not supported."));
		}

		// Token: 0x06001595 RID: 5525 RVA: 0x0006C5BC File Offset: 0x0006A7BC
		internal static Exception ROR_FailoverNotSupportedServer(SqlInternalConnectionTds internalConnection)
		{
			SqlException ex = SqlException.CreateException(new SqlErrorCollection
			{
				new SqlError(0, 0, 20, null, SR.GetString("Connecting to a mirrored SQL Server instance using the ApplicationIntent ReadOnly connection option is not supported."), "", 0, null)
			}, null, internalConnection, null);
			ex._doNotReconnect = true;
			return ex;
		}

		// Token: 0x06001596 RID: 5526 RVA: 0x0006C600 File Offset: 0x0006A800
		internal static Exception ROR_RecursiveRoutingNotSupported(SqlInternalConnectionTds internalConnection)
		{
			SqlException ex = SqlException.CreateException(new SqlErrorCollection
			{
				new SqlError(0, 0, 20, null, SR.GetString("Two or more redirections have occurred. Only one redirection per login is allowed."), "", 0, null)
			}, null, internalConnection, null);
			ex._doNotReconnect = true;
			return ex;
		}

		// Token: 0x06001597 RID: 5527 RVA: 0x0006C644 File Offset: 0x0006A844
		internal static Exception ROR_UnexpectedRoutingInfo(SqlInternalConnectionTds internalConnection)
		{
			SqlException ex = SqlException.CreateException(new SqlErrorCollection
			{
				new SqlError(0, 0, 20, null, SR.GetString("Unexpected routing information received."), "", 0, null)
			}, null, internalConnection, null);
			ex._doNotReconnect = true;
			return ex;
		}

		// Token: 0x06001598 RID: 5528 RVA: 0x0006C688 File Offset: 0x0006A888
		internal static Exception ROR_InvalidRoutingInfo(SqlInternalConnectionTds internalConnection)
		{
			SqlException ex = SqlException.CreateException(new SqlErrorCollection
			{
				new SqlError(0, 0, 20, null, SR.GetString("Invalid routing information received."), "", 0, null)
			}, null, internalConnection, null);
			ex._doNotReconnect = true;
			return ex;
		}

		// Token: 0x06001599 RID: 5529 RVA: 0x0006C6CC File Offset: 0x0006A8CC
		internal static Exception ROR_TimeoutAfterRoutingInfo(SqlInternalConnectionTds internalConnection)
		{
			SqlException ex = SqlException.CreateException(new SqlErrorCollection
			{
				new SqlError(0, 0, 20, null, SR.GetString("Server provided routing information, but timeout already expired."), "", 0, null)
			}, null, internalConnection, null);
			ex._doNotReconnect = true;
			return ex;
		}

		// Token: 0x0600159A RID: 5530 RVA: 0x0006C710 File Offset: 0x0006A910
		internal static SqlException CR_ReconnectTimeout()
		{
			return SqlException.CreateException(new SqlErrorCollection
			{
				new SqlError(-2, 0, 11, null, SQLMessage.Timeout(), "", 0, 258U, null)
			}, "");
		}

		// Token: 0x0600159B RID: 5531 RVA: 0x0006C750 File Offset: 0x0006A950
		internal static SqlException CR_ReconnectionCancelled()
		{
			return SqlException.CreateException(new SqlErrorCollection
			{
				new SqlError(0, 0, 11, null, SQLMessage.OperationCancelled(), "", 0, null)
			}, "");
		}

		// Token: 0x0600159C RID: 5532 RVA: 0x0006C788 File Offset: 0x0006A988
		internal static Exception CR_NextAttemptWillExceedQueryTimeout(SqlException innerException, Guid connectionId)
		{
			return SqlException.CreateException(new SqlErrorCollection
			{
				new SqlError(0, 0, 11, null, SR.GetString("Next reconnection attempt will exceed query timeout. Reconnection was terminated."), "", 0, null)
			}, "", connectionId, innerException);
		}

		// Token: 0x0600159D RID: 5533 RVA: 0x0006C7C8 File Offset: 0x0006A9C8
		internal static Exception CR_EncryptionChanged(SqlInternalConnectionTds internalConnection)
		{
			return SqlException.CreateException(new SqlErrorCollection
			{
				new SqlError(0, 0, 20, null, SR.GetString("The server did not preserve SSL encryption during a recovery attempt, connection recovery is not possible."), "", 0, null)
			}, "", internalConnection, null);
		}

		// Token: 0x0600159E RID: 5534 RVA: 0x0006C808 File Offset: 0x0006AA08
		internal static SqlException CR_AllAttemptsFailed(SqlException innerException, Guid connectionId)
		{
			return SqlException.CreateException(new SqlErrorCollection
			{
				new SqlError(0, 0, 11, null, SR.GetString("The connection is broken and recovery is not possible.  The client driver attempted to recover the connection one or more times and all attempts failed.  Increase the value of ConnectRetryCount to increase the number of recovery attempts."), "", 0, null)
			}, "", connectionId, innerException);
		}

		// Token: 0x0600159F RID: 5535 RVA: 0x0006C848 File Offset: 0x0006AA48
		internal static SqlException CR_NoCRAckAtReconnection(SqlInternalConnectionTds internalConnection)
		{
			return SqlException.CreateException(new SqlErrorCollection
			{
				new SqlError(0, 0, 20, null, SR.GetString("The server did not acknowledge a recovery attempt, connection recovery is not possible."), "", 0, null)
			}, "", internalConnection, null);
		}

		// Token: 0x060015A0 RID: 5536 RVA: 0x0006C888 File Offset: 0x0006AA88
		internal static SqlException CR_TDSVersionNotPreserved(SqlInternalConnectionTds internalConnection)
		{
			return SqlException.CreateException(new SqlErrorCollection
			{
				new SqlError(0, 0, 20, null, SR.GetString("The server did not preserve the exact client TDS version requested during a recovery attempt, connection recovery is not possible."), "", 0, null)
			}, "", internalConnection, null);
		}

		// Token: 0x060015A1 RID: 5537 RVA: 0x0006C8C8 File Offset: 0x0006AAC8
		internal static SqlException CR_UnrecoverableServer(Guid connectionId)
		{
			return SqlException.CreateException(new SqlErrorCollection
			{
				new SqlError(0, 0, 20, null, SR.GetString("The connection is broken and recovery is not possible.  The connection is marked by the server as unrecoverable.  No attempt was made to restore the connection."), "", 0, null)
			}, "", connectionId, null);
		}

		// Token: 0x060015A2 RID: 5538 RVA: 0x0006C908 File Offset: 0x0006AB08
		internal static SqlException CR_UnrecoverableClient(Guid connectionId)
		{
			return SqlException.CreateException(new SqlErrorCollection
			{
				new SqlError(0, 0, 20, null, SR.GetString("The connection is broken and recovery is not possible.  The connection is marked by the client driver as unrecoverable.  No attempt was made to restore the connection."), "", 0, null)
			}, "", connectionId, null);
		}

		// Token: 0x060015A3 RID: 5539 RVA: 0x0006C947 File Offset: 0x0006AB47
		internal static Exception StreamWriteNotSupported()
		{
			return ADP.NotSupported(SR.GetString("The Stream does not support writing."));
		}

		// Token: 0x060015A4 RID: 5540 RVA: 0x0006C958 File Offset: 0x0006AB58
		internal static Exception StreamReadNotSupported()
		{
			return ADP.NotSupported(SR.GetString("The Stream does not support reading."));
		}

		// Token: 0x060015A5 RID: 5541 RVA: 0x0006C969 File Offset: 0x0006AB69
		internal static Exception StreamSeekNotSupported()
		{
			return ADP.NotSupported(SR.GetString("The Stream does not support seeking."));
		}

		// Token: 0x060015A6 RID: 5542 RVA: 0x0006C97A File Offset: 0x0006AB7A
		internal static SqlNullValueException SqlNullValue()
		{
			return new SqlNullValueException();
		}

		// Token: 0x060015A7 RID: 5543 RVA: 0x0006C981 File Offset: 0x0006AB81
		internal static Exception SubclassMustOverride()
		{
			return ADP.InvalidOperation(SR.GetString("Subclass did not override a required method."));
		}

		// Token: 0x060015A8 RID: 5544 RVA: 0x0006C992 File Offset: 0x0006AB92
		internal static Exception UnsupportedKeyword(string keyword)
		{
			return ADP.NotSupported(SR.GetString("The keyword '{0}' is not supported on this platform.", new object[]
			{
				keyword
			}));
		}

		// Token: 0x060015A9 RID: 5545 RVA: 0x0006C9AD File Offset: 0x0006ABAD
		internal static Exception NetworkLibraryKeywordNotSupported()
		{
			return ADP.NotSupported(SR.GetString("The keyword 'Network Library' is not supported on this platform, prefix the 'Data Source' with the protocol desired instead ('tcp:' for a TCP connection, or 'np:' for a Named Pipe connection)."));
		}

		// Token: 0x060015AA RID: 5546 RVA: 0x0006C9C0 File Offset: 0x0006ABC0
		internal static Exception UnsupportedFeatureAndToken(SqlInternalConnectionTds internalConnection, string token)
		{
			NotSupportedException innerException = ADP.NotSupported(SR.GetString("Received an unsupported token '{0}' while reading data from the server.", new object[]
			{
				token
			}));
			return SqlException.CreateException(new SqlErrorCollection
			{
				new SqlError(0, 0, 20, null, SR.GetString("The server is attempting to use a feature that is not supported on this platform."), "", 0, null)
			}, "", internalConnection, innerException);
		}

		// Token: 0x060015AB RID: 5547 RVA: 0x0006CA19 File Offset: 0x0006AC19
		internal static Exception BatchedUpdatesNotAvailableOnContextConnection()
		{
			return ADP.InvalidOperation(SR.GetString("Batching updates is not supported on the context connection."));
		}

		// Token: 0x060015AC RID: 5548 RVA: 0x0006CA2A File Offset: 0x0006AC2A
		internal static string GetSNIErrorMessage(int sniError)
		{
			string text = string.Format(null, "SNI_ERROR_{0}", sniError);
			return SR.GetResourceString(text, text);
		}

		// Token: 0x060015AD RID: 5549 RVA: 0x0006CA43 File Offset: 0x0006AC43
		// Note: this type is marked as 'beforefieldinit'.
		static SQL()
		{
		}

		// Token: 0x04000EC1 RID: 3777
		internal static readonly byte[] AttentionHeader = new byte[]
		{
			6,
			1,
			0,
			8,
			0,
			0,
			0,
			0
		};

		// Token: 0x04000EC2 RID: 3778
		internal const int SqlDependencyTimeoutDefault = 0;

		// Token: 0x04000EC3 RID: 3779
		internal const int SqlDependencyServerTimeout = 432000;

		// Token: 0x04000EC4 RID: 3780
		internal const string SqlNotificationServiceDefault = "SqlQueryNotificationService";

		// Token: 0x04000EC5 RID: 3781
		internal const string SqlNotificationStoredProcedureDefault = "SqlQueryNotificationStoredProcedure";
	}
}
