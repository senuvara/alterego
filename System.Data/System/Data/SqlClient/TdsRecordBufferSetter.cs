﻿using System;
using System.Data.SqlTypes;
using System.Diagnostics;
using Microsoft.SqlServer.Server;

namespace System.Data.SqlClient
{
	// Token: 0x02000213 RID: 531
	internal class TdsRecordBufferSetter : SmiRecordBuffer
	{
		// Token: 0x060017AE RID: 6062 RVA: 0x0007BBCC File Offset: 0x00079DCC
		internal TdsRecordBufferSetter(TdsParserStateObject stateObj, SmiMetaData md)
		{
			this._fieldSetters = new TdsValueSetter[md.FieldMetaData.Count];
			for (int i = 0; i < md.FieldMetaData.Count; i++)
			{
				this._fieldSetters[i] = new TdsValueSetter(stateObj, md.FieldMetaData[i]);
			}
			this._stateObj = stateObj;
			this._metaData = md;
		}

		// Token: 0x17000455 RID: 1109
		// (get) Token: 0x060017AF RID: 6063 RVA: 0x000061C5 File Offset: 0x000043C5
		internal override bool CanGet
		{
			get
			{
				return false;
			}
		}

		// Token: 0x17000456 RID: 1110
		// (get) Token: 0x060017B0 RID: 6064 RVA: 0x0000EF1B File Offset: 0x0000D11B
		internal override bool CanSet
		{
			get
			{
				return true;
			}
		}

		// Token: 0x060017B1 RID: 6065 RVA: 0x0007BC33 File Offset: 0x00079E33
		public override void SetDBNull(SmiEventSink sink, int ordinal)
		{
			this._fieldSetters[ordinal].SetDBNull();
		}

		// Token: 0x060017B2 RID: 6066 RVA: 0x0007BC42 File Offset: 0x00079E42
		public override void SetBoolean(SmiEventSink sink, int ordinal, bool value)
		{
			this._fieldSetters[ordinal].SetBoolean(value);
		}

		// Token: 0x060017B3 RID: 6067 RVA: 0x0007BC52 File Offset: 0x00079E52
		public override void SetByte(SmiEventSink sink, int ordinal, byte value)
		{
			this._fieldSetters[ordinal].SetByte(value);
		}

		// Token: 0x060017B4 RID: 6068 RVA: 0x0007BC62 File Offset: 0x00079E62
		public override int SetBytes(SmiEventSink sink, int ordinal, long fieldOffset, byte[] buffer, int bufferOffset, int length)
		{
			return this._fieldSetters[ordinal].SetBytes(fieldOffset, buffer, bufferOffset, length);
		}

		// Token: 0x060017B5 RID: 6069 RVA: 0x0007BC78 File Offset: 0x00079E78
		public override void SetBytesLength(SmiEventSink sink, int ordinal, long length)
		{
			this._fieldSetters[ordinal].SetBytesLength(length);
		}

		// Token: 0x060017B6 RID: 6070 RVA: 0x0007BC88 File Offset: 0x00079E88
		public override int SetChars(SmiEventSink sink, int ordinal, long fieldOffset, char[] buffer, int bufferOffset, int length)
		{
			return this._fieldSetters[ordinal].SetChars(fieldOffset, buffer, bufferOffset, length);
		}

		// Token: 0x060017B7 RID: 6071 RVA: 0x0007BC9E File Offset: 0x00079E9E
		public override void SetCharsLength(SmiEventSink sink, int ordinal, long length)
		{
			this._fieldSetters[ordinal].SetCharsLength(length);
		}

		// Token: 0x060017B8 RID: 6072 RVA: 0x0007BCAE File Offset: 0x00079EAE
		public override void SetString(SmiEventSink sink, int ordinal, string value, int offset, int length)
		{
			this._fieldSetters[ordinal].SetString(value, offset, length);
		}

		// Token: 0x060017B9 RID: 6073 RVA: 0x0007BCC2 File Offset: 0x00079EC2
		public override void SetInt16(SmiEventSink sink, int ordinal, short value)
		{
			this._fieldSetters[ordinal].SetInt16(value);
		}

		// Token: 0x060017BA RID: 6074 RVA: 0x0007BCD2 File Offset: 0x00079ED2
		public override void SetInt32(SmiEventSink sink, int ordinal, int value)
		{
			this._fieldSetters[ordinal].SetInt32(value);
		}

		// Token: 0x060017BB RID: 6075 RVA: 0x0007BCE2 File Offset: 0x00079EE2
		public override void SetInt64(SmiEventSink sink, int ordinal, long value)
		{
			this._fieldSetters[ordinal].SetInt64(value);
		}

		// Token: 0x060017BC RID: 6076 RVA: 0x0007BCF2 File Offset: 0x00079EF2
		public override void SetSingle(SmiEventSink sink, int ordinal, float value)
		{
			this._fieldSetters[ordinal].SetSingle(value);
		}

		// Token: 0x060017BD RID: 6077 RVA: 0x0007BD02 File Offset: 0x00079F02
		public override void SetDouble(SmiEventSink sink, int ordinal, double value)
		{
			this._fieldSetters[ordinal].SetDouble(value);
		}

		// Token: 0x060017BE RID: 6078 RVA: 0x0007BD12 File Offset: 0x00079F12
		public override void SetSqlDecimal(SmiEventSink sink, int ordinal, SqlDecimal value)
		{
			this._fieldSetters[ordinal].SetSqlDecimal(value);
		}

		// Token: 0x060017BF RID: 6079 RVA: 0x0007BD22 File Offset: 0x00079F22
		public override void SetDateTime(SmiEventSink sink, int ordinal, DateTime value)
		{
			this._fieldSetters[ordinal].SetDateTime(value);
		}

		// Token: 0x060017C0 RID: 6080 RVA: 0x0007BD32 File Offset: 0x00079F32
		public override void SetGuid(SmiEventSink sink, int ordinal, Guid value)
		{
			this._fieldSetters[ordinal].SetGuid(value);
		}

		// Token: 0x060017C1 RID: 6081 RVA: 0x0007BD42 File Offset: 0x00079F42
		public override void SetTimeSpan(SmiEventSink sink, int ordinal, TimeSpan value)
		{
			this._fieldSetters[ordinal].SetTimeSpan(value);
		}

		// Token: 0x060017C2 RID: 6082 RVA: 0x0007BD52 File Offset: 0x00079F52
		public override void SetDateTimeOffset(SmiEventSink sink, int ordinal, DateTimeOffset value)
		{
			this._fieldSetters[ordinal].SetDateTimeOffset(value);
		}

		// Token: 0x060017C3 RID: 6083 RVA: 0x0007BD62 File Offset: 0x00079F62
		public override void SetVariantMetaData(SmiEventSink sink, int ordinal, SmiMetaData metaData)
		{
			this._fieldSetters[ordinal].SetVariantType(metaData);
		}

		// Token: 0x060017C4 RID: 6084 RVA: 0x0007BD72 File Offset: 0x00079F72
		internal override void NewElement(SmiEventSink sink)
		{
			this._stateObj.WriteByte(1);
		}

		// Token: 0x060017C5 RID: 6085 RVA: 0x0007BD80 File Offset: 0x00079F80
		internal override void EndElements(SmiEventSink sink)
		{
			this._stateObj.WriteByte(0);
		}

		// Token: 0x060017C6 RID: 6086 RVA: 0x00005E03 File Offset: 0x00004003
		[Conditional("DEBUG")]
		private void CheckWritingToColumn(int ordinal)
		{
		}

		// Token: 0x060017C7 RID: 6087 RVA: 0x00005E03 File Offset: 0x00004003
		[Conditional("DEBUG")]
		private void SkipPossibleDefaultedColumns(int targetColumn)
		{
		}

		// Token: 0x060017C8 RID: 6088 RVA: 0x00005E03 File Offset: 0x00004003
		[Conditional("DEBUG")]
		internal void CheckSettingColumn(int ordinal)
		{
		}

		// Token: 0x040011CA RID: 4554
		private TdsValueSetter[] _fieldSetters;

		// Token: 0x040011CB RID: 4555
		private TdsParserStateObject _stateObj;

		// Token: 0x040011CC RID: 4556
		private SmiMetaData _metaData;
	}
}
