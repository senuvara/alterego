﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace System.Data.SqlClient.SNI
{
	// Token: 0x0200023B RID: 571
	internal class SSRP
	{
		// Token: 0x060018DF RID: 6367 RVA: 0x00080E88 File Offset: 0x0007F088
		internal static int GetPortByInstanceName(string browserHostName, string instanceName)
		{
			byte[] requestPacket = SSRP.CreateInstanceInfoRequest(instanceName);
			byte[] array = null;
			try
			{
				array = SSRP.SendUDPRequest(browserHostName, 1434, requestPacket);
			}
			catch (SocketException innerException)
			{
				throw new Exception(SQLMessage.SqlServerBrowserNotAccessible(), innerException);
			}
			if (array == null || array.Length <= 3 || array[0] != 5 || (int)BitConverter.ToUInt16(array, 1) != array.Length - 3)
			{
				throw new SocketException();
			}
			string[] array2 = Encoding.ASCII.GetString(array, 3, array.Length - 3).Split(new char[]
			{
				';'
			});
			int num = Array.IndexOf<string>(array2, "tcp");
			if (num < 0 || num == array2.Length - 1)
			{
				throw new SocketException();
			}
			return (int)ushort.Parse(array2[num + 1]);
		}

		// Token: 0x060018E0 RID: 6368 RVA: 0x00080F3C File Offset: 0x0007F13C
		private static byte[] CreateInstanceInfoRequest(string instanceName)
		{
			instanceName += "\0";
			byte[] array = new byte[Encoding.ASCII.GetByteCount(instanceName) + 1];
			array[0] = 4;
			Encoding.ASCII.GetBytes(instanceName, 0, instanceName.Length, array, 1);
			return array;
		}

		// Token: 0x060018E1 RID: 6369 RVA: 0x00080F84 File Offset: 0x0007F184
		internal static int GetDacPortByInstanceName(string browserHostName, string instanceName)
		{
			byte[] requestPacket = SSRP.CreateDacPortInfoRequest(instanceName);
			byte[] array = SSRP.SendUDPRequest(browserHostName, 1434, requestPacket);
			if (array == null || array.Length <= 4 || array[0] != 5 || BitConverter.ToUInt16(array, 1) != 6 || array[3] != 1)
			{
				throw new SocketException();
			}
			return (int)BitConverter.ToUInt16(array, 4);
		}

		// Token: 0x060018E2 RID: 6370 RVA: 0x00080FD4 File Offset: 0x0007F1D4
		private static byte[] CreateDacPortInfoRequest(string instanceName)
		{
			instanceName += "\0";
			byte[] array = new byte[Encoding.ASCII.GetByteCount(instanceName) + 2];
			array[0] = 15;
			array[1] = 1;
			Encoding.ASCII.GetBytes(instanceName, 0, instanceName.Length, array, 2);
			return array;
		}

		// Token: 0x060018E3 RID: 6371 RVA: 0x00081020 File Offset: 0x0007F220
		private static byte[] SendUDPRequest(string browserHostname, int port, byte[] requestPacket)
		{
			IPAddress ipaddress = null;
			bool flag = IPAddress.TryParse(browserHostname, out ipaddress);
			byte[] result = null;
			using (UdpClient udpClient = new UdpClient((!flag) ? AddressFamily.InterNetwork : ipaddress.AddressFamily))
			{
				Task<UdpReceiveResult> task;
				if (udpClient.SendAsync(requestPacket, requestPacket.Length, browserHostname, port).Wait(1000) && (task = udpClient.ReceiveAsync()).Wait(1000))
				{
					result = task.Result.Buffer;
				}
			}
			return result;
		}

		// Token: 0x060018E4 RID: 6372 RVA: 0x00005C14 File Offset: 0x00003E14
		public SSRP()
		{
		}

		// Token: 0x040012BA RID: 4794
		private const char SemicolonSeparator = ';';

		// Token: 0x040012BB RID: 4795
		private const int SqlServerBrowserPort = 1434;
	}
}
