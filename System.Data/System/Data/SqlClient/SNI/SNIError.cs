﻿using System;

namespace System.Data.SqlClient.SNI
{
	// Token: 0x02000227 RID: 551
	internal class SNIError
	{
		// Token: 0x06001822 RID: 6178 RVA: 0x0007DA0C File Offset: 0x0007BC0C
		public SNIError(SNIProviders provider, uint nativeError, uint sniErrorCode, string errorMessage)
		{
			this.lineNumber = 0U;
			this.function = string.Empty;
			this.provider = provider;
			this.nativeError = nativeError;
			this.sniError = sniErrorCode;
			this.errorMessage = errorMessage;
			this.exception = null;
		}

		// Token: 0x06001823 RID: 6179 RVA: 0x0007DA4C File Offset: 0x0007BC4C
		public SNIError(SNIProviders provider, uint sniErrorCode, Exception sniException)
		{
			this.lineNumber = 0U;
			this.function = string.Empty;
			this.provider = provider;
			this.nativeError = 0U;
			this.sniError = sniErrorCode;
			this.errorMessage = string.Empty;
			this.exception = sniException;
		}

		// Token: 0x0400122C RID: 4652
		public readonly SNIProviders provider;

		// Token: 0x0400122D RID: 4653
		public readonly string errorMessage;

		// Token: 0x0400122E RID: 4654
		public readonly uint nativeError;

		// Token: 0x0400122F RID: 4655
		public readonly uint sniError;

		// Token: 0x04001230 RID: 4656
		public readonly string function;

		// Token: 0x04001231 RID: 4657
		public readonly uint lineNumber;

		// Token: 0x04001232 RID: 4658
		public readonly Exception exception;
	}
}
