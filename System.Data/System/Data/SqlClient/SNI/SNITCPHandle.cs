﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Security;
using System.Net.Sockets;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;
using System.Threading;
using System.Threading.Tasks;

namespace System.Data.SqlClient.SNI
{
	// Token: 0x02000236 RID: 566
	internal class SNITCPHandle : SNIHandle
	{
		// Token: 0x060018C0 RID: 6336 RVA: 0x0007FF3C File Offset: 0x0007E13C
		public override void Dispose()
		{
			lock (this)
			{
				if (this._sslOverTdsStream != null)
				{
					this._sslOverTdsStream.Dispose();
					this._sslOverTdsStream = null;
				}
				if (this._sslStream != null)
				{
					this._sslStream.Dispose();
					this._sslStream = null;
				}
				if (this._tcpStream != null)
				{
					this._tcpStream.Dispose();
					this._tcpStream = null;
				}
				this._stream = null;
			}
		}

		// Token: 0x1700047F RID: 1151
		// (get) Token: 0x060018C1 RID: 6337 RVA: 0x0007FFC8 File Offset: 0x0007E1C8
		public override Guid ConnectionId
		{
			get
			{
				return this._connectionId;
			}
		}

		// Token: 0x17000480 RID: 1152
		// (get) Token: 0x060018C2 RID: 6338 RVA: 0x0007FFD0 File Offset: 0x0007E1D0
		public override uint Status
		{
			get
			{
				return this._status;
			}
		}

		// Token: 0x060018C3 RID: 6339 RVA: 0x0007FFD8 File Offset: 0x0007E1D8
		public SNITCPHandle(string serverName, int port, long timerExpire, object callbackObject, bool parallel)
		{
			this._writeScheduler = new ConcurrentExclusiveSchedulerPair().ExclusiveScheduler;
			this._writeTaskFactory = new TaskFactory(this._writeScheduler);
			this._callbackObject = callbackObject;
			this._targetServer = serverName;
			try
			{
				TimeSpan timeSpan = default(TimeSpan);
				bool flag = long.MaxValue == timerExpire;
				if (!flag)
				{
					timeSpan = DateTime.FromFileTime(timerExpire) - DateTime.Now;
					timeSpan = ((timeSpan.Ticks < 0L) ? TimeSpan.FromTicks(0L) : timeSpan);
				}
				Task<Socket> task;
				if (parallel)
				{
					Task<IPAddress[]> hostAddressesAsync = Dns.GetHostAddressesAsync(serverName);
					hostAddressesAsync.Wait(timeSpan);
					IPAddress[] result = hostAddressesAsync.Result;
					if (result.Length > 64)
					{
						this.ReportTcpSNIError(0U, 47U, string.Empty);
						return;
					}
					task = SNITCPHandle.ParallelConnectAsync(result, port);
				}
				else
				{
					task = SNITCPHandle.ConnectAsync(serverName, port);
				}
				if (!(flag ? task.Wait(-1) : task.Wait(timeSpan)))
				{
					this.ReportTcpSNIError(0U, 40U, string.Empty);
					return;
				}
				this._socket = task.Result;
				if (this._socket == null || !this._socket.Connected)
				{
					if (this._socket != null)
					{
						this._socket.Dispose();
						this._socket = null;
					}
					this.ReportTcpSNIError(0U, 40U, string.Empty);
					return;
				}
				this._socket.NoDelay = true;
				this._tcpStream = new NetworkStream(this._socket, true);
				this._sslOverTdsStream = new SslOverTdsStream(this._tcpStream);
				this._sslStream = new SslStream(this._sslOverTdsStream, true, new RemoteCertificateValidationCallback(this.ValidateServerCertificate), null);
			}
			catch (SocketException sniException)
			{
				this.ReportTcpSNIError(sniException);
				return;
			}
			catch (Exception sniException2)
			{
				this.ReportTcpSNIError(sniException2);
				return;
			}
			this._stream = this._tcpStream;
			this._status = 0U;
		}

		// Token: 0x060018C4 RID: 6340 RVA: 0x000801EC File Offset: 0x0007E3EC
		private static async Task<Socket> ConnectAsync(string serverName, int port)
		{
			IPAddress[] array = await Dns.GetHostAddressesAsync(serverName).ConfigureAwait(false);
			IPAddress targetAddrV4 = Array.Find<IPAddress>(array, (IPAddress addr) => addr.AddressFamily == AddressFamily.InterNetwork);
			IPAddress targetAddrV5 = Array.Find<IPAddress>(array, (IPAddress addr) => addr.AddressFamily == AddressFamily.InterNetworkV6);
			Socket result;
			if (targetAddrV4 != null && targetAddrV5 != null)
			{
				result = await SNITCPHandle.ParallelConnectAsync(new IPAddress[]
				{
					targetAddrV4,
					targetAddrV5
				}, port).ConfigureAwait(false);
			}
			else
			{
				IPAddress ipaddress = (targetAddrV4 != null) ? targetAddrV4 : targetAddrV5;
				Socket socket = new Socket(ipaddress.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
				try
				{
					await socket.ConnectAsync(ipaddress, port).ConfigureAwait(false);
				}
				catch
				{
					socket.Dispose();
					throw;
				}
				result = socket;
			}
			return result;
		}

		// Token: 0x060018C5 RID: 6341 RVA: 0x0008023C File Offset: 0x0007E43C
		private static Task<Socket> ParallelConnectAsync(IPAddress[] serverAddresses, int port)
		{
			if (serverAddresses == null)
			{
				throw new ArgumentNullException("serverAddresses");
			}
			if (serverAddresses.Length == 0)
			{
				throw new ArgumentOutOfRangeException("serverAddresses");
			}
			List<Socket> list = new List<Socket>(serverAddresses.Length);
			List<Task> list2 = new List<Task>(serverAddresses.Length);
			TaskCompletionSource<Socket> taskCompletionSource = new TaskCompletionSource<Socket>();
			StrongBox<Exception> lastError = new StrongBox<Exception>();
			StrongBox<int> pendingCompleteCount = new StrongBox<int>(serverAddresses.Length);
			foreach (IPAddress ipaddress in serverAddresses)
			{
				Socket socket = new Socket(ipaddress.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
				list.Add(socket);
				try
				{
					list2.Add(socket.ConnectAsync(ipaddress, port));
				}
				catch (Exception exception)
				{
					list2.Add(Task.FromException(exception));
				}
			}
			for (int j = 0; j < list.Count; j++)
			{
				SNITCPHandle.ParallelConnectHelper(list[j], list2[j], taskCompletionSource, pendingCompleteCount, lastError, list);
			}
			return taskCompletionSource.Task;
		}

		// Token: 0x060018C6 RID: 6342 RVA: 0x0008032C File Offset: 0x0007E52C
		private static async void ParallelConnectHelper(Socket socket, Task connectTask, TaskCompletionSource<Socket> tcs, StrongBox<int> pendingCompleteCount, StrongBox<Exception> lastError, List<Socket> sockets)
		{
			bool success = false;
			try
			{
				await connectTask.ConfigureAwait(false);
				success = tcs.TrySetResult(socket);
				if (success)
				{
					foreach (Socket socket2 in sockets)
					{
						if (socket2 != socket)
						{
							socket2.Dispose();
						}
					}
				}
			}
			catch (Exception value)
			{
				Interlocked.Exchange<Exception>(ref lastError.Value, value);
			}
			finally
			{
				if (!success && Interlocked.Decrement(ref pendingCompleteCount.Value) == 0)
				{
					if (lastError.Value != null)
					{
						tcs.TrySetException(lastError.Value);
					}
					else
					{
						tcs.TrySetCanceled();
					}
					List<Socket>.Enumerator enumerator = sockets.GetEnumerator();
					try
					{
						while (enumerator.MoveNext())
						{
							Socket socket3 = enumerator.Current;
							socket3.Dispose();
						}
					}
					finally
					{
						int num;
						if (num < 0)
						{
							((IDisposable)enumerator).Dispose();
						}
					}
				}
			}
		}

		// Token: 0x060018C7 RID: 6343 RVA: 0x00080390 File Offset: 0x0007E590
		public override uint EnableSsl(uint options)
		{
			this._validateCert = ((options & 1U) > 0U);
			try
			{
				this._sslStream.AuthenticateAsClientAsync(this._targetServer).GetAwaiter().GetResult();
				this._sslOverTdsStream.FinishHandshake();
			}
			catch (AuthenticationException sniException)
			{
				return this.ReportTcpSNIError(sniException);
			}
			catch (InvalidOperationException sniException2)
			{
				return this.ReportTcpSNIError(sniException2);
			}
			this._stream = this._sslStream;
			return 0U;
		}

		// Token: 0x060018C8 RID: 6344 RVA: 0x00080418 File Offset: 0x0007E618
		public override void DisableSsl()
		{
			if (Environment.OSVersion.Platform != PlatformID.Win32NT)
			{
				this._sslStream.Dispose();
			}
			this._sslStream = null;
			this._sslOverTdsStream.Dispose();
			this._sslOverTdsStream = null;
			this._stream = this._tcpStream;
		}

		// Token: 0x060018C9 RID: 6345 RVA: 0x00080457 File Offset: 0x0007E657
		private bool ValidateServerCertificate(object sender, X509Certificate cert, X509Chain chain, SslPolicyErrors policyErrors)
		{
			return !this._validateCert || SNICommon.ValidateSslServerCertificate(this._targetServer, sender, cert, chain, policyErrors);
		}

		// Token: 0x060018CA RID: 6346 RVA: 0x00080473 File Offset: 0x0007E673
		public override void SetBufferSize(int bufferSize)
		{
			this._bufferSize = bufferSize;
			this._socket.SendBufferSize = bufferSize;
			this._socket.ReceiveBufferSize = bufferSize;
		}

		// Token: 0x060018CB RID: 6347 RVA: 0x00080494 File Offset: 0x0007E694
		public override uint Send(SNIPacket packet)
		{
			uint result;
			lock (this)
			{
				try
				{
					packet.WriteToStream(this._stream);
					result = 0U;
				}
				catch (ObjectDisposedException sniException)
				{
					result = this.ReportTcpSNIError(sniException);
				}
				catch (SocketException sniException2)
				{
					result = this.ReportTcpSNIError(sniException2);
				}
				catch (IOException sniException3)
				{
					result = this.ReportTcpSNIError(sniException3);
				}
			}
			return result;
		}

		// Token: 0x060018CC RID: 6348 RVA: 0x00080524 File Offset: 0x0007E724
		public override uint Receive(out SNIPacket packet, int timeoutInMilliseconds)
		{
			uint result;
			lock (this)
			{
				packet = null;
				try
				{
					if (timeoutInMilliseconds > 0)
					{
						this._socket.ReceiveTimeout = timeoutInMilliseconds;
					}
					else
					{
						if (timeoutInMilliseconds != -1)
						{
							this.ReportTcpSNIError(0U, 11U, string.Empty);
							return 258U;
						}
						this._socket.ReceiveTimeout = 0;
					}
					packet = new SNIPacket(null);
					packet.Allocate(this._bufferSize);
					packet.ReadFromStream(this._stream);
					if (packet.Length == 0)
					{
						Win32Exception ex = new Win32Exception();
						result = this.ReportErrorAndReleasePacket(packet, (uint)ex.NativeErrorCode, 0U, ex.Message);
					}
					else
					{
						result = 0U;
					}
				}
				catch (ObjectDisposedException sniException)
				{
					result = this.ReportErrorAndReleasePacket(packet, sniException);
				}
				catch (SocketException sniException2)
				{
					result = this.ReportErrorAndReleasePacket(packet, sniException2);
				}
				catch (IOException ex2)
				{
					uint num = this.ReportErrorAndReleasePacket(packet, ex2);
					if (ex2.InnerException is SocketException && ((SocketException)ex2.InnerException).SocketErrorCode == SocketError.TimedOut)
					{
						num = 258U;
					}
					result = num;
				}
				finally
				{
					this._socket.ReceiveTimeout = 0;
				}
			}
			return result;
		}

		// Token: 0x060018CD RID: 6349 RVA: 0x000806C0 File Offset: 0x0007E8C0
		public override void SetAsyncCallbacks(SNIAsyncCallback receiveCallback, SNIAsyncCallback sendCallback)
		{
			this._receiveCallback = receiveCallback;
			this._sendCallback = sendCallback;
		}

		// Token: 0x060018CE RID: 6350 RVA: 0x000806D0 File Offset: 0x0007E8D0
		public override uint SendAsync(SNIPacket packet, SNIAsyncCallback callback = null)
		{
			SNIPacket packet2 = packet;
			this._writeTaskFactory.StartNew(delegate()
			{
				try
				{
					SNITCPHandle <>4__this = this;
					lock (<>4__this)
					{
						packet.WriteToStream(this._stream);
					}
				}
				catch (Exception sniException)
				{
					SNILoadHandle.SingletonInstance.LastError = new SNIError(SNIProviders.TCP_PROV, 35U, sniException);
					if (callback != null)
					{
						callback(packet, 1U);
					}
					else
					{
						this._sendCallback(packet, 1U);
					}
					return;
				}
				if (callback != null)
				{
					callback(packet, 0U);
					return;
				}
				this._sendCallback(packet, 0U);
			});
			return 997U;
		}

		// Token: 0x060018CF RID: 6351 RVA: 0x0008071C File Offset: 0x0007E91C
		public override uint ReceiveAsync(ref SNIPacket packet)
		{
			uint result;
			lock (this)
			{
				packet = new SNIPacket(null);
				packet.Allocate(this._bufferSize);
				try
				{
					packet.ReadFromStreamAsync(this._stream, this._receiveCallback);
					result = 997U;
				}
				catch (ObjectDisposedException sniException)
				{
					result = this.ReportErrorAndReleasePacket(packet, sniException);
				}
				catch (SocketException sniException2)
				{
					result = this.ReportErrorAndReleasePacket(packet, sniException2);
				}
				catch (IOException sniException3)
				{
					result = this.ReportErrorAndReleasePacket(packet, sniException3);
				}
			}
			return result;
		}

		// Token: 0x060018D0 RID: 6352 RVA: 0x000807D0 File Offset: 0x0007E9D0
		public override uint CheckConnection()
		{
			try
			{
				if (!this._socket.Connected || this._socket.Poll(0, SelectMode.SelectError))
				{
					return 1U;
				}
			}
			catch (SocketException sniException)
			{
				return this.ReportTcpSNIError(sniException);
			}
			catch (ObjectDisposedException sniException2)
			{
				return this.ReportTcpSNIError(sniException2);
			}
			return 0U;
		}

		// Token: 0x060018D1 RID: 6353 RVA: 0x00080834 File Offset: 0x0007EA34
		private uint ReportTcpSNIError(Exception sniException)
		{
			this._status = 1U;
			return SNICommon.ReportSNIError(SNIProviders.TCP_PROV, 35U, sniException);
		}

		// Token: 0x060018D2 RID: 6354 RVA: 0x00080846 File Offset: 0x0007EA46
		private uint ReportTcpSNIError(uint nativeError, uint sniError, string errorMessage)
		{
			this._status = 1U;
			return SNICommon.ReportSNIError(SNIProviders.TCP_PROV, nativeError, sniError, errorMessage);
		}

		// Token: 0x060018D3 RID: 6355 RVA: 0x00080858 File Offset: 0x0007EA58
		private uint ReportErrorAndReleasePacket(SNIPacket packet, Exception sniException)
		{
			if (packet != null)
			{
				packet.Release();
			}
			return this.ReportTcpSNIError(sniException);
		}

		// Token: 0x060018D4 RID: 6356 RVA: 0x0008086A File Offset: 0x0007EA6A
		private uint ReportErrorAndReleasePacket(SNIPacket packet, uint nativeError, uint sniError, string errorMessage)
		{
			if (packet != null)
			{
				packet.Release();
			}
			return this.ReportTcpSNIError(nativeError, sniError, errorMessage);
		}

		// Token: 0x04001290 RID: 4752
		private readonly string _targetServer;

		// Token: 0x04001291 RID: 4753
		private readonly object _callbackObject;

		// Token: 0x04001292 RID: 4754
		private readonly Socket _socket;

		// Token: 0x04001293 RID: 4755
		private NetworkStream _tcpStream;

		// Token: 0x04001294 RID: 4756
		private readonly TaskScheduler _writeScheduler;

		// Token: 0x04001295 RID: 4757
		private readonly TaskFactory _writeTaskFactory;

		// Token: 0x04001296 RID: 4758
		private Stream _stream;

		// Token: 0x04001297 RID: 4759
		private SslStream _sslStream;

		// Token: 0x04001298 RID: 4760
		private SslOverTdsStream _sslOverTdsStream;

		// Token: 0x04001299 RID: 4761
		private SNIAsyncCallback _receiveCallback;

		// Token: 0x0400129A RID: 4762
		private SNIAsyncCallback _sendCallback;

		// Token: 0x0400129B RID: 4763
		private bool _validateCert = true;

		// Token: 0x0400129C RID: 4764
		private int _bufferSize = 4096;

		// Token: 0x0400129D RID: 4765
		private uint _status = uint.MaxValue;

		// Token: 0x0400129E RID: 4766
		private Guid _connectionId = Guid.NewGuid();

		// Token: 0x0400129F RID: 4767
		private const int MaxParallelIpAddresses = 64;

		// Token: 0x02000237 RID: 567
		[CompilerGenerated]
		[Serializable]
		private sealed class <>c
		{
			// Token: 0x060018D5 RID: 6357 RVA: 0x0008087F File Offset: 0x0007EA7F
			// Note: this type is marked as 'beforefieldinit'.
			static <>c()
			{
			}

			// Token: 0x060018D6 RID: 6358 RVA: 0x00005C14 File Offset: 0x00003E14
			public <>c()
			{
			}

			// Token: 0x060018D7 RID: 6359 RVA: 0x0008088B File Offset: 0x0007EA8B
			internal bool <ConnectAsync>b__22_0(IPAddress addr)
			{
				return addr.AddressFamily == AddressFamily.InterNetwork;
			}

			// Token: 0x060018D8 RID: 6360 RVA: 0x00080896 File Offset: 0x0007EA96
			internal bool <ConnectAsync>b__22_1(IPAddress addr)
			{
				return addr.AddressFamily == AddressFamily.InterNetworkV6;
			}

			// Token: 0x040012A0 RID: 4768
			public static readonly SNITCPHandle.<>c <>9 = new SNITCPHandle.<>c();

			// Token: 0x040012A1 RID: 4769
			public static Predicate<IPAddress> <>9__22_0;

			// Token: 0x040012A2 RID: 4770
			public static Predicate<IPAddress> <>9__22_1;
		}

		// Token: 0x02000238 RID: 568
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ConnectAsync>d__22 : IAsyncStateMachine
		{
			// Token: 0x060018D9 RID: 6361 RVA: 0x000808A4 File Offset: 0x0007EAA4
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				Socket result;
				try
				{
					ConfiguredTaskAwaitable<IPAddress[]>.ConfiguredTaskAwaiter configuredTaskAwaiter;
					ConfiguredTaskAwaitable<Socket>.ConfiguredTaskAwaiter configuredTaskAwaiter3;
					switch (num)
					{
					case 0:
					{
						ConfiguredTaskAwaitable<IPAddress[]>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<IPAddress[]>.ConfiguredTaskAwaiter);
						num = (num2 = -1);
						break;
					}
					case 1:
					{
						ConfiguredTaskAwaitable<Socket>.ConfiguredTaskAwaiter configuredTaskAwaiter4;
						configuredTaskAwaiter3 = configuredTaskAwaiter4;
						configuredTaskAwaiter4 = default(ConfiguredTaskAwaitable<Socket>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_16E;
					}
					case 2:
						IL_1A7:
						try
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter5;
							if (num != 2)
							{
								IPAddress ipaddress;
								configuredTaskAwaiter5 = socket.ConnectAsync(ipaddress, port).ConfigureAwait(false).GetAwaiter();
								if (!configuredTaskAwaiter5.IsCompleted)
								{
									num2 = 2;
									ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter6 = configuredTaskAwaiter5;
									this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, SNITCPHandle.<ConnectAsync>d__22>(ref configuredTaskAwaiter5, ref this);
									return;
								}
							}
							else
							{
								ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter6;
								configuredTaskAwaiter5 = configuredTaskAwaiter6;
								configuredTaskAwaiter6 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
								num2 = -1;
							}
							configuredTaskAwaiter5.GetResult();
						}
						catch
						{
							socket.Dispose();
							throw;
						}
						result = socket;
						goto IL_250;
					default:
						configuredTaskAwaiter = Dns.GetHostAddressesAsync(serverName).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable<IPAddress[]>.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<IPAddress[]>.ConfiguredTaskAwaiter, SNITCPHandle.<ConnectAsync>d__22>(ref configuredTaskAwaiter, ref this);
							return;
						}
						break;
					}
					IPAddress[] result2 = configuredTaskAwaiter.GetResult();
					targetAddrV4 = Array.Find<IPAddress>(result2, new Predicate<IPAddress>(SNITCPHandle.<>c.<>9.<ConnectAsync>b__22_0));
					targetAddrV5 = Array.Find<IPAddress>(result2, new Predicate<IPAddress>(SNITCPHandle.<>c.<>9.<ConnectAsync>b__22_1));
					if (targetAddrV4 == null || targetAddrV5 == null)
					{
						IPAddress ipaddress = (targetAddrV4 != null) ? targetAddrV4 : targetAddrV5;
						socket = new Socket(ipaddress.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
						goto IL_1A7;
					}
					configuredTaskAwaiter3 = SNITCPHandle.ParallelConnectAsync(new IPAddress[]
					{
						targetAddrV4,
						targetAddrV5
					}, port).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter3.IsCompleted)
					{
						num2 = 1;
						ConfiguredTaskAwaitable<Socket>.ConfiguredTaskAwaiter configuredTaskAwaiter4 = configuredTaskAwaiter3;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<Socket>.ConfiguredTaskAwaiter, SNITCPHandle.<ConnectAsync>d__22>(ref configuredTaskAwaiter3, ref this);
						return;
					}
					IL_16E:
					result = configuredTaskAwaiter3.GetResult();
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_250:
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x060018DA RID: 6362 RVA: 0x00080B4C File Offset: 0x0007ED4C
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x040012A3 RID: 4771
			public int <>1__state;

			// Token: 0x040012A4 RID: 4772
			public AsyncTaskMethodBuilder<Socket> <>t__builder;

			// Token: 0x040012A5 RID: 4773
			public string serverName;

			// Token: 0x040012A6 RID: 4774
			public int port;

			// Token: 0x040012A7 RID: 4775
			private IPAddress <targetAddrV4>5__1;

			// Token: 0x040012A8 RID: 4776
			private IPAddress <targetAddrV6>5__2;

			// Token: 0x040012A9 RID: 4777
			private Socket <socket>5__3;

			// Token: 0x040012AA RID: 4778
			private ConfiguredTaskAwaitable<IPAddress[]>.ConfiguredTaskAwaiter <>u__1;

			// Token: 0x040012AB RID: 4779
			private ConfiguredTaskAwaitable<Socket>.ConfiguredTaskAwaiter <>u__2;

			// Token: 0x040012AC RID: 4780
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__3;
		}

		// Token: 0x02000239 RID: 569
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ParallelConnectHelper>d__24 : IAsyncStateMachine
		{
			// Token: 0x060018DB RID: 6363 RVA: 0x00080B5C File Offset: 0x0007ED5C
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				try
				{
					if (num != 0)
					{
						success = false;
					}
					try
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
						if (num != 0)
						{
							configuredTaskAwaiter = connectTask.ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter.IsCompleted)
							{
								num = (num2 = 0);
								ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, SNITCPHandle.<ParallelConnectHelper>d__24>(ref configuredTaskAwaiter, ref this);
								return;
							}
						}
						else
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num = (num2 = -1);
						}
						configuredTaskAwaiter.GetResult();
						Socket socket;
						success = tcs.TrySetResult(socket);
						if (success)
						{
							List<Socket>.Enumerator enumerator = sockets.GetEnumerator();
							try
							{
								while (enumerator.MoveNext())
								{
									socket = enumerator.Current;
									if (socket != socket)
									{
										socket.Dispose();
									}
								}
							}
							finally
							{
								if (num < 0)
								{
									((IDisposable)enumerator).Dispose();
								}
							}
						}
					}
					catch (Exception value)
					{
						Interlocked.Exchange<Exception>(ref lastError.Value, value);
					}
					finally
					{
						if (num < 0 && !success && Interlocked.Decrement(ref pendingCompleteCount.Value) == 0)
						{
							if (lastError.Value != null)
							{
								tcs.TrySetException(lastError.Value);
							}
							else
							{
								tcs.TrySetCanceled();
							}
							List<Socket>.Enumerator enumerator = sockets.GetEnumerator();
							try
							{
								while (enumerator.MoveNext())
								{
									Socket socket2 = enumerator.Current;
									socket2.Dispose();
								}
							}
							finally
							{
								if (num < 0)
								{
									((IDisposable)enumerator).Dispose();
								}
							}
						}
					}
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x060018DC RID: 6364 RVA: 0x00080D9C File Offset: 0x0007EF9C
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x040012AD RID: 4781
			public int <>1__state;

			// Token: 0x040012AE RID: 4782
			public AsyncVoidMethodBuilder <>t__builder;

			// Token: 0x040012AF RID: 4783
			public Task connectTask;

			// Token: 0x040012B0 RID: 4784
			public TaskCompletionSource<Socket> tcs;

			// Token: 0x040012B1 RID: 4785
			public Socket socket;

			// Token: 0x040012B2 RID: 4786
			public List<Socket> sockets;

			// Token: 0x040012B3 RID: 4787
			public StrongBox<Exception> lastError;

			// Token: 0x040012B4 RID: 4788
			private bool <success>5__1;

			// Token: 0x040012B5 RID: 4789
			public StrongBox<int> pendingCompleteCount;

			// Token: 0x040012B6 RID: 4790
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x0200023A RID: 570
		[CompilerGenerated]
		private sealed class <>c__DisplayClass32_0
		{
			// Token: 0x060018DD RID: 6365 RVA: 0x00005C14 File Offset: 0x00003E14
			public <>c__DisplayClass32_0()
			{
			}

			// Token: 0x060018DE RID: 6366 RVA: 0x00080DAC File Offset: 0x0007EFAC
			internal void <SendAsync>b__0()
			{
				try
				{
					SNITCPHandle obj = this.<>4__this;
					lock (obj)
					{
						this.packet.WriteToStream(this.<>4__this._stream);
					}
				}
				catch (Exception sniException)
				{
					SNILoadHandle.SingletonInstance.LastError = new SNIError(SNIProviders.TCP_PROV, 35U, sniException);
					if (this.callback != null)
					{
						this.callback(this.packet, 1U);
					}
					else
					{
						this.<>4__this._sendCallback(this.packet, 1U);
					}
					return;
				}
				if (this.callback != null)
				{
					this.callback(this.packet, 0U);
					return;
				}
				this.<>4__this._sendCallback(this.packet, 0U);
			}

			// Token: 0x040012B7 RID: 4791
			public SNITCPHandle <>4__this;

			// Token: 0x040012B8 RID: 4792
			public SNIPacket packet;

			// Token: 0x040012B9 RID: 4793
			public SNIAsyncCallback callback;
		}
	}
}
