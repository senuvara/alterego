﻿using System;

namespace System.Data.SqlClient.SNI
{
	// Token: 0x0200022D RID: 557
	internal class SNIMarsQueuedPacket
	{
		// Token: 0x0600185E RID: 6238 RVA: 0x0007EA38 File Offset: 0x0007CC38
		public SNIMarsQueuedPacket(SNIPacket packet, SNIAsyncCallback callback)
		{
			this._packet = packet;
			this._callback = callback;
		}

		// Token: 0x17000470 RID: 1136
		// (get) Token: 0x0600185F RID: 6239 RVA: 0x0007EA4E File Offset: 0x0007CC4E
		// (set) Token: 0x06001860 RID: 6240 RVA: 0x0007EA56 File Offset: 0x0007CC56
		public SNIPacket Packet
		{
			get
			{
				return this._packet;
			}
			set
			{
				this._packet = value;
			}
		}

		// Token: 0x17000471 RID: 1137
		// (get) Token: 0x06001861 RID: 6241 RVA: 0x0007EA5F File Offset: 0x0007CC5F
		// (set) Token: 0x06001862 RID: 6242 RVA: 0x0007EA67 File Offset: 0x0007CC67
		public SNIAsyncCallback Callback
		{
			get
			{
				return this._callback;
			}
			set
			{
				this._callback = value;
			}
		}

		// Token: 0x04001253 RID: 4691
		private SNIPacket _packet;

		// Token: 0x04001254 RID: 4692
		private SNIAsyncCallback _callback;
	}
}
