﻿using System;

namespace System.Data.SqlClient.SNI
{
	// Token: 0x02000222 RID: 546
	// (Invoke) Token: 0x06001819 RID: 6169
	internal delegate void SNIAsyncCallback(SNIPacket packet, uint sniErrorCode);
}
