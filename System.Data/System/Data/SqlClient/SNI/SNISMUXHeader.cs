﻿using System;

namespace System.Data.SqlClient.SNI
{
	// Token: 0x02000224 RID: 548
	internal class SNISMUXHeader
	{
		// Token: 0x0600181C RID: 6172 RVA: 0x00005C14 File Offset: 0x00003E14
		public SNISMUXHeader()
		{
		}

		// Token: 0x0400120A RID: 4618
		public const int HEADER_LENGTH = 16;

		// Token: 0x0400120B RID: 4619
		public byte SMID;

		// Token: 0x0400120C RID: 4620
		public byte flags;

		// Token: 0x0400120D RID: 4621
		public ushort sessionId;

		// Token: 0x0400120E RID: 4622
		public uint length;

		// Token: 0x0400120F RID: 4623
		public uint sequenceNumber;

		// Token: 0x04001210 RID: 4624
		public uint highwater;
	}
}
