﻿using System;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;

namespace System.Data.SqlClient.SNI
{
	// Token: 0x02000226 RID: 550
	internal class SNICommon
	{
		// Token: 0x0600181D RID: 6173 RVA: 0x0007D958 File Offset: 0x0007BB58
		internal static bool ValidateSslServerCertificate(string targetServerName, object sender, X509Certificate cert, X509Chain chain, SslPolicyErrors policyErrors)
		{
			if (policyErrors == SslPolicyErrors.None)
			{
				return true;
			}
			if ((policyErrors & SslPolicyErrors.RemoteCertificateNameMismatch) == SslPolicyErrors.None)
			{
				return false;
			}
			string text = cert.Subject.Substring(cert.Subject.IndexOf('=') + 1);
			if (targetServerName.Length > text.Length)
			{
				return false;
			}
			if (targetServerName.Length == text.Length)
			{
				if (!targetServerName.Equals(text, StringComparison.OrdinalIgnoreCase))
				{
					return false;
				}
			}
			else
			{
				if (string.Compare(targetServerName, 0, text, 0, targetServerName.Length, StringComparison.OrdinalIgnoreCase) != 0)
				{
					return false;
				}
				if (text[targetServerName.Length] != '.')
				{
					return false;
				}
			}
			return true;
		}

		// Token: 0x0600181E RID: 6174 RVA: 0x0007D9DF File Offset: 0x0007BBDF
		internal static uint ReportSNIError(SNIProviders provider, uint nativeError, uint sniError, string errorMessage)
		{
			return SNICommon.ReportSNIError(new SNIError(provider, nativeError, sniError, errorMessage));
		}

		// Token: 0x0600181F RID: 6175 RVA: 0x0007D9EF File Offset: 0x0007BBEF
		internal static uint ReportSNIError(SNIProviders provider, uint sniError, Exception sniException)
		{
			return SNICommon.ReportSNIError(new SNIError(provider, sniError, sniException));
		}

		// Token: 0x06001820 RID: 6176 RVA: 0x0007D9FE File Offset: 0x0007BBFE
		internal static uint ReportSNIError(SNIError error)
		{
			SNILoadHandle.SingletonInstance.LastError = error;
			return 1U;
		}

		// Token: 0x06001821 RID: 6177 RVA: 0x00005C14 File Offset: 0x00003E14
		public SNICommon()
		{
		}

		// Token: 0x04001216 RID: 4630
		internal const int ConnTerminatedError = 2;

		// Token: 0x04001217 RID: 4631
		internal const int InvalidParameterError = 5;

		// Token: 0x04001218 RID: 4632
		internal const int ProtocolNotSupportedError = 8;

		// Token: 0x04001219 RID: 4633
		internal const int ConnTimeoutError = 11;

		// Token: 0x0400121A RID: 4634
		internal const int ConnNotUsableError = 19;

		// Token: 0x0400121B RID: 4635
		internal const int InvalidConnStringError = 25;

		// Token: 0x0400121C RID: 4636
		internal const int HandshakeFailureError = 31;

		// Token: 0x0400121D RID: 4637
		internal const int InternalExceptionError = 35;

		// Token: 0x0400121E RID: 4638
		internal const int ConnOpenFailedError = 40;

		// Token: 0x0400121F RID: 4639
		internal const int ErrorSpnLookup = 44;

		// Token: 0x04001220 RID: 4640
		internal const int LocalDBErrorCode = 50;

		// Token: 0x04001221 RID: 4641
		internal const int MultiSubnetFailoverWithMoreThan64IPs = 47;

		// Token: 0x04001222 RID: 4642
		internal const int MultiSubnetFailoverWithInstanceSpecified = 48;

		// Token: 0x04001223 RID: 4643
		internal const int MultiSubnetFailoverWithNonTcpProtocol = 49;

		// Token: 0x04001224 RID: 4644
		internal const int MaxErrorValue = 50157;

		// Token: 0x04001225 RID: 4645
		internal const int LocalDBNoInstanceName = 51;

		// Token: 0x04001226 RID: 4646
		internal const int LocalDBNoInstallation = 52;

		// Token: 0x04001227 RID: 4647
		internal const int LocalDBInvalidConfig = 53;

		// Token: 0x04001228 RID: 4648
		internal const int LocalDBNoSqlUserInstanceDllPath = 54;

		// Token: 0x04001229 RID: 4649
		internal const int LocalDBInvalidSqlUserInstanceDllPath = 55;

		// Token: 0x0400122A RID: 4650
		internal const int LocalDBFailedToLoadDll = 56;

		// Token: 0x0400122B RID: 4651
		internal const int LocalDBBadRuntime = 57;
	}
}
