﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace System.Data.SqlClient.SNI
{
	// Token: 0x0200022C RID: 556
	internal class SNIMarsHandle : SNIHandle
	{
		// Token: 0x1700046E RID: 1134
		// (get) Token: 0x06001847 RID: 6215 RVA: 0x0007E107 File Offset: 0x0007C307
		public override Guid ConnectionId
		{
			get
			{
				return this._connectionId;
			}
		}

		// Token: 0x1700046F RID: 1135
		// (get) Token: 0x06001848 RID: 6216 RVA: 0x0007E10F File Offset: 0x0007C30F
		public override uint Status
		{
			get
			{
				return this._status;
			}
		}

		// Token: 0x06001849 RID: 6217 RVA: 0x0007E118 File Offset: 0x0007C318
		public override void Dispose()
		{
			try
			{
				this.SendControlPacket(SNISMUXFlags.SMUX_FIN);
			}
			catch (Exception sniException)
			{
				SNICommon.ReportSNIError(SNIProviders.SMUX_PROV, 35U, sniException);
				throw;
			}
		}

		// Token: 0x0600184A RID: 6218 RVA: 0x0007E14C File Offset: 0x0007C34C
		public SNIMarsHandle(SNIMarsConnection connection, ushort sessionId, object callbackObject, bool async)
		{
			this._sessionId = sessionId;
			this._connection = connection;
			this._callbackObject = callbackObject;
			this.SendControlPacket(SNISMUXFlags.SMUX_SYN);
			this._status = 0U;
		}

		// Token: 0x0600184B RID: 6219 RVA: 0x0007E1E4 File Offset: 0x0007C3E4
		private void SendControlPacket(SNISMUXFlags flags)
		{
			byte[] data = null;
			lock (this)
			{
				this.GetSMUXHeaderBytes(0, (byte)flags, ref data);
			}
			SNIPacket snipacket = new SNIPacket(null);
			snipacket.SetData(data, 16);
			this._connection.Send(snipacket);
		}

		// Token: 0x0600184C RID: 6220 RVA: 0x0007E244 File Offset: 0x0007C444
		private void GetSMUXHeaderBytes(int length, byte flags, ref byte[] headerBytes)
		{
			headerBytes = new byte[16];
			this._currentHeader.SMID = 83;
			this._currentHeader.flags = flags;
			this._currentHeader.sessionId = this._sessionId;
			this._currentHeader.length = (uint)(16 + length);
			SNISMUXHeader currentHeader = this._currentHeader;
			uint sequenceNumber2;
			if (flags != 4 && flags != 2)
			{
				uint sequenceNumber = this._sequenceNumber;
				this._sequenceNumber = sequenceNumber + 1U;
				sequenceNumber2 = sequenceNumber;
			}
			else
			{
				sequenceNumber2 = this._sequenceNumber - 1U;
			}
			currentHeader.sequenceNumber = sequenceNumber2;
			this._currentHeader.highwater = this._receiveHighwater;
			this._receiveHighwaterLastAck = this._currentHeader.highwater;
			BitConverter.GetBytes((short)this._currentHeader.SMID).CopyTo(headerBytes, 0);
			BitConverter.GetBytes((short)this._currentHeader.flags).CopyTo(headerBytes, 1);
			BitConverter.GetBytes(this._currentHeader.sessionId).CopyTo(headerBytes, 2);
			BitConverter.GetBytes(this._currentHeader.length).CopyTo(headerBytes, 4);
			BitConverter.GetBytes(this._currentHeader.sequenceNumber).CopyTo(headerBytes, 8);
			BitConverter.GetBytes(this._currentHeader.highwater).CopyTo(headerBytes, 12);
		}

		// Token: 0x0600184D RID: 6221 RVA: 0x0007E374 File Offset: 0x0007C574
		private SNIPacket GetSMUXEncapsulatedPacket(SNIPacket packet)
		{
			uint sequenceNumber = this._sequenceNumber;
			byte[] data = null;
			this.GetSMUXHeaderBytes(packet.Length, 8, ref data);
			SNIPacket snipacket = new SNIPacket(null);
			snipacket.Description = string.Format("({0}) SMUX packet {1}", (packet.Description == null) ? "" : packet.Description, sequenceNumber);
			snipacket.Allocate(16 + packet.Length);
			snipacket.AppendData(data, 16);
			snipacket.AppendPacket(packet);
			return snipacket;
		}

		// Token: 0x0600184E RID: 6222 RVA: 0x0007E3EC File Offset: 0x0007C5EC
		public override uint Send(SNIPacket packet)
		{
			for (;;)
			{
				SNIMarsHandle obj = this;
				lock (obj)
				{
					if (this._sequenceNumber < this._sendHighwater)
					{
						break;
					}
				}
				this._ackEvent.Wait();
				obj = this;
				lock (obj)
				{
					this._ackEvent.Reset();
					continue;
				}
				break;
			}
			return this._connection.Send(this.GetSMUXEncapsulatedPacket(packet));
		}

		// Token: 0x0600184F RID: 6223 RVA: 0x0007E480 File Offset: 0x0007C680
		private uint InternalSendAsync(SNIPacket packet, SNIAsyncCallback callback)
		{
			uint result;
			lock (this)
			{
				if (this._sequenceNumber >= this._sendHighwater)
				{
					result = 1048576U;
				}
				else
				{
					SNIPacket smuxencapsulatedPacket = this.GetSMUXEncapsulatedPacket(packet);
					if (callback != null)
					{
						smuxencapsulatedPacket.SetCompletionCallback(callback);
					}
					else
					{
						smuxencapsulatedPacket.SetCompletionCallback(new SNIAsyncCallback(this.HandleSendComplete));
					}
					result = this._connection.SendAsync(smuxencapsulatedPacket, callback);
				}
			}
			return result;
		}

		// Token: 0x06001850 RID: 6224 RVA: 0x0007E504 File Offset: 0x0007C704
		private uint SendPendingPackets()
		{
			for (;;)
			{
				lock (this)
				{
					if (this._sequenceNumber < this._sendHighwater)
					{
						if (this._sendPacketQueue.Count != 0)
						{
							SNIMarsQueuedPacket snimarsQueuedPacket = this._sendPacketQueue.Peek();
							uint num = this.InternalSendAsync(snimarsQueuedPacket.Packet, snimarsQueuedPacket.Callback);
							if (num != 0U && num != 997U)
							{
								return num;
							}
							this._sendPacketQueue.Dequeue();
							continue;
						}
						else
						{
							this._ackEvent.Set();
						}
					}
				}
				break;
			}
			return 0U;
		}

		// Token: 0x06001851 RID: 6225 RVA: 0x0007E5A4 File Offset: 0x0007C7A4
		public override uint SendAsync(SNIPacket packet, SNIAsyncCallback callback = null)
		{
			lock (this)
			{
				this._sendPacketQueue.Enqueue(new SNIMarsQueuedPacket(packet, (callback != null) ? callback : new SNIAsyncCallback(this.HandleSendComplete)));
			}
			this.SendPendingPackets();
			return 997U;
		}

		// Token: 0x06001852 RID: 6226 RVA: 0x0007E608 File Offset: 0x0007C808
		public override uint ReceiveAsync(ref SNIPacket packet)
		{
			Queue<SNIPacket> receivedPacketQueue = this._receivedPacketQueue;
			lock (receivedPacketQueue)
			{
				int count = this._receivedPacketQueue.Count;
				if (this._connectionError != null)
				{
					return SNICommon.ReportSNIError(this._connectionError);
				}
				if (count == 0)
				{
					this._asyncReceives++;
					return 997U;
				}
				packet = this._receivedPacketQueue.Dequeue();
				if (count == 1)
				{
					this._packetEvent.Reset();
				}
			}
			lock (this)
			{
				this._receiveHighwater += 1U;
			}
			this.SendAckIfNecessary();
			return 0U;
		}

		// Token: 0x06001853 RID: 6227 RVA: 0x0007E6D8 File Offset: 0x0007C8D8
		public void HandleReceiveError(SNIPacket packet)
		{
			Queue<SNIPacket> receivedPacketQueue = this._receivedPacketQueue;
			lock (receivedPacketQueue)
			{
				this._connectionError = SNILoadHandle.SingletonInstance.LastError;
				this._packetEvent.Set();
			}
			((TdsParserStateObject)this._callbackObject).ReadAsyncCallback<SNIPacket>(packet, 1U);
		}

		// Token: 0x06001854 RID: 6228 RVA: 0x0007E740 File Offset: 0x0007C940
		public void HandleSendComplete(SNIPacket packet, uint sniErrorCode)
		{
			lock (this)
			{
				((TdsParserStateObject)this._callbackObject).WriteAsyncCallback<SNIPacket>(packet, sniErrorCode);
			}
		}

		// Token: 0x06001855 RID: 6229 RVA: 0x0007E788 File Offset: 0x0007C988
		public void HandleAck(uint highwater)
		{
			lock (this)
			{
				if (this._sendHighwater != highwater)
				{
					this._sendHighwater = highwater;
					this.SendPendingPackets();
				}
			}
		}

		// Token: 0x06001856 RID: 6230 RVA: 0x0007E7D4 File Offset: 0x0007C9D4
		public void HandleReceiveComplete(SNIPacket packet, SNISMUXHeader header)
		{
			SNIMarsHandle obj = this;
			lock (obj)
			{
				if (this._sendHighwater != header.highwater)
				{
					this.HandleAck(header.highwater);
				}
				Queue<SNIPacket> receivedPacketQueue = this._receivedPacketQueue;
				lock (receivedPacketQueue)
				{
					if (this._asyncReceives == 0)
					{
						this._receivedPacketQueue.Enqueue(packet);
						this._packetEvent.Set();
						return;
					}
					this._asyncReceives--;
					((TdsParserStateObject)this._callbackObject).ReadAsyncCallback<SNIPacket>(packet, 0U);
				}
			}
			obj = this;
			lock (obj)
			{
				this._receiveHighwater += 1U;
			}
			this.SendAckIfNecessary();
		}

		// Token: 0x06001857 RID: 6231 RVA: 0x0007E8C4 File Offset: 0x0007CAC4
		private void SendAckIfNecessary()
		{
			uint receiveHighwater;
			uint receiveHighwaterLastAck;
			lock (this)
			{
				receiveHighwater = this._receiveHighwater;
				receiveHighwaterLastAck = this._receiveHighwaterLastAck;
			}
			if (receiveHighwater - receiveHighwaterLastAck > 2U)
			{
				this.SendControlPacket(SNISMUXFlags.SMUX_ACK);
			}
		}

		// Token: 0x06001858 RID: 6232 RVA: 0x0007E914 File Offset: 0x0007CB14
		public override uint Receive(out SNIPacket packet, int timeoutInMilliseconds)
		{
			packet = null;
			uint num = 997U;
			for (;;)
			{
				Queue<SNIPacket> receivedPacketQueue = this._receivedPacketQueue;
				lock (receivedPacketQueue)
				{
					if (this._connectionError != null)
					{
						return SNICommon.ReportSNIError(this._connectionError);
					}
					int count = this._receivedPacketQueue.Count;
					if (count > 0)
					{
						packet = this._receivedPacketQueue.Dequeue();
						if (count == 1)
						{
							this._packetEvent.Reset();
						}
						num = 0U;
					}
				}
				if (num == 0U)
				{
					break;
				}
				if (!this._packetEvent.Wait(timeoutInMilliseconds))
				{
					goto Block_4;
				}
			}
			lock (this)
			{
				this._receiveHighwater += 1U;
			}
			this.SendAckIfNecessary();
			return num;
			Block_4:
			SNILoadHandle.SingletonInstance.LastError = new SNIError(SNIProviders.SMUX_PROV, 0U, 11U, string.Empty);
			return 258U;
		}

		// Token: 0x06001859 RID: 6233 RVA: 0x0007EA10 File Offset: 0x0007CC10
		public override uint CheckConnection()
		{
			return this._connection.CheckConnection();
		}

		// Token: 0x0600185A RID: 6234 RVA: 0x00005E03 File Offset: 0x00004003
		public override void SetAsyncCallbacks(SNIAsyncCallback receiveCallback, SNIAsyncCallback sendCallback)
		{
		}

		// Token: 0x0600185B RID: 6235 RVA: 0x00005E03 File Offset: 0x00004003
		public override void SetBufferSize(int bufferSize)
		{
		}

		// Token: 0x0600185C RID: 6236 RVA: 0x0007EA1D File Offset: 0x0007CC1D
		public override uint EnableSsl(uint options)
		{
			return this._connection.EnableSsl(options);
		}

		// Token: 0x0600185D RID: 6237 RVA: 0x0007EA2B File Offset: 0x0007CC2B
		public override void DisableSsl()
		{
			this._connection.DisableSsl();
		}

		// Token: 0x04001242 RID: 4674
		private const uint ACK_THRESHOLD = 2U;

		// Token: 0x04001243 RID: 4675
		private readonly SNIMarsConnection _connection;

		// Token: 0x04001244 RID: 4676
		private readonly uint _status = uint.MaxValue;

		// Token: 0x04001245 RID: 4677
		private readonly Queue<SNIPacket> _receivedPacketQueue = new Queue<SNIPacket>();

		// Token: 0x04001246 RID: 4678
		private readonly Queue<SNIMarsQueuedPacket> _sendPacketQueue = new Queue<SNIMarsQueuedPacket>();

		// Token: 0x04001247 RID: 4679
		private readonly object _callbackObject;

		// Token: 0x04001248 RID: 4680
		private readonly Guid _connectionId = Guid.NewGuid();

		// Token: 0x04001249 RID: 4681
		private readonly ushort _sessionId;

		// Token: 0x0400124A RID: 4682
		private readonly ManualResetEventSlim _packetEvent = new ManualResetEventSlim(false);

		// Token: 0x0400124B RID: 4683
		private readonly ManualResetEventSlim _ackEvent = new ManualResetEventSlim(false);

		// Token: 0x0400124C RID: 4684
		private readonly SNISMUXHeader _currentHeader = new SNISMUXHeader();

		// Token: 0x0400124D RID: 4685
		private uint _sendHighwater = 4U;

		// Token: 0x0400124E RID: 4686
		private int _asyncReceives;

		// Token: 0x0400124F RID: 4687
		private uint _receiveHighwater = 4U;

		// Token: 0x04001250 RID: 4688
		private uint _receiveHighwaterLastAck = 4U;

		// Token: 0x04001251 RID: 4689
		private uint _sequenceNumber;

		// Token: 0x04001252 RID: 4690
		private SNIError _connectionError;
	}
}
