﻿using System;

namespace System.Data.SqlClient.SNI
{
	// Token: 0x02000225 RID: 549
	[Flags]
	internal enum SNISMUXFlags
	{
		// Token: 0x04001212 RID: 4626
		SMUX_SYN = 1,
		// Token: 0x04001213 RID: 4627
		SMUX_ACK = 2,
		// Token: 0x04001214 RID: 4628
		SMUX_FIN = 4,
		// Token: 0x04001215 RID: 4629
		SMUX_DATA = 8
	}
}
