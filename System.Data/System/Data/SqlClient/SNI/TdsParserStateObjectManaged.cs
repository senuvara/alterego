﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Threading.Tasks;

namespace System.Data.SqlClient.SNI
{
	// Token: 0x0200023E RID: 574
	internal class TdsParserStateObjectManaged : TdsParserStateObject
	{
		// Token: 0x060018F9 RID: 6393 RVA: 0x000812A2 File Offset: 0x0007F4A2
		public TdsParserStateObjectManaged(TdsParser parser) : base(parser)
		{
		}

		// Token: 0x060018FA RID: 6394 RVA: 0x000812CC File Offset: 0x0007F4CC
		internal TdsParserStateObjectManaged(TdsParser parser, TdsParserStateObject physicalConnection, bool async) : base(parser, physicalConnection, async)
		{
		}

		// Token: 0x17000489 RID: 1161
		// (get) Token: 0x060018FB RID: 6395 RVA: 0x000812F8 File Offset: 0x0007F4F8
		internal SNIHandle Handle
		{
			get
			{
				return this._sessionHandle;
			}
		}

		// Token: 0x1700048A RID: 1162
		// (get) Token: 0x060018FC RID: 6396 RVA: 0x00081300 File Offset: 0x0007F500
		internal override uint Status
		{
			get
			{
				if (this._sessionHandle == null)
				{
					return uint.MaxValue;
				}
				return this._sessionHandle.Status;
			}
		}

		// Token: 0x1700048B RID: 1163
		// (get) Token: 0x060018FD RID: 6397 RVA: 0x000812F8 File Offset: 0x0007F4F8
		internal override object SessionHandle
		{
			get
			{
				return this._sessionHandle;
			}
		}

		// Token: 0x1700048C RID: 1164
		// (get) Token: 0x060018FE RID: 6398 RVA: 0x00004526 File Offset: 0x00002726
		protected override object EmptyReadPacket
		{
			get
			{
				return null;
			}
		}

		// Token: 0x060018FF RID: 6399 RVA: 0x00081318 File Offset: 0x0007F518
		protected override bool CheckPacket(object packet, TaskCompletionSource<object> source)
		{
			SNIPacket snipacket = packet as SNIPacket;
			return snipacket.IsInvalid || (!snipacket.IsInvalid && source != null);
		}

		// Token: 0x06001900 RID: 6400 RVA: 0x00081344 File Offset: 0x0007F544
		protected override void CreateSessionHandle(TdsParserStateObject physicalConnection, bool async)
		{
			TdsParserStateObjectManaged tdsParserStateObjectManaged = physicalConnection as TdsParserStateObjectManaged;
			this._sessionHandle = tdsParserStateObjectManaged.CreateMarsSession(this, async);
		}

		// Token: 0x06001901 RID: 6401 RVA: 0x00081366 File Offset: 0x0007F566
		internal SNIMarsHandle CreateMarsSession(object callbackObject, bool async)
		{
			return this._marsConnection.CreateMarsSession(callbackObject, async);
		}

		// Token: 0x06001902 RID: 6402 RVA: 0x00081375 File Offset: 0x0007F575
		protected override uint SNIPacketGetData(object packet, byte[] _inBuff, ref uint dataSize)
		{
			return SNIProxy.Singleton.PacketGetData(packet as SNIPacket, _inBuff, ref dataSize);
		}

		// Token: 0x06001903 RID: 6403 RVA: 0x0008138C File Offset: 0x0007F58C
		internal override void CreatePhysicalSNIHandle(string serverName, bool ignoreSniOpenTimeout, long timerExpire, out byte[] instanceName, ref byte[] spnBuffer, bool flushCache, bool async, bool parallel, bool isIntegratedSecurity)
		{
			this._sessionHandle = SNIProxy.Singleton.CreateConnectionHandle(this, serverName, ignoreSniOpenTimeout, timerExpire, out instanceName, ref spnBuffer, flushCache, async, parallel, isIntegratedSecurity);
			if (this._sessionHandle == null)
			{
				this._parser.ProcessSNIError(this);
				return;
			}
			if (async)
			{
				SNIAsyncCallback receiveCallback = new SNIAsyncCallback(this.ReadAsyncCallback);
				SNIAsyncCallback sendCallback = new SNIAsyncCallback(this.WriteAsyncCallback);
				this._sessionHandle.SetAsyncCallbacks(receiveCallback, sendCallback);
			}
		}

		// Token: 0x06001904 RID: 6404 RVA: 0x000813FA File Offset: 0x0007F5FA
		internal void ReadAsyncCallback(SNIPacket packet, uint error)
		{
			base.ReadAsyncCallback<SNIPacket>(IntPtr.Zero, packet, error);
		}

		// Token: 0x06001905 RID: 6405 RVA: 0x00081409 File Offset: 0x0007F609
		internal void WriteAsyncCallback(SNIPacket packet, uint sniError)
		{
			base.WriteAsyncCallback<SNIPacket>(IntPtr.Zero, packet, sniError);
		}

		// Token: 0x06001906 RID: 6406 RVA: 0x00005E03 File Offset: 0x00004003
		protected override void RemovePacketFromPendingList(object packet)
		{
		}

		// Token: 0x06001907 RID: 6407 RVA: 0x00081418 File Offset: 0x0007F618
		internal override void Dispose()
		{
			SNIPacket sniPacket = this._sniPacket;
			SNIHandle sessionHandle = this._sessionHandle;
			SNIPacket sniAsyncAttnPacket = this._sniAsyncAttnPacket;
			this._sniPacket = null;
			this._sessionHandle = null;
			this._sniAsyncAttnPacket = null;
			this._marsConnection = null;
			base.DisposeCounters();
			if (sessionHandle != null || sniPacket != null)
			{
				if (sniPacket != null)
				{
					sniPacket.Dispose();
				}
				if (sniAsyncAttnPacket != null)
				{
					sniAsyncAttnPacket.Dispose();
				}
				if (sessionHandle != null)
				{
					sessionHandle.Dispose();
					base.DecrementPendingCallbacks(true);
				}
			}
			this.DisposePacketCache();
		}

		// Token: 0x06001908 RID: 6408 RVA: 0x0008148C File Offset: 0x0007F68C
		internal override void DisposePacketCache()
		{
			object writePacketLockObject = this._writePacketLockObject;
			lock (writePacketLockObject)
			{
				this._writePacketCache.Dispose();
			}
		}

		// Token: 0x06001909 RID: 6409 RVA: 0x00005E03 File Offset: 0x00004003
		protected override void FreeGcHandle(int remaining, bool release)
		{
		}

		// Token: 0x0600190A RID: 6410 RVA: 0x000814D4 File Offset: 0x0007F6D4
		internal override bool IsFailedHandle()
		{
			return this._sessionHandle.Status > 0U;
		}

		// Token: 0x0600190B RID: 6411 RVA: 0x000814E4 File Offset: 0x0007F6E4
		internal override object ReadSyncOverAsync(int timeoutRemaining, bool isMarsOn, out uint error)
		{
			SNIHandle handle = this.Handle;
			if (handle == null)
			{
				throw ADP.ClosedConnectionError();
			}
			if (isMarsOn)
			{
				base.IncrementPendingCallbacks();
			}
			SNIPacket result = null;
			error = SNIProxy.Singleton.ReadSyncOverAsync(handle, out result, timeoutRemaining);
			return result;
		}

		// Token: 0x0600190C RID: 6412 RVA: 0x0008151E File Offset: 0x0007F71E
		internal override bool IsPacketEmpty(object packet)
		{
			return packet == null;
		}

		// Token: 0x0600190D RID: 6413 RVA: 0x00081524 File Offset: 0x0007F724
		internal override void ReleasePacket(object syncReadPacket)
		{
			((SNIPacket)syncReadPacket).Dispose();
		}

		// Token: 0x0600190E RID: 6414 RVA: 0x00081534 File Offset: 0x0007F734
		internal override uint CheckConnection()
		{
			SNIHandle handle = this.Handle;
			if (handle != null)
			{
				return SNIProxy.Singleton.CheckConnection(handle);
			}
			return 0U;
		}

		// Token: 0x0600190F RID: 6415 RVA: 0x00081558 File Offset: 0x0007F758
		internal override object ReadAsync(out uint error, ref object handle)
		{
			SNIPacket result;
			error = SNIProxy.Singleton.ReadAsync((SNIHandle)handle, out result);
			return result;
		}

		// Token: 0x06001910 RID: 6416 RVA: 0x0008157C File Offset: 0x0007F77C
		internal override object CreateAndSetAttentionPacket()
		{
			SNIPacket snipacket = new SNIPacket(this.Handle);
			this._sniAsyncAttnPacket = snipacket;
			this.SetPacketData(snipacket, SQL.AttentionHeader, 8);
			return snipacket;
		}

		// Token: 0x06001911 RID: 6417 RVA: 0x000815AA File Offset: 0x0007F7AA
		internal override uint WritePacket(object packet, bool sync)
		{
			return SNIProxy.Singleton.WritePacket(this.Handle, (SNIPacket)packet, sync);
		}

		// Token: 0x06001912 RID: 6418 RVA: 0x00005DA6 File Offset: 0x00003FA6
		internal override object AddPacketToPendingList(object packet)
		{
			return packet;
		}

		// Token: 0x06001913 RID: 6419 RVA: 0x000815C3 File Offset: 0x0007F7C3
		internal override bool IsValidPacket(object packetPointer)
		{
			return (SNIPacket)packetPointer != null && !((SNIPacket)packetPointer).IsInvalid;
		}

		// Token: 0x06001914 RID: 6420 RVA: 0x000815E0 File Offset: 0x0007F7E0
		internal override object GetResetWritePacket()
		{
			if (this._sniPacket != null)
			{
				this._sniPacket.Reset();
			}
			else
			{
				object writePacketLockObject = this._writePacketLockObject;
				lock (writePacketLockObject)
				{
					this._sniPacket = this._writePacketCache.Take(this.Handle);
				}
			}
			return this._sniPacket;
		}

		// Token: 0x06001915 RID: 6421 RVA: 0x0008164C File Offset: 0x0007F84C
		internal override void ClearAllWritePackets()
		{
			if (this._sniPacket != null)
			{
				this._sniPacket.Dispose();
				this._sniPacket = null;
			}
			object writePacketLockObject = this._writePacketLockObject;
			lock (writePacketLockObject)
			{
				this._writePacketCache.Clear();
			}
		}

		// Token: 0x06001916 RID: 6422 RVA: 0x000816AC File Offset: 0x0007F8AC
		internal override void SetPacketData(object packet, byte[] buffer, int bytesUsed)
		{
			SNIProxy.Singleton.PacketSetData((SNIPacket)packet, buffer, bytesUsed);
		}

		// Token: 0x06001917 RID: 6423 RVA: 0x000816C0 File Offset: 0x0007F8C0
		internal override uint SniGetConnectionId(ref Guid clientConnectionId)
		{
			return SNIProxy.Singleton.GetConnectionId(this.Handle, ref clientConnectionId);
		}

		// Token: 0x06001918 RID: 6424 RVA: 0x000816D3 File Offset: 0x0007F8D3
		internal override uint DisabeSsl()
		{
			return SNIProxy.Singleton.DisableSsl(this.Handle);
		}

		// Token: 0x06001919 RID: 6425 RVA: 0x000816E5 File Offset: 0x0007F8E5
		internal override uint EnableMars(ref uint info)
		{
			this._marsConnection = new SNIMarsConnection(this.Handle);
			if (this._marsConnection.StartReceive() == 997U)
			{
				return 0U;
			}
			return 1U;
		}

		// Token: 0x0600191A RID: 6426 RVA: 0x0008170D File Offset: 0x0007F90D
		internal override uint EnableSsl(ref uint info)
		{
			return SNIProxy.Singleton.EnableSsl(this.Handle, info);
		}

		// Token: 0x0600191B RID: 6427 RVA: 0x00081721 File Offset: 0x0007F921
		internal override uint SetConnectionBufferSize(ref uint unsignedPacketSize)
		{
			return SNIProxy.Singleton.SetConnectionBufferSize(this.Handle, unsignedPacketSize);
		}

		// Token: 0x0600191C RID: 6428 RVA: 0x00081735 File Offset: 0x0007F935
		internal override uint GenerateSspiClientContext(byte[] receivedBuff, uint receivedLength, ref byte[] sendBuff, ref uint sendLength, byte[] _sniSpnBuffer)
		{
			SNIProxy.Singleton.GenSspiClientContext(this.sspiClientContextStatus, receivedBuff, ref sendBuff, _sniSpnBuffer);
			sendLength = (uint)((sendBuff != null) ? sendBuff.Length : 0);
			return 0U;
		}

		// Token: 0x0600191D RID: 6429 RVA: 0x000061C5 File Offset: 0x000043C5
		internal override uint WaitForSSLHandShakeToComplete()
		{
			return 0U;
		}

		// Token: 0x040012C4 RID: 4804
		private SNIMarsConnection _marsConnection;

		// Token: 0x040012C5 RID: 4805
		private SNIHandle _sessionHandle;

		// Token: 0x040012C6 RID: 4806
		private SNIPacket _sniPacket;

		// Token: 0x040012C7 RID: 4807
		internal SNIPacket _sniAsyncAttnPacket;

		// Token: 0x040012C8 RID: 4808
		private readonly Dictionary<SNIPacket, SNIPacket> _pendingWritePackets = new Dictionary<SNIPacket, SNIPacket>();

		// Token: 0x040012C9 RID: 4809
		private readonly TdsParserStateObjectManaged.WritePacketCache _writePacketCache = new TdsParserStateObjectManaged.WritePacketCache();

		// Token: 0x040012CA RID: 4810
		internal SspiClientContextStatus sspiClientContextStatus = new SspiClientContextStatus();

		// Token: 0x0200023F RID: 575
		internal sealed class WritePacketCache : IDisposable
		{
			// Token: 0x0600191E RID: 6430 RVA: 0x0008175A File Offset: 0x0007F95A
			public WritePacketCache()
			{
				this._disposed = false;
				this._packets = new Stack<SNIPacket>();
			}

			// Token: 0x0600191F RID: 6431 RVA: 0x00081774 File Offset: 0x0007F974
			public SNIPacket Take(SNIHandle sniHandle)
			{
				SNIPacket snipacket;
				if (this._packets.Count > 0)
				{
					snipacket = this._packets.Pop();
					snipacket.Reset();
				}
				else
				{
					snipacket = new SNIPacket(sniHandle);
				}
				return snipacket;
			}

			// Token: 0x06001920 RID: 6432 RVA: 0x000817AB File Offset: 0x0007F9AB
			public void Add(SNIPacket packet)
			{
				if (!this._disposed)
				{
					this._packets.Push(packet);
					return;
				}
				packet.Dispose();
			}

			// Token: 0x06001921 RID: 6433 RVA: 0x000817C8 File Offset: 0x0007F9C8
			public void Clear()
			{
				while (this._packets.Count > 0)
				{
					this._packets.Pop().Dispose();
				}
			}

			// Token: 0x06001922 RID: 6434 RVA: 0x000817EA File Offset: 0x0007F9EA
			public void Dispose()
			{
				if (!this._disposed)
				{
					this._disposed = true;
					this.Clear();
				}
			}

			// Token: 0x040012CB RID: 4811
			private bool _disposed;

			// Token: 0x040012CC RID: 4812
			private Stack<SNIPacket> _packets;
		}
	}
}
