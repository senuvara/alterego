﻿using System;

namespace System.Data.SqlClient.SNI
{
	// Token: 0x02000223 RID: 547
	internal enum SNIProviders
	{
		// Token: 0x04001200 RID: 4608
		HTTP_PROV,
		// Token: 0x04001201 RID: 4609
		NP_PROV,
		// Token: 0x04001202 RID: 4610
		SESSION_PROV,
		// Token: 0x04001203 RID: 4611
		SIGN_PROV,
		// Token: 0x04001204 RID: 4612
		SM_PROV,
		// Token: 0x04001205 RID: 4613
		SMUX_PROV,
		// Token: 0x04001206 RID: 4614
		SSL_PROV,
		// Token: 0x04001207 RID: 4615
		TCP_PROV,
		// Token: 0x04001208 RID: 4616
		MAX_PROVS,
		// Token: 0x04001209 RID: 4617
		INVALID_PROV
	}
}
