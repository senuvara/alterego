﻿using System;
using System.IO;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;

namespace System.Data.SqlClient.SNI
{
	// Token: 0x02000230 RID: 560
	internal class SNIPacket : IDisposable, IEquatable<SNIPacket>
	{
		// Token: 0x06001875 RID: 6261 RVA: 0x0007F0D0 File Offset: 0x0007D2D0
		public SNIPacket(SNIHandle handle)
		{
			this._offset = 0;
		}

		// Token: 0x17000474 RID: 1140
		// (get) Token: 0x06001876 RID: 6262 RVA: 0x0007F0DF File Offset: 0x0007D2DF
		// (set) Token: 0x06001877 RID: 6263 RVA: 0x0007F0E7 File Offset: 0x0007D2E7
		public string Description
		{
			get
			{
				return this._description;
			}
			set
			{
				this._description = value;
			}
		}

		// Token: 0x17000475 RID: 1141
		// (get) Token: 0x06001878 RID: 6264 RVA: 0x0007F0F0 File Offset: 0x0007D2F0
		public int DataLeft
		{
			get
			{
				return this._length - this._offset;
			}
		}

		// Token: 0x17000476 RID: 1142
		// (get) Token: 0x06001879 RID: 6265 RVA: 0x0007F0FF File Offset: 0x0007D2FF
		public int Length
		{
			get
			{
				return this._length;
			}
		}

		// Token: 0x17000477 RID: 1143
		// (get) Token: 0x0600187A RID: 6266 RVA: 0x0007F107 File Offset: 0x0007D307
		public bool IsInvalid
		{
			get
			{
				return this._data == null;
			}
		}

		// Token: 0x0600187B RID: 6267 RVA: 0x0007F112 File Offset: 0x0007D312
		public void Dispose()
		{
			this._data = null;
			this._length = 0;
			this._capacity = 0;
		}

		// Token: 0x0600187C RID: 6268 RVA: 0x0007F129 File Offset: 0x0007D329
		public void SetCompletionCallback(SNIAsyncCallback completionCallback)
		{
			this._completionCallback = completionCallback;
		}

		// Token: 0x0600187D RID: 6269 RVA: 0x0007F132 File Offset: 0x0007D332
		public void InvokeCompletionCallback(uint sniErrorCode)
		{
			this._completionCallback(this, sniErrorCode);
		}

		// Token: 0x0600187E RID: 6270 RVA: 0x0007F141 File Offset: 0x0007D341
		public void Allocate(int capacity)
		{
			this._capacity = capacity;
			this._data = new byte[capacity];
		}

		// Token: 0x0600187F RID: 6271 RVA: 0x0007F158 File Offset: 0x0007D358
		public SNIPacket Clone()
		{
			SNIPacket snipacket = new SNIPacket(null);
			snipacket._data = new byte[this._length];
			Buffer.BlockCopy(this._data, 0, snipacket._data, 0, this._length);
			snipacket._length = this._length;
			return snipacket;
		}

		// Token: 0x06001880 RID: 6272 RVA: 0x0007F1A3 File Offset: 0x0007D3A3
		public void GetData(byte[] buffer, ref int dataSize)
		{
			Buffer.BlockCopy(this._data, 0, buffer, 0, this._length);
			dataSize = this._length;
		}

		// Token: 0x06001881 RID: 6273 RVA: 0x0007F1C1 File Offset: 0x0007D3C1
		public void SetData(byte[] data, int length)
		{
			this._data = data;
			this._length = length;
			this._capacity = length;
			this._offset = 0;
		}

		// Token: 0x06001882 RID: 6274 RVA: 0x0007F1E0 File Offset: 0x0007D3E0
		public int TakeData(SNIPacket packet, int size)
		{
			int num = this.TakeData(packet._data, packet._length, size);
			packet._length += num;
			return num;
		}

		// Token: 0x06001883 RID: 6275 RVA: 0x0007F210 File Offset: 0x0007D410
		public void AppendData(byte[] data, int size)
		{
			Buffer.BlockCopy(data, 0, this._data, this._length, size);
			this._length += size;
		}

		// Token: 0x06001884 RID: 6276 RVA: 0x0007F234 File Offset: 0x0007D434
		public void AppendPacket(SNIPacket packet)
		{
			Buffer.BlockCopy(packet._data, 0, this._data, this._length, packet._length);
			this._length += packet._length;
		}

		// Token: 0x06001885 RID: 6277 RVA: 0x0007F268 File Offset: 0x0007D468
		public int TakeData(byte[] buffer, int dataOffset, int size)
		{
			if (this._offset >= this._length)
			{
				return 0;
			}
			if (this._offset + size > this._length)
			{
				size = this._length - this._offset;
			}
			Buffer.BlockCopy(this._data, this._offset, buffer, dataOffset, size);
			this._offset += size;
			return size;
		}

		// Token: 0x06001886 RID: 6278 RVA: 0x0007F2C7 File Offset: 0x0007D4C7
		public void Release()
		{
			this._length = 0;
			this._capacity = 0;
			this._data = null;
		}

		// Token: 0x06001887 RID: 6279 RVA: 0x0007F2DE File Offset: 0x0007D4DE
		public void Reset()
		{
			this._length = 0;
			this._data = new byte[this._capacity];
		}

		// Token: 0x06001888 RID: 6280 RVA: 0x0007F2F8 File Offset: 0x0007D4F8
		public void ReadFromStreamAsync(Stream stream, SNIAsyncCallback callback)
		{
			bool error = false;
			stream.ReadAsync(this._data, 0, this._capacity).ContinueWith(delegate(Task<int> t)
			{
				Exception ex = (t.Exception != null) ? t.Exception.InnerException : null;
				if (ex != null)
				{
					SNILoadHandle.SingletonInstance.LastError = new SNIError(SNIProviders.TCP_PROV, 35U, ex);
					error = true;
				}
				else
				{
					this._length = t.Result;
					if (this._length == 0)
					{
						SNILoadHandle.SingletonInstance.LastError = new SNIError(SNIProviders.TCP_PROV, 0U, 2U, string.Empty);
						error = true;
					}
				}
				if (error)
				{
					this.Release();
				}
				callback(this, error ? 1U : 0U);
			}, CancellationToken.None, TaskContinuationOptions.LongRunning | TaskContinuationOptions.DenyChildAttach, TaskScheduler.Default);
		}

		// Token: 0x06001889 RID: 6281 RVA: 0x0007F351 File Offset: 0x0007D551
		public void ReadFromStream(Stream stream)
		{
			this._length = stream.Read(this._data, 0, this._capacity);
		}

		// Token: 0x0600188A RID: 6282 RVA: 0x0007F36C File Offset: 0x0007D56C
		public void WriteToStream(Stream stream)
		{
			stream.Write(this._data, 0, this._length);
		}

		// Token: 0x0600188B RID: 6283 RVA: 0x0003514E File Offset: 0x0003334E
		public override int GetHashCode()
		{
			return base.GetHashCode();
		}

		// Token: 0x0600188C RID: 6284 RVA: 0x0007F384 File Offset: 0x0007D584
		public override bool Equals(object obj)
		{
			SNIPacket snipacket = obj as SNIPacket;
			return snipacket != null && this.Equals(snipacket);
		}

		// Token: 0x0600188D RID: 6285 RVA: 0x0007F3A4 File Offset: 0x0007D5A4
		public bool Equals(SNIPacket packet)
		{
			return packet != null && packet == this;
		}

		// Token: 0x04001268 RID: 4712
		private byte[] _data;

		// Token: 0x04001269 RID: 4713
		private int _length;

		// Token: 0x0400126A RID: 4714
		private int _capacity;

		// Token: 0x0400126B RID: 4715
		private int _offset;

		// Token: 0x0400126C RID: 4716
		private string _description;

		// Token: 0x0400126D RID: 4717
		private SNIAsyncCallback _completionCallback;

		// Token: 0x02000231 RID: 561
		[CompilerGenerated]
		private sealed class <>c__DisplayClass29_0
		{
			// Token: 0x0600188E RID: 6286 RVA: 0x00005C14 File Offset: 0x00003E14
			public <>c__DisplayClass29_0()
			{
			}

			// Token: 0x0600188F RID: 6287 RVA: 0x0007F3B0 File Offset: 0x0007D5B0
			internal void <ReadFromStreamAsync>b__0(Task<int> t)
			{
				Exception ex = (t.Exception != null) ? t.Exception.InnerException : null;
				if (ex != null)
				{
					SNILoadHandle.SingletonInstance.LastError = new SNIError(SNIProviders.TCP_PROV, 35U, ex);
					this.error = true;
				}
				else
				{
					this.<>4__this._length = t.Result;
					if (this.<>4__this._length == 0)
					{
						SNILoadHandle.SingletonInstance.LastError = new SNIError(SNIProviders.TCP_PROV, 0U, 2U, string.Empty);
						this.error = true;
					}
				}
				if (this.error)
				{
					this.<>4__this.Release();
				}
				this.callback(this.<>4__this, this.error ? 1U : 0U);
			}

			// Token: 0x0400126E RID: 4718
			public bool error;

			// Token: 0x0400126F RID: 4719
			public SNIPacket <>4__this;

			// Token: 0x04001270 RID: 4720
			public SNIAsyncCallback callback;
		}
	}
}
