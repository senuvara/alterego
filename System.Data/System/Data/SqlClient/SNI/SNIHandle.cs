﻿using System;

namespace System.Data.SqlClient.SNI
{
	// Token: 0x02000228 RID: 552
	internal abstract class SNIHandle
	{
		// Token: 0x06001824 RID: 6180
		public abstract void Dispose();

		// Token: 0x06001825 RID: 6181
		public abstract void SetAsyncCallbacks(SNIAsyncCallback receiveCallback, SNIAsyncCallback sendCallback);

		// Token: 0x06001826 RID: 6182
		public abstract void SetBufferSize(int bufferSize);

		// Token: 0x06001827 RID: 6183
		public abstract uint Send(SNIPacket packet);

		// Token: 0x06001828 RID: 6184
		public abstract uint SendAsync(SNIPacket packet, SNIAsyncCallback callback = null);

		// Token: 0x06001829 RID: 6185
		public abstract uint Receive(out SNIPacket packet, int timeoutInMilliseconds);

		// Token: 0x0600182A RID: 6186
		public abstract uint ReceiveAsync(ref SNIPacket packet);

		// Token: 0x0600182B RID: 6187
		public abstract uint EnableSsl(uint options);

		// Token: 0x0600182C RID: 6188
		public abstract void DisableSsl();

		// Token: 0x0600182D RID: 6189
		public abstract uint CheckConnection();

		// Token: 0x17000468 RID: 1128
		// (get) Token: 0x0600182E RID: 6190
		public abstract uint Status { get; }

		// Token: 0x17000469 RID: 1129
		// (get) Token: 0x0600182F RID: 6191
		public abstract Guid ConnectionId { get; }

		// Token: 0x06001830 RID: 6192 RVA: 0x00005C14 File Offset: 0x00003E14
		protected SNIHandle()
		{
		}
	}
}
