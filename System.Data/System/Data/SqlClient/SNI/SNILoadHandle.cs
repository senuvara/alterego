﻿using System;
using System.Runtime.CompilerServices;
using System.Threading;

namespace System.Data.SqlClient.SNI
{
	// Token: 0x02000229 RID: 553
	internal class SNILoadHandle
	{
		// Token: 0x1700046A RID: 1130
		// (get) Token: 0x06001831 RID: 6193 RVA: 0x0007DA98 File Offset: 0x0007BC98
		// (set) Token: 0x06001832 RID: 6194 RVA: 0x0007DAA5 File Offset: 0x0007BCA5
		public SNIError LastError
		{
			get
			{
				return this._lastError.Value;
			}
			set
			{
				this._lastError.Value = value;
			}
		}

		// Token: 0x1700046B RID: 1131
		// (get) Token: 0x06001833 RID: 6195 RVA: 0x0007DAB3 File Offset: 0x0007BCB3
		public uint Status
		{
			get
			{
				return this._status;
			}
		}

		// Token: 0x1700046C RID: 1132
		// (get) Token: 0x06001834 RID: 6196 RVA: 0x0007DABB File Offset: 0x0007BCBB
		public EncryptionOptions Options
		{
			get
			{
				return this._encryptionOption;
			}
		}

		// Token: 0x06001835 RID: 6197 RVA: 0x0007DAC3 File Offset: 0x0007BCC3
		public SNILoadHandle()
		{
		}

		// Token: 0x06001836 RID: 6198 RVA: 0x0007DAF5 File Offset: 0x0007BCF5
		// Note: this type is marked as 'beforefieldinit'.
		static SNILoadHandle()
		{
		}

		// Token: 0x04001233 RID: 4659
		public static readonly SNILoadHandle SingletonInstance = new SNILoadHandle();

		// Token: 0x04001234 RID: 4660
		public readonly EncryptionOptions _encryptionOption;

		// Token: 0x04001235 RID: 4661
		public ThreadLocal<SNIError> _lastError = new ThreadLocal<SNIError>(() => new SNIError(SNIProviders.INVALID_PROV, 0U, 0U, string.Empty));

		// Token: 0x04001236 RID: 4662
		private readonly uint _status;

		// Token: 0x0200022A RID: 554
		[CompilerGenerated]
		[Serializable]
		private sealed class <>c
		{
			// Token: 0x06001837 RID: 6199 RVA: 0x0007DB01 File Offset: 0x0007BD01
			// Note: this type is marked as 'beforefieldinit'.
			static <>c()
			{
			}

			// Token: 0x06001838 RID: 6200 RVA: 0x00005C14 File Offset: 0x00003E14
			public <>c()
			{
			}

			// Token: 0x06001839 RID: 6201 RVA: 0x0007DB0D File Offset: 0x0007BD0D
			internal SNIError <.ctor>b__11_0()
			{
				return new SNIError(SNIProviders.INVALID_PROV, 0U, 0U, string.Empty);
			}

			// Token: 0x04001237 RID: 4663
			public static readonly SNILoadHandle.<>c <>9 = new SNILoadHandle.<>c();

			// Token: 0x04001238 RID: 4664
			public static Func<SNIError> <>9__11_0;
		}
	}
}
