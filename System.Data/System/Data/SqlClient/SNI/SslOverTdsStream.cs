﻿using System;
using System.IO;
using System.IO.Pipes;

namespace System.Data.SqlClient.SNI
{
	// Token: 0x0200023C RID: 572
	internal sealed class SslOverTdsStream : Stream
	{
		// Token: 0x060018E5 RID: 6373 RVA: 0x000810A8 File Offset: 0x0007F2A8
		public SslOverTdsStream(Stream stream)
		{
			this._stream = stream;
			this._encapsulate = true;
		}

		// Token: 0x060018E6 RID: 6374 RVA: 0x000810BE File Offset: 0x0007F2BE
		public void FinishHandshake()
		{
			this._encapsulate = false;
		}

		// Token: 0x060018E7 RID: 6375 RVA: 0x000810C8 File Offset: 0x0007F2C8
		public override int Read(byte[] buffer, int offset, int count)
		{
			int i = 0;
			byte[] array = new byte[(count < 8) ? 8 : count];
			if (this._encapsulate)
			{
				if (this._packetBytes == 0)
				{
					while (i < 8)
					{
						i += this._stream.Read(array, i, 8 - i);
					}
					this._packetBytes = ((int)array[2] << 8 | (int)array[3]);
					this._packetBytes -= 8;
				}
				if (count > this._packetBytes)
				{
					count = this._packetBytes;
				}
			}
			i = this._stream.Read(array, 0, count);
			if (this._encapsulate)
			{
				this._packetBytes -= i;
			}
			Buffer.BlockCopy(array, 0, buffer, offset, i);
			return i;
		}

		// Token: 0x060018E8 RID: 6376 RVA: 0x0008116C File Offset: 0x0007F36C
		public override void Write(byte[] buffer, int offset, int count)
		{
			int num = offset;
			while (count > 0)
			{
				int num2;
				if (this._encapsulate)
				{
					if (count > 4088)
					{
						num2 = 4088;
					}
					else
					{
						num2 = count;
					}
					count -= num2;
					byte[] array = new byte[8 + num2];
					array[0] = 18;
					array[1] = ((count > 0) ? 0 : 1);
					array[2] = (byte)((num2 + 8) / 256);
					array[3] = (byte)((num2 + 8) % 256);
					array[4] = 0;
					array[5] = 0;
					array[6] = 0;
					array[7] = 0;
					for (int i = 8; i < array.Length; i++)
					{
						array[i] = buffer[num + (i - 8)];
					}
					this._stream.Write(array, 0, array.Length);
				}
				else
				{
					num2 = count;
					count = 0;
					this._stream.Write(buffer, num, num2);
				}
				this._stream.Flush();
				num += num2;
			}
		}

		// Token: 0x060018E9 RID: 6377 RVA: 0x00076A6D File Offset: 0x00074C6D
		public override void SetLength(long value)
		{
			throw new NotSupportedException();
		}

		// Token: 0x060018EA RID: 6378 RVA: 0x0008123B File Offset: 0x0007F43B
		public override void Flush()
		{
			if (!(this._stream is PipeStream))
			{
				this._stream.Flush();
			}
		}

		// Token: 0x17000481 RID: 1153
		// (get) Token: 0x060018EB RID: 6379 RVA: 0x00076A6D File Offset: 0x00074C6D
		// (set) Token: 0x060018EC RID: 6380 RVA: 0x00076A6D File Offset: 0x00074C6D
		public override long Position
		{
			get
			{
				throw new NotSupportedException();
			}
			set
			{
				throw new NotSupportedException();
			}
		}

		// Token: 0x060018ED RID: 6381 RVA: 0x00076A6D File Offset: 0x00074C6D
		public override long Seek(long offset, SeekOrigin origin)
		{
			throw new NotSupportedException();
		}

		// Token: 0x17000482 RID: 1154
		// (get) Token: 0x060018EE RID: 6382 RVA: 0x00081255 File Offset: 0x0007F455
		public override bool CanRead
		{
			get
			{
				return this._stream.CanRead;
			}
		}

		// Token: 0x17000483 RID: 1155
		// (get) Token: 0x060018EF RID: 6383 RVA: 0x00081262 File Offset: 0x0007F462
		public override bool CanWrite
		{
			get
			{
				return this._stream.CanWrite;
			}
		}

		// Token: 0x17000484 RID: 1156
		// (get) Token: 0x060018F0 RID: 6384 RVA: 0x000061C5 File Offset: 0x000043C5
		public override bool CanSeek
		{
			get
			{
				return false;
			}
		}

		// Token: 0x17000485 RID: 1157
		// (get) Token: 0x060018F1 RID: 6385 RVA: 0x00076A6D File Offset: 0x00074C6D
		public override long Length
		{
			get
			{
				throw new NotSupportedException();
			}
		}

		// Token: 0x040012BC RID: 4796
		private readonly Stream _stream;

		// Token: 0x040012BD RID: 4797
		private int _packetBytes;

		// Token: 0x040012BE RID: 4798
		private bool _encapsulate;

		// Token: 0x040012BF RID: 4799
		private const int PACKET_SIZE_WITHOUT_HEADER = 4088;

		// Token: 0x040012C0 RID: 4800
		private const int PRELOGIN_PACKET_TYPE = 18;
	}
}
