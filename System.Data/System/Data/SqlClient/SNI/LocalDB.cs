﻿using System;

namespace System.Data.SqlClient.SNI
{
	// Token: 0x02000221 RID: 545
	internal class LocalDB
	{
		// Token: 0x06001816 RID: 6166 RVA: 0x0004B969 File Offset: 0x00049B69
		internal static string GetLocalDBConnectionString(string localDbInstance)
		{
			throw new PlatformNotSupportedException("LocalDB is not supported on this platform.");
		}

		// Token: 0x06001817 RID: 6167 RVA: 0x00005C14 File Offset: 0x00003E14
		public LocalDB()
		{
		}
	}
}
