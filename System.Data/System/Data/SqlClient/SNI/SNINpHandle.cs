﻿using System;
using System.ComponentModel;
using System.IO;
using System.IO.Pipes;
using System.Net.Security;
using System.Runtime.CompilerServices;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;

namespace System.Data.SqlClient.SNI
{
	// Token: 0x0200022E RID: 558
	internal class SNINpHandle : SNIHandle
	{
		// Token: 0x06001863 RID: 6243 RVA: 0x0007EA70 File Offset: 0x0007CC70
		public SNINpHandle(string serverName, string pipeName, long timerExpire, object callbackObject)
		{
			this._targetServer = serverName;
			this._callbackObject = callbackObject;
			this._writeScheduler = new ConcurrentExclusiveSchedulerPair().ExclusiveScheduler;
			this._writeTaskFactory = new TaskFactory(this._writeScheduler);
			try
			{
				this._pipeStream = new NamedPipeClientStream(serverName, pipeName, PipeDirection.InOut, PipeOptions.WriteThrough | PipeOptions.Asynchronous);
				if (9223372036854775807L == timerExpire)
				{
					this._pipeStream.Connect(-1);
				}
				else
				{
					TimeSpan timeSpan = DateTime.FromFileTime(timerExpire) - DateTime.Now;
					timeSpan = ((timeSpan.Ticks < 0L) ? TimeSpan.FromTicks(0L) : timeSpan);
					this._pipeStream.Connect((int)timeSpan.TotalMilliseconds);
				}
			}
			catch (TimeoutException sniException)
			{
				SNICommon.ReportSNIError(SNIProviders.NP_PROV, 40U, sniException);
				this._status = 1U;
				return;
			}
			catch (IOException sniException2)
			{
				SNICommon.ReportSNIError(SNIProviders.NP_PROV, 40U, sniException2);
				this._status = 1U;
				return;
			}
			if (!this._pipeStream.IsConnected || !this._pipeStream.CanWrite || !this._pipeStream.CanRead)
			{
				SNICommon.ReportSNIError(SNIProviders.NP_PROV, 0U, 40U, string.Empty);
				this._status = 1U;
				return;
			}
			this._sslOverTdsStream = new SslOverTdsStream(this._pipeStream);
			this._sslStream = new SslStream(this._sslOverTdsStream, true, new RemoteCertificateValidationCallback(this.ValidateServerCertificate), null);
			this._stream = this._pipeStream;
			this._status = 0U;
		}

		// Token: 0x17000472 RID: 1138
		// (get) Token: 0x06001864 RID: 6244 RVA: 0x0007EC10 File Offset: 0x0007CE10
		public override Guid ConnectionId
		{
			get
			{
				return this._connectionId;
			}
		}

		// Token: 0x17000473 RID: 1139
		// (get) Token: 0x06001865 RID: 6245 RVA: 0x0007EC18 File Offset: 0x0007CE18
		public override uint Status
		{
			get
			{
				return this._status;
			}
		}

		// Token: 0x06001866 RID: 6246 RVA: 0x0007EC20 File Offset: 0x0007CE20
		public override uint CheckConnection()
		{
			if (!this._stream.CanWrite || !this._stream.CanRead)
			{
				return 1U;
			}
			return 0U;
		}

		// Token: 0x06001867 RID: 6247 RVA: 0x0007EC40 File Offset: 0x0007CE40
		public override void Dispose()
		{
			lock (this)
			{
				if (this._sslOverTdsStream != null)
				{
					this._sslOverTdsStream.Dispose();
					this._sslOverTdsStream = null;
				}
				if (this._sslStream != null)
				{
					this._sslStream.Dispose();
					this._sslStream = null;
				}
				if (this._pipeStream != null)
				{
					this._pipeStream.Dispose();
					this._pipeStream = null;
				}
				this._stream = null;
			}
		}

		// Token: 0x06001868 RID: 6248 RVA: 0x0007ECCC File Offset: 0x0007CECC
		public override uint Receive(out SNIPacket packet, int timeout)
		{
			uint result;
			lock (this)
			{
				packet = null;
				try
				{
					packet = new SNIPacket(null);
					packet.Allocate(this._bufferSize);
					packet.ReadFromStream(this._stream);
					if (packet.Length == 0)
					{
						Win32Exception ex = new Win32Exception();
						return this.ReportErrorAndReleasePacket(packet, (uint)ex.NativeErrorCode, 0U, ex.Message);
					}
				}
				catch (ObjectDisposedException sniException)
				{
					return this.ReportErrorAndReleasePacket(packet, sniException);
				}
				catch (IOException sniException2)
				{
					return this.ReportErrorAndReleasePacket(packet, sniException2);
				}
				result = 0U;
			}
			return result;
		}

		// Token: 0x06001869 RID: 6249 RVA: 0x0007ED88 File Offset: 0x0007CF88
		public override uint ReceiveAsync(ref SNIPacket packet)
		{
			uint result;
			lock (this)
			{
				packet = new SNIPacket(null);
				packet.Allocate(this._bufferSize);
				try
				{
					packet.ReadFromStreamAsync(this._stream, this._receiveCallback);
					result = 997U;
				}
				catch (ObjectDisposedException sniException)
				{
					result = this.ReportErrorAndReleasePacket(packet, sniException);
				}
				catch (IOException sniException2)
				{
					result = this.ReportErrorAndReleasePacket(packet, sniException2);
				}
			}
			return result;
		}

		// Token: 0x0600186A RID: 6250 RVA: 0x0007EE20 File Offset: 0x0007D020
		public override uint Send(SNIPacket packet)
		{
			uint result;
			lock (this)
			{
				try
				{
					packet.WriteToStream(this._stream);
					result = 0U;
				}
				catch (ObjectDisposedException sniException)
				{
					result = this.ReportErrorAndReleasePacket(packet, sniException);
				}
				catch (IOException sniException2)
				{
					result = this.ReportErrorAndReleasePacket(packet, sniException2);
				}
			}
			return result;
		}

		// Token: 0x0600186B RID: 6251 RVA: 0x0007EE98 File Offset: 0x0007D098
		public override uint SendAsync(SNIPacket packet, SNIAsyncCallback callback = null)
		{
			SNIPacket packet2 = packet;
			this._writeTaskFactory.StartNew(delegate()
			{
				try
				{
					SNINpHandle <>4__this = this;
					lock (<>4__this)
					{
						packet.WriteToStream(this._stream);
					}
				}
				catch (Exception sniException)
				{
					SNICommon.ReportSNIError(SNIProviders.NP_PROV, 35U, sniException);
					if (callback != null)
					{
						callback(packet, 1U);
					}
					else
					{
						this._sendCallback(packet, 1U);
					}
					return;
				}
				if (callback != null)
				{
					callback(packet, 0U);
					return;
				}
				this._sendCallback(packet, 0U);
			});
			return 997U;
		}

		// Token: 0x0600186C RID: 6252 RVA: 0x0007EEE4 File Offset: 0x0007D0E4
		public override void SetAsyncCallbacks(SNIAsyncCallback receiveCallback, SNIAsyncCallback sendCallback)
		{
			this._receiveCallback = receiveCallback;
			this._sendCallback = sendCallback;
		}

		// Token: 0x0600186D RID: 6253 RVA: 0x0007EEF4 File Offset: 0x0007D0F4
		public override uint EnableSsl(uint options)
		{
			this._validateCert = ((options & 1U) > 0U);
			try
			{
				this._sslStream.AuthenticateAsClientAsync(this._targetServer).GetAwaiter().GetResult();
				this._sslOverTdsStream.FinishHandshake();
			}
			catch (AuthenticationException sniException)
			{
				return SNICommon.ReportSNIError(SNIProviders.NP_PROV, 35U, sniException);
			}
			catch (InvalidOperationException sniException2)
			{
				return SNICommon.ReportSNIError(SNIProviders.NP_PROV, 35U, sniException2);
			}
			this._stream = this._sslStream;
			return 0U;
		}

		// Token: 0x0600186E RID: 6254 RVA: 0x0007EF80 File Offset: 0x0007D180
		public override void DisableSsl()
		{
			this._sslStream.Dispose();
			this._sslStream = null;
			this._sslOverTdsStream.Dispose();
			this._sslOverTdsStream = null;
			this._stream = this._pipeStream;
		}

		// Token: 0x0600186F RID: 6255 RVA: 0x0007EFB2 File Offset: 0x0007D1B2
		private bool ValidateServerCertificate(object sender, X509Certificate cert, X509Chain chain, SslPolicyErrors policyErrors)
		{
			return !this._validateCert || SNICommon.ValidateSslServerCertificate(this._targetServer, sender, cert, chain, policyErrors);
		}

		// Token: 0x06001870 RID: 6256 RVA: 0x0007EFCE File Offset: 0x0007D1CE
		public override void SetBufferSize(int bufferSize)
		{
			this._bufferSize = bufferSize;
		}

		// Token: 0x06001871 RID: 6257 RVA: 0x0007EFD7 File Offset: 0x0007D1D7
		private uint ReportErrorAndReleasePacket(SNIPacket packet, Exception sniException)
		{
			if (packet != null)
			{
				packet.Release();
			}
			return SNICommon.ReportSNIError(SNIProviders.NP_PROV, 35U, sniException);
		}

		// Token: 0x06001872 RID: 6258 RVA: 0x0007EFEB File Offset: 0x0007D1EB
		private uint ReportErrorAndReleasePacket(SNIPacket packet, uint nativeError, uint sniError, string errorMessage)
		{
			if (packet != null)
			{
				packet.Release();
			}
			return SNICommon.ReportSNIError(SNIProviders.NP_PROV, nativeError, sniError, errorMessage);
		}

		// Token: 0x04001255 RID: 4693
		internal const string DefaultPipePath = "sql\\query";

		// Token: 0x04001256 RID: 4694
		private const int MAX_PIPE_INSTANCES = 255;

		// Token: 0x04001257 RID: 4695
		private readonly string _targetServer;

		// Token: 0x04001258 RID: 4696
		private readonly object _callbackObject;

		// Token: 0x04001259 RID: 4697
		private readonly TaskScheduler _writeScheduler;

		// Token: 0x0400125A RID: 4698
		private readonly TaskFactory _writeTaskFactory;

		// Token: 0x0400125B RID: 4699
		private Stream _stream;

		// Token: 0x0400125C RID: 4700
		private NamedPipeClientStream _pipeStream;

		// Token: 0x0400125D RID: 4701
		private SslOverTdsStream _sslOverTdsStream;

		// Token: 0x0400125E RID: 4702
		private SslStream _sslStream;

		// Token: 0x0400125F RID: 4703
		private SNIAsyncCallback _receiveCallback;

		// Token: 0x04001260 RID: 4704
		private SNIAsyncCallback _sendCallback;

		// Token: 0x04001261 RID: 4705
		private bool _validateCert = true;

		// Token: 0x04001262 RID: 4706
		private readonly uint _status = uint.MaxValue;

		// Token: 0x04001263 RID: 4707
		private int _bufferSize = 4096;

		// Token: 0x04001264 RID: 4708
		private readonly Guid _connectionId = Guid.NewGuid();

		// Token: 0x0200022F RID: 559
		[CompilerGenerated]
		private sealed class <>c__DisplayClass26_0
		{
			// Token: 0x06001873 RID: 6259 RVA: 0x00005C14 File Offset: 0x00003E14
			public <>c__DisplayClass26_0()
			{
			}

			// Token: 0x06001874 RID: 6260 RVA: 0x0007F000 File Offset: 0x0007D200
			internal void <SendAsync>b__0()
			{
				try
				{
					SNINpHandle obj = this.<>4__this;
					lock (obj)
					{
						this.packet.WriteToStream(this.<>4__this._stream);
					}
				}
				catch (Exception sniException)
				{
					SNICommon.ReportSNIError(SNIProviders.NP_PROV, 35U, sniException);
					if (this.callback != null)
					{
						this.callback(this.packet, 1U);
					}
					else
					{
						this.<>4__this._sendCallback(this.packet, 1U);
					}
					return;
				}
				if (this.callback != null)
				{
					this.callback(this.packet, 0U);
					return;
				}
				this.<>4__this._sendCallback(this.packet, 0U);
			}

			// Token: 0x04001265 RID: 4709
			public SNINpHandle <>4__this;

			// Token: 0x04001266 RID: 4710
			public SNIPacket packet;

			// Token: 0x04001267 RID: 4711
			public SNIAsyncCallback callback;
		}
	}
}
