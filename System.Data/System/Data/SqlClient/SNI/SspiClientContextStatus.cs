﻿using System;
using System.Net;
using System.Net.Security;
using System.Runtime.CompilerServices;

namespace System.Data.SqlClient.SNI
{
	// Token: 0x0200023D RID: 573
	internal class SspiClientContextStatus
	{
		// Token: 0x17000486 RID: 1158
		// (get) Token: 0x060018F2 RID: 6386 RVA: 0x0008126F File Offset: 0x0007F46F
		// (set) Token: 0x060018F3 RID: 6387 RVA: 0x00081277 File Offset: 0x0007F477
		public SafeFreeCredentials CredentialsHandle
		{
			[CompilerGenerated]
			get
			{
				return this.<CredentialsHandle>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<CredentialsHandle>k__BackingField = value;
			}
		}

		// Token: 0x17000487 RID: 1159
		// (get) Token: 0x060018F4 RID: 6388 RVA: 0x00081280 File Offset: 0x0007F480
		// (set) Token: 0x060018F5 RID: 6389 RVA: 0x00081288 File Offset: 0x0007F488
		public SafeDeleteContext SecurityContext
		{
			[CompilerGenerated]
			get
			{
				return this.<SecurityContext>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<SecurityContext>k__BackingField = value;
			}
		}

		// Token: 0x17000488 RID: 1160
		// (get) Token: 0x060018F6 RID: 6390 RVA: 0x00081291 File Offset: 0x0007F491
		// (set) Token: 0x060018F7 RID: 6391 RVA: 0x00081299 File Offset: 0x0007F499
		public ContextFlagsPal ContextFlags
		{
			[CompilerGenerated]
			get
			{
				return this.<ContextFlags>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<ContextFlags>k__BackingField = value;
			}
		}

		// Token: 0x060018F8 RID: 6392 RVA: 0x00005C14 File Offset: 0x00003E14
		public SspiClientContextStatus()
		{
		}

		// Token: 0x040012C1 RID: 4801
		[CompilerGenerated]
		private SafeFreeCredentials <CredentialsHandle>k__BackingField;

		// Token: 0x040012C2 RID: 4802
		[CompilerGenerated]
		private SafeDeleteContext <SecurityContext>k__BackingField;

		// Token: 0x040012C3 RID: 4803
		[CompilerGenerated]
		private ContextFlagsPal <ContextFlags>k__BackingField;
	}
}
