﻿using System;
using System.Collections.Generic;

namespace System.Data.SqlClient.SNI
{
	// Token: 0x0200022B RID: 555
	internal class SNIMarsConnection
	{
		// Token: 0x1700046D RID: 1133
		// (get) Token: 0x0600183A RID: 6202 RVA: 0x0007DB1D File Offset: 0x0007BD1D
		public Guid ConnectionId
		{
			get
			{
				return this._connectionId;
			}
		}

		// Token: 0x0600183B RID: 6203 RVA: 0x0007DB28 File Offset: 0x0007BD28
		public SNIMarsConnection(SNIHandle lowerHandle)
		{
			this._lowerHandle = lowerHandle;
			this._lowerHandle.SetAsyncCallbacks(new SNIAsyncCallback(this.HandleReceiveComplete), new SNIAsyncCallback(this.HandleSendComplete));
		}

		// Token: 0x0600183C RID: 6204 RVA: 0x0007DB88 File Offset: 0x0007BD88
		public SNIMarsHandle CreateMarsSession(object callbackObject, bool async)
		{
			SNIMarsHandle result;
			lock (this)
			{
				ushort nextSessionId = this._nextSessionId;
				this._nextSessionId = nextSessionId + 1;
				ushort num = nextSessionId;
				SNIMarsHandle snimarsHandle = new SNIMarsHandle(this, num, callbackObject, async);
				this._sessions.Add((int)num, snimarsHandle);
				result = snimarsHandle;
			}
			return result;
		}

		// Token: 0x0600183D RID: 6205 RVA: 0x0007DBF0 File Offset: 0x0007BDF0
		public uint StartReceive()
		{
			SNIPacket snipacket = null;
			if (this.ReceiveAsync(ref snipacket) == 997U)
			{
				return 997U;
			}
			return SNICommon.ReportSNIError(SNIProviders.SMUX_PROV, 0U, 19U, string.Empty);
		}

		// Token: 0x0600183E RID: 6206 RVA: 0x0007DC24 File Offset: 0x0007BE24
		public uint Send(SNIPacket packet)
		{
			uint result;
			lock (this)
			{
				result = this._lowerHandle.Send(packet);
			}
			return result;
		}

		// Token: 0x0600183F RID: 6207 RVA: 0x0007DC68 File Offset: 0x0007BE68
		public uint SendAsync(SNIPacket packet, SNIAsyncCallback callback)
		{
			uint result;
			lock (this)
			{
				result = this._lowerHandle.SendAsync(packet, callback);
			}
			return result;
		}

		// Token: 0x06001840 RID: 6208 RVA: 0x0007DCAC File Offset: 0x0007BEAC
		public uint ReceiveAsync(ref SNIPacket packet)
		{
			uint result;
			lock (this)
			{
				result = this._lowerHandle.ReceiveAsync(ref packet);
			}
			return result;
		}

		// Token: 0x06001841 RID: 6209 RVA: 0x0007DCF0 File Offset: 0x0007BEF0
		public uint CheckConnection()
		{
			uint result;
			lock (this)
			{
				result = this._lowerHandle.CheckConnection();
			}
			return result;
		}

		// Token: 0x06001842 RID: 6210 RVA: 0x0007DD34 File Offset: 0x0007BF34
		public void HandleReceiveError(SNIPacket packet)
		{
			foreach (SNIMarsHandle snimarsHandle in this._sessions.Values)
			{
				snimarsHandle.HandleReceiveError(packet);
			}
		}

		// Token: 0x06001843 RID: 6211 RVA: 0x0007DD8C File Offset: 0x0007BF8C
		public void HandleSendComplete(SNIPacket packet, uint sniErrorCode)
		{
			packet.InvokeCompletionCallback(sniErrorCode);
		}

		// Token: 0x06001844 RID: 6212 RVA: 0x0007DD98 File Offset: 0x0007BF98
		public void HandleReceiveComplete(SNIPacket packet, uint sniErrorCode)
		{
			SNISMUXHeader snismuxheader = null;
			SNIPacket packet2 = null;
			SNIMarsHandle snimarsHandle = null;
			if (sniErrorCode != 0U)
			{
				SNIMarsConnection obj = this;
				lock (obj)
				{
					this.HandleReceiveError(packet);
					return;
				}
			}
			for (;;)
			{
				SNIMarsConnection obj = this;
				lock (obj)
				{
					if (this._currentHeaderByteCount != 16)
					{
						snismuxheader = null;
						packet2 = null;
						snimarsHandle = null;
						while (this._currentHeaderByteCount != 16)
						{
							int num = packet.TakeData(this._headerBytes, this._currentHeaderByteCount, 16 - this._currentHeaderByteCount);
							this._currentHeaderByteCount += num;
							if (num == 0)
							{
								sniErrorCode = this.ReceiveAsync(ref packet);
								if (sniErrorCode == 997U)
								{
									return;
								}
								this.HandleReceiveError(packet);
								return;
							}
						}
						this._currentHeader = new SNISMUXHeader
						{
							SMID = this._headerBytes[0],
							flags = this._headerBytes[1],
							sessionId = BitConverter.ToUInt16(this._headerBytes, 2),
							length = BitConverter.ToUInt32(this._headerBytes, 4) - 16U,
							sequenceNumber = BitConverter.ToUInt32(this._headerBytes, 8),
							highwater = BitConverter.ToUInt32(this._headerBytes, 12)
						};
						this._dataBytesLeft = (int)this._currentHeader.length;
						this._currentPacket = new SNIPacket(null);
						this._currentPacket.Allocate((int)this._currentHeader.length);
					}
					snismuxheader = this._currentHeader;
					packet2 = this._currentPacket;
					if (this._currentHeader.flags == 8 && this._dataBytesLeft > 0)
					{
						int num2 = packet.TakeData(this._currentPacket, this._dataBytesLeft);
						this._dataBytesLeft -= num2;
						if (this._dataBytesLeft > 0)
						{
							sniErrorCode = this.ReceiveAsync(ref packet);
							if (sniErrorCode == 997U)
							{
								break;
							}
							this.HandleReceiveError(packet);
							break;
						}
					}
					this._currentHeaderByteCount = 0;
					if (!this._sessions.ContainsKey((int)this._currentHeader.sessionId))
					{
						SNILoadHandle.SingletonInstance.LastError = new SNIError(SNIProviders.SMUX_PROV, 0U, 5U, string.Empty);
						this.HandleReceiveError(packet);
						this._lowerHandle.Dispose();
						this._lowerHandle = null;
						break;
					}
					if (this._currentHeader.flags == 4)
					{
						this._sessions.Remove((int)this._currentHeader.sessionId);
					}
					else
					{
						snimarsHandle = this._sessions[(int)this._currentHeader.sessionId];
					}
				}
				if (snismuxheader.flags == 8)
				{
					snimarsHandle.HandleReceiveComplete(packet2, snismuxheader);
				}
				if (this._currentHeader.flags == 2)
				{
					try
					{
						snimarsHandle.HandleAck(snismuxheader.highwater);
					}
					catch (Exception sniException)
					{
						SNICommon.ReportSNIError(SNIProviders.SMUX_PROV, 35U, sniException);
					}
				}
				obj = this;
				lock (obj)
				{
					if (packet.DataLeft != 0)
					{
						continue;
					}
					sniErrorCode = this.ReceiveAsync(ref packet);
					if (sniErrorCode != 997U)
					{
						this.HandleReceiveError(packet);
					}
				}
				break;
			}
		}

		// Token: 0x06001845 RID: 6213 RVA: 0x0007E0EC File Offset: 0x0007C2EC
		public uint EnableSsl(uint options)
		{
			return this._lowerHandle.EnableSsl(options);
		}

		// Token: 0x06001846 RID: 6214 RVA: 0x0007E0FA File Offset: 0x0007C2FA
		public void DisableSsl()
		{
			this._lowerHandle.DisableSsl();
		}

		// Token: 0x04001239 RID: 4665
		private readonly Guid _connectionId = Guid.NewGuid();

		// Token: 0x0400123A RID: 4666
		private readonly Dictionary<int, SNIMarsHandle> _sessions = new Dictionary<int, SNIMarsHandle>();

		// Token: 0x0400123B RID: 4667
		private readonly byte[] _headerBytes = new byte[16];

		// Token: 0x0400123C RID: 4668
		private SNIHandle _lowerHandle;

		// Token: 0x0400123D RID: 4669
		private ushort _nextSessionId;

		// Token: 0x0400123E RID: 4670
		private int _currentHeaderByteCount;

		// Token: 0x0400123F RID: 4671
		private int _dataBytesLeft;

		// Token: 0x04001240 RID: 4672
		private SNISMUXHeader _currentHeader;

		// Token: 0x04001241 RID: 4673
		private SNIPacket _currentPacket;
	}
}
