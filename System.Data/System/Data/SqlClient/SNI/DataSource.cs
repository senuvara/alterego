﻿using System;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;

namespace System.Data.SqlClient.SNI
{
	// Token: 0x02000234 RID: 564
	internal class DataSource
	{
		// Token: 0x17000478 RID: 1144
		// (get) Token: 0x060018A9 RID: 6313 RVA: 0x0007F933 File Offset: 0x0007DB33
		// (set) Token: 0x060018AA RID: 6314 RVA: 0x0007F93B File Offset: 0x0007DB3B
		internal string ServerName
		{
			[CompilerGenerated]
			get
			{
				return this.<ServerName>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<ServerName>k__BackingField = value;
			}
		}

		// Token: 0x17000479 RID: 1145
		// (get) Token: 0x060018AB RID: 6315 RVA: 0x0007F944 File Offset: 0x0007DB44
		// (set) Token: 0x060018AC RID: 6316 RVA: 0x0007F94C File Offset: 0x0007DB4C
		internal int Port
		{
			[CompilerGenerated]
			get
			{
				return this.<Port>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<Port>k__BackingField = value;
			}
		} = -1;

		// Token: 0x1700047A RID: 1146
		// (get) Token: 0x060018AD RID: 6317 RVA: 0x0007F955 File Offset: 0x0007DB55
		// (set) Token: 0x060018AE RID: 6318 RVA: 0x0007F95D File Offset: 0x0007DB5D
		public string InstanceName
		{
			[CompilerGenerated]
			get
			{
				return this.<InstanceName>k__BackingField;
			}
			[CompilerGenerated]
			internal set
			{
				this.<InstanceName>k__BackingField = value;
			}
		}

		// Token: 0x1700047B RID: 1147
		// (get) Token: 0x060018AF RID: 6319 RVA: 0x0007F966 File Offset: 0x0007DB66
		// (set) Token: 0x060018B0 RID: 6320 RVA: 0x0007F96E File Offset: 0x0007DB6E
		public string PipeName
		{
			[CompilerGenerated]
			get
			{
				return this.<PipeName>k__BackingField;
			}
			[CompilerGenerated]
			internal set
			{
				this.<PipeName>k__BackingField = value;
			}
		}

		// Token: 0x1700047C RID: 1148
		// (get) Token: 0x060018B1 RID: 6321 RVA: 0x0007F977 File Offset: 0x0007DB77
		// (set) Token: 0x060018B2 RID: 6322 RVA: 0x0007F97F File Offset: 0x0007DB7F
		public string PipeHostName
		{
			[CompilerGenerated]
			get
			{
				return this.<PipeHostName>k__BackingField;
			}
			[CompilerGenerated]
			internal set
			{
				this.<PipeHostName>k__BackingField = value;
			}
		}

		// Token: 0x1700047D RID: 1149
		// (get) Token: 0x060018B3 RID: 6323 RVA: 0x0007F988 File Offset: 0x0007DB88
		// (set) Token: 0x060018B4 RID: 6324 RVA: 0x0007F990 File Offset: 0x0007DB90
		internal bool IsBadDataSource
		{
			[CompilerGenerated]
			get
			{
				return this.<IsBadDataSource>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<IsBadDataSource>k__BackingField = value;
			}
		}

		// Token: 0x1700047E RID: 1150
		// (get) Token: 0x060018B5 RID: 6325 RVA: 0x0007F999 File Offset: 0x0007DB99
		// (set) Token: 0x060018B6 RID: 6326 RVA: 0x0007F9A1 File Offset: 0x0007DBA1
		internal bool IsSsrpRequired
		{
			[CompilerGenerated]
			get
			{
				return this.<IsSsrpRequired>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<IsSsrpRequired>k__BackingField = value;
			}
		}

		// Token: 0x060018B7 RID: 6327 RVA: 0x0007F9AC File Offset: 0x0007DBAC
		private DataSource(string dataSource)
		{
			this._workingDataSource = dataSource.Trim().ToLowerInvariant();
			int num = this._workingDataSource.IndexOf(':');
			this.PopulateProtocol();
			this._dataSourceAfterTrimmingProtocol = ((num > -1 && this.ConnectionProtocol != DataSource.Protocol.None) ? this._workingDataSource.Substring(num + 1).Trim() : this._workingDataSource);
			if (this._dataSourceAfterTrimmingProtocol.Contains("/"))
			{
				if (this.ConnectionProtocol == DataSource.Protocol.None)
				{
					this.ReportSNIError(SNIProviders.INVALID_PROV);
					return;
				}
				if (this.ConnectionProtocol == DataSource.Protocol.NP)
				{
					this.ReportSNIError(SNIProviders.NP_PROV);
					return;
				}
				if (this.ConnectionProtocol == DataSource.Protocol.TCP)
				{
					this.ReportSNIError(SNIProviders.TCP_PROV);
				}
			}
		}

		// Token: 0x060018B8 RID: 6328 RVA: 0x0007FA64 File Offset: 0x0007DC64
		private void PopulateProtocol()
		{
			string[] array = this._workingDataSource.Split(new char[]
			{
				':'
			});
			if (array.Length <= 1)
			{
				this.ConnectionProtocol = DataSource.Protocol.None;
				return;
			}
			string a = array[0].Trim();
			if (a == "tcp")
			{
				this.ConnectionProtocol = DataSource.Protocol.TCP;
				return;
			}
			if (a == "np")
			{
				this.ConnectionProtocol = DataSource.Protocol.NP;
				return;
			}
			if (!(a == "admin"))
			{
				this.ConnectionProtocol = DataSource.Protocol.None;
				return;
			}
			this.ConnectionProtocol = DataSource.Protocol.Admin;
		}

		// Token: 0x060018B9 RID: 6329 RVA: 0x0007FAE8 File Offset: 0x0007DCE8
		public static string GetLocalDBInstance(string dataSource, out bool error)
		{
			string result = null;
			string[] array = dataSource.ToLowerInvariant().Split(new char[]
			{
				'\\'
			});
			error = false;
			if (array.Length == 2 && "(localdb)".Equals(array[0].TrimStart(Array.Empty<char>())))
			{
				if (string.IsNullOrWhiteSpace(array[1]))
				{
					SNILoadHandle.SingletonInstance.LastError = new SNIError(SNIProviders.INVALID_PROV, 0U, 51U, string.Empty);
					error = true;
					return null;
				}
				result = array[1].Trim();
			}
			return result;
		}

		// Token: 0x060018BA RID: 6330 RVA: 0x0007FB64 File Offset: 0x0007DD64
		public static DataSource ParseServerName(string dataSource)
		{
			DataSource dataSource2 = new DataSource(dataSource);
			if (dataSource2.IsBadDataSource)
			{
				return null;
			}
			if (dataSource2.InferNamedPipesInformation())
			{
				return dataSource2;
			}
			if (dataSource2.IsBadDataSource)
			{
				return null;
			}
			if (dataSource2.InferConnectionDetails())
			{
				return dataSource2;
			}
			return null;
		}

		// Token: 0x060018BB RID: 6331 RVA: 0x0007FBA1 File Offset: 0x0007DDA1
		private void InferLocalServerName()
		{
			if (string.IsNullOrEmpty(this.ServerName) || DataSource.IsLocalHost(this.ServerName))
			{
				this.ServerName = ((this.ConnectionProtocol == DataSource.Protocol.Admin) ? Environment.MachineName : "localhost");
			}
		}

		// Token: 0x060018BC RID: 6332 RVA: 0x0007FBD8 File Offset: 0x0007DDD8
		private bool InferConnectionDetails()
		{
			string[] array = this._dataSourceAfterTrimmingProtocol.Split(new char[]
			{
				'\\',
				','
			});
			this.ServerName = array[0].Trim();
			int num = this._dataSourceAfterTrimmingProtocol.IndexOf(',');
			int num2 = this._dataSourceAfterTrimmingProtocol.IndexOf('\\');
			if (num > -1)
			{
				string text = (num2 > -1) ? ((num > num2) ? array[2].Trim() : array[1].Trim()) : array[1].Trim();
				if (string.IsNullOrEmpty(text))
				{
					this.ReportSNIError(SNIProviders.INVALID_PROV);
					return false;
				}
				if (this.ConnectionProtocol == DataSource.Protocol.None)
				{
					this.ConnectionProtocol = DataSource.Protocol.TCP;
				}
				else if (this.ConnectionProtocol != DataSource.Protocol.TCP)
				{
					this.ReportSNIError(SNIProviders.INVALID_PROV);
					return false;
				}
				int num3;
				if (!int.TryParse(text, out num3))
				{
					this.ReportSNIError(SNIProviders.TCP_PROV);
					return false;
				}
				if (num3 < 1)
				{
					this.ReportSNIError(SNIProviders.TCP_PROV);
					return false;
				}
				this.Port = num3;
			}
			else if (num2 > -1)
			{
				this.InstanceName = array[1].Trim();
				if (string.IsNullOrWhiteSpace(this.InstanceName))
				{
					this.ReportSNIError(SNIProviders.INVALID_PROV);
					return false;
				}
				if ("mssqlserver".Equals(this.InstanceName))
				{
					this.ReportSNIError(SNIProviders.INVALID_PROV);
					return false;
				}
				this.IsSsrpRequired = true;
			}
			this.InferLocalServerName();
			return true;
		}

		// Token: 0x060018BD RID: 6333 RVA: 0x0007FD0B File Offset: 0x0007DF0B
		private void ReportSNIError(SNIProviders provider)
		{
			SNILoadHandle.SingletonInstance.LastError = new SNIError(provider, 0U, 25U, string.Empty);
			this.IsBadDataSource = true;
		}

		// Token: 0x060018BE RID: 6334 RVA: 0x0007FD2C File Offset: 0x0007DF2C
		private bool InferNamedPipesInformation()
		{
			if (!this._dataSourceAfterTrimmingProtocol.StartsWith("\\\\") && this.ConnectionProtocol != DataSource.Protocol.NP)
			{
				return false;
			}
			if (!this._dataSourceAfterTrimmingProtocol.Contains('\\'))
			{
				this.PipeHostName = (this.ServerName = this._dataSourceAfterTrimmingProtocol);
				this.InferLocalServerName();
				this.PipeName = "sql\\query";
				return true;
			}
			try
			{
				string[] array = this._dataSourceAfterTrimmingProtocol.Split(new char[]
				{
					'\\'
				});
				if (array.Length < 6)
				{
					this.ReportSNIError(SNIProviders.NP_PROV);
					return false;
				}
				string text = array[2];
				if (string.IsNullOrEmpty(text))
				{
					this.ReportSNIError(SNIProviders.NP_PROV);
					return false;
				}
				if (!"pipe".Equals(array[3]))
				{
					this.ReportSNIError(SNIProviders.NP_PROV);
					return false;
				}
				if (array[4].StartsWith("mssql$"))
				{
					this.InstanceName = array[4].Substring("mssql$".Length);
				}
				StringBuilder stringBuilder = new StringBuilder();
				for (int i = 4; i < array.Length - 1; i++)
				{
					stringBuilder.Append(array[i]);
					stringBuilder.Append(Path.DirectorySeparatorChar);
				}
				stringBuilder.Append(array[array.Length - 1]);
				this.PipeName = stringBuilder.ToString();
				if (string.IsNullOrWhiteSpace(this.InstanceName) && !"sql\\query".Equals(this.PipeName))
				{
					this.InstanceName = "pipe" + this.PipeName;
				}
				this.ServerName = (DataSource.IsLocalHost(text) ? Environment.MachineName : text);
				this.PipeHostName = text;
			}
			catch (UriFormatException)
			{
				this.ReportSNIError(SNIProviders.NP_PROV);
				return false;
			}
			if (this.ConnectionProtocol == DataSource.Protocol.None)
			{
				this.ConnectionProtocol = DataSource.Protocol.NP;
			}
			else if (this.ConnectionProtocol != DataSource.Protocol.NP)
			{
				this.ReportSNIError(SNIProviders.NP_PROV);
				return false;
			}
			return true;
		}

		// Token: 0x060018BF RID: 6335 RVA: 0x0007FF10 File Offset: 0x0007E110
		private static bool IsLocalHost(string serverName)
		{
			return ".".Equals(serverName) || "(local)".Equals(serverName) || "localhost".Equals(serverName);
		}

		// Token: 0x04001278 RID: 4728
		private const char CommaSeparator = ',';

		// Token: 0x04001279 RID: 4729
		private const char BackSlashSeparator = '\\';

		// Token: 0x0400127A RID: 4730
		private const string DefaultHostName = "localhost";

		// Token: 0x0400127B RID: 4731
		private const string DefaultSqlServerInstanceName = "mssqlserver";

		// Token: 0x0400127C RID: 4732
		private const string PipeBeginning = "\\\\";

		// Token: 0x0400127D RID: 4733
		private const string PipeToken = "pipe";

		// Token: 0x0400127E RID: 4734
		private const string LocalDbHost = "(localdb)";

		// Token: 0x0400127F RID: 4735
		private const string NamedPipeInstanceNameHeader = "mssql$";

		// Token: 0x04001280 RID: 4736
		private const string DefaultPipeName = "sql\\query";

		// Token: 0x04001281 RID: 4737
		internal DataSource.Protocol ConnectionProtocol = DataSource.Protocol.None;

		// Token: 0x04001282 RID: 4738
		[CompilerGenerated]
		private string <ServerName>k__BackingField;

		// Token: 0x04001283 RID: 4739
		[CompilerGenerated]
		private int <Port>k__BackingField;

		// Token: 0x04001284 RID: 4740
		[CompilerGenerated]
		private string <InstanceName>k__BackingField;

		// Token: 0x04001285 RID: 4741
		[CompilerGenerated]
		private string <PipeName>k__BackingField;

		// Token: 0x04001286 RID: 4742
		[CompilerGenerated]
		private string <PipeHostName>k__BackingField;

		// Token: 0x04001287 RID: 4743
		private string _workingDataSource;

		// Token: 0x04001288 RID: 4744
		private string _dataSourceAfterTrimmingProtocol;

		// Token: 0x04001289 RID: 4745
		[CompilerGenerated]
		private bool <IsBadDataSource>k__BackingField;

		// Token: 0x0400128A RID: 4746
		[CompilerGenerated]
		private bool <IsSsrpRequired>k__BackingField;

		// Token: 0x02000235 RID: 565
		internal enum Protocol
		{
			// Token: 0x0400128C RID: 4748
			TCP,
			// Token: 0x0400128D RID: 4749
			NP,
			// Token: 0x0400128E RID: 4750
			None,
			// Token: 0x0400128F RID: 4751
			Admin
		}
	}
}
