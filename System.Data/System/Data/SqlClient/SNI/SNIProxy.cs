﻿using System;
using System.Net;
using System.Net.Security;
using System.Net.Sockets;
using System.Text;

namespace System.Data.SqlClient.SNI
{
	// Token: 0x02000232 RID: 562
	internal class SNIProxy
	{
		// Token: 0x06001890 RID: 6288 RVA: 0x00005E03 File Offset: 0x00004003
		public void Terminate()
		{
		}

		// Token: 0x06001891 RID: 6289 RVA: 0x0007F460 File Offset: 0x0007D660
		public uint EnableSsl(SNIHandle handle, uint options)
		{
			uint result;
			try
			{
				result = handle.EnableSsl(options);
			}
			catch (Exception sniException)
			{
				result = SNICommon.ReportSNIError(SNIProviders.SSL_PROV, 31U, sniException);
			}
			return result;
		}

		// Token: 0x06001892 RID: 6290 RVA: 0x0007F498 File Offset: 0x0007D698
		public uint DisableSsl(SNIHandle handle)
		{
			handle.DisableSsl();
			return 0U;
		}

		// Token: 0x06001893 RID: 6291 RVA: 0x0007F4A4 File Offset: 0x0007D6A4
		public void GenSspiClientContext(SspiClientContextStatus sspiClientContextStatus, byte[] receivedBuff, ref byte[] sendBuff, byte[] serverName)
		{
			SafeDeleteContext securityContext = sspiClientContextStatus.SecurityContext;
			ContextFlagsPal contextFlags = sspiClientContextStatus.ContextFlags;
			SafeFreeCredentials credentialsHandle = sspiClientContextStatus.CredentialsHandle;
			string package = "Negotiate";
			if (securityContext == null)
			{
				credentialsHandle = NegotiateStreamPal.AcquireDefaultCredential(package, false);
			}
			SecurityBuffer[] inSecurityBufferArray;
			if (receivedBuff != null)
			{
				inSecurityBufferArray = new SecurityBuffer[]
				{
					new SecurityBuffer(receivedBuff, SecurityBufferType.SECBUFFER_TOKEN)
				};
			}
			else
			{
				inSecurityBufferArray = new SecurityBuffer[0];
			}
			SecurityBuffer securityBuffer = new SecurityBuffer(NegotiateStreamPal.QueryMaxTokenSize(package), SecurityBufferType.SECBUFFER_TOKEN);
			ContextFlagsPal requestedContextFlags = ContextFlagsPal.MutualAuth | ContextFlagsPal.Confidentiality | ContextFlagsPal.Connection;
			string @string = Encoding.UTF8.GetString(serverName);
			SecurityStatusPal securityStatusPal = NegotiateStreamPal.InitializeSecurityContext(credentialsHandle, ref securityContext, @string, requestedContextFlags, inSecurityBufferArray, securityBuffer, ref contextFlags);
			if (securityStatusPal.ErrorCode == SecurityStatusPalErrorCode.CompleteNeeded || securityStatusPal.ErrorCode == SecurityStatusPalErrorCode.CompAndContinue)
			{
				inSecurityBufferArray = new SecurityBuffer[]
				{
					securityBuffer
				};
				securityStatusPal = NegotiateStreamPal.CompleteAuthToken(ref securityContext, inSecurityBufferArray);
				securityBuffer.token = null;
			}
			sendBuff = securityBuffer.token;
			if (sendBuff == null)
			{
				sendBuff = Array.Empty<byte>();
			}
			sspiClientContextStatus.SecurityContext = securityContext;
			sspiClientContextStatus.ContextFlags = contextFlags;
			sspiClientContextStatus.CredentialsHandle = credentialsHandle;
			if (!SNIProxy.IsErrorStatus(securityStatusPal.ErrorCode))
			{
				return;
			}
			if (securityStatusPal.ErrorCode == SecurityStatusPalErrorCode.InternalError)
			{
				throw new Exception(SQLMessage.KerberosTicketMissingError() + "\n" + securityStatusPal);
			}
			throw new Exception(SQLMessage.SSPIGenerateError() + "\n" + securityStatusPal);
		}

		// Token: 0x06001894 RID: 6292 RVA: 0x0007F5DE File Offset: 0x0007D7DE
		private static bool IsErrorStatus(SecurityStatusPalErrorCode errorCode)
		{
			return errorCode != SecurityStatusPalErrorCode.NotSet && errorCode != SecurityStatusPalErrorCode.OK && errorCode != SecurityStatusPalErrorCode.ContinueNeeded && errorCode != SecurityStatusPalErrorCode.CompleteNeeded && errorCode != SecurityStatusPalErrorCode.CompAndContinue && errorCode != SecurityStatusPalErrorCode.ContextExpired && errorCode != SecurityStatusPalErrorCode.CredentialsNeeded && errorCode != SecurityStatusPalErrorCode.Renegotiate;
		}

		// Token: 0x06001895 RID: 6293 RVA: 0x0003BAB5 File Offset: 0x00039CB5
		public uint InitializeSspiPackage(ref uint maxLength)
		{
			throw new PlatformNotSupportedException();
		}

		// Token: 0x06001896 RID: 6294 RVA: 0x0007F604 File Offset: 0x0007D804
		public uint SetConnectionBufferSize(SNIHandle handle, uint bufferSize)
		{
			handle.SetBufferSize((int)bufferSize);
			return 0U;
		}

		// Token: 0x06001897 RID: 6295 RVA: 0x0007F610 File Offset: 0x0007D810
		public uint PacketGetData(SNIPacket packet, byte[] inBuff, ref uint dataSize)
		{
			int num = 0;
			packet.GetData(inBuff, ref num);
			dataSize = (uint)num;
			return 0U;
		}

		// Token: 0x06001898 RID: 6296 RVA: 0x0007F62C File Offset: 0x0007D82C
		public uint ReadSyncOverAsync(SNIHandle handle, out SNIPacket packet, int timeout)
		{
			return handle.Receive(out packet, timeout);
		}

		// Token: 0x06001899 RID: 6297 RVA: 0x0007F636 File Offset: 0x0007D836
		public uint GetConnectionId(SNIHandle handle, ref Guid clientConnectionId)
		{
			clientConnectionId = handle.ConnectionId;
			return 0U;
		}

		// Token: 0x0600189A RID: 6298 RVA: 0x0007F645 File Offset: 0x0007D845
		public uint WritePacket(SNIHandle handle, SNIPacket packet, bool sync)
		{
			if (sync)
			{
				return handle.Send(packet.Clone());
			}
			return handle.SendAsync(packet.Clone(), null);
		}

		// Token: 0x0600189B RID: 6299 RVA: 0x0007F664 File Offset: 0x0007D864
		public SNIHandle CreateConnectionHandle(object callbackObject, string fullServerName, bool ignoreSniOpenTimeout, long timerExpire, out byte[] instanceName, ref byte[] spnBuffer, bool flushCache, bool async, bool parallel, bool isIntegratedSecurity)
		{
			instanceName = new byte[1];
			bool flag;
			string localDBDataSource = this.GetLocalDBDataSource(fullServerName, out flag);
			if (flag)
			{
				return null;
			}
			fullServerName = (localDBDataSource ?? fullServerName);
			DataSource dataSource = DataSource.ParseServerName(fullServerName);
			if (dataSource == null)
			{
				return null;
			}
			SNIHandle result = null;
			switch (dataSource.ConnectionProtocol)
			{
			case DataSource.Protocol.TCP:
			case DataSource.Protocol.None:
			case DataSource.Protocol.Admin:
				result = this.CreateTcpHandle(dataSource, timerExpire, callbackObject, parallel);
				break;
			case DataSource.Protocol.NP:
				result = this.CreateNpHandle(dataSource, timerExpire, callbackObject, parallel);
				break;
			}
			if (isIntegratedSecurity)
			{
				try
				{
					spnBuffer = SNIProxy.GetSqlServerSPN(dataSource);
				}
				catch (Exception sniException)
				{
					SNILoadHandle.SingletonInstance.LastError = new SNIError(SNIProviders.INVALID_PROV, 44U, sniException);
				}
			}
			return result;
		}

		// Token: 0x0600189C RID: 6300 RVA: 0x0007F718 File Offset: 0x0007D918
		private static byte[] GetSqlServerSPN(DataSource dataSource)
		{
			string serverName = dataSource.ServerName;
			string portOrInstanceName = null;
			if (dataSource.Port != -1)
			{
				portOrInstanceName = dataSource.Port.ToString();
			}
			else if (!string.IsNullOrWhiteSpace(dataSource.InstanceName))
			{
				portOrInstanceName = dataSource.InstanceName;
			}
			else if (dataSource.ConnectionProtocol == DataSource.Protocol.TCP)
			{
				portOrInstanceName = 1433.ToString();
			}
			return SNIProxy.GetSqlServerSPN(serverName, portOrInstanceName);
		}

		// Token: 0x0600189D RID: 6301 RVA: 0x0007F77C File Offset: 0x0007D97C
		private static byte[] GetSqlServerSPN(string hostNameOrAddress, string portOrInstanceName)
		{
			string hostName = Dns.GetHostEntry(hostNameOrAddress).HostName;
			string text = "MSSQLSvc/" + hostName;
			if (!string.IsNullOrWhiteSpace(portOrInstanceName))
			{
				text = text + ":" + portOrInstanceName;
			}
			return Encoding.UTF8.GetBytes(text);
		}

		// Token: 0x0600189E RID: 6302 RVA: 0x0007F7C4 File Offset: 0x0007D9C4
		private SNITCPHandle CreateTcpHandle(DataSource details, long timerExpire, object callbackObject, bool parallel)
		{
			string serverName = details.ServerName;
			if (string.IsNullOrWhiteSpace(serverName))
			{
				SNILoadHandle.SingletonInstance.LastError = new SNIError(SNIProviders.TCP_PROV, 0U, 25U, string.Empty);
				return null;
			}
			int port = -1;
			bool flag = details.ConnectionProtocol == DataSource.Protocol.Admin;
			if (details.IsSsrpRequired)
			{
				try
				{
					port = (flag ? SSRP.GetDacPortByInstanceName(serverName, details.InstanceName) : SSRP.GetPortByInstanceName(serverName, details.InstanceName));
					goto IL_98;
				}
				catch (SocketException sniException)
				{
					SNILoadHandle.SingletonInstance.LastError = new SNIError(SNIProviders.TCP_PROV, 25U, sniException);
					return null;
				}
			}
			if (details.Port != -1)
			{
				port = details.Port;
			}
			else
			{
				port = (flag ? 1434 : 1433);
			}
			IL_98:
			return new SNITCPHandle(serverName, port, timerExpire, callbackObject, parallel);
		}

		// Token: 0x0600189F RID: 6303 RVA: 0x0007F888 File Offset: 0x0007DA88
		private SNINpHandle CreateNpHandle(DataSource details, long timerExpire, object callbackObject, bool parallel)
		{
			if (parallel)
			{
				SNICommon.ReportSNIError(SNIProviders.NP_PROV, 0U, 49U, string.Empty);
				return null;
			}
			return new SNINpHandle(details.PipeHostName, details.PipeName, timerExpire, callbackObject);
		}

		// Token: 0x060018A0 RID: 6304 RVA: 0x0007F8B2 File Offset: 0x0007DAB2
		public uint ReadAsync(SNIHandle handle, out SNIPacket packet)
		{
			packet = new SNIPacket(null);
			return handle.ReceiveAsync(ref packet);
		}

		// Token: 0x060018A1 RID: 6305 RVA: 0x0007F8C3 File Offset: 0x0007DAC3
		public void PacketSetData(SNIPacket packet, byte[] data, int length)
		{
			packet.SetData(data, length);
		}

		// Token: 0x060018A2 RID: 6306 RVA: 0x0007F8CD File Offset: 0x0007DACD
		public void PacketRelease(SNIPacket packet)
		{
			packet.Release();
		}

		// Token: 0x060018A3 RID: 6307 RVA: 0x0007F8D5 File Offset: 0x0007DAD5
		public uint CheckConnection(SNIHandle handle)
		{
			return handle.CheckConnection();
		}

		// Token: 0x060018A4 RID: 6308 RVA: 0x0007F8DD File Offset: 0x0007DADD
		public SNIError GetLastError()
		{
			return SNILoadHandle.SingletonInstance.LastError;
		}

		// Token: 0x060018A5 RID: 6309 RVA: 0x0007F8EC File Offset: 0x0007DAEC
		private string GetLocalDBDataSource(string fullServerName, out bool error)
		{
			string result = null;
			bool flag;
			string localDBInstance = DataSource.GetLocalDBInstance(fullServerName, out flag);
			if (flag)
			{
				error = true;
				return null;
			}
			if (!string.IsNullOrEmpty(localDBInstance))
			{
				result = LocalDB.GetLocalDBConnectionString(localDBInstance);
				if (fullServerName == null)
				{
					error = true;
					return null;
				}
			}
			error = false;
			return result;
		}

		// Token: 0x060018A6 RID: 6310 RVA: 0x00005C14 File Offset: 0x00003E14
		public SNIProxy()
		{
		}

		// Token: 0x060018A7 RID: 6311 RVA: 0x0007F927 File Offset: 0x0007DB27
		// Note: this type is marked as 'beforefieldinit'.
		static SNIProxy()
		{
		}

		// Token: 0x04001271 RID: 4721
		private const int DefaultSqlServerPort = 1433;

		// Token: 0x04001272 RID: 4722
		private const int DefaultSqlServerDacPort = 1434;

		// Token: 0x04001273 RID: 4723
		private const string SqlServerSpnHeader = "MSSQLSvc";

		// Token: 0x04001274 RID: 4724
		public static readonly SNIProxy Singleton = new SNIProxy();

		// Token: 0x02000233 RID: 563
		internal class SspiClientContextResult
		{
			// Token: 0x060018A8 RID: 6312 RVA: 0x00005C14 File Offset: 0x00003E14
			public SspiClientContextResult()
			{
			}

			// Token: 0x04001275 RID: 4725
			internal const uint OK = 0U;

			// Token: 0x04001276 RID: 4726
			internal const uint Failed = 1U;

			// Token: 0x04001277 RID: 4727
			internal const uint KerberosTicketMissing = 2U;
		}
	}
}
