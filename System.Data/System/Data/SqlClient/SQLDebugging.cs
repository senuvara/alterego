﻿using System;
using System.Runtime.InteropServices;

namespace System.Data.SqlClient
{
	/// <summary>Included to support debugging applications. Not intended for direct use.</summary>
	// Token: 0x0200021B RID: 539
	[ClassInterface(ClassInterfaceType.None)]
	[Guid("afef65ad-4577-447a-a148-83acadd3d4b9")]
	[ComVisible(true)]
	public sealed class SQLDebugging
	{
		/// <summary>Included to support debugging applications. Not intended for direct use.</summary>
		// Token: 0x06001812 RID: 6162 RVA: 0x0007D8FC File Offset: 0x0007BAFC
		[MonoTODO]
		public SQLDebugging()
		{
			throw new NotImplementedException();
		}
	}
}
