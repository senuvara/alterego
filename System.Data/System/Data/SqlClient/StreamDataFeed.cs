﻿using System;
using System.IO;

namespace System.Data.SqlClient
{
	// Token: 0x020001B4 RID: 436
	internal class StreamDataFeed : DataFeed
	{
		// Token: 0x060013DE RID: 5086 RVA: 0x00066C79 File Offset: 0x00064E79
		internal StreamDataFeed(Stream source)
		{
			this._source = source;
		}

		// Token: 0x04000E3B RID: 3643
		internal Stream _source;
	}
}
