﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Globalization;
using System.Threading;

namespace System.Data.SqlClient
{
	// Token: 0x0200019B RID: 411
	internal class SqlDependencyPerAppDomainDispatcher : MarshalByRefObject
	{
		// Token: 0x060012E9 RID: 4841 RVA: 0x00061E4C File Offset: 0x0006004C
		private SqlDependencyPerAppDomainDispatcher()
		{
			this._dependencyIdToDependencyHash = new Dictionary<string, SqlDependency>();
			this._notificationIdToDependenciesHash = new Dictionary<string, SqlDependencyPerAppDomainDispatcher.DependencyList>();
			this._commandHashToNotificationId = new Dictionary<string, string>();
			this._timeoutTimer = new Timer(new TimerCallback(SqlDependencyPerAppDomainDispatcher.TimeoutTimerCallback), null, -1, -1);
			this.SubscribeToAppDomainUnload();
		}

		// Token: 0x060012EA RID: 4842 RVA: 0x00004526 File Offset: 0x00002726
		public override object InitializeLifetimeService()
		{
			return null;
		}

		// Token: 0x060012EB RID: 4843 RVA: 0x00061EAC File Offset: 0x000600AC
		internal void AddDependencyEntry(SqlDependency dep)
		{
			object instanceLock = this._instanceLock;
			lock (instanceLock)
			{
				this._dependencyIdToDependencyHash.Add(dep.Id, dep);
			}
		}

		// Token: 0x060012EC RID: 4844 RVA: 0x00061EF8 File Offset: 0x000600F8
		internal string AddCommandEntry(string commandHash, SqlDependency dep)
		{
			string text = string.Empty;
			object instanceLock = this._instanceLock;
			lock (instanceLock)
			{
				if (this._dependencyIdToDependencyHash.ContainsKey(dep.Id))
				{
					if (this._commandHashToNotificationId.TryGetValue(commandHash, out text))
					{
						SqlDependencyPerAppDomainDispatcher.DependencyList dependencyList = null;
						if (!this._notificationIdToDependenciesHash.TryGetValue(text, out dependencyList))
						{
							throw ADP.InternalError(ADP.InternalErrorCode.SqlDependencyCommandHashIsNotAssociatedWithNotification);
						}
						if (!dependencyList.Contains(dep))
						{
							dependencyList.Add(dep);
						}
					}
					else
					{
						text = string.Format(CultureInfo.InvariantCulture, "{0};{1}", SqlDependency.AppDomainKey, Guid.NewGuid().ToString("D", CultureInfo.InvariantCulture));
						SqlDependencyPerAppDomainDispatcher.DependencyList dependencyList2 = new SqlDependencyPerAppDomainDispatcher.DependencyList(commandHash);
						dependencyList2.Add(dep);
						this._commandHashToNotificationId.Add(commandHash, text);
						this._notificationIdToDependenciesHash.Add(text, dependencyList2);
					}
				}
			}
			return text;
		}

		// Token: 0x060012ED RID: 4845 RVA: 0x00061FE4 File Offset: 0x000601E4
		internal void InvalidateCommandID(SqlNotification sqlNotification)
		{
			List<SqlDependency> list = null;
			object instanceLock = this._instanceLock;
			lock (instanceLock)
			{
				list = this.LookupCommandEntryWithRemove(sqlNotification.Key);
				if (list != null)
				{
					foreach (SqlDependency sqlDependency in list)
					{
						this.LookupDependencyEntryWithRemove(sqlDependency.Id);
						this.RemoveDependencyFromCommandToDependenciesHash(sqlDependency);
					}
				}
			}
			if (list != null)
			{
				foreach (SqlDependency sqlDependency2 in list)
				{
					try
					{
						sqlDependency2.Invalidate(sqlNotification.Type, sqlNotification.Info, sqlNotification.Source);
					}
					catch (Exception e)
					{
						if (!ADP.IsCatchableExceptionType(e))
						{
							throw;
						}
						ADP.TraceExceptionWithoutRethrow(e);
					}
				}
			}
		}

		// Token: 0x060012EE RID: 4846 RVA: 0x000620F0 File Offset: 0x000602F0
		internal void InvalidateServer(string server, SqlNotification sqlNotification)
		{
			List<SqlDependency> list = new List<SqlDependency>();
			object instanceLock = this._instanceLock;
			lock (instanceLock)
			{
				foreach (KeyValuePair<string, SqlDependency> keyValuePair in this._dependencyIdToDependencyHash)
				{
					SqlDependency value = keyValuePair.Value;
					if (value.ContainsServer(server))
					{
						list.Add(value);
					}
				}
				foreach (SqlDependency sqlDependency in list)
				{
					this.LookupDependencyEntryWithRemove(sqlDependency.Id);
					this.RemoveDependencyFromCommandToDependenciesHash(sqlDependency);
				}
			}
			foreach (SqlDependency sqlDependency2 in list)
			{
				try
				{
					sqlDependency2.Invalidate(sqlNotification.Type, sqlNotification.Info, sqlNotification.Source);
				}
				catch (Exception e)
				{
					if (!ADP.IsCatchableExceptionType(e))
					{
						throw;
					}
					ADP.TraceExceptionWithoutRethrow(e);
				}
			}
		}

		// Token: 0x060012EF RID: 4847 RVA: 0x00062244 File Offset: 0x00060444
		internal SqlDependency LookupDependencyEntry(string id)
		{
			if (id == null)
			{
				throw ADP.ArgumentNull("id");
			}
			if (string.IsNullOrEmpty(id))
			{
				throw SQL.SqlDependencyIdMismatch();
			}
			SqlDependency result = null;
			object instanceLock = this._instanceLock;
			lock (instanceLock)
			{
				if (this._dependencyIdToDependencyHash.ContainsKey(id))
				{
					result = this._dependencyIdToDependencyHash[id];
				}
			}
			return result;
		}

		// Token: 0x060012F0 RID: 4848 RVA: 0x000622B8 File Offset: 0x000604B8
		private void LookupDependencyEntryWithRemove(string id)
		{
			object instanceLock = this._instanceLock;
			lock (instanceLock)
			{
				if (this._dependencyIdToDependencyHash.ContainsKey(id))
				{
					this._dependencyIdToDependencyHash.Remove(id);
					if (this._dependencyIdToDependencyHash.Count == 0)
					{
						this._timeoutTimer.Change(-1, -1);
						this._sqlDependencyTimeOutTimerStarted = false;
					}
				}
			}
		}

		// Token: 0x060012F1 RID: 4849 RVA: 0x00062330 File Offset: 0x00060530
		private List<SqlDependency> LookupCommandEntryWithRemove(string notificationId)
		{
			SqlDependencyPerAppDomainDispatcher.DependencyList dependencyList = null;
			object instanceLock = this._instanceLock;
			lock (instanceLock)
			{
				if (this._notificationIdToDependenciesHash.TryGetValue(notificationId, out dependencyList))
				{
					this._notificationIdToDependenciesHash.Remove(notificationId);
					this._commandHashToNotificationId.Remove(dependencyList.CommandHash);
				}
			}
			return dependencyList;
		}

		// Token: 0x060012F2 RID: 4850 RVA: 0x0006239C File Offset: 0x0006059C
		private void RemoveDependencyFromCommandToDependenciesHash(SqlDependency dependency)
		{
			object instanceLock = this._instanceLock;
			lock (instanceLock)
			{
				List<string> list = new List<string>();
				List<string> list2 = new List<string>();
				foreach (KeyValuePair<string, SqlDependencyPerAppDomainDispatcher.DependencyList> keyValuePair in this._notificationIdToDependenciesHash)
				{
					SqlDependencyPerAppDomainDispatcher.DependencyList value = keyValuePair.Value;
					if (value.Remove(dependency) && value.Count == 0)
					{
						list.Add(keyValuePair.Key);
						list2.Add(keyValuePair.Value.CommandHash);
					}
				}
				for (int i = 0; i < list.Count; i++)
				{
					this._notificationIdToDependenciesHash.Remove(list[i]);
					this._commandHashToNotificationId.Remove(list2[i]);
				}
			}
		}

		// Token: 0x060012F3 RID: 4851 RVA: 0x00062498 File Offset: 0x00060698
		internal void StartTimer(SqlDependency dep)
		{
			object instanceLock = this._instanceLock;
			lock (instanceLock)
			{
				if (!this._sqlDependencyTimeOutTimerStarted)
				{
					this._timeoutTimer.Change(15000, 15000);
					this._nextTimeout = dep.ExpirationTime;
					this._sqlDependencyTimeOutTimerStarted = true;
				}
				else if (this._nextTimeout > dep.ExpirationTime)
				{
					this._nextTimeout = dep.ExpirationTime;
				}
			}
		}

		// Token: 0x060012F4 RID: 4852 RVA: 0x00062524 File Offset: 0x00060724
		private static void TimeoutTimerCallback(object state)
		{
			object instanceLock = SqlDependencyPerAppDomainDispatcher.SingletonInstance._instanceLock;
			SqlDependency[] array;
			lock (instanceLock)
			{
				if (SqlDependencyPerAppDomainDispatcher.SingletonInstance._dependencyIdToDependencyHash.Count == 0)
				{
					return;
				}
				if (SqlDependencyPerAppDomainDispatcher.SingletonInstance._nextTimeout > DateTime.UtcNow)
				{
					return;
				}
				array = new SqlDependency[SqlDependencyPerAppDomainDispatcher.SingletonInstance._dependencyIdToDependencyHash.Count];
				SqlDependencyPerAppDomainDispatcher.SingletonInstance._dependencyIdToDependencyHash.Values.CopyTo(array, 0);
			}
			DateTime utcNow = DateTime.UtcNow;
			DateTime dateTime = DateTime.MaxValue;
			int i = 0;
			while (i < array.Length)
			{
				if (array[i].ExpirationTime <= utcNow)
				{
					try
					{
						array[i].Invalidate(SqlNotificationType.Change, SqlNotificationInfo.Error, SqlNotificationSource.Timeout);
						goto IL_E0;
					}
					catch (Exception e)
					{
						if (!ADP.IsCatchableExceptionType(e))
						{
							throw;
						}
						ADP.TraceExceptionWithoutRethrow(e);
						goto IL_E0;
					}
					goto IL_C0;
				}
				goto IL_C0;
				IL_E0:
				i++;
				continue;
				IL_C0:
				if (array[i].ExpirationTime < dateTime)
				{
					dateTime = array[i].ExpirationTime;
				}
				array[i] = null;
				goto IL_E0;
			}
			instanceLock = SqlDependencyPerAppDomainDispatcher.SingletonInstance._instanceLock;
			lock (instanceLock)
			{
				for (int j = 0; j < array.Length; j++)
				{
					if (array[j] != null)
					{
						SqlDependencyPerAppDomainDispatcher.SingletonInstance._dependencyIdToDependencyHash.Remove(array[j].Id);
					}
				}
				if (dateTime < SqlDependencyPerAppDomainDispatcher.SingletonInstance._nextTimeout)
				{
					SqlDependencyPerAppDomainDispatcher.SingletonInstance._nextTimeout = dateTime;
				}
			}
		}

		// Token: 0x060012F5 RID: 4853 RVA: 0x00005E03 File Offset: 0x00004003
		private void SubscribeToAppDomainUnload()
		{
		}

		// Token: 0x060012F6 RID: 4854 RVA: 0x000626B8 File Offset: 0x000608B8
		// Note: this type is marked as 'beforefieldinit'.
		static SqlDependencyPerAppDomainDispatcher()
		{
		}

		// Token: 0x04000D4D RID: 3405
		internal static readonly SqlDependencyPerAppDomainDispatcher SingletonInstance = new SqlDependencyPerAppDomainDispatcher();

		// Token: 0x04000D4E RID: 3406
		internal object _instanceLock = new object();

		// Token: 0x04000D4F RID: 3407
		private Dictionary<string, SqlDependency> _dependencyIdToDependencyHash;

		// Token: 0x04000D50 RID: 3408
		private Dictionary<string, SqlDependencyPerAppDomainDispatcher.DependencyList> _notificationIdToDependenciesHash;

		// Token: 0x04000D51 RID: 3409
		private Dictionary<string, string> _commandHashToNotificationId;

		// Token: 0x04000D52 RID: 3410
		private bool _sqlDependencyTimeOutTimerStarted;

		// Token: 0x04000D53 RID: 3411
		private DateTime _nextTimeout;

		// Token: 0x04000D54 RID: 3412
		private Timer _timeoutTimer;

		// Token: 0x0200019C RID: 412
		private sealed class DependencyList : List<SqlDependency>
		{
			// Token: 0x060012F7 RID: 4855 RVA: 0x000626C4 File Offset: 0x000608C4
			internal DependencyList(string commandHash)
			{
				this.CommandHash = commandHash;
			}

			// Token: 0x04000D55 RID: 3413
			public readonly string CommandHash;
		}
	}
}
