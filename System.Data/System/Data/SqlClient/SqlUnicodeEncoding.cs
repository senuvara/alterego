﻿using System;
using System.Text;

namespace System.Data.SqlClient
{
	// Token: 0x020001C7 RID: 455
	internal sealed class SqlUnicodeEncoding : UnicodeEncoding
	{
		// Token: 0x060014C7 RID: 5319 RVA: 0x0006A444 File Offset: 0x00068644
		private SqlUnicodeEncoding() : base(false, false, false)
		{
		}

		// Token: 0x060014C8 RID: 5320 RVA: 0x0006A44F File Offset: 0x0006864F
		public override Decoder GetDecoder()
		{
			return new SqlUnicodeEncoding.SqlUnicodeDecoder();
		}

		// Token: 0x060014C9 RID: 5321 RVA: 0x0006A456 File Offset: 0x00068656
		public override int GetMaxByteCount(int charCount)
		{
			return charCount * 2;
		}

		// Token: 0x170003FF RID: 1023
		// (get) Token: 0x060014CA RID: 5322 RVA: 0x0006A45B File Offset: 0x0006865B
		public static Encoding SqlUnicodeEncodingInstance
		{
			get
			{
				return SqlUnicodeEncoding.s_singletonEncoding;
			}
		}

		// Token: 0x060014CB RID: 5323 RVA: 0x0006A462 File Offset: 0x00068662
		// Note: this type is marked as 'beforefieldinit'.
		static SqlUnicodeEncoding()
		{
		}

		// Token: 0x04000E7E RID: 3710
		private static SqlUnicodeEncoding s_singletonEncoding = new SqlUnicodeEncoding();

		// Token: 0x020001C8 RID: 456
		private sealed class SqlUnicodeDecoder : Decoder
		{
			// Token: 0x060014CC RID: 5324 RVA: 0x0006A46E File Offset: 0x0006866E
			public override int GetCharCount(byte[] bytes, int index, int count)
			{
				return count / 2;
			}

			// Token: 0x060014CD RID: 5325 RVA: 0x0006A474 File Offset: 0x00068674
			public override int GetChars(byte[] bytes, int byteIndex, int byteCount, char[] chars, int charIndex)
			{
				int num;
				int result;
				bool flag;
				this.Convert(bytes, byteIndex, byteCount, chars, charIndex, chars.Length - charIndex, true, out num, out result, out flag);
				return result;
			}

			// Token: 0x060014CE RID: 5326 RVA: 0x0006A49D File Offset: 0x0006869D
			public override void Convert(byte[] bytes, int byteIndex, int byteCount, char[] chars, int charIndex, int charCount, bool flush, out int bytesUsed, out int charsUsed, out bool completed)
			{
				charsUsed = Math.Min(charCount, byteCount / 2);
				bytesUsed = charsUsed * 2;
				completed = (bytesUsed == byteCount);
				Buffer.BlockCopy(bytes, byteIndex, chars, charIndex * 2, bytesUsed);
			}

			// Token: 0x060014CF RID: 5327 RVA: 0x0006A4CD File Offset: 0x000686CD
			public SqlUnicodeDecoder()
			{
			}
		}
	}
}
