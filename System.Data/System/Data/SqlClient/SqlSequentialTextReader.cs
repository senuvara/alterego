﻿using System;
using System.Data.Common;
using System.IO;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace System.Data.SqlClient
{
	// Token: 0x020001C4 RID: 452
	internal sealed class SqlSequentialTextReader : TextReader
	{
		// Token: 0x060014B6 RID: 5302 RVA: 0x00069C94 File Offset: 0x00067E94
		internal SqlSequentialTextReader(SqlDataReader reader, int columnIndex, Encoding encoding)
		{
			this._reader = reader;
			this._columnIndex = columnIndex;
			this._encoding = encoding;
			this._decoder = encoding.GetDecoder();
			this._leftOverBytes = null;
			this._peekedChar = -1;
			this._currentTask = null;
			this._disposalTokenSource = new CancellationTokenSource();
		}

		// Token: 0x170003FC RID: 1020
		// (get) Token: 0x060014B7 RID: 5303 RVA: 0x00069CE8 File Offset: 0x00067EE8
		internal int ColumnIndex
		{
			get
			{
				return this._columnIndex;
			}
		}

		// Token: 0x060014B8 RID: 5304 RVA: 0x00069CF0 File Offset: 0x00067EF0
		public override int Peek()
		{
			if (this._currentTask != null)
			{
				throw ADP.AsyncOperationPending();
			}
			if (this.IsClosed)
			{
				throw ADP.ObjectDisposed(this);
			}
			if (!this.HasPeekedChar)
			{
				this._peekedChar = this.Read();
			}
			return this._peekedChar;
		}

		// Token: 0x060014B9 RID: 5305 RVA: 0x00069D2C File Offset: 0x00067F2C
		public override int Read()
		{
			if (this._currentTask != null)
			{
				throw ADP.AsyncOperationPending();
			}
			if (this.IsClosed)
			{
				throw ADP.ObjectDisposed(this);
			}
			int result = -1;
			if (this.HasPeekedChar)
			{
				result = this._peekedChar;
				this._peekedChar = -1;
			}
			else
			{
				char[] array = new char[1];
				if (this.InternalRead(array, 0, 1) == 1)
				{
					result = (int)array[0];
				}
			}
			return result;
		}

		// Token: 0x060014BA RID: 5306 RVA: 0x00069D88 File Offset: 0x00067F88
		public override int Read(char[] buffer, int index, int count)
		{
			SqlSequentialTextReader.ValidateReadParameters(buffer, index, count);
			if (this.IsClosed)
			{
				throw ADP.ObjectDisposed(this);
			}
			if (this._currentTask != null)
			{
				throw ADP.AsyncOperationPending();
			}
			int num = 0;
			int num2 = count;
			if (num2 > 0 && this.HasPeekedChar)
			{
				buffer[index + num] = (char)this._peekedChar;
				num++;
				num2--;
				this._peekedChar = -1;
			}
			return num + this.InternalRead(buffer, index + num, num2);
		}

		// Token: 0x060014BB RID: 5307 RVA: 0x00069DF4 File Offset: 0x00067FF4
		public override Task<int> ReadAsync(char[] buffer, int index, int count)
		{
			SqlSequentialTextReader.ValidateReadParameters(buffer, index, count);
			TaskCompletionSource<int> completion = new TaskCompletionSource<int>();
			if (this.IsClosed)
			{
				completion.SetException(ADP.ExceptionWithStackTrace(ADP.ObjectDisposed(this)));
			}
			else
			{
				try
				{
					if (Interlocked.CompareExchange<Task>(ref this._currentTask, completion.Task, null) != null)
					{
						completion.SetException(ADP.ExceptionWithStackTrace(ADP.AsyncOperationPending()));
					}
					else
					{
						bool flag = true;
						int charsRead = 0;
						int adjustedIndex = index;
						int charsNeeded = count;
						if (this.HasPeekedChar && charsNeeded > 0)
						{
							int peekedChar = this._peekedChar;
							if (peekedChar >= 0)
							{
								buffer[adjustedIndex] = (char)peekedChar;
								int num = adjustedIndex;
								adjustedIndex = num + 1;
								num = charsRead;
								charsRead = num + 1;
								num = charsNeeded;
								charsNeeded = num - 1;
								this._peekedChar = -1;
							}
						}
						int byteBufferUsed;
						byte[] byteBuffer = this.PrepareByteBuffer(charsNeeded, out byteBufferUsed);
						if (byteBufferUsed < byteBuffer.Length || byteBuffer.Length == 0)
						{
							SqlDataReader reader = this._reader;
							if (reader != null)
							{
								int num2;
								Task<int> bytesAsync = reader.GetBytesAsync(this._columnIndex, byteBuffer, byteBufferUsed, byteBuffer.Length - byteBufferUsed, -1, this._disposalTokenSource.Token, out num2);
								if (bytesAsync == null)
								{
									byteBufferUsed += num2;
								}
								else
								{
									flag = false;
									bytesAsync.ContinueWith(delegate(Task<int> t)
									{
										this._currentTask = null;
										if (t.Status == TaskStatus.RanToCompletion && !this.IsClosed)
										{
											try
											{
												int result = t.Result;
												byteBufferUsed += result;
												if (byteBufferUsed > 0)
												{
													charsRead += this.DecodeBytesToChars(byteBuffer, byteBufferUsed, buffer, adjustedIndex, charsNeeded);
												}
												completion.SetResult(charsRead);
												return;
											}
											catch (Exception exception2)
											{
												completion.SetException(exception2);
												return;
											}
										}
										if (this.IsClosed)
										{
											completion.SetException(ADP.ExceptionWithStackTrace(ADP.ObjectDisposed(this)));
											return;
										}
										if (t.Status == TaskStatus.Faulted)
										{
											if (t.Exception.InnerException is SqlException)
											{
												completion.SetException(ADP.ExceptionWithStackTrace(ADP.ErrorReadingFromStream(t.Exception.InnerException)));
												return;
											}
											completion.SetException(t.Exception.InnerException);
											return;
										}
										else
										{
											completion.SetCanceled();
										}
									}, TaskScheduler.Default);
								}
								if (flag && byteBufferUsed > 0)
								{
									charsRead += this.DecodeBytesToChars(byteBuffer, byteBufferUsed, buffer, adjustedIndex, charsNeeded);
								}
							}
							else
							{
								completion.SetException(ADP.ExceptionWithStackTrace(ADP.ObjectDisposed(this)));
							}
						}
						if (flag)
						{
							this._currentTask = null;
							if (this.IsClosed)
							{
								completion.SetCanceled();
							}
							else
							{
								completion.SetResult(charsRead);
							}
						}
					}
				}
				catch (Exception exception)
				{
					completion.TrySetException(exception);
					Interlocked.CompareExchange<Task>(ref this._currentTask, null, completion.Task);
					throw;
				}
			}
			return completion.Task;
		}

		// Token: 0x060014BC RID: 5308 RVA: 0x0006A0AC File Offset: 0x000682AC
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				this.SetClosed();
			}
			base.Dispose(disposing);
		}

		// Token: 0x060014BD RID: 5309 RVA: 0x0006A0C0 File Offset: 0x000682C0
		internal void SetClosed()
		{
			this._disposalTokenSource.Cancel();
			this._reader = null;
			this._peekedChar = -1;
			Task currentTask = this._currentTask;
			if (currentTask != null)
			{
				((IAsyncResult)currentTask).AsyncWaitHandle.WaitOne();
			}
		}

		// Token: 0x060014BE RID: 5310 RVA: 0x0006A0FC File Offset: 0x000682FC
		private int InternalRead(char[] buffer, int index, int count)
		{
			int result;
			try
			{
				int num;
				byte[] array = this.PrepareByteBuffer(count, out num);
				num += this._reader.GetBytesInternalSequential(this._columnIndex, array, num, array.Length - num, null);
				if (num > 0)
				{
					result = this.DecodeBytesToChars(array, num, buffer, index, count);
				}
				else
				{
					result = 0;
				}
			}
			catch (SqlException internalException)
			{
				throw ADP.ErrorReadingFromStream(internalException);
			}
			return result;
		}

		// Token: 0x060014BF RID: 5311 RVA: 0x0006A164 File Offset: 0x00068364
		private byte[] PrepareByteBuffer(int numberOfChars, out int byteBufferUsed)
		{
			byte[] array;
			if (numberOfChars == 0)
			{
				array = Array.Empty<byte>();
				byteBufferUsed = 0;
			}
			else
			{
				int maxByteCount = this._encoding.GetMaxByteCount(numberOfChars);
				if (this._leftOverBytes != null)
				{
					if (this._leftOverBytes.Length > maxByteCount)
					{
						array = this._leftOverBytes;
						byteBufferUsed = array.Length;
					}
					else
					{
						array = new byte[maxByteCount];
						Buffer.BlockCopy(this._leftOverBytes, 0, array, 0, this._leftOverBytes.Length);
						byteBufferUsed = this._leftOverBytes.Length;
					}
				}
				else
				{
					array = new byte[maxByteCount];
					byteBufferUsed = 0;
				}
			}
			return array;
		}

		// Token: 0x060014C0 RID: 5312 RVA: 0x0006A1E4 File Offset: 0x000683E4
		private int DecodeBytesToChars(byte[] inBuffer, int inBufferCount, char[] outBuffer, int outBufferOffset, int outBufferCount)
		{
			int num;
			int result;
			bool flag;
			this._decoder.Convert(inBuffer, 0, inBufferCount, outBuffer, outBufferOffset, outBufferCount, false, out num, out result, out flag);
			if (!flag && num < inBufferCount)
			{
				this._leftOverBytes = new byte[inBufferCount - num];
				Buffer.BlockCopy(inBuffer, num, this._leftOverBytes, 0, this._leftOverBytes.Length);
			}
			else
			{
				this._leftOverBytes = null;
			}
			return result;
		}

		// Token: 0x170003FD RID: 1021
		// (get) Token: 0x060014C1 RID: 5313 RVA: 0x0006A240 File Offset: 0x00068440
		private bool IsClosed
		{
			get
			{
				return this._reader == null;
			}
		}

		// Token: 0x170003FE RID: 1022
		// (get) Token: 0x060014C2 RID: 5314 RVA: 0x0006A24B File Offset: 0x0006844B
		private bool HasPeekedChar
		{
			get
			{
				return this._peekedChar >= 0;
			}
		}

		// Token: 0x060014C3 RID: 5315 RVA: 0x0006A25C File Offset: 0x0006845C
		internal static void ValidateReadParameters(char[] buffer, int index, int count)
		{
			if (buffer == null)
			{
				throw ADP.ArgumentNull("buffer");
			}
			if (index < 0)
			{
				throw ADP.ArgumentOutOfRange("index");
			}
			if (count < 0)
			{
				throw ADP.ArgumentOutOfRange("count");
			}
			try
			{
				if (checked(index + count) > buffer.Length)
				{
					throw ExceptionBuilder.InvalidOffsetLength();
				}
			}
			catch (OverflowException)
			{
				throw ExceptionBuilder.InvalidOffsetLength();
			}
		}

		// Token: 0x04000E6D RID: 3693
		private SqlDataReader _reader;

		// Token: 0x04000E6E RID: 3694
		private int _columnIndex;

		// Token: 0x04000E6F RID: 3695
		private Encoding _encoding;

		// Token: 0x04000E70 RID: 3696
		private Decoder _decoder;

		// Token: 0x04000E71 RID: 3697
		private byte[] _leftOverBytes;

		// Token: 0x04000E72 RID: 3698
		private int _peekedChar;

		// Token: 0x04000E73 RID: 3699
		private Task _currentTask;

		// Token: 0x04000E74 RID: 3700
		private CancellationTokenSource _disposalTokenSource;

		// Token: 0x020001C5 RID: 453
		[CompilerGenerated]
		private sealed class <>c__DisplayClass14_0
		{
			// Token: 0x060014C4 RID: 5316 RVA: 0x00005C14 File Offset: 0x00003E14
			public <>c__DisplayClass14_0()
			{
			}

			// Token: 0x04000E75 RID: 3701
			public SqlSequentialTextReader <>4__this;

			// Token: 0x04000E76 RID: 3702
			public char[] buffer;

			// Token: 0x04000E77 RID: 3703
			public TaskCompletionSource<int> completion;
		}

		// Token: 0x020001C6 RID: 454
		[CompilerGenerated]
		private sealed class <>c__DisplayClass14_1
		{
			// Token: 0x060014C5 RID: 5317 RVA: 0x00005C14 File Offset: 0x00003E14
			public <>c__DisplayClass14_1()
			{
			}

			// Token: 0x060014C6 RID: 5318 RVA: 0x0006A2C0 File Offset: 0x000684C0
			internal void <ReadAsync>b__0(Task<int> t)
			{
				this.CS$<>8__locals1.<>4__this._currentTask = null;
				if (t.Status == TaskStatus.RanToCompletion && !this.CS$<>8__locals1.<>4__this.IsClosed)
				{
					try
					{
						int result = t.Result;
						this.byteBufferUsed += result;
						if (this.byteBufferUsed > 0)
						{
							this.charsRead += this.CS$<>8__locals1.<>4__this.DecodeBytesToChars(this.byteBuffer, this.byteBufferUsed, this.CS$<>8__locals1.buffer, this.adjustedIndex, this.charsNeeded);
						}
						this.CS$<>8__locals1.completion.SetResult(this.charsRead);
						return;
					}
					catch (Exception exception)
					{
						this.CS$<>8__locals1.completion.SetException(exception);
						return;
					}
				}
				if (this.CS$<>8__locals1.<>4__this.IsClosed)
				{
					this.CS$<>8__locals1.completion.SetException(ADP.ExceptionWithStackTrace(ADP.ObjectDisposed(this.CS$<>8__locals1.<>4__this)));
					return;
				}
				if (t.Status == TaskStatus.Faulted)
				{
					if (t.Exception.InnerException is SqlException)
					{
						this.CS$<>8__locals1.completion.SetException(ADP.ExceptionWithStackTrace(ADP.ErrorReadingFromStream(t.Exception.InnerException)));
						return;
					}
					this.CS$<>8__locals1.completion.SetException(t.Exception.InnerException);
					return;
				}
				else
				{
					this.CS$<>8__locals1.completion.SetCanceled();
				}
			}

			// Token: 0x04000E78 RID: 3704
			public int byteBufferUsed;

			// Token: 0x04000E79 RID: 3705
			public int charsRead;

			// Token: 0x04000E7A RID: 3706
			public byte[] byteBuffer;

			// Token: 0x04000E7B RID: 3707
			public int adjustedIndex;

			// Token: 0x04000E7C RID: 3708
			public int charsNeeded;

			// Token: 0x04000E7D RID: 3709
			public SqlSequentialTextReader.<>c__DisplayClass14_0 CS$<>8__locals1;
		}
	}
}
