﻿using System;
using System.Diagnostics;

namespace System.Data.SqlClient
{
	// Token: 0x02000181 RID: 385
	internal class SqlConnectionTimeoutPhaseDuration
	{
		// Token: 0x060011BF RID: 4543 RVA: 0x0005A4AC File Offset: 0x000586AC
		internal void StartCapture()
		{
			this._swDuration.Start();
		}

		// Token: 0x060011C0 RID: 4544 RVA: 0x0005A4B9 File Offset: 0x000586B9
		internal void StopCapture()
		{
			if (this._swDuration.IsRunning)
			{
				this._swDuration.Stop();
			}
		}

		// Token: 0x060011C1 RID: 4545 RVA: 0x0005A4D3 File Offset: 0x000586D3
		internal long GetMilliSecondDuration()
		{
			return this._swDuration.ElapsedMilliseconds;
		}

		// Token: 0x060011C2 RID: 4546 RVA: 0x0005A4E0 File Offset: 0x000586E0
		public SqlConnectionTimeoutPhaseDuration()
		{
		}

		// Token: 0x04000CB1 RID: 3249
		private Stopwatch _swDuration = new Stopwatch();
	}
}
