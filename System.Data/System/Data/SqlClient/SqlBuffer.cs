﻿using System;
using System.Data.SqlTypes;
using System.Globalization;
using System.Runtime.InteropServices;

namespace System.Data.SqlClient
{
	// Token: 0x02000128 RID: 296
	internal sealed class SqlBuffer
	{
		// Token: 0x06000E7D RID: 3709 RVA: 0x00005C14 File Offset: 0x00003E14
		internal SqlBuffer()
		{
		}

		// Token: 0x06000E7E RID: 3710 RVA: 0x0004BF18 File Offset: 0x0004A118
		private SqlBuffer(SqlBuffer value)
		{
			this._isNull = value._isNull;
			this._type = value._type;
			this._value = value._value;
			this._object = value._object;
		}

		// Token: 0x17000275 RID: 629
		// (get) Token: 0x06000E7F RID: 3711 RVA: 0x0004BF50 File Offset: 0x0004A150
		internal bool IsEmpty
		{
			get
			{
				return this._type == SqlBuffer.StorageType.Empty;
			}
		}

		// Token: 0x17000276 RID: 630
		// (get) Token: 0x06000E80 RID: 3712 RVA: 0x0004BF5B File Offset: 0x0004A15B
		internal bool IsNull
		{
			get
			{
				return this._isNull;
			}
		}

		// Token: 0x17000277 RID: 631
		// (get) Token: 0x06000E81 RID: 3713 RVA: 0x0004BF63 File Offset: 0x0004A163
		internal SqlBuffer.StorageType VariantInternalStorageType
		{
			get
			{
				return this._type;
			}
		}

		// Token: 0x17000278 RID: 632
		// (get) Token: 0x06000E82 RID: 3714 RVA: 0x0004BF6B File Offset: 0x0004A16B
		// (set) Token: 0x06000E83 RID: 3715 RVA: 0x0004BF93 File Offset: 0x0004A193
		internal bool Boolean
		{
			get
			{
				this.ThrowIfNull();
				if (SqlBuffer.StorageType.Boolean == this._type)
				{
					return this._value._boolean;
				}
				return (bool)this.Value;
			}
			set
			{
				this._value._boolean = value;
				this._type = SqlBuffer.StorageType.Boolean;
				this._isNull = false;
			}
		}

		// Token: 0x17000279 RID: 633
		// (get) Token: 0x06000E84 RID: 3716 RVA: 0x0004BFAF File Offset: 0x0004A1AF
		// (set) Token: 0x06000E85 RID: 3717 RVA: 0x0004BFD7 File Offset: 0x0004A1D7
		internal byte Byte
		{
			get
			{
				this.ThrowIfNull();
				if (SqlBuffer.StorageType.Byte == this._type)
				{
					return this._value._byte;
				}
				return (byte)this.Value;
			}
			set
			{
				this._value._byte = value;
				this._type = SqlBuffer.StorageType.Byte;
				this._isNull = false;
			}
		}

		// Token: 0x1700027A RID: 634
		// (get) Token: 0x06000E86 RID: 3718 RVA: 0x0004BFF4 File Offset: 0x0004A1F4
		internal byte[] ByteArray
		{
			get
			{
				this.ThrowIfNull();
				return this.SqlBinary.Value;
			}
		}

		// Token: 0x1700027B RID: 635
		// (get) Token: 0x06000E87 RID: 3719 RVA: 0x0004C018 File Offset: 0x0004A218
		internal DateTime DateTime
		{
			get
			{
				this.ThrowIfNull();
				if (SqlBuffer.StorageType.Date == this._type)
				{
					return DateTime.MinValue.AddDays((double)this._value._int32);
				}
				if (SqlBuffer.StorageType.DateTime2 == this._type)
				{
					return new DateTime(SqlBuffer.GetTicksFromDateTime2Info(this._value._dateTime2Info));
				}
				if (SqlBuffer.StorageType.DateTime == this._type)
				{
					return SqlTypeWorkarounds.SqlDateTimeToDateTime(this._value._dateTimeInfo.daypart, this._value._dateTimeInfo.timepart);
				}
				return (DateTime)this.Value;
			}
		}

		// Token: 0x1700027C RID: 636
		// (get) Token: 0x06000E88 RID: 3720 RVA: 0x0004C0AC File Offset: 0x0004A2AC
		internal decimal Decimal
		{
			get
			{
				this.ThrowIfNull();
				if (SqlBuffer.StorageType.Decimal == this._type)
				{
					if (this._value._numericInfo.data4 != 0 || this._value._numericInfo.scale > 28)
					{
						throw new OverflowException(SQLResource.ConversionOverflowMessage);
					}
					return new decimal(this._value._numericInfo.data1, this._value._numericInfo.data2, this._value._numericInfo.data3, !this._value._numericInfo.positive, this._value._numericInfo.scale);
				}
				else
				{
					if (SqlBuffer.StorageType.Money == this._type)
					{
						long num = this._value._int64;
						bool isNegative = false;
						if (num < 0L)
						{
							isNegative = true;
							num = -num;
						}
						return new decimal((int)(num & (long)((ulong)-1)), (int)(num >> 32), 0, isNegative, 4);
					}
					return (decimal)this.Value;
				}
			}
		}

		// Token: 0x1700027D RID: 637
		// (get) Token: 0x06000E89 RID: 3721 RVA: 0x0004C195 File Offset: 0x0004A395
		// (set) Token: 0x06000E8A RID: 3722 RVA: 0x0004C1BD File Offset: 0x0004A3BD
		internal double Double
		{
			get
			{
				this.ThrowIfNull();
				if (SqlBuffer.StorageType.Double == this._type)
				{
					return this._value._double;
				}
				return (double)this.Value;
			}
			set
			{
				this._value._double = value;
				this._type = SqlBuffer.StorageType.Double;
				this._isNull = false;
			}
		}

		// Token: 0x1700027E RID: 638
		// (get) Token: 0x06000E8B RID: 3723 RVA: 0x0004C1DC File Offset: 0x0004A3DC
		internal Guid Guid
		{
			get
			{
				this.ThrowIfNull();
				return this.SqlGuid.Value;
			}
		}

		// Token: 0x1700027F RID: 639
		// (get) Token: 0x06000E8C RID: 3724 RVA: 0x0004C1FD File Offset: 0x0004A3FD
		// (set) Token: 0x06000E8D RID: 3725 RVA: 0x0004C225 File Offset: 0x0004A425
		internal short Int16
		{
			get
			{
				this.ThrowIfNull();
				if (SqlBuffer.StorageType.Int16 == this._type)
				{
					return this._value._int16;
				}
				return (short)this.Value;
			}
			set
			{
				this._value._int16 = value;
				this._type = SqlBuffer.StorageType.Int16;
				this._isNull = false;
			}
		}

		// Token: 0x17000280 RID: 640
		// (get) Token: 0x06000E8E RID: 3726 RVA: 0x0004C241 File Offset: 0x0004A441
		// (set) Token: 0x06000E8F RID: 3727 RVA: 0x0004C269 File Offset: 0x0004A469
		internal int Int32
		{
			get
			{
				this.ThrowIfNull();
				if (SqlBuffer.StorageType.Int32 == this._type)
				{
					return this._value._int32;
				}
				return (int)this.Value;
			}
			set
			{
				this._value._int32 = value;
				this._type = SqlBuffer.StorageType.Int32;
				this._isNull = false;
			}
		}

		// Token: 0x17000281 RID: 641
		// (get) Token: 0x06000E90 RID: 3728 RVA: 0x0004C285 File Offset: 0x0004A485
		// (set) Token: 0x06000E91 RID: 3729 RVA: 0x0004C2AD File Offset: 0x0004A4AD
		internal long Int64
		{
			get
			{
				this.ThrowIfNull();
				if (SqlBuffer.StorageType.Int64 == this._type)
				{
					return this._value._int64;
				}
				return (long)this.Value;
			}
			set
			{
				this._value._int64 = value;
				this._type = SqlBuffer.StorageType.Int64;
				this._isNull = false;
			}
		}

		// Token: 0x17000282 RID: 642
		// (get) Token: 0x06000E92 RID: 3730 RVA: 0x0004C2C9 File Offset: 0x0004A4C9
		// (set) Token: 0x06000E93 RID: 3731 RVA: 0x0004C2F2 File Offset: 0x0004A4F2
		internal float Single
		{
			get
			{
				this.ThrowIfNull();
				if (SqlBuffer.StorageType.Single == this._type)
				{
					return this._value._single;
				}
				return (float)this.Value;
			}
			set
			{
				this._value._single = value;
				this._type = SqlBuffer.StorageType.Single;
				this._isNull = false;
			}
		}

		// Token: 0x17000283 RID: 643
		// (get) Token: 0x06000E94 RID: 3732 RVA: 0x0004C310 File Offset: 0x0004A510
		internal string String
		{
			get
			{
				this.ThrowIfNull();
				if (SqlBuffer.StorageType.String == this._type)
				{
					return (string)this._object;
				}
				if (SqlBuffer.StorageType.SqlCachedBuffer == this._type)
				{
					return ((SqlCachedBuffer)this._object).ToString();
				}
				return (string)this.Value;
			}
		}

		// Token: 0x17000284 RID: 644
		// (get) Token: 0x06000E95 RID: 3733 RVA: 0x0004C360 File Offset: 0x0004A560
		internal string KatmaiDateTimeString
		{
			get
			{
				this.ThrowIfNull();
				if (SqlBuffer.StorageType.Date == this._type)
				{
					return this.DateTime.ToString("yyyy-MM-dd", DateTimeFormatInfo.InvariantInfo);
				}
				if (SqlBuffer.StorageType.Time == this._type)
				{
					byte scale = this._value._timeInfo.scale;
					return new DateTime(this._value._timeInfo.ticks).ToString(SqlBuffer.s_katmaiTimeFormatByScale[(int)scale], DateTimeFormatInfo.InvariantInfo);
				}
				if (SqlBuffer.StorageType.DateTime2 == this._type)
				{
					byte scale2 = this._value._dateTime2Info.timeInfo.scale;
					return this.DateTime.ToString(SqlBuffer.s_katmaiDateTime2FormatByScale[(int)scale2], DateTimeFormatInfo.InvariantInfo);
				}
				if (SqlBuffer.StorageType.DateTimeOffset == this._type)
				{
					DateTimeOffset dateTimeOffset = this.DateTimeOffset;
					byte scale3 = this._value._dateTimeOffsetInfo.dateTime2Info.timeInfo.scale;
					return dateTimeOffset.ToString(SqlBuffer.s_katmaiDateTimeOffsetFormatByScale[(int)scale3], DateTimeFormatInfo.InvariantInfo);
				}
				return (string)this.Value;
			}
		}

		// Token: 0x17000285 RID: 645
		// (get) Token: 0x06000E96 RID: 3734 RVA: 0x0004C464 File Offset: 0x0004A664
		internal SqlString KatmaiDateTimeSqlString
		{
			get
			{
				if (SqlBuffer.StorageType.Date != this._type && SqlBuffer.StorageType.Time != this._type && SqlBuffer.StorageType.DateTime2 != this._type && SqlBuffer.StorageType.DateTimeOffset != this._type)
				{
					return (SqlString)this.SqlValue;
				}
				if (this.IsNull)
				{
					return SqlString.Null;
				}
				return new SqlString(this.KatmaiDateTimeString);
			}
		}

		// Token: 0x17000286 RID: 646
		// (get) Token: 0x06000E97 RID: 3735 RVA: 0x0004C4BE File Offset: 0x0004A6BE
		internal TimeSpan Time
		{
			get
			{
				this.ThrowIfNull();
				if (SqlBuffer.StorageType.Time == this._type)
				{
					return new TimeSpan(this._value._timeInfo.ticks);
				}
				return (TimeSpan)this.Value;
			}
		}

		// Token: 0x17000287 RID: 647
		// (get) Token: 0x06000E98 RID: 3736 RVA: 0x0004C4F4 File Offset: 0x0004A6F4
		internal DateTimeOffset DateTimeOffset
		{
			get
			{
				this.ThrowIfNull();
				if (SqlBuffer.StorageType.DateTimeOffset == this._type)
				{
					TimeSpan offset = new TimeSpan(0, (int)this._value._dateTimeOffsetInfo.offset, 0);
					return new DateTimeOffset(SqlBuffer.GetTicksFromDateTime2Info(this._value._dateTimeOffsetInfo.dateTime2Info) + offset.Ticks, offset);
				}
				return (DateTimeOffset)this.Value;
			}
		}

		// Token: 0x06000E99 RID: 3737 RVA: 0x0004C559 File Offset: 0x0004A759
		private static long GetTicksFromDateTime2Info(SqlBuffer.DateTime2Info dateTime2Info)
		{
			return (long)dateTime2Info.date * 864000000000L + dateTime2Info.timeInfo.ticks;
		}

		// Token: 0x17000288 RID: 648
		// (get) Token: 0x06000E9A RID: 3738 RVA: 0x0004C578 File Offset: 0x0004A778
		// (set) Token: 0x06000E9B RID: 3739 RVA: 0x0004C59B File Offset: 0x0004A79B
		internal SqlBinary SqlBinary
		{
			get
			{
				if (SqlBuffer.StorageType.SqlBinary == this._type)
				{
					return (SqlBinary)this._object;
				}
				return (SqlBinary)this.SqlValue;
			}
			set
			{
				this._object = value;
				this._type = SqlBuffer.StorageType.SqlBinary;
				this._isNull = value.IsNull;
			}
		}

		// Token: 0x17000289 RID: 649
		// (get) Token: 0x06000E9C RID: 3740 RVA: 0x0004C5BE File Offset: 0x0004A7BE
		internal SqlBoolean SqlBoolean
		{
			get
			{
				if (SqlBuffer.StorageType.Boolean != this._type)
				{
					return (SqlBoolean)this.SqlValue;
				}
				if (this.IsNull)
				{
					return SqlBoolean.Null;
				}
				return new SqlBoolean(this._value._boolean);
			}
		}

		// Token: 0x1700028A RID: 650
		// (get) Token: 0x06000E9D RID: 3741 RVA: 0x0004C5F3 File Offset: 0x0004A7F3
		internal SqlByte SqlByte
		{
			get
			{
				if (SqlBuffer.StorageType.Byte != this._type)
				{
					return (SqlByte)this.SqlValue;
				}
				if (this.IsNull)
				{
					return SqlByte.Null;
				}
				return new SqlByte(this._value._byte);
			}
		}

		// Token: 0x1700028B RID: 651
		// (get) Token: 0x06000E9E RID: 3742 RVA: 0x0004C628 File Offset: 0x0004A828
		// (set) Token: 0x06000E9F RID: 3743 RVA: 0x0004C659 File Offset: 0x0004A859
		internal SqlCachedBuffer SqlCachedBuffer
		{
			get
			{
				if (SqlBuffer.StorageType.SqlCachedBuffer != this._type)
				{
					return (SqlCachedBuffer)this.SqlValue;
				}
				if (this.IsNull)
				{
					return SqlCachedBuffer.Null;
				}
				return (SqlCachedBuffer)this._object;
			}
			set
			{
				this._object = value;
				this._type = SqlBuffer.StorageType.SqlCachedBuffer;
				this._isNull = value.IsNull;
			}
		}

		// Token: 0x1700028C RID: 652
		// (get) Token: 0x06000EA0 RID: 3744 RVA: 0x0004C676 File Offset: 0x0004A876
		// (set) Token: 0x06000EA1 RID: 3745 RVA: 0x0004C6A7 File Offset: 0x0004A8A7
		internal SqlXml SqlXml
		{
			get
			{
				if (SqlBuffer.StorageType.SqlXml != this._type)
				{
					return (SqlXml)this.SqlValue;
				}
				if (this.IsNull)
				{
					return SqlXml.Null;
				}
				return (SqlXml)this._object;
			}
			set
			{
				this._object = value;
				this._type = SqlBuffer.StorageType.SqlXml;
				this._isNull = value.IsNull;
			}
		}

		// Token: 0x1700028D RID: 653
		// (get) Token: 0x06000EA2 RID: 3746 RVA: 0x0004C6C4 File Offset: 0x0004A8C4
		internal SqlDateTime SqlDateTime
		{
			get
			{
				if (SqlBuffer.StorageType.DateTime != this._type)
				{
					return (SqlDateTime)this.SqlValue;
				}
				if (this.IsNull)
				{
					return SqlDateTime.Null;
				}
				return new SqlDateTime(this._value._dateTimeInfo.daypart, this._value._dateTimeInfo.timepart);
			}
		}

		// Token: 0x1700028E RID: 654
		// (get) Token: 0x06000EA3 RID: 3747 RVA: 0x0004C71C File Offset: 0x0004A91C
		internal SqlDecimal SqlDecimal
		{
			get
			{
				if (SqlBuffer.StorageType.Decimal != this._type)
				{
					return (SqlDecimal)this.SqlValue;
				}
				if (this.IsNull)
				{
					return SqlDecimal.Null;
				}
				return new SqlDecimal(this._value._numericInfo.precision, this._value._numericInfo.scale, this._value._numericInfo.positive, this._value._numericInfo.data1, this._value._numericInfo.data2, this._value._numericInfo.data3, this._value._numericInfo.data4);
			}
		}

		// Token: 0x1700028F RID: 655
		// (get) Token: 0x06000EA4 RID: 3748 RVA: 0x0004C7C4 File Offset: 0x0004A9C4
		internal SqlDouble SqlDouble
		{
			get
			{
				if (SqlBuffer.StorageType.Double != this._type)
				{
					return (SqlDouble)this.SqlValue;
				}
				if (this.IsNull)
				{
					return SqlDouble.Null;
				}
				return new SqlDouble(this._value._double);
			}
		}

		// Token: 0x17000290 RID: 656
		// (get) Token: 0x06000EA5 RID: 3749 RVA: 0x0004C7F9 File Offset: 0x0004A9F9
		// (set) Token: 0x06000EA6 RID: 3750 RVA: 0x0004C81C File Offset: 0x0004AA1C
		internal SqlGuid SqlGuid
		{
			get
			{
				if (SqlBuffer.StorageType.SqlGuid == this._type)
				{
					return (SqlGuid)this._object;
				}
				return (SqlGuid)this.SqlValue;
			}
			set
			{
				this._object = value;
				this._type = SqlBuffer.StorageType.SqlGuid;
				this._isNull = value.IsNull;
			}
		}

		// Token: 0x17000291 RID: 657
		// (get) Token: 0x06000EA7 RID: 3751 RVA: 0x0004C83F File Offset: 0x0004AA3F
		internal SqlInt16 SqlInt16
		{
			get
			{
				if (SqlBuffer.StorageType.Int16 != this._type)
				{
					return (SqlInt16)this.SqlValue;
				}
				if (this.IsNull)
				{
					return SqlInt16.Null;
				}
				return new SqlInt16(this._value._int16);
			}
		}

		// Token: 0x17000292 RID: 658
		// (get) Token: 0x06000EA8 RID: 3752 RVA: 0x0004C874 File Offset: 0x0004AA74
		internal SqlInt32 SqlInt32
		{
			get
			{
				if (SqlBuffer.StorageType.Int32 != this._type)
				{
					return (SqlInt32)this.SqlValue;
				}
				if (this.IsNull)
				{
					return SqlInt32.Null;
				}
				return new SqlInt32(this._value._int32);
			}
		}

		// Token: 0x17000293 RID: 659
		// (get) Token: 0x06000EA9 RID: 3753 RVA: 0x0004C8A9 File Offset: 0x0004AAA9
		internal SqlInt64 SqlInt64
		{
			get
			{
				if (SqlBuffer.StorageType.Int64 != this._type)
				{
					return (SqlInt64)this.SqlValue;
				}
				if (this.IsNull)
				{
					return SqlInt64.Null;
				}
				return new SqlInt64(this._value._int64);
			}
		}

		// Token: 0x17000294 RID: 660
		// (get) Token: 0x06000EAA RID: 3754 RVA: 0x0004C8DE File Offset: 0x0004AADE
		internal SqlMoney SqlMoney
		{
			get
			{
				if (SqlBuffer.StorageType.Money != this._type)
				{
					return (SqlMoney)this.SqlValue;
				}
				if (this.IsNull)
				{
					return SqlMoney.Null;
				}
				return SqlTypeWorkarounds.SqlMoneyCtor(this._value._int64, 1);
			}
		}

		// Token: 0x17000295 RID: 661
		// (get) Token: 0x06000EAB RID: 3755 RVA: 0x0004C915 File Offset: 0x0004AB15
		internal SqlSingle SqlSingle
		{
			get
			{
				if (SqlBuffer.StorageType.Single != this._type)
				{
					return (SqlSingle)this.SqlValue;
				}
				if (this.IsNull)
				{
					return SqlSingle.Null;
				}
				return new SqlSingle(this._value._single);
			}
		}

		// Token: 0x17000296 RID: 662
		// (get) Token: 0x06000EAC RID: 3756 RVA: 0x0004C94C File Offset: 0x0004AB4C
		internal SqlString SqlString
		{
			get
			{
				if (SqlBuffer.StorageType.String == this._type)
				{
					if (this.IsNull)
					{
						return SqlString.Null;
					}
					return new SqlString((string)this._object);
				}
				else
				{
					if (SqlBuffer.StorageType.SqlCachedBuffer != this._type)
					{
						return (SqlString)this.SqlValue;
					}
					SqlCachedBuffer sqlCachedBuffer = (SqlCachedBuffer)this._object;
					if (sqlCachedBuffer.IsNull)
					{
						return SqlString.Null;
					}
					return sqlCachedBuffer.ToSqlString();
				}
			}
		}

		// Token: 0x17000297 RID: 663
		// (get) Token: 0x06000EAD RID: 3757 RVA: 0x0004C9B8 File Offset: 0x0004ABB8
		internal object SqlValue
		{
			get
			{
				switch (this._type)
				{
				case SqlBuffer.StorageType.Empty:
					return DBNull.Value;
				case SqlBuffer.StorageType.Boolean:
					return this.SqlBoolean;
				case SqlBuffer.StorageType.Byte:
					return this.SqlByte;
				case SqlBuffer.StorageType.DateTime:
					return this.SqlDateTime;
				case SqlBuffer.StorageType.Decimal:
					return this.SqlDecimal;
				case SqlBuffer.StorageType.Double:
					return this.SqlDouble;
				case SqlBuffer.StorageType.Int16:
					return this.SqlInt16;
				case SqlBuffer.StorageType.Int32:
					return this.SqlInt32;
				case SqlBuffer.StorageType.Int64:
					return this.SqlInt64;
				case SqlBuffer.StorageType.Money:
					return this.SqlMoney;
				case SqlBuffer.StorageType.Single:
					return this.SqlSingle;
				case SqlBuffer.StorageType.String:
					return this.SqlString;
				case SqlBuffer.StorageType.SqlBinary:
				case SqlBuffer.StorageType.SqlGuid:
					return this._object;
				case SqlBuffer.StorageType.SqlCachedBuffer:
				{
					SqlCachedBuffer sqlCachedBuffer = (SqlCachedBuffer)this._object;
					if (sqlCachedBuffer.IsNull)
					{
						return SqlXml.Null;
					}
					return sqlCachedBuffer.ToSqlXml();
				}
				case SqlBuffer.StorageType.SqlXml:
					if (this._isNull)
					{
						return SqlXml.Null;
					}
					return (SqlXml)this._object;
				case SqlBuffer.StorageType.Date:
				case SqlBuffer.StorageType.DateTime2:
					if (this._isNull)
					{
						return DBNull.Value;
					}
					return this.DateTime;
				case SqlBuffer.StorageType.DateTimeOffset:
					if (this._isNull)
					{
						return DBNull.Value;
					}
					return this.DateTimeOffset;
				case SqlBuffer.StorageType.Time:
					if (this._isNull)
					{
						return DBNull.Value;
					}
					return this.Time;
				default:
					return null;
				}
			}
		}

		// Token: 0x17000298 RID: 664
		// (get) Token: 0x06000EAE RID: 3758 RVA: 0x0004CB44 File Offset: 0x0004AD44
		internal object Value
		{
			get
			{
				if (this.IsNull)
				{
					return DBNull.Value;
				}
				switch (this._type)
				{
				case SqlBuffer.StorageType.Empty:
					return DBNull.Value;
				case SqlBuffer.StorageType.Boolean:
					return this.Boolean;
				case SqlBuffer.StorageType.Byte:
					return this.Byte;
				case SqlBuffer.StorageType.DateTime:
					return this.DateTime;
				case SqlBuffer.StorageType.Decimal:
					return this.Decimal;
				case SqlBuffer.StorageType.Double:
					return this.Double;
				case SqlBuffer.StorageType.Int16:
					return this.Int16;
				case SqlBuffer.StorageType.Int32:
					return this.Int32;
				case SqlBuffer.StorageType.Int64:
					return this.Int64;
				case SqlBuffer.StorageType.Money:
					return this.Decimal;
				case SqlBuffer.StorageType.Single:
					return this.Single;
				case SqlBuffer.StorageType.String:
					return this.String;
				case SqlBuffer.StorageType.SqlBinary:
					return this.ByteArray;
				case SqlBuffer.StorageType.SqlCachedBuffer:
					return ((SqlCachedBuffer)this._object).ToString();
				case SqlBuffer.StorageType.SqlGuid:
					return this.Guid;
				case SqlBuffer.StorageType.SqlXml:
					return ((SqlXml)this._object).Value;
				case SqlBuffer.StorageType.Date:
					return this.DateTime;
				case SqlBuffer.StorageType.DateTime2:
					return this.DateTime;
				case SqlBuffer.StorageType.DateTimeOffset:
					return this.DateTimeOffset;
				case SqlBuffer.StorageType.Time:
					return this.Time;
				default:
					return null;
				}
			}
		}

		// Token: 0x06000EAF RID: 3759 RVA: 0x0004CCAC File Offset: 0x0004AEAC
		internal Type GetTypeFromStorageType(bool isSqlType)
		{
			if (isSqlType)
			{
				switch (this._type)
				{
				case SqlBuffer.StorageType.Empty:
					return null;
				case SqlBuffer.StorageType.Boolean:
					return typeof(SqlBoolean);
				case SqlBuffer.StorageType.Byte:
					return typeof(SqlByte);
				case SqlBuffer.StorageType.DateTime:
					return typeof(SqlDateTime);
				case SqlBuffer.StorageType.Decimal:
					return typeof(SqlDecimal);
				case SqlBuffer.StorageType.Double:
					return typeof(SqlDouble);
				case SqlBuffer.StorageType.Int16:
					return typeof(SqlInt16);
				case SqlBuffer.StorageType.Int32:
					return typeof(SqlInt32);
				case SqlBuffer.StorageType.Int64:
					return typeof(SqlInt64);
				case SqlBuffer.StorageType.Money:
					return typeof(SqlMoney);
				case SqlBuffer.StorageType.Single:
					return typeof(SqlSingle);
				case SqlBuffer.StorageType.String:
					return typeof(SqlString);
				case SqlBuffer.StorageType.SqlBinary:
					return typeof(object);
				case SqlBuffer.StorageType.SqlCachedBuffer:
					return typeof(SqlString);
				case SqlBuffer.StorageType.SqlGuid:
					return typeof(object);
				case SqlBuffer.StorageType.SqlXml:
					return typeof(SqlXml);
				}
			}
			else
			{
				switch (this._type)
				{
				case SqlBuffer.StorageType.Empty:
					return null;
				case SqlBuffer.StorageType.Boolean:
					return typeof(bool);
				case SqlBuffer.StorageType.Byte:
					return typeof(byte);
				case SqlBuffer.StorageType.DateTime:
					return typeof(DateTime);
				case SqlBuffer.StorageType.Decimal:
					return typeof(decimal);
				case SqlBuffer.StorageType.Double:
					return typeof(double);
				case SqlBuffer.StorageType.Int16:
					return typeof(short);
				case SqlBuffer.StorageType.Int32:
					return typeof(int);
				case SqlBuffer.StorageType.Int64:
					return typeof(long);
				case SqlBuffer.StorageType.Money:
					return typeof(decimal);
				case SqlBuffer.StorageType.Single:
					return typeof(float);
				case SqlBuffer.StorageType.String:
					return typeof(string);
				case SqlBuffer.StorageType.SqlBinary:
					return typeof(byte[]);
				case SqlBuffer.StorageType.SqlCachedBuffer:
					return typeof(string);
				case SqlBuffer.StorageType.SqlGuid:
					return typeof(Guid);
				case SqlBuffer.StorageType.SqlXml:
					return typeof(string);
				}
			}
			return null;
		}

		// Token: 0x06000EB0 RID: 3760 RVA: 0x0004CEB4 File Offset: 0x0004B0B4
		internal static SqlBuffer[] CreateBufferArray(int length)
		{
			SqlBuffer[] array = new SqlBuffer[length];
			for (int i = 0; i < array.Length; i++)
			{
				array[i] = new SqlBuffer();
			}
			return array;
		}

		// Token: 0x06000EB1 RID: 3761 RVA: 0x0004CEE0 File Offset: 0x0004B0E0
		internal static SqlBuffer[] CloneBufferArray(SqlBuffer[] values)
		{
			SqlBuffer[] array = new SqlBuffer[values.Length];
			for (int i = 0; i < values.Length; i++)
			{
				array[i] = new SqlBuffer(values[i]);
			}
			return array;
		}

		// Token: 0x06000EB2 RID: 3762 RVA: 0x0004CF10 File Offset: 0x0004B110
		internal static void Clear(SqlBuffer[] values)
		{
			if (values != null)
			{
				for (int i = 0; i < values.Length; i++)
				{
					values[i].Clear();
				}
			}
		}

		// Token: 0x06000EB3 RID: 3763 RVA: 0x0004CF36 File Offset: 0x0004B136
		internal void Clear()
		{
			this._isNull = false;
			this._type = SqlBuffer.StorageType.Empty;
			this._object = null;
		}

		// Token: 0x06000EB4 RID: 3764 RVA: 0x0004CF4D File Offset: 0x0004B14D
		internal void SetToDateTime(int daypart, int timepart)
		{
			this._value._dateTimeInfo.daypart = daypart;
			this._value._dateTimeInfo.timepart = timepart;
			this._type = SqlBuffer.StorageType.DateTime;
			this._isNull = false;
		}

		// Token: 0x06000EB5 RID: 3765 RVA: 0x0004CF80 File Offset: 0x0004B180
		internal void SetToDecimal(byte precision, byte scale, bool positive, int[] bits)
		{
			this._value._numericInfo.precision = precision;
			this._value._numericInfo.scale = scale;
			this._value._numericInfo.positive = positive;
			this._value._numericInfo.data1 = bits[0];
			this._value._numericInfo.data2 = bits[1];
			this._value._numericInfo.data3 = bits[2];
			this._value._numericInfo.data4 = bits[3];
			this._type = SqlBuffer.StorageType.Decimal;
			this._isNull = false;
		}

		// Token: 0x06000EB6 RID: 3766 RVA: 0x0004D01E File Offset: 0x0004B21E
		internal void SetToMoney(long value)
		{
			this._value._int64 = value;
			this._type = SqlBuffer.StorageType.Money;
			this._isNull = false;
		}

		// Token: 0x06000EB7 RID: 3767 RVA: 0x0004D03B File Offset: 0x0004B23B
		internal void SetToNullOfType(SqlBuffer.StorageType storageType)
		{
			this._type = storageType;
			this._isNull = true;
			this._object = null;
		}

		// Token: 0x06000EB8 RID: 3768 RVA: 0x0004D052 File Offset: 0x0004B252
		internal void SetToString(string value)
		{
			this._object = value;
			this._type = SqlBuffer.StorageType.String;
			this._isNull = false;
		}

		// Token: 0x06000EB9 RID: 3769 RVA: 0x0004D06A File Offset: 0x0004B26A
		internal void SetToDate(byte[] bytes)
		{
			this._type = SqlBuffer.StorageType.Date;
			this._value._int32 = SqlBuffer.GetDateFromByteArray(bytes, 0);
			this._isNull = false;
		}

		// Token: 0x06000EBA RID: 3770 RVA: 0x0004D090 File Offset: 0x0004B290
		internal void SetToDate(DateTime date)
		{
			this._type = SqlBuffer.StorageType.Date;
			this._value._int32 = date.Subtract(DateTime.MinValue).Days;
			this._isNull = false;
		}

		// Token: 0x06000EBB RID: 3771 RVA: 0x0004D0CB File Offset: 0x0004B2CB
		internal void SetToTime(byte[] bytes, int length, byte scale)
		{
			this._type = SqlBuffer.StorageType.Time;
			SqlBuffer.FillInTimeInfo(ref this._value._timeInfo, bytes, length, scale);
			this._isNull = false;
		}

		// Token: 0x06000EBC RID: 3772 RVA: 0x0004D0EF File Offset: 0x0004B2EF
		internal void SetToTime(TimeSpan timeSpan, byte scale)
		{
			this._type = SqlBuffer.StorageType.Time;
			this._value._timeInfo.ticks = timeSpan.Ticks;
			this._value._timeInfo.scale = scale;
			this._isNull = false;
		}

		// Token: 0x06000EBD RID: 3773 RVA: 0x0004D128 File Offset: 0x0004B328
		internal void SetToDateTime2(byte[] bytes, int length, byte scale)
		{
			this._type = SqlBuffer.StorageType.DateTime2;
			SqlBuffer.FillInTimeInfo(ref this._value._dateTime2Info.timeInfo, bytes, length - 3, scale);
			this._value._dateTime2Info.date = SqlBuffer.GetDateFromByteArray(bytes, length - 3);
			this._isNull = false;
		}

		// Token: 0x06000EBE RID: 3774 RVA: 0x0004D178 File Offset: 0x0004B378
		internal void SetToDateTime2(DateTime dateTime, byte scale)
		{
			this._type = SqlBuffer.StorageType.DateTime2;
			this._value._dateTime2Info.timeInfo.ticks = dateTime.TimeOfDay.Ticks;
			this._value._dateTime2Info.timeInfo.scale = scale;
			this._value._dateTime2Info.date = dateTime.Subtract(DateTime.MinValue).Days;
			this._isNull = false;
		}

		// Token: 0x06000EBF RID: 3775 RVA: 0x0004D1F4 File Offset: 0x0004B3F4
		internal void SetToDateTimeOffset(byte[] bytes, int length, byte scale)
		{
			this._type = SqlBuffer.StorageType.DateTimeOffset;
			SqlBuffer.FillInTimeInfo(ref this._value._dateTimeOffsetInfo.dateTime2Info.timeInfo, bytes, length - 5, scale);
			this._value._dateTimeOffsetInfo.dateTime2Info.date = SqlBuffer.GetDateFromByteArray(bytes, length - 5);
			this._value._dateTimeOffsetInfo.offset = (short)((int)bytes[length - 2] + ((int)bytes[length - 1] << 8));
			this._isNull = false;
		}

		// Token: 0x06000EC0 RID: 3776 RVA: 0x0004D26C File Offset: 0x0004B46C
		internal void SetToDateTimeOffset(DateTimeOffset dateTimeOffset, byte scale)
		{
			this._type = SqlBuffer.StorageType.DateTimeOffset;
			DateTime utcDateTime = dateTimeOffset.UtcDateTime;
			this._value._dateTimeOffsetInfo.dateTime2Info.timeInfo.ticks = utcDateTime.TimeOfDay.Ticks;
			this._value._dateTimeOffsetInfo.dateTime2Info.timeInfo.scale = scale;
			this._value._dateTimeOffsetInfo.dateTime2Info.date = utcDateTime.Subtract(DateTime.MinValue).Days;
			this._value._dateTimeOffsetInfo.offset = (short)dateTimeOffset.Offset.TotalMinutes;
			this._isNull = false;
		}

		// Token: 0x06000EC1 RID: 3777 RVA: 0x0004D320 File Offset: 0x0004B520
		private static void FillInTimeInfo(ref SqlBuffer.TimeInfo timeInfo, byte[] timeBytes, int length, byte scale)
		{
			long num = (long)((ulong)timeBytes[0] + ((ulong)timeBytes[1] << 8) + ((ulong)timeBytes[2] << 16));
			if (length > 3)
			{
				num += (long)((long)((ulong)timeBytes[3]) << 24);
			}
			if (length > 4)
			{
				num += (long)((long)((ulong)timeBytes[4]) << 32);
			}
			timeInfo.ticks = num * TdsEnums.TICKS_FROM_SCALE[(int)scale];
			timeInfo.scale = scale;
		}

		// Token: 0x06000EC2 RID: 3778 RVA: 0x0004D373 File Offset: 0x0004B573
		private static int GetDateFromByteArray(byte[] buf, int offset)
		{
			return (int)buf[offset] + ((int)buf[offset + 1] << 8) + ((int)buf[offset + 2] << 16);
		}

		// Token: 0x06000EC3 RID: 3779 RVA: 0x0004D389 File Offset: 0x0004B589
		private void ThrowIfNull()
		{
			if (this.IsNull)
			{
				throw new SqlNullValueException();
			}
		}

		// Token: 0x06000EC4 RID: 3780 RVA: 0x0004D39C File Offset: 0x0004B59C
		// Note: this type is marked as 'beforefieldinit'.
		static SqlBuffer()
		{
		}

		// Token: 0x04000A3D RID: 2621
		private bool _isNull;

		// Token: 0x04000A3E RID: 2622
		private SqlBuffer.StorageType _type;

		// Token: 0x04000A3F RID: 2623
		private SqlBuffer.Storage _value;

		// Token: 0x04000A40 RID: 2624
		private object _object;

		// Token: 0x04000A41 RID: 2625
		private static string[] s_katmaiDateTimeOffsetFormatByScale = new string[]
		{
			"yyyy-MM-dd HH:mm:ss zzz",
			"yyyy-MM-dd HH:mm:ss.f zzz",
			"yyyy-MM-dd HH:mm:ss.ff zzz",
			"yyyy-MM-dd HH:mm:ss.fff zzz",
			"yyyy-MM-dd HH:mm:ss.ffff zzz",
			"yyyy-MM-dd HH:mm:ss.fffff zzz",
			"yyyy-MM-dd HH:mm:ss.ffffff zzz",
			"yyyy-MM-dd HH:mm:ss.fffffff zzz"
		};

		// Token: 0x04000A42 RID: 2626
		private static string[] s_katmaiDateTime2FormatByScale = new string[]
		{
			"yyyy-MM-dd HH:mm:ss",
			"yyyy-MM-dd HH:mm:ss.f",
			"yyyy-MM-dd HH:mm:ss.ff",
			"yyyy-MM-dd HH:mm:ss.fff",
			"yyyy-MM-dd HH:mm:ss.ffff",
			"yyyy-MM-dd HH:mm:ss.fffff",
			"yyyy-MM-dd HH:mm:ss.ffffff",
			"yyyy-MM-dd HH:mm:ss.fffffff"
		};

		// Token: 0x04000A43 RID: 2627
		private static string[] s_katmaiTimeFormatByScale = new string[]
		{
			"HH:mm:ss",
			"HH:mm:ss.f",
			"HH:mm:ss.ff",
			"HH:mm:ss.fff",
			"HH:mm:ss.ffff",
			"HH:mm:ss.fffff",
			"HH:mm:ss.ffffff",
			"HH:mm:ss.fffffff"
		};

		// Token: 0x02000129 RID: 297
		internal enum StorageType
		{
			// Token: 0x04000A45 RID: 2629
			Empty,
			// Token: 0x04000A46 RID: 2630
			Boolean,
			// Token: 0x04000A47 RID: 2631
			Byte,
			// Token: 0x04000A48 RID: 2632
			DateTime,
			// Token: 0x04000A49 RID: 2633
			Decimal,
			// Token: 0x04000A4A RID: 2634
			Double,
			// Token: 0x04000A4B RID: 2635
			Int16,
			// Token: 0x04000A4C RID: 2636
			Int32,
			// Token: 0x04000A4D RID: 2637
			Int64,
			// Token: 0x04000A4E RID: 2638
			Money,
			// Token: 0x04000A4F RID: 2639
			Single,
			// Token: 0x04000A50 RID: 2640
			String,
			// Token: 0x04000A51 RID: 2641
			SqlBinary,
			// Token: 0x04000A52 RID: 2642
			SqlCachedBuffer,
			// Token: 0x04000A53 RID: 2643
			SqlGuid,
			// Token: 0x04000A54 RID: 2644
			SqlXml,
			// Token: 0x04000A55 RID: 2645
			Date,
			// Token: 0x04000A56 RID: 2646
			DateTime2,
			// Token: 0x04000A57 RID: 2647
			DateTimeOffset,
			// Token: 0x04000A58 RID: 2648
			Time
		}

		// Token: 0x0200012A RID: 298
		internal struct DateTimeInfo
		{
			// Token: 0x04000A59 RID: 2649
			internal int daypart;

			// Token: 0x04000A5A RID: 2650
			internal int timepart;
		}

		// Token: 0x0200012B RID: 299
		internal struct NumericInfo
		{
			// Token: 0x04000A5B RID: 2651
			internal int data1;

			// Token: 0x04000A5C RID: 2652
			internal int data2;

			// Token: 0x04000A5D RID: 2653
			internal int data3;

			// Token: 0x04000A5E RID: 2654
			internal int data4;

			// Token: 0x04000A5F RID: 2655
			internal byte precision;

			// Token: 0x04000A60 RID: 2656
			internal byte scale;

			// Token: 0x04000A61 RID: 2657
			internal bool positive;
		}

		// Token: 0x0200012C RID: 300
		internal struct TimeInfo
		{
			// Token: 0x04000A62 RID: 2658
			internal long ticks;

			// Token: 0x04000A63 RID: 2659
			internal byte scale;
		}

		// Token: 0x0200012D RID: 301
		internal struct DateTime2Info
		{
			// Token: 0x04000A64 RID: 2660
			internal int date;

			// Token: 0x04000A65 RID: 2661
			internal SqlBuffer.TimeInfo timeInfo;
		}

		// Token: 0x0200012E RID: 302
		internal struct DateTimeOffsetInfo
		{
			// Token: 0x04000A66 RID: 2662
			internal SqlBuffer.DateTime2Info dateTime2Info;

			// Token: 0x04000A67 RID: 2663
			internal short offset;
		}

		// Token: 0x0200012F RID: 303
		[StructLayout(LayoutKind.Explicit)]
		internal struct Storage
		{
			// Token: 0x04000A68 RID: 2664
			[FieldOffset(0)]
			internal bool _boolean;

			// Token: 0x04000A69 RID: 2665
			[FieldOffset(0)]
			internal byte _byte;

			// Token: 0x04000A6A RID: 2666
			[FieldOffset(0)]
			internal SqlBuffer.DateTimeInfo _dateTimeInfo;

			// Token: 0x04000A6B RID: 2667
			[FieldOffset(0)]
			internal double _double;

			// Token: 0x04000A6C RID: 2668
			[FieldOffset(0)]
			internal SqlBuffer.NumericInfo _numericInfo;

			// Token: 0x04000A6D RID: 2669
			[FieldOffset(0)]
			internal short _int16;

			// Token: 0x04000A6E RID: 2670
			[FieldOffset(0)]
			internal int _int32;

			// Token: 0x04000A6F RID: 2671
			[FieldOffset(0)]
			internal long _int64;

			// Token: 0x04000A70 RID: 2672
			[FieldOffset(0)]
			internal float _single;

			// Token: 0x04000A71 RID: 2673
			[FieldOffset(0)]
			internal SqlBuffer.TimeInfo _timeInfo;

			// Token: 0x04000A72 RID: 2674
			[FieldOffset(0)]
			internal SqlBuffer.DateTime2Info _dateTime2Info;

			// Token: 0x04000A73 RID: 2675
			[FieldOffset(0)]
			internal SqlBuffer.DateTimeOffsetInfo _dateTimeOffsetInfo;
		}
	}
}
