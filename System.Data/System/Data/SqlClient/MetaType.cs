﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlTypes;
using System.IO;
using System.Xml;
using Microsoft.SqlServer.Server;

namespace System.Data.SqlClient
{
	// Token: 0x0200019E RID: 414
	internal sealed class MetaType
	{
		// Token: 0x060012FD RID: 4861 RVA: 0x00062718 File Offset: 0x00060918
		public MetaType(byte precision, byte scale, int fixedLength, bool isFixed, bool isLong, bool isPlp, byte tdsType, byte nullableTdsType, string typeName, Type classType, Type sqlType, SqlDbType sqldbType, DbType dbType, byte propBytes)
		{
			this.Precision = precision;
			this.Scale = scale;
			this.FixedLength = fixedLength;
			this.IsFixed = isFixed;
			this.IsLong = isLong;
			this.IsPlp = isPlp;
			this.TDSType = tdsType;
			this.NullableType = nullableTdsType;
			this.TypeName = typeName;
			this.SqlDbType = sqldbType;
			this.DbType = dbType;
			this.ClassType = classType;
			this.SqlType = sqlType;
			this.PropBytes = propBytes;
			this.IsAnsiType = MetaType._IsAnsiType(sqldbType);
			this.IsBinType = MetaType._IsBinType(sqldbType);
			this.IsCharType = MetaType._IsCharType(sqldbType);
			this.IsNCharType = MetaType._IsNCharType(sqldbType);
			this.IsSizeInCharacters = MetaType._IsSizeInCharacters(sqldbType);
			this.IsNewKatmaiType = MetaType._IsNewKatmaiType(sqldbType);
			this.IsVarTime = MetaType._IsVarTime(sqldbType);
			this.Is70Supported = MetaType._Is70Supported(this.SqlDbType);
			this.Is80Supported = MetaType._Is80Supported(this.SqlDbType);
			this.Is90Supported = MetaType._Is90Supported(this.SqlDbType);
			this.Is100Supported = MetaType._Is100Supported(this.SqlDbType);
		}

		// Token: 0x1700037C RID: 892
		// (get) Token: 0x060012FE RID: 4862 RVA: 0x000061C5 File Offset: 0x000043C5
		public int TypeId
		{
			get
			{
				return 0;
			}
		}

		// Token: 0x060012FF RID: 4863 RVA: 0x00062837 File Offset: 0x00060A37
		private static bool _IsAnsiType(SqlDbType type)
		{
			return type == SqlDbType.Char || type == SqlDbType.VarChar || type == SqlDbType.Text;
		}

		// Token: 0x06001300 RID: 4864 RVA: 0x00062849 File Offset: 0x00060A49
		private static bool _IsSizeInCharacters(SqlDbType type)
		{
			return type == SqlDbType.NChar || type == SqlDbType.NVarChar || type == SqlDbType.Xml || type == SqlDbType.NText;
		}

		// Token: 0x06001301 RID: 4865 RVA: 0x00062861 File Offset: 0x00060A61
		private static bool _IsCharType(SqlDbType type)
		{
			return type == SqlDbType.NChar || type == SqlDbType.NVarChar || type == SqlDbType.NText || type == SqlDbType.Char || type == SqlDbType.VarChar || type == SqlDbType.Text || type == SqlDbType.Xml;
		}

		// Token: 0x06001302 RID: 4866 RVA: 0x00062887 File Offset: 0x00060A87
		private static bool _IsNCharType(SqlDbType type)
		{
			return type == SqlDbType.NChar || type == SqlDbType.NVarChar || type == SqlDbType.NText || type == SqlDbType.Xml;
		}

		// Token: 0x06001303 RID: 4867 RVA: 0x0006289F File Offset: 0x00060A9F
		private static bool _IsBinType(SqlDbType type)
		{
			return type == SqlDbType.Image || type == SqlDbType.Binary || type == SqlDbType.VarBinary || type == SqlDbType.Timestamp || type == SqlDbType.Udt || type == (SqlDbType)24;
		}

		// Token: 0x06001304 RID: 4868 RVA: 0x000628BF File Offset: 0x00060ABF
		private static bool _Is70Supported(SqlDbType type)
		{
			return type != SqlDbType.BigInt && type > SqlDbType.BigInt && type <= SqlDbType.VarChar;
		}

		// Token: 0x06001305 RID: 4869 RVA: 0x000628D2 File Offset: 0x00060AD2
		private static bool _Is80Supported(SqlDbType type)
		{
			return type >= SqlDbType.BigInt && type <= SqlDbType.Variant;
		}

		// Token: 0x06001306 RID: 4870 RVA: 0x000628E2 File Offset: 0x00060AE2
		private static bool _Is90Supported(SqlDbType type)
		{
			return MetaType._Is80Supported(type) || SqlDbType.Xml == type || SqlDbType.Udt == type;
		}

		// Token: 0x06001307 RID: 4871 RVA: 0x000628F8 File Offset: 0x00060AF8
		private static bool _Is100Supported(SqlDbType type)
		{
			return MetaType._Is90Supported(type) || SqlDbType.Date == type || SqlDbType.Time == type || SqlDbType.DateTime2 == type || SqlDbType.DateTimeOffset == type;
		}

		// Token: 0x06001308 RID: 4872 RVA: 0x00062918 File Offset: 0x00060B18
		private static bool _IsNewKatmaiType(SqlDbType type)
		{
			return SqlDbType.Structured == type;
		}

		// Token: 0x06001309 RID: 4873 RVA: 0x0006291F File Offset: 0x00060B1F
		internal static bool _IsVarTime(SqlDbType type)
		{
			return type == SqlDbType.Time || type == SqlDbType.DateTime2 || type == SqlDbType.DateTimeOffset;
		}

		// Token: 0x0600130A RID: 4874 RVA: 0x00062934 File Offset: 0x00060B34
		internal static MetaType GetMetaTypeFromSqlDbType(SqlDbType target, bool isMultiValued)
		{
			switch (target)
			{
			case SqlDbType.BigInt:
				return MetaType.s_metaBigInt;
			case SqlDbType.Binary:
				return MetaType.s_metaBinary;
			case SqlDbType.Bit:
				return MetaType.s_metaBit;
			case SqlDbType.Char:
				return MetaType.s_metaChar;
			case SqlDbType.DateTime:
				return MetaType.s_metaDateTime;
			case SqlDbType.Decimal:
				return MetaType.MetaDecimal;
			case SqlDbType.Float:
				return MetaType.s_metaFloat;
			case SqlDbType.Image:
				return MetaType.MetaImage;
			case SqlDbType.Int:
				return MetaType.s_metaInt;
			case SqlDbType.Money:
				return MetaType.s_metaMoney;
			case SqlDbType.NChar:
				return MetaType.s_metaNChar;
			case SqlDbType.NText:
				return MetaType.MetaNText;
			case SqlDbType.NVarChar:
				return MetaType.MetaNVarChar;
			case SqlDbType.Real:
				return MetaType.s_metaReal;
			case SqlDbType.UniqueIdentifier:
				return MetaType.s_metaUniqueId;
			case SqlDbType.SmallDateTime:
				return MetaType.s_metaSmallDateTime;
			case SqlDbType.SmallInt:
				return MetaType.s_metaSmallInt;
			case SqlDbType.SmallMoney:
				return MetaType.s_metaSmallMoney;
			case SqlDbType.Text:
				return MetaType.MetaText;
			case SqlDbType.Timestamp:
				return MetaType.s_metaTimestamp;
			case SqlDbType.TinyInt:
				return MetaType.s_metaTinyInt;
			case SqlDbType.VarBinary:
				return MetaType.MetaVarBinary;
			case SqlDbType.VarChar:
				return MetaType.s_metaVarChar;
			case SqlDbType.Variant:
				return MetaType.s_metaVariant;
			case (SqlDbType)24:
				return MetaType.s_metaSmallVarBinary;
			case SqlDbType.Xml:
				return MetaType.MetaXml;
			case SqlDbType.Udt:
				return MetaType.MetaUdt;
			case SqlDbType.Structured:
				if (isMultiValued)
				{
					return MetaType.s_metaTable;
				}
				return MetaType.s_metaSUDT;
			case SqlDbType.Date:
				return MetaType.s_metaDate;
			case SqlDbType.Time:
				return MetaType.MetaTime;
			case SqlDbType.DateTime2:
				return MetaType.s_metaDateTime2;
			case SqlDbType.DateTimeOffset:
				return MetaType.MetaDateTimeOffset;
			}
			throw SQL.InvalidSqlDbType(target);
		}

		// Token: 0x0600130B RID: 4875 RVA: 0x00062AA8 File Offset: 0x00060CA8
		internal static MetaType GetMetaTypeFromDbType(DbType target)
		{
			switch (target)
			{
			case DbType.AnsiString:
				return MetaType.s_metaVarChar;
			case DbType.Binary:
				return MetaType.MetaVarBinary;
			case DbType.Byte:
				return MetaType.s_metaTinyInt;
			case DbType.Boolean:
				return MetaType.s_metaBit;
			case DbType.Currency:
				return MetaType.s_metaMoney;
			case DbType.Date:
			case DbType.DateTime:
				return MetaType.s_metaDateTime;
			case DbType.Decimal:
				return MetaType.MetaDecimal;
			case DbType.Double:
				return MetaType.s_metaFloat;
			case DbType.Guid:
				return MetaType.s_metaUniqueId;
			case DbType.Int16:
				return MetaType.s_metaSmallInt;
			case DbType.Int32:
				return MetaType.s_metaInt;
			case DbType.Int64:
				return MetaType.s_metaBigInt;
			case DbType.Object:
				return MetaType.s_metaVariant;
			case DbType.Single:
				return MetaType.s_metaReal;
			case DbType.String:
				return MetaType.MetaNVarChar;
			case DbType.Time:
				return MetaType.s_metaDateTime;
			case DbType.AnsiStringFixedLength:
				return MetaType.s_metaChar;
			case DbType.StringFixedLength:
				return MetaType.s_metaNChar;
			case DbType.Xml:
				return MetaType.MetaXml;
			case DbType.DateTime2:
				return MetaType.s_metaDateTime2;
			case DbType.DateTimeOffset:
				return MetaType.MetaDateTimeOffset;
			}
			throw ADP.DbTypeNotSupported(target, typeof(SqlDbType));
		}

		// Token: 0x0600130C RID: 4876 RVA: 0x00062BBC File Offset: 0x00060DBC
		internal static MetaType GetMaxMetaTypeFromMetaType(MetaType mt)
		{
			SqlDbType sqlDbType = mt.SqlDbType;
			if (sqlDbType <= SqlDbType.NChar)
			{
				if (sqlDbType != SqlDbType.Binary)
				{
					if (sqlDbType == SqlDbType.Char)
					{
						goto IL_3E;
					}
					if (sqlDbType != SqlDbType.NChar)
					{
						return mt;
					}
					goto IL_44;
				}
			}
			else if (sqlDbType <= SqlDbType.VarBinary)
			{
				if (sqlDbType == SqlDbType.NVarChar)
				{
					goto IL_44;
				}
				if (sqlDbType != SqlDbType.VarBinary)
				{
					return mt;
				}
			}
			else
			{
				if (sqlDbType == SqlDbType.VarChar)
				{
					goto IL_3E;
				}
				if (sqlDbType != SqlDbType.Udt)
				{
					return mt;
				}
				return MetaType.s_metaMaxUdt;
			}
			return MetaType.MetaMaxVarBinary;
			IL_3E:
			return MetaType.MetaMaxVarChar;
			IL_44:
			return MetaType.MetaMaxNVarChar;
		}

		// Token: 0x0600130D RID: 4877 RVA: 0x00062C1C File Offset: 0x00060E1C
		internal static MetaType GetMetaTypeFromType(Type dataType, bool streamAllowed = true)
		{
			if (dataType == typeof(byte[]))
			{
				return MetaType.MetaVarBinary;
			}
			if (dataType == typeof(Guid))
			{
				return MetaType.s_metaUniqueId;
			}
			if (dataType == typeof(object))
			{
				return MetaType.s_metaVariant;
			}
			if (dataType == typeof(SqlBinary))
			{
				return MetaType.MetaVarBinary;
			}
			if (dataType == typeof(SqlBoolean))
			{
				return MetaType.s_metaBit;
			}
			if (dataType == typeof(SqlByte))
			{
				return MetaType.s_metaTinyInt;
			}
			if (dataType == typeof(SqlBytes))
			{
				return MetaType.MetaVarBinary;
			}
			if (dataType == typeof(SqlChars))
			{
				return MetaType.MetaNVarChar;
			}
			if (dataType == typeof(SqlDateTime))
			{
				return MetaType.s_metaDateTime;
			}
			if (dataType == typeof(SqlDouble))
			{
				return MetaType.s_metaFloat;
			}
			if (dataType == typeof(SqlGuid))
			{
				return MetaType.s_metaUniqueId;
			}
			if (dataType == typeof(SqlInt16))
			{
				return MetaType.s_metaSmallInt;
			}
			if (dataType == typeof(SqlInt32))
			{
				return MetaType.s_metaInt;
			}
			if (dataType == typeof(SqlInt64))
			{
				return MetaType.s_metaBigInt;
			}
			if (dataType == typeof(SqlMoney))
			{
				return MetaType.s_metaMoney;
			}
			if (dataType == typeof(SqlDecimal))
			{
				return MetaType.MetaDecimal;
			}
			if (dataType == typeof(SqlSingle))
			{
				return MetaType.s_metaReal;
			}
			if (dataType == typeof(SqlXml))
			{
				return MetaType.MetaXml;
			}
			if (dataType == typeof(SqlString))
			{
				return MetaType.MetaNVarChar;
			}
			if (dataType == typeof(IEnumerable<DbDataRecord>))
			{
				return MetaType.s_metaTable;
			}
			if (dataType == typeof(TimeSpan))
			{
				return MetaType.MetaTime;
			}
			if (dataType == typeof(DateTimeOffset))
			{
				return MetaType.MetaDateTimeOffset;
			}
			if (dataType == typeof(DBNull))
			{
				throw ADP.InvalidDataType("DBNull");
			}
			if (dataType == typeof(bool))
			{
				return MetaType.s_metaBit;
			}
			if (dataType == typeof(char))
			{
				throw ADP.InvalidDataType("Char");
			}
			if (dataType == typeof(sbyte))
			{
				throw ADP.InvalidDataType("SByte");
			}
			if (dataType == typeof(byte))
			{
				return MetaType.s_metaTinyInt;
			}
			if (dataType == typeof(short))
			{
				return MetaType.s_metaSmallInt;
			}
			if (dataType == typeof(ushort))
			{
				throw ADP.InvalidDataType("UInt16");
			}
			if (dataType == typeof(int))
			{
				return MetaType.s_metaInt;
			}
			if (dataType == typeof(uint))
			{
				throw ADP.InvalidDataType("UInt32");
			}
			if (dataType == typeof(long))
			{
				return MetaType.s_metaBigInt;
			}
			if (dataType == typeof(ulong))
			{
				throw ADP.InvalidDataType("UInt64");
			}
			if (dataType == typeof(float))
			{
				return MetaType.s_metaReal;
			}
			if (dataType == typeof(double))
			{
				return MetaType.s_metaFloat;
			}
			if (dataType == typeof(decimal))
			{
				return MetaType.MetaDecimal;
			}
			if (dataType == typeof(DateTime))
			{
				return MetaType.s_metaDateTime;
			}
			if (dataType == typeof(string))
			{
				return MetaType.MetaNVarChar;
			}
			throw ADP.UnknownDataType(dataType);
		}

		// Token: 0x0600130E RID: 4878 RVA: 0x00062FE0 File Offset: 0x000611E0
		internal static MetaType GetMetaTypeFromValue(object value, bool inferLen = true, bool streamAllowed = true)
		{
			if (value == null)
			{
				throw ADP.InvalidDataType("null");
			}
			if (value is DBNull)
			{
				throw ADP.InvalidDataType("DBNull");
			}
			Type type = value.GetType();
			switch (Convert.GetTypeCode(value))
			{
			case TypeCode.Empty:
				throw ADP.InvalidDataType("Empty");
			case TypeCode.Object:
				if (type == typeof(byte[]))
				{
					if (!inferLen || ((byte[])value).Length <= 8000)
					{
						return MetaType.MetaVarBinary;
					}
					return MetaType.MetaImage;
				}
				else
				{
					if (type == typeof(Guid))
					{
						return MetaType.s_metaUniqueId;
					}
					if (type == typeof(object))
					{
						return MetaType.s_metaVariant;
					}
					if (type == typeof(SqlBinary))
					{
						return MetaType.MetaVarBinary;
					}
					if (type == typeof(SqlBoolean))
					{
						return MetaType.s_metaBit;
					}
					if (type == typeof(SqlByte))
					{
						return MetaType.s_metaTinyInt;
					}
					if (type == typeof(SqlBytes))
					{
						return MetaType.MetaVarBinary;
					}
					if (type == typeof(SqlChars))
					{
						return MetaType.MetaNVarChar;
					}
					if (type == typeof(SqlDateTime))
					{
						return MetaType.s_metaDateTime;
					}
					if (type == typeof(SqlDouble))
					{
						return MetaType.s_metaFloat;
					}
					if (type == typeof(SqlGuid))
					{
						return MetaType.s_metaUniqueId;
					}
					if (type == typeof(SqlInt16))
					{
						return MetaType.s_metaSmallInt;
					}
					if (type == typeof(SqlInt32))
					{
						return MetaType.s_metaInt;
					}
					if (type == typeof(SqlInt64))
					{
						return MetaType.s_metaBigInt;
					}
					if (type == typeof(SqlMoney))
					{
						return MetaType.s_metaMoney;
					}
					if (type == typeof(SqlDecimal))
					{
						return MetaType.MetaDecimal;
					}
					if (type == typeof(SqlSingle))
					{
						return MetaType.s_metaReal;
					}
					if (type == typeof(SqlXml))
					{
						return MetaType.MetaXml;
					}
					if (type == typeof(SqlString))
					{
						if (!inferLen || ((SqlString)value).IsNull)
						{
							return MetaType.MetaNVarChar;
						}
						return MetaType.PromoteStringType(((SqlString)value).Value);
					}
					else
					{
						if (type == typeof(IEnumerable<DbDataRecord>) || type == typeof(DataTable))
						{
							return MetaType.s_metaTable;
						}
						if (type == typeof(TimeSpan))
						{
							return MetaType.MetaTime;
						}
						if (type == typeof(DateTimeOffset))
						{
							return MetaType.MetaDateTimeOffset;
						}
						if (streamAllowed)
						{
							if (value is Stream)
							{
								return MetaType.MetaVarBinary;
							}
							if (value is TextReader)
							{
								return MetaType.MetaNVarChar;
							}
							if (value is XmlReader)
							{
								return MetaType.MetaXml;
							}
						}
						throw ADP.UnknownDataType(type);
					}
				}
				break;
			case TypeCode.Boolean:
				return MetaType.s_metaBit;
			case TypeCode.Char:
				throw ADP.InvalidDataType("Char");
			case TypeCode.SByte:
				throw ADP.InvalidDataType("SByte");
			case TypeCode.Byte:
				return MetaType.s_metaTinyInt;
			case TypeCode.Int16:
				return MetaType.s_metaSmallInt;
			case TypeCode.UInt16:
				throw ADP.InvalidDataType("UInt16");
			case TypeCode.Int32:
				return MetaType.s_metaInt;
			case TypeCode.UInt32:
				throw ADP.InvalidDataType("UInt32");
			case TypeCode.Int64:
				return MetaType.s_metaBigInt;
			case TypeCode.UInt64:
				throw ADP.InvalidDataType("UInt64");
			case TypeCode.Single:
				return MetaType.s_metaReal;
			case TypeCode.Double:
				return MetaType.s_metaFloat;
			case TypeCode.Decimal:
				return MetaType.MetaDecimal;
			case TypeCode.DateTime:
				return MetaType.s_metaDateTime;
			case TypeCode.String:
				if (!inferLen)
				{
					return MetaType.MetaNVarChar;
				}
				return MetaType.PromoteStringType((string)value);
			}
			throw ADP.UnknownDataType(type);
		}

		// Token: 0x0600130F RID: 4879 RVA: 0x0006339C File Offset: 0x0006159C
		internal static object GetNullSqlValue(Type sqlType)
		{
			if (sqlType == typeof(SqlSingle))
			{
				return SqlSingle.Null;
			}
			if (sqlType == typeof(SqlString))
			{
				return SqlString.Null;
			}
			if (sqlType == typeof(SqlDouble))
			{
				return SqlDouble.Null;
			}
			if (sqlType == typeof(SqlBinary))
			{
				return SqlBinary.Null;
			}
			if (sqlType == typeof(SqlGuid))
			{
				return SqlGuid.Null;
			}
			if (sqlType == typeof(SqlBoolean))
			{
				return SqlBoolean.Null;
			}
			if (sqlType == typeof(SqlByte))
			{
				return SqlByte.Null;
			}
			if (sqlType == typeof(SqlInt16))
			{
				return SqlInt16.Null;
			}
			if (sqlType == typeof(SqlInt32))
			{
				return SqlInt32.Null;
			}
			if (sqlType == typeof(SqlInt64))
			{
				return SqlInt64.Null;
			}
			if (sqlType == typeof(SqlDecimal))
			{
				return SqlDecimal.Null;
			}
			if (sqlType == typeof(SqlDateTime))
			{
				return SqlDateTime.Null;
			}
			if (sqlType == typeof(SqlMoney))
			{
				return SqlMoney.Null;
			}
			if (sqlType == typeof(SqlXml))
			{
				return SqlXml.Null;
			}
			if (sqlType == typeof(object))
			{
				return DBNull.Value;
			}
			if (sqlType == typeof(IEnumerable<DbDataRecord>))
			{
				return DBNull.Value;
			}
			if (sqlType == typeof(DataTable))
			{
				return DBNull.Value;
			}
			if (sqlType == typeof(DateTime))
			{
				return DBNull.Value;
			}
			if (sqlType == typeof(TimeSpan))
			{
				return DBNull.Value;
			}
			sqlType == typeof(DateTimeOffset);
			return DBNull.Value;
		}

		// Token: 0x06001310 RID: 4880 RVA: 0x000635C8 File Offset: 0x000617C8
		internal static MetaType PromoteStringType(string s)
		{
			if (s.Length << 1 > 8000)
			{
				return MetaType.s_metaVarChar;
			}
			return MetaType.MetaNVarChar;
		}

		// Token: 0x06001311 RID: 4881 RVA: 0x000635E4 File Offset: 0x000617E4
		internal static object GetComValueFromSqlVariant(object sqlVal)
		{
			object result = null;
			if (ADP.IsNull(sqlVal))
			{
				return result;
			}
			if (sqlVal is SqlSingle)
			{
				result = ((SqlSingle)sqlVal).Value;
			}
			else if (sqlVal is SqlString)
			{
				result = ((SqlString)sqlVal).Value;
			}
			else if (sqlVal is SqlDouble)
			{
				result = ((SqlDouble)sqlVal).Value;
			}
			else if (sqlVal is SqlBinary)
			{
				result = ((SqlBinary)sqlVal).Value;
			}
			else if (sqlVal is SqlGuid)
			{
				result = ((SqlGuid)sqlVal).Value;
			}
			else if (sqlVal is SqlBoolean)
			{
				result = ((SqlBoolean)sqlVal).Value;
			}
			else if (sqlVal is SqlByte)
			{
				result = ((SqlByte)sqlVal).Value;
			}
			else if (sqlVal is SqlInt16)
			{
				result = ((SqlInt16)sqlVal).Value;
			}
			else if (sqlVal is SqlInt32)
			{
				result = ((SqlInt32)sqlVal).Value;
			}
			else if (sqlVal is SqlInt64)
			{
				result = ((SqlInt64)sqlVal).Value;
			}
			else if (sqlVal is SqlDecimal)
			{
				result = ((SqlDecimal)sqlVal).Value;
			}
			else if (sqlVal is SqlDateTime)
			{
				result = ((SqlDateTime)sqlVal).Value;
			}
			else if (sqlVal is SqlMoney)
			{
				result = ((SqlMoney)sqlVal).Value;
			}
			else if (sqlVal is SqlXml)
			{
				result = ((SqlXml)sqlVal).Value;
			}
			return result;
		}

		// Token: 0x06001312 RID: 4882 RVA: 0x000637B4 File Offset: 0x000619B4
		internal static object GetSqlValueFromComVariant(object comVal)
		{
			object result = null;
			if (comVal != null && DBNull.Value != comVal)
			{
				if (comVal is float)
				{
					result = new SqlSingle((float)comVal);
				}
				else if (comVal is string)
				{
					result = new SqlString((string)comVal);
				}
				else if (comVal is double)
				{
					result = new SqlDouble((double)comVal);
				}
				else if (comVal is byte[])
				{
					result = new SqlBinary((byte[])comVal);
				}
				else if (comVal is char)
				{
					result = new SqlString(((char)comVal).ToString());
				}
				else if (comVal is char[])
				{
					result = new SqlChars((char[])comVal);
				}
				else if (comVal is Guid)
				{
					result = new SqlGuid((Guid)comVal);
				}
				else if (comVal is bool)
				{
					result = new SqlBoolean((bool)comVal);
				}
				else if (comVal is byte)
				{
					result = new SqlByte((byte)comVal);
				}
				else if (comVal is short)
				{
					result = new SqlInt16((short)comVal);
				}
				else if (comVal is int)
				{
					result = new SqlInt32((int)comVal);
				}
				else if (comVal is long)
				{
					result = new SqlInt64((long)comVal);
				}
				else if (comVal is decimal)
				{
					result = new SqlDecimal((decimal)comVal);
				}
				else if (comVal is DateTime)
				{
					result = new SqlDateTime((DateTime)comVal);
				}
				else if (comVal is XmlReader)
				{
					result = new SqlXml((XmlReader)comVal);
				}
				else if (comVal is TimeSpan || comVal is DateTimeOffset)
				{
					result = comVal;
				}
			}
			return result;
		}

		// Token: 0x06001313 RID: 4883 RVA: 0x00063998 File Offset: 0x00061B98
		internal static SqlDbType GetSqlDbTypeFromOleDbType(short dbType, string typeName)
		{
			return SqlDbType.Variant;
		}

		// Token: 0x06001314 RID: 4884 RVA: 0x0006399C File Offset: 0x00061B9C
		internal static MetaType GetSqlDataType(int tdsType, uint userType, int length)
		{
			if (tdsType <= 165)
			{
				if (tdsType <= 111)
				{
					switch (tdsType)
					{
					case 31:
					case 32:
					case 33:
					case 44:
					case 46:
					case 49:
					case 51:
					case 53:
					case 54:
					case 55:
					case 57:
						goto IL_279;
					case 34:
						return MetaType.MetaImage;
					case 35:
						return MetaType.MetaText;
					case 36:
						return MetaType.s_metaUniqueId;
					case 37:
						return MetaType.s_metaSmallVarBinary;
					case 38:
						if (4 > length)
						{
							if (2 != length)
							{
								return MetaType.s_metaTinyInt;
							}
							return MetaType.s_metaSmallInt;
						}
						else
						{
							if (4 != length)
							{
								return MetaType.s_metaBigInt;
							}
							return MetaType.s_metaInt;
						}
						break;
					case 39:
						goto IL_1C6;
					case 40:
						return MetaType.s_metaDate;
					case 41:
						return MetaType.MetaTime;
					case 42:
						return MetaType.s_metaDateTime2;
					case 43:
						return MetaType.MetaDateTimeOffset;
					case 45:
						goto IL_1CC;
					case 47:
						goto IL_1E3;
					case 48:
						return MetaType.s_metaTinyInt;
					case 50:
						break;
					case 52:
						return MetaType.s_metaSmallInt;
					case 56:
						return MetaType.s_metaInt;
					case 58:
						return MetaType.s_metaSmallDateTime;
					case 59:
						return MetaType.s_metaReal;
					case 60:
						return MetaType.s_metaMoney;
					case 61:
						return MetaType.s_metaDateTime;
					case 62:
						return MetaType.s_metaFloat;
					default:
						switch (tdsType)
						{
						case 98:
							return MetaType.s_metaVariant;
						case 99:
							return MetaType.MetaNText;
						case 100:
						case 101:
						case 102:
						case 103:
						case 105:
						case 107:
							goto IL_279;
						case 104:
							break;
						case 106:
						case 108:
							return MetaType.MetaDecimal;
						case 109:
							if (4 != length)
							{
								return MetaType.s_metaFloat;
							}
							return MetaType.s_metaReal;
						case 110:
							if (4 != length)
							{
								return MetaType.s_metaMoney;
							}
							return MetaType.s_metaSmallMoney;
						case 111:
							if (4 != length)
							{
								return MetaType.s_metaDateTime;
							}
							return MetaType.s_metaSmallDateTime;
						default:
							goto IL_279;
						}
						break;
					}
					return MetaType.s_metaBit;
				}
				if (tdsType == 122)
				{
					return MetaType.s_metaSmallMoney;
				}
				if (tdsType == 127)
				{
					return MetaType.s_metaBigInt;
				}
				if (tdsType != 165)
				{
					goto IL_279;
				}
				return MetaType.MetaVarBinary;
			}
			else if (tdsType <= 173)
			{
				if (tdsType != 167)
				{
					if (tdsType != 173)
					{
						goto IL_279;
					}
					goto IL_1CC;
				}
			}
			else
			{
				if (tdsType == 175)
				{
					goto IL_1E3;
				}
				if (tdsType == 231)
				{
					return MetaType.MetaNVarChar;
				}
				switch (tdsType)
				{
				case 239:
					return MetaType.s_metaNChar;
				case 240:
					return MetaType.MetaUdt;
				case 241:
					return MetaType.MetaXml;
				case 242:
					goto IL_279;
				case 243:
					return MetaType.s_metaTable;
				default:
					goto IL_279;
				}
			}
			IL_1C6:
			return MetaType.s_metaVarChar;
			IL_1CC:
			if (80U != userType)
			{
				return MetaType.s_metaBinary;
			}
			return MetaType.s_metaTimestamp;
			IL_1E3:
			return MetaType.s_metaChar;
			IL_279:
			throw SQL.InvalidSqlDbType((SqlDbType)tdsType);
		}

		// Token: 0x06001315 RID: 4885 RVA: 0x00063C28 File Offset: 0x00061E28
		internal static MetaType GetDefaultMetaType()
		{
			return MetaType.MetaNVarChar;
		}

		// Token: 0x06001316 RID: 4886 RVA: 0x00063C2F File Offset: 0x00061E2F
		internal static string GetStringFromXml(XmlReader xmlreader)
		{
			return new SqlXml(xmlreader).Value;
		}

		// Token: 0x06001317 RID: 4887 RVA: 0x00063C3C File Offset: 0x00061E3C
		public static TdsDateTime FromDateTime(DateTime dateTime, byte cb)
		{
			TdsDateTime result = default(TdsDateTime);
			SqlDateTime sqlDateTime;
			if (cb == 8)
			{
				sqlDateTime = new SqlDateTime(dateTime);
				result.time = sqlDateTime.TimeTicks;
			}
			else
			{
				sqlDateTime = new SqlDateTime(dateTime.AddSeconds(30.0));
				result.time = sqlDateTime.TimeTicks / SqlDateTime.SQLTicksPerMinute;
			}
			result.days = sqlDateTime.DayTicks;
			return result;
		}

		// Token: 0x06001318 RID: 4888 RVA: 0x00063CA8 File Offset: 0x00061EA8
		public static DateTime ToDateTime(int sqlDays, int sqlTime, int length)
		{
			if (length == 4)
			{
				return new SqlDateTime(sqlDays, sqlTime * SqlDateTime.SQLTicksPerMinute).Value;
			}
			return new SqlDateTime(sqlDays, sqlTime).Value;
		}

		// Token: 0x06001319 RID: 4889 RVA: 0x00063CDE File Offset: 0x00061EDE
		internal static int GetTimeSizeFromScale(byte scale)
		{
			if (scale <= 2)
			{
				return 3;
			}
			if (scale <= 4)
			{
				return 4;
			}
			return 5;
		}

		// Token: 0x0600131A RID: 4890 RVA: 0x00063CF0 File Offset: 0x00061EF0
		// Note: this type is marked as 'beforefieldinit'.
		static MetaType()
		{
		}

		// Token: 0x04000D5A RID: 3418
		internal readonly Type ClassType;

		// Token: 0x04000D5B RID: 3419
		internal readonly Type SqlType;

		// Token: 0x04000D5C RID: 3420
		internal readonly int FixedLength;

		// Token: 0x04000D5D RID: 3421
		internal readonly bool IsFixed;

		// Token: 0x04000D5E RID: 3422
		internal readonly bool IsLong;

		// Token: 0x04000D5F RID: 3423
		internal readonly bool IsPlp;

		// Token: 0x04000D60 RID: 3424
		internal readonly byte Precision;

		// Token: 0x04000D61 RID: 3425
		internal readonly byte Scale;

		// Token: 0x04000D62 RID: 3426
		internal readonly byte TDSType;

		// Token: 0x04000D63 RID: 3427
		internal readonly byte NullableType;

		// Token: 0x04000D64 RID: 3428
		internal readonly string TypeName;

		// Token: 0x04000D65 RID: 3429
		internal readonly SqlDbType SqlDbType;

		// Token: 0x04000D66 RID: 3430
		internal readonly DbType DbType;

		// Token: 0x04000D67 RID: 3431
		internal readonly byte PropBytes;

		// Token: 0x04000D68 RID: 3432
		internal readonly bool IsAnsiType;

		// Token: 0x04000D69 RID: 3433
		internal readonly bool IsBinType;

		// Token: 0x04000D6A RID: 3434
		internal readonly bool IsCharType;

		// Token: 0x04000D6B RID: 3435
		internal readonly bool IsNCharType;

		// Token: 0x04000D6C RID: 3436
		internal readonly bool IsSizeInCharacters;

		// Token: 0x04000D6D RID: 3437
		internal readonly bool IsNewKatmaiType;

		// Token: 0x04000D6E RID: 3438
		internal readonly bool IsVarTime;

		// Token: 0x04000D6F RID: 3439
		internal readonly bool Is70Supported;

		// Token: 0x04000D70 RID: 3440
		internal readonly bool Is80Supported;

		// Token: 0x04000D71 RID: 3441
		internal readonly bool Is90Supported;

		// Token: 0x04000D72 RID: 3442
		internal readonly bool Is100Supported;

		// Token: 0x04000D73 RID: 3443
		private static readonly MetaType s_metaBigInt = new MetaType(19, byte.MaxValue, 8, true, false, false, 127, 38, "bigint", typeof(long), typeof(SqlInt64), SqlDbType.BigInt, DbType.Int64, 0);

		// Token: 0x04000D74 RID: 3444
		private static readonly MetaType s_metaFloat = new MetaType(15, byte.MaxValue, 8, true, false, false, 62, 109, "float", typeof(double), typeof(SqlDouble), SqlDbType.Float, DbType.Double, 0);

		// Token: 0x04000D75 RID: 3445
		private static readonly MetaType s_metaReal = new MetaType(7, byte.MaxValue, 4, true, false, false, 59, 109, "real", typeof(float), typeof(SqlSingle), SqlDbType.Real, DbType.Single, 0);

		// Token: 0x04000D76 RID: 3446
		private static readonly MetaType s_metaBinary = new MetaType(byte.MaxValue, byte.MaxValue, -1, false, false, false, 173, 173, "binary", typeof(byte[]), typeof(SqlBinary), SqlDbType.Binary, DbType.Binary, 2);

		// Token: 0x04000D77 RID: 3447
		private static readonly MetaType s_metaTimestamp = new MetaType(byte.MaxValue, byte.MaxValue, -1, false, false, false, 173, 173, "timestamp", typeof(byte[]), typeof(SqlBinary), SqlDbType.Timestamp, DbType.Binary, 2);

		// Token: 0x04000D78 RID: 3448
		internal static readonly MetaType MetaVarBinary = new MetaType(byte.MaxValue, byte.MaxValue, -1, false, false, false, 165, 165, "varbinary", typeof(byte[]), typeof(SqlBinary), SqlDbType.VarBinary, DbType.Binary, 2);

		// Token: 0x04000D79 RID: 3449
		internal static readonly MetaType MetaMaxVarBinary = new MetaType(byte.MaxValue, byte.MaxValue, -1, false, true, true, 165, 165, "varbinary", typeof(byte[]), typeof(SqlBinary), SqlDbType.VarBinary, DbType.Binary, 2);

		// Token: 0x04000D7A RID: 3450
		private static readonly MetaType s_metaSmallVarBinary = new MetaType(byte.MaxValue, byte.MaxValue, -1, false, false, false, 37, 173, ADP.StrEmpty, typeof(byte[]), typeof(SqlBinary), (SqlDbType)24, DbType.Binary, 2);

		// Token: 0x04000D7B RID: 3451
		internal static readonly MetaType MetaImage = new MetaType(byte.MaxValue, byte.MaxValue, -1, false, true, false, 34, 34, "image", typeof(byte[]), typeof(SqlBinary), SqlDbType.Image, DbType.Binary, 0);

		// Token: 0x04000D7C RID: 3452
		private static readonly MetaType s_metaBit = new MetaType(byte.MaxValue, byte.MaxValue, 1, true, false, false, 50, 104, "bit", typeof(bool), typeof(SqlBoolean), SqlDbType.Bit, DbType.Boolean, 0);

		// Token: 0x04000D7D RID: 3453
		private static readonly MetaType s_metaTinyInt = new MetaType(3, byte.MaxValue, 1, true, false, false, 48, 38, "tinyint", typeof(byte), typeof(SqlByte), SqlDbType.TinyInt, DbType.Byte, 0);

		// Token: 0x04000D7E RID: 3454
		private static readonly MetaType s_metaSmallInt = new MetaType(5, byte.MaxValue, 2, true, false, false, 52, 38, "smallint", typeof(short), typeof(SqlInt16), SqlDbType.SmallInt, DbType.Int16, 0);

		// Token: 0x04000D7F RID: 3455
		private static readonly MetaType s_metaInt = new MetaType(10, byte.MaxValue, 4, true, false, false, 56, 38, "int", typeof(int), typeof(SqlInt32), SqlDbType.Int, DbType.Int32, 0);

		// Token: 0x04000D80 RID: 3456
		private static readonly MetaType s_metaChar = new MetaType(byte.MaxValue, byte.MaxValue, -1, false, false, false, 175, 175, "char", typeof(string), typeof(SqlString), SqlDbType.Char, DbType.AnsiStringFixedLength, 7);

		// Token: 0x04000D81 RID: 3457
		private static readonly MetaType s_metaVarChar = new MetaType(byte.MaxValue, byte.MaxValue, -1, false, false, false, 167, 167, "varchar", typeof(string), typeof(SqlString), SqlDbType.VarChar, DbType.AnsiString, 7);

		// Token: 0x04000D82 RID: 3458
		internal static readonly MetaType MetaMaxVarChar = new MetaType(byte.MaxValue, byte.MaxValue, -1, false, true, true, 167, 167, "varchar", typeof(string), typeof(SqlString), SqlDbType.VarChar, DbType.AnsiString, 7);

		// Token: 0x04000D83 RID: 3459
		internal static readonly MetaType MetaText = new MetaType(byte.MaxValue, byte.MaxValue, -1, false, true, false, 35, 35, "text", typeof(string), typeof(SqlString), SqlDbType.Text, DbType.AnsiString, 0);

		// Token: 0x04000D84 RID: 3460
		private static readonly MetaType s_metaNChar = new MetaType(byte.MaxValue, byte.MaxValue, -1, false, false, false, 239, 239, "nchar", typeof(string), typeof(SqlString), SqlDbType.NChar, DbType.StringFixedLength, 7);

		// Token: 0x04000D85 RID: 3461
		internal static readonly MetaType MetaNVarChar = new MetaType(byte.MaxValue, byte.MaxValue, -1, false, false, false, 231, 231, "nvarchar", typeof(string), typeof(SqlString), SqlDbType.NVarChar, DbType.String, 7);

		// Token: 0x04000D86 RID: 3462
		internal static readonly MetaType MetaMaxNVarChar = new MetaType(byte.MaxValue, byte.MaxValue, -1, false, true, true, 231, 231, "nvarchar", typeof(string), typeof(SqlString), SqlDbType.NVarChar, DbType.String, 7);

		// Token: 0x04000D87 RID: 3463
		internal static readonly MetaType MetaNText = new MetaType(byte.MaxValue, byte.MaxValue, -1, false, true, false, 99, 99, "ntext", typeof(string), typeof(SqlString), SqlDbType.NText, DbType.String, 7);

		// Token: 0x04000D88 RID: 3464
		internal static readonly MetaType MetaDecimal = new MetaType(38, 4, 17, true, false, false, 108, 108, "decimal", typeof(decimal), typeof(SqlDecimal), SqlDbType.Decimal, DbType.Decimal, 2);

		// Token: 0x04000D89 RID: 3465
		internal static readonly MetaType MetaXml = new MetaType(byte.MaxValue, byte.MaxValue, -1, false, true, true, 241, 241, "xml", typeof(string), typeof(SqlXml), SqlDbType.Xml, DbType.Xml, 0);

		// Token: 0x04000D8A RID: 3466
		private static readonly MetaType s_metaDateTime = new MetaType(23, 3, 8, true, false, false, 61, 111, "datetime", typeof(DateTime), typeof(SqlDateTime), SqlDbType.DateTime, DbType.DateTime, 0);

		// Token: 0x04000D8B RID: 3467
		private static readonly MetaType s_metaSmallDateTime = new MetaType(16, 0, 4, true, false, false, 58, 111, "smalldatetime", typeof(DateTime), typeof(SqlDateTime), SqlDbType.SmallDateTime, DbType.DateTime, 0);

		// Token: 0x04000D8C RID: 3468
		private static readonly MetaType s_metaMoney = new MetaType(19, byte.MaxValue, 8, true, false, false, 60, 110, "money", typeof(decimal), typeof(SqlMoney), SqlDbType.Money, DbType.Currency, 0);

		// Token: 0x04000D8D RID: 3469
		private static readonly MetaType s_metaSmallMoney = new MetaType(10, byte.MaxValue, 4, true, false, false, 122, 110, "smallmoney", typeof(decimal), typeof(SqlMoney), SqlDbType.SmallMoney, DbType.Currency, 0);

		// Token: 0x04000D8E RID: 3470
		private static readonly MetaType s_metaUniqueId = new MetaType(byte.MaxValue, byte.MaxValue, 16, true, false, false, 36, 36, "uniqueidentifier", typeof(Guid), typeof(SqlGuid), SqlDbType.UniqueIdentifier, DbType.Guid, 0);

		// Token: 0x04000D8F RID: 3471
		private static readonly MetaType s_metaVariant = new MetaType(byte.MaxValue, byte.MaxValue, -1, true, false, false, 98, 98, "sql_variant", typeof(object), typeof(object), SqlDbType.Variant, DbType.Object, 0);

		// Token: 0x04000D90 RID: 3472
		internal static readonly MetaType MetaUdt = new MetaType(byte.MaxValue, byte.MaxValue, -1, false, false, true, 240, 240, "udt", typeof(object), typeof(object), SqlDbType.Udt, DbType.Object, 0);

		// Token: 0x04000D91 RID: 3473
		private static readonly MetaType s_metaMaxUdt = new MetaType(byte.MaxValue, byte.MaxValue, -1, false, true, true, 240, 240, "udt", typeof(object), typeof(object), SqlDbType.Udt, DbType.Object, 0);

		// Token: 0x04000D92 RID: 3474
		private static readonly MetaType s_metaTable = new MetaType(byte.MaxValue, byte.MaxValue, -1, false, false, false, 243, 243, "table", typeof(IEnumerable<DbDataRecord>), typeof(IEnumerable<DbDataRecord>), SqlDbType.Structured, DbType.Object, 0);

		// Token: 0x04000D93 RID: 3475
		private static readonly MetaType s_metaSUDT = new MetaType(byte.MaxValue, byte.MaxValue, -1, false, false, false, 31, 31, "", typeof(SqlDataRecord), typeof(SqlDataRecord), SqlDbType.Structured, DbType.Object, 0);

		// Token: 0x04000D94 RID: 3476
		private static readonly MetaType s_metaDate = new MetaType(byte.MaxValue, byte.MaxValue, 3, true, false, false, 40, 40, "date", typeof(DateTime), typeof(DateTime), SqlDbType.Date, DbType.Date, 0);

		// Token: 0x04000D95 RID: 3477
		internal static readonly MetaType MetaTime = new MetaType(byte.MaxValue, 7, -1, false, false, false, 41, 41, "time", typeof(TimeSpan), typeof(TimeSpan), SqlDbType.Time, DbType.Time, 1);

		// Token: 0x04000D96 RID: 3478
		private static readonly MetaType s_metaDateTime2 = new MetaType(byte.MaxValue, 7, -1, false, false, false, 42, 42, "datetime2", typeof(DateTime), typeof(DateTime), SqlDbType.DateTime2, DbType.DateTime2, 1);

		// Token: 0x04000D97 RID: 3479
		internal static readonly MetaType MetaDateTimeOffset = new MetaType(byte.MaxValue, 7, -1, false, false, false, 43, 43, "datetimeoffset", typeof(DateTimeOffset), typeof(DateTimeOffset), SqlDbType.DateTimeOffset, DbType.DateTimeOffset, 1);

		// Token: 0x0200019F RID: 415
		private static class MetaTypeName
		{
			// Token: 0x04000D98 RID: 3480
			public const string BIGINT = "bigint";

			// Token: 0x04000D99 RID: 3481
			public const string BINARY = "binary";

			// Token: 0x04000D9A RID: 3482
			public const string BIT = "bit";

			// Token: 0x04000D9B RID: 3483
			public const string CHAR = "char";

			// Token: 0x04000D9C RID: 3484
			public const string DATETIME = "datetime";

			// Token: 0x04000D9D RID: 3485
			public const string DECIMAL = "decimal";

			// Token: 0x04000D9E RID: 3486
			public const string FLOAT = "float";

			// Token: 0x04000D9F RID: 3487
			public const string IMAGE = "image";

			// Token: 0x04000DA0 RID: 3488
			public const string INT = "int";

			// Token: 0x04000DA1 RID: 3489
			public const string MONEY = "money";

			// Token: 0x04000DA2 RID: 3490
			public const string NCHAR = "nchar";

			// Token: 0x04000DA3 RID: 3491
			public const string NTEXT = "ntext";

			// Token: 0x04000DA4 RID: 3492
			public const string NVARCHAR = "nvarchar";

			// Token: 0x04000DA5 RID: 3493
			public const string REAL = "real";

			// Token: 0x04000DA6 RID: 3494
			public const string ROWGUID = "uniqueidentifier";

			// Token: 0x04000DA7 RID: 3495
			public const string SMALLDATETIME = "smalldatetime";

			// Token: 0x04000DA8 RID: 3496
			public const string SMALLINT = "smallint";

			// Token: 0x04000DA9 RID: 3497
			public const string SMALLMONEY = "smallmoney";

			// Token: 0x04000DAA RID: 3498
			public const string TEXT = "text";

			// Token: 0x04000DAB RID: 3499
			public const string TIMESTAMP = "timestamp";

			// Token: 0x04000DAC RID: 3500
			public const string TINYINT = "tinyint";

			// Token: 0x04000DAD RID: 3501
			public const string UDT = "udt";

			// Token: 0x04000DAE RID: 3502
			public const string VARBINARY = "varbinary";

			// Token: 0x04000DAF RID: 3503
			public const string VARCHAR = "varchar";

			// Token: 0x04000DB0 RID: 3504
			public const string VARIANT = "sql_variant";

			// Token: 0x04000DB1 RID: 3505
			public const string XML = "xml";

			// Token: 0x04000DB2 RID: 3506
			public const string TABLE = "table";

			// Token: 0x04000DB3 RID: 3507
			public const string DATE = "date";

			// Token: 0x04000DB4 RID: 3508
			public const string TIME = "time";

			// Token: 0x04000DB5 RID: 3509
			public const string DATETIME2 = "datetime2";

			// Token: 0x04000DB6 RID: 3510
			public const string DATETIMEOFFSET = "datetimeoffset";
		}
	}
}
