﻿using System;
using Microsoft.SqlServer.Server;

namespace System.Data.SqlClient
{
	// Token: 0x020001E2 RID: 482
	internal class TdsParameterSetter : SmiTypedGetterSetter
	{
		// Token: 0x060015DC RID: 5596 RVA: 0x0006CF68 File Offset: 0x0006B168
		internal TdsParameterSetter(TdsParserStateObject stateObj, SmiMetaData md)
		{
			this._target = new TdsRecordBufferSetter(stateObj, md);
		}

		// Token: 0x1700041D RID: 1053
		// (get) Token: 0x060015DD RID: 5597 RVA: 0x000061C5 File Offset: 0x000043C5
		internal override bool CanGet
		{
			get
			{
				return false;
			}
		}

		// Token: 0x1700041E RID: 1054
		// (get) Token: 0x060015DE RID: 5598 RVA: 0x0000EF1B File Offset: 0x0000D11B
		internal override bool CanSet
		{
			get
			{
				return true;
			}
		}

		// Token: 0x060015DF RID: 5599 RVA: 0x0006CF7D File Offset: 0x0006B17D
		internal override SmiTypedGetterSetter GetTypedGetterSetter(SmiEventSink sink, int ordinal)
		{
			return this._target;
		}

		// Token: 0x060015E0 RID: 5600 RVA: 0x0006CF85 File Offset: 0x0006B185
		public override void SetDBNull(SmiEventSink sink, int ordinal)
		{
			this._target.EndElements(sink);
		}

		// Token: 0x04001058 RID: 4184
		private TdsRecordBufferSetter _target;
	}
}
