﻿using System;
using System.Collections.Generic;

namespace System.Data.SqlClient
{
	// Token: 0x02000202 RID: 514
	internal sealed class _SqlMetaDataSetCollection
	{
		// Token: 0x060016E1 RID: 5857 RVA: 0x00077F76 File Offset: 0x00076176
		internal _SqlMetaDataSetCollection()
		{
			this._altMetaDataSetArray = new List<_SqlMetaDataSet>();
		}

		// Token: 0x060016E2 RID: 5858 RVA: 0x00077F8C File Offset: 0x0007618C
		internal void SetAltMetaData(_SqlMetaDataSet altMetaDataSet)
		{
			int id = (int)altMetaDataSet.id;
			for (int i = 0; i < this._altMetaDataSetArray.Count; i++)
			{
				if ((int)this._altMetaDataSetArray[i].id == id)
				{
					this._altMetaDataSetArray[i] = altMetaDataSet;
					return;
				}
			}
			this._altMetaDataSetArray.Add(altMetaDataSet);
		}

		// Token: 0x060016E3 RID: 5859 RVA: 0x00077FE4 File Offset: 0x000761E4
		internal _SqlMetaDataSet GetAltMetaData(int id)
		{
			foreach (_SqlMetaDataSet sqlMetaDataSet in this._altMetaDataSetArray)
			{
				if ((int)sqlMetaDataSet.id == id)
				{
					return sqlMetaDataSet;
				}
			}
			return null;
		}

		// Token: 0x060016E4 RID: 5860 RVA: 0x00078040 File Offset: 0x00076240
		public object Clone()
		{
			_SqlMetaDataSetCollection sqlMetaDataSetCollection = new _SqlMetaDataSetCollection();
			sqlMetaDataSetCollection.metaDataSet = ((this.metaDataSet == null) ? null : ((_SqlMetaDataSet)this.metaDataSet.Clone()));
			foreach (_SqlMetaDataSet sqlMetaDataSet in this._altMetaDataSetArray)
			{
				sqlMetaDataSetCollection._altMetaDataSetArray.Add((_SqlMetaDataSet)sqlMetaDataSet.Clone());
			}
			return sqlMetaDataSetCollection;
		}

		// Token: 0x04001132 RID: 4402
		private readonly List<_SqlMetaDataSet> _altMetaDataSetArray;

		// Token: 0x04001133 RID: 4403
		internal _SqlMetaDataSet metaDataSet;
	}
}
