﻿using System;

namespace System.Data.SqlClient
{
	/// <summary>Indicates the source of the notification received by the dependency event handler.</summary>
	// Token: 0x020001B1 RID: 433
	public enum SqlNotificationSource
	{
		/// <summary>Data has changed; for example, an insert, update, delete, or truncate operation occurred.</summary>
		// Token: 0x04000E2C RID: 3628
		Data,
		/// <summary>The subscription time-out expired.</summary>
		// Token: 0x04000E2D RID: 3629
		Timeout,
		/// <summary>A database object changed; for example, an underlying object related to the query was dropped or modified.</summary>
		// Token: 0x04000E2E RID: 3630
		Object,
		/// <summary>The database state changed; for example, the database related to the query was dropped or detached.</summary>
		// Token: 0x04000E2F RID: 3631
		Database,
		/// <summary>A system-related event occurred. For example, there was an internal error, the server was restarted, or resource pressure caused the invalidation.</summary>
		// Token: 0x04000E30 RID: 3632
		System,
		/// <summary>The Transact-SQL statement is not valid for notifications; for example, a SELECT statement that could not be notified or a non-SELECT statement was executed.</summary>
		// Token: 0x04000E31 RID: 3633
		Statement,
		/// <summary>The run-time environment was not compatible with notifications; for example, the isolation level was set to snapshot, or one or more SET options are not compatible.</summary>
		// Token: 0x04000E32 RID: 3634
		Environment,
		/// <summary>A run-time error occurred during execution.</summary>
		// Token: 0x04000E33 RID: 3635
		Execution,
		/// <summary>Internal only; not intended to be used in your code.</summary>
		// Token: 0x04000E34 RID: 3636
		Owner,
		/// <summary>Used when the source option sent by the server was not recognized by the client. </summary>
		// Token: 0x04000E35 RID: 3637
		Unknown = -1,
		/// <summary>A client-initiated notification occurred, such as a client-side time-out or as a result of attempting to add a command to a dependency that has already fired.</summary>
		// Token: 0x04000E36 RID: 3638
		Client = -2
	}
}
