﻿using System;
using System.Data.Common;
using System.Data.SqlTypes;
using System.IO;
using System.Xml;

namespace System.Data.SqlClient
{
	// Token: 0x020001CC RID: 460
	internal sealed class SqlStream : Stream
	{
		// Token: 0x060014EE RID: 5358 RVA: 0x0006AB22 File Offset: 0x00068D22
		internal SqlStream(SqlDataReader reader, bool addByteOrderMark, bool processAllRows) : this(0, reader, addByteOrderMark, processAllRows, true)
		{
		}

		// Token: 0x060014EF RID: 5359 RVA: 0x0006AB2F File Offset: 0x00068D2F
		internal SqlStream(int columnOrdinal, SqlDataReader reader, bool addByteOrderMark, bool processAllRows, bool advanceReader)
		{
			this._columnOrdinal = columnOrdinal;
			this._reader = reader;
			this._bom = (addByteOrderMark ? 65279 : 0);
			this._processAllRows = processAllRows;
			this._advanceReader = advanceReader;
		}

		// Token: 0x17000407 RID: 1031
		// (get) Token: 0x060014F0 RID: 5360 RVA: 0x0000EF1B File Offset: 0x0000D11B
		public override bool CanRead
		{
			get
			{
				return true;
			}
		}

		// Token: 0x17000408 RID: 1032
		// (get) Token: 0x060014F1 RID: 5361 RVA: 0x000061C5 File Offset: 0x000043C5
		public override bool CanSeek
		{
			get
			{
				return false;
			}
		}

		// Token: 0x17000409 RID: 1033
		// (get) Token: 0x060014F2 RID: 5362 RVA: 0x000061C5 File Offset: 0x000043C5
		public override bool CanWrite
		{
			get
			{
				return false;
			}
		}

		// Token: 0x1700040A RID: 1034
		// (get) Token: 0x060014F3 RID: 5363 RVA: 0x0005CDBE File Offset: 0x0005AFBE
		public override long Length
		{
			get
			{
				throw ADP.NotSupported();
			}
		}

		// Token: 0x1700040B RID: 1035
		// (get) Token: 0x060014F4 RID: 5364 RVA: 0x0005CDBE File Offset: 0x0005AFBE
		// (set) Token: 0x060014F5 RID: 5365 RVA: 0x0005CDBE File Offset: 0x0005AFBE
		public override long Position
		{
			get
			{
				throw ADP.NotSupported();
			}
			set
			{
				throw ADP.NotSupported();
			}
		}

		// Token: 0x060014F6 RID: 5366 RVA: 0x0006AB68 File Offset: 0x00068D68
		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing && this._advanceReader && this._reader != null && !this._reader.IsClosed)
				{
					this._reader.Close();
				}
				this._reader = null;
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		// Token: 0x060014F7 RID: 5367 RVA: 0x0005CDBE File Offset: 0x0005AFBE
		public override void Flush()
		{
			throw ADP.NotSupported();
		}

		// Token: 0x060014F8 RID: 5368 RVA: 0x0006ABC4 File Offset: 0x00068DC4
		public override int Read(byte[] buffer, int offset, int count)
		{
			int num = 0;
			int num2 = 0;
			if (this._reader == null)
			{
				throw ADP.StreamClosed("Read");
			}
			if (buffer == null)
			{
				throw ADP.ArgumentNull("buffer");
			}
			if (offset < 0 || count < 0)
			{
				throw ADP.ArgumentOutOfRange(string.Empty, (offset < 0) ? "offset" : "count");
			}
			if (buffer.Length - offset < count)
			{
				throw ADP.ArgumentOutOfRange("count");
			}
			if (this._bom > 0)
			{
				this._bufferedData = new byte[2];
				num2 = this.ReadBytes(this._bufferedData, 0, 2);
				if (num2 < 2 || (this._bufferedData[0] == 223 && this._bufferedData[1] == 255))
				{
					this._bom = 0;
				}
				while (count > 0 && this._bom > 0)
				{
					buffer[offset] = (byte)this._bom;
					this._bom >>= 8;
					offset++;
					count--;
					num++;
				}
			}
			if (num2 > 0)
			{
				while (count > 0)
				{
					buffer[offset++] = this._bufferedData[0];
					num++;
					count--;
					if (num2 > 1 && count > 0)
					{
						buffer[offset++] = this._bufferedData[1];
						num++;
						count--;
						break;
					}
				}
				this._bufferedData = null;
			}
			return num + this.ReadBytes(buffer, offset, count);
		}

		// Token: 0x060014F9 RID: 5369 RVA: 0x0006AD08 File Offset: 0x00068F08
		private static bool AdvanceToNextRow(SqlDataReader reader)
		{
			while (!reader.Read())
			{
				if (!reader.NextResult())
				{
					return false;
				}
			}
			return true;
		}

		// Token: 0x060014FA RID: 5370 RVA: 0x0006AD20 File Offset: 0x00068F20
		private int ReadBytes(byte[] buffer, int offset, int count)
		{
			bool flag = true;
			int num = 0;
			if (this._reader.IsClosed || this._endOfColumn)
			{
				return 0;
			}
			try
			{
				while (count > 0)
				{
					if (this._advanceReader && this._bytesCol == 0L)
					{
						flag = false;
						if ((!this._readFirstRow || this._processAllRows) && SqlStream.AdvanceToNextRow(this._reader))
						{
							this._readFirstRow = true;
							if (this._reader.IsDBNull(this._columnOrdinal))
							{
								continue;
							}
							flag = true;
						}
					}
					if (!flag)
					{
						break;
					}
					int num2 = (int)this._reader.GetBytesInternal(this._columnOrdinal, this._bytesCol, buffer, offset, count);
					if (num2 < count)
					{
						this._bytesCol = 0L;
						flag = false;
						if (!this._advanceReader)
						{
							this._endOfColumn = true;
						}
					}
					else
					{
						this._bytesCol += (long)num2;
					}
					count -= num2;
					offset += num2;
					num += num2;
				}
				if (!flag && this._advanceReader)
				{
					this._reader.Close();
				}
			}
			catch (Exception e)
			{
				if (this._advanceReader && ADP.IsCatchableExceptionType(e))
				{
					this._reader.Close();
				}
				throw;
			}
			return num;
		}

		// Token: 0x060014FB RID: 5371 RVA: 0x0006AE44 File Offset: 0x00069044
		internal XmlReader ToXmlReader(bool async = false)
		{
			return SqlTypeWorkarounds.SqlXmlCreateSqlXmlReader(this, true, async);
		}

		// Token: 0x060014FC RID: 5372 RVA: 0x0005CDBE File Offset: 0x0005AFBE
		public override long Seek(long offset, SeekOrigin origin)
		{
			throw ADP.NotSupported();
		}

		// Token: 0x060014FD RID: 5373 RVA: 0x0005CDBE File Offset: 0x0005AFBE
		public override void SetLength(long value)
		{
			throw ADP.NotSupported();
		}

		// Token: 0x060014FE RID: 5374 RVA: 0x0005CDBE File Offset: 0x0005AFBE
		public override void Write(byte[] buffer, int offset, int count)
		{
			throw ADP.NotSupported();
		}

		// Token: 0x04000E9C RID: 3740
		private SqlDataReader _reader;

		// Token: 0x04000E9D RID: 3741
		private int _columnOrdinal;

		// Token: 0x04000E9E RID: 3742
		private long _bytesCol;

		// Token: 0x04000E9F RID: 3743
		private int _bom;

		// Token: 0x04000EA0 RID: 3744
		private byte[] _bufferedData;

		// Token: 0x04000EA1 RID: 3745
		private bool _processAllRows;

		// Token: 0x04000EA2 RID: 3746
		private bool _advanceReader;

		// Token: 0x04000EA3 RID: 3747
		private bool _readFirstRow;

		// Token: 0x04000EA4 RID: 3748
		private bool _endOfColumn;
	}
}
