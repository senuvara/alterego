﻿using System;

namespace System.Data.SqlClient
{
	// Token: 0x02000180 RID: 384
	internal enum SqlConnectionInternalSourceType
	{
		// Token: 0x04000CAE RID: 3246
		Principle,
		// Token: 0x04000CAF RID: 3247
		Failover,
		// Token: 0x04000CB0 RID: 3248
		RoutingDestination
	}
}
