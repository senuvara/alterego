﻿using System;

namespace System.Data
{
	/// <summary>Specifies how conflicting changes to the data source will be detected and resolved.</summary>
	// Token: 0x02000054 RID: 84
	public enum ConflictOption
	{
		/// <summary>Update and delete statements will include all searchable columns from the table in the WHERE clause. This is equivalent to specifying <see langword="CompareAllValuesUpdate" /> | <see langword="CompareAllValuesDelete" />.</summary>
		// Token: 0x040004FA RID: 1274
		CompareAllSearchableValues = 1,
		/// <summary>If any Timestamp columns exist in the table, they are used in the WHERE clause for all generated update statements. This is equivalent to specifying <see langword="CompareRowVersionUpdate" /> | <see langword="CompareRowVersionDelete" />.</summary>
		// Token: 0x040004FB RID: 1275
		CompareRowVersion,
		/// <summary>All update and delete statements include only <see cref="P:System.Data.DataTable.PrimaryKey" /> columns in the WHERE clause. If no <see cref="P:System.Data.DataTable.PrimaryKey" /> is defined, all searchable columns are included in the WHERE clause. This is equivalent to <see langword="OverwriteChangesUpdate" /> | <see langword="OverwriteChangesDelete" />.</summary>
		// Token: 0x040004FC RID: 1276
		OverwriteChanges
	}
}
