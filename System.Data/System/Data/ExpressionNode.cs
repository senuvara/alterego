﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Globalization;

namespace System.Data
{
	// Token: 0x020000AC RID: 172
	internal abstract class ExpressionNode
	{
		// Token: 0x06000A59 RID: 2649 RVA: 0x000304E4 File Offset: 0x0002E6E4
		protected ExpressionNode(DataTable table)
		{
			this._table = table;
		}

		// Token: 0x170001E1 RID: 481
		// (get) Token: 0x06000A5A RID: 2650 RVA: 0x000304F4 File Offset: 0x0002E6F4
		internal IFormatProvider FormatProvider
		{
			get
			{
				if (this._table == null)
				{
					return CultureInfo.CurrentCulture;
				}
				return this._table.FormatProvider;
			}
		}

		// Token: 0x170001E2 RID: 482
		// (get) Token: 0x06000A5B RID: 2651 RVA: 0x000061C5 File Offset: 0x000043C5
		internal virtual bool IsSqlColumn
		{
			get
			{
				return false;
			}
		}

		// Token: 0x170001E3 RID: 483
		// (get) Token: 0x06000A5C RID: 2652 RVA: 0x0003051C File Offset: 0x0002E71C
		protected DataTable table
		{
			get
			{
				return this._table;
			}
		}

		// Token: 0x06000A5D RID: 2653 RVA: 0x00030524 File Offset: 0x0002E724
		protected void BindTable(DataTable table)
		{
			this._table = table;
		}

		// Token: 0x06000A5E RID: 2654
		internal abstract void Bind(DataTable table, List<DataColumn> list);

		// Token: 0x06000A5F RID: 2655
		internal abstract object Eval();

		// Token: 0x06000A60 RID: 2656
		internal abstract object Eval(DataRow row, DataRowVersion version);

		// Token: 0x06000A61 RID: 2657
		internal abstract object Eval(int[] recordNos);

		// Token: 0x06000A62 RID: 2658
		internal abstract bool IsConstant();

		// Token: 0x06000A63 RID: 2659
		internal abstract bool IsTableConstant();

		// Token: 0x06000A64 RID: 2660
		internal abstract bool HasLocalAggregate();

		// Token: 0x06000A65 RID: 2661
		internal abstract bool HasRemoteAggregate();

		// Token: 0x06000A66 RID: 2662
		internal abstract ExpressionNode Optimize();

		// Token: 0x06000A67 RID: 2663 RVA: 0x000061C5 File Offset: 0x000043C5
		internal virtual bool DependsOn(DataColumn column)
		{
			return false;
		}

		// Token: 0x06000A68 RID: 2664 RVA: 0x0003052D File Offset: 0x0002E72D
		internal static bool IsInteger(StorageType type)
		{
			return type == StorageType.Int16 || type == StorageType.Int32 || type == StorageType.Int64 || type == StorageType.UInt16 || type == StorageType.UInt32 || type == StorageType.UInt64 || type == StorageType.SByte || type == StorageType.Byte;
		}

		// Token: 0x06000A69 RID: 2665 RVA: 0x00030555 File Offset: 0x0002E755
		internal static bool IsIntegerSql(StorageType type)
		{
			return type == StorageType.Int16 || type == StorageType.Int32 || type == StorageType.Int64 || type == StorageType.UInt16 || type == StorageType.UInt32 || type == StorageType.UInt64 || type == StorageType.SByte || type == StorageType.Byte || type == StorageType.SqlInt64 || type == StorageType.SqlInt32 || type == StorageType.SqlInt16 || type == StorageType.SqlByte;
		}

		// Token: 0x06000A6A RID: 2666 RVA: 0x00030591 File Offset: 0x0002E791
		internal static bool IsSigned(StorageType type)
		{
			return type == StorageType.Int16 || type == StorageType.Int32 || type == StorageType.Int64 || type == StorageType.SByte || ExpressionNode.IsFloat(type);
		}

		// Token: 0x06000A6B RID: 2667 RVA: 0x000305AD File Offset: 0x0002E7AD
		internal static bool IsSignedSql(StorageType type)
		{
			return type == StorageType.Int16 || type == StorageType.Int32 || type == StorageType.Int64 || type == StorageType.SByte || type == StorageType.SqlInt64 || type == StorageType.SqlInt32 || type == StorageType.SqlInt16 || ExpressionNode.IsFloatSql(type);
		}

		// Token: 0x06000A6C RID: 2668 RVA: 0x000305D8 File Offset: 0x0002E7D8
		internal static bool IsUnsigned(StorageType type)
		{
			return type == StorageType.UInt16 || type == StorageType.UInt32 || type == StorageType.UInt64 || type == StorageType.Byte;
		}

		// Token: 0x06000A6D RID: 2669 RVA: 0x000305EE File Offset: 0x0002E7EE
		internal static bool IsUnsignedSql(StorageType type)
		{
			return type == StorageType.UInt16 || type == StorageType.UInt32 || type == StorageType.UInt64 || type == StorageType.SqlByte || type == StorageType.Byte;
		}

		// Token: 0x06000A6E RID: 2670 RVA: 0x00030609 File Offset: 0x0002E809
		internal static bool IsNumeric(StorageType type)
		{
			return ExpressionNode.IsFloat(type) || ExpressionNode.IsInteger(type);
		}

		// Token: 0x06000A6F RID: 2671 RVA: 0x0003061B File Offset: 0x0002E81B
		internal static bool IsNumericSql(StorageType type)
		{
			return ExpressionNode.IsFloatSql(type) || ExpressionNode.IsIntegerSql(type);
		}

		// Token: 0x06000A70 RID: 2672 RVA: 0x0003062D File Offset: 0x0002E82D
		internal static bool IsFloat(StorageType type)
		{
			return type == StorageType.Single || type == StorageType.Double || type == StorageType.Decimal;
		}

		// Token: 0x06000A71 RID: 2673 RVA: 0x00030640 File Offset: 0x0002E840
		internal static bool IsFloatSql(StorageType type)
		{
			return type == StorageType.Single || type == StorageType.Double || type == StorageType.Decimal || type == StorageType.SqlDouble || type == StorageType.SqlDecimal || type == StorageType.SqlMoney || type == StorageType.SqlSingle;
		}

		// Token: 0x040006EE RID: 1774
		private DataTable _table;
	}
}
