﻿using System;
using System.ComponentModel;
using System.Globalization;

namespace System.Data
{
	// Token: 0x020000D9 RID: 217
	internal sealed class PrimaryKeyTypeConverter : ReferenceConverter
	{
		// Token: 0x06000BDE RID: 3038 RVA: 0x0003632E File Offset: 0x0003452E
		public PrimaryKeyTypeConverter() : base(typeof(DataColumn[]))
		{
		}

		// Token: 0x06000BDF RID: 3039 RVA: 0x000061C5 File Offset: 0x000043C5
		public override bool GetPropertiesSupported(ITypeDescriptorContext context)
		{
			return false;
		}

		// Token: 0x06000BE0 RID: 3040 RVA: 0x0000EE3C File Offset: 0x0000D03C
		public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
		{
			return destinationType == typeof(string) || base.CanConvertTo(context, destinationType);
		}

		// Token: 0x06000BE1 RID: 3041 RVA: 0x00036340 File Offset: 0x00034540
		public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType)
		{
			if (destinationType == null)
			{
				throw new ArgumentNullException("destinationType");
			}
			if (!(destinationType == typeof(string)))
			{
				return base.ConvertTo(context, culture, value, destinationType);
			}
			return Array.Empty<DataColumn>().GetType().Name;
		}
	}
}
