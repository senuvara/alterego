﻿using System;

namespace System.Data
{
	/// <summary>Associates a data source column with a <see cref="T:System.Data.DataSet" /> column, and is implemented by the <see cref="T:System.Data.Common.DataColumnMapping" /> class, which is used in common by .NET Framework data providers.</summary>
	// Token: 0x020000C1 RID: 193
	public interface IColumnMapping
	{
		/// <summary>Gets or sets the name of the column within the <see cref="T:System.Data.DataSet" /> to map to.</summary>
		/// <returns>The name of the column within the <see cref="T:System.Data.DataSet" /> to map to. The name is not case sensitive.</returns>
		// Token: 0x170001F3 RID: 499
		// (get) Token: 0x06000B47 RID: 2887
		// (set) Token: 0x06000B48 RID: 2888
		string DataSetColumn { get; set; }

		/// <summary>Gets or sets the name of the column within the data source to map from. The name is case-sensitive.</summary>
		/// <returns>The case-sensitive name of the column in the data source.</returns>
		// Token: 0x170001F4 RID: 500
		// (get) Token: 0x06000B49 RID: 2889
		// (set) Token: 0x06000B4A RID: 2890
		string SourceColumn { get; set; }
	}
}
