﻿using System;

namespace System.Data
{
	/// <summary>Specifies how a command string is interpreted.</summary>
	// Token: 0x02000052 RID: 82
	public enum CommandType
	{
		/// <summary>An SQL text command. (Default.) </summary>
		// Token: 0x040004F1 RID: 1265
		Text = 1,
		/// <summary>The name of a stored procedure.</summary>
		// Token: 0x040004F2 RID: 1266
		StoredProcedure = 4,
		/// <summary>The name of a table.</summary>
		// Token: 0x040004F3 RID: 1267
		TableDirect = 512
	}
}
