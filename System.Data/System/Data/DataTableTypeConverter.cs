﻿using System;
using System.ComponentModel;

namespace System.Data
{
	// Token: 0x02000096 RID: 150
	internal sealed class DataTableTypeConverter : ReferenceConverter
	{
		// Token: 0x0600090E RID: 2318 RVA: 0x0002A39B File Offset: 0x0002859B
		public DataTableTypeConverter() : base(typeof(DataTable))
		{
		}

		// Token: 0x0600090F RID: 2319 RVA: 0x000061C5 File Offset: 0x000043C5
		public override bool GetPropertiesSupported(ITypeDescriptorContext context)
		{
			return false;
		}
	}
}
