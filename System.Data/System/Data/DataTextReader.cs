﻿using System;
using System.Xml;

namespace System.Data
{
	// Token: 0x02000114 RID: 276
	internal sealed class DataTextReader : XmlReader
	{
		// Token: 0x06000E1F RID: 3615 RVA: 0x0004B68B File Offset: 0x0004988B
		internal static XmlReader CreateReader(XmlReader xr)
		{
			return new DataTextReader(xr);
		}

		// Token: 0x06000E20 RID: 3616 RVA: 0x0004B693 File Offset: 0x00049893
		private DataTextReader(XmlReader input)
		{
			this._xmlreader = input;
		}

		// Token: 0x17000258 RID: 600
		// (get) Token: 0x06000E21 RID: 3617 RVA: 0x0004B6A2 File Offset: 0x000498A2
		public override XmlReaderSettings Settings
		{
			get
			{
				return this._xmlreader.Settings;
			}
		}

		// Token: 0x17000259 RID: 601
		// (get) Token: 0x06000E22 RID: 3618 RVA: 0x0004B6AF File Offset: 0x000498AF
		public override XmlNodeType NodeType
		{
			get
			{
				return this._xmlreader.NodeType;
			}
		}

		// Token: 0x1700025A RID: 602
		// (get) Token: 0x06000E23 RID: 3619 RVA: 0x0004B6BC File Offset: 0x000498BC
		public override string Name
		{
			get
			{
				return this._xmlreader.Name;
			}
		}

		// Token: 0x1700025B RID: 603
		// (get) Token: 0x06000E24 RID: 3620 RVA: 0x0004B6C9 File Offset: 0x000498C9
		public override string LocalName
		{
			get
			{
				return this._xmlreader.LocalName;
			}
		}

		// Token: 0x1700025C RID: 604
		// (get) Token: 0x06000E25 RID: 3621 RVA: 0x0004B6D6 File Offset: 0x000498D6
		public override string NamespaceURI
		{
			get
			{
				return this._xmlreader.NamespaceURI;
			}
		}

		// Token: 0x1700025D RID: 605
		// (get) Token: 0x06000E26 RID: 3622 RVA: 0x0004B6E3 File Offset: 0x000498E3
		public override string Prefix
		{
			get
			{
				return this._xmlreader.Prefix;
			}
		}

		// Token: 0x1700025E RID: 606
		// (get) Token: 0x06000E27 RID: 3623 RVA: 0x0004B6F0 File Offset: 0x000498F0
		public override bool HasValue
		{
			get
			{
				return this._xmlreader.HasValue;
			}
		}

		// Token: 0x1700025F RID: 607
		// (get) Token: 0x06000E28 RID: 3624 RVA: 0x0004B6FD File Offset: 0x000498FD
		public override string Value
		{
			get
			{
				return this._xmlreader.Value;
			}
		}

		// Token: 0x17000260 RID: 608
		// (get) Token: 0x06000E29 RID: 3625 RVA: 0x0004B70A File Offset: 0x0004990A
		public override int Depth
		{
			get
			{
				return this._xmlreader.Depth;
			}
		}

		// Token: 0x17000261 RID: 609
		// (get) Token: 0x06000E2A RID: 3626 RVA: 0x0004B717 File Offset: 0x00049917
		public override string BaseURI
		{
			get
			{
				return this._xmlreader.BaseURI;
			}
		}

		// Token: 0x17000262 RID: 610
		// (get) Token: 0x06000E2B RID: 3627 RVA: 0x0004B724 File Offset: 0x00049924
		public override bool IsEmptyElement
		{
			get
			{
				return this._xmlreader.IsEmptyElement;
			}
		}

		// Token: 0x17000263 RID: 611
		// (get) Token: 0x06000E2C RID: 3628 RVA: 0x0004B731 File Offset: 0x00049931
		public override bool IsDefault
		{
			get
			{
				return this._xmlreader.IsDefault;
			}
		}

		// Token: 0x17000264 RID: 612
		// (get) Token: 0x06000E2D RID: 3629 RVA: 0x0004B73E File Offset: 0x0004993E
		public override char QuoteChar
		{
			get
			{
				return this._xmlreader.QuoteChar;
			}
		}

		// Token: 0x17000265 RID: 613
		// (get) Token: 0x06000E2E RID: 3630 RVA: 0x0004B74B File Offset: 0x0004994B
		public override XmlSpace XmlSpace
		{
			get
			{
				return this._xmlreader.XmlSpace;
			}
		}

		// Token: 0x17000266 RID: 614
		// (get) Token: 0x06000E2F RID: 3631 RVA: 0x0004B758 File Offset: 0x00049958
		public override string XmlLang
		{
			get
			{
				return this._xmlreader.XmlLang;
			}
		}

		// Token: 0x17000267 RID: 615
		// (get) Token: 0x06000E30 RID: 3632 RVA: 0x0004B765 File Offset: 0x00049965
		public override int AttributeCount
		{
			get
			{
				return this._xmlreader.AttributeCount;
			}
		}

		// Token: 0x06000E31 RID: 3633 RVA: 0x0004B772 File Offset: 0x00049972
		public override string GetAttribute(string name)
		{
			return this._xmlreader.GetAttribute(name);
		}

		// Token: 0x06000E32 RID: 3634 RVA: 0x0004B780 File Offset: 0x00049980
		public override string GetAttribute(string localName, string namespaceURI)
		{
			return this._xmlreader.GetAttribute(localName, namespaceURI);
		}

		// Token: 0x06000E33 RID: 3635 RVA: 0x0004B78F File Offset: 0x0004998F
		public override string GetAttribute(int i)
		{
			return this._xmlreader.GetAttribute(i);
		}

		// Token: 0x06000E34 RID: 3636 RVA: 0x0004B79D File Offset: 0x0004999D
		public override bool MoveToAttribute(string name)
		{
			return this._xmlreader.MoveToAttribute(name);
		}

		// Token: 0x06000E35 RID: 3637 RVA: 0x0004B7AB File Offset: 0x000499AB
		public override bool MoveToAttribute(string localName, string namespaceURI)
		{
			return this._xmlreader.MoveToAttribute(localName, namespaceURI);
		}

		// Token: 0x06000E36 RID: 3638 RVA: 0x0004B7BA File Offset: 0x000499BA
		public override void MoveToAttribute(int i)
		{
			this._xmlreader.MoveToAttribute(i);
		}

		// Token: 0x06000E37 RID: 3639 RVA: 0x0004B7C8 File Offset: 0x000499C8
		public override bool MoveToFirstAttribute()
		{
			return this._xmlreader.MoveToFirstAttribute();
		}

		// Token: 0x06000E38 RID: 3640 RVA: 0x0004B7D5 File Offset: 0x000499D5
		public override bool MoveToNextAttribute()
		{
			return this._xmlreader.MoveToNextAttribute();
		}

		// Token: 0x06000E39 RID: 3641 RVA: 0x0004B7E2 File Offset: 0x000499E2
		public override bool MoveToElement()
		{
			return this._xmlreader.MoveToElement();
		}

		// Token: 0x06000E3A RID: 3642 RVA: 0x0004B7EF File Offset: 0x000499EF
		public override bool ReadAttributeValue()
		{
			return this._xmlreader.ReadAttributeValue();
		}

		// Token: 0x06000E3B RID: 3643 RVA: 0x0004B7FC File Offset: 0x000499FC
		public override bool Read()
		{
			return this._xmlreader.Read();
		}

		// Token: 0x17000268 RID: 616
		// (get) Token: 0x06000E3C RID: 3644 RVA: 0x0004B809 File Offset: 0x00049A09
		public override bool EOF
		{
			get
			{
				return this._xmlreader.EOF;
			}
		}

		// Token: 0x06000E3D RID: 3645 RVA: 0x0004B816 File Offset: 0x00049A16
		public override void Close()
		{
			this._xmlreader.Close();
		}

		// Token: 0x17000269 RID: 617
		// (get) Token: 0x06000E3E RID: 3646 RVA: 0x0004B823 File Offset: 0x00049A23
		public override ReadState ReadState
		{
			get
			{
				return this._xmlreader.ReadState;
			}
		}

		// Token: 0x06000E3F RID: 3647 RVA: 0x0004B830 File Offset: 0x00049A30
		public override void Skip()
		{
			this._xmlreader.Skip();
		}

		// Token: 0x1700026A RID: 618
		// (get) Token: 0x06000E40 RID: 3648 RVA: 0x0004B83D File Offset: 0x00049A3D
		public override XmlNameTable NameTable
		{
			get
			{
				return this._xmlreader.NameTable;
			}
		}

		// Token: 0x06000E41 RID: 3649 RVA: 0x0004B84A File Offset: 0x00049A4A
		public override string LookupNamespace(string prefix)
		{
			return this._xmlreader.LookupNamespace(prefix);
		}

		// Token: 0x1700026B RID: 619
		// (get) Token: 0x06000E42 RID: 3650 RVA: 0x0004B858 File Offset: 0x00049A58
		public override bool CanResolveEntity
		{
			get
			{
				return this._xmlreader.CanResolveEntity;
			}
		}

		// Token: 0x06000E43 RID: 3651 RVA: 0x0004B865 File Offset: 0x00049A65
		public override void ResolveEntity()
		{
			this._xmlreader.ResolveEntity();
		}

		// Token: 0x1700026C RID: 620
		// (get) Token: 0x06000E44 RID: 3652 RVA: 0x0004B872 File Offset: 0x00049A72
		public override bool CanReadBinaryContent
		{
			get
			{
				return this._xmlreader.CanReadBinaryContent;
			}
		}

		// Token: 0x06000E45 RID: 3653 RVA: 0x0004B87F File Offset: 0x00049A7F
		public override int ReadContentAsBase64(byte[] buffer, int index, int count)
		{
			return this._xmlreader.ReadContentAsBase64(buffer, index, count);
		}

		// Token: 0x06000E46 RID: 3654 RVA: 0x0004B88F File Offset: 0x00049A8F
		public override int ReadElementContentAsBase64(byte[] buffer, int index, int count)
		{
			return this._xmlreader.ReadElementContentAsBase64(buffer, index, count);
		}

		// Token: 0x06000E47 RID: 3655 RVA: 0x0004B89F File Offset: 0x00049A9F
		public override int ReadContentAsBinHex(byte[] buffer, int index, int count)
		{
			return this._xmlreader.ReadContentAsBinHex(buffer, index, count);
		}

		// Token: 0x06000E48 RID: 3656 RVA: 0x0004B8AF File Offset: 0x00049AAF
		public override int ReadElementContentAsBinHex(byte[] buffer, int index, int count)
		{
			return this._xmlreader.ReadElementContentAsBinHex(buffer, index, count);
		}

		// Token: 0x1700026D RID: 621
		// (get) Token: 0x06000E49 RID: 3657 RVA: 0x0004B8BF File Offset: 0x00049ABF
		public override bool CanReadValueChunk
		{
			get
			{
				return this._xmlreader.CanReadValueChunk;
			}
		}

		// Token: 0x06000E4A RID: 3658 RVA: 0x0004B8CC File Offset: 0x00049ACC
		public override string ReadString()
		{
			return this._xmlreader.ReadString();
		}

		// Token: 0x040009EE RID: 2542
		private XmlReader _xmlreader;
	}
}
