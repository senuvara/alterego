﻿using System;
using System.Runtime.InteropServices;

namespace System.Data
{
	// Token: 0x02000115 RID: 277
	internal static class SafeNativeMethods
	{
		// Token: 0x06000E4B RID: 3659 RVA: 0x0004B8D9 File Offset: 0x00049AD9
		internal static IntPtr LocalAlloc(IntPtr initialSize)
		{
			IntPtr intPtr = Marshal.AllocHGlobal(initialSize);
			SafeNativeMethods.ZeroMemory(intPtr, (int)initialSize);
			return intPtr;
		}

		// Token: 0x06000E4C RID: 3660 RVA: 0x0004B8ED File Offset: 0x00049AED
		internal static void LocalFree(IntPtr ptr)
		{
			Marshal.FreeHGlobal(ptr);
		}

		// Token: 0x06000E4D RID: 3661 RVA: 0x0004B8F5 File Offset: 0x00049AF5
		internal static void ZeroMemory(IntPtr ptr, int length)
		{
			Marshal.Copy(new byte[length], 0, ptr, length);
		}

		// Token: 0x06000E4E RID: 3662 RVA: 0x0004B905 File Offset: 0x00049B05
		internal static IntPtr GetProcAddress(IntPtr HModule, string funcName)
		{
			throw new PlatformNotSupportedException("SafeNativeMethods.GetProcAddress is not supported on non-Windows platforms");
		}
	}
}
