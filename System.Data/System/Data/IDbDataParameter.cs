﻿using System;

namespace System.Data
{
	/// <summary>Used by the Visual Basic .NET Data Designers to represent a parameter to a Command object, and optionally, its mapping to <see cref="T:System.Data.DataSet" /> columns.</summary>
	// Token: 0x020000CB RID: 203
	public interface IDbDataParameter : IDataParameter
	{
		/// <summary>Indicates the precision of numeric parameters.</summary>
		/// <returns>The maximum number of digits used to represent the Value property of a data provider Parameter object. The default value is 0, which indicates that a data provider sets the precision for Value.</returns>
		// Token: 0x17000216 RID: 534
		// (get) Token: 0x06000BB4 RID: 2996
		// (set) Token: 0x06000BB5 RID: 2997
		byte Precision { get; set; }

		/// <summary>Indicates the scale of numeric parameters.</summary>
		/// <returns>The number of decimal places to which <see cref="T:System.Data.OleDb.OleDbParameter.Value" /> is resolved. The default is 0.</returns>
		// Token: 0x17000217 RID: 535
		// (get) Token: 0x06000BB6 RID: 2998
		// (set) Token: 0x06000BB7 RID: 2999
		byte Scale { get; set; }

		/// <summary>The size of the parameter.</summary>
		/// <returns>The maximum size, in bytes, of the data within the column. The default value is inferred from the the parameter value.</returns>
		// Token: 0x17000218 RID: 536
		// (get) Token: 0x06000BB8 RID: 3000
		// (set) Token: 0x06000BB9 RID: 3001
		int Size { get; set; }
	}
}
