﻿using System;

namespace System.Data
{
	// Token: 0x020000AD RID: 173
	internal enum ValueType
	{
		// Token: 0x040006F0 RID: 1776
		Unknown = -1,
		// Token: 0x040006F1 RID: 1777
		Null,
		// Token: 0x040006F2 RID: 1778
		Bool,
		// Token: 0x040006F3 RID: 1779
		Numeric,
		// Token: 0x040006F4 RID: 1780
		Str,
		// Token: 0x040006F5 RID: 1781
		Float,
		// Token: 0x040006F6 RID: 1782
		Decimal,
		// Token: 0x040006F7 RID: 1783
		Object,
		// Token: 0x040006F8 RID: 1784
		Date
	}
}
