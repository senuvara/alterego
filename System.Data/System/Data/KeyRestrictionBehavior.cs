﻿using System;

namespace System.Data
{
	/// <summary>Identifies a list of connection string parameters identified by the <see langword="KeyRestrictions" /> property that are either allowed or not allowed.</summary>
	// Token: 0x020000D0 RID: 208
	public enum KeyRestrictionBehavior
	{
		/// <summary>Default. Identifies the only additional connection string parameters that are allowed.</summary>
		// Token: 0x040007D1 RID: 2001
		AllowOnly,
		/// <summary>Identifies additional connection string parameters that are not allowed.</summary>
		// Token: 0x040007D2 RID: 2002
		PreventUsage
	}
}
