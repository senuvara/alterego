﻿using System;
using System.ComponentModel;

namespace System.Data
{
	/// <summary>Marks a property, event, or extender with a description. Visual designers can display this description when referencing the member.</summary>
	// Token: 0x0200008A RID: 138
	[Obsolete("DataSysDescriptionAttribute has been deprecated.  http://go.microsoft.com/fwlink/?linkid=14202", false)]
	[AttributeUsage(AttributeTargets.All)]
	public class DataSysDescriptionAttribute : DescriptionAttribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Data.DataSysDescriptionAttribute" /> class using the specified description string.</summary>
		/// <param name="description">The description string. </param>
		// Token: 0x06000747 RID: 1863 RVA: 0x0001E806 File Offset: 0x0001CA06
		[Obsolete("DataSysDescriptionAttribute has been deprecated.  http://go.microsoft.com/fwlink/?linkid=14202", false)]
		public DataSysDescriptionAttribute(string description) : base(description)
		{
		}

		/// <summary>Gets the text for the description. </summary>
		/// <returns>The description string.</returns>
		// Token: 0x17000152 RID: 338
		// (get) Token: 0x06000748 RID: 1864 RVA: 0x0001E80F File Offset: 0x0001CA0F
		public override string Description
		{
			get
			{
				if (!this._replaced)
				{
					this._replaced = true;
					base.DescriptionValue = base.Description;
				}
				return base.Description;
			}
		}

		// Token: 0x040005D4 RID: 1492
		private bool _replaced;
	}
}
