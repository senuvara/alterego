﻿using System;
using System.Diagnostics.Tracing;
using System.Threading;

namespace System.Data
{
	// Token: 0x02000053 RID: 83
	[EventSource(Name = "System.Data.DataCommonEventSource")]
	internal class DataCommonEventSource : EventSource
	{
		// Token: 0x060002A0 RID: 672 RVA: 0x0000F11A File Offset: 0x0000D31A
		[Event(1, Level = EventLevel.Informational)]
		internal void Trace(string message)
		{
			base.WriteEvent(1, message);
		}

		// Token: 0x060002A1 RID: 673 RVA: 0x0000F124 File Offset: 0x0000D324
		[NonEvent]
		internal void Trace<T0>(string format, T0 arg0)
		{
			if (!DataCommonEventSource.Log.IsEnabled())
			{
				return;
			}
			this.Trace(string.Format(format, arg0));
		}

		// Token: 0x060002A2 RID: 674 RVA: 0x0000F145 File Offset: 0x0000D345
		[NonEvent]
		internal void Trace<T0, T1>(string format, T0 arg0, T1 arg1)
		{
			if (!DataCommonEventSource.Log.IsEnabled())
			{
				return;
			}
			this.Trace(string.Format(format, arg0, arg1));
		}

		// Token: 0x060002A3 RID: 675 RVA: 0x0000F16C File Offset: 0x0000D36C
		[NonEvent]
		internal void Trace<T0, T1, T2>(string format, T0 arg0, T1 arg1, T2 arg2)
		{
			if (!DataCommonEventSource.Log.IsEnabled())
			{
				return;
			}
			this.Trace(string.Format(format, arg0, arg1, arg2));
		}

		// Token: 0x060002A4 RID: 676 RVA: 0x0000F19C File Offset: 0x0000D39C
		[NonEvent]
		internal void Trace<T0, T1, T2, T3>(string format, T0 arg0, T1 arg1, T2 arg2, T3 arg3)
		{
			if (!DataCommonEventSource.Log.IsEnabled())
			{
				return;
			}
			this.Trace(string.Format(format, new object[]
			{
				arg0,
				arg1,
				arg2,
				arg3
			}));
		}

		// Token: 0x060002A5 RID: 677 RVA: 0x0000F1F0 File Offset: 0x0000D3F0
		[NonEvent]
		internal void Trace<T0, T1, T2, T3, T4>(string format, T0 arg0, T1 arg1, T2 arg2, T3 arg3, T4 arg4)
		{
			if (!DataCommonEventSource.Log.IsEnabled())
			{
				return;
			}
			this.Trace(string.Format(format, new object[]
			{
				arg0,
				arg1,
				arg2,
				arg3,
				arg4
			}));
		}

		// Token: 0x060002A6 RID: 678 RVA: 0x0000F24C File Offset: 0x0000D44C
		[NonEvent]
		internal void Trace<T0, T1, T2, T3, T4, T5, T6>(string format, T0 arg0, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6)
		{
			if (!DataCommonEventSource.Log.IsEnabled())
			{
				return;
			}
			this.Trace(string.Format(format, new object[]
			{
				arg0,
				arg1,
				arg2,
				arg3,
				arg4,
				arg5,
				arg6
			}));
		}

		// Token: 0x060002A7 RID: 679 RVA: 0x0000F2BC File Offset: 0x0000D4BC
		[Event(2, Level = EventLevel.Verbose)]
		internal long EnterScope(string message)
		{
			long num = 0L;
			if (DataCommonEventSource.Log.IsEnabled())
			{
				num = Interlocked.Increment(ref DataCommonEventSource.s_nextScopeId);
				base.WriteEvent(2, num, message);
			}
			return num;
		}

		// Token: 0x060002A8 RID: 680 RVA: 0x0000F2ED File Offset: 0x0000D4ED
		[NonEvent]
		internal long EnterScope<T1>(string format, T1 arg1)
		{
			if (!DataCommonEventSource.Log.IsEnabled())
			{
				return 0L;
			}
			return this.EnterScope(string.Format(format, arg1));
		}

		// Token: 0x060002A9 RID: 681 RVA: 0x0000F310 File Offset: 0x0000D510
		[NonEvent]
		internal long EnterScope<T1, T2>(string format, T1 arg1, T2 arg2)
		{
			if (!DataCommonEventSource.Log.IsEnabled())
			{
				return 0L;
			}
			return this.EnterScope(string.Format(format, arg1, arg2));
		}

		// Token: 0x060002AA RID: 682 RVA: 0x0000F339 File Offset: 0x0000D539
		[NonEvent]
		internal long EnterScope<T1, T2, T3>(string format, T1 arg1, T2 arg2, T3 arg3)
		{
			if (!DataCommonEventSource.Log.IsEnabled())
			{
				return 0L;
			}
			return this.EnterScope(string.Format(format, arg1, arg2, arg3));
		}

		// Token: 0x060002AB RID: 683 RVA: 0x0000F36C File Offset: 0x0000D56C
		[NonEvent]
		internal long EnterScope<T1, T2, T3, T4>(string format, T1 arg1, T2 arg2, T3 arg3, T4 arg4)
		{
			if (!DataCommonEventSource.Log.IsEnabled())
			{
				return 0L;
			}
			return this.EnterScope(string.Format(format, new object[]
			{
				arg1,
				arg2,
				arg3,
				arg4
			}));
		}

		// Token: 0x060002AC RID: 684 RVA: 0x0000F3C0 File Offset: 0x0000D5C0
		[Event(3, Level = EventLevel.Verbose)]
		internal void ExitScope(long scopeId)
		{
			base.WriteEvent(3, scopeId);
		}

		// Token: 0x060002AD RID: 685 RVA: 0x0000E215 File Offset: 0x0000C415
		public DataCommonEventSource()
		{
		}

		// Token: 0x060002AE RID: 686 RVA: 0x0000F3CA File Offset: 0x0000D5CA
		// Note: this type is marked as 'beforefieldinit'.
		static DataCommonEventSource()
		{
		}

		// Token: 0x040004F4 RID: 1268
		internal static readonly DataCommonEventSource Log = new DataCommonEventSource();

		// Token: 0x040004F5 RID: 1269
		private static long s_nextScopeId = 0L;

		// Token: 0x040004F6 RID: 1270
		private const int TraceEventId = 1;

		// Token: 0x040004F7 RID: 1271
		private const int EnterScopeId = 2;

		// Token: 0x040004F8 RID: 1272
		private const int ExitScopeId = 3;
	}
}
