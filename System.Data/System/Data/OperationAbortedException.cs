﻿using System;
using Unity;

namespace System.Data
{
	/// <summary>This exception is thrown when an ongoing operation is aborted by the user. </summary>
	// Token: 0x02000117 RID: 279
	[Serializable]
	public sealed class OperationAbortedException : SystemException
	{
		// Token: 0x06000E50 RID: 3664 RVA: 0x0004B91D File Offset: 0x00049B1D
		private OperationAbortedException(string message, Exception innerException) : base(message, innerException)
		{
			base.HResult = -2146232010;
		}

		// Token: 0x06000E51 RID: 3665 RVA: 0x0004B934 File Offset: 0x00049B34
		internal static OperationAbortedException Aborted(Exception inner)
		{
			OperationAbortedException result;
			if (inner == null)
			{
				result = new OperationAbortedException(SR.GetString("Operation aborted."), null);
			}
			else
			{
				result = new OperationAbortedException(SR.GetString("Operation aborted due to an exception (see InnerException for details)."), inner);
			}
			return result;
		}

		// Token: 0x06000E52 RID: 3666 RVA: 0x00010458 File Offset: 0x0000E658
		internal OperationAbortedException()
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
