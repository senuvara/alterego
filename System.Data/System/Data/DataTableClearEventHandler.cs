﻿using System;

namespace System.Data
{
	/// <summary>Represents the method that handles the <see cref="M:System.Data.DataTable.Clear" /> method.</summary>
	/// <param name="sender">The source of the event. </param>
	/// <param name="e">A <see cref="T:System.Data.DataTableClearEventArgs" /> that contains the event data.</param>
	// Token: 0x0200008F RID: 143
	// (Invoke) Token: 0x0600088D RID: 2189
	public delegate void DataTableClearEventHandler(object sender, DataTableClearEventArgs e);
}
