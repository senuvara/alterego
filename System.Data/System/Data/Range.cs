﻿using System;

namespace System.Data
{
	// Token: 0x020000DB RID: 219
	internal struct Range
	{
		// Token: 0x06000BE5 RID: 3045 RVA: 0x0003640C File Offset: 0x0003460C
		public Range(int min, int max)
		{
			if (min > max)
			{
				throw ExceptionBuilder.RangeArgument(min, max);
			}
			this._min = min;
			this._max = max;
			this._isNotNull = true;
		}

		// Token: 0x17000221 RID: 545
		// (get) Token: 0x06000BE6 RID: 3046 RVA: 0x0003642F File Offset: 0x0003462F
		public int Count
		{
			get
			{
				if (!this.IsNull)
				{
					return this._max - this._min + 1;
				}
				return 0;
			}
		}

		// Token: 0x17000222 RID: 546
		// (get) Token: 0x06000BE7 RID: 3047 RVA: 0x0003644A File Offset: 0x0003464A
		public bool IsNull
		{
			get
			{
				return !this._isNotNull;
			}
		}

		// Token: 0x17000223 RID: 547
		// (get) Token: 0x06000BE8 RID: 3048 RVA: 0x00036455 File Offset: 0x00034655
		public int Max
		{
			get
			{
				this.CheckNull();
				return this._max;
			}
		}

		// Token: 0x17000224 RID: 548
		// (get) Token: 0x06000BE9 RID: 3049 RVA: 0x00036463 File Offset: 0x00034663
		public int Min
		{
			get
			{
				this.CheckNull();
				return this._min;
			}
		}

		// Token: 0x06000BEA RID: 3050 RVA: 0x00036471 File Offset: 0x00034671
		internal void CheckNull()
		{
			if (this.IsNull)
			{
				throw ExceptionBuilder.NullRange();
			}
		}

		// Token: 0x040007F2 RID: 2034
		private int _min;

		// Token: 0x040007F3 RID: 2035
		private int _max;

		// Token: 0x040007F4 RID: 2036
		private bool _isNotNull;
	}
}
