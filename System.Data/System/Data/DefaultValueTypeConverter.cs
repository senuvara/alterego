﻿using System;
using System.ComponentModel;
using System.Globalization;

namespace System.Data
{
	// Token: 0x020000A2 RID: 162
	internal sealed class DefaultValueTypeConverter : StringConverter
	{
		// Token: 0x06000A05 RID: 2565 RVA: 0x0002D05C File Offset: 0x0002B25C
		public DefaultValueTypeConverter()
		{
		}

		// Token: 0x06000A06 RID: 2566 RVA: 0x0002D064 File Offset: 0x0002B264
		public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType)
		{
			if (destinationType == null)
			{
				throw new ArgumentNullException("destinationType");
			}
			if (destinationType == typeof(string))
			{
				if (value == null)
				{
					return "<null>";
				}
				if (value == DBNull.Value)
				{
					return "<DBNull>";
				}
			}
			return base.ConvertTo(context, culture, value, destinationType);
		}

		// Token: 0x06000A07 RID: 2567 RVA: 0x0002D0BC File Offset: 0x0002B2BC
		public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
		{
			if (value != null && value.GetType() == typeof(string))
			{
				string a = (string)value;
				if (string.Equals(a, "<null>", StringComparison.OrdinalIgnoreCase))
				{
					return null;
				}
				if (string.Equals(a, "<DBNull>", StringComparison.OrdinalIgnoreCase))
				{
					return DBNull.Value;
				}
			}
			return base.ConvertFrom(context, culture, value);
		}

		// Token: 0x040006A1 RID: 1697
		private const string NullString = "<null>";

		// Token: 0x040006A2 RID: 1698
		private const string DbNullString = "<DBNull>";
	}
}
