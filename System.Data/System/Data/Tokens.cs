﻿using System;

namespace System.Data
{
	// Token: 0x020000B1 RID: 177
	internal enum Tokens
	{
		// Token: 0x0400071F RID: 1823
		None,
		// Token: 0x04000720 RID: 1824
		Name,
		// Token: 0x04000721 RID: 1825
		Numeric,
		// Token: 0x04000722 RID: 1826
		Decimal,
		// Token: 0x04000723 RID: 1827
		Float,
		// Token: 0x04000724 RID: 1828
		BinaryConst,
		// Token: 0x04000725 RID: 1829
		StringConst,
		// Token: 0x04000726 RID: 1830
		Date,
		// Token: 0x04000727 RID: 1831
		ListSeparator,
		// Token: 0x04000728 RID: 1832
		LeftParen,
		// Token: 0x04000729 RID: 1833
		RightParen,
		// Token: 0x0400072A RID: 1834
		ZeroOp,
		// Token: 0x0400072B RID: 1835
		UnaryOp,
		// Token: 0x0400072C RID: 1836
		BinaryOp,
		// Token: 0x0400072D RID: 1837
		Child,
		// Token: 0x0400072E RID: 1838
		Parent,
		// Token: 0x0400072F RID: 1839
		Dot,
		// Token: 0x04000730 RID: 1840
		Unknown,
		// Token: 0x04000731 RID: 1841
		EOS
	}
}
