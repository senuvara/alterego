﻿using System;

namespace System.Data
{
	// Token: 0x02000107 RID: 263
	internal sealed class Keywords
	{
		// Token: 0x06000D99 RID: 3481 RVA: 0x00005C14 File Offset: 0x00003E14
		private Keywords()
		{
		}

		// Token: 0x040008EF RID: 2287
		internal const string DFF = "diffgr";

		// Token: 0x040008F0 RID: 2288
		internal const string DFFNS = "urn:schemas-microsoft-com:xml-diffgram-v1";

		// Token: 0x040008F1 RID: 2289
		internal const string DIFFGRAM = "diffgram";

		// Token: 0x040008F2 RID: 2290
		internal const string DIFFID = "id";

		// Token: 0x040008F3 RID: 2291
		internal const string DIFFPID = "parentId";

		// Token: 0x040008F4 RID: 2292
		internal const string HASCHANGES = "hasChanges";

		// Token: 0x040008F5 RID: 2293
		internal const string HASERRORS = "hasErrors";

		// Token: 0x040008F6 RID: 2294
		internal const string ROWORDER = "rowOrder";

		// Token: 0x040008F7 RID: 2295
		internal const string MSD_ERRORS = "errors";

		// Token: 0x040008F8 RID: 2296
		internal const string CHANGES = "changes";

		// Token: 0x040008F9 RID: 2297
		internal const string MODIFIED = "modified";

		// Token: 0x040008FA RID: 2298
		internal const string INSERTED = "inserted";

		// Token: 0x040008FB RID: 2299
		internal const string MSD = "msdata";

		// Token: 0x040008FC RID: 2300
		internal const string MSDNS = "urn:schemas-microsoft-com:xml-msdata";

		// Token: 0x040008FD RID: 2301
		internal const string MSD_ACCEPTREJECTRULE = "AcceptRejectRule";

		// Token: 0x040008FE RID: 2302
		internal const string MSD_ALLOWDBNULL = "AllowDBNull";

		// Token: 0x040008FF RID: 2303
		internal const string MSD_CHILD = "child";

		// Token: 0x04000900 RID: 2304
		internal const string MSD_CHILDKEY = "childkey";

		// Token: 0x04000901 RID: 2305
		internal const string MSD_CHILDTABLENS = "ChildTableNamespace";

		// Token: 0x04000902 RID: 2306
		internal const string MSD_COLUMNNAME = "ColumnName";

		// Token: 0x04000903 RID: 2307
		internal const string MSD_CONSTRAINTNAME = "ConstraintName";

		// Token: 0x04000904 RID: 2308
		internal const string MSD_CONSTRAINTONLY = "ConstraintOnly";

		// Token: 0x04000905 RID: 2309
		internal const string MSD_CASESENSITIVE = "CaseSensitive";

		// Token: 0x04000906 RID: 2310
		internal const string MSD_DATASETNAME = "DataSetName";

		// Token: 0x04000907 RID: 2311
		internal const string MSD_DATASETNAMESPACE = "DataSetNamespace";

		// Token: 0x04000908 RID: 2312
		internal const string MSD_DATATYPE = "DataType";

		// Token: 0x04000909 RID: 2313
		internal const string MSD_DEFAULTVALUE = "DefaultValue";

		// Token: 0x0400090A RID: 2314
		internal const string MSD_DELETERULE = "DeleteRule";

		// Token: 0x0400090B RID: 2315
		internal const string MSD_ERROR = "Error";

		// Token: 0x0400090C RID: 2316
		internal const string MSD_ISDATASET = "IsDataSet";

		// Token: 0x0400090D RID: 2317
		internal const string MSD_ISNESTED = "IsNested";

		// Token: 0x0400090E RID: 2318
		internal const string MSD_LOCALE = "Locale";

		// Token: 0x0400090F RID: 2319
		internal const string MSD_USECURRENTLOCALE = "UseCurrentLocale";

		// Token: 0x04000910 RID: 2320
		internal const string MSD_ORDINAL = "Ordinal";

		// Token: 0x04000911 RID: 2321
		internal const string MSD_PARENT = "parent";

		// Token: 0x04000912 RID: 2322
		internal const string MSD_PARENTKEY = "parentkey";

		// Token: 0x04000913 RID: 2323
		internal const string MSD_PRIMARYKEY = "PrimaryKey";

		// Token: 0x04000914 RID: 2324
		internal const string MSD_RELATION = "Relationship";

		// Token: 0x04000915 RID: 2325
		internal const string MSD_RELATIONNAME = "RelationName";

		// Token: 0x04000916 RID: 2326
		internal const string MSD_UPDATERULE = "UpdateRule";

		// Token: 0x04000917 RID: 2327
		internal const char MSD_KEYFIELDSEP = ' ';

		// Token: 0x04000918 RID: 2328
		internal const char MSD_KEYFIELDOLDSEP = '+';

		// Token: 0x04000919 RID: 2329
		internal const string MSD_REL_PREFIX = "rel_";

		// Token: 0x0400091A RID: 2330
		internal const string MSD_FK_PREFIX = "fk_";

		// Token: 0x0400091B RID: 2331
		internal const string MSD_MAINDATATABLE = "MainDataTable";

		// Token: 0x0400091C RID: 2332
		internal const string MSD_TABLENS = "TableNamespace";

		// Token: 0x0400091D RID: 2333
		internal const string MSD_PARENTTABLENS = "ParentTableNamespace";

		// Token: 0x0400091E RID: 2334
		internal const string MSD_INSTANCETYPE = "InstanceType";

		// Token: 0x0400091F RID: 2335
		internal const string MSD_EXCLUDESCHEMA = "ExcludeSchema";

		// Token: 0x04000920 RID: 2336
		internal const string MSD_INCLUDESCHEMA = "IncludeSchema";

		// Token: 0x04000921 RID: 2337
		internal const string MSD_FRAGMENTCOUNT = "schemafragmentcount";

		// Token: 0x04000922 RID: 2338
		internal const string MSD_SCHEMASERIALIZATIONMODE = "SchemaSerializationMode";

		// Token: 0x04000923 RID: 2339
		internal const string DTNS = "urn:schemas-microsoft-com:datatypes";

		// Token: 0x04000924 RID: 2340
		internal const string DT_TYPE = "type";

		// Token: 0x04000925 RID: 2341
		internal const string DT_VALUES = "values";

		// Token: 0x04000926 RID: 2342
		internal const string XDRNS = "urn:schemas-microsoft-com:xml-data";

		// Token: 0x04000927 RID: 2343
		internal const string XDR_ATTRIBUTE = "attribute";

		// Token: 0x04000928 RID: 2344
		internal const string XDR_ATTRIBUTETYPE = "AttributeType";

		// Token: 0x04000929 RID: 2345
		internal const string XDR_DATATYPE = "datatype";

		// Token: 0x0400092A RID: 2346
		internal const string XDR_DESCRIPTION = "description";

		// Token: 0x0400092B RID: 2347
		internal const string XDR_ELEMENT = "element";

		// Token: 0x0400092C RID: 2348
		internal const string XDR_ELEMENTTYPE = "ElementType";

		// Token: 0x0400092D RID: 2349
		internal const string XDR_GROUP = "group";

		// Token: 0x0400092E RID: 2350
		internal const string XDR_SCHEMA = "Schema";

		// Token: 0x0400092F RID: 2351
		internal const string XSDNS = "http://www.w3.org/2001/XMLSchema";

		// Token: 0x04000930 RID: 2352
		internal const string XSD_NS_START = "http://www.w3.org/";

		// Token: 0x04000931 RID: 2353
		internal const string XSD_XMLNS_NS = "http://www.w3.org/2000/xmlns/";

		// Token: 0x04000932 RID: 2354
		internal const string XSD_PREFIX = "xs";

		// Token: 0x04000933 RID: 2355
		internal const string XSD_PREFIXCOLON = "xs:";

		// Token: 0x04000934 RID: 2356
		internal const string XSD_ANNOTATION = "annotation";

		// Token: 0x04000935 RID: 2357
		internal const string XSD_APPINFO = "appinfo";

		// Token: 0x04000936 RID: 2358
		internal const string XSD_ATTRIBUTE = "attribute";

		// Token: 0x04000937 RID: 2359
		internal const string XSD_SIMPLETYPE = "simpleType";

		// Token: 0x04000938 RID: 2360
		internal const string XSD_ELEMENT = "element";

		// Token: 0x04000939 RID: 2361
		internal const string XSD_COMPLEXTYPE = "complexType";

		// Token: 0x0400093A RID: 2362
		internal const string XSD_SCHEMA = "schema";

		// Token: 0x0400093B RID: 2363
		internal const string XSD_PATTERN = "pattern";

		// Token: 0x0400093C RID: 2364
		internal const string XSD_LENGTH = "length";

		// Token: 0x0400093D RID: 2365
		internal const string XSD_MAXLENGTH = "maxLength";

		// Token: 0x0400093E RID: 2366
		internal const string XSD_MINLENGTH = "minLength";

		// Token: 0x0400093F RID: 2367
		internal const string XSD_ENUMERATION = "enumeration";

		// Token: 0x04000940 RID: 2368
		internal const string XSD_MININCLUSIVE = "minInclusive";

		// Token: 0x04000941 RID: 2369
		internal const string XSD_MINEXCLUSIVE = "minExclusive";

		// Token: 0x04000942 RID: 2370
		internal const string XSD_MAXINCLUSIVE = "maxInclusive";

		// Token: 0x04000943 RID: 2371
		internal const string XSD_MAXEXCLUSIVE = "maxExclusive";

		// Token: 0x04000944 RID: 2372
		internal const string XSD_NAMESPACE = "namespace";

		// Token: 0x04000945 RID: 2373
		internal const string XSD_NILLABLE = "nillable";

		// Token: 0x04000946 RID: 2374
		internal const string XSD_IMPORT = "import";

		// Token: 0x04000947 RID: 2375
		internal const string XSD_SELECTOR = "selector";

		// Token: 0x04000948 RID: 2376
		internal const string XSD_FIELD = "field";

		// Token: 0x04000949 RID: 2377
		internal const string XSD_UNIQUE = "unique";

		// Token: 0x0400094A RID: 2378
		internal const string XSD_KEY = "key";

		// Token: 0x0400094B RID: 2379
		internal const string XSD_KEYREF = "keyref";

		// Token: 0x0400094C RID: 2380
		internal const string XSD_DATATYPE = "datatype";

		// Token: 0x0400094D RID: 2381
		internal const string XSD_ALL = "all";

		// Token: 0x0400094E RID: 2382
		internal const string XSD_SEQUENCE = "sequence";

		// Token: 0x0400094F RID: 2383
		internal const string XSD_ENCODING = "encoding";

		// Token: 0x04000950 RID: 2384
		internal const string XSD_EXTENSION = "extension";

		// Token: 0x04000951 RID: 2385
		internal const string XSD_SIMPLECONTENT = "simpleContent";

		// Token: 0x04000952 RID: 2386
		internal const string XSD_XPATH = "xpath";

		// Token: 0x04000953 RID: 2387
		internal const string XSD_ATTRIBUTEFORMDEFAULT = "attributeFormDefault";

		// Token: 0x04000954 RID: 2388
		internal const string XSD_ELEMENTFORMDEFAULT = "elementFormDefault";

		// Token: 0x04000955 RID: 2389
		internal const string XSD_SCHEMALOCATION = "schemaLocation";

		// Token: 0x04000956 RID: 2390
		internal const string XSD_CHOICE = "choice";

		// Token: 0x04000957 RID: 2391
		internal const string XSD_RESTRICTION = "restriction";

		// Token: 0x04000958 RID: 2392
		internal const string XSD_ANYTYPE = "anyType";

		// Token: 0x04000959 RID: 2393
		internal const string XSINS = "http://www.w3.org/2001/XMLSchema-instance";

		// Token: 0x0400095A RID: 2394
		internal const string XSI_NIL = "nil";

		// Token: 0x0400095B RID: 2395
		internal const string XSI = "xsi";

		// Token: 0x0400095C RID: 2396
		internal const string XML_XMLNS = "http://www.w3.org/XML/1998/namespace";

		// Token: 0x0400095D RID: 2397
		internal const string UPDGNS = "urn:schemas-microsoft-com:xml-updategram";

		// Token: 0x0400095E RID: 2398
		internal const string UPDG = "updg";

		// Token: 0x0400095F RID: 2399
		internal const string SQL_SYNC = "sync";

		// Token: 0x04000960 RID: 2400
		internal const string SQL_BEFORE = "before";

		// Token: 0x04000961 RID: 2401
		internal const string SQL_AFTER = "after";

		// Token: 0x04000962 RID: 2402
		internal const string SQL_ID = "id";

		// Token: 0x04000963 RID: 2403
		internal const string SQL_UNCHANGED = "unchanged";

		// Token: 0x04000964 RID: 2404
		internal const string ATTRIBUTE = "attribute";

		// Token: 0x04000965 RID: 2405
		internal const string CONTENT = "content";

		// Token: 0x04000966 RID: 2406
		internal const string DEFAULT = "default";

		// Token: 0x04000967 RID: 2407
		internal const string XSDID = "id";

		// Token: 0x04000968 RID: 2408
		internal const string MINOCCURS = "minOccurs";

		// Token: 0x04000969 RID: 2409
		internal const string MAXOCCURS = "maxOccurs";

		// Token: 0x0400096A RID: 2410
		internal const string MODEL = "model";

		// Token: 0x0400096B RID: 2411
		internal const string NAME = "name";

		// Token: 0x0400096C RID: 2412
		internal const string NULLABLE = "nullable";

		// Token: 0x0400096D RID: 2413
		internal const string ORDER = "order";

		// Token: 0x0400096E RID: 2414
		internal const string REQUIRED = "required";

		// Token: 0x0400096F RID: 2415
		internal const string REF = "ref";

		// Token: 0x04000970 RID: 2416
		internal const string BASE = "base";

		// Token: 0x04000971 RID: 2417
		internal const string TARGETNAMESPACE = "targetNamespace";

		// Token: 0x04000972 RID: 2418
		internal const string TYPE = "type";

		// Token: 0x04000973 RID: 2419
		internal const string XMLNS = "xmlns";

		// Token: 0x04000974 RID: 2420
		internal const string XMLNS_XSD = "xmlns:xs";

		// Token: 0x04000975 RID: 2421
		internal const string XMLNS_XSI = "xmlns:xsi";

		// Token: 0x04000976 RID: 2422
		internal const string XMLNS_MSDATA = "xmlns:msdata";

		// Token: 0x04000977 RID: 2423
		internal const string XMLNS_MSPROP = "xmlns:msprop";

		// Token: 0x04000978 RID: 2424
		internal const string XMLNS_MSTNS = "xmlns:mstns";

		// Token: 0x04000979 RID: 2425
		internal const string MSTNS_PREFIX = "mstns:";

		// Token: 0x0400097A RID: 2426
		internal const string SPACE = "space";

		// Token: 0x0400097B RID: 2427
		internal const string PRESERVE = "preserve";

		// Token: 0x0400097C RID: 2428
		internal const string VALUE = "value";

		// Token: 0x0400097D RID: 2429
		internal const string REFER = "refer";

		// Token: 0x0400097E RID: 2430
		internal const string USE = "use";

		// Token: 0x0400097F RID: 2431
		internal const string PROHIBITED = "prohibited";

		// Token: 0x04000980 RID: 2432
		internal const string POSITIVEINFINITY = "INF";

		// Token: 0x04000981 RID: 2433
		internal const string NEGATIVEINFINITY = "-INF";

		// Token: 0x04000982 RID: 2434
		internal const string QUALIFIED = "qualified";

		// Token: 0x04000983 RID: 2435
		internal const string UNQUALIFIED = "unqualified";

		// Token: 0x04000984 RID: 2436
		internal const string APP = "app";

		// Token: 0x04000985 RID: 2437
		internal const string CLOSED = "closed";

		// Token: 0x04000986 RID: 2438
		internal const string CURRENT = "Current";

		// Token: 0x04000987 RID: 2439
		internal const string DOCUMENTELEMENT = "DocumentElement";

		// Token: 0x04000988 RID: 2440
		internal const string FALSE = "false";

		// Token: 0x04000989 RID: 2441
		internal const string FIXED = "fixed";

		// Token: 0x0400098A RID: 2442
		internal const string FORM = "form";

		// Token: 0x0400098B RID: 2443
		internal const string ENCODING = "encoding";

		// Token: 0x0400098C RID: 2444
		internal const string ELEMENTONLY = "elementOnly";

		// Token: 0x0400098D RID: 2445
		internal const string ELTONLY = "eltOnly";

		// Token: 0x0400098E RID: 2446
		internal const string EMPTY = "empty";

		// Token: 0x0400098F RID: 2447
		internal const string MANY = "many";

		// Token: 0x04000990 RID: 2448
		internal const string MIXED = "mixed";

		// Token: 0x04000991 RID: 2449
		internal const string NO = "no";

		// Token: 0x04000992 RID: 2450
		internal const string NOTATION = "notation";

		// Token: 0x04000993 RID: 2451
		internal const string OCCURS = "occurs";

		// Token: 0x04000994 RID: 2452
		internal const string ONE_OR_MORE = "oneormore";

		// Token: 0x04000995 RID: 2453
		internal const string ONE = "one";

		// Token: 0x04000996 RID: 2454
		internal const string ONE_DIGIT = "1";

		// Token: 0x04000997 RID: 2455
		internal const string ONCE = "once";

		// Token: 0x04000998 RID: 2456
		internal const string OPTIONAL = "optional";

		// Token: 0x04000999 RID: 2457
		internal const string OPEN = "open";

		// Token: 0x0400099A RID: 2458
		internal const string ORIGINAL = "Original";

		// Token: 0x0400099B RID: 2459
		internal const string RANGE = "range";

		// Token: 0x0400099C RID: 2460
		internal const string SEQ = "seq";

		// Token: 0x0400099D RID: 2461
		internal const string STAR = "*";

		// Token: 0x0400099E RID: 2462
		internal const string TRUE = "true";

		// Token: 0x0400099F RID: 2463
		internal const string TEXTONLY = "textOnly";

		// Token: 0x040009A0 RID: 2464
		internal const string VERSION = "version";

		// Token: 0x040009A1 RID: 2465
		internal const string XML = "xml";

		// Token: 0x040009A2 RID: 2466
		internal const string X_SCHEMA = "x-schema";

		// Token: 0x040009A3 RID: 2467
		internal const string YES = "yes";

		// Token: 0x040009A4 RID: 2468
		internal const string ZERO_DIGIT = "0";

		// Token: 0x040009A5 RID: 2469
		internal const string ZERO_OR_MORE = "unbounded";

		// Token: 0x040009A6 RID: 2470
		internal const string USEDATASETSCHEMAONLY = "UseDataSetSchemaOnly";

		// Token: 0x040009A7 RID: 2471
		internal const string UDTCOLUMNVALUEWRAPPED = "UDTColumnValueWrapped";

		// Token: 0x040009A8 RID: 2472
		internal const string TYPEINSTANCE = "Type";

		// Token: 0x040009A9 RID: 2473
		internal const string MSPROPNS = "urn:schemas-microsoft-com:xml-msprop";

		// Token: 0x040009AA RID: 2474
		internal const string WS_DATASETFULLQNAME = "system.data.dataset";

		// Token: 0x040009AB RID: 2475
		internal const string WS_VERSION = "WSDL_VERSION";
	}
}
