﻿using System;
using System.Runtime.CompilerServices;

namespace System.Data
{
	/// <summary>Provides data for the <see cref="E:System.Data.DataTable.ColumnChanging" /> event.</summary>
	// Token: 0x02000062 RID: 98
	public class DataColumnChangeEventArgs : EventArgs
	{
		// Token: 0x060003B1 RID: 945 RVA: 0x00012D15 File Offset: 0x00010F15
		internal DataColumnChangeEventArgs(DataRow row)
		{
			this.Row = row;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Data.DataColumnChangeEventArgs" /> class.</summary>
		/// <param name="row">The <see cref="T:System.Data.DataRow" /> of the column with the changing value. </param>
		/// <param name="column">The <see cref="T:System.Data.DataColumn" /> with the changing value. </param>
		/// <param name="value">The new value. </param>
		// Token: 0x060003B2 RID: 946 RVA: 0x00012D24 File Offset: 0x00010F24
		public DataColumnChangeEventArgs(DataRow row, DataColumn column, object value)
		{
			this.Row = row;
			this._column = column;
			this.ProposedValue = value;
		}

		/// <summary>Gets the <see cref="T:System.Data.DataColumn" /> with a changing value.</summary>
		/// <returns>The <see cref="T:System.Data.DataColumn" /> with a changing value.</returns>
		// Token: 0x170000EA RID: 234
		// (get) Token: 0x060003B3 RID: 947 RVA: 0x00012D41 File Offset: 0x00010F41
		public DataColumn Column
		{
			get
			{
				return this._column;
			}
		}

		/// <summary>Gets the <see cref="T:System.Data.DataRow" /> of the column with a changing value.</summary>
		/// <returns>The <see cref="T:System.Data.DataRow" /> of the column with a changing value.</returns>
		// Token: 0x170000EB RID: 235
		// (get) Token: 0x060003B4 RID: 948 RVA: 0x00012D49 File Offset: 0x00010F49
		public DataRow Row
		{
			[CompilerGenerated]
			get
			{
				return this.<Row>k__BackingField;
			}
		}

		/// <summary>Gets or sets the proposed new value for the column.</summary>
		/// <returns>The proposed value, of type <see cref="T:System.Object" />.</returns>
		// Token: 0x170000EC RID: 236
		// (get) Token: 0x060003B5 RID: 949 RVA: 0x00012D51 File Offset: 0x00010F51
		// (set) Token: 0x060003B6 RID: 950 RVA: 0x00012D59 File Offset: 0x00010F59
		public object ProposedValue
		{
			[CompilerGenerated]
			get
			{
				return this.<ProposedValue>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<ProposedValue>k__BackingField = value;
			}
		}

		// Token: 0x060003B7 RID: 951 RVA: 0x00012D62 File Offset: 0x00010F62
		internal void InitializeColumnChangeEvent(DataColumn column, object value)
		{
			this._column = column;
			this.ProposedValue = value;
		}

		// Token: 0x0400053F RID: 1343
		private DataColumn _column;

		// Token: 0x04000540 RID: 1344
		[CompilerGenerated]
		private readonly DataRow <Row>k__BackingField;

		// Token: 0x04000541 RID: 1345
		[CompilerGenerated]
		private object <ProposedValue>k__BackingField;
	}
}
