﻿using System;
using System.Runtime.CompilerServices;

namespace System.Data
{
	/// <summary>Provides additional information for the <see cref="E:System.Data.SqlClient.SqlCommand.StatementCompleted" /> event.</summary>
	// Token: 0x020000F8 RID: 248
	public sealed class StatementCompletedEventArgs : EventArgs
	{
		/// <summary>Creates a new instance of the <see cref="T:System.Data.StatementCompletedEventArgs" /> class.</summary>
		/// <param name="recordCount">Indicates the number of rows affected by the statement that caused the <see cref="E:System.Data.SqlClient.SqlCommand.StatementCompleted" />  event to occur.</param>
		// Token: 0x06000CF1 RID: 3313 RVA: 0x0003C218 File Offset: 0x0003A418
		public StatementCompletedEventArgs(int recordCount)
		{
			this.RecordCount = recordCount;
		}

		/// <summary>Indicates the number of rows affected by the statement that caused the <see cref="E:System.Data.SqlClient.SqlCommand.StatementCompleted" /> event to occur.</summary>
		/// <returns>The number of rows affected.</returns>
		// Token: 0x17000245 RID: 581
		// (get) Token: 0x06000CF2 RID: 3314 RVA: 0x0003C227 File Offset: 0x0003A427
		public int RecordCount
		{
			[CompilerGenerated]
			get
			{
				return this.<RecordCount>k__BackingField;
			}
		}

		// Token: 0x040008A0 RID: 2208
		[CompilerGenerated]
		private readonly int <RecordCount>k__BackingField;
	}
}
