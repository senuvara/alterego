﻿using System;
using System.Globalization;
using System.Runtime.CompilerServices;

namespace System.Data.Common
{
	// Token: 0x020002ED RID: 749
	internal static class ActivityCorrelator
	{
		// Token: 0x1700063F RID: 1599
		// (get) Token: 0x060025B8 RID: 9656 RVA: 0x000ABF8B File Offset: 0x000AA18B
		internal static ActivityCorrelator.ActivityId Current
		{
			get
			{
				if (ActivityCorrelator.t_tlsActivity == null)
				{
					ActivityCorrelator.t_tlsActivity = new ActivityCorrelator.ActivityId();
				}
				return new ActivityCorrelator.ActivityId(ActivityCorrelator.t_tlsActivity);
			}
		}

		// Token: 0x060025B9 RID: 9657 RVA: 0x000ABFA8 File Offset: 0x000AA1A8
		internal static ActivityCorrelator.ActivityId Next()
		{
			if (ActivityCorrelator.t_tlsActivity == null)
			{
				ActivityCorrelator.t_tlsActivity = new ActivityCorrelator.ActivityId();
			}
			ActivityCorrelator.t_tlsActivity.Increment();
			return new ActivityCorrelator.ActivityId(ActivityCorrelator.t_tlsActivity);
		}

		// Token: 0x040016A5 RID: 5797
		[ThreadStatic]
		private static ActivityCorrelator.ActivityId t_tlsActivity;

		// Token: 0x020002EE RID: 750
		internal class ActivityId
		{
			// Token: 0x17000640 RID: 1600
			// (get) Token: 0x060025BA RID: 9658 RVA: 0x000ABFCF File Offset: 0x000AA1CF
			// (set) Token: 0x060025BB RID: 9659 RVA: 0x000ABFD7 File Offset: 0x000AA1D7
			internal Guid Id
			{
				[CompilerGenerated]
				get
				{
					return this.<Id>k__BackingField;
				}
				[CompilerGenerated]
				private set
				{
					this.<Id>k__BackingField = value;
				}
			}

			// Token: 0x17000641 RID: 1601
			// (get) Token: 0x060025BC RID: 9660 RVA: 0x000ABFE0 File Offset: 0x000AA1E0
			// (set) Token: 0x060025BD RID: 9661 RVA: 0x000ABFE8 File Offset: 0x000AA1E8
			internal uint Sequence
			{
				[CompilerGenerated]
				get
				{
					return this.<Sequence>k__BackingField;
				}
				[CompilerGenerated]
				private set
				{
					this.<Sequence>k__BackingField = value;
				}
			}

			// Token: 0x060025BE RID: 9662 RVA: 0x000ABFF1 File Offset: 0x000AA1F1
			internal ActivityId()
			{
				this.Id = Guid.NewGuid();
				this.Sequence = 0U;
			}

			// Token: 0x060025BF RID: 9663 RVA: 0x000AC00B File Offset: 0x000AA20B
			internal ActivityId(ActivityCorrelator.ActivityId activity)
			{
				this.Id = activity.Id;
				this.Sequence = activity.Sequence;
			}

			// Token: 0x060025C0 RID: 9664 RVA: 0x000AC02C File Offset: 0x000AA22C
			internal void Increment()
			{
				uint sequence = this.Sequence + 1U;
				this.Sequence = sequence;
			}

			// Token: 0x060025C1 RID: 9665 RVA: 0x000AC049 File Offset: 0x000AA249
			public override string ToString()
			{
				return string.Format(CultureInfo.InvariantCulture, "{0}:{1}", this.Id, this.Sequence);
			}

			// Token: 0x040016A6 RID: 5798
			[CompilerGenerated]
			private Guid <Id>k__BackingField;

			// Token: 0x040016A7 RID: 5799
			[CompilerGenerated]
			private uint <Sequence>k__BackingField;
		}
	}
}
