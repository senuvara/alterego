﻿using System;
using System.Collections;
using System.Data.SqlTypes;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace System.Data.Common
{
	// Token: 0x020002DC RID: 732
	internal sealed class SqlSingleStorage : DataStorage
	{
		// Token: 0x060024F8 RID: 9464 RVA: 0x000A82E0 File Offset: 0x000A64E0
		public SqlSingleStorage(DataColumn column) : base(column, typeof(SqlSingle), SqlSingle.Null, SqlSingle.Null, StorageType.SqlSingle)
		{
		}

		// Token: 0x060024F9 RID: 9465 RVA: 0x000A830C File Offset: 0x000A650C
		public override object Aggregate(int[] records, AggregateType kind)
		{
			bool flag = false;
			try
			{
				switch (kind)
				{
				case AggregateType.Sum:
				{
					SqlSingle sqlSingle = 0f;
					foreach (int num in records)
					{
						if (!this.IsNull(num))
						{
							sqlSingle += this._values[num];
							flag = true;
						}
					}
					if (flag)
					{
						return sqlSingle;
					}
					return this._nullValue;
				}
				case AggregateType.Mean:
				{
					SqlDouble x = 0.0;
					int num2 = 0;
					foreach (int num3 in records)
					{
						if (!this.IsNull(num3))
						{
							x += this._values[num3].ToSqlDouble();
							num2++;
							flag = true;
						}
					}
					if (flag)
					{
						0f;
						return (x / (double)num2).ToSqlSingle();
					}
					return this._nullValue;
				}
				case AggregateType.Min:
				{
					SqlSingle sqlSingle2 = SqlSingle.MaxValue;
					foreach (int num4 in records)
					{
						if (!this.IsNull(num4))
						{
							if (SqlSingle.LessThan(this._values[num4], sqlSingle2).IsTrue)
							{
								sqlSingle2 = this._values[num4];
							}
							flag = true;
						}
					}
					if (flag)
					{
						return sqlSingle2;
					}
					return this._nullValue;
				}
				case AggregateType.Max:
				{
					SqlSingle sqlSingle3 = SqlSingle.MinValue;
					foreach (int num5 in records)
					{
						if (!this.IsNull(num5))
						{
							if (SqlSingle.GreaterThan(this._values[num5], sqlSingle3).IsTrue)
							{
								sqlSingle3 = this._values[num5];
							}
							flag = true;
						}
					}
					if (flag)
					{
						return sqlSingle3;
					}
					return this._nullValue;
				}
				case AggregateType.First:
					if (records.Length != 0)
					{
						return this._values[records[0]];
					}
					return null;
				case AggregateType.Count:
				{
					int num6 = 0;
					for (int l = 0; l < records.Length; l++)
					{
						if (!this.IsNull(records[l]))
						{
							num6++;
						}
					}
					return num6;
				}
				case AggregateType.Var:
				case AggregateType.StDev:
				{
					int num6 = 0;
					SqlDouble sqlDouble = 0.0;
					0.0;
					SqlDouble sqlDouble2 = 0.0;
					SqlDouble sqlDouble3 = 0.0;
					foreach (int num7 in records)
					{
						if (!this.IsNull(num7))
						{
							sqlDouble2 += this._values[num7].ToSqlDouble();
							sqlDouble3 += this._values[num7].ToSqlDouble() * this._values[num7].ToSqlDouble();
							num6++;
						}
					}
					if (num6 <= 1)
					{
						return this._nullValue;
					}
					sqlDouble = (double)num6 * sqlDouble3 - sqlDouble2 * sqlDouble2;
					SqlBoolean sqlBoolean = sqlDouble / (sqlDouble2 * sqlDouble2) < 1E-15;
					if (sqlBoolean ? sqlBoolean : (sqlBoolean | sqlDouble < 0.0))
					{
						sqlDouble = 0.0;
					}
					else
					{
						sqlDouble /= (double)(num6 * (num6 - 1));
					}
					if (kind == AggregateType.StDev)
					{
						return Math.Sqrt(sqlDouble.Value);
					}
					return sqlDouble;
				}
				}
			}
			catch (OverflowException)
			{
				throw ExprException.Overflow(typeof(SqlSingle));
			}
			throw ExceptionBuilder.AggregateException(kind, this._dataType);
		}

		// Token: 0x060024FA RID: 9466 RVA: 0x000A8754 File Offset: 0x000A6954
		public override int Compare(int recordNo1, int recordNo2)
		{
			return this._values[recordNo1].CompareTo(this._values[recordNo2]);
		}

		// Token: 0x060024FB RID: 9467 RVA: 0x000A8773 File Offset: 0x000A6973
		public override int CompareValueTo(int recordNo, object value)
		{
			return this._values[recordNo].CompareTo((SqlSingle)value);
		}

		// Token: 0x060024FC RID: 9468 RVA: 0x000A878C File Offset: 0x000A698C
		public override object ConvertValue(object value)
		{
			if (value != null)
			{
				return SqlConvert.ConvertToSqlSingle(value);
			}
			return this._nullValue;
		}

		// Token: 0x060024FD RID: 9469 RVA: 0x000A87A3 File Offset: 0x000A69A3
		public override void Copy(int recordNo1, int recordNo2)
		{
			this._values[recordNo2] = this._values[recordNo1];
		}

		// Token: 0x060024FE RID: 9470 RVA: 0x000A87BD File Offset: 0x000A69BD
		public override object Get(int record)
		{
			return this._values[record];
		}

		// Token: 0x060024FF RID: 9471 RVA: 0x000A87D0 File Offset: 0x000A69D0
		public override bool IsNull(int record)
		{
			return this._values[record].IsNull;
		}

		// Token: 0x06002500 RID: 9472 RVA: 0x000A87E3 File Offset: 0x000A69E3
		public override void Set(int record, object value)
		{
			this._values[record] = SqlConvert.ConvertToSqlSingle(value);
		}

		// Token: 0x06002501 RID: 9473 RVA: 0x000A87F8 File Offset: 0x000A69F8
		public override void SetCapacity(int capacity)
		{
			SqlSingle[] array = new SqlSingle[capacity];
			if (this._values != null)
			{
				Array.Copy(this._values, 0, array, 0, Math.Min(capacity, this._values.Length));
			}
			this._values = array;
		}

		// Token: 0x06002502 RID: 9474 RVA: 0x000A8838 File Offset: 0x000A6A38
		public override object ConvertXmlToObject(string s)
		{
			SqlSingle sqlSingle = default(SqlSingle);
			TextReader input = new StringReader("<col>" + s + "</col>");
			IXmlSerializable xmlSerializable = sqlSingle;
			using (XmlTextReader xmlTextReader = new XmlTextReader(input))
			{
				xmlSerializable.ReadXml(xmlTextReader);
			}
			return (SqlSingle)xmlSerializable;
		}

		// Token: 0x06002503 RID: 9475 RVA: 0x000A88A0 File Offset: 0x000A6AA0
		public override string ConvertObjectToXml(object value)
		{
			StringWriter stringWriter = new StringWriter(base.FormatProvider);
			using (XmlTextWriter xmlTextWriter = new XmlTextWriter(stringWriter))
			{
				((IXmlSerializable)value).WriteXml(xmlTextWriter);
			}
			return stringWriter.ToString();
		}

		// Token: 0x06002504 RID: 9476 RVA: 0x000A88F0 File Offset: 0x000A6AF0
		protected override object GetEmptyStorage(int recordCount)
		{
			return new SqlSingle[recordCount];
		}

		// Token: 0x06002505 RID: 9477 RVA: 0x000A88F8 File Offset: 0x000A6AF8
		protected override void CopyValue(int record, object store, BitArray nullbits, int storeIndex)
		{
			((SqlSingle[])store)[storeIndex] = this._values[record];
			nullbits.Set(storeIndex, this.IsNull(record));
		}

		// Token: 0x06002506 RID: 9478 RVA: 0x000A8922 File Offset: 0x000A6B22
		protected override void SetStorage(object store, BitArray nullbits)
		{
			this._values = (SqlSingle[])store;
		}

		// Token: 0x0400165B RID: 5723
		private SqlSingle[] _values;
	}
}
