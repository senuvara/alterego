﻿using System;
using System.Runtime.CompilerServices;

namespace System.Data.Common
{
	/// <summary>Represents a column within a data source.</summary>
	// Token: 0x020002AC RID: 684
	public abstract class DbColumn
	{
		/// <summary>Gets a nullable boolean value that indicates whether <see langword="DBNull" /> values are allowed in this column, or returns <see langword="null" /> if no value is set. Can be set to either <see langword="true" /> or <see langword="false" /> indicating whether <see langword="DBNull" /> values are allowed in this column, or <see langword="null" /> (<see langword="Nothing" /> in Visual Basic) when overridden in a derived class.</summary>
		/// <returns>Returns <see langword="true" /> if <see langword="DBNull" /> values are allowed in this column; otherwise, <see langword="false" />. If no value is set, returns a null reference (<see langword="Nothing" /> in Visual Basic).</returns>
		// Token: 0x170005B0 RID: 1456
		// (get) Token: 0x060021C6 RID: 8646 RVA: 0x0009CA43 File Offset: 0x0009AC43
		// (set) Token: 0x060021C7 RID: 8647 RVA: 0x0009CA4B File Offset: 0x0009AC4B
		public bool? AllowDBNull
		{
			[CompilerGenerated]
			get
			{
				return this.<AllowDBNull>k__BackingField;
			}
			[CompilerGenerated]
			protected set
			{
				this.<AllowDBNull>k__BackingField = value;
			}
		}

		/// <summary>Gets the catalog name associated with the data source; otherwise, <see langword="null" /> if no value is set. Can be set to either the catalog name or <see langword="null" /> when overridden in a derived class.</summary>
		/// <returns>The catalog name associated with the data source; otherwise, a null reference (<see langword="Nothing" /> in Visual Basic) if no value is set.</returns>
		// Token: 0x170005B1 RID: 1457
		// (get) Token: 0x060021C8 RID: 8648 RVA: 0x0009CA54 File Offset: 0x0009AC54
		// (set) Token: 0x060021C9 RID: 8649 RVA: 0x0009CA5C File Offset: 0x0009AC5C
		public string BaseCatalogName
		{
			[CompilerGenerated]
			get
			{
				return this.<BaseCatalogName>k__BackingField;
			}
			[CompilerGenerated]
			protected set
			{
				this.<BaseCatalogName>k__BackingField = value;
			}
		}

		/// <summary>Gets the base column name; otherwise, 
		/// 				<see langword="null" /> if no value is set. Can be set to either the column name 
		/// 			 or <see langword="null" /> when overridden in a derived class.
		/// 			</summary>
		/// <returns>The base column name; otherwise, a null reference (<see langword="Nothing" /> in Visual Basic) if no value is set.
		/// 			</returns>
		// Token: 0x170005B2 RID: 1458
		// (get) Token: 0x060021CA RID: 8650 RVA: 0x0009CA65 File Offset: 0x0009AC65
		// (set) Token: 0x060021CB RID: 8651 RVA: 0x0009CA6D File Offset: 0x0009AC6D
		public string BaseColumnName
		{
			[CompilerGenerated]
			get
			{
				return this.<BaseColumnName>k__BackingField;
			}
			[CompilerGenerated]
			protected set
			{
				this.<BaseColumnName>k__BackingField = value;
			}
		}

		/// <summary>Gets the schema name associated with the data source; otherwise, <see langword="null" /> if no value is set. Can be set to either the schema name or <see langword="null" /> when overridden in a derived class.</summary>
		/// <returns>The schema name associated with the data source; otherwise, a null reference (<see langword="Nothing" /> in Visual Basic) if no value is set.</returns>
		// Token: 0x170005B3 RID: 1459
		// (get) Token: 0x060021CC RID: 8652 RVA: 0x0009CA76 File Offset: 0x0009AC76
		// (set) Token: 0x060021CD RID: 8653 RVA: 0x0009CA7E File Offset: 0x0009AC7E
		public string BaseSchemaName
		{
			[CompilerGenerated]
			get
			{
				return this.<BaseSchemaName>k__BackingField;
			}
			[CompilerGenerated]
			protected set
			{
				this.<BaseSchemaName>k__BackingField = value;
			}
		}

		/// <summary>Gets the server name associated with the column; otherwise, <see langword="null" /> if no value is set. Can be set to either the server name or <see langword="null" /> when overridden in a derived class.</summary>
		/// <returns>The server name associated with the column; otherwise, a null reference (<see langword="Nothing" /> in Visual Basic) if no value is set.</returns>
		// Token: 0x170005B4 RID: 1460
		// (get) Token: 0x060021CE RID: 8654 RVA: 0x0009CA87 File Offset: 0x0009AC87
		// (set) Token: 0x060021CF RID: 8655 RVA: 0x0009CA8F File Offset: 0x0009AC8F
		public string BaseServerName
		{
			[CompilerGenerated]
			get
			{
				return this.<BaseServerName>k__BackingField;
			}
			[CompilerGenerated]
			protected set
			{
				this.<BaseServerName>k__BackingField = value;
			}
		}

		/// <summary>Gets the table name in the schema; otherwise, <see langword="null" /> if no value is set. Can be set to either the table name or <see langword="null" /> when overridden in a derived class.</summary>
		/// <returns>The table name in the schema; otherwise, a null reference (<see langword="Nothing" /> in Visual Basic) if no value is set.</returns>
		// Token: 0x170005B5 RID: 1461
		// (get) Token: 0x060021D0 RID: 8656 RVA: 0x0009CA98 File Offset: 0x0009AC98
		// (set) Token: 0x060021D1 RID: 8657 RVA: 0x0009CAA0 File Offset: 0x0009ACA0
		public string BaseTableName
		{
			[CompilerGenerated]
			get
			{
				return this.<BaseTableName>k__BackingField;
			}
			[CompilerGenerated]
			protected set
			{
				this.<BaseTableName>k__BackingField = value;
			}
		}

		/// <summary>Gets the name of the column. Can be set to the column name when overridden in a derived class.</summary>
		/// <returns>The name of the column.</returns>
		// Token: 0x170005B6 RID: 1462
		// (get) Token: 0x060021D2 RID: 8658 RVA: 0x0009CAA9 File Offset: 0x0009ACA9
		// (set) Token: 0x060021D3 RID: 8659 RVA: 0x0009CAB1 File Offset: 0x0009ACB1
		public string ColumnName
		{
			[CompilerGenerated]
			get
			{
				return this.<ColumnName>k__BackingField;
			}
			[CompilerGenerated]
			protected set
			{
				this.<ColumnName>k__BackingField = value;
			}
		}

		/// <summary>Gets the column position (ordinal) in the datasource row; otherwise, <see langword="null" /> if no value is set. Can be set to either an <see langword="int32" /> value to specify the column position or <see langword="null" /> when overridden in a derived class.</summary>
		/// <returns>An <see langword="int32" /> value for column ordinal; otherwise, a null reference (<see langword="Nothing" /> in Visual Basic) if no value is set.</returns>
		// Token: 0x170005B7 RID: 1463
		// (get) Token: 0x060021D4 RID: 8660 RVA: 0x0009CABA File Offset: 0x0009ACBA
		// (set) Token: 0x060021D5 RID: 8661 RVA: 0x0009CAC2 File Offset: 0x0009ACC2
		public int? ColumnOrdinal
		{
			[CompilerGenerated]
			get
			{
				return this.<ColumnOrdinal>k__BackingField;
			}
			[CompilerGenerated]
			protected set
			{
				this.<ColumnOrdinal>k__BackingField = value;
			}
		}

		/// <summary>Gets the column size; otherwise, 
		///               <see langword="null" /> if no value is set. Can be set to either an 
		///               <see langword="int32" /> value to specify the column size or 
		///               <see langword="null" /> when overridden in a derived class.
		///           </summary>
		/// <returns>An 
		///               <see langword="int32" /> value for column size; otherwise, a null reference (
		///               <see langword="Nothing" /> in Visual Basic) if no value is set.
		///           </returns>
		// Token: 0x170005B8 RID: 1464
		// (get) Token: 0x060021D6 RID: 8662 RVA: 0x0009CACB File Offset: 0x0009ACCB
		// (set) Token: 0x060021D7 RID: 8663 RVA: 0x0009CAD3 File Offset: 0x0009ACD3
		public int? ColumnSize
		{
			[CompilerGenerated]
			get
			{
				return this.<ColumnSize>k__BackingField;
			}
			[CompilerGenerated]
			protected set
			{
				this.<ColumnSize>k__BackingField = value;
			}
		}

		/// <summary>Gets a nullable boolean value that indicates whether this column is aliased, or returns <see langword="null" /> if no value is set. Can be set to either <see langword="true" /> or <see langword="false" /> indicating whether this column is aliased, or <see langword="null" /> (<see langword="Nothing" /> in Visual Basic) when overridden in a derived class.</summary>
		/// <returns>Returns <see langword="true" /> if this column is aliased; otherwise, <see langword="false" />. If no value is set, returns a null reference (<see langword="Nothing" /> in Visual Basic).</returns>
		// Token: 0x170005B9 RID: 1465
		// (get) Token: 0x060021D8 RID: 8664 RVA: 0x0009CADC File Offset: 0x0009ACDC
		// (set) Token: 0x060021D9 RID: 8665 RVA: 0x0009CAE4 File Offset: 0x0009ACE4
		public bool? IsAliased
		{
			[CompilerGenerated]
			get
			{
				return this.<IsAliased>k__BackingField;
			}
			[CompilerGenerated]
			protected set
			{
				this.<IsAliased>k__BackingField = value;
			}
		}

		/// <summary>Gets a nullable boolean value that indicates whether values in this column are automatically incremented, or returns <see langword="null" /> if no value is set. Can be set to either <see langword="true" /> or <see langword="false" /> indicating whether values in this column are automatically incremented, or <see langword="null" /> (<see langword="Nothing" /> in Visual Basic) when overridden in a derived class.</summary>
		/// <returns>Returns <see langword="true" /> if values in this column are automatically incremented; otherwise, <see langword="false" />. If no value is set, returns a null reference (<see langword="Nothing" /> in Visual Basic).</returns>
		// Token: 0x170005BA RID: 1466
		// (get) Token: 0x060021DA RID: 8666 RVA: 0x0009CAED File Offset: 0x0009ACED
		// (set) Token: 0x060021DB RID: 8667 RVA: 0x0009CAF5 File Offset: 0x0009ACF5
		public bool? IsAutoIncrement
		{
			[CompilerGenerated]
			get
			{
				return this.<IsAutoIncrement>k__BackingField;
			}
			[CompilerGenerated]
			protected set
			{
				this.<IsAutoIncrement>k__BackingField = value;
			}
		}

		/// <summary>Gets a nullable boolean value that indicates whether this column is an expression, or returns <see langword="null" /> if no value is set. Can be set to either <see langword="true" /> or <see langword="false" /> indicating whether this column is an expression, or <see langword="null" /> (<see langword="Nothing" /> in Visual Basic) when overridden in a derived class.</summary>
		/// <returns>Returns <see langword="true" /> if this column is an expression; otherwise, <see langword="false" />. If no value is set, returns a null reference (<see langword="Nothing" /> in Visual Basic).</returns>
		// Token: 0x170005BB RID: 1467
		// (get) Token: 0x060021DC RID: 8668 RVA: 0x0009CAFE File Offset: 0x0009ACFE
		// (set) Token: 0x060021DD RID: 8669 RVA: 0x0009CB06 File Offset: 0x0009AD06
		public bool? IsExpression
		{
			[CompilerGenerated]
			get
			{
				return this.<IsExpression>k__BackingField;
			}
			[CompilerGenerated]
			protected set
			{
				this.<IsExpression>k__BackingField = value;
			}
		}

		/// <summary>Gets a nullable boolean value that indicates whether this column is hidden, or returns <see langword="null" /> if no value is set. Can be set to either <see langword="true" /> or <see langword="false" /> indicating whether this column is hidden, or <see langword="null" /> (<see langword="Nothing" /> in Visual Basic) when overridden in a derived class.</summary>
		/// <returns>Returns <see langword="true" /> if this column is hidden; otherwise, <see langword="false" />. If no value is set, returns a null reference (<see langword="Nothing" /> in Visual Basic).</returns>
		// Token: 0x170005BC RID: 1468
		// (get) Token: 0x060021DE RID: 8670 RVA: 0x0009CB0F File Offset: 0x0009AD0F
		// (set) Token: 0x060021DF RID: 8671 RVA: 0x0009CB17 File Offset: 0x0009AD17
		public bool? IsHidden
		{
			[CompilerGenerated]
			get
			{
				return this.<IsHidden>k__BackingField;
			}
			[CompilerGenerated]
			protected set
			{
				this.<IsHidden>k__BackingField = value;
			}
		}

		/// <summary>Gets a nullable boolean value that indicates whether this column is an identity, or returns <see langword="null" /> if no value is set. Can be set to either <see langword="true" /> or <see langword="false" /> indicating whether this column is an identity, or <see langword="null" /> (<see langword="Nothing" /> in Visual Basic) when overridden in a derived class.</summary>
		/// <returns>Returns <see langword="true" /> if this column is an identity; otherwise, <see langword="false" />. If no value is set, returns a null reference (<see langword="Nothing" /> in Visual Basic).</returns>
		// Token: 0x170005BD RID: 1469
		// (get) Token: 0x060021E0 RID: 8672 RVA: 0x0009CB20 File Offset: 0x0009AD20
		// (set) Token: 0x060021E1 RID: 8673 RVA: 0x0009CB28 File Offset: 0x0009AD28
		public bool? IsIdentity
		{
			[CompilerGenerated]
			get
			{
				return this.<IsIdentity>k__BackingField;
			}
			[CompilerGenerated]
			protected set
			{
				this.<IsIdentity>k__BackingField = value;
			}
		}

		/// <summary>Gets a nullable boolean value that indicates whether this column is a key, or returns <see langword="null" /> if no value is set. Can be set to either <see langword="true" /> or <see langword="false" /> indicating whether this column is a key, or <see langword="null" /> (<see langword="Nothing" /> in Visual Basic) when overridden in a derived class.</summary>
		/// <returns>Returns <see langword="true" /> if this column is a key; otherwise, <see langword="false" />. If no value is set, returns a null reference (<see langword="Nothing" /> in Visual Basic).</returns>
		// Token: 0x170005BE RID: 1470
		// (get) Token: 0x060021E2 RID: 8674 RVA: 0x0009CB31 File Offset: 0x0009AD31
		// (set) Token: 0x060021E3 RID: 8675 RVA: 0x0009CB39 File Offset: 0x0009AD39
		public bool? IsKey
		{
			[CompilerGenerated]
			get
			{
				return this.<IsKey>k__BackingField;
			}
			[CompilerGenerated]
			protected set
			{
				this.<IsKey>k__BackingField = value;
			}
		}

		/// <summary>Gets a nullable boolean value that indicates whether this column contains long data, or returns <see langword="null" /> if no value is set. Can be set to either <see langword="true" /> or <see langword="false" /> indicating whether this column contains long data, or <see langword="null" /> (<see langword="Nothing" /> in Visual Basic) when overridden in a derived class.</summary>
		/// <returns>Returns <see langword="true" /> if this column contains long data; otherwise, <see langword="false" />. If no value is set, returns a null reference (<see langword="Nothing" /> in Visual Basic).</returns>
		// Token: 0x170005BF RID: 1471
		// (get) Token: 0x060021E4 RID: 8676 RVA: 0x0009CB42 File Offset: 0x0009AD42
		// (set) Token: 0x060021E5 RID: 8677 RVA: 0x0009CB4A File Offset: 0x0009AD4A
		public bool? IsLong
		{
			[CompilerGenerated]
			get
			{
				return this.<IsLong>k__BackingField;
			}
			[CompilerGenerated]
			protected set
			{
				this.<IsLong>k__BackingField = value;
			}
		}

		/// <summary>Gets a nullable boolean value that indicates whether this column is read-only, or returns <see langword="null" /> if no value is set. Can be set to either <see langword="true" /> or <see langword="false" /> indicating whether this column is read-only, or <see langword="null" /> (<see langword="Nothing" /> in Visual Basic) when overridden in a derived class.</summary>
		/// <returns>Returns <see langword="true" /> if this column is read-only; otherwise, <see langword="false" />. If no value is set, returns a null reference (<see langword="Nothing" /> in Visual Basic).</returns>
		// Token: 0x170005C0 RID: 1472
		// (get) Token: 0x060021E6 RID: 8678 RVA: 0x0009CB53 File Offset: 0x0009AD53
		// (set) Token: 0x060021E7 RID: 8679 RVA: 0x0009CB5B File Offset: 0x0009AD5B
		public bool? IsReadOnly
		{
			[CompilerGenerated]
			get
			{
				return this.<IsReadOnly>k__BackingField;
			}
			[CompilerGenerated]
			protected set
			{
				this.<IsReadOnly>k__BackingField = value;
			}
		}

		/// <summary>Gets a nullable boolean value that indicates whether a unique constraint applies to this column, or returns <see langword="null" /> if no value is set. Can be set to either <see langword="true" /> or <see langword="false" /> indicating whether a unique constraint applies to this column, or <see langword="null" /> (<see langword="Nothing" /> in Visual Basic) when overridden in a derived class.</summary>
		/// <returns>Returns <see langword="true" /> if a unique constraint applies to this column; otherwise, <see langword="false" />. If no value is set, returns a null reference (<see langword="Nothing" /> in Visual Basic).</returns>
		// Token: 0x170005C1 RID: 1473
		// (get) Token: 0x060021E8 RID: 8680 RVA: 0x0009CB64 File Offset: 0x0009AD64
		// (set) Token: 0x060021E9 RID: 8681 RVA: 0x0009CB6C File Offset: 0x0009AD6C
		public bool? IsUnique
		{
			[CompilerGenerated]
			get
			{
				return this.<IsUnique>k__BackingField;
			}
			[CompilerGenerated]
			protected set
			{
				this.<IsUnique>k__BackingField = value;
			}
		}

		/// <summary>Gets the numeric precision of the column data; otherwise, 
		///   				<see langword="null" /> if no value is set. Can be set to either an 
		///   				<see langword="int32" /> value to specify the numeric precision of the column data or 
		///   				<see langword="null" /> when overridden in a derived class.
		///   			</summary>
		/// <returns>An 
		///   				<see langword="int32" /> value that specifies the precision of the column data, if the data is numeric; otherwise, a null reference (
		///   				<see langword="Nothing" /> in Visual Basic) if no value is set.
		///   			</returns>
		// Token: 0x170005C2 RID: 1474
		// (get) Token: 0x060021EA RID: 8682 RVA: 0x0009CB75 File Offset: 0x0009AD75
		// (set) Token: 0x060021EB RID: 8683 RVA: 0x0009CB7D File Offset: 0x0009AD7D
		public int? NumericPrecision
		{
			[CompilerGenerated]
			get
			{
				return this.<NumericPrecision>k__BackingField;
			}
			[CompilerGenerated]
			protected set
			{
				this.<NumericPrecision>k__BackingField = value;
			}
		}

		/// <summary>Gets a nullable <see langword="int32" /> value that either returns <see langword="null" /> or the numeric scale of the column data. Can be set to either <see langword="null" /> or an <see langword="int32" /> value for the numeric scale of the column data when overridden in a derived class.</summary>
		/// <returns>A null reference (<see langword="Nothing" /> in Visual Basic) if no value is set; otherwise, a <see langword="int32" /> value that specifies the scale of the column data, if the data is numeric.</returns>
		// Token: 0x170005C3 RID: 1475
		// (get) Token: 0x060021EC RID: 8684 RVA: 0x0009CB86 File Offset: 0x0009AD86
		// (set) Token: 0x060021ED RID: 8685 RVA: 0x0009CB8E File Offset: 0x0009AD8E
		public int? NumericScale
		{
			[CompilerGenerated]
			get
			{
				return this.<NumericScale>k__BackingField;
			}
			[CompilerGenerated]
			protected set
			{
				this.<NumericScale>k__BackingField = value;
			}
		}

		/// <summary>Gets the assembly-qualified name of the <see cref="T:System.Type" /> object that represents the type of data in the column; otherwise, 
		///               <see langword="null" /> if no value is set. Can be set to either the assembly-qualified name or 
		///               <see langword="null" /> when overridden in a derived class.
		///           </summary>
		/// <returns>The assembly-qualified name of the <see cref="T:System.Type" /> object that represents the type of data in the column; otherwise, a null reference (<see langword="Nothing" /> in Visual Basic) if no value is set.
		///           </returns>
		// Token: 0x170005C4 RID: 1476
		// (get) Token: 0x060021EE RID: 8686 RVA: 0x0009CB97 File Offset: 0x0009AD97
		// (set) Token: 0x060021EF RID: 8687 RVA: 0x0009CB9F File Offset: 0x0009AD9F
		public string UdtAssemblyQualifiedName
		{
			[CompilerGenerated]
			get
			{
				return this.<UdtAssemblyQualifiedName>k__BackingField;
			}
			[CompilerGenerated]
			protected set
			{
				this.<UdtAssemblyQualifiedName>k__BackingField = value;
			}
		}

		/// <summary>Gets the type of data stored in the column. Can be set to a <see cref="T:System.Type" /> object that represents the type of data in the column when overridden in a derived class.</summary>
		/// <returns>A <see cref="T:System.Type" /> object that represents the type of data the column contains.</returns>
		// Token: 0x170005C5 RID: 1477
		// (get) Token: 0x060021F0 RID: 8688 RVA: 0x0009CBA8 File Offset: 0x0009ADA8
		// (set) Token: 0x060021F1 RID: 8689 RVA: 0x0009CBB0 File Offset: 0x0009ADB0
		public Type DataType
		{
			[CompilerGenerated]
			get
			{
				return this.<DataType>k__BackingField;
			}
			[CompilerGenerated]
			protected set
			{
				this.<DataType>k__BackingField = value;
			}
		}

		/// <summary>Gets the name of the data type; otherwise, <see langword="null" /> if no value is set. Can be set to either the data type name or <see langword="null" /> when overridden in a derived class.</summary>
		/// <returns>The name of the data type; otherwise, a null reference (<see langword="Nothing" /> in Visual Basic) if no value is set.</returns>
		// Token: 0x170005C6 RID: 1478
		// (get) Token: 0x060021F2 RID: 8690 RVA: 0x0009CBB9 File Offset: 0x0009ADB9
		// (set) Token: 0x060021F3 RID: 8691 RVA: 0x0009CBC1 File Offset: 0x0009ADC1
		public string DataTypeName
		{
			[CompilerGenerated]
			get
			{
				return this.<DataTypeName>k__BackingField;
			}
			[CompilerGenerated]
			protected set
			{
				this.<DataTypeName>k__BackingField = value;
			}
		}

		/// <summary>Gets the object based on the column property name.</summary>
		/// <param name="property">The column property name.</param>
		/// <returns>The object based on the column property name.</returns>
		// Token: 0x170005C7 RID: 1479
		public virtual object this[string property]
		{
			get
			{
				uint num = <PrivateImplementationDetails>.ComputeStringHash(property);
				if (num <= 2477638934U)
				{
					if (num <= 1067318116U)
					{
						if (num <= 687909556U)
						{
							if (num != 405521230U)
							{
								if (num == 687909556U)
								{
									if (property == "ColumnOrdinal")
									{
										return this.ColumnOrdinal;
									}
								}
							}
							else if (property == "DataTypeName")
							{
								return this.DataTypeName;
							}
						}
						else if (num != 720006947U)
						{
							if (num != 1005639113U)
							{
								if (num == 1067318116U)
								{
									if (property == "ColumnName")
									{
										return this.ColumnName;
									}
								}
							}
							else if (property == "IsHidden")
							{
								return this.IsHidden;
							}
						}
						else if (property == "IsLong")
						{
							return this.IsLong;
						}
					}
					else if (num <= 2215472237U)
					{
						if (num != 1154057342U)
						{
							if (num != 1309233724U)
							{
								if (num == 2215472237U)
								{
									if (property == "DataType")
									{
										return this.DataType;
									}
								}
							}
							else if (property == "IsKey")
							{
								return this.IsKey;
							}
						}
						else if (property == "ColumnSize")
						{
							return this.ColumnSize;
						}
					}
					else if (num != 2239129947U)
					{
						if (num != 2380251540U)
						{
							if (num == 2477638934U)
							{
								if (property == "IsUnique")
								{
									return this.IsUnique;
								}
							}
						}
						else if (property == "NumericPrecision")
						{
							return this.NumericPrecision;
						}
					}
					else if (property == "IsExpression")
					{
						return this.IsExpression;
					}
				}
				else if (num <= 3042527364U)
				{
					if (num <= 2711511624U)
					{
						if (num != 2504653387U)
						{
							if (num != 2586490225U)
							{
								if (num == 2711511624U)
								{
									if (property == "BaseServerName")
									{
										return this.BaseServerName;
									}
								}
							}
							else if (property == "UdtAssemblyQualifiedName")
							{
								return this.UdtAssemblyQualifiedName;
							}
						}
						else if (property == "IsIdentity")
						{
							return this.IsIdentity;
						}
					}
					else if (num != 2741140585U)
					{
						if (num != 2757192823U)
						{
							if (num == 3042527364U)
							{
								if (property == "BaseCatalogName")
								{
									return this.BaseCatalogName;
								}
							}
						}
						else if (property == "BaseTableName")
						{
							return this.BaseTableName;
						}
					}
					else if (property == "BaseColumnName")
					{
						return this.BaseColumnName;
					}
				}
				else if (num <= 3656290791U)
				{
					if (num != 3115085976U)
					{
						if (num != 3173893005U)
						{
							if (num == 3656290791U)
							{
								if (property == "IsReadOnly")
								{
									return this.IsReadOnly;
								}
							}
						}
						else if (property == "AllowDBNull")
						{
							return this.AllowDBNull;
						}
					}
					else if (property == "BaseSchemaName")
					{
						return this.BaseSchemaName;
					}
				}
				else if (num != 3912158903U)
				{
					if (num != 3938522122U)
					{
						if (num == 4233439846U)
						{
							if (property == "IsAliased")
							{
								return this.IsAliased;
							}
						}
					}
					else if (property == "NumericScale")
					{
						return this.NumericScale;
					}
				}
				else if (property == "IsAutoIncrement")
				{
					return this.IsAutoIncrement;
				}
				return null;
			}
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Data.Common.DbColumn" /> class.</summary>
		// Token: 0x060021F5 RID: 8693 RVA: 0x00005C14 File Offset: 0x00003E14
		protected DbColumn()
		{
		}

		// Token: 0x040015B2 RID: 5554
		[CompilerGenerated]
		private bool? <AllowDBNull>k__BackingField;

		// Token: 0x040015B3 RID: 5555
		[CompilerGenerated]
		private string <BaseCatalogName>k__BackingField;

		// Token: 0x040015B4 RID: 5556
		[CompilerGenerated]
		private string <BaseColumnName>k__BackingField;

		// Token: 0x040015B5 RID: 5557
		[CompilerGenerated]
		private string <BaseSchemaName>k__BackingField;

		// Token: 0x040015B6 RID: 5558
		[CompilerGenerated]
		private string <BaseServerName>k__BackingField;

		// Token: 0x040015B7 RID: 5559
		[CompilerGenerated]
		private string <BaseTableName>k__BackingField;

		// Token: 0x040015B8 RID: 5560
		[CompilerGenerated]
		private string <ColumnName>k__BackingField;

		// Token: 0x040015B9 RID: 5561
		[CompilerGenerated]
		private int? <ColumnOrdinal>k__BackingField;

		// Token: 0x040015BA RID: 5562
		[CompilerGenerated]
		private int? <ColumnSize>k__BackingField;

		// Token: 0x040015BB RID: 5563
		[CompilerGenerated]
		private bool? <IsAliased>k__BackingField;

		// Token: 0x040015BC RID: 5564
		[CompilerGenerated]
		private bool? <IsAutoIncrement>k__BackingField;

		// Token: 0x040015BD RID: 5565
		[CompilerGenerated]
		private bool? <IsExpression>k__BackingField;

		// Token: 0x040015BE RID: 5566
		[CompilerGenerated]
		private bool? <IsHidden>k__BackingField;

		// Token: 0x040015BF RID: 5567
		[CompilerGenerated]
		private bool? <IsIdentity>k__BackingField;

		// Token: 0x040015C0 RID: 5568
		[CompilerGenerated]
		private bool? <IsKey>k__BackingField;

		// Token: 0x040015C1 RID: 5569
		[CompilerGenerated]
		private bool? <IsLong>k__BackingField;

		// Token: 0x040015C2 RID: 5570
		[CompilerGenerated]
		private bool? <IsReadOnly>k__BackingField;

		// Token: 0x040015C3 RID: 5571
		[CompilerGenerated]
		private bool? <IsUnique>k__BackingField;

		// Token: 0x040015C4 RID: 5572
		[CompilerGenerated]
		private int? <NumericPrecision>k__BackingField;

		// Token: 0x040015C5 RID: 5573
		[CompilerGenerated]
		private int? <NumericScale>k__BackingField;

		// Token: 0x040015C6 RID: 5574
		[CompilerGenerated]
		private string <UdtAssemblyQualifiedName>k__BackingField;

		// Token: 0x040015C7 RID: 5575
		[CompilerGenerated]
		private Type <DataType>k__BackingField;

		// Token: 0x040015C8 RID: 5576
		[CompilerGenerated]
		private string <DataTypeName>k__BackingField;
	}
}
