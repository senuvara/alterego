﻿using System;
using System.Data.SqlClient;

namespace System.Data.Common
{
	// Token: 0x020002F0 RID: 752
	internal static class DbConnectionStringDefaults
	{
		// Token: 0x040016AA RID: 5802
		internal const ApplicationIntent ApplicationIntent = ApplicationIntent.ReadWrite;

		// Token: 0x040016AB RID: 5803
		internal const string ApplicationName = "Core .Net SqlClient Data Provider";

		// Token: 0x040016AC RID: 5804
		internal const string AttachDBFilename = "";

		// Token: 0x040016AD RID: 5805
		internal const int ConnectTimeout = 15;

		// Token: 0x040016AE RID: 5806
		internal const string CurrentLanguage = "";

		// Token: 0x040016AF RID: 5807
		internal const string DataSource = "";

		// Token: 0x040016B0 RID: 5808
		internal const bool Encrypt = false;

		// Token: 0x040016B1 RID: 5809
		internal const bool Enlist = true;

		// Token: 0x040016B2 RID: 5810
		internal const string FailoverPartner = "";

		// Token: 0x040016B3 RID: 5811
		internal const string InitialCatalog = "";

		// Token: 0x040016B4 RID: 5812
		internal const bool IntegratedSecurity = false;

		// Token: 0x040016B5 RID: 5813
		internal const int LoadBalanceTimeout = 0;

		// Token: 0x040016B6 RID: 5814
		internal const bool MultipleActiveResultSets = false;

		// Token: 0x040016B7 RID: 5815
		internal const bool MultiSubnetFailover = false;

		// Token: 0x040016B8 RID: 5816
		internal const int MaxPoolSize = 100;

		// Token: 0x040016B9 RID: 5817
		internal const int MinPoolSize = 0;

		// Token: 0x040016BA RID: 5818
		internal const int PacketSize = 8000;

		// Token: 0x040016BB RID: 5819
		internal const string Password = "";

		// Token: 0x040016BC RID: 5820
		internal const bool PersistSecurityInfo = false;

		// Token: 0x040016BD RID: 5821
		internal const bool Pooling = true;

		// Token: 0x040016BE RID: 5822
		internal const bool TrustServerCertificate = false;

		// Token: 0x040016BF RID: 5823
		internal const string TypeSystemVersion = "Latest";

		// Token: 0x040016C0 RID: 5824
		internal const string UserID = "";

		// Token: 0x040016C1 RID: 5825
		internal const bool UserInstance = false;

		// Token: 0x040016C2 RID: 5826
		internal const bool Replication = false;

		// Token: 0x040016C3 RID: 5827
		internal const string WorkstationID = "";

		// Token: 0x040016C4 RID: 5828
		internal const string TransactionBinding = "Implicit Unbind";

		// Token: 0x040016C5 RID: 5829
		internal const int ConnectRetryCount = 1;

		// Token: 0x040016C6 RID: 5830
		internal const int ConnectRetryInterval = 10;
	}
}
