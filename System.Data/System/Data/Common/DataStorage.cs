﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.ComponentModel;
using System.Data.SqlTypes;
using System.Numerics;
using System.Xml;
using System.Xml.Serialization;

namespace System.Data.Common
{
	// Token: 0x020002A6 RID: 678
	internal abstract class DataStorage
	{
		// Token: 0x06002133 RID: 8499 RVA: 0x0009ADDD File Offset: 0x00098FDD
		protected DataStorage(DataColumn column, Type type, object defaultValue, StorageType storageType) : this(column, type, defaultValue, DBNull.Value, false, storageType)
		{
		}

		// Token: 0x06002134 RID: 8500 RVA: 0x0009ADF0 File Offset: 0x00098FF0
		protected DataStorage(DataColumn column, Type type, object defaultValue, object nullValue, StorageType storageType) : this(column, type, defaultValue, nullValue, false, storageType)
		{
		}

		// Token: 0x06002135 RID: 8501 RVA: 0x0009AE00 File Offset: 0x00099000
		protected DataStorage(DataColumn column, Type type, object defaultValue, object nullValue, bool isICloneable, StorageType storageType)
		{
			this._column = column;
			this._table = column.Table;
			this._dataType = type;
			this._storageTypeCode = storageType;
			this._defaultValue = defaultValue;
			this._nullValue = nullValue;
			this._isCloneable = isICloneable;
			this._isCustomDefinedType = DataStorage.IsTypeCustomType(this._storageTypeCode);
			this._isStringType = (StorageType.String == this._storageTypeCode || StorageType.SqlString == this._storageTypeCode);
			this._isValueType = DataStorage.DetermineIfValueType(this._storageTypeCode, type);
		}

		// Token: 0x1700059F RID: 1439
		// (get) Token: 0x06002136 RID: 8502 RVA: 0x0009AE8C File Offset: 0x0009908C
		internal DataSetDateTime DateTimeMode
		{
			get
			{
				return this._column.DateTimeMode;
			}
		}

		// Token: 0x170005A0 RID: 1440
		// (get) Token: 0x06002137 RID: 8503 RVA: 0x0009AE99 File Offset: 0x00099099
		internal IFormatProvider FormatProvider
		{
			get
			{
				return this._table.FormatProvider;
			}
		}

		// Token: 0x06002138 RID: 8504 RVA: 0x0009AEA6 File Offset: 0x000990A6
		public virtual object Aggregate(int[] recordNos, AggregateType kind)
		{
			if (AggregateType.Count == kind)
			{
				return this.AggregateCount(recordNos);
			}
			return null;
		}

		// Token: 0x06002139 RID: 8505 RVA: 0x0009AEB8 File Offset: 0x000990B8
		public object AggregateCount(int[] recordNos)
		{
			int num = 0;
			for (int i = 0; i < recordNos.Length; i++)
			{
				if (!this._dbNullBits.Get(recordNos[i]))
				{
					num++;
				}
			}
			return num;
		}

		// Token: 0x0600213A RID: 8506 RVA: 0x0009AEF0 File Offset: 0x000990F0
		protected int CompareBits(int recordNo1, int recordNo2)
		{
			bool flag = this._dbNullBits.Get(recordNo1);
			bool flag2 = this._dbNullBits.Get(recordNo2);
			if (!(flag ^ flag2))
			{
				return 0;
			}
			if (flag)
			{
				return -1;
			}
			return 1;
		}

		// Token: 0x0600213B RID: 8507
		public abstract int Compare(int recordNo1, int recordNo2);

		// Token: 0x0600213C RID: 8508
		public abstract int CompareValueTo(int recordNo1, object value);

		// Token: 0x0600213D RID: 8509 RVA: 0x00005DA6 File Offset: 0x00003FA6
		public virtual object ConvertValue(object value)
		{
			return value;
		}

		// Token: 0x0600213E RID: 8510 RVA: 0x0009AF24 File Offset: 0x00099124
		protected void CopyBits(int srcRecordNo, int dstRecordNo)
		{
			this._dbNullBits.Set(dstRecordNo, this._dbNullBits.Get(srcRecordNo));
		}

		// Token: 0x0600213F RID: 8511
		public abstract void Copy(int recordNo1, int recordNo2);

		// Token: 0x06002140 RID: 8512
		public abstract object Get(int recordNo);

		// Token: 0x06002141 RID: 8513 RVA: 0x0009AF3E File Offset: 0x0009913E
		protected object GetBits(int recordNo)
		{
			if (this._dbNullBits.Get(recordNo))
			{
				return this._nullValue;
			}
			return this._defaultValue;
		}

		// Token: 0x06002142 RID: 8514 RVA: 0x0009AF5B File Offset: 0x0009915B
		public virtual int GetStringLength(int record)
		{
			return int.MaxValue;
		}

		// Token: 0x06002143 RID: 8515 RVA: 0x0009AF62 File Offset: 0x00099162
		protected bool HasValue(int recordNo)
		{
			return !this._dbNullBits.Get(recordNo);
		}

		// Token: 0x06002144 RID: 8516 RVA: 0x0009AF73 File Offset: 0x00099173
		public virtual bool IsNull(int recordNo)
		{
			return this._dbNullBits.Get(recordNo);
		}

		// Token: 0x06002145 RID: 8517
		public abstract void Set(int recordNo, object value);

		// Token: 0x06002146 RID: 8518 RVA: 0x0009AF81 File Offset: 0x00099181
		protected void SetNullBit(int recordNo, bool flag)
		{
			this._dbNullBits.Set(recordNo, flag);
		}

		// Token: 0x06002147 RID: 8519 RVA: 0x0009AF90 File Offset: 0x00099190
		public virtual void SetCapacity(int capacity)
		{
			if (this._dbNullBits == null)
			{
				this._dbNullBits = new BitArray(capacity);
				return;
			}
			this._dbNullBits.Length = capacity;
		}

		// Token: 0x06002148 RID: 8520
		public abstract object ConvertXmlToObject(string s);

		// Token: 0x06002149 RID: 8521 RVA: 0x0009AFB3 File Offset: 0x000991B3
		public virtual object ConvertXmlToObject(XmlReader xmlReader, XmlRootAttribute xmlAttrib)
		{
			return this.ConvertXmlToObject(xmlReader.Value);
		}

		// Token: 0x0600214A RID: 8522
		public abstract string ConvertObjectToXml(object value);

		// Token: 0x0600214B RID: 8523 RVA: 0x0009AFC1 File Offset: 0x000991C1
		public virtual void ConvertObjectToXml(object value, XmlWriter xmlWriter, XmlRootAttribute xmlAttrib)
		{
			xmlWriter.WriteString(this.ConvertObjectToXml(value));
		}

		// Token: 0x0600214C RID: 8524 RVA: 0x0009AFD0 File Offset: 0x000991D0
		public static DataStorage CreateStorage(DataColumn column, Type dataType, StorageType typeCode)
		{
			if (typeCode != StorageType.Empty || !(null != dataType))
			{
				switch (typeCode)
				{
				case StorageType.Empty:
					throw ExceptionBuilder.InvalidStorageType(TypeCode.Empty);
				case StorageType.DBNull:
					throw ExceptionBuilder.InvalidStorageType(TypeCode.DBNull);
				case StorageType.Boolean:
					return new BooleanStorage(column);
				case StorageType.Char:
					return new CharStorage(column);
				case StorageType.SByte:
					return new SByteStorage(column);
				case StorageType.Byte:
					return new ByteStorage(column);
				case StorageType.Int16:
					return new Int16Storage(column);
				case StorageType.UInt16:
					return new UInt16Storage(column);
				case StorageType.Int32:
					return new Int32Storage(column);
				case StorageType.UInt32:
					return new UInt32Storage(column);
				case StorageType.Int64:
					return new Int64Storage(column);
				case StorageType.UInt64:
					return new UInt64Storage(column);
				case StorageType.Single:
					return new SingleStorage(column);
				case StorageType.Double:
					return new DoubleStorage(column);
				case StorageType.Decimal:
					return new DecimalStorage(column);
				case StorageType.DateTime:
					return new DateTimeStorage(column);
				case StorageType.TimeSpan:
					return new TimeSpanStorage(column);
				case StorageType.String:
					return new StringStorage(column);
				case StorageType.Guid:
					return new ObjectStorage(column, dataType);
				case StorageType.ByteArray:
					return new ObjectStorage(column, dataType);
				case StorageType.CharArray:
					return new ObjectStorage(column, dataType);
				case StorageType.Type:
					return new ObjectStorage(column, dataType);
				case StorageType.DateTimeOffset:
					return new DateTimeOffsetStorage(column);
				case StorageType.BigInteger:
					return new BigIntegerStorage(column);
				case StorageType.Uri:
					return new ObjectStorage(column, dataType);
				case StorageType.SqlBinary:
					return new SqlBinaryStorage(column);
				case StorageType.SqlBoolean:
					return new SqlBooleanStorage(column);
				case StorageType.SqlByte:
					return new SqlByteStorage(column);
				case StorageType.SqlBytes:
					return new SqlBytesStorage(column);
				case StorageType.SqlChars:
					return new SqlCharsStorage(column);
				case StorageType.SqlDateTime:
					return new SqlDateTimeStorage(column);
				case StorageType.SqlDecimal:
					return new SqlDecimalStorage(column);
				case StorageType.SqlDouble:
					return new SqlDoubleStorage(column);
				case StorageType.SqlGuid:
					return new SqlGuidStorage(column);
				case StorageType.SqlInt16:
					return new SqlInt16Storage(column);
				case StorageType.SqlInt32:
					return new SqlInt32Storage(column);
				case StorageType.SqlInt64:
					return new SqlInt64Storage(column);
				case StorageType.SqlMoney:
					return new SqlMoneyStorage(column);
				case StorageType.SqlSingle:
					return new SqlSingleStorage(column);
				case StorageType.SqlString:
					return new SqlStringStorage(column);
				}
				return new ObjectStorage(column, dataType);
			}
			if (typeof(INullable).IsAssignableFrom(dataType))
			{
				return new SqlUdtStorage(column, dataType);
			}
			return new ObjectStorage(column, dataType);
		}

		// Token: 0x0600214D RID: 8525 RVA: 0x0009B1DC File Offset: 0x000993DC
		internal static StorageType GetStorageType(Type dataType)
		{
			for (int i = 0; i < DataStorage.s_storageClassType.Length; i++)
			{
				if (dataType == DataStorage.s_storageClassType[i])
				{
					return (StorageType)i;
				}
			}
			TypeCode typeCode = Type.GetTypeCode(dataType);
			if (TypeCode.Object != typeCode)
			{
				return (StorageType)typeCode;
			}
			return StorageType.Empty;
		}

		// Token: 0x0600214E RID: 8526 RVA: 0x0009B21A File Offset: 0x0009941A
		internal static Type GetTypeStorage(StorageType storageType)
		{
			return DataStorage.s_storageClassType[(int)storageType];
		}

		// Token: 0x0600214F RID: 8527 RVA: 0x0009B223 File Offset: 0x00099423
		internal static bool IsTypeCustomType(Type type)
		{
			return DataStorage.IsTypeCustomType(DataStorage.GetStorageType(type));
		}

		// Token: 0x06002150 RID: 8528 RVA: 0x0009B230 File Offset: 0x00099430
		internal static bool IsTypeCustomType(StorageType typeCode)
		{
			return StorageType.Object == typeCode || typeCode == StorageType.Empty || StorageType.CharArray == typeCode;
		}

		// Token: 0x06002151 RID: 8529 RVA: 0x0009B240 File Offset: 0x00099440
		internal static bool IsSqlType(StorageType storageType)
		{
			return StorageType.SqlBinary <= storageType;
		}

		// Token: 0x06002152 RID: 8530 RVA: 0x0009B24C File Offset: 0x0009944C
		public static bool IsSqlType(Type dataType)
		{
			for (int i = 26; i < DataStorage.s_storageClassType.Length; i++)
			{
				if (dataType == DataStorage.s_storageClassType[i])
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x06002153 RID: 8531 RVA: 0x0009B280 File Offset: 0x00099480
		private static bool DetermineIfValueType(StorageType typeCode, Type dataType)
		{
			bool result;
			switch (typeCode)
			{
			case StorageType.Boolean:
			case StorageType.Char:
			case StorageType.SByte:
			case StorageType.Byte:
			case StorageType.Int16:
			case StorageType.UInt16:
			case StorageType.Int32:
			case StorageType.UInt32:
			case StorageType.Int64:
			case StorageType.UInt64:
			case StorageType.Single:
			case StorageType.Double:
			case StorageType.Decimal:
			case StorageType.DateTime:
			case StorageType.TimeSpan:
			case StorageType.Guid:
			case StorageType.DateTimeOffset:
			case StorageType.BigInteger:
			case StorageType.SqlBinary:
			case StorageType.SqlBoolean:
			case StorageType.SqlByte:
			case StorageType.SqlDateTime:
			case StorageType.SqlDecimal:
			case StorageType.SqlDouble:
			case StorageType.SqlGuid:
			case StorageType.SqlInt16:
			case StorageType.SqlInt32:
			case StorageType.SqlInt64:
			case StorageType.SqlMoney:
			case StorageType.SqlSingle:
			case StorageType.SqlString:
				result = true;
				break;
			case StorageType.String:
			case StorageType.ByteArray:
			case StorageType.CharArray:
			case StorageType.Type:
			case StorageType.Uri:
			case StorageType.SqlBytes:
			case StorageType.SqlChars:
				result = false;
				break;
			default:
				result = dataType.IsValueType;
				break;
			}
			return result;
		}

		// Token: 0x06002154 RID: 8532 RVA: 0x0009B340 File Offset: 0x00099540
		internal static void ImplementsInterfaces(StorageType typeCode, Type dataType, out bool sqlType, out bool nullable, out bool xmlSerializable, out bool changeTracking, out bool revertibleChangeTracking)
		{
			if (DataStorage.IsSqlType(typeCode))
			{
				sqlType = true;
				nullable = true;
				changeTracking = false;
				revertibleChangeTracking = false;
				xmlSerializable = true;
				return;
			}
			if (typeCode != StorageType.Empty)
			{
				sqlType = false;
				nullable = false;
				changeTracking = false;
				revertibleChangeTracking = false;
				xmlSerializable = false;
				return;
			}
			Tuple<bool, bool, bool, bool> orAdd = DataStorage.s_typeImplementsInterface.GetOrAdd(dataType, DataStorage.s_inspectTypeForInterfaces);
			sqlType = false;
			nullable = orAdd.Item1;
			changeTracking = orAdd.Item2;
			revertibleChangeTracking = orAdd.Item3;
			xmlSerializable = orAdd.Item4;
		}

		// Token: 0x06002155 RID: 8533 RVA: 0x0009B3B8 File Offset: 0x000995B8
		private static Tuple<bool, bool, bool, bool> InspectTypeForInterfaces(Type dataType)
		{
			return new Tuple<bool, bool, bool, bool>(typeof(INullable).IsAssignableFrom(dataType), typeof(IChangeTracking).IsAssignableFrom(dataType), typeof(IRevertibleChangeTracking).IsAssignableFrom(dataType), typeof(IXmlSerializable).IsAssignableFrom(dataType));
		}

		// Token: 0x06002156 RID: 8534 RVA: 0x0009B40A File Offset: 0x0009960A
		internal static bool ImplementsINullableValue(StorageType typeCode, Type dataType)
		{
			return typeCode == StorageType.Empty && dataType.IsGenericType && dataType.GetGenericTypeDefinition() == typeof(Nullable<>);
		}

		// Token: 0x06002157 RID: 8535 RVA: 0x0009B42E File Offset: 0x0009962E
		public static bool IsObjectNull(object value)
		{
			return value == null || DBNull.Value == value || DataStorage.IsObjectSqlNull(value);
		}

		// Token: 0x06002158 RID: 8536 RVA: 0x0009B444 File Offset: 0x00099644
		public static bool IsObjectSqlNull(object value)
		{
			INullable nullable = value as INullable;
			return nullable != null && nullable.IsNull;
		}

		// Token: 0x06002159 RID: 8537 RVA: 0x0009B463 File Offset: 0x00099663
		internal object GetEmptyStorageInternal(int recordCount)
		{
			return this.GetEmptyStorage(recordCount);
		}

		// Token: 0x0600215A RID: 8538 RVA: 0x0009B46C File Offset: 0x0009966C
		internal void CopyValueInternal(int record, object store, BitArray nullbits, int storeIndex)
		{
			this.CopyValue(record, store, nullbits, storeIndex);
		}

		// Token: 0x0600215B RID: 8539 RVA: 0x0009B479 File Offset: 0x00099679
		internal void SetStorageInternal(object store, BitArray nullbits)
		{
			this.SetStorage(store, nullbits);
		}

		// Token: 0x0600215C RID: 8540
		protected abstract object GetEmptyStorage(int recordCount);

		// Token: 0x0600215D RID: 8541
		protected abstract void CopyValue(int record, object store, BitArray nullbits, int storeIndex);

		// Token: 0x0600215E RID: 8542
		protected abstract void SetStorage(object store, BitArray nullbits);

		// Token: 0x0600215F RID: 8543 RVA: 0x0009B483 File Offset: 0x00099683
		protected void SetNullStorage(BitArray nullbits)
		{
			this._dbNullBits = nullbits;
		}

		// Token: 0x06002160 RID: 8544 RVA: 0x0009B48C File Offset: 0x0009968C
		internal static Type GetType(string value)
		{
			Type type = Type.GetType(value);
			if (null == type && "System.Numerics.BigInteger" == value)
			{
				type = typeof(BigInteger);
			}
			ObjectStorage.VerifyIDynamicMetaObjectProvider(type);
			return type;
		}

		// Token: 0x06002161 RID: 8545 RVA: 0x0009B4C8 File Offset: 0x000996C8
		internal static string GetQualifiedName(Type type)
		{
			ObjectStorage.VerifyIDynamicMetaObjectProvider(type);
			return type.AssemblyQualifiedName;
		}

		// Token: 0x06002162 RID: 8546 RVA: 0x0009B4D8 File Offset: 0x000996D8
		// Note: this type is marked as 'beforefieldinit'.
		static DataStorage()
		{
		}

		// Token: 0x0400159B RID: 5531
		private static readonly Type[] s_storageClassType = new Type[]
		{
			null,
			typeof(object),
			typeof(DBNull),
			typeof(bool),
			typeof(char),
			typeof(sbyte),
			typeof(byte),
			typeof(short),
			typeof(ushort),
			typeof(int),
			typeof(uint),
			typeof(long),
			typeof(ulong),
			typeof(float),
			typeof(double),
			typeof(decimal),
			typeof(DateTime),
			typeof(TimeSpan),
			typeof(string),
			typeof(Guid),
			typeof(byte[]),
			typeof(char[]),
			typeof(Type),
			typeof(DateTimeOffset),
			typeof(BigInteger),
			typeof(Uri),
			typeof(SqlBinary),
			typeof(SqlBoolean),
			typeof(SqlByte),
			typeof(SqlBytes),
			typeof(SqlChars),
			typeof(SqlDateTime),
			typeof(SqlDecimal),
			typeof(SqlDouble),
			typeof(SqlGuid),
			typeof(SqlInt16),
			typeof(SqlInt32),
			typeof(SqlInt64),
			typeof(SqlMoney),
			typeof(SqlSingle),
			typeof(SqlString)
		};

		// Token: 0x0400159C RID: 5532
		internal readonly DataColumn _column;

		// Token: 0x0400159D RID: 5533
		internal readonly DataTable _table;

		// Token: 0x0400159E RID: 5534
		internal readonly Type _dataType;

		// Token: 0x0400159F RID: 5535
		internal readonly StorageType _storageTypeCode;

		// Token: 0x040015A0 RID: 5536
		private BitArray _dbNullBits;

		// Token: 0x040015A1 RID: 5537
		private readonly object _defaultValue;

		// Token: 0x040015A2 RID: 5538
		internal readonly object _nullValue;

		// Token: 0x040015A3 RID: 5539
		internal readonly bool _isCloneable;

		// Token: 0x040015A4 RID: 5540
		internal readonly bool _isCustomDefinedType;

		// Token: 0x040015A5 RID: 5541
		internal readonly bool _isStringType;

		// Token: 0x040015A6 RID: 5542
		internal readonly bool _isValueType;

		// Token: 0x040015A7 RID: 5543
		private static readonly Func<Type, Tuple<bool, bool, bool, bool>> s_inspectTypeForInterfaces = new Func<Type, Tuple<bool, bool, bool, bool>>(DataStorage.InspectTypeForInterfaces);

		// Token: 0x040015A8 RID: 5544
		private static readonly ConcurrentDictionary<Type, Tuple<bool, bool, bool, bool>> s_typeImplementsInterface = new ConcurrentDictionary<Type, Tuple<bool, bool, bool, bool>>();
	}
}
