﻿using System;

namespace System.Data.Common
{
	// Token: 0x020002F2 RID: 754
	internal static class DbConnectionStringSynonyms
	{
		// Token: 0x040016E9 RID: 5865
		internal const string Async = "async";

		// Token: 0x040016EA RID: 5866
		internal const string APP = "app";

		// Token: 0x040016EB RID: 5867
		internal const string EXTENDEDPROPERTIES = "extended properties";

		// Token: 0x040016EC RID: 5868
		internal const string INITIALFILENAME = "initial file name";

		// Token: 0x040016ED RID: 5869
		internal const string CONNECTIONTIMEOUT = "connection timeout";

		// Token: 0x040016EE RID: 5870
		internal const string TIMEOUT = "timeout";

		// Token: 0x040016EF RID: 5871
		internal const string LANGUAGE = "language";

		// Token: 0x040016F0 RID: 5872
		internal const string ADDR = "addr";

		// Token: 0x040016F1 RID: 5873
		internal const string ADDRESS = "address";

		// Token: 0x040016F2 RID: 5874
		internal const string SERVER = "server";

		// Token: 0x040016F3 RID: 5875
		internal const string NETWORKADDRESS = "network address";

		// Token: 0x040016F4 RID: 5876
		internal const string DATABASE = "database";

		// Token: 0x040016F5 RID: 5877
		internal const string TRUSTEDCONNECTION = "trusted_connection";

		// Token: 0x040016F6 RID: 5878
		internal const string ConnectionLifetime = "connection lifetime";

		// Token: 0x040016F7 RID: 5879
		internal const string NET = "net";

		// Token: 0x040016F8 RID: 5880
		internal const string NETWORK = "network";

		// Token: 0x040016F9 RID: 5881
		internal const string Pwd = "pwd";

		// Token: 0x040016FA RID: 5882
		internal const string PERSISTSECURITYINFO = "persistsecurityinfo";

		// Token: 0x040016FB RID: 5883
		internal const string UID = "uid";

		// Token: 0x040016FC RID: 5884
		internal const string User = "user";

		// Token: 0x040016FD RID: 5885
		internal const string WSID = "wsid";
	}
}
