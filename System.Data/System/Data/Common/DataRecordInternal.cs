﻿using System;
using System.ComponentModel;
using System.Data.ProviderBase;

namespace System.Data.Common
{
	// Token: 0x020002A3 RID: 675
	internal sealed class DataRecordInternal : DbDataRecord, ICustomTypeDescriptor
	{
		// Token: 0x0600210E RID: 8462 RVA: 0x0009AA2D File Offset: 0x00098C2D
		internal DataRecordInternal(SchemaInfo[] schemaInfo, object[] values, PropertyDescriptorCollection descriptors, FieldNameLookup fieldNameLookup)
		{
			this._schemaInfo = schemaInfo;
			this._values = values;
			this._propertyDescriptors = descriptors;
			this._fieldNameLookup = fieldNameLookup;
		}

		// Token: 0x1700059C RID: 1436
		// (get) Token: 0x0600210F RID: 8463 RVA: 0x0009AA52 File Offset: 0x00098C52
		public override int FieldCount
		{
			get
			{
				return this._schemaInfo.Length;
			}
		}

		// Token: 0x06002110 RID: 8464 RVA: 0x0009AA5C File Offset: 0x00098C5C
		public override int GetValues(object[] values)
		{
			if (values == null)
			{
				throw ADP.ArgumentNull("values");
			}
			int num = (values.Length < this._schemaInfo.Length) ? values.Length : this._schemaInfo.Length;
			for (int i = 0; i < num; i++)
			{
				values[i] = this._values[i];
			}
			return num;
		}

		// Token: 0x06002111 RID: 8465 RVA: 0x0009AAAA File Offset: 0x00098CAA
		public override string GetName(int i)
		{
			return this._schemaInfo[i].name;
		}

		// Token: 0x06002112 RID: 8466 RVA: 0x0009AABD File Offset: 0x00098CBD
		public override object GetValue(int i)
		{
			return this._values[i];
		}

		// Token: 0x06002113 RID: 8467 RVA: 0x0009AAC7 File Offset: 0x00098CC7
		public override string GetDataTypeName(int i)
		{
			return this._schemaInfo[i].typeName;
		}

		// Token: 0x06002114 RID: 8468 RVA: 0x0009AADA File Offset: 0x00098CDA
		public override Type GetFieldType(int i)
		{
			return this._schemaInfo[i].type;
		}

		// Token: 0x06002115 RID: 8469 RVA: 0x0009AAED File Offset: 0x00098CED
		public override int GetOrdinal(string name)
		{
			return this._fieldNameLookup.GetOrdinal(name);
		}

		// Token: 0x1700059D RID: 1437
		public override object this[int i]
		{
			get
			{
				return this.GetValue(i);
			}
		}

		// Token: 0x1700059E RID: 1438
		public override object this[string name]
		{
			get
			{
				return this.GetValue(this.GetOrdinal(name));
			}
		}

		// Token: 0x06002118 RID: 8472 RVA: 0x0009AB13 File Offset: 0x00098D13
		public override bool GetBoolean(int i)
		{
			return (bool)this._values[i];
		}

		// Token: 0x06002119 RID: 8473 RVA: 0x0009AB22 File Offset: 0x00098D22
		public override byte GetByte(int i)
		{
			return (byte)this._values[i];
		}

		// Token: 0x0600211A RID: 8474 RVA: 0x0009AB34 File Offset: 0x00098D34
		public override long GetBytes(int i, long dataIndex, byte[] buffer, int bufferIndex, int length)
		{
			int num = 0;
			byte[] array = (byte[])this._values[i];
			num = array.Length;
			if (dataIndex > 2147483647L)
			{
				throw ADP.InvalidSourceBufferIndex(num, dataIndex, "dataIndex");
			}
			int num2 = (int)dataIndex;
			if (buffer == null)
			{
				return (long)num;
			}
			try
			{
				if (num2 < num)
				{
					if (num2 + length > num)
					{
						num -= num2;
					}
					else
					{
						num = length;
					}
				}
				Array.Copy(array, num2, buffer, bufferIndex, num);
			}
			catch (Exception e) when (ADP.IsCatchableExceptionType(e))
			{
				num = array.Length;
				if (length < 0)
				{
					throw ADP.InvalidDataLength((long)length);
				}
				if (bufferIndex < 0 || bufferIndex >= buffer.Length)
				{
					throw ADP.InvalidDestinationBufferIndex(length, bufferIndex, "bufferIndex");
				}
				if (dataIndex < 0L || dataIndex >= (long)num)
				{
					throw ADP.InvalidSourceBufferIndex(length, dataIndex, "dataIndex");
				}
				if (num + bufferIndex > buffer.Length)
				{
					throw ADP.InvalidBufferSizeOrIndex(num, bufferIndex);
				}
			}
			return (long)num;
		}

		// Token: 0x0600211B RID: 8475 RVA: 0x0009AC18 File Offset: 0x00098E18
		public override char GetChar(int i)
		{
			return ((string)this._values[i])[0];
		}

		// Token: 0x0600211C RID: 8476 RVA: 0x0009AC30 File Offset: 0x00098E30
		public override long GetChars(int i, long dataIndex, char[] buffer, int bufferIndex, int length)
		{
			char[] array = ((string)this._values[i]).ToCharArray();
			int num = array.Length;
			if (dataIndex > 2147483647L)
			{
				throw ADP.InvalidSourceBufferIndex(num, dataIndex, "dataIndex");
			}
			int num2 = (int)dataIndex;
			if (buffer == null)
			{
				return (long)num;
			}
			try
			{
				if (num2 < num)
				{
					if (num2 + length > num)
					{
						num -= num2;
					}
					else
					{
						num = length;
					}
				}
				Array.Copy(array, num2, buffer, bufferIndex, num);
			}
			catch (Exception e) when (ADP.IsCatchableExceptionType(e))
			{
				num = array.Length;
				if (length < 0)
				{
					throw ADP.InvalidDataLength((long)length);
				}
				if (bufferIndex < 0 || bufferIndex >= buffer.Length)
				{
					throw ADP.InvalidDestinationBufferIndex(buffer.Length, bufferIndex, "bufferIndex");
				}
				if (num2 < 0 || num2 >= num)
				{
					throw ADP.InvalidSourceBufferIndex(num, dataIndex, "dataIndex");
				}
				if (num + bufferIndex > buffer.Length)
				{
					throw ADP.InvalidBufferSizeOrIndex(num, bufferIndex);
				}
			}
			return (long)num;
		}

		// Token: 0x0600211D RID: 8477 RVA: 0x0009AD18 File Offset: 0x00098F18
		public override Guid GetGuid(int i)
		{
			return (Guid)this._values[i];
		}

		// Token: 0x0600211E RID: 8478 RVA: 0x0009AD27 File Offset: 0x00098F27
		public override short GetInt16(int i)
		{
			return (short)this._values[i];
		}

		// Token: 0x0600211F RID: 8479 RVA: 0x0009AD36 File Offset: 0x00098F36
		public override int GetInt32(int i)
		{
			return (int)this._values[i];
		}

		// Token: 0x06002120 RID: 8480 RVA: 0x0009AD45 File Offset: 0x00098F45
		public override long GetInt64(int i)
		{
			return (long)this._values[i];
		}

		// Token: 0x06002121 RID: 8481 RVA: 0x0009AD54 File Offset: 0x00098F54
		public override float GetFloat(int i)
		{
			return (float)this._values[i];
		}

		// Token: 0x06002122 RID: 8482 RVA: 0x0009AD63 File Offset: 0x00098F63
		public override double GetDouble(int i)
		{
			return (double)this._values[i];
		}

		// Token: 0x06002123 RID: 8483 RVA: 0x0009AD72 File Offset: 0x00098F72
		public override string GetString(int i)
		{
			return (string)this._values[i];
		}

		// Token: 0x06002124 RID: 8484 RVA: 0x0009AD81 File Offset: 0x00098F81
		public override decimal GetDecimal(int i)
		{
			return (decimal)this._values[i];
		}

		// Token: 0x06002125 RID: 8485 RVA: 0x0009AD90 File Offset: 0x00098F90
		public override DateTime GetDateTime(int i)
		{
			return (DateTime)this._values[i];
		}

		// Token: 0x06002126 RID: 8486 RVA: 0x0009ADA0 File Offset: 0x00098FA0
		public override bool IsDBNull(int i)
		{
			object obj = this._values[i];
			return obj == null || Convert.IsDBNull(obj);
		}

		// Token: 0x06002127 RID: 8487 RVA: 0x0001A00E File Offset: 0x0001820E
		AttributeCollection ICustomTypeDescriptor.GetAttributes()
		{
			return new AttributeCollection(null);
		}

		// Token: 0x06002128 RID: 8488 RVA: 0x00004526 File Offset: 0x00002726
		string ICustomTypeDescriptor.GetClassName()
		{
			return null;
		}

		// Token: 0x06002129 RID: 8489 RVA: 0x00004526 File Offset: 0x00002726
		string ICustomTypeDescriptor.GetComponentName()
		{
			return null;
		}

		// Token: 0x0600212A RID: 8490 RVA: 0x00004526 File Offset: 0x00002726
		TypeConverter ICustomTypeDescriptor.GetConverter()
		{
			return null;
		}

		// Token: 0x0600212B RID: 8491 RVA: 0x00004526 File Offset: 0x00002726
		EventDescriptor ICustomTypeDescriptor.GetDefaultEvent()
		{
			return null;
		}

		// Token: 0x0600212C RID: 8492 RVA: 0x00004526 File Offset: 0x00002726
		PropertyDescriptor ICustomTypeDescriptor.GetDefaultProperty()
		{
			return null;
		}

		// Token: 0x0600212D RID: 8493 RVA: 0x00004526 File Offset: 0x00002726
		object ICustomTypeDescriptor.GetEditor(Type editorBaseType)
		{
			return null;
		}

		// Token: 0x0600212E RID: 8494 RVA: 0x0001A016 File Offset: 0x00018216
		EventDescriptorCollection ICustomTypeDescriptor.GetEvents()
		{
			return new EventDescriptorCollection(null);
		}

		// Token: 0x0600212F RID: 8495 RVA: 0x0001A016 File Offset: 0x00018216
		EventDescriptorCollection ICustomTypeDescriptor.GetEvents(Attribute[] attributes)
		{
			return new EventDescriptorCollection(null);
		}

		// Token: 0x06002130 RID: 8496 RVA: 0x0001A01E File Offset: 0x0001821E
		PropertyDescriptorCollection ICustomTypeDescriptor.GetProperties()
		{
			return ((ICustomTypeDescriptor)this).GetProperties(null);
		}

		// Token: 0x06002131 RID: 8497 RVA: 0x0009ADC1 File Offset: 0x00098FC1
		PropertyDescriptorCollection ICustomTypeDescriptor.GetProperties(Attribute[] attributes)
		{
			if (this._propertyDescriptors == null)
			{
				this._propertyDescriptors = new PropertyDescriptorCollection(null);
			}
			return this._propertyDescriptors;
		}

		// Token: 0x06002132 RID: 8498 RVA: 0x00005D82 File Offset: 0x00003F82
		object ICustomTypeDescriptor.GetPropertyOwner(PropertyDescriptor pd)
		{
			return this;
		}

		// Token: 0x0400156A RID: 5482
		private SchemaInfo[] _schemaInfo;

		// Token: 0x0400156B RID: 5483
		private object[] _values;

		// Token: 0x0400156C RID: 5484
		private PropertyDescriptorCollection _propertyDescriptors;

		// Token: 0x0400156D RID: 5485
		private FieldNameLookup _fieldNameLookup;
	}
}
