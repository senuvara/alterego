﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data.ProviderBase;

namespace System.Data.Common
{
	/// <summary>Exposes the <see cref="M:System.Collections.IEnumerable.GetEnumerator" /> method, which supports a simple iteration over a collection by a .NET Framework data provider.</summary>
	// Token: 0x020002B8 RID: 696
	public class DbEnumerator : IEnumerator
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Data.Common.DbEnumerator" /> class using the specified <see langword="DataReader" />.</summary>
		/// <param name="reader">The <see langword="DataReader" /> through which to iterate. </param>
		// Token: 0x0600233E RID: 9022 RVA: 0x0009FFEB File Offset: 0x0009E1EB
		public DbEnumerator(IDataReader reader)
		{
			if (reader == null)
			{
				throw ADP.ArgumentNull("reader");
			}
			this._reader = reader;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Data.Common.DbEnumerator" /> class using the specified <see langword="DataReader" />, and indicates whether to automatically close the <see langword="DataReader" /> after iterating through its data.</summary>
		/// <param name="reader">The <see langword="DataReader" /> through which to iterate. </param>
		/// <param name="closeReader">
		///       <see langword="true" /> to automatically close the <see langword="DataReader" /> after iterating through its data; otherwise, <see langword="false" />. </param>
		// Token: 0x0600233F RID: 9023 RVA: 0x000A0008 File Offset: 0x0009E208
		public DbEnumerator(IDataReader reader, bool closeReader)
		{
			if (reader == null)
			{
				throw ADP.ArgumentNull("reader");
			}
			this._reader = reader;
			this._closeReader = closeReader;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Data.Common.DbEnumerator" /> class with the give n data reader.</summary>
		/// <param name="reader">The DataReader through which to iterate.</param>
		// Token: 0x06002340 RID: 9024 RVA: 0x000A002C File Offset: 0x0009E22C
		public DbEnumerator(DbDataReader reader) : this(reader)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Data.Common.DbEnumerator" /> class using the specified reader and indicates whether to automatically close the reader after iterating through its data.</summary>
		/// <param name="reader">The DataReader through which to iterate.</param>
		/// <param name="closeReader">
		///       <see langword="true" /> to automatically close the DataReader after iterating through its data; otherwise, <see langword="false" />.</param>
		// Token: 0x06002341 RID: 9025 RVA: 0x000A0035 File Offset: 0x0009E235
		public DbEnumerator(DbDataReader reader, bool closeReader) : this(reader, closeReader)
		{
		}

		/// <summary>Gets the current element in the collection.</summary>
		/// <returns>The current element in the collection.</returns>
		/// <exception cref="T:System.InvalidOperationException">The enumerator is positioned before the first element of the collection or after the last element. </exception>
		// Token: 0x17000609 RID: 1545
		// (get) Token: 0x06002342 RID: 9026 RVA: 0x000A003F File Offset: 0x0009E23F
		public object Current
		{
			get
			{
				return this._current;
			}
		}

		/// <summary>Advances the enumerator to the next element of the collection.</summary>
		/// <returns>
		///     <see langword="true" /> if the enumerator was successfully advanced to the next element; <see langword="false" /> if the enumerator has passed the end of the collection.</returns>
		/// <exception cref="T:System.InvalidOperationException">The collection was modified after the enumerator was created. </exception>
		// Token: 0x06002343 RID: 9027 RVA: 0x000A0048 File Offset: 0x0009E248
		public bool MoveNext()
		{
			if (this._schemaInfo == null)
			{
				this.BuildSchemaInfo();
			}
			this._current = null;
			if (this._reader.Read())
			{
				object[] values = new object[this._schemaInfo.Length];
				this._reader.GetValues(values);
				this._current = new DataRecordInternal(this._schemaInfo, values, this._descriptors, this._fieldNameLookup);
				return true;
			}
			if (this._closeReader)
			{
				this._reader.Close();
			}
			return false;
		}

		/// <summary>Sets the enumerator to its initial position, which is before the first element in the collection.</summary>
		/// <exception cref="T:System.InvalidOperationException">The collection was modified after the enumerator was created. </exception>
		// Token: 0x06002344 RID: 9028 RVA: 0x0005CDBE File Offset: 0x0005AFBE
		[EditorBrowsable(EditorBrowsableState.Never)]
		public void Reset()
		{
			throw ADP.NotSupported();
		}

		// Token: 0x06002345 RID: 9029 RVA: 0x000A00C8 File Offset: 0x0009E2C8
		private void BuildSchemaInfo()
		{
			int fieldCount = this._reader.FieldCount;
			string[] array = new string[fieldCount];
			for (int i = 0; i < fieldCount; i++)
			{
				array[i] = this._reader.GetName(i);
			}
			ADP.BuildSchemaTableInfoTableNames(array);
			SchemaInfo[] array2 = new SchemaInfo[fieldCount];
			PropertyDescriptor[] array3 = new PropertyDescriptor[this._reader.FieldCount];
			for (int j = 0; j < array2.Length; j++)
			{
				SchemaInfo schemaInfo = default(SchemaInfo);
				schemaInfo.name = this._reader.GetName(j);
				schemaInfo.type = this._reader.GetFieldType(j);
				schemaInfo.typeName = this._reader.GetDataTypeName(j);
				array3[j] = new DbEnumerator.DbColumnDescriptor(j, array[j], schemaInfo.type);
				array2[j] = schemaInfo;
			}
			this._schemaInfo = array2;
			this._fieldNameLookup = new FieldNameLookup(this._reader, -1);
			this._descriptors = new PropertyDescriptorCollection(array3);
		}

		// Token: 0x040015E9 RID: 5609
		internal IDataReader _reader;

		// Token: 0x040015EA RID: 5610
		internal DbDataRecord _current;

		// Token: 0x040015EB RID: 5611
		internal SchemaInfo[] _schemaInfo;

		// Token: 0x040015EC RID: 5612
		internal PropertyDescriptorCollection _descriptors;

		// Token: 0x040015ED RID: 5613
		private FieldNameLookup _fieldNameLookup;

		// Token: 0x040015EE RID: 5614
		private bool _closeReader;

		// Token: 0x020002B9 RID: 697
		private sealed class DbColumnDescriptor : PropertyDescriptor
		{
			// Token: 0x06002346 RID: 9030 RVA: 0x000A01C2 File Offset: 0x0009E3C2
			internal DbColumnDescriptor(int ordinal, string name, Type type) : base(name, null)
			{
				this._ordinal = ordinal;
				this._type = type;
			}

			// Token: 0x1700060A RID: 1546
			// (get) Token: 0x06002347 RID: 9031 RVA: 0x000A01DA File Offset: 0x0009E3DA
			public override Type ComponentType
			{
				get
				{
					return typeof(IDataRecord);
				}
			}

			// Token: 0x1700060B RID: 1547
			// (get) Token: 0x06002348 RID: 9032 RVA: 0x0000EF1B File Offset: 0x0000D11B
			public override bool IsReadOnly
			{
				get
				{
					return true;
				}
			}

			// Token: 0x1700060C RID: 1548
			// (get) Token: 0x06002349 RID: 9033 RVA: 0x000A01E6 File Offset: 0x0009E3E6
			public override Type PropertyType
			{
				get
				{
					return this._type;
				}
			}

			// Token: 0x0600234A RID: 9034 RVA: 0x000061C5 File Offset: 0x000043C5
			public override bool CanResetValue(object component)
			{
				return false;
			}

			// Token: 0x0600234B RID: 9035 RVA: 0x000A01EE File Offset: 0x0009E3EE
			public override object GetValue(object component)
			{
				return ((IDataRecord)component)[this._ordinal];
			}

			// Token: 0x0600234C RID: 9036 RVA: 0x0005CDBE File Offset: 0x0005AFBE
			public override void ResetValue(object component)
			{
				throw ADP.NotSupported();
			}

			// Token: 0x0600234D RID: 9037 RVA: 0x0005CDBE File Offset: 0x0005AFBE
			public override void SetValue(object component, object value)
			{
				throw ADP.NotSupported();
			}

			// Token: 0x0600234E RID: 9038 RVA: 0x000061C5 File Offset: 0x000043C5
			public override bool ShouldSerializeValue(object component)
			{
				return false;
			}

			// Token: 0x040015EF RID: 5615
			private int _ordinal;

			// Token: 0x040015F0 RID: 5616
			private Type _type;
		}
	}
}
