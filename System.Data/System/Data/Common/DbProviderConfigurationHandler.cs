﻿using System;
using System.Configuration;
using System.Xml;
using Unity;

namespace System.Data.Common
{
	/// <summary>This class can be used by any provider to support a provider-specific configuration section.</summary>
	// Token: 0x02000389 RID: 905
	public class DbProviderConfigurationHandler : IConfigurationSectionHandler
	{
		/// <summary>This class can be used by any provider to support a provider-specific configuration section.</summary>
		// Token: 0x06002B47 RID: 11079 RVA: 0x00010458 File Offset: 0x0000E658
		public DbProviderConfigurationHandler()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Creates a new <see cref="System.Collections.Specialized.NameValueCollection" /> expression.</summary>
		/// <param name="parent">This type supports the .NET Framework infrastructure and is not intended to be used directly from your code.</param>
		/// <param name="configContext">This type supports the .NET Framework infrastructure and is not intended to be used directly from your code.</param>
		/// <param name="section">This type supports the .NET Framework infrastructure and is not intended to be used directly from your code.</param>
		/// <returns>The new expression.</returns>
		// Token: 0x06002B48 RID: 11080 RVA: 0x00051759 File Offset: 0x0004F959
		public virtual object Create(object parent, object configContext, XmlNode section)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}
	}
}
