﻿using System;
using System.ComponentModel;
using System.Data.ProviderBase;
using System.Diagnostics;
using System.Globalization;
using System.Reflection;
using System.Threading;

namespace System.Data.Common
{
	/// <summary>Represents a set of SQL commands and a database connection that are used to fill the <see cref="T:System.Data.DataSet" /> and update the data source.</summary>
	// Token: 0x0200029E RID: 670
	public class DataAdapter : Component, IDataAdapter
	{
		// Token: 0x06002092 RID: 8338 RVA: 0x00005E03 File Offset: 0x00004003
		[Conditional("DEBUG")]
		private void AssertReaderHandleFieldCount(DataReaderContainer readerHandler)
		{
		}

		// Token: 0x06002093 RID: 8339 RVA: 0x00005E03 File Offset: 0x00004003
		[Conditional("DEBUG")]
		private void AssertSchemaMapping(SchemaMapping mapping)
		{
		}

		/// <summary>Initializes a new instance of a <see cref="T:System.Data.Common.DataAdapter" /> class.</summary>
		// Token: 0x06002094 RID: 8340 RVA: 0x000995A0 File Offset: 0x000977A0
		protected DataAdapter()
		{
			GC.SuppressFinalize(this);
		}

		/// <summary>Initializes a new instance of a <see cref="T:System.Data.Common.DataAdapter" /> class from an existing object of the same type.</summary>
		/// <param name="from">A <see cref="T:System.Data.Common.DataAdapter" /> object used to create the new <see cref="T:System.Data.Common.DataAdapter" />. </param>
		// Token: 0x06002095 RID: 8341 RVA: 0x000995EC File Offset: 0x000977EC
		protected DataAdapter(DataAdapter from)
		{
			this.CloneFrom(from);
		}

		/// <summary>Gets or sets a value indicating whether <see cref="M:System.Data.DataRow.AcceptChanges" /> is called on a <see cref="T:System.Data.DataRow" /> after it is added to the <see cref="T:System.Data.DataTable" /> during any of the Fill operations.</summary>
		/// <returns>
		///     <see langword="true" /> if <see cref="M:System.Data.DataRow.AcceptChanges" /> is called on the <see cref="T:System.Data.DataRow" />; otherwise <see langword="false" />. The default is <see langword="true" />.</returns>
		// Token: 0x17000585 RID: 1413
		// (get) Token: 0x06002096 RID: 8342 RVA: 0x00099639 File Offset: 0x00097839
		// (set) Token: 0x06002097 RID: 8343 RVA: 0x00099641 File Offset: 0x00097841
		[DefaultValue(true)]
		public bool AcceptChangesDuringFill
		{
			get
			{
				return this._acceptChangesDuringFill;
			}
			set
			{
				this._acceptChangesDuringFill = value;
			}
		}

		/// <summary>Determines whether the <see cref="P:System.Data.Common.DataAdapter.AcceptChangesDuringFill" /> property should be persisted.</summary>
		/// <returns>
		///     <see langword="true" /> if the <see cref="P:System.Data.Common.DataAdapter.AcceptChangesDuringFill" /> property is persisted; otherwise <see langword="false" />.</returns>
		// Token: 0x06002098 RID: 8344 RVA: 0x0009964A File Offset: 0x0009784A
		[EditorBrowsable(EditorBrowsableState.Never)]
		public virtual bool ShouldSerializeAcceptChangesDuringFill()
		{
			return this._fillLoadOption == (LoadOption)0;
		}

		/// <summary>Gets or sets whether <see cref="M:System.Data.DataRow.AcceptChanges" /> is called during a <see cref="M:System.Data.Common.DataAdapter.Update(System.Data.DataSet)" />.</summary>
		/// <returns>
		///     <see langword="true" /> if <see cref="M:System.Data.DataRow.AcceptChanges" /> is called during an <see cref="M:System.Data.Common.DataAdapter.Update(System.Data.DataSet)" />; otherwise <see langword="false" />. The default is <see langword="true" />.</returns>
		// Token: 0x17000586 RID: 1414
		// (get) Token: 0x06002099 RID: 8345 RVA: 0x00099655 File Offset: 0x00097855
		// (set) Token: 0x0600209A RID: 8346 RVA: 0x0009965D File Offset: 0x0009785D
		[DefaultValue(true)]
		public bool AcceptChangesDuringUpdate
		{
			get
			{
				return this._acceptChangesDuringUpdate;
			}
			set
			{
				this._acceptChangesDuringUpdate = value;
			}
		}

		/// <summary>Gets or sets a value that specifies whether to generate an exception when an error is encountered during a row update.</summary>
		/// <returns>
		///     <see langword="true" /> to continue the update without generating an exception; otherwise <see langword="false" />. The default is <see langword="false" />.</returns>
		// Token: 0x17000587 RID: 1415
		// (get) Token: 0x0600209B RID: 8347 RVA: 0x00099666 File Offset: 0x00097866
		// (set) Token: 0x0600209C RID: 8348 RVA: 0x0009966E File Offset: 0x0009786E
		[DefaultValue(false)]
		public bool ContinueUpdateOnError
		{
			get
			{
				return this._continueUpdateOnError;
			}
			set
			{
				this._continueUpdateOnError = value;
			}
		}

		/// <summary>Gets or sets the <see cref="T:System.Data.LoadOption" /> that determines how the adapter fills the <see cref="T:System.Data.DataTable" /> from the <see cref="T:System.Data.Common.DbDataReader" />.</summary>
		/// <returns>A <see cref="T:System.Data.LoadOption" /> value.</returns>
		// Token: 0x17000588 RID: 1416
		// (get) Token: 0x0600209D RID: 8349 RVA: 0x00099678 File Offset: 0x00097878
		// (set) Token: 0x0600209E RID: 8350 RVA: 0x00099697 File Offset: 0x00097897
		[RefreshProperties(RefreshProperties.All)]
		public LoadOption FillLoadOption
		{
			get
			{
				if (this._fillLoadOption == (LoadOption)0)
				{
					return LoadOption.OverwriteChanges;
				}
				return this._fillLoadOption;
			}
			set
			{
				if (value <= LoadOption.Upsert)
				{
					this._fillLoadOption = value;
					return;
				}
				throw ADP.InvalidLoadOption(value);
			}
		}

		/// <summary>Resets <see cref="P:System.Data.Common.DataAdapter.FillLoadOption" /> to its default state and causes <see cref="M:System.Data.Common.DataAdapter.Fill(System.Data.DataSet)" /> to honor <see cref="P:System.Data.Common.DataAdapter.AcceptChangesDuringFill" />.</summary>
		// Token: 0x0600209F RID: 8351 RVA: 0x000996AB File Offset: 0x000978AB
		[EditorBrowsable(EditorBrowsableState.Never)]
		public void ResetFillLoadOption()
		{
			this._fillLoadOption = (LoadOption)0;
		}

		/// <summary>Determines whether the <see cref="P:System.Data.Common.DataAdapter.FillLoadOption" /> property should be persisted.</summary>
		/// <returns>
		///     <see langword="true" /> if the <see cref="P:System.Data.Common.DataAdapter.FillLoadOption" /> property is persisted; otherwise <see langword="false" />.</returns>
		// Token: 0x060020A0 RID: 8352 RVA: 0x000996B4 File Offset: 0x000978B4
		[EditorBrowsable(EditorBrowsableState.Never)]
		public virtual bool ShouldSerializeFillLoadOption()
		{
			return this._fillLoadOption > (LoadOption)0;
		}

		/// <summary>Determines the action to take when incoming data does not have a matching table or column.</summary>
		/// <returns>One of the <see cref="T:System.Data.MissingMappingAction" /> values. The default is <see langword="Passthrough" />.</returns>
		/// <exception cref="T:System.ArgumentException">The value set is not one of the <see cref="T:System.Data.MissingMappingAction" /> values. </exception>
		// Token: 0x17000589 RID: 1417
		// (get) Token: 0x060020A1 RID: 8353 RVA: 0x000996BF File Offset: 0x000978BF
		// (set) Token: 0x060020A2 RID: 8354 RVA: 0x000996C7 File Offset: 0x000978C7
		[DefaultValue(MissingMappingAction.Passthrough)]
		public MissingMappingAction MissingMappingAction
		{
			get
			{
				return this._missingMappingAction;
			}
			set
			{
				if (value - MissingMappingAction.Passthrough <= 2)
				{
					this._missingMappingAction = value;
					return;
				}
				throw ADP.InvalidMissingMappingAction(value);
			}
		}

		/// <summary>Determines the action to take when existing <see cref="T:System.Data.DataSet" /> schema does not match incoming data.</summary>
		/// <returns>One of the <see cref="T:System.Data.MissingSchemaAction" /> values. The default is <see langword="Add" />.</returns>
		/// <exception cref="T:System.ArgumentException">The value set is not one of the <see cref="T:System.Data.MissingSchemaAction" /> values. </exception>
		// Token: 0x1700058A RID: 1418
		// (get) Token: 0x060020A3 RID: 8355 RVA: 0x000996DD File Offset: 0x000978DD
		// (set) Token: 0x060020A4 RID: 8356 RVA: 0x000996E5 File Offset: 0x000978E5
		[DefaultValue(MissingSchemaAction.Add)]
		public MissingSchemaAction MissingSchemaAction
		{
			get
			{
				return this._missingSchemaAction;
			}
			set
			{
				if (value - MissingSchemaAction.Add <= 3)
				{
					this._missingSchemaAction = value;
					return;
				}
				throw ADP.InvalidMissingSchemaAction(value);
			}
		}

		// Token: 0x1700058B RID: 1419
		// (get) Token: 0x060020A5 RID: 8357 RVA: 0x000996FB File Offset: 0x000978FB
		internal int ObjectID
		{
			get
			{
				return this._objectID;
			}
		}

		/// <summary>Gets or sets whether the <see langword="Fill" /> method should return provider-specific values or common CLS-compliant values.</summary>
		/// <returns>
		///     <see langword="true" /> if the <see langword="Fill" /> method should return provider-specific values; otherwise <see langword="false" /> to return common CLS-compliant values.</returns>
		// Token: 0x1700058C RID: 1420
		// (get) Token: 0x060020A6 RID: 8358 RVA: 0x00099703 File Offset: 0x00097903
		// (set) Token: 0x060020A7 RID: 8359 RVA: 0x0009970B File Offset: 0x0009790B
		[DefaultValue(false)]
		public virtual bool ReturnProviderSpecificTypes
		{
			get
			{
				return this._returnProviderSpecificTypes;
			}
			set
			{
				this._returnProviderSpecificTypes = value;
			}
		}

		/// <summary>Gets a collection that provides the master mapping between a source table and a <see cref="T:System.Data.DataTable" />.</summary>
		/// <returns>A collection that provides the master mapping between the returned records and the <see cref="T:System.Data.DataSet" />. The default value is an empty collection.</returns>
		// Token: 0x1700058D RID: 1421
		// (get) Token: 0x060020A8 RID: 8360 RVA: 0x00099714 File Offset: 0x00097914
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		public DataTableMappingCollection TableMappings
		{
			get
			{
				DataTableMappingCollection dataTableMappingCollection = this._tableMappings;
				if (dataTableMappingCollection == null)
				{
					dataTableMappingCollection = this.CreateTableMappings();
					if (dataTableMappingCollection == null)
					{
						dataTableMappingCollection = new DataTableMappingCollection();
					}
					this._tableMappings = dataTableMappingCollection;
				}
				return dataTableMappingCollection;
			}
		}

		/// <summary>Indicates how a source table is mapped to a dataset table.</summary>
		/// <returns>A collection that provides the master mapping between the returned records and the <see cref="T:System.Data.DataSet" />. The default value is an empty collection.</returns>
		// Token: 0x1700058E RID: 1422
		// (get) Token: 0x060020A9 RID: 8361 RVA: 0x00099743 File Offset: 0x00097943
		ITableMappingCollection IDataAdapter.TableMappings
		{
			get
			{
				return this.TableMappings;
			}
		}

		/// <summary>Determines whether one or more <see cref="T:System.Data.Common.DataTableMapping" /> objects exist and they should be persisted.</summary>
		/// <returns>
		///     <see langword="true" /> if one or more <see cref="T:System.Data.Common.DataTableMapping" /> objects exist; otherwise <see langword="false" />.</returns>
		// Token: 0x060020AA RID: 8362 RVA: 0x0000EF1B File Offset: 0x0000D11B
		protected virtual bool ShouldSerializeTableMappings()
		{
			return true;
		}

		/// <summary>Indicates whether a <see cref="T:System.Data.Common.DataTableMappingCollection" /> has been created.</summary>
		/// <returns>
		///     <see langword="true" /> if a <see cref="T:System.Data.Common.DataTableMappingCollection" /> has been created; otherwise <see langword="false" />.</returns>
		// Token: 0x060020AB RID: 8363 RVA: 0x0009974B File Offset: 0x0009794B
		protected bool HasTableMappings()
		{
			return this._tableMappings != null && 0 < this.TableMappings.Count;
		}

		/// <summary>Returned when an error occurs during a fill operation.</summary>
		// Token: 0x14000026 RID: 38
		// (add) Token: 0x060020AC RID: 8364 RVA: 0x00099765 File Offset: 0x00097965
		// (remove) Token: 0x060020AD RID: 8365 RVA: 0x0009977F File Offset: 0x0009797F
		public event FillErrorEventHandler FillError
		{
			add
			{
				this._hasFillErrorHandler = true;
				base.Events.AddHandler(DataAdapter.s_eventFillError, value);
			}
			remove
			{
				base.Events.RemoveHandler(DataAdapter.s_eventFillError, value);
			}
		}

		/// <summary>Creates a copy of this instance of <see cref="T:System.Data.Common.DataAdapter" />.</summary>
		/// <returns>The cloned instance of <see cref="T:System.Data.Common.DataAdapter" />.</returns>
		// Token: 0x060020AE RID: 8366 RVA: 0x00099792 File Offset: 0x00097992
		[Obsolete("CloneInternals() has been deprecated.  Use the DataAdapter(DataAdapter from) constructor.  http://go.microsoft.com/fwlink/?linkid=14202")]
		protected virtual DataAdapter CloneInternals()
		{
			DataAdapter dataAdapter = (DataAdapter)Activator.CreateInstance(base.GetType(), BindingFlags.Instance | BindingFlags.Public, null, null, CultureInfo.InvariantCulture, null);
			dataAdapter.CloneFrom(this);
			return dataAdapter;
		}

		// Token: 0x060020AF RID: 8367 RVA: 0x000997B8 File Offset: 0x000979B8
		private void CloneFrom(DataAdapter from)
		{
			this._acceptChangesDuringUpdate = from._acceptChangesDuringUpdate;
			this._acceptChangesDuringUpdateAfterInsert = from._acceptChangesDuringUpdateAfterInsert;
			this._continueUpdateOnError = from._continueUpdateOnError;
			this._returnProviderSpecificTypes = from._returnProviderSpecificTypes;
			this._acceptChangesDuringFill = from._acceptChangesDuringFill;
			this._fillLoadOption = from._fillLoadOption;
			this._missingMappingAction = from._missingMappingAction;
			this._missingSchemaAction = from._missingSchemaAction;
			if (from._tableMappings != null && 0 < from.TableMappings.Count)
			{
				DataTableMappingCollection tableMappings = this.TableMappings;
				foreach (object obj in from.TableMappings)
				{
					tableMappings.Add((obj is ICloneable) ? ((ICloneable)obj).Clone() : obj);
				}
			}
		}

		/// <summary>Creates a new <see cref="T:System.Data.Common.DataTableMappingCollection" />.</summary>
		/// <returns>A new table mapping collection.</returns>
		// Token: 0x060020B0 RID: 8368 RVA: 0x000998A0 File Offset: 0x00097AA0
		protected virtual DataTableMappingCollection CreateTableMappings()
		{
			DataCommonEventSource.Log.Trace<int>("<comm.DataAdapter.CreateTableMappings|API> {0}", this.ObjectID);
			return new DataTableMappingCollection();
		}

		/// <summary>Releases the unmanaged resources used by the <see cref="T:System.Data.Common.DataAdapter" /> and optionally releases the managed resources.</summary>
		/// <param name="disposing">
		///       <see langword="true" /> to release both managed and unmanaged resources; <see langword="false" /> to release only unmanaged resources. </param>
		// Token: 0x060020B1 RID: 8369 RVA: 0x000998BC File Offset: 0x00097ABC
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				this._tableMappings = null;
			}
			base.Dispose(disposing);
		}

		/// <summary>Adds a <see cref="T:System.Data.DataTable" /> to the specified <see cref="T:System.Data.DataSet" /> and configures the schema to match that in the data source based on the specified <see cref="T:System.Data.SchemaType" />.</summary>
		/// <param name="dataSet">The <see cref="T:System.Data.DataSet" /> to be filled with the schema from the data source. </param>
		/// <param name="schemaType">One of the <see cref="T:System.Data.SchemaType" /> values. </param>
		/// <returns>A <see cref="T:System.Data.DataTable" /> object that contains schema information returned from the data source.</returns>
		// Token: 0x060020B2 RID: 8370 RVA: 0x0005CDBE File Offset: 0x0005AFBE
		public virtual DataTable[] FillSchema(DataSet dataSet, SchemaType schemaType)
		{
			throw ADP.NotSupported();
		}

		/// <summary>Adds a <see cref="T:System.Data.DataTable" /> to the specified <see cref="T:System.Data.DataSet" />.</summary>
		/// <param name="dataSet">The <see cref="T:System.Data.DataTable" /> to be filled from the <see cref="T:System.Data.IDataReader" />.</param>
		/// <param name="schemaType">One of the <see cref="T:System.Data.SchemaType" /> values.</param>
		/// <param name="srcTable">The name of the source table to use for table mapping.</param>
		/// <param name="dataReader">The <see cref="T:System.Data.IDataReader" /> to be used as the data source when filling the <see cref="T:System.Data.DataTable" />.</param>
		/// <returns>A reference to a collection of <see cref="T:System.Data.DataTable" /> objects that were added to the <see cref="T:System.Data.DataSet" />.</returns>
		// Token: 0x060020B3 RID: 8371 RVA: 0x000998D0 File Offset: 0x00097AD0
		protected virtual DataTable[] FillSchema(DataSet dataSet, SchemaType schemaType, string srcTable, IDataReader dataReader)
		{
			long scopeId = DataCommonEventSource.Log.EnterScope<int, SchemaType>("<comm.DataAdapter.FillSchema|API> {0}, dataSet, schemaType={1}, srcTable, dataReader", this.ObjectID, schemaType);
			DataTable[] result;
			try
			{
				if (dataSet == null)
				{
					throw ADP.ArgumentNull("dataSet");
				}
				if (SchemaType.Source != schemaType && SchemaType.Mapped != schemaType)
				{
					throw ADP.InvalidSchemaType(schemaType);
				}
				if (string.IsNullOrEmpty(srcTable))
				{
					throw ADP.FillSchemaRequiresSourceTableName("srcTable");
				}
				if (dataReader == null || dataReader.IsClosed)
				{
					throw ADP.FillRequires("dataReader");
				}
				result = (DataTable[])this.FillSchemaFromReader(dataSet, null, schemaType, srcTable, dataReader);
			}
			finally
			{
				DataCommonEventSource.Log.ExitScope(scopeId);
			}
			return result;
		}

		/// <summary>Adds a <see cref="T:System.Data.DataTable" /> to the specified <see cref="T:System.Data.DataSet" />.</summary>
		/// <param name="dataTable">The <see cref="T:System.Data.DataTable" /> to be filled from the <see cref="T:System.Data.IDataReader" />.</param>
		/// <param name="schemaType">One of the <see cref="T:System.Data.SchemaType" /> values.</param>
		/// <param name="dataReader">The <see cref="T:System.Data.IDataReader" /> to be used as the data source when filling the <see cref="T:System.Data.DataTable" />.</param>
		/// <returns>A <see cref="T:System.Data.DataTable" /> object that contains schema information returned from the data source.</returns>
		// Token: 0x060020B4 RID: 8372 RVA: 0x00099970 File Offset: 0x00097B70
		protected virtual DataTable FillSchema(DataTable dataTable, SchemaType schemaType, IDataReader dataReader)
		{
			long scopeId = DataCommonEventSource.Log.EnterScope<int>("<comm.DataAdapter.FillSchema|API> {0}, dataTable, schemaType, dataReader", this.ObjectID);
			DataTable result;
			try
			{
				if (dataTable == null)
				{
					throw ADP.ArgumentNull("dataTable");
				}
				if (SchemaType.Source != schemaType && SchemaType.Mapped != schemaType)
				{
					throw ADP.InvalidSchemaType(schemaType);
				}
				if (dataReader == null || dataReader.IsClosed)
				{
					throw ADP.FillRequires("dataReader");
				}
				result = (DataTable)this.FillSchemaFromReader(null, dataTable, schemaType, null, dataReader);
			}
			finally
			{
				DataCommonEventSource.Log.ExitScope(scopeId);
			}
			return result;
		}

		// Token: 0x060020B5 RID: 8373 RVA: 0x000999F8 File Offset: 0x00097BF8
		internal object FillSchemaFromReader(DataSet dataset, DataTable datatable, SchemaType schemaType, string srcTable, IDataReader dataReader)
		{
			DataTable[] array = null;
			int num = 0;
			SchemaMapping schemaMapping;
			for (;;)
			{
				DataReaderContainer dataReaderContainer = DataReaderContainer.Create(dataReader, this.ReturnProviderSpecificTypes);
				if (0 < dataReaderContainer.FieldCount)
				{
					string sourceTableName = null;
					if (dataset != null)
					{
						sourceTableName = DataAdapter.GetSourceTableName(srcTable, num);
						num++;
					}
					schemaMapping = new SchemaMapping(this, dataset, datatable, dataReaderContainer, true, schemaType, sourceTableName, false, null, null);
					if (datatable != null)
					{
						break;
					}
					if (schemaMapping.DataTable != null)
					{
						if (array == null)
						{
							array = new DataTable[]
							{
								schemaMapping.DataTable
							};
						}
						else
						{
							array = DataAdapter.AddDataTableToArray(array, schemaMapping.DataTable);
						}
					}
				}
				if (!dataReader.NextResult())
				{
					goto Block_6;
				}
			}
			return schemaMapping.DataTable;
			Block_6:
			object obj = array;
			if (obj == null && datatable == null)
			{
				obj = Array.Empty<DataTable>();
			}
			return obj;
		}

		/// <summary>Adds or refreshes rows in the <see cref="T:System.Data.DataSet" /> to match those in the data source.</summary>
		/// <param name="dataSet">A <see cref="T:System.Data.DataSet" /> to fill with records and, if necessary, schema. </param>
		/// <returns>The number of rows successfully added to or refreshed in the <see cref="T:System.Data.DataSet" />. This does not include rows affected by statements that do not return rows.</returns>
		// Token: 0x060020B6 RID: 8374 RVA: 0x0005CDBE File Offset: 0x0005AFBE
		public virtual int Fill(DataSet dataSet)
		{
			throw ADP.NotSupported();
		}

		/// <summary>Adds or refreshes rows in a specified range in the <see cref="T:System.Data.DataSet" /> to match those in the data source using the <see cref="T:System.Data.DataSet" /> and <see cref="T:System.Data.DataTable" /> names.</summary>
		/// <param name="dataSet">A <see cref="T:System.Data.DataSet" /> to fill with records.</param>
		/// <param name="srcTable">A string indicating the name of the source table.</param>
		/// <param name="dataReader">An instance of <see cref="T:System.Data.IDataReader" />.</param>
		/// <param name="startRecord">The zero-based index of the starting record.</param>
		/// <param name="maxRecords">An integer indicating the maximum number of records.</param>
		/// <returns>The number of rows successfully added to or refreshed in the <see cref="T:System.Data.DataSet" />. This does not include rows affected by statements that do not return rows.</returns>
		// Token: 0x060020B7 RID: 8375 RVA: 0x00099A98 File Offset: 0x00097C98
		protected virtual int Fill(DataSet dataSet, string srcTable, IDataReader dataReader, int startRecord, int maxRecords)
		{
			long scopeId = DataCommonEventSource.Log.EnterScope<int>("<comm.DataAdapter.Fill|API> {0}, dataSet, srcTable, dataReader, startRecord, maxRecords", this.ObjectID);
			int result;
			try
			{
				if (dataSet == null)
				{
					throw ADP.FillRequires("dataSet");
				}
				if (string.IsNullOrEmpty(srcTable))
				{
					throw ADP.FillRequiresSourceTableName("srcTable");
				}
				if (dataReader == null)
				{
					throw ADP.FillRequires("dataReader");
				}
				if (startRecord < 0)
				{
					throw ADP.InvalidStartRecord("startRecord", startRecord);
				}
				if (maxRecords < 0)
				{
					throw ADP.InvalidMaxRecords("maxRecords", maxRecords);
				}
				if (dataReader.IsClosed)
				{
					result = 0;
				}
				else
				{
					DataReaderContainer dataReader2 = DataReaderContainer.Create(dataReader, this.ReturnProviderSpecificTypes);
					result = this.FillFromReader(dataSet, null, srcTable, dataReader2, startRecord, maxRecords, null, null);
				}
			}
			finally
			{
				DataCommonEventSource.Log.ExitScope(scopeId);
			}
			return result;
		}

		/// <summary>Adds or refreshes rows in the <see cref="T:System.Data.DataTable" /> to match those in the data source using the <see cref="T:System.Data.DataTable" /> name and the specified <see cref="T:System.Data.IDataReader" />.</summary>
		/// <param name="dataTable">A <see cref="T:System.Data.DataTable" /> to fill with records.</param>
		/// <param name="dataReader">An instance of <see cref="T:System.Data.IDataReader" />.</param>
		/// <returns>The number of rows successfully added to or refreshed in the <see cref="T:System.Data.DataTable" />. This does not include rows affected by statements that do not return rows.</returns>
		// Token: 0x060020B8 RID: 8376 RVA: 0x00099B58 File Offset: 0x00097D58
		protected virtual int Fill(DataTable dataTable, IDataReader dataReader)
		{
			DataTable[] dataTables = new DataTable[]
			{
				dataTable
			};
			return this.Fill(dataTables, dataReader, 0, 0);
		}

		/// <summary>Adds or refreshes rows in a specified range in the collection of <see cref="T:System.Data.DataTable" /> objects to match those in the data source.</summary>
		/// <param name="dataTables">A collection of <see cref="T:System.Data.DataTable" /> objects to fill with records.</param>
		/// <param name="dataReader">An instance of <see cref="T:System.Data.IDataReader" />.</param>
		/// <param name="startRecord">The zero-based index of the starting record.</param>
		/// <param name="maxRecords">An integer indicating the maximum number of records.</param>
		/// <returns>The number of rows successfully added to or refreshed in the <see cref="T:System.Data.DataTable" />. This does not include rows affected by statements that do not return rows.</returns>
		// Token: 0x060020B9 RID: 8377 RVA: 0x00099B7C File Offset: 0x00097D7C
		protected virtual int Fill(DataTable[] dataTables, IDataReader dataReader, int startRecord, int maxRecords)
		{
			long scopeId = DataCommonEventSource.Log.EnterScope<int>("<comm.DataAdapter.Fill|API> {0}, dataTables[], dataReader, startRecord, maxRecords", this.ObjectID);
			int result;
			try
			{
				ADP.CheckArgumentLength(dataTables, "dataTables");
				if (dataTables == null || dataTables.Length == 0 || dataTables[0] == null)
				{
					throw ADP.FillRequires("dataTable");
				}
				if (dataReader == null)
				{
					throw ADP.FillRequires("dataReader");
				}
				if (1 < dataTables.Length && (startRecord != 0 || maxRecords != 0))
				{
					throw ADP.NotSupported();
				}
				int num = 0;
				bool flag = false;
				DataSet dataSet = dataTables[0].DataSet;
				try
				{
					if (dataSet != null)
					{
						flag = dataSet.EnforceConstraints;
						dataSet.EnforceConstraints = false;
					}
					int num2 = 0;
					while (num2 < dataTables.Length && !dataReader.IsClosed)
					{
						DataReaderContainer dataReaderContainer = DataReaderContainer.Create(dataReader, this.ReturnProviderSpecificTypes);
						if (dataReaderContainer.FieldCount > 0)
						{
							goto IL_BC;
						}
						if (num2 == 0)
						{
							bool flag2;
							do
							{
								flag2 = this.FillNextResult(dataReaderContainer);
							}
							while (flag2 && dataReaderContainer.FieldCount <= 0);
							if (flag2)
							{
								goto IL_BC;
							}
							break;
						}
						IL_E7:
						num2++;
						continue;
						IL_BC:
						if (0 < num2 && !this.FillNextResult(dataReaderContainer))
						{
							break;
						}
						int num3 = this.FillFromReader(null, dataTables[num2], null, dataReaderContainer, startRecord, maxRecords, null, null);
						if (num2 == 0)
						{
							num = num3;
							goto IL_E7;
						}
						goto IL_E7;
					}
				}
				catch (ConstraintException)
				{
					flag = false;
					throw;
				}
				finally
				{
					if (flag)
					{
						dataSet.EnforceConstraints = true;
					}
				}
				result = num;
			}
			finally
			{
				DataCommonEventSource.Log.ExitScope(scopeId);
			}
			return result;
		}

		// Token: 0x060020BA RID: 8378 RVA: 0x00099CCC File Offset: 0x00097ECC
		internal int FillFromReader(DataSet dataset, DataTable datatable, string srcTable, DataReaderContainer dataReader, int startRecord, int maxRecords, DataColumn parentChapterColumn, object parentChapterValue)
		{
			int result = 0;
			int num = 0;
			do
			{
				if (0 < dataReader.FieldCount)
				{
					SchemaMapping schemaMapping = this.FillMapping(dataset, datatable, srcTable, dataReader, num, parentChapterColumn, parentChapterValue);
					num++;
					if (schemaMapping != null && schemaMapping.DataValues != null && schemaMapping.DataTable != null)
					{
						schemaMapping.DataTable.BeginLoadData();
						try
						{
							if (1 == num && (0 < startRecord || 0 < maxRecords))
							{
								result = this.FillLoadDataRowChunk(schemaMapping, startRecord, maxRecords);
							}
							else
							{
								int num2 = this.FillLoadDataRow(schemaMapping);
								if (1 == num)
								{
									result = num2;
								}
							}
						}
						finally
						{
							schemaMapping.DataTable.EndLoadData();
						}
						if (datatable != null)
						{
							break;
						}
					}
				}
			}
			while (this.FillNextResult(dataReader));
			return result;
		}

		// Token: 0x060020BB RID: 8379 RVA: 0x00099D74 File Offset: 0x00097F74
		private int FillLoadDataRowChunk(SchemaMapping mapping, int startRecord, int maxRecords)
		{
			DataReaderContainer dataReader = mapping.DataReader;
			while (0 < startRecord)
			{
				if (!dataReader.Read())
				{
					return 0;
				}
				startRecord--;
			}
			int i = 0;
			if (0 < maxRecords)
			{
				while (i < maxRecords)
				{
					if (!dataReader.Read())
					{
						break;
					}
					if (this._hasFillErrorHandler)
					{
						try
						{
							mapping.LoadDataRowWithClear();
							i++;
							continue;
						}
						catch (Exception e) when (ADP.IsCatchableExceptionType(e))
						{
							ADP.TraceExceptionForCapture(e);
							this.OnFillErrorHandler(e, mapping.DataTable, mapping.DataValues);
							continue;
						}
					}
					mapping.LoadDataRow();
					i++;
				}
			}
			else
			{
				i = this.FillLoadDataRow(mapping);
			}
			return i;
		}

		// Token: 0x060020BC RID: 8380 RVA: 0x00099E20 File Offset: 0x00098020
		private int FillLoadDataRow(SchemaMapping mapping)
		{
			int num = 0;
			DataReaderContainer dataReader = mapping.DataReader;
			if (this._hasFillErrorHandler)
			{
				while (dataReader.Read())
				{
					try
					{
						mapping.LoadDataRowWithClear();
						num++;
					}
					catch (Exception e) when (ADP.IsCatchableExceptionType(e))
					{
						ADP.TraceExceptionForCapture(e);
						this.OnFillErrorHandler(e, mapping.DataTable, mapping.DataValues);
					}
				}
			}
			else
			{
				while (dataReader.Read())
				{
					mapping.LoadDataRow();
					num++;
				}
			}
			return num;
		}

		// Token: 0x060020BD RID: 8381 RVA: 0x00099EB0 File Offset: 0x000980B0
		private SchemaMapping FillMappingInternal(DataSet dataset, DataTable datatable, string srcTable, DataReaderContainer dataReader, int schemaCount, DataColumn parentChapterColumn, object parentChapterValue)
		{
			bool keyInfo = MissingSchemaAction.AddWithKey == this.MissingSchemaAction;
			string sourceTableName = null;
			if (dataset != null)
			{
				sourceTableName = DataAdapter.GetSourceTableName(srcTable, schemaCount);
			}
			return new SchemaMapping(this, dataset, datatable, dataReader, keyInfo, SchemaType.Mapped, sourceTableName, true, parentChapterColumn, parentChapterValue);
		}

		// Token: 0x060020BE RID: 8382 RVA: 0x00099EE8 File Offset: 0x000980E8
		private SchemaMapping FillMapping(DataSet dataset, DataTable datatable, string srcTable, DataReaderContainer dataReader, int schemaCount, DataColumn parentChapterColumn, object parentChapterValue)
		{
			SchemaMapping result = null;
			if (this._hasFillErrorHandler)
			{
				try
				{
					return this.FillMappingInternal(dataset, datatable, srcTable, dataReader, schemaCount, parentChapterColumn, parentChapterValue);
				}
				catch (Exception e) when (ADP.IsCatchableExceptionType(e))
				{
					ADP.TraceExceptionForCapture(e);
					this.OnFillErrorHandler(e, null, null);
					return result;
				}
			}
			result = this.FillMappingInternal(dataset, datatable, srcTable, dataReader, schemaCount, parentChapterColumn, parentChapterValue);
			return result;
		}

		// Token: 0x060020BF RID: 8383 RVA: 0x00099F60 File Offset: 0x00098160
		private bool FillNextResult(DataReaderContainer dataReader)
		{
			bool result = true;
			if (this._hasFillErrorHandler)
			{
				try
				{
					return dataReader.NextResult();
				}
				catch (Exception e) when (ADP.IsCatchableExceptionType(e))
				{
					ADP.TraceExceptionForCapture(e);
					this.OnFillErrorHandler(e, null, null);
					return result;
				}
			}
			result = dataReader.NextResult();
			return result;
		}

		/// <summary>Gets the parameters set by the user when executing an SQL SELECT statement.</summary>
		/// <returns>An array of <see cref="T:System.Data.IDataParameter" /> objects that contains the parameters set by the user.</returns>
		// Token: 0x060020C0 RID: 8384 RVA: 0x00099FC4 File Offset: 0x000981C4
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public virtual IDataParameter[] GetFillParameters()
		{
			return Array.Empty<IDataParameter>();
		}

		// Token: 0x060020C1 RID: 8385 RVA: 0x00099FCB File Offset: 0x000981CB
		internal DataTableMapping GetTableMappingBySchemaAction(string sourceTableName, string dataSetTableName, MissingMappingAction mappingAction)
		{
			return DataTableMappingCollection.GetTableMappingBySchemaAction(this._tableMappings, sourceTableName, dataSetTableName, mappingAction);
		}

		// Token: 0x060020C2 RID: 8386 RVA: 0x00099FDB File Offset: 0x000981DB
		internal int IndexOfDataSetTable(string dataSetTable)
		{
			if (this._tableMappings != null)
			{
				return this.TableMappings.IndexOfDataSetTable(dataSetTable);
			}
			return -1;
		}

		/// <summary>Invoked when an error occurs during a <see langword="Fill" />.</summary>
		/// <param name="value">A <see cref="T:System.Data.FillErrorEventArgs" /> object.</param>
		// Token: 0x060020C3 RID: 8387 RVA: 0x00099FF3 File Offset: 0x000981F3
		protected virtual void OnFillError(FillErrorEventArgs value)
		{
			FillErrorEventHandler fillErrorEventHandler = (FillErrorEventHandler)base.Events[DataAdapter.s_eventFillError];
			if (fillErrorEventHandler == null)
			{
				return;
			}
			fillErrorEventHandler(this, value);
		}

		// Token: 0x060020C4 RID: 8388 RVA: 0x0009A018 File Offset: 0x00098218
		private void OnFillErrorHandler(Exception e, DataTable dataTable, object[] dataValues)
		{
			FillErrorEventArgs fillErrorEventArgs = new FillErrorEventArgs(dataTable, dataValues);
			fillErrorEventArgs.Errors = e;
			this.OnFillError(fillErrorEventArgs);
			if (fillErrorEventArgs.Continue)
			{
				return;
			}
			if (fillErrorEventArgs.Errors != null)
			{
				throw fillErrorEventArgs.Errors;
			}
			throw e;
		}

		/// <summary>Calls the respective INSERT, UPDATE, or DELETE statements for each inserted, updated, or deleted row in the specified <see cref="T:System.Data.DataSet" /> from a <see cref="T:System.Data.DataTable" /> named "Table."</summary>
		/// <param name="dataSet">The <see cref="T:System.Data.DataSet" /> used to update the data source. </param>
		/// <returns>The number of rows successfully updated from the <see cref="T:System.Data.DataSet" />.</returns>
		/// <exception cref="T:System.InvalidOperationException">The source table is invalid. </exception>
		/// <exception cref="T:System.Data.DBConcurrencyException">An attempt to execute an INSERT, UPDATE, or DELETE statement resulted in zero records affected. </exception>
		// Token: 0x060020C5 RID: 8389 RVA: 0x0005CDBE File Offset: 0x0005AFBE
		public virtual int Update(DataSet dataSet)
		{
			throw ADP.NotSupported();
		}

		// Token: 0x060020C6 RID: 8390 RVA: 0x0009A054 File Offset: 0x00098254
		private static DataTable[] AddDataTableToArray(DataTable[] tables, DataTable newTable)
		{
			for (int i = 0; i < tables.Length; i++)
			{
				if (tables[i] == newTable)
				{
					return tables;
				}
			}
			DataTable[] array = new DataTable[tables.Length + 1];
			for (int j = 0; j < tables.Length; j++)
			{
				array[j] = tables[j];
			}
			array[tables.Length] = newTable;
			return array;
		}

		// Token: 0x060020C7 RID: 8391 RVA: 0x0009A09D File Offset: 0x0009829D
		private static string GetSourceTableName(string srcTable, int index)
		{
			if (index == 0)
			{
				return srcTable;
			}
			return srcTable + index.ToString(CultureInfo.InvariantCulture);
		}

		// Token: 0x060020C8 RID: 8392 RVA: 0x0009A0B6 File Offset: 0x000982B6
		// Note: this type is marked as 'beforefieldinit'.
		static DataAdapter()
		{
		}

		// Token: 0x04001559 RID: 5465
		private static readonly object s_eventFillError = new object();

		// Token: 0x0400155A RID: 5466
		private bool _acceptChangesDuringUpdate = true;

		// Token: 0x0400155B RID: 5467
		private bool _acceptChangesDuringUpdateAfterInsert = true;

		// Token: 0x0400155C RID: 5468
		private bool _continueUpdateOnError;

		// Token: 0x0400155D RID: 5469
		private bool _hasFillErrorHandler;

		// Token: 0x0400155E RID: 5470
		private bool _returnProviderSpecificTypes;

		// Token: 0x0400155F RID: 5471
		private bool _acceptChangesDuringFill = true;

		// Token: 0x04001560 RID: 5472
		private LoadOption _fillLoadOption;

		// Token: 0x04001561 RID: 5473
		private MissingMappingAction _missingMappingAction = MissingMappingAction.Passthrough;

		// Token: 0x04001562 RID: 5474
		private MissingSchemaAction _missingSchemaAction = MissingSchemaAction.Add;

		// Token: 0x04001563 RID: 5475
		private DataTableMappingCollection _tableMappings;

		// Token: 0x04001564 RID: 5476
		private static int s_objectTypeCount;

		// Token: 0x04001565 RID: 5477
		internal readonly int _objectID = Interlocked.Increment(ref DataAdapter.s_objectTypeCount);
	}
}
