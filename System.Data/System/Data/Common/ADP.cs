﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Globalization;
using System.IO;
using System.Runtime.CompilerServices;
using System.Runtime.ConstrainedExecution;
using System.Security;
using System.Security.Permissions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;
using Microsoft.SqlServer.Server;

namespace System.Data.Common
{
	// Token: 0x02000289 RID: 649
	internal static class ADP
	{
		// Token: 0x17000546 RID: 1350
		// (get) Token: 0x06001E8E RID: 7822 RVA: 0x00092DD2 File Offset: 0x00090FD2
		internal static Task<bool> TrueTask
		{
			get
			{
				Task<bool> result;
				if ((result = ADP._trueTask) == null)
				{
					result = (ADP._trueTask = Task.FromResult<bool>(true));
				}
				return result;
			}
		}

		// Token: 0x17000547 RID: 1351
		// (get) Token: 0x06001E8F RID: 7823 RVA: 0x00092DE9 File Offset: 0x00090FE9
		internal static Task<bool> FalseTask
		{
			get
			{
				Task<bool> result;
				if ((result = ADP._falseTask) == null)
				{
					result = (ADP._falseTask = Task.FromResult<bool>(false));
				}
				return result;
			}
		}

		// Token: 0x06001E90 RID: 7824 RVA: 0x00014672 File Offset: 0x00012872
		private static void TraceException(string trace, Exception e)
		{
			if (e != null)
			{
				DataCommonEventSource.Log.Trace<Exception>(trace, e);
			}
		}

		// Token: 0x06001E91 RID: 7825 RVA: 0x00092E00 File Offset: 0x00091000
		internal static void TraceExceptionAsReturnValue(Exception e)
		{
			ADP.TraceException("<comm.ADP.TraceException|ERR|THROW> '{0}'", e);
		}

		// Token: 0x06001E92 RID: 7826 RVA: 0x00092E0D File Offset: 0x0009100D
		internal static void TraceExceptionWithoutRethrow(Exception e)
		{
			ADP.TraceException("<comm.ADP.TraceException|ERR|CATCH> '%ls'\n", e);
		}

		// Token: 0x06001E93 RID: 7827 RVA: 0x00092E1A File Offset: 0x0009101A
		internal static ArgumentException Argument(string error)
		{
			ArgumentException ex = new ArgumentException(error);
			ADP.TraceExceptionAsReturnValue(ex);
			return ex;
		}

		// Token: 0x06001E94 RID: 7828 RVA: 0x00092E28 File Offset: 0x00091028
		internal static ArgumentException Argument(string error, Exception inner)
		{
			ArgumentException ex = new ArgumentException(error, inner);
			ADP.TraceExceptionAsReturnValue(ex);
			return ex;
		}

		// Token: 0x06001E95 RID: 7829 RVA: 0x00092E37 File Offset: 0x00091037
		internal static ArgumentException Argument(string error, string parameter)
		{
			ArgumentException ex = new ArgumentException(error, parameter);
			ADP.TraceExceptionAsReturnValue(ex);
			return ex;
		}

		// Token: 0x06001E96 RID: 7830 RVA: 0x00092E46 File Offset: 0x00091046
		internal static ArgumentNullException ArgumentNull(string parameter)
		{
			ArgumentNullException ex = new ArgumentNullException(parameter);
			ADP.TraceExceptionAsReturnValue(ex);
			return ex;
		}

		// Token: 0x06001E97 RID: 7831 RVA: 0x00092E54 File Offset: 0x00091054
		internal static ArgumentNullException ArgumentNull(string parameter, string error)
		{
			ArgumentNullException ex = new ArgumentNullException(parameter, error);
			ADP.TraceExceptionAsReturnValue(ex);
			return ex;
		}

		// Token: 0x06001E98 RID: 7832 RVA: 0x00092E63 File Offset: 0x00091063
		internal static ArgumentOutOfRangeException ArgumentOutOfRange(string parameterName)
		{
			ArgumentOutOfRangeException ex = new ArgumentOutOfRangeException(parameterName);
			ADP.TraceExceptionAsReturnValue(ex);
			return ex;
		}

		// Token: 0x06001E99 RID: 7833 RVA: 0x00092E71 File Offset: 0x00091071
		internal static ArgumentOutOfRangeException ArgumentOutOfRange(string message, string parameterName)
		{
			ArgumentOutOfRangeException ex = new ArgumentOutOfRangeException(parameterName, message);
			ADP.TraceExceptionAsReturnValue(ex);
			return ex;
		}

		// Token: 0x06001E9A RID: 7834 RVA: 0x00092E80 File Offset: 0x00091080
		internal static IndexOutOfRangeException IndexOutOfRange(string error)
		{
			IndexOutOfRangeException ex = new IndexOutOfRangeException(error);
			ADP.TraceExceptionAsReturnValue(ex);
			return ex;
		}

		// Token: 0x06001E9B RID: 7835 RVA: 0x00092E8E File Offset: 0x0009108E
		internal static InvalidCastException InvalidCast(string error)
		{
			return ADP.InvalidCast(error, null);
		}

		// Token: 0x06001E9C RID: 7836 RVA: 0x00092E97 File Offset: 0x00091097
		internal static InvalidCastException InvalidCast(string error, Exception inner)
		{
			InvalidCastException ex = new InvalidCastException(error, inner);
			ADP.TraceExceptionAsReturnValue(ex);
			return ex;
		}

		// Token: 0x06001E9D RID: 7837 RVA: 0x00092EA6 File Offset: 0x000910A6
		internal static InvalidOperationException InvalidOperation(string error)
		{
			InvalidOperationException ex = new InvalidOperationException(error);
			ADP.TraceExceptionAsReturnValue(ex);
			return ex;
		}

		// Token: 0x06001E9E RID: 7838 RVA: 0x00092EB4 File Offset: 0x000910B4
		internal static NotSupportedException NotSupported()
		{
			NotSupportedException ex = new NotSupportedException();
			ADP.TraceExceptionAsReturnValue(ex);
			return ex;
		}

		// Token: 0x06001E9F RID: 7839 RVA: 0x00092EC1 File Offset: 0x000910C1
		internal static NotSupportedException NotSupported(string error)
		{
			NotSupportedException ex = new NotSupportedException(error);
			ADP.TraceExceptionAsReturnValue(ex);
			return ex;
		}

		// Token: 0x06001EA0 RID: 7840 RVA: 0x00092ED0 File Offset: 0x000910D0
		internal static bool RemoveStringQuotes(string quotePrefix, string quoteSuffix, string quotedString, out string unquotedString)
		{
			int num = (quotePrefix != null) ? quotePrefix.Length : 0;
			int num2 = (quoteSuffix != null) ? quoteSuffix.Length : 0;
			if (num2 + num == 0)
			{
				unquotedString = quotedString;
				return true;
			}
			if (quotedString == null)
			{
				unquotedString = quotedString;
				return false;
			}
			int length = quotedString.Length;
			if (length < num + num2)
			{
				unquotedString = quotedString;
				return false;
			}
			if (num > 0 && !quotedString.StartsWith(quotePrefix, StringComparison.Ordinal))
			{
				unquotedString = quotedString;
				return false;
			}
			if (num2 > 0)
			{
				if (!quotedString.EndsWith(quoteSuffix, StringComparison.Ordinal))
				{
					unquotedString = quotedString;
					return false;
				}
				unquotedString = quotedString.Substring(num, length - (num + num2)).Replace(quoteSuffix + quoteSuffix, quoteSuffix);
			}
			else
			{
				unquotedString = quotedString.Substring(num, length - num);
			}
			return true;
		}

		// Token: 0x06001EA1 RID: 7841 RVA: 0x00092F6B File Offset: 0x0009116B
		internal static ArgumentOutOfRangeException NotSupportedEnumerationValue(Type type, string value, string method)
		{
			return ADP.ArgumentOutOfRange(SR.Format("The {0} enumeration value, {1}, is not supported by the {2} method.", type.Name, value, method), type.Name);
		}

		// Token: 0x06001EA2 RID: 7842 RVA: 0x00092F8A File Offset: 0x0009118A
		internal static InvalidOperationException DataAdapter(string error)
		{
			return ADP.InvalidOperation(error);
		}

		// Token: 0x06001EA3 RID: 7843 RVA: 0x00092F8A File Offset: 0x0009118A
		private static InvalidOperationException Provider(string error)
		{
			return ADP.InvalidOperation(error);
		}

		// Token: 0x06001EA4 RID: 7844 RVA: 0x00092F92 File Offset: 0x00091192
		internal static ArgumentException InvalidMultipartName(string property, string value)
		{
			ArgumentException ex = new ArgumentException(SR.Format("{0} \"{1}\".", property, value));
			ADP.TraceExceptionAsReturnValue(ex);
			return ex;
		}

		// Token: 0x06001EA5 RID: 7845 RVA: 0x00092FAB File Offset: 0x000911AB
		internal static ArgumentException InvalidMultipartNameIncorrectUsageOfQuotes(string property, string value)
		{
			ArgumentException ex = new ArgumentException(SR.Format("{0} \"{1}\", incorrect usage of quotes.", property, value));
			ADP.TraceExceptionAsReturnValue(ex);
			return ex;
		}

		// Token: 0x06001EA6 RID: 7846 RVA: 0x00092FC4 File Offset: 0x000911C4
		internal static ArgumentException InvalidMultipartNameToManyParts(string property, string value, int limit)
		{
			ArgumentException ex = new ArgumentException(SR.Format("{0} \"{1}\", the current limit of \"{2}\" is insufficient.", property, value, limit));
			ADP.TraceExceptionAsReturnValue(ex);
			return ex;
		}

		// Token: 0x06001EA7 RID: 7847 RVA: 0x00092FE3 File Offset: 0x000911E3
		internal static void CheckArgumentNull(object value, string parameterName)
		{
			if (value == null)
			{
				throw ADP.ArgumentNull(parameterName);
			}
		}

		// Token: 0x06001EA8 RID: 7848 RVA: 0x00092FF0 File Offset: 0x000911F0
		internal static bool IsCatchableExceptionType(Exception e)
		{
			Type type = e.GetType();
			return type != ADP.s_stackOverflowType && type != ADP.s_outOfMemoryType && type != ADP.s_threadAbortType && type != ADP.s_nullReferenceType && type != ADP.s_accessViolationType && !ADP.s_securityType.IsAssignableFrom(type);
		}

		// Token: 0x06001EA9 RID: 7849 RVA: 0x00093058 File Offset: 0x00091258
		internal static bool IsCatchableOrSecurityExceptionType(Exception e)
		{
			Type type = e.GetType();
			return type != ADP.s_stackOverflowType && type != ADP.s_outOfMemoryType && type != ADP.s_threadAbortType && type != ADP.s_nullReferenceType && type != ADP.s_accessViolationType;
		}

		// Token: 0x06001EAA RID: 7850 RVA: 0x000930AD File Offset: 0x000912AD
		internal static ArgumentOutOfRangeException InvalidEnumerationValue(Type type, int value)
		{
			return ADP.ArgumentOutOfRange(SR.Format("The {0} enumeration value, {1}, is invalid.", type.Name, value.ToString(CultureInfo.InvariantCulture)), type.Name);
		}

		// Token: 0x06001EAB RID: 7851 RVA: 0x000930D6 File Offset: 0x000912D6
		internal static ArgumentException ConnectionStringSyntax(int index)
		{
			return ADP.Argument(SR.Format("Format of the initialization string does not conform to specification starting at index {0}.", index));
		}

		// Token: 0x06001EAC RID: 7852 RVA: 0x000930ED File Offset: 0x000912ED
		internal static ArgumentException KeywordNotSupported(string keyword)
		{
			return ADP.Argument(SR.Format("Keyword not supported: '{0}'.", keyword));
		}

		// Token: 0x06001EAD RID: 7853 RVA: 0x000930FF File Offset: 0x000912FF
		internal static ArgumentException ConvertFailed(Type fromType, Type toType, Exception innerException)
		{
			return ADP.Argument(SR.Format(" Cannot convert object of type '{0}' to object of type '{1}'.", fromType.FullName, toType.FullName), innerException);
		}

		// Token: 0x06001EAE RID: 7854 RVA: 0x0009311D File Offset: 0x0009131D
		internal static Exception InvalidConnectionOptionValue(string key)
		{
			return ADP.InvalidConnectionOptionValue(key, null);
		}

		// Token: 0x06001EAF RID: 7855 RVA: 0x00093126 File Offset: 0x00091326
		internal static Exception InvalidConnectionOptionValue(string key, Exception inner)
		{
			return ADP.Argument(SR.Format("Invalid value for key '{0}'.", key), inner);
		}

		// Token: 0x06001EB0 RID: 7856 RVA: 0x00093139 File Offset: 0x00091339
		internal static ArgumentException CollectionRemoveInvalidObject(Type itemType, ICollection collection)
		{
			return ADP.Argument(SR.Format("Attempted to remove an {0} that is not contained by this {1}.", itemType.Name, collection.GetType().Name));
		}

		// Token: 0x06001EB1 RID: 7857 RVA: 0x0009315B File Offset: 0x0009135B
		internal static ArgumentNullException CollectionNullValue(string parameter, Type collection, Type itemType)
		{
			return ADP.ArgumentNull(parameter, SR.Format("The {0} only accepts non-null {1} type objects.", collection.Name, itemType.Name));
		}

		// Token: 0x06001EB2 RID: 7858 RVA: 0x00093179 File Offset: 0x00091379
		internal static IndexOutOfRangeException CollectionIndexInt32(int index, Type collection, int count)
		{
			return ADP.IndexOutOfRange(SR.Format("Invalid index {0} for this {1} with Count={2}.", index.ToString(CultureInfo.InvariantCulture), collection.Name, count.ToString(CultureInfo.InvariantCulture)));
		}

		// Token: 0x06001EB3 RID: 7859 RVA: 0x000931A8 File Offset: 0x000913A8
		internal static IndexOutOfRangeException CollectionIndexString(Type itemType, string propertyName, string propertyValue, Type collection)
		{
			return ADP.IndexOutOfRange(SR.Format("An {0} with {1} '{2}' is not contained by this {3}.", new object[]
			{
				itemType.Name,
				propertyName,
				propertyValue,
				collection.Name
			}));
		}

		// Token: 0x06001EB4 RID: 7860 RVA: 0x000931D9 File Offset: 0x000913D9
		internal static InvalidCastException CollectionInvalidType(Type collection, Type itemType, object invalidValue)
		{
			return ADP.InvalidCast(SR.Format("The {0} only accepts non-null {1} type objects, not {2} objects.", collection.Name, itemType.Name, invalidValue.GetType().Name));
		}

		// Token: 0x06001EB5 RID: 7861 RVA: 0x00093204 File Offset: 0x00091404
		private static string ConnectionStateMsg(ConnectionState state)
		{
			switch (state)
			{
			case ConnectionState.Closed:
				break;
			case ConnectionState.Open:
				return "The connection's current state is open.";
			case ConnectionState.Connecting:
				return "The connection's current state is connecting.";
			case ConnectionState.Open | ConnectionState.Connecting:
			case ConnectionState.Executing:
				goto IL_46;
			case ConnectionState.Open | ConnectionState.Executing:
				return "The connection's current state is executing.";
			default:
				if (state == (ConnectionState.Open | ConnectionState.Fetching))
				{
					return "The connection's current state is fetching.";
				}
				if (state != (ConnectionState.Connecting | ConnectionState.Broken))
				{
					goto IL_46;
				}
				break;
			}
			return "The connection's current state is closed.";
			IL_46:
			return SR.Format("The connection's current state: {0}.", state.ToString());
		}

		// Token: 0x06001EB6 RID: 7862 RVA: 0x0009326E File Offset: 0x0009146E
		internal static Exception StreamClosed([CallerMemberName] string method = "")
		{
			return ADP.InvalidOperation(SR.Format("Invalid attempt to {0} when stream is closed.", method));
		}

		// Token: 0x06001EB7 RID: 7863 RVA: 0x00093280 File Offset: 0x00091480
		internal static string BuildQuotedString(string quotePrefix, string quoteSuffix, string unQuotedString)
		{
			StringBuilder stringBuilder = new StringBuilder();
			if (!string.IsNullOrEmpty(quotePrefix))
			{
				stringBuilder.Append(quotePrefix);
			}
			if (!string.IsNullOrEmpty(quoteSuffix))
			{
				stringBuilder.Append(unQuotedString.Replace(quoteSuffix, quoteSuffix + quoteSuffix));
				stringBuilder.Append(quoteSuffix);
			}
			else
			{
				stringBuilder.Append(unQuotedString);
			}
			return stringBuilder.ToString();
		}

		// Token: 0x06001EB8 RID: 7864 RVA: 0x000932D8 File Offset: 0x000914D8
		internal static ArgumentException ParametersIsNotParent(Type parameterType, ICollection collection)
		{
			return ADP.Argument(SR.Format("The {0} is already contained by another {1}.", parameterType.Name, collection.GetType().Name));
		}

		// Token: 0x06001EB9 RID: 7865 RVA: 0x000932D8 File Offset: 0x000914D8
		internal static ArgumentException ParametersIsParent(Type parameterType, ICollection collection)
		{
			return ADP.Argument(SR.Format("The {0} is already contained by another {1}.", parameterType.Name, collection.GetType().Name));
		}

		// Token: 0x06001EBA RID: 7866 RVA: 0x000932FA File Offset: 0x000914FA
		internal static Exception InternalError(ADP.InternalErrorCode internalError)
		{
			return ADP.InvalidOperation(SR.Format("Internal .Net Framework Data Provider error {0}.", (int)internalError));
		}

		// Token: 0x06001EBB RID: 7867 RVA: 0x00093311 File Offset: 0x00091511
		internal static Exception DataReaderClosed([CallerMemberName] string method = "")
		{
			return ADP.InvalidOperation(SR.Format("Invalid attempt to call {0} when reader is closed.", method));
		}

		// Token: 0x06001EBC RID: 7868 RVA: 0x00093323 File Offset: 0x00091523
		internal static ArgumentOutOfRangeException InvalidSourceBufferIndex(int maxLen, long srcOffset, string parameterName)
		{
			return ADP.ArgumentOutOfRange(SR.Format("Invalid source buffer (size of {0}) offset: {1}", maxLen.ToString(CultureInfo.InvariantCulture), srcOffset.ToString(CultureInfo.InvariantCulture)), parameterName);
		}

		// Token: 0x06001EBD RID: 7869 RVA: 0x0009334D File Offset: 0x0009154D
		internal static ArgumentOutOfRangeException InvalidDestinationBufferIndex(int maxLen, int dstOffset, string parameterName)
		{
			return ADP.ArgumentOutOfRange(SR.Format("Invalid destination buffer (size of {0}) offset: {1}", maxLen.ToString(CultureInfo.InvariantCulture), dstOffset.ToString(CultureInfo.InvariantCulture)), parameterName);
		}

		// Token: 0x06001EBE RID: 7870 RVA: 0x00093377 File Offset: 0x00091577
		internal static IndexOutOfRangeException InvalidBufferSizeOrIndex(int numBytes, int bufferIndex)
		{
			return ADP.IndexOutOfRange(SR.Format("Buffer offset '{1}' plus the bytes available '{0}' is greater than the length of the passed in buffer.", numBytes.ToString(CultureInfo.InvariantCulture), bufferIndex.ToString(CultureInfo.InvariantCulture)));
		}

		// Token: 0x06001EBF RID: 7871 RVA: 0x000933A0 File Offset: 0x000915A0
		internal static Exception InvalidDataLength(long length)
		{
			return ADP.IndexOutOfRange(SR.Format("Data length '{0}' is less than 0.", length.ToString(CultureInfo.InvariantCulture)));
		}

		// Token: 0x06001EC0 RID: 7872 RVA: 0x000933BD File Offset: 0x000915BD
		internal static bool CompareInsensitiveInvariant(string strvalue, string strconst)
		{
			return CultureInfo.InvariantCulture.CompareInfo.Compare(strvalue, strconst, CompareOptions.IgnoreCase) == 0;
		}

		// Token: 0x06001EC1 RID: 7873 RVA: 0x000933D4 File Offset: 0x000915D4
		internal static int DstCompare(string strA, string strB)
		{
			return CultureInfo.CurrentCulture.CompareInfo.Compare(strA, strB, CompareOptions.IgnoreCase | CompareOptions.IgnoreKanaType | CompareOptions.IgnoreWidth);
		}

		// Token: 0x06001EC2 RID: 7874 RVA: 0x000933E9 File Offset: 0x000915E9
		internal static bool IsEmptyArray(string[] array)
		{
			return array == null || array.Length == 0;
		}

		// Token: 0x06001EC3 RID: 7875 RVA: 0x000933F8 File Offset: 0x000915F8
		internal static bool IsNull(object value)
		{
			if (value == null || DBNull.Value == value)
			{
				return true;
			}
			INullable nullable = value as INullable;
			return nullable != null && nullable.IsNull;
		}

		// Token: 0x06001EC4 RID: 7876 RVA: 0x00093424 File Offset: 0x00091624
		internal static Exception InvalidSeekOrigin(string parameterName)
		{
			return ADP.ArgumentOutOfRange("Specified SeekOrigin value is invalid.", parameterName);
		}

		// Token: 0x06001EC5 RID: 7877 RVA: 0x00093431 File Offset: 0x00091631
		internal static void SetCurrentTransaction(Transaction transaction)
		{
			Transaction.Current = transaction;
		}

		// Token: 0x06001EC6 RID: 7878 RVA: 0x00093439 File Offset: 0x00091639
		internal static Task<T> CreatedTaskWithCancellation<T>()
		{
			return Task.FromCanceled<T>(new CancellationToken(true));
		}

		// Token: 0x06001EC7 RID: 7879 RVA: 0x00093446 File Offset: 0x00091646
		internal static void TraceExceptionForCapture(Exception e)
		{
			ADP.TraceException("<comm.ADP.TraceException|ERR|CATCH> '{0}'", e);
		}

		// Token: 0x06001EC8 RID: 7880 RVA: 0x00093453 File Offset: 0x00091653
		internal static DataException Data(string message)
		{
			DataException ex = new DataException(message);
			ADP.TraceExceptionAsReturnValue(ex);
			return ex;
		}

		// Token: 0x06001EC9 RID: 7881 RVA: 0x00093461 File Offset: 0x00091661
		internal static void CheckArgumentLength(string value, string parameterName)
		{
			ADP.CheckArgumentNull(value, parameterName);
			if (value.Length == 0)
			{
				throw ADP.Argument(SR.Format("Expecting non-empty string for '{0}' parameter.", parameterName));
			}
		}

		// Token: 0x06001ECA RID: 7882 RVA: 0x00093483 File Offset: 0x00091683
		internal static void CheckArgumentLength(Array value, string parameterName)
		{
			ADP.CheckArgumentNull(value, parameterName);
			if (value.Length == 0)
			{
				throw ADP.Argument(SR.Format("Expecting non-empty array for '{0}' parameter.", parameterName));
			}
		}

		// Token: 0x06001ECB RID: 7883 RVA: 0x000934A5 File Offset: 0x000916A5
		internal static ArgumentOutOfRangeException InvalidAcceptRejectRule(AcceptRejectRule value)
		{
			return ADP.InvalidEnumerationValue(typeof(AcceptRejectRule), (int)value);
		}

		// Token: 0x06001ECC RID: 7884 RVA: 0x000934B7 File Offset: 0x000916B7
		internal static ArgumentOutOfRangeException InvalidCatalogLocation(CatalogLocation value)
		{
			return ADP.InvalidEnumerationValue(typeof(CatalogLocation), (int)value);
		}

		// Token: 0x06001ECD RID: 7885 RVA: 0x000934C9 File Offset: 0x000916C9
		internal static ArgumentOutOfRangeException InvalidConflictOptions(ConflictOption value)
		{
			return ADP.InvalidEnumerationValue(typeof(ConflictOption), (int)value);
		}

		// Token: 0x06001ECE RID: 7886 RVA: 0x000934DB File Offset: 0x000916DB
		internal static ArgumentOutOfRangeException InvalidDataRowState(DataRowState value)
		{
			return ADP.InvalidEnumerationValue(typeof(DataRowState), (int)value);
		}

		// Token: 0x06001ECF RID: 7887 RVA: 0x000934ED File Offset: 0x000916ED
		internal static ArgumentOutOfRangeException InvalidKeyRestrictionBehavior(KeyRestrictionBehavior value)
		{
			return ADP.InvalidEnumerationValue(typeof(KeyRestrictionBehavior), (int)value);
		}

		// Token: 0x06001ED0 RID: 7888 RVA: 0x000934FF File Offset: 0x000916FF
		internal static ArgumentOutOfRangeException InvalidLoadOption(LoadOption value)
		{
			return ADP.InvalidEnumerationValue(typeof(LoadOption), (int)value);
		}

		// Token: 0x06001ED1 RID: 7889 RVA: 0x00093511 File Offset: 0x00091711
		internal static ArgumentOutOfRangeException InvalidMissingMappingAction(MissingMappingAction value)
		{
			return ADP.InvalidEnumerationValue(typeof(MissingMappingAction), (int)value);
		}

		// Token: 0x06001ED2 RID: 7890 RVA: 0x00093523 File Offset: 0x00091723
		internal static ArgumentOutOfRangeException InvalidMissingSchemaAction(MissingSchemaAction value)
		{
			return ADP.InvalidEnumerationValue(typeof(MissingSchemaAction), (int)value);
		}

		// Token: 0x06001ED3 RID: 7891 RVA: 0x00093535 File Offset: 0x00091735
		internal static ArgumentOutOfRangeException InvalidRule(Rule value)
		{
			return ADP.InvalidEnumerationValue(typeof(Rule), (int)value);
		}

		// Token: 0x06001ED4 RID: 7892 RVA: 0x00093547 File Offset: 0x00091747
		internal static ArgumentOutOfRangeException InvalidSchemaType(SchemaType value)
		{
			return ADP.InvalidEnumerationValue(typeof(SchemaType), (int)value);
		}

		// Token: 0x06001ED5 RID: 7893 RVA: 0x00093559 File Offset: 0x00091759
		internal static ArgumentOutOfRangeException InvalidStatementType(StatementType value)
		{
			return ADP.InvalidEnumerationValue(typeof(StatementType), (int)value);
		}

		// Token: 0x06001ED6 RID: 7894 RVA: 0x0009356B File Offset: 0x0009176B
		internal static ArgumentOutOfRangeException InvalidUpdateStatus(UpdateStatus value)
		{
			return ADP.InvalidEnumerationValue(typeof(UpdateStatus), (int)value);
		}

		// Token: 0x06001ED7 RID: 7895 RVA: 0x0009357D File Offset: 0x0009177D
		internal static ArgumentOutOfRangeException NotSupportedStatementType(StatementType value, string method)
		{
			return ADP.NotSupportedEnumerationValue(typeof(StatementType), value.ToString(), method);
		}

		// Token: 0x06001ED8 RID: 7896 RVA: 0x0009359C File Offset: 0x0009179C
		internal static ArgumentException InvalidKeyname(string parameterName)
		{
			return ADP.Argument("Invalid keyword, contain one or more of 'no characters', 'control characters', 'leading or trailing whitespace' or 'leading semicolons'.", parameterName);
		}

		// Token: 0x06001ED9 RID: 7897 RVA: 0x000935A9 File Offset: 0x000917A9
		internal static ArgumentException InvalidValue(string parameterName)
		{
			return ADP.Argument("The value contains embedded nulls (\\\\u0000).", parameterName);
		}

		// Token: 0x06001EDA RID: 7898 RVA: 0x000935B6 File Offset: 0x000917B6
		internal static Exception WrongType(Type got, Type expected)
		{
			return ADP.Argument(SR.Format("Expecting argument of type {1}, but received type {0}.", got.ToString(), expected.ToString()));
		}

		// Token: 0x06001EDB RID: 7899 RVA: 0x000935D3 File Offset: 0x000917D3
		internal static Exception CollectionUniqueValue(Type itemType, string propertyName, string propertyValue)
		{
			return ADP.Argument(SR.Format("The {0}.{1} is required to be unique, '{2}' already exists in the collection.", itemType.Name, propertyName, propertyValue));
		}

		// Token: 0x06001EDC RID: 7900 RVA: 0x000935EC File Offset: 0x000917EC
		internal static InvalidOperationException MissingSelectCommand(string method)
		{
			return ADP.Provider(SR.Format("The SelectCommand property has not been initialized before calling '{0}'.", method));
		}

		// Token: 0x06001EDD RID: 7901 RVA: 0x00092F8A File Offset: 0x0009118A
		private static InvalidOperationException DataMapping(string error)
		{
			return ADP.InvalidOperation(error);
		}

		// Token: 0x06001EDE RID: 7902 RVA: 0x000935FE File Offset: 0x000917FE
		internal static InvalidOperationException ColumnSchemaExpression(string srcColumn, string cacheColumn)
		{
			return ADP.DataMapping(SR.Format("The column mapping from SourceColumn '{0}' failed because the DataColumn '{1}' is a computed column.", srcColumn, cacheColumn));
		}

		// Token: 0x06001EDF RID: 7903 RVA: 0x00093611 File Offset: 0x00091811
		internal static InvalidOperationException ColumnSchemaMismatch(string srcColumn, Type srcType, DataColumn column)
		{
			return ADP.DataMapping(SR.Format("Inconvertible type mismatch between SourceColumn '{0}' of {1} and the DataColumn '{2}' of {3}.", new object[]
			{
				srcColumn,
				srcType.Name,
				column.ColumnName,
				column.DataType.Name
			}));
		}

		// Token: 0x06001EE0 RID: 7904 RVA: 0x0009364C File Offset: 0x0009184C
		internal static InvalidOperationException ColumnSchemaMissing(string cacheColumn, string tableName, string srcColumn)
		{
			if (string.IsNullOrEmpty(tableName))
			{
				return ADP.InvalidOperation(SR.Format("Missing the DataColumn '{0}' for the SourceColumn '{2}'.", cacheColumn, tableName, srcColumn));
			}
			return ADP.DataMapping(SR.Format("Missing the DataColumn '{0}' in the DataTable '{1}' for the SourceColumn '{2}'.", cacheColumn, tableName, srcColumn));
		}

		// Token: 0x06001EE1 RID: 7905 RVA: 0x0009367B File Offset: 0x0009187B
		internal static InvalidOperationException MissingColumnMapping(string srcColumn)
		{
			return ADP.DataMapping(SR.Format("Missing SourceColumn mapping for '{0}'.", srcColumn));
		}

		// Token: 0x06001EE2 RID: 7906 RVA: 0x0009368D File Offset: 0x0009188D
		internal static InvalidOperationException MissingTableSchema(string cacheTable, string srcTable)
		{
			return ADP.DataMapping(SR.Format("Missing the '{0}' DataTable for the '{1}' SourceTable.", cacheTable, srcTable));
		}

		// Token: 0x06001EE3 RID: 7907 RVA: 0x000936A0 File Offset: 0x000918A0
		internal static InvalidOperationException MissingTableMapping(string srcTable)
		{
			return ADP.DataMapping(SR.Format("Missing SourceTable mapping: '{0}'", srcTable));
		}

		// Token: 0x06001EE4 RID: 7908 RVA: 0x000936B2 File Offset: 0x000918B2
		internal static InvalidOperationException MissingTableMappingDestination(string dstTable)
		{
			return ADP.DataMapping(SR.Format("Missing TableMapping when TableMapping.DataSetTable='{0}'.", dstTable));
		}

		// Token: 0x06001EE5 RID: 7909 RVA: 0x000936C4 File Offset: 0x000918C4
		internal static Exception InvalidSourceColumn(string parameter)
		{
			return ADP.Argument("SourceColumn is required to be a non-empty string.", parameter);
		}

		// Token: 0x06001EE6 RID: 7910 RVA: 0x000936D1 File Offset: 0x000918D1
		internal static Exception ColumnsAddNullAttempt(string parameter)
		{
			return ADP.CollectionNullValue(parameter, typeof(DataColumnMappingCollection), typeof(DataColumnMapping));
		}

		// Token: 0x06001EE7 RID: 7911 RVA: 0x000936ED File Offset: 0x000918ED
		internal static Exception ColumnsDataSetColumn(string cacheColumn)
		{
			return ADP.CollectionIndexString(typeof(DataColumnMapping), "DataSetColumn", cacheColumn, typeof(DataColumnMappingCollection));
		}

		// Token: 0x06001EE8 RID: 7912 RVA: 0x0009370E File Offset: 0x0009190E
		internal static Exception ColumnsIndexInt32(int index, IColumnMappingCollection collection)
		{
			return ADP.CollectionIndexInt32(index, collection.GetType(), collection.Count);
		}

		// Token: 0x06001EE9 RID: 7913 RVA: 0x00093722 File Offset: 0x00091922
		internal static Exception ColumnsIndexSource(string srcColumn)
		{
			return ADP.CollectionIndexString(typeof(DataColumnMapping), "SourceColumn", srcColumn, typeof(DataColumnMappingCollection));
		}

		// Token: 0x06001EEA RID: 7914 RVA: 0x00093743 File Offset: 0x00091943
		internal static Exception ColumnsIsNotParent(ICollection collection)
		{
			return ADP.ParametersIsNotParent(typeof(DataColumnMapping), collection);
		}

		// Token: 0x06001EEB RID: 7915 RVA: 0x00093755 File Offset: 0x00091955
		internal static Exception ColumnsIsParent(ICollection collection)
		{
			return ADP.ParametersIsParent(typeof(DataColumnMapping), collection);
		}

		// Token: 0x06001EEC RID: 7916 RVA: 0x00093767 File Offset: 0x00091967
		internal static Exception ColumnsUniqueSourceColumn(string srcColumn)
		{
			return ADP.CollectionUniqueValue(typeof(DataColumnMapping), "SourceColumn", srcColumn);
		}

		// Token: 0x06001EED RID: 7917 RVA: 0x0009377E File Offset: 0x0009197E
		internal static Exception NotADataColumnMapping(object value)
		{
			return ADP.CollectionInvalidType(typeof(DataColumnMappingCollection), typeof(DataColumnMapping), value);
		}

		// Token: 0x06001EEE RID: 7918 RVA: 0x0009379A File Offset: 0x0009199A
		internal static Exception InvalidSourceTable(string parameter)
		{
			return ADP.Argument("SourceTable is required to be a non-empty string", parameter);
		}

		// Token: 0x06001EEF RID: 7919 RVA: 0x000937A7 File Offset: 0x000919A7
		internal static Exception TablesAddNullAttempt(string parameter)
		{
			return ADP.CollectionNullValue(parameter, typeof(DataTableMappingCollection), typeof(DataTableMapping));
		}

		// Token: 0x06001EF0 RID: 7920 RVA: 0x000937C3 File Offset: 0x000919C3
		internal static Exception TablesDataSetTable(string cacheTable)
		{
			return ADP.CollectionIndexString(typeof(DataTableMapping), "DataSetTable", cacheTable, typeof(DataTableMappingCollection));
		}

		// Token: 0x06001EF1 RID: 7921 RVA: 0x0009370E File Offset: 0x0009190E
		internal static Exception TablesIndexInt32(int index, ITableMappingCollection collection)
		{
			return ADP.CollectionIndexInt32(index, collection.GetType(), collection.Count);
		}

		// Token: 0x06001EF2 RID: 7922 RVA: 0x000937E4 File Offset: 0x000919E4
		internal static Exception TablesIsNotParent(ICollection collection)
		{
			return ADP.ParametersIsNotParent(typeof(DataTableMapping), collection);
		}

		// Token: 0x06001EF3 RID: 7923 RVA: 0x000937F6 File Offset: 0x000919F6
		internal static Exception TablesIsParent(ICollection collection)
		{
			return ADP.ParametersIsParent(typeof(DataTableMapping), collection);
		}

		// Token: 0x06001EF4 RID: 7924 RVA: 0x00093808 File Offset: 0x00091A08
		internal static Exception TablesSourceIndex(string srcTable)
		{
			return ADP.CollectionIndexString(typeof(DataTableMapping), "SourceTable", srcTable, typeof(DataTableMappingCollection));
		}

		// Token: 0x06001EF5 RID: 7925 RVA: 0x00093829 File Offset: 0x00091A29
		internal static Exception TablesUniqueSourceTable(string srcTable)
		{
			return ADP.CollectionUniqueValue(typeof(DataTableMapping), "SourceTable", srcTable);
		}

		// Token: 0x06001EF6 RID: 7926 RVA: 0x00093840 File Offset: 0x00091A40
		internal static Exception NotADataTableMapping(object value)
		{
			return ADP.CollectionInvalidType(typeof(DataTableMappingCollection), typeof(DataTableMapping), value);
		}

		// Token: 0x06001EF7 RID: 7927 RVA: 0x0009385C File Offset: 0x00091A5C
		internal static InvalidOperationException UpdateConnectionRequired(StatementType statementType, bool isRowUpdatingCommand)
		{
			string error;
			if (!isRowUpdatingCommand)
			{
				switch (statementType)
				{
				case StatementType.Insert:
					error = "Update requires the InsertCommand to have a connection object. The Connection property of the InsertCommand has not been initialized.";
					goto IL_4A;
				case StatementType.Update:
					error = "Update requires the UpdateCommand to have a connection object. The Connection property of the UpdateCommand has not been initialized.";
					goto IL_4A;
				case StatementType.Delete:
					error = "Update requires the DeleteCommand to have a connection object. The Connection property of the DeleteCommand has not been initialized.";
					goto IL_4A;
				}
				throw ADP.InvalidStatementType(statementType);
			}
			error = "Update requires the command clone to have a connection object. The Connection property of the command clone has not been initialized.";
			IL_4A:
			return ADP.InvalidOperation(error);
		}

		// Token: 0x06001EF8 RID: 7928 RVA: 0x000938B9 File Offset: 0x00091AB9
		internal static InvalidOperationException ConnectionRequired_Res(string method)
		{
			return ADP.InvalidOperation("ADP_ConnectionRequired_" + method);
		}

		// Token: 0x06001EF9 RID: 7929 RVA: 0x000938CC File Offset: 0x00091ACC
		internal static InvalidOperationException UpdateOpenConnectionRequired(StatementType statementType, bool isRowUpdatingCommand, ConnectionState state)
		{
			string resourceFormat;
			if (isRowUpdatingCommand)
			{
				resourceFormat = "Update requires the updating command to have an open connection object. {1}";
			}
			else
			{
				switch (statementType)
				{
				case StatementType.Insert:
					resourceFormat = "Update requires the {0}Command to have an open connection object. {1}";
					break;
				case StatementType.Update:
					resourceFormat = "Update requires the {0}Command to have an open connection object. {1}";
					break;
				case StatementType.Delete:
					resourceFormat = "Update requires the {0}Command to have an open connection object. {1}";
					break;
				default:
					throw ADP.InvalidStatementType(statementType);
				}
			}
			return ADP.InvalidOperation(SR.Format(resourceFormat, ADP.ConnectionStateMsg(state)));
		}

		// Token: 0x06001EFA RID: 7930 RVA: 0x0009392A File Offset: 0x00091B2A
		internal static ArgumentException UnwantedStatementType(StatementType statementType)
		{
			return ADP.Argument(SR.Format("The StatementType {0} is not expected here.", statementType.ToString()));
		}

		// Token: 0x06001EFB RID: 7931 RVA: 0x00093948 File Offset: 0x00091B48
		internal static Exception FillSchemaRequiresSourceTableName(string parameter)
		{
			return ADP.Argument("FillSchema: expected a non-empty string for the SourceTable name.", parameter);
		}

		// Token: 0x06001EFC RID: 7932 RVA: 0x00093955 File Offset: 0x00091B55
		internal static Exception InvalidMaxRecords(string parameter, int max)
		{
			return ADP.Argument(SR.Format("The MaxRecords value of {0} is invalid; the value must be >= 0.", max.ToString(CultureInfo.InvariantCulture)), parameter);
		}

		// Token: 0x06001EFD RID: 7933 RVA: 0x00093973 File Offset: 0x00091B73
		internal static Exception InvalidStartRecord(string parameter, int start)
		{
			return ADP.Argument(SR.Format("The StartRecord value of {0} is invalid; the value must be >= 0.", start.ToString(CultureInfo.InvariantCulture)), parameter);
		}

		// Token: 0x06001EFE RID: 7934 RVA: 0x00093991 File Offset: 0x00091B91
		internal static Exception FillRequires(string parameter)
		{
			return ADP.ArgumentNull(parameter);
		}

		// Token: 0x06001EFF RID: 7935 RVA: 0x00093999 File Offset: 0x00091B99
		internal static Exception FillRequiresSourceTableName(string parameter)
		{
			return ADP.Argument("Fill: expected a non-empty string for the SourceTable name.", parameter);
		}

		// Token: 0x06001F00 RID: 7936 RVA: 0x000939A6 File Offset: 0x00091BA6
		internal static Exception FillChapterAutoIncrement()
		{
			return ADP.InvalidOperation("Hierarchical chapter columns must map to an AutoIncrement DataColumn.");
		}

		// Token: 0x06001F01 RID: 7937 RVA: 0x000939B2 File Offset: 0x00091BB2
		internal static InvalidOperationException MissingDataReaderFieldType(int index)
		{
			return ADP.DataAdapter(SR.Format("DataReader.GetFieldType({0}) returned null.", index));
		}

		// Token: 0x06001F02 RID: 7938 RVA: 0x000939C9 File Offset: 0x00091BC9
		internal static InvalidOperationException OnlyOneTableForStartRecordOrMaxRecords()
		{
			return ADP.DataAdapter("Only specify one item in the dataTables array when using non-zero values for startRecords or maxRecords.");
		}

		// Token: 0x06001F03 RID: 7939 RVA: 0x00093991 File Offset: 0x00091B91
		internal static ArgumentNullException UpdateRequiresNonNullDataSet(string parameter)
		{
			return ADP.ArgumentNull(parameter);
		}

		// Token: 0x06001F04 RID: 7940 RVA: 0x000939D5 File Offset: 0x00091BD5
		internal static InvalidOperationException UpdateRequiresSourceTable(string defaultSrcTableName)
		{
			return ADP.InvalidOperation(SR.Format("Update unable to find TableMapping['{0}'] or DataTable '{0}'.", defaultSrcTableName));
		}

		// Token: 0x06001F05 RID: 7941 RVA: 0x000939E7 File Offset: 0x00091BE7
		internal static InvalidOperationException UpdateRequiresSourceTableName(string srcTable)
		{
			return ADP.InvalidOperation(SR.Format("Update: expected a non-empty SourceTable name.", srcTable));
		}

		// Token: 0x06001F06 RID: 7942 RVA: 0x00093991 File Offset: 0x00091B91
		internal static ArgumentNullException UpdateRequiresDataTable(string parameter)
		{
			return ADP.ArgumentNull(parameter);
		}

		// Token: 0x06001F07 RID: 7943 RVA: 0x000939FC File Offset: 0x00091BFC
		internal static Exception UpdateConcurrencyViolation(StatementType statementType, int affected, int expected, DataRow[] dataRows)
		{
			string resourceFormat;
			switch (statementType)
			{
			case StatementType.Update:
				resourceFormat = "Concurrency violation: the UpdateCommand affected {0} of the expected {1} records.";
				break;
			case StatementType.Delete:
				resourceFormat = "Concurrency violation: the DeleteCommand affected {0} of the expected {1} records.";
				break;
			case StatementType.Batch:
				resourceFormat = "Concurrency violation: the batched command affected {0} of the expected {1} records.";
				break;
			default:
				throw ADP.InvalidStatementType(statementType);
			}
			DBConcurrencyException ex = new DBConcurrencyException(SR.Format(resourceFormat, affected.ToString(CultureInfo.InvariantCulture), expected.ToString(CultureInfo.InvariantCulture)), null, dataRows);
			ADP.TraceExceptionAsReturnValue(ex);
			return ex;
		}

		// Token: 0x06001F08 RID: 7944 RVA: 0x00093A6C File Offset: 0x00091C6C
		internal static InvalidOperationException UpdateRequiresCommand(StatementType statementType, bool isRowUpdatingCommand)
		{
			string error;
			if (isRowUpdatingCommand)
			{
				error = "Update requires the command clone to be valid.";
			}
			else
			{
				switch (statementType)
				{
				case StatementType.Select:
					error = "Auto SQL generation during Update requires a valid SelectCommand.";
					break;
				case StatementType.Insert:
					error = "Update requires a valid InsertCommand when passed DataRow collection with new rows.";
					break;
				case StatementType.Update:
					error = "Update requires a valid UpdateCommand when passed DataRow collection with modified rows.";
					break;
				case StatementType.Delete:
					error = "Update requires a valid DeleteCommand when passed DataRow collection with deleted rows.";
					break;
				default:
					throw ADP.InvalidStatementType(statementType);
				}
			}
			return ADP.InvalidOperation(error);
		}

		// Token: 0x06001F09 RID: 7945 RVA: 0x00093AC9 File Offset: 0x00091CC9
		internal static ArgumentException UpdateMismatchRowTable(int i)
		{
			return ADP.Argument(SR.Format("DataRow[{0}] is from a different DataTable than DataRow[0].", i.ToString(CultureInfo.InvariantCulture)));
		}

		// Token: 0x06001F0A RID: 7946 RVA: 0x00093AE6 File Offset: 0x00091CE6
		internal static DataException RowUpdatedErrors()
		{
			return ADP.Data("RowUpdatedEvent: Errors occurred; no additional is information available.");
		}

		// Token: 0x06001F0B RID: 7947 RVA: 0x00093AF2 File Offset: 0x00091CF2
		internal static DataException RowUpdatingErrors()
		{
			return ADP.Data("RowUpdatingEvent: Errors occurred; no additional is information available.");
		}

		// Token: 0x06001F0C RID: 7948 RVA: 0x00093AFE File Offset: 0x00091CFE
		internal static InvalidOperationException ResultsNotAllowedDuringBatch()
		{
			return ADP.DataAdapter("When batching, the command's UpdatedRowSource property value of UpdateRowSource.FirstReturnedRecord or UpdateRowSource.Both is invalid.");
		}

		// Token: 0x06001F0D RID: 7949 RVA: 0x00093B0A File Offset: 0x00091D0A
		internal static InvalidOperationException DynamicSQLJoinUnsupported()
		{
			return ADP.InvalidOperation("Dynamic SQL generation is not supported against multiple base tables.");
		}

		// Token: 0x06001F0E RID: 7950 RVA: 0x00093B16 File Offset: 0x00091D16
		internal static InvalidOperationException DynamicSQLNoTableInfo()
		{
			return ADP.InvalidOperation("Dynamic SQL generation is not supported against a SelectCommand that does not return any base table information.");
		}

		// Token: 0x06001F0F RID: 7951 RVA: 0x00093B22 File Offset: 0x00091D22
		internal static InvalidOperationException DynamicSQLNoKeyInfoDelete()
		{
			return ADP.InvalidOperation("Dynamic SQL generation for the DeleteCommand is not supported against a SelectCommand that does not return any key column information.");
		}

		// Token: 0x06001F10 RID: 7952 RVA: 0x00093B2E File Offset: 0x00091D2E
		internal static InvalidOperationException DynamicSQLNoKeyInfoUpdate()
		{
			return ADP.InvalidOperation("Dynamic SQL generation for the UpdateCommand is not supported against a SelectCommand that does not return any key column information.");
		}

		// Token: 0x06001F11 RID: 7953 RVA: 0x00093B3A File Offset: 0x00091D3A
		internal static InvalidOperationException DynamicSQLNoKeyInfoRowVersionDelete()
		{
			return ADP.InvalidOperation("Dynamic SQL generation for the DeleteCommand is not supported against a SelectCommand that does not contain a row version column.");
		}

		// Token: 0x06001F12 RID: 7954 RVA: 0x00093B46 File Offset: 0x00091D46
		internal static InvalidOperationException DynamicSQLNoKeyInfoRowVersionUpdate()
		{
			return ADP.InvalidOperation("Dynamic SQL generation for the UpdateCommand is not supported against a SelectCommand that does not contain a row version column.");
		}

		// Token: 0x06001F13 RID: 7955 RVA: 0x00093B52 File Offset: 0x00091D52
		internal static InvalidOperationException DynamicSQLNestedQuote(string name, string quote)
		{
			return ADP.InvalidOperation(SR.Format("Dynamic SQL generation not supported against table names '{0}' that contain the QuotePrefix or QuoteSuffix character '{1}'.", name, quote));
		}

		// Token: 0x06001F14 RID: 7956 RVA: 0x00093B65 File Offset: 0x00091D65
		internal static InvalidOperationException NoQuoteChange()
		{
			return ADP.InvalidOperation("The QuotePrefix and QuoteSuffix properties cannot be changed once an Insert, Update, or Delete command has been generated.");
		}

		// Token: 0x06001F15 RID: 7957 RVA: 0x00093B71 File Offset: 0x00091D71
		internal static InvalidOperationException MissingSourceCommand()
		{
			return ADP.InvalidOperation("The DataAdapter.SelectCommand property needs to be initialized.");
		}

		// Token: 0x06001F16 RID: 7958 RVA: 0x00093B7D File Offset: 0x00091D7D
		internal static InvalidOperationException MissingSourceCommandConnection()
		{
			return ADP.InvalidOperation("The DataAdapter.SelectCommand.Connection property needs to be initialized;");
		}

		// Token: 0x06001F17 RID: 7959 RVA: 0x00093B8C File Offset: 0x00091D8C
		internal static DataRow[] SelectAdapterRows(DataTable dataTable, bool sorted)
		{
			int num = 0;
			int num2 = 0;
			int num3 = 0;
			DataRowCollection rows = dataTable.Rows;
			foreach (object obj in rows)
			{
				DataRowState rowState = ((DataRow)obj).RowState;
				if (rowState != DataRowState.Added)
				{
					if (rowState != DataRowState.Deleted)
					{
						if (rowState == DataRowState.Modified)
						{
							num3++;
						}
					}
					else
					{
						num2++;
					}
				}
				else
				{
					num++;
				}
			}
			DataRow[] array = new DataRow[num + num2 + num3];
			if (sorted)
			{
				num3 = num + num2;
				num2 = num;
				num = 0;
				using (IEnumerator enumerator = rows.GetEnumerator())
				{
					while (enumerator.MoveNext())
					{
						object obj2 = enumerator.Current;
						DataRow dataRow = (DataRow)obj2;
						DataRowState rowState = dataRow.RowState;
						if (rowState != DataRowState.Added)
						{
							if (rowState != DataRowState.Deleted)
							{
								if (rowState == DataRowState.Modified)
								{
									array[num3++] = dataRow;
								}
							}
							else
							{
								array[num2++] = dataRow;
							}
						}
						else
						{
							array[num++] = dataRow;
						}
					}
					return array;
				}
			}
			int num4 = 0;
			foreach (object obj3 in rows)
			{
				DataRow dataRow2 = (DataRow)obj3;
				if ((dataRow2.RowState & (DataRowState.Added | DataRowState.Deleted | DataRowState.Modified)) != (DataRowState)0)
				{
					array[num4++] = dataRow2;
					if (num4 == array.Length)
					{
						break;
					}
				}
			}
			return array;
		}

		// Token: 0x06001F18 RID: 7960 RVA: 0x00093D18 File Offset: 0x00091F18
		internal static void BuildSchemaTableInfoTableNames(string[] columnNameArray)
		{
			Dictionary<string, int> dictionary = new Dictionary<string, int>(columnNameArray.Length);
			int num = columnNameArray.Length;
			int num2 = columnNameArray.Length - 1;
			while (0 <= num2)
			{
				string text = columnNameArray[num2];
				if (text != null && 0 < text.Length)
				{
					text = text.ToLower(CultureInfo.InvariantCulture);
					int val;
					if (dictionary.TryGetValue(text, out val))
					{
						num = Math.Min(num, val);
					}
					dictionary[text] = num2;
				}
				else
				{
					columnNameArray[num2] = string.Empty;
					num = num2;
				}
				num2--;
			}
			int uniqueIndex = 1;
			for (int i = num; i < columnNameArray.Length; i++)
			{
				string text2 = columnNameArray[i];
				if (text2.Length == 0)
				{
					columnNameArray[i] = "Column";
					uniqueIndex = ADP.GenerateUniqueName(dictionary, ref columnNameArray[i], i, uniqueIndex);
				}
				else
				{
					text2 = text2.ToLower(CultureInfo.InvariantCulture);
					if (i != dictionary[text2])
					{
						ADP.GenerateUniqueName(dictionary, ref columnNameArray[i], i, 1);
					}
				}
			}
		}

		// Token: 0x06001F19 RID: 7961 RVA: 0x00093DFC File Offset: 0x00091FFC
		private static int GenerateUniqueName(Dictionary<string, int> hash, ref string columnName, int index, int uniqueIndex)
		{
			string text;
			for (;;)
			{
				text = columnName + uniqueIndex.ToString(CultureInfo.InvariantCulture);
				string key = text.ToLower(CultureInfo.InvariantCulture);
				if (hash.TryAdd(key, index))
				{
					break;
				}
				uniqueIndex++;
			}
			columnName = text;
			return uniqueIndex;
		}

		// Token: 0x06001F1A RID: 7962 RVA: 0x00093E40 File Offset: 0x00092040
		internal static int SrcCompare(string strA, string strB)
		{
			if (!(strA == strB))
			{
				return 1;
			}
			return 0;
		}

		// Token: 0x06001F1B RID: 7963 RVA: 0x00093E50 File Offset: 0x00092050
		internal static Exception ExceptionWithStackTrace(Exception e)
		{
			try
			{
				throw e;
			}
			catch (Exception result)
			{
			}
			Exception result;
			return result;
		}

		// Token: 0x06001F1C RID: 7964 RVA: 0x00093E74 File Offset: 0x00092074
		internal static IndexOutOfRangeException IndexOutOfRange(int value)
		{
			return new IndexOutOfRangeException(value.ToString(CultureInfo.InvariantCulture));
		}

		// Token: 0x06001F1D RID: 7965 RVA: 0x00093E87 File Offset: 0x00092087
		internal static IndexOutOfRangeException IndexOutOfRange()
		{
			return new IndexOutOfRangeException();
		}

		// Token: 0x06001F1E RID: 7966 RVA: 0x00093E8E File Offset: 0x0009208E
		internal static TimeoutException TimeoutException(string error)
		{
			return new TimeoutException(error);
		}

		// Token: 0x06001F1F RID: 7967 RVA: 0x00093E96 File Offset: 0x00092096
		internal static InvalidOperationException InvalidOperation(string error, Exception inner)
		{
			return new InvalidOperationException(error, inner);
		}

		// Token: 0x06001F20 RID: 7968 RVA: 0x00093E9F File Offset: 0x0009209F
		internal static OverflowException Overflow(string error)
		{
			return ADP.Overflow(error, null);
		}

		// Token: 0x06001F21 RID: 7969 RVA: 0x00093EA8 File Offset: 0x000920A8
		internal static OverflowException Overflow(string error, Exception inner)
		{
			return new OverflowException(error, inner);
		}

		// Token: 0x06001F22 RID: 7970 RVA: 0x00093EB1 File Offset: 0x000920B1
		internal static PlatformNotSupportedException DbTypeNotSupported(string dbType)
		{
			return new PlatformNotSupportedException(SR.GetString("Type {0} is not supported on this platform.", new object[]
			{
				dbType
			}));
		}

		// Token: 0x06001F23 RID: 7971 RVA: 0x00093ECC File Offset: 0x000920CC
		internal static InvalidCastException InvalidCast()
		{
			return new InvalidCastException();
		}

		// Token: 0x06001F24 RID: 7972 RVA: 0x00093ED3 File Offset: 0x000920D3
		internal static IOException IO(string error)
		{
			return new IOException(error);
		}

		// Token: 0x06001F25 RID: 7973 RVA: 0x00093EDB File Offset: 0x000920DB
		internal static IOException IO(string error, Exception inner)
		{
			return new IOException(error, inner);
		}

		// Token: 0x06001F26 RID: 7974 RVA: 0x00093EE4 File Offset: 0x000920E4
		internal static ObjectDisposedException ObjectDisposed(object instance)
		{
			return new ObjectDisposedException(instance.GetType().Name);
		}

		// Token: 0x06001F27 RID: 7975 RVA: 0x00093EF6 File Offset: 0x000920F6
		internal static Exception DataTableDoesNotExist(string collectionName)
		{
			return ADP.Argument(SR.GetString("The collection '{0}' is missing from the metadata XML.", new object[]
			{
				collectionName
			}));
		}

		// Token: 0x06001F28 RID: 7976 RVA: 0x00093F11 File Offset: 0x00092111
		internal static InvalidOperationException MethodCalledTwice(string method)
		{
			return new InvalidOperationException(SR.GetString("The method '{0}' cannot be called more than once for the same execution.", new object[]
			{
				method
			}));
		}

		// Token: 0x06001F29 RID: 7977 RVA: 0x00093F2C File Offset: 0x0009212C
		internal static ArgumentOutOfRangeException InvalidCommandType(CommandType value)
		{
			return ADP.InvalidEnumerationValue(typeof(CommandType), (int)value);
		}

		// Token: 0x06001F2A RID: 7978 RVA: 0x00093F3E File Offset: 0x0009213E
		internal static ArgumentOutOfRangeException InvalidIsolationLevel(IsolationLevel value)
		{
			return ADP.InvalidEnumerationValue(typeof(IsolationLevel), (int)value);
		}

		// Token: 0x06001F2B RID: 7979 RVA: 0x00093F50 File Offset: 0x00092150
		internal static ArgumentOutOfRangeException InvalidParameterDirection(ParameterDirection value)
		{
			return ADP.InvalidEnumerationValue(typeof(ParameterDirection), (int)value);
		}

		// Token: 0x06001F2C RID: 7980 RVA: 0x00093F62 File Offset: 0x00092162
		internal static Exception TooManyRestrictions(string collectionName)
		{
			return ADP.Argument(SR.GetString("More restrictions were provided than the requested schema ('{0}') supports.", new object[]
			{
				collectionName
			}));
		}

		// Token: 0x06001F2D RID: 7981 RVA: 0x00093F7D File Offset: 0x0009217D
		internal static ArgumentOutOfRangeException InvalidUpdateRowSource(UpdateRowSource value)
		{
			return ADP.InvalidEnumerationValue(typeof(UpdateRowSource), (int)value);
		}

		// Token: 0x06001F2E RID: 7982 RVA: 0x00093F8F File Offset: 0x0009218F
		internal static ArgumentException InvalidMinMaxPoolSizeValues()
		{
			return ADP.Argument(SR.GetString("Invalid min or max pool size values, min pool size cannot be greater than the max pool size."));
		}

		// Token: 0x06001F2F RID: 7983 RVA: 0x00093FA0 File Offset: 0x000921A0
		internal static InvalidOperationException NoConnectionString()
		{
			return ADP.InvalidOperation(SR.GetString("The ConnectionString property has not been initialized."));
		}

		// Token: 0x06001F30 RID: 7984 RVA: 0x00093FB1 File Offset: 0x000921B1
		internal static Exception MethodNotImplemented([CallerMemberName] string methodName = "")
		{
			return NotImplemented.ByDesignWithMessage(methodName);
		}

		// Token: 0x06001F31 RID: 7985 RVA: 0x00093FB9 File Offset: 0x000921B9
		internal static Exception QueryFailed(string collectionName, Exception e)
		{
			return ADP.InvalidOperation(SR.GetString("Unable to build the '{0}' collection because execution of the SQL query failed. See the inner exception for details.", new object[]
			{
				collectionName
			}), e);
		}

		// Token: 0x06001F32 RID: 7986 RVA: 0x00093FD5 File Offset: 0x000921D5
		internal static Exception InvalidConnectionOptionValueLength(string key, int limit)
		{
			return ADP.Argument(SR.GetString("The value's length for key '{0}' exceeds it's limit of '{1}'.", new object[]
			{
				key,
				limit
			}));
		}

		// Token: 0x06001F33 RID: 7987 RVA: 0x00093FF9 File Offset: 0x000921F9
		internal static Exception MissingConnectionOptionValue(string key, string requiredAdditionalKey)
		{
			return ADP.Argument(SR.GetString("Use of key '{0}' requires the key '{1}' to be present.", new object[]
			{
				key,
				requiredAdditionalKey
			}));
		}

		// Token: 0x06001F34 RID: 7988 RVA: 0x00094018 File Offset: 0x00092218
		internal static Exception PooledOpenTimeout()
		{
			return ADP.InvalidOperation(SR.GetString("Timeout expired.  The timeout period elapsed prior to obtaining a connection from the pool.  This may have occurred because all pooled connections were in use and max pool size was reached."));
		}

		// Token: 0x06001F35 RID: 7989 RVA: 0x00094029 File Offset: 0x00092229
		internal static Exception NonPooledOpenTimeout()
		{
			return ADP.TimeoutException(SR.GetString("Timeout attempting to open the connection.  The time period elapsed prior to attempting to open the connection has been exceeded.  This may have occurred because of too many simultaneous non-pooled connection attempts."));
		}

		// Token: 0x06001F36 RID: 7990 RVA: 0x0009403A File Offset: 0x0009223A
		internal static InvalidOperationException TransactionConnectionMismatch()
		{
			return ADP.Provider(SR.GetString("The transaction is either not associated with the current connection or has been completed."));
		}

		// Token: 0x06001F37 RID: 7991 RVA: 0x0009404B File Offset: 0x0009224B
		internal static InvalidOperationException TransactionRequired(string method)
		{
			return ADP.Provider(SR.GetString("{0} requires the command to have a transaction when the connection assigned to the command is in a pending local transaction.  The Transaction property of the command has not been initialized.", new object[]
			{
				method
			}));
		}

		// Token: 0x06001F38 RID: 7992 RVA: 0x00094066 File Offset: 0x00092266
		internal static Exception CommandTextRequired(string method)
		{
			return ADP.InvalidOperation(SR.GetString("{0}: CommandText property has not been initialized", new object[]
			{
				method
			}));
		}

		// Token: 0x06001F39 RID: 7993 RVA: 0x00094081 File Offset: 0x00092281
		internal static Exception NoColumns()
		{
			return ADP.Argument(SR.GetString("The schema table contains no columns."));
		}

		// Token: 0x06001F3A RID: 7994 RVA: 0x00094092 File Offset: 0x00092292
		internal static InvalidOperationException ConnectionRequired(string method)
		{
			return ADP.InvalidOperation(SR.GetString("{0}: Connection property has not been initialized.", new object[]
			{
				method
			}));
		}

		// Token: 0x06001F3B RID: 7995 RVA: 0x000940AD File Offset: 0x000922AD
		internal static InvalidOperationException OpenConnectionRequired(string method, ConnectionState state)
		{
			return ADP.InvalidOperation(SR.GetString("{0} requires an open and available Connection. {1}", new object[]
			{
				method,
				ADP.ConnectionStateMsg(state)
			}));
		}

		// Token: 0x06001F3C RID: 7996 RVA: 0x000940D1 File Offset: 0x000922D1
		internal static Exception OpenReaderExists()
		{
			return ADP.OpenReaderExists(null);
		}

		// Token: 0x06001F3D RID: 7997 RVA: 0x000940D9 File Offset: 0x000922D9
		internal static Exception OpenReaderExists(Exception e)
		{
			return ADP.InvalidOperation(SR.GetString("There is already an open DataReader associated with this Command which must be closed first."), e);
		}

		// Token: 0x06001F3E RID: 7998 RVA: 0x000940EB File Offset: 0x000922EB
		internal static Exception NonSeqByteAccess(long badIndex, long currIndex, string method)
		{
			return ADP.InvalidOperation(SR.GetString("Invalid {2} attempt at dataIndex '{0}'.  With CommandBehavior.SequentialAccess, you may only read from dataIndex '{1}' or greater.", new object[]
			{
				badIndex.ToString(CultureInfo.InvariantCulture),
				currIndex.ToString(CultureInfo.InvariantCulture),
				method
			}));
		}

		// Token: 0x06001F3F RID: 7999 RVA: 0x00094124 File Offset: 0x00092324
		internal static Exception InvalidXml()
		{
			return ADP.Argument(SR.GetString("The metadata XML is invalid."));
		}

		// Token: 0x06001F40 RID: 8000 RVA: 0x00094135 File Offset: 0x00092335
		internal static Exception NegativeParameter(string parameterName)
		{
			return ADP.InvalidOperation(SR.GetString("Invalid value for argument '{0}'. The value must be greater than or equal to 0.", new object[]
			{
				parameterName
			}));
		}

		// Token: 0x06001F41 RID: 8001 RVA: 0x00094150 File Offset: 0x00092350
		internal static Exception InvalidXmlMissingColumn(string collectionName, string columnName)
		{
			return ADP.Argument(SR.GetString("The metadata XML is invalid. The {0} collection must contain a {1} column and it must be a string column.", new object[]
			{
				collectionName,
				columnName
			}));
		}

		// Token: 0x06001F42 RID: 8002 RVA: 0x0009416F File Offset: 0x0009236F
		internal static Exception InvalidMetaDataValue()
		{
			return ADP.Argument(SR.GetString("Invalid value for this metadata."));
		}

		// Token: 0x06001F43 RID: 8003 RVA: 0x00094180 File Offset: 0x00092380
		internal static InvalidOperationException NonSequentialColumnAccess(int badCol, int currCol)
		{
			return ADP.InvalidOperation(SR.GetString("Invalid attempt to read from column ordinal '{0}'.  With CommandBehavior.SequentialAccess, you may only read from column ordinal '{1}' or greater.", new object[]
			{
				badCol.ToString(CultureInfo.InvariantCulture),
				currCol.ToString(CultureInfo.InvariantCulture)
			}));
		}

		// Token: 0x06001F44 RID: 8004 RVA: 0x000941B5 File Offset: 0x000923B5
		internal static Exception InvalidXmlInvalidValue(string collectionName, string columnName)
		{
			return ADP.Argument(SR.GetString("The metadata XML is invalid. The {1} column of the {0} collection must contain a non-empty string.", new object[]
			{
				collectionName,
				columnName
			}));
		}

		// Token: 0x06001F45 RID: 8005 RVA: 0x000941D4 File Offset: 0x000923D4
		internal static Exception CollectionNameIsNotUnique(string collectionName)
		{
			return ADP.Argument(SR.GetString("There are multiple collections named '{0}'.", new object[]
			{
				collectionName
			}));
		}

		// Token: 0x06001F46 RID: 8006 RVA: 0x000941EF File Offset: 0x000923EF
		internal static Exception InvalidCommandTimeout(int value, [CallerMemberName] string property = "")
		{
			return ADP.Argument(SR.GetString("Invalid CommandTimeout value {0}; the value must be >= 0.", new object[]
			{
				value.ToString(CultureInfo.InvariantCulture)
			}), property);
		}

		// Token: 0x06001F47 RID: 8007 RVA: 0x00094216 File Offset: 0x00092416
		internal static Exception UninitializedParameterSize(int index, Type dataType)
		{
			return ADP.InvalidOperation(SR.GetString("{1}[{0}]: the Size property has an invalid size of 0.", new object[]
			{
				index.ToString(CultureInfo.InvariantCulture),
				dataType.Name
			}));
		}

		// Token: 0x06001F48 RID: 8008 RVA: 0x00094245 File Offset: 0x00092445
		internal static Exception UnableToBuildCollection(string collectionName)
		{
			return ADP.Argument(SR.GetString("Unable to build schema collection '{0}';", new object[]
			{
				collectionName
			}));
		}

		// Token: 0x06001F49 RID: 8009 RVA: 0x00094260 File Offset: 0x00092460
		internal static Exception PrepareParameterType(DbCommand cmd)
		{
			return ADP.InvalidOperation(SR.GetString("{0}.Prepare method requires all parameters to have an explicitly set type.", new object[]
			{
				cmd.GetType().Name
			}));
		}

		// Token: 0x06001F4A RID: 8010 RVA: 0x00094285 File Offset: 0x00092485
		internal static Exception UndefinedCollection(string collectionName)
		{
			return ADP.Argument(SR.GetString("The requested collection ({0}) is not defined.", new object[]
			{
				collectionName
			}));
		}

		// Token: 0x06001F4B RID: 8011 RVA: 0x000942A0 File Offset: 0x000924A0
		internal static Exception UnsupportedVersion(string collectionName)
		{
			return ADP.Argument(SR.GetString(" requested collection ({0}) is not supported by this version of the provider.", new object[]
			{
				collectionName
			}));
		}

		// Token: 0x06001F4C RID: 8012 RVA: 0x000942BB File Offset: 0x000924BB
		internal static Exception AmbigousCollectionName(string collectionName)
		{
			return ADP.Argument(SR.GetString("The collection name '{0}' matches at least two collections with the same name but with different case, but does not match any of them exactly.", new object[]
			{
				collectionName
			}));
		}

		// Token: 0x06001F4D RID: 8013 RVA: 0x000942D6 File Offset: 0x000924D6
		internal static Exception PrepareParameterSize(DbCommand cmd)
		{
			return ADP.InvalidOperation(SR.GetString("{0}.Prepare method requires all variable length parameters to have an explicitly set non-zero Size.", new object[]
			{
				cmd.GetType().Name
			}));
		}

		// Token: 0x06001F4E RID: 8014 RVA: 0x000942FB File Offset: 0x000924FB
		internal static Exception PrepareParameterScale(DbCommand cmd, string type)
		{
			return ADP.InvalidOperation(SR.GetString("{0}.Prepare method requires parameters of type '{1}' have an explicitly set Precision and Scale.", new object[]
			{
				cmd.GetType().Name,
				type
			}));
		}

		// Token: 0x06001F4F RID: 8015 RVA: 0x00094324 File Offset: 0x00092524
		internal static Exception MissingDataSourceInformationColumn()
		{
			return ADP.Argument(SR.GetString("One of the required DataSourceInformation tables columns is missing."));
		}

		// Token: 0x06001F50 RID: 8016 RVA: 0x00094335 File Offset: 0x00092535
		internal static Exception IncorrectNumberOfDataSourceInformationRows()
		{
			return ADP.Argument(SR.GetString("The DataSourceInformation table must contain exactly one row."));
		}

		// Token: 0x06001F51 RID: 8017 RVA: 0x00094346 File Offset: 0x00092546
		internal static Exception MismatchedAsyncResult(string expectedMethod, string gotMethod)
		{
			return ADP.InvalidOperation(SR.GetString("Mismatched end method call for asyncResult.  Expected call to {0} but {1} was called instead.", new object[]
			{
				expectedMethod,
				gotMethod
			}));
		}

		// Token: 0x06001F52 RID: 8018 RVA: 0x00094365 File Offset: 0x00092565
		internal static Exception ClosedConnectionError()
		{
			return ADP.InvalidOperation(SR.GetString("Invalid operation. The connection is closed."));
		}

		// Token: 0x06001F53 RID: 8019 RVA: 0x00094376 File Offset: 0x00092576
		internal static Exception ConnectionAlreadyOpen(ConnectionState state)
		{
			return ADP.InvalidOperation(SR.GetString("The connection was not closed. {0}", new object[]
			{
				ADP.ConnectionStateMsg(state)
			}));
		}

		// Token: 0x06001F54 RID: 8020 RVA: 0x00094396 File Offset: 0x00092596
		internal static Exception TransactionPresent()
		{
			return ADP.InvalidOperation(SR.GetString("Connection currently has transaction enlisted.  Finish current transaction and retry."));
		}

		// Token: 0x06001F55 RID: 8021 RVA: 0x000943A7 File Offset: 0x000925A7
		internal static Exception LocalTransactionPresent()
		{
			return ADP.InvalidOperation(SR.GetString("Cannot enlist in the transaction because a local transaction is in progress on the connection.  Finish local transaction and retry."));
		}

		// Token: 0x06001F56 RID: 8022 RVA: 0x000943B8 File Offset: 0x000925B8
		internal static Exception OpenConnectionPropertySet(string property, ConnectionState state)
		{
			return ADP.InvalidOperation(SR.GetString("Not allowed to change the '{0}' property. {1}", new object[]
			{
				property,
				ADP.ConnectionStateMsg(state)
			}));
		}

		// Token: 0x06001F57 RID: 8023 RVA: 0x000943DC File Offset: 0x000925DC
		internal static Exception EmptyDatabaseName()
		{
			return ADP.Argument(SR.GetString("Database cannot be null, the empty string, or string of only whitespace."));
		}

		// Token: 0x06001F58 RID: 8024 RVA: 0x000943ED File Offset: 0x000925ED
		internal static Exception MissingRestrictionColumn()
		{
			return ADP.Argument(SR.GetString("One or more of the required columns of the restrictions collection is missing."));
		}

		// Token: 0x06001F59 RID: 8025 RVA: 0x000943FE File Offset: 0x000925FE
		internal static Exception InternalConnectionError(ADP.ConnectionError internalError)
		{
			return ADP.InvalidOperation(SR.GetString("Internal DbConnection Error: {0}", new object[]
			{
				(int)internalError
			}));
		}

		// Token: 0x06001F5A RID: 8026 RVA: 0x0009441E File Offset: 0x0009261E
		internal static Exception InvalidConnectRetryCountValue()
		{
			return ADP.Argument(SR.GetString("Invalid ConnectRetryCount value (should be 0-255)."));
		}

		// Token: 0x06001F5B RID: 8027 RVA: 0x0009442F File Offset: 0x0009262F
		internal static Exception MissingRestrictionRow()
		{
			return ADP.Argument(SR.GetString("A restriction exists for which there is no matching row in the restrictions collection."));
		}

		// Token: 0x06001F5C RID: 8028 RVA: 0x00094440 File Offset: 0x00092640
		internal static Exception InvalidConnectRetryIntervalValue()
		{
			return ADP.Argument(SR.GetString("Invalid ConnectRetryInterval value (should be 1-60)."));
		}

		// Token: 0x06001F5D RID: 8029 RVA: 0x00094451 File Offset: 0x00092651
		internal static InvalidOperationException AsyncOperationPending()
		{
			return ADP.InvalidOperation(SR.GetString("Can not start another operation while there is an asynchronous operation pending."));
		}

		// Token: 0x06001F5E RID: 8030 RVA: 0x00094462 File Offset: 0x00092662
		internal static IOException ErrorReadingFromStream(Exception internalException)
		{
			return ADP.IO(SR.GetString("An error occurred while reading."), internalException);
		}

		// Token: 0x06001F5F RID: 8031 RVA: 0x00094474 File Offset: 0x00092674
		internal static ArgumentException InvalidDataType(string typeName)
		{
			return ADP.Argument(SR.GetString("The parameter data type of {0} is invalid.", new object[]
			{
				typeName
			}));
		}

		// Token: 0x06001F60 RID: 8032 RVA: 0x0009448F File Offset: 0x0009268F
		internal static ArgumentException UnknownDataType(Type dataType)
		{
			return ADP.Argument(SR.GetString("No mapping exists from object type {0} to a known managed provider native type.", new object[]
			{
				dataType.FullName
			}));
		}

		// Token: 0x06001F61 RID: 8033 RVA: 0x000944AF File Offset: 0x000926AF
		internal static ArgumentException DbTypeNotSupported(DbType type, Type enumtype)
		{
			return ADP.Argument(SR.GetString("No mapping exists from DbType {0} to a known {1}.", new object[]
			{
				type.ToString(),
				enumtype.Name
			}));
		}

		// Token: 0x06001F62 RID: 8034 RVA: 0x000944DF File Offset: 0x000926DF
		internal static ArgumentException InvalidOffsetValue(int value)
		{
			return ADP.Argument(SR.GetString("Invalid parameter Offset value '{0}'. The value must be greater than or equal to 0.", new object[]
			{
				value.ToString(CultureInfo.InvariantCulture)
			}));
		}

		// Token: 0x06001F63 RID: 8035 RVA: 0x00094505 File Offset: 0x00092705
		internal static ArgumentException InvalidSizeValue(int value)
		{
			return ADP.Argument(SR.GetString("Invalid parameter Size value '{0}'. The value must be greater than or equal to 0.", new object[]
			{
				value.ToString(CultureInfo.InvariantCulture)
			}));
		}

		// Token: 0x06001F64 RID: 8036 RVA: 0x0009452B File Offset: 0x0009272B
		internal static ArgumentException ParameterValueOutOfRange(decimal value)
		{
			return ADP.Argument(SR.GetString("Parameter value '{0}' is out of range.", new object[]
			{
				value.ToString(null)
			}));
		}

		// Token: 0x06001F65 RID: 8037 RVA: 0x0009454D File Offset: 0x0009274D
		internal static ArgumentException ParameterValueOutOfRange(SqlDecimal value)
		{
			return ADP.Argument(SR.GetString("Parameter value '{0}' is out of range.", new object[]
			{
				value.ToString()
			}));
		}

		// Token: 0x06001F66 RID: 8038 RVA: 0x00094574 File Offset: 0x00092774
		internal static ArgumentException VersionDoesNotSupportDataType(string typeName)
		{
			return ADP.Argument(SR.GetString("The version of SQL Server in use does not support datatype '{0}'.", new object[]
			{
				typeName
			}));
		}

		// Token: 0x06001F67 RID: 8039 RVA: 0x00094590 File Offset: 0x00092790
		internal static Exception ParameterConversionFailed(object value, Type destType, Exception inner)
		{
			string @string = SR.GetString("Failed to convert parameter value from a {0} to a {1}.", new object[]
			{
				value.GetType().Name,
				destType.Name
			});
			Exception result;
			if (inner is ArgumentException)
			{
				result = new ArgumentException(@string, inner);
			}
			else if (inner is FormatException)
			{
				result = new FormatException(@string, inner);
			}
			else if (inner is InvalidCastException)
			{
				result = new InvalidCastException(@string, inner);
			}
			else if (inner is OverflowException)
			{
				result = new OverflowException(@string, inner);
			}
			else
			{
				result = inner;
			}
			return result;
		}

		// Token: 0x06001F68 RID: 8040 RVA: 0x00094610 File Offset: 0x00092810
		internal static Exception ParametersMappingIndex(int index, DbParameterCollection collection)
		{
			return ADP.CollectionIndexInt32(index, collection.GetType(), collection.Count);
		}

		// Token: 0x06001F69 RID: 8041 RVA: 0x00094624 File Offset: 0x00092824
		internal static Exception ParametersSourceIndex(string parameterName, DbParameterCollection collection, Type parameterType)
		{
			return ADP.CollectionIndexString(parameterType, "ParameterName", parameterName, collection.GetType());
		}

		// Token: 0x06001F6A RID: 8042 RVA: 0x00094638 File Offset: 0x00092838
		internal static Exception ParameterNull(string parameter, DbParameterCollection collection, Type parameterType)
		{
			return ADP.CollectionNullValue(parameter, collection.GetType(), parameterType);
		}

		// Token: 0x06001F6B RID: 8043 RVA: 0x000554AE File Offset: 0x000536AE
		internal static Exception UndefinedPopulationMechanism(string populationMechanism)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06001F6C RID: 8044 RVA: 0x00094647 File Offset: 0x00092847
		internal static Exception InvalidParameterType(DbParameterCollection collection, Type parameterType, object invalidValue)
		{
			return ADP.CollectionInvalidType(collection.GetType(), parameterType, invalidValue);
		}

		// Token: 0x06001F6D RID: 8045 RVA: 0x00094656 File Offset: 0x00092856
		internal static Exception ParallelTransactionsNotSupported(DbConnection obj)
		{
			return ADP.InvalidOperation(SR.GetString("{0} does not support parallel transactions.", new object[]
			{
				obj.GetType().Name
			}));
		}

		// Token: 0x06001F6E RID: 8046 RVA: 0x0009467B File Offset: 0x0009287B
		internal static Exception TransactionZombied(DbTransaction obj)
		{
			return ADP.InvalidOperation(SR.GetString("This {0} has completed; it is no longer usable.", new object[]
			{
				obj.GetType().Name
			}));
		}

		// Token: 0x06001F6F RID: 8047 RVA: 0x000946A0 File Offset: 0x000928A0
		internal static Delegate FindBuilder(MulticastDelegate mcd)
		{
			if (mcd != null)
			{
				foreach (Delegate @delegate in mcd.GetInvocationList())
				{
					if (@delegate.Target is DbCommandBuilder)
					{
						return @delegate;
					}
				}
			}
			return null;
		}

		// Token: 0x06001F70 RID: 8048 RVA: 0x000946DC File Offset: 0x000928DC
		internal static void TimerCurrent(out long ticks)
		{
			ticks = DateTime.UtcNow.ToFileTimeUtc();
		}

		// Token: 0x06001F71 RID: 8049 RVA: 0x000946F8 File Offset: 0x000928F8
		internal static long TimerCurrent()
		{
			return DateTime.UtcNow.ToFileTimeUtc();
		}

		// Token: 0x06001F72 RID: 8050 RVA: 0x00094712 File Offset: 0x00092912
		internal static long TimerFromSeconds(int seconds)
		{
			return checked(unchecked((long)seconds) * 10000000L);
		}

		// Token: 0x06001F73 RID: 8051 RVA: 0x0009471D File Offset: 0x0009291D
		internal static long TimerFromMilliseconds(long milliseconds)
		{
			return checked(milliseconds * 10000L);
		}

		// Token: 0x06001F74 RID: 8052 RVA: 0x00094727 File Offset: 0x00092927
		internal static bool TimerHasExpired(long timerExpire)
		{
			return ADP.TimerCurrent() > timerExpire;
		}

		// Token: 0x06001F75 RID: 8053 RVA: 0x00094734 File Offset: 0x00092934
		internal static long TimerRemaining(long timerExpire)
		{
			long num = ADP.TimerCurrent();
			return checked(timerExpire - num);
		}

		// Token: 0x06001F76 RID: 8054 RVA: 0x0009474A File Offset: 0x0009294A
		internal static long TimerRemainingMilliseconds(long timerExpire)
		{
			return ADP.TimerToMilliseconds(ADP.TimerRemaining(timerExpire));
		}

		// Token: 0x06001F77 RID: 8055 RVA: 0x00094757 File Offset: 0x00092957
		internal static long TimerRemainingSeconds(long timerExpire)
		{
			return ADP.TimerToSeconds(ADP.TimerRemaining(timerExpire));
		}

		// Token: 0x06001F78 RID: 8056 RVA: 0x00094764 File Offset: 0x00092964
		internal static long TimerToMilliseconds(long timerValue)
		{
			return timerValue / 10000L;
		}

		// Token: 0x06001F79 RID: 8057 RVA: 0x0009476E File Offset: 0x0009296E
		private static long TimerToSeconds(long timerValue)
		{
			return timerValue / 10000000L;
		}

		// Token: 0x06001F7A RID: 8058 RVA: 0x00094778 File Offset: 0x00092978
		internal static string MachineName()
		{
			return Environment.MachineName;
		}

		// Token: 0x06001F7B RID: 8059 RVA: 0x0009477F File Offset: 0x0009297F
		internal static Transaction GetCurrentTransaction()
		{
			return Transaction.Current;
		}

		// Token: 0x06001F7C RID: 8060 RVA: 0x00094786 File Offset: 0x00092986
		internal static bool IsDirection(DbParameter value, ParameterDirection condition)
		{
			return condition == (condition & value.Direction);
		}

		// Token: 0x06001F7D RID: 8061 RVA: 0x00094794 File Offset: 0x00092994
		internal static void IsNullOrSqlType(object value, out bool isNull, out bool isSqlType)
		{
			if (value == null || value == DBNull.Value)
			{
				isNull = true;
				isSqlType = false;
				return;
			}
			INullable nullable = value as INullable;
			if (nullable != null)
			{
				isNull = nullable.IsNull;
				isSqlType = (value is SqlBinary || value is SqlBoolean || value is SqlByte || value is SqlBytes || value is SqlChars || value is SqlDateTime || value is SqlDecimal || value is SqlDouble || value is SqlGuid || value is SqlInt16 || value is SqlInt32 || value is SqlInt64 || value is SqlMoney || value is SqlSingle || value is SqlString);
				return;
			}
			isNull = false;
			isSqlType = false;
		}

		// Token: 0x06001F7E RID: 8062 RVA: 0x0009484D File Offset: 0x00092A4D
		internal static Version GetAssemblyVersion()
		{
			if (ADP.s_systemDataVersion == null)
			{
				ADP.s_systemDataVersion = new Version("4.0.30319.17020");
			}
			return ADP.s_systemDataVersion;
		}

		// Token: 0x06001F7F RID: 8063 RVA: 0x00094870 File Offset: 0x00092A70
		internal static bool IsAzureSqlServerEndpoint(string dataSource)
		{
			int i = dataSource.LastIndexOf(',');
			if (i >= 0)
			{
				dataSource = dataSource.Substring(0, i);
			}
			i = dataSource.LastIndexOf('\\');
			if (i >= 0)
			{
				dataSource = dataSource.Substring(0, i);
			}
			dataSource = dataSource.Trim();
			for (i = 0; i < ADP.AzureSqlServerEndpoints.Length; i++)
			{
				if (dataSource.EndsWith(ADP.AzureSqlServerEndpoints[i], StringComparison.OrdinalIgnoreCase))
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x06001F80 RID: 8064 RVA: 0x000948D8 File Offset: 0x00092AD8
		internal static ArgumentOutOfRangeException InvalidDataRowVersion(DataRowVersion value)
		{
			return ADP.InvalidEnumerationValue(typeof(DataRowVersion), (int)value);
		}

		// Token: 0x06001F81 RID: 8065 RVA: 0x000948EA File Offset: 0x00092AEA
		internal static ArgumentException SingleValuedProperty(string propertyName, string value)
		{
			ArgumentException ex = new ArgumentException(SR.GetString("The only acceptable value for the property '{0}' is '{1}'.", new object[]
			{
				propertyName,
				value
			}));
			ADP.TraceExceptionAsReturnValue(ex);
			return ex;
		}

		// Token: 0x06001F82 RID: 8066 RVA: 0x0009490F File Offset: 0x00092B0F
		internal static ArgumentException DoubleValuedProperty(string propertyName, string value1, string value2)
		{
			ArgumentException ex = new ArgumentException(SR.GetString("The acceptable values for the property '{0}' are '{1}' or '{2}'.", new object[]
			{
				propertyName,
				value1,
				value2
			}));
			ADP.TraceExceptionAsReturnValue(ex);
			return ex;
		}

		// Token: 0x06001F83 RID: 8067 RVA: 0x00094938 File Offset: 0x00092B38
		internal static ArgumentException InvalidPrefixSuffix()
		{
			ArgumentException ex = new ArgumentException(SR.GetString("Specified QuotePrefix and QuoteSuffix values do not match."));
			ADP.TraceExceptionAsReturnValue(ex);
			return ex;
		}

		// Token: 0x06001F84 RID: 8068 RVA: 0x0009494F File Offset: 0x00092B4F
		internal static ArgumentOutOfRangeException InvalidCommandBehavior(CommandBehavior value)
		{
			return ADP.InvalidEnumerationValue(typeof(CommandBehavior), (int)value);
		}

		// Token: 0x06001F85 RID: 8069 RVA: 0x00094961 File Offset: 0x00092B61
		internal static void ValidateCommandBehavior(CommandBehavior value)
		{
			if (value < CommandBehavior.Default || (CommandBehavior.SingleResult | CommandBehavior.SchemaOnly | CommandBehavior.KeyInfo | CommandBehavior.SingleRow | CommandBehavior.SequentialAccess | CommandBehavior.CloseConnection) < value)
			{
				throw ADP.InvalidCommandBehavior(value);
			}
		}

		// Token: 0x06001F86 RID: 8070 RVA: 0x00094973 File Offset: 0x00092B73
		internal static ArgumentOutOfRangeException NotSupportedCommandBehavior(CommandBehavior value, string method)
		{
			return ADP.NotSupportedEnumerationValue(typeof(CommandBehavior), value.ToString(), method);
		}

		// Token: 0x06001F87 RID: 8071 RVA: 0x00094992 File Offset: 0x00092B92
		internal static ArgumentException BadParameterName(string parameterName)
		{
			ArgumentException ex = new ArgumentException(SR.GetString("Specified parameter name '{0}' is not valid.", new object[]
			{
				parameterName
			}));
			ADP.TraceExceptionAsReturnValue(ex);
			return ex;
		}

		// Token: 0x06001F88 RID: 8072 RVA: 0x000949B4 File Offset: 0x00092BB4
		internal static Exception DeriveParametersNotSupported(IDbCommand value)
		{
			return ADP.DataAdapter(SR.GetString("{0} DeriveParameters only supports CommandType.StoredProcedure, not CommandType.{1}.", new object[]
			{
				value.GetType().Name,
				value.CommandType.ToString()
			}));
		}

		// Token: 0x06001F89 RID: 8073 RVA: 0x000949FB File Offset: 0x00092BFB
		internal static Exception NoStoredProcedureExists(string sproc)
		{
			return ADP.InvalidOperation(SR.GetString("The stored procedure '{0}' doesn't exist.", new object[]
			{
				sproc
			}));
		}

		// Token: 0x06001F8A RID: 8074 RVA: 0x00094A16 File Offset: 0x00092C16
		internal static InvalidOperationException TransactionCompletedButNotDisposed()
		{
			return ADP.Provider(SR.GetString("The transaction associated with the current connection has completed but has not been disposed.  The transaction must be disposed before the connection can be used to execute SQL statements."));
		}

		// Token: 0x06001F8B RID: 8075 RVA: 0x000061C5 File Offset: 0x000043C5
		internal static bool NeedManualEnlistment()
		{
			return false;
		}

		// Token: 0x06001F8C RID: 8076 RVA: 0x00094A27 File Offset: 0x00092C27
		internal static bool IsEmpty(string str)
		{
			return string.IsNullOrEmpty(str);
		}

		// Token: 0x06001F8D RID: 8077 RVA: 0x00094A2F File Offset: 0x00092C2F
		internal static Exception DatabaseNameTooLong()
		{
			return ADP.Argument(SR.GetString("The argument is too long."));
		}

		// Token: 0x06001F8E RID: 8078 RVA: 0x0007BB86 File Offset: 0x00079D86
		internal static int StringLength(string inputString)
		{
			if (inputString == null)
			{
				return 0;
			}
			return inputString.Length;
		}

		// Token: 0x06001F8F RID: 8079 RVA: 0x00094A40 File Offset: 0x00092C40
		internal static Exception NumericToDecimalOverflow()
		{
			return ADP.InvalidCast(SR.GetString("The numerical value is too large to fit into a 96 bit decimal."));
		}

		// Token: 0x06001F90 RID: 8080 RVA: 0x00094A51 File Offset: 0x00092C51
		internal static Exception OdbcNoTypesFromProvider()
		{
			return ADP.InvalidOperation(SR.GetString("The ODBC provider did not return results from SQLGETTYPEINFO."));
		}

		// Token: 0x06001F91 RID: 8081 RVA: 0x00094A62 File Offset: 0x00092C62
		internal static ArgumentException InvalidRestrictionValue(string collectionName, string restrictionName, string restrictionValue)
		{
			return ADP.Argument(SR.GetString("'{2}' is not a valid value for the '{1}' restriction of the '{0}' schema collection.", new object[]
			{
				collectionName,
				restrictionName,
				restrictionValue
			}));
		}

		// Token: 0x06001F92 RID: 8082 RVA: 0x00094A85 File Offset: 0x00092C85
		internal static Exception DataReaderNoData()
		{
			return ADP.InvalidOperation(SR.GetString("No data exists for the row/column."));
		}

		// Token: 0x06001F93 RID: 8083 RVA: 0x00094A96 File Offset: 0x00092C96
		internal static Exception ConnectionIsDisabled(Exception InnerException)
		{
			return ADP.InvalidOperation(SR.GetString("The connection has been disabled."), InnerException);
		}

		// Token: 0x06001F94 RID: 8084 RVA: 0x00094AA8 File Offset: 0x00092CA8
		internal static Exception OffsetOutOfRangeException()
		{
			return ADP.InvalidOperation(SR.GetString("Offset must refer to a location within the value."));
		}

		// Token: 0x06001F95 RID: 8085 RVA: 0x00094AB9 File Offset: 0x00092CB9
		internal static ArgumentException InvalidDataType(TypeCode typecode)
		{
			return ADP.Argument(SR.GetString("The parameter data type of {0} is invalid.", new object[]
			{
				typecode.ToString()
			}));
		}

		// Token: 0x06001F96 RID: 8086 RVA: 0x00094AE0 File Offset: 0x00092CE0
		internal static InvalidOperationException QuotePrefixNotSet(string method)
		{
			return ADP.InvalidOperation(Res.GetString("{0} requires open connection when the quote prefix has not been set.", new object[]
			{
				method
			}));
		}

		// Token: 0x06001F97 RID: 8087 RVA: 0x00094AFB File Offset: 0x00092CFB
		internal static string GetFullPath(string filename)
		{
			return Path.GetFullPath(filename);
		}

		// Token: 0x06001F98 RID: 8088 RVA: 0x00094B03 File Offset: 0x00092D03
		internal static InvalidOperationException InvalidDataDirectory()
		{
			return ADP.InvalidOperation(SR.GetString("The DataDirectory substitute is not a string."));
		}

		// Token: 0x06001F99 RID: 8089 RVA: 0x00094B14 File Offset: 0x00092D14
		internal static ArgumentException UnknownDataTypeCode(Type dataType, TypeCode typeCode)
		{
			string name = "Unable to handle an unknown TypeCode {0} returned by Type {1}.";
			object[] array = new object[2];
			int num = 0;
			int num2 = (int)typeCode;
			array[num] = num2.ToString(CultureInfo.InvariantCulture);
			array[1] = dataType.FullName;
			return ADP.Argument(SR.GetString(name, array));
		}

		// Token: 0x06001F9A RID: 8090 RVA: 0x00094B50 File Offset: 0x00092D50
		internal static void EscapeSpecialCharacters(string unescapedString, StringBuilder escapedString)
		{
			foreach (char value in unescapedString)
			{
				if (".$^{[(|)*+?\\]".IndexOf(value) >= 0)
				{
					escapedString.Append("\\");
				}
				escapedString.Append(value);
			}
		}

		// Token: 0x06001F9B RID: 8091 RVA: 0x00094B9A File Offset: 0x00092D9A
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.MayFail)]
		internal static IntPtr IntPtrOffset(IntPtr pbase, int offset)
		{
			checked
			{
				if (4 == ADP.PtrSize)
				{
					return (IntPtr)(pbase.ToInt32() + offset);
				}
				return (IntPtr)(pbase.ToInt64() + unchecked((long)offset));
			}
		}

		// Token: 0x06001F9C RID: 8092 RVA: 0x00094BC2 File Offset: 0x00092DC2
		internal static ArgumentOutOfRangeException NotSupportedUserDefinedTypeSerializationFormat(Format value, string method)
		{
			return ADP.NotSupportedEnumerationValue(typeof(Format), value.ToString(), method);
		}

		// Token: 0x06001F9D RID: 8093 RVA: 0x00094BE1 File Offset: 0x00092DE1
		internal static ArgumentOutOfRangeException InvalidUserDefinedTypeSerializationFormat(Format value)
		{
			return ADP.InvalidEnumerationValue(typeof(Format), (int)value);
		}

		// Token: 0x06001F9E RID: 8094 RVA: 0x00094BF3 File Offset: 0x00092DF3
		internal static ArgumentOutOfRangeException ArgumentOutOfRange(string message, string parameterName, object value)
		{
			ArgumentOutOfRangeException ex = new ArgumentOutOfRangeException(parameterName, value, message);
			ADP.TraceExceptionAsReturnValue(ex);
			return ex;
		}

		// Token: 0x06001F9F RID: 8095 RVA: 0x00094C03 File Offset: 0x00092E03
		internal static Exception InvalidXMLBadVersion()
		{
			return ADP.Argument(Res.GetString("Invalid Xml; can only parse elements of version one."));
		}

		// Token: 0x06001FA0 RID: 8096 RVA: 0x00094C14 File Offset: 0x00092E14
		internal static Exception NotAPermissionElement()
		{
			return ADP.Argument(Res.GetString("Given security element is not a permission element."));
		}

		// Token: 0x06001FA1 RID: 8097 RVA: 0x00094C25 File Offset: 0x00092E25
		internal static Exception PermissionTypeMismatch()
		{
			return ADP.Argument(Res.GetString("Type mismatch."));
		}

		// Token: 0x06001FA2 RID: 8098 RVA: 0x00094C36 File Offset: 0x00092E36
		internal static ArgumentOutOfRangeException InvalidPermissionState(PermissionState value)
		{
			return ADP.InvalidEnumerationValue(typeof(PermissionState), (int)value);
		}

		// Token: 0x06001FA3 RID: 8099 RVA: 0x00094C48 File Offset: 0x00092E48
		internal static ArgumentException ConfigProviderNotFound()
		{
			return ADP.Argument(Res.GetString("Unable to find the requested .Net Framework Data Provider.  It may not be installed."));
		}

		// Token: 0x06001FA4 RID: 8100 RVA: 0x00094C59 File Offset: 0x00092E59
		internal static InvalidOperationException ConfigProviderInvalid()
		{
			return ADP.InvalidOperation(Res.GetString("The requested .Net Framework Data Provider's implementation does not have an Instance field of a System.Data.Common.DbProviderFactory derived type."));
		}

		// Token: 0x06001FA5 RID: 8101 RVA: 0x00094C6A File Offset: 0x00092E6A
		internal static Exception OleDb()
		{
			return new NotImplementedException("OleDb is not implemented.");
		}

		// Token: 0x06001FA6 RID: 8102 RVA: 0x00094C78 File Offset: 0x00092E78
		// Note: this type is marked as 'beforefieldinit'.
		static ADP()
		{
		}

		// Token: 0x04001475 RID: 5237
		private static Task<bool> _trueTask;

		// Token: 0x04001476 RID: 5238
		private static Task<bool> _falseTask;

		// Token: 0x04001477 RID: 5239
		internal const CompareOptions DefaultCompareOptions = CompareOptions.IgnoreCase | CompareOptions.IgnoreKanaType | CompareOptions.IgnoreWidth;

		// Token: 0x04001478 RID: 5240
		internal const int DefaultConnectionTimeout = 15;

		// Token: 0x04001479 RID: 5241
		private static readonly Type s_stackOverflowType = typeof(StackOverflowException);

		// Token: 0x0400147A RID: 5242
		private static readonly Type s_outOfMemoryType = typeof(OutOfMemoryException);

		// Token: 0x0400147B RID: 5243
		private static readonly Type s_threadAbortType = typeof(ThreadAbortException);

		// Token: 0x0400147C RID: 5244
		private static readonly Type s_nullReferenceType = typeof(NullReferenceException);

		// Token: 0x0400147D RID: 5245
		private static readonly Type s_accessViolationType = typeof(AccessViolationException);

		// Token: 0x0400147E RID: 5246
		private static readonly Type s_securityType = typeof(SecurityException);

		// Token: 0x0400147F RID: 5247
		internal static readonly bool IsWindowsNT = PlatformID.Win32NT == Environment.OSVersion.Platform;

		// Token: 0x04001480 RID: 5248
		internal static readonly bool IsPlatformNT5 = ADP.IsWindowsNT && Environment.OSVersion.Version.Major >= 5;

		// Token: 0x04001481 RID: 5249
		internal const string ConnectionString = "ConnectionString";

		// Token: 0x04001482 RID: 5250
		internal const string DataSetColumn = "DataSetColumn";

		// Token: 0x04001483 RID: 5251
		internal const string DataSetTable = "DataSetTable";

		// Token: 0x04001484 RID: 5252
		internal const string Fill = "Fill";

		// Token: 0x04001485 RID: 5253
		internal const string FillSchema = "FillSchema";

		// Token: 0x04001486 RID: 5254
		internal const string SourceColumn = "SourceColumn";

		// Token: 0x04001487 RID: 5255
		internal const string SourceTable = "SourceTable";

		// Token: 0x04001488 RID: 5256
		internal const string Parameter = "Parameter";

		// Token: 0x04001489 RID: 5257
		internal const string ParameterName = "ParameterName";

		// Token: 0x0400148A RID: 5258
		internal const string ParameterSetPosition = "set_Position";

		// Token: 0x0400148B RID: 5259
		internal const int DefaultCommandTimeout = 30;

		// Token: 0x0400148C RID: 5260
		internal const float FailoverTimeoutStep = 0.08f;

		// Token: 0x0400148D RID: 5261
		internal static readonly string StrEmpty = "";

		// Token: 0x0400148E RID: 5262
		internal const int CharSize = 2;

		// Token: 0x0400148F RID: 5263
		private static Version s_systemDataVersion;

		// Token: 0x04001490 RID: 5264
		internal static readonly string[] AzureSqlServerEndpoints = new string[]
		{
			SR.GetString(".database.windows.net"),
			SR.GetString(".database.cloudapi.de"),
			SR.GetString(".database.usgovcloudapi.net"),
			SR.GetString(".database.chinacloudapi.cn")
		};

		// Token: 0x04001491 RID: 5265
		internal const int DecimalMaxPrecision = 29;

		// Token: 0x04001492 RID: 5266
		internal const int DecimalMaxPrecision28 = 28;

		// Token: 0x04001493 RID: 5267
		internal static readonly IntPtr PtrZero = new IntPtr(0);

		// Token: 0x04001494 RID: 5268
		internal static readonly int PtrSize = IntPtr.Size;

		// Token: 0x04001495 RID: 5269
		internal const string BeginTransaction = "BeginTransaction";

		// Token: 0x04001496 RID: 5270
		internal const string ChangeDatabase = "ChangeDatabase";

		// Token: 0x04001497 RID: 5271
		internal const string CommitTransaction = "CommitTransaction";

		// Token: 0x04001498 RID: 5272
		internal const string CommandTimeout = "CommandTimeout";

		// Token: 0x04001499 RID: 5273
		internal const string DeriveParameters = "DeriveParameters";

		// Token: 0x0400149A RID: 5274
		internal const string ExecuteReader = "ExecuteReader";

		// Token: 0x0400149B RID: 5275
		internal const string ExecuteNonQuery = "ExecuteNonQuery";

		// Token: 0x0400149C RID: 5276
		internal const string ExecuteScalar = "ExecuteScalar";

		// Token: 0x0400149D RID: 5277
		internal const string GetSchema = "GetSchema";

		// Token: 0x0400149E RID: 5278
		internal const string GetSchemaTable = "GetSchemaTable";

		// Token: 0x0400149F RID: 5279
		internal const string Prepare = "Prepare";

		// Token: 0x040014A0 RID: 5280
		internal const string RollbackTransaction = "RollbackTransaction";

		// Token: 0x040014A1 RID: 5281
		internal const string QuoteIdentifier = "QuoteIdentifier";

		// Token: 0x040014A2 RID: 5282
		internal const string UnquoteIdentifier = "UnquoteIdentifier";

		// Token: 0x0200028A RID: 650
		internal enum InternalErrorCode
		{
			// Token: 0x040014A4 RID: 5284
			UnpooledObjectHasOwner,
			// Token: 0x040014A5 RID: 5285
			UnpooledObjectHasWrongOwner,
			// Token: 0x040014A6 RID: 5286
			PushingObjectSecondTime,
			// Token: 0x040014A7 RID: 5287
			PooledObjectHasOwner,
			// Token: 0x040014A8 RID: 5288
			PooledObjectInPoolMoreThanOnce,
			// Token: 0x040014A9 RID: 5289
			CreateObjectReturnedNull,
			// Token: 0x040014AA RID: 5290
			NewObjectCannotBePooled,
			// Token: 0x040014AB RID: 5291
			NonPooledObjectUsedMoreThanOnce,
			// Token: 0x040014AC RID: 5292
			AttemptingToPoolOnRestrictedToken,
			// Token: 0x040014AD RID: 5293
			ConvertSidToStringSidWReturnedNull = 10,
			// Token: 0x040014AE RID: 5294
			AttemptingToConstructReferenceCollectionOnStaticObject = 12,
			// Token: 0x040014AF RID: 5295
			AttemptingToEnlistTwice,
			// Token: 0x040014B0 RID: 5296
			CreateReferenceCollectionReturnedNull,
			// Token: 0x040014B1 RID: 5297
			PooledObjectWithoutPool,
			// Token: 0x040014B2 RID: 5298
			UnexpectedWaitAnyResult,
			// Token: 0x040014B3 RID: 5299
			SynchronousConnectReturnedPending,
			// Token: 0x040014B4 RID: 5300
			CompletedConnectReturnedPending,
			// Token: 0x040014B5 RID: 5301
			NameValuePairNext = 20,
			// Token: 0x040014B6 RID: 5302
			InvalidParserState1,
			// Token: 0x040014B7 RID: 5303
			InvalidParserState2,
			// Token: 0x040014B8 RID: 5304
			InvalidParserState3,
			// Token: 0x040014B9 RID: 5305
			InvalidBuffer = 30,
			// Token: 0x040014BA RID: 5306
			UnimplementedSMIMethod = 40,
			// Token: 0x040014BB RID: 5307
			InvalidSmiCall,
			// Token: 0x040014BC RID: 5308
			SqlDependencyObtainProcessDispatcherFailureObjectHandle = 50,
			// Token: 0x040014BD RID: 5309
			SqlDependencyProcessDispatcherFailureCreateInstance,
			// Token: 0x040014BE RID: 5310
			SqlDependencyProcessDispatcherFailureAppDomain,
			// Token: 0x040014BF RID: 5311
			SqlDependencyCommandHashIsNotAssociatedWithNotification,
			// Token: 0x040014C0 RID: 5312
			UnknownTransactionFailure = 60
		}

		// Token: 0x0200028B RID: 651
		internal enum ConnectionError
		{
			// Token: 0x040014C2 RID: 5314
			BeginGetConnectionReturnsNull,
			// Token: 0x040014C3 RID: 5315
			GetConnectionReturnsNull,
			// Token: 0x040014C4 RID: 5316
			ConnectionOptionsMissing,
			// Token: 0x040014C5 RID: 5317
			CouldNotSwitchToClosedPreviouslyOpenedState
		}
	}
}
