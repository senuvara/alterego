﻿using System;
using System.Collections;
using System.Xml;

namespace System.Data.Common
{
	// Token: 0x020002E8 RID: 744
	internal sealed class UInt64Storage : DataStorage
	{
		// Token: 0x06002588 RID: 9608 RVA: 0x000AAF32 File Offset: 0x000A9132
		public UInt64Storage(DataColumn column) : base(column, typeof(ulong), UInt64Storage.s_defaultValue, StorageType.UInt64)
		{
		}

		// Token: 0x06002589 RID: 9609 RVA: 0x000AAF54 File Offset: 0x000A9154
		public override object Aggregate(int[] records, AggregateType kind)
		{
			bool flag = false;
			try
			{
				switch (kind)
				{
				case AggregateType.Sum:
				{
					ulong num = UInt64Storage.s_defaultValue;
					checked
					{
						foreach (int num2 in records)
						{
							if (base.HasValue(num2))
							{
								num += this._values[num2];
								flag = true;
							}
						}
						if (flag)
						{
							return num;
						}
						return this._nullValue;
					}
				}
				case AggregateType.Mean:
				{
					decimal d = UInt64Storage.s_defaultValue;
					int num3 = 0;
					foreach (int num4 in records)
					{
						if (base.HasValue(num4))
						{
							d += this._values[num4];
							num3++;
							flag = true;
						}
					}
					if (flag)
					{
						return (ulong)(d / num3);
					}
					return this._nullValue;
				}
				case AggregateType.Min:
				{
					ulong num5 = ulong.MaxValue;
					foreach (int num6 in records)
					{
						if (base.HasValue(num6))
						{
							num5 = Math.Min(this._values[num6], num5);
							flag = true;
						}
					}
					if (flag)
					{
						return num5;
					}
					return this._nullValue;
				}
				case AggregateType.Max:
				{
					ulong num7 = 0UL;
					foreach (int num8 in records)
					{
						if (base.HasValue(num8))
						{
							num7 = Math.Max(this._values[num8], num7);
							flag = true;
						}
					}
					if (flag)
					{
						return num7;
					}
					return this._nullValue;
				}
				case AggregateType.First:
					if (records.Length != 0)
					{
						return this._values[records[0]];
					}
					return null;
				case AggregateType.Count:
					return base.Aggregate(records, kind);
				case AggregateType.Var:
				case AggregateType.StDev:
				{
					int num9 = 0;
					double num10 = 0.0;
					double num11 = 0.0;
					foreach (int num12 in records)
					{
						if (base.HasValue(num12))
						{
							num10 += this._values[num12];
							num11 += this._values[num12] * this._values[num12];
							num9++;
						}
					}
					if (num9 <= 1)
					{
						return this._nullValue;
					}
					double num13 = (double)num9 * num11 - num10 * num10;
					if (num13 / (num10 * num10) < 1E-15 || num13 < 0.0)
					{
						num13 = 0.0;
					}
					else
					{
						num13 /= (double)(num9 * (num9 - 1));
					}
					if (kind == AggregateType.StDev)
					{
						return Math.Sqrt(num13);
					}
					return num13;
				}
				}
			}
			catch (OverflowException)
			{
				throw ExprException.Overflow(typeof(ulong));
			}
			throw ExceptionBuilder.AggregateException(kind, this._dataType);
		}

		// Token: 0x0600258A RID: 9610 RVA: 0x000AB274 File Offset: 0x000A9474
		public override int Compare(int recordNo1, int recordNo2)
		{
			ulong num = this._values[recordNo1];
			ulong num2 = this._values[recordNo2];
			if (num.Equals(UInt64Storage.s_defaultValue) || num2.Equals(UInt64Storage.s_defaultValue))
			{
				int num3 = base.CompareBits(recordNo1, recordNo2);
				if (num3 != 0)
				{
					return num3;
				}
			}
			if (num < num2)
			{
				return -1;
			}
			if (num <= num2)
			{
				return 0;
			}
			return 1;
		}

		// Token: 0x0600258B RID: 9611 RVA: 0x000AB2CC File Offset: 0x000A94CC
		public override int CompareValueTo(int recordNo, object value)
		{
			if (this._nullValue == value)
			{
				if (!base.HasValue(recordNo))
				{
					return 0;
				}
				return 1;
			}
			else
			{
				ulong num = this._values[recordNo];
				if (UInt64Storage.s_defaultValue == num && !base.HasValue(recordNo))
				{
					return -1;
				}
				return num.CompareTo((ulong)value);
			}
		}

		// Token: 0x0600258C RID: 9612 RVA: 0x000AB318 File Offset: 0x000A9518
		public override object ConvertValue(object value)
		{
			if (this._nullValue != value)
			{
				if (value != null)
				{
					value = ((IConvertible)value).ToUInt64(base.FormatProvider);
				}
				else
				{
					value = this._nullValue;
				}
			}
			return value;
		}

		// Token: 0x0600258D RID: 9613 RVA: 0x000AB349 File Offset: 0x000A9549
		public override void Copy(int recordNo1, int recordNo2)
		{
			base.CopyBits(recordNo1, recordNo2);
			this._values[recordNo2] = this._values[recordNo1];
		}

		// Token: 0x0600258E RID: 9614 RVA: 0x000AB364 File Offset: 0x000A9564
		public override object Get(int record)
		{
			ulong num = this._values[record];
			if (!num.Equals(UInt64Storage.s_defaultValue))
			{
				return num;
			}
			return base.GetBits(record);
		}

		// Token: 0x0600258F RID: 9615 RVA: 0x000AB398 File Offset: 0x000A9598
		public override void Set(int record, object value)
		{
			if (this._nullValue == value)
			{
				this._values[record] = UInt64Storage.s_defaultValue;
				base.SetNullBit(record, true);
				return;
			}
			this._values[record] = ((IConvertible)value).ToUInt64(base.FormatProvider);
			base.SetNullBit(record, false);
		}

		// Token: 0x06002590 RID: 9616 RVA: 0x000AB3E8 File Offset: 0x000A95E8
		public override void SetCapacity(int capacity)
		{
			ulong[] array = new ulong[capacity];
			if (this._values != null)
			{
				Array.Copy(this._values, 0, array, 0, Math.Min(capacity, this._values.Length));
			}
			this._values = array;
			base.SetCapacity(capacity);
		}

		// Token: 0x06002591 RID: 9617 RVA: 0x000AB42E File Offset: 0x000A962E
		public override object ConvertXmlToObject(string s)
		{
			return XmlConvert.ToUInt64(s);
		}

		// Token: 0x06002592 RID: 9618 RVA: 0x000AB43B File Offset: 0x000A963B
		public override string ConvertObjectToXml(object value)
		{
			return XmlConvert.ToString((ulong)value);
		}

		// Token: 0x06002593 RID: 9619 RVA: 0x000AB448 File Offset: 0x000A9648
		protected override object GetEmptyStorage(int recordCount)
		{
			return new ulong[recordCount];
		}

		// Token: 0x06002594 RID: 9620 RVA: 0x000AB450 File Offset: 0x000A9650
		protected override void CopyValue(int record, object store, BitArray nullbits, int storeIndex)
		{
			((ulong[])store)[storeIndex] = this._values[record];
			nullbits.Set(storeIndex, !base.HasValue(record));
		}

		// Token: 0x06002595 RID: 9621 RVA: 0x000AB475 File Offset: 0x000A9675
		protected override void SetStorage(object store, BitArray nullbits)
		{
			this._values = (ulong[])store;
			base.SetNullStorage(nullbits);
		}

		// Token: 0x06002596 RID: 9622 RVA: 0x00005E03 File Offset: 0x00004003
		// Note: this type is marked as 'beforefieldinit'.
		static UInt64Storage()
		{
		}

		// Token: 0x04001690 RID: 5776
		private static readonly ulong s_defaultValue;

		// Token: 0x04001691 RID: 5777
		private ulong[] _values;
	}
}
