﻿using System;
using System.Collections;
using System.Xml;

namespace System.Data.Common
{
	// Token: 0x020002E1 RID: 737
	internal sealed class SingleStorage : DataStorage
	{
		// Token: 0x06002529 RID: 9513 RVA: 0x000A91DD File Offset: 0x000A73DD
		public SingleStorage(DataColumn column) : base(column, typeof(float), 0f, StorageType.Single)
		{
		}

		// Token: 0x0600252A RID: 9514 RVA: 0x000A91FC File Offset: 0x000A73FC
		public override object Aggregate(int[] records, AggregateType kind)
		{
			bool flag = false;
			try
			{
				switch (kind)
				{
				case AggregateType.Sum:
				{
					float num = 0f;
					foreach (int num2 in records)
					{
						if (!this.IsNull(num2))
						{
							num += this._values[num2];
							flag = true;
						}
					}
					if (flag)
					{
						return num;
					}
					return this._nullValue;
				}
				case AggregateType.Mean:
				{
					double num3 = 0.0;
					int num4 = 0;
					foreach (int num5 in records)
					{
						if (!this.IsNull(num5))
						{
							num3 += (double)this._values[num5];
							num4++;
							flag = true;
						}
					}
					if (flag)
					{
						return (float)(num3 / (double)num4);
					}
					return this._nullValue;
				}
				case AggregateType.Min:
				{
					float num6 = float.MaxValue;
					foreach (int num7 in records)
					{
						if (!this.IsNull(num7))
						{
							num6 = Math.Min(this._values[num7], num6);
							flag = true;
						}
					}
					if (flag)
					{
						return num6;
					}
					return this._nullValue;
				}
				case AggregateType.Max:
				{
					float num8 = float.MinValue;
					foreach (int num9 in records)
					{
						if (!this.IsNull(num9))
						{
							num8 = Math.Max(this._values[num9], num8);
							flag = true;
						}
					}
					if (flag)
					{
						return num8;
					}
					return this._nullValue;
				}
				case AggregateType.First:
					if (records.Length != 0)
					{
						return this._values[records[0]];
					}
					return null;
				case AggregateType.Count:
					return base.Aggregate(records, kind);
				case AggregateType.Var:
				case AggregateType.StDev:
				{
					int num10 = 0;
					double num11 = 0.0;
					double num12 = 0.0;
					foreach (int num13 in records)
					{
						if (!this.IsNull(num13))
						{
							num11 += (double)this._values[num13];
							num12 += (double)this._values[num13] * (double)this._values[num13];
							num10++;
						}
					}
					if (num10 <= 1)
					{
						return this._nullValue;
					}
					double num14 = (double)num10 * num12 - num11 * num11;
					if (num14 / (num11 * num11) < 1E-15 || num14 < 0.0)
					{
						num14 = 0.0;
					}
					else
					{
						num14 /= (double)(num10 * (num10 - 1));
					}
					if (kind == AggregateType.StDev)
					{
						return Math.Sqrt(num14);
					}
					return num14;
				}
				}
			}
			catch (OverflowException)
			{
				throw ExprException.Overflow(typeof(float));
			}
			throw ExceptionBuilder.AggregateException(kind, this._dataType);
		}

		// Token: 0x0600252B RID: 9515 RVA: 0x000A9508 File Offset: 0x000A7708
		public override int Compare(int recordNo1, int recordNo2)
		{
			float num = this._values[recordNo1];
			float num2 = this._values[recordNo2];
			if (num == 0f || num2 == 0f)
			{
				int num3 = base.CompareBits(recordNo1, recordNo2);
				if (num3 != 0)
				{
					return num3;
				}
			}
			return num.CompareTo(num2);
		}

		// Token: 0x0600252C RID: 9516 RVA: 0x000A9550 File Offset: 0x000A7750
		public override int CompareValueTo(int recordNo, object value)
		{
			if (this._nullValue == value)
			{
				if (this.IsNull(recordNo))
				{
					return 0;
				}
				return 1;
			}
			else
			{
				float num = this._values[recordNo];
				if (0f == num && this.IsNull(recordNo))
				{
					return -1;
				}
				return num.CompareTo((float)value);
			}
		}

		// Token: 0x0600252D RID: 9517 RVA: 0x000A959C File Offset: 0x000A779C
		public override object ConvertValue(object value)
		{
			if (this._nullValue != value)
			{
				if (value != null)
				{
					value = ((IConvertible)value).ToSingle(base.FormatProvider);
				}
				else
				{
					value = this._nullValue;
				}
			}
			return value;
		}

		// Token: 0x0600252E RID: 9518 RVA: 0x000A95CD File Offset: 0x000A77CD
		public override void Copy(int recordNo1, int recordNo2)
		{
			base.CopyBits(recordNo1, recordNo2);
			this._values[recordNo2] = this._values[recordNo1];
		}

		// Token: 0x0600252F RID: 9519 RVA: 0x000A95E8 File Offset: 0x000A77E8
		public override object Get(int record)
		{
			float num = this._values[record];
			if (num != 0f)
			{
				return num;
			}
			return base.GetBits(record);
		}

		// Token: 0x06002530 RID: 9520 RVA: 0x000A9614 File Offset: 0x000A7814
		public override void Set(int record, object value)
		{
			if (this._nullValue == value)
			{
				this._values[record] = 0f;
				base.SetNullBit(record, true);
				return;
			}
			this._values[record] = ((IConvertible)value).ToSingle(base.FormatProvider);
			base.SetNullBit(record, false);
		}

		// Token: 0x06002531 RID: 9521 RVA: 0x000A9664 File Offset: 0x000A7864
		public override void SetCapacity(int capacity)
		{
			float[] array = new float[capacity];
			if (this._values != null)
			{
				Array.Copy(this._values, 0, array, 0, Math.Min(capacity, this._values.Length));
			}
			this._values = array;
			base.SetCapacity(capacity);
		}

		// Token: 0x06002532 RID: 9522 RVA: 0x000A96AA File Offset: 0x000A78AA
		public override object ConvertXmlToObject(string s)
		{
			return XmlConvert.ToSingle(s);
		}

		// Token: 0x06002533 RID: 9523 RVA: 0x000A96B7 File Offset: 0x000A78B7
		public override string ConvertObjectToXml(object value)
		{
			return XmlConvert.ToString((float)value);
		}

		// Token: 0x06002534 RID: 9524 RVA: 0x000A96C4 File Offset: 0x000A78C4
		protected override object GetEmptyStorage(int recordCount)
		{
			return new float[recordCount];
		}

		// Token: 0x06002535 RID: 9525 RVA: 0x000A96CC File Offset: 0x000A78CC
		protected override void CopyValue(int record, object store, BitArray nullbits, int storeIndex)
		{
			((float[])store)[storeIndex] = this._values[record];
			nullbits.Set(storeIndex, this.IsNull(record));
		}

		// Token: 0x06002536 RID: 9526 RVA: 0x000A96EE File Offset: 0x000A78EE
		protected override void SetStorage(object store, BitArray nullbits)
		{
			this._values = (float[])store;
			base.SetNullStorage(nullbits);
		}

		// Token: 0x0400167D RID: 5757
		private const float defaultValue = 0f;

		// Token: 0x0400167E RID: 5758
		private float[] _values;
	}
}
