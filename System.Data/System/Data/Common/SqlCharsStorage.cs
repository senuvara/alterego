﻿using System;
using System.Collections;
using System.Data.SqlTypes;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace System.Data.Common
{
	// Token: 0x020002D3 RID: 723
	internal sealed class SqlCharsStorage : DataStorage
	{
		// Token: 0x06002472 RID: 9330 RVA: 0x000A5504 File Offset: 0x000A3704
		public SqlCharsStorage(DataColumn column) : base(column, typeof(SqlChars), SqlChars.Null, SqlChars.Null, StorageType.SqlChars)
		{
		}

		// Token: 0x06002473 RID: 9331 RVA: 0x000A5524 File Offset: 0x000A3724
		public override object Aggregate(int[] records, AggregateType kind)
		{
			try
			{
				if (kind != AggregateType.First)
				{
					if (kind == AggregateType.Count)
					{
						int num = 0;
						for (int i = 0; i < records.Length; i++)
						{
							if (!this.IsNull(records[i]))
							{
								num++;
							}
						}
						return num;
					}
				}
				else
				{
					if (records.Length != 0)
					{
						return this._values[records[0]];
					}
					return null;
				}
			}
			catch (OverflowException)
			{
				throw ExprException.Overflow(typeof(SqlChars));
			}
			throw ExceptionBuilder.AggregateException(kind, this._dataType);
		}

		// Token: 0x06002474 RID: 9332 RVA: 0x000061C5 File Offset: 0x000043C5
		public override int Compare(int recordNo1, int recordNo2)
		{
			return 0;
		}

		// Token: 0x06002475 RID: 9333 RVA: 0x000061C5 File Offset: 0x000043C5
		public override int CompareValueTo(int recordNo, object value)
		{
			return 0;
		}

		// Token: 0x06002476 RID: 9334 RVA: 0x000A55AC File Offset: 0x000A37AC
		public override void Copy(int recordNo1, int recordNo2)
		{
			this._values[recordNo2] = this._values[recordNo1];
		}

		// Token: 0x06002477 RID: 9335 RVA: 0x000A55BE File Offset: 0x000A37BE
		public override object Get(int record)
		{
			return this._values[record];
		}

		// Token: 0x06002478 RID: 9336 RVA: 0x000A55C8 File Offset: 0x000A37C8
		public override bool IsNull(int record)
		{
			return this._values[record].IsNull;
		}

		// Token: 0x06002479 RID: 9337 RVA: 0x000A55D7 File Offset: 0x000A37D7
		public override void Set(int record, object value)
		{
			if (value == DBNull.Value || value == null)
			{
				this._values[record] = SqlChars.Null;
				return;
			}
			this._values[record] = (SqlChars)value;
		}

		// Token: 0x0600247A RID: 9338 RVA: 0x000A5600 File Offset: 0x000A3800
		public override void SetCapacity(int capacity)
		{
			SqlChars[] array = new SqlChars[capacity];
			if (this._values != null)
			{
				Array.Copy(this._values, 0, array, 0, Math.Min(capacity, this._values.Length));
			}
			this._values = array;
		}

		// Token: 0x0600247B RID: 9339 RVA: 0x000A5640 File Offset: 0x000A3840
		public override object ConvertXmlToObject(string s)
		{
			SqlString sqlString = default(SqlString);
			TextReader input = new StringReader("<col>" + s + "</col>");
			IXmlSerializable xmlSerializable = sqlString;
			using (XmlTextReader xmlTextReader = new XmlTextReader(input))
			{
				xmlSerializable.ReadXml(xmlTextReader);
			}
			return new SqlChars((SqlString)xmlSerializable);
		}

		// Token: 0x0600247C RID: 9340 RVA: 0x000A56A8 File Offset: 0x000A38A8
		public override string ConvertObjectToXml(object value)
		{
			StringWriter stringWriter = new StringWriter(base.FormatProvider);
			using (XmlTextWriter xmlTextWriter = new XmlTextWriter(stringWriter))
			{
				((IXmlSerializable)value).WriteXml(xmlTextWriter);
			}
			return stringWriter.ToString();
		}

		// Token: 0x0600247D RID: 9341 RVA: 0x000A56F8 File Offset: 0x000A38F8
		protected override object GetEmptyStorage(int recordCount)
		{
			return new SqlChars[recordCount];
		}

		// Token: 0x0600247E RID: 9342 RVA: 0x000A5700 File Offset: 0x000A3900
		protected override void CopyValue(int record, object store, BitArray nullbits, int storeIndex)
		{
			((SqlChars[])store)[storeIndex] = this._values[record];
			nullbits.Set(storeIndex, this.IsNull(record));
		}

		// Token: 0x0600247F RID: 9343 RVA: 0x000A5722 File Offset: 0x000A3922
		protected override void SetStorage(object store, BitArray nullbits)
		{
			this._values = (SqlChars[])store;
		}

		// Token: 0x04001652 RID: 5714
		private SqlChars[] _values;
	}
}
