﻿using System;
using System.Collections;
using System.Data.SqlTypes;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace System.Data.Common
{
	// Token: 0x020002D0 RID: 720
	internal sealed class SqlBinaryStorage : DataStorage
	{
		// Token: 0x06002446 RID: 9286 RVA: 0x000A49FF File Offset: 0x000A2BFF
		public SqlBinaryStorage(DataColumn column) : base(column, typeof(SqlBinary), SqlBinary.Null, SqlBinary.Null, StorageType.SqlBinary)
		{
		}

		// Token: 0x06002447 RID: 9287 RVA: 0x000A4A28 File Offset: 0x000A2C28
		public override object Aggregate(int[] records, AggregateType kind)
		{
			try
			{
				if (kind != AggregateType.First)
				{
					if (kind == AggregateType.Count)
					{
						int num = 0;
						for (int i = 0; i < records.Length; i++)
						{
							if (!this.IsNull(records[i]))
							{
								num++;
							}
						}
						return num;
					}
				}
				else
				{
					if (records.Length != 0)
					{
						return this._values[records[0]];
					}
					return null;
				}
			}
			catch (OverflowException)
			{
				throw ExprException.Overflow(typeof(SqlBinary));
			}
			throw ExceptionBuilder.AggregateException(kind, this._dataType);
		}

		// Token: 0x06002448 RID: 9288 RVA: 0x000A4AB8 File Offset: 0x000A2CB8
		public override int Compare(int recordNo1, int recordNo2)
		{
			return this._values[recordNo1].CompareTo(this._values[recordNo2]);
		}

		// Token: 0x06002449 RID: 9289 RVA: 0x000A4AD7 File Offset: 0x000A2CD7
		public override int CompareValueTo(int recordNo, object value)
		{
			return this._values[recordNo].CompareTo((SqlBinary)value);
		}

		// Token: 0x0600244A RID: 9290 RVA: 0x000A4AF0 File Offset: 0x000A2CF0
		public override object ConvertValue(object value)
		{
			if (value != null)
			{
				return SqlConvert.ConvertToSqlBinary(value);
			}
			return this._nullValue;
		}

		// Token: 0x0600244B RID: 9291 RVA: 0x000A4B07 File Offset: 0x000A2D07
		public override void Copy(int recordNo1, int recordNo2)
		{
			this._values[recordNo2] = this._values[recordNo1];
		}

		// Token: 0x0600244C RID: 9292 RVA: 0x000A4B21 File Offset: 0x000A2D21
		public override object Get(int record)
		{
			return this._values[record];
		}

		// Token: 0x0600244D RID: 9293 RVA: 0x000A4B34 File Offset: 0x000A2D34
		public override bool IsNull(int record)
		{
			return this._values[record].IsNull;
		}

		// Token: 0x0600244E RID: 9294 RVA: 0x000A4B47 File Offset: 0x000A2D47
		public override void Set(int record, object value)
		{
			this._values[record] = SqlConvert.ConvertToSqlBinary(value);
		}

		// Token: 0x0600244F RID: 9295 RVA: 0x000A4B5C File Offset: 0x000A2D5C
		public override void SetCapacity(int capacity)
		{
			SqlBinary[] array = new SqlBinary[capacity];
			if (this._values != null)
			{
				Array.Copy(this._values, 0, array, 0, Math.Min(capacity, this._values.Length));
			}
			this._values = array;
		}

		// Token: 0x06002450 RID: 9296 RVA: 0x000A4B9C File Offset: 0x000A2D9C
		public override object ConvertXmlToObject(string s)
		{
			SqlBinary sqlBinary = default(SqlBinary);
			TextReader input = new StringReader("<col>" + s + "</col>");
			IXmlSerializable xmlSerializable = sqlBinary;
			using (XmlTextReader xmlTextReader = new XmlTextReader(input))
			{
				xmlSerializable.ReadXml(xmlTextReader);
			}
			return (SqlBinary)xmlSerializable;
		}

		// Token: 0x06002451 RID: 9297 RVA: 0x000A4C04 File Offset: 0x000A2E04
		public override string ConvertObjectToXml(object value)
		{
			StringWriter stringWriter = new StringWriter(base.FormatProvider);
			using (XmlTextWriter xmlTextWriter = new XmlTextWriter(stringWriter))
			{
				((IXmlSerializable)value).WriteXml(xmlTextWriter);
			}
			return stringWriter.ToString();
		}

		// Token: 0x06002452 RID: 9298 RVA: 0x000A4C54 File Offset: 0x000A2E54
		protected override object GetEmptyStorage(int recordCount)
		{
			return new SqlBinary[recordCount];
		}

		// Token: 0x06002453 RID: 9299 RVA: 0x000A4C5C File Offset: 0x000A2E5C
		protected override void CopyValue(int record, object store, BitArray nullbits, int storeIndex)
		{
			((SqlBinary[])store)[storeIndex] = this._values[record];
			nullbits.Set(storeIndex, this.IsNull(record));
		}

		// Token: 0x06002454 RID: 9300 RVA: 0x000A4C86 File Offset: 0x000A2E86
		protected override void SetStorage(object store, BitArray nullbits)
		{
			this._values = (SqlBinary[])store;
		}

		// Token: 0x0400164F RID: 5711
		private SqlBinary[] _values;
	}
}
