﻿using System;

namespace System.Data.Common
{
	/// <summary>Specifies the relationship between the columns in a GROUP BY clause and the non-aggregated columns in the select-list of a SELECT statement.</summary>
	// Token: 0x020002C4 RID: 708
	public enum GroupByBehavior
	{
		/// <summary>The support for the GROUP BY clause is unknown.</summary>
		// Token: 0x04001627 RID: 5671
		Unknown,
		/// <summary>The GROUP BY clause is not supported.</summary>
		// Token: 0x04001628 RID: 5672
		NotSupported,
		/// <summary>There is no relationship between the columns in the GROUP BY clause and the nonaggregated columns in the SELECT list. You may group by any column.</summary>
		// Token: 0x04001629 RID: 5673
		Unrelated,
		/// <summary>The GROUP BY clause must contain all nonaggregated columns in the select list, and can contain other columns not in the select list.</summary>
		// Token: 0x0400162A RID: 5674
		MustContainAll,
		/// <summary>The GROUP BY clause must contain all nonaggregated columns in the select list, and must not contain other columns not in the select list.</summary>
		// Token: 0x0400162B RID: 5675
		ExactMatch
	}
}
