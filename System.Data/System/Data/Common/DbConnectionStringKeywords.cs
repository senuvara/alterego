﻿using System;

namespace System.Data.Common
{
	// Token: 0x020002F1 RID: 753
	internal static class DbConnectionStringKeywords
	{
		// Token: 0x040016C7 RID: 5831
		internal const string ApplicationIntent = "ApplicationIntent";

		// Token: 0x040016C8 RID: 5832
		internal const string ApplicationName = "Application Name";

		// Token: 0x040016C9 RID: 5833
		internal const string AsynchronousProcessing = "Asynchronous Processing";

		// Token: 0x040016CA RID: 5834
		internal const string AttachDBFilename = "AttachDbFilename";

		// Token: 0x040016CB RID: 5835
		internal const string ConnectTimeout = "Connect Timeout";

		// Token: 0x040016CC RID: 5836
		internal const string ConnectionReset = "Connection Reset";

		// Token: 0x040016CD RID: 5837
		internal const string ContextConnection = "Context Connection";

		// Token: 0x040016CE RID: 5838
		internal const string CurrentLanguage = "Current Language";

		// Token: 0x040016CF RID: 5839
		internal const string Encrypt = "Encrypt";

		// Token: 0x040016D0 RID: 5840
		internal const string FailoverPartner = "Failover Partner";

		// Token: 0x040016D1 RID: 5841
		internal const string InitialCatalog = "Initial Catalog";

		// Token: 0x040016D2 RID: 5842
		internal const string MultipleActiveResultSets = "MultipleActiveResultSets";

		// Token: 0x040016D3 RID: 5843
		internal const string MultiSubnetFailover = "MultiSubnetFailover";

		// Token: 0x040016D4 RID: 5844
		internal const string NetworkLibrary = "Network Library";

		// Token: 0x040016D5 RID: 5845
		internal const string PacketSize = "Packet Size";

		// Token: 0x040016D6 RID: 5846
		internal const string Replication = "Replication";

		// Token: 0x040016D7 RID: 5847
		internal const string TransactionBinding = "Transaction Binding";

		// Token: 0x040016D8 RID: 5848
		internal const string TrustServerCertificate = "TrustServerCertificate";

		// Token: 0x040016D9 RID: 5849
		internal const string TypeSystemVersion = "Type System Version";

		// Token: 0x040016DA RID: 5850
		internal const string UserInstance = "User Instance";

		// Token: 0x040016DB RID: 5851
		internal const string WorkstationID = "Workstation ID";

		// Token: 0x040016DC RID: 5852
		internal const string ConnectRetryCount = "ConnectRetryCount";

		// Token: 0x040016DD RID: 5853
		internal const string ConnectRetryInterval = "ConnectRetryInterval";

		// Token: 0x040016DE RID: 5854
		internal const string DataSource = "Data Source";

		// Token: 0x040016DF RID: 5855
		internal const string IntegratedSecurity = "Integrated Security";

		// Token: 0x040016E0 RID: 5856
		internal const string Password = "Password";

		// Token: 0x040016E1 RID: 5857
		internal const string Driver = "Driver";

		// Token: 0x040016E2 RID: 5858
		internal const string PersistSecurityInfo = "Persist Security Info";

		// Token: 0x040016E3 RID: 5859
		internal const string UserID = "User ID";

		// Token: 0x040016E4 RID: 5860
		internal const string Enlist = "Enlist";

		// Token: 0x040016E5 RID: 5861
		internal const string LoadBalanceTimeout = "Load Balance Timeout";

		// Token: 0x040016E6 RID: 5862
		internal const string MaxPoolSize = "Max Pool Size";

		// Token: 0x040016E7 RID: 5863
		internal const string Pooling = "Pooling";

		// Token: 0x040016E8 RID: 5864
		internal const string MinPoolSize = "Min Pool Size";
	}
}
