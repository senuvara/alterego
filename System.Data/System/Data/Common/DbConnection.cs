﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;

namespace System.Data.Common
{
	/// <summary>Represents a connection to a database. </summary>
	// Token: 0x020002AF RID: 687
	public abstract class DbConnection : Component, IDbConnection, IDisposable
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Data.Common.DbConnection" /> class.</summary>
		// Token: 0x0600222B RID: 8747 RVA: 0x0009D002 File Offset: 0x0009B202
		protected DbConnection()
		{
		}

		/// <summary>Gets or sets the string used to open the connection.</summary>
		/// <returns>The connection string used to establish the initial connection. The exact contents of the connection string depend on the specific data source for this connection. The default value is an empty string.</returns>
		// Token: 0x170005D6 RID: 1494
		// (get) Token: 0x0600222C RID: 8748
		// (set) Token: 0x0600222D RID: 8749
		[RecommendedAsConfigurable(true)]
		[RefreshProperties(RefreshProperties.All)]
		[SettingsBindable(true)]
		[DefaultValue("")]
		public abstract string ConnectionString { get; set; }

		/// <summary>Gets the time to wait while establishing a connection before terminating the attempt and generating an error.</summary>
		/// <returns>The time (in seconds) to wait for a connection to open. The default value is determined by the specific type of connection that you are using.</returns>
		// Token: 0x170005D7 RID: 1495
		// (get) Token: 0x0600222E RID: 8750 RVA: 0x0009D2B5 File Offset: 0x0009B4B5
		public virtual int ConnectionTimeout
		{
			get
			{
				return 15;
			}
		}

		/// <summary>Gets the name of the current database after a connection is opened, or the database name specified in the connection string before the connection is opened.</summary>
		/// <returns>The name of the current database or the name of the database to be used after a connection is opened. The default value is an empty string.</returns>
		// Token: 0x170005D8 RID: 1496
		// (get) Token: 0x0600222F RID: 8751
		public abstract string Database { get; }

		/// <summary>Gets the name of the database server to which to connect.</summary>
		/// <returns>The name of the database server to which to connect. The default value is an empty string.</returns>
		// Token: 0x170005D9 RID: 1497
		// (get) Token: 0x06002230 RID: 8752
		public abstract string DataSource { get; }

		/// <summary>Gets the <see cref="T:System.Data.Common.DbProviderFactory" /> for this <see cref="T:System.Data.Common.DbConnection" />.</summary>
		/// <returns>A set of methods for creating instances of a provider's implementation of the data source classes.</returns>
		// Token: 0x170005DA RID: 1498
		// (get) Token: 0x06002231 RID: 8753 RVA: 0x00004526 File Offset: 0x00002726
		protected virtual DbProviderFactory DbProviderFactory
		{
			get
			{
				return null;
			}
		}

		/// <summary>Gets a string that represents the version of the server to which the object is connected.</summary>
		/// <returns>The version of the database. The format of the string returned depends on the specific type of connection you are using.</returns>
		/// <exception cref="T:System.InvalidOperationException">
		///         <see cref="P:System.Data.Common.DbConnection.ServerVersion" /> was called while the returned Task was not completed and the connection was not opened after a call to <see cref="Overload:System.Data.Common.DbConnection.OpenAsync" />.</exception>
		// Token: 0x170005DB RID: 1499
		// (get) Token: 0x06002232 RID: 8754
		[Browsable(false)]
		public abstract string ServerVersion { get; }

		/// <summary>Gets a string that describes the state of the connection.</summary>
		/// <returns>The state of the connection. The format of the string returned depends on the specific type of connection you are using.</returns>
		// Token: 0x170005DC RID: 1500
		// (get) Token: 0x06002233 RID: 8755
		[Browsable(false)]
		public abstract ConnectionState State { get; }

		/// <summary>Occurs when the state of the event changes.</summary>
		// Token: 0x14000027 RID: 39
		// (add) Token: 0x06002234 RID: 8756 RVA: 0x0009D2BC File Offset: 0x0009B4BC
		// (remove) Token: 0x06002235 RID: 8757 RVA: 0x0009D2F4 File Offset: 0x0009B4F4
		public virtual event StateChangeEventHandler StateChange
		{
			[CompilerGenerated]
			add
			{
				StateChangeEventHandler stateChangeEventHandler = this.StateChange;
				StateChangeEventHandler stateChangeEventHandler2;
				do
				{
					stateChangeEventHandler2 = stateChangeEventHandler;
					StateChangeEventHandler value2 = (StateChangeEventHandler)Delegate.Combine(stateChangeEventHandler2, value);
					stateChangeEventHandler = Interlocked.CompareExchange<StateChangeEventHandler>(ref this.StateChange, value2, stateChangeEventHandler2);
				}
				while (stateChangeEventHandler != stateChangeEventHandler2);
			}
			[CompilerGenerated]
			remove
			{
				StateChangeEventHandler stateChangeEventHandler = this.StateChange;
				StateChangeEventHandler stateChangeEventHandler2;
				do
				{
					stateChangeEventHandler2 = stateChangeEventHandler;
					StateChangeEventHandler value2 = (StateChangeEventHandler)Delegate.Remove(stateChangeEventHandler2, value);
					stateChangeEventHandler = Interlocked.CompareExchange<StateChangeEventHandler>(ref this.StateChange, value2, stateChangeEventHandler2);
				}
				while (stateChangeEventHandler != stateChangeEventHandler2);
			}
		}

		/// <summary>Starts a database transaction.</summary>
		/// <param name="isolationLevel">Specifies the isolation level for the transaction.</param>
		/// <returns>An object representing the new transaction.</returns>
		// Token: 0x06002236 RID: 8758
		protected abstract DbTransaction BeginDbTransaction(IsolationLevel isolationLevel);

		/// <summary>Starts a database transaction.</summary>
		/// <returns>An object representing the new transaction.</returns>
		// Token: 0x06002237 RID: 8759 RVA: 0x0009D329 File Offset: 0x0009B529
		public DbTransaction BeginTransaction()
		{
			return this.BeginDbTransaction(IsolationLevel.Unspecified);
		}

		/// <summary>Starts a database transaction with the specified isolation level.</summary>
		/// <param name="isolationLevel">Specifies the isolation level for the transaction.</param>
		/// <returns>An object representing the new transaction.</returns>
		// Token: 0x06002238 RID: 8760 RVA: 0x0009D332 File Offset: 0x0009B532
		public DbTransaction BeginTransaction(IsolationLevel isolationLevel)
		{
			return this.BeginDbTransaction(isolationLevel);
		}

		/// <summary>Begins a database transaction.</summary>
		/// <returns>An object that represents the new transaction.</returns>
		// Token: 0x06002239 RID: 8761 RVA: 0x0009D329 File Offset: 0x0009B529
		IDbTransaction IDbConnection.BeginTransaction()
		{
			return this.BeginDbTransaction(IsolationLevel.Unspecified);
		}

		/// <summary>Begins a database transaction with the specified <see cref="T:System.Data.IsolationLevel" /> value.</summary>
		/// <param name="isolationLevel">One of the <see cref="T:System.Data.IsolationLevel" /> values.</param>
		/// <returns>An object that represents the new transaction.</returns>
		// Token: 0x0600223A RID: 8762 RVA: 0x0009D332 File Offset: 0x0009B532
		IDbTransaction IDbConnection.BeginTransaction(IsolationLevel isolationLevel)
		{
			return this.BeginDbTransaction(isolationLevel);
		}

		/// <summary>Closes the connection to the database. This is the preferred method of closing any open connection.</summary>
		/// <exception cref="T:System.Data.Common.DbException">The connection-level error that occurred while opening the connection. </exception>
		// Token: 0x0600223B RID: 8763
		public abstract void Close();

		/// <summary>Changes the current database for an open connection.</summary>
		/// <param name="databaseName">Specifies the name of the database for the connection to use.</param>
		// Token: 0x0600223C RID: 8764
		public abstract void ChangeDatabase(string databaseName);

		/// <summary>Creates and returns a <see cref="T:System.Data.Common.DbCommand" /> object associated with the current connection.</summary>
		/// <returns>A <see cref="T:System.Data.Common.DbCommand" /> object.</returns>
		// Token: 0x0600223D RID: 8765 RVA: 0x0009D33B File Offset: 0x0009B53B
		public DbCommand CreateCommand()
		{
			return this.CreateDbCommand();
		}

		/// <summary>Creates and returns a <see cref="T:System.Data.Common.DbCommand" /> object that is associated with the current connection.</summary>
		/// <returns>A <see cref="T:System.Data.Common.DbCommand" /> object that is associated with the connection.</returns>
		// Token: 0x0600223E RID: 8766 RVA: 0x0009D33B File Offset: 0x0009B53B
		IDbCommand IDbConnection.CreateCommand()
		{
			return this.CreateDbCommand();
		}

		/// <summary>Creates and returns a <see cref="T:System.Data.Common.DbCommand" /> object associated with the current connection.</summary>
		/// <returns>A <see cref="T:System.Data.Common.DbCommand" /> object.</returns>
		// Token: 0x0600223F RID: 8767
		protected abstract DbCommand CreateDbCommand();

		/// <summary>Enlists in the specified transaction.</summary>
		/// <param name="transaction">A reference to an existing <see cref="T:System.Transactions.Transaction" /> in which to enlist.</param>
		// Token: 0x06002240 RID: 8768 RVA: 0x0005CDBE File Offset: 0x0005AFBE
		public virtual void EnlistTransaction(Transaction transaction)
		{
			throw ADP.NotSupported();
		}

		/// <summary>Returns schema information for the data source of this <see cref="T:System.Data.Common.DbConnection" />.</summary>
		/// <returns>A <see cref="T:System.Data.DataTable" /> that contains schema information.</returns>
		// Token: 0x06002241 RID: 8769 RVA: 0x0005CDBE File Offset: 0x0005AFBE
		public virtual DataTable GetSchema()
		{
			throw ADP.NotSupported();
		}

		/// <summary>Returns schema information for the data source of this <see cref="T:System.Data.Common.DbConnection" /> using the specified string for the schema name.</summary>
		/// <param name="collectionName">Specifies the name of the schema to return. </param>
		/// <returns>A <see cref="T:System.Data.DataTable" /> that contains schema information.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="collectionName" /> is specified as null.</exception>
		// Token: 0x06002242 RID: 8770 RVA: 0x0005CDBE File Offset: 0x0005AFBE
		public virtual DataTable GetSchema(string collectionName)
		{
			throw ADP.NotSupported();
		}

		/// <summary>Returns schema information for the data source of this <see cref="T:System.Data.Common.DbConnection" /> using the specified string for the schema name and the specified string array for the restriction values.</summary>
		/// <param name="collectionName">Specifies the name of the schema to return.</param>
		/// <param name="restrictionValues">Specifies a set of restriction values for the requested schema.</param>
		/// <returns>A <see cref="T:System.Data.DataTable" /> that contains schema information.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="collectionName" /> is specified as null.</exception>
		// Token: 0x06002243 RID: 8771 RVA: 0x0005CDBE File Offset: 0x0005AFBE
		public virtual DataTable GetSchema(string collectionName, string[] restrictionValues)
		{
			throw ADP.NotSupported();
		}

		/// <summary>Raises the <see cref="E:System.Data.Common.DbConnection.StateChange" /> event.</summary>
		/// <param name="stateChange">A <see cref="T:System.Data.StateChangeEventArgs" /> that contains the event data.</param>
		// Token: 0x06002244 RID: 8772 RVA: 0x0009D343 File Offset: 0x0009B543
		protected virtual void OnStateChange(StateChangeEventArgs stateChange)
		{
			if (this._suppressStateChangeForReconnection)
			{
				return;
			}
			StateChangeEventHandler stateChange2 = this.StateChange;
			if (stateChange2 == null)
			{
				return;
			}
			stateChange2(this, stateChange);
		}

		/// <summary>Opens a database connection with the settings specified by the <see cref="P:System.Data.Common.DbConnection.ConnectionString" />.</summary>
		// Token: 0x06002245 RID: 8773
		public abstract void Open();

		/// <summary>An asynchronous version of <see cref="M:System.Data.Common.DbConnection.Open" />, which opens a database connection with the settings specified by the <see cref="P:System.Data.Common.DbConnection.ConnectionString" />. This method invokes the virtual method <see cref="M:System.Data.Common.DbConnection.OpenAsync(System.Threading.CancellationToken)" /> with CancellationToken.None.</summary>
		/// <returns>A task representing the asynchronous operation.</returns>
		// Token: 0x06002246 RID: 8774 RVA: 0x0009D360 File Offset: 0x0009B560
		public Task OpenAsync()
		{
			return this.OpenAsync(CancellationToken.None);
		}

		/// <summary>This is the asynchronous version of <see cref="M:System.Data.Common.DbConnection.Open" />. Providers should override with an appropriate implementation. The cancellation token can optionally be honored.The default implementation invokes the synchronous <see cref="M:System.Data.Common.DbConnection.Open" /> call and returns a completed task. The default implementation will return a cancelled task if passed an already cancelled cancellationToken. Exceptions thrown by Open will be communicated via the returned Task Exception property.Do not invoke other methods and properties of the <see langword="DbConnection" /> object until the returned Task is complete.</summary>
		/// <param name="cancellationToken">The cancellation instruction.</param>
		/// <returns>A task representing the asynchronous operation.</returns>
		// Token: 0x06002247 RID: 8775 RVA: 0x0009D370 File Offset: 0x0009B570
		public virtual Task OpenAsync(CancellationToken cancellationToken)
		{
			if (cancellationToken.IsCancellationRequested)
			{
				return Task.FromCanceled(cancellationToken);
			}
			Task result;
			try
			{
				this.Open();
				result = Task.CompletedTask;
			}
			catch (Exception exception)
			{
				result = Task.FromException(exception);
			}
			return result;
		}

		// Token: 0x170005DD RID: 1501
		// (get) Token: 0x06002248 RID: 8776 RVA: 0x0009D3B8 File Offset: 0x0009B5B8
		internal DbProviderFactory ProviderFactory
		{
			get
			{
				return this.DbProviderFactory;
			}
		}

		// Token: 0x040015CD RID: 5581
		internal bool _suppressStateChangeForReconnection;

		// Token: 0x040015CE RID: 5582
		[CompilerGenerated]
		private StateChangeEventHandler StateChange;
	}
}
