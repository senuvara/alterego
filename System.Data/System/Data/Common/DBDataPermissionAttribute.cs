﻿using System;
using System.ComponentModel;
using System.Security.Permissions;

namespace System.Data.Common
{
	/// <summary>Associates a security action with a custom security attribute. </summary>
	// Token: 0x020002F5 RID: 757
	[AttributeUsage(AttributeTargets.Assembly | AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Constructor | AttributeTargets.Method, AllowMultiple = true, Inherited = false)]
	[Serializable]
	public abstract class DBDataPermissionAttribute : CodeAccessSecurityAttribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Data.Common.DBDataPermissionAttribute" />.</summary>
		/// <param name="action">One of the security action values representing an action that can be performed by declarative security.</param>
		// Token: 0x060025E1 RID: 9697 RVA: 0x000ACCFC File Offset: 0x000AAEFC
		protected DBDataPermissionAttribute(SecurityAction action) : base(action)
		{
		}

		/// <summary>Gets or sets a value indicating whether a blank password is allowed.</summary>
		/// <returns>
		///     <see langword="true" /> if a blank password is allowed; otherwise <see langword="false" />.</returns>
		// Token: 0x17000643 RID: 1603
		// (get) Token: 0x060025E2 RID: 9698 RVA: 0x000ACD05 File Offset: 0x000AAF05
		// (set) Token: 0x060025E3 RID: 9699 RVA: 0x000ACD0D File Offset: 0x000AAF0D
		public bool AllowBlankPassword
		{
			get
			{
				return this._allowBlankPassword;
			}
			set
			{
				this._allowBlankPassword = value;
			}
		}

		/// <summary>Gets or sets a permitted connection string.</summary>
		/// <returns>A permitted connection string.</returns>
		// Token: 0x17000644 RID: 1604
		// (get) Token: 0x060025E4 RID: 9700 RVA: 0x000ACD18 File Offset: 0x000AAF18
		// (set) Token: 0x060025E5 RID: 9701 RVA: 0x000ACD36 File Offset: 0x000AAF36
		public string ConnectionString
		{
			get
			{
				string connectionString = this._connectionString;
				if (connectionString == null)
				{
					return string.Empty;
				}
				return connectionString;
			}
			set
			{
				this._connectionString = value;
			}
		}

		/// <summary>Identifies whether the list of connection string parameters identified by the <see cref="P:System.Data.Common.DBDataPermissionAttribute.KeyRestrictions" /> property are the only connection string parameters allowed.</summary>
		/// <returns>One of the <see cref="T:System.Data.KeyRestrictionBehavior" /> values.</returns>
		// Token: 0x17000645 RID: 1605
		// (get) Token: 0x060025E6 RID: 9702 RVA: 0x000ACD3F File Offset: 0x000AAF3F
		// (set) Token: 0x060025E7 RID: 9703 RVA: 0x000ACD47 File Offset: 0x000AAF47
		public KeyRestrictionBehavior KeyRestrictionBehavior
		{
			get
			{
				return this._behavior;
			}
			set
			{
				if (value <= KeyRestrictionBehavior.PreventUsage)
				{
					this._behavior = value;
					return;
				}
				throw ADP.InvalidKeyRestrictionBehavior(value);
			}
		}

		/// <summary>Gets or sets connection string parameters that are allowed or disallowed.</summary>
		/// <returns>One or more connection string parameters that are allowed or disallowed.</returns>
		// Token: 0x17000646 RID: 1606
		// (get) Token: 0x060025E8 RID: 9704 RVA: 0x000ACD5C File Offset: 0x000AAF5C
		// (set) Token: 0x060025E9 RID: 9705 RVA: 0x000ACD7A File Offset: 0x000AAF7A
		public string KeyRestrictions
		{
			get
			{
				string restrictions = this._restrictions;
				if (restrictions == null)
				{
					return ADP.StrEmpty;
				}
				return restrictions;
			}
			set
			{
				this._restrictions = value;
			}
		}

		/// <summary>Identifies whether the attribute should serialize the connection string.</summary>
		/// <returns>
		///     <see langword="true" /> if the attribute should serialize the connection string; otherwise <see langword="false" />.</returns>
		// Token: 0x060025EA RID: 9706 RVA: 0x000ACD83 File Offset: 0x000AAF83
		[EditorBrowsable(EditorBrowsableState.Never)]
		public bool ShouldSerializeConnectionString()
		{
			return this._connectionString != null;
		}

		/// <summary>Identifies whether the attribute should serialize the set of key restrictions.</summary>
		/// <returns>
		///     <see langword="true" /> if the attribute should serialize the set of key restrictions; otherwise <see langword="false" />.</returns>
		// Token: 0x060025EB RID: 9707 RVA: 0x000ACD8E File Offset: 0x000AAF8E
		[EditorBrowsable(EditorBrowsableState.Never)]
		public bool ShouldSerializeKeyRestrictions()
		{
			return this._restrictions != null;
		}

		// Token: 0x0400170E RID: 5902
		private bool _allowBlankPassword;

		// Token: 0x0400170F RID: 5903
		private string _connectionString;

		// Token: 0x04001710 RID: 5904
		private string _restrictions;

		// Token: 0x04001711 RID: 5905
		private KeyRestrictionBehavior _behavior;
	}
}
