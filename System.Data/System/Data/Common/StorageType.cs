﻿using System;

namespace System.Data.Common
{
	// Token: 0x020002A5 RID: 677
	internal enum StorageType
	{
		// Token: 0x04001572 RID: 5490
		Empty,
		// Token: 0x04001573 RID: 5491
		Object,
		// Token: 0x04001574 RID: 5492
		DBNull,
		// Token: 0x04001575 RID: 5493
		Boolean,
		// Token: 0x04001576 RID: 5494
		Char,
		// Token: 0x04001577 RID: 5495
		SByte,
		// Token: 0x04001578 RID: 5496
		Byte,
		// Token: 0x04001579 RID: 5497
		Int16,
		// Token: 0x0400157A RID: 5498
		UInt16,
		// Token: 0x0400157B RID: 5499
		Int32,
		// Token: 0x0400157C RID: 5500
		UInt32,
		// Token: 0x0400157D RID: 5501
		Int64,
		// Token: 0x0400157E RID: 5502
		UInt64,
		// Token: 0x0400157F RID: 5503
		Single,
		// Token: 0x04001580 RID: 5504
		Double,
		// Token: 0x04001581 RID: 5505
		Decimal,
		// Token: 0x04001582 RID: 5506
		DateTime,
		// Token: 0x04001583 RID: 5507
		TimeSpan,
		// Token: 0x04001584 RID: 5508
		String,
		// Token: 0x04001585 RID: 5509
		Guid,
		// Token: 0x04001586 RID: 5510
		ByteArray,
		// Token: 0x04001587 RID: 5511
		CharArray,
		// Token: 0x04001588 RID: 5512
		Type,
		// Token: 0x04001589 RID: 5513
		DateTimeOffset,
		// Token: 0x0400158A RID: 5514
		BigInteger,
		// Token: 0x0400158B RID: 5515
		Uri,
		// Token: 0x0400158C RID: 5516
		SqlBinary,
		// Token: 0x0400158D RID: 5517
		SqlBoolean,
		// Token: 0x0400158E RID: 5518
		SqlByte,
		// Token: 0x0400158F RID: 5519
		SqlBytes,
		// Token: 0x04001590 RID: 5520
		SqlChars,
		// Token: 0x04001591 RID: 5521
		SqlDateTime,
		// Token: 0x04001592 RID: 5522
		SqlDecimal,
		// Token: 0x04001593 RID: 5523
		SqlDouble,
		// Token: 0x04001594 RID: 5524
		SqlGuid,
		// Token: 0x04001595 RID: 5525
		SqlInt16,
		// Token: 0x04001596 RID: 5526
		SqlInt32,
		// Token: 0x04001597 RID: 5527
		SqlInt64,
		// Token: 0x04001598 RID: 5528
		SqlMoney,
		// Token: 0x04001599 RID: 5529
		SqlSingle,
		// Token: 0x0400159A RID: 5530
		SqlString
	}
}
