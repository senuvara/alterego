﻿using System;
using System.Collections;
using System.Data.SqlTypes;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace System.Data.Common
{
	// Token: 0x020002D2 RID: 722
	internal sealed class SqlBytesStorage : DataStorage
	{
		// Token: 0x06002464 RID: 9316 RVA: 0x000A52D7 File Offset: 0x000A34D7
		public SqlBytesStorage(DataColumn column) : base(column, typeof(SqlBytes), SqlBytes.Null, SqlBytes.Null, StorageType.SqlBytes)
		{
		}

		// Token: 0x06002465 RID: 9317 RVA: 0x000A52F8 File Offset: 0x000A34F8
		public override object Aggregate(int[] records, AggregateType kind)
		{
			try
			{
				if (kind != AggregateType.First)
				{
					if (kind == AggregateType.Count)
					{
						int num = 0;
						for (int i = 0; i < records.Length; i++)
						{
							if (!this.IsNull(records[i]))
							{
								num++;
							}
						}
						return num;
					}
				}
				else
				{
					if (records.Length != 0)
					{
						return this._values[records[0]];
					}
					return null;
				}
			}
			catch (OverflowException)
			{
				throw ExprException.Overflow(typeof(SqlBytes));
			}
			throw ExceptionBuilder.AggregateException(kind, this._dataType);
		}

		// Token: 0x06002466 RID: 9318 RVA: 0x000061C5 File Offset: 0x000043C5
		public override int Compare(int recordNo1, int recordNo2)
		{
			return 0;
		}

		// Token: 0x06002467 RID: 9319 RVA: 0x000061C5 File Offset: 0x000043C5
		public override int CompareValueTo(int recordNo, object value)
		{
			return 0;
		}

		// Token: 0x06002468 RID: 9320 RVA: 0x000A5380 File Offset: 0x000A3580
		public override void Copy(int recordNo1, int recordNo2)
		{
			this._values[recordNo2] = this._values[recordNo1];
		}

		// Token: 0x06002469 RID: 9321 RVA: 0x000A5392 File Offset: 0x000A3592
		public override object Get(int record)
		{
			return this._values[record];
		}

		// Token: 0x0600246A RID: 9322 RVA: 0x000A539C File Offset: 0x000A359C
		public override bool IsNull(int record)
		{
			return this._values[record].IsNull;
		}

		// Token: 0x0600246B RID: 9323 RVA: 0x000A53AB File Offset: 0x000A35AB
		public override void Set(int record, object value)
		{
			if (value == DBNull.Value || value == null)
			{
				this._values[record] = SqlBytes.Null;
				return;
			}
			this._values[record] = (SqlBytes)value;
		}

		// Token: 0x0600246C RID: 9324 RVA: 0x000A53D4 File Offset: 0x000A35D4
		public override void SetCapacity(int capacity)
		{
			SqlBytes[] array = new SqlBytes[capacity];
			if (this._values != null)
			{
				Array.Copy(this._values, 0, array, 0, Math.Min(capacity, this._values.Length));
			}
			this._values = array;
		}

		// Token: 0x0600246D RID: 9325 RVA: 0x000A5414 File Offset: 0x000A3614
		public override object ConvertXmlToObject(string s)
		{
			SqlBinary sqlBinary = default(SqlBinary);
			TextReader input = new StringReader("<col>" + s + "</col>");
			IXmlSerializable xmlSerializable = sqlBinary;
			using (XmlTextReader xmlTextReader = new XmlTextReader(input))
			{
				xmlSerializable.ReadXml(xmlTextReader);
			}
			return new SqlBytes((SqlBinary)xmlSerializable);
		}

		// Token: 0x0600246E RID: 9326 RVA: 0x000A547C File Offset: 0x000A367C
		public override string ConvertObjectToXml(object value)
		{
			StringWriter stringWriter = new StringWriter(base.FormatProvider);
			using (XmlTextWriter xmlTextWriter = new XmlTextWriter(stringWriter))
			{
				((IXmlSerializable)value).WriteXml(xmlTextWriter);
			}
			return stringWriter.ToString();
		}

		// Token: 0x0600246F RID: 9327 RVA: 0x000A54CC File Offset: 0x000A36CC
		protected override object GetEmptyStorage(int recordCount)
		{
			return new SqlBytes[recordCount];
		}

		// Token: 0x06002470 RID: 9328 RVA: 0x000A54D4 File Offset: 0x000A36D4
		protected override void CopyValue(int record, object store, BitArray nullbits, int storeIndex)
		{
			((SqlBytes[])store)[storeIndex] = this._values[record];
			nullbits.Set(storeIndex, this.IsNull(record));
		}

		// Token: 0x06002471 RID: 9329 RVA: 0x000A54F6 File Offset: 0x000A36F6
		protected override void SetStorage(object store, BitArray nullbits)
		{
			this._values = (SqlBytes[])store;
		}

		// Token: 0x04001651 RID: 5713
		private SqlBytes[] _values;
	}
}
