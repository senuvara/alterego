﻿using System;

namespace System.Data.Common
{
	// Token: 0x0200029C RID: 668
	internal sealed class DbSchemaTable
	{
		// Token: 0x0600207C RID: 8316 RVA: 0x0009936D File Offset: 0x0009756D
		internal DbSchemaTable(DataTable dataTable, bool returnProviderSpecificTypes)
		{
			this._dataTable = dataTable;
			this._columns = dataTable.Columns;
			this._returnProviderSpecificTypes = returnProviderSpecificTypes;
		}

		// Token: 0x17000573 RID: 1395
		// (get) Token: 0x0600207D RID: 8317 RVA: 0x000993A1 File Offset: 0x000975A1
		internal DataColumn ColumnName
		{
			get
			{
				return this.CachedDataColumn(DbSchemaTable.ColumnEnum.ColumnName);
			}
		}

		// Token: 0x17000574 RID: 1396
		// (get) Token: 0x0600207E RID: 8318 RVA: 0x000993AA File Offset: 0x000975AA
		internal DataColumn Size
		{
			get
			{
				return this.CachedDataColumn(DbSchemaTable.ColumnEnum.ColumnSize);
			}
		}

		// Token: 0x17000575 RID: 1397
		// (get) Token: 0x0600207F RID: 8319 RVA: 0x000993B3 File Offset: 0x000975B3
		internal DataColumn BaseServerName
		{
			get
			{
				return this.CachedDataColumn(DbSchemaTable.ColumnEnum.BaseServerName);
			}
		}

		// Token: 0x17000576 RID: 1398
		// (get) Token: 0x06002080 RID: 8320 RVA: 0x000993BC File Offset: 0x000975BC
		internal DataColumn BaseColumnName
		{
			get
			{
				return this.CachedDataColumn(DbSchemaTable.ColumnEnum.BaseColumnName);
			}
		}

		// Token: 0x17000577 RID: 1399
		// (get) Token: 0x06002081 RID: 8321 RVA: 0x000993C5 File Offset: 0x000975C5
		internal DataColumn BaseTableName
		{
			get
			{
				return this.CachedDataColumn(DbSchemaTable.ColumnEnum.BaseTableName);
			}
		}

		// Token: 0x17000578 RID: 1400
		// (get) Token: 0x06002082 RID: 8322 RVA: 0x000993CE File Offset: 0x000975CE
		internal DataColumn BaseCatalogName
		{
			get
			{
				return this.CachedDataColumn(DbSchemaTable.ColumnEnum.BaseCatalogName);
			}
		}

		// Token: 0x17000579 RID: 1401
		// (get) Token: 0x06002083 RID: 8323 RVA: 0x000993D7 File Offset: 0x000975D7
		internal DataColumn BaseSchemaName
		{
			get
			{
				return this.CachedDataColumn(DbSchemaTable.ColumnEnum.BaseSchemaName);
			}
		}

		// Token: 0x1700057A RID: 1402
		// (get) Token: 0x06002084 RID: 8324 RVA: 0x000993E0 File Offset: 0x000975E0
		internal DataColumn IsAutoIncrement
		{
			get
			{
				return this.CachedDataColumn(DbSchemaTable.ColumnEnum.IsAutoIncrement);
			}
		}

		// Token: 0x1700057B RID: 1403
		// (get) Token: 0x06002085 RID: 8325 RVA: 0x000993E9 File Offset: 0x000975E9
		internal DataColumn IsUnique
		{
			get
			{
				return this.CachedDataColumn(DbSchemaTable.ColumnEnum.IsUnique);
			}
		}

		// Token: 0x1700057C RID: 1404
		// (get) Token: 0x06002086 RID: 8326 RVA: 0x000993F3 File Offset: 0x000975F3
		internal DataColumn IsKey
		{
			get
			{
				return this.CachedDataColumn(DbSchemaTable.ColumnEnum.IsKey);
			}
		}

		// Token: 0x1700057D RID: 1405
		// (get) Token: 0x06002087 RID: 8327 RVA: 0x000993FD File Offset: 0x000975FD
		internal DataColumn IsRowVersion
		{
			get
			{
				return this.CachedDataColumn(DbSchemaTable.ColumnEnum.IsRowVersion);
			}
		}

		// Token: 0x1700057E RID: 1406
		// (get) Token: 0x06002088 RID: 8328 RVA: 0x00099407 File Offset: 0x00097607
		internal DataColumn AllowDBNull
		{
			get
			{
				return this.CachedDataColumn(DbSchemaTable.ColumnEnum.AllowDBNull);
			}
		}

		// Token: 0x1700057F RID: 1407
		// (get) Token: 0x06002089 RID: 8329 RVA: 0x00099411 File Offset: 0x00097611
		internal DataColumn IsExpression
		{
			get
			{
				return this.CachedDataColumn(DbSchemaTable.ColumnEnum.IsExpression);
			}
		}

		// Token: 0x17000580 RID: 1408
		// (get) Token: 0x0600208A RID: 8330 RVA: 0x0009941B File Offset: 0x0009761B
		internal DataColumn IsHidden
		{
			get
			{
				return this.CachedDataColumn(DbSchemaTable.ColumnEnum.IsHidden);
			}
		}

		// Token: 0x17000581 RID: 1409
		// (get) Token: 0x0600208B RID: 8331 RVA: 0x00099425 File Offset: 0x00097625
		internal DataColumn IsLong
		{
			get
			{
				return this.CachedDataColumn(DbSchemaTable.ColumnEnum.IsLong);
			}
		}

		// Token: 0x17000582 RID: 1410
		// (get) Token: 0x0600208C RID: 8332 RVA: 0x0009942F File Offset: 0x0009762F
		internal DataColumn IsReadOnly
		{
			get
			{
				return this.CachedDataColumn(DbSchemaTable.ColumnEnum.IsReadOnly);
			}
		}

		// Token: 0x17000583 RID: 1411
		// (get) Token: 0x0600208D RID: 8333 RVA: 0x00099439 File Offset: 0x00097639
		internal DataColumn UnsortedIndex
		{
			get
			{
				return this.CachedDataColumn(DbSchemaTable.ColumnEnum.SchemaMappingUnsortedIndex);
			}
		}

		// Token: 0x17000584 RID: 1412
		// (get) Token: 0x0600208E RID: 8334 RVA: 0x00099443 File Offset: 0x00097643
		internal DataColumn DataType
		{
			get
			{
				if (this._returnProviderSpecificTypes)
				{
					return this.CachedDataColumn(DbSchemaTable.ColumnEnum.ProviderSpecificDataType, DbSchemaTable.ColumnEnum.DataType);
				}
				return this.CachedDataColumn(DbSchemaTable.ColumnEnum.DataType);
			}
		}

		// Token: 0x0600208F RID: 8335 RVA: 0x00099460 File Offset: 0x00097660
		private DataColumn CachedDataColumn(DbSchemaTable.ColumnEnum column)
		{
			return this.CachedDataColumn(column, column);
		}

		// Token: 0x06002090 RID: 8336 RVA: 0x0009946C File Offset: 0x0009766C
		private DataColumn CachedDataColumn(DbSchemaTable.ColumnEnum column, DbSchemaTable.ColumnEnum column2)
		{
			DataColumn dataColumn = this._columnCache[(int)column];
			if (dataColumn == null)
			{
				int num = this._columns.IndexOf(DbSchemaTable.s_DBCOLUMN_NAME[(int)column]);
				if (-1 == num && column != column2)
				{
					num = this._columns.IndexOf(DbSchemaTable.s_DBCOLUMN_NAME[(int)column2]);
				}
				if (-1 != num)
				{
					dataColumn = this._columns[num];
					this._columnCache[(int)column] = dataColumn;
				}
			}
			return dataColumn;
		}

		// Token: 0x06002091 RID: 8337 RVA: 0x000994D0 File Offset: 0x000976D0
		// Note: this type is marked as 'beforefieldinit'.
		static DbSchemaTable()
		{
		}

		// Token: 0x0400153E RID: 5438
		private static readonly string[] s_DBCOLUMN_NAME = new string[]
		{
			SchemaTableColumn.ColumnName,
			SchemaTableColumn.ColumnOrdinal,
			SchemaTableColumn.ColumnSize,
			SchemaTableOptionalColumn.BaseServerName,
			SchemaTableOptionalColumn.BaseCatalogName,
			SchemaTableColumn.BaseColumnName,
			SchemaTableColumn.BaseSchemaName,
			SchemaTableColumn.BaseTableName,
			SchemaTableOptionalColumn.IsAutoIncrement,
			SchemaTableColumn.IsUnique,
			SchemaTableColumn.IsKey,
			SchemaTableOptionalColumn.IsRowVersion,
			SchemaTableColumn.DataType,
			SchemaTableOptionalColumn.ProviderSpecificDataType,
			SchemaTableColumn.AllowDBNull,
			SchemaTableColumn.ProviderType,
			SchemaTableColumn.IsExpression,
			SchemaTableOptionalColumn.IsHidden,
			SchemaTableColumn.IsLong,
			SchemaTableOptionalColumn.IsReadOnly,
			"SchemaMapping Unsorted Index"
		};

		// Token: 0x0400153F RID: 5439
		internal DataTable _dataTable;

		// Token: 0x04001540 RID: 5440
		private DataColumnCollection _columns;

		// Token: 0x04001541 RID: 5441
		private DataColumn[] _columnCache = new DataColumn[DbSchemaTable.s_DBCOLUMN_NAME.Length];

		// Token: 0x04001542 RID: 5442
		private bool _returnProviderSpecificTypes;

		// Token: 0x0200029D RID: 669
		private enum ColumnEnum
		{
			// Token: 0x04001544 RID: 5444
			ColumnName,
			// Token: 0x04001545 RID: 5445
			ColumnOrdinal,
			// Token: 0x04001546 RID: 5446
			ColumnSize,
			// Token: 0x04001547 RID: 5447
			BaseServerName,
			// Token: 0x04001548 RID: 5448
			BaseCatalogName,
			// Token: 0x04001549 RID: 5449
			BaseColumnName,
			// Token: 0x0400154A RID: 5450
			BaseSchemaName,
			// Token: 0x0400154B RID: 5451
			BaseTableName,
			// Token: 0x0400154C RID: 5452
			IsAutoIncrement,
			// Token: 0x0400154D RID: 5453
			IsUnique,
			// Token: 0x0400154E RID: 5454
			IsKey,
			// Token: 0x0400154F RID: 5455
			IsRowVersion,
			// Token: 0x04001550 RID: 5456
			DataType,
			// Token: 0x04001551 RID: 5457
			ProviderSpecificDataType,
			// Token: 0x04001552 RID: 5458
			AllowDBNull,
			// Token: 0x04001553 RID: 5459
			ProviderType,
			// Token: 0x04001554 RID: 5460
			IsExpression,
			// Token: 0x04001555 RID: 5461
			IsHidden,
			// Token: 0x04001556 RID: 5462
			IsLong,
			// Token: 0x04001557 RID: 5463
			IsReadOnly,
			// Token: 0x04001558 RID: 5464
			SchemaMappingUnsortedIndex
		}
	}
}
