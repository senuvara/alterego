﻿using System;

namespace System.Data.Common
{
	// Token: 0x02000290 RID: 656
	internal class DbConnectionPoolKey : ICloneable
	{
		// Token: 0x06001FCA RID: 8138 RVA: 0x00095EDE File Offset: 0x000940DE
		internal DbConnectionPoolKey(string connectionString)
		{
			this._connectionString = connectionString;
		}

		// Token: 0x06001FCB RID: 8139 RVA: 0x00095EED File Offset: 0x000940ED
		protected DbConnectionPoolKey(DbConnectionPoolKey key)
		{
			this._connectionString = key.ConnectionString;
		}

		// Token: 0x06001FCC RID: 8140 RVA: 0x00095F01 File Offset: 0x00094101
		public virtual object Clone()
		{
			return new DbConnectionPoolKey(this);
		}

		// Token: 0x1700054D RID: 1357
		// (get) Token: 0x06001FCD RID: 8141 RVA: 0x00095F09 File Offset: 0x00094109
		// (set) Token: 0x06001FCE RID: 8142 RVA: 0x00095F11 File Offset: 0x00094111
		internal virtual string ConnectionString
		{
			get
			{
				return this._connectionString;
			}
			set
			{
				this._connectionString = value;
			}
		}

		// Token: 0x06001FCF RID: 8143 RVA: 0x00095F1C File Offset: 0x0009411C
		public override bool Equals(object obj)
		{
			if (obj == null || obj.GetType() != typeof(DbConnectionPoolKey))
			{
				return false;
			}
			DbConnectionPoolKey dbConnectionPoolKey = obj as DbConnectionPoolKey;
			return dbConnectionPoolKey != null && this._connectionString == dbConnectionPoolKey._connectionString;
		}

		// Token: 0x06001FD0 RID: 8144 RVA: 0x00095F62 File Offset: 0x00094162
		public override int GetHashCode()
		{
			if (this._connectionString != null)
			{
				return this._connectionString.GetHashCode();
			}
			return 0;
		}

		// Token: 0x040014E9 RID: 5353
		private string _connectionString;
	}
}
