﻿using System;

namespace System.Data.Common
{
	// Token: 0x02000293 RID: 659
	[Serializable]
	internal sealed class NameValuePair
	{
		// Token: 0x06001FD6 RID: 8150 RVA: 0x000962EC File Offset: 0x000944EC
		internal NameValuePair(string name, string value, int length)
		{
			this._name = name;
			this._value = value;
			this._length = length;
		}

		// Token: 0x1700054E RID: 1358
		// (get) Token: 0x06001FD7 RID: 8151 RVA: 0x00096309 File Offset: 0x00094509
		internal int Length
		{
			get
			{
				return this._length;
			}
		}

		// Token: 0x1700054F RID: 1359
		// (get) Token: 0x06001FD8 RID: 8152 RVA: 0x00096311 File Offset: 0x00094511
		internal string Name
		{
			get
			{
				return this._name;
			}
		}

		// Token: 0x17000550 RID: 1360
		// (get) Token: 0x06001FD9 RID: 8153 RVA: 0x00096319 File Offset: 0x00094519
		internal string Value
		{
			get
			{
				return this._value;
			}
		}

		// Token: 0x17000551 RID: 1361
		// (get) Token: 0x06001FDA RID: 8154 RVA: 0x00096321 File Offset: 0x00094521
		// (set) Token: 0x06001FDB RID: 8155 RVA: 0x00096329 File Offset: 0x00094529
		internal NameValuePair Next
		{
			get
			{
				return this._next;
			}
			set
			{
				if (this._next != null || value == null)
				{
					throw ADP.InternalError(ADP.InternalErrorCode.NameValuePairNext);
				}
				this._next = value;
			}
		}

		// Token: 0x040014F6 RID: 5366
		private readonly string _name;

		// Token: 0x040014F7 RID: 5367
		private readonly string _value;

		// Token: 0x040014F8 RID: 5368
		private readonly int _length;

		// Token: 0x040014F9 RID: 5369
		private NameValuePair _next;
	}
}
