﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace System.Data.Common
{
	// Token: 0x020002B1 RID: 689
	internal class DbConnectionStringBuilderDescriptor : PropertyDescriptor
	{
		// Token: 0x06002281 RID: 8833 RVA: 0x0009DE5C File Offset: 0x0009C05C
		internal DbConnectionStringBuilderDescriptor(string propertyName, Type componentType, Type propertyType, bool isReadOnly, Attribute[] attributes) : base(propertyName, attributes)
		{
			this.ComponentType = componentType;
			this.PropertyType = propertyType;
			this.IsReadOnly = isReadOnly;
		}

		// Token: 0x170005ED RID: 1517
		// (get) Token: 0x06002282 RID: 8834 RVA: 0x0009DE7D File Offset: 0x0009C07D
		// (set) Token: 0x06002283 RID: 8835 RVA: 0x0009DE85 File Offset: 0x0009C085
		internal bool RefreshOnChange
		{
			[CompilerGenerated]
			get
			{
				return this.<RefreshOnChange>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<RefreshOnChange>k__BackingField = value;
			}
		}

		// Token: 0x170005EE RID: 1518
		// (get) Token: 0x06002284 RID: 8836 RVA: 0x0009DE8E File Offset: 0x0009C08E
		public override Type ComponentType
		{
			[CompilerGenerated]
			get
			{
				return this.<ComponentType>k__BackingField;
			}
		}

		// Token: 0x170005EF RID: 1519
		// (get) Token: 0x06002285 RID: 8837 RVA: 0x0009DE96 File Offset: 0x0009C096
		public override bool IsReadOnly
		{
			[CompilerGenerated]
			get
			{
				return this.<IsReadOnly>k__BackingField;
			}
		}

		// Token: 0x170005F0 RID: 1520
		// (get) Token: 0x06002286 RID: 8838 RVA: 0x0009DE9E File Offset: 0x0009C09E
		public override Type PropertyType
		{
			[CompilerGenerated]
			get
			{
				return this.<PropertyType>k__BackingField;
			}
		}

		// Token: 0x06002287 RID: 8839 RVA: 0x0009DEA8 File Offset: 0x0009C0A8
		public override bool CanResetValue(object component)
		{
			DbConnectionStringBuilder dbConnectionStringBuilder = component as DbConnectionStringBuilder;
			return dbConnectionStringBuilder != null && dbConnectionStringBuilder.ShouldSerialize(this.DisplayName);
		}

		// Token: 0x06002288 RID: 8840 RVA: 0x0009DED0 File Offset: 0x0009C0D0
		public override object GetValue(object component)
		{
			DbConnectionStringBuilder dbConnectionStringBuilder = component as DbConnectionStringBuilder;
			object result;
			if (dbConnectionStringBuilder != null && dbConnectionStringBuilder.TryGetValue(this.DisplayName, out result))
			{
				return result;
			}
			return null;
		}

		// Token: 0x06002289 RID: 8841 RVA: 0x0009DEFC File Offset: 0x0009C0FC
		public override void ResetValue(object component)
		{
			DbConnectionStringBuilder dbConnectionStringBuilder = component as DbConnectionStringBuilder;
			if (dbConnectionStringBuilder != null)
			{
				dbConnectionStringBuilder.Remove(this.DisplayName);
				if (this.RefreshOnChange)
				{
					dbConnectionStringBuilder.ClearPropertyDescriptors();
				}
			}
		}

		// Token: 0x0600228A RID: 8842 RVA: 0x0009DF30 File Offset: 0x0009C130
		public override void SetValue(object component, object value)
		{
			DbConnectionStringBuilder dbConnectionStringBuilder = component as DbConnectionStringBuilder;
			if (dbConnectionStringBuilder != null)
			{
				if (typeof(string) == this.PropertyType && string.Empty.Equals(value))
				{
					value = null;
				}
				dbConnectionStringBuilder[this.DisplayName] = value;
				if (this.RefreshOnChange)
				{
					dbConnectionStringBuilder.ClearPropertyDescriptors();
				}
			}
		}

		// Token: 0x0600228B RID: 8843 RVA: 0x0009DF8C File Offset: 0x0009C18C
		public override bool ShouldSerializeValue(object component)
		{
			DbConnectionStringBuilder dbConnectionStringBuilder = component as DbConnectionStringBuilder;
			return dbConnectionStringBuilder != null && dbConnectionStringBuilder.ShouldSerialize(this.DisplayName);
		}

		// Token: 0x040015D6 RID: 5590
		[CompilerGenerated]
		private bool <RefreshOnChange>k__BackingField;

		// Token: 0x040015D7 RID: 5591
		[CompilerGenerated]
		private readonly Type <ComponentType>k__BackingField;

		// Token: 0x040015D8 RID: 5592
		[CompilerGenerated]
		private readonly bool <IsReadOnly>k__BackingField;

		// Token: 0x040015D9 RID: 5593
		[CompilerGenerated]
		private readonly Type <PropertyType>k__BackingField;
	}
}
