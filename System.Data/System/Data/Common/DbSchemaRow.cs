﻿using System;
using System.Globalization;

namespace System.Data.Common
{
	// Token: 0x0200029B RID: 667
	internal sealed class DbSchemaRow
	{
		// Token: 0x06002067 RID: 8295 RVA: 0x00098D68 File Offset: 0x00096F68
		internal static DbSchemaRow[] GetSortedSchemaRows(DataTable dataTable, bool returnProviderSpecificTypes)
		{
			DataColumn dataColumn = dataTable.Columns["SchemaMapping Unsorted Index"];
			if (dataColumn == null)
			{
				dataColumn = new DataColumn("SchemaMapping Unsorted Index", typeof(int));
				dataTable.Columns.Add(dataColumn);
			}
			int count = dataTable.Rows.Count;
			for (int i = 0; i < count; i++)
			{
				dataTable.Rows[i][dataColumn] = i;
			}
			DbSchemaTable schemaTable = new DbSchemaTable(dataTable, returnProviderSpecificTypes);
			DataRow[] array = dataTable.Select(null, "ColumnOrdinal ASC", DataViewRowState.CurrentRows);
			DbSchemaRow[] array2 = new DbSchemaRow[array.Length];
			for (int j = 0; j < array.Length; j++)
			{
				array2[j] = new DbSchemaRow(schemaTable, array[j]);
			}
			return array2;
		}

		// Token: 0x06002068 RID: 8296 RVA: 0x00098E24 File Offset: 0x00097024
		internal DbSchemaRow(DbSchemaTable schemaTable, DataRow dataRow)
		{
			this._schemaTable = schemaTable;
			this._dataRow = dataRow;
		}

		// Token: 0x17000560 RID: 1376
		// (get) Token: 0x06002069 RID: 8297 RVA: 0x00098E3A File Offset: 0x0009703A
		internal DataRow DataRow
		{
			get
			{
				return this._dataRow;
			}
		}

		// Token: 0x17000561 RID: 1377
		// (get) Token: 0x0600206A RID: 8298 RVA: 0x00098E44 File Offset: 0x00097044
		internal string ColumnName
		{
			get
			{
				object value = this._dataRow[this._schemaTable.ColumnName, DataRowVersion.Default];
				if (!Convert.IsDBNull(value))
				{
					return Convert.ToString(value, CultureInfo.InvariantCulture);
				}
				return string.Empty;
			}
		}

		// Token: 0x17000562 RID: 1378
		// (get) Token: 0x0600206B RID: 8299 RVA: 0x00098E88 File Offset: 0x00097088
		internal int Size
		{
			get
			{
				object value = this._dataRow[this._schemaTable.Size, DataRowVersion.Default];
				if (!Convert.IsDBNull(value))
				{
					return Convert.ToInt32(value, CultureInfo.InvariantCulture);
				}
				return 0;
			}
		}

		// Token: 0x17000563 RID: 1379
		// (get) Token: 0x0600206C RID: 8300 RVA: 0x00098EC8 File Offset: 0x000970C8
		internal string BaseColumnName
		{
			get
			{
				if (this._schemaTable.BaseColumnName != null)
				{
					object value = this._dataRow[this._schemaTable.BaseColumnName, DataRowVersion.Default];
					if (!Convert.IsDBNull(value))
					{
						return Convert.ToString(value, CultureInfo.InvariantCulture);
					}
				}
				return string.Empty;
			}
		}

		// Token: 0x17000564 RID: 1380
		// (get) Token: 0x0600206D RID: 8301 RVA: 0x00098F18 File Offset: 0x00097118
		internal string BaseServerName
		{
			get
			{
				if (this._schemaTable.BaseServerName != null)
				{
					object value = this._dataRow[this._schemaTable.BaseServerName, DataRowVersion.Default];
					if (!Convert.IsDBNull(value))
					{
						return Convert.ToString(value, CultureInfo.InvariantCulture);
					}
				}
				return string.Empty;
			}
		}

		// Token: 0x17000565 RID: 1381
		// (get) Token: 0x0600206E RID: 8302 RVA: 0x00098F68 File Offset: 0x00097168
		internal string BaseCatalogName
		{
			get
			{
				if (this._schemaTable.BaseCatalogName != null)
				{
					object value = this._dataRow[this._schemaTable.BaseCatalogName, DataRowVersion.Default];
					if (!Convert.IsDBNull(value))
					{
						return Convert.ToString(value, CultureInfo.InvariantCulture);
					}
				}
				return string.Empty;
			}
		}

		// Token: 0x17000566 RID: 1382
		// (get) Token: 0x0600206F RID: 8303 RVA: 0x00098FB8 File Offset: 0x000971B8
		internal string BaseSchemaName
		{
			get
			{
				if (this._schemaTable.BaseSchemaName != null)
				{
					object value = this._dataRow[this._schemaTable.BaseSchemaName, DataRowVersion.Default];
					if (!Convert.IsDBNull(value))
					{
						return Convert.ToString(value, CultureInfo.InvariantCulture);
					}
				}
				return string.Empty;
			}
		}

		// Token: 0x17000567 RID: 1383
		// (get) Token: 0x06002070 RID: 8304 RVA: 0x00099008 File Offset: 0x00097208
		internal string BaseTableName
		{
			get
			{
				if (this._schemaTable.BaseTableName != null)
				{
					object value = this._dataRow[this._schemaTable.BaseTableName, DataRowVersion.Default];
					if (!Convert.IsDBNull(value))
					{
						return Convert.ToString(value, CultureInfo.InvariantCulture);
					}
				}
				return string.Empty;
			}
		}

		// Token: 0x17000568 RID: 1384
		// (get) Token: 0x06002071 RID: 8305 RVA: 0x00099058 File Offset: 0x00097258
		internal bool IsAutoIncrement
		{
			get
			{
				if (this._schemaTable.IsAutoIncrement != null)
				{
					object value = this._dataRow[this._schemaTable.IsAutoIncrement, DataRowVersion.Default];
					if (!Convert.IsDBNull(value))
					{
						return Convert.ToBoolean(value, CultureInfo.InvariantCulture);
					}
				}
				return false;
			}
		}

		// Token: 0x17000569 RID: 1385
		// (get) Token: 0x06002072 RID: 8306 RVA: 0x000990A4 File Offset: 0x000972A4
		internal bool IsUnique
		{
			get
			{
				if (this._schemaTable.IsUnique != null)
				{
					object value = this._dataRow[this._schemaTable.IsUnique, DataRowVersion.Default];
					if (!Convert.IsDBNull(value))
					{
						return Convert.ToBoolean(value, CultureInfo.InvariantCulture);
					}
				}
				return false;
			}
		}

		// Token: 0x1700056A RID: 1386
		// (get) Token: 0x06002073 RID: 8307 RVA: 0x000990F0 File Offset: 0x000972F0
		internal bool IsRowVersion
		{
			get
			{
				if (this._schemaTable.IsRowVersion != null)
				{
					object value = this._dataRow[this._schemaTable.IsRowVersion, DataRowVersion.Default];
					if (!Convert.IsDBNull(value))
					{
						return Convert.ToBoolean(value, CultureInfo.InvariantCulture);
					}
				}
				return false;
			}
		}

		// Token: 0x1700056B RID: 1387
		// (get) Token: 0x06002074 RID: 8308 RVA: 0x0009913C File Offset: 0x0009733C
		internal bool IsKey
		{
			get
			{
				if (this._schemaTable.IsKey != null)
				{
					object value = this._dataRow[this._schemaTable.IsKey, DataRowVersion.Default];
					if (!Convert.IsDBNull(value))
					{
						return Convert.ToBoolean(value, CultureInfo.InvariantCulture);
					}
				}
				return false;
			}
		}

		// Token: 0x1700056C RID: 1388
		// (get) Token: 0x06002075 RID: 8309 RVA: 0x00099188 File Offset: 0x00097388
		internal bool IsExpression
		{
			get
			{
				if (this._schemaTable.IsExpression != null)
				{
					object value = this._dataRow[this._schemaTable.IsExpression, DataRowVersion.Default];
					if (!Convert.IsDBNull(value))
					{
						return Convert.ToBoolean(value, CultureInfo.InvariantCulture);
					}
				}
				return false;
			}
		}

		// Token: 0x1700056D RID: 1389
		// (get) Token: 0x06002076 RID: 8310 RVA: 0x000991D4 File Offset: 0x000973D4
		internal bool IsHidden
		{
			get
			{
				if (this._schemaTable.IsHidden != null)
				{
					object value = this._dataRow[this._schemaTable.IsHidden, DataRowVersion.Default];
					if (!Convert.IsDBNull(value))
					{
						return Convert.ToBoolean(value, CultureInfo.InvariantCulture);
					}
				}
				return false;
			}
		}

		// Token: 0x1700056E RID: 1390
		// (get) Token: 0x06002077 RID: 8311 RVA: 0x00099220 File Offset: 0x00097420
		internal bool IsLong
		{
			get
			{
				if (this._schemaTable.IsLong != null)
				{
					object value = this._dataRow[this._schemaTable.IsLong, DataRowVersion.Default];
					if (!Convert.IsDBNull(value))
					{
						return Convert.ToBoolean(value, CultureInfo.InvariantCulture);
					}
				}
				return false;
			}
		}

		// Token: 0x1700056F RID: 1391
		// (get) Token: 0x06002078 RID: 8312 RVA: 0x0009926C File Offset: 0x0009746C
		internal bool IsReadOnly
		{
			get
			{
				if (this._schemaTable.IsReadOnly != null)
				{
					object value = this._dataRow[this._schemaTable.IsReadOnly, DataRowVersion.Default];
					if (!Convert.IsDBNull(value))
					{
						return Convert.ToBoolean(value, CultureInfo.InvariantCulture);
					}
				}
				return false;
			}
		}

		// Token: 0x17000570 RID: 1392
		// (get) Token: 0x06002079 RID: 8313 RVA: 0x000992B8 File Offset: 0x000974B8
		internal Type DataType
		{
			get
			{
				if (this._schemaTable.DataType != null)
				{
					object obj = this._dataRow[this._schemaTable.DataType, DataRowVersion.Default];
					if (!Convert.IsDBNull(obj))
					{
						return (Type)obj;
					}
				}
				return null;
			}
		}

		// Token: 0x17000571 RID: 1393
		// (get) Token: 0x0600207A RID: 8314 RVA: 0x00099300 File Offset: 0x00097500
		internal bool AllowDBNull
		{
			get
			{
				if (this._schemaTable.AllowDBNull != null)
				{
					object value = this._dataRow[this._schemaTable.AllowDBNull, DataRowVersion.Default];
					if (!Convert.IsDBNull(value))
					{
						return Convert.ToBoolean(value, CultureInfo.InvariantCulture);
					}
				}
				return true;
			}
		}

		// Token: 0x17000572 RID: 1394
		// (get) Token: 0x0600207B RID: 8315 RVA: 0x0009934B File Offset: 0x0009754B
		internal int UnsortedIndex
		{
			get
			{
				return (int)this._dataRow[this._schemaTable.UnsortedIndex, DataRowVersion.Default];
			}
		}

		// Token: 0x0400153B RID: 5435
		internal const string SchemaMappingUnsortedIndex = "SchemaMapping Unsorted Index";

		// Token: 0x0400153C RID: 5436
		private DbSchemaTable _schemaTable;

		// Token: 0x0400153D RID: 5437
		private DataRow _dataRow;
	}
}
