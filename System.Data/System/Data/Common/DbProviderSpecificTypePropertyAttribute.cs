﻿using System;
using System.Runtime.CompilerServices;

namespace System.Data.Common
{
	/// <summary>Identifies which provider-specific property in the strongly typed parameter classes is to be used when setting a provider-specific type.</summary>
	// Token: 0x020002C0 RID: 704
	[AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = true)]
	[Serializable]
	public sealed class DbProviderSpecificTypePropertyAttribute : Attribute
	{
		/// <summary>Initializes a new instance of a <see cref="T:System.Data.Common.DbProviderSpecificTypePropertyAttribute" /> class.</summary>
		/// <param name="isProviderSpecificTypeProperty">Specifies whether this property is a provider-specific property.</param>
		// Token: 0x0600239B RID: 9115 RVA: 0x000A0480 File Offset: 0x0009E680
		public DbProviderSpecificTypePropertyAttribute(bool isProviderSpecificTypeProperty)
		{
			this.IsProviderSpecificTypeProperty = isProviderSpecificTypeProperty;
		}

		/// <summary>Indicates whether the attributed property is a provider-specific type.</summary>
		/// <returns>
		///     <see langword="true" /> if the property that this attribute is applied to is a provider-specific type property; otherwise <see langword="false" />.</returns>
		// Token: 0x17000624 RID: 1572
		// (get) Token: 0x0600239C RID: 9116 RVA: 0x000A048F File Offset: 0x0009E68F
		public bool IsProviderSpecificTypeProperty
		{
			[CompilerGenerated]
			get
			{
				return this.<IsProviderSpecificTypeProperty>k__BackingField;
			}
		}

		// Token: 0x04001621 RID: 5665
		[CompilerGenerated]
		private readonly bool <IsProviderSpecificTypeProperty>k__BackingField;
	}
}
