﻿using System;
using Unity;

namespace System.Data.Common
{
	/// <summary>Represents a set of static methods for creating one or more instances of <see cref="T:System.Data.Common.DbProviderFactory" /> classes.</summary>
	// Token: 0x0200038A RID: 906
	public static class DbProviderFactories
	{
		/// <summary>Returns an instance of a <see cref="T:System.Data.Common.DbProviderFactory" />.</summary>
		/// <param name="connection">The connection used.</param>
		/// <returns>An instance of a <see cref="T:System.Data.Common.DbProviderFactory" /> for a specified connection.</returns>
		// Token: 0x06002B49 RID: 11081 RVA: 0x00051759 File Offset: 0x0004F959
		public static DbProviderFactory GetFactory(DbConnection connection)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Returns an instance of a <see cref="T:System.Data.Common.DbProviderFactory" />.</summary>
		/// <param name="providerRow">
		///       <see cref="T:System.Data.DataRow" /> containing the provider's configuration information.</param>
		/// <returns>An instance of a <see cref="T:System.Data.Common.DbProviderFactory" /> for a specified <see cref="T:System.Data.DataRow" />.</returns>
		// Token: 0x06002B4A RID: 11082 RVA: 0x00051759 File Offset: 0x0004F959
		public static DbProviderFactory GetFactory(DataRow providerRow)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Returns an instance of a <see cref="T:System.Data.Common.DbProviderFactory" />.</summary>
		/// <param name="providerInvariantName">Invariant name of a provider.</param>
		/// <returns>An instance of a <see cref="T:System.Data.Common.DbProviderFactory" /> for a specified provider name.</returns>
		// Token: 0x06002B4B RID: 11083 RVA: 0x00051759 File Offset: 0x0004F959
		public static DbProviderFactory GetFactory(string providerInvariantName)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Returns a <see cref="T:System.Data.DataTable" /> that contains information about all installed providers that implement <see cref="T:System.Data.Common.DbProviderFactory" />.</summary>
		/// <returns>Returns a <see cref="T:System.Data.DataTable" /> containing <see cref="T:System.Data.DataRow" /> objects that contain the following data. Column ordinalColumn nameDescription0
		///             Name
		///           Human-readable name for the data provider.1
		///             Description
		///           Human-readable description of the data provider.2
		///             InvariantName
		///           Name that can be used programmatically to refer to the data provider.3
		///             AssemblyQualifiedName
		///           Fully qualified name of the factory class, which contains enough information to instantiate the object.</returns>
		// Token: 0x06002B4C RID: 11084 RVA: 0x00051759 File Offset: 0x0004F959
		public static DataTable GetFactoryClasses()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}
	}
}
