﻿using System;

namespace System.Data.Common
{
	/// <summary>Describes the column metadata of the schema for a database table.</summary>
	// Token: 0x020002DF RID: 735
	public static class SchemaTableColumn
	{
		// Token: 0x06002527 RID: 9511 RVA: 0x000A908C File Offset: 0x000A728C
		// Note: this type is marked as 'beforefieldinit'.
		static SchemaTableColumn()
		{
		}

		/// <summary>Specifies the name of the column in the schema table.</summary>
		// Token: 0x0400165E RID: 5726
		public static readonly string ColumnName = "ColumnName";

		/// <summary>Specifies the ordinal of the column.</summary>
		// Token: 0x0400165F RID: 5727
		public static readonly string ColumnOrdinal = "ColumnOrdinal";

		/// <summary>Specifies the size of the column.</summary>
		// Token: 0x04001660 RID: 5728
		public static readonly string ColumnSize = "ColumnSize";

		/// <summary>Specifies the precision of the column data, if the data is numeric.</summary>
		// Token: 0x04001661 RID: 5729
		public static readonly string NumericPrecision = "NumericPrecision";

		/// <summary>Specifies the scale of the column data, if the data is numeric.</summary>
		// Token: 0x04001662 RID: 5730
		public static readonly string NumericScale = "NumericScale";

		/// <summary>Specifies the type of data in the column.</summary>
		// Token: 0x04001663 RID: 5731
		public static readonly string DataType = "DataType";

		/// <summary>Specifies the provider-specific data type of the column.</summary>
		// Token: 0x04001664 RID: 5732
		public static readonly string ProviderType = "ProviderType";

		/// <summary>Specifies the non-versioned provider-specific data type of the column.</summary>
		// Token: 0x04001665 RID: 5733
		public static readonly string NonVersionedProviderType = "NonVersionedProviderType";

		/// <summary>Specifies whether this column contains long data.</summary>
		// Token: 0x04001666 RID: 5734
		public static readonly string IsLong = "IsLong";

		/// <summary>Specifies whether value <see langword="DBNull" /> is allowed.</summary>
		// Token: 0x04001667 RID: 5735
		public static readonly string AllowDBNull = "AllowDBNull";

		/// <summary>Specifies whether this column is aliased.</summary>
		// Token: 0x04001668 RID: 5736
		public static readonly string IsAliased = "IsAliased";

		/// <summary>Specifies whether this column is an expression.</summary>
		// Token: 0x04001669 RID: 5737
		public static readonly string IsExpression = "IsExpression";

		/// <summary>Specifies whether this column is a key for the table. </summary>
		// Token: 0x0400166A RID: 5738
		public static readonly string IsKey = "IsKey";

		/// <summary>Specifies whether a unique constraint applies to this column.</summary>
		// Token: 0x0400166B RID: 5739
		public static readonly string IsUnique = "IsUnique";

		/// <summary>Specifies the name of the schema in the schema table.</summary>
		// Token: 0x0400166C RID: 5740
		public static readonly string BaseSchemaName = "BaseSchemaName";

		/// <summary>Specifies the name of the table in the schema table.</summary>
		// Token: 0x0400166D RID: 5741
		public static readonly string BaseTableName = "BaseTableName";

		/// <summary>Specifies the name of the column in the schema table.</summary>
		// Token: 0x0400166E RID: 5742
		public static readonly string BaseColumnName = "BaseColumnName";
	}
}
