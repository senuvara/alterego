﻿using System;
using System.Collections;
using System.Data.SqlTypes;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace System.Data.Common
{
	// Token: 0x020002D7 RID: 727
	internal sealed class SqlGuidStorage : DataStorage
	{
		// Token: 0x060024AD RID: 9389 RVA: 0x000A6738 File Offset: 0x000A4938
		public SqlGuidStorage(DataColumn column) : base(column, typeof(SqlGuid), SqlGuid.Null, SqlGuid.Null, StorageType.SqlGuid)
		{
		}

		// Token: 0x060024AE RID: 9390 RVA: 0x000A6764 File Offset: 0x000A4964
		public override object Aggregate(int[] records, AggregateType kind)
		{
			try
			{
				if (kind != AggregateType.First)
				{
					if (kind == AggregateType.Count)
					{
						int num = 0;
						for (int i = 0; i < records.Length; i++)
						{
							if (!this.IsNull(records[i]))
							{
								num++;
							}
						}
						return num;
					}
				}
				else
				{
					if (records.Length != 0)
					{
						return this._values[records[0]];
					}
					return null;
				}
			}
			catch (OverflowException)
			{
				throw ExprException.Overflow(typeof(SqlGuid));
			}
			throw ExceptionBuilder.AggregateException(kind, this._dataType);
		}

		// Token: 0x060024AF RID: 9391 RVA: 0x000A67F4 File Offset: 0x000A49F4
		public override int Compare(int recordNo1, int recordNo2)
		{
			return this._values[recordNo1].CompareTo(this._values[recordNo2]);
		}

		// Token: 0x060024B0 RID: 9392 RVA: 0x000A6813 File Offset: 0x000A4A13
		public override int CompareValueTo(int recordNo, object value)
		{
			return this._values[recordNo].CompareTo((SqlGuid)value);
		}

		// Token: 0x060024B1 RID: 9393 RVA: 0x000A682C File Offset: 0x000A4A2C
		public override object ConvertValue(object value)
		{
			if (value != null)
			{
				return SqlConvert.ConvertToSqlGuid(value);
			}
			return this._nullValue;
		}

		// Token: 0x060024B2 RID: 9394 RVA: 0x000A6843 File Offset: 0x000A4A43
		public override void Copy(int recordNo1, int recordNo2)
		{
			this._values[recordNo2] = this._values[recordNo1];
		}

		// Token: 0x060024B3 RID: 9395 RVA: 0x000A685D File Offset: 0x000A4A5D
		public override object Get(int record)
		{
			return this._values[record];
		}

		// Token: 0x060024B4 RID: 9396 RVA: 0x000A6870 File Offset: 0x000A4A70
		public override bool IsNull(int record)
		{
			return this._values[record].IsNull;
		}

		// Token: 0x060024B5 RID: 9397 RVA: 0x000A6883 File Offset: 0x000A4A83
		public override void Set(int record, object value)
		{
			this._values[record] = SqlConvert.ConvertToSqlGuid(value);
		}

		// Token: 0x060024B6 RID: 9398 RVA: 0x000A6898 File Offset: 0x000A4A98
		public override void SetCapacity(int capacity)
		{
			SqlGuid[] array = new SqlGuid[capacity];
			if (this._values != null)
			{
				Array.Copy(this._values, 0, array, 0, Math.Min(capacity, this._values.Length));
			}
			this._values = array;
		}

		// Token: 0x060024B7 RID: 9399 RVA: 0x000A68D8 File Offset: 0x000A4AD8
		public override object ConvertXmlToObject(string s)
		{
			SqlGuid sqlGuid = default(SqlGuid);
			TextReader input = new StringReader("<col>" + s + "</col>");
			IXmlSerializable xmlSerializable = sqlGuid;
			using (XmlTextReader xmlTextReader = new XmlTextReader(input))
			{
				xmlSerializable.ReadXml(xmlTextReader);
			}
			return (SqlGuid)xmlSerializable;
		}

		// Token: 0x060024B8 RID: 9400 RVA: 0x000A6940 File Offset: 0x000A4B40
		public override string ConvertObjectToXml(object value)
		{
			StringWriter stringWriter = new StringWriter(base.FormatProvider);
			using (XmlTextWriter xmlTextWriter = new XmlTextWriter(stringWriter))
			{
				((IXmlSerializable)value).WriteXml(xmlTextWriter);
			}
			return stringWriter.ToString();
		}

		// Token: 0x060024B9 RID: 9401 RVA: 0x000A6990 File Offset: 0x000A4B90
		protected override object GetEmptyStorage(int recordCount)
		{
			return new SqlGuid[recordCount];
		}

		// Token: 0x060024BA RID: 9402 RVA: 0x000A6998 File Offset: 0x000A4B98
		protected override void CopyValue(int record, object store, BitArray nullbits, int storeIndex)
		{
			((SqlGuid[])store)[storeIndex] = this._values[record];
			nullbits.Set(storeIndex, this.IsNull(record));
		}

		// Token: 0x060024BB RID: 9403 RVA: 0x000A69C2 File Offset: 0x000A4BC2
		protected override void SetStorage(object store, BitArray nullbits)
		{
			this._values = (SqlGuid[])store;
		}

		// Token: 0x04001656 RID: 5718
		private SqlGuid[] _values;
	}
}
