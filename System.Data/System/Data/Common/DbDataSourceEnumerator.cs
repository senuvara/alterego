﻿using System;

namespace System.Data.Common
{
	/// <summary>Provides a mechanism for enumerating all available instances of database servers within the local network. </summary>
	// Token: 0x020002B7 RID: 695
	public abstract class DbDataSourceEnumerator
	{
		/// <summary>Creates a new instance of the <see cref="T:System.Data.Common.DbDataSourceEnumerator" /> class. </summary>
		// Token: 0x0600233C RID: 9020 RVA: 0x00005C14 File Offset: 0x00003E14
		protected DbDataSourceEnumerator()
		{
		}

		/// <summary>Retrieves a <see cref="T:System.Data.DataTable" /> containing information about all visible instances of the server represented by the strongly typed instance of this class.</summary>
		/// <returns>Returns a <see cref="T:System.Data.DataTable" /> containing information about the visible instances of the associated data source.</returns>
		// Token: 0x0600233D RID: 9021
		public abstract DataTable GetDataSources();
	}
}
