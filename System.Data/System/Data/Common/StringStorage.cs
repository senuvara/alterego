﻿using System;
using System.Collections;

namespace System.Data.Common
{
	// Token: 0x020002E3 RID: 739
	internal sealed class StringStorage : DataStorage
	{
		// Token: 0x0600254A RID: 9546 RVA: 0x000A9B59 File Offset: 0x000A7D59
		public StringStorage(DataColumn column) : base(column, typeof(string), string.Empty, StorageType.String)
		{
		}

		// Token: 0x0600254B RID: 9547 RVA: 0x000A9B74 File Offset: 0x000A7D74
		public override object Aggregate(int[] recordNos, AggregateType kind)
		{
			switch (kind)
			{
			case AggregateType.Min:
			{
				int num = -1;
				int i;
				for (i = 0; i < recordNos.Length; i++)
				{
					if (!this.IsNull(recordNos[i]))
					{
						num = recordNos[i];
						break;
					}
				}
				if (num >= 0)
				{
					for (i++; i < recordNos.Length; i++)
					{
						if (!this.IsNull(recordNos[i]) && this.Compare(num, recordNos[i]) > 0)
						{
							num = recordNos[i];
						}
					}
					return this.Get(num);
				}
				return this._nullValue;
			}
			case AggregateType.Max:
			{
				int num2 = -1;
				int i;
				for (i = 0; i < recordNos.Length; i++)
				{
					if (!this.IsNull(recordNos[i]))
					{
						num2 = recordNos[i];
						break;
					}
				}
				if (num2 >= 0)
				{
					for (i++; i < recordNos.Length; i++)
					{
						if (this.Compare(num2, recordNos[i]) < 0)
						{
							num2 = recordNos[i];
						}
					}
					return this.Get(num2);
				}
				return this._nullValue;
			}
			case AggregateType.Count:
			{
				int num3 = 0;
				for (int i = 0; i < recordNos.Length; i++)
				{
					if (this._values[recordNos[i]] != null)
					{
						num3++;
					}
				}
				return num3;
			}
			}
			throw ExceptionBuilder.AggregateException(kind, this._dataType);
		}

		// Token: 0x0600254C RID: 9548 RVA: 0x000A9C88 File Offset: 0x000A7E88
		public override int Compare(int recordNo1, int recordNo2)
		{
			string text = this._values[recordNo1];
			string text2 = this._values[recordNo2];
			if (text == text2)
			{
				return 0;
			}
			if (text == null)
			{
				return -1;
			}
			if (text2 == null)
			{
				return 1;
			}
			return this._table.Compare(text, text2);
		}

		// Token: 0x0600254D RID: 9549 RVA: 0x000A9CC4 File Offset: 0x000A7EC4
		public override int CompareValueTo(int recordNo, object value)
		{
			string text = this._values[recordNo];
			if (text == null)
			{
				if (this._nullValue == value)
				{
					return 0;
				}
				return -1;
			}
			else
			{
				if (this._nullValue == value)
				{
					return 1;
				}
				return this._table.Compare(text, (string)value);
			}
		}

		// Token: 0x0600254E RID: 9550 RVA: 0x000A9D07 File Offset: 0x000A7F07
		public override object ConvertValue(object value)
		{
			if (this._nullValue != value)
			{
				if (value != null)
				{
					value = value.ToString();
				}
				else
				{
					value = this._nullValue;
				}
			}
			return value;
		}

		// Token: 0x0600254F RID: 9551 RVA: 0x000A9D28 File Offset: 0x000A7F28
		public override void Copy(int recordNo1, int recordNo2)
		{
			this._values[recordNo2] = this._values[recordNo1];
		}

		// Token: 0x06002550 RID: 9552 RVA: 0x000A9D3C File Offset: 0x000A7F3C
		public override object Get(int recordNo)
		{
			string text = this._values[recordNo];
			if (text != null)
			{
				return text;
			}
			return this._nullValue;
		}

		// Token: 0x06002551 RID: 9553 RVA: 0x000A9D60 File Offset: 0x000A7F60
		public override int GetStringLength(int record)
		{
			string text = this._values[record];
			if (text == null)
			{
				return 0;
			}
			return text.Length;
		}

		// Token: 0x06002552 RID: 9554 RVA: 0x000A9D81 File Offset: 0x000A7F81
		public override bool IsNull(int record)
		{
			return this._values[record] == null;
		}

		// Token: 0x06002553 RID: 9555 RVA: 0x000A9D8E File Offset: 0x000A7F8E
		public override void Set(int record, object value)
		{
			if (this._nullValue == value)
			{
				this._values[record] = null;
				return;
			}
			this._values[record] = value.ToString();
		}

		// Token: 0x06002554 RID: 9556 RVA: 0x000A9DB4 File Offset: 0x000A7FB4
		public override void SetCapacity(int capacity)
		{
			string[] array = new string[capacity];
			if (this._values != null)
			{
				Array.Copy(this._values, 0, array, 0, Math.Min(capacity, this._values.Length));
			}
			this._values = array;
		}

		// Token: 0x06002555 RID: 9557 RVA: 0x00005DA6 File Offset: 0x00003FA6
		public override object ConvertXmlToObject(string s)
		{
			return s;
		}

		// Token: 0x06002556 RID: 9558 RVA: 0x000A9DF3 File Offset: 0x000A7FF3
		public override string ConvertObjectToXml(object value)
		{
			return (string)value;
		}

		// Token: 0x06002557 RID: 9559 RVA: 0x000A9DFB File Offset: 0x000A7FFB
		protected override object GetEmptyStorage(int recordCount)
		{
			return new string[recordCount];
		}

		// Token: 0x06002558 RID: 9560 RVA: 0x000A9E03 File Offset: 0x000A8003
		protected override void CopyValue(int record, object store, BitArray nullbits, int storeIndex)
		{
			((string[])store)[storeIndex] = this._values[record];
			nullbits.Set(storeIndex, this.IsNull(record));
		}

		// Token: 0x06002559 RID: 9561 RVA: 0x000A9E25 File Offset: 0x000A8025
		protected override void SetStorage(object store, BitArray nullbits)
		{
			this._values = (string[])store;
		}

		// Token: 0x04001683 RID: 5763
		private string[] _values;
	}
}
