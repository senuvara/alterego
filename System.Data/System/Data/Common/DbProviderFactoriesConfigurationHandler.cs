﻿using System;
using System.Configuration;
using System.Xml;
using Unity;

namespace System.Data.Common
{
	/// <summary>This type supports the .NET Framework infrastructure and is not intended to be used directly from your code.</summary>
	// Token: 0x0200038B RID: 907
	public class DbProviderFactoriesConfigurationHandler : IConfigurationSectionHandler
	{
		/// <summary>This type supports the .NET Framework infrastructure and is not intended to be used directly from your code.</summary>
		// Token: 0x06002B4D RID: 11085 RVA: 0x00010458 File Offset: 0x0000E658
		public DbProviderFactoriesConfigurationHandler()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>This type supports the .NET Framework infrastructure and is not intended to be used directly from your code.</summary>
		/// <param name="parent">This type supports the .NET Framework infrastructure and is not intended to be used directly from your code.</param>
		/// <param name="configContext">This type supports the .NET Framework infrastructure and is not intended to be used directly from your code.</param>
		/// <param name="section">This type supports the .NET Framework infrastructure and is not intended to be used directly from your code.</param>
		/// <returns>This type supports the .NET Framework infrastructure and is not intended to be used directly from your code.</returns>
		// Token: 0x06002B4E RID: 11086 RVA: 0x00051759 File Offset: 0x0004F959
		public virtual object Create(object parent, object configContext, XmlNode section)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}
	}
}
