﻿using System;

namespace System.Data.Common
{
	/// <summary>Describes optional column metadata of the schema for a database table.</summary>
	// Token: 0x020002E0 RID: 736
	public static class SchemaTableOptionalColumn
	{
		// Token: 0x06002528 RID: 9512 RVA: 0x000A9144 File Offset: 0x000A7344
		// Note: this type is marked as 'beforefieldinit'.
		static SchemaTableOptionalColumn()
		{
		}

		/// <summary>Specifies the provider-specific data type of the column.</summary>
		// Token: 0x0400166F RID: 5743
		public static readonly string ProviderSpecificDataType = "ProviderSpecificDataType";

		/// <summary>Specifies whether the column values in the column are automatically incremented.</summary>
		// Token: 0x04001670 RID: 5744
		public static readonly string IsAutoIncrement = "IsAutoIncrement";

		/// <summary>Specifies whether this column is hidden.</summary>
		// Token: 0x04001671 RID: 5745
		public static readonly string IsHidden = "IsHidden";

		/// <summary>Specifies whether this column is read-only.</summary>
		// Token: 0x04001672 RID: 5746
		public static readonly string IsReadOnly = "IsReadOnly";

		/// <summary>Specifies whether this column contains row version information.</summary>
		// Token: 0x04001673 RID: 5747
		public static readonly string IsRowVersion = "IsRowVersion";

		/// <summary>The server name of the column.</summary>
		// Token: 0x04001674 RID: 5748
		public static readonly string BaseServerName = "BaseServerName";

		/// <summary>The name of the catalog associated with the results of the latest query.</summary>
		// Token: 0x04001675 RID: 5749
		public static readonly string BaseCatalogName = "BaseCatalogName";

		/// <summary>Specifies the value at which the series for new identity columns is assigned.</summary>
		// Token: 0x04001676 RID: 5750
		public static readonly string AutoIncrementSeed = "AutoIncrementSeed";

		/// <summary>Specifies the increment between values in the identity column.</summary>
		// Token: 0x04001677 RID: 5751
		public static readonly string AutoIncrementStep = "AutoIncrementStep";

		/// <summary>The default value for the column.</summary>
		// Token: 0x04001678 RID: 5752
		public static readonly string DefaultValue = "DefaultValue";

		/// <summary>The expression used to compute the column.</summary>
		// Token: 0x04001679 RID: 5753
		public static readonly string Expression = "Expression";

		/// <summary>The namespace for the table that contains the column.</summary>
		// Token: 0x0400167A RID: 5754
		public static readonly string BaseTableNamespace = "BaseTableNamespace";

		/// <summary>The namespace of the column.</summary>
		// Token: 0x0400167B RID: 5755
		public static readonly string BaseColumnNamespace = "BaseColumnNamespace";

		/// <summary>Specifies the mapping for the column.</summary>
		// Token: 0x0400167C RID: 5756
		public static readonly string ColumnMapping = "ColumnMapping";
	}
}
