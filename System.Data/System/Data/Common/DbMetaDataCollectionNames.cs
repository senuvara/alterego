﻿using System;

namespace System.Data.Common
{
	/// <summary>Provides a list of constants for the well-known MetaDataCollections: DataSourceInformation, DataTypes, MetaDataCollections, ReservedWords, and Restrictions.</summary>
	// Token: 0x020002BB RID: 699
	public static class DbMetaDataCollectionNames
	{
		// Token: 0x06002354 RID: 9044 RVA: 0x000A0235 File Offset: 0x0009E435
		// Note: this type is marked as 'beforefieldinit'.
		static DbMetaDataCollectionNames()
		{
		}

		/// <summary>A constant for use with the <see cref="M:System.Data.Common.DbConnection.GetSchema" /> method that represents the MetaDataCollections collection.</summary>
		// Token: 0x040015F1 RID: 5617
		public static readonly string MetaDataCollections = "MetaDataCollections";

		/// <summary>A constant for use with the <see cref="M:System.Data.Common.DbConnection.GetSchema" /> method that represents the DataSourceInformation collection.</summary>
		// Token: 0x040015F2 RID: 5618
		public static readonly string DataSourceInformation = "DataSourceInformation";

		/// <summary>A constant for use with the <see cref="M:System.Data.Common.DbConnection.GetSchema" /> method that represents the DataTypes collection.</summary>
		// Token: 0x040015F3 RID: 5619
		public static readonly string DataTypes = "DataTypes";

		/// <summary>A constant for use with the <see cref="M:System.Data.Common.DbConnection.GetSchema" /> method that represents the Restrictions collection.  </summary>
		// Token: 0x040015F4 RID: 5620
		public static readonly string Restrictions = "Restrictions";

		/// <summary>A constant for use with the <see cref="M:System.Data.Common.DbConnection.GetSchema" /> method that represents the ReservedWords collection.</summary>
		// Token: 0x040015F5 RID: 5621
		public static readonly string ReservedWords = "ReservedWords";
	}
}
