﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.IO;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Xml;
using System.Xml.Serialization;

namespace System.Data.Common
{
	// Token: 0x020002E2 RID: 738
	internal sealed class SqlUdtStorage : DataStorage
	{
		// Token: 0x06002537 RID: 9527 RVA: 0x000A9703 File Offset: 0x000A7903
		public SqlUdtStorage(DataColumn column, Type type) : this(column, type, SqlUdtStorage.GetStaticNullForUdtType(type))
		{
		}

		// Token: 0x06002538 RID: 9528 RVA: 0x000A9714 File Offset: 0x000A7914
		private SqlUdtStorage(DataColumn column, Type type, object nullValue) : base(column, type, nullValue, nullValue, typeof(ICloneable).IsAssignableFrom(type), DataStorage.GetStorageType(type))
		{
			this._implementsIXmlSerializable = typeof(IXmlSerializable).IsAssignableFrom(type);
			this._implementsIComparable = typeof(IComparable).IsAssignableFrom(type);
		}

		// Token: 0x06002539 RID: 9529 RVA: 0x000A9770 File Offset: 0x000A7970
		internal static object GetStaticNullForUdtType(Type type)
		{
			object value;
			if (!SqlUdtStorage.s_typeToNull.TryGetValue(type, out value))
			{
				PropertyInfo property = type.GetProperty("Null", BindingFlags.Static | BindingFlags.Public);
				if (property != null)
				{
					value = property.GetValue(null, null);
				}
				else
				{
					FieldInfo field = type.GetField("Null", BindingFlags.Static | BindingFlags.Public);
					if (!(field != null))
					{
						throw ExceptionBuilder.INullableUDTwithoutStaticNull(type.AssemblyQualifiedName);
					}
					value = field.GetValue(null);
				}
				Dictionary<Type, object> obj = SqlUdtStorage.s_typeToNull;
				lock (obj)
				{
					SqlUdtStorage.s_typeToNull[type] = value;
				}
			}
			return value;
		}

		// Token: 0x0600253A RID: 9530 RVA: 0x000A9818 File Offset: 0x000A7A18
		public override bool IsNull(int record)
		{
			return ((INullable)this._values[record]).IsNull;
		}

		// Token: 0x0600253B RID: 9531 RVA: 0x00096364 File Offset: 0x00094564
		public override object Aggregate(int[] records, AggregateType kind)
		{
			throw ExceptionBuilder.AggregateException(kind, this._dataType);
		}

		// Token: 0x0600253C RID: 9532 RVA: 0x000A982C File Offset: 0x000A7A2C
		public override int Compare(int recordNo1, int recordNo2)
		{
			return this.CompareValueTo(recordNo1, this._values[recordNo2]);
		}

		// Token: 0x0600253D RID: 9533 RVA: 0x000A9840 File Offset: 0x000A7A40
		public override int CompareValueTo(int recordNo1, object value)
		{
			if (DBNull.Value == value)
			{
				value = this._nullValue;
			}
			if (this._implementsIComparable)
			{
				return ((IComparable)this._values[recordNo1]).CompareTo(value);
			}
			if (this._nullValue != value)
			{
				throw ExceptionBuilder.IComparableNotImplemented(this._dataType.AssemblyQualifiedName);
			}
			if (!((INullable)this._values[recordNo1]).IsNull)
			{
				return 1;
			}
			return 0;
		}

		// Token: 0x0600253E RID: 9534 RVA: 0x000A98AA File Offset: 0x000A7AAA
		public override void Copy(int recordNo1, int recordNo2)
		{
			base.CopyBits(recordNo1, recordNo2);
			this._values[recordNo2] = this._values[recordNo1];
		}

		// Token: 0x0600253F RID: 9535 RVA: 0x000A98C4 File Offset: 0x000A7AC4
		public override object Get(int recordNo)
		{
			return this._values[recordNo];
		}

		// Token: 0x06002540 RID: 9536 RVA: 0x000A98D0 File Offset: 0x000A7AD0
		public override void Set(int recordNo, object value)
		{
			if (DBNull.Value == value)
			{
				this._values[recordNo] = this._nullValue;
				base.SetNullBit(recordNo, true);
				return;
			}
			if (value == null)
			{
				if (this._isValueType)
				{
					throw ExceptionBuilder.StorageSetFailed();
				}
				this._values[recordNo] = this._nullValue;
				base.SetNullBit(recordNo, true);
				return;
			}
			else
			{
				if (!this._dataType.IsInstanceOfType(value))
				{
					throw ExceptionBuilder.StorageSetFailed();
				}
				this._values[recordNo] = value;
				base.SetNullBit(recordNo, false);
				return;
			}
		}

		// Token: 0x06002541 RID: 9537 RVA: 0x000A994C File Offset: 0x000A7B4C
		public override void SetCapacity(int capacity)
		{
			object[] array = new object[capacity];
			if (this._values != null)
			{
				Array.Copy(this._values, 0, array, 0, Math.Min(capacity, this._values.Length));
			}
			this._values = array;
			base.SetCapacity(capacity);
		}

		// Token: 0x06002542 RID: 9538 RVA: 0x000A9994 File Offset: 0x000A7B94
		[MethodImpl(MethodImplOptions.NoInlining)]
		public override object ConvertXmlToObject(string s)
		{
			if (this._implementsIXmlSerializable)
			{
				object obj = Activator.CreateInstance(this._dataType, true);
				using (XmlTextReader xmlTextReader = new XmlTextReader(new StringReader("<col>" + s + "</col>")))
				{
					((IXmlSerializable)obj).ReadXml(xmlTextReader);
				}
				return obj;
			}
			StringReader textReader = new StringReader(s);
			return ObjectStorage.GetXmlSerializer(this._dataType).Deserialize(textReader);
		}

		// Token: 0x06002543 RID: 9539 RVA: 0x000A9A14 File Offset: 0x000A7C14
		[MethodImpl(MethodImplOptions.NoInlining)]
		public override object ConvertXmlToObject(XmlReader xmlReader, XmlRootAttribute xmlAttrib)
		{
			if (xmlAttrib == null)
			{
				string text = xmlReader.GetAttribute("InstanceType", "urn:schemas-microsoft-com:xml-msdata");
				if (text == null)
				{
					string attribute = xmlReader.GetAttribute("InstanceType", "http://www.w3.org/2001/XMLSchema-instance");
					if (attribute != null)
					{
						text = XSDSchema.XsdtoClr(attribute).FullName;
					}
				}
				object obj = Activator.CreateInstance((text == null) ? this._dataType : Type.GetType(text), true);
				((IXmlSerializable)obj).ReadXml(xmlReader);
				return obj;
			}
			return ObjectStorage.GetXmlSerializer(this._dataType, xmlAttrib).Deserialize(xmlReader);
		}

		// Token: 0x06002544 RID: 9540 RVA: 0x000A9A90 File Offset: 0x000A7C90
		public override string ConvertObjectToXml(object value)
		{
			StringWriter stringWriter = new StringWriter(base.FormatProvider);
			if (this._implementsIXmlSerializable)
			{
				using (XmlTextWriter xmlTextWriter = new XmlTextWriter(stringWriter))
				{
					((IXmlSerializable)value).WriteXml(xmlTextWriter);
					goto IL_45;
				}
			}
			ObjectStorage.GetXmlSerializer(value.GetType()).Serialize(stringWriter, value);
			IL_45:
			return stringWriter.ToString();
		}

		// Token: 0x06002545 RID: 9541 RVA: 0x000A9AF8 File Offset: 0x000A7CF8
		public override void ConvertObjectToXml(object value, XmlWriter xmlWriter, XmlRootAttribute xmlAttrib)
		{
			if (xmlAttrib == null)
			{
				((IXmlSerializable)value).WriteXml(xmlWriter);
				return;
			}
			ObjectStorage.GetXmlSerializer(this._dataType, xmlAttrib).Serialize(xmlWriter, value);
		}

		// Token: 0x06002546 RID: 9542 RVA: 0x000A292D File Offset: 0x000A0B2D
		protected override object GetEmptyStorage(int recordCount)
		{
			return new object[recordCount];
		}

		// Token: 0x06002547 RID: 9543 RVA: 0x000A9B1D File Offset: 0x000A7D1D
		protected override void CopyValue(int record, object store, BitArray nullbits, int storeIndex)
		{
			((object[])store)[storeIndex] = this._values[record];
			nullbits.Set(storeIndex, this.IsNull(record));
		}

		// Token: 0x06002548 RID: 9544 RVA: 0x000A9B3F File Offset: 0x000A7D3F
		protected override void SetStorage(object store, BitArray nullbits)
		{
			this._values = (object[])store;
		}

		// Token: 0x06002549 RID: 9545 RVA: 0x000A9B4D File Offset: 0x000A7D4D
		// Note: this type is marked as 'beforefieldinit'.
		static SqlUdtStorage()
		{
		}

		// Token: 0x0400167F RID: 5759
		private object[] _values;

		// Token: 0x04001680 RID: 5760
		private readonly bool _implementsIXmlSerializable;

		// Token: 0x04001681 RID: 5761
		private readonly bool _implementsIComparable;

		// Token: 0x04001682 RID: 5762
		private static readonly Dictionary<Type, object> s_typeToNull = new Dictionary<Type, object>();
	}
}
