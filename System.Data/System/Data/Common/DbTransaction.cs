﻿using System;

namespace System.Data.Common
{
	/// <summary>The base class for a transaction. </summary>
	// Token: 0x020002C1 RID: 705
	public abstract class DbTransaction : MarshalByRefObject, IDbTransaction, IDisposable
	{
		/// <summary>Initializes a new <see cref="T:System.Data.Common.DbTransaction" /> object.</summary>
		// Token: 0x0600239D RID: 9117 RVA: 0x000044AD File Offset: 0x000026AD
		protected DbTransaction()
		{
		}

		/// <summary>Specifies the <see cref="T:System.Data.Common.DbConnection" /> object associated with the transaction.</summary>
		/// <returns>The <see cref="T:System.Data.Common.DbConnection" /> object associated with the transaction.</returns>
		// Token: 0x17000625 RID: 1573
		// (get) Token: 0x0600239E RID: 9118 RVA: 0x000A0497 File Offset: 0x0009E697
		public DbConnection Connection
		{
			get
			{
				return this.DbConnection;
			}
		}

		/// <summary>Gets the <see cref="T:System.Data.Common.DbConnection" /> object associated with the transaction, or a null reference if the transaction is no longer valid.</summary>
		/// <returns>The <see cref="T:System.Data.Common.DbConnection" /> object associated with the transaction.</returns>
		// Token: 0x17000626 RID: 1574
		// (get) Token: 0x0600239F RID: 9119 RVA: 0x000A0497 File Offset: 0x0009E697
		IDbConnection IDbTransaction.Connection
		{
			get
			{
				return this.DbConnection;
			}
		}

		/// <summary>Specifies the <see cref="T:System.Data.Common.DbConnection" /> object associated with the transaction.</summary>
		/// <returns>The <see cref="T:System.Data.Common.DbConnection" /> object associated with the transaction.</returns>
		// Token: 0x17000627 RID: 1575
		// (get) Token: 0x060023A0 RID: 9120
		protected abstract DbConnection DbConnection { get; }

		/// <summary>Specifies the <see cref="T:System.Data.IsolationLevel" /> for this transaction.</summary>
		/// <returns>The <see cref="T:System.Data.IsolationLevel" /> for this transaction.</returns>
		// Token: 0x17000628 RID: 1576
		// (get) Token: 0x060023A1 RID: 9121
		public abstract IsolationLevel IsolationLevel { get; }

		/// <summary>Commits the database transaction.</summary>
		// Token: 0x060023A2 RID: 9122
		public abstract void Commit();

		/// <summary>Releases the unmanaged resources used by the <see cref="T:System.Data.Common.DbTransaction" />.</summary>
		// Token: 0x060023A3 RID: 9123 RVA: 0x000A049F File Offset: 0x0009E69F
		public void Dispose()
		{
			this.Dispose(true);
		}

		/// <summary>Releases the unmanaged resources used by the <see cref="T:System.Data.Common.DbTransaction" /> and optionally releases the managed resources.</summary>
		/// <param name="disposing">If <see langword="true" />, this method releases all resources held by any managed objects that this <see cref="T:System.Data.Common.DbTransaction" /> references.</param>
		// Token: 0x060023A4 RID: 9124 RVA: 0x00005E03 File Offset: 0x00004003
		protected virtual void Dispose(bool disposing)
		{
		}

		/// <summary>Rolls back a transaction from a pending state.</summary>
		// Token: 0x060023A5 RID: 9125
		public abstract void Rollback();
	}
}
