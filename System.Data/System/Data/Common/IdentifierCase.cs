﻿using System;

namespace System.Data.Common
{
	/// <summary>Specifies how identifiers are treated by the data source when searching the system catalog.</summary>
	// Token: 0x020002E9 RID: 745
	public enum IdentifierCase
	{
		/// <summary>The data source has ambiguous rules regarding identifier case and cannot discern this information.</summary>
		// Token: 0x04001693 RID: 5779
		Unknown,
		/// <summary>The data source ignores identifier case when searching the system catalog. The identifiers "ab" and "AB" will match.</summary>
		// Token: 0x04001694 RID: 5780
		Insensitive,
		/// <summary>The data source distinguishes identifier case when searching the system catalog. The identifiers "ab" and "AB" will not match.</summary>
		// Token: 0x04001695 RID: 5781
		Sensitive
	}
}
