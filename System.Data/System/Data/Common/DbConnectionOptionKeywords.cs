﻿using System;

namespace System.Data.Common
{
	// Token: 0x020002F6 RID: 758
	internal static class DbConnectionOptionKeywords
	{
		// Token: 0x04001712 RID: 5906
		internal const string Driver = "driver";

		// Token: 0x04001713 RID: 5907
		internal const string Pwd = "pwd";

		// Token: 0x04001714 RID: 5908
		internal const string UID = "uid";

		// Token: 0x04001715 RID: 5909
		internal const string DataProvider = "data provider";

		// Token: 0x04001716 RID: 5910
		internal const string ExtendedProperties = "extended properties";

		// Token: 0x04001717 RID: 5911
		internal const string FileName = "file name";

		// Token: 0x04001718 RID: 5912
		internal const string Provider = "provider";

		// Token: 0x04001719 RID: 5913
		internal const string RemoteProvider = "remote provider";

		// Token: 0x0400171A RID: 5914
		internal const string Password = "password";

		// Token: 0x0400171B RID: 5915
		internal const string UserID = "user id";
	}
}
