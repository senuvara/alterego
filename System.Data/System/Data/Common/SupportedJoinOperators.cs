﻿using System;

namespace System.Data.Common
{
	/// <summary>Specifies what types of Transact-SQL join statements are supported by the data source.</summary>
	// Token: 0x020002E4 RID: 740
	[Flags]
	public enum SupportedJoinOperators
	{
		/// <summary>The data source does not support join queries.</summary>
		// Token: 0x04001685 RID: 5765
		None = 0,
		/// <summary>The data source supports inner joins.</summary>
		// Token: 0x04001686 RID: 5766
		Inner = 1,
		/// <summary>The data source supports left outer joins.</summary>
		// Token: 0x04001687 RID: 5767
		LeftOuter = 2,
		/// <summary>The data source supports right outer joins.</summary>
		// Token: 0x04001688 RID: 5768
		RightOuter = 4,
		/// <summary>The data source supports full outer joins.</summary>
		// Token: 0x04001689 RID: 5769
		FullOuter = 8
	}
}
