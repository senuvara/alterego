﻿using System;

namespace System.Data.Common
{
	// Token: 0x0200029F RID: 671
	internal sealed class LoadAdapter : DataAdapter
	{
		// Token: 0x060020C9 RID: 8393 RVA: 0x0009A0C2 File Offset: 0x000982C2
		internal LoadAdapter()
		{
		}

		// Token: 0x060020CA RID: 8394 RVA: 0x0009A0CA File Offset: 0x000982CA
		internal int FillFromReader(DataTable[] dataTables, IDataReader dataReader, int startRecord, int maxRecords)
		{
			return this.Fill(dataTables, dataReader, startRecord, maxRecords);
		}
	}
}
