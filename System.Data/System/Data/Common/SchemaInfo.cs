﻿using System;

namespace System.Data.Common
{
	// Token: 0x020002A4 RID: 676
	internal struct SchemaInfo
	{
		// Token: 0x0400156E RID: 5486
		public string name;

		// Token: 0x0400156F RID: 5487
		public string typeName;

		// Token: 0x04001570 RID: 5488
		public Type type;
	}
}
