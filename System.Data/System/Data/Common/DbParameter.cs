﻿using System;
using System.ComponentModel;

namespace System.Data.Common
{
	/// <summary>Represents a parameter to a <see cref="T:System.Data.Common.DbCommand" /> and optionally, its mapping to a <see cref="T:System.Data.DataSet" /> column. For more information on parameters, see Configuring Parameters and Parameter Data Types.</summary>
	// Token: 0x020002BD RID: 701
	public abstract class DbParameter : MarshalByRefObject, IDbDataParameter, IDataParameter
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Data.Common.DbParameter" /> class.</summary>
		// Token: 0x06002356 RID: 9046 RVA: 0x000044AD File Offset: 0x000026AD
		protected DbParameter()
		{
		}

		/// <summary>Gets or sets the <see cref="T:System.Data.DbType" /> of the parameter.</summary>
		/// <returns>One of the <see cref="T:System.Data.DbType" /> values. The default is <see cref="F:System.Data.DbType.String" />.</returns>
		/// <exception cref="T:System.ArgumentException">The property is not set to a valid <see cref="T:System.Data.DbType" />.</exception>
		// Token: 0x1700060D RID: 1549
		// (get) Token: 0x06002357 RID: 9047
		// (set) Token: 0x06002358 RID: 9048
		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[RefreshProperties(RefreshProperties.All)]
		public abstract DbType DbType { get; set; }

		/// <summary>Resets the DbType property to its original settings.</summary>
		// Token: 0x06002359 RID: 9049
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public abstract void ResetDbType();

		/// <summary>Gets or sets a value that indicates whether the parameter is input-only, output-only, bidirectional, or a stored procedure return value parameter.</summary>
		/// <returns>One of the <see cref="T:System.Data.ParameterDirection" /> values. The default is <see langword="Input" />.</returns>
		/// <exception cref="T:System.ArgumentException">The property is not set to one of the valid <see cref="T:System.Data.ParameterDirection" /> values.</exception>
		// Token: 0x1700060E RID: 1550
		// (get) Token: 0x0600235A RID: 9050
		// (set) Token: 0x0600235B RID: 9051
		[RefreshProperties(RefreshProperties.All)]
		[DefaultValue(ParameterDirection.Input)]
		public abstract ParameterDirection Direction { get; set; }

		/// <summary>Gets or sets a value that indicates whether the parameter accepts null values.</summary>
		/// <returns>
		///     <see langword="true" /> if null values are accepted; otherwise <see langword="false" />. The default is <see langword="false" />.</returns>
		// Token: 0x1700060F RID: 1551
		// (get) Token: 0x0600235C RID: 9052
		// (set) Token: 0x0600235D RID: 9053
		[EditorBrowsable(EditorBrowsableState.Never)]
		[DesignOnly(true)]
		[Browsable(false)]
		public abstract bool IsNullable { get; set; }

		/// <summary>Gets or sets the name of the <see cref="T:System.Data.Common.DbParameter" />.</summary>
		/// <returns>The name of the <see cref="T:System.Data.Common.DbParameter" />. The default is an empty string ("").</returns>
		// Token: 0x17000610 RID: 1552
		// (get) Token: 0x0600235E RID: 9054
		// (set) Token: 0x0600235F RID: 9055
		[DefaultValue("")]
		public abstract string ParameterName { get; set; }

		/// <summary>Indicates the precision of numeric parameters.</summary>
		/// <returns>The maximum number of digits used to represent the <see langword="Value" /> property of a data provider <see langword="Parameter" /> object. The default value is 0, which indicates that a data provider sets the precision for <see langword="Value" />.</returns>
		// Token: 0x17000611 RID: 1553
		// (get) Token: 0x06002360 RID: 9056 RVA: 0x000061C5 File Offset: 0x000043C5
		// (set) Token: 0x06002361 RID: 9057 RVA: 0x00005E03 File Offset: 0x00004003
		byte IDbDataParameter.Precision
		{
			get
			{
				return 0;
			}
			set
			{
			}
		}

		/// <summary>For a description of this member, see <see cref="P:System.Data.IDbDataParameter.Scale" />.</summary>
		/// <returns>The number of decimal places to which <see cref="T:System.Data.OleDb.OleDbParameter.Value" /> is resolved. The default is 0.</returns>
		// Token: 0x17000612 RID: 1554
		// (get) Token: 0x06002362 RID: 9058 RVA: 0x000061C5 File Offset: 0x000043C5
		// (set) Token: 0x06002363 RID: 9059 RVA: 0x00005E03 File Offset: 0x00004003
		byte IDbDataParameter.Scale
		{
			get
			{
				return 0;
			}
			set
			{
			}
		}

		/// <summary>[Supported in the .NET Framework 4.5.1 and later versions] Gets or sets the maximum number of digits used to represent the <see cref="P:System.Data.Common.DbParameter.Value" /> property.</summary>
		/// <returns>The maximum number of digits used to represent the <see cref="P:System.Data.Common.DbParameter.Value" /> property.</returns>
		// Token: 0x17000613 RID: 1555
		// (get) Token: 0x06002364 RID: 9060 RVA: 0x000A0427 File Offset: 0x0009E627
		// (set) Token: 0x06002365 RID: 9061 RVA: 0x000A042F File Offset: 0x0009E62F
		public virtual byte Precision
		{
			get
			{
				return ((IDbDataParameter)this).Precision;
			}
			set
			{
				((IDbDataParameter)this).Precision = value;
			}
		}

		/// <summary>Gets or sets the number of decimal places to which <see cref="P:System.Data.Common.DbParameter.Value" /> is resolved.</summary>
		/// <returns>The number of decimal places to which <see cref="P:System.Data.Common.DbParameter.Value" /> is resolved.</returns>
		// Token: 0x17000614 RID: 1556
		// (get) Token: 0x06002366 RID: 9062 RVA: 0x000A0438 File Offset: 0x0009E638
		// (set) Token: 0x06002367 RID: 9063 RVA: 0x000A0440 File Offset: 0x0009E640
		public virtual byte Scale
		{
			get
			{
				return ((IDbDataParameter)this).Scale;
			}
			set
			{
				((IDbDataParameter)this).Scale = value;
			}
		}

		/// <summary>Gets or sets the maximum size, in bytes, of the data within the column.</summary>
		/// <returns>The maximum size, in bytes, of the data within the column. The default value is inferred from the parameter value.</returns>
		// Token: 0x17000615 RID: 1557
		// (get) Token: 0x06002368 RID: 9064
		// (set) Token: 0x06002369 RID: 9065
		public abstract int Size { get; set; }

		/// <summary>Gets or sets the name of the source column mapped to the <see cref="T:System.Data.DataSet" /> and used for loading or returning the <see cref="P:System.Data.Common.DbParameter.Value" />.</summary>
		/// <returns>The name of the source column mapped to the <see cref="T:System.Data.DataSet" />. The default is an empty string.</returns>
		// Token: 0x17000616 RID: 1558
		// (get) Token: 0x0600236A RID: 9066
		// (set) Token: 0x0600236B RID: 9067
		[DefaultValue("")]
		public abstract string SourceColumn { get; set; }

		/// <summary>Sets or gets a value which indicates whether the source column is nullable. This allows <see cref="T:System.Data.Common.DbCommandBuilder" /> to correctly generate Update statements for nullable columns.</summary>
		/// <returns>
		///     <see langword="true" /> if the source column is nullable; <see langword="false" /> if it is not.</returns>
		// Token: 0x17000617 RID: 1559
		// (get) Token: 0x0600236C RID: 9068
		// (set) Token: 0x0600236D RID: 9069
		[RefreshProperties(RefreshProperties.All)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		[DefaultValue(false)]
		public abstract bool SourceColumnNullMapping { get; set; }

		/// <summary>Gets or sets the <see cref="T:System.Data.DataRowVersion" /> to use when you load <see cref="P:System.Data.Common.DbParameter.Value" />.</summary>
		/// <returns>One of the <see cref="T:System.Data.DataRowVersion" /> values. The default is <see langword="Current" />.</returns>
		/// <exception cref="T:System.ArgumentException">The property is not set to one of the <see cref="T:System.Data.DataRowVersion" /> values.</exception>
		// Token: 0x17000618 RID: 1560
		// (get) Token: 0x0600236E RID: 9070 RVA: 0x000A0449 File Offset: 0x0009E649
		// (set) Token: 0x0600236F RID: 9071 RVA: 0x00005E03 File Offset: 0x00004003
		[DefaultValue(DataRowVersion.Current)]
		public virtual DataRowVersion SourceVersion
		{
			get
			{
				return DataRowVersion.Default;
			}
			set
			{
			}
		}

		/// <summary>Gets or sets the value of the parameter.</summary>
		/// <returns>An <see cref="T:System.Object" /> that is the value of the parameter. The default value is null.</returns>
		// Token: 0x17000619 RID: 1561
		// (get) Token: 0x06002370 RID: 9072
		// (set) Token: 0x06002371 RID: 9073
		[DefaultValue(null)]
		[RefreshProperties(RefreshProperties.All)]
		public abstract object Value { get; set; }
	}
}
