﻿using System;

namespace System.Data.Common
{
	/// <summary>Indicates the position of the catalog name in a qualified table name in a text command. </summary>
	// Token: 0x02000294 RID: 660
	public enum CatalogLocation
	{
		/// <summary>Indicates that the position of the catalog name occurs before the schema portion of a fully qualified table name in a text command.</summary>
		// Token: 0x040014FB RID: 5371
		Start = 1,
		/// <summary>Indicates that the position of the catalog name occurs after the schema portion of a fully qualified table name in a text command.</summary>
		// Token: 0x040014FC RID: 5372
		End
	}
}
