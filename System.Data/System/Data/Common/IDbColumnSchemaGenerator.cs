﻿using System;
using System.Collections.ObjectModel;

namespace System.Data.Common
{
	/// <summary>Generates a column schema.</summary>
	// Token: 0x020002C5 RID: 709
	public interface IDbColumnSchemaGenerator
	{
		/// <summary>Gets the column schema (<see cref="T:System.Data.Common.DbColumn" /> collection).</summary>
		/// <returns>The column schema (<see cref="T:System.Data.Common.DbColumn" /> collection).</returns>
		// Token: 0x060023C3 RID: 9155
		ReadOnlyCollection<DbColumn> GetColumnSchema();
	}
}
