﻿using System;
using System.CodeDom;
using System.CodeDom.Compiler;
using System.Xml.Schema;
using System.Xml.Serialization;
using System.Xml.Serialization.Advanced;
using Unity;

namespace System.Data
{
	/// <summary>This member supports the .NET Framework infrastructure and is not intended to be used directly from your code.</summary>
	// Token: 0x02000339 RID: 825
	public class DataSetSchemaImporterExtension : SchemaImporterExtension
	{
		/// <summary>This member supports the .NET Framework infrastructure and is not intended to be used directly from your code.</summary>
		// Token: 0x06002943 RID: 10563 RVA: 0x00010458 File Offset: 0x0000E658
		public DataSetSchemaImporterExtension()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>This member supports the .NET Framework infrastructure and is not intended to be used directly from your code.</summary>
		/// <param name="name">
		///       <paramref name="name" />
		///     </param>
		/// <param name="schemaNamespace">
		///       <paramref name="schemaNamespace" />
		///     </param>
		/// <param name="context">
		///       <paramref name="context" />
		///     </param>
		/// <param name="schemas">
		///       <paramref name="schemas" />
		///     </param>
		/// <param name="importer">
		///       <paramref name="importer" />
		///     </param>
		/// <param name="compileUnit">
		///       <paramref name="compileUnit" />
		///     </param>
		/// <param name="mainNamespace">
		///       <paramref name="mainNamespace" />
		///     </param>
		/// <param name="options">
		///       <paramref name="options" />
		///     </param>
		/// <param name="codeProvider">
		///       <paramref name="codeProvider" />
		///     </param>
		/// <returns>This member supports the .NET Framework infrastructure and is not intended to be used directly from your code.</returns>
		// Token: 0x06002944 RID: 10564 RVA: 0x00051759 File Offset: 0x0004F959
		public override string ImportSchemaType(string name, string schemaNamespace, XmlSchemaObject context, XmlSchemas schemas, XmlSchemaImporter importer, CodeCompileUnit compileUnit, CodeNamespace mainNamespace, CodeGenerationOptions options, CodeDomProvider codeProvider)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>This member supports the .NET Framework infrastructure and is not intended to be used directly from your code.</summary>
		/// <param name="type">
		///       <paramref name="type" />
		///     </param>
		/// <param name="context">
		///       <paramref name="context" />
		///     </param>
		/// <param name="schemas">
		///       <paramref name="schemas" />
		///     </param>
		/// <param name="importer">
		///       <paramref name="importer" />
		///     </param>
		/// <param name="compileUnit">
		///       <paramref name="compileUnit" />
		///     </param>
		/// <param name="mainNamespace">
		///       <paramref name="mainNamespace" />
		///     </param>
		/// <param name="options">
		///       <paramref name="options" />
		///     </param>
		/// <param name="codeProvider">
		///       <paramref name="codeProvider" />
		///     </param>
		/// <returns>This member supports the .NET Framework infrastructure and is not intended to be used directly from your code.</returns>
		// Token: 0x06002945 RID: 10565 RVA: 0x00051759 File Offset: 0x0004F959
		public override string ImportSchemaType(XmlSchemaType type, XmlSchemaObject context, XmlSchemas schemas, XmlSchemaImporter importer, CodeCompileUnit compileUnit, CodeNamespace mainNamespace, CodeGenerationOptions options, CodeDomProvider codeProvider)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}
	}
}
