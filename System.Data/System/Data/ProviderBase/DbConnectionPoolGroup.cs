﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data.Common;

namespace System.Data.ProviderBase
{
	// Token: 0x02000280 RID: 640
	internal sealed class DbConnectionPoolGroup
	{
		// Token: 0x06001E45 RID: 7749 RVA: 0x000919C0 File Offset: 0x0008FBC0
		internal DbConnectionPoolGroup(DbConnectionOptions connectionOptions, DbConnectionPoolKey key, DbConnectionPoolGroupOptions poolGroupOptions)
		{
			this._connectionOptions = connectionOptions;
			this._poolKey = key;
			this._poolGroupOptions = poolGroupOptions;
			this._poolCollection = new ConcurrentDictionary<DbConnectionPoolIdentity, DbConnectionPool>();
			this._state = 1;
		}

		// Token: 0x1700052D RID: 1325
		// (get) Token: 0x06001E46 RID: 7750 RVA: 0x000919EF File Offset: 0x0008FBEF
		internal DbConnectionOptions ConnectionOptions
		{
			get
			{
				return this._connectionOptions;
			}
		}

		// Token: 0x1700052E RID: 1326
		// (get) Token: 0x06001E47 RID: 7751 RVA: 0x000919F7 File Offset: 0x0008FBF7
		internal DbConnectionPoolKey PoolKey
		{
			get
			{
				return this._poolKey;
			}
		}

		// Token: 0x1700052F RID: 1327
		// (get) Token: 0x06001E48 RID: 7752 RVA: 0x000919FF File Offset: 0x0008FBFF
		// (set) Token: 0x06001E49 RID: 7753 RVA: 0x00091A07 File Offset: 0x0008FC07
		internal DbConnectionPoolGroupProviderInfo ProviderInfo
		{
			get
			{
				return this._providerInfo;
			}
			set
			{
				this._providerInfo = value;
				if (value != null)
				{
					this._providerInfo.PoolGroup = this;
				}
			}
		}

		// Token: 0x17000530 RID: 1328
		// (get) Token: 0x06001E4A RID: 7754 RVA: 0x00091A1F File Offset: 0x0008FC1F
		internal bool IsDisabled
		{
			get
			{
				return 4 == this._state;
			}
		}

		// Token: 0x17000531 RID: 1329
		// (get) Token: 0x06001E4B RID: 7755 RVA: 0x00091A2A File Offset: 0x0008FC2A
		internal DbConnectionPoolGroupOptions PoolGroupOptions
		{
			get
			{
				return this._poolGroupOptions;
			}
		}

		// Token: 0x17000532 RID: 1330
		// (get) Token: 0x06001E4C RID: 7756 RVA: 0x00091A32 File Offset: 0x0008FC32
		// (set) Token: 0x06001E4D RID: 7757 RVA: 0x00091A3A File Offset: 0x0008FC3A
		internal DbMetaDataFactory MetaDataFactory
		{
			get
			{
				return this._metaDataFactory;
			}
			set
			{
				this._metaDataFactory = value;
			}
		}

		// Token: 0x06001E4E RID: 7758 RVA: 0x00091A44 File Offset: 0x0008FC44
		internal int Clear()
		{
			ConcurrentDictionary<DbConnectionPoolIdentity, DbConnectionPool> concurrentDictionary = null;
			lock (this)
			{
				if (this._poolCollection.Count > 0)
				{
					concurrentDictionary = this._poolCollection;
					this._poolCollection = new ConcurrentDictionary<DbConnectionPoolIdentity, DbConnectionPool>();
				}
			}
			if (concurrentDictionary != null)
			{
				foreach (KeyValuePair<DbConnectionPoolIdentity, DbConnectionPool> keyValuePair in concurrentDictionary)
				{
					DbConnectionPool value = keyValuePair.Value;
					if (value != null)
					{
						value.ConnectionFactory.QueuePoolForRelease(value, true);
					}
				}
			}
			return this._poolCollection.Count;
		}

		// Token: 0x06001E4F RID: 7759 RVA: 0x00091AF8 File Offset: 0x0008FCF8
		internal DbConnectionPool GetConnectionPool(DbConnectionFactory connectionFactory)
		{
			DbConnectionPool dbConnectionPool = null;
			if (this._poolGroupOptions != null)
			{
				DbConnectionPoolIdentity dbConnectionPoolIdentity = DbConnectionPoolIdentity.NoIdentity;
				if (this._poolGroupOptions.PoolByIdentity)
				{
					dbConnectionPoolIdentity = DbConnectionPoolIdentity.GetCurrent();
					if (dbConnectionPoolIdentity.IsRestricted)
					{
						dbConnectionPoolIdentity = null;
					}
				}
				if (dbConnectionPoolIdentity != null && !this._poolCollection.TryGetValue(dbConnectionPoolIdentity, out dbConnectionPool))
				{
					DbConnectionPoolGroup obj = this;
					lock (obj)
					{
						if (!this._poolCollection.TryGetValue(dbConnectionPoolIdentity, out dbConnectionPool))
						{
							DbConnectionPoolProviderInfo connectionPoolProviderInfo = connectionFactory.CreateConnectionPoolProviderInfo(this.ConnectionOptions);
							DbConnectionPool dbConnectionPool2 = new DbConnectionPool(connectionFactory, this, dbConnectionPoolIdentity, connectionPoolProviderInfo);
							if (this.MarkPoolGroupAsActive())
							{
								dbConnectionPool2.Startup();
								this._poolCollection.TryAdd(dbConnectionPoolIdentity, dbConnectionPool2);
								dbConnectionPool = dbConnectionPool2;
							}
							else
							{
								dbConnectionPool2.Shutdown();
							}
						}
					}
				}
			}
			if (dbConnectionPool == null)
			{
				DbConnectionPoolGroup obj = this;
				lock (obj)
				{
					this.MarkPoolGroupAsActive();
				}
			}
			return dbConnectionPool;
		}

		// Token: 0x06001E50 RID: 7760 RVA: 0x00091BF4 File Offset: 0x0008FDF4
		private bool MarkPoolGroupAsActive()
		{
			if (2 == this._state)
			{
				this._state = 1;
			}
			return 1 == this._state;
		}

		// Token: 0x06001E51 RID: 7761 RVA: 0x00091C10 File Offset: 0x0008FE10
		internal bool Prune()
		{
			bool result;
			lock (this)
			{
				if (this._poolCollection.Count > 0)
				{
					ConcurrentDictionary<DbConnectionPoolIdentity, DbConnectionPool> concurrentDictionary = new ConcurrentDictionary<DbConnectionPoolIdentity, DbConnectionPool>();
					foreach (KeyValuePair<DbConnectionPoolIdentity, DbConnectionPool> keyValuePair in this._poolCollection)
					{
						DbConnectionPool value = keyValuePair.Value;
						if (value != null)
						{
							if (!value.ErrorOccurred && value.Count == 0)
							{
								value.ConnectionFactory.QueuePoolForRelease(value, false);
							}
							else
							{
								concurrentDictionary.TryAdd(keyValuePair.Key, keyValuePair.Value);
							}
						}
					}
					this._poolCollection = concurrentDictionary;
				}
				if (this._poolCollection.Count == 0)
				{
					if (1 == this._state)
					{
						this._state = 2;
					}
					else if (2 == this._state)
					{
						this._state = 4;
					}
				}
				result = (4 == this._state);
			}
			return result;
		}

		// Token: 0x04001440 RID: 5184
		private readonly DbConnectionOptions _connectionOptions;

		// Token: 0x04001441 RID: 5185
		private readonly DbConnectionPoolKey _poolKey;

		// Token: 0x04001442 RID: 5186
		private readonly DbConnectionPoolGroupOptions _poolGroupOptions;

		// Token: 0x04001443 RID: 5187
		private ConcurrentDictionary<DbConnectionPoolIdentity, DbConnectionPool> _poolCollection;

		// Token: 0x04001444 RID: 5188
		private int _state;

		// Token: 0x04001445 RID: 5189
		private DbConnectionPoolGroupProviderInfo _providerInfo;

		// Token: 0x04001446 RID: 5190
		private DbMetaDataFactory _metaDataFactory;

		// Token: 0x04001447 RID: 5191
		private const int PoolGroupStateActive = 1;

		// Token: 0x04001448 RID: 5192
		private const int PoolGroupStateIdle = 2;

		// Token: 0x04001449 RID: 5193
		private const int PoolGroupStateDisabled = 4;
	}
}
