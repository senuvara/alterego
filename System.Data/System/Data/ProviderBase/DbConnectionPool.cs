﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;

namespace System.Data.ProviderBase
{
	// Token: 0x0200027A RID: 634
	internal sealed class DbConnectionPool
	{
		// Token: 0x06001E04 RID: 7684 RVA: 0x0009053C File Offset: 0x0008E73C
		internal DbConnectionPool(DbConnectionFactory connectionFactory, DbConnectionPoolGroup connectionPoolGroup, DbConnectionPoolIdentity identity, DbConnectionPoolProviderInfo connectionPoolProviderInfo)
		{
			if (identity != null && identity.IsRestricted)
			{
				throw ADP.InternalError(ADP.InternalErrorCode.AttemptingToPoolOnRestrictedToken);
			}
			this._state = DbConnectionPool.State.Initializing;
			Random obj = DbConnectionPool.s_random;
			lock (obj)
			{
				this._cleanupWait = DbConnectionPool.s_random.Next(12, 24) * 10 * 1000;
			}
			this._connectionFactory = connectionFactory;
			this._connectionPoolGroup = connectionPoolGroup;
			this._connectionPoolGroupOptions = connectionPoolGroup.PoolGroupOptions;
			this._connectionPoolProviderInfo = connectionPoolProviderInfo;
			this._identity = identity;
			this._waitHandles = new DbConnectionPool.PoolWaitHandles();
			this._errorWait = 5000;
			this._errorTimer = null;
			this._objectList = new List<DbConnectionInternal>(this.MaxPoolSize);
			if (ADP.IsPlatformNT5)
			{
				this._transactedConnectionPool = new DbConnectionPool.TransactedConnectionPool(this);
			}
			this._poolCreateRequest = new WaitCallback(this.PoolCreateRequest);
			this._state = DbConnectionPool.State.Running;
		}

		// Token: 0x17000514 RID: 1300
		// (get) Token: 0x06001E05 RID: 7685 RVA: 0x00090658 File Offset: 0x0008E858
		private int CreationTimeout
		{
			get
			{
				return this.PoolGroupOptions.CreationTimeout;
			}
		}

		// Token: 0x17000515 RID: 1301
		// (get) Token: 0x06001E06 RID: 7686 RVA: 0x00090665 File Offset: 0x0008E865
		internal int Count
		{
			get
			{
				return this._totalObjects;
			}
		}

		// Token: 0x17000516 RID: 1302
		// (get) Token: 0x06001E07 RID: 7687 RVA: 0x0009066D File Offset: 0x0008E86D
		internal DbConnectionFactory ConnectionFactory
		{
			get
			{
				return this._connectionFactory;
			}
		}

		// Token: 0x17000517 RID: 1303
		// (get) Token: 0x06001E08 RID: 7688 RVA: 0x00090675 File Offset: 0x0008E875
		internal bool ErrorOccurred
		{
			get
			{
				return this._errorOccurred;
			}
		}

		// Token: 0x17000518 RID: 1304
		// (get) Token: 0x06001E09 RID: 7689 RVA: 0x0009067F File Offset: 0x0008E87F
		private bool HasTransactionAffinity
		{
			get
			{
				return this.PoolGroupOptions.HasTransactionAffinity;
			}
		}

		// Token: 0x17000519 RID: 1305
		// (get) Token: 0x06001E0A RID: 7690 RVA: 0x0009068C File Offset: 0x0008E88C
		internal TimeSpan LoadBalanceTimeout
		{
			get
			{
				return this.PoolGroupOptions.LoadBalanceTimeout;
			}
		}

		// Token: 0x1700051A RID: 1306
		// (get) Token: 0x06001E0B RID: 7691 RVA: 0x0009069C File Offset: 0x0008E89C
		private bool NeedToReplenish
		{
			get
			{
				if (DbConnectionPool.State.Running != this._state)
				{
					return false;
				}
				int count = this.Count;
				if (count >= this.MaxPoolSize)
				{
					return false;
				}
				if (count < this.MinPoolSize)
				{
					return true;
				}
				int num = this._stackNew.Count + this._stackOld.Count;
				int waitCount = this._waitCount;
				return num < waitCount || (num == waitCount && count > 1);
			}
		}

		// Token: 0x1700051B RID: 1307
		// (get) Token: 0x06001E0C RID: 7692 RVA: 0x00090700 File Offset: 0x0008E900
		internal DbConnectionPoolIdentity Identity
		{
			get
			{
				return this._identity;
			}
		}

		// Token: 0x1700051C RID: 1308
		// (get) Token: 0x06001E0D RID: 7693 RVA: 0x00090708 File Offset: 0x0008E908
		internal bool IsRunning
		{
			get
			{
				return DbConnectionPool.State.Running == this._state;
			}
		}

		// Token: 0x1700051D RID: 1309
		// (get) Token: 0x06001E0E RID: 7694 RVA: 0x00090713 File Offset: 0x0008E913
		private int MaxPoolSize
		{
			get
			{
				return this.PoolGroupOptions.MaxPoolSize;
			}
		}

		// Token: 0x1700051E RID: 1310
		// (get) Token: 0x06001E0F RID: 7695 RVA: 0x00090720 File Offset: 0x0008E920
		private int MinPoolSize
		{
			get
			{
				return this.PoolGroupOptions.MinPoolSize;
			}
		}

		// Token: 0x1700051F RID: 1311
		// (get) Token: 0x06001E10 RID: 7696 RVA: 0x0009072D File Offset: 0x0008E92D
		internal DbConnectionPoolGroup PoolGroup
		{
			get
			{
				return this._connectionPoolGroup;
			}
		}

		// Token: 0x17000520 RID: 1312
		// (get) Token: 0x06001E11 RID: 7697 RVA: 0x00090735 File Offset: 0x0008E935
		internal DbConnectionPoolGroupOptions PoolGroupOptions
		{
			get
			{
				return this._connectionPoolGroupOptions;
			}
		}

		// Token: 0x17000521 RID: 1313
		// (get) Token: 0x06001E12 RID: 7698 RVA: 0x0009073D File Offset: 0x0008E93D
		internal DbConnectionPoolProviderInfo ProviderInfo
		{
			get
			{
				return this._connectionPoolProviderInfo;
			}
		}

		// Token: 0x17000522 RID: 1314
		// (get) Token: 0x06001E13 RID: 7699 RVA: 0x00090745 File Offset: 0x0008E945
		internal bool UseLoadBalancing
		{
			get
			{
				return this.PoolGroupOptions.UseLoadBalancing;
			}
		}

		// Token: 0x17000523 RID: 1315
		// (get) Token: 0x06001E14 RID: 7700 RVA: 0x00090752 File Offset: 0x0008E952
		private bool UsingIntegrateSecurity
		{
			get
			{
				return this._identity != null && DbConnectionPoolIdentity.NoIdentity != this._identity;
			}
		}

		// Token: 0x06001E15 RID: 7701 RVA: 0x00090770 File Offset: 0x0008E970
		private void CleanupCallback(object state)
		{
			while (this.Count > this.MinPoolSize && this._waitHandles.PoolSemaphore.WaitOne(0))
			{
				DbConnectionInternal dbConnectionInternal;
				if (!this._stackOld.TryPop(out dbConnectionInternal))
				{
					this._waitHandles.PoolSemaphore.Release(1);
					break;
				}
				bool flag = true;
				DbConnectionInternal obj = dbConnectionInternal;
				lock (obj)
				{
					if (dbConnectionInternal.IsTransactionRoot)
					{
						flag = false;
					}
				}
				if (flag)
				{
					this.DestroyObject(dbConnectionInternal);
				}
				else
				{
					dbConnectionInternal.SetInStasis();
				}
			}
			if (this._waitHandles.PoolSemaphore.WaitOne(0))
			{
				DbConnectionInternal item;
				while (this._stackNew.TryPop(out item))
				{
					this._stackOld.Push(item);
				}
				this._waitHandles.PoolSemaphore.Release(1);
			}
			this.QueuePoolCreateRequest();
		}

		// Token: 0x06001E16 RID: 7702 RVA: 0x00090854 File Offset: 0x0008EA54
		internal void Clear()
		{
			List<DbConnectionInternal> objectList = this._objectList;
			DbConnectionInternal dbConnectionInternal;
			lock (objectList)
			{
				int count = this._objectList.Count;
				for (int i = 0; i < count; i++)
				{
					dbConnectionInternal = this._objectList[i];
					if (dbConnectionInternal != null)
					{
						dbConnectionInternal.DoNotPoolThisConnection();
					}
				}
				goto IL_57;
			}
			IL_50:
			this.DestroyObject(dbConnectionInternal);
			IL_57:
			if (!this._stackNew.TryPop(out dbConnectionInternal))
			{
				while (this._stackOld.TryPop(out dbConnectionInternal))
				{
					this.DestroyObject(dbConnectionInternal);
				}
				this.ReclaimEmancipatedObjects();
				return;
			}
			goto IL_50;
		}

		// Token: 0x06001E17 RID: 7703 RVA: 0x000908F8 File Offset: 0x0008EAF8
		private Timer CreateCleanupTimer()
		{
			return new Timer(new TimerCallback(this.CleanupCallback), null, this._cleanupWait, this._cleanupWait);
		}

		// Token: 0x06001E18 RID: 7704 RVA: 0x00090918 File Offset: 0x0008EB18
		private DbConnectionInternal CreateObject(DbConnection owningObject, DbConnectionOptions userOptions, DbConnectionInternal oldConnection)
		{
			DbConnectionInternal dbConnectionInternal = null;
			try
			{
				dbConnectionInternal = this._connectionFactory.CreatePooledConnection(this, owningObject, this._connectionPoolGroup.ConnectionOptions, this._connectionPoolGroup.PoolKey, userOptions);
				if (dbConnectionInternal == null)
				{
					throw ADP.InternalError(ADP.InternalErrorCode.CreateObjectReturnedNull);
				}
				if (!dbConnectionInternal.CanBePooled)
				{
					throw ADP.InternalError(ADP.InternalErrorCode.NewObjectCannotBePooled);
				}
				dbConnectionInternal.PrePush(null);
				List<DbConnectionInternal> objectList = this._objectList;
				lock (objectList)
				{
					if (oldConnection != null && oldConnection.Pool == this)
					{
						this._objectList.Remove(oldConnection);
					}
					this._objectList.Add(dbConnectionInternal);
					this._totalObjects = this._objectList.Count;
				}
				if (oldConnection != null)
				{
					DbConnectionPool pool = oldConnection.Pool;
					if (pool != null && pool != this)
					{
						objectList = pool._objectList;
						lock (objectList)
						{
							pool._objectList.Remove(oldConnection);
							pool._totalObjects = pool._objectList.Count;
						}
					}
				}
				this._errorWait = 5000;
			}
			catch (Exception ex)
			{
				if (!ADP.IsCatchableExceptionType(ex))
				{
					throw;
				}
				dbConnectionInternal = null;
				this._resError = ex;
				Timer timer = new Timer(new TimerCallback(this.ErrorCallback), null, -1, -1);
				try
				{
				}
				finally
				{
					this._waitHandles.ErrorEvent.Set();
					this._errorOccurred = true;
					this._errorTimer = timer;
					timer.Change(this._errorWait, this._errorWait);
				}
				if (30000 < this._errorWait)
				{
					this._errorWait = 60000;
				}
				else
				{
					this._errorWait *= 2;
				}
				throw;
			}
			return dbConnectionInternal;
		}

		// Token: 0x06001E19 RID: 7705 RVA: 0x00090AE0 File Offset: 0x0008ECE0
		private void DeactivateObject(DbConnectionInternal obj)
		{
			obj.DeactivateConnection();
			bool flag = false;
			bool flag2 = false;
			if (obj.IsConnectionDoomed)
			{
				flag2 = true;
			}
			else
			{
				lock (obj)
				{
					if (this._state == DbConnectionPool.State.ShuttingDown)
					{
						if (obj.IsTransactionRoot)
						{
							obj.SetInStasis();
						}
						else
						{
							flag2 = true;
						}
					}
					else if (obj.IsNonPoolableTransactionRoot)
					{
						obj.SetInStasis();
					}
					else if (obj.CanBePooled)
					{
						Transaction enlistedTransaction = obj.EnlistedTransaction;
						if (null != enlistedTransaction)
						{
							this._transactedConnectionPool.PutTransactedObject(enlistedTransaction, obj);
						}
						else
						{
							flag = true;
						}
					}
					else if (obj.IsTransactionRoot && !obj.IsConnectionDoomed)
					{
						obj.SetInStasis();
					}
					else
					{
						flag2 = true;
					}
				}
			}
			if (flag)
			{
				this.PutNewObject(obj);
				return;
			}
			if (flag2)
			{
				this.DestroyObject(obj);
				this.QueuePoolCreateRequest();
			}
		}

		// Token: 0x06001E1A RID: 7706 RVA: 0x00090BC0 File Offset: 0x0008EDC0
		internal void DestroyObject(DbConnectionInternal obj)
		{
			if (!obj.IsTxRootWaitingForTxEnd)
			{
				List<DbConnectionInternal> objectList = this._objectList;
				lock (objectList)
				{
					this._objectList.Remove(obj);
					this._totalObjects = this._objectList.Count;
				}
				obj.Dispose();
			}
		}

		// Token: 0x06001E1B RID: 7707 RVA: 0x00090C28 File Offset: 0x0008EE28
		private void ErrorCallback(object state)
		{
			this._errorOccurred = false;
			this._waitHandles.ErrorEvent.Reset();
			Timer errorTimer = this._errorTimer;
			this._errorTimer = null;
			if (errorTimer != null)
			{
				errorTimer.Dispose();
			}
		}

		// Token: 0x06001E1C RID: 7708 RVA: 0x00090C68 File Offset: 0x0008EE68
		private Exception TryCloneCachedException()
		{
			if (this._resError == null)
			{
				return null;
			}
			SqlException ex = this._resError as SqlException;
			if (ex != null)
			{
				return ex.InternalClone();
			}
			return this._resError;
		}

		// Token: 0x06001E1D RID: 7709 RVA: 0x00090C9C File Offset: 0x0008EE9C
		private void WaitForPendingOpen()
		{
			DbConnectionPool.PendingGetConnection pendingGetConnection;
			do
			{
				bool flag = false;
				try
				{
					try
					{
					}
					finally
					{
						flag = (Interlocked.CompareExchange(ref this._pendingOpensWaiting, 1, 0) == 0);
					}
					if (!flag)
					{
						break;
					}
					while (this._pendingOpens.TryDequeue(out pendingGetConnection))
					{
						if (!pendingGetConnection.Completion.Task.IsCompleted)
						{
							uint waitForMultipleObjectsTimeout;
							if (pendingGetConnection.DueTime == -1L)
							{
								waitForMultipleObjectsTimeout = uint.MaxValue;
							}
							else
							{
								waitForMultipleObjectsTimeout = (uint)Math.Max(ADP.TimerRemainingMilliseconds(pendingGetConnection.DueTime), 0L);
							}
							DbConnectionInternal dbConnectionInternal = null;
							bool flag2 = false;
							Exception ex = null;
							try
							{
								bool allowCreate = true;
								bool onlyOneCheckConnection = false;
								ADP.SetCurrentTransaction(pendingGetConnection.Completion.Task.AsyncState as Transaction);
								flag2 = !this.TryGetConnection(pendingGetConnection.Owner, waitForMultipleObjectsTimeout, allowCreate, onlyOneCheckConnection, pendingGetConnection.UserOptions, out dbConnectionInternal);
							}
							catch (Exception ex)
							{
							}
							if (ex != null)
							{
								pendingGetConnection.Completion.TrySetException(ex);
							}
							else if (flag2)
							{
								pendingGetConnection.Completion.TrySetException(ADP.ExceptionWithStackTrace(ADP.PooledOpenTimeout()));
							}
							else if (!pendingGetConnection.Completion.TrySetResult(dbConnectionInternal))
							{
								this.PutObject(dbConnectionInternal, pendingGetConnection.Owner);
							}
						}
					}
				}
				finally
				{
					if (flag)
					{
						Interlocked.Exchange(ref this._pendingOpensWaiting, 0);
					}
				}
			}
			while (this._pendingOpens.TryPeek(out pendingGetConnection));
		}

		// Token: 0x06001E1E RID: 7710 RVA: 0x00090E1C File Offset: 0x0008F01C
		internal bool TryGetConnection(DbConnection owningObject, TaskCompletionSource<DbConnectionInternal> retry, DbConnectionOptions userOptions, out DbConnectionInternal connection)
		{
			uint num = 0U;
			bool allowCreate = false;
			if (retry == null)
			{
				num = (uint)this.CreationTimeout;
				if (num == 0U)
				{
					num = uint.MaxValue;
				}
				allowCreate = true;
			}
			if (this._state != DbConnectionPool.State.Running)
			{
				connection = null;
				return true;
			}
			bool onlyOneCheckConnection = true;
			if (this.TryGetConnection(owningObject, num, allowCreate, onlyOneCheckConnection, userOptions, out connection))
			{
				return true;
			}
			if (retry == null)
			{
				return true;
			}
			DbConnectionPool.PendingGetConnection item = new DbConnectionPool.PendingGetConnection((this.CreationTimeout == 0) ? -1L : (ADP.TimerCurrent() + ADP.TimerFromSeconds(this.CreationTimeout / 1000)), owningObject, retry, userOptions);
			this._pendingOpens.Enqueue(item);
			if (this._pendingOpensWaiting == 0)
			{
				new Thread(new ThreadStart(this.WaitForPendingOpen))
				{
					IsBackground = true
				}.Start();
			}
			connection = null;
			return false;
		}

		// Token: 0x06001E1F RID: 7711 RVA: 0x00090EC8 File Offset: 0x0008F0C8
		private bool TryGetConnection(DbConnection owningObject, uint waitForMultipleObjectsTimeout, bool allowCreate, bool onlyOneCheckConnection, DbConnectionOptions userOptions, out DbConnectionInternal connection)
		{
			DbConnectionInternal dbConnectionInternal = null;
			Transaction transaction = null;
			if (this.HasTransactionAffinity)
			{
				dbConnectionInternal = this.GetFromTransactedPool(out transaction);
			}
			if (dbConnectionInternal == null)
			{
				Interlocked.Increment(ref this._waitCount);
				for (;;)
				{
					int num = 3;
					try
					{
						try
						{
						}
						finally
						{
							num = WaitHandle.WaitAny(this._waitHandles.GetHandles(allowCreate), (int)waitForMultipleObjectsTimeout);
						}
						switch (num)
						{
						case 0:
							Interlocked.Decrement(ref this._waitCount);
							dbConnectionInternal = this.GetFromGeneralPool();
							if (dbConnectionInternal != null && !dbConnectionInternal.IsConnectionAlive(false))
							{
								this.DestroyObject(dbConnectionInternal);
								dbConnectionInternal = null;
								if (onlyOneCheckConnection)
								{
									if (this._waitHandles.CreationSemaphore.WaitOne((int)waitForMultipleObjectsTimeout))
									{
										try
										{
											dbConnectionInternal = this.UserCreateRequest(owningObject, userOptions, null);
											break;
										}
										finally
										{
											this._waitHandles.CreationSemaphore.Release(1);
										}
									}
									connection = null;
									return false;
								}
							}
							break;
						case 1:
							Interlocked.Decrement(ref this._waitCount);
							throw this.TryCloneCachedException();
						case 2:
							try
							{
								dbConnectionInternal = this.UserCreateRequest(owningObject, userOptions, null);
							}
							catch
							{
								if (dbConnectionInternal == null)
								{
									Interlocked.Decrement(ref this._waitCount);
								}
								throw;
							}
							finally
							{
								if (dbConnectionInternal != null)
								{
									Interlocked.Decrement(ref this._waitCount);
								}
							}
							if (dbConnectionInternal == null && this.Count >= this.MaxPoolSize && this.MaxPoolSize != 0 && !this.ReclaimEmancipatedObjects())
							{
								allowCreate = false;
							}
							break;
						default:
							if (num == 258)
							{
								Interlocked.Decrement(ref this._waitCount);
								connection = null;
								return false;
							}
							Interlocked.Decrement(ref this._waitCount);
							throw ADP.InternalError(ADP.InternalErrorCode.UnexpectedWaitAnyResult);
						}
					}
					finally
					{
						if (2 == num)
						{
							this._waitHandles.CreationSemaphore.Release(1);
						}
					}
					if (dbConnectionInternal != null)
					{
						goto IL_185;
					}
				}
				bool result;
				return result;
			}
			IL_185:
			if (dbConnectionInternal != null)
			{
				this.PrepareConnection(owningObject, dbConnectionInternal, transaction);
			}
			connection = dbConnectionInternal;
			return true;
		}

		// Token: 0x06001E20 RID: 7712 RVA: 0x000910EC File Offset: 0x0008F2EC
		private void PrepareConnection(DbConnection owningObject, DbConnectionInternal obj, Transaction transaction)
		{
			lock (obj)
			{
				obj.PostPop(owningObject);
			}
			try
			{
				obj.ActivateConnection(transaction);
			}
			catch
			{
				this.PutObject(obj, owningObject);
				throw;
			}
		}

		// Token: 0x06001E21 RID: 7713 RVA: 0x0009114C File Offset: 0x0008F34C
		internal DbConnectionInternal ReplaceConnection(DbConnection owningObject, DbConnectionOptions userOptions, DbConnectionInternal oldConnection)
		{
			DbConnectionInternal dbConnectionInternal = this.UserCreateRequest(owningObject, userOptions, oldConnection);
			if (dbConnectionInternal != null)
			{
				this.PrepareConnection(owningObject, dbConnectionInternal, oldConnection.EnlistedTransaction);
				oldConnection.PrepareForReplaceConnection();
				oldConnection.DeactivateConnection();
				oldConnection.Dispose();
			}
			return dbConnectionInternal;
		}

		// Token: 0x06001E22 RID: 7714 RVA: 0x00091188 File Offset: 0x0008F388
		private DbConnectionInternal GetFromGeneralPool()
		{
			DbConnectionInternal result = null;
			if (!this._stackNew.TryPop(out result) && !this._stackOld.TryPop(out result))
			{
				result = null;
			}
			return result;
		}

		// Token: 0x06001E23 RID: 7715 RVA: 0x000911BC File Offset: 0x0008F3BC
		private DbConnectionInternal GetFromTransactedPool(out Transaction transaction)
		{
			transaction = ADP.GetCurrentTransaction();
			DbConnectionInternal dbConnectionInternal = null;
			if (null != transaction && this._transactedConnectionPool != null)
			{
				dbConnectionInternal = this._transactedConnectionPool.GetTransactedObject(transaction);
				if (dbConnectionInternal != null)
				{
					if (dbConnectionInternal.IsTransactionRoot)
					{
						try
						{
							dbConnectionInternal.IsConnectionAlive(true);
							return dbConnectionInternal;
						}
						catch
						{
							this.DestroyObject(dbConnectionInternal);
							throw;
						}
					}
					if (!dbConnectionInternal.IsConnectionAlive(false))
					{
						this.DestroyObject(dbConnectionInternal);
						dbConnectionInternal = null;
					}
				}
			}
			return dbConnectionInternal;
		}

		// Token: 0x06001E24 RID: 7716 RVA: 0x00091234 File Offset: 0x0008F434
		private void PoolCreateRequest(object state)
		{
			if (DbConnectionPool.State.Running == this._state)
			{
				if (!this._pendingOpens.IsEmpty && this._pendingOpensWaiting == 0)
				{
					new Thread(new ThreadStart(this.WaitForPendingOpen))
					{
						IsBackground = true
					}.Start();
				}
				this.ReclaimEmancipatedObjects();
				if (!this.ErrorOccurred && this.NeedToReplenish)
				{
					if (this.UsingIntegrateSecurity && !this._identity.Equals(DbConnectionPoolIdentity.GetCurrent()))
					{
						return;
					}
					int num = 3;
					try
					{
						try
						{
						}
						finally
						{
							num = WaitHandle.WaitAny(this._waitHandles.GetHandles(true), this.CreationTimeout);
						}
						if (2 == num)
						{
							if (!this.ErrorOccurred)
							{
								while (this.NeedToReplenish)
								{
									DbConnectionInternal dbConnectionInternal = this.CreateObject(null, null, null);
									if (dbConnectionInternal == null)
									{
										break;
									}
									this.PutNewObject(dbConnectionInternal);
								}
							}
						}
						else if (258 == num)
						{
							this.QueuePoolCreateRequest();
						}
					}
					finally
					{
						if (2 == num)
						{
							this._waitHandles.CreationSemaphore.Release(1);
						}
					}
				}
			}
		}

		// Token: 0x06001E25 RID: 7717 RVA: 0x00091344 File Offset: 0x0008F544
		internal void PutNewObject(DbConnectionInternal obj)
		{
			this._stackNew.Push(obj);
			this._waitHandles.PoolSemaphore.Release(1);
		}

		// Token: 0x06001E26 RID: 7718 RVA: 0x00091364 File Offset: 0x0008F564
		internal void PutObject(DbConnectionInternal obj, object owningObject)
		{
			lock (obj)
			{
				obj.PrePush(owningObject);
			}
			this.DeactivateObject(obj);
		}

		// Token: 0x06001E27 RID: 7719 RVA: 0x000913A8 File Offset: 0x0008F5A8
		internal void PutObjectFromTransactedPool(DbConnectionInternal obj)
		{
			if (this._state == DbConnectionPool.State.Running && obj.CanBePooled)
			{
				this.PutNewObject(obj);
				return;
			}
			this.DestroyObject(obj);
			this.QueuePoolCreateRequest();
		}

		// Token: 0x06001E28 RID: 7720 RVA: 0x000913D0 File Offset: 0x0008F5D0
		private void QueuePoolCreateRequest()
		{
			if (DbConnectionPool.State.Running == this._state)
			{
				ThreadPool.QueueUserWorkItem(this._poolCreateRequest);
			}
		}

		// Token: 0x06001E29 RID: 7721 RVA: 0x000913E8 File Offset: 0x0008F5E8
		private bool ReclaimEmancipatedObjects()
		{
			bool result = false;
			List<DbConnectionInternal> list = new List<DbConnectionInternal>();
			List<DbConnectionInternal> objectList = this._objectList;
			int count;
			lock (objectList)
			{
				count = this._objectList.Count;
				for (int i = 0; i < count; i++)
				{
					DbConnectionInternal dbConnectionInternal = this._objectList[i];
					if (dbConnectionInternal != null)
					{
						bool flag2 = false;
						try
						{
							Monitor.TryEnter(dbConnectionInternal, ref flag2);
							if (flag2 && dbConnectionInternal.IsEmancipated)
							{
								dbConnectionInternal.PrePush(null);
								list.Add(dbConnectionInternal);
							}
						}
						finally
						{
							if (flag2)
							{
								Monitor.Exit(dbConnectionInternal);
							}
						}
					}
				}
			}
			count = list.Count;
			for (int j = 0; j < count; j++)
			{
				DbConnectionInternal dbConnectionInternal2 = list[j];
				result = true;
				dbConnectionInternal2.DetachCurrentTransactionIfEnded();
				this.DeactivateObject(dbConnectionInternal2);
			}
			return result;
		}

		// Token: 0x06001E2A RID: 7722 RVA: 0x000914D4 File Offset: 0x0008F6D4
		internal void Startup()
		{
			this._cleanupTimer = this.CreateCleanupTimer();
			if (this.NeedToReplenish)
			{
				this.QueuePoolCreateRequest();
			}
		}

		// Token: 0x06001E2B RID: 7723 RVA: 0x000914F0 File Offset: 0x0008F6F0
		internal void Shutdown()
		{
			this._state = DbConnectionPool.State.ShuttingDown;
			Timer cleanupTimer = this._cleanupTimer;
			this._cleanupTimer = null;
			if (cleanupTimer != null)
			{
				cleanupTimer.Dispose();
			}
		}

		// Token: 0x06001E2C RID: 7724 RVA: 0x0009151C File Offset: 0x0008F71C
		internal void TransactionEnded(Transaction transaction, DbConnectionInternal transactedObject)
		{
			DbConnectionPool.TransactedConnectionPool transactedConnectionPool = this._transactedConnectionPool;
			if (transactedConnectionPool != null)
			{
				transactedConnectionPool.TransactionEnded(transaction, transactedObject);
			}
		}

		// Token: 0x06001E2D RID: 7725 RVA: 0x0009153C File Offset: 0x0008F73C
		private DbConnectionInternal UserCreateRequest(DbConnection owningObject, DbConnectionOptions userOptions, DbConnectionInternal oldConnection = null)
		{
			DbConnectionInternal result = null;
			if (this.ErrorOccurred)
			{
				throw this.TryCloneCachedException();
			}
			if ((oldConnection != null || this.Count < this.MaxPoolSize || this.MaxPoolSize == 0) && (oldConnection != null || (this.Count & 1) == 1 || !this.ReclaimEmancipatedObjects()))
			{
				result = this.CreateObject(owningObject, userOptions, oldConnection);
			}
			return result;
		}

		// Token: 0x06001E2E RID: 7726 RVA: 0x00091594 File Offset: 0x0008F794
		// Note: this type is marked as 'beforefieldinit'.
		static DbConnectionPool()
		{
		}

		// Token: 0x04001411 RID: 5137
		private const int MAX_Q_SIZE = 1048576;

		// Token: 0x04001412 RID: 5138
		private const int SEMAPHORE_HANDLE = 0;

		// Token: 0x04001413 RID: 5139
		private const int ERROR_HANDLE = 1;

		// Token: 0x04001414 RID: 5140
		private const int CREATION_HANDLE = 2;

		// Token: 0x04001415 RID: 5141
		private const int BOGUS_HANDLE = 3;

		// Token: 0x04001416 RID: 5142
		private const int ERROR_WAIT_DEFAULT = 5000;

		// Token: 0x04001417 RID: 5143
		private static readonly Random s_random = new Random(5101977);

		// Token: 0x04001418 RID: 5144
		private readonly int _cleanupWait;

		// Token: 0x04001419 RID: 5145
		private readonly DbConnectionPoolIdentity _identity;

		// Token: 0x0400141A RID: 5146
		private readonly DbConnectionFactory _connectionFactory;

		// Token: 0x0400141B RID: 5147
		private readonly DbConnectionPoolGroup _connectionPoolGroup;

		// Token: 0x0400141C RID: 5148
		private readonly DbConnectionPoolGroupOptions _connectionPoolGroupOptions;

		// Token: 0x0400141D RID: 5149
		private DbConnectionPoolProviderInfo _connectionPoolProviderInfo;

		// Token: 0x0400141E RID: 5150
		private DbConnectionPool.State _state;

		// Token: 0x0400141F RID: 5151
		private readonly ConcurrentStack<DbConnectionInternal> _stackOld = new ConcurrentStack<DbConnectionInternal>();

		// Token: 0x04001420 RID: 5152
		private readonly ConcurrentStack<DbConnectionInternal> _stackNew = new ConcurrentStack<DbConnectionInternal>();

		// Token: 0x04001421 RID: 5153
		private readonly ConcurrentQueue<DbConnectionPool.PendingGetConnection> _pendingOpens = new ConcurrentQueue<DbConnectionPool.PendingGetConnection>();

		// Token: 0x04001422 RID: 5154
		private int _pendingOpensWaiting;

		// Token: 0x04001423 RID: 5155
		private readonly WaitCallback _poolCreateRequest;

		// Token: 0x04001424 RID: 5156
		private int _waitCount;

		// Token: 0x04001425 RID: 5157
		private readonly DbConnectionPool.PoolWaitHandles _waitHandles;

		// Token: 0x04001426 RID: 5158
		private Exception _resError;

		// Token: 0x04001427 RID: 5159
		private volatile bool _errorOccurred;

		// Token: 0x04001428 RID: 5160
		private int _errorWait;

		// Token: 0x04001429 RID: 5161
		private Timer _errorTimer;

		// Token: 0x0400142A RID: 5162
		private Timer _cleanupTimer;

		// Token: 0x0400142B RID: 5163
		private readonly DbConnectionPool.TransactedConnectionPool _transactedConnectionPool;

		// Token: 0x0400142C RID: 5164
		private readonly List<DbConnectionInternal> _objectList;

		// Token: 0x0400142D RID: 5165
		private int _totalObjects;

		// Token: 0x0200027B RID: 635
		private enum State
		{
			// Token: 0x0400142F RID: 5167
			Initializing,
			// Token: 0x04001430 RID: 5168
			Running,
			// Token: 0x04001431 RID: 5169
			ShuttingDown
		}

		// Token: 0x0200027C RID: 636
		private sealed class TransactedConnectionList : List<DbConnectionInternal>
		{
			// Token: 0x06001E2F RID: 7727 RVA: 0x000915A5 File Offset: 0x0008F7A5
			internal TransactedConnectionList(int initialAllocation, Transaction tx) : base(initialAllocation)
			{
				this._transaction = tx;
			}

			// Token: 0x06001E30 RID: 7728 RVA: 0x000915B5 File Offset: 0x0008F7B5
			internal void Dispose()
			{
				if (null != this._transaction)
				{
					this._transaction.Dispose();
				}
			}

			// Token: 0x04001432 RID: 5170
			private Transaction _transaction;
		}

		// Token: 0x0200027D RID: 637
		private sealed class PendingGetConnection
		{
			// Token: 0x06001E31 RID: 7729 RVA: 0x000915D0 File Offset: 0x0008F7D0
			public PendingGetConnection(long dueTime, DbConnection owner, TaskCompletionSource<DbConnectionInternal> completion, DbConnectionOptions userOptions)
			{
				this.DueTime = dueTime;
				this.Owner = owner;
				this.Completion = completion;
			}

			// Token: 0x17000524 RID: 1316
			// (get) Token: 0x06001E32 RID: 7730 RVA: 0x000915ED File Offset: 0x0008F7ED
			// (set) Token: 0x06001E33 RID: 7731 RVA: 0x000915F5 File Offset: 0x0008F7F5
			public long DueTime
			{
				[CompilerGenerated]
				get
				{
					return this.<DueTime>k__BackingField;
				}
				[CompilerGenerated]
				private set
				{
					this.<DueTime>k__BackingField = value;
				}
			}

			// Token: 0x17000525 RID: 1317
			// (get) Token: 0x06001E34 RID: 7732 RVA: 0x000915FE File Offset: 0x0008F7FE
			// (set) Token: 0x06001E35 RID: 7733 RVA: 0x00091606 File Offset: 0x0008F806
			public DbConnection Owner
			{
				[CompilerGenerated]
				get
				{
					return this.<Owner>k__BackingField;
				}
				[CompilerGenerated]
				private set
				{
					this.<Owner>k__BackingField = value;
				}
			}

			// Token: 0x17000526 RID: 1318
			// (get) Token: 0x06001E36 RID: 7734 RVA: 0x0009160F File Offset: 0x0008F80F
			// (set) Token: 0x06001E37 RID: 7735 RVA: 0x00091617 File Offset: 0x0008F817
			public TaskCompletionSource<DbConnectionInternal> Completion
			{
				[CompilerGenerated]
				get
				{
					return this.<Completion>k__BackingField;
				}
				[CompilerGenerated]
				private set
				{
					this.<Completion>k__BackingField = value;
				}
			}

			// Token: 0x17000527 RID: 1319
			// (get) Token: 0x06001E38 RID: 7736 RVA: 0x00091620 File Offset: 0x0008F820
			// (set) Token: 0x06001E39 RID: 7737 RVA: 0x00091628 File Offset: 0x0008F828
			public DbConnectionOptions UserOptions
			{
				[CompilerGenerated]
				get
				{
					return this.<UserOptions>k__BackingField;
				}
				[CompilerGenerated]
				private set
				{
					this.<UserOptions>k__BackingField = value;
				}
			}

			// Token: 0x04001433 RID: 5171
			[CompilerGenerated]
			private long <DueTime>k__BackingField;

			// Token: 0x04001434 RID: 5172
			[CompilerGenerated]
			private DbConnection <Owner>k__BackingField;

			// Token: 0x04001435 RID: 5173
			[CompilerGenerated]
			private TaskCompletionSource<DbConnectionInternal> <Completion>k__BackingField;

			// Token: 0x04001436 RID: 5174
			[CompilerGenerated]
			private DbConnectionOptions <UserOptions>k__BackingField;
		}

		// Token: 0x0200027E RID: 638
		private sealed class TransactedConnectionPool
		{
			// Token: 0x06001E3A RID: 7738 RVA: 0x00091631 File Offset: 0x0008F831
			internal TransactedConnectionPool(DbConnectionPool pool)
			{
				this._pool = pool;
				this._transactedCxns = new Dictionary<Transaction, DbConnectionPool.TransactedConnectionList>();
			}

			// Token: 0x17000528 RID: 1320
			// (get) Token: 0x06001E3B RID: 7739 RVA: 0x0009165B File Offset: 0x0008F85B
			internal int ObjectID
			{
				get
				{
					return this._objectID;
				}
			}

			// Token: 0x17000529 RID: 1321
			// (get) Token: 0x06001E3C RID: 7740 RVA: 0x00091663 File Offset: 0x0008F863
			internal DbConnectionPool Pool
			{
				get
				{
					return this._pool;
				}
			}

			// Token: 0x06001E3D RID: 7741 RVA: 0x0009166C File Offset: 0x0008F86C
			internal DbConnectionInternal GetTransactedObject(Transaction transaction)
			{
				DbConnectionInternal result = null;
				bool flag = false;
				Dictionary<Transaction, DbConnectionPool.TransactedConnectionList> transactedCxns = this._transactedCxns;
				DbConnectionPool.TransactedConnectionList transactedConnectionList;
				lock (transactedCxns)
				{
					flag = this._transactedCxns.TryGetValue(transaction, out transactedConnectionList);
				}
				if (flag)
				{
					DbConnectionPool.TransactedConnectionList obj = transactedConnectionList;
					lock (obj)
					{
						int num = transactedConnectionList.Count - 1;
						if (0 <= num)
						{
							result = transactedConnectionList[num];
							transactedConnectionList.RemoveAt(num);
						}
					}
				}
				return result;
			}

			// Token: 0x06001E3E RID: 7742 RVA: 0x00091708 File Offset: 0x0008F908
			internal void PutTransactedObject(Transaction transaction, DbConnectionInternal transactedObject)
			{
				bool flag = false;
				Dictionary<Transaction, DbConnectionPool.TransactedConnectionList> transactedCxns = this._transactedCxns;
				lock (transactedCxns)
				{
					DbConnectionPool.TransactedConnectionList transactedConnectionList;
					if (flag = this._transactedCxns.TryGetValue(transaction, out transactedConnectionList))
					{
						DbConnectionPool.TransactedConnectionList obj = transactedConnectionList;
						lock (obj)
						{
							transactedConnectionList.Add(transactedObject);
						}
					}
				}
				if (!flag)
				{
					Transaction transaction2 = null;
					DbConnectionPool.TransactedConnectionList transactedConnectionList2 = null;
					try
					{
						transaction2 = transaction.Clone();
						transactedConnectionList2 = new DbConnectionPool.TransactedConnectionList(2, transaction2);
						transactedCxns = this._transactedCxns;
						lock (transactedCxns)
						{
							DbConnectionPool.TransactedConnectionList transactedConnectionList;
							if (flag = this._transactedCxns.TryGetValue(transaction, out transactedConnectionList))
							{
								DbConnectionPool.TransactedConnectionList obj = transactedConnectionList;
								lock (obj)
								{
									transactedConnectionList.Add(transactedObject);
									return;
								}
							}
							transactedConnectionList2.Add(transactedObject);
							this._transactedCxns.Add(transaction2, transactedConnectionList2);
							transaction2 = null;
						}
					}
					finally
					{
						if (null != transaction2)
						{
							if (transactedConnectionList2 != null)
							{
								transactedConnectionList2.Dispose();
							}
							else
							{
								transaction2.Dispose();
							}
						}
					}
				}
			}

			// Token: 0x06001E3F RID: 7743 RVA: 0x00091854 File Offset: 0x0008FA54
			internal void TransactionEnded(Transaction transaction, DbConnectionInternal transactedObject)
			{
				int num = -1;
				Dictionary<Transaction, DbConnectionPool.TransactedConnectionList> transactedCxns = this._transactedCxns;
				lock (transactedCxns)
				{
					DbConnectionPool.TransactedConnectionList transactedConnectionList;
					if (this._transactedCxns.TryGetValue(transaction, out transactedConnectionList))
					{
						bool flag2 = false;
						DbConnectionPool.TransactedConnectionList obj = transactedConnectionList;
						lock (obj)
						{
							num = transactedConnectionList.IndexOf(transactedObject);
							if (num >= 0)
							{
								transactedConnectionList.RemoveAt(num);
							}
							if (0 >= transactedConnectionList.Count)
							{
								this._transactedCxns.Remove(transaction);
								flag2 = true;
							}
						}
						if (flag2)
						{
							transactedConnectionList.Dispose();
						}
					}
				}
				if (0 <= num)
				{
					this.Pool.PutObjectFromTransactedPool(transactedObject);
				}
			}

			// Token: 0x04001437 RID: 5175
			private Dictionary<Transaction, DbConnectionPool.TransactedConnectionList> _transactedCxns;

			// Token: 0x04001438 RID: 5176
			private DbConnectionPool _pool;

			// Token: 0x04001439 RID: 5177
			private static int _objectTypeCount;

			// Token: 0x0400143A RID: 5178
			internal readonly int _objectID = Interlocked.Increment(ref DbConnectionPool.TransactedConnectionPool._objectTypeCount);
		}

		// Token: 0x0200027F RID: 639
		private sealed class PoolWaitHandles
		{
			// Token: 0x06001E40 RID: 7744 RVA: 0x00091914 File Offset: 0x0008FB14
			internal PoolWaitHandles()
			{
				this._poolSemaphore = new Semaphore(0, 1048576);
				this._errorEvent = new ManualResetEvent(false);
				this._creationSemaphore = new Semaphore(1, 1);
				this._handlesWithCreate = new WaitHandle[]
				{
					this._poolSemaphore,
					this._errorEvent,
					this._creationSemaphore
				};
				this._handlesWithoutCreate = new WaitHandle[]
				{
					this._poolSemaphore,
					this._errorEvent
				};
			}

			// Token: 0x1700052A RID: 1322
			// (get) Token: 0x06001E41 RID: 7745 RVA: 0x00091996 File Offset: 0x0008FB96
			internal Semaphore CreationSemaphore
			{
				get
				{
					return this._creationSemaphore;
				}
			}

			// Token: 0x1700052B RID: 1323
			// (get) Token: 0x06001E42 RID: 7746 RVA: 0x0009199E File Offset: 0x0008FB9E
			internal ManualResetEvent ErrorEvent
			{
				get
				{
					return this._errorEvent;
				}
			}

			// Token: 0x1700052C RID: 1324
			// (get) Token: 0x06001E43 RID: 7747 RVA: 0x000919A6 File Offset: 0x0008FBA6
			internal Semaphore PoolSemaphore
			{
				get
				{
					return this._poolSemaphore;
				}
			}

			// Token: 0x06001E44 RID: 7748 RVA: 0x000919AE File Offset: 0x0008FBAE
			internal WaitHandle[] GetHandles(bool withCreate)
			{
				if (!withCreate)
				{
					return this._handlesWithoutCreate;
				}
				return this._handlesWithCreate;
			}

			// Token: 0x0400143B RID: 5179
			private readonly Semaphore _poolSemaphore;

			// Token: 0x0400143C RID: 5180
			private readonly ManualResetEvent _errorEvent;

			// Token: 0x0400143D RID: 5181
			private readonly Semaphore _creationSemaphore;

			// Token: 0x0400143E RID: 5182
			private readonly WaitHandle[] _handlesWithCreate;

			// Token: 0x0400143F RID: 5183
			private readonly WaitHandle[] _handlesWithoutCreate;
		}
	}
}
