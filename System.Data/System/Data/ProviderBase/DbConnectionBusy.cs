﻿using System;
using System.Data.Common;
using System.Threading.Tasks;

namespace System.Data.ProviderBase
{
	// Token: 0x02000270 RID: 624
	internal abstract class DbConnectionBusy : DbConnectionClosed
	{
		// Token: 0x06001D93 RID: 7571 RVA: 0x0008F31D File Offset: 0x0008D51D
		protected DbConnectionBusy(ConnectionState state) : base(state, true, false)
		{
		}

		// Token: 0x06001D94 RID: 7572 RVA: 0x0008F328 File Offset: 0x0008D528
		internal override bool TryOpenConnection(DbConnection outerConnection, DbConnectionFactory connectionFactory, TaskCompletionSource<DbConnectionInternal> retry, DbConnectionOptions userOptions)
		{
			throw ADP.ConnectionAlreadyOpen(base.State);
		}
	}
}
