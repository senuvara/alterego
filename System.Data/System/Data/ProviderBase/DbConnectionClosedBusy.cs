﻿using System;

namespace System.Data.ProviderBase
{
	// Token: 0x02000271 RID: 625
	internal sealed class DbConnectionClosedBusy : DbConnectionBusy
	{
		// Token: 0x06001D95 RID: 7573 RVA: 0x0008F335 File Offset: 0x0008D535
		private DbConnectionClosedBusy() : base(ConnectionState.Closed)
		{
		}

		// Token: 0x06001D96 RID: 7574 RVA: 0x0008F33E File Offset: 0x0008D53E
		// Note: this type is marked as 'beforefieldinit'.
		static DbConnectionClosedBusy()
		{
		}

		// Token: 0x040013EB RID: 5099
		internal static readonly DbConnectionInternal SingletonInstance = new DbConnectionClosedBusy();
	}
}
