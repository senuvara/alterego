﻿using System;
using System.Data.Common;
using System.Threading.Tasks;

namespace System.Data.ProviderBase
{
	// Token: 0x02000275 RID: 629
	internal sealed class DbConnectionClosedPreviouslyOpened : DbConnectionClosed
	{
		// Token: 0x06001DA0 RID: 7584 RVA: 0x0008F3F7 File Offset: 0x0008D5F7
		private DbConnectionClosedPreviouslyOpened() : base(ConnectionState.Closed, true, true)
		{
		}

		// Token: 0x06001DA1 RID: 7585 RVA: 0x0008F376 File Offset: 0x0008D576
		internal override bool TryReplaceConnection(DbConnection outerConnection, DbConnectionFactory connectionFactory, TaskCompletionSource<DbConnectionInternal> retry, DbConnectionOptions userOptions)
		{
			return this.TryOpenConnection(outerConnection, connectionFactory, retry, userOptions);
		}

		// Token: 0x06001DA2 RID: 7586 RVA: 0x0008F402 File Offset: 0x0008D602
		// Note: this type is marked as 'beforefieldinit'.
		static DbConnectionClosedPreviouslyOpened()
		{
		}

		// Token: 0x040013EF RID: 5103
		internal static readonly DbConnectionInternal SingletonInstance = new DbConnectionClosedPreviouslyOpened();
	}
}
