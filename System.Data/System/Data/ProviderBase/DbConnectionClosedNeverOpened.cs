﻿using System;

namespace System.Data.ProviderBase
{
	// Token: 0x02000274 RID: 628
	internal sealed class DbConnectionClosedNeverOpened : DbConnectionClosed
	{
		// Token: 0x06001D9E RID: 7582 RVA: 0x0008F3E0 File Offset: 0x0008D5E0
		private DbConnectionClosedNeverOpened() : base(ConnectionState.Closed, false, true)
		{
		}

		// Token: 0x06001D9F RID: 7583 RVA: 0x0008F3EB File Offset: 0x0008D5EB
		// Note: this type is marked as 'beforefieldinit'.
		static DbConnectionClosedNeverOpened()
		{
		}

		// Token: 0x040013EE RID: 5102
		internal static readonly DbConnectionInternal SingletonInstance = new DbConnectionClosedNeverOpened();
	}
}
