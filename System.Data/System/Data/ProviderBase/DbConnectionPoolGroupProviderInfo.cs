﻿using System;

namespace System.Data.ProviderBase
{
	// Token: 0x02000281 RID: 641
	internal class DbConnectionPoolGroupProviderInfo
	{
		// Token: 0x17000533 RID: 1331
		// (get) Token: 0x06001E52 RID: 7762 RVA: 0x00091D18 File Offset: 0x0008FF18
		// (set) Token: 0x06001E53 RID: 7763 RVA: 0x00091D20 File Offset: 0x0008FF20
		internal DbConnectionPoolGroup PoolGroup
		{
			get
			{
				return this._poolGroup;
			}
			set
			{
				this._poolGroup = value;
			}
		}

		// Token: 0x06001E54 RID: 7764 RVA: 0x00005C14 File Offset: 0x00003E14
		public DbConnectionPoolGroupProviderInfo()
		{
		}

		// Token: 0x0400144A RID: 5194
		private DbConnectionPoolGroup _poolGroup;
	}
}
