﻿using System;
using System.Data.Common;

namespace System.Data.ProviderBase
{
	// Token: 0x02000288 RID: 648
	internal class TimeoutTimer
	{
		// Token: 0x06001E85 RID: 7813 RVA: 0x00092D06 File Offset: 0x00090F06
		internal static TimeoutTimer StartSecondsTimeout(int seconds)
		{
			TimeoutTimer timeoutTimer = new TimeoutTimer();
			timeoutTimer.SetTimeoutSeconds(seconds);
			return timeoutTimer;
		}

		// Token: 0x06001E86 RID: 7814 RVA: 0x00092D14 File Offset: 0x00090F14
		internal static TimeoutTimer StartMillisecondsTimeout(long milliseconds)
		{
			return new TimeoutTimer
			{
				_timerExpire = checked(ADP.TimerCurrent() + milliseconds * 10000L),
				_isInfiniteTimeout = false
			};
		}

		// Token: 0x06001E87 RID: 7815 RVA: 0x00092D36 File Offset: 0x00090F36
		internal void SetTimeoutSeconds(int seconds)
		{
			if (TimeoutTimer.InfiniteTimeout == (long)seconds)
			{
				this._isInfiniteTimeout = true;
				return;
			}
			this._timerExpire = checked(ADP.TimerCurrent() + ADP.TimerFromSeconds(seconds));
			this._isInfiniteTimeout = false;
		}

		// Token: 0x17000542 RID: 1346
		// (get) Token: 0x06001E88 RID: 7816 RVA: 0x00092D62 File Offset: 0x00090F62
		internal bool IsExpired
		{
			get
			{
				return !this.IsInfinite && ADP.TimerHasExpired(this._timerExpire);
			}
		}

		// Token: 0x17000543 RID: 1347
		// (get) Token: 0x06001E89 RID: 7817 RVA: 0x00092D79 File Offset: 0x00090F79
		internal bool IsInfinite
		{
			get
			{
				return this._isInfiniteTimeout;
			}
		}

		// Token: 0x17000544 RID: 1348
		// (get) Token: 0x06001E8A RID: 7818 RVA: 0x00092D81 File Offset: 0x00090F81
		internal long LegacyTimerExpire
		{
			get
			{
				if (!this._isInfiniteTimeout)
				{
					return this._timerExpire;
				}
				return long.MaxValue;
			}
		}

		// Token: 0x17000545 RID: 1349
		// (get) Token: 0x06001E8B RID: 7819 RVA: 0x00092D9C File Offset: 0x00090F9C
		internal long MillisecondsRemaining
		{
			get
			{
				long num;
				if (this._isInfiniteTimeout)
				{
					num = long.MaxValue;
				}
				else
				{
					num = ADP.TimerRemainingMilliseconds(this._timerExpire);
					if (0L > num)
					{
						num = 0L;
					}
				}
				return num;
			}
		}

		// Token: 0x06001E8C RID: 7820 RVA: 0x00005C14 File Offset: 0x00003E14
		public TimeoutTimer()
		{
		}

		// Token: 0x06001E8D RID: 7821 RVA: 0x00005E03 File Offset: 0x00004003
		// Note: this type is marked as 'beforefieldinit'.
		static TimeoutTimer()
		{
		}

		// Token: 0x04001472 RID: 5234
		private long _timerExpire;

		// Token: 0x04001473 RID: 5235
		private bool _isInfiniteTimeout;

		// Token: 0x04001474 RID: 5236
		internal static readonly long InfiniteTimeout;
	}
}
