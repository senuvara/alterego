﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Common;
using System.Globalization;

namespace System.Data.ProviderBase
{
	// Token: 0x02000268 RID: 616
	internal class BasicFieldNameLookup
	{
		// Token: 0x06001D1E RID: 7454 RVA: 0x0008CC85 File Offset: 0x0008AE85
		public BasicFieldNameLookup(string[] fieldNames)
		{
			if (fieldNames == null)
			{
				throw ADP.ArgumentNull("fieldNames");
			}
			this._fieldNames = fieldNames;
		}

		// Token: 0x06001D1F RID: 7455 RVA: 0x0008CCA4 File Offset: 0x0008AEA4
		public BasicFieldNameLookup(ReadOnlyCollection<string> columnNames)
		{
			int count = columnNames.Count;
			string[] array = new string[count];
			for (int i = 0; i < count; i++)
			{
				array[i] = columnNames[i];
			}
			this._fieldNames = array;
			this.GenerateLookup();
		}

		// Token: 0x06001D20 RID: 7456 RVA: 0x0008CCE8 File Offset: 0x0008AEE8
		public BasicFieldNameLookup(IDataReader reader)
		{
			int fieldCount = reader.FieldCount;
			string[] array = new string[fieldCount];
			for (int i = 0; i < fieldCount; i++)
			{
				array[i] = reader.GetName(i);
			}
			this._fieldNames = array;
		}

		// Token: 0x06001D21 RID: 7457 RVA: 0x0008CD28 File Offset: 0x0008AF28
		public int GetOrdinal(string fieldName)
		{
			if (fieldName == null)
			{
				throw ADP.ArgumentNull("fieldName");
			}
			int num = this.IndexOf(fieldName);
			if (-1 == num)
			{
				throw ADP.IndexOutOfRange(fieldName);
			}
			return num;
		}

		// Token: 0x06001D22 RID: 7458 RVA: 0x0008CD58 File Offset: 0x0008AF58
		public int IndexOfName(string fieldName)
		{
			if (this._fieldNameLookup == null)
			{
				this.GenerateLookup();
			}
			int result;
			if (!this._fieldNameLookup.TryGetValue(fieldName, out result))
			{
				return -1;
			}
			return result;
		}

		// Token: 0x06001D23 RID: 7459 RVA: 0x0008CD88 File Offset: 0x0008AF88
		public int IndexOf(string fieldName)
		{
			if (this._fieldNameLookup == null)
			{
				this.GenerateLookup();
			}
			int num;
			if (!this._fieldNameLookup.TryGetValue(fieldName, out num))
			{
				num = this.LinearIndexOf(fieldName, CompareOptions.IgnoreCase);
				if (-1 == num)
				{
					num = this.LinearIndexOf(fieldName, CompareOptions.IgnoreCase | CompareOptions.IgnoreKanaType | CompareOptions.IgnoreWidth);
				}
			}
			return num;
		}

		// Token: 0x06001D24 RID: 7460 RVA: 0x0008CDCB File Offset: 0x0008AFCB
		protected virtual CompareInfo GetCompareInfo()
		{
			return CultureInfo.InvariantCulture.CompareInfo;
		}

		// Token: 0x06001D25 RID: 7461 RVA: 0x0008CDD8 File Offset: 0x0008AFD8
		private int LinearIndexOf(string fieldName, CompareOptions compareOptions)
		{
			if (this._compareInfo == null)
			{
				this._compareInfo = this.GetCompareInfo();
			}
			int num = this._fieldNames.Length;
			for (int i = 0; i < num; i++)
			{
				if (this._compareInfo.Compare(fieldName, this._fieldNames[i], compareOptions) == 0)
				{
					this._fieldNameLookup[fieldName] = i;
					return i;
				}
			}
			return -1;
		}

		// Token: 0x06001D26 RID: 7462 RVA: 0x0008CE38 File Offset: 0x0008B038
		private void GenerateLookup()
		{
			int num = this._fieldNames.Length;
			Dictionary<string, int> dictionary = new Dictionary<string, int>(num);
			int num2 = num - 1;
			while (0 <= num2)
			{
				string key = this._fieldNames[num2];
				dictionary[key] = num2;
				num2--;
			}
			this._fieldNameLookup = dictionary;
		}

		// Token: 0x040013CD RID: 5069
		private Dictionary<string, int> _fieldNameLookup;

		// Token: 0x040013CE RID: 5070
		private readonly string[] _fieldNames;

		// Token: 0x040013CF RID: 5071
		private CompareInfo _compareInfo;
	}
}
