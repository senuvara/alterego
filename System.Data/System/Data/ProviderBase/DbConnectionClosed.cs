﻿using System;
using System.Data.Common;
using System.Threading.Tasks;
using System.Transactions;

namespace System.Data.ProviderBase
{
	// Token: 0x0200026F RID: 623
	internal abstract class DbConnectionClosed : DbConnectionInternal
	{
		// Token: 0x06001D89 RID: 7561 RVA: 0x0008F30B File Offset: 0x0008D50B
		protected DbConnectionClosed(ConnectionState state, bool hidePassword, bool allowSetConnectionString) : base(state, hidePassword, allowSetConnectionString)
		{
		}

		// Token: 0x170004FF RID: 1279
		// (get) Token: 0x06001D8A RID: 7562 RVA: 0x0008F316 File Offset: 0x0008D516
		public override string ServerVersion
		{
			get
			{
				throw ADP.ClosedConnectionError();
			}
		}

		// Token: 0x06001D8B RID: 7563 RVA: 0x0008F316 File Offset: 0x0008D516
		protected override void Activate(Transaction transaction)
		{
			throw ADP.ClosedConnectionError();
		}

		// Token: 0x06001D8C RID: 7564 RVA: 0x0008F316 File Offset: 0x0008D516
		public override DbTransaction BeginTransaction(IsolationLevel il)
		{
			throw ADP.ClosedConnectionError();
		}

		// Token: 0x06001D8D RID: 7565 RVA: 0x0008F316 File Offset: 0x0008D516
		public override void ChangeDatabase(string database)
		{
			throw ADP.ClosedConnectionError();
		}

		// Token: 0x06001D8E RID: 7566 RVA: 0x00005E03 File Offset: 0x00004003
		internal override void CloseConnection(DbConnection owningObject, DbConnectionFactory connectionFactory)
		{
		}

		// Token: 0x06001D8F RID: 7567 RVA: 0x0008F316 File Offset: 0x0008D516
		protected override void Deactivate()
		{
			throw ADP.ClosedConnectionError();
		}

		// Token: 0x06001D90 RID: 7568 RVA: 0x0008F316 File Offset: 0x0008D516
		public override void EnlistTransaction(Transaction transaction)
		{
			throw ADP.ClosedConnectionError();
		}

		// Token: 0x06001D91 RID: 7569 RVA: 0x0008F316 File Offset: 0x0008D516
		protected override DbReferenceCollection CreateReferenceCollection()
		{
			throw ADP.ClosedConnectionError();
		}

		// Token: 0x06001D92 RID: 7570 RVA: 0x000668E9 File Offset: 0x00064AE9
		internal override bool TryOpenConnection(DbConnection outerConnection, DbConnectionFactory connectionFactory, TaskCompletionSource<DbConnectionInternal> retry, DbConnectionOptions userOptions)
		{
			return base.TryOpenConnectionInternal(outerConnection, connectionFactory, retry, userOptions);
		}
	}
}
