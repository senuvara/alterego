﻿using System;
using System.Data.Common;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;

namespace System.Data.ProviderBase
{
	// Token: 0x02000279 RID: 633
	internal abstract class DbConnectionInternal
	{
		// Token: 0x06001DC7 RID: 7623 RVA: 0x0008FDEB File Offset: 0x0008DFEB
		protected DbConnectionInternal() : this(ConnectionState.Open, true, false)
		{
		}

		// Token: 0x06001DC8 RID: 7624 RVA: 0x0008FDF6 File Offset: 0x0008DFF6
		internal DbConnectionInternal(ConnectionState state, bool hidePassword, bool allowSetConnectionString)
		{
			this._allowSetConnectionString = allowSetConnectionString;
			this._hidePassword = hidePassword;
			this._state = state;
		}

		// Token: 0x17000501 RID: 1281
		// (get) Token: 0x06001DC9 RID: 7625 RVA: 0x0008FE20 File Offset: 0x0008E020
		internal bool AllowSetConnectionString
		{
			get
			{
				return this._allowSetConnectionString;
			}
		}

		// Token: 0x17000502 RID: 1282
		// (get) Token: 0x06001DCA RID: 7626 RVA: 0x0008FE28 File Offset: 0x0008E028
		internal bool CanBePooled
		{
			get
			{
				return !this._connectionIsDoomed && !this._cannotBePooled && !this._owningObject.IsAlive;
			}
		}

		// Token: 0x17000503 RID: 1283
		// (get) Token: 0x06001DCB RID: 7627 RVA: 0x0008FE4A File Offset: 0x0008E04A
		// (set) Token: 0x06001DCC RID: 7628 RVA: 0x0008FE54 File Offset: 0x0008E054
		protected internal Transaction EnlistedTransaction
		{
			get
			{
				return this._enlistedTransaction;
			}
			set
			{
				Transaction enlistedTransaction = this._enlistedTransaction;
				if ((null == enlistedTransaction && null != value) || (null != enlistedTransaction && !enlistedTransaction.Equals(value)))
				{
					Transaction transaction = null;
					Transaction transaction2 = null;
					try
					{
						if (null != value)
						{
							transaction = value.Clone();
						}
						lock (this)
						{
							transaction2 = Interlocked.Exchange<Transaction>(ref this._enlistedTransaction, transaction);
							this._enlistedTransactionOriginal = value;
							value = transaction;
							transaction = null;
						}
					}
					finally
					{
						if (null != transaction2 && transaction2 != this._enlistedTransaction)
						{
							transaction2.Dispose();
						}
						if (null != transaction && transaction != this._enlistedTransaction)
						{
							transaction.Dispose();
						}
					}
					if (null != value)
					{
						this.TransactionOutcomeEnlist(value);
					}
				}
			}
		}

		// Token: 0x17000504 RID: 1284
		// (get) Token: 0x06001DCD RID: 7629 RVA: 0x0008FF38 File Offset: 0x0008E138
		protected bool EnlistedTransactionDisposed
		{
			get
			{
				bool result;
				try
				{
					Transaction enlistedTransactionOriginal = this._enlistedTransactionOriginal;
					bool flag = enlistedTransactionOriginal != null && enlistedTransactionOriginal.TransactionInformation == null;
					result = flag;
				}
				catch (ObjectDisposedException)
				{
					result = true;
				}
				return result;
			}
		}

		// Token: 0x17000505 RID: 1285
		// (get) Token: 0x06001DCE RID: 7630 RVA: 0x0008FF80 File Offset: 0x0008E180
		internal bool IsTxRootWaitingForTxEnd
		{
			get
			{
				return this._isInStasis;
			}
		}

		// Token: 0x17000506 RID: 1286
		// (get) Token: 0x06001DCF RID: 7631 RVA: 0x0000EF1B File Offset: 0x0000D11B
		protected virtual bool UnbindOnTransactionCompletion
		{
			get
			{
				return true;
			}
		}

		// Token: 0x17000507 RID: 1287
		// (get) Token: 0x06001DD0 RID: 7632 RVA: 0x000061C5 File Offset: 0x000043C5
		protected internal virtual bool IsNonPoolableTransactionRoot
		{
			get
			{
				return false;
			}
		}

		// Token: 0x17000508 RID: 1288
		// (get) Token: 0x06001DD1 RID: 7633 RVA: 0x000061C5 File Offset: 0x000043C5
		internal virtual bool IsTransactionRoot
		{
			get
			{
				return false;
			}
		}

		// Token: 0x17000509 RID: 1289
		// (get) Token: 0x06001DD2 RID: 7634 RVA: 0x0008FF88 File Offset: 0x0008E188
		protected internal bool IsConnectionDoomed
		{
			get
			{
				return this._connectionIsDoomed;
			}
		}

		// Token: 0x1700050A RID: 1290
		// (get) Token: 0x06001DD3 RID: 7635 RVA: 0x0008FF90 File Offset: 0x0008E190
		internal bool IsEmancipated
		{
			get
			{
				return this._pooledCount < 1 && !this._owningObject.IsAlive;
			}
		}

		// Token: 0x1700050B RID: 1291
		// (get) Token: 0x06001DD4 RID: 7636 RVA: 0x0008FFAB File Offset: 0x0008E1AB
		internal bool IsInPool
		{
			get
			{
				return this._pooledCount == 1;
			}
		}

		// Token: 0x1700050C RID: 1292
		// (get) Token: 0x06001DD5 RID: 7637 RVA: 0x0008FFB6 File Offset: 0x0008E1B6
		protected internal object Owner
		{
			get
			{
				return this._owningObject.Target;
			}
		}

		// Token: 0x1700050D RID: 1293
		// (get) Token: 0x06001DD6 RID: 7638 RVA: 0x0008FFC3 File Offset: 0x0008E1C3
		internal DbConnectionPool Pool
		{
			get
			{
				return this._connectionPool;
			}
		}

		// Token: 0x1700050E RID: 1294
		// (get) Token: 0x06001DD7 RID: 7639 RVA: 0x0000EF1B File Offset: 0x0000D11B
		protected virtual bool ReadyToPrepareTransaction
		{
			get
			{
				return true;
			}
		}

		// Token: 0x1700050F RID: 1295
		// (get) Token: 0x06001DD8 RID: 7640 RVA: 0x0008FFCB File Offset: 0x0008E1CB
		protected internal DbReferenceCollection ReferenceCollection
		{
			get
			{
				return this._referenceCollection;
			}
		}

		// Token: 0x17000510 RID: 1296
		// (get) Token: 0x06001DD9 RID: 7641
		public abstract string ServerVersion { get; }

		// Token: 0x17000511 RID: 1297
		// (get) Token: 0x06001DDA RID: 7642 RVA: 0x0005CDBE File Offset: 0x0005AFBE
		public virtual string ServerVersionNormalized
		{
			get
			{
				throw ADP.NotSupported();
			}
		}

		// Token: 0x17000512 RID: 1298
		// (get) Token: 0x06001DDB RID: 7643 RVA: 0x0008FFD3 File Offset: 0x0008E1D3
		public bool ShouldHidePassword
		{
			get
			{
				return this._hidePassword;
			}
		}

		// Token: 0x17000513 RID: 1299
		// (get) Token: 0x06001DDC RID: 7644 RVA: 0x0008FFDB File Offset: 0x0008E1DB
		public ConnectionState State
		{
			get
			{
				return this._state;
			}
		}

		// Token: 0x06001DDD RID: 7645
		protected abstract void Activate(Transaction transaction);

		// Token: 0x06001DDE RID: 7646 RVA: 0x0008FFE3 File Offset: 0x0008E1E3
		internal void ActivateConnection(Transaction transaction)
		{
			this.Activate(transaction);
		}

		// Token: 0x06001DDF RID: 7647 RVA: 0x0008FFEC File Offset: 0x0008E1EC
		internal void AddWeakReference(object value, int tag)
		{
			if (this._referenceCollection == null)
			{
				this._referenceCollection = this.CreateReferenceCollection();
				if (this._referenceCollection == null)
				{
					throw ADP.InternalError(ADP.InternalErrorCode.CreateReferenceCollectionReturnedNull);
				}
			}
			this._referenceCollection.Add(value, tag);
		}

		// Token: 0x06001DE0 RID: 7648
		public abstract DbTransaction BeginTransaction(IsolationLevel il);

		// Token: 0x06001DE1 RID: 7649 RVA: 0x0009001F File Offset: 0x0008E21F
		public virtual void ChangeDatabase(string value)
		{
			throw ADP.MethodNotImplemented("ChangeDatabase");
		}

		// Token: 0x06001DE2 RID: 7650 RVA: 0x0009002C File Offset: 0x0008E22C
		internal virtual void CloseConnection(DbConnection owningObject, DbConnectionFactory connectionFactory)
		{
			if (connectionFactory.SetInnerConnectionFrom(owningObject, DbConnectionOpenBusy.SingletonInstance, this))
			{
				lock (this)
				{
					object lockToken = this.ObtainAdditionalLocksForClose();
					try
					{
						this.PrepareForCloseConnection();
						DbConnectionPool pool = this.Pool;
						this.DetachCurrentTransactionIfEnded();
						if (pool != null)
						{
							pool.PutObject(this, owningObject);
						}
						else
						{
							this.Deactivate();
							this._owningObject.Target = null;
							if (this.IsTransactionRoot)
							{
								this.SetInStasis();
							}
							else
							{
								this.Dispose();
							}
						}
					}
					finally
					{
						this.ReleaseAdditionalLocksForClose(lockToken);
						connectionFactory.SetInnerConnectionEvent(owningObject, DbConnectionClosedPreviouslyOpened.SingletonInstance);
					}
				}
			}
		}

		// Token: 0x06001DE3 RID: 7651 RVA: 0x00005E03 File Offset: 0x00004003
		internal virtual void PrepareForReplaceConnection()
		{
		}

		// Token: 0x06001DE4 RID: 7652 RVA: 0x00005E03 File Offset: 0x00004003
		protected virtual void PrepareForCloseConnection()
		{
		}

		// Token: 0x06001DE5 RID: 7653 RVA: 0x00004526 File Offset: 0x00002726
		protected virtual object ObtainAdditionalLocksForClose()
		{
			return null;
		}

		// Token: 0x06001DE6 RID: 7654 RVA: 0x00005E03 File Offset: 0x00004003
		protected virtual void ReleaseAdditionalLocksForClose(object lockToken)
		{
		}

		// Token: 0x06001DE7 RID: 7655 RVA: 0x000900E0 File Offset: 0x0008E2E0
		protected virtual DbReferenceCollection CreateReferenceCollection()
		{
			throw ADP.InternalError(ADP.InternalErrorCode.AttemptingToConstructReferenceCollectionOnStaticObject);
		}

		// Token: 0x06001DE8 RID: 7656
		protected abstract void Deactivate();

		// Token: 0x06001DE9 RID: 7657 RVA: 0x000900EC File Offset: 0x0008E2EC
		internal void DeactivateConnection()
		{
			if (!this._connectionIsDoomed && this.Pool.UseLoadBalancing && DateTime.UtcNow.Ticks - this._createTime.Ticks > this.Pool.LoadBalanceTimeout.Ticks)
			{
				this.DoNotPoolThisConnection();
			}
			this.Deactivate();
		}

		// Token: 0x06001DEA RID: 7658 RVA: 0x00090148 File Offset: 0x0008E348
		internal virtual void DelegatedTransactionEnded()
		{
			if (1 != this._pooledCount)
			{
				if (-1 == this._pooledCount && !this._owningObject.IsAlive)
				{
					this.TerminateStasis(false);
					this.Deactivate();
					this.Dispose();
				}
				return;
			}
			this.TerminateStasis(true);
			this.Deactivate();
			DbConnectionPool pool = this.Pool;
			if (pool == null)
			{
				throw ADP.InternalError(ADP.InternalErrorCode.PooledObjectWithoutPool);
			}
			pool.PutObjectFromTransactedPool(this);
		}

		// Token: 0x06001DEB RID: 7659 RVA: 0x000901B0 File Offset: 0x0008E3B0
		public virtual void Dispose()
		{
			this._connectionPool = null;
			this._connectionIsDoomed = true;
			this._enlistedTransactionOriginal = null;
			Transaction transaction = Interlocked.Exchange<Transaction>(ref this._enlistedTransaction, null);
			if (transaction != null)
			{
				transaction.Dispose();
			}
		}

		// Token: 0x06001DEC RID: 7660 RVA: 0x000901EE File Offset: 0x0008E3EE
		protected internal void DoNotPoolThisConnection()
		{
			this._cannotBePooled = true;
		}

		// Token: 0x06001DED RID: 7661 RVA: 0x000901F7 File Offset: 0x0008E3F7
		protected internal void DoomThisConnection()
		{
			this._connectionIsDoomed = true;
		}

		// Token: 0x06001DEE RID: 7662
		public abstract void EnlistTransaction(Transaction transaction);

		// Token: 0x06001DEF RID: 7663 RVA: 0x00090200 File Offset: 0x0008E400
		protected internal virtual DataTable GetSchema(DbConnectionFactory factory, DbConnectionPoolGroup poolGroup, DbConnection outerConnection, string collectionName, string[] restrictions)
		{
			return factory.GetMetaDataFactory(poolGroup, this).GetSchema(outerConnection, collectionName, restrictions);
		}

		// Token: 0x06001DF0 RID: 7664 RVA: 0x00090214 File Offset: 0x0008E414
		internal void MakeNonPooledObject(object owningObject)
		{
			this._connectionPool = null;
			this._owningObject.Target = owningObject;
			this._pooledCount = -1;
		}

		// Token: 0x06001DF1 RID: 7665 RVA: 0x00090230 File Offset: 0x0008E430
		internal void MakePooledConnection(DbConnectionPool connectionPool)
		{
			this._createTime = DateTime.UtcNow;
			this._connectionPool = connectionPool;
		}

		// Token: 0x06001DF2 RID: 7666 RVA: 0x00090244 File Offset: 0x0008E444
		internal void NotifyWeakReference(int message)
		{
			DbReferenceCollection referenceCollection = this.ReferenceCollection;
			if (referenceCollection != null)
			{
				referenceCollection.Notify(message);
			}
		}

		// Token: 0x06001DF3 RID: 7667 RVA: 0x00090262 File Offset: 0x0008E462
		internal virtual void OpenConnection(DbConnection outerConnection, DbConnectionFactory connectionFactory)
		{
			if (!this.TryOpenConnection(outerConnection, connectionFactory, null, null))
			{
				throw ADP.InternalError(ADP.InternalErrorCode.SynchronousConnectReturnedPending);
			}
		}

		// Token: 0x06001DF4 RID: 7668 RVA: 0x0008F328 File Offset: 0x0008D528
		internal virtual bool TryOpenConnection(DbConnection outerConnection, DbConnectionFactory connectionFactory, TaskCompletionSource<DbConnectionInternal> retry, DbConnectionOptions userOptions)
		{
			throw ADP.ConnectionAlreadyOpen(this.State);
		}

		// Token: 0x06001DF5 RID: 7669 RVA: 0x00090278 File Offset: 0x0008E478
		internal virtual bool TryReplaceConnection(DbConnection outerConnection, DbConnectionFactory connectionFactory, TaskCompletionSource<DbConnectionInternal> retry, DbConnectionOptions userOptions)
		{
			throw ADP.MethodNotImplemented("TryReplaceConnection");
		}

		// Token: 0x06001DF6 RID: 7670 RVA: 0x00090284 File Offset: 0x0008E484
		protected bool TryOpenConnectionInternal(DbConnection outerConnection, DbConnectionFactory connectionFactory, TaskCompletionSource<DbConnectionInternal> retry, DbConnectionOptions userOptions)
		{
			if (connectionFactory.SetInnerConnectionFrom(outerConnection, DbConnectionClosedConnecting.SingletonInstance, this))
			{
				DbConnectionInternal dbConnectionInternal = null;
				try
				{
					connectionFactory.PermissionDemand(outerConnection);
					if (!connectionFactory.TryGetConnection(outerConnection, retry, userOptions, this, out dbConnectionInternal))
					{
						return false;
					}
				}
				catch
				{
					connectionFactory.SetInnerConnectionTo(outerConnection, this);
					throw;
				}
				if (dbConnectionInternal == null)
				{
					connectionFactory.SetInnerConnectionTo(outerConnection, this);
					throw ADP.InternalConnectionError(ADP.ConnectionError.GetConnectionReturnsNull);
				}
				connectionFactory.SetInnerConnectionEvent(outerConnection, dbConnectionInternal);
				return true;
			}
			return true;
		}

		// Token: 0x06001DF7 RID: 7671 RVA: 0x000902F8 File Offset: 0x0008E4F8
		internal void PrePush(object expectedOwner)
		{
			if (expectedOwner == null)
			{
				if (this._owningObject.Target != null)
				{
					throw ADP.InternalError(ADP.InternalErrorCode.UnpooledObjectHasOwner);
				}
			}
			else if (this._owningObject.Target != expectedOwner)
			{
				throw ADP.InternalError(ADP.InternalErrorCode.UnpooledObjectHasWrongOwner);
			}
			if (this._pooledCount != 0)
			{
				throw ADP.InternalError(ADP.InternalErrorCode.PushingObjectSecondTime);
			}
			this._pooledCount++;
			this._owningObject.Target = null;
		}

		// Token: 0x06001DF8 RID: 7672 RVA: 0x0009035C File Offset: 0x0008E55C
		internal void PostPop(object newOwner)
		{
			if (this._owningObject.Target != null)
			{
				throw ADP.InternalError(ADP.InternalErrorCode.PooledObjectHasOwner);
			}
			this._owningObject.Target = newOwner;
			this._pooledCount--;
			if (this.Pool != null)
			{
				if (this._pooledCount != 0)
				{
					throw ADP.InternalError(ADP.InternalErrorCode.PooledObjectInPoolMoreThanOnce);
				}
			}
			else if (-1 != this._pooledCount)
			{
				throw ADP.InternalError(ADP.InternalErrorCode.NonPooledObjectUsedMoreThanOnce);
			}
		}

		// Token: 0x06001DF9 RID: 7673 RVA: 0x000903C0 File Offset: 0x0008E5C0
		internal void RemoveWeakReference(object value)
		{
			DbReferenceCollection referenceCollection = this.ReferenceCollection;
			if (referenceCollection != null)
			{
				referenceCollection.Remove(value);
			}
		}

		// Token: 0x06001DFA RID: 7674 RVA: 0x00005E03 File Offset: 0x00004003
		protected virtual void CleanupTransactionOnCompletion(Transaction transaction)
		{
		}

		// Token: 0x06001DFB RID: 7675 RVA: 0x000903E0 File Offset: 0x0008E5E0
		internal void DetachCurrentTransactionIfEnded()
		{
			Transaction enlistedTransaction = this.EnlistedTransaction;
			if (enlistedTransaction != null)
			{
				bool flag;
				try
				{
					flag = (enlistedTransaction.TransactionInformation.Status > TransactionStatus.Active);
				}
				catch (TransactionException)
				{
					flag = true;
				}
				if (flag)
				{
					this.DetachTransaction(enlistedTransaction, true);
				}
			}
		}

		// Token: 0x06001DFC RID: 7676 RVA: 0x00090430 File Offset: 0x0008E630
		internal void DetachTransaction(Transaction transaction, bool isExplicitlyReleasing)
		{
			lock (this)
			{
				DbConnection dbConnection = (DbConnection)this.Owner;
				if (isExplicitlyReleasing || this.UnbindOnTransactionCompletion || dbConnection == null)
				{
					Transaction enlistedTransaction = this._enlistedTransaction;
					if (enlistedTransaction != null && transaction.Equals(enlistedTransaction))
					{
						this.EnlistedTransaction = null;
						if (this.IsTxRootWaitingForTxEnd)
						{
							this.DelegatedTransactionEnded();
						}
					}
				}
			}
		}

		// Token: 0x06001DFD RID: 7677 RVA: 0x000904B0 File Offset: 0x0008E6B0
		internal void CleanupConnectionOnTransactionCompletion(Transaction transaction)
		{
			this.DetachTransaction(transaction, false);
			DbConnectionPool pool = this.Pool;
			if (pool != null)
			{
				pool.TransactionEnded(transaction, this);
			}
		}

		// Token: 0x06001DFE RID: 7678 RVA: 0x000904D8 File Offset: 0x0008E6D8
		private void TransactionCompletedEvent(object sender, TransactionEventArgs e)
		{
			Transaction transaction = e.Transaction;
			this.CleanupTransactionOnCompletion(transaction);
			this.CleanupConnectionOnTransactionCompletion(transaction);
		}

		// Token: 0x06001DFF RID: 7679 RVA: 0x000904FA File Offset: 0x0008E6FA
		private void TransactionOutcomeEnlist(Transaction transaction)
		{
			transaction.TransactionCompleted += this.TransactionCompletedEvent;
		}

		// Token: 0x06001E00 RID: 7680 RVA: 0x0009050E File Offset: 0x0008E70E
		internal void SetInStasis()
		{
			this._isInStasis = true;
		}

		// Token: 0x06001E01 RID: 7681 RVA: 0x00090517 File Offset: 0x0008E717
		private void TerminateStasis(bool returningToPool)
		{
			this._isInStasis = false;
		}

		// Token: 0x06001E02 RID: 7682 RVA: 0x0000EF1B File Offset: 0x0000D11B
		internal virtual bool IsConnectionAlive(bool throwOnException = false)
		{
			return true;
		}

		// Token: 0x06001E03 RID: 7683 RVA: 0x00090520 File Offset: 0x0008E720
		// Note: this type is marked as 'beforefieldinit'.
		static DbConnectionInternal()
		{
		}

		// Token: 0x04001402 RID: 5122
		internal static readonly StateChangeEventArgs StateChangeClosed = new StateChangeEventArgs(ConnectionState.Open, ConnectionState.Closed);

		// Token: 0x04001403 RID: 5123
		internal static readonly StateChangeEventArgs StateChangeOpen = new StateChangeEventArgs(ConnectionState.Closed, ConnectionState.Open);

		// Token: 0x04001404 RID: 5124
		private readonly bool _allowSetConnectionString;

		// Token: 0x04001405 RID: 5125
		private readonly bool _hidePassword;

		// Token: 0x04001406 RID: 5126
		private readonly ConnectionState _state;

		// Token: 0x04001407 RID: 5127
		private readonly WeakReference _owningObject = new WeakReference(null, false);

		// Token: 0x04001408 RID: 5128
		private DbConnectionPool _connectionPool;

		// Token: 0x04001409 RID: 5129
		private DbReferenceCollection _referenceCollection;

		// Token: 0x0400140A RID: 5130
		private int _pooledCount;

		// Token: 0x0400140B RID: 5131
		private bool _connectionIsDoomed;

		// Token: 0x0400140C RID: 5132
		private bool _cannotBePooled;

		// Token: 0x0400140D RID: 5133
		private bool _isInStasis;

		// Token: 0x0400140E RID: 5134
		private DateTime _createTime;

		// Token: 0x0400140F RID: 5135
		private Transaction _enlistedTransaction;

		// Token: 0x04001410 RID: 5136
		private Transaction _enlistedTransactionOriginal;
	}
}
