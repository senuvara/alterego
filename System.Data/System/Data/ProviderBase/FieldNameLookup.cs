﻿using System;
using System.Collections.ObjectModel;
using System.Globalization;

namespace System.Data.ProviderBase
{
	// Token: 0x02000269 RID: 617
	internal sealed class FieldNameLookup : BasicFieldNameLookup
	{
		// Token: 0x06001D27 RID: 7463 RVA: 0x0008CE79 File Offset: 0x0008B079
		public FieldNameLookup(string[] fieldNames, int defaultLocaleID) : base(fieldNames)
		{
			this._defaultLocaleID = defaultLocaleID;
		}

		// Token: 0x06001D28 RID: 7464 RVA: 0x0008CE89 File Offset: 0x0008B089
		public FieldNameLookup(ReadOnlyCollection<string> columnNames, int defaultLocaleID) : base(columnNames)
		{
			this._defaultLocaleID = defaultLocaleID;
		}

		// Token: 0x06001D29 RID: 7465 RVA: 0x0008CE99 File Offset: 0x0008B099
		public FieldNameLookup(IDataReader reader, int defaultLocaleID) : base(reader)
		{
			this._defaultLocaleID = defaultLocaleID;
		}

		// Token: 0x06001D2A RID: 7466 RVA: 0x0008CEAC File Offset: 0x0008B0AC
		protected override CompareInfo GetCompareInfo()
		{
			CompareInfo compareInfo = null;
			if (-1 != this._defaultLocaleID)
			{
				compareInfo = CompareInfo.GetCompareInfo(this._defaultLocaleID);
			}
			if (compareInfo == null)
			{
				compareInfo = base.GetCompareInfo();
			}
			return compareInfo;
		}

		// Token: 0x040013D0 RID: 5072
		private readonly int _defaultLocaleID;
	}
}
