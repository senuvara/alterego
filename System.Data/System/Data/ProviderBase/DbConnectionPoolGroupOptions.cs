﻿using System;

namespace System.Data.ProviderBase
{
	// Token: 0x02000283 RID: 643
	internal sealed class DbConnectionPoolGroupOptions
	{
		// Token: 0x06001E5C RID: 7772 RVA: 0x00091E28 File Offset: 0x00090028
		public DbConnectionPoolGroupOptions(bool poolByIdentity, int minPoolSize, int maxPoolSize, int creationTimeout, int loadBalanceTimeout, bool hasTransactionAffinity)
		{
			this._poolByIdentity = poolByIdentity;
			this._minPoolSize = minPoolSize;
			this._maxPoolSize = maxPoolSize;
			this._creationTimeout = creationTimeout;
			if (loadBalanceTimeout != 0)
			{
				this._loadBalanceTimeout = new TimeSpan(0, 0, loadBalanceTimeout);
				this._useLoadBalancing = true;
			}
			this._hasTransactionAffinity = hasTransactionAffinity;
		}

		// Token: 0x17000535 RID: 1333
		// (get) Token: 0x06001E5D RID: 7773 RVA: 0x00091E7A File Offset: 0x0009007A
		public int CreationTimeout
		{
			get
			{
				return this._creationTimeout;
			}
		}

		// Token: 0x17000536 RID: 1334
		// (get) Token: 0x06001E5E RID: 7774 RVA: 0x00091E82 File Offset: 0x00090082
		public bool HasTransactionAffinity
		{
			get
			{
				return this._hasTransactionAffinity;
			}
		}

		// Token: 0x17000537 RID: 1335
		// (get) Token: 0x06001E5F RID: 7775 RVA: 0x00091E8A File Offset: 0x0009008A
		public TimeSpan LoadBalanceTimeout
		{
			get
			{
				return this._loadBalanceTimeout;
			}
		}

		// Token: 0x17000538 RID: 1336
		// (get) Token: 0x06001E60 RID: 7776 RVA: 0x00091E92 File Offset: 0x00090092
		public int MaxPoolSize
		{
			get
			{
				return this._maxPoolSize;
			}
		}

		// Token: 0x17000539 RID: 1337
		// (get) Token: 0x06001E61 RID: 7777 RVA: 0x00091E9A File Offset: 0x0009009A
		public int MinPoolSize
		{
			get
			{
				return this._minPoolSize;
			}
		}

		// Token: 0x1700053A RID: 1338
		// (get) Token: 0x06001E62 RID: 7778 RVA: 0x00091EA2 File Offset: 0x000900A2
		public bool PoolByIdentity
		{
			get
			{
				return this._poolByIdentity;
			}
		}

		// Token: 0x1700053B RID: 1339
		// (get) Token: 0x06001E63 RID: 7779 RVA: 0x00091EAA File Offset: 0x000900AA
		public bool UseLoadBalancing
		{
			get
			{
				return this._useLoadBalancing;
			}
		}

		// Token: 0x04001450 RID: 5200
		private readonly bool _poolByIdentity;

		// Token: 0x04001451 RID: 5201
		private readonly int _minPoolSize;

		// Token: 0x04001452 RID: 5202
		private readonly int _maxPoolSize;

		// Token: 0x04001453 RID: 5203
		private readonly int _creationTimeout;

		// Token: 0x04001454 RID: 5204
		private readonly TimeSpan _loadBalanceTimeout;

		// Token: 0x04001455 RID: 5205
		private readonly bool _hasTransactionAffinity;

		// Token: 0x04001456 RID: 5206
		private readonly bool _useLoadBalancing;
	}
}
