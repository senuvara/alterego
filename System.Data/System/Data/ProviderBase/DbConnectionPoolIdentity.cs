﻿using System;

namespace System.Data.ProviderBase
{
	// Token: 0x02000282 RID: 642
	[Serializable]
	internal sealed class DbConnectionPoolIdentity
	{
		// Token: 0x06001E55 RID: 7765 RVA: 0x00091D29 File Offset: 0x0008FF29
		internal static DbConnectionPoolIdentity GetCurrent()
		{
			return DbConnectionPoolIdentity.GetCurrentManaged();
		}

		// Token: 0x06001E56 RID: 7766 RVA: 0x00091D30 File Offset: 0x0008FF30
		private DbConnectionPoolIdentity(string sidString, bool isRestricted, bool isNetwork)
		{
			this._sidString = sidString;
			this._isRestricted = isRestricted;
			this._isNetwork = isNetwork;
			this._hashCode = ((sidString == null) ? 0 : sidString.GetHashCode());
		}

		// Token: 0x17000534 RID: 1332
		// (get) Token: 0x06001E57 RID: 7767 RVA: 0x00091D5F File Offset: 0x0008FF5F
		internal bool IsRestricted
		{
			get
			{
				return this._isRestricted;
			}
		}

		// Token: 0x06001E58 RID: 7768 RVA: 0x00091D68 File Offset: 0x0008FF68
		public override bool Equals(object value)
		{
			bool flag = this == DbConnectionPoolIdentity.NoIdentity || this == value;
			if (!flag && value != null)
			{
				DbConnectionPoolIdentity dbConnectionPoolIdentity = (DbConnectionPoolIdentity)value;
				flag = (this._sidString == dbConnectionPoolIdentity._sidString && this._isRestricted == dbConnectionPoolIdentity._isRestricted && this._isNetwork == dbConnectionPoolIdentity._isNetwork);
			}
			return flag;
		}

		// Token: 0x06001E59 RID: 7769 RVA: 0x00091DC6 File Offset: 0x0008FFC6
		public override int GetHashCode()
		{
			return this._hashCode;
		}

		// Token: 0x06001E5A RID: 7770 RVA: 0x00091DD0 File Offset: 0x0008FFD0
		internal static DbConnectionPoolIdentity GetCurrentManaged()
		{
			string sidString = ((!string.IsNullOrWhiteSpace(Environment.UserDomainName)) ? (Environment.UserDomainName + "\\") : "") + Environment.UserName;
			bool isNetwork = false;
			bool isRestricted = false;
			return new DbConnectionPoolIdentity(sidString, isRestricted, isNetwork);
		}

		// Token: 0x06001E5B RID: 7771 RVA: 0x00091E14 File Offset: 0x00090014
		// Note: this type is marked as 'beforefieldinit'.
		static DbConnectionPoolIdentity()
		{
		}

		// Token: 0x0400144B RID: 5195
		public static readonly DbConnectionPoolIdentity NoIdentity = new DbConnectionPoolIdentity(string.Empty, false, true);

		// Token: 0x0400144C RID: 5196
		private readonly string _sidString;

		// Token: 0x0400144D RID: 5197
		private readonly bool _isRestricted;

		// Token: 0x0400144E RID: 5198
		private readonly bool _isNetwork;

		// Token: 0x0400144F RID: 5199
		private readonly int _hashCode;
	}
}
