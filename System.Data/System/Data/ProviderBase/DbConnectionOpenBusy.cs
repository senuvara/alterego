﻿using System;

namespace System.Data.ProviderBase
{
	// Token: 0x02000272 RID: 626
	internal sealed class DbConnectionOpenBusy : DbConnectionBusy
	{
		// Token: 0x06001D97 RID: 7575 RVA: 0x0008F34A File Offset: 0x0008D54A
		private DbConnectionOpenBusy() : base(ConnectionState.Open)
		{
		}

		// Token: 0x06001D98 RID: 7576 RVA: 0x0008F353 File Offset: 0x0008D553
		// Note: this type is marked as 'beforefieldinit'.
		static DbConnectionOpenBusy()
		{
		}

		// Token: 0x040013EC RID: 5100
		internal static readonly DbConnectionInternal SingletonInstance = new DbConnectionOpenBusy();
	}
}
