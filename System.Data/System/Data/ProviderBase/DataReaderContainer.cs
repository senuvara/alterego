﻿using System;
using System.Data.Common;

namespace System.Data.ProviderBase
{
	// Token: 0x0200026A RID: 618
	internal abstract class DataReaderContainer
	{
		// Token: 0x06001D2B RID: 7467 RVA: 0x0008CEDC File Offset: 0x0008B0DC
		internal static DataReaderContainer Create(IDataReader dataReader, bool returnProviderSpecificTypes)
		{
			if (returnProviderSpecificTypes)
			{
				DbDataReader dbDataReader = dataReader as DbDataReader;
				if (dbDataReader != null)
				{
					return new DataReaderContainer.ProviderSpecificDataReader(dataReader, dbDataReader);
				}
			}
			return new DataReaderContainer.CommonLanguageSubsetDataReader(dataReader);
		}

		// Token: 0x06001D2C RID: 7468 RVA: 0x0008CF04 File Offset: 0x0008B104
		protected DataReaderContainer(IDataReader dataReader)
		{
			this._dataReader = dataReader;
		}

		// Token: 0x170004F2 RID: 1266
		// (get) Token: 0x06001D2D RID: 7469 RVA: 0x0008CF13 File Offset: 0x0008B113
		internal int FieldCount
		{
			get
			{
				return this._fieldCount;
			}
		}

		// Token: 0x170004F3 RID: 1267
		// (get) Token: 0x06001D2E RID: 7470
		internal abstract bool ReturnProviderSpecificTypes { get; }

		// Token: 0x170004F4 RID: 1268
		// (get) Token: 0x06001D2F RID: 7471
		protected abstract int VisibleFieldCount { get; }

		// Token: 0x06001D30 RID: 7472
		internal abstract Type GetFieldType(int ordinal);

		// Token: 0x06001D31 RID: 7473
		internal abstract object GetValue(int ordinal);

		// Token: 0x06001D32 RID: 7474
		internal abstract int GetValues(object[] values);

		// Token: 0x06001D33 RID: 7475 RVA: 0x0008CF1C File Offset: 0x0008B11C
		internal string GetName(int ordinal)
		{
			string name = this._dataReader.GetName(ordinal);
			if (name == null)
			{
				return "";
			}
			return name;
		}

		// Token: 0x06001D34 RID: 7476 RVA: 0x0008CF40 File Offset: 0x0008B140
		internal DataTable GetSchemaTable()
		{
			return this._dataReader.GetSchemaTable();
		}

		// Token: 0x06001D35 RID: 7477 RVA: 0x0008CF4D File Offset: 0x0008B14D
		internal bool NextResult()
		{
			this._fieldCount = 0;
			if (this._dataReader.NextResult())
			{
				this._fieldCount = this.VisibleFieldCount;
				return true;
			}
			return false;
		}

		// Token: 0x06001D36 RID: 7478 RVA: 0x0008CF72 File Offset: 0x0008B172
		internal bool Read()
		{
			return this._dataReader.Read();
		}

		// Token: 0x040013D1 RID: 5073
		protected readonly IDataReader _dataReader;

		// Token: 0x040013D2 RID: 5074
		protected int _fieldCount;

		// Token: 0x0200026B RID: 619
		private sealed class ProviderSpecificDataReader : DataReaderContainer
		{
			// Token: 0x06001D37 RID: 7479 RVA: 0x0008CF7F File Offset: 0x0008B17F
			internal ProviderSpecificDataReader(IDataReader dataReader, DbDataReader dbDataReader) : base(dataReader)
			{
				this._providerSpecificDataReader = dbDataReader;
				this._fieldCount = this.VisibleFieldCount;
			}

			// Token: 0x170004F5 RID: 1269
			// (get) Token: 0x06001D38 RID: 7480 RVA: 0x0000EF1B File Offset: 0x0000D11B
			internal override bool ReturnProviderSpecificTypes
			{
				get
				{
					return true;
				}
			}

			// Token: 0x170004F6 RID: 1270
			// (get) Token: 0x06001D39 RID: 7481 RVA: 0x0008CF9C File Offset: 0x0008B19C
			protected override int VisibleFieldCount
			{
				get
				{
					int visibleFieldCount = this._providerSpecificDataReader.VisibleFieldCount;
					if (0 > visibleFieldCount)
					{
						return 0;
					}
					return visibleFieldCount;
				}
			}

			// Token: 0x06001D3A RID: 7482 RVA: 0x0008CFBC File Offset: 0x0008B1BC
			internal override Type GetFieldType(int ordinal)
			{
				return this._providerSpecificDataReader.GetProviderSpecificFieldType(ordinal);
			}

			// Token: 0x06001D3B RID: 7483 RVA: 0x0008CFCA File Offset: 0x0008B1CA
			internal override object GetValue(int ordinal)
			{
				return this._providerSpecificDataReader.GetProviderSpecificValue(ordinal);
			}

			// Token: 0x06001D3C RID: 7484 RVA: 0x0008CFD8 File Offset: 0x0008B1D8
			internal override int GetValues(object[] values)
			{
				return this._providerSpecificDataReader.GetProviderSpecificValues(values);
			}

			// Token: 0x040013D3 RID: 5075
			private DbDataReader _providerSpecificDataReader;
		}

		// Token: 0x0200026C RID: 620
		private sealed class CommonLanguageSubsetDataReader : DataReaderContainer
		{
			// Token: 0x06001D3D RID: 7485 RVA: 0x0008CFE6 File Offset: 0x0008B1E6
			internal CommonLanguageSubsetDataReader(IDataReader dataReader) : base(dataReader)
			{
				this._fieldCount = this.VisibleFieldCount;
			}

			// Token: 0x170004F7 RID: 1271
			// (get) Token: 0x06001D3E RID: 7486 RVA: 0x000061C5 File Offset: 0x000043C5
			internal override bool ReturnProviderSpecificTypes
			{
				get
				{
					return false;
				}
			}

			// Token: 0x170004F8 RID: 1272
			// (get) Token: 0x06001D3F RID: 7487 RVA: 0x0008CFFC File Offset: 0x0008B1FC
			protected override int VisibleFieldCount
			{
				get
				{
					int fieldCount = this._dataReader.FieldCount;
					if (0 > fieldCount)
					{
						return 0;
					}
					return fieldCount;
				}
			}

			// Token: 0x06001D40 RID: 7488 RVA: 0x0008D01C File Offset: 0x0008B21C
			internal override Type GetFieldType(int ordinal)
			{
				return this._dataReader.GetFieldType(ordinal);
			}

			// Token: 0x06001D41 RID: 7489 RVA: 0x0008D02A File Offset: 0x0008B22A
			internal override object GetValue(int ordinal)
			{
				return this._dataReader.GetValue(ordinal);
			}

			// Token: 0x06001D42 RID: 7490 RVA: 0x0008D038 File Offset: 0x0008B238
			internal override int GetValues(object[] values)
			{
				return this._dataReader.GetValues(values);
			}
		}
	}
}
