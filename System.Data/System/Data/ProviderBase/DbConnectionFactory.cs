﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;

namespace System.Data.ProviderBase
{
	// Token: 0x02000276 RID: 630
	internal abstract class DbConnectionFactory
	{
		// Token: 0x06001DA3 RID: 7587 RVA: 0x0008F40E File Offset: 0x0008D60E
		protected DbConnectionFactory()
		{
			this._connectionPoolGroups = new Dictionary<DbConnectionPoolKey, DbConnectionPoolGroup>();
			this._poolsToRelease = new List<DbConnectionPool>();
			this._poolGroupsToRelease = new List<DbConnectionPoolGroup>();
			this._pruningTimer = this.CreatePruningTimer();
		}

		// Token: 0x17000500 RID: 1280
		// (get) Token: 0x06001DA4 RID: 7588
		public abstract DbProviderFactory ProviderFactory { get; }

		// Token: 0x06001DA5 RID: 7589 RVA: 0x0008F444 File Offset: 0x0008D644
		public void ClearAllPools()
		{
			foreach (KeyValuePair<DbConnectionPoolKey, DbConnectionPoolGroup> keyValuePair in this._connectionPoolGroups)
			{
				DbConnectionPoolGroup value = keyValuePair.Value;
				if (value != null)
				{
					value.Clear();
				}
			}
		}

		// Token: 0x06001DA6 RID: 7590 RVA: 0x0008F4A4 File Offset: 0x0008D6A4
		public void ClearPool(DbConnection connection)
		{
			ADP.CheckArgumentNull(connection, "connection");
			DbConnectionPoolGroup connectionPoolGroup = this.GetConnectionPoolGroup(connection);
			if (connectionPoolGroup != null)
			{
				connectionPoolGroup.Clear();
			}
		}

		// Token: 0x06001DA7 RID: 7591 RVA: 0x0008F4D0 File Offset: 0x0008D6D0
		public void ClearPool(DbConnectionPoolKey key)
		{
			ADP.CheckArgumentNull(key.ConnectionString, "key.ConnectionString");
			DbConnectionPoolGroup dbConnectionPoolGroup;
			if (this._connectionPoolGroups.TryGetValue(key, out dbConnectionPoolGroup))
			{
				dbConnectionPoolGroup.Clear();
			}
		}

		// Token: 0x06001DA8 RID: 7592 RVA: 0x00004526 File Offset: 0x00002726
		internal virtual DbConnectionPoolProviderInfo CreateConnectionPoolProviderInfo(DbConnectionOptions connectionOptions)
		{
			return null;
		}

		// Token: 0x06001DA9 RID: 7593 RVA: 0x0008F504 File Offset: 0x0008D704
		internal DbConnectionInternal CreateNonPooledConnection(DbConnection owningConnection, DbConnectionPoolGroup poolGroup, DbConnectionOptions userOptions)
		{
			DbConnectionOptions connectionOptions = poolGroup.ConnectionOptions;
			DbConnectionPoolGroupProviderInfo providerInfo = poolGroup.ProviderInfo;
			DbConnectionPoolKey poolKey = poolGroup.PoolKey;
			DbConnectionInternal dbConnectionInternal = this.CreateConnection(connectionOptions, poolKey, providerInfo, null, owningConnection, userOptions);
			if (dbConnectionInternal != null)
			{
				dbConnectionInternal.MakeNonPooledObject(owningConnection);
			}
			return dbConnectionInternal;
		}

		// Token: 0x06001DAA RID: 7594 RVA: 0x0008F540 File Offset: 0x0008D740
		internal DbConnectionInternal CreatePooledConnection(DbConnectionPool pool, DbConnection owningObject, DbConnectionOptions options, DbConnectionPoolKey poolKey, DbConnectionOptions userOptions)
		{
			DbConnectionPoolGroupProviderInfo providerInfo = pool.PoolGroup.ProviderInfo;
			DbConnectionInternal dbConnectionInternal = this.CreateConnection(options, poolKey, providerInfo, pool, owningObject, userOptions);
			if (dbConnectionInternal != null)
			{
				dbConnectionInternal.MakePooledConnection(pool);
			}
			return dbConnectionInternal;
		}

		// Token: 0x06001DAB RID: 7595 RVA: 0x00004526 File Offset: 0x00002726
		internal virtual DbConnectionPoolGroupProviderInfo CreateConnectionPoolGroupProviderInfo(DbConnectionOptions connectionOptions)
		{
			return null;
		}

		// Token: 0x06001DAC RID: 7596 RVA: 0x0008F573 File Offset: 0x0008D773
		private Timer CreatePruningTimer()
		{
			return new Timer(new TimerCallback(this.PruneConnectionPoolGroups), null, 240000, 30000);
		}

		// Token: 0x06001DAD RID: 7597 RVA: 0x0008F594 File Offset: 0x0008D794
		protected DbConnectionOptions FindConnectionOptions(DbConnectionPoolKey key)
		{
			DbConnectionPoolGroup dbConnectionPoolGroup;
			if (!string.IsNullOrEmpty(key.ConnectionString) && this._connectionPoolGroups.TryGetValue(key, out dbConnectionPoolGroup))
			{
				return dbConnectionPoolGroup.ConnectionOptions;
			}
			return null;
		}

		// Token: 0x06001DAE RID: 7598 RVA: 0x0008F5C6 File Offset: 0x0008D7C6
		private static Task<DbConnectionInternal> GetCompletedTask()
		{
			Task<DbConnectionInternal> result;
			if ((result = DbConnectionFactory.s_completedTask) == null)
			{
				result = (DbConnectionFactory.s_completedTask = Task.FromResult<DbConnectionInternal>(null));
			}
			return result;
		}

		// Token: 0x06001DAF RID: 7599 RVA: 0x0008F5E0 File Offset: 0x0008D7E0
		internal bool TryGetConnection(DbConnection owningConnection, TaskCompletionSource<DbConnectionInternal> retry, DbConnectionOptions userOptions, DbConnectionInternal oldConnection, out DbConnectionInternal connection)
		{
			DbConnectionFactory.<>c__DisplayClass22_0 CS$<>8__locals1 = new DbConnectionFactory.<>c__DisplayClass22_0();
			CS$<>8__locals1.retry = retry;
			CS$<>8__locals1.<>4__this = this;
			CS$<>8__locals1.owningConnection = owningConnection;
			CS$<>8__locals1.userOptions = userOptions;
			CS$<>8__locals1.oldConnection = oldConnection;
			connection = null;
			int num = 10;
			int num2 = 1;
			for (;;)
			{
				CS$<>8__locals1.poolGroup = this.GetConnectionPoolGroup(CS$<>8__locals1.owningConnection);
				DbConnectionPool connectionPool = this.GetConnectionPool(CS$<>8__locals1.owningConnection, CS$<>8__locals1.poolGroup);
				if (connectionPool == null)
				{
					CS$<>8__locals1.poolGroup = this.GetConnectionPoolGroup(CS$<>8__locals1.owningConnection);
					if (CS$<>8__locals1.retry != null)
					{
						break;
					}
					connection = this.CreateNonPooledConnection(CS$<>8__locals1.owningConnection, CS$<>8__locals1.poolGroup, CS$<>8__locals1.userOptions);
				}
				else
				{
					if (((SqlConnection)CS$<>8__locals1.owningConnection).ForceNewConnection)
					{
						connection = connectionPool.ReplaceConnection(CS$<>8__locals1.owningConnection, CS$<>8__locals1.userOptions, CS$<>8__locals1.oldConnection);
					}
					else if (!connectionPool.TryGetConnection(CS$<>8__locals1.owningConnection, CS$<>8__locals1.retry, CS$<>8__locals1.userOptions, out connection))
					{
						return false;
					}
					if (connection == null)
					{
						if (connectionPool.IsRunning)
						{
							goto Block_8;
						}
						Thread.Sleep(num2);
						num2 *= 2;
					}
				}
				if (connection != null || num-- <= 0)
				{
					goto IL_268;
				}
			}
			CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();
			Task<DbConnectionInternal>[] obj = DbConnectionFactory.s_pendingOpenNonPooled;
			Task<DbConnectionInternal> task3;
			lock (obj)
			{
				int i;
				for (i = 0; i < DbConnectionFactory.s_pendingOpenNonPooled.Length; i++)
				{
					Task task4 = DbConnectionFactory.s_pendingOpenNonPooled[i];
					if (task4 == null)
					{
						DbConnectionFactory.s_pendingOpenNonPooled[i] = DbConnectionFactory.GetCompletedTask();
						break;
					}
					if (task4.IsCompleted)
					{
						break;
					}
				}
				if (i == DbConnectionFactory.s_pendingOpenNonPooled.Length)
				{
					i = (int)((ulong)DbConnectionFactory.s_pendingOpenNonPooledNext % (ulong)((long)DbConnectionFactory.s_pendingOpenNonPooled.Length));
					DbConnectionFactory.s_pendingOpenNonPooledNext += 1U;
				}
				Task<DbConnectionInternal> task2 = DbConnectionFactory.s_pendingOpenNonPooled[i];
				Func<Task<DbConnectionInternal>, DbConnectionInternal> continuationFunction;
				if ((continuationFunction = CS$<>8__locals1.<>9__1) == null)
				{
					continuationFunction = (CS$<>8__locals1.<>9__1 = delegate(Task<DbConnectionInternal> _)
					{
						Transaction currentTransaction = ADP.GetCurrentTransaction();
						DbConnectionInternal result;
						try
						{
							ADP.SetCurrentTransaction(CS$<>8__locals1.retry.Task.AsyncState as Transaction);
							DbConnectionInternal dbConnectionInternal = CS$<>8__locals1.<>4__this.CreateNonPooledConnection(CS$<>8__locals1.owningConnection, CS$<>8__locals1.poolGroup, CS$<>8__locals1.userOptions);
							if (CS$<>8__locals1.oldConnection != null && CS$<>8__locals1.oldConnection.State == ConnectionState.Open)
							{
								CS$<>8__locals1.oldConnection.PrepareForReplaceConnection();
								CS$<>8__locals1.oldConnection.Dispose();
							}
							result = dbConnectionInternal;
						}
						finally
						{
							ADP.SetCurrentTransaction(currentTransaction);
						}
						return result;
					});
				}
				task3 = task2.ContinueWith<DbConnectionInternal>(continuationFunction, cancellationTokenSource.Token, TaskContinuationOptions.LongRunning, TaskScheduler.Default);
				DbConnectionFactory.s_pendingOpenNonPooled[i] = task3;
			}
			if (CS$<>8__locals1.owningConnection.ConnectionTimeout > 0)
			{
				int millisecondsDelay = CS$<>8__locals1.owningConnection.ConnectionTimeout * 1000;
				cancellationTokenSource.CancelAfter(millisecondsDelay);
			}
			task3.ContinueWith(delegate(Task<DbConnectionInternal> task)
			{
				cancellationTokenSource.Dispose();
				if (task.IsCanceled)
				{
					CS$<>8__locals1.retry.TrySetException(ADP.ExceptionWithStackTrace(ADP.NonPooledOpenTimeout()));
					return;
				}
				if (task.IsFaulted)
				{
					CS$<>8__locals1.retry.TrySetException(task.Exception.InnerException);
					return;
				}
				if (!CS$<>8__locals1.retry.TrySetResult(task.Result))
				{
					task.Result.DoomThisConnection();
					task.Result.Dispose();
				}
			}, TaskScheduler.Default);
			return false;
			Block_8:
			throw ADP.PooledOpenTimeout();
			IL_268:
			if (connection == null)
			{
				throw ADP.PooledOpenTimeout();
			}
			return true;
		}

		// Token: 0x06001DB0 RID: 7600 RVA: 0x0008F874 File Offset: 0x0008DA74
		private DbConnectionPool GetConnectionPool(DbConnection owningObject, DbConnectionPoolGroup connectionPoolGroup)
		{
			if (connectionPoolGroup.IsDisabled && connectionPoolGroup.PoolGroupOptions != null)
			{
				DbConnectionPoolGroupOptions poolGroupOptions = connectionPoolGroup.PoolGroupOptions;
				DbConnectionOptions connectionOptions = connectionPoolGroup.ConnectionOptions;
				connectionPoolGroup = this.GetConnectionPoolGroup(connectionPoolGroup.PoolKey, poolGroupOptions, ref connectionOptions);
				this.SetConnectionPoolGroup(owningObject, connectionPoolGroup);
			}
			return connectionPoolGroup.GetConnectionPool(this);
		}

		// Token: 0x06001DB1 RID: 7601 RVA: 0x0008F8C0 File Offset: 0x0008DAC0
		internal DbConnectionPoolGroup GetConnectionPoolGroup(DbConnectionPoolKey key, DbConnectionPoolGroupOptions poolOptions, ref DbConnectionOptions userConnectionOptions)
		{
			if (string.IsNullOrEmpty(key.ConnectionString))
			{
				return null;
			}
			Dictionary<DbConnectionPoolKey, DbConnectionPoolGroup> connectionPoolGroups = this._connectionPoolGroups;
			DbConnectionPoolGroup dbConnectionPoolGroup;
			if (!connectionPoolGroups.TryGetValue(key, out dbConnectionPoolGroup) || (dbConnectionPoolGroup.IsDisabled && dbConnectionPoolGroup.PoolGroupOptions != null))
			{
				DbConnectionOptions dbConnectionOptions = this.CreateConnectionOptions(key.ConnectionString, userConnectionOptions);
				if (dbConnectionOptions == null)
				{
					throw ADP.InternalConnectionError(ADP.ConnectionError.ConnectionOptionsMissing);
				}
				if (userConnectionOptions == null)
				{
					userConnectionOptions = dbConnectionOptions;
				}
				if (poolOptions == null)
				{
					if (dbConnectionPoolGroup != null)
					{
						poolOptions = dbConnectionPoolGroup.PoolGroupOptions;
					}
					else
					{
						poolOptions = this.CreateConnectionPoolGroupOptions(dbConnectionOptions);
					}
				}
				lock (this)
				{
					connectionPoolGroups = this._connectionPoolGroups;
					if (!connectionPoolGroups.TryGetValue(key, out dbConnectionPoolGroup))
					{
						DbConnectionPoolGroup dbConnectionPoolGroup2 = new DbConnectionPoolGroup(dbConnectionOptions, key, poolOptions);
						dbConnectionPoolGroup2.ProviderInfo = this.CreateConnectionPoolGroupProviderInfo(dbConnectionOptions);
						Dictionary<DbConnectionPoolKey, DbConnectionPoolGroup> dictionary = new Dictionary<DbConnectionPoolKey, DbConnectionPoolGroup>(1 + connectionPoolGroups.Count);
						foreach (KeyValuePair<DbConnectionPoolKey, DbConnectionPoolGroup> keyValuePair in connectionPoolGroups)
						{
							dictionary.Add(keyValuePair.Key, keyValuePair.Value);
						}
						dictionary.Add(key, dbConnectionPoolGroup2);
						dbConnectionPoolGroup = dbConnectionPoolGroup2;
						this._connectionPoolGroups = dictionary;
					}
					return dbConnectionPoolGroup;
				}
			}
			if (userConnectionOptions == null)
			{
				userConnectionOptions = dbConnectionPoolGroup.ConnectionOptions;
			}
			return dbConnectionPoolGroup;
		}

		// Token: 0x06001DB2 RID: 7602 RVA: 0x0008FA10 File Offset: 0x0008DC10
		private void PruneConnectionPoolGroups(object state)
		{
			List<DbConnectionPool> poolsToRelease = this._poolsToRelease;
			lock (poolsToRelease)
			{
				if (this._poolsToRelease.Count != 0)
				{
					foreach (DbConnectionPool dbConnectionPool in this._poolsToRelease.ToArray())
					{
						if (dbConnectionPool != null)
						{
							dbConnectionPool.Clear();
							if (dbConnectionPool.Count == 0)
							{
								this._poolsToRelease.Remove(dbConnectionPool);
							}
						}
					}
				}
			}
			List<DbConnectionPoolGroup> poolGroupsToRelease = this._poolGroupsToRelease;
			lock (poolGroupsToRelease)
			{
				if (this._poolGroupsToRelease.Count != 0)
				{
					foreach (DbConnectionPoolGroup dbConnectionPoolGroup in this._poolGroupsToRelease.ToArray())
					{
						if (dbConnectionPoolGroup != null && dbConnectionPoolGroup.Clear() == 0)
						{
							this._poolGroupsToRelease.Remove(dbConnectionPoolGroup);
						}
					}
				}
			}
			lock (this)
			{
				Dictionary<DbConnectionPoolKey, DbConnectionPoolGroup> connectionPoolGroups = this._connectionPoolGroups;
				Dictionary<DbConnectionPoolKey, DbConnectionPoolGroup> dictionary = new Dictionary<DbConnectionPoolKey, DbConnectionPoolGroup>(connectionPoolGroups.Count);
				foreach (KeyValuePair<DbConnectionPoolKey, DbConnectionPoolGroup> keyValuePair in connectionPoolGroups)
				{
					if (keyValuePair.Value != null)
					{
						if (keyValuePair.Value.Prune())
						{
							this.QueuePoolGroupForRelease(keyValuePair.Value);
						}
						else
						{
							dictionary.Add(keyValuePair.Key, keyValuePair.Value);
						}
					}
				}
				this._connectionPoolGroups = dictionary;
			}
		}

		// Token: 0x06001DB3 RID: 7603 RVA: 0x0008FBC8 File Offset: 0x0008DDC8
		internal void QueuePoolForRelease(DbConnectionPool pool, bool clearing)
		{
			pool.Shutdown();
			List<DbConnectionPool> poolsToRelease = this._poolsToRelease;
			lock (poolsToRelease)
			{
				if (clearing)
				{
					pool.Clear();
				}
				this._poolsToRelease.Add(pool);
			}
		}

		// Token: 0x06001DB4 RID: 7604 RVA: 0x0008FC20 File Offset: 0x0008DE20
		internal void QueuePoolGroupForRelease(DbConnectionPoolGroup poolGroup)
		{
			List<DbConnectionPoolGroup> poolGroupsToRelease = this._poolGroupsToRelease;
			lock (poolGroupsToRelease)
			{
				this._poolGroupsToRelease.Add(poolGroup);
			}
		}

		// Token: 0x06001DB5 RID: 7605 RVA: 0x0008FC68 File Offset: 0x0008DE68
		protected virtual DbConnectionInternal CreateConnection(DbConnectionOptions options, DbConnectionPoolKey poolKey, object poolGroupProviderInfo, DbConnectionPool pool, DbConnection owningConnection, DbConnectionOptions userOptions)
		{
			return this.CreateConnection(options, poolKey, poolGroupProviderInfo, pool, owningConnection);
		}

		// Token: 0x06001DB6 RID: 7606 RVA: 0x0008FC78 File Offset: 0x0008DE78
		internal DbMetaDataFactory GetMetaDataFactory(DbConnectionPoolGroup connectionPoolGroup, DbConnectionInternal internalConnection)
		{
			DbMetaDataFactory dbMetaDataFactory = connectionPoolGroup.MetaDataFactory;
			if (dbMetaDataFactory == null)
			{
				bool flag = false;
				dbMetaDataFactory = this.CreateMetaDataFactory(internalConnection, out flag);
				if (flag)
				{
					connectionPoolGroup.MetaDataFactory = dbMetaDataFactory;
				}
			}
			return dbMetaDataFactory;
		}

		// Token: 0x06001DB7 RID: 7607 RVA: 0x0008FCA6 File Offset: 0x0008DEA6
		protected virtual DbMetaDataFactory CreateMetaDataFactory(DbConnectionInternal internalConnection, out bool cacheMetaDataFactory)
		{
			cacheMetaDataFactory = false;
			throw ADP.NotSupported();
		}

		// Token: 0x06001DB8 RID: 7608
		protected abstract DbConnectionInternal CreateConnection(DbConnectionOptions options, DbConnectionPoolKey poolKey, object poolGroupProviderInfo, DbConnectionPool pool, DbConnection owningConnection);

		// Token: 0x06001DB9 RID: 7609
		protected abstract DbConnectionOptions CreateConnectionOptions(string connectionString, DbConnectionOptions previous);

		// Token: 0x06001DBA RID: 7610
		protected abstract DbConnectionPoolGroupOptions CreateConnectionPoolGroupOptions(DbConnectionOptions options);

		// Token: 0x06001DBB RID: 7611
		internal abstract DbConnectionPoolGroup GetConnectionPoolGroup(DbConnection connection);

		// Token: 0x06001DBC RID: 7612
		internal abstract DbConnectionInternal GetInnerConnection(DbConnection connection);

		// Token: 0x06001DBD RID: 7613
		internal abstract void PermissionDemand(DbConnection outerConnection);

		// Token: 0x06001DBE RID: 7614
		internal abstract void SetConnectionPoolGroup(DbConnection outerConnection, DbConnectionPoolGroup poolGroup);

		// Token: 0x06001DBF RID: 7615
		internal abstract void SetInnerConnectionEvent(DbConnection owningObject, DbConnectionInternal to);

		// Token: 0x06001DC0 RID: 7616
		internal abstract bool SetInnerConnectionFrom(DbConnection owningObject, DbConnectionInternal to, DbConnectionInternal from);

		// Token: 0x06001DC1 RID: 7617
		internal abstract void SetInnerConnectionTo(DbConnection owningObject, DbConnectionInternal to);

		// Token: 0x06001DC2 RID: 7618 RVA: 0x0008FCB0 File Offset: 0x0008DEB0
		// Note: this type is marked as 'beforefieldinit'.
		static DbConnectionFactory()
		{
		}

		// Token: 0x040013F0 RID: 5104
		private Dictionary<DbConnectionPoolKey, DbConnectionPoolGroup> _connectionPoolGroups;

		// Token: 0x040013F1 RID: 5105
		private readonly List<DbConnectionPool> _poolsToRelease;

		// Token: 0x040013F2 RID: 5106
		private readonly List<DbConnectionPoolGroup> _poolGroupsToRelease;

		// Token: 0x040013F3 RID: 5107
		private readonly Timer _pruningTimer;

		// Token: 0x040013F4 RID: 5108
		private const int PruningDueTime = 240000;

		// Token: 0x040013F5 RID: 5109
		private const int PruningPeriod = 30000;

		// Token: 0x040013F6 RID: 5110
		private static uint s_pendingOpenNonPooledNext = 0U;

		// Token: 0x040013F7 RID: 5111
		private static Task<DbConnectionInternal>[] s_pendingOpenNonPooled = new Task<DbConnectionInternal>[Environment.ProcessorCount];

		// Token: 0x040013F8 RID: 5112
		private static Task<DbConnectionInternal> s_completedTask;

		// Token: 0x02000277 RID: 631
		[CompilerGenerated]
		private sealed class <>c__DisplayClass22_0
		{
			// Token: 0x06001DC3 RID: 7619 RVA: 0x00005C14 File Offset: 0x00003E14
			public <>c__DisplayClass22_0()
			{
			}

			// Token: 0x06001DC4 RID: 7620 RVA: 0x0008FCC8 File Offset: 0x0008DEC8
			internal DbConnectionInternal <TryGetConnection>b__1(Task<DbConnectionInternal> _)
			{
				Transaction currentTransaction = ADP.GetCurrentTransaction();
				DbConnectionInternal result;
				try
				{
					ADP.SetCurrentTransaction(this.retry.Task.AsyncState as Transaction);
					DbConnectionInternal dbConnectionInternal = this.<>4__this.CreateNonPooledConnection(this.owningConnection, this.poolGroup, this.userOptions);
					if (this.oldConnection != null && this.oldConnection.State == ConnectionState.Open)
					{
						this.oldConnection.PrepareForReplaceConnection();
						this.oldConnection.Dispose();
					}
					result = dbConnectionInternal;
				}
				finally
				{
					ADP.SetCurrentTransaction(currentTransaction);
				}
				return result;
			}

			// Token: 0x040013F9 RID: 5113
			public TaskCompletionSource<DbConnectionInternal> retry;

			// Token: 0x040013FA RID: 5114
			public DbConnectionFactory <>4__this;

			// Token: 0x040013FB RID: 5115
			public DbConnection owningConnection;

			// Token: 0x040013FC RID: 5116
			public DbConnectionPoolGroup poolGroup;

			// Token: 0x040013FD RID: 5117
			public DbConnectionOptions userOptions;

			// Token: 0x040013FE RID: 5118
			public DbConnectionInternal oldConnection;

			// Token: 0x040013FF RID: 5119
			public Func<Task<DbConnectionInternal>, DbConnectionInternal> <>9__1;
		}

		// Token: 0x02000278 RID: 632
		[CompilerGenerated]
		private sealed class <>c__DisplayClass22_1
		{
			// Token: 0x06001DC5 RID: 7621 RVA: 0x00005C14 File Offset: 0x00003E14
			public <>c__DisplayClass22_1()
			{
			}

			// Token: 0x06001DC6 RID: 7622 RVA: 0x0008FD5C File Offset: 0x0008DF5C
			internal void <TryGetConnection>b__0(Task<DbConnectionInternal> task)
			{
				this.cancellationTokenSource.Dispose();
				if (task.IsCanceled)
				{
					this.CS$<>8__locals1.retry.TrySetException(ADP.ExceptionWithStackTrace(ADP.NonPooledOpenTimeout()));
					return;
				}
				if (task.IsFaulted)
				{
					this.CS$<>8__locals1.retry.TrySetException(task.Exception.InnerException);
					return;
				}
				if (!this.CS$<>8__locals1.retry.TrySetResult(task.Result))
				{
					task.Result.DoomThisConnection();
					task.Result.Dispose();
				}
			}

			// Token: 0x04001400 RID: 5120
			public CancellationTokenSource cancellationTokenSource;

			// Token: 0x04001401 RID: 5121
			public DbConnectionFactory.<>c__DisplayClass22_0 CS$<>8__locals1;
		}
	}
}
