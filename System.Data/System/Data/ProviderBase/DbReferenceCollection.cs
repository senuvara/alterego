﻿using System;
using System.Threading;

namespace System.Data.ProviderBase
{
	// Token: 0x02000286 RID: 646
	internal abstract class DbReferenceCollection
	{
		// Token: 0x06001E76 RID: 7798 RVA: 0x000928E5 File Offset: 0x00090AE5
		protected DbReferenceCollection()
		{
			this._items = new DbReferenceCollection.CollectionEntry[20];
			this._itemLock = new object();
			this._optimisticCount = 0;
			this._lastItemIndex = 0;
		}

		// Token: 0x06001E77 RID: 7799
		public abstract void Add(object value, int tag);

		// Token: 0x06001E78 RID: 7800 RVA: 0x00092914 File Offset: 0x00090B14
		protected void AddItem(object value, int tag)
		{
			bool flag = false;
			object itemLock = this._itemLock;
			lock (itemLock)
			{
				for (int i = 0; i <= this._lastItemIndex; i++)
				{
					if (this._items[i].Tag == 0)
					{
						this._items[i].NewTarget(tag, value);
						flag = true;
						break;
					}
				}
				if (!flag && this._lastItemIndex + 1 < this._items.Length)
				{
					this._lastItemIndex++;
					this._items[this._lastItemIndex].NewTarget(tag, value);
					flag = true;
				}
				if (!flag)
				{
					for (int j = 0; j <= this._lastItemIndex; j++)
					{
						if (!this._items[j].HasTarget)
						{
							this._items[j].NewTarget(tag, value);
							flag = true;
							break;
						}
					}
				}
				if (!flag)
				{
					Array.Resize<DbReferenceCollection.CollectionEntry>(ref this._items, this._items.Length * 2);
					this._lastItemIndex++;
					this._items[this._lastItemIndex].NewTarget(tag, value);
				}
				this._optimisticCount++;
			}
		}

		// Token: 0x06001E79 RID: 7801 RVA: 0x00092A64 File Offset: 0x00090C64
		internal T FindItem<T>(int tag, Func<T, bool> filterMethod) where T : class
		{
			bool flag = false;
			try
			{
				this.TryEnterItemLock(ref flag);
				if (flag && this._optimisticCount > 0)
				{
					for (int i = 0; i <= this._lastItemIndex; i++)
					{
						if (this._items[i].Tag == tag)
						{
							object target = this._items[i].Target;
							if (target != null)
							{
								T t = target as T;
								if (t != null && filterMethod(t))
								{
									return t;
								}
							}
						}
					}
				}
			}
			finally
			{
				this.ExitItemLockIfNeeded(flag);
			}
			return default(T);
		}

		// Token: 0x06001E7A RID: 7802 RVA: 0x00092B0C File Offset: 0x00090D0C
		public void Notify(int message)
		{
			bool flag = false;
			try
			{
				this.TryEnterItemLock(ref flag);
				if (flag)
				{
					try
					{
						this._isNotifying = true;
						if (this._optimisticCount > 0)
						{
							for (int i = 0; i <= this._lastItemIndex; i++)
							{
								object target = this._items[i].Target;
								if (target != null)
								{
									this.NotifyItem(message, this._items[i].Tag, target);
									this._items[i].RemoveTarget();
								}
							}
							this._optimisticCount = 0;
						}
						if (this._items.Length > 100)
						{
							this._lastItemIndex = 0;
							this._items = new DbReferenceCollection.CollectionEntry[20];
						}
					}
					finally
					{
						this._isNotifying = false;
					}
				}
			}
			finally
			{
				this.ExitItemLockIfNeeded(flag);
			}
		}

		// Token: 0x06001E7B RID: 7803
		protected abstract void NotifyItem(int message, int tag, object value);

		// Token: 0x06001E7C RID: 7804
		public abstract void Remove(object value);

		// Token: 0x06001E7D RID: 7805 RVA: 0x00092BE4 File Offset: 0x00090DE4
		protected void RemoveItem(object value)
		{
			bool flag = false;
			try
			{
				this.TryEnterItemLock(ref flag);
				if (flag && this._optimisticCount > 0)
				{
					for (int i = 0; i <= this._lastItemIndex; i++)
					{
						if (value == this._items[i].Target)
						{
							this._items[i].RemoveTarget();
							this._optimisticCount--;
							break;
						}
					}
				}
			}
			finally
			{
				this.ExitItemLockIfNeeded(flag);
			}
		}

		// Token: 0x06001E7E RID: 7806 RVA: 0x00092C68 File Offset: 0x00090E68
		private void TryEnterItemLock(ref bool lockObtained)
		{
			lockObtained = false;
			while (!this._isNotifying && !lockObtained)
			{
				Monitor.TryEnter(this._itemLock, 100, ref lockObtained);
			}
		}

		// Token: 0x06001E7F RID: 7807 RVA: 0x00092C8B File Offset: 0x00090E8B
		private void ExitItemLockIfNeeded(bool lockObtained)
		{
			if (lockObtained)
			{
				Monitor.Exit(this._itemLock);
			}
		}

		// Token: 0x04001469 RID: 5225
		private const int LockPollTime = 100;

		// Token: 0x0400146A RID: 5226
		private const int DefaultCollectionSize = 20;

		// Token: 0x0400146B RID: 5227
		private DbReferenceCollection.CollectionEntry[] _items;

		// Token: 0x0400146C RID: 5228
		private readonly object _itemLock;

		// Token: 0x0400146D RID: 5229
		private int _optimisticCount;

		// Token: 0x0400146E RID: 5230
		private int _lastItemIndex;

		// Token: 0x0400146F RID: 5231
		private volatile bool _isNotifying;

		// Token: 0x02000287 RID: 647
		private struct CollectionEntry
		{
			// Token: 0x06001E80 RID: 7808 RVA: 0x00092C9B File Offset: 0x00090E9B
			public void NewTarget(int tag, object target)
			{
				if (this._weak == null)
				{
					this._weak = new WeakReference(target, false);
				}
				else
				{
					this._weak.Target = target;
				}
				this._tag = tag;
			}

			// Token: 0x06001E81 RID: 7809 RVA: 0x00092CC7 File Offset: 0x00090EC7
			public void RemoveTarget()
			{
				this._tag = 0;
			}

			// Token: 0x1700053F RID: 1343
			// (get) Token: 0x06001E82 RID: 7810 RVA: 0x00092CD0 File Offset: 0x00090ED0
			public bool HasTarget
			{
				get
				{
					return this._tag != 0 && this._weak.IsAlive;
				}
			}

			// Token: 0x17000540 RID: 1344
			// (get) Token: 0x06001E83 RID: 7811 RVA: 0x00092CE7 File Offset: 0x00090EE7
			public int Tag
			{
				get
				{
					return this._tag;
				}
			}

			// Token: 0x17000541 RID: 1345
			// (get) Token: 0x06001E84 RID: 7812 RVA: 0x00092CEF File Offset: 0x00090EEF
			public object Target
			{
				get
				{
					if (this._tag != 0)
					{
						return this._weak.Target;
					}
					return null;
				}
			}

			// Token: 0x04001470 RID: 5232
			private int _tag;

			// Token: 0x04001471 RID: 5233
			private WeakReference _weak;
		}
	}
}
