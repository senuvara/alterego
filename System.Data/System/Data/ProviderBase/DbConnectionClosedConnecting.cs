﻿using System;
using System.Data.Common;
using System.Threading.Tasks;

namespace System.Data.ProviderBase
{
	// Token: 0x02000273 RID: 627
	internal sealed class DbConnectionClosedConnecting : DbConnectionBusy
	{
		// Token: 0x06001D99 RID: 7577 RVA: 0x0008F35F File Offset: 0x0008D55F
		private DbConnectionClosedConnecting() : base(ConnectionState.Connecting)
		{
		}

		// Token: 0x06001D9A RID: 7578 RVA: 0x0008F368 File Offset: 0x0008D568
		internal override void CloseConnection(DbConnection owningObject, DbConnectionFactory connectionFactory)
		{
			connectionFactory.SetInnerConnectionTo(owningObject, DbConnectionClosedPreviouslyOpened.SingletonInstance);
		}

		// Token: 0x06001D9B RID: 7579 RVA: 0x0008F376 File Offset: 0x0008D576
		internal override bool TryReplaceConnection(DbConnection outerConnection, DbConnectionFactory connectionFactory, TaskCompletionSource<DbConnectionInternal> retry, DbConnectionOptions userOptions)
		{
			return this.TryOpenConnection(outerConnection, connectionFactory, retry, userOptions);
		}

		// Token: 0x06001D9C RID: 7580 RVA: 0x0008F384 File Offset: 0x0008D584
		internal override bool TryOpenConnection(DbConnection outerConnection, DbConnectionFactory connectionFactory, TaskCompletionSource<DbConnectionInternal> retry, DbConnectionOptions userOptions)
		{
			if (retry == null || !retry.Task.IsCompleted)
			{
				throw ADP.ConnectionAlreadyOpen(base.State);
			}
			DbConnectionInternal result = retry.Task.Result;
			if (result == null)
			{
				connectionFactory.SetInnerConnectionTo(outerConnection, this);
				throw ADP.InternalConnectionError(ADP.ConnectionError.GetConnectionReturnsNull);
			}
			connectionFactory.SetInnerConnectionEvent(outerConnection, result);
			return true;
		}

		// Token: 0x06001D9D RID: 7581 RVA: 0x0008F3D4 File Offset: 0x0008D5D4
		// Note: this type is marked as 'beforefieldinit'.
		static DbConnectionClosedConnecting()
		{
		}

		// Token: 0x040013ED RID: 5101
		internal static readonly DbConnectionInternal SingletonInstance = new DbConnectionClosedConnecting();
	}
}
