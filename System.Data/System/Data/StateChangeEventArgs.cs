﻿using System;

namespace System.Data
{
	/// <summary>Provides data for the state change event of a .NET Framework data provider.</summary>
	// Token: 0x020000F6 RID: 246
	public sealed class StateChangeEventArgs : EventArgs
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Data.StateChangeEventArgs" /> class, when given the original state and the current state of the object.</summary>
		/// <param name="originalState">One of the <see cref="T:System.Data.ConnectionState" /> values. </param>
		/// <param name="currentState">One of the <see cref="T:System.Data.ConnectionState" /> values. </param>
		// Token: 0x06000CEA RID: 3306 RVA: 0x0003C1F2 File Offset: 0x0003A3F2
		public StateChangeEventArgs(ConnectionState originalState, ConnectionState currentState)
		{
			this._originalState = originalState;
			this._currentState = currentState;
		}

		/// <summary>Gets the new state of the connection. The connection object will be in the new state already when the event is fired.</summary>
		/// <returns>One of the <see cref="T:System.Data.ConnectionState" /> values.</returns>
		// Token: 0x17000243 RID: 579
		// (get) Token: 0x06000CEB RID: 3307 RVA: 0x0003C208 File Offset: 0x0003A408
		public ConnectionState CurrentState
		{
			get
			{
				return this._currentState;
			}
		}

		/// <summary>Gets the original state of the connection.</summary>
		/// <returns>One of the <see cref="T:System.Data.ConnectionState" /> values.</returns>
		// Token: 0x17000244 RID: 580
		// (get) Token: 0x06000CEC RID: 3308 RVA: 0x0003C210 File Offset: 0x0003A410
		public ConnectionState OriginalState
		{
			get
			{
				return this._originalState;
			}
		}

		// Token: 0x0400089E RID: 2206
		private ConnectionState _originalState;

		// Token: 0x0400089F RID: 2207
		private ConnectionState _currentState;
	}
}
