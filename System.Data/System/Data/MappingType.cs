﻿using System;

namespace System.Data
{
	/// <summary>Specifies how a <see cref="T:System.Data.DataColumn" /> is mapped.</summary>
	// Token: 0x020000D2 RID: 210
	public enum MappingType
	{
		/// <summary>The column is mapped to an XML element.</summary>
		// Token: 0x040007D8 RID: 2008
		Element = 1,
		/// <summary>The column is mapped to an XML attribute.</summary>
		// Token: 0x040007D9 RID: 2009
		Attribute,
		/// <summary>The column is mapped to an <see cref="T:System.Xml.XmlText" /> node.</summary>
		// Token: 0x040007DA RID: 2010
		SimpleContent,
		/// <summary>The column is mapped to an internal structure.</summary>
		// Token: 0x040007DB RID: 2011
		Hidden
	}
}
