﻿using System;

namespace System.Data
{
	// Token: 0x020000B8 RID: 184
	internal enum FunctionId
	{
		// Token: 0x0400073C RID: 1852
		none = -1,
		// Token: 0x0400073D RID: 1853
		Ascii,
		// Token: 0x0400073E RID: 1854
		Char,
		// Token: 0x0400073F RID: 1855
		Charindex,
		// Token: 0x04000740 RID: 1856
		Difference,
		// Token: 0x04000741 RID: 1857
		Len,
		// Token: 0x04000742 RID: 1858
		Lower,
		// Token: 0x04000743 RID: 1859
		LTrim,
		// Token: 0x04000744 RID: 1860
		Patindex,
		// Token: 0x04000745 RID: 1861
		Replicate,
		// Token: 0x04000746 RID: 1862
		Reverse,
		// Token: 0x04000747 RID: 1863
		Right,
		// Token: 0x04000748 RID: 1864
		RTrim,
		// Token: 0x04000749 RID: 1865
		Soundex,
		// Token: 0x0400074A RID: 1866
		Space,
		// Token: 0x0400074B RID: 1867
		Str,
		// Token: 0x0400074C RID: 1868
		Stuff,
		// Token: 0x0400074D RID: 1869
		Substring,
		// Token: 0x0400074E RID: 1870
		Upper,
		// Token: 0x0400074F RID: 1871
		IsNull,
		// Token: 0x04000750 RID: 1872
		Iif,
		// Token: 0x04000751 RID: 1873
		Convert,
		// Token: 0x04000752 RID: 1874
		cInt,
		// Token: 0x04000753 RID: 1875
		cBool,
		// Token: 0x04000754 RID: 1876
		cDate,
		// Token: 0x04000755 RID: 1877
		cDbl,
		// Token: 0x04000756 RID: 1878
		cStr,
		// Token: 0x04000757 RID: 1879
		Abs,
		// Token: 0x04000758 RID: 1880
		Acos,
		// Token: 0x04000759 RID: 1881
		In,
		// Token: 0x0400075A RID: 1882
		Trim,
		// Token: 0x0400075B RID: 1883
		Sum,
		// Token: 0x0400075C RID: 1884
		Avg,
		// Token: 0x0400075D RID: 1885
		Min,
		// Token: 0x0400075E RID: 1886
		Max,
		// Token: 0x0400075F RID: 1887
		Count,
		// Token: 0x04000760 RID: 1888
		StDev,
		// Token: 0x04000761 RID: 1889
		Var = 37,
		// Token: 0x04000762 RID: 1890
		DateTimeOffset
	}
}
