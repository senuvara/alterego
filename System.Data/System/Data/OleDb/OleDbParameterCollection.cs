﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data.Common;
using Unity;

namespace System.Data.OleDb
{
	/// <summary>Represents a collection of parameters relevant to an <see cref="T:System.Data.OleDb.OleDbCommand" /> as well as their respective mappings to columns in a <see cref="T:System.Data.DataSet" />. </summary>
	// Token: 0x0200036D RID: 877
	[ListBindable(false)]
	[Editor("Microsoft.VSDesigner.Data.Design.DBParametersEditor, Microsoft.VSDesigner, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a", "System.Drawing.Design.UITypeEditor, System.Drawing, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")]
	public sealed class OleDbParameterCollection : DbParameterCollection
	{
		// Token: 0x06002A41 RID: 10817 RVA: 0x00010458 File Offset: 0x0000E658
		internal OleDbParameterCollection()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Returns an integer that contains the number of elements in the <see cref="T:System.Data.OleDb.OleDbParameterCollection" />. Read-only. </summary>
		/// <returns>The number of elements in the <see cref="T:System.Data.OleDb.OleDbParameterCollection" /> as an integer.</returns>
		// Token: 0x170006F8 RID: 1784
		// (get) Token: 0x06002A42 RID: 10818 RVA: 0x000B7150 File Offset: 0x000B5350
		public override int Count
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
		}

		// Token: 0x06002A43 RID: 10819 RVA: 0x00010458 File Offset: 0x0000E658
		public void set_Item(int index, OleDbParameter value)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the <see cref="T:System.Data.OleDb.OleDbParameter" /> with the specified name.</summary>
		/// <param name="parameterName">The name of the parameter to retrieve. </param>
		/// <returns>The <see cref="T:System.Data.OleDb.OleDbParameter" /> with the specified name.</returns>
		/// <exception cref="T:System.IndexOutOfRangeException">The name specified does not exist. </exception>
		// Token: 0x170006F9 RID: 1785
		public string this[string parameterName]
		{
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets an object that can be used to synchronize access to the <see cref="T:System.Data.OleDb.OleDbParameterCollection" />. Read-only.</summary>
		/// <returns>An object that can be used to synchronize access to the <see cref="T:System.Data.OleDb.OleDbParameterCollection" />.</returns>
		// Token: 0x170006FA RID: 1786
		// (get) Token: 0x06002A45 RID: 10821 RVA: 0x00051759 File Offset: 0x0004F959
		public override object SyncRoot
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Adds the specified <see cref="T:System.Data.OleDb.OleDbParameter" /> to the <see cref="T:System.Data.OleDb.OleDbParameterCollection" />.</summary>
		/// <param name="value">The <see cref="T:System.Data.OleDb.OleDbParameter" /> to add to the collection.</param>
		/// <returns>The index of the new <see cref="T:System.Data.OleDb.OleDbParameter" /> object.</returns>
		/// <exception cref="T:System.ArgumentException">The <see cref="T:System.Data.OleDb.OleDbParameter" /> specified in the <paramref name="value" /> parameter is already added to this or another <see cref="T:System.Data.OleDb.OleDbParameterCollection" />. </exception>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="value" /> parameter is null. </exception>
		// Token: 0x06002A46 RID: 10822 RVA: 0x00051759 File Offset: 0x0004F959
		public OleDbParameter Add(OleDbParameter value)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Adds the specified <see cref="T:System.Data.OleDb.OleDbParameter" /> object to the <see cref="T:System.Data.OleDb.OleDbParameterCollection" />.</summary>
		/// <param name="value">A <see cref="T:System.Object" />.</param>
		/// <returns>The index of the new <see cref="T:System.Data.OleDb.OleDbParameter" /> object in the collection.</returns>
		// Token: 0x06002A47 RID: 10823 RVA: 0x000B716C File Offset: 0x000B536C
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override int Add(object value)
		{
			ThrowStub.ThrowNotSupportedException();
			return 0;
		}

		/// <summary>Adds an <see cref="T:System.Data.OleDb.OleDbParameter" /> to the <see cref="T:System.Data.OleDb.OleDbParameterCollection" />, given the parameter name and data type.</summary>
		/// <param name="parameterName">The name of the parameter. </param>
		/// <param name="oleDbType">One of the <see cref="T:System.Data.OleDb.OleDbType" /> values. </param>
		/// <returns>The index of the new <see cref="T:System.Data.OleDb.OleDbParameter" /> object.</returns>
		// Token: 0x06002A48 RID: 10824 RVA: 0x00051759 File Offset: 0x0004F959
		public OleDbParameter Add(string parameterName, OleDbType oleDbType)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Adds an <see cref="T:System.Data.OleDb.OleDbParameter" /> to the <see cref="T:System.Data.OleDb.OleDbParameterCollection" /> given the parameter name, data type, and column length.</summary>
		/// <param name="parameterName">The name of the parameter. </param>
		/// <param name="oleDbType">One of the <see cref="T:System.Data.OleDb.OleDbType" /> values. </param>
		/// <param name="size">The length of the column. </param>
		/// <returns>The index of the new <see cref="T:System.Data.OleDb.OleDbParameter" /> object.</returns>
		// Token: 0x06002A49 RID: 10825 RVA: 0x00051759 File Offset: 0x0004F959
		public OleDbParameter Add(string parameterName, OleDbType oleDbType, int size)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Adds an <see cref="T:System.Data.OleDb.OleDbParameter" /> to the <see cref="T:System.Data.OleDb.OleDbParameterCollection" /> given the parameter name, data type, column length, and source column name.</summary>
		/// <param name="parameterName">The name of the parameter. </param>
		/// <param name="oleDbType">One of the <see cref="T:System.Data.OleDb.OleDbType" /> values. </param>
		/// <param name="size">The length of the column. </param>
		/// <param name="sourceColumn">The name of the source column. </param>
		/// <returns>The index of the new <see cref="T:System.Data.OleDb.OleDbParameter" /> object.</returns>
		// Token: 0x06002A4A RID: 10826 RVA: 0x00051759 File Offset: 0x0004F959
		public OleDbParameter Add(string parameterName, OleDbType oleDbType, int size, string sourceColumn)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Adds an <see cref="T:System.Data.OleDb.OleDbParameter" /> to the <see cref="T:System.Data.OleDb.OleDbParameterCollection" /> given the parameter name and value.</summary>
		/// <param name="parameterName">The name of the parameter. </param>
		/// <param name="value">The <see cref="P:System.Data.OleDb.OleDbParameter.Value" /> of the <see cref="T:System.Data.OleDb.OleDbParameter" /> to add to the collection. </param>
		/// <returns>The index of the new <see cref="T:System.Data.OleDb.OleDbParameter" /> object.</returns>
		/// <exception cref="T:System.InvalidCastException">The <paramref name="value" /> parameter is not an <see cref="T:System.Data.OleDb.OleDbParameter" />. </exception>
		// Token: 0x06002A4B RID: 10827 RVA: 0x00051759 File Offset: 0x0004F959
		[Obsolete("Add(String parameterName, Object value) has been deprecated.  Use AddWithValue(String parameterName, Object value).  http://go.microsoft.com/fwlink/?linkid=14202", false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public OleDbParameter Add(string parameterName, object value)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Adds an array of values to the end of the <see cref="T:System.Data.OleDb.OleDbParameterCollection" />.</summary>
		/// <param name="values">The <see cref="T:System.Array" /> values to add.</param>
		// Token: 0x06002A4C RID: 10828 RVA: 0x00010458 File Offset: 0x0000E658
		public override void AddRange(Array values)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Adds an array of <see cref="T:System.Data.OleDb.OleDbParameter" /> values to the end of the <see cref="T:System.Data.OleDb.OleDbParameterCollection" />.</summary>
		/// <param name="values">The <see cref="T:System.Data.OleDbParameter" /> values to add.</param>
		// Token: 0x06002A4D RID: 10829 RVA: 0x00010458 File Offset: 0x0000E658
		public void AddRange(OleDbParameter[] values)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Adds a value to the end of the <see cref="T:System.Data.OleDb.OleDbParameterCollection" />.</summary>
		/// <param name="parameterName">The name of the parameter.</param>
		/// <param name="value">The value to be added.</param>
		/// <returns>An <see cref="T:System.Data.OleDb.OleDbParameter" /> object.</returns>
		// Token: 0x06002A4E RID: 10830 RVA: 0x00051759 File Offset: 0x0004F959
		public OleDbParameter AddWithValue(string parameterName, object value)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Removes all <see cref="T:System.Data.OleDb.OleDbParameter" /> objects from the <see cref="T:System.Data.OleDb.OleDbParameterCollection" />.</summary>
		// Token: 0x06002A4F RID: 10831 RVA: 0x00010458 File Offset: 0x0000E658
		public override void Clear()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Determines whether the specified <see cref="T:System.Data.OleDb.OleDbParameter" /> is in this <see cref="T:System.Data.OleDb.OleDbParameterCollection" />.</summary>
		/// <param name="value">The <see cref="T:System.Data.OleDb.OleDbParameter" /> value.</param>
		/// <returns>
		///     <see langword="true" /> if the <see cref="T:System.Data.OleDb.OleDbParameter" /> is in the collection; otherwise <see langword="false" />.</returns>
		// Token: 0x06002A50 RID: 10832 RVA: 0x000B7188 File Offset: 0x000B5388
		public bool Contains(OleDbParameter value)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Determines whether the specified <see cref="T:System.Object" /> is in this <see cref="T:System.Data.OleDb.OleDbParameterCollection" />.</summary>
		/// <param name="value">The <see cref="T:System.Object" /> value.</param>
		/// <returns>
		///     <see langword="true" /> if the <see cref="T:System.Data.OleDb.OleDbParameterCollection" /> contains <paramref name="value" />; otherwise <see langword="false" />.</returns>
		// Token: 0x06002A51 RID: 10833 RVA: 0x000B71A4 File Offset: 0x000B53A4
		public override bool Contains(object value)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Determines whether the specified <see cref="T:System.String" /> is in this <see cref="T:System.Data.OleDb.OleDbParameterCollection" />.</summary>
		/// <param name="value">The <see cref="T:System.String" /> value.</param>
		/// <returns>
		///     <see langword="true" /> if the <see cref="T:System.Data.OleDb.OleDbParameterCollection" /> contains the value; otherwise <see langword="false" />.</returns>
		// Token: 0x06002A52 RID: 10834 RVA: 0x000B71C0 File Offset: 0x000B53C0
		public override bool Contains(string value)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Copies all the elements of the current <see cref="T:System.Data.OleDb.OleDbParameterCollection" /> to the specified one-dimensional <see cref="T:System.Array" /> starting at the specified destination <see cref="T:System.Array" /> index.</summary>
		/// <param name="array">The one-dimensional <see cref="T:System.Array" /> that is the destination of the elements copied from the current <see cref="T:System.Data.OleDb.OleDbParameterCollection" />.</param>
		/// <param name="index">A 32-bit integer that represents the index in the <see cref="T:System.Array" /> at which copying starts.</param>
		// Token: 0x06002A53 RID: 10835 RVA: 0x00010458 File Offset: 0x0000E658
		public override void CopyTo(Array array, int index)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Copies all the elements of the current <see cref="T:System.Data.OleDb.OleDbParameterCollection" /> to the specified <see cref="T:System.Data.OleDb.OleDbParameterCollection" /> starting at the specified destination index.</summary>
		/// <param name="array">The <see cref="T:System.Data.OleDb.OleDbParameterCollection" /> that is the destination of the elements copied from the current <see cref="T:System.Data.OleDb.OleDbParameterCollection" />.</param>
		/// <param name="index">A 32-bit integer that represents the index in the <see cref="T:System.Data.OleDb.OleDbParameterCollection" /> at which copying starts.</param>
		// Token: 0x06002A54 RID: 10836 RVA: 0x00010458 File Offset: 0x0000E658
		public void CopyTo(OleDbParameter[] array, int index)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Returns an enumerator that iterates through the <see cref="T:System.Data.OleDb.OleDbParameterCollection" />.</summary>
		/// <returns>An <see cref="T:System.Collections.Generic.IEnumerator" /> for the <see cref="T:System.Data.OleDb.OleDbParameterCollection" />.</returns>
		// Token: 0x06002A55 RID: 10837 RVA: 0x00051759 File Offset: 0x0004F959
		public override IEnumerator GetEnumerator()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		// Token: 0x06002A56 RID: 10838 RVA: 0x00051759 File Offset: 0x0004F959
		protected override DbParameter GetParameter(int index)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		// Token: 0x06002A57 RID: 10839 RVA: 0x00051759 File Offset: 0x0004F959
		protected override DbParameter GetParameter(string parameterName)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Gets the location of the specified <see cref="T:System.Data.OleDb.OleDbParameter" /> within the collection.</summary>
		/// <param name="value">The <see cref="T:System.Data.OleDb.OleDbParameter" /> object in the collection to find.</param>
		/// <returns>The zero-based location of the specified <see cref="T:System.Data.OleDb.OleDbParameter" /> that is a <see cref="T:System.Data.OleDb.OleDbParameter" /> within the collection.</returns>
		// Token: 0x06002A58 RID: 10840 RVA: 0x000B71DC File Offset: 0x000B53DC
		public int IndexOf(OleDbParameter value)
		{
			ThrowStub.ThrowNotSupportedException();
			return 0;
		}

		/// <summary>The location of the specified <see cref="T:System.Object" /> within the collection.</summary>
		/// <param name="value">The <see cref="T:System.Object" /> to find.</param>
		/// <returns>The zero-based location of the specified <see cref="T:System.Object" /> that is a <see cref="T:System.Data.OleDb.OleDbParameterCollection" /> within the collection.</returns>
		// Token: 0x06002A59 RID: 10841 RVA: 0x000B71F8 File Offset: 0x000B53F8
		public override int IndexOf(object value)
		{
			ThrowStub.ThrowNotSupportedException();
			return 0;
		}

		/// <summary>Gets the location of the specified <see cref="T:System.Data.OleDb.OleDbParameter" /> with the specified name.</summary>
		/// <param name="parameterName">The case-sensitive name of the <see cref="T:System.Data.OleDb.OleDbParameter" /> to find.</param>
		/// <returns>The zero-based location of the specified <see cref="T:System.Data.OleDb.OleDbParameter" /> with the specified case-sensitive name.</returns>
		// Token: 0x06002A5A RID: 10842 RVA: 0x000B7214 File Offset: 0x000B5414
		public override int IndexOf(string parameterName)
		{
			ThrowStub.ThrowNotSupportedException();
			return 0;
		}

		/// <summary>Inserts a <see cref="T:System.Data.OleDb.OleDbParameter" /> object into the <see cref="T:System.Data.OleDb.OleDbParameterCollection" /> at the specified index.</summary>
		/// <param name="index">The zero-based index at which value should be inserted.</param>
		/// <param name="value">An <see cref="T:System.Data.OleDb.OleDbParameter" /> object to be inserted in the <see cref="T:System.Data.OleDb.OleDbParameterCollection" />.</param>
		// Token: 0x06002A5B RID: 10843 RVA: 0x00010458 File Offset: 0x0000E658
		public void Insert(int index, OleDbParameter value)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Inserts a <see cref="T:System.Object" /> into the <see cref="T:System.Data.OleDb.OleDbParameterCollection" /> at the specified index.</summary>
		/// <param name="index">The zero-based index at which value should be inserted.</param>
		/// <param name="value">A <see cref="T:System.Object" /> to be inserted in the <see cref="T:System.Data.OleDb.OleDbParameterCollection" />.</param>
		// Token: 0x06002A5C RID: 10844 RVA: 0x00010458 File Offset: 0x0000E658
		public override void Insert(int index, object value)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Removes the <see cref="T:System.Data.OleDb.OleDbParameter" /> from the <see cref="T:System.Data.OleDb.OleDbParameterCollection" />.</summary>
		/// <param name="value">An <see cref="T:System.Data.OleDb.OleDbParameter" /> object to remove from the collection.</param>
		/// <exception cref="T:System.InvalidCastException">The parameter is not a <see cref="T:System.Data.OleDb.OleDbParameter" />. </exception>
		/// <exception cref="T:System.SystemException">The parameter does not exist in the collection. </exception>
		// Token: 0x06002A5D RID: 10845 RVA: 0x00010458 File Offset: 0x0000E658
		public void Remove(OleDbParameter value)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Removes the <see cref="T:System.Object" /> object from the <see cref="T:System.Data.OleDb.OleDbParameterCollection" />.</summary>
		/// <param name="value">An <see cref="T:System.Object" /> to be removed from the <see cref="T:System.Data.OleDb.OleDbParameterCollection" />.</param>
		// Token: 0x06002A5E RID: 10846 RVA: 0x00010458 File Offset: 0x0000E658
		public override void Remove(object value)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Removes the <see cref="T:System.Data.OleDb.OleDbParameter" /> from the <see cref="T:System.Data.OleDb.OleDbParameterCollection" /> at the specified index.</summary>
		/// <param name="index">The zero-based index of the <see cref="T:System.Data.OleDb.OleDbParameter" /> object to remove.</param>
		// Token: 0x06002A5F RID: 10847 RVA: 0x00010458 File Offset: 0x0000E658
		public override void RemoveAt(int index)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Removes the <see cref="T:System.Data.OleDb.OleDbParameter" /> from the <see cref="T:System.Data.OleDb.OleDbParameterCollection" /> at the specified parameter name.</summary>
		/// <param name="parameterName">The name of the <see cref="T:System.Data.OleDb.OleDbParameter" /> object to remove.</param>
		// Token: 0x06002A60 RID: 10848 RVA: 0x00010458 File Offset: 0x0000E658
		public override void RemoveAt(string parameterName)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x06002A61 RID: 10849 RVA: 0x00010458 File Offset: 0x0000E658
		protected override void SetParameter(int index, DbParameter value)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x06002A62 RID: 10850 RVA: 0x00010458 File Offset: 0x0000E658
		protected override void SetParameter(string parameterName, DbParameter value)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
