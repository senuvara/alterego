﻿using System;
using System.Data.Common;
using System.Security;
using System.Security.Permissions;
using Unity;

namespace System.Data.OleDb
{
	/// <summary>Associates a security action with a custom security attribute.</summary>
	// Token: 0x0200036F RID: 879
	[AttributeUsage(AttributeTargets.Assembly | AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Constructor | AttributeTargets.Method, AllowMultiple = true, Inherited = false)]
	[Serializable]
	public sealed class OleDbPermissionAttribute : DBDataPermissionAttribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Data.OleDb.OleDbPermissionAttribute" /> class.</summary>
		/// <param name="action">One of the <see cref="T:System.Security.Permissions.SecurityAction" /> values representing an action that can be performed by using declarative security. </param>
		// Token: 0x06002A68 RID: 10856 RVA: 0x00005E03 File Offset: 0x00004003
		public OleDbPermissionAttribute(SecurityAction action)
		{
		}

		/// <summary>Gets or sets a comma-delimited string that contains a list of supported providers.</summary>
		/// <returns>A comma-delimited list of providers allowed by the security policy.</returns>
		// Token: 0x170006FC RID: 1788
		// (get) Token: 0x06002A69 RID: 10857 RVA: 0x00051759 File Offset: 0x0004F959
		// (set) Token: 0x06002A6A RID: 10858 RVA: 0x00010458 File Offset: 0x0000E658
		public string Provider
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Returns an <see cref="T:System.Data.OleDb.OleDbPermission" /> object that is configured according to the attribute properties.</summary>
		/// <returns>An <see cref="T:System.Data.OleDb.OleDbPermission" /> object.</returns>
		// Token: 0x06002A6B RID: 10859 RVA: 0x00051759 File Offset: 0x0004F959
		public override IPermission CreatePermission()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}
	}
}
