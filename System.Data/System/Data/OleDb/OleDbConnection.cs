﻿using System;
using System.ComponentModel;
using System.Data.Common;
using System.EnterpriseServices;
using Unity;

namespace System.Data.OleDb
{
	/// <summary>Represents an open connection to a data source.</summary>
	// Token: 0x02000358 RID: 856
	[DefaultEvent("InfoMessage")]
	public sealed class OleDbConnection : DbConnection, ICloneable
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Data.OleDb.OleDbConnection" /> class.</summary>
		// Token: 0x0600299B RID: 10651 RVA: 0x00010458 File Offset: 0x0000E658
		public OleDbConnection()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Data.OleDb.OleDbConnection" /> class with the specified connection string.</summary>
		/// <param name="connectionString">The connection used to open the database. </param>
		// Token: 0x0600299C RID: 10652 RVA: 0x00010458 File Offset: 0x0000E658
		public OleDbConnection(string connectionString)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the string used to open a database.</summary>
		/// <returns>The OLE DB provider connection string that includes the data source name, and other parameters needed to establish the initial connection. The default value is an empty string.</returns>
		/// <exception cref="T:System.ArgumentException">An invalid connection string argument has been supplied or a required connection string argument has not been supplied. </exception>
		// Token: 0x170006CB RID: 1739
		// (get) Token: 0x0600299D RID: 10653 RVA: 0x00051759 File Offset: 0x0004F959
		// (set) Token: 0x0600299E RID: 10654 RVA: 0x00010458 File Offset: 0x0000E658
		public override string ConnectionString
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets the name of the current database or the database to be used after a connection is opened.</summary>
		/// <returns>The name of the current database or the name of the database to be used after a connection is opened. The default value is an empty string.</returns>
		// Token: 0x170006CC RID: 1740
		// (get) Token: 0x0600299F RID: 10655 RVA: 0x00051759 File Offset: 0x0004F959
		public override string Database
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the server name or file name of the data source.</summary>
		/// <returns>The server name or file name of the data source. The default value is an empty string.</returns>
		// Token: 0x170006CD RID: 1741
		// (get) Token: 0x060029A0 RID: 10656 RVA: 0x00051759 File Offset: 0x0004F959
		public override string DataSource
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the name of the OLE DB provider specified in the "Provider= " clause of the connection string.</summary>
		/// <returns>The name of the provider as specified in the "Provider= " clause of the connection string. The default value is an empty string.</returns>
		// Token: 0x170006CE RID: 1742
		// (get) Token: 0x060029A1 RID: 10657 RVA: 0x00051759 File Offset: 0x0004F959
		public string Provider
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a string that contains the version of the server to which the client is connected.</summary>
		/// <returns>The version of the connected server.</returns>
		/// <exception cref="T:System.InvalidOperationException">The connection is closed. </exception>
		// Token: 0x170006CF RID: 1743
		// (get) Token: 0x060029A2 RID: 10658 RVA: 0x00051759 File Offset: 0x0004F959
		public override string ServerVersion
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the current state of the connection.</summary>
		/// <returns>A bitwise combination of the <see cref="T:System.Data.ConnectionState" /> values. The default is Closed.</returns>
		// Token: 0x170006D0 RID: 1744
		// (get) Token: 0x060029A3 RID: 10659 RVA: 0x000B6CF0 File Offset: 0x000B4EF0
		public override ConnectionState State
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return ConnectionState.Closed;
			}
		}

		/// <summary>Occurs when the provider sends a warning or an informational message.</summary>
		// Token: 0x14000028 RID: 40
		// (add) Token: 0x060029A4 RID: 10660 RVA: 0x00010458 File Offset: 0x0000E658
		// (remove) Token: 0x060029A5 RID: 10661 RVA: 0x00010458 File Offset: 0x0000E658
		public event OleDbInfoMessageEventHandler InfoMessage
		{
			add
			{
				ThrowStub.ThrowNotSupportedException();
			}
			remove
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		// Token: 0x060029A6 RID: 10662 RVA: 0x00051759 File Offset: 0x0004F959
		protected override DbTransaction BeginDbTransaction(IsolationLevel isolationLevel)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Changes the current database for an open <see cref="T:System.Data.OleDb.OleDbConnection" />.</summary>
		/// <param name="value">The database name. </param>
		/// <exception cref="T:System.ArgumentException">The database name is not valid. </exception>
		/// <exception cref="T:System.InvalidOperationException">The connection is not open. </exception>
		/// <exception cref="T:System.Data.OleDb.OleDbException">Cannot change the database. </exception>
		// Token: 0x060029A7 RID: 10663 RVA: 0x00010458 File Offset: 0x0000E658
		public override void ChangeDatabase(string value)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Closes the connection to the data source.</summary>
		// Token: 0x060029A8 RID: 10664 RVA: 0x00010458 File Offset: 0x0000E658
		public override void Close()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x060029A9 RID: 10665 RVA: 0x00051759 File Offset: 0x0004F959
		protected override DbCommand CreateDbCommand()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Enlists in the specified transaction as a distributed transaction.</summary>
		/// <param name="transaction">A reference to an existing <see cref="T:System.EnterpriseServices.ITransaction" /> in which to enlist.</param>
		// Token: 0x060029AA RID: 10666 RVA: 0x00010458 File Offset: 0x0000E658
		public void EnlistDistributedTransaction(ITransaction transaction)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Returns schema information from a data source as indicated by a GUID, and after it applies the specified restrictions.</summary>
		/// <param name="schema">One of the <see cref="T:System.Data.OleDb.OleDbSchemaGuid" /> values that specifies the schema table to return. </param>
		/// <param name="restrictions">An <see cref="T:System.Object" /> array of restriction values. These are applied in the order of the restriction columns. That is, the first restriction value applies to the first restriction column, the second restriction value applies to the second restriction column, and so on. </param>
		/// <returns>A <see cref="T:System.Data.DataTable" /> that contains the requested schema information.</returns>
		/// <exception cref="T:System.Data.OleDb.OleDbException">The specified set of restrictions is invalid. </exception>
		/// <exception cref="T:System.InvalidOperationException">The <see cref="T:System.Data.OleDb.OleDbConnection" /> is closed. </exception>
		/// <exception cref="T:System.ArgumentException">The specified schema rowset is not supported by the OLE DB provider.-or- The <paramref name="schema" /> parameter contains a value of <see cref="F:System.Data.OleDb.OleDbSchemaGuid.DbInfoLiterals" /> and the <paramref name="restrictions" /> parameter contains one or more restrictions. </exception>
		// Token: 0x060029AB RID: 10667 RVA: 0x00051759 File Offset: 0x0004F959
		public DataTable GetOleDbSchemaTable(Guid schema, object[] restrictions)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Opens a database connection with the property settings specified by the <see cref="P:System.Data.OleDb.OleDbConnection.ConnectionString" />.</summary>
		/// <exception cref="T:System.InvalidOperationException">The connection is already open.</exception>
		/// <exception cref="T:System.Data.OleDb.OleDbException">A connection-level error occurred while opening the connection.</exception>
		// Token: 0x060029AC RID: 10668 RVA: 0x00010458 File Offset: 0x0000E658
		public override void Open()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Indicates that the <see cref="T:System.Data.OleDb.OleDbConnection" /> object pool can be released when the last underlying connection is released.</summary>
		// Token: 0x060029AD RID: 10669 RVA: 0x00010458 File Offset: 0x0000E658
		public static void ReleaseObjectPool()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Updates the <see cref="P:System.Data.OleDb.OleDbConnection.State" /> property of the <see cref="T:System.Data.OleDb.OleDbConnection" /> object.</summary>
		// Token: 0x060029AE RID: 10670 RVA: 0x00010458 File Offset: 0x0000E658
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public void ResetState()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>For a description of this member, see <see cref="M:System.ICloneable.Clone" />.</summary>
		/// <returns>A new <see cref="T:System.Object" /> that is a copy of this instance.</returns>
		// Token: 0x060029AF RID: 10671 RVA: 0x00051759 File Offset: 0x0004F959
		object ICloneable.Clone()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}
	}
}
