﻿using System;
using System.ComponentModel;
using System.Data.Common;
using Unity;

namespace System.Data.OleDb
{
	/// <summary>Represents a parameter to an <see cref="T:System.Data.OleDb.OleDbCommand" /> and optionally its mapping to a <see cref="T:System.Data.DataSet" /> column. This class cannot be inherited.</summary>
	// Token: 0x0200036C RID: 876
	[TypeConverter("System.Data.OleDb.OleDbParameter.OleDbParameterConverter")]
	public sealed class OleDbParameter : DbParameter, ICloneable
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Data.OleDb.OleDbParameter" /> class.</summary>
		// Token: 0x06002A25 RID: 10789 RVA: 0x00010458 File Offset: 0x0000E658
		public OleDbParameter()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Data.OleDb.OleDbParameter" /> class that uses the parameter name and data type.</summary>
		/// <param name="name">The name of the parameter to map. </param>
		/// <param name="dataType">One of the <see cref="T:System.Data.OleDb.OleDbType" /> values. </param>
		/// <exception cref="T:System.ArgumentException">The value supplied in the <paramref name="dataType" /> parameter is an invalid back-end data type. </exception>
		// Token: 0x06002A26 RID: 10790 RVA: 0x00010458 File Offset: 0x0000E658
		public OleDbParameter(string name, OleDbType dataType)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Data.OleDb.OleDbParameter" /> class that uses the parameter name, data type, and length.</summary>
		/// <param name="name">The name of the parameter to map. </param>
		/// <param name="dataType">One of the <see cref="T:System.Data.OleDb.OleDbType" /> values. </param>
		/// <param name="size">The length of the parameter. </param>
		/// <exception cref="T:System.ArgumentException">The value supplied in the <paramref name="dataType" /> parameter is an invalid back-end data type. </exception>
		// Token: 0x06002A27 RID: 10791 RVA: 0x00010458 File Offset: 0x0000E658
		public OleDbParameter(string name, OleDbType dataType, int size)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Data.OleDb.OleDbParameter" /> class that uses the parameter name, data type, length, source column name, parameter direction, numeric precision, and other properties.</summary>
		/// <param name="parameterName">The name of the parameter. </param>
		/// <param name="dbType">One of the <see cref="T:System.Data.OleDb.OleDbType" /> values. </param>
		/// <param name="size">The length of the parameter. </param>
		/// <param name="direction">One of the <see cref="T:System.Data.ParameterDirection" /> values. </param>
		/// <param name="isNullable">
		///       <see langword="true" /> if the value of the field can be null; otherwise <see langword="false" />. </param>
		/// <param name="precision">The total number of digits to the left and right of the decimal point to which <see cref="P:System.Data.OleDb.OleDbParameter.Value" /> is resolved. </param>
		/// <param name="scale">The total number of decimal places to which <see cref="P:System.Data.OleDb.OleDbParameter.Value" /> is resolved. </param>
		/// <param name="srcColumn">The name of the source column. </param>
		/// <param name="srcVersion">One of the <see cref="T:System.Data.DataRowVersion" /> values. </param>
		/// <param name="value">An <see cref="T:System.Object" /> that is the value of the <see cref="T:System.Data.OleDb.OleDbParameter" />. </param>
		/// <exception cref="T:System.ArgumentException">The value supplied in the <paramref name="dataType" /> parameter is an invalid back-end data type. </exception>
		// Token: 0x06002A28 RID: 10792 RVA: 0x00010458 File Offset: 0x0000E658
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public OleDbParameter(string parameterName, OleDbType dbType, int size, ParameterDirection direction, bool isNullable, byte precision, byte scale, string srcColumn, DataRowVersion srcVersion, object value)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Data.OleDb.OleDbParameter" /> class that uses the parameter name, data type, length, source column name, parameter direction, numeric precision, and other properties.</summary>
		/// <param name="parameterName">The name of the parameter. </param>
		/// <param name="dbType">One of the <see cref="T:System.Data.OleDb.OleDbType" /> values. </param>
		/// <param name="size">The length of the parameter. </param>
		/// <param name="direction">One of the <see cref="T:System.Data.ParameterDirection" /> values. </param>
		/// <param name="precision">The total number of digits to the left and right of the decimal point to which <see cref="P:System.Data.OleDb.OleDbParameter.Value" /> is resolved.</param>
		/// <param name="scale">The total number of decimal places to which <see cref="P:System.Data.OleDb.OleDbParameter.Value" /> is resolved.</param>
		/// <param name="sourceColumn">The name of the source column.</param>
		/// <param name="sourceVersion">One of the <see cref="T:System.Data.DataRowVersion" /> values.</param>
		/// <param name="sourceColumnNullMapping">
		///       <see langword="true" /> if the source column is nullable; <see langword="false" /> if it is not.</param>
		/// <param name="value">An <see cref="T:System.Object" /> that is the value of the <see cref="T:System.Data.OleDb.OleDbParameter" />. </param>
		/// <exception cref="T:System.ArgumentException">The value supplied in the <paramref name="dataType" /> parameter is an invalid back-end data type. </exception>
		// Token: 0x06002A29 RID: 10793 RVA: 0x00010458 File Offset: 0x0000E658
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public OleDbParameter(string parameterName, OleDbType dbType, int size, ParameterDirection direction, byte precision, byte scale, string sourceColumn, DataRowVersion sourceVersion, bool sourceColumnNullMapping, object value)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Data.OleDb.OleDbParameter" /> class that uses the parameter name, data type, length, and source column name.</summary>
		/// <param name="name">The name of the parameter to map. </param>
		/// <param name="dataType">One of the <see cref="T:System.Data.OleDb.OleDbType" /> values. </param>
		/// <param name="size">The length of the parameter. </param>
		/// <param name="srcColumn">The name of the source column. </param>
		/// <exception cref="T:System.ArgumentException">The value supplied in the <paramref name="dataType" /> parameter is an invalid back-end data type. </exception>
		// Token: 0x06002A2A RID: 10794 RVA: 0x00010458 File Offset: 0x0000E658
		public OleDbParameter(string name, OleDbType dataType, int size, string srcColumn)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Data.OleDb.OleDbParameter" /> class that uses the parameter name and the value of the new <see cref="T:System.Data.OleDb.OleDbParameter" />.</summary>
		/// <param name="name">The name of the parameter to map. </param>
		/// <param name="value">The value of the new <see cref="T:System.Data.OleDb.OleDbParameter" /> object. </param>
		// Token: 0x06002A2B RID: 10795 RVA: 0x00010458 File Offset: 0x0000E658
		public OleDbParameter(string name, object value)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the <see cref="T:System.Data.DbType" /> of the parameter.</summary>
		/// <returns>One of the <see cref="T:System.Data.DbType" /> values. The default is <see cref="F:System.Data.DbType.String" />.</returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The property was not set to a valid <see cref="T:System.Data.DbType" />. </exception>
		// Token: 0x170006EF RID: 1775
		// (get) Token: 0x06002A2C RID: 10796 RVA: 0x000B70A8 File Offset: 0x000B52A8
		// (set) Token: 0x06002A2D RID: 10797 RVA: 0x00010458 File Offset: 0x0000E658
		public override DbType DbType
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return DbType.AnsiString;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets a value that indicates whether the parameter is input-only, output-only, bidirectional, or a stored procedure return-value parameter.</summary>
		/// <returns>One of the <see cref="T:System.Data.ParameterDirection" /> values. The default is <see langword="Input" />.</returns>
		/// <exception cref="T:System.ArgumentException">The property was not set to one of the valid <see cref="T:System.Data.ParameterDirection" /> values.</exception>
		// Token: 0x170006F0 RID: 1776
		// (get) Token: 0x06002A2E RID: 10798 RVA: 0x000B70C4 File Offset: 0x000B52C4
		// (set) Token: 0x06002A2F RID: 10799 RVA: 0x00010458 File Offset: 0x0000E658
		public override ParameterDirection Direction
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return (ParameterDirection)0;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets a value that indicates whether the parameter accepts null values.</summary>
		/// <returns>
		///     <see langword="true" /> if null values are accepted; otherwise <see langword="false" />. The default is <see langword="false" />.</returns>
		// Token: 0x170006F1 RID: 1777
		// (get) Token: 0x06002A30 RID: 10800 RVA: 0x000B70E0 File Offset: 0x000B52E0
		// (set) Token: 0x06002A31 RID: 10801 RVA: 0x00010458 File Offset: 0x0000E658
		public override bool IsNullable
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the <see cref="T:System.Data.OleDb.OleDbType" /> of the parameter.</summary>
		/// <returns>The <see cref="T:System.Data.OleDb.OleDbType" /> of the parameter. The default is <see cref="F:System.Data.OleDb.OleDbType.VarWChar" />.</returns>
		// Token: 0x170006F2 RID: 1778
		// (get) Token: 0x06002A32 RID: 10802 RVA: 0x000B70FC File Offset: 0x000B52FC
		// (set) Token: 0x06002A33 RID: 10803 RVA: 0x00010458 File Offset: 0x0000E658
		public OleDbType OleDbType
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return OleDbType.Empty;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the name of the <see cref="T:System.Data.OleDb.OleDbParameter" />.</summary>
		/// <returns>The name of the <see cref="T:System.Data.OleDb.OleDbParameter" />. The default is an empty string ("").</returns>
		// Token: 0x170006F3 RID: 1779
		// (get) Token: 0x06002A34 RID: 10804 RVA: 0x00051759 File Offset: 0x0004F959
		// (set) Token: 0x06002A35 RID: 10805 RVA: 0x00010458 File Offset: 0x0000E658
		public override string ParameterName
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the maximum size, in bytes, of the data within the column.</summary>
		/// <returns>The maximum size, in bytes, of the data within the column. The default value is inferred from the parameter value.</returns>
		// Token: 0x170006F4 RID: 1780
		// (get) Token: 0x06002A36 RID: 10806 RVA: 0x000B7118 File Offset: 0x000B5318
		// (set) Token: 0x06002A37 RID: 10807 RVA: 0x00010458 File Offset: 0x0000E658
		public override int Size
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the name of the source column mapped to the <see cref="T:System.Data.DataSet" /> and used for loading or returning the <see cref="P:System.Data.OleDb.OleDbParameter.Value" />.</summary>
		/// <returns>The name of the source column mapped to the <see cref="T:System.Data.DataSet" />. The default is an empty string.</returns>
		// Token: 0x170006F5 RID: 1781
		// (get) Token: 0x06002A38 RID: 10808 RVA: 0x00051759 File Offset: 0x0004F959
		// (set) Token: 0x06002A39 RID: 10809 RVA: 0x00010458 File Offset: 0x0000E658
		public override string SourceColumn
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Sets or gets a value which indicates whether the source column is nullable. This allows <see cref="T:System.Data.Common.DbCommandBuilder" /> to correctly generate Update statements for nullable columns.</summary>
		/// <returns>
		///     <see langword="true" /> if the source column is nullable; <see langword="false" /> if it is not.</returns>
		// Token: 0x170006F6 RID: 1782
		// (get) Token: 0x06002A3A RID: 10810 RVA: 0x000B7134 File Offset: 0x000B5334
		// (set) Token: 0x06002A3B RID: 10811 RVA: 0x00010458 File Offset: 0x0000E658
		public override bool SourceColumnNullMapping
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the value of the parameter.</summary>
		/// <returns>An <see cref="T:System.Object" /> that is the value of the parameter. The default value is null.</returns>
		// Token: 0x170006F7 RID: 1783
		// (get) Token: 0x06002A3C RID: 10812 RVA: 0x00051759 File Offset: 0x0004F959
		// (set) Token: 0x06002A3D RID: 10813 RVA: 0x00010458 File Offset: 0x0000E658
		public override object Value
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Resets the type associated with this <see cref="T:System.Data.OleDb.OleDbParameter" />.</summary>
		// Token: 0x06002A3E RID: 10814 RVA: 0x00010458 File Offset: 0x0000E658
		public override void ResetDbType()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Resets the type associated with this <see cref="T:System.Data.OleDb.OleDbParameter" />.</summary>
		// Token: 0x06002A3F RID: 10815 RVA: 0x00010458 File Offset: 0x0000E658
		public void ResetOleDbType()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>For a description of this member, see <see cref="M:System.ICloneable.Clone" />.</summary>
		/// <returns>A new <see cref="T:System.Object" /> that is a copy of this instance.</returns>
		// Token: 0x06002A40 RID: 10816 RVA: 0x00051759 File Offset: 0x0004F959
		object ICloneable.Clone()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}
	}
}
