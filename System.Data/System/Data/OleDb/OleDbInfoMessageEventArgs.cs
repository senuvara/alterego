﻿using System;
using Unity;

namespace System.Data.OleDb
{
	/// <summary>Provides data for the <see cref="E:System.Data.OleDb.OleDbConnection.InfoMessage" /> event. This class cannot be inherited.</summary>
	// Token: 0x0200035A RID: 858
	public sealed class OleDbInfoMessageEventArgs : EventArgs
	{
		// Token: 0x060029B4 RID: 10676 RVA: 0x00010458 File Offset: 0x0000E658
		internal OleDbInfoMessageEventArgs()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the HRESULT following the ANSI SQL standard for the database.</summary>
		/// <returns>The HRESULT, which identifies the source of the error, if the error can be issued from more than one place.</returns>
		// Token: 0x170006D1 RID: 1745
		// (get) Token: 0x060029B5 RID: 10677 RVA: 0x000B6D0C File Offset: 0x000B4F0C
		public int ErrorCode
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
		}

		/// <summary>Gets the collection of warnings sent from the data source.</summary>
		/// <returns>The collection of warnings sent from the data source.</returns>
		// Token: 0x170006D2 RID: 1746
		// (get) Token: 0x060029B6 RID: 10678 RVA: 0x00051759 File Offset: 0x0004F959
		public OleDbErrorCollection Errors
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the full text of the error sent from the data source.</summary>
		/// <returns>The full text of the error.</returns>
		// Token: 0x170006D3 RID: 1747
		// (get) Token: 0x060029B7 RID: 10679 RVA: 0x00051759 File Offset: 0x0004F959
		public string Message
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the name of the object that generated the error.</summary>
		/// <returns>The name of the object that generated the error.</returns>
		// Token: 0x170006D4 RID: 1748
		// (get) Token: 0x060029B8 RID: 10680 RVA: 0x00051759 File Offset: 0x0004F959
		public string Source
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}
	}
}
