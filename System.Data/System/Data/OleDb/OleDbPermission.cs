﻿using System;
using System.Data.Common;
using System.Security.Permissions;
using Unity;

namespace System.Data.OleDb
{
	/// <summary>Enables the .NET Framework Data Provider for OLE DB to help make sure that a user has a security level sufficient to access an OLE DB data source.</summary>
	// Token: 0x0200036E RID: 878
	[Serializable]
	public sealed class OleDbPermission : DBDataPermission
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Data.OleDb.OleDbPermission" /> class.</summary>
		// Token: 0x06002A63 RID: 10851 RVA: 0x00010458 File Offset: 0x0000E658
		[Obsolete("OleDbPermission() has been deprecated.  Use the OleDbPermission(PermissionState.None) constructor.  http://go.microsoft.com/fwlink/?linkid=14202", true)]
		public OleDbPermission()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Data.OleDb.OleDbPermission" /> class.</summary>
		/// <param name="state">One of the <see cref="T:System.Security.Permissions.PermissionState" /> values. </param>
		// Token: 0x06002A64 RID: 10852 RVA: 0x00010458 File Offset: 0x0000E658
		public OleDbPermission(PermissionState state)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Data.OleDb.OleDbPermission" /> class.</summary>
		/// <param name="state">One of the <see cref="T:System.Security.Permissions.PermissionState" /> values. </param>
		/// <param name="allowBlankPassword">Indicates whether a blank password is allowed. </param>
		// Token: 0x06002A65 RID: 10853 RVA: 0x00010458 File Offset: 0x0000E658
		[Obsolete("OleDbPermission(PermissionState state, Boolean allowBlankPassword) has been deprecated.  Use the OleDbPermission(PermissionState.None) constructor.  http://go.microsoft.com/fwlink/?linkid=14202", true)]
		public OleDbPermission(PermissionState state, bool allowBlankPassword)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>This property has been marked as obsolete. Setting this property will have no effect.</summary>
		/// <returns>This property has been marked as obsolete. Setting this property will have no effect.</returns>
		// Token: 0x170006FB RID: 1787
		// (get) Token: 0x06002A66 RID: 10854 RVA: 0x00051759 File Offset: 0x0004F959
		// (set) Token: 0x06002A67 RID: 10855 RVA: 0x00010458 File Offset: 0x0000E658
		public string Provider
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}
	}
}
