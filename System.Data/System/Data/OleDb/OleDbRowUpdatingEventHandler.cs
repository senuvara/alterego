﻿using System;
using Unity;

namespace System.Data.OleDb
{
	/// <summary>Represents the method that will handle the <see cref="E:System.Data.OleDb.OleDbDataAdapter.RowUpdating" /> event of an <see cref="T:System.Data.OleDb.OleDbDataAdapter" />.</summary>
	/// <param name="sender">The source of the event. </param>
	/// <param name="e">The <see cref="T:System.Data.OleDb.OleDbRowUpdatingEventArgs" /> that contains the event data. </param>
	// Token: 0x02000362 RID: 866
	public sealed class OleDbRowUpdatingEventHandler : MulticastDelegate
	{
		// Token: 0x060029EB RID: 10731 RVA: 0x00010458 File Offset: 0x0000E658
		public OleDbRowUpdatingEventHandler(object @object, IntPtr method)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x060029EC RID: 10732 RVA: 0x00010458 File Offset: 0x0000E658
		public void Invoke(object sender, OleDbRowUpdatingEventArgs e)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x060029ED RID: 10733 RVA: 0x00051759 File Offset: 0x0004F959
		public IAsyncResult BeginInvoke(object sender, OleDbRowUpdatingEventArgs e, AsyncCallback callback, object @object)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		// Token: 0x060029EE RID: 10734 RVA: 0x00010458 File Offset: 0x0000E658
		public void EndInvoke(IAsyncResult result)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
