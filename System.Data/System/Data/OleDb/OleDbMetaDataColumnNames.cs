﻿using System;

namespace System.Data.OleDb
{
	/// <summary>Provides static values that are used for the column names in the <see cref="T:System.Data.OleDb.OleDbMetaDataCollectionNames" /> objects contained in the <see cref="T:System.Data.DataTable" />. The <see cref="T:System.Data.DataTable" /> is created by the GetSchema method.</summary>
	// Token: 0x0200036B RID: 875
	public static class OleDbMetaDataColumnNames
	{
		/// <summary>Used by the GetSchema method to create the BooleanFalseLiteral column.</summary>
		// Token: 0x04001948 RID: 6472
		public static readonly string BooleanFalseLiteral;

		/// <summary>Used by the GetSchema method to create the BooleanTrueLiteral column.</summary>
		// Token: 0x04001949 RID: 6473
		public static readonly string BooleanTrueLiteral;

		/// <summary>Used by the GetSchema method to create the DateTimeDigits column. </summary>
		// Token: 0x0400194A RID: 6474
		public static readonly string DateTimeDigits;

		/// <summary>Used by the GetSchema method to create the NativeDataType column.</summary>
		// Token: 0x0400194B RID: 6475
		public static readonly string NativeDataType;
	}
}
