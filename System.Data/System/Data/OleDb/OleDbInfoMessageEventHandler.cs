﻿using System;
using Unity;

namespace System.Data.OleDb
{
	/// <summary>Represents the method that will handle the <see cref="E:System.Data.OleDb.OleDbConnection.InfoMessage" /> event of an <see cref="T:System.Data.OleDb.OleDbConnection" />.</summary>
	/// <param name="sender">The source of the event. </param>
	/// <param name="e">An <see cref="T:System.Data.OleDb.OleDbInfoMessageEventArgs" /> object that contains the event data. </param>
	// Token: 0x02000359 RID: 857
	public sealed class OleDbInfoMessageEventHandler : MulticastDelegate
	{
		// Token: 0x060029B0 RID: 10672 RVA: 0x00010458 File Offset: 0x0000E658
		public OleDbInfoMessageEventHandler(object @object, IntPtr method)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x060029B1 RID: 10673 RVA: 0x00010458 File Offset: 0x0000E658
		public void Invoke(object sender, OleDbInfoMessageEventArgs e)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x060029B2 RID: 10674 RVA: 0x00051759 File Offset: 0x0004F959
		public IAsyncResult BeginInvoke(object sender, OleDbInfoMessageEventArgs e, AsyncCallback callback, object @object)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		// Token: 0x060029B3 RID: 10675 RVA: 0x00010458 File Offset: 0x0000E658
		public void EndInvoke(IAsyncResult result)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
