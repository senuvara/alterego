﻿using System;
using System.Data.Common;
using Unity;

namespace System.Data.OleDb
{
	/// <summary>Represents an SQL transaction to be made at a data source. This class cannot be inherited. </summary>
	// Token: 0x0200035D RID: 861
	public sealed class OleDbTransaction : DbTransaction
	{
		// Token: 0x060029C6 RID: 10694 RVA: 0x00010458 File Offset: 0x0000E658
		internal OleDbTransaction()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x170006DB RID: 1755
		// (get) Token: 0x060029C7 RID: 10695 RVA: 0x00051759 File Offset: 0x0004F959
		protected override DbConnection DbConnection
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Specifies the <see cref="T:System.Data.IsolationLevel" /> for this transaction.</summary>
		/// <returns>The <see cref="T:System.Data.IsolationLevel" /> for this transaction. The default is <see langword="ReadCommitted" />.</returns>
		// Token: 0x170006DC RID: 1756
		// (get) Token: 0x060029C8 RID: 10696 RVA: 0x000B6D7C File Offset: 0x000B4F7C
		public override IsolationLevel IsolationLevel
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return (IsolationLevel)0;
			}
		}

		/// <summary>Initiates a nested database transaction.</summary>
		/// <returns>A nested database transaction.</returns>
		/// <exception cref="T:System.InvalidOperationException">Nested transactions are not supported. </exception>
		// Token: 0x060029C9 RID: 10697 RVA: 0x00051759 File Offset: 0x0004F959
		public OleDbTransaction Begin()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Initiates a nested database transaction and specifies the isolation level to use for the new transaction.</summary>
		/// <param name="isolevel">The isolation level to use for the transaction. </param>
		/// <returns>A nested database transaction.</returns>
		/// <exception cref="T:System.InvalidOperationException">Nested transactions are not supported. </exception>
		// Token: 0x060029CA RID: 10698 RVA: 0x00051759 File Offset: 0x0004F959
		public OleDbTransaction Begin(IsolationLevel isolevel)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Commits the database transaction.</summary>
		/// <exception cref="T:System.Exception">An error occurred while trying to commit the transaction. </exception>
		/// <exception cref="T:System.InvalidOperationException">The transaction has already been committed or rolled back.-or- The connection is broken. </exception>
		// Token: 0x060029CB RID: 10699 RVA: 0x00010458 File Offset: 0x0000E658
		public override void Commit()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Rolls back a transaction from a pending state.</summary>
		/// <exception cref="T:System.Exception">An error occurred while trying to commit the transaction. </exception>
		/// <exception cref="T:System.InvalidOperationException">The transaction has already been committed or rolled back.-or- The connection is broken.</exception>
		// Token: 0x060029CC RID: 10700 RVA: 0x00010458 File Offset: 0x0000E658
		public override void Rollback()
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
