﻿using System;
using Unity;

namespace System.Data.OleDb
{
	/// <summary>Provides a mechanism for enumerating all available OLE DB providers within the local network.</summary>
	// Token: 0x02000366 RID: 870
	public sealed class OleDbEnumerator
	{
		/// <summary>Creates an instance of the <see cref="T:System.Data.OleDb.OleDbEnumerator" /> class.</summary>
		// Token: 0x06002A1E RID: 10782 RVA: 0x00010458 File Offset: 0x0000E658
		public OleDbEnumerator()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Retrieves a <see cref="T:System.Data.DataTable" /> that contains information about all visible OLE DB providers.</summary>
		/// <returns>Returns a <see cref="T:System.Data.DataTable" /> that contains information about the visible OLE DB providers.</returns>
		/// <exception cref="T:System.InvalidCastException">The provider does not support ISourcesRowset.</exception>
		/// <exception cref="T:System.Data.OleDb.OleDbException">Exception has occurred in the underlying provider.</exception>
		// Token: 0x06002A1F RID: 10783 RVA: 0x00051759 File Offset: 0x0004F959
		public DataTable GetElements()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Uses a specific OLE DB enumerator to return an <see cref="T:System.Data.OleDb.OleDbDataReader" /> that contains information about the currently installed OLE DB providers, without requiring an instance of the <see cref="T:System.Data.OleDb.OleDbEnumerator" /> class.</summary>
		/// <param name="type">A <see cref="T:System.Type" />.</param>
		/// <returns>Returns an <see cref="T:System.Data.OleDb.OleDbDataReader" /> that contains information about the requested OLE DB providers, using the specified OLE DB enumerator.</returns>
		/// <exception cref="T:System.InvalidCastException">The provider does not support ISourcesRowset.</exception>
		/// <exception cref="T:System.Data.OleDb.OleDbException">An exception has occurred in the underlying provider.</exception>
		// Token: 0x06002A20 RID: 10784 RVA: 0x00051759 File Offset: 0x0004F959
		public static OleDbDataReader GetEnumerator(Type type)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Returns an <see cref="T:System.Data.OleDb.OleDbDataReader" /> that contains information about the currently installed OLE DB providers, without requiring an instance of the <see cref="T:System.Data.OleDb.OleDbEnumerator" /> class.</summary>
		/// <returns>Returns a <see cref="T:System.Data.OleDb.OleDbDataReader" /> that contains information about the visible OLE DB providers.</returns>
		/// <exception cref="T:System.InvalidCastException">The provider does not support ISourcesRowset.</exception>
		/// <exception cref="T:System.Data.OleDb.OleDbException">Exception has occurred in the underlying provider.</exception>
		// Token: 0x06002A21 RID: 10785 RVA: 0x00051759 File Offset: 0x0004F959
		public static OleDbDataReader GetRootEnumerator()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}
	}
}
