﻿using System;
using System.Data.Common;
using Unity;

namespace System.Data.OleDb
{
	/// <summary>Represents a set of methods for creating instances of the OLEDB provider's implementation of the data source classes.</summary>
	// Token: 0x02000368 RID: 872
	public sealed class OleDbFactory : DbProviderFactory
	{
		// Token: 0x06002A24 RID: 10788 RVA: 0x00010458 File Offset: 0x0000E658
		internal OleDbFactory()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets an instance of the <see cref="T:System.Data.OleDb.OleDbFactory" />. This can be used to retrieve strongly-typed data objects.</summary>
		// Token: 0x0400191E RID: 6430
		public static readonly OleDbFactory Instance;
	}
}
