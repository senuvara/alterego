﻿using System;
using Unity;

namespace System.Data.OleDb
{
	/// <summary>Collects information relevant to a warning or error returned by the data source.</summary>
	// Token: 0x0200035C RID: 860
	[Serializable]
	public sealed class OleDbError
	{
		// Token: 0x060029C1 RID: 10689 RVA: 0x00010458 File Offset: 0x0000E658
		internal OleDbError()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets a short description of the error.</summary>
		/// <returns>A short description of the error.</returns>
		// Token: 0x170006D7 RID: 1751
		// (get) Token: 0x060029C2 RID: 10690 RVA: 0x00051759 File Offset: 0x0004F959
		public string Message
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the database-specific error information.</summary>
		/// <returns>The database-specific error information.</returns>
		// Token: 0x170006D8 RID: 1752
		// (get) Token: 0x060029C3 RID: 10691 RVA: 0x000B6D60 File Offset: 0x000B4F60
		public int NativeError
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
		}

		/// <summary>Gets the name of the provider that generated the error.</summary>
		/// <returns>The name of the provider that generated the error.</returns>
		// Token: 0x170006D9 RID: 1753
		// (get) Token: 0x060029C4 RID: 10692 RVA: 0x00051759 File Offset: 0x0004F959
		public string Source
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the five-character error code following the ANSI SQL standard for the database.</summary>
		/// <returns>The five-character error code, which identifies the source of the error, if the error can be issued from more than one place.</returns>
		// Token: 0x170006DA RID: 1754
		// (get) Token: 0x060029C5 RID: 10693 RVA: 0x00051759 File Offset: 0x0004F959
		public string SQLState
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}
	}
}
