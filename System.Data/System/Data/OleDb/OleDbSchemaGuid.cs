﻿using System;
using Unity;

namespace System.Data.OleDb
{
	/// <summary>Returns the type of schema table specified by the <see cref="M:System.Data.OleDb.OleDbConnection.GetOleDbSchemaTable(System.Guid,System.Object[])" /> method.</summary>
	// Token: 0x02000370 RID: 880
	public sealed class OleDbSchemaGuid
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Data.OleDb.OleDbSchemaGuid" /> class.</summary>
		// Token: 0x06002A6C RID: 10860 RVA: 0x00010458 File Offset: 0x0000E658
		public OleDbSchemaGuid()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Returns the assertions defined in the catalog that is owned by a given user.</summary>
		// Token: 0x0400194C RID: 6476
		public static readonly Guid Assertions;

		/// <summary>Returns the physical attributes associated with catalogs accessible from the data source. Returns the assertions defined in the catalog that is owned by a given user.</summary>
		// Token: 0x0400194D RID: 6477
		public static readonly Guid Catalogs;

		/// <summary>Returns the character sets defined in the catalog that is accessible to a given user.</summary>
		// Token: 0x0400194E RID: 6478
		public static readonly Guid Character_Sets;

		/// <summary>Returns the check constraints defined in the catalog that is owned by a given user.</summary>
		// Token: 0x0400194F RID: 6479
		public static readonly Guid Check_Constraints;

		/// <summary>Returns the check constraints defined in the catalog that is owned by a given user.</summary>
		// Token: 0x04001950 RID: 6480
		public static readonly Guid Check_Constraints_By_Table;

		/// <summary>Returns the character collations defined in the catalog that is accessible to a given user.</summary>
		// Token: 0x04001951 RID: 6481
		public static readonly Guid Collations;

		/// <summary>Returns the columns of tables (including views) defined in the catalog that is accessible to a given user.</summary>
		// Token: 0x04001952 RID: 6482
		public static readonly Guid Columns;

		/// <summary>Returns the columns defined in the catalog that are dependent on a domain defined in the catalog and owned by a given user.</summary>
		// Token: 0x04001953 RID: 6483
		public static readonly Guid Column_Domain_Usage;

		/// <summary>Returns the privileges on columns of tables defined in the catalog that are available to or granted by a given user.</summary>
		// Token: 0x04001954 RID: 6484
		public static readonly Guid Column_Privileges;

		/// <summary>Returns the columns used by referential constraints, unique constraints, check constraints, and assertions, defined in the catalog and owned by a given user.</summary>
		// Token: 0x04001955 RID: 6485
		public static readonly Guid Constraint_Column_Usage;

		/// <summary>Returns the tables that are used by referential constraints, unique constraints, check constraints, and assertions defined in the catalog and owned by a given user.</summary>
		// Token: 0x04001956 RID: 6486
		public static readonly Guid Constraint_Table_Usage;

		/// <summary>Returns a list of provider-specific keywords.</summary>
		// Token: 0x04001957 RID: 6487
		public static readonly Guid DbInfoKeywords;

		/// <summary>Returns a list of provider-specific literals used in text commands.</summary>
		// Token: 0x04001958 RID: 6488
		public static readonly Guid DbInfoLiterals;

		/// <summary>Returns the foreign key columns defined in the catalog by a given user.</summary>
		// Token: 0x04001959 RID: 6489
		public static readonly Guid Foreign_Keys;

		/// <summary>Returns the indexes defined in the catalog that is owned by a given user.</summary>
		// Token: 0x0400195A RID: 6490
		public static readonly Guid Indexes;

		/// <summary>Returns the columns defined in the catalog that is constrained as keys by a given user.</summary>
		// Token: 0x0400195B RID: 6491
		public static readonly Guid Key_Column_Usage;

		/// <summary>Returns the primary key columns defined in the catalog by a given user.</summary>
		// Token: 0x0400195C RID: 6492
		public static readonly Guid Primary_Keys;

		/// <summary>Returns the procedures defined in the catalog that is owned by a given user.</summary>
		// Token: 0x0400195D RID: 6493
		public static readonly Guid Procedures;

		/// <summary>Returns information about the columns of rowsets returned by procedures.</summary>
		// Token: 0x0400195E RID: 6494
		public static readonly Guid Procedure_Columns;

		/// <summary>Returns information about the parameters and return codes of procedures.</summary>
		// Token: 0x0400195F RID: 6495
		public static readonly Guid Procedure_Parameters;

		/// <summary>Returns the base data types supported by the .NET Framework Data Provider for OLE DB.</summary>
		// Token: 0x04001960 RID: 6496
		public static readonly Guid Provider_Types;

		/// <summary>Returns the referential constraints defined in the catalog that is owned by a given user.</summary>
		// Token: 0x04001961 RID: 6497
		public static readonly Guid Referential_Constraints;

		/// <summary>Returns a list of schema rowsets, identified by their GUIDs, and a pointer to the descriptions of the restriction columns.</summary>
		// Token: 0x04001962 RID: 6498
		public static readonly Guid SchemaGuids;

		/// <summary>Returns the schema objects that are owned by a given user.</summary>
		// Token: 0x04001963 RID: 6499
		public static readonly Guid Schemata;

		/// <summary>Returns the conformance levels, options, and dialects supported by the SQL-implementation processing data defined in the catalog.</summary>
		// Token: 0x04001964 RID: 6500
		public static readonly Guid Sql_Languages;

		/// <summary>Returns the statistics defined in the catalog that is owned by a given user.</summary>
		// Token: 0x04001965 RID: 6501
		public static readonly Guid Statistics;

		/// <summary>Returns the tables (including views) defined in the catalog that are accessible to a given user.</summary>
		// Token: 0x04001966 RID: 6502
		public static readonly Guid Tables;

		/// <summary>Returns the tables (including views) that are accessible to a given user.</summary>
		// Token: 0x04001967 RID: 6503
		public static readonly Guid Tables_Info;

		/// <summary>Returns the table constraints defined in the catalog that is owned by a given user.</summary>
		// Token: 0x04001968 RID: 6504
		public static readonly Guid Table_Constraints;

		/// <summary>Returns the privileges on tables defined in the catalog that are available to, or granted by, a given user.</summary>
		// Token: 0x04001969 RID: 6505
		public static readonly Guid Table_Privileges;

		/// <summary>Describes the available set of statistics on tables in the provider.</summary>
		// Token: 0x0400196A RID: 6506
		public static readonly Guid Table_Statistics;

		/// <summary>Returns the character translations defined in the catalog that is accessible to a given user.</summary>
		// Token: 0x0400196B RID: 6507
		public static readonly Guid Translations;

		/// <summary>Identifies the trustees defined in the data source.</summary>
		// Token: 0x0400196C RID: 6508
		public static readonly Guid Trustee;

		/// <summary>Returns the USAGE privileges on objects defined in the catalog that are available to or granted by a given user.</summary>
		// Token: 0x0400196D RID: 6509
		public static readonly Guid Usage_Privileges;

		/// <summary>Returns the views defined in the catalog that is accessible to a given user.</summary>
		// Token: 0x0400196E RID: 6510
		public static readonly Guid Views;

		/// <summary>Returns the columns on which viewed tables depend, as defined in the catalog and owned by a given user.</summary>
		// Token: 0x0400196F RID: 6511
		public static readonly Guid View_Column_Usage;

		/// <summary>Returns the tables on which viewed tables, defined in the catalog and owned by a given user, are dependent.</summary>
		// Token: 0x04001970 RID: 6512
		public static readonly Guid View_Table_Usage;
	}
}
