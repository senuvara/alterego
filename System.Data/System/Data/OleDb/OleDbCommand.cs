﻿using System;
using System.ComponentModel;
using System.Data.Common;
using Unity;

namespace System.Data.OleDb
{
	/// <summary>Represents an SQL statement or stored procedure to execute against a data source.</summary>
	// Token: 0x02000357 RID: 855
	[DefaultEvent("RecordsAffected")]
	[Designer("Microsoft.VSDesigner.Data.VS.OleDbCommandDesigner, Microsoft.VSDesigner, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")]
	[ToolboxItem(true)]
	public sealed class OleDbCommand : DbCommand, ICloneable
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Data.OleDb.OleDbCommand" /> class.</summary>
		// Token: 0x0600297D RID: 10621 RVA: 0x00010458 File Offset: 0x0000E658
		public OleDbCommand()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Data.OleDb.OleDbCommand" /> class with the text of the query.</summary>
		/// <param name="cmdText">The text of the query. </param>
		// Token: 0x0600297E RID: 10622 RVA: 0x00010458 File Offset: 0x0000E658
		public OleDbCommand(string cmdText)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Data.OleDb.OleDbCommand" /> class with the text of the query and an <see cref="T:System.Data.OleDb.OleDbConnection" />.</summary>
		/// <param name="cmdText">The text of the query. </param>
		/// <param name="connection">An <see cref="T:System.Data.OleDb.OleDbConnection" /> that represents the connection to a data source. </param>
		// Token: 0x0600297F RID: 10623 RVA: 0x00010458 File Offset: 0x0000E658
		public OleDbCommand(string cmdText, OleDbConnection connection)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Data.OleDb.OleDbCommand" /> class with the text of the query, an <see cref="T:System.Data.OleDb.OleDbConnection" />, and the <see cref="P:System.Data.OleDb.OleDbCommand.Transaction" />.</summary>
		/// <param name="cmdText">The text of the query. </param>
		/// <param name="connection">An <see cref="T:System.Data.OleDb.OleDbConnection" /> that represents the connection to a data source. </param>
		/// <param name="transaction">The transaction in which the <see cref="T:System.Data.OleDb.OleDbCommand" /> executes. </param>
		// Token: 0x06002980 RID: 10624 RVA: 0x00010458 File Offset: 0x0000E658
		public OleDbCommand(string cmdText, OleDbConnection connection, OleDbTransaction transaction)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the SQL statement or stored procedure to execute at the data source.</summary>
		/// <returns>The SQL statement or stored procedure to execute. The default value is an empty string.</returns>
		// Token: 0x170006C1 RID: 1729
		// (get) Token: 0x06002981 RID: 10625 RVA: 0x00051759 File Offset: 0x0004F959
		// (set) Token: 0x06002982 RID: 10626 RVA: 0x00010458 File Offset: 0x0000E658
		public override string CommandText
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the wait time before terminating an attempt to execute a command and generating an error.</summary>
		/// <returns>The time (in seconds) to wait for the command to execute. The default is 30 seconds.</returns>
		// Token: 0x170006C2 RID: 1730
		// (get) Token: 0x06002983 RID: 10627 RVA: 0x000B6C64 File Offset: 0x000B4E64
		// (set) Token: 0x06002984 RID: 10628 RVA: 0x00010458 File Offset: 0x0000E658
		public override int CommandTimeout
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets a value that indicates how the <see cref="P:System.Data.OleDb.OleDbCommand.CommandText" /> property is interpreted.</summary>
		/// <returns>One of the <see cref="P:System.Data.OleDb.OleDbCommand.CommandType" /> values. The default is Text.</returns>
		/// <exception cref="T:System.ArgumentException">The value was not a valid <see cref="P:System.Data.OleDb.OleDbCommand.CommandType" />.</exception>
		// Token: 0x170006C3 RID: 1731
		// (get) Token: 0x06002985 RID: 10629 RVA: 0x000B6C80 File Offset: 0x000B4E80
		// (set) Token: 0x06002986 RID: 10630 RVA: 0x00010458 File Offset: 0x0000E658
		public override CommandType CommandType
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return (CommandType)0;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the <see cref="T:System.Data.OleDb.OleDbConnection" /> used by this instance of the <see cref="T:System.Data.OleDb.OleDbCommand" />.</summary>
		/// <returns>The connection to a data source. The default value is <see langword="null" />.</returns>
		/// <exception cref="T:System.InvalidOperationException">The <see cref="P:System.Data.OleDb.OleDbCommand.Connection" /> property was changed while a transaction was in progress. </exception>
		// Token: 0x170006C4 RID: 1732
		// (set) Token: 0x06002987 RID: 10631 RVA: 0x00010458 File Offset: 0x0000E658
		public new OleDbConnection Connection
		{
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		// Token: 0x170006C5 RID: 1733
		// (get) Token: 0x06002988 RID: 10632 RVA: 0x00051759 File Offset: 0x0004F959
		// (set) Token: 0x06002989 RID: 10633 RVA: 0x00010458 File Offset: 0x0000E658
		protected override DbConnection DbConnection
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		// Token: 0x170006C6 RID: 1734
		// (get) Token: 0x0600298A RID: 10634 RVA: 0x00051759 File Offset: 0x0004F959
		protected override DbParameterCollection DbParameterCollection
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		// Token: 0x170006C7 RID: 1735
		// (get) Token: 0x0600298B RID: 10635 RVA: 0x00051759 File Offset: 0x0004F959
		// (set) Token: 0x0600298C RID: 10636 RVA: 0x00010458 File Offset: 0x0000E658
		protected override DbTransaction DbTransaction
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets a value that indicates whether the command object should be visible in a customized Windows Forms Designer control.</summary>
		/// <returns>A value that indicates whether the command object should be visible in a control. The default is <see langword="true" />.</returns>
		// Token: 0x170006C8 RID: 1736
		// (get) Token: 0x0600298D RID: 10637 RVA: 0x000B6C9C File Offset: 0x000B4E9C
		// (set) Token: 0x0600298E RID: 10638 RVA: 0x00010458 File Offset: 0x0000E658
		public override bool DesignTimeVisible
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the <see cref="T:System.Data.OleDb.OleDbTransaction" /> within which the <see cref="T:System.Data.OleDb.OleDbCommand" /> executes.</summary>
		/// <returns>The <see cref="T:System.Data.OleDb.OleDbTransaction" />. The default value is <see langword="null" />.</returns>
		// Token: 0x170006C9 RID: 1737
		// (set) Token: 0x0600298F RID: 10639 RVA: 0x00010458 File Offset: 0x0000E658
		public new OleDbTransaction Transaction
		{
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets how command results are applied to the <see cref="T:System.Data.DataRow" /> when used by the <see langword="Update" /> method of the <see cref="T:System.Data.OleDb.OleDbDataAdapter" />.</summary>
		/// <returns>One of the <see cref="T:System.Data.UpdateRowSource" /> values.</returns>
		/// <exception cref="T:System.ArgumentException">The value entered was not one of the <see cref="T:System.Data.UpdateRowSource" /> values.</exception>
		// Token: 0x170006CA RID: 1738
		// (get) Token: 0x06002990 RID: 10640 RVA: 0x000B6CB8 File Offset: 0x000B4EB8
		// (set) Token: 0x06002991 RID: 10641 RVA: 0x00010458 File Offset: 0x0000E658
		public override UpdateRowSource UpdatedRowSource
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return UpdateRowSource.None;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Tries to cancel the execution of an <see cref="T:System.Data.OleDb.OleDbCommand" />.</summary>
		// Token: 0x06002992 RID: 10642 RVA: 0x00010458 File Offset: 0x0000E658
		public override void Cancel()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Creates a new <see cref="T:System.Data.OleDb.OleDbCommand" /> object that is a copy of the current instance.</summary>
		/// <returns>A new <see cref="T:System.Data.OleDb.OleDbCommand" /> object that is a copy of this instance.</returns>
		// Token: 0x06002993 RID: 10643 RVA: 0x00051759 File Offset: 0x0004F959
		public OleDbCommand Clone()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		// Token: 0x06002994 RID: 10644 RVA: 0x00051759 File Offset: 0x0004F959
		protected override DbParameter CreateDbParameter()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		// Token: 0x06002995 RID: 10645 RVA: 0x00051759 File Offset: 0x0004F959
		protected override DbDataReader ExecuteDbDataReader(CommandBehavior behavior)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Executes an SQL statement against the <see cref="P:System.Data.OleDb.OleDbCommand.Connection" /> and returns the number of rows affected.</summary>
		/// <returns>The number of rows affected.</returns>
		/// <exception cref="T:System.InvalidOperationException">The connection does not exist.-or- The connection is not open.-or- Cannot execute a command within a transaction context that differs from the context in which the connection was originally enlisted. </exception>
		// Token: 0x06002996 RID: 10646 RVA: 0x000B6CD4 File Offset: 0x000B4ED4
		public override int ExecuteNonQuery()
		{
			ThrowStub.ThrowNotSupportedException();
			return 0;
		}

		/// <summary>Executes the query, and returns the first column of the first row in the result set returned by the query. Additional columns or rows are ignored.</summary>
		/// <returns>The first column of the first row in the result set, or a null reference if the result set is empty.</returns>
		/// <exception cref="T:System.InvalidOperationException">Cannot execute a command within a transaction context that differs from the context in which the connection was originally enlisted. </exception>
		// Token: 0x06002997 RID: 10647 RVA: 0x00051759 File Offset: 0x0004F959
		public override object ExecuteScalar()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Creates a prepared (or compiled) version of the command on the data source.</summary>
		/// <exception cref="T:System.InvalidOperationException">The <see cref="P:System.Data.OleDb.OleDbCommand.Connection" /> is not set.-or- The <see cref="P:System.Data.OleDb.OleDbCommand.Connection" /> is not open. </exception>
		// Token: 0x06002998 RID: 10648 RVA: 0x00010458 File Offset: 0x0000E658
		public override void Prepare()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Resets the <see cref="P:System.Data.OleDb.OleDbCommand.CommandTimeout" /> property to the default value.</summary>
		// Token: 0x06002999 RID: 10649 RVA: 0x00010458 File Offset: 0x0000E658
		public void ResetCommandTimeout()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>For a description of this member, see <see cref="M:System.ICloneable.Clone" />.</summary>
		/// <returns>A new <see cref="T:System.Object" /> that is a copy of this instance.</returns>
		// Token: 0x0600299A RID: 10650 RVA: 0x00051759 File Offset: 0x0004F959
		object ICloneable.Clone()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}
	}
}
