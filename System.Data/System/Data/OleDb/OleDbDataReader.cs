﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data.Common;
using Unity;

namespace System.Data.OleDb
{
	/// <summary>Provides a way of reading a forward-only stream of data rows from a data source. This class cannot be inherited.</summary>
	// Token: 0x02000365 RID: 869
	public sealed class OleDbDataReader : DbDataReader
	{
		// Token: 0x060029FD RID: 10749 RVA: 0x00010458 File Offset: 0x0000E658
		internal OleDbDataReader()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets a value that indicates the depth of nesting for the current row.</summary>
		/// <returns>The depth of nesting for the current row.</returns>
		// Token: 0x170006E8 RID: 1768
		// (get) Token: 0x060029FE RID: 10750 RVA: 0x000B6E08 File Offset: 0x000B5008
		public override int Depth
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
		}

		/// <summary>Gets the number of columns in the current row.</summary>
		/// <returns>When not positioned in a valid recordset, 0; otherwise the number of columns in the current record. The default is -1.</returns>
		/// <exception cref="T:System.NotSupportedException">There is no current connection to a data source. </exception>
		// Token: 0x170006E9 RID: 1769
		// (get) Token: 0x060029FF RID: 10751 RVA: 0x000B6E24 File Offset: 0x000B5024
		public override int FieldCount
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
		}

		/// <summary>Gets a value that indicates whether the <see cref="T:System.Data.OleDb.OleDbDataReader" /> contains one or more rows.</summary>
		/// <returns>
		///     <see langword="true" /> if the <see cref="T:System.Data.OleDb.OleDbDataReader" /> contains one or more rows; otherwise <see langword="false" />.</returns>
		// Token: 0x170006EA RID: 1770
		// (get) Token: 0x06002A00 RID: 10752 RVA: 0x000B6E40 File Offset: 0x000B5040
		public override bool HasRows
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
		}

		/// <summary>Indicates whether the data reader is closed.</summary>
		/// <returns>
		///     <see langword="true" /> if the <see cref="T:System.Data.OleDb.OleDbDataReader" /> is closed; otherwise, <see langword="false" />.</returns>
		// Token: 0x170006EB RID: 1771
		// (get) Token: 0x06002A01 RID: 10753 RVA: 0x000B6E5C File Offset: 0x000B505C
		public override bool IsClosed
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
		}

		// Token: 0x06002A02 RID: 10754 RVA: 0x00051759 File Offset: 0x0004F959
		public override object get_Item(int index)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Gets the value of the specified column in its native format given the column name.</summary>
		/// <param name="name">The column name. </param>
		/// <returns>The value of the specified column in its native format.</returns>
		/// <exception cref="T:System.IndexOutOfRangeException">No column with the specified name was found. </exception>
		// Token: 0x170006EC RID: 1772
		public override object this[string name]
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the number of rows changed, inserted, or deleted by execution of the SQL statement.</summary>
		/// <returns>The number of rows changed, inserted, or deleted; 0 if no rows were affected or the statement failed; and -1 for SELECT statements.</returns>
		// Token: 0x170006ED RID: 1773
		// (get) Token: 0x06002A04 RID: 10756 RVA: 0x000B6E78 File Offset: 0x000B5078
		public override int RecordsAffected
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
		}

		/// <summary>Gets the value of the specified column as a Boolean.</summary>
		/// <param name="ordinal">The zero-based column ordinal. </param>
		/// <returns>The value of the column.</returns>
		/// <exception cref="T:System.InvalidCastException">The specified cast is not valid. </exception>
		// Token: 0x06002A05 RID: 10757 RVA: 0x000B6E94 File Offset: 0x000B5094
		public override bool GetBoolean(int ordinal)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Gets the value of the specified column as a byte.</summary>
		/// <param name="ordinal">The zero-based column ordinal. </param>
		/// <returns>The value of the specified column as a byte.</returns>
		/// <exception cref="T:System.InvalidCastException">The specified cast is not valid. </exception>
		// Token: 0x06002A06 RID: 10758 RVA: 0x000B6EB0 File Offset: 0x000B50B0
		public override byte GetByte(int ordinal)
		{
			ThrowStub.ThrowNotSupportedException();
			return 0;
		}

		/// <summary>Reads a stream of bytes from the specified column offset into the buffer as an array starting at the given buffer offset.</summary>
		/// <param name="ordinal">The zero-based column ordinal. </param>
		/// <param name="dataIndex">The index within the field from which to start the read operation. </param>
		/// <param name="buffer">The buffer into which to read the stream of bytes. </param>
		/// <param name="bufferIndex">The index within the <paramref name="buffer" /> where the write operation is to start. </param>
		/// <param name="length">The maximum length to copy into the buffer. </param>
		/// <returns>The actual number of bytes read.</returns>
		// Token: 0x06002A07 RID: 10759 RVA: 0x000B6ECC File Offset: 0x000B50CC
		public override long GetBytes(int ordinal, long dataIndex, byte[] buffer, int bufferIndex, int length)
		{
			ThrowStub.ThrowNotSupportedException();
			return 0L;
		}

		/// <summary>Gets the value of the specified column as a character.</summary>
		/// <param name="ordinal">The zero-based column ordinal. </param>
		/// <returns>The value of the specified column.</returns>
		/// <exception cref="T:System.InvalidCastException">The specified cast is not valid. </exception>
		// Token: 0x06002A08 RID: 10760 RVA: 0x000B6EE8 File Offset: 0x000B50E8
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override char GetChar(int ordinal)
		{
			ThrowStub.ThrowNotSupportedException();
			return '\0';
		}

		/// <summary>Reads a stream of characters from the specified column offset into the buffer as an array starting at the given buffer offset.</summary>
		/// <param name="ordinal">The zero-based column ordinal. </param>
		/// <param name="dataIndex">The index within the row from which to start the read operation. </param>
		/// <param name="buffer">The buffer into which to copy data. </param>
		/// <param name="bufferIndex">The index within the <paramref name="buffer" /> where the write operation is to start. </param>
		/// <param name="length">The number of characters to read. </param>
		/// <returns>The actual number of characters read.</returns>
		// Token: 0x06002A09 RID: 10761 RVA: 0x000B6F04 File Offset: 0x000B5104
		public override long GetChars(int ordinal, long dataIndex, char[] buffer, int bufferIndex, int length)
		{
			ThrowStub.ThrowNotSupportedException();
			return 0L;
		}

		/// <summary>Gets the name of the source data type.</summary>
		/// <param name="index">The zero-based column ordinal. </param>
		/// <returns>The name of the back-end data type. For more information, see SQL Server data types or Access data types.</returns>
		// Token: 0x06002A0A RID: 10762 RVA: 0x00051759 File Offset: 0x0004F959
		public override string GetDataTypeName(int index)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Gets the value of the specified column as a <see cref="T:System.DateTime" /> object.</summary>
		/// <param name="ordinal">The zero-based column ordinal. </param>
		/// <returns>The value of the specified column.</returns>
		/// <exception cref="T:System.InvalidCastException">The specified cast is not valid. </exception>
		// Token: 0x06002A0B RID: 10763 RVA: 0x000B6F20 File Offset: 0x000B5120
		public override DateTime GetDateTime(int ordinal)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(DateTime);
		}

		/// <summary>Gets the value of the specified column as a <see cref="T:System.Decimal" /> object.</summary>
		/// <param name="ordinal">The zero-based column ordinal. </param>
		/// <returns>The value of the specified column.</returns>
		/// <exception cref="T:System.InvalidCastException">The specified cast is not valid. </exception>
		// Token: 0x06002A0C RID: 10764 RVA: 0x000B6F3C File Offset: 0x000B513C
		public override decimal GetDecimal(int ordinal)
		{
			ThrowStub.ThrowNotSupportedException();
			return 0m;
		}

		/// <summary>Gets the value of the specified column as a double-precision floating-point number.</summary>
		/// <param name="ordinal">The zero-based column ordinal. </param>
		/// <returns>The value of the specified column.</returns>
		/// <exception cref="T:System.InvalidCastException">The specified cast is not valid. </exception>
		// Token: 0x06002A0D RID: 10765 RVA: 0x000B6F58 File Offset: 0x000B5158
		public override double GetDouble(int ordinal)
		{
			ThrowStub.ThrowNotSupportedException();
			return 0.0;
		}

		/// <summary>Returns an <see cref="T:System.Collections.IEnumerator" /> that can be used to iterate through the rows in the data reader.</summary>
		/// <returns>An <see cref="T:System.Collections.IEnumerator" /> that can be used to iterate through the rows in the data reader.</returns>
		// Token: 0x06002A0E RID: 10766 RVA: 0x00051759 File Offset: 0x0004F959
		public override IEnumerator GetEnumerator()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Gets the <see cref="T:System.Type" /> that is the data type of the object.</summary>
		/// <param name="index">The zero-based column ordinal. </param>
		/// <returns>The <see cref="T:System.Type" /> that is the data type of the object.</returns>
		// Token: 0x06002A0F RID: 10767 RVA: 0x00051759 File Offset: 0x0004F959
		public override Type GetFieldType(int index)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Gets the value of the specified column as a single-precision floating-point number.</summary>
		/// <param name="ordinal">The zero-based column ordinal. </param>
		/// <returns>The value of the specified column.</returns>
		/// <exception cref="T:System.InvalidCastException">The specified cast is not valid. </exception>
		// Token: 0x06002A10 RID: 10768 RVA: 0x000B6F74 File Offset: 0x000B5174
		public override float GetFloat(int ordinal)
		{
			ThrowStub.ThrowNotSupportedException();
			return 0f;
		}

		/// <summary>Gets the value of the specified column as a globally unique identifier (GUID).</summary>
		/// <param name="ordinal">The zero-based column ordinal. </param>
		/// <returns>The value of the specified column.</returns>
		/// <exception cref="T:System.InvalidCastException">The specified cast is not valid. </exception>
		// Token: 0x06002A11 RID: 10769 RVA: 0x000B6F90 File Offset: 0x000B5190
		public override Guid GetGuid(int ordinal)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(Guid);
		}

		/// <summary>Gets the value of the specified column as a 16-bit signed integer.</summary>
		/// <param name="ordinal">The zero-based column ordinal. </param>
		/// <returns>The value of the specified column.</returns>
		/// <exception cref="T:System.InvalidCastException">The specified cast is not valid. </exception>
		// Token: 0x06002A12 RID: 10770 RVA: 0x000B6FAC File Offset: 0x000B51AC
		public override short GetInt16(int ordinal)
		{
			ThrowStub.ThrowNotSupportedException();
			return 0;
		}

		/// <summary>Gets the value of the specified column as a 32-bit signed integer.</summary>
		/// <param name="ordinal">The zero-based column ordinal. </param>
		/// <returns>The value of the specified column.</returns>
		/// <exception cref="T:System.InvalidCastException">The specified cast is not valid. </exception>
		// Token: 0x06002A13 RID: 10771 RVA: 0x000B6FC8 File Offset: 0x000B51C8
		public override int GetInt32(int ordinal)
		{
			ThrowStub.ThrowNotSupportedException();
			return 0;
		}

		/// <summary>Gets the value of the specified column as a 64-bit signed integer.</summary>
		/// <param name="ordinal">The zero-based column ordinal. </param>
		/// <returns>The value of the specified column.</returns>
		/// <exception cref="T:System.InvalidCastException">The specified cast is not valid. </exception>
		// Token: 0x06002A14 RID: 10772 RVA: 0x000B6FE4 File Offset: 0x000B51E4
		public override long GetInt64(int ordinal)
		{
			ThrowStub.ThrowNotSupportedException();
			return 0L;
		}

		/// <summary>Gets the name of the specified column.</summary>
		/// <param name="index">The zero-based column ordinal. </param>
		/// <returns>The name of the specified column.</returns>
		// Token: 0x06002A15 RID: 10773 RVA: 0x00051759 File Offset: 0x0004F959
		public override string GetName(int index)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Gets the column ordinal, given the name of the column.</summary>
		/// <param name="name">The name of the column. </param>
		/// <returns>The zero-based column ordinal.</returns>
		/// <exception cref="T:System.IndexOutOfRangeException">The name specified is not a valid column name. </exception>
		// Token: 0x06002A16 RID: 10774 RVA: 0x000B7000 File Offset: 0x000B5200
		public override int GetOrdinal(string name)
		{
			ThrowStub.ThrowNotSupportedException();
			return 0;
		}

		/// <summary>Gets the value of the specified column as a string.</summary>
		/// <param name="ordinal">The zero-based column ordinal. </param>
		/// <returns>The value of the specified column.</returns>
		/// <exception cref="T:System.InvalidCastException">The specified cast is not valid. </exception>
		// Token: 0x06002A17 RID: 10775 RVA: 0x00051759 File Offset: 0x0004F959
		public override string GetString(int ordinal)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Gets the value of the specified column as a <see cref="T:System.TimeSpan" /> object.</summary>
		/// <param name="ordinal">The zero-based column ordinal. </param>
		/// <returns>The value of the specified column.</returns>
		/// <exception cref="T:System.InvalidCastException">The specified cast is not valid. </exception>
		// Token: 0x06002A18 RID: 10776 RVA: 0x000B701C File Offset: 0x000B521C
		public TimeSpan GetTimeSpan(int ordinal)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(TimeSpan);
		}

		/// <summary>Gets the value of the column at the specified ordinal in its native format.</summary>
		/// <param name="ordinal">The zero-based column ordinal. </param>
		/// <returns>The value to return.</returns>
		// Token: 0x06002A19 RID: 10777 RVA: 0x00051759 File Offset: 0x0004F959
		public override object GetValue(int ordinal)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Populates an array of objects with the column values of the current row.</summary>
		/// <param name="values">An array of <see cref="T:System.Object" /> into which to copy the attribute columns. </param>
		/// <returns>The number of instances of <see cref="T:System.Object" /> in the array.</returns>
		// Token: 0x06002A1A RID: 10778 RVA: 0x000B7038 File Offset: 0x000B5238
		public override int GetValues(object[] values)
		{
			ThrowStub.ThrowNotSupportedException();
			return 0;
		}

		/// <summary>Gets a value that indicates whether the column contains nonexistent or missing values.</summary>
		/// <param name="ordinal">The zero-based column ordinal. </param>
		/// <returns>
		///     <see langword="true" /> if the specified column value is equivalent to <see cref="T:System.DBNull" />; otherwise, <see langword="false" />.</returns>
		// Token: 0x06002A1B RID: 10779 RVA: 0x000B7054 File Offset: 0x000B5254
		public override bool IsDBNull(int ordinal)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Advances the data reader to the next result, when reading the results of batch SQL statements.</summary>
		/// <returns>
		///     <see langword="true" /> if there are more result sets; otherwise, <see langword="false" />.</returns>
		// Token: 0x06002A1C RID: 10780 RVA: 0x000B7070 File Offset: 0x000B5270
		public override bool NextResult()
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Advances the <see cref="T:System.Data.OleDb.OleDbDataReader" /> to the next record.</summary>
		/// <returns>
		///     <see langword="true" /> if there are more rows; otherwise, <see langword="false" />.</returns>
		// Token: 0x06002A1D RID: 10781 RVA: 0x000B708C File Offset: 0x000B528C
		public override bool Read()
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}
	}
}
