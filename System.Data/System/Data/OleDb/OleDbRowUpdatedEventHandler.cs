﻿using System;
using Unity;

namespace System.Data.OleDb
{
	/// <summary>Represents the method that will handle the <see cref="E:System.Data.OleDb.OleDbDataAdapter.RowUpdated" /> event of an <see cref="T:System.Data.OleDb.OleDbDataAdapter" />.</summary>
	/// <param name="sender">The source of the event. </param>
	/// <param name="e">The <see cref="T:System.Data.OleDb.OleDbRowUpdatedEventArgs" /> that contains the event data. </param>
	// Token: 0x02000360 RID: 864
	public sealed class OleDbRowUpdatedEventHandler : MulticastDelegate
	{
		// Token: 0x060029E6 RID: 10726 RVA: 0x00010458 File Offset: 0x0000E658
		public OleDbRowUpdatedEventHandler(object @object, IntPtr method)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x060029E7 RID: 10727 RVA: 0x00010458 File Offset: 0x0000E658
		public void Invoke(object sender, OleDbRowUpdatedEventArgs e)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x060029E8 RID: 10728 RVA: 0x00051759 File Offset: 0x0004F959
		public IAsyncResult BeginInvoke(object sender, OleDbRowUpdatedEventArgs e, AsyncCallback callback, object @object)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		// Token: 0x060029E9 RID: 10729 RVA: 0x00010458 File Offset: 0x0000E658
		public void EndInvoke(IAsyncResult result)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
