﻿using System;
using System.ComponentModel;
using System.Data.Common;
using System.Reflection;
using Unity;

namespace System.Data.OleDb
{
	/// <summary>Provides a simple way to create and manage the contents of connection strings used by the <see cref="T:System.Data.OleDb.OleDbConnection" /> class.</summary>
	// Token: 0x02000364 RID: 868
	[DefaultMember("Item")]
	[DefaultProperty("Provider")]
	[RefreshProperties(RefreshProperties.All)]
	[TypeConverter("System.Data.OleDb.OleDbConnectionStringBuilder.OleDbConnectionStringBuilderConverter")]
	public sealed class OleDbConnectionStringBuilder : DbConnectionStringBuilder
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Data.OleDb.OleDbConnectionStringBuilder" /> class.</summary>
		// Token: 0x060029F1 RID: 10737 RVA: 0x00010458 File Offset: 0x0000E658
		public OleDbConnectionStringBuilder()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Data.OleDb.OleDbConnectionStringBuilder" /> class. The provided connection string provides the data for the instance's internal connection information.</summary>
		/// <param name="connectionString">The basis for the object's internal connection information. Parsed into key/value pairs.</param>
		/// <exception cref="T:System.ArgumentException">The connection string is incorrectly formatted (perhaps missing the required "=" within a key/value pair).</exception>
		// Token: 0x060029F2 RID: 10738 RVA: 0x00010458 File Offset: 0x0000E658
		public OleDbConnectionStringBuilder(string connectionString)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the name of the data source to connect to.</summary>
		/// <returns>The value of the <see cref="P:System.Data.OleDb.OleDbConnectionStringBuilder.DataSource" /> property, or <see langword="String.Empty" /> if none has been supplied.</returns>
		// Token: 0x170006E3 RID: 1763
		// (get) Token: 0x060029F3 RID: 10739 RVA: 0x00051759 File Offset: 0x0004F959
		// (set) Token: 0x060029F4 RID: 10740 RVA: 0x00010458 File Offset: 0x0000E658
		public string DataSource
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the name of the Universal Data Link (UDL) file for connecting to the data source.</summary>
		/// <returns>The value of the <see cref="P:System.Data.OleDb.OleDbConnectionStringBuilder.FileName" /> property, or <see langword="String.Empty" /> if none has been supplied.</returns>
		// Token: 0x170006E4 RID: 1764
		// (get) Token: 0x060029F5 RID: 10741 RVA: 0x00051759 File Offset: 0x0004F959
		// (set) Token: 0x060029F6 RID: 10742 RVA: 0x00010458 File Offset: 0x0000E658
		public string FileName
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the value to be passed for the OLE DB Services key within the connection string.</summary>
		/// <returns>Returns the value corresponding to the OLE DB Services key within the connection string. By default, the value is -13.</returns>
		// Token: 0x170006E5 RID: 1765
		// (get) Token: 0x060029F7 RID: 10743 RVA: 0x000B6DD0 File Offset: 0x000B4FD0
		// (set) Token: 0x060029F8 RID: 10744 RVA: 0x00010458 File Offset: 0x0000E658
		public int OleDbServices
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets a Boolean value that indicates whether security-sensitive information, such as the password, is returned as part of the connection if the connection is open or has ever been in an open state.</summary>
		/// <returns>The value of the <see cref="P:System.Data.OleDb.OleDbConnectionStringBuilder.PersistSecurityInfo" /> property, or <see langword="false" /> if none has been supplied.</returns>
		// Token: 0x170006E6 RID: 1766
		// (get) Token: 0x060029F9 RID: 10745 RVA: 0x000B6DEC File Offset: 0x000B4FEC
		// (set) Token: 0x060029FA RID: 10746 RVA: 0x00010458 File Offset: 0x0000E658
		public bool PersistSecurityInfo
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets a string that contains the name of the data provider associated with the internal connection string.</summary>
		/// <returns>The value of the <see cref="P:System.Data.OleDb.OleDbConnectionStringBuilder.Provider" /> property, or <see langword="String.Empty" /> if none has been supplied.</returns>
		// Token: 0x170006E7 RID: 1767
		// (get) Token: 0x060029FB RID: 10747 RVA: 0x00051759 File Offset: 0x0004F959
		// (set) Token: 0x060029FC RID: 10748 RVA: 0x00010458 File Offset: 0x0000E658
		public string Provider
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}
	}
}
