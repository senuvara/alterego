﻿using System;
using System.ComponentModel;
using System.Data.Common;
using Unity;

namespace System.Data.OleDb
{
	/// <summary>Represents a set of data commands and a database connection that are used to fill the <see cref="T:System.Data.DataSet" /> and update the data source.</summary>
	// Token: 0x0200035F RID: 863
	[DefaultEvent("RowUpdated")]
	[ToolboxItem("Microsoft.VSDesigner.Data.VS.OleDbDataAdapterToolboxItem, Microsoft.VSDesigner, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")]
	[Designer("Microsoft.VSDesigner.Data.VS.OleDbDataAdapterDesigner, Microsoft.VSDesigner, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")]
	public sealed class OleDbDataAdapter : DbDataAdapter
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Data.OleDb.OleDbDataAdapter" /> class.</summary>
		// Token: 0x060029D8 RID: 10712 RVA: 0x00010458 File Offset: 0x0000E658
		public OleDbDataAdapter()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Data.OleDb.OleDbDataAdapter" /> class with the specified <see cref="T:System.Data.OleDb.OleDbCommand" /> as the <see cref="P:System.Data.OleDb.OleDbDataAdapter.SelectCommand" /> property.</summary>
		/// <param name="selectCommand">An <see cref="T:System.Data.OleDb.OleDbCommand" /> that is a SELECT statement or stored procedure, and is set as the <see cref="P:System.Data.OleDb.OleDbDataAdapter.SelectCommand" /> property of the <see cref="T:System.Data.OleDb.OleDbDataAdapter" />.</param>
		// Token: 0x060029D9 RID: 10713 RVA: 0x00010458 File Offset: 0x0000E658
		public OleDbDataAdapter(OleDbCommand selectCommand)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Data.OleDb.OleDbDataAdapter" /> class with a <see cref="P:System.Data.OleDb.OleDbDataAdapter.SelectCommand" />.</summary>
		/// <param name="selectCommandText">A string that is an SQL SELECT statement or stored procedure to be used by the <see cref="P:System.Data.OleDb.OleDbDataAdapter.SelectCommand" /> property of the <see cref="T:System.Data.OleDb.OleDbDataAdapter" />. </param>
		/// <param name="selectConnection">An <see cref="T:System.Data.OleDb.OleDbConnection" /> that represents the connection. </param>
		// Token: 0x060029DA RID: 10714 RVA: 0x00010458 File Offset: 0x0000E658
		public OleDbDataAdapter(string selectCommandText, OleDbConnection selectConnection)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Data.OleDb.OleDbDataAdapter" /> class with a <see cref="P:System.Data.OleDb.OleDbDataAdapter.SelectCommand" />.</summary>
		/// <param name="selectCommandText">A string that is an SQL SELECT statement or stored procedure to be used by the <see cref="P:System.Data.OleDb.OleDbDataAdapter.SelectCommand" /> property of the <see cref="T:System.Data.OleDb.OleDbDataAdapter" />. </param>
		/// <param name="selectConnectionString">The connection string. </param>
		// Token: 0x060029DB RID: 10715 RVA: 0x00010458 File Offset: 0x0000E658
		public OleDbDataAdapter(string selectCommandText, string selectConnectionString)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets an SQL statement or stored procedure for deleting records from the data set.</summary>
		/// <returns>An <see cref="T:System.Data.OleDb.OleDbCommand" /> used during <see cref="M:System.Data.Common.DbDataAdapter.Update(System.Data.DataSet)" /> to delete records in the data source that correspond to deleted rows in the <see cref="T:System.Data.DataSet" />.</returns>
		// Token: 0x170006DE RID: 1758
		// (set) Token: 0x060029DC RID: 10716 RVA: 0x00010458 File Offset: 0x0000E658
		public new OleDbCommand DeleteCommand
		{
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets an SQL statement or stored procedure used to insert new records into the data source.</summary>
		/// <returns>An <see cref="T:System.Data.OleDb.OleDbCommand" /> used during <see cref="M:System.Data.Common.DbDataAdapter.Update(System.Data.DataSet)" /> to insert records in the data source that correspond to new rows in the <see cref="T:System.Data.DataSet" />.</returns>
		// Token: 0x170006DF RID: 1759
		// (set) Token: 0x060029DD RID: 10717 RVA: 0x00010458 File Offset: 0x0000E658
		public new OleDbCommand InsertCommand
		{
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets an SQL statement or stored procedure used to select records in the data source.</summary>
		/// <returns>An <see cref="T:System.Data.OleDb.OleDbCommand" /> that is used during <see cref="M:System.Data.Common.DbDataAdapter.Fill(System.Data.DataSet)" /> to select records from data source for placement in the <see cref="T:System.Data.DataSet" />.</returns>
		// Token: 0x170006E0 RID: 1760
		// (set) Token: 0x060029DE RID: 10718 RVA: 0x00010458 File Offset: 0x0000E658
		public new OleDbCommand SelectCommand
		{
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets an SQL statement or stored procedure used to update records in the data source.</summary>
		/// <returns>An <see cref="T:System.Data.OleDb.OleDbCommand" /> used during <see cref="M:System.Data.Common.DbDataAdapter.Update(System.Data.DataSet)" /> to update records in the data source that correspond to modified rows in the <see cref="T:System.Data.DataSet" />.</returns>
		// Token: 0x170006E1 RID: 1761
		// (set) Token: 0x060029DF RID: 10719 RVA: 0x00010458 File Offset: 0x0000E658
		public new OleDbCommand UpdateCommand
		{
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Occurs during <see cref="M:System.Data.Common.DbDataAdapter.Update(System.Data.DataSet)" /> after a command is executed against the data source. The attempt to update is made. Therefore, the event occurs.</summary>
		// Token: 0x14000029 RID: 41
		// (add) Token: 0x060029E0 RID: 10720 RVA: 0x00010458 File Offset: 0x0000E658
		// (remove) Token: 0x060029E1 RID: 10721 RVA: 0x00010458 File Offset: 0x0000E658
		public event OleDbRowUpdatedEventHandler RowUpdated
		{
			add
			{
				ThrowStub.ThrowNotSupportedException();
			}
			remove
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Occurs during <see cref="M:System.Data.Common.DbDataAdapter.Update(System.Data.DataSet)" /> before a command is executed against the data source. The attempt to update is made. Therefore, the event occurs.</summary>
		// Token: 0x1400002A RID: 42
		// (add) Token: 0x060029E2 RID: 10722 RVA: 0x00010458 File Offset: 0x0000E658
		// (remove) Token: 0x060029E3 RID: 10723 RVA: 0x00010458 File Offset: 0x0000E658
		public event OleDbRowUpdatingEventHandler RowUpdating
		{
			add
			{
				ThrowStub.ThrowNotSupportedException();
			}
			remove
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Adds or refreshes rows in the <see cref="T:System.Data.DataSet" /> to match those in an ADO <see langword="Recordset" /> or <see langword="Record" /> object using the specified <see cref="T:System.Data.DataSet" />, ADO object, and source table name.</summary>
		/// <param name="dataSet">A <see cref="T:System.Data.DataSet" /> to fill with records and, if it is required, schema. </param>
		/// <param name="ADODBRecordSet">An ADO <see langword="Recordset" /> or <see langword="Record" /> object. </param>
		/// <param name="srcTable">The source table used for the table mappings. </param>
		/// <returns>The number of rows successfully added to or refreshed in the <see cref="T:System.Data.DataSet" />. This does not include rows affected by statements that do not return rows.</returns>
		/// <exception cref="T:System.SystemException">The source table is invalid. </exception>
		// Token: 0x060029E4 RID: 10724 RVA: 0x000B6D98 File Offset: 0x000B4F98
		public int Fill(DataSet dataSet, object ADODBRecordSet, string srcTable)
		{
			ThrowStub.ThrowNotSupportedException();
			return 0;
		}

		/// <summary>Adds or refreshes rows in a <see cref="T:System.Data.DataTable" /> to match those in an ADO <see langword="Recordset" /> or <see langword="Record" /> object using the specified <see cref="T:System.Data.DataTable" /> and ADO objects.</summary>
		/// <param name="dataTable">A <see cref="T:System.Data.DataTable" /> to fill with records and, if it is required, schema. </param>
		/// <param name="ADODBRecordSet">An ADO <see langword="Recordset" /> or <see langword="Record" /> object. </param>
		/// <returns>The number of rows successfully refreshed to the <see cref="T:System.Data.DataTable" />. This does not include rows affected by statements that do not return rows.</returns>
		// Token: 0x060029E5 RID: 10725 RVA: 0x000B6DB4 File Offset: 0x000B4FB4
		public int Fill(DataTable dataTable, object ADODBRecordSet)
		{
			ThrowStub.ThrowNotSupportedException();
			return 0;
		}
	}
}
