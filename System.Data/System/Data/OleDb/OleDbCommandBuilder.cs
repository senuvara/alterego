﻿using System;
using System.Data.Common;
using Unity;

namespace System.Data.OleDb
{
	/// <summary>Automatically generates single-table commands that are used to reconcile changes made to a <see cref="T:System.Data.DataSet" /> with the associated database. This class cannot be inherited.</summary>
	// Token: 0x0200035E RID: 862
	public sealed class OleDbCommandBuilder : DbCommandBuilder
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Data.OleDb.OleDbCommandBuilder" /> class.</summary>
		// Token: 0x060029CD RID: 10701 RVA: 0x00010458 File Offset: 0x0000E658
		public OleDbCommandBuilder()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Data.OleDb.OleDbCommandBuilder" /> class with the associated <see cref="T:System.Data.OleDb.OleDbDataAdapter" /> object.</summary>
		/// <param name="adapter">An <see cref="T:System.Data.OleDb.OleDbDataAdapter" />. </param>
		// Token: 0x060029CE RID: 10702 RVA: 0x00010458 File Offset: 0x0000E658
		public OleDbCommandBuilder(OleDbDataAdapter adapter)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets an <see cref="T:System.Data.OleDb.OleDbDataAdapter" /> object for which SQL statements are automatically generated.</summary>
		/// <returns>An <see cref="T:System.Data.OleDb.OleDbDataAdapter" /> object.</returns>
		// Token: 0x170006DD RID: 1757
		// (set) Token: 0x060029CF RID: 10703 RVA: 0x00010458 File Offset: 0x0000E658
		public new OleDbDataAdapter DataAdapter
		{
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		// Token: 0x060029D0 RID: 10704 RVA: 0x00010458 File Offset: 0x0000E658
		protected override void ApplyParameterInfo(DbParameter parameter, DataRow datarow, StatementType statementType, bool whereClause)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Retrieves parameter information from the stored procedure specified in the <see cref="T:System.Data.OleDb.OleDbCommand" /> and populates the <see cref="P:System.Data.OleDb.OleDbCommand.Parameters" /> collection of the specified <see cref="T:System.Data.OleDb.OleDbCommand" /> object.</summary>
		/// <param name="command">The <see cref="T:System.Data.OleDb.OleDbCommand" /> referencing the stored procedure from which the parameter information is to be derived. The derived parameters are added to the <see cref="P:System.Data.OleDb.OleDbCommand.Parameters" /> collection of the <see cref="T:System.Data.OleDb.OleDbCommand" />. </param>
		/// <exception cref="T:System.InvalidOperationException">The underlying OLE DB provider does not support returning stored procedure parameter information, the command text is not a valid stored procedure name, or the <see cref="P:System.Data.OleDb.OleDbCommand.CommandType" /> specified was not <see langword="StoredProcedure" />.</exception>
		// Token: 0x060029D1 RID: 10705 RVA: 0x00010458 File Offset: 0x0000E658
		public static void DeriveParameters(OleDbCommand command)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x060029D2 RID: 10706 RVA: 0x00051759 File Offset: 0x0004F959
		protected override string GetParameterName(int parameterOrdinal)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		// Token: 0x060029D3 RID: 10707 RVA: 0x00051759 File Offset: 0x0004F959
		protected override string GetParameterName(string parameterName)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		// Token: 0x060029D4 RID: 10708 RVA: 0x00051759 File Offset: 0x0004F959
		protected override string GetParameterPlaceholder(int parameterOrdinal)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Given an unquoted identifier in the correct catalog case, returns the correct quoted form of that identifier. This includes correctly escaping any embedded quotes in the identifier.</summary>
		/// <param name="unquotedIdentifier">The unquoted identifier to be returned in quoted format.</param>
		/// <param name="connection">When a connection is passed, causes the managed wrapper to get the quote character from the OLE DB provider. When no connection is passed, the string is quoted using values from <see cref="P:System.Data.Common.DbCommandBuilder.QuotePrefix" /> and <see cref="P:System.Data.Common.DbCommandBuilder.QuoteSuffix" />.</param>
		/// <returns>The quoted version of the identifier. Embedded quotes within the identifier are correctly escaped.</returns>
		// Token: 0x060029D5 RID: 10709 RVA: 0x00051759 File Offset: 0x0004F959
		public string QuoteIdentifier(string unquotedIdentifier, OleDbConnection connection)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		// Token: 0x060029D6 RID: 10710 RVA: 0x00010458 File Offset: 0x0000E658
		protected override void SetRowUpdatingHandler(DbDataAdapter adapter)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Given a quoted identifier, returns the correct unquoted form of that identifier. This includes correctly un-escaping any embedded quotes in the identifier.</summary>
		/// <param name="quotedIdentifier">The identifier that will have its embedded quotes removed.</param>
		/// <param name="connection">The <see cref="T:System.Data.OleDb.OleDbConnection" />.</param>
		/// <returns>The unquoted identifier, with embedded quotes correctly un-escaped.</returns>
		// Token: 0x060029D7 RID: 10711 RVA: 0x00051759 File Offset: 0x0004F959
		public string UnquoteIdentifier(string quotedIdentifier, OleDbConnection connection)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}
	}
}
