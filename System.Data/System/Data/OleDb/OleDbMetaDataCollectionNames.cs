﻿using System;

namespace System.Data.OleDb
{
	/// <summary>Provides a list of constants for use with the GetSchema method to retrieve metadata collections.</summary>
	// Token: 0x0200036A RID: 874
	public static class OleDbMetaDataCollectionNames
	{
		/// <summary>A constant for use with the GetSchema method that represents the Catalogs collection.</summary>
		// Token: 0x0400193F RID: 6463
		public static readonly string Catalogs;

		/// <summary>A constant for use with the GetSchema method that represents the Collations collection.</summary>
		// Token: 0x04001940 RID: 6464
		public static readonly string Collations;

		/// <summary>A constant for use with the GetSchema method that represents the Columns collection.</summary>
		// Token: 0x04001941 RID: 6465
		public static readonly string Columns;

		/// <summary>A constant for use with the GetSchema method that represents the Indexes collection.</summary>
		// Token: 0x04001942 RID: 6466
		public static readonly string Indexes;

		/// <summary>A constant for use with the GetSchema method that represents the ProcedureColumns collection.</summary>
		// Token: 0x04001943 RID: 6467
		public static readonly string ProcedureColumns;

		/// <summary>A constant for use with the GetSchema method that represents the ProcedureParameters collection.</summary>
		// Token: 0x04001944 RID: 6468
		public static readonly string ProcedureParameters;

		/// <summary>A constant for use with the GetSchema method that represents the Procedures collection.</summary>
		// Token: 0x04001945 RID: 6469
		public static readonly string Procedures;

		/// <summary>A constant for use with the GetSchema method that represents the Tables collection.</summary>
		// Token: 0x04001946 RID: 6470
		public static readonly string Tables;

		/// <summary>A constant for use with the GetSchema method that represents the Views collection. </summary>
		// Token: 0x04001947 RID: 6471
		public static readonly string Views;
	}
}
