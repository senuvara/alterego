﻿using System;
using System.ComponentModel;

namespace System.Data
{
	// Token: 0x02000095 RID: 149
	internal sealed class DataTableReaderListener
	{
		// Token: 0x06000906 RID: 2310 RVA: 0x0002A144 File Offset: 0x00028344
		internal DataTableReaderListener(DataTableReader reader)
		{
			if (reader == null)
			{
				throw ExceptionBuilder.ArgumentNull("DataTableReader");
			}
			if (this._currentDataTable != null)
			{
				this.UnSubscribeEvents();
			}
			this._readerWeak = new WeakReference(reader);
			this._currentDataTable = reader.CurrentDataTable;
			if (this._currentDataTable != null)
			{
				this.SubscribeEvents();
			}
		}

		// Token: 0x06000907 RID: 2311 RVA: 0x0002A199 File Offset: 0x00028399
		internal void CleanUp()
		{
			this.UnSubscribeEvents();
		}

		// Token: 0x06000908 RID: 2312 RVA: 0x0002A1A1 File Offset: 0x000283A1
		internal void UpdataTable(DataTable datatable)
		{
			if (datatable == null)
			{
				throw ExceptionBuilder.ArgumentNull("DataTable");
			}
			this.UnSubscribeEvents();
			this._currentDataTable = datatable;
			this.SubscribeEvents();
		}

		// Token: 0x06000909 RID: 2313 RVA: 0x0002A1C4 File Offset: 0x000283C4
		private void SubscribeEvents()
		{
			if (this._currentDataTable == null)
			{
				return;
			}
			if (this._isSubscribed)
			{
				return;
			}
			this._currentDataTable.Columns.ColumnPropertyChanged += this.SchemaChanged;
			this._currentDataTable.Columns.CollectionChanged += this.SchemaChanged;
			this._currentDataTable.RowChanged += this.DataChanged;
			this._currentDataTable.RowDeleted += this.DataChanged;
			this._currentDataTable.TableCleared += this.DataTableCleared;
			this._isSubscribed = true;
		}

		// Token: 0x0600090A RID: 2314 RVA: 0x0002A268 File Offset: 0x00028468
		private void UnSubscribeEvents()
		{
			if (this._currentDataTable == null)
			{
				return;
			}
			if (!this._isSubscribed)
			{
				return;
			}
			this._currentDataTable.Columns.ColumnPropertyChanged -= this.SchemaChanged;
			this._currentDataTable.Columns.CollectionChanged -= this.SchemaChanged;
			this._currentDataTable.RowChanged -= this.DataChanged;
			this._currentDataTable.RowDeleted -= this.DataChanged;
			this._currentDataTable.TableCleared -= this.DataTableCleared;
			this._isSubscribed = false;
		}

		// Token: 0x0600090B RID: 2315 RVA: 0x0002A30C File Offset: 0x0002850C
		private void DataTableCleared(object sender, DataTableClearEventArgs e)
		{
			DataTableReader dataTableReader = (DataTableReader)this._readerWeak.Target;
			if (dataTableReader != null)
			{
				dataTableReader.DataTableCleared();
				return;
			}
			this.UnSubscribeEvents();
		}

		// Token: 0x0600090C RID: 2316 RVA: 0x0002A33C File Offset: 0x0002853C
		private void SchemaChanged(object sender, CollectionChangeEventArgs e)
		{
			DataTableReader dataTableReader = (DataTableReader)this._readerWeak.Target;
			if (dataTableReader != null)
			{
				dataTableReader.SchemaChanged();
				return;
			}
			this.UnSubscribeEvents();
		}

		// Token: 0x0600090D RID: 2317 RVA: 0x0002A36C File Offset: 0x0002856C
		private void DataChanged(object sender, DataRowChangeEventArgs args)
		{
			DataTableReader dataTableReader = (DataTableReader)this._readerWeak.Target;
			if (dataTableReader != null)
			{
				dataTableReader.DataChanged(args);
				return;
			}
			this.UnSubscribeEvents();
		}

		// Token: 0x04000641 RID: 1601
		private DataTable _currentDataTable;

		// Token: 0x04000642 RID: 1602
		private bool _isSubscribed;

		// Token: 0x04000643 RID: 1603
		private WeakReference _readerWeak;
	}
}
