﻿using System;

namespace System.Data
{
	// Token: 0x02000082 RID: 130
	// (Invoke) Token: 0x06000667 RID: 1639
	internal delegate void DataSetClearEventhandler(object sender, DataTable table);
}
