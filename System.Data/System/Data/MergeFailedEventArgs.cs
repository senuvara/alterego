﻿using System;
using System.Runtime.CompilerServices;

namespace System.Data
{
	/// <summary>Occurs when a target and source <see langword="DataRow" /> have the same primary key value, and the <see cref="P:System.Data.DataSet.EnforceConstraints" /> property is set to true.</summary>
	// Token: 0x020000D3 RID: 211
	public class MergeFailedEventArgs : EventArgs
	{
		/// <summary>Initializes a new instance of a <see cref="T:System.Data.MergeFailedEventArgs" /> class with the <see cref="T:System.Data.DataTable" /> and a description of the merge conflict.</summary>
		/// <param name="table">The <see cref="T:System.Data.DataTable" /> object. </param>
		/// <param name="conflict">A description of the merge conflict. </param>
		// Token: 0x06000BCA RID: 3018 RVA: 0x00035203 File Offset: 0x00033403
		public MergeFailedEventArgs(DataTable table, string conflict)
		{
			this.Table = table;
			this.Conflict = conflict;
		}

		/// <summary>Returns the <see cref="T:System.Data.DataTable" /> object.</summary>
		/// <returns>The <see cref="T:System.Data.DataTable" /> object.</returns>
		// Token: 0x1700021F RID: 543
		// (get) Token: 0x06000BCB RID: 3019 RVA: 0x00035219 File Offset: 0x00033419
		public DataTable Table
		{
			[CompilerGenerated]
			get
			{
				return this.<Table>k__BackingField;
			}
		}

		/// <summary>Returns a description of the merge conflict.</summary>
		/// <returns>A description of the merge conflict.</returns>
		// Token: 0x17000220 RID: 544
		// (get) Token: 0x06000BCC RID: 3020 RVA: 0x00035221 File Offset: 0x00033421
		public string Conflict
		{
			[CompilerGenerated]
			get
			{
				return this.<Conflict>k__BackingField;
			}
		}

		// Token: 0x040007DC RID: 2012
		[CompilerGenerated]
		private readonly DataTable <Table>k__BackingField;

		// Token: 0x040007DD RID: 2013
		[CompilerGenerated]
		private readonly string <Conflict>k__BackingField;
	}
}
