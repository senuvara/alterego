﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace System.Data
{
	// Token: 0x02000093 RID: 147
	internal sealed class DataTablePropertyDescriptor : PropertyDescriptor
	{
		// Token: 0x17000189 RID: 393
		// (get) Token: 0x060008C6 RID: 2246 RVA: 0x00028A56 File Offset: 0x00026C56
		public DataTable Table
		{
			[CompilerGenerated]
			get
			{
				return this.<Table>k__BackingField;
			}
		}

		// Token: 0x060008C7 RID: 2247 RVA: 0x00028A5E File Offset: 0x00026C5E
		internal DataTablePropertyDescriptor(DataTable dataTable) : base(dataTable.TableName, null)
		{
			this.Table = dataTable;
		}

		// Token: 0x1700018A RID: 394
		// (get) Token: 0x060008C8 RID: 2248 RVA: 0x00013FC2 File Offset: 0x000121C2
		public override Type ComponentType
		{
			get
			{
				return typeof(DataRowView);
			}
		}

		// Token: 0x1700018B RID: 395
		// (get) Token: 0x060008C9 RID: 2249 RVA: 0x000061C5 File Offset: 0x000043C5
		public override bool IsReadOnly
		{
			get
			{
				return false;
			}
		}

		// Token: 0x1700018C RID: 396
		// (get) Token: 0x060008CA RID: 2250 RVA: 0x00017FD8 File Offset: 0x000161D8
		public override Type PropertyType
		{
			get
			{
				return typeof(IBindingList);
			}
		}

		// Token: 0x060008CB RID: 2251 RVA: 0x00028A74 File Offset: 0x00026C74
		public override bool Equals(object other)
		{
			return other is DataTablePropertyDescriptor && ((DataTablePropertyDescriptor)other).Table == this.Table;
		}

		// Token: 0x060008CC RID: 2252 RVA: 0x00028A93 File Offset: 0x00026C93
		public override int GetHashCode()
		{
			return this.Table.GetHashCode();
		}

		// Token: 0x060008CD RID: 2253 RVA: 0x000061C5 File Offset: 0x000043C5
		public override bool CanResetValue(object component)
		{
			return false;
		}

		// Token: 0x060008CE RID: 2254 RVA: 0x00028AA0 File Offset: 0x00026CA0
		public override object GetValue(object component)
		{
			return ((DataViewManagerListItemTypeDescriptor)component).GetDataView(this.Table);
		}

		// Token: 0x060008CF RID: 2255 RVA: 0x00005E03 File Offset: 0x00004003
		public override void ResetValue(object component)
		{
		}

		// Token: 0x060008D0 RID: 2256 RVA: 0x00005E03 File Offset: 0x00004003
		public override void SetValue(object component, object value)
		{
		}

		// Token: 0x060008D1 RID: 2257 RVA: 0x000061C5 File Offset: 0x000043C5
		public override bool ShouldSerializeValue(object component)
		{
			return false;
		}

		// Token: 0x04000631 RID: 1585
		[CompilerGenerated]
		private readonly DataTable <Table>k__BackingField;
	}
}
