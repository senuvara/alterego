﻿using System;
using System.Runtime.CompilerServices;

namespace System.Data
{
	// Token: 0x0200005F RID: 95
	internal abstract class AutoIncrementValue
	{
		// Token: 0x170000DD RID: 221
		// (get) Token: 0x0600038B RID: 907 RVA: 0x000129DF File Offset: 0x00010BDF
		// (set) Token: 0x0600038C RID: 908 RVA: 0x000129E7 File Offset: 0x00010BE7
		internal bool Auto
		{
			[CompilerGenerated]
			get
			{
				return this.<Auto>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<Auto>k__BackingField = value;
			}
		}

		// Token: 0x170000DE RID: 222
		// (get) Token: 0x0600038D RID: 909
		// (set) Token: 0x0600038E RID: 910
		internal abstract object Current { get; set; }

		// Token: 0x170000DF RID: 223
		// (get) Token: 0x0600038F RID: 911
		// (set) Token: 0x06000390 RID: 912
		internal abstract long Seed { get; set; }

		// Token: 0x170000E0 RID: 224
		// (get) Token: 0x06000391 RID: 913
		// (set) Token: 0x06000392 RID: 914
		internal abstract long Step { get; set; }

		// Token: 0x170000E1 RID: 225
		// (get) Token: 0x06000393 RID: 915
		internal abstract Type DataType { get; }

		// Token: 0x06000394 RID: 916
		internal abstract void SetCurrent(object value, IFormatProvider formatProvider);

		// Token: 0x06000395 RID: 917
		internal abstract void SetCurrentAndIncrement(object value);

		// Token: 0x06000396 RID: 918
		internal abstract void MoveAfter();

		// Token: 0x06000397 RID: 919 RVA: 0x000129F0 File Offset: 0x00010BF0
		internal AutoIncrementValue Clone()
		{
			AutoIncrementInt64 autoIncrementInt = (this is AutoIncrementInt64) ? new AutoIncrementInt64() : new AutoIncrementBigInteger();
			autoIncrementInt.Auto = this.Auto;
			autoIncrementInt.Seed = this.Seed;
			autoIncrementInt.Step = this.Step;
			autoIncrementInt.Current = this.Current;
			return autoIncrementInt;
		}

		// Token: 0x06000398 RID: 920 RVA: 0x00005C14 File Offset: 0x00003E14
		protected AutoIncrementValue()
		{
		}

		// Token: 0x04000538 RID: 1336
		[CompilerGenerated]
		private bool <Auto>k__BackingField;
	}
}
