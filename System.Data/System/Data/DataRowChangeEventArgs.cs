﻿using System;
using System.Runtime.CompilerServices;

namespace System.Data
{
	/// <summary>Provides data for the <see cref="E:System.Data.DataTable.RowChanged" />, <see cref="E:System.Data.DataTable.RowChanging" />, <see cref="M:System.Data.DataTable.OnRowDeleting(System.Data.DataRowChangeEventArgs)" />, and <see cref="M:System.Data.DataTable.OnRowDeleted(System.Data.DataRowChangeEventArgs)" /> events.</summary>
	// Token: 0x0200007D RID: 125
	public class DataRowChangeEventArgs : EventArgs
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Data.DataRowChangeEventArgs" /> class.</summary>
		/// <param name="row">The <see cref="T:System.Data.DataRow" /> upon which an action is occuring. </param>
		/// <param name="action">One of the <see cref="T:System.Data.DataRowAction" /> values. </param>
		// Token: 0x06000640 RID: 1600 RVA: 0x00019846 File Offset: 0x00017A46
		public DataRowChangeEventArgs(DataRow row, DataRowAction action)
		{
			this.Row = row;
			this.Action = action;
		}

		/// <summary>Gets the row upon which an action has occurred.</summary>
		/// <returns>The <see cref="T:System.Data.DataRow" /> upon which an action has occurred.</returns>
		// Token: 0x1700012E RID: 302
		// (get) Token: 0x06000641 RID: 1601 RVA: 0x0001985C File Offset: 0x00017A5C
		public DataRow Row
		{
			[CompilerGenerated]
			get
			{
				return this.<Row>k__BackingField;
			}
		}

		/// <summary>Gets the action that has occurred on a <see cref="T:System.Data.DataRow" />.</summary>
		/// <returns>One of the <see cref="T:System.Data.DataRowAction" /> values.</returns>
		// Token: 0x1700012F RID: 303
		// (get) Token: 0x06000642 RID: 1602 RVA: 0x00019864 File Offset: 0x00017A64
		public DataRowAction Action
		{
			[CompilerGenerated]
			get
			{
				return this.<Action>k__BackingField;
			}
		}

		// Token: 0x04000594 RID: 1428
		[CompilerGenerated]
		private readonly DataRow <Row>k__BackingField;

		// Token: 0x04000595 RID: 1429
		[CompilerGenerated]
		private readonly DataRowAction <Action>k__BackingField;
	}
}
