﻿using System;
using System.Runtime.Serialization;

namespace System.Data
{
	/// <summary>Represents the exception that is thrown when attempting an action that violates a constraint.</summary>
	// Token: 0x02000069 RID: 105
	[Serializable]
	public class ConstraintException : DataException
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Data.ConstraintException" /> class using the specified serialization and stream context.</summary>
		/// <param name="info">The data necessary to serialize or deserialize an object. </param>
		/// <param name="context">Description of the source and destination of the specified serialized stream. </param>
		// Token: 0x06000410 RID: 1040 RVA: 0x000143D9 File Offset: 0x000125D9
		protected ConstraintException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Data.ConstraintException" /> class. This is the default constructor.</summary>
		// Token: 0x06000411 RID: 1041 RVA: 0x000143E8 File Offset: 0x000125E8
		public ConstraintException() : base("Constraint Exception.")
		{
			base.HResult = -2146232022;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Data.ConstraintException" /> class with the specified string.</summary>
		/// <param name="s">The string to display when the exception is thrown. </param>
		// Token: 0x06000412 RID: 1042 RVA: 0x00014400 File Offset: 0x00012600
		public ConstraintException(string s) : base(s)
		{
			base.HResult = -2146232022;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Data.ConstraintException" /> class using the specified string and inner exception.</summary>
		/// <param name="message">The string to display when the exception is thrown. </param>
		/// <param name="innerException">Gets the <see langword="Exception" /> instance that caused the current exception.</param>
		// Token: 0x06000413 RID: 1043 RVA: 0x00014414 File Offset: 0x00012614
		public ConstraintException(string message, Exception innerException) : base(message, innerException)
		{
			base.HResult = -2146232022;
		}
	}
}
