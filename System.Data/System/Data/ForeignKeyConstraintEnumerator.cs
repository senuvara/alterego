﻿using System;

namespace System.Data
{
	// Token: 0x0200005A RID: 90
	internal class ForeignKeyConstraintEnumerator : ConstraintEnumerator
	{
		// Token: 0x060002FC RID: 764 RVA: 0x000106EA File Offset: 0x0000E8EA
		public ForeignKeyConstraintEnumerator(DataSet dataSet) : base(dataSet)
		{
		}

		// Token: 0x060002FD RID: 765 RVA: 0x000106F3 File Offset: 0x0000E8F3
		protected override bool IsValidCandidate(Constraint constraint)
		{
			return constraint is ForeignKeyConstraint;
		}

		// Token: 0x060002FE RID: 766 RVA: 0x000106FE File Offset: 0x0000E8FE
		public ForeignKeyConstraint GetForeignKeyConstraint()
		{
			return (ForeignKeyConstraint)base.CurrentObject;
		}
	}
}
