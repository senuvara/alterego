﻿using System;

namespace System.Data
{
	// Token: 0x02000066 RID: 102
	internal sealed class DataError
	{
		// Token: 0x06000400 RID: 1024 RVA: 0x000140C3 File Offset: 0x000122C3
		internal DataError()
		{
		}

		// Token: 0x06000401 RID: 1025 RVA: 0x000140D6 File Offset: 0x000122D6
		internal DataError(string rowError)
		{
			this.SetText(rowError);
		}

		// Token: 0x170000FA RID: 250
		// (get) Token: 0x06000402 RID: 1026 RVA: 0x000140F0 File Offset: 0x000122F0
		// (set) Token: 0x06000403 RID: 1027 RVA: 0x000140F8 File Offset: 0x000122F8
		internal string Text
		{
			get
			{
				return this._rowError;
			}
			set
			{
				this.SetText(value);
			}
		}

		// Token: 0x170000FB RID: 251
		// (get) Token: 0x06000404 RID: 1028 RVA: 0x00014101 File Offset: 0x00012301
		internal bool HasErrors
		{
			get
			{
				return this._rowError.Length != 0 || this._count != 0;
			}
		}

		// Token: 0x06000405 RID: 1029 RVA: 0x0001411C File Offset: 0x0001231C
		internal void SetColumnError(DataColumn column, string error)
		{
			if (error == null || error.Length == 0)
			{
				this.Clear(column);
				return;
			}
			if (this._errorList == null)
			{
				this._errorList = new DataError.ColumnError[1];
			}
			int num = this.IndexOf(column);
			this._errorList[num]._column = column;
			this._errorList[num]._error = error;
			column._errors++;
			if (num == this._count)
			{
				this._count++;
			}
		}

		// Token: 0x06000406 RID: 1030 RVA: 0x000141A4 File Offset: 0x000123A4
		internal string GetColumnError(DataColumn column)
		{
			for (int i = 0; i < this._count; i++)
			{
				if (this._errorList[i]._column == column)
				{
					return this._errorList[i]._error;
				}
			}
			return string.Empty;
		}

		// Token: 0x06000407 RID: 1031 RVA: 0x000141F0 File Offset: 0x000123F0
		internal void Clear(DataColumn column)
		{
			if (this._count == 0)
			{
				return;
			}
			for (int i = 0; i < this._count; i++)
			{
				if (this._errorList[i]._column == column)
				{
					Array.Copy(this._errorList, i + 1, this._errorList, i, this._count - i - 1);
					this._count--;
					column._errors--;
				}
			}
		}

		// Token: 0x06000408 RID: 1032 RVA: 0x00014268 File Offset: 0x00012468
		internal void Clear()
		{
			for (int i = 0; i < this._count; i++)
			{
				this._errorList[i]._column._errors--;
			}
			this._count = 0;
			this._rowError = string.Empty;
		}

		// Token: 0x06000409 RID: 1033 RVA: 0x000142B8 File Offset: 0x000124B8
		internal DataColumn[] GetColumnsInError()
		{
			DataColumn[] array = new DataColumn[this._count];
			for (int i = 0; i < this._count; i++)
			{
				array[i] = this._errorList[i]._column;
			}
			return array;
		}

		// Token: 0x0600040A RID: 1034 RVA: 0x000142F7 File Offset: 0x000124F7
		private void SetText(string errorText)
		{
			if (errorText == null)
			{
				errorText = string.Empty;
			}
			this._rowError = errorText;
		}

		// Token: 0x0600040B RID: 1035 RVA: 0x0001430C File Offset: 0x0001250C
		internal int IndexOf(DataColumn column)
		{
			for (int i = 0; i < this._count; i++)
			{
				if (this._errorList[i]._column == column)
				{
					return i;
				}
			}
			if (this._count >= this._errorList.Length)
			{
				DataError.ColumnError[] array = new DataError.ColumnError[Math.Min(this._count * 2, column.Table.Columns.Count)];
				Array.Copy(this._errorList, 0, array, 0, this._count);
				this._errorList = array;
			}
			return this._count;
		}

		// Token: 0x0400054F RID: 1359
		private string _rowError = string.Empty;

		// Token: 0x04000550 RID: 1360
		private int _count;

		// Token: 0x04000551 RID: 1361
		private DataError.ColumnError[] _errorList;

		// Token: 0x04000552 RID: 1362
		internal const int initialCapacity = 1;

		// Token: 0x02000067 RID: 103
		internal struct ColumnError
		{
			// Token: 0x04000553 RID: 1363
			internal DataColumn _column;

			// Token: 0x04000554 RID: 1364
			internal string _error;
		}
	}
}
