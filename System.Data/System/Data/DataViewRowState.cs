﻿using System;

namespace System.Data
{
	/// <summary>Describes the version of data in a <see cref="T:System.Data.DataRow" />.</summary>
	// Token: 0x0200009D RID: 157
	[Flags]
	public enum DataViewRowState
	{
		/// <summary>None.</summary>
		// Token: 0x04000673 RID: 1651
		None = 0,
		/// <summary>An unchanged row.</summary>
		// Token: 0x04000674 RID: 1652
		Unchanged = 2,
		/// <summary>A new row.</summary>
		// Token: 0x04000675 RID: 1653
		Added = 4,
		/// <summary>A deleted row.</summary>
		// Token: 0x04000676 RID: 1654
		Deleted = 8,
		/// <summary>A current version of original data that has been modified (see <see langword="ModifiedOriginal" />).</summary>
		// Token: 0x04000677 RID: 1655
		ModifiedCurrent = 16,
		/// <summary>The original version of the data that was modified. (Although the data has since been modified, it is available as <see langword="ModifiedCurrent" />).</summary>
		// Token: 0x04000678 RID: 1656
		ModifiedOriginal = 32,
		/// <summary>Original rows including unchanged and deleted rows.</summary>
		// Token: 0x04000679 RID: 1657
		OriginalRows = 42,
		/// <summary>Current rows including unchanged, new, and modified rows. By default, <see cref="T:System.Data.DataViewRowState" /> is set to CurrentRows.</summary>
		// Token: 0x0400067A RID: 1658
		CurrentRows = 22
	}
}
