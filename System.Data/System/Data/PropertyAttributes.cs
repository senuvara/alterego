﻿using System;
using System.ComponentModel;

namespace System.Data
{
	/// <summary>Specifies the attributes of a property.</summary>
	// Token: 0x0200011B RID: 283
	[EditorBrowsable(EditorBrowsableState.Never)]
	[Obsolete("PropertyAttributes has been deprecated.  http://go.microsoft.com/fwlink/?linkid=14202")]
	[Flags]
	public enum PropertyAttributes
	{
		/// <summary>The property is not supported by the provider.</summary>
		// Token: 0x040009F4 RID: 2548
		NotSupported = 0,
		/// <summary>The user must specify a value for this property before the data source is initialized.</summary>
		// Token: 0x040009F5 RID: 2549
		Required = 1,
		/// <summary>The user does not need to specify a value for this property before the data source is initialized.</summary>
		// Token: 0x040009F6 RID: 2550
		Optional = 2,
		/// <summary>The user can read the property.</summary>
		// Token: 0x040009F7 RID: 2551
		Read = 512,
		/// <summary>The user can write to the property.</summary>
		// Token: 0x040009F8 RID: 2552
		Write = 1024
	}
}
