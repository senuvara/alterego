﻿using System;
using System.Xml.Schema;

namespace System.Data
{
	// Token: 0x02000102 RID: 258
	internal sealed class ConstraintTable
	{
		// Token: 0x06000D40 RID: 3392 RVA: 0x0003E684 File Offset: 0x0003C884
		public ConstraintTable(DataTable t, XmlSchemaIdentityConstraint c)
		{
			this.table = t;
			this.constraint = c;
		}

		// Token: 0x040008BD RID: 2237
		public DataTable table;

		// Token: 0x040008BE RID: 2238
		public XmlSchemaIdentityConstraint constraint;
	}
}
