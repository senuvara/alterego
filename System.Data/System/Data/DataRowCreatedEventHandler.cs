﻿using System;

namespace System.Data
{
	// Token: 0x02000081 RID: 129
	// (Invoke) Token: 0x06000663 RID: 1635
	internal delegate void DataRowCreatedEventHandler(object sender, DataRow r);
}
