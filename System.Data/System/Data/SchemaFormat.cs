﻿using System;

namespace System.Data
{
	// Token: 0x0200010F RID: 271
	internal enum SchemaFormat
	{
		// Token: 0x040009C5 RID: 2501
		Public = 1,
		// Token: 0x040009C6 RID: 2502
		Remoting,
		// Token: 0x040009C7 RID: 2503
		WebService,
		// Token: 0x040009C8 RID: 2504
		RemotingSkipSchema,
		// Token: 0x040009C9 RID: 2505
		WebServiceSkipSchema
	}
}
