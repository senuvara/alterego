﻿using System;
using System.Data.Common;
using System.Numerics;

namespace System.Data
{
	// Token: 0x02000061 RID: 97
	internal sealed class AutoIncrementBigInteger : AutoIncrementValue
	{
		// Token: 0x170000E6 RID: 230
		// (get) Token: 0x060003A5 RID: 933 RVA: 0x00012B92 File Offset: 0x00010D92
		// (set) Token: 0x060003A6 RID: 934 RVA: 0x00012B9F File Offset: 0x00010D9F
		internal override object Current
		{
			get
			{
				return this._current;
			}
			set
			{
				this._current = (BigInteger)value;
			}
		}

		// Token: 0x170000E7 RID: 231
		// (get) Token: 0x060003A7 RID: 935 RVA: 0x00012BAD File Offset: 0x00010DAD
		internal override Type DataType
		{
			get
			{
				return typeof(BigInteger);
			}
		}

		// Token: 0x170000E8 RID: 232
		// (get) Token: 0x060003A8 RID: 936 RVA: 0x00012BB9 File Offset: 0x00010DB9
		// (set) Token: 0x060003A9 RID: 937 RVA: 0x00012BC1 File Offset: 0x00010DC1
		internal override long Seed
		{
			get
			{
				return this._seed;
			}
			set
			{
				if (this._current == this._seed || this.BoundaryCheck(value))
				{
					this._current = value;
				}
				this._seed = value;
			}
		}

		// Token: 0x170000E9 RID: 233
		// (get) Token: 0x060003AA RID: 938 RVA: 0x00012BF7 File Offset: 0x00010DF7
		// (set) Token: 0x060003AB RID: 939 RVA: 0x00012C04 File Offset: 0x00010E04
		internal override long Step
		{
			get
			{
				return (long)this._step;
			}
			set
			{
				if (value == 0L)
				{
					throw ExceptionBuilder.AutoIncrementSeed();
				}
				if (this._step != value)
				{
					if (this._current != this.Seed)
					{
						this._current = this._current - this._step + value;
					}
					this._step = value;
				}
			}
		}

		// Token: 0x060003AC RID: 940 RVA: 0x00012C69 File Offset: 0x00010E69
		internal override void MoveAfter()
		{
			this._current += this._step;
		}

		// Token: 0x060003AD RID: 941 RVA: 0x00012C82 File Offset: 0x00010E82
		internal override void SetCurrent(object value, IFormatProvider formatProvider)
		{
			this._current = BigIntegerStorage.ConvertToBigInteger(value, formatProvider);
		}

		// Token: 0x060003AE RID: 942 RVA: 0x00012C94 File Offset: 0x00010E94
		internal override void SetCurrentAndIncrement(object value)
		{
			BigInteger bigInteger = (BigInteger)value;
			if (this.BoundaryCheck(bigInteger))
			{
				this._current = bigInteger + this._step;
			}
		}

		// Token: 0x060003AF RID: 943 RVA: 0x00012CC3 File Offset: 0x00010EC3
		private bool BoundaryCheck(BigInteger value)
		{
			return (this._step < 0L && value <= this._current) || (0L < this._step && this._current <= value);
		}

		// Token: 0x060003B0 RID: 944 RVA: 0x00012D01 File Offset: 0x00010F01
		public AutoIncrementBigInteger()
		{
		}

		// Token: 0x0400053C RID: 1340
		private BigInteger _current;

		// Token: 0x0400053D RID: 1341
		private long _seed;

		// Token: 0x0400053E RID: 1342
		private BigInteger _step = 1;
	}
}
