﻿using System;

namespace System.Data
{
	/// <summary>Provides data for the <see cref="E:System.Data.Common.DataAdapter.FillError" /> event of a <see cref="T:System.Data.Common.DbDataAdapter" />.</summary>
	// Token: 0x020000A3 RID: 163
	public class FillErrorEventArgs : EventArgs
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Data.FillErrorEventArgs" /> class.</summary>
		/// <param name="dataTable">The <see cref="T:System.Data.DataTable" /> being updated. </param>
		/// <param name="values">The values for the row being updated. </param>
		// Token: 0x06000A08 RID: 2568 RVA: 0x0002D117 File Offset: 0x0002B317
		public FillErrorEventArgs(DataTable dataTable, object[] values)
		{
			this._dataTable = dataTable;
			this._values = values;
			if (this._values == null)
			{
				this._values = Array.Empty<object>();
			}
		}

		/// <summary>Gets or sets a value indicating whether to continue the fill operation despite the error.</summary>
		/// <returns>
		///     <see langword="true" /> if the fill operation should continue; otherwise, <see langword="false" />.</returns>
		// Token: 0x170001DA RID: 474
		// (get) Token: 0x06000A09 RID: 2569 RVA: 0x0002D140 File Offset: 0x0002B340
		// (set) Token: 0x06000A0A RID: 2570 RVA: 0x0002D148 File Offset: 0x0002B348
		public bool Continue
		{
			get
			{
				return this._continueFlag;
			}
			set
			{
				this._continueFlag = value;
			}
		}

		/// <summary>Gets the <see cref="T:System.Data.DataTable" /> being updated when the error occurred.</summary>
		/// <returns>The <see cref="T:System.Data.DataTable" /> being updated.</returns>
		// Token: 0x170001DB RID: 475
		// (get) Token: 0x06000A0B RID: 2571 RVA: 0x0002D151 File Offset: 0x0002B351
		public DataTable DataTable
		{
			get
			{
				return this._dataTable;
			}
		}

		/// <summary>Gets the errors being handled.</summary>
		/// <returns>The errors being handled.</returns>
		// Token: 0x170001DC RID: 476
		// (get) Token: 0x06000A0C RID: 2572 RVA: 0x0002D159 File Offset: 0x0002B359
		// (set) Token: 0x06000A0D RID: 2573 RVA: 0x0002D161 File Offset: 0x0002B361
		public Exception Errors
		{
			get
			{
				return this._errors;
			}
			set
			{
				this._errors = value;
			}
		}

		/// <summary>Gets the values for the row being updated when the error occurred.</summary>
		/// <returns>The values for the row being updated.</returns>
		// Token: 0x170001DD RID: 477
		// (get) Token: 0x06000A0E RID: 2574 RVA: 0x0002D16C File Offset: 0x0002B36C
		public object[] Values
		{
			get
			{
				object[] array = new object[this._values.Length];
				for (int i = 0; i < this._values.Length; i++)
				{
					array[i] = this._values[i];
				}
				return array;
			}
		}

		// Token: 0x040006A3 RID: 1699
		private bool _continueFlag;

		// Token: 0x040006A4 RID: 1700
		private DataTable _dataTable;

		// Token: 0x040006A5 RID: 1701
		private Exception _errors;

		// Token: 0x040006A6 RID: 1702
		private object[] _values;
	}
}
