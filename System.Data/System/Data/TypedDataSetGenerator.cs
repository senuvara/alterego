﻿using System;
using System.CodeDom;
using System.CodeDom.Compiler;
using System.Security.Permissions;
using Unity;

namespace System.Data
{
	/// <summary>Used to create a strongly typed <see cref="T:System.Data.DataSet" />.</summary>
	// Token: 0x0200033A RID: 826
	[Obsolete("TypedDataSetGenerator class will be removed in a future release. Please use System.Data.Design.TypedDataSetGenerator in System.Design.dll.")]
	[HostProtection(SecurityAction.LinkDemand, SharedState = true, Synchronization = true)]
	public class TypedDataSetGenerator
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Data.TypedDataSetGenerator" /> class.</summary>
		// Token: 0x06002946 RID: 10566 RVA: 0x00010458 File Offset: 0x0000E658
		public TypedDataSetGenerator()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Generates a strongly typed <see cref="T:System.Data.DataSet" />.</summary>
		/// <param name="dataSet">The source <see cref="T:System.Data.DataSet" /> that specifies the metadata for the typed <see cref="T:System.Data.DataSet" />. </param>
		/// <param name="codeNamespace">The namespace that provides the target namespace for the typed <see cref="T:System.Data.DataSet" />. </param>
		/// <param name="codeGen">The generator used to create the typed <see cref="T:System.Data.DataSet" />. </param>
		// Token: 0x06002947 RID: 10567 RVA: 0x00010458 File Offset: 0x0000E658
		public static void Generate(DataSet dataSet, CodeNamespace codeNamespace, ICodeGenerator codeGen)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Transforms a string in a valid, typed <see cref="T:System.Data.DataSet" /> name.</summary>
		/// <param name="name">The source name to transform into a valid, typed <see cref="T:System.Data.DataSet" /> name. </param>
		/// <param name="codeGen">The generator used to perform the conversion. </param>
		/// <returns>A string that is the converted name.</returns>
		// Token: 0x06002948 RID: 10568 RVA: 0x00051759 File Offset: 0x0004F959
		public static string GenerateIdName(string name, ICodeGenerator codeGen)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}
	}
}
