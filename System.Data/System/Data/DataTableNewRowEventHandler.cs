﻿using System;

namespace System.Data
{
	/// <summary>Represents the method that handles the <see cref="M:System.Data.DataTable.NewRow" /> method.</summary>
	/// <param name="sender">The source of the event.</param>
	/// <param name="e">A <see cref="T:System.Data.DataTableNewRowEventArgs" /> that contains the event data.</param>
	// Token: 0x02000092 RID: 146
	// (Invoke) Token: 0x060008C3 RID: 2243
	public delegate void DataTableNewRowEventHandler(object sender, DataTableNewRowEventArgs e);
}
