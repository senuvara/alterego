﻿using System;

namespace System.Data
{
	// Token: 0x0200005B RID: 91
	internal sealed class ChildForeignKeyConstraintEnumerator : ForeignKeyConstraintEnumerator
	{
		// Token: 0x060002FF RID: 767 RVA: 0x0001070B File Offset: 0x0000E90B
		public ChildForeignKeyConstraintEnumerator(DataSet dataSet, DataTable inTable) : base(dataSet)
		{
			this._table = inTable;
		}

		// Token: 0x06000300 RID: 768 RVA: 0x0001071B File Offset: 0x0000E91B
		protected override bool IsValidCandidate(Constraint constraint)
		{
			return constraint is ForeignKeyConstraint && ((ForeignKeyConstraint)constraint).Table == this._table;
		}

		// Token: 0x04000512 RID: 1298
		private readonly DataTable _table;
	}
}
