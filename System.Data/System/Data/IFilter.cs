﻿using System;

namespace System.Data
{
	// Token: 0x020000BA RID: 186
	internal interface IFilter
	{
		// Token: 0x06000AE4 RID: 2788
		bool Invoke(DataRow row, DataRowVersion version);
	}
}
