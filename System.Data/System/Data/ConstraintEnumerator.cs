﻿using System;
using System.Collections;

namespace System.Data
{
	// Token: 0x02000059 RID: 89
	internal class ConstraintEnumerator
	{
		// Token: 0x060002F7 RID: 759 RVA: 0x00010625 File Offset: 0x0000E825
		public ConstraintEnumerator(DataSet dataSet)
		{
			this._tables = ((dataSet != null) ? dataSet.Tables.GetEnumerator() : null);
			this._currentObject = null;
		}

		// Token: 0x060002F8 RID: 760 RVA: 0x0001064C File Offset: 0x0000E84C
		public bool GetNext()
		{
			this._currentObject = null;
			while (this._tables != null)
			{
				if (this._constraints == null)
				{
					if (!this._tables.MoveNext())
					{
						this._tables = null;
						return false;
					}
					this._constraints = ((DataTable)this._tables.Current).Constraints.GetEnumerator();
				}
				if (!this._constraints.MoveNext())
				{
					this._constraints = null;
				}
				else
				{
					Constraint constraint = (Constraint)this._constraints.Current;
					if (this.IsValidCandidate(constraint))
					{
						this._currentObject = constraint;
						return true;
					}
				}
			}
			return false;
		}

		// Token: 0x060002F9 RID: 761 RVA: 0x000106E2 File Offset: 0x0000E8E2
		public Constraint GetConstraint()
		{
			return this._currentObject;
		}

		// Token: 0x060002FA RID: 762 RVA: 0x0000EF1B File Offset: 0x0000D11B
		protected virtual bool IsValidCandidate(Constraint constraint)
		{
			return true;
		}

		// Token: 0x170000B0 RID: 176
		// (get) Token: 0x060002FB RID: 763 RVA: 0x000106E2 File Offset: 0x0000E8E2
		protected Constraint CurrentObject
		{
			get
			{
				return this._currentObject;
			}
		}

		// Token: 0x0400050F RID: 1295
		private IEnumerator _tables;

		// Token: 0x04000510 RID: 1296
		private IEnumerator _constraints;

		// Token: 0x04000511 RID: 1297
		private Constraint _currentObject;
	}
}
