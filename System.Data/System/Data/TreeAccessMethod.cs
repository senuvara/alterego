﻿using System;

namespace System.Data
{
	// Token: 0x020000DD RID: 221
	internal enum TreeAccessMethod
	{
		// Token: 0x04000809 RID: 2057
		KEY_SEARCH_AND_INDEX = 1,
		// Token: 0x0400080A RID: 2058
		INDEX_ONLY
	}
}
