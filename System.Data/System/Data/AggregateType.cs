﻿using System;

namespace System.Data
{
	// Token: 0x0200004E RID: 78
	internal enum AggregateType
	{
		// Token: 0x040004DC RID: 1244
		None,
		// Token: 0x040004DD RID: 1245
		Sum = 4,
		// Token: 0x040004DE RID: 1246
		Mean,
		// Token: 0x040004DF RID: 1247
		Min,
		// Token: 0x040004E0 RID: 1248
		Max,
		// Token: 0x040004E1 RID: 1249
		First,
		// Token: 0x040004E2 RID: 1250
		Count,
		// Token: 0x040004E3 RID: 1251
		Var,
		// Token: 0x040004E4 RID: 1252
		StDev
	}
}
