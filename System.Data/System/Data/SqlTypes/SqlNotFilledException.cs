﻿using System;

namespace System.Data.SqlTypes
{
	/// <summary>The <see cref="T:System.Data.SqlTypes.SqlNotFilledException" /> class is not intended for use as a stand-alone component, but as a class from which other classes derive standard functionality.</summary>
	// Token: 0x0200025A RID: 602
	[Serializable]
	public sealed class SqlNotFilledException : SqlTypeException
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Data.SqlTypes.SqlNotFilledException" /> class.</summary>
		// Token: 0x06001CDF RID: 7391 RVA: 0x0008C121 File Offset: 0x0008A321
		public SqlNotFilledException() : this(SQLResource.NotFilledMessage, null)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Data.SqlTypes.SqlNotFilledException" /> class.</summary>
		/// <param name="message">The string to display when the exception is thrown.</param>
		// Token: 0x06001CE0 RID: 7392 RVA: 0x0008C12F File Offset: 0x0008A32F
		public SqlNotFilledException(string message) : this(message, null)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Data.SqlTypes.SqlNotFilledException" /> class.</summary>
		/// <param name="message">The string to display when the exception is thrown.</param>
		/// <param name="e">A reference to an inner exception.</param>
		// Token: 0x06001CE1 RID: 7393 RVA: 0x0008C093 File Offset: 0x0008A293
		public SqlNotFilledException(string message, Exception e) : base(message, e)
		{
			base.HResult = -2146232015;
		}
	}
}
