﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Xml;

namespace System.Data.SqlTypes
{
	// Token: 0x0200025F RID: 607
	internal static class SqlTypeWorkarounds
	{
		// Token: 0x06001D16 RID: 7446 RVA: 0x0008CA6C File Offset: 0x0008AC6C
		internal static XmlReader SqlXmlCreateSqlXmlReader(Stream stream, bool closeInput = false, bool async = false)
		{
			XmlReaderSettings settings = closeInput ? (async ? SqlTypeWorkarounds.s_defaultXmlReaderSettingsAsyncCloseInput : SqlTypeWorkarounds.s_defaultXmlReaderSettingsCloseInput) : SqlTypeWorkarounds.s_defaultXmlReaderSettings;
			return XmlReader.Create(stream, settings);
		}

		// Token: 0x06001D17 RID: 7447 RVA: 0x0008CA9C File Offset: 0x0008AC9C
		internal static DateTime SqlDateTimeToDateTime(int daypart, int timepart)
		{
			if (daypart < -53690 || daypart > 2958463 || timepart < 0 || timepart > 25919999)
			{
				throw new OverflowException(SQLResource.DateTimeOverflowMessage);
			}
			long ticks = new DateTime(1900, 1, 1).Ticks;
			long num = (long)daypart * 864000000000L;
			long num2 = (long)((double)timepart / 0.3 + 0.5) * 10000L;
			return new DateTime(ticks + num + num2);
		}

		// Token: 0x06001D18 RID: 7448 RVA: 0x0008CB1C File Offset: 0x0008AD1C
		internal static SqlMoney SqlMoneyCtor(long value, int ignored)
		{
			SqlTypeWorkarounds.SqlMoneyCaster sqlMoneyCaster = default(SqlTypeWorkarounds.SqlMoneyCaster);
			sqlMoneyCaster.Fake._fNotNull = true;
			sqlMoneyCaster.Fake._value = value;
			return sqlMoneyCaster.Real;
		}

		// Token: 0x06001D19 RID: 7449 RVA: 0x0008CB54 File Offset: 0x0008AD54
		internal static long SqlMoneyToSqlInternalRepresentation(SqlMoney money)
		{
			SqlTypeWorkarounds.SqlMoneyCaster sqlMoneyCaster = default(SqlTypeWorkarounds.SqlMoneyCaster);
			sqlMoneyCaster.Real = money;
			if (money.IsNull)
			{
				throw new SqlNullValueException();
			}
			return sqlMoneyCaster.Fake._value;
		}

		// Token: 0x06001D1A RID: 7450 RVA: 0x0008CB8C File Offset: 0x0008AD8C
		internal static void SqlDecimalExtractData(SqlDecimal d, out uint data1, out uint data2, out uint data3, out uint data4)
		{
			SqlTypeWorkarounds.SqlDecimalCaster sqlDecimalCaster = new SqlTypeWorkarounds.SqlDecimalCaster
			{
				Real = d
			};
			data1 = sqlDecimalCaster.Fake._data1;
			data2 = sqlDecimalCaster.Fake._data2;
			data3 = sqlDecimalCaster.Fake._data3;
			data4 = sqlDecimalCaster.Fake._data4;
		}

		// Token: 0x06001D1B RID: 7451 RVA: 0x0008CBE0 File Offset: 0x0008ADE0
		internal static SqlBinary SqlBinaryCtor(byte[] value, bool ignored)
		{
			SqlTypeWorkarounds.SqlBinaryCaster sqlBinaryCaster = default(SqlTypeWorkarounds.SqlBinaryCaster);
			sqlBinaryCaster.Fake._value = value;
			return sqlBinaryCaster.Real;
		}

		// Token: 0x06001D1C RID: 7452 RVA: 0x0008CC08 File Offset: 0x0008AE08
		internal static SqlGuid SqlGuidCtor(byte[] value, bool ignored)
		{
			SqlTypeWorkarounds.SqlGuidCaster sqlGuidCaster = default(SqlTypeWorkarounds.SqlGuidCaster);
			sqlGuidCaster.Fake._value = value;
			return sqlGuidCaster.Real;
		}

		// Token: 0x06001D1D RID: 7453 RVA: 0x0008CC30 File Offset: 0x0008AE30
		// Note: this type is marked as 'beforefieldinit'.
		static SqlTypeWorkarounds()
		{
		}

		// Token: 0x040013B5 RID: 5045
		private static readonly XmlReaderSettings s_defaultXmlReaderSettings = new XmlReaderSettings
		{
			ConformanceLevel = ConformanceLevel.Fragment
		};

		// Token: 0x040013B6 RID: 5046
		private static readonly XmlReaderSettings s_defaultXmlReaderSettingsCloseInput = new XmlReaderSettings
		{
			ConformanceLevel = ConformanceLevel.Fragment,
			CloseInput = true
		};

		// Token: 0x040013B7 RID: 5047
		private static readonly XmlReaderSettings s_defaultXmlReaderSettingsAsyncCloseInput = new XmlReaderSettings
		{
			Async = true,
			ConformanceLevel = ConformanceLevel.Fragment,
			CloseInput = true
		};

		// Token: 0x040013B8 RID: 5048
		internal const SqlCompareOptions SqlStringValidSqlCompareOptionMask = SqlCompareOptions.IgnoreCase | SqlCompareOptions.IgnoreNonSpace | SqlCompareOptions.IgnoreKanaType | SqlCompareOptions.IgnoreWidth | SqlCompareOptions.BinarySort | SqlCompareOptions.BinarySort2;

		// Token: 0x02000260 RID: 608
		private struct SqlMoneyLookalike
		{
			// Token: 0x040013B9 RID: 5049
			internal bool _fNotNull;

			// Token: 0x040013BA RID: 5050
			internal long _value;
		}

		// Token: 0x02000261 RID: 609
		[StructLayout(LayoutKind.Explicit)]
		private struct SqlMoneyCaster
		{
			// Token: 0x040013BB RID: 5051
			[FieldOffset(0)]
			internal SqlMoney Real;

			// Token: 0x040013BC RID: 5052
			[FieldOffset(0)]
			internal SqlTypeWorkarounds.SqlMoneyLookalike Fake;
		}

		// Token: 0x02000262 RID: 610
		private struct SqlDecimalLookalike
		{
			// Token: 0x040013BD RID: 5053
			internal byte _bStatus;

			// Token: 0x040013BE RID: 5054
			internal byte _bLen;

			// Token: 0x040013BF RID: 5055
			internal byte _bPrec;

			// Token: 0x040013C0 RID: 5056
			internal byte _bScale;

			// Token: 0x040013C1 RID: 5057
			internal uint _data1;

			// Token: 0x040013C2 RID: 5058
			internal uint _data2;

			// Token: 0x040013C3 RID: 5059
			internal uint _data3;

			// Token: 0x040013C4 RID: 5060
			internal uint _data4;
		}

		// Token: 0x02000263 RID: 611
		[StructLayout(LayoutKind.Explicit)]
		private struct SqlDecimalCaster
		{
			// Token: 0x040013C5 RID: 5061
			[FieldOffset(0)]
			internal SqlDecimal Real;

			// Token: 0x040013C6 RID: 5062
			[FieldOffset(0)]
			internal SqlTypeWorkarounds.SqlDecimalLookalike Fake;
		}

		// Token: 0x02000264 RID: 612
		private struct SqlBinaryLookalike
		{
			// Token: 0x040013C7 RID: 5063
			internal byte[] _value;
		}

		// Token: 0x02000265 RID: 613
		[StructLayout(LayoutKind.Explicit)]
		private struct SqlBinaryCaster
		{
			// Token: 0x040013C8 RID: 5064
			[FieldOffset(0)]
			internal SqlBinary Real;

			// Token: 0x040013C9 RID: 5065
			[FieldOffset(0)]
			internal SqlTypeWorkarounds.SqlBinaryLookalike Fake;
		}

		// Token: 0x02000266 RID: 614
		private struct SqlGuidLookalike
		{
			// Token: 0x040013CA RID: 5066
			internal byte[] _value;
		}

		// Token: 0x02000267 RID: 615
		[StructLayout(LayoutKind.Explicit)]
		private struct SqlGuidCaster
		{
			// Token: 0x040013CB RID: 5067
			[FieldOffset(0)]
			internal SqlGuid Real;

			// Token: 0x040013CC RID: 5068
			[FieldOffset(0)]
			internal SqlTypeWorkarounds.SqlGuidLookalike Fake;
		}
	}
}
