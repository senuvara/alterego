﻿using System;
using Unity;

namespace System.Data.SqlTypes
{
	/// <summary>The <see cref="T:System.Data.SqlTypes.TypeSmallIntSchemaImporterExtension" /> class is not intended for use as a stand-alone component, but as a class from which other classes derive standard functionality. </summary>
	// Token: 0x0200034C RID: 844
	public sealed class TypeSmallIntSchemaImporterExtension : SqlTypesSchemaImporterExtensionHelper
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Data.SqlTypes.TypeSmallIntSchemaImporterExtension" /> class.</summary>
		// Token: 0x0600296C RID: 10604 RVA: 0x00010458 File Offset: 0x0000E658
		public TypeSmallIntSchemaImporterExtension()
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
