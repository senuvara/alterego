﻿using System;
using Unity;

namespace System.Data.SqlTypes
{
	/// <summary>The <see cref="T:System.Data.SqlTypes.TypeNumericSchemaImporterExtension" /> class is not intended for use as a stand-alone component, but as a class from which other classes derive standard functionality. </summary>
	// Token: 0x02000348 RID: 840
	public sealed class TypeNumericSchemaImporterExtension : SqlTypesSchemaImporterExtensionHelper
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Data.SqlTypes.TypeNumericSchemaImporterExtension" /> class.</summary>
		// Token: 0x06002968 RID: 10600 RVA: 0x00010458 File Offset: 0x0000E658
		public TypeNumericSchemaImporterExtension()
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
