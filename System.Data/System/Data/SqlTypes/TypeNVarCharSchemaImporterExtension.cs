﻿using System;
using Unity;

namespace System.Data.SqlTypes
{
	/// <summary>The <see cref="T:System.Data.SqlTypes.TypeNVarCharSchemaImporterExtension" /> class is not intended for use as a stand-alone component, but as a class from which other classes derive standard functionality. </summary>
	// Token: 0x02000349 RID: 841
	public sealed class TypeNVarCharSchemaImporterExtension : SqlTypesSchemaImporterExtensionHelper
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Data.SqlTypes.TypeNVarCharSchemaImporterExtension" /> class.</summary>
		// Token: 0x06002969 RID: 10601 RVA: 0x00010458 File Offset: 0x0000E658
		public TypeNVarCharSchemaImporterExtension()
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
