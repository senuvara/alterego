﻿using System;
using Unity;

namespace System.Data.SqlTypes
{
	/// <summary>The <see cref="T:System.Data.SqlTypes.TypeBigIntSchemaImporterExtension" /> class is not intended for use as a stand-alone component, but as a class from which other classes derive standard functionality. </summary>
	// Token: 0x0200033D RID: 829
	public sealed class TypeBigIntSchemaImporterExtension : SqlTypesSchemaImporterExtensionHelper
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Data.SqlTypes.TypeBigIntSchemaImporterExtension" /> class.</summary>
		// Token: 0x0600295D RID: 10589 RVA: 0x00010458 File Offset: 0x0000E658
		public TypeBigIntSchemaImporterExtension()
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
