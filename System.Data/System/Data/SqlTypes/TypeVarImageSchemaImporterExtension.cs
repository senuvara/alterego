﻿using System;
using Unity;

namespace System.Data.SqlTypes
{
	/// <summary>The <see cref="T:System.Data.SqlTypes.TypeVarImageSchemaImporterExtension" /> class is not intended for use as a stand-alone component, but as a class from which other classes derive standard functionality. </summary>
	// Token: 0x02000353 RID: 851
	public sealed class TypeVarImageSchemaImporterExtension : SqlTypesSchemaImporterExtensionHelper
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Data.SqlTypes.TypeVarImageSchemaImporterExtension" /> class.</summary>
		// Token: 0x06002973 RID: 10611 RVA: 0x00010458 File Offset: 0x0000E658
		public TypeVarImageSchemaImporterExtension()
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
