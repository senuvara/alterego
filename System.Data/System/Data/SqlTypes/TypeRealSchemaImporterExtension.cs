﻿using System;
using Unity;

namespace System.Data.SqlTypes
{
	/// <summary>The <see cref="T:System.Data.SqlTypes.TypeRealSchemaImporterExtension" /> class is not intended for use as a stand-alone component, but as a class from which other classes derive standard functionality. </summary>
	// Token: 0x0200034A RID: 842
	public sealed class TypeRealSchemaImporterExtension : SqlTypesSchemaImporterExtensionHelper
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Data.SqlTypes.TypeRealSchemaImporterExtension" /> class.</summary>
		// Token: 0x0600296A RID: 10602 RVA: 0x00010458 File Offset: 0x0000E658
		public TypeRealSchemaImporterExtension()
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
