﻿using System;

namespace System.Data.SqlTypes
{
	// Token: 0x02000240 RID: 576
	internal static class SQLResource
	{
		// Token: 0x1700048D RID: 1165
		// (get) Token: 0x06001923 RID: 6435 RVA: 0x00081801 File Offset: 0x0007FA01
		internal static string NullString
		{
			get
			{
				return "Null";
			}
		}

		// Token: 0x1700048E RID: 1166
		// (get) Token: 0x06001924 RID: 6436 RVA: 0x00081808 File Offset: 0x0007FA08
		internal static string MessageString
		{
			get
			{
				return "Message";
			}
		}

		// Token: 0x1700048F RID: 1167
		// (get) Token: 0x06001925 RID: 6437 RVA: 0x0008180F File Offset: 0x0007FA0F
		internal static string ArithOverflowMessage
		{
			get
			{
				return "Arithmetic Overflow.";
			}
		}

		// Token: 0x17000490 RID: 1168
		// (get) Token: 0x06001926 RID: 6438 RVA: 0x00081816 File Offset: 0x0007FA16
		internal static string DivideByZeroMessage
		{
			get
			{
				return "Divide by zero error encountered.";
			}
		}

		// Token: 0x17000491 RID: 1169
		// (get) Token: 0x06001927 RID: 6439 RVA: 0x0008181D File Offset: 0x0007FA1D
		internal static string NullValueMessage
		{
			get
			{
				return "Data is Null. This method or property cannot be called on Null values.";
			}
		}

		// Token: 0x17000492 RID: 1170
		// (get) Token: 0x06001928 RID: 6440 RVA: 0x00081824 File Offset: 0x0007FA24
		internal static string TruncationMessage
		{
			get
			{
				return "Numeric arithmetic causes truncation.";
			}
		}

		// Token: 0x17000493 RID: 1171
		// (get) Token: 0x06001929 RID: 6441 RVA: 0x0008182B File Offset: 0x0007FA2B
		internal static string DateTimeOverflowMessage
		{
			get
			{
				return "SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.";
			}
		}

		// Token: 0x17000494 RID: 1172
		// (get) Token: 0x0600192A RID: 6442 RVA: 0x00081832 File Offset: 0x0007FA32
		internal static string ConcatDiffCollationMessage
		{
			get
			{
				return "Two strings to be concatenated have different collation.";
			}
		}

		// Token: 0x17000495 RID: 1173
		// (get) Token: 0x0600192B RID: 6443 RVA: 0x00081839 File Offset: 0x0007FA39
		internal static string CompareDiffCollationMessage
		{
			get
			{
				return "Two strings to be compared have different collation.";
			}
		}

		// Token: 0x17000496 RID: 1174
		// (get) Token: 0x0600192C RID: 6444 RVA: 0x00081840 File Offset: 0x0007FA40
		internal static string InvalidFlagMessage
		{
			get
			{
				return "Invalid flag value.";
			}
		}

		// Token: 0x17000497 RID: 1175
		// (get) Token: 0x0600192D RID: 6445 RVA: 0x00081847 File Offset: 0x0007FA47
		internal static string NumeToDecOverflowMessage
		{
			get
			{
				return "Conversion from SqlDecimal to Decimal overflows.";
			}
		}

		// Token: 0x17000498 RID: 1176
		// (get) Token: 0x0600192E RID: 6446 RVA: 0x0008184E File Offset: 0x0007FA4E
		internal static string ConversionOverflowMessage
		{
			get
			{
				return "Conversion overflows.";
			}
		}

		// Token: 0x17000499 RID: 1177
		// (get) Token: 0x0600192F RID: 6447 RVA: 0x00081855 File Offset: 0x0007FA55
		internal static string InvalidDateTimeMessage
		{
			get
			{
				return "Invalid SqlDateTime.";
			}
		}

		// Token: 0x1700049A RID: 1178
		// (get) Token: 0x06001930 RID: 6448 RVA: 0x0008185C File Offset: 0x0007FA5C
		internal static string TimeZoneSpecifiedMessage
		{
			get
			{
				return "A time zone was specified. SqlDateTime does not support time zones.";
			}
		}

		// Token: 0x1700049B RID: 1179
		// (get) Token: 0x06001931 RID: 6449 RVA: 0x00081863 File Offset: 0x0007FA63
		internal static string InvalidArraySizeMessage
		{
			get
			{
				return "Invalid array size.";
			}
		}

		// Token: 0x1700049C RID: 1180
		// (get) Token: 0x06001932 RID: 6450 RVA: 0x0008186A File Offset: 0x0007FA6A
		internal static string InvalidPrecScaleMessage
		{
			get
			{
				return "Invalid numeric precision/scale.";
			}
		}

		// Token: 0x1700049D RID: 1181
		// (get) Token: 0x06001933 RID: 6451 RVA: 0x00081871 File Offset: 0x0007FA71
		internal static string FormatMessage
		{
			get
			{
				return "The input wasn't in a correct format.";
			}
		}

		// Token: 0x1700049E RID: 1182
		// (get) Token: 0x06001934 RID: 6452 RVA: 0x00081878 File Offset: 0x0007FA78
		internal static string NotFilledMessage
		{
			get
			{
				return "SQL Type has not been loaded with data.";
			}
		}

		// Token: 0x1700049F RID: 1183
		// (get) Token: 0x06001935 RID: 6453 RVA: 0x0008187F File Offset: 0x0007FA7F
		internal static string AlreadyFilledMessage
		{
			get
			{
				return "SQL Type has already been loaded with data.";
			}
		}

		// Token: 0x170004A0 RID: 1184
		// (get) Token: 0x06001936 RID: 6454 RVA: 0x00081886 File Offset: 0x0007FA86
		internal static string ClosedXmlReaderMessage
		{
			get
			{
				return "Invalid attempt to access a closed XmlReader.";
			}
		}

		// Token: 0x06001937 RID: 6455 RVA: 0x0008188D File Offset: 0x0007FA8D
		internal static string InvalidOpStreamClosed(string method)
		{
			return SR.Format("Invalid attempt to call {0} when the stream is closed.", method);
		}

		// Token: 0x06001938 RID: 6456 RVA: 0x0008189A File Offset: 0x0007FA9A
		internal static string InvalidOpStreamNonWritable(string method)
		{
			return SR.Format("Invalid attempt to call {0} when the stream non-writable.", method);
		}

		// Token: 0x06001939 RID: 6457 RVA: 0x000818A7 File Offset: 0x0007FAA7
		internal static string InvalidOpStreamNonReadable(string method)
		{
			return SR.Format("Invalid attempt to call {0} when the stream non-readable.", method);
		}

		// Token: 0x0600193A RID: 6458 RVA: 0x000818B4 File Offset: 0x0007FAB4
		internal static string InvalidOpStreamNonSeekable(string method)
		{
			return SR.Format("Invalid attempt to call {0} when the stream is non-seekable.", method);
		}
	}
}
