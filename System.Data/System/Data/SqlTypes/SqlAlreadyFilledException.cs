﻿using System;

namespace System.Data.SqlTypes
{
	/// <summary>The <see cref="T:System.Data.SqlTypes.SqlAlreadyFilledException" /> class is not intended for use as a stand-alone component, but as a class from which other classes derive standard functionality. </summary>
	// Token: 0x0200025B RID: 603
	[Serializable]
	public sealed class SqlAlreadyFilledException : SqlTypeException
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Data.SqlTypes.SqlAlreadyFilledException" /> class.</summary>
		// Token: 0x06001CE2 RID: 7394 RVA: 0x0008C139 File Offset: 0x0008A339
		public SqlAlreadyFilledException() : this(SQLResource.AlreadyFilledMessage, null)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Data.SqlTypes.SqlAlreadyFilledException" /> class.</summary>
		/// <param name="message">The string to display when the exception is thrown.</param>
		// Token: 0x06001CE3 RID: 7395 RVA: 0x0008C147 File Offset: 0x0008A347
		public SqlAlreadyFilledException(string message) : this(message, null)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Data.SqlTypes.SqlAlreadyFilledException" /> class.</summary>
		/// <param name="message">The string to display when the exception is thrown.</param>
		/// <param name="e">A reference to an inner exception.</param>
		// Token: 0x06001CE4 RID: 7396 RVA: 0x0008C093 File Offset: 0x0008A293
		public SqlAlreadyFilledException(string message, Exception e) : base(message, e)
		{
			base.HResult = -2146232015;
		}
	}
}
