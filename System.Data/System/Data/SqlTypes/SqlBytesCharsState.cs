﻿using System;

namespace System.Data.SqlTypes
{
	// Token: 0x02000245 RID: 581
	internal enum SqlBytesCharsState
	{
		// Token: 0x040012E0 RID: 4832
		Null,
		// Token: 0x040012E1 RID: 4833
		Buffer,
		// Token: 0x040012E2 RID: 4834
		Stream = 3
	}
}
