﻿using System;
using Unity;

namespace System.Data.SqlTypes
{
	/// <summary>The <see cref="T:System.Data.SqlTypes.TypeTinyIntSchemaImporterExtension" /> class is not intended for use as a stand-alone component, but as a class from which other classes derive standard functionality. </summary>
	// Token: 0x0200034F RID: 847
	public sealed class TypeTinyIntSchemaImporterExtension : SqlTypesSchemaImporterExtensionHelper
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Data.SqlTypes.TypeTinyIntSchemaImporterExtension" /> class.</summary>
		// Token: 0x0600296F RID: 10607 RVA: 0x00010458 File Offset: 0x0000E658
		public TypeTinyIntSchemaImporterExtension()
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
