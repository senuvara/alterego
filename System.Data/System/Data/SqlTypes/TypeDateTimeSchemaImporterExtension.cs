﻿using System;
using Unity;

namespace System.Data.SqlTypes
{
	/// <summary>The <see cref="T:System.Data.SqlTypes.TypeDateTimeSchemaImporterExtension" /> class is not intended for use as a stand-alone component, but as a class from which other classes derive standard functionality. </summary>
	// Token: 0x02000341 RID: 833
	public sealed class TypeDateTimeSchemaImporterExtension : SqlTypesSchemaImporterExtensionHelper
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Data.SqlTypes.TypeDateTimeSchemaImporterExtension" /> class.</summary>
		// Token: 0x06002961 RID: 10593 RVA: 0x00010458 File Offset: 0x0000E658
		public TypeDateTimeSchemaImporterExtension()
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
