﻿using System;
using System.Data.Common;
using System.IO;
using System.Runtime.CompilerServices;

namespace System.Data.SqlTypes
{
	// Token: 0x02000249 RID: 585
	internal sealed class StreamOnSqlChars : SqlStreamChars
	{
		// Token: 0x06001A31 RID: 6705 RVA: 0x000840A8 File Offset: 0x000822A8
		internal StreamOnSqlChars(SqlChars s)
		{
			this._sqlchars = s;
			this._lPosition = 0L;
		}

		// Token: 0x170004C4 RID: 1220
		// (get) Token: 0x06001A32 RID: 6706 RVA: 0x000840BF File Offset: 0x000822BF
		public override bool IsNull
		{
			get
			{
				return this._sqlchars == null || this._sqlchars.IsNull;
			}
		}

		// Token: 0x170004C5 RID: 1221
		// (get) Token: 0x06001A33 RID: 6707 RVA: 0x000840D6 File Offset: 0x000822D6
		public override long Length
		{
			get
			{
				this.CheckIfStreamClosed("get_Length");
				return this._sqlchars.Length;
			}
		}

		// Token: 0x170004C6 RID: 1222
		// (get) Token: 0x06001A34 RID: 6708 RVA: 0x000840EE File Offset: 0x000822EE
		// (set) Token: 0x06001A35 RID: 6709 RVA: 0x00084101 File Offset: 0x00082301
		public override long Position
		{
			get
			{
				this.CheckIfStreamClosed("get_Position");
				return this._lPosition;
			}
			set
			{
				this.CheckIfStreamClosed("set_Position");
				if (value < 0L || value > this._sqlchars.Length)
				{
					throw new ArgumentOutOfRangeException("value");
				}
				this._lPosition = value;
			}
		}

		// Token: 0x06001A36 RID: 6710 RVA: 0x00084134 File Offset: 0x00082334
		public override long Seek(long offset, SeekOrigin origin)
		{
			this.CheckIfStreamClosed("Seek");
			switch (origin)
			{
			case SeekOrigin.Begin:
				if (offset < 0L || offset > this._sqlchars.Length)
				{
					throw ADP.ArgumentOutOfRange("offset");
				}
				this._lPosition = offset;
				break;
			case SeekOrigin.Current:
			{
				long num = this._lPosition + offset;
				if (num < 0L || num > this._sqlchars.Length)
				{
					throw ADP.ArgumentOutOfRange("offset");
				}
				this._lPosition = num;
				break;
			}
			case SeekOrigin.End:
			{
				long num = this._sqlchars.Length + offset;
				if (num < 0L || num > this._sqlchars.Length)
				{
					throw ADP.ArgumentOutOfRange("offset");
				}
				this._lPosition = num;
				break;
			}
			default:
				throw ADP.ArgumentOutOfRange("offset");
			}
			return this._lPosition;
		}

		// Token: 0x06001A37 RID: 6711 RVA: 0x00084204 File Offset: 0x00082404
		public override int Read(char[] buffer, int offset, int count)
		{
			this.CheckIfStreamClosed("Read");
			if (buffer == null)
			{
				throw new ArgumentNullException("buffer");
			}
			if (offset < 0 || offset > buffer.Length)
			{
				throw new ArgumentOutOfRangeException("offset");
			}
			if (count < 0 || count > buffer.Length - offset)
			{
				throw new ArgumentOutOfRangeException("count");
			}
			int num = (int)this._sqlchars.Read(this._lPosition, buffer, offset, count);
			this._lPosition += (long)num;
			return num;
		}

		// Token: 0x06001A38 RID: 6712 RVA: 0x0008427C File Offset: 0x0008247C
		public override void Write(char[] buffer, int offset, int count)
		{
			this.CheckIfStreamClosed("Write");
			if (buffer == null)
			{
				throw new ArgumentNullException("buffer");
			}
			if (offset < 0 || offset > buffer.Length)
			{
				throw new ArgumentOutOfRangeException("offset");
			}
			if (count < 0 || count > buffer.Length - offset)
			{
				throw new ArgumentOutOfRangeException("count");
			}
			this._sqlchars.Write(this._lPosition, buffer, offset, count);
			this._lPosition += (long)count;
		}

		// Token: 0x06001A39 RID: 6713 RVA: 0x000842F1 File Offset: 0x000824F1
		public override void SetLength(long value)
		{
			this.CheckIfStreamClosed("SetLength");
			this._sqlchars.SetLength(value);
			if (this._lPosition > value)
			{
				this._lPosition = value;
			}
		}

		// Token: 0x06001A3A RID: 6714 RVA: 0x0008431A File Offset: 0x0008251A
		protected override void Dispose(bool disposing)
		{
			this._sqlchars = null;
		}

		// Token: 0x06001A3B RID: 6715 RVA: 0x00084323 File Offset: 0x00082523
		private bool FClosed()
		{
			return this._sqlchars == null;
		}

		// Token: 0x06001A3C RID: 6716 RVA: 0x0008432E File Offset: 0x0008252E
		private void CheckIfStreamClosed([CallerMemberName] string methodname = "")
		{
			if (this.FClosed())
			{
				throw ADP.StreamClosed(methodname);
			}
		}

		// Token: 0x040012F3 RID: 4851
		private SqlChars _sqlchars;

		// Token: 0x040012F4 RID: 4852
		private long _lPosition;
	}
}
