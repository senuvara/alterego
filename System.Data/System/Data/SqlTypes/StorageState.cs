﻿using System;

namespace System.Data.SqlTypes
{
	/// <summary>The <see cref="T:System.Data.SqlTypes.StorageState" /> enumeration is not intended for use as a stand-alone component, but as an enumeration from which other classes derive standard functionality.</summary>
	// Token: 0x02000256 RID: 598
	public enum StorageState
	{
		/// <summary>Buffer size.</summary>
		// Token: 0x040013A7 RID: 5031
		Buffer,
		/// <summary>Stream.</summary>
		// Token: 0x040013A8 RID: 5032
		Stream,
		/// <summary>Unmanaged buffer.</summary>
		// Token: 0x040013A9 RID: 5033
		UnmanagedBuffer
	}
}
