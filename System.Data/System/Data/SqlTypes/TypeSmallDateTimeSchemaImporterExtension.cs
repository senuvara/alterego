﻿using System;
using Unity;

namespace System.Data.SqlTypes
{
	/// <summary>The TypeSmallDateTimeSchemaImporterExtension class is not intended for use as a stand-alone component, but as a class from which other classes derive standard functionality. </summary>
	// Token: 0x0200034B RID: 843
	public sealed class TypeSmallDateTimeSchemaImporterExtension : SqlTypesSchemaImporterExtensionHelper
	{
		/// <summary>Initializes a new instance of the TypeSmallDateTimeSchemaImporterExtension class.</summary>
		// Token: 0x0600296B RID: 10603 RVA: 0x00010458 File Offset: 0x0000E658
		public TypeSmallDateTimeSchemaImporterExtension()
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
