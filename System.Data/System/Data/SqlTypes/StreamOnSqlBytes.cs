﻿using System;
using System.Data.Common;
using System.IO;
using System.Runtime.CompilerServices;

namespace System.Data.SqlTypes
{
	// Token: 0x02000247 RID: 583
	internal sealed class StreamOnSqlBytes : Stream
	{
		// Token: 0x06001A01 RID: 6657 RVA: 0x000835C3 File Offset: 0x000817C3
		internal StreamOnSqlBytes(SqlBytes sb)
		{
			this._sb = sb;
			this._lPosition = 0L;
		}

		// Token: 0x170004B6 RID: 1206
		// (get) Token: 0x06001A02 RID: 6658 RVA: 0x000835DA File Offset: 0x000817DA
		public override bool CanRead
		{
			get
			{
				return this._sb != null && !this._sb.IsNull;
			}
		}

		// Token: 0x170004B7 RID: 1207
		// (get) Token: 0x06001A03 RID: 6659 RVA: 0x000835F4 File Offset: 0x000817F4
		public override bool CanSeek
		{
			get
			{
				return this._sb != null;
			}
		}

		// Token: 0x170004B8 RID: 1208
		// (get) Token: 0x06001A04 RID: 6660 RVA: 0x000835FF File Offset: 0x000817FF
		public override bool CanWrite
		{
			get
			{
				return this._sb != null && (!this._sb.IsNull || this._sb._rgbBuf != null);
			}
		}

		// Token: 0x170004B9 RID: 1209
		// (get) Token: 0x06001A05 RID: 6661 RVA: 0x00083628 File Offset: 0x00081828
		public override long Length
		{
			get
			{
				this.CheckIfStreamClosed("get_Length");
				return this._sb.Length;
			}
		}

		// Token: 0x170004BA RID: 1210
		// (get) Token: 0x06001A06 RID: 6662 RVA: 0x00083640 File Offset: 0x00081840
		// (set) Token: 0x06001A07 RID: 6663 RVA: 0x00083653 File Offset: 0x00081853
		public override long Position
		{
			get
			{
				this.CheckIfStreamClosed("get_Position");
				return this._lPosition;
			}
			set
			{
				this.CheckIfStreamClosed("set_Position");
				if (value < 0L || value > this._sb.Length)
				{
					throw new ArgumentOutOfRangeException("value");
				}
				this._lPosition = value;
			}
		}

		// Token: 0x06001A08 RID: 6664 RVA: 0x00083688 File Offset: 0x00081888
		public override long Seek(long offset, SeekOrigin origin)
		{
			this.CheckIfStreamClosed("Seek");
			switch (origin)
			{
			case SeekOrigin.Begin:
				if (offset < 0L || offset > this._sb.Length)
				{
					throw new ArgumentOutOfRangeException("offset");
				}
				this._lPosition = offset;
				break;
			case SeekOrigin.Current:
			{
				long num = this._lPosition + offset;
				if (num < 0L || num > this._sb.Length)
				{
					throw new ArgumentOutOfRangeException("offset");
				}
				this._lPosition = num;
				break;
			}
			case SeekOrigin.End:
			{
				long num = this._sb.Length + offset;
				if (num < 0L || num > this._sb.Length)
				{
					throw new ArgumentOutOfRangeException("offset");
				}
				this._lPosition = num;
				break;
			}
			default:
				throw ADP.InvalidSeekOrigin("offset");
			}
			return this._lPosition;
		}

		// Token: 0x06001A09 RID: 6665 RVA: 0x00083758 File Offset: 0x00081958
		public override int Read(byte[] buffer, int offset, int count)
		{
			this.CheckIfStreamClosed("Read");
			if (buffer == null)
			{
				throw new ArgumentNullException("buffer");
			}
			if (offset < 0 || offset > buffer.Length)
			{
				throw new ArgumentOutOfRangeException("offset");
			}
			if (count < 0 || count > buffer.Length - offset)
			{
				throw new ArgumentOutOfRangeException("count");
			}
			int num = (int)this._sb.Read(this._lPosition, buffer, offset, count);
			this._lPosition += (long)num;
			return num;
		}

		// Token: 0x06001A0A RID: 6666 RVA: 0x000837D0 File Offset: 0x000819D0
		public override void Write(byte[] buffer, int offset, int count)
		{
			this.CheckIfStreamClosed("Write");
			if (buffer == null)
			{
				throw new ArgumentNullException("buffer");
			}
			if (offset < 0 || offset > buffer.Length)
			{
				throw new ArgumentOutOfRangeException("offset");
			}
			if (count < 0 || count > buffer.Length - offset)
			{
				throw new ArgumentOutOfRangeException("count");
			}
			this._sb.Write(this._lPosition, buffer, offset, count);
			this._lPosition += (long)count;
		}

		// Token: 0x06001A0B RID: 6667 RVA: 0x00083848 File Offset: 0x00081A48
		public override int ReadByte()
		{
			this.CheckIfStreamClosed("ReadByte");
			if (this._lPosition >= this._sb.Length)
			{
				return -1;
			}
			int result = (int)this._sb[this._lPosition];
			this._lPosition += 1L;
			return result;
		}

		// Token: 0x06001A0C RID: 6668 RVA: 0x00083895 File Offset: 0x00081A95
		public override void WriteByte(byte value)
		{
			this.CheckIfStreamClosed("WriteByte");
			this._sb[this._lPosition] = value;
			this._lPosition += 1L;
		}

		// Token: 0x06001A0D RID: 6669 RVA: 0x000838C3 File Offset: 0x00081AC3
		public override void SetLength(long value)
		{
			this.CheckIfStreamClosed("SetLength");
			this._sb.SetLength(value);
			if (this._lPosition > value)
			{
				this._lPosition = value;
			}
		}

		// Token: 0x06001A0E RID: 6670 RVA: 0x000838EC File Offset: 0x00081AEC
		public override void Flush()
		{
			if (this._sb.FStream())
			{
				this._sb._stream.Flush();
			}
		}

		// Token: 0x06001A0F RID: 6671 RVA: 0x0008390C File Offset: 0x00081B0C
		protected override void Dispose(bool disposing)
		{
			try
			{
				this._sb = null;
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		// Token: 0x06001A10 RID: 6672 RVA: 0x0008393C File Offset: 0x00081B3C
		private bool FClosed()
		{
			return this._sb == null;
		}

		// Token: 0x06001A11 RID: 6673 RVA: 0x00083947 File Offset: 0x00081B47
		private void CheckIfStreamClosed([CallerMemberName] string methodname = "")
		{
			if (this.FClosed())
			{
				throw ADP.StreamClosed(methodname);
			}
		}

		// Token: 0x040012EA RID: 4842
		private SqlBytes _sb;

		// Token: 0x040012EB RID: 4843
		private long _lPosition;
	}
}
