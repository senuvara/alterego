﻿using System;
using Unity;

namespace System.Data.SqlTypes
{
	/// <summary>The <see cref="T:System.Data.SqlTypes.TypeIntSchemaImporterExtension" /> class is not intended for use as a stand-alone component, but as a class from which other classes derive standard functionality. </summary>
	// Token: 0x02000344 RID: 836
	public sealed class TypeIntSchemaImporterExtension : SqlTypesSchemaImporterExtensionHelper
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Data.SqlTypes.TypeIntSchemaImporterExtension" /> class.</summary>
		// Token: 0x06002964 RID: 10596 RVA: 0x00010458 File Offset: 0x0000E658
		public TypeIntSchemaImporterExtension()
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
