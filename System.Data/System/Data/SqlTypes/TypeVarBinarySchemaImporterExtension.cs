﻿using System;
using Unity;

namespace System.Data.SqlTypes
{
	/// <summary>The <see cref="T:System.Data.SqlTypes.TypeVarBinarySchemaImporterExtension" /> class is not intended for use as a stand-alone component, but as a class from which other classes derive standard functionality. </summary>
	// Token: 0x02000351 RID: 849
	public sealed class TypeVarBinarySchemaImporterExtension : SqlTypesSchemaImporterExtensionHelper
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Data.SqlTypes.TypeVarBinarySchemaImporterExtension" /> class.</summary>
		// Token: 0x06002971 RID: 10609 RVA: 0x00010458 File Offset: 0x0000E658
		public TypeVarBinarySchemaImporterExtension()
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
