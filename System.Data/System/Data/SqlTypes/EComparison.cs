﻿using System;

namespace System.Data.SqlTypes
{
	// Token: 0x02000255 RID: 597
	internal enum EComparison
	{
		// Token: 0x040013A0 RID: 5024
		LT,
		// Token: 0x040013A1 RID: 5025
		LE,
		// Token: 0x040013A2 RID: 5026
		EQ,
		// Token: 0x040013A3 RID: 5027
		GE,
		// Token: 0x040013A4 RID: 5028
		GT,
		// Token: 0x040013A5 RID: 5029
		NE
	}
}
