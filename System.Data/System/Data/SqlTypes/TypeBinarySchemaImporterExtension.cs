﻿using System;
using Unity;

namespace System.Data.SqlTypes
{
	/// <summary>The <see cref="T:System.Data.SqlTypes.TypeBinarySchemaImporterExtension" /> class is not intended for use as a stand-alone component, but as a class from which other classes derive standard functionality. </summary>
	// Token: 0x0200033E RID: 830
	public sealed class TypeBinarySchemaImporterExtension : SqlTypesSchemaImporterExtensionHelper
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Data.SqlTypes.TypeBinarySchemaImporterExtension" /> class.</summary>
		// Token: 0x0600295E RID: 10590 RVA: 0x00010458 File Offset: 0x0000E658
		public TypeBinarySchemaImporterExtension()
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
