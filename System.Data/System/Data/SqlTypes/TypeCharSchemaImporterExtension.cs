﻿using System;
using Unity;

namespace System.Data.SqlTypes
{
	/// <summary>The <see cref="T:System.Data.SqlTypes.TypeCharSchemaImporterExtension" /> class is not intended for use as a stand-alone component, but as a class from which other classes derive standard functionality. </summary>
	// Token: 0x02000340 RID: 832
	public sealed class TypeCharSchemaImporterExtension : SqlTypesSchemaImporterExtensionHelper
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Data.SqlTypes.TypeCharSchemaImporterExtension" /> class.</summary>
		// Token: 0x06002960 RID: 10592 RVA: 0x00010458 File Offset: 0x0000E658
		public TypeCharSchemaImporterExtension()
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
