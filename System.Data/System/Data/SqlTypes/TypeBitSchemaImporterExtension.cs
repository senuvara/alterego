﻿using System;
using Unity;

namespace System.Data.SqlTypes
{
	/// <summary>The <see cref="T:System.Data.SqlTypes.TypeBitSchemaImporterExtension" /> class is not intended for use as a stand-alone component, but as a class from which other classes derive standard functionality. </summary>
	// Token: 0x0200033F RID: 831
	public sealed class TypeBitSchemaImporterExtension : SqlTypesSchemaImporterExtensionHelper
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Data.SqlTypes.TypeBitSchemaImporterExtension" /> class. </summary>
		// Token: 0x0600295F RID: 10591 RVA: 0x00010458 File Offset: 0x0000E658
		public TypeBitSchemaImporterExtension()
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
