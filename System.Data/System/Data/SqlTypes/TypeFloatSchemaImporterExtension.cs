﻿using System;
using Unity;

namespace System.Data.SqlTypes
{
	/// <summary>The <see cref="T:System.Data.SqlTypes.TypeFloatSchemaImporterExtension" /> class is not intended for use as a stand-alone component, but as a class from which other classes derive standard functionality. </summary>
	// Token: 0x02000343 RID: 835
	public sealed class TypeFloatSchemaImporterExtension : SqlTypesSchemaImporterExtensionHelper
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Data.SqlTypes.TypeFloatSchemaImporterExtension" /> class.</summary>
		// Token: 0x06002963 RID: 10595 RVA: 0x00010458 File Offset: 0x0000E658
		public TypeFloatSchemaImporterExtension()
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
