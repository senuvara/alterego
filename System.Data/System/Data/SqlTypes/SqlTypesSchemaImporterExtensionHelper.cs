﻿using System;
using System.CodeDom;
using System.CodeDom.Compiler;
using System.Xml.Schema;
using System.Xml.Serialization;
using System.Xml.Serialization.Advanced;
using Unity;

namespace System.Data.SqlTypes
{
	/// <summary>The <see cref="T:System.Data.SqlTypes.SqlTypesSchemaImporterExtensionHelper" /> class is not intended for use as a stand-alone component, but as a class from which other classes derive standard functionality.</summary>
	// Token: 0x0200033C RID: 828
	public class SqlTypesSchemaImporterExtensionHelper : SchemaImporterExtension
	{
		/// <summary>The <see cref="T:System.Data.SqlTypes.SqlTypesSchemaImporterExtensionHelper" /> class is not intended for use as a stand-alone component, but as a class from which other classes derive standard functionality.</summary>
		/// <param name="name">The name as a string.</param>
		/// <param name="destinationType">The destination type as a string.</param>
		// Token: 0x06002958 RID: 10584 RVA: 0x00010458 File Offset: 0x0000E658
		public SqlTypesSchemaImporterExtensionHelper(string name, string destinationType)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>The <see cref="T:System.Data.SqlTypes.SqlTypesSchemaImporterExtensionHelper" /> class is not intended for use as a stand-alone component, but as a class from which other classes derive standard functionality.</summary>
		/// <param name="name">The name as a string.</param>
		/// <param name="destinationType">The destination type as a string.</param>
		/// <param name="direct">A Boolean.</param>
		// Token: 0x06002959 RID: 10585 RVA: 0x00010458 File Offset: 0x0000E658
		public SqlTypesSchemaImporterExtensionHelper(string name, string destinationType, bool direct)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>The <see cref="T:System.Data.SqlTypes.SqlTypesSchemaImporterExtensionHelper" /> class is not intended for use as a stand-alone component, but as a class from which other classes derive standard functionality.</summary>
		/// <param name="name">The name as a string.</param>
		/// <param name="targetNamespace">The target namespace.</param>
		/// <param name="references">String array of references.</param>
		/// <param name="namespaceImports">Array of CodeNamespaceImport objects.</param>
		/// <param name="destinationType">The destination type as a string.</param>
		/// <param name="direct">A Boolean for direct.</param>
		// Token: 0x0600295A RID: 10586 RVA: 0x00010458 File Offset: 0x0000E658
		public SqlTypesSchemaImporterExtensionHelper(string name, string targetNamespace, string[] references, CodeNamespaceImport[] namespaceImports, string destinationType, bool direct)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>The <see cref="T:System.Data.SqlTypes.SqlTypesSchemaImporterExtensionHelper" /> class is not intended for use as a stand-alone component, but as a class from which other classes derive standard functionality.</summary>
		/// <param name="name">
		///       <paramref name="name" />
		///     </param>
		/// <param name="xmlNamespace">
		///       <paramref name="xmlNamespace" />
		///     </param>
		/// <param name="context">
		///       <paramref name="context" />
		///     </param>
		/// <param name="schemas">
		///       <paramref name="schemas" />
		///     </param>
		/// <param name="importer">
		///       <paramref name="importer" />
		///     </param>
		/// <param name="compileUnit">
		///       <paramref name="compileUnit" />
		///     </param>
		/// <param name="mainNamespace">
		///       <paramref name="mainNamespace" />
		///     </param>
		/// <param name="options">
		///       <paramref name="options" />
		///     </param>
		/// <param name="codeProvider">
		///       <paramref name="codeProvider" />
		///     </param>
		/// <returns>The <see cref="T:System.Data.SqlTypes.SqlTypesSchemaImporterExtensionHelper" /> class is not intended for use as a stand-alone component, but as a class from which other classes derive standard functionality.</returns>
		// Token: 0x0600295B RID: 10587 RVA: 0x00051759 File Offset: 0x0004F959
		public override string ImportSchemaType(string name, string xmlNamespace, XmlSchemaObject context, XmlSchemas schemas, XmlSchemaImporter importer, CodeCompileUnit compileUnit, CodeNamespace mainNamespace, CodeGenerationOptions options, CodeDomProvider codeProvider)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>The <see cref="T:System.Data.SqlTypes.SqlTypesSchemaImporterExtensionHelper" /> class is not intended for use as a stand-alone component, but as a class from which other classes derive standard functionality.</summary>
		/// <param name="type">
		///       <paramref name="type" />
		///     </param>
		/// <param name="context">
		///       <paramref name="context" />
		///     </param>
		/// <param name="schemas">
		///       <paramref name="schemas" />
		///     </param>
		/// <param name="importer">
		///       <paramref name="importer" />
		///     </param>
		/// <param name="compileUnit">
		///       <paramref name="compileUnit" />
		///     </param>
		/// <param name="mainNamespace">
		///       <paramref name="mainNamespace" />
		///     </param>
		/// <param name="options">
		///       <paramref name="options" />
		///     </param>
		/// <param name="codeProvider">
		///       <paramref name="codeProvider" />
		///     </param>
		/// <returns>The <see cref="T:System.Data.SqlTypes.SqlTypesSchemaImporterExtensionHelper" /> class is not intended for use as a stand-alone component, but as a class from which other classes derive standard functionality.</returns>
		// Token: 0x0600295C RID: 10588 RVA: 0x00051759 File Offset: 0x0004F959
		public override string ImportSchemaType(XmlSchemaType type, XmlSchemaObject context, XmlSchemas schemas, XmlSchemaImporter importer, CodeCompileUnit compileUnit, CodeNamespace mainNamespace, CodeGenerationOptions options, CodeDomProvider codeProvider)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>The <see cref="T:System.Data.SqlTypes.SqlTypesSchemaImporterExtensionHelper" /> class is not intended for use as a stand-alone component, but as a class from which other classes derive standard functionality. </summary>
		// Token: 0x0400191A RID: 6426
		protected static readonly string SqlTypesNamespace;
	}
}
