﻿using System;
using Unity;

namespace System.Data.SqlTypes
{
	/// <summary>The <see cref="T:System.Data.SqlTypes.TypeNTextSchemaImporterExtension" /> class is not intended for use as a stand-alone component, but as a class from which other classes derive standard functionality. </summary>
	// Token: 0x02000347 RID: 839
	public sealed class TypeNTextSchemaImporterExtension : SqlTypesSchemaImporterExtensionHelper
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Data.SqlTypes.TypeNTextSchemaImporterExtension" /> class.</summary>
		// Token: 0x06002967 RID: 10599 RVA: 0x00010458 File Offset: 0x0000E658
		public TypeNTextSchemaImporterExtension()
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
