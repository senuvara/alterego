﻿using System;
using Unity;

namespace System.Data.SqlTypes
{
	/// <summary>The <see cref="T:System.Data.SqlTypes.TypeVarCharSchemaImporterExtension" /> class is not intended for use as a stand-alone component, but as a class from which other classes derive standard functionality. </summary>
	// Token: 0x02000352 RID: 850
	public sealed class TypeVarCharSchemaImporterExtension : SqlTypesSchemaImporterExtensionHelper
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Data.SqlTypes.TypeVarCharSchemaImporterExtension" /> class.</summary>
		// Token: 0x06002972 RID: 10610 RVA: 0x00010458 File Offset: 0x0000E658
		public TypeVarCharSchemaImporterExtension()
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
