﻿using System;
using Unity;

namespace System.Data.SqlTypes
{
	/// <summary>The <see cref="T:System.Data.SqlTypes.TypeSmallMoneySchemaImporterExtension" /> class is not intended for use as a stand-alone component, but as a class from which other classes derive standard functionality. </summary>
	// Token: 0x0200034D RID: 845
	public sealed class TypeSmallMoneySchemaImporterExtension : SqlTypesSchemaImporterExtensionHelper
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Data.SqlTypes.TypeSmallMoneySchemaImporterExtension" /> class.</summary>
		// Token: 0x0600296D RID: 10605 RVA: 0x00010458 File Offset: 0x0000E658
		public TypeSmallMoneySchemaImporterExtension()
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
