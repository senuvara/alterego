﻿using System;
using System.IO;

namespace System.Data.SqlTypes
{
	// Token: 0x0200025C RID: 604
	internal abstract class SqlStreamChars : INullable, IDisposable
	{
		// Token: 0x170004E6 RID: 1254
		// (get) Token: 0x06001CE5 RID: 7397
		public abstract bool IsNull { get; }

		// Token: 0x170004E7 RID: 1255
		// (get) Token: 0x06001CE6 RID: 7398
		public abstract long Length { get; }

		// Token: 0x170004E8 RID: 1256
		// (get) Token: 0x06001CE7 RID: 7399
		// (set) Token: 0x06001CE8 RID: 7400
		public abstract long Position { get; set; }

		// Token: 0x06001CE9 RID: 7401
		public abstract int Read(char[] buffer, int offset, int count);

		// Token: 0x06001CEA RID: 7402
		public abstract void Write(char[] buffer, int offset, int count);

		// Token: 0x06001CEB RID: 7403
		public abstract long Seek(long offset, SeekOrigin origin);

		// Token: 0x06001CEC RID: 7404
		public abstract void SetLength(long value);

		// Token: 0x06001CED RID: 7405 RVA: 0x0008C151 File Offset: 0x0008A351
		void IDisposable.Dispose()
		{
			this.Dispose(true);
		}

		// Token: 0x06001CEE RID: 7406 RVA: 0x00005E03 File Offset: 0x00004003
		protected virtual void Dispose(bool disposing)
		{
		}

		// Token: 0x06001CEF RID: 7407 RVA: 0x00005C14 File Offset: 0x00003E14
		protected SqlStreamChars()
		{
		}
	}
}
