﻿using System;
using Unity;

namespace System.Data.SqlTypes
{
	/// <summary>The <see cref="T:System.Data.SqlTypes.TypeTextSchemaImporterExtension" /> class is not intended for use as a stand-alone component, but as a class from which other classes derive standard functionality. </summary>
	// Token: 0x0200034E RID: 846
	public sealed class TypeTextSchemaImporterExtension : SqlTypesSchemaImporterExtensionHelper
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Data.SqlTypes.TypeTextSchemaImporterExtension" /> class.</summary>
		// Token: 0x0600296E RID: 10606 RVA: 0x00010458 File Offset: 0x0000E658
		public TypeTextSchemaImporterExtension()
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
