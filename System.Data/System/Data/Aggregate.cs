﻿using System;

namespace System.Data
{
	// Token: 0x020000A5 RID: 165
	internal enum Aggregate
	{
		// Token: 0x040006A8 RID: 1704
		None = -1,
		// Token: 0x040006A9 RID: 1705
		Sum = 30,
		// Token: 0x040006AA RID: 1706
		Avg,
		// Token: 0x040006AB RID: 1707
		Min,
		// Token: 0x040006AC RID: 1708
		Max,
		// Token: 0x040006AD RID: 1709
		Count,
		// Token: 0x040006AE RID: 1710
		StDev,
		// Token: 0x040006AF RID: 1711
		Var = 37
	}
}
