﻿using System;

namespace System.Data
{
	/// <summary>Provides a means of reading one or more forward-only streams of result sets obtained by executing a command at a data source, and is implemented by .NET Framework data providers that access relational databases.</summary>
	// Token: 0x020000C6 RID: 198
	public interface IDataReader : IDisposable, IDataRecord
	{
		/// <summary>Gets a value indicating the depth of nesting for the current row.</summary>
		/// <returns>The level of nesting.</returns>
		// Token: 0x17000201 RID: 513
		// (get) Token: 0x06000B6D RID: 2925
		int Depth { get; }

		/// <summary>Gets a value indicating whether the data reader is closed.</summary>
		/// <returns>
		///     <see langword="true" /> if the data reader is closed; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000202 RID: 514
		// (get) Token: 0x06000B6E RID: 2926
		bool IsClosed { get; }

		/// <summary>Gets the number of rows changed, inserted, or deleted by execution of the SQL statement.</summary>
		/// <returns>The number of rows changed, inserted, or deleted; 0 if no rows were affected or the statement failed; and -1 for SELECT statements.</returns>
		// Token: 0x17000203 RID: 515
		// (get) Token: 0x06000B6F RID: 2927
		int RecordsAffected { get; }

		/// <summary>Closes the <see cref="T:System.Data.IDataReader" /> Object.</summary>
		// Token: 0x06000B70 RID: 2928
		void Close();

		/// <summary>Returns a <see cref="T:System.Data.DataTable" /> that describes the column metadata of the <see cref="T:System.Data.IDataReader" />.</summary>
		/// <returns>A <see cref="T:System.Data.DataTable" /> that describes the column metadata.</returns>
		/// <exception cref="T:System.InvalidOperationException">The <see cref="T:System.Data.IDataReader" /> is closed. </exception>
		// Token: 0x06000B71 RID: 2929
		DataTable GetSchemaTable();

		/// <summary>Advances the data reader to the next result, when reading the results of batch SQL statements.</summary>
		/// <returns>
		///     <see langword="true" /> if there are more rows; otherwise, <see langword="false" />.</returns>
		// Token: 0x06000B72 RID: 2930
		bool NextResult();

		/// <summary>Advances the <see cref="T:System.Data.IDataReader" /> to the next record.</summary>
		/// <returns>
		///     <see langword="true" /> if there are more rows; otherwise, <see langword="false" />.</returns>
		// Token: 0x06000B73 RID: 2931
		bool Read();
	}
}
