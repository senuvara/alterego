﻿using System;

namespace System.Data
{
	// Token: 0x020000AE RID: 174
	internal enum Nodes
	{
		// Token: 0x040006FA RID: 1786
		Noop,
		// Token: 0x040006FB RID: 1787
		Unop,
		// Token: 0x040006FC RID: 1788
		UnopSpec,
		// Token: 0x040006FD RID: 1789
		Binop,
		// Token: 0x040006FE RID: 1790
		BinopSpec,
		// Token: 0x040006FF RID: 1791
		Zop,
		// Token: 0x04000700 RID: 1792
		Call,
		// Token: 0x04000701 RID: 1793
		Const,
		// Token: 0x04000702 RID: 1794
		Name,
		// Token: 0x04000703 RID: 1795
		Paren,
		// Token: 0x04000704 RID: 1796
		Conv
	}
}
