﻿using System;
using System.Runtime.CompilerServices;

namespace System.Data
{
	/// <summary>Provides data for the <see cref="M:System.Data.DataTable.NewRow" /> method.</summary>
	// Token: 0x02000091 RID: 145
	public sealed class DataTableNewRowEventArgs : EventArgs
	{
		/// <summary>Initializes a new instance of <see cref="T:System.Data.DataTableNewRowEventArgs" />.</summary>
		/// <param name="dataRow">The <see cref="T:System.Data.DataRow" /> being added.</param>
		// Token: 0x060008C0 RID: 2240 RVA: 0x00028A3F File Offset: 0x00026C3F
		public DataTableNewRowEventArgs(DataRow dataRow)
		{
			this.Row = dataRow;
		}

		/// <summary>Gets the row that is being added.</summary>
		/// <returns>The <see cref="T:System.Data.DataRow" /> that is being added. </returns>
		// Token: 0x17000188 RID: 392
		// (get) Token: 0x060008C1 RID: 2241 RVA: 0x00028A4E File Offset: 0x00026C4E
		public DataRow Row
		{
			[CompilerGenerated]
			get
			{
				return this.<Row>k__BackingField;
			}
		}

		// Token: 0x04000630 RID: 1584
		[CompilerGenerated]
		private readonly DataRow <Row>k__BackingField;
	}
}
