﻿using System;
using System.Collections.Generic;

namespace System.Data
{
	// Token: 0x020000F1 RID: 241
	internal sealed class Listeners<TElem> where TElem : class
	{
		// Token: 0x06000CC4 RID: 3268 RVA: 0x0003B7ED File Offset: 0x000399ED
		internal Listeners(int ObjectID, Listeners<TElem>.Func<TElem, bool> notifyFilter)
		{
			this._listeners = new List<TElem>();
			this._filter = notifyFilter;
			this._objectID = ObjectID;
			this._listenerReaderCount = 0;
		}

		// Token: 0x1700023A RID: 570
		// (get) Token: 0x06000CC5 RID: 3269 RVA: 0x0003B815 File Offset: 0x00039A15
		internal bool HasListeners
		{
			get
			{
				return 0 < this._listeners.Count;
			}
		}

		// Token: 0x06000CC6 RID: 3270 RVA: 0x0003B825 File Offset: 0x00039A25
		internal void Add(TElem listener)
		{
			this._listeners.Add(listener);
		}

		// Token: 0x06000CC7 RID: 3271 RVA: 0x0003B833 File Offset: 0x00039A33
		internal int IndexOfReference(TElem listener)
		{
			return Index.IndexOfReference<TElem>(this._listeners, listener);
		}

		// Token: 0x06000CC8 RID: 3272 RVA: 0x0003B844 File Offset: 0x00039A44
		internal void Remove(TElem listener)
		{
			int index = this.IndexOfReference(listener);
			this._listeners[index] = default(TElem);
			if (this._listenerReaderCount == 0)
			{
				this._listeners.RemoveAt(index);
				this._listeners.TrimExcess();
			}
		}

		// Token: 0x06000CC9 RID: 3273 RVA: 0x0003B890 File Offset: 0x00039A90
		internal void Notify<T1, T2, T3>(T1 arg1, T2 arg2, T3 arg3, Listeners<TElem>.Action<TElem, T1, T2, T3> action)
		{
			int count = this._listeners.Count;
			if (0 < count)
			{
				int nullIndex = -1;
				this._listenerReaderCount++;
				try
				{
					for (int i = 0; i < count; i++)
					{
						TElem arg4 = this._listeners[i];
						if (this._filter(arg4))
						{
							action(arg4, arg1, arg2, arg3);
						}
						else
						{
							this._listeners[i] = default(TElem);
							nullIndex = i;
						}
					}
				}
				finally
				{
					this._listenerReaderCount--;
				}
				if (this._listenerReaderCount == 0)
				{
					this.RemoveNullListeners(nullIndex);
				}
			}
		}

		// Token: 0x06000CCA RID: 3274 RVA: 0x0003B93C File Offset: 0x00039B3C
		private void RemoveNullListeners(int nullIndex)
		{
			int num = nullIndex;
			while (0 <= num)
			{
				if (this._listeners[num] == null)
				{
					this._listeners.RemoveAt(num);
				}
				num--;
			}
		}

		// Token: 0x0400086C RID: 2156
		private readonly List<TElem> _listeners;

		// Token: 0x0400086D RID: 2157
		private readonly Listeners<TElem>.Func<TElem, bool> _filter;

		// Token: 0x0400086E RID: 2158
		private readonly int _objectID;

		// Token: 0x0400086F RID: 2159
		private int _listenerReaderCount;

		// Token: 0x020000F2 RID: 242
		// (Invoke) Token: 0x06000CCC RID: 3276
		internal delegate void Action<T1, T2, T3, T4>(T1 arg1, T2 arg2, T3 arg3, T4 arg4);

		// Token: 0x020000F3 RID: 243
		// (Invoke) Token: 0x06000CD0 RID: 3280
		internal delegate TResult Func<T1, TResult>(T1 arg1);
	}
}
