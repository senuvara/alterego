﻿using System;

namespace System.Data
{
	/// <summary>Represents the method that will handle the <see cref="E:System.Data.DataSet.MergeFailed" /> event.</summary>
	/// <param name="sender">The source of the event.</param>
	/// <param name="e">The data for the event.</param>
	// Token: 0x020000D4 RID: 212
	// (Invoke) Token: 0x06000BCE RID: 3022
	public delegate void MergeFailedEventHandler(object sender, MergeFailedEventArgs e);
}
