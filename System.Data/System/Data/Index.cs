﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading;

namespace System.Data
{
	// Token: 0x020000ED RID: 237
	internal sealed class Index
	{
		// Token: 0x06000C77 RID: 3191 RVA: 0x0003A52F File Offset: 0x0003872F
		public Index(DataTable table, IndexField[] indexFields, DataViewRowState recordStates, IFilter rowFilter) : this(table, indexFields, null, recordStates, rowFilter)
		{
		}

		// Token: 0x06000C78 RID: 3192 RVA: 0x0003A53D File Offset: 0x0003873D
		public Index(DataTable table, Comparison<DataRow> comparison, DataViewRowState recordStates, IFilter rowFilter) : this(table, Index.GetAllFields(table.Columns), comparison, recordStates, rowFilter)
		{
		}

		// Token: 0x06000C79 RID: 3193 RVA: 0x0003A558 File Offset: 0x00038758
		private static IndexField[] GetAllFields(DataColumnCollection columns)
		{
			IndexField[] array = new IndexField[columns.Count];
			for (int i = 0; i < array.Length; i++)
			{
				array[i] = new IndexField(columns[i], false);
			}
			return array;
		}

		// Token: 0x06000C7A RID: 3194 RVA: 0x0003A594 File Offset: 0x00038794
		private Index(DataTable table, IndexField[] indexFields, Comparison<DataRow> comparison, DataViewRowState recordStates, IFilter rowFilter)
		{
			DataCommonEventSource.Log.Trace<int, int, DataViewRowState>("<ds.Index.Index|API> {0}, table={1}, recordStates={2}", this.ObjectID, (table != null) ? table.ObjectID : 0, recordStates);
			if ((recordStates & ~(DataViewRowState.Unchanged | DataViewRowState.Added | DataViewRowState.Deleted | DataViewRowState.ModifiedCurrent | DataViewRowState.ModifiedOriginal)) != DataViewRowState.None)
			{
				throw ExceptionBuilder.RecordStateRange();
			}
			this._table = table;
			this._listeners = new Listeners<DataViewListener>(this.ObjectID, (DataViewListener listener) => listener != null);
			this._indexFields = indexFields;
			this._recordStates = recordStates;
			this._comparison = comparison;
			DataColumnCollection columns = table.Columns;
			this._isSharable = (rowFilter == null && comparison == null);
			if (rowFilter != null)
			{
				this._rowFilter = new WeakReference(rowFilter);
				DataExpression dataExpression = rowFilter as DataExpression;
				if (dataExpression != null)
				{
					this._hasRemoteAggregate = dataExpression.HasRemoteAggregate();
				}
			}
			this.InitRecords(rowFilter);
		}

		// Token: 0x06000C7B RID: 3195 RVA: 0x0003A67C File Offset: 0x0003887C
		public bool Equal(IndexField[] indexDesc, DataViewRowState recordStates, IFilter rowFilter)
		{
			if (!this._isSharable || this._indexFields.Length != indexDesc.Length || this._recordStates != recordStates || rowFilter != null)
			{
				return false;
			}
			for (int i = 0; i < this._indexFields.Length; i++)
			{
				if (this._indexFields[i].Column != indexDesc[i].Column || this._indexFields[i].IsDescending != indexDesc[i].IsDescending)
				{
					return false;
				}
			}
			return true;
		}

		// Token: 0x17000230 RID: 560
		// (get) Token: 0x06000C7C RID: 3196 RVA: 0x0003A700 File Offset: 0x00038900
		internal bool HasRemoteAggregate
		{
			get
			{
				return this._hasRemoteAggregate;
			}
		}

		// Token: 0x17000231 RID: 561
		// (get) Token: 0x06000C7D RID: 3197 RVA: 0x0003A708 File Offset: 0x00038908
		internal int ObjectID
		{
			get
			{
				return this._objectID;
			}
		}

		// Token: 0x17000232 RID: 562
		// (get) Token: 0x06000C7E RID: 3198 RVA: 0x0003A710 File Offset: 0x00038910
		public DataViewRowState RecordStates
		{
			get
			{
				return this._recordStates;
			}
		}

		// Token: 0x17000233 RID: 563
		// (get) Token: 0x06000C7F RID: 3199 RVA: 0x0003A718 File Offset: 0x00038918
		public IFilter RowFilter
		{
			get
			{
				return (IFilter)((this._rowFilter != null) ? this._rowFilter.Target : null);
			}
		}

		// Token: 0x06000C80 RID: 3200 RVA: 0x0003A735 File Offset: 0x00038935
		public int GetRecord(int recordIndex)
		{
			return this._records[recordIndex];
		}

		// Token: 0x17000234 RID: 564
		// (get) Token: 0x06000C81 RID: 3201 RVA: 0x0003A743 File Offset: 0x00038943
		public bool HasDuplicates
		{
			get
			{
				return this._records.HasDuplicates;
			}
		}

		// Token: 0x17000235 RID: 565
		// (get) Token: 0x06000C82 RID: 3202 RVA: 0x0003A750 File Offset: 0x00038950
		public int RecordCount
		{
			get
			{
				return this._recordCount;
			}
		}

		// Token: 0x17000236 RID: 566
		// (get) Token: 0x06000C83 RID: 3203 RVA: 0x0003A758 File Offset: 0x00038958
		public bool IsSharable
		{
			get
			{
				return this._isSharable;
			}
		}

		// Token: 0x06000C84 RID: 3204 RVA: 0x0003A760 File Offset: 0x00038960
		private bool AcceptRecord(int record)
		{
			return this.AcceptRecord(record, this.RowFilter);
		}

		// Token: 0x06000C85 RID: 3205 RVA: 0x0003A770 File Offset: 0x00038970
		private bool AcceptRecord(int record, IFilter filter)
		{
			DataCommonEventSource.Log.Trace<int, int>("<ds.Index.AcceptRecord|API> {0}, record={1}", this.ObjectID, record);
			if (filter == null)
			{
				return true;
			}
			DataRow dataRow = this._table._recordManager[record];
			if (dataRow == null)
			{
				return true;
			}
			DataRowVersion version = DataRowVersion.Default;
			if (dataRow._oldRecord == record)
			{
				version = DataRowVersion.Original;
			}
			else if (dataRow._newRecord == record)
			{
				version = DataRowVersion.Current;
			}
			else if (dataRow._tempRecord == record)
			{
				version = DataRowVersion.Proposed;
			}
			return filter.Invoke(dataRow, version);
		}

		// Token: 0x06000C86 RID: 3206 RVA: 0x0003A7EE File Offset: 0x000389EE
		internal void ListChangedAdd(DataViewListener listener)
		{
			this._listeners.Add(listener);
		}

		// Token: 0x06000C87 RID: 3207 RVA: 0x0003A7FC File Offset: 0x000389FC
		internal void ListChangedRemove(DataViewListener listener)
		{
			this._listeners.Remove(listener);
		}

		// Token: 0x17000237 RID: 567
		// (get) Token: 0x06000C88 RID: 3208 RVA: 0x0003A80A File Offset: 0x00038A0A
		public int RefCount
		{
			get
			{
				return this._refCount;
			}
		}

		// Token: 0x06000C89 RID: 3209 RVA: 0x0003A814 File Offset: 0x00038A14
		public void AddRef()
		{
			DataCommonEventSource.Log.Trace<int>("<ds.Index.AddRef|API> {0}", this.ObjectID);
			this._table._indexesLock.EnterWriteLock();
			try
			{
				if (this._refCount == 0)
				{
					this._table.ShadowIndexCopy();
					this._table._indexes.Add(this);
				}
				this._refCount++;
			}
			finally
			{
				this._table._indexesLock.ExitWriteLock();
			}
		}

		// Token: 0x06000C8A RID: 3210 RVA: 0x0003A89C File Offset: 0x00038A9C
		public int RemoveRef()
		{
			DataCommonEventSource.Log.Trace<int>("<ds.Index.RemoveRef|API> {0}", this.ObjectID);
			this._table._indexesLock.EnterWriteLock();
			int result;
			try
			{
				int num = this._refCount - 1;
				this._refCount = num;
				result = num;
				if (this._refCount <= 0)
				{
					this._table.ShadowIndexCopy();
					this._table._indexes.Remove(this);
				}
			}
			finally
			{
				this._table._indexesLock.ExitWriteLock();
			}
			return result;
		}

		// Token: 0x06000C8B RID: 3211 RVA: 0x0003A92C File Offset: 0x00038B2C
		private void ApplyChangeAction(int record, int action, int changeRecord)
		{
			if (action != 0)
			{
				if (action > 0)
				{
					if (this.AcceptRecord(record))
					{
						this.InsertRecord(record, true);
						return;
					}
				}
				else
				{
					if (this._comparison != null && -1 != record)
					{
						this.DeleteRecord(this.GetIndex(record, changeRecord));
						return;
					}
					this.DeleteRecord(this.GetIndex(record));
				}
			}
		}

		// Token: 0x06000C8C RID: 3212 RVA: 0x0003A97B File Offset: 0x00038B7B
		public bool CheckUnique()
		{
			return !this.HasDuplicates;
		}

		// Token: 0x06000C8D RID: 3213 RVA: 0x0003A988 File Offset: 0x00038B88
		private int CompareRecords(int record1, int record2)
		{
			if (this._comparison != null)
			{
				return this.CompareDataRows(record1, record2);
			}
			if (this._indexFields.Length != 0)
			{
				int i = 0;
				while (i < this._indexFields.Length)
				{
					int num = this._indexFields[i].Column.Compare(record1, record2);
					if (num != 0)
					{
						if (!this._indexFields[i].IsDescending)
						{
							return num;
						}
						return -num;
					}
					else
					{
						i++;
					}
				}
				return 0;
			}
			return this._table.Rows.IndexOf(this._table._recordManager[record1]).CompareTo(this._table.Rows.IndexOf(this._table._recordManager[record2]));
		}

		// Token: 0x06000C8E RID: 3214 RVA: 0x0003AA42 File Offset: 0x00038C42
		private int CompareDataRows(int record1, int record2)
		{
			return this._comparison(this._table._recordManager[record1], this._table._recordManager[record2]);
		}

		// Token: 0x06000C8F RID: 3215 RVA: 0x0003AA74 File Offset: 0x00038C74
		private int CompareDuplicateRecords(int record1, int record2)
		{
			if (this._table._recordManager[record1] == null)
			{
				if (this._table._recordManager[record2] != null)
				{
					return -1;
				}
				return 0;
			}
			else
			{
				if (this._table._recordManager[record2] == null)
				{
					return 1;
				}
				int num = this._table._recordManager[record1].rowID.CompareTo(this._table._recordManager[record2].rowID);
				if (num == 0 && record1 != record2)
				{
					num = ((int)this._table._recordManager[record1].GetRecordState(record1)).CompareTo((int)this._table._recordManager[record2].GetRecordState(record2));
				}
				return num;
			}
		}

		// Token: 0x06000C90 RID: 3216 RVA: 0x0003AB34 File Offset: 0x00038D34
		private int CompareRecordToKey(int record1, object[] vals)
		{
			int i = 0;
			while (i < this._indexFields.Length)
			{
				int num = this._indexFields[i].Column.CompareValueTo(record1, vals[i]);
				if (num != 0)
				{
					if (!this._indexFields[i].IsDescending)
					{
						return num;
					}
					return -num;
				}
				else
				{
					i++;
				}
			}
			return 0;
		}

		// Token: 0x06000C91 RID: 3217 RVA: 0x0003AB8B File Offset: 0x00038D8B
		public void DeleteRecordFromIndex(int recordIndex)
		{
			this.DeleteRecord(recordIndex, false);
		}

		// Token: 0x06000C92 RID: 3218 RVA: 0x0003AB95 File Offset: 0x00038D95
		private void DeleteRecord(int recordIndex)
		{
			this.DeleteRecord(recordIndex, true);
		}

		// Token: 0x06000C93 RID: 3219 RVA: 0x0003ABA0 File Offset: 0x00038DA0
		private void DeleteRecord(int recordIndex, bool fireEvent)
		{
			DataCommonEventSource.Log.Trace<int, int, bool>("<ds.Index.DeleteRecord|INFO> {0}, recordIndex={1}, fireEvent={2}", this.ObjectID, recordIndex, fireEvent);
			if (recordIndex >= 0)
			{
				this._recordCount--;
				int record = this._records.DeleteByIndex(recordIndex);
				this.MaintainDataView(ListChangedType.ItemDeleted, record, !fireEvent);
				if (fireEvent)
				{
					this.OnListChanged(ListChangedType.ItemDeleted, recordIndex);
				}
			}
		}

		// Token: 0x06000C94 RID: 3220 RVA: 0x0003ABFA File Offset: 0x00038DFA
		public RBTree<int>.RBTreeEnumerator GetEnumerator(int startIndex)
		{
			return new RBTree<int>.RBTreeEnumerator(this._records, startIndex);
		}

		// Token: 0x06000C95 RID: 3221 RVA: 0x0003AC08 File Offset: 0x00038E08
		public int GetIndex(int record)
		{
			return this._records.GetIndexByKey(record);
		}

		// Token: 0x06000C96 RID: 3222 RVA: 0x0003AC18 File Offset: 0x00038E18
		private int GetIndex(int record, int changeRecord)
		{
			DataRow dataRow = this._table._recordManager[record];
			int newRecord = dataRow._newRecord;
			int oldRecord = dataRow._oldRecord;
			int indexByKey;
			try
			{
				if (changeRecord != 1)
				{
					if (changeRecord == 2)
					{
						dataRow._oldRecord = record;
					}
				}
				else
				{
					dataRow._newRecord = record;
				}
				indexByKey = this._records.GetIndexByKey(record);
			}
			finally
			{
				if (changeRecord != 1)
				{
					if (changeRecord == 2)
					{
						dataRow._oldRecord = oldRecord;
					}
				}
				else
				{
					dataRow._newRecord = newRecord;
				}
			}
			return indexByKey;
		}

		// Token: 0x06000C97 RID: 3223 RVA: 0x0003AC9C File Offset: 0x00038E9C
		public object[] GetUniqueKeyValues()
		{
			if (this._indexFields == null || this._indexFields.Length == 0)
			{
				return Array.Empty<object>();
			}
			List<object[]> list = new List<object[]>();
			this.GetUniqueKeyValues(list, this._records.root);
			return list.ToArray();
		}

		// Token: 0x06000C98 RID: 3224 RVA: 0x0003ACE0 File Offset: 0x00038EE0
		public int FindRecord(int record)
		{
			int num = this._records.Search(record);
			if (num != 0)
			{
				return this._records.GetIndexByNode(num);
			}
			return -1;
		}

		// Token: 0x06000C99 RID: 3225 RVA: 0x0003AD0C File Offset: 0x00038F0C
		public int FindRecordByKey(object key)
		{
			int num = this.FindNodeByKey(key);
			if (num != 0)
			{
				return this._records.GetIndexByNode(num);
			}
			return -1;
		}

		// Token: 0x06000C9A RID: 3226 RVA: 0x0003AD34 File Offset: 0x00038F34
		public int FindRecordByKey(object[] key)
		{
			int num = this.FindNodeByKeys(key);
			if (num != 0)
			{
				return this._records.GetIndexByNode(num);
			}
			return -1;
		}

		// Token: 0x06000C9B RID: 3227 RVA: 0x0003AD5C File Offset: 0x00038F5C
		private int FindNodeByKey(object originalKey)
		{
			if (this._indexFields.Length != 1)
			{
				throw ExceptionBuilder.IndexKeyLength(this._indexFields.Length, 1);
			}
			int num = this._records.root;
			if (num != 0)
			{
				DataColumn column = this._indexFields[0].Column;
				object value = column.ConvertValue(originalKey);
				num = this._records.root;
				if (this._indexFields[0].IsDescending)
				{
					while (num != 0)
					{
						int num2 = column.CompareValueTo(this._records.Key(num), value);
						if (num2 == 0)
						{
							break;
						}
						if (num2 < 0)
						{
							num = this._records.Left(num);
						}
						else
						{
							num = this._records.Right(num);
						}
					}
				}
				else
				{
					while (num != 0)
					{
						int num2 = column.CompareValueTo(this._records.Key(num), value);
						if (num2 == 0)
						{
							break;
						}
						if (num2 > 0)
						{
							num = this._records.Left(num);
						}
						else
						{
							num = this._records.Right(num);
						}
					}
				}
			}
			return num;
		}

		// Token: 0x06000C9C RID: 3228 RVA: 0x0003AE48 File Offset: 0x00039048
		private int FindNodeByKeys(object[] originalKey)
		{
			int num = (originalKey != null) ? originalKey.Length : 0;
			if (num == 0 || this._indexFields.Length != num)
			{
				throw ExceptionBuilder.IndexKeyLength(this._indexFields.Length, num);
			}
			int num2 = this._records.root;
			if (num2 != 0)
			{
				object[] array = new object[originalKey.Length];
				for (int i = 0; i < originalKey.Length; i++)
				{
					array[i] = this._indexFields[i].Column.ConvertValue(originalKey[i]);
				}
				num2 = this._records.root;
				while (num2 != 0)
				{
					num = this.CompareRecordToKey(this._records.Key(num2), array);
					if (num == 0)
					{
						break;
					}
					if (num > 0)
					{
						num2 = this._records.Left(num2);
					}
					else
					{
						num2 = this._records.Right(num2);
					}
				}
			}
			return num2;
		}

		// Token: 0x06000C9D RID: 3229 RVA: 0x0003AF08 File Offset: 0x00039108
		private int FindNodeByKeyRecord(int record)
		{
			int num = this._records.root;
			if (num != 0)
			{
				num = this._records.root;
				while (num != 0)
				{
					int num2 = this.CompareRecords(this._records.Key(num), record);
					if (num2 == 0)
					{
						break;
					}
					if (num2 > 0)
					{
						num = this._records.Left(num);
					}
					else
					{
						num = this._records.Right(num);
					}
				}
			}
			return num;
		}

		// Token: 0x06000C9E RID: 3230 RVA: 0x0003AF70 File Offset: 0x00039170
		private Range GetRangeFromNode(int nodeId)
		{
			if (nodeId == 0)
			{
				return default(Range);
			}
			int indexByNode = this._records.GetIndexByNode(nodeId);
			if (this._records.Next(nodeId) == 0)
			{
				return new Range(indexByNode, indexByNode);
			}
			int num = this._records.SubTreeSize(this._records.Next(nodeId));
			return new Range(indexByNode, indexByNode + num - 1);
		}

		// Token: 0x06000C9F RID: 3231 RVA: 0x0003AFD0 File Offset: 0x000391D0
		public Range FindRecords(object key)
		{
			int nodeId = this.FindNodeByKey(key);
			return this.GetRangeFromNode(nodeId);
		}

		// Token: 0x06000CA0 RID: 3232 RVA: 0x0003AFEC File Offset: 0x000391EC
		public Range FindRecords(object[] key)
		{
			int nodeId = this.FindNodeByKeys(key);
			return this.GetRangeFromNode(nodeId);
		}

		// Token: 0x06000CA1 RID: 3233 RVA: 0x0003B008 File Offset: 0x00039208
		internal void FireResetEvent()
		{
			DataCommonEventSource.Log.Trace<int>("<ds.Index.FireResetEvent|API> {0}", this.ObjectID);
			if (this.DoListChanged)
			{
				this.OnListChanged(DataView.s_resetEventArgs);
			}
		}

		// Token: 0x06000CA2 RID: 3234 RVA: 0x0003B034 File Offset: 0x00039234
		private int GetChangeAction(DataViewRowState oldState, DataViewRowState newState)
		{
			int num = ((this._recordStates & oldState) == DataViewRowState.None) ? 0 : 1;
			return (((this._recordStates & newState) == DataViewRowState.None) ? 0 : 1) - num;
		}

		// Token: 0x06000CA3 RID: 3235 RVA: 0x0003B060 File Offset: 0x00039260
		private static int GetReplaceAction(DataViewRowState oldState)
		{
			if ((DataViewRowState.CurrentRows & oldState) != DataViewRowState.None)
			{
				return 1;
			}
			if ((DataViewRowState.OriginalRows & oldState) == DataViewRowState.None)
			{
				return 0;
			}
			return 2;
		}

		// Token: 0x06000CA4 RID: 3236 RVA: 0x0003B073 File Offset: 0x00039273
		public DataRow GetRow(int i)
		{
			return this._table._recordManager[this.GetRecord(i)];
		}

		// Token: 0x06000CA5 RID: 3237 RVA: 0x0003B08C File Offset: 0x0003928C
		public DataRow[] GetRows(object[] values)
		{
			return this.GetRows(this.FindRecords(values));
		}

		// Token: 0x06000CA6 RID: 3238 RVA: 0x0003B09C File Offset: 0x0003929C
		public DataRow[] GetRows(Range range)
		{
			DataRow[] array = this._table.NewRowArray(range.Count);
			if (array.Length != 0)
			{
				RBTree<int>.RBTreeEnumerator enumerator = this.GetEnumerator(range.Min);
				int num = 0;
				while (num < array.Length && enumerator.MoveNext())
				{
					array[num] = this._table._recordManager[enumerator.Current];
					num++;
				}
			}
			return array;
		}

		// Token: 0x06000CA7 RID: 3239 RVA: 0x0003B100 File Offset: 0x00039300
		private void InitRecords(IFilter filter)
		{
			DataViewRowState recordStates = this._recordStates;
			bool append = this._indexFields.Length == 0;
			this._records = new Index.IndexTree(this);
			this._recordCount = 0;
			foreach (object obj in this._table.Rows)
			{
				DataRow dataRow = (DataRow)obj;
				int num = -1;
				if (dataRow._oldRecord == dataRow._newRecord)
				{
					if ((recordStates & DataViewRowState.Unchanged) != DataViewRowState.None)
					{
						num = dataRow._oldRecord;
					}
				}
				else if (dataRow._oldRecord == -1)
				{
					if ((recordStates & DataViewRowState.Added) != DataViewRowState.None)
					{
						num = dataRow._newRecord;
					}
				}
				else if (dataRow._newRecord == -1)
				{
					if ((recordStates & DataViewRowState.Deleted) != DataViewRowState.None)
					{
						num = dataRow._oldRecord;
					}
				}
				else if ((recordStates & DataViewRowState.ModifiedCurrent) != DataViewRowState.None)
				{
					num = dataRow._newRecord;
				}
				else if ((recordStates & DataViewRowState.ModifiedOriginal) != DataViewRowState.None)
				{
					num = dataRow._oldRecord;
				}
				if (num != -1 && this.AcceptRecord(num, filter))
				{
					this._records.InsertAt(-1, num, append);
					this._recordCount++;
				}
			}
		}

		// Token: 0x06000CA8 RID: 3240 RVA: 0x0003B224 File Offset: 0x00039424
		public int InsertRecordToIndex(int record)
		{
			int result = -1;
			if (this.AcceptRecord(record))
			{
				result = this.InsertRecord(record, false);
			}
			return result;
		}

		// Token: 0x06000CA9 RID: 3241 RVA: 0x0003B248 File Offset: 0x00039448
		private int InsertRecord(int record, bool fireEvent)
		{
			DataCommonEventSource.Log.Trace<int, int, bool>("<ds.Index.InsertRecord|INFO> {0}, record={1}, fireEvent={2}", this.ObjectID, record, fireEvent);
			bool append = false;
			if (this._indexFields.Length == 0 && this._table != null)
			{
				DataRow row = this._table._recordManager[record];
				append = (this._table.Rows.IndexOf(row) + 1 == this._table.Rows.Count);
			}
			int node = this._records.InsertAt(-1, record, append);
			this._recordCount++;
			this.MaintainDataView(ListChangedType.ItemAdded, record, !fireEvent);
			if (fireEvent)
			{
				if (this.DoListChanged)
				{
					this.OnListChanged(ListChangedType.ItemAdded, this._records.GetIndexByNode(node));
				}
				return 0;
			}
			return this._records.GetIndexByNode(node);
		}

		// Token: 0x06000CAA RID: 3242 RVA: 0x0003B30C File Offset: 0x0003950C
		public bool IsKeyInIndex(object key)
		{
			int num = this.FindNodeByKey(key);
			return num != 0;
		}

		// Token: 0x06000CAB RID: 3243 RVA: 0x0003B328 File Offset: 0x00039528
		public bool IsKeyInIndex(object[] key)
		{
			int num = this.FindNodeByKeys(key);
			return num != 0;
		}

		// Token: 0x06000CAC RID: 3244 RVA: 0x0003B344 File Offset: 0x00039544
		public bool IsKeyRecordInIndex(int record)
		{
			int num = this.FindNodeByKeyRecord(record);
			return num != 0;
		}

		// Token: 0x17000238 RID: 568
		// (get) Token: 0x06000CAD RID: 3245 RVA: 0x0003B35D File Offset: 0x0003955D
		private bool DoListChanged
		{
			get
			{
				return !this._suspendEvents && this._listeners.HasListeners && !this._table.AreIndexEventsSuspended;
			}
		}

		// Token: 0x06000CAE RID: 3246 RVA: 0x0003B384 File Offset: 0x00039584
		private void OnListChanged(ListChangedType changedType, int newIndex, int oldIndex)
		{
			if (this.DoListChanged)
			{
				this.OnListChanged(new ListChangedEventArgs(changedType, newIndex, oldIndex));
			}
		}

		// Token: 0x06000CAF RID: 3247 RVA: 0x0003B39C File Offset: 0x0003959C
		private void OnListChanged(ListChangedType changedType, int index)
		{
			if (this.DoListChanged)
			{
				this.OnListChanged(new ListChangedEventArgs(changedType, index));
			}
		}

		// Token: 0x06000CB0 RID: 3248 RVA: 0x0003B3B4 File Offset: 0x000395B4
		private void OnListChanged(ListChangedEventArgs e)
		{
			DataCommonEventSource.Log.Trace<int>("<ds.Index.OnListChanged|INFO> {0}", this.ObjectID);
			this._listeners.Notify<ListChangedEventArgs, bool, bool>(e, false, false, delegate(DataViewListener listener, ListChangedEventArgs args, bool arg2, bool arg3)
			{
				listener.IndexListChanged(args);
			});
		}

		// Token: 0x06000CB1 RID: 3249 RVA: 0x0003B404 File Offset: 0x00039604
		private void MaintainDataView(ListChangedType changedType, int record, bool trackAddRemove)
		{
			this._listeners.Notify<ListChangedType, DataRow, bool>(changedType, (0 <= record) ? this._table._recordManager[record] : null, trackAddRemove, delegate(DataViewListener listener, ListChangedType type, DataRow row, bool track)
			{
				listener.MaintainDataView(changedType, row, track);
			});
		}

		// Token: 0x06000CB2 RID: 3250 RVA: 0x0003B454 File Offset: 0x00039654
		public void Reset()
		{
			DataCommonEventSource.Log.Trace<int>("<ds.Index.Reset|API> {0}", this.ObjectID);
			this.InitRecords(this.RowFilter);
			this.MaintainDataView(ListChangedType.Reset, -1, false);
			this.FireResetEvent();
		}

		// Token: 0x06000CB3 RID: 3251 RVA: 0x0003B488 File Offset: 0x00039688
		public void RecordChanged(int record)
		{
			DataCommonEventSource.Log.Trace<int, int>("<ds.Index.RecordChanged|API> {0}, record={1}", this.ObjectID, record);
			if (this.DoListChanged)
			{
				int index = this.GetIndex(record);
				if (index >= 0)
				{
					this.OnListChanged(ListChangedType.ItemChanged, index);
				}
			}
		}

		// Token: 0x06000CB4 RID: 3252 RVA: 0x0003B4C8 File Offset: 0x000396C8
		public void RecordChanged(int oldIndex, int newIndex)
		{
			DataCommonEventSource.Log.Trace<int, int, int>("<ds.Index.RecordChanged|API> {0}, oldIndex={1}, newIndex={2}", this.ObjectID, oldIndex, newIndex);
			if (oldIndex > -1 || newIndex > -1)
			{
				if (oldIndex == newIndex)
				{
					this.OnListChanged(ListChangedType.ItemChanged, newIndex, oldIndex);
					return;
				}
				if (oldIndex == -1)
				{
					this.OnListChanged(ListChangedType.ItemAdded, newIndex, oldIndex);
					return;
				}
				if (newIndex == -1)
				{
					this.OnListChanged(ListChangedType.ItemDeleted, oldIndex);
					return;
				}
				this.OnListChanged(ListChangedType.ItemMoved, newIndex, oldIndex);
			}
		}

		// Token: 0x06000CB5 RID: 3253 RVA: 0x0003B528 File Offset: 0x00039728
		public void RecordStateChanged(int record, DataViewRowState oldState, DataViewRowState newState)
		{
			DataCommonEventSource.Log.Trace<int, int, DataViewRowState, DataViewRowState>("<ds.Index.RecordStateChanged|API> {0}, record={1}, oldState={2}, newState={3}", this.ObjectID, record, oldState, newState);
			int changeAction = this.GetChangeAction(oldState, newState);
			this.ApplyChangeAction(record, changeAction, Index.GetReplaceAction(oldState));
		}

		// Token: 0x06000CB6 RID: 3254 RVA: 0x0003B564 File Offset: 0x00039764
		public void RecordStateChanged(int oldRecord, DataViewRowState oldOldState, DataViewRowState oldNewState, int newRecord, DataViewRowState newOldState, DataViewRowState newNewState)
		{
			DataCommonEventSource.Log.Trace<int, int, DataViewRowState, DataViewRowState, int, DataViewRowState, DataViewRowState>("<ds.Index.RecordStateChanged|API> {0}, oldRecord={1}, oldOldState={2}, oldNewState={3}, newRecord={4}, newOldState={5}, newNewState={6}", this.ObjectID, oldRecord, oldOldState, oldNewState, newRecord, newOldState, newNewState);
			int changeAction = this.GetChangeAction(oldOldState, oldNewState);
			int changeAction2 = this.GetChangeAction(newOldState, newNewState);
			if (changeAction != -1 || changeAction2 != 1 || !this.AcceptRecord(newRecord))
			{
				this.ApplyChangeAction(oldRecord, changeAction, Index.GetReplaceAction(oldOldState));
				this.ApplyChangeAction(newRecord, changeAction2, Index.GetReplaceAction(newOldState));
				return;
			}
			int index;
			if (this._comparison != null && changeAction < 0)
			{
				index = this.GetIndex(oldRecord, Index.GetReplaceAction(oldOldState));
			}
			else
			{
				index = this.GetIndex(oldRecord);
			}
			if (this._comparison == null && index != -1 && this.CompareRecords(oldRecord, newRecord) == 0)
			{
				this._records.UpdateNodeKey(oldRecord, newRecord);
				int index2 = this.GetIndex(newRecord);
				this.OnListChanged(ListChangedType.ItemChanged, index2, index2);
				return;
			}
			this._suspendEvents = true;
			if (index != -1)
			{
				this._records.DeleteByIndex(index);
				this._recordCount--;
			}
			this._records.Insert(newRecord);
			this._recordCount++;
			this._suspendEvents = false;
			int index3 = this.GetIndex(newRecord);
			if (index == index3)
			{
				this.OnListChanged(ListChangedType.ItemChanged, index3, index);
				return;
			}
			if (index == -1)
			{
				this.MaintainDataView(ListChangedType.ItemAdded, newRecord, false);
				this.OnListChanged(ListChangedType.ItemAdded, this.GetIndex(newRecord));
				return;
			}
			this.OnListChanged(ListChangedType.ItemMoved, index3, index);
		}

		// Token: 0x17000239 RID: 569
		// (get) Token: 0x06000CB7 RID: 3255 RVA: 0x0003B6C4 File Offset: 0x000398C4
		internal DataTable Table
		{
			get
			{
				return this._table;
			}
		}

		// Token: 0x06000CB8 RID: 3256 RVA: 0x0003B6CC File Offset: 0x000398CC
		private void GetUniqueKeyValues(List<object[]> list, int curNodeId)
		{
			if (curNodeId != 0)
			{
				this.GetUniqueKeyValues(list, this._records.Left(curNodeId));
				int record = this._records.Key(curNodeId);
				object[] array = new object[this._indexFields.Length];
				for (int i = 0; i < array.Length; i++)
				{
					array[i] = this._indexFields[i].Column[record];
				}
				list.Add(array);
				this.GetUniqueKeyValues(list, this._records.Right(curNodeId));
			}
		}

		// Token: 0x06000CB9 RID: 3257 RVA: 0x0003B74C File Offset: 0x0003994C
		internal static int IndexOfReference<T>(List<T> list, T item) where T : class
		{
			if (list != null)
			{
				for (int i = 0; i < list.Count; i++)
				{
					if (list[i] == item)
					{
						return i;
					}
				}
			}
			return -1;
		}

		// Token: 0x06000CBA RID: 3258 RVA: 0x0003B784 File Offset: 0x00039984
		internal static bool ContainsReference<T>(List<T> list, T item) where T : class
		{
			return 0 <= Index.IndexOfReference<T>(list, item);
		}

		// Token: 0x04000855 RID: 2133
		private const int DoNotReplaceCompareRecord = 0;

		// Token: 0x04000856 RID: 2134
		private const int ReplaceNewRecordForCompare = 1;

		// Token: 0x04000857 RID: 2135
		private const int ReplaceOldRecordForCompare = 2;

		// Token: 0x04000858 RID: 2136
		private readonly DataTable _table;

		// Token: 0x04000859 RID: 2137
		internal readonly IndexField[] _indexFields;

		// Token: 0x0400085A RID: 2138
		private readonly Comparison<DataRow> _comparison;

		// Token: 0x0400085B RID: 2139
		private readonly DataViewRowState _recordStates;

		// Token: 0x0400085C RID: 2140
		private WeakReference _rowFilter;

		// Token: 0x0400085D RID: 2141
		private Index.IndexTree _records;

		// Token: 0x0400085E RID: 2142
		private int _recordCount;

		// Token: 0x0400085F RID: 2143
		private int _refCount;

		// Token: 0x04000860 RID: 2144
		private Listeners<DataViewListener> _listeners;

		// Token: 0x04000861 RID: 2145
		private bool _suspendEvents;

		// Token: 0x04000862 RID: 2146
		private readonly bool _isSharable;

		// Token: 0x04000863 RID: 2147
		private readonly bool _hasRemoteAggregate;

		// Token: 0x04000864 RID: 2148
		internal const int MaskBits = 2147483647;

		// Token: 0x04000865 RID: 2149
		private static int s_objectTypeCount;

		// Token: 0x04000866 RID: 2150
		private readonly int _objectID = Interlocked.Increment(ref Index.s_objectTypeCount);

		// Token: 0x020000EE RID: 238
		private sealed class IndexTree : RBTree<int>
		{
			// Token: 0x06000CBB RID: 3259 RVA: 0x0003B793 File Offset: 0x00039993
			internal IndexTree(Index index) : base(TreeAccessMethod.KEY_SEARCH_AND_INDEX)
			{
				this._index = index;
			}

			// Token: 0x06000CBC RID: 3260 RVA: 0x0003B7A3 File Offset: 0x000399A3
			protected override int CompareNode(int record1, int record2)
			{
				return this._index.CompareRecords(record1, record2);
			}

			// Token: 0x06000CBD RID: 3261 RVA: 0x0003B7B2 File Offset: 0x000399B2
			protected override int CompareSateliteTreeNode(int record1, int record2)
			{
				return this._index.CompareDuplicateRecords(record1, record2);
			}

			// Token: 0x04000867 RID: 2151
			private readonly Index _index;
		}

		// Token: 0x020000EF RID: 239
		[CompilerGenerated]
		[Serializable]
		private sealed class <>c
		{
			// Token: 0x06000CBE RID: 3262 RVA: 0x0003B7C1 File Offset: 0x000399C1
			// Note: this type is marked as 'beforefieldinit'.
			static <>c()
			{
			}

			// Token: 0x06000CBF RID: 3263 RVA: 0x00005C14 File Offset: 0x00003E14
			public <>c()
			{
			}

			// Token: 0x06000CC0 RID: 3264 RVA: 0x0003B7CD File Offset: 0x000399CD
			internal bool <.ctor>b__22_0(DataViewListener listener)
			{
				return listener != null;
			}

			// Token: 0x06000CC1 RID: 3265 RVA: 0x0003B7D3 File Offset: 0x000399D3
			internal void <OnListChanged>b__85_0(DataViewListener listener, ListChangedEventArgs args, bool arg2, bool arg3)
			{
				listener.IndexListChanged(args);
			}

			// Token: 0x04000868 RID: 2152
			public static readonly Index.<>c <>9 = new Index.<>c();

			// Token: 0x04000869 RID: 2153
			public static Listeners<DataViewListener>.Func<DataViewListener, bool> <>9__22_0;

			// Token: 0x0400086A RID: 2154
			public static Listeners<DataViewListener>.Action<DataViewListener, ListChangedEventArgs, bool, bool> <>9__85_0;
		}

		// Token: 0x020000F0 RID: 240
		[CompilerGenerated]
		private sealed class <>c__DisplayClass86_0
		{
			// Token: 0x06000CC2 RID: 3266 RVA: 0x00005C14 File Offset: 0x00003E14
			public <>c__DisplayClass86_0()
			{
			}

			// Token: 0x06000CC3 RID: 3267 RVA: 0x0003B7DC File Offset: 0x000399DC
			internal void <MaintainDataView>b__0(DataViewListener listener, ListChangedType type, DataRow row, bool track)
			{
				listener.MaintainDataView(this.changedType, row, track);
			}

			// Token: 0x0400086B RID: 2155
			public ListChangedType changedType;
		}
	}
}
