﻿using System;
using System.Data.Common;
using System.Globalization;

namespace System.Data.Sql
{
	/// <summary>Provides a mechanism for enumerating all available instances of SQL Server within the local network.</summary>
	// Token: 0x0200011E RID: 286
	public sealed class SqlDataSourceEnumerator : DbDataSourceEnumerator
	{
		// Token: 0x06000E68 RID: 3688 RVA: 0x0004BC37 File Offset: 0x00049E37
		private SqlDataSourceEnumerator()
		{
		}

		/// <summary>Gets an instance of the <see cref="T:System.Data.Sql.SqlDataSourceEnumerator" />, which can be used to retrieve information about available SQL Server instances.</summary>
		/// <returns>An instance of the <see cref="T:System.Data.Sql.SqlDataSourceEnumerator" /> used to retrieve information about available SQL Server instances.</returns>
		// Token: 0x17000272 RID: 626
		// (get) Token: 0x06000E69 RID: 3689 RVA: 0x0004BC3F File Offset: 0x00049E3F
		public static SqlDataSourceEnumerator Instance
		{
			get
			{
				return SqlDataSourceEnumerator.SingletonInstance;
			}
		}

		/// <summary>Retrieves a <see cref="T:System.Data.DataTable" /> containing information about all visible SQL Server 2000 or SQL Server 2005 instances.</summary>
		/// <returns>Returns a <see cref="T:System.Data.DataTable" /> containing information about the visible SQL Server instances.</returns>
		// Token: 0x06000E6A RID: 3690 RVA: 0x0004BC46 File Offset: 0x00049E46
		public override DataTable GetDataSources()
		{
			this.timeoutTime = 0L;
			throw new NotImplementedException();
		}

		// Token: 0x06000E6B RID: 3691 RVA: 0x0004BC58 File Offset: 0x00049E58
		private static DataTable ParseServerEnumString(string serverInstances)
		{
			DataTable dataTable = new DataTable("SqlDataSources");
			dataTable.Locale = CultureInfo.InvariantCulture;
			dataTable.Columns.Add("ServerName", typeof(string));
			dataTable.Columns.Add("InstanceName", typeof(string));
			dataTable.Columns.Add("IsClustered", typeof(string));
			dataTable.Columns.Add("Version", typeof(string));
			string text = null;
			string text2 = null;
			string text3 = null;
			string value = null;
			string[] array = serverInstances.Split(new char[1]);
			for (int i = 0; i < array.Length; i++)
			{
				string text4 = array[i].Trim(new char[1]);
				if (text4.Length != 0)
				{
					foreach (string text5 in text4.Split(new char[]
					{
						';'
					}))
					{
						if (text == null)
						{
							foreach (string text6 in text5.Split(new char[]
							{
								'\\'
							}))
							{
								if (text == null)
								{
									text = text6;
								}
								else
								{
									text2 = text6;
								}
							}
						}
						else if (text3 == null)
						{
							text3 = text5.Substring(SqlDataSourceEnumerator._clusterLength);
						}
						else
						{
							value = text5.Substring(SqlDataSourceEnumerator._versionLength);
						}
					}
					string text7 = "ServerName='" + text + "'";
					if (!ADP.IsEmpty(text2))
					{
						text7 = text7 + " AND InstanceName='" + text2 + "'";
					}
					if (dataTable.Select(text7).Length == 0)
					{
						DataRow dataRow = dataTable.NewRow();
						dataRow[0] = text;
						dataRow[1] = text2;
						dataRow[2] = text3;
						dataRow[3] = value;
						dataTable.Rows.Add(dataRow);
					}
					text = null;
					text2 = null;
					text3 = null;
					value = null;
				}
			}
			foreach (object obj in dataTable.Columns)
			{
				((DataColumn)obj).ReadOnly = true;
			}
			return dataTable;
		}

		// Token: 0x06000E6C RID: 3692 RVA: 0x0004BE90 File Offset: 0x0004A090
		// Note: this type is marked as 'beforefieldinit'.
		static SqlDataSourceEnumerator()
		{
		}

		// Token: 0x04000A22 RID: 2594
		private static readonly SqlDataSourceEnumerator SingletonInstance = new SqlDataSourceEnumerator();

		// Token: 0x04000A23 RID: 2595
		internal const string ServerName = "ServerName";

		// Token: 0x04000A24 RID: 2596
		internal const string InstanceName = "InstanceName";

		// Token: 0x04000A25 RID: 2597
		internal const string IsClustered = "IsClustered";

		// Token: 0x04000A26 RID: 2598
		internal const string Version = "Version";

		// Token: 0x04000A27 RID: 2599
		private long timeoutTime;

		// Token: 0x04000A28 RID: 2600
		private static string _Version = "Version:";

		// Token: 0x04000A29 RID: 2601
		private static string _Cluster = "Clustered:";

		// Token: 0x04000A2A RID: 2602
		private static int _clusterLength = SqlDataSourceEnumerator._Cluster.Length;

		// Token: 0x04000A2B RID: 2603
		private static int _versionLength = SqlDataSourceEnumerator._Version.Length;
	}
}
