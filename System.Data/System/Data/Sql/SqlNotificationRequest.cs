﻿using System;
using System.Data.Common;

namespace System.Data.Sql
{
	/// <summary>Represents a request for notification for a given command. </summary>
	// Token: 0x0200011D RID: 285
	public sealed class SqlNotificationRequest
	{
		/// <summary>Creates a new instance of the <see cref="T:System.Data.Sql.SqlNotificationRequest" /> class with default values.</summary>
		// Token: 0x06000E60 RID: 3680 RVA: 0x0004BB88 File Offset: 0x00049D88
		public SqlNotificationRequest() : this(null, null, 0)
		{
		}

		/// <summary>Creates a new instance of the <see cref="T:System.Data.Sql.SqlNotificationRequest" /> class with a user-defined string that identifies a particular notification request, the name of a predefined SQL Server 2005 Service Broker service name, and the time-out period, measured in seconds.</summary>
		/// <param name="userData">A string that contains an application-specific identifier for this notification. It is not used by the notifications infrastructure, but it allows you to associate notifications with the application state. The value indicated in this parameter is included in the Service Broker queue message. </param>
		/// <param name="options">A string that contains the Service Broker service name where notification messages are posted, and it must include a database name or a Service Broker instance GUID that restricts the scope of the service name lookup to a particular database.For more information about the format of the <paramref name="options" /> parameter, see <see cref="P:System.Data.Sql.SqlNotificationRequest.Options" />.</param>
		/// <param name="timeout">The time, in seconds, to wait for a notification message. </param>
		/// <exception cref="T:System.ArgumentNullException">The value of the <paramref name="options" /> parameter is NULL. </exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The <paramref name="options" /> or <paramref name="userData" /> parameter is longer than <see langword="uint16.MaxValue" /> or the value in the <paramref name="timeout" /> parameter is less than zero. </exception>
		// Token: 0x06000E61 RID: 3681 RVA: 0x0004BB93 File Offset: 0x00049D93
		public SqlNotificationRequest(string userData, string options, int timeout)
		{
			this.UserData = userData;
			this.Timeout = timeout;
			this.Options = options;
		}

		/// <summary>Gets or sets the SQL Server Service Broker service name where notification messages are posted.</summary>
		/// <returns>
		///     <see langword="string" /> that contains the SQL Server 2005 Service Broker service name where notification messages are posted and the database or service broker instance GUID to scope the server name lookup.</returns>
		/// <exception cref="T:System.ArgumentNullException">The value is NULL. </exception>
		/// <exception cref="T:System.ArgumentException">The value is longer than <see langword="uint16.MaxValue" />. </exception>
		// Token: 0x1700026F RID: 623
		// (get) Token: 0x06000E62 RID: 3682 RVA: 0x0004BBB0 File Offset: 0x00049DB0
		// (set) Token: 0x06000E63 RID: 3683 RVA: 0x0004BBB8 File Offset: 0x00049DB8
		public string Options
		{
			get
			{
				return this._options;
			}
			set
			{
				if (value != null && 65535 < value.Length)
				{
					throw ADP.ArgumentOutOfRange(string.Empty, "Options");
				}
				this._options = value;
			}
		}

		/// <summary>Gets or sets a value that specifies how long SQL Server waits for a change to occur before the operation times out.</summary>
		/// <returns>A signed integer value that specifies, in seconds, how long SQL Server waits for a change to occur before the operation times out.</returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The value is less than zero. </exception>
		// Token: 0x17000270 RID: 624
		// (get) Token: 0x06000E64 RID: 3684 RVA: 0x0004BBE1 File Offset: 0x00049DE1
		// (set) Token: 0x06000E65 RID: 3685 RVA: 0x0004BBE9 File Offset: 0x00049DE9
		public int Timeout
		{
			get
			{
				return this._timeout;
			}
			set
			{
				if (0 > value)
				{
					throw ADP.ArgumentOutOfRange(string.Empty, "Timeout");
				}
				this._timeout = value;
			}
		}

		/// <summary>Gets or sets an application-specific identifier for this notification.</summary>
		/// <returns>A <see langword="string" /> value of the application-specific identifier for this notification.</returns>
		/// <exception cref="T:System.ArgumentException">The value is longer than <see langword="uint16.MaxValue" />. </exception>
		// Token: 0x17000271 RID: 625
		// (get) Token: 0x06000E66 RID: 3686 RVA: 0x0004BC06 File Offset: 0x00049E06
		// (set) Token: 0x06000E67 RID: 3687 RVA: 0x0004BC0E File Offset: 0x00049E0E
		public string UserData
		{
			get
			{
				return this._userData;
			}
			set
			{
				if (value != null && 65535 < value.Length)
				{
					throw ADP.ArgumentOutOfRange(string.Empty, "UserData");
				}
				this._userData = value;
			}
		}

		// Token: 0x04000A1F RID: 2591
		private string _userData;

		// Token: 0x04000A20 RID: 2592
		private string _options;

		// Token: 0x04000A21 RID: 2593
		private int _timeout;
	}
}
