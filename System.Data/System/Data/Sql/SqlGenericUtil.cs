﻿using System;
using System.Data.Common;

namespace System.Data.Sql
{
	// Token: 0x0200011F RID: 287
	internal sealed class SqlGenericUtil
	{
		// Token: 0x06000E6D RID: 3693 RVA: 0x00005C14 File Offset: 0x00003E14
		private SqlGenericUtil()
		{
		}

		// Token: 0x06000E6E RID: 3694 RVA: 0x0004BECE File Offset: 0x0004A0CE
		internal static Exception NullCommandText()
		{
			return ADP.Argument(Res.GetString("Command parameter must have a non null and non empty command text."));
		}

		// Token: 0x06000E6F RID: 3695 RVA: 0x0004BEDF File Offset: 0x0004A0DF
		internal static Exception MismatchedMetaDataDirectionArrayLengths()
		{
			return ADP.Argument(Res.GetString("MetaData parameter array must have length equivalent to ParameterDirection array argument."));
		}
	}
}
