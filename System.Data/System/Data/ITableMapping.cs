﻿using System;

namespace System.Data
{
	/// <summary>Associates a source table with a table in a <see cref="T:System.Data.DataSet" />, and is implemented by the <see cref="T:System.Data.Common.DataTableMapping" /> class, which is used in common by .NET Framework data providers.</summary>
	// Token: 0x020000CD RID: 205
	public interface ITableMapping
	{
		/// <summary>Gets the derived <see cref="T:System.Data.Common.DataColumnMappingCollection" /> for the <see cref="T:System.Data.DataTable" />.</summary>
		/// <returns>A collection of data column mappings.</returns>
		// Token: 0x1700021B RID: 539
		// (get) Token: 0x06000BBE RID: 3006
		IColumnMappingCollection ColumnMappings { get; }

		/// <summary>Gets or sets the case-insensitive name of the table within the <see cref="T:System.Data.DataSet" />.</summary>
		/// <returns>The case-insensitive name of the table within the <see cref="T:System.Data.DataSet" />.</returns>
		// Token: 0x1700021C RID: 540
		// (get) Token: 0x06000BBF RID: 3007
		// (set) Token: 0x06000BC0 RID: 3008
		string DataSetTable { get; set; }

		/// <summary>Gets or sets the case-sensitive name of the source table.</summary>
		/// <returns>The case-sensitive name of the source table.</returns>
		// Token: 0x1700021D RID: 541
		// (get) Token: 0x06000BC1 RID: 3009
		// (set) Token: 0x06000BC2 RID: 3010
		string SourceTable { get; set; }
	}
}
