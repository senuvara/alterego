﻿using System;

namespace System.Runtime.CompilerServices
{
	// Token: 0x02000028 RID: 40
	internal class FriendAccessAllowedAttribute : Attribute
	{
		// Token: 0x060000C6 RID: 198 RVA: 0x00005E14 File Offset: 0x00004014
		public FriendAccessAllowedAttribute()
		{
		}
	}
}
