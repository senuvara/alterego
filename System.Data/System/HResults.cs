﻿using System;

namespace System
{
	// Token: 0x02000021 RID: 33
	internal static class HResults
	{
		// Token: 0x040003F7 RID: 1015
		internal const int Data = -2146232032;

		// Token: 0x040003F8 RID: 1016
		internal const int DataDeletedRowInaccessible = -2146232031;

		// Token: 0x040003F9 RID: 1017
		internal const int DataDuplicateName = -2146232030;

		// Token: 0x040003FA RID: 1018
		internal const int DataInRowChangingEvent = -2146232029;

		// Token: 0x040003FB RID: 1019
		internal const int DataInvalidConstraint = -2146232028;

		// Token: 0x040003FC RID: 1020
		internal const int DataMissingPrimaryKey = -2146232027;

		// Token: 0x040003FD RID: 1021
		internal const int DataNoNullAllowed = -2146232026;

		// Token: 0x040003FE RID: 1022
		internal const int DataReadOnly = -2146232025;

		// Token: 0x040003FF RID: 1023
		internal const int DataRowNotInTable = -2146232024;

		// Token: 0x04000400 RID: 1024
		internal const int DataVersionNotFound = -2146232023;

		// Token: 0x04000401 RID: 1025
		internal const int DataConstraint = -2146232022;

		// Token: 0x04000402 RID: 1026
		internal const int StrongTyping = -2146232021;

		// Token: 0x04000403 RID: 1027
		internal const int SqlType = -2146232016;

		// Token: 0x04000404 RID: 1028
		internal const int SqlNullValue = -2146232015;

		// Token: 0x04000405 RID: 1029
		internal const int SqlTruncate = -2146232014;

		// Token: 0x04000406 RID: 1030
		internal const int DBConcurrency = -2146232011;

		// Token: 0x04000407 RID: 1031
		internal const int OperationAborted = -2146232010;
	}
}
