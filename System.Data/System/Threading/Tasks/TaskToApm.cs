﻿using System;
using System.Runtime.CompilerServices;

namespace System.Threading.Tasks
{
	// Token: 0x02000036 RID: 54
	internal static class TaskToApm
	{
		// Token: 0x06000221 RID: 545 RVA: 0x0000CFFC File Offset: 0x0000B1FC
		public static IAsyncResult Begin(Task task, AsyncCallback callback, object state)
		{
			IAsyncResult asyncResult;
			if (task.IsCompleted)
			{
				asyncResult = new TaskToApm.TaskWrapperAsyncResult(task, state, true);
				if (callback != null)
				{
					callback(asyncResult);
				}
			}
			else
			{
				IAsyncResult asyncResult3;
				if (task.AsyncState != state)
				{
					IAsyncResult asyncResult2 = new TaskToApm.TaskWrapperAsyncResult(task, state, false);
					asyncResult3 = asyncResult2;
				}
				else
				{
					asyncResult3 = task;
				}
				asyncResult = asyncResult3;
				if (callback != null)
				{
					TaskToApm.InvokeCallbackWhenTaskCompletes(task, callback, asyncResult);
				}
			}
			return asyncResult;
		}

		// Token: 0x06000222 RID: 546 RVA: 0x0000D04C File Offset: 0x0000B24C
		public static void End(IAsyncResult asyncResult)
		{
			TaskToApm.TaskWrapperAsyncResult taskWrapperAsyncResult = asyncResult as TaskToApm.TaskWrapperAsyncResult;
			Task task;
			if (taskWrapperAsyncResult != null)
			{
				task = taskWrapperAsyncResult.Task;
			}
			else
			{
				task = (asyncResult as Task);
			}
			if (task == null)
			{
				throw new ArgumentNullException();
			}
			task.GetAwaiter().GetResult();
		}

		// Token: 0x06000223 RID: 547 RVA: 0x0000D08C File Offset: 0x0000B28C
		public static TResult End<TResult>(IAsyncResult asyncResult)
		{
			TaskToApm.TaskWrapperAsyncResult taskWrapperAsyncResult = asyncResult as TaskToApm.TaskWrapperAsyncResult;
			Task<TResult> task;
			if (taskWrapperAsyncResult != null)
			{
				task = (taskWrapperAsyncResult.Task as Task<TResult>);
			}
			else
			{
				task = (asyncResult as Task<TResult>);
			}
			if (task == null)
			{
				throw new ArgumentNullException();
			}
			return task.GetAwaiter().GetResult();
		}

		// Token: 0x06000224 RID: 548 RVA: 0x0000D0D0 File Offset: 0x0000B2D0
		private static void InvokeCallbackWhenTaskCompletes(Task antecedent, AsyncCallback callback, IAsyncResult asyncResult)
		{
			antecedent.ConfigureAwait(false).GetAwaiter().OnCompleted(delegate
			{
				callback(asyncResult);
			});
		}

		// Token: 0x02000037 RID: 55
		private sealed class TaskWrapperAsyncResult : IAsyncResult
		{
			// Token: 0x06000225 RID: 549 RVA: 0x0000D114 File Offset: 0x0000B314
			internal TaskWrapperAsyncResult(Task task, object state, bool completedSynchronously)
			{
				this.Task = task;
				this._state = state;
				this._completedSynchronously = completedSynchronously;
			}

			// Token: 0x17000091 RID: 145
			// (get) Token: 0x06000226 RID: 550 RVA: 0x0000D131 File Offset: 0x0000B331
			object IAsyncResult.AsyncState
			{
				get
				{
					return this._state;
				}
			}

			// Token: 0x17000092 RID: 146
			// (get) Token: 0x06000227 RID: 551 RVA: 0x0000D139 File Offset: 0x0000B339
			bool IAsyncResult.CompletedSynchronously
			{
				get
				{
					return this._completedSynchronously;
				}
			}

			// Token: 0x17000093 RID: 147
			// (get) Token: 0x06000228 RID: 552 RVA: 0x0000D141 File Offset: 0x0000B341
			bool IAsyncResult.IsCompleted
			{
				get
				{
					return this.Task.IsCompleted;
				}
			}

			// Token: 0x17000094 RID: 148
			// (get) Token: 0x06000229 RID: 553 RVA: 0x0000D14E File Offset: 0x0000B34E
			WaitHandle IAsyncResult.AsyncWaitHandle
			{
				get
				{
					return ((IAsyncResult)this.Task).AsyncWaitHandle;
				}
			}

			// Token: 0x04000446 RID: 1094
			internal readonly Task Task;

			// Token: 0x04000447 RID: 1095
			private readonly object _state;

			// Token: 0x04000448 RID: 1096
			private readonly bool _completedSynchronously;
		}

		// Token: 0x02000038 RID: 56
		[CompilerGenerated]
		private sealed class <>c__DisplayClass3_0
		{
			// Token: 0x0600022A RID: 554 RVA: 0x00005C14 File Offset: 0x00003E14
			public <>c__DisplayClass3_0()
			{
			}

			// Token: 0x0600022B RID: 555 RVA: 0x0000D15B File Offset: 0x0000B35B
			internal void <InvokeCallbackWhenTaskCompletes>b__0()
			{
				this.callback(this.asyncResult);
			}

			// Token: 0x04000449 RID: 1097
			public AsyncCallback callback;

			// Token: 0x0400044A RID: 1098
			public IAsyncResult asyncResult;
		}
	}
}
