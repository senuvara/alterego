﻿using System;

namespace System.Net
{
	// Token: 0x0200003B RID: 59
	[Flags]
	internal enum ContextFlagsPal
	{
		// Token: 0x0400044F RID: 1103
		None = 0,
		// Token: 0x04000450 RID: 1104
		Delegate = 1,
		// Token: 0x04000451 RID: 1105
		MutualAuth = 2,
		// Token: 0x04000452 RID: 1106
		ReplayDetect = 4,
		// Token: 0x04000453 RID: 1107
		SequenceDetect = 8,
		// Token: 0x04000454 RID: 1108
		Confidentiality = 16,
		// Token: 0x04000455 RID: 1109
		UseSessionKey = 32,
		// Token: 0x04000456 RID: 1110
		AllocateMemory = 256,
		// Token: 0x04000457 RID: 1111
		Connection = 2048,
		// Token: 0x04000458 RID: 1112
		InitExtendedError = 16384,
		// Token: 0x04000459 RID: 1113
		AcceptExtendedError = 32768,
		// Token: 0x0400045A RID: 1114
		InitStream = 32768,
		// Token: 0x0400045B RID: 1115
		AcceptStream = 65536,
		// Token: 0x0400045C RID: 1116
		InitIntegrity = 65536,
		// Token: 0x0400045D RID: 1117
		AcceptIntegrity = 131072,
		// Token: 0x0400045E RID: 1118
		InitManualCredValidation = 524288,
		// Token: 0x0400045F RID: 1119
		InitUseSuppliedCreds = 128,
		// Token: 0x04000460 RID: 1120
		InitIdentify = 131072,
		// Token: 0x04000461 RID: 1121
		AcceptIdentify = 524288,
		// Token: 0x04000462 RID: 1122
		ProxyBindings = 67108864,
		// Token: 0x04000463 RID: 1123
		AllowMissingBindings = 268435456,
		// Token: 0x04000464 RID: 1124
		UnverifiedTargetName = 536870912
	}
}
