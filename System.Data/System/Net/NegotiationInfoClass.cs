﻿using System;

namespace System.Net
{
	// Token: 0x02000042 RID: 66
	internal class NegotiationInfoClass
	{
		// Token: 0x06000260 RID: 608 RVA: 0x00005C14 File Offset: 0x00003E14
		public NegotiationInfoClass()
		{
		}

		// Token: 0x04000488 RID: 1160
		internal const string NTLM = "NTLM";

		// Token: 0x04000489 RID: 1161
		internal const string Kerberos = "Kerberos";

		// Token: 0x0400048A RID: 1162
		internal const string Negotiate = "Negotiate";

		// Token: 0x0400048B RID: 1163
		internal const string Basic = "Basic";
	}
}
