﻿using System;

namespace System.Net
{
	// Token: 0x0200003C RID: 60
	internal class InternalException : Exception
	{
		// Token: 0x06000230 RID: 560 RVA: 0x0000D2B0 File Offset: 0x0000B4B0
		internal InternalException()
		{
			NetEventSource.Fail(this, "InternalException thrown.", ".ctor");
		}
	}
}
