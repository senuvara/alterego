﻿using System;

namespace System.Net
{
	// Token: 0x02000043 RID: 67
	internal struct SecurityStatusPal
	{
		// Token: 0x06000261 RID: 609 RVA: 0x0000E229 File Offset: 0x0000C429
		public SecurityStatusPal(SecurityStatusPalErrorCode errorCode, Exception exception = null)
		{
			this.ErrorCode = errorCode;
			this.Exception = exception;
		}

		// Token: 0x06000262 RID: 610 RVA: 0x0000E23C File Offset: 0x0000C43C
		public override string ToString()
		{
			if (this.Exception != null)
			{
				return string.Format("{0}={1}, {2}={3}", new object[]
				{
					"ErrorCode",
					this.ErrorCode,
					"Exception",
					this.Exception
				});
			}
			return string.Format("{0}={1}", "ErrorCode", this.ErrorCode);
		}

		// Token: 0x0400048C RID: 1164
		public readonly SecurityStatusPalErrorCode ErrorCode;

		// Token: 0x0400048D RID: 1165
		public readonly Exception Exception;
	}
}
