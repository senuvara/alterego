﻿using System;
using Microsoft.Win32.SafeHandles;

namespace System.Net.Security
{
	// Token: 0x02000049 RID: 73
	internal sealed class SafeDeleteNegoContext : SafeDeleteContext
	{
		// Token: 0x17000099 RID: 153
		// (get) Token: 0x0600027B RID: 635 RVA: 0x0000EA6F File Offset: 0x0000CC6F
		public SafeGssNameHandle TargetName
		{
			get
			{
				return this._targetName;
			}
		}

		// Token: 0x1700009A RID: 154
		// (get) Token: 0x0600027C RID: 636 RVA: 0x0000EA77 File Offset: 0x0000CC77
		public bool IsNtlmUsed
		{
			get
			{
				return this._isNtlmUsed;
			}
		}

		// Token: 0x1700009B RID: 155
		// (get) Token: 0x0600027D RID: 637 RVA: 0x0000EA7F File Offset: 0x0000CC7F
		public SafeGssContextHandle GssContext
		{
			get
			{
				return this._context;
			}
		}

		// Token: 0x0600027E RID: 638 RVA: 0x0000EA88 File Offset: 0x0000CC88
		public SafeDeleteNegoContext(SafeFreeNegoCredentials credential, string targetName) : base(credential)
		{
			try
			{
				this._targetName = SafeGssNameHandle.CreatePrincipal(targetName);
			}
			catch
			{
				base.Dispose();
				throw;
			}
		}

		// Token: 0x0600027F RID: 639 RVA: 0x0000EAC4 File Offset: 0x0000CCC4
		public void SetGssContext(SafeGssContextHandle context)
		{
			this._context = context;
		}

		// Token: 0x06000280 RID: 640 RVA: 0x0000EACD File Offset: 0x0000CCCD
		public void SetAuthenticationPackage(bool isNtlmUsed)
		{
			this._isNtlmUsed = isNtlmUsed;
		}

		// Token: 0x06000281 RID: 641 RVA: 0x0000EAD6 File Offset: 0x0000CCD6
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (this._context != null)
				{
					this._context.Dispose();
					this._context = null;
				}
				if (this._targetName != null)
				{
					this._targetName.Dispose();
					this._targetName = null;
				}
			}
			base.Dispose(disposing);
		}

		// Token: 0x040004D0 RID: 1232
		private SafeGssNameHandle _targetName;

		// Token: 0x040004D1 RID: 1233
		private SafeGssContextHandle _context;

		// Token: 0x040004D2 RID: 1234
		private bool _isNtlmUsed;
	}
}
