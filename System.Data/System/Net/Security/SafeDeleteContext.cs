﻿using System;
using System.Runtime.InteropServices;

namespace System.Net.Security
{
	// Token: 0x02000048 RID: 72
	internal abstract class SafeDeleteContext : SafeHandle
	{
		// Token: 0x06000278 RID: 632 RVA: 0x0000EA20 File Offset: 0x0000CC20
		protected SafeDeleteContext(SafeFreeCredentials credential) : base(IntPtr.Zero, true)
		{
			bool flag = false;
			this._credential = credential;
			this._credential.DangerousAddRef(ref flag);
		}

		// Token: 0x17000098 RID: 152
		// (get) Token: 0x06000279 RID: 633 RVA: 0x0000EA4F File Offset: 0x0000CC4F
		public override bool IsInvalid
		{
			get
			{
				return this._credential == null;
			}
		}

		// Token: 0x0600027A RID: 634 RVA: 0x0000EA5A File Offset: 0x0000CC5A
		protected override bool ReleaseHandle()
		{
			this._credential.DangerousRelease();
			this._credential = null;
			return true;
		}

		// Token: 0x040004CF RID: 1231
		private SafeFreeCredentials _credential;
	}
}
