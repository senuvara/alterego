﻿using System;
using System.Runtime.InteropServices;

namespace System.Net.Security
{
	// Token: 0x0200004A RID: 74
	internal abstract class SafeFreeCredentials : SafeHandle
	{
		// Token: 0x06000282 RID: 642 RVA: 0x0000EB16 File Offset: 0x0000CD16
		protected SafeFreeCredentials(IntPtr handle, bool ownsHandle) : base(handle, ownsHandle)
		{
		}
	}
}
