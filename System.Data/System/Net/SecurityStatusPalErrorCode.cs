﻿using System;

namespace System.Net
{
	// Token: 0x02000044 RID: 68
	internal enum SecurityStatusPalErrorCode
	{
		// Token: 0x0400048F RID: 1167
		NotSet,
		// Token: 0x04000490 RID: 1168
		OK,
		// Token: 0x04000491 RID: 1169
		ContinueNeeded,
		// Token: 0x04000492 RID: 1170
		CompleteNeeded,
		// Token: 0x04000493 RID: 1171
		CompAndContinue,
		// Token: 0x04000494 RID: 1172
		ContextExpired,
		// Token: 0x04000495 RID: 1173
		CredentialsNeeded,
		// Token: 0x04000496 RID: 1174
		Renegotiate,
		// Token: 0x04000497 RID: 1175
		OutOfMemory,
		// Token: 0x04000498 RID: 1176
		InvalidHandle,
		// Token: 0x04000499 RID: 1177
		Unsupported,
		// Token: 0x0400049A RID: 1178
		TargetUnknown,
		// Token: 0x0400049B RID: 1179
		InternalError,
		// Token: 0x0400049C RID: 1180
		PackageNotFound,
		// Token: 0x0400049D RID: 1181
		NotOwner,
		// Token: 0x0400049E RID: 1182
		CannotInstall,
		// Token: 0x0400049F RID: 1183
		InvalidToken,
		// Token: 0x040004A0 RID: 1184
		CannotPack,
		// Token: 0x040004A1 RID: 1185
		QopNotSupported,
		// Token: 0x040004A2 RID: 1186
		NoImpersonation,
		// Token: 0x040004A3 RID: 1187
		LogonDenied,
		// Token: 0x040004A4 RID: 1188
		UnknownCredentials,
		// Token: 0x040004A5 RID: 1189
		NoCredentials,
		// Token: 0x040004A6 RID: 1190
		MessageAltered,
		// Token: 0x040004A7 RID: 1191
		OutOfSequence,
		// Token: 0x040004A8 RID: 1192
		NoAuthenticatingAuthority,
		// Token: 0x040004A9 RID: 1193
		IncompleteMessage,
		// Token: 0x040004AA RID: 1194
		IncompleteCredentials,
		// Token: 0x040004AB RID: 1195
		BufferNotEnough,
		// Token: 0x040004AC RID: 1196
		WrongPrincipal,
		// Token: 0x040004AD RID: 1197
		TimeSkew,
		// Token: 0x040004AE RID: 1198
		UntrustedRoot,
		// Token: 0x040004AF RID: 1199
		IllegalMessage,
		// Token: 0x040004B0 RID: 1200
		CertUnknown,
		// Token: 0x040004B1 RID: 1201
		CertExpired,
		// Token: 0x040004B2 RID: 1202
		AlgorithmMismatch,
		// Token: 0x040004B3 RID: 1203
		SecurityQosFailed,
		// Token: 0x040004B4 RID: 1204
		SmartcardLogonRequired,
		// Token: 0x040004B5 RID: 1205
		UnsupportedPreauth,
		// Token: 0x040004B6 RID: 1206
		BadBinding,
		// Token: 0x040004B7 RID: 1207
		DowngradeDetected
	}
}
