﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;

// Token: 0x02000009 RID: 9
[CompilerGenerated]
internal sealed class <>f__AnonymousType7<<OperationId>j__TPar, <Operation>j__TPar, <IsolationLevel>j__TPar, <Connection>j__TPar, <Timestamp>j__TPar>
{
	// Token: 0x17000028 RID: 40
	// (get) Token: 0x06000044 RID: 68 RVA: 0x00003593 File Offset: 0x00001793
	public <OperationId>j__TPar OperationId
	{
		get
		{
			return this.<OperationId>i__Field;
		}
	}

	// Token: 0x17000029 RID: 41
	// (get) Token: 0x06000045 RID: 69 RVA: 0x0000359B File Offset: 0x0000179B
	public <Operation>j__TPar Operation
	{
		get
		{
			return this.<Operation>i__Field;
		}
	}

	// Token: 0x1700002A RID: 42
	// (get) Token: 0x06000046 RID: 70 RVA: 0x000035A3 File Offset: 0x000017A3
	public <IsolationLevel>j__TPar IsolationLevel
	{
		get
		{
			return this.<IsolationLevel>i__Field;
		}
	}

	// Token: 0x1700002B RID: 43
	// (get) Token: 0x06000047 RID: 71 RVA: 0x000035AB File Offset: 0x000017AB
	public <Connection>j__TPar Connection
	{
		get
		{
			return this.<Connection>i__Field;
		}
	}

	// Token: 0x1700002C RID: 44
	// (get) Token: 0x06000048 RID: 72 RVA: 0x000035B3 File Offset: 0x000017B3
	public <Timestamp>j__TPar Timestamp
	{
		get
		{
			return this.<Timestamp>i__Field;
		}
	}

	// Token: 0x06000049 RID: 73 RVA: 0x000035BB File Offset: 0x000017BB
	[DebuggerHidden]
	public <>f__AnonymousType7(<OperationId>j__TPar OperationId, <Operation>j__TPar Operation, <IsolationLevel>j__TPar IsolationLevel, <Connection>j__TPar Connection, <Timestamp>j__TPar Timestamp)
	{
		this.<OperationId>i__Field = OperationId;
		this.<Operation>i__Field = Operation;
		this.<IsolationLevel>i__Field = IsolationLevel;
		this.<Connection>i__Field = Connection;
		this.<Timestamp>i__Field = Timestamp;
	}

	// Token: 0x0600004A RID: 74 RVA: 0x000035E8 File Offset: 0x000017E8
	[DebuggerHidden]
	public override bool Equals(object value)
	{
		var <>f__AnonymousType = value as <>f__AnonymousType7<<OperationId>j__TPar, <Operation>j__TPar, <IsolationLevel>j__TPar, <Connection>j__TPar, <Timestamp>j__TPar>;
		return <>f__AnonymousType != null && EqualityComparer<<OperationId>j__TPar>.Default.Equals(this.<OperationId>i__Field, <>f__AnonymousType.<OperationId>i__Field) && EqualityComparer<<Operation>j__TPar>.Default.Equals(this.<Operation>i__Field, <>f__AnonymousType.<Operation>i__Field) && EqualityComparer<<IsolationLevel>j__TPar>.Default.Equals(this.<IsolationLevel>i__Field, <>f__AnonymousType.<IsolationLevel>i__Field) && EqualityComparer<<Connection>j__TPar>.Default.Equals(this.<Connection>i__Field, <>f__AnonymousType.<Connection>i__Field) && EqualityComparer<<Timestamp>j__TPar>.Default.Equals(this.<Timestamp>i__Field, <>f__AnonymousType.<Timestamp>i__Field);
	}

	// Token: 0x0600004B RID: 75 RVA: 0x00003678 File Offset: 0x00001878
	[DebuggerHidden]
	public override int GetHashCode()
	{
		return ((((1883534822 * -1521134295 + EqualityComparer<<OperationId>j__TPar>.Default.GetHashCode(this.<OperationId>i__Field)) * -1521134295 + EqualityComparer<<Operation>j__TPar>.Default.GetHashCode(this.<Operation>i__Field)) * -1521134295 + EqualityComparer<<IsolationLevel>j__TPar>.Default.GetHashCode(this.<IsolationLevel>i__Field)) * -1521134295 + EqualityComparer<<Connection>j__TPar>.Default.GetHashCode(this.<Connection>i__Field)) * -1521134295 + EqualityComparer<<Timestamp>j__TPar>.Default.GetHashCode(this.<Timestamp>i__Field);
	}

	// Token: 0x0600004C RID: 76 RVA: 0x00003700 File Offset: 0x00001900
	[DebuggerHidden]
	public override string ToString()
	{
		IFormatProvider provider = null;
		string format = "{{ OperationId = {0}, Operation = {1}, IsolationLevel = {2}, Connection = {3}, Timestamp = {4} }}";
		object[] array = new object[5];
		int num = 0;
		<OperationId>j__TPar <OperationId>j__TPar = this.<OperationId>i__Field;
		ref <OperationId>j__TPar ptr = ref <OperationId>j__TPar;
		<OperationId>j__TPar <OperationId>j__TPar2 = default(<OperationId>j__TPar);
		object obj;
		if (<OperationId>j__TPar2 == null)
		{
			<OperationId>j__TPar2 = <OperationId>j__TPar;
			ptr = ref <OperationId>j__TPar2;
			if (<OperationId>j__TPar2 == null)
			{
				obj = null;
				goto IL_46;
			}
		}
		obj = ptr.ToString();
		IL_46:
		array[num] = obj;
		int num2 = 1;
		<Operation>j__TPar <Operation>j__TPar = this.<Operation>i__Field;
		ref <Operation>j__TPar ptr2 = ref <Operation>j__TPar;
		<Operation>j__TPar <Operation>j__TPar2 = default(<Operation>j__TPar);
		object obj2;
		if (<Operation>j__TPar2 == null)
		{
			<Operation>j__TPar2 = <Operation>j__TPar;
			ptr2 = ref <Operation>j__TPar2;
			if (<Operation>j__TPar2 == null)
			{
				obj2 = null;
				goto IL_81;
			}
		}
		obj2 = ptr2.ToString();
		IL_81:
		array[num2] = obj2;
		int num3 = 2;
		<IsolationLevel>j__TPar <IsolationLevel>j__TPar = this.<IsolationLevel>i__Field;
		ref <IsolationLevel>j__TPar ptr3 = ref <IsolationLevel>j__TPar;
		<IsolationLevel>j__TPar <IsolationLevel>j__TPar2 = default(<IsolationLevel>j__TPar);
		object obj3;
		if (<IsolationLevel>j__TPar2 == null)
		{
			<IsolationLevel>j__TPar2 = <IsolationLevel>j__TPar;
			ptr3 = ref <IsolationLevel>j__TPar2;
			if (<IsolationLevel>j__TPar2 == null)
			{
				obj3 = null;
				goto IL_C0;
			}
		}
		obj3 = ptr3.ToString();
		IL_C0:
		array[num3] = obj3;
		int num4 = 3;
		<Connection>j__TPar <Connection>j__TPar = this.<Connection>i__Field;
		ref <Connection>j__TPar ptr4 = ref <Connection>j__TPar;
		<Connection>j__TPar <Connection>j__TPar2 = default(<Connection>j__TPar);
		object obj4;
		if (<Connection>j__TPar2 == null)
		{
			<Connection>j__TPar2 = <Connection>j__TPar;
			ptr4 = ref <Connection>j__TPar2;
			if (<Connection>j__TPar2 == null)
			{
				obj4 = null;
				goto IL_FF;
			}
		}
		obj4 = ptr4.ToString();
		IL_FF:
		array[num4] = obj4;
		int num5 = 4;
		<Timestamp>j__TPar <Timestamp>j__TPar = this.<Timestamp>i__Field;
		ref <Timestamp>j__TPar ptr5 = ref <Timestamp>j__TPar;
		<Timestamp>j__TPar <Timestamp>j__TPar2 = default(<Timestamp>j__TPar);
		object obj5;
		if (<Timestamp>j__TPar2 == null)
		{
			<Timestamp>j__TPar2 = <Timestamp>j__TPar;
			ptr5 = ref <Timestamp>j__TPar2;
			if (<Timestamp>j__TPar2 == null)
			{
				obj5 = null;
				goto IL_13E;
			}
		}
		obj5 = ptr5.ToString();
		IL_13E:
		array[num5] = obj5;
		return string.Format(provider, format, array);
	}

	// Token: 0x04000028 RID: 40
	[DebuggerBrowsable(DebuggerBrowsableState.Never)]
	private readonly <OperationId>j__TPar <OperationId>i__Field;

	// Token: 0x04000029 RID: 41
	[DebuggerBrowsable(DebuggerBrowsableState.Never)]
	private readonly <Operation>j__TPar <Operation>i__Field;

	// Token: 0x0400002A RID: 42
	[DebuggerBrowsable(DebuggerBrowsableState.Never)]
	private readonly <IsolationLevel>j__TPar <IsolationLevel>i__Field;

	// Token: 0x0400002B RID: 43
	[DebuggerBrowsable(DebuggerBrowsableState.Never)]
	private readonly <Connection>j__TPar <Connection>i__Field;

	// Token: 0x0400002C RID: 44
	[DebuggerBrowsable(DebuggerBrowsableState.Never)]
	private readonly <Timestamp>j__TPar <Timestamp>i__Field;
}
