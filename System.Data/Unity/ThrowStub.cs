﻿using System;

namespace Unity
{
	// Token: 0x0200038E RID: 910
	internal sealed class ThrowStub : ObjectDisposedException
	{
		// Token: 0x06002B5D RID: 11101 RVA: 0x0003BAB5 File Offset: 0x00039CB5
		public static void ThrowNotSupportedException()
		{
			throw new PlatformNotSupportedException();
		}
	}
}
