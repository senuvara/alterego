﻿using System;

// Token: 0x0200001F RID: 31
internal static class ThisAssembly
{
	// Token: 0x040003F6 RID: 1014
	public const string InformationalVersion = "4.0.30319.17020";
}
