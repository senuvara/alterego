﻿using System;
using System.Runtime.InteropServices;
using System.Text;

namespace Microsoft.Win32.SafeHandles
{
	// Token: 0x02000329 RID: 809
	internal class SafeGssCredHandle : SafeHandle
	{
		// Token: 0x06002938 RID: 10552 RVA: 0x000B6A98 File Offset: 0x000B4C98
		public static SafeGssCredHandle Create(string username, string password, bool isNtlmOnly)
		{
			if (string.IsNullOrEmpty(username))
			{
				return new SafeGssCredHandle();
			}
			SafeGssCredHandle safeGssCredHandle = null;
			using (SafeGssNameHandle safeGssNameHandle = SafeGssNameHandle.CreateUser(username))
			{
				Interop.NetSecurityNative.Status minorStatus;
				Interop.NetSecurityNative.Status status;
				if (string.IsNullOrEmpty(password))
				{
					status = Interop.NetSecurityNative.InitiateCredSpNego(out minorStatus, safeGssNameHandle, out safeGssCredHandle);
				}
				else
				{
					status = Interop.NetSecurityNative.InitiateCredWithPassword(out minorStatus, isNtlmOnly, safeGssNameHandle, password, Encoding.UTF8.GetByteCount(password), out safeGssCredHandle);
				}
				if (status != Interop.NetSecurityNative.Status.GSS_S_COMPLETE)
				{
					safeGssCredHandle.Dispose();
					throw new Interop.NetSecurityNative.GssApiException(status, minorStatus);
				}
			}
			return safeGssCredHandle;
		}

		// Token: 0x06002939 RID: 10553 RVA: 0x000B6A88 File Offset: 0x000B4C88
		private SafeGssCredHandle() : base(IntPtr.Zero, true)
		{
		}

		// Token: 0x170006B8 RID: 1720
		// (get) Token: 0x0600293A RID: 10554 RVA: 0x000B6A4D File Offset: 0x000B4C4D
		public override bool IsInvalid
		{
			get
			{
				return this.handle == IntPtr.Zero;
			}
		}

		// Token: 0x0600293B RID: 10555 RVA: 0x000B6B18 File Offset: 0x000B4D18
		protected override bool ReleaseHandle()
		{
			Interop.NetSecurityNative.Status status;
			int num = (int)Interop.NetSecurityNative.ReleaseCred(out status, ref this.handle);
			base.SetHandle(IntPtr.Zero);
			return num == 0;
		}
	}
}
