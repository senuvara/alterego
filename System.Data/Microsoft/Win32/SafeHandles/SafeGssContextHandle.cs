﻿using System;
using System.Runtime.InteropServices;

namespace Microsoft.Win32.SafeHandles
{
	// Token: 0x0200032A RID: 810
	internal sealed class SafeGssContextHandle : SafeHandle
	{
		// Token: 0x0600293C RID: 10556 RVA: 0x000B6A88 File Offset: 0x000B4C88
		public SafeGssContextHandle() : base(IntPtr.Zero, true)
		{
		}

		// Token: 0x170006B9 RID: 1721
		// (get) Token: 0x0600293D RID: 10557 RVA: 0x000B6A4D File Offset: 0x000B4C4D
		public override bool IsInvalid
		{
			get
			{
				return this.handle == IntPtr.Zero;
			}
		}

		// Token: 0x0600293E RID: 10558 RVA: 0x000B6B40 File Offset: 0x000B4D40
		protected override bool ReleaseHandle()
		{
			Interop.NetSecurityNative.Status status;
			int num = (int)Interop.NetSecurityNative.DeleteSecContext(out status, ref this.handle);
			base.SetHandle(IntPtr.Zero);
			return num == 0;
		}
	}
}
