﻿using System;
using System.Runtime.InteropServices;
using System.Text;

namespace Microsoft.Win32.SafeHandles
{
	// Token: 0x02000328 RID: 808
	internal sealed class SafeGssNameHandle : SafeHandle
	{
		// Token: 0x06002933 RID: 10547 RVA: 0x000B69E0 File Offset: 0x000B4BE0
		public static SafeGssNameHandle CreateUser(string name)
		{
			Interop.NetSecurityNative.Status minorStatus;
			SafeGssNameHandle safeGssNameHandle;
			Interop.NetSecurityNative.Status status = Interop.NetSecurityNative.ImportUserName(out minorStatus, name, Encoding.UTF8.GetByteCount(name), out safeGssNameHandle);
			if (status != Interop.NetSecurityNative.Status.GSS_S_COMPLETE)
			{
				safeGssNameHandle.Dispose();
				throw new Interop.NetSecurityNative.GssApiException(status, minorStatus);
			}
			return safeGssNameHandle;
		}

		// Token: 0x06002934 RID: 10548 RVA: 0x000B6A18 File Offset: 0x000B4C18
		public static SafeGssNameHandle CreatePrincipal(string name)
		{
			Interop.NetSecurityNative.Status minorStatus;
			SafeGssNameHandle safeGssNameHandle;
			Interop.NetSecurityNative.Status status = Interop.NetSecurityNative.ImportPrincipalName(out minorStatus, name, Encoding.UTF8.GetByteCount(name), out safeGssNameHandle);
			if (status != Interop.NetSecurityNative.Status.GSS_S_COMPLETE)
			{
				safeGssNameHandle.Dispose();
				throw new Interop.NetSecurityNative.GssApiException(status, minorStatus);
			}
			return safeGssNameHandle;
		}

		// Token: 0x170006B7 RID: 1719
		// (get) Token: 0x06002935 RID: 10549 RVA: 0x000B6A4D File Offset: 0x000B4C4D
		public override bool IsInvalid
		{
			get
			{
				return this.handle == IntPtr.Zero;
			}
		}

		// Token: 0x06002936 RID: 10550 RVA: 0x000B6A60 File Offset: 0x000B4C60
		protected override bool ReleaseHandle()
		{
			Interop.NetSecurityNative.Status status;
			int num = (int)Interop.NetSecurityNative.ReleaseName(out status, ref this.handle);
			base.SetHandle(IntPtr.Zero);
			return num == 0;
		}

		// Token: 0x06002937 RID: 10551 RVA: 0x000B6A88 File Offset: 0x000B4C88
		private SafeGssNameHandle() : base(IntPtr.Zero, true)
		{
		}
	}
}
