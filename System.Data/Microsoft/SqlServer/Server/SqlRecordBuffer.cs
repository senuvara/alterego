﻿using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlTypes;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;

namespace Microsoft.SqlServer.Server
{
	// Token: 0x02000313 RID: 787
	internal sealed class SqlRecordBuffer
	{
		// Token: 0x060027B3 RID: 10163 RVA: 0x000AFD4F File Offset: 0x000ADF4F
		internal SqlRecordBuffer(SmiMetaData metaData)
		{
			this._isNull = true;
		}

		// Token: 0x17000670 RID: 1648
		// (get) Token: 0x060027B4 RID: 10164 RVA: 0x000AFD5E File Offset: 0x000ADF5E
		internal bool IsNull
		{
			get
			{
				return this._isNull;
			}
		}

		// Token: 0x17000671 RID: 1649
		// (get) Token: 0x060027B5 RID: 10165 RVA: 0x000AFD66 File Offset: 0x000ADF66
		// (set) Token: 0x060027B6 RID: 10166 RVA: 0x000AFD73 File Offset: 0x000ADF73
		internal bool Boolean
		{
			get
			{
				return this._value._boolean;
			}
			set
			{
				this._value._boolean = value;
				this._type = SqlRecordBuffer.StorageType.Boolean;
				this._isNull = false;
			}
		}

		// Token: 0x17000672 RID: 1650
		// (get) Token: 0x060027B7 RID: 10167 RVA: 0x000AFD8F File Offset: 0x000ADF8F
		// (set) Token: 0x060027B8 RID: 10168 RVA: 0x000AFD9C File Offset: 0x000ADF9C
		internal byte Byte
		{
			get
			{
				return this._value._byte;
			}
			set
			{
				this._value._byte = value;
				this._type = SqlRecordBuffer.StorageType.Byte;
				this._isNull = false;
			}
		}

		// Token: 0x17000673 RID: 1651
		// (get) Token: 0x060027B9 RID: 10169 RVA: 0x000AFDB8 File Offset: 0x000ADFB8
		// (set) Token: 0x060027BA RID: 10170 RVA: 0x000AFDC5 File Offset: 0x000ADFC5
		internal DateTime DateTime
		{
			get
			{
				return this._value._dateTime;
			}
			set
			{
				this._value._dateTime = value;
				this._type = SqlRecordBuffer.StorageType.DateTime;
				this._isNull = false;
				if (this._isMetaSet)
				{
					this._isMetaSet = false;
					return;
				}
				this._metadata = null;
			}
		}

		// Token: 0x17000674 RID: 1652
		// (get) Token: 0x060027BB RID: 10171 RVA: 0x000AFDF8 File Offset: 0x000ADFF8
		// (set) Token: 0x060027BC RID: 10172 RVA: 0x000AFE05 File Offset: 0x000AE005
		internal DateTimeOffset DateTimeOffset
		{
			get
			{
				return this._value._dateTimeOffset;
			}
			set
			{
				this._value._dateTimeOffset = value;
				this._type = SqlRecordBuffer.StorageType.DateTimeOffset;
				this._isNull = false;
			}
		}

		// Token: 0x17000675 RID: 1653
		// (get) Token: 0x060027BD RID: 10173 RVA: 0x000AFE21 File Offset: 0x000AE021
		// (set) Token: 0x060027BE RID: 10174 RVA: 0x000AFE2E File Offset: 0x000AE02E
		internal double Double
		{
			get
			{
				return this._value._double;
			}
			set
			{
				this._value._double = value;
				this._type = SqlRecordBuffer.StorageType.Double;
				this._isNull = false;
			}
		}

		// Token: 0x17000676 RID: 1654
		// (get) Token: 0x060027BF RID: 10175 RVA: 0x000AFE4A File Offset: 0x000AE04A
		// (set) Token: 0x060027C0 RID: 10176 RVA: 0x000AFE57 File Offset: 0x000AE057
		internal Guid Guid
		{
			get
			{
				return this._value._guid;
			}
			set
			{
				this._value._guid = value;
				this._type = SqlRecordBuffer.StorageType.Guid;
				this._isNull = false;
			}
		}

		// Token: 0x17000677 RID: 1655
		// (get) Token: 0x060027C1 RID: 10177 RVA: 0x000AFE73 File Offset: 0x000AE073
		// (set) Token: 0x060027C2 RID: 10178 RVA: 0x000AFE80 File Offset: 0x000AE080
		internal short Int16
		{
			get
			{
				return this._value._int16;
			}
			set
			{
				this._value._int16 = value;
				this._type = SqlRecordBuffer.StorageType.Int16;
				this._isNull = false;
			}
		}

		// Token: 0x17000678 RID: 1656
		// (get) Token: 0x060027C3 RID: 10179 RVA: 0x000AFE9C File Offset: 0x000AE09C
		// (set) Token: 0x060027C4 RID: 10180 RVA: 0x000AFEA9 File Offset: 0x000AE0A9
		internal int Int32
		{
			get
			{
				return this._value._int32;
			}
			set
			{
				this._value._int32 = value;
				this._type = SqlRecordBuffer.StorageType.Int32;
				this._isNull = false;
			}
		}

		// Token: 0x17000679 RID: 1657
		// (get) Token: 0x060027C5 RID: 10181 RVA: 0x000AFEC6 File Offset: 0x000AE0C6
		// (set) Token: 0x060027C6 RID: 10182 RVA: 0x000AFED3 File Offset: 0x000AE0D3
		internal long Int64
		{
			get
			{
				return this._value._int64;
			}
			set
			{
				this._value._int64 = value;
				this._type = SqlRecordBuffer.StorageType.Int64;
				this._isNull = false;
				if (this._isMetaSet)
				{
					this._isMetaSet = false;
					return;
				}
				this._metadata = null;
			}
		}

		// Token: 0x1700067A RID: 1658
		// (get) Token: 0x060027C7 RID: 10183 RVA: 0x000AFF07 File Offset: 0x000AE107
		// (set) Token: 0x060027C8 RID: 10184 RVA: 0x000AFF14 File Offset: 0x000AE114
		internal float Single
		{
			get
			{
				return this._value._single;
			}
			set
			{
				this._value._single = value;
				this._type = SqlRecordBuffer.StorageType.Single;
				this._isNull = false;
			}
		}

		// Token: 0x1700067B RID: 1659
		// (get) Token: 0x060027C9 RID: 10185 RVA: 0x000AFF34 File Offset: 0x000AE134
		// (set) Token: 0x060027CA RID: 10186 RVA: 0x000AFF94 File Offset: 0x000AE194
		internal string String
		{
			get
			{
				if (SqlRecordBuffer.StorageType.String == this._type)
				{
					return (string)this._object;
				}
				if (SqlRecordBuffer.StorageType.CharArray == this._type)
				{
					return new string((char[])this._object, 0, (int)this.CharsLength);
				}
				return new SqlXml(new MemoryStream((byte[])this._object, false)).Value;
			}
			set
			{
				this._object = value;
				this._value._int64 = (long)value.Length;
				this._type = SqlRecordBuffer.StorageType.String;
				this._isNull = false;
				if (this._isMetaSet)
				{
					this._isMetaSet = false;
					return;
				}
				this._metadata = null;
			}
		}

		// Token: 0x1700067C RID: 1660
		// (get) Token: 0x060027CB RID: 10187 RVA: 0x000AFFE0 File Offset: 0x000AE1E0
		// (set) Token: 0x060027CC RID: 10188 RVA: 0x000AFFED File Offset: 0x000AE1ED
		internal SqlDecimal SqlDecimal
		{
			get
			{
				return (SqlDecimal)this._object;
			}
			set
			{
				this._object = value;
				this._type = SqlRecordBuffer.StorageType.SqlDecimal;
				this._isNull = false;
			}
		}

		// Token: 0x1700067D RID: 1661
		// (get) Token: 0x060027CD RID: 10189 RVA: 0x000B000A File Offset: 0x000AE20A
		// (set) Token: 0x060027CE RID: 10190 RVA: 0x000B0017 File Offset: 0x000AE217
		internal TimeSpan TimeSpan
		{
			get
			{
				return this._value._timeSpan;
			}
			set
			{
				this._value._timeSpan = value;
				this._type = SqlRecordBuffer.StorageType.TimeSpan;
				this._isNull = false;
			}
		}

		// Token: 0x1700067E RID: 1662
		// (get) Token: 0x060027CF RID: 10191 RVA: 0x000B0034 File Offset: 0x000AE234
		// (set) Token: 0x060027D0 RID: 10192 RVA: 0x000B0051 File Offset: 0x000AE251
		internal long BytesLength
		{
			get
			{
				if (SqlRecordBuffer.StorageType.String == this._type)
				{
					this.ConvertXmlStringToByteArray();
				}
				return this._value._int64;
			}
			set
			{
				if (value == 0L)
				{
					this._value._int64 = value;
					this._object = Array.Empty<byte>();
					this._type = SqlRecordBuffer.StorageType.ByteArray;
					this._isNull = false;
					return;
				}
				this._value._int64 = value;
			}
		}

		// Token: 0x1700067F RID: 1663
		// (get) Token: 0x060027D1 RID: 10193 RVA: 0x000AFEC6 File Offset: 0x000AE0C6
		// (set) Token: 0x060027D2 RID: 10194 RVA: 0x000B0088 File Offset: 0x000AE288
		internal long CharsLength
		{
			get
			{
				return this._value._int64;
			}
			set
			{
				if (value == 0L)
				{
					this._value._int64 = value;
					this._object = Array.Empty<char>();
					this._type = SqlRecordBuffer.StorageType.CharArray;
					this._isNull = false;
					return;
				}
				this._value._int64 = value;
			}
		}

		// Token: 0x17000680 RID: 1664
		// (get) Token: 0x060027D3 RID: 10195 RVA: 0x000B00C0 File Offset: 0x000AE2C0
		// (set) Token: 0x060027D4 RID: 10196 RVA: 0x000B01C1 File Offset: 0x000AE3C1
		internal SmiMetaData VariantType
		{
			get
			{
				switch (this._type)
				{
				case SqlRecordBuffer.StorageType.Boolean:
					return SmiMetaData.DefaultBit;
				case SqlRecordBuffer.StorageType.Byte:
					return SmiMetaData.DefaultTinyInt;
				case SqlRecordBuffer.StorageType.ByteArray:
					return SmiMetaData.DefaultVarBinary;
				case SqlRecordBuffer.StorageType.CharArray:
					return SmiMetaData.DefaultNVarChar;
				case SqlRecordBuffer.StorageType.DateTime:
					return this._metadata ?? SmiMetaData.DefaultDateTime;
				case SqlRecordBuffer.StorageType.DateTimeOffset:
					return SmiMetaData.DefaultDateTimeOffset;
				case SqlRecordBuffer.StorageType.Double:
					return SmiMetaData.DefaultFloat;
				case SqlRecordBuffer.StorageType.Guid:
					return SmiMetaData.DefaultUniqueIdentifier;
				case SqlRecordBuffer.StorageType.Int16:
					return SmiMetaData.DefaultSmallInt;
				case SqlRecordBuffer.StorageType.Int32:
					return SmiMetaData.DefaultInt;
				case SqlRecordBuffer.StorageType.Int64:
					return this._metadata ?? SmiMetaData.DefaultBigInt;
				case SqlRecordBuffer.StorageType.Single:
					return SmiMetaData.DefaultReal;
				case SqlRecordBuffer.StorageType.String:
					return this._metadata ?? SmiMetaData.DefaultNVarChar;
				case SqlRecordBuffer.StorageType.SqlDecimal:
					return new SmiMetaData(SqlDbType.Decimal, 17L, ((SqlDecimal)this._object).Precision, ((SqlDecimal)this._object).Scale, 0L, SqlCompareOptions.None);
				case SqlRecordBuffer.StorageType.TimeSpan:
					return SmiMetaData.DefaultTime;
				default:
					return null;
				}
			}
			set
			{
				this._metadata = value;
				this._isMetaSet = true;
			}
		}

		// Token: 0x060027D5 RID: 10197 RVA: 0x000B01D4 File Offset: 0x000AE3D4
		internal int GetBytes(long fieldOffset, byte[] buffer, int bufferOffset, int length)
		{
			int srcOffset = (int)fieldOffset;
			if (SqlRecordBuffer.StorageType.String == this._type)
			{
				this.ConvertXmlStringToByteArray();
			}
			Buffer.BlockCopy((byte[])this._object, srcOffset, buffer, bufferOffset, length);
			return length;
		}

		// Token: 0x060027D6 RID: 10198 RVA: 0x000B020C File Offset: 0x000AE40C
		internal int GetChars(long fieldOffset, char[] buffer, int bufferOffset, int length)
		{
			int sourceIndex = (int)fieldOffset;
			if (SqlRecordBuffer.StorageType.CharArray == this._type)
			{
				Array.Copy((char[])this._object, sourceIndex, buffer, bufferOffset, length);
			}
			else
			{
				((string)this._object).CopyTo(sourceIndex, buffer, bufferOffset, length);
			}
			return length;
		}

		// Token: 0x060027D7 RID: 10199 RVA: 0x000B0254 File Offset: 0x000AE454
		internal int SetBytes(long fieldOffset, byte[] buffer, int bufferOffset, int length)
		{
			int num = (int)fieldOffset;
			if (this.IsNull || SqlRecordBuffer.StorageType.ByteArray != this._type)
			{
				if (num != 0)
				{
					throw ADP.ArgumentOutOfRange("fieldOffset");
				}
				this._object = new byte[length];
				this._type = SqlRecordBuffer.StorageType.ByteArray;
				this._isNull = false;
				this.BytesLength = (long)length;
			}
			else
			{
				if ((long)num > this.BytesLength)
				{
					throw ADP.ArgumentOutOfRange("fieldOffset");
				}
				if ((long)(num + length) > this.BytesLength)
				{
					int num2 = ((byte[])this._object).Length;
					if (num + length > num2)
					{
						byte[] array = new byte[Math.Max(num + length, 2 * num2)];
						Buffer.BlockCopy((byte[])this._object, 0, array, 0, (int)this.BytesLength);
						this._object = array;
					}
					this.BytesLength = (long)(num + length);
				}
			}
			Buffer.BlockCopy(buffer, bufferOffset, (byte[])this._object, num, length);
			return length;
		}

		// Token: 0x060027D8 RID: 10200 RVA: 0x000B0338 File Offset: 0x000AE538
		internal int SetChars(long fieldOffset, char[] buffer, int bufferOffset, int length)
		{
			int num = (int)fieldOffset;
			if (this.IsNull || (SqlRecordBuffer.StorageType.CharArray != this._type && SqlRecordBuffer.StorageType.String != this._type))
			{
				if (num != 0)
				{
					throw ADP.ArgumentOutOfRange("fieldOffset");
				}
				this._object = new char[length];
				this._type = SqlRecordBuffer.StorageType.CharArray;
				this._isNull = false;
				this.CharsLength = (long)length;
			}
			else
			{
				if ((long)num > this.CharsLength)
				{
					throw ADP.ArgumentOutOfRange("fieldOffset");
				}
				if (SqlRecordBuffer.StorageType.String == this._type)
				{
					this._object = ((string)this._object).ToCharArray();
					this._type = SqlRecordBuffer.StorageType.CharArray;
				}
				if ((long)(num + length) > this.CharsLength)
				{
					int num2 = ((char[])this._object).Length;
					if (num + length > num2)
					{
						char[] array = new char[Math.Max(num + length, 2 * num2)];
						Array.Copy((char[])this._object, 0, array, 0, (int)this.CharsLength);
						this._object = array;
					}
					this.CharsLength = (long)(num + length);
				}
			}
			Array.Copy(buffer, bufferOffset, (char[])this._object, num, length);
			return length;
		}

		// Token: 0x060027D9 RID: 10201 RVA: 0x000B044D File Offset: 0x000AE64D
		internal void SetNull()
		{
			this._isNull = true;
		}

		// Token: 0x060027DA RID: 10202 RVA: 0x000B0458 File Offset: 0x000AE658
		private void ConvertXmlStringToByteArray()
		{
			string text = (string)this._object;
			byte[] array = new byte[2 + Encoding.Unicode.GetByteCount(text)];
			array[0] = byte.MaxValue;
			array[1] = 254;
			Encoding.Unicode.GetBytes(text, 0, text.Length, array, 2);
			this._object = array;
			this._value._int64 = (long)array.Length;
			this._type = SqlRecordBuffer.StorageType.ByteArray;
		}

		// Token: 0x040017D5 RID: 6101
		private bool _isNull;

		// Token: 0x040017D6 RID: 6102
		private SqlRecordBuffer.StorageType _type;

		// Token: 0x040017D7 RID: 6103
		private SqlRecordBuffer.Storage _value;

		// Token: 0x040017D8 RID: 6104
		private object _object;

		// Token: 0x040017D9 RID: 6105
		private SmiMetaData _metadata;

		// Token: 0x040017DA RID: 6106
		private bool _isMetaSet;

		// Token: 0x02000314 RID: 788
		internal enum StorageType
		{
			// Token: 0x040017DC RID: 6108
			Boolean,
			// Token: 0x040017DD RID: 6109
			Byte,
			// Token: 0x040017DE RID: 6110
			ByteArray,
			// Token: 0x040017DF RID: 6111
			CharArray,
			// Token: 0x040017E0 RID: 6112
			DateTime,
			// Token: 0x040017E1 RID: 6113
			DateTimeOffset,
			// Token: 0x040017E2 RID: 6114
			Double,
			// Token: 0x040017E3 RID: 6115
			Guid,
			// Token: 0x040017E4 RID: 6116
			Int16,
			// Token: 0x040017E5 RID: 6117
			Int32,
			// Token: 0x040017E6 RID: 6118
			Int64,
			// Token: 0x040017E7 RID: 6119
			Single,
			// Token: 0x040017E8 RID: 6120
			String,
			// Token: 0x040017E9 RID: 6121
			SqlDecimal,
			// Token: 0x040017EA RID: 6122
			TimeSpan
		}

		// Token: 0x02000315 RID: 789
		[StructLayout(LayoutKind.Explicit)]
		internal struct Storage
		{
			// Token: 0x040017EB RID: 6123
			[FieldOffset(0)]
			internal bool _boolean;

			// Token: 0x040017EC RID: 6124
			[FieldOffset(0)]
			internal byte _byte;

			// Token: 0x040017ED RID: 6125
			[FieldOffset(0)]
			internal DateTime _dateTime;

			// Token: 0x040017EE RID: 6126
			[FieldOffset(0)]
			internal DateTimeOffset _dateTimeOffset;

			// Token: 0x040017EF RID: 6127
			[FieldOffset(0)]
			internal double _double;

			// Token: 0x040017F0 RID: 6128
			[FieldOffset(0)]
			internal Guid _guid;

			// Token: 0x040017F1 RID: 6129
			[FieldOffset(0)]
			internal short _int16;

			// Token: 0x040017F2 RID: 6130
			[FieldOffset(0)]
			internal int _int32;

			// Token: 0x040017F3 RID: 6131
			[FieldOffset(0)]
			internal long _int64;

			// Token: 0x040017F4 RID: 6132
			[FieldOffset(0)]
			internal float _single;

			// Token: 0x040017F5 RID: 6133
			[FieldOffset(0)]
			internal TimeSpan _timeSpan;
		}
	}
}
