﻿using System;

namespace Microsoft.SqlServer.Server
{
	/// <summary>Annotates the returned result of a user-defined type (UDT) with additional information that can be used in Transact-SQL.</summary>
	// Token: 0x0200031A RID: 794
	[AttributeUsage(AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Parameter | AttributeTargets.ReturnValue, AllowMultiple = false, Inherited = false)]
	public class SqlFacetAttribute : Attribute
	{
		/// <summary>Indicates whether the return type of the user-defined type is of a fixed length.</summary>
		/// <returns>
		///     <see langword="true" /> if the return type is of a fixed length; otherwise <see langword="false" />.</returns>
		// Token: 0x17000697 RID: 1687
		// (get) Token: 0x060028E7 RID: 10471 RVA: 0x000B6636 File Offset: 0x000B4836
		// (set) Token: 0x060028E8 RID: 10472 RVA: 0x000B663E File Offset: 0x000B483E
		public bool IsFixedLength
		{
			get
			{
				return this.m_IsFixedLength;
			}
			set
			{
				this.m_IsFixedLength = value;
			}
		}

		/// <summary>The maximum size, in logical units, of the underlying field type of the user-defined type.</summary>
		/// <returns>An <see cref="T:System.Int32" /> representing the maximum size, in logical units, of the underlying field type.</returns>
		// Token: 0x17000698 RID: 1688
		// (get) Token: 0x060028E9 RID: 10473 RVA: 0x000B6647 File Offset: 0x000B4847
		// (set) Token: 0x060028EA RID: 10474 RVA: 0x000B664F File Offset: 0x000B484F
		public int MaxSize
		{
			get
			{
				return this.m_MaxSize;
			}
			set
			{
				this.m_MaxSize = value;
			}
		}

		/// <summary>The precision of the return type of the user-defined type.</summary>
		/// <returns>An <see cref="T:System.Int32" /> representing the precision of the return type.</returns>
		// Token: 0x17000699 RID: 1689
		// (get) Token: 0x060028EB RID: 10475 RVA: 0x000B6658 File Offset: 0x000B4858
		// (set) Token: 0x060028EC RID: 10476 RVA: 0x000B6660 File Offset: 0x000B4860
		public int Precision
		{
			get
			{
				return this.m_Precision;
			}
			set
			{
				this.m_Precision = value;
			}
		}

		/// <summary>The scale of the return type of the user-defined type.</summary>
		/// <returns>An <see cref="T:System.Int32" /> representing the scale of the return type.</returns>
		// Token: 0x1700069A RID: 1690
		// (get) Token: 0x060028ED RID: 10477 RVA: 0x000B6669 File Offset: 0x000B4869
		// (set) Token: 0x060028EE RID: 10478 RVA: 0x000B6671 File Offset: 0x000B4871
		public int Scale
		{
			get
			{
				return this.m_Scale;
			}
			set
			{
				this.m_Scale = value;
			}
		}

		/// <summary>Indicates whether the return type of the user-defined type can be <see langword="null" />.</summary>
		/// <returns>
		///     <see langword="true" /> if the return type of the user-defined type can be <see langword="null" />; otherwise <see langword="false" />.</returns>
		// Token: 0x1700069B RID: 1691
		// (get) Token: 0x060028EF RID: 10479 RVA: 0x000B667A File Offset: 0x000B487A
		// (set) Token: 0x060028F0 RID: 10480 RVA: 0x000B6682 File Offset: 0x000B4882
		public bool IsNullable
		{
			get
			{
				return this.m_IsNullable;
			}
			set
			{
				this.m_IsNullable = value;
			}
		}

		/// <summary>An optional attribute on a user-defined type (UDT) return type, used to annotate the returned result with additional information that can be used in Transact-SQL.</summary>
		// Token: 0x060028F1 RID: 10481 RVA: 0x00005E14 File Offset: 0x00004014
		public SqlFacetAttribute()
		{
		}

		// Token: 0x0400182A RID: 6186
		private bool m_IsFixedLength;

		// Token: 0x0400182B RID: 6187
		private int m_MaxSize;

		// Token: 0x0400182C RID: 6188
		private int m_Scale;

		// Token: 0x0400182D RID: 6189
		private int m_Precision;

		// Token: 0x0400182E RID: 6190
		private bool m_IsNullable;
	}
}
