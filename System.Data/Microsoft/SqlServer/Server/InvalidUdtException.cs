﻿using System;
using System.Data.Common;
using System.Runtime.Serialization;
using System.Security.Permissions;

namespace Microsoft.SqlServer.Server
{
	/// <summary>Thrown when SQL Server or the ADO.NET <see cref="N:System.Data.SqlClient" /> provider detects an invalid user-defined type (UDT). </summary>
	// Token: 0x02000326 RID: 806
	[Serializable]
	public sealed class InvalidUdtException : SystemException
	{
		// Token: 0x0600292C RID: 10540 RVA: 0x000B695D File Offset: 0x000B4B5D
		internal InvalidUdtException()
		{
			base.HResult = -2146232009;
		}

		// Token: 0x0600292D RID: 10541 RVA: 0x000B6970 File Offset: 0x000B4B70
		internal InvalidUdtException(string message) : base(message)
		{
			base.HResult = -2146232009;
		}

		// Token: 0x0600292E RID: 10542 RVA: 0x000B6984 File Offset: 0x000B4B84
		internal InvalidUdtException(string message, Exception innerException) : base(message, innerException)
		{
			base.HResult = -2146232009;
		}

		// Token: 0x0600292F RID: 10543 RVA: 0x000B6999 File Offset: 0x000B4B99
		private InvalidUdtException(SerializationInfo si, StreamingContext sc) : base(si, sc)
		{
		}

		/// <summary>Streams all the <see cref="T:Microsoft.SqlServer.Server.InvalidUdtException" /> properties into the <see cref="T:System.Runtime.Serialization.SerializationInfo" /> class for the given <see cref="T:System.Runtime.Serialization.StreamingContext" />.</summary>
		/// <param name="si">The <see cref="T:System.Runtime.Serialization.SerializationInfo" /> object.</param>
		/// <param name="context">The <see cref="T:System.Runtime.Serialization.StreamingContext" /> object.</param>
		// Token: 0x06002930 RID: 10544 RVA: 0x000107B2 File Offset: 0x0000E9B2
		[SecurityPermission(SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.SerializationFormatter)]
		public override void GetObjectData(SerializationInfo si, StreamingContext context)
		{
			base.GetObjectData(si, context);
		}

		// Token: 0x06002931 RID: 10545 RVA: 0x000B69A4 File Offset: 0x000B4BA4
		internal static InvalidUdtException Create(Type udtType, string resourceReason)
		{
			string @string = Res.GetString(resourceReason);
			InvalidUdtException ex = new InvalidUdtException(Res.GetString("'{0}' is an invalid user defined type, reason: {1}.", new object[]
			{
				udtType.FullName,
				@string
			}));
			ADP.TraceExceptionAsReturnValue(ex);
			return ex;
		}

		// Token: 0x02000327 RID: 807
		private class HResults
		{
			// Token: 0x06002932 RID: 10546 RVA: 0x00005C14 File Offset: 0x00003E14
			public HResults()
			{
			}

			// Token: 0x04001901 RID: 6401
			internal const int InvalidUdt = -2146232009;
		}
	}
}
