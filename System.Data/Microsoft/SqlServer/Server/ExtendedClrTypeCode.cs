﻿using System;

namespace Microsoft.SqlServer.Server
{
	// Token: 0x020002F7 RID: 759
	internal enum ExtendedClrTypeCode
	{
		// Token: 0x0400171D RID: 5917
		Invalid = -1,
		// Token: 0x0400171E RID: 5918
		Boolean,
		// Token: 0x0400171F RID: 5919
		Byte,
		// Token: 0x04001720 RID: 5920
		Char,
		// Token: 0x04001721 RID: 5921
		DateTime,
		// Token: 0x04001722 RID: 5922
		DBNull,
		// Token: 0x04001723 RID: 5923
		Decimal,
		// Token: 0x04001724 RID: 5924
		Double,
		// Token: 0x04001725 RID: 5925
		Empty,
		// Token: 0x04001726 RID: 5926
		Int16,
		// Token: 0x04001727 RID: 5927
		Int32,
		// Token: 0x04001728 RID: 5928
		Int64,
		// Token: 0x04001729 RID: 5929
		SByte,
		// Token: 0x0400172A RID: 5930
		Single,
		// Token: 0x0400172B RID: 5931
		String,
		// Token: 0x0400172C RID: 5932
		UInt16,
		// Token: 0x0400172D RID: 5933
		UInt32,
		// Token: 0x0400172E RID: 5934
		UInt64,
		// Token: 0x0400172F RID: 5935
		Object,
		// Token: 0x04001730 RID: 5936
		ByteArray,
		// Token: 0x04001731 RID: 5937
		CharArray,
		// Token: 0x04001732 RID: 5938
		Guid,
		// Token: 0x04001733 RID: 5939
		SqlBinary,
		// Token: 0x04001734 RID: 5940
		SqlBoolean,
		// Token: 0x04001735 RID: 5941
		SqlByte,
		// Token: 0x04001736 RID: 5942
		SqlDateTime,
		// Token: 0x04001737 RID: 5943
		SqlDouble,
		// Token: 0x04001738 RID: 5944
		SqlGuid,
		// Token: 0x04001739 RID: 5945
		SqlInt16,
		// Token: 0x0400173A RID: 5946
		SqlInt32,
		// Token: 0x0400173B RID: 5947
		SqlInt64,
		// Token: 0x0400173C RID: 5948
		SqlMoney,
		// Token: 0x0400173D RID: 5949
		SqlDecimal,
		// Token: 0x0400173E RID: 5950
		SqlSingle,
		// Token: 0x0400173F RID: 5951
		SqlString,
		// Token: 0x04001740 RID: 5952
		SqlChars,
		// Token: 0x04001741 RID: 5953
		SqlBytes,
		// Token: 0x04001742 RID: 5954
		SqlXml,
		// Token: 0x04001743 RID: 5955
		DataTable,
		// Token: 0x04001744 RID: 5956
		DbDataReader,
		// Token: 0x04001745 RID: 5957
		IEnumerableOfSqlDataRecord,
		// Token: 0x04001746 RID: 5958
		TimeSpan,
		// Token: 0x04001747 RID: 5959
		DateTimeOffset,
		// Token: 0x04001748 RID: 5960
		Stream,
		// Token: 0x04001749 RID: 5961
		TextReader,
		// Token: 0x0400174A RID: 5962
		XmlReader,
		// Token: 0x0400174B RID: 5963
		Last = 44,
		// Token: 0x0400174C RID: 5964
		First = 0
	}
}
