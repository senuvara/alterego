﻿using System;
using System.Data;
using System.Data.SqlTypes;

namespace Microsoft.SqlServer.Server
{
	// Token: 0x020002F8 RID: 760
	internal interface ITypedGetters
	{
		// Token: 0x060025EC RID: 9708
		bool IsDBNull(int ordinal);

		// Token: 0x060025ED RID: 9709
		SqlDbType GetVariantType(int ordinal);

		// Token: 0x060025EE RID: 9710
		bool GetBoolean(int ordinal);

		// Token: 0x060025EF RID: 9711
		byte GetByte(int ordinal);

		// Token: 0x060025F0 RID: 9712
		long GetBytes(int ordinal, long fieldOffset, byte[] buffer, int bufferOffset, int length);

		// Token: 0x060025F1 RID: 9713
		char GetChar(int ordinal);

		// Token: 0x060025F2 RID: 9714
		long GetChars(int ordinal, long fieldOffset, char[] buffer, int bufferOffset, int length);

		// Token: 0x060025F3 RID: 9715
		short GetInt16(int ordinal);

		// Token: 0x060025F4 RID: 9716
		int GetInt32(int ordinal);

		// Token: 0x060025F5 RID: 9717
		long GetInt64(int ordinal);

		// Token: 0x060025F6 RID: 9718
		float GetFloat(int ordinal);

		// Token: 0x060025F7 RID: 9719
		double GetDouble(int ordinal);

		// Token: 0x060025F8 RID: 9720
		string GetString(int ordinal);

		// Token: 0x060025F9 RID: 9721
		decimal GetDecimal(int ordinal);

		// Token: 0x060025FA RID: 9722
		DateTime GetDateTime(int ordinal);

		// Token: 0x060025FB RID: 9723
		Guid GetGuid(int ordinal);

		// Token: 0x060025FC RID: 9724
		SqlBoolean GetSqlBoolean(int ordinal);

		// Token: 0x060025FD RID: 9725
		SqlByte GetSqlByte(int ordinal);

		// Token: 0x060025FE RID: 9726
		SqlInt16 GetSqlInt16(int ordinal);

		// Token: 0x060025FF RID: 9727
		SqlInt32 GetSqlInt32(int ordinal);

		// Token: 0x06002600 RID: 9728
		SqlInt64 GetSqlInt64(int ordinal);

		// Token: 0x06002601 RID: 9729
		SqlSingle GetSqlSingle(int ordinal);

		// Token: 0x06002602 RID: 9730
		SqlDouble GetSqlDouble(int ordinal);

		// Token: 0x06002603 RID: 9731
		SqlMoney GetSqlMoney(int ordinal);

		// Token: 0x06002604 RID: 9732
		SqlDateTime GetSqlDateTime(int ordinal);

		// Token: 0x06002605 RID: 9733
		SqlDecimal GetSqlDecimal(int ordinal);

		// Token: 0x06002606 RID: 9734
		SqlString GetSqlString(int ordinal);

		// Token: 0x06002607 RID: 9735
		SqlBinary GetSqlBinary(int ordinal);

		// Token: 0x06002608 RID: 9736
		SqlGuid GetSqlGuid(int ordinal);

		// Token: 0x06002609 RID: 9737
		SqlChars GetSqlChars(int ordinal);

		// Token: 0x0600260A RID: 9738
		SqlBytes GetSqlBytes(int ordinal);

		// Token: 0x0600260B RID: 9739
		SqlXml GetSqlXml(int ordinal);

		// Token: 0x0600260C RID: 9740
		SqlBytes GetSqlBytesRef(int ordinal);

		// Token: 0x0600260D RID: 9741
		SqlChars GetSqlCharsRef(int ordinal);

		// Token: 0x0600260E RID: 9742
		SqlXml GetSqlXmlRef(int ordinal);
	}
}
