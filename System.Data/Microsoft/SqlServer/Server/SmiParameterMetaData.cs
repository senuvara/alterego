﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlTypes;

namespace Microsoft.SqlServer.Server
{
	// Token: 0x02000303 RID: 771
	internal sealed class SmiParameterMetaData : SmiExtendedMetaData
	{
		// Token: 0x060026BA RID: 9914 RVA: 0x000AEB64 File Offset: 0x000ACD64
		internal SmiParameterMetaData(SqlDbType dbType, long maxLength, byte precision, byte scale, long localeId, SqlCompareOptions compareOptions, bool isMultiValued, IList<SmiExtendedMetaData> fieldMetaData, SmiMetaDataPropertyCollection extendedProperties, string name, string typeSpecificNamePart1, string typeSpecificNamePart2, string typeSpecificNamePart3, ParameterDirection direction) : base(dbType, maxLength, precision, scale, localeId, compareOptions, isMultiValued, fieldMetaData, extendedProperties, name, typeSpecificNamePart1, typeSpecificNamePart2, typeSpecificNamePart3)
		{
			this._direction = direction;
		}

		// Token: 0x1700065C RID: 1628
		// (get) Token: 0x060026BB RID: 9915 RVA: 0x000AEB96 File Offset: 0x000ACD96
		internal ParameterDirection Direction
		{
			get
			{
				return this._direction;
			}
		}

		// Token: 0x04001795 RID: 6037
		private ParameterDirection _direction;
	}
}
