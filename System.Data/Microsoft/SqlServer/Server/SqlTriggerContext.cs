﻿using System;
using System.Data.Common;
using System.Data.SqlTypes;
using Unity;

namespace Microsoft.SqlServer.Server
{
	/// <summary>Provides contextual information about the trigger that was fired. </summary>
	// Token: 0x02000318 RID: 792
	public sealed class SqlTriggerContext
	{
		// Token: 0x060028DF RID: 10463 RVA: 0x000B65CD File Offset: 0x000B47CD
		internal SqlTriggerContext(TriggerAction triggerAction, bool[] columnsUpdated, SqlXml eventInstanceData)
		{
			this._triggerAction = triggerAction;
			this._columnsUpdated = columnsUpdated;
			this._eventInstanceData = eventInstanceData;
		}

		/// <summary>Gets the number of columns contained by the data table bound to the trigger. This property is read-only.</summary>
		/// <returns>The number of columns contained by the data table bound to the trigger, as an integer. </returns>
		// Token: 0x17000694 RID: 1684
		// (get) Token: 0x060028E0 RID: 10464 RVA: 0x000B65EC File Offset: 0x000B47EC
		public int ColumnCount
		{
			get
			{
				int result = 0;
				if (this._columnsUpdated != null)
				{
					result = this._columnsUpdated.Length;
				}
				return result;
			}
		}

		/// <summary>Gets the event data specific to the action that fired the trigger.</summary>
		/// <returns>The event data specific to the action that fired the trigger as a <see cref="T:System.Data.SqlTypes.SqlXml" /> if more information is available; <see langword="null" /> otherwise.</returns>
		// Token: 0x17000695 RID: 1685
		// (get) Token: 0x060028E1 RID: 10465 RVA: 0x000B660D File Offset: 0x000B480D
		public SqlXml EventData
		{
			get
			{
				return this._eventInstanceData;
			}
		}

		/// <summary>Indicates what action fired the trigger.</summary>
		/// <returns>The action that fired the trigger as a <see cref="T:Microsoft.SqlServer.Server.TriggerAction" />.</returns>
		// Token: 0x17000696 RID: 1686
		// (get) Token: 0x060028E2 RID: 10466 RVA: 0x000B6615 File Offset: 0x000B4815
		public TriggerAction TriggerAction
		{
			get
			{
				return this._triggerAction;
			}
		}

		/// <summary>Returns <see langword="true" /> if a column was affected by an INSERT or UPDATE statement.</summary>
		/// <param name="columnOrdinal">The zero-based ordinal of the column.</param>
		/// <returns>
		///     <see langword="true" /> if the column was affected by an INSERT or UPDATE operation.</returns>
		/// <exception cref="T:System.InvalidOperationException">Called in the context of a trigger where the value of the <see cref="P:Microsoft.SqlServer.Server.SqlTriggerContext.TriggerAction" /> property is not <see langword="Insert" /> or <see langword="Update" />.</exception>
		// Token: 0x060028E3 RID: 10467 RVA: 0x000B661D File Offset: 0x000B481D
		public bool IsUpdatedColumn(int columnOrdinal)
		{
			if (this._columnsUpdated != null)
			{
				return this._columnsUpdated[columnOrdinal];
			}
			throw ADP.IndexOutOfRange(columnOrdinal);
		}

		// Token: 0x060028E4 RID: 10468 RVA: 0x00010458 File Offset: 0x0000E658
		internal SqlTriggerContext()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x04001827 RID: 6183
		private TriggerAction _triggerAction;

		// Token: 0x04001828 RID: 6184
		private bool[] _columnsUpdated;

		// Token: 0x04001829 RID: 6185
		private SqlXml _eventInstanceData;
	}
}
