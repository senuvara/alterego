﻿using System;
using System.Data.SqlTypes;

namespace Microsoft.SqlServer.Server
{
	// Token: 0x020002FB RID: 763
	internal interface ITypedSettersV3
	{
		// Token: 0x06002644 RID: 9796
		void SetVariantMetaData(SmiEventSink sink, int ordinal, SmiMetaData metaData);

		// Token: 0x06002645 RID: 9797
		void SetDBNull(SmiEventSink sink, int ordinal);

		// Token: 0x06002646 RID: 9798
		void SetBoolean(SmiEventSink sink, int ordinal, bool value);

		// Token: 0x06002647 RID: 9799
		void SetByte(SmiEventSink sink, int ordinal, byte value);

		// Token: 0x06002648 RID: 9800
		int SetBytes(SmiEventSink sink, int ordinal, long fieldOffset, byte[] buffer, int bufferOffset, int length);

		// Token: 0x06002649 RID: 9801
		void SetBytesLength(SmiEventSink sink, int ordinal, long length);

		// Token: 0x0600264A RID: 9802
		int SetChars(SmiEventSink sink, int ordinal, long fieldOffset, char[] buffer, int bufferOffset, int length);

		// Token: 0x0600264B RID: 9803
		void SetCharsLength(SmiEventSink sink, int ordinal, long length);

		// Token: 0x0600264C RID: 9804
		void SetString(SmiEventSink sink, int ordinal, string value, int offset, int length);

		// Token: 0x0600264D RID: 9805
		void SetInt16(SmiEventSink sink, int ordinal, short value);

		// Token: 0x0600264E RID: 9806
		void SetInt32(SmiEventSink sink, int ordinal, int value);

		// Token: 0x0600264F RID: 9807
		void SetInt64(SmiEventSink sink, int ordinal, long value);

		// Token: 0x06002650 RID: 9808
		void SetSingle(SmiEventSink sink, int ordinal, float value);

		// Token: 0x06002651 RID: 9809
		void SetDouble(SmiEventSink sink, int ordinal, double value);

		// Token: 0x06002652 RID: 9810
		void SetSqlDecimal(SmiEventSink sink, int ordinal, SqlDecimal value);

		// Token: 0x06002653 RID: 9811
		void SetDateTime(SmiEventSink sink, int ordinal, DateTime value);

		// Token: 0x06002654 RID: 9812
		void SetGuid(SmiEventSink sink, int ordinal, Guid value);
	}
}
