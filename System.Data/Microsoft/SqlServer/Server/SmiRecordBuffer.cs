﻿using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlTypes;

namespace Microsoft.SqlServer.Server
{
	// Token: 0x0200030D RID: 781
	internal abstract class SmiRecordBuffer : SmiTypedGetterSetter, ITypedGettersV3, ITypedSettersV3, ITypedGetters, ITypedSetters, IDisposable
	{
		// Token: 0x17000663 RID: 1635
		// (get) Token: 0x060026D2 RID: 9938 RVA: 0x0000EF1B File Offset: 0x0000D11B
		internal override bool CanGet
		{
			get
			{
				return true;
			}
		}

		// Token: 0x17000664 RID: 1636
		// (get) Token: 0x060026D3 RID: 9939 RVA: 0x0000EF1B File Offset: 0x0000D11B
		internal override bool CanSet
		{
			get
			{
				return true;
			}
		}

		// Token: 0x060026D4 RID: 9940 RVA: 0x000AEE57 File Offset: 0x000AD057
		public virtual void Dispose()
		{
			throw ADP.InternalError(ADP.InternalErrorCode.UnimplementedSMIMethod);
		}

		// Token: 0x060026D5 RID: 9941 RVA: 0x000AEE57 File Offset: 0x000AD057
		public virtual bool IsDBNull(int ordinal)
		{
			throw ADP.InternalError(ADP.InternalErrorCode.UnimplementedSMIMethod);
		}

		// Token: 0x060026D6 RID: 9942 RVA: 0x000AEE57 File Offset: 0x000AD057
		public virtual SqlDbType GetVariantType(int ordinal)
		{
			throw ADP.InternalError(ADP.InternalErrorCode.UnimplementedSMIMethod);
		}

		// Token: 0x060026D7 RID: 9943 RVA: 0x000AEE57 File Offset: 0x000AD057
		public virtual bool GetBoolean(int ordinal)
		{
			throw ADP.InternalError(ADP.InternalErrorCode.UnimplementedSMIMethod);
		}

		// Token: 0x060026D8 RID: 9944 RVA: 0x000AEE57 File Offset: 0x000AD057
		public virtual byte GetByte(int ordinal)
		{
			throw ADP.InternalError(ADP.InternalErrorCode.UnimplementedSMIMethod);
		}

		// Token: 0x060026D9 RID: 9945 RVA: 0x000AEE57 File Offset: 0x000AD057
		public virtual long GetBytes(int ordinal, long fieldOffset, byte[] buffer, int bufferOffset, int length)
		{
			throw ADP.InternalError(ADP.InternalErrorCode.UnimplementedSMIMethod);
		}

		// Token: 0x060026DA RID: 9946 RVA: 0x000AEE57 File Offset: 0x000AD057
		public virtual char GetChar(int ordinal)
		{
			throw ADP.InternalError(ADP.InternalErrorCode.UnimplementedSMIMethod);
		}

		// Token: 0x060026DB RID: 9947 RVA: 0x000AEE57 File Offset: 0x000AD057
		public virtual long GetChars(int ordinal, long fieldOffset, char[] buffer, int bufferOffset, int length)
		{
			throw ADP.InternalError(ADP.InternalErrorCode.UnimplementedSMIMethod);
		}

		// Token: 0x060026DC RID: 9948 RVA: 0x000AEE57 File Offset: 0x000AD057
		public virtual short GetInt16(int ordinal)
		{
			throw ADP.InternalError(ADP.InternalErrorCode.UnimplementedSMIMethod);
		}

		// Token: 0x060026DD RID: 9949 RVA: 0x000AEE57 File Offset: 0x000AD057
		public virtual int GetInt32(int ordinal)
		{
			throw ADP.InternalError(ADP.InternalErrorCode.UnimplementedSMIMethod);
		}

		// Token: 0x060026DE RID: 9950 RVA: 0x000AEE57 File Offset: 0x000AD057
		public virtual long GetInt64(int ordinal)
		{
			throw ADP.InternalError(ADP.InternalErrorCode.UnimplementedSMIMethod);
		}

		// Token: 0x060026DF RID: 9951 RVA: 0x000AEE57 File Offset: 0x000AD057
		public virtual float GetFloat(int ordinal)
		{
			throw ADP.InternalError(ADP.InternalErrorCode.UnimplementedSMIMethod);
		}

		// Token: 0x060026E0 RID: 9952 RVA: 0x000AEE57 File Offset: 0x000AD057
		public virtual double GetDouble(int ordinal)
		{
			throw ADP.InternalError(ADP.InternalErrorCode.UnimplementedSMIMethod);
		}

		// Token: 0x060026E1 RID: 9953 RVA: 0x000AEE57 File Offset: 0x000AD057
		public virtual string GetString(int ordinal)
		{
			throw ADP.InternalError(ADP.InternalErrorCode.UnimplementedSMIMethod);
		}

		// Token: 0x060026E2 RID: 9954 RVA: 0x000AEE57 File Offset: 0x000AD057
		public virtual decimal GetDecimal(int ordinal)
		{
			throw ADP.InternalError(ADP.InternalErrorCode.UnimplementedSMIMethod);
		}

		// Token: 0x060026E3 RID: 9955 RVA: 0x000AEE57 File Offset: 0x000AD057
		public virtual DateTime GetDateTime(int ordinal)
		{
			throw ADP.InternalError(ADP.InternalErrorCode.UnimplementedSMIMethod);
		}

		// Token: 0x060026E4 RID: 9956 RVA: 0x000AEE57 File Offset: 0x000AD057
		public virtual Guid GetGuid(int ordinal)
		{
			throw ADP.InternalError(ADP.InternalErrorCode.UnimplementedSMIMethod);
		}

		// Token: 0x060026E5 RID: 9957 RVA: 0x000AEE57 File Offset: 0x000AD057
		public virtual SqlBoolean GetSqlBoolean(int ordinal)
		{
			throw ADP.InternalError(ADP.InternalErrorCode.UnimplementedSMIMethod);
		}

		// Token: 0x060026E6 RID: 9958 RVA: 0x000AEE57 File Offset: 0x000AD057
		public virtual SqlByte GetSqlByte(int ordinal)
		{
			throw ADP.InternalError(ADP.InternalErrorCode.UnimplementedSMIMethod);
		}

		// Token: 0x060026E7 RID: 9959 RVA: 0x000AEE57 File Offset: 0x000AD057
		public virtual SqlInt16 GetSqlInt16(int ordinal)
		{
			throw ADP.InternalError(ADP.InternalErrorCode.UnimplementedSMIMethod);
		}

		// Token: 0x060026E8 RID: 9960 RVA: 0x000AEE57 File Offset: 0x000AD057
		public virtual SqlInt32 GetSqlInt32(int ordinal)
		{
			throw ADP.InternalError(ADP.InternalErrorCode.UnimplementedSMIMethod);
		}

		// Token: 0x060026E9 RID: 9961 RVA: 0x000AEE57 File Offset: 0x000AD057
		public virtual SqlInt64 GetSqlInt64(int ordinal)
		{
			throw ADP.InternalError(ADP.InternalErrorCode.UnimplementedSMIMethod);
		}

		// Token: 0x060026EA RID: 9962 RVA: 0x000AEE57 File Offset: 0x000AD057
		public virtual SqlSingle GetSqlSingle(int ordinal)
		{
			throw ADP.InternalError(ADP.InternalErrorCode.UnimplementedSMIMethod);
		}

		// Token: 0x060026EB RID: 9963 RVA: 0x000AEE57 File Offset: 0x000AD057
		public virtual SqlDouble GetSqlDouble(int ordinal)
		{
			throw ADP.InternalError(ADP.InternalErrorCode.UnimplementedSMIMethod);
		}

		// Token: 0x060026EC RID: 9964 RVA: 0x000AEE57 File Offset: 0x000AD057
		public virtual SqlMoney GetSqlMoney(int ordinal)
		{
			throw ADP.InternalError(ADP.InternalErrorCode.UnimplementedSMIMethod);
		}

		// Token: 0x060026ED RID: 9965 RVA: 0x000AEE57 File Offset: 0x000AD057
		public virtual SqlDateTime GetSqlDateTime(int ordinal)
		{
			throw ADP.InternalError(ADP.InternalErrorCode.UnimplementedSMIMethod);
		}

		// Token: 0x060026EE RID: 9966 RVA: 0x000AEE57 File Offset: 0x000AD057
		public virtual SqlDecimal GetSqlDecimal(int ordinal)
		{
			throw ADP.InternalError(ADP.InternalErrorCode.UnimplementedSMIMethod);
		}

		// Token: 0x060026EF RID: 9967 RVA: 0x000AEE57 File Offset: 0x000AD057
		public virtual SqlString GetSqlString(int ordinal)
		{
			throw ADP.InternalError(ADP.InternalErrorCode.UnimplementedSMIMethod);
		}

		// Token: 0x060026F0 RID: 9968 RVA: 0x000AEE57 File Offset: 0x000AD057
		public virtual SqlBinary GetSqlBinary(int ordinal)
		{
			throw ADP.InternalError(ADP.InternalErrorCode.UnimplementedSMIMethod);
		}

		// Token: 0x060026F1 RID: 9969 RVA: 0x000AEE57 File Offset: 0x000AD057
		public virtual SqlGuid GetSqlGuid(int ordinal)
		{
			throw ADP.InternalError(ADP.InternalErrorCode.UnimplementedSMIMethod);
		}

		// Token: 0x060026F2 RID: 9970 RVA: 0x000AEE57 File Offset: 0x000AD057
		public virtual SqlChars GetSqlChars(int ordinal)
		{
			throw ADP.InternalError(ADP.InternalErrorCode.UnimplementedSMIMethod);
		}

		// Token: 0x060026F3 RID: 9971 RVA: 0x000AEE57 File Offset: 0x000AD057
		public virtual SqlBytes GetSqlBytes(int ordinal)
		{
			throw ADP.InternalError(ADP.InternalErrorCode.UnimplementedSMIMethod);
		}

		// Token: 0x060026F4 RID: 9972 RVA: 0x000AEE57 File Offset: 0x000AD057
		public virtual SqlXml GetSqlXml(int ordinal)
		{
			throw ADP.InternalError(ADP.InternalErrorCode.UnimplementedSMIMethod);
		}

		// Token: 0x060026F5 RID: 9973 RVA: 0x000AEE57 File Offset: 0x000AD057
		public virtual SqlXml GetSqlXmlRef(int ordinal)
		{
			throw ADP.InternalError(ADP.InternalErrorCode.UnimplementedSMIMethod);
		}

		// Token: 0x060026F6 RID: 9974 RVA: 0x000AEE57 File Offset: 0x000AD057
		public virtual SqlBytes GetSqlBytesRef(int ordinal)
		{
			throw ADP.InternalError(ADP.InternalErrorCode.UnimplementedSMIMethod);
		}

		// Token: 0x060026F7 RID: 9975 RVA: 0x000AEE57 File Offset: 0x000AD057
		public virtual SqlChars GetSqlCharsRef(int ordinal)
		{
			throw ADP.InternalError(ADP.InternalErrorCode.UnimplementedSMIMethod);
		}

		// Token: 0x060026F8 RID: 9976 RVA: 0x000AEE57 File Offset: 0x000AD057
		public virtual void SetDBNull(int ordinal)
		{
			throw ADP.InternalError(ADP.InternalErrorCode.UnimplementedSMIMethod);
		}

		// Token: 0x060026F9 RID: 9977 RVA: 0x000AEE57 File Offset: 0x000AD057
		public virtual void SetBoolean(int ordinal, bool value)
		{
			throw ADP.InternalError(ADP.InternalErrorCode.UnimplementedSMIMethod);
		}

		// Token: 0x060026FA RID: 9978 RVA: 0x000AEE57 File Offset: 0x000AD057
		public virtual void SetByte(int ordinal, byte value)
		{
			throw ADP.InternalError(ADP.InternalErrorCode.UnimplementedSMIMethod);
		}

		// Token: 0x060026FB RID: 9979 RVA: 0x000AEE57 File Offset: 0x000AD057
		public virtual void SetBytes(int ordinal, long fieldOffset, byte[] buffer, int bufferOffset, int length)
		{
			throw ADP.InternalError(ADP.InternalErrorCode.UnimplementedSMIMethod);
		}

		// Token: 0x060026FC RID: 9980 RVA: 0x000AEE57 File Offset: 0x000AD057
		public virtual void SetChar(int ordinal, char value)
		{
			throw ADP.InternalError(ADP.InternalErrorCode.UnimplementedSMIMethod);
		}

		// Token: 0x060026FD RID: 9981 RVA: 0x000AEE57 File Offset: 0x000AD057
		public virtual void SetChars(int ordinal, long fieldOffset, char[] buffer, int bufferOffset, int length)
		{
			throw ADP.InternalError(ADP.InternalErrorCode.UnimplementedSMIMethod);
		}

		// Token: 0x060026FE RID: 9982 RVA: 0x000AEE57 File Offset: 0x000AD057
		public virtual void SetInt16(int ordinal, short value)
		{
			throw ADP.InternalError(ADP.InternalErrorCode.UnimplementedSMIMethod);
		}

		// Token: 0x060026FF RID: 9983 RVA: 0x000AEE57 File Offset: 0x000AD057
		public virtual void SetInt32(int ordinal, int value)
		{
			throw ADP.InternalError(ADP.InternalErrorCode.UnimplementedSMIMethod);
		}

		// Token: 0x06002700 RID: 9984 RVA: 0x000AEE57 File Offset: 0x000AD057
		public virtual void SetInt64(int ordinal, long value)
		{
			throw ADP.InternalError(ADP.InternalErrorCode.UnimplementedSMIMethod);
		}

		// Token: 0x06002701 RID: 9985 RVA: 0x000AEE57 File Offset: 0x000AD057
		public virtual void SetFloat(int ordinal, float value)
		{
			throw ADP.InternalError(ADP.InternalErrorCode.UnimplementedSMIMethod);
		}

		// Token: 0x06002702 RID: 9986 RVA: 0x000AEE57 File Offset: 0x000AD057
		public virtual void SetDouble(int ordinal, double value)
		{
			throw ADP.InternalError(ADP.InternalErrorCode.UnimplementedSMIMethod);
		}

		// Token: 0x06002703 RID: 9987 RVA: 0x000AEE57 File Offset: 0x000AD057
		public virtual void SetString(int ordinal, string value)
		{
			throw ADP.InternalError(ADP.InternalErrorCode.UnimplementedSMIMethod);
		}

		// Token: 0x06002704 RID: 9988 RVA: 0x000AEE57 File Offset: 0x000AD057
		public virtual void SetString(int ordinal, string value, int offset)
		{
			throw ADP.InternalError(ADP.InternalErrorCode.UnimplementedSMIMethod);
		}

		// Token: 0x06002705 RID: 9989 RVA: 0x000AEE57 File Offset: 0x000AD057
		public virtual void SetDecimal(int ordinal, decimal value)
		{
			throw ADP.InternalError(ADP.InternalErrorCode.UnimplementedSMIMethod);
		}

		// Token: 0x06002706 RID: 9990 RVA: 0x000AEE57 File Offset: 0x000AD057
		public virtual void SetDateTime(int ordinal, DateTime value)
		{
			throw ADP.InternalError(ADP.InternalErrorCode.UnimplementedSMIMethod);
		}

		// Token: 0x06002707 RID: 9991 RVA: 0x000AEE57 File Offset: 0x000AD057
		public virtual void SetGuid(int ordinal, Guid value)
		{
			throw ADP.InternalError(ADP.InternalErrorCode.UnimplementedSMIMethod);
		}

		// Token: 0x06002708 RID: 9992 RVA: 0x000AEE57 File Offset: 0x000AD057
		public virtual void SetSqlBoolean(int ordinal, SqlBoolean value)
		{
			throw ADP.InternalError(ADP.InternalErrorCode.UnimplementedSMIMethod);
		}

		// Token: 0x06002709 RID: 9993 RVA: 0x000AEE57 File Offset: 0x000AD057
		public virtual void SetSqlByte(int ordinal, SqlByte value)
		{
			throw ADP.InternalError(ADP.InternalErrorCode.UnimplementedSMIMethod);
		}

		// Token: 0x0600270A RID: 9994 RVA: 0x000AEE57 File Offset: 0x000AD057
		public virtual void SetSqlInt16(int ordinal, SqlInt16 value)
		{
			throw ADP.InternalError(ADP.InternalErrorCode.UnimplementedSMIMethod);
		}

		// Token: 0x0600270B RID: 9995 RVA: 0x000AEE57 File Offset: 0x000AD057
		public virtual void SetSqlInt32(int ordinal, SqlInt32 value)
		{
			throw ADP.InternalError(ADP.InternalErrorCode.UnimplementedSMIMethod);
		}

		// Token: 0x0600270C RID: 9996 RVA: 0x000AEE57 File Offset: 0x000AD057
		public virtual void SetSqlInt64(int ordinal, SqlInt64 value)
		{
			throw ADP.InternalError(ADP.InternalErrorCode.UnimplementedSMIMethod);
		}

		// Token: 0x0600270D RID: 9997 RVA: 0x000AEE57 File Offset: 0x000AD057
		public virtual void SetSqlSingle(int ordinal, SqlSingle value)
		{
			throw ADP.InternalError(ADP.InternalErrorCode.UnimplementedSMIMethod);
		}

		// Token: 0x0600270E RID: 9998 RVA: 0x000AEE57 File Offset: 0x000AD057
		public virtual void SetSqlDouble(int ordinal, SqlDouble value)
		{
			throw ADP.InternalError(ADP.InternalErrorCode.UnimplementedSMIMethod);
		}

		// Token: 0x0600270F RID: 9999 RVA: 0x000AEE57 File Offset: 0x000AD057
		public virtual void SetSqlMoney(int ordinal, SqlMoney value)
		{
			throw ADP.InternalError(ADP.InternalErrorCode.UnimplementedSMIMethod);
		}

		// Token: 0x06002710 RID: 10000 RVA: 0x000AEE57 File Offset: 0x000AD057
		public virtual void SetSqlDateTime(int ordinal, SqlDateTime value)
		{
			throw ADP.InternalError(ADP.InternalErrorCode.UnimplementedSMIMethod);
		}

		// Token: 0x06002711 RID: 10001 RVA: 0x000AEE57 File Offset: 0x000AD057
		public virtual void SetSqlDecimal(int ordinal, SqlDecimal value)
		{
			throw ADP.InternalError(ADP.InternalErrorCode.UnimplementedSMIMethod);
		}

		// Token: 0x06002712 RID: 10002 RVA: 0x000AEE57 File Offset: 0x000AD057
		public virtual void SetSqlString(int ordinal, SqlString value)
		{
			throw ADP.InternalError(ADP.InternalErrorCode.UnimplementedSMIMethod);
		}

		// Token: 0x06002713 RID: 10003 RVA: 0x000AEE57 File Offset: 0x000AD057
		public virtual void SetSqlString(int ordinal, SqlString value, int offset)
		{
			throw ADP.InternalError(ADP.InternalErrorCode.UnimplementedSMIMethod);
		}

		// Token: 0x06002714 RID: 10004 RVA: 0x000AEE57 File Offset: 0x000AD057
		public virtual void SetSqlBinary(int ordinal, SqlBinary value)
		{
			throw ADP.InternalError(ADP.InternalErrorCode.UnimplementedSMIMethod);
		}

		// Token: 0x06002715 RID: 10005 RVA: 0x000AEE57 File Offset: 0x000AD057
		public virtual void SetSqlBinary(int ordinal, SqlBinary value, int offset)
		{
			throw ADP.InternalError(ADP.InternalErrorCode.UnimplementedSMIMethod);
		}

		// Token: 0x06002716 RID: 10006 RVA: 0x000AEE57 File Offset: 0x000AD057
		public virtual void SetSqlGuid(int ordinal, SqlGuid value)
		{
			throw ADP.InternalError(ADP.InternalErrorCode.UnimplementedSMIMethod);
		}

		// Token: 0x06002717 RID: 10007 RVA: 0x000AEE57 File Offset: 0x000AD057
		public virtual void SetSqlChars(int ordinal, SqlChars value)
		{
			throw ADP.InternalError(ADP.InternalErrorCode.UnimplementedSMIMethod);
		}

		// Token: 0x06002718 RID: 10008 RVA: 0x000AEE57 File Offset: 0x000AD057
		public virtual void SetSqlChars(int ordinal, SqlChars value, int offset)
		{
			throw ADP.InternalError(ADP.InternalErrorCode.UnimplementedSMIMethod);
		}

		// Token: 0x06002719 RID: 10009 RVA: 0x000AEE57 File Offset: 0x000AD057
		public virtual void SetSqlBytes(int ordinal, SqlBytes value)
		{
			throw ADP.InternalError(ADP.InternalErrorCode.UnimplementedSMIMethod);
		}

		// Token: 0x0600271A RID: 10010 RVA: 0x000AEE57 File Offset: 0x000AD057
		public virtual void SetSqlBytes(int ordinal, SqlBytes value, int offset)
		{
			throw ADP.InternalError(ADP.InternalErrorCode.UnimplementedSMIMethod);
		}

		// Token: 0x0600271B RID: 10011 RVA: 0x000AEE57 File Offset: 0x000AD057
		public virtual void SetSqlXml(int ordinal, SqlXml value)
		{
			throw ADP.InternalError(ADP.InternalErrorCode.UnimplementedSMIMethod);
		}

		// Token: 0x0600271C RID: 10012 RVA: 0x000AEE60 File Offset: 0x000AD060
		protected SmiRecordBuffer()
		{
		}
	}
}
