﻿using System;
using System.Data.SqlTypes;

namespace Microsoft.SqlServer.Server
{
	// Token: 0x020002FC RID: 764
	internal sealed class MemoryRecordBuffer : SmiRecordBuffer
	{
		// Token: 0x06002655 RID: 9813 RVA: 0x000ACD9C File Offset: 0x000AAF9C
		internal MemoryRecordBuffer(SmiMetaData[] metaData)
		{
			this._buffer = new SqlRecordBuffer[metaData.Length];
			for (int i = 0; i < this._buffer.Length; i++)
			{
				this._buffer[i] = new SqlRecordBuffer(metaData[i]);
			}
		}

		// Token: 0x06002656 RID: 9814 RVA: 0x000ACDE0 File Offset: 0x000AAFE0
		public override bool IsDBNull(SmiEventSink sink, int ordinal)
		{
			return this._buffer[ordinal].IsNull;
		}

		// Token: 0x06002657 RID: 9815 RVA: 0x000ACDEF File Offset: 0x000AAFEF
		public override SmiMetaData GetVariantType(SmiEventSink sink, int ordinal)
		{
			return this._buffer[ordinal].VariantType;
		}

		// Token: 0x06002658 RID: 9816 RVA: 0x000ACDFE File Offset: 0x000AAFFE
		public override bool GetBoolean(SmiEventSink sink, int ordinal)
		{
			return this._buffer[ordinal].Boolean;
		}

		// Token: 0x06002659 RID: 9817 RVA: 0x000ACE0D File Offset: 0x000AB00D
		public override byte GetByte(SmiEventSink sink, int ordinal)
		{
			return this._buffer[ordinal].Byte;
		}

		// Token: 0x0600265A RID: 9818 RVA: 0x000ACE1C File Offset: 0x000AB01C
		public override long GetBytesLength(SmiEventSink sink, int ordinal)
		{
			return this._buffer[ordinal].BytesLength;
		}

		// Token: 0x0600265B RID: 9819 RVA: 0x000ACE2B File Offset: 0x000AB02B
		public override int GetBytes(SmiEventSink sink, int ordinal, long fieldOffset, byte[] buffer, int bufferOffset, int length)
		{
			return this._buffer[ordinal].GetBytes(fieldOffset, buffer, bufferOffset, length);
		}

		// Token: 0x0600265C RID: 9820 RVA: 0x000ACE41 File Offset: 0x000AB041
		public override long GetCharsLength(SmiEventSink sink, int ordinal)
		{
			return this._buffer[ordinal].CharsLength;
		}

		// Token: 0x0600265D RID: 9821 RVA: 0x000ACE50 File Offset: 0x000AB050
		public override int GetChars(SmiEventSink sink, int ordinal, long fieldOffset, char[] buffer, int bufferOffset, int length)
		{
			return this._buffer[ordinal].GetChars(fieldOffset, buffer, bufferOffset, length);
		}

		// Token: 0x0600265E RID: 9822 RVA: 0x000ACE66 File Offset: 0x000AB066
		public override string GetString(SmiEventSink sink, int ordinal)
		{
			return this._buffer[ordinal].String;
		}

		// Token: 0x0600265F RID: 9823 RVA: 0x000ACE75 File Offset: 0x000AB075
		public override short GetInt16(SmiEventSink sink, int ordinal)
		{
			return this._buffer[ordinal].Int16;
		}

		// Token: 0x06002660 RID: 9824 RVA: 0x000ACE84 File Offset: 0x000AB084
		public override int GetInt32(SmiEventSink sink, int ordinal)
		{
			return this._buffer[ordinal].Int32;
		}

		// Token: 0x06002661 RID: 9825 RVA: 0x000ACE93 File Offset: 0x000AB093
		public override long GetInt64(SmiEventSink sink, int ordinal)
		{
			return this._buffer[ordinal].Int64;
		}

		// Token: 0x06002662 RID: 9826 RVA: 0x000ACEA2 File Offset: 0x000AB0A2
		public override float GetSingle(SmiEventSink sink, int ordinal)
		{
			return this._buffer[ordinal].Single;
		}

		// Token: 0x06002663 RID: 9827 RVA: 0x000ACEB1 File Offset: 0x000AB0B1
		public override double GetDouble(SmiEventSink sink, int ordinal)
		{
			return this._buffer[ordinal].Double;
		}

		// Token: 0x06002664 RID: 9828 RVA: 0x000ACEC0 File Offset: 0x000AB0C0
		public override SqlDecimal GetSqlDecimal(SmiEventSink sink, int ordinal)
		{
			return this._buffer[ordinal].SqlDecimal;
		}

		// Token: 0x06002665 RID: 9829 RVA: 0x000ACECF File Offset: 0x000AB0CF
		public override DateTime GetDateTime(SmiEventSink sink, int ordinal)
		{
			return this._buffer[ordinal].DateTime;
		}

		// Token: 0x06002666 RID: 9830 RVA: 0x000ACEDE File Offset: 0x000AB0DE
		public override Guid GetGuid(SmiEventSink sink, int ordinal)
		{
			return this._buffer[ordinal].Guid;
		}

		// Token: 0x06002667 RID: 9831 RVA: 0x000ACEED File Offset: 0x000AB0ED
		public override TimeSpan GetTimeSpan(SmiEventSink sink, int ordinal)
		{
			return this._buffer[ordinal].TimeSpan;
		}

		// Token: 0x06002668 RID: 9832 RVA: 0x000ACEFC File Offset: 0x000AB0FC
		public override DateTimeOffset GetDateTimeOffset(SmiEventSink sink, int ordinal)
		{
			return this._buffer[ordinal].DateTimeOffset;
		}

		// Token: 0x06002669 RID: 9833 RVA: 0x000ACF0B File Offset: 0x000AB10B
		public override void SetDBNull(SmiEventSink sink, int ordinal)
		{
			this._buffer[ordinal].SetNull();
		}

		// Token: 0x0600266A RID: 9834 RVA: 0x000ACF1A File Offset: 0x000AB11A
		public override void SetBoolean(SmiEventSink sink, int ordinal, bool value)
		{
			this._buffer[ordinal].Boolean = value;
		}

		// Token: 0x0600266B RID: 9835 RVA: 0x000ACF2A File Offset: 0x000AB12A
		public override void SetByte(SmiEventSink sink, int ordinal, byte value)
		{
			this._buffer[ordinal].Byte = value;
		}

		// Token: 0x0600266C RID: 9836 RVA: 0x000ACF3A File Offset: 0x000AB13A
		public override int SetBytes(SmiEventSink sink, int ordinal, long fieldOffset, byte[] buffer, int bufferOffset, int length)
		{
			return this._buffer[ordinal].SetBytes(fieldOffset, buffer, bufferOffset, length);
		}

		// Token: 0x0600266D RID: 9837 RVA: 0x000ACF50 File Offset: 0x000AB150
		public override void SetBytesLength(SmiEventSink sink, int ordinal, long length)
		{
			this._buffer[ordinal].BytesLength = length;
		}

		// Token: 0x0600266E RID: 9838 RVA: 0x000ACF60 File Offset: 0x000AB160
		public override int SetChars(SmiEventSink sink, int ordinal, long fieldOffset, char[] buffer, int bufferOffset, int length)
		{
			return this._buffer[ordinal].SetChars(fieldOffset, buffer, bufferOffset, length);
		}

		// Token: 0x0600266F RID: 9839 RVA: 0x000ACF76 File Offset: 0x000AB176
		public override void SetCharsLength(SmiEventSink sink, int ordinal, long length)
		{
			this._buffer[ordinal].CharsLength = length;
		}

		// Token: 0x06002670 RID: 9840 RVA: 0x000ACF86 File Offset: 0x000AB186
		public override void SetString(SmiEventSink sink, int ordinal, string value, int offset, int length)
		{
			this._buffer[ordinal].String = value.Substring(offset, length);
		}

		// Token: 0x06002671 RID: 9841 RVA: 0x000ACF9F File Offset: 0x000AB19F
		public override void SetInt16(SmiEventSink sink, int ordinal, short value)
		{
			this._buffer[ordinal].Int16 = value;
		}

		// Token: 0x06002672 RID: 9842 RVA: 0x000ACFAF File Offset: 0x000AB1AF
		public override void SetInt32(SmiEventSink sink, int ordinal, int value)
		{
			this._buffer[ordinal].Int32 = value;
		}

		// Token: 0x06002673 RID: 9843 RVA: 0x000ACFBF File Offset: 0x000AB1BF
		public override void SetInt64(SmiEventSink sink, int ordinal, long value)
		{
			this._buffer[ordinal].Int64 = value;
		}

		// Token: 0x06002674 RID: 9844 RVA: 0x000ACFCF File Offset: 0x000AB1CF
		public override void SetSingle(SmiEventSink sink, int ordinal, float value)
		{
			this._buffer[ordinal].Single = value;
		}

		// Token: 0x06002675 RID: 9845 RVA: 0x000ACFDF File Offset: 0x000AB1DF
		public override void SetDouble(SmiEventSink sink, int ordinal, double value)
		{
			this._buffer[ordinal].Double = value;
		}

		// Token: 0x06002676 RID: 9846 RVA: 0x000ACFEF File Offset: 0x000AB1EF
		public override void SetSqlDecimal(SmiEventSink sink, int ordinal, SqlDecimal value)
		{
			this._buffer[ordinal].SqlDecimal = value;
		}

		// Token: 0x06002677 RID: 9847 RVA: 0x000ACFFF File Offset: 0x000AB1FF
		public override void SetDateTime(SmiEventSink sink, int ordinal, DateTime value)
		{
			this._buffer[ordinal].DateTime = value;
		}

		// Token: 0x06002678 RID: 9848 RVA: 0x000AD00F File Offset: 0x000AB20F
		public override void SetGuid(SmiEventSink sink, int ordinal, Guid value)
		{
			this._buffer[ordinal].Guid = value;
		}

		// Token: 0x06002679 RID: 9849 RVA: 0x000AD01F File Offset: 0x000AB21F
		public override void SetTimeSpan(SmiEventSink sink, int ordinal, TimeSpan value)
		{
			this._buffer[ordinal].TimeSpan = value;
		}

		// Token: 0x0600267A RID: 9850 RVA: 0x000AD02F File Offset: 0x000AB22F
		public override void SetDateTimeOffset(SmiEventSink sink, int ordinal, DateTimeOffset value)
		{
			this._buffer[ordinal].DateTimeOffset = value;
		}

		// Token: 0x0600267B RID: 9851 RVA: 0x000AD03F File Offset: 0x000AB23F
		public override void SetVariantMetaData(SmiEventSink sink, int ordinal, SmiMetaData metaData)
		{
			this._buffer[ordinal].VariantType = metaData;
		}

		// Token: 0x0400174D RID: 5965
		private SqlRecordBuffer[] _buffer;
	}
}
