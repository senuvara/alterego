﻿using System;

namespace Microsoft.SqlServer.Server
{
	// Token: 0x02000324 RID: 804
	internal enum EMDEventType
	{
		// Token: 0x04001857 RID: 6231
		x_eet_Invalid,
		// Token: 0x04001858 RID: 6232
		x_eet_Insert,
		// Token: 0x04001859 RID: 6233
		x_eet_Update,
		// Token: 0x0400185A RID: 6234
		x_eet_Delete,
		// Token: 0x0400185B RID: 6235
		x_eet_Create_Table = 21,
		// Token: 0x0400185C RID: 6236
		x_eet_Alter_Table,
		// Token: 0x0400185D RID: 6237
		x_eet_Drop_Table,
		// Token: 0x0400185E RID: 6238
		x_eet_Create_Index,
		// Token: 0x0400185F RID: 6239
		x_eet_Alter_Index,
		// Token: 0x04001860 RID: 6240
		x_eet_Drop_Index,
		// Token: 0x04001861 RID: 6241
		x_eet_Create_Stats,
		// Token: 0x04001862 RID: 6242
		x_eet_Update_Stats,
		// Token: 0x04001863 RID: 6243
		x_eet_Drop_Stats,
		// Token: 0x04001864 RID: 6244
		x_eet_Create_Secexpr = 31,
		// Token: 0x04001865 RID: 6245
		x_eet_Drop_Secexpr = 33,
		// Token: 0x04001866 RID: 6246
		x_eet_Create_Synonym,
		// Token: 0x04001867 RID: 6247
		x_eet_Drop_Synonym = 36,
		// Token: 0x04001868 RID: 6248
		x_eet_Create_View = 41,
		// Token: 0x04001869 RID: 6249
		x_eet_Alter_View,
		// Token: 0x0400186A RID: 6250
		x_eet_Drop_View,
		// Token: 0x0400186B RID: 6251
		x_eet_Create_Procedure = 51,
		// Token: 0x0400186C RID: 6252
		x_eet_Alter_Procedure,
		// Token: 0x0400186D RID: 6253
		x_eet_Drop_Procedure,
		// Token: 0x0400186E RID: 6254
		x_eet_Create_Function = 61,
		// Token: 0x0400186F RID: 6255
		x_eet_Alter_Function,
		// Token: 0x04001870 RID: 6256
		x_eet_Drop_Function,
		// Token: 0x04001871 RID: 6257
		x_eet_Create_Trigger = 71,
		// Token: 0x04001872 RID: 6258
		x_eet_Alter_Trigger,
		// Token: 0x04001873 RID: 6259
		x_eet_Drop_Trigger,
		// Token: 0x04001874 RID: 6260
		x_eet_Create_Event_Notification,
		// Token: 0x04001875 RID: 6261
		x_eet_Drop_Event_Notification = 76,
		// Token: 0x04001876 RID: 6262
		x_eet_Create_Type = 91,
		// Token: 0x04001877 RID: 6263
		x_eet_Drop_Type = 93,
		// Token: 0x04001878 RID: 6264
		x_eet_Create_Assembly = 101,
		// Token: 0x04001879 RID: 6265
		x_eet_Alter_Assembly,
		// Token: 0x0400187A RID: 6266
		x_eet_Drop_Assembly,
		// Token: 0x0400187B RID: 6267
		x_eet_Create_User = 131,
		// Token: 0x0400187C RID: 6268
		x_eet_Alter_User,
		// Token: 0x0400187D RID: 6269
		x_eet_Drop_User,
		// Token: 0x0400187E RID: 6270
		x_eet_Create_Role,
		// Token: 0x0400187F RID: 6271
		x_eet_Alter_Role,
		// Token: 0x04001880 RID: 6272
		x_eet_Drop_Role,
		// Token: 0x04001881 RID: 6273
		x_eet_Create_AppRole,
		// Token: 0x04001882 RID: 6274
		x_eet_Alter_AppRole,
		// Token: 0x04001883 RID: 6275
		x_eet_Drop_AppRole,
		// Token: 0x04001884 RID: 6276
		x_eet_Create_Schema = 141,
		// Token: 0x04001885 RID: 6277
		x_eet_Alter_Schema,
		// Token: 0x04001886 RID: 6278
		x_eet_Drop_Schema,
		// Token: 0x04001887 RID: 6279
		x_eet_Create_Login,
		// Token: 0x04001888 RID: 6280
		x_eet_Alter_Login,
		// Token: 0x04001889 RID: 6281
		x_eet_Drop_Login,
		// Token: 0x0400188A RID: 6282
		x_eet_Create_MsgType = 151,
		// Token: 0x0400188B RID: 6283
		x_eet_Alter_MsgType,
		// Token: 0x0400188C RID: 6284
		x_eet_Drop_MsgType,
		// Token: 0x0400188D RID: 6285
		x_eet_Create_Contract,
		// Token: 0x0400188E RID: 6286
		x_eet_Alter_Contract,
		// Token: 0x0400188F RID: 6287
		x_eet_Drop_Contract,
		// Token: 0x04001890 RID: 6288
		x_eet_Create_Queue,
		// Token: 0x04001891 RID: 6289
		x_eet_Alter_Queue,
		// Token: 0x04001892 RID: 6290
		x_eet_Drop_Queue,
		// Token: 0x04001893 RID: 6291
		x_eet_Create_Service = 161,
		// Token: 0x04001894 RID: 6292
		x_eet_Alter_Service,
		// Token: 0x04001895 RID: 6293
		x_eet_Drop_Service,
		// Token: 0x04001896 RID: 6294
		x_eet_Create_Route,
		// Token: 0x04001897 RID: 6295
		x_eet_Alter_Route,
		// Token: 0x04001898 RID: 6296
		x_eet_Drop_Route,
		// Token: 0x04001899 RID: 6297
		x_eet_Grant_Statement,
		// Token: 0x0400189A RID: 6298
		x_eet_Deny_Statement,
		// Token: 0x0400189B RID: 6299
		x_eet_Revoke_Statement,
		// Token: 0x0400189C RID: 6300
		x_eet_Grant_Object,
		// Token: 0x0400189D RID: 6301
		x_eet_Deny_Object,
		// Token: 0x0400189E RID: 6302
		x_eet_Revoke_Object,
		// Token: 0x0400189F RID: 6303
		x_eet_Activation,
		// Token: 0x040018A0 RID: 6304
		x_eet_Create_Binding,
		// Token: 0x040018A1 RID: 6305
		x_eet_Alter_Binding,
		// Token: 0x040018A2 RID: 6306
		x_eet_Drop_Binding,
		// Token: 0x040018A3 RID: 6307
		x_eet_Create_XmlSchema,
		// Token: 0x040018A4 RID: 6308
		x_eet_Alter_XmlSchema,
		// Token: 0x040018A5 RID: 6309
		x_eet_Drop_XmlSchema,
		// Token: 0x040018A6 RID: 6310
		x_eet_Create_HttpEndpoint = 181,
		// Token: 0x040018A7 RID: 6311
		x_eet_Alter_HttpEndpoint,
		// Token: 0x040018A8 RID: 6312
		x_eet_Drop_HttpEndpoint,
		// Token: 0x040018A9 RID: 6313
		x_eet_Create_Partition_Function = 191,
		// Token: 0x040018AA RID: 6314
		x_eet_Alter_Partition_Function,
		// Token: 0x040018AB RID: 6315
		x_eet_Drop_Partition_Function,
		// Token: 0x040018AC RID: 6316
		x_eet_Create_Partition_Scheme,
		// Token: 0x040018AD RID: 6317
		x_eet_Alter_Partition_Scheme,
		// Token: 0x040018AE RID: 6318
		x_eet_Drop_Partition_Scheme,
		// Token: 0x040018AF RID: 6319
		x_eet_Create_Database = 201,
		// Token: 0x040018B0 RID: 6320
		x_eet_Alter_Database,
		// Token: 0x040018B1 RID: 6321
		x_eet_Drop_Database,
		// Token: 0x040018B2 RID: 6322
		x_eet_Trace_Start = 1000,
		// Token: 0x040018B3 RID: 6323
		x_eet_Trace_End = 1999
	}
}
