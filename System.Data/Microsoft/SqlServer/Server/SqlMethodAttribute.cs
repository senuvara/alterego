﻿using System;

namespace Microsoft.SqlServer.Server
{
	/// <summary>Indicates the determinism and data access properties of a method or property on a user-defined type (UDT). The properties on the attribute reflect the physical characteristics that are used when the type is registered with SQL Server.</summary>
	// Token: 0x0200031E RID: 798
	[AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = false)]
	[Serializable]
	public sealed class SqlMethodAttribute : SqlFunctionAttribute
	{
		/// <summary>An attribute on a user-defined type (UDT), used to indicate the determinism and data access properties of a method or a property on a UDT.</summary>
		// Token: 0x06002901 RID: 10497 RVA: 0x000B673B File Offset: 0x000B493B
		public SqlMethodAttribute()
		{
			this.m_fCallOnNullInputs = true;
			this.m_fMutator = false;
			this.m_fInvokeIfReceiverIsNull = false;
		}

		/// <summary>Indicates whether the method on a user-defined type (UDT) is called when <see langword="null" /> input arguments are specified in the method invocation.</summary>
		/// <returns>
		///     <see langword="true" /> if the method is called when <see langword="null" /> input arguments are specified in the method invocation; <see langword="false" /> if the method returns a <see langword="null" /> value when any of its input parameters are <see langword="null" />. If the method cannot be invoked (because of an attribute on the method), the SQL Server <see langword="DbNull" /> is returned.</returns>
		// Token: 0x170006A3 RID: 1699
		// (get) Token: 0x06002902 RID: 10498 RVA: 0x000B6758 File Offset: 0x000B4958
		// (set) Token: 0x06002903 RID: 10499 RVA: 0x000B6760 File Offset: 0x000B4960
		public bool OnNullCall
		{
			get
			{
				return this.m_fCallOnNullInputs;
			}
			set
			{
				this.m_fCallOnNullInputs = value;
			}
		}

		/// <summary>Indicates whether a method on a user-defined type (UDT) is a mutator.</summary>
		/// <returns>
		///     <see langword="true" /> if the method is a mutator; otherwise <see langword="false" />.</returns>
		// Token: 0x170006A4 RID: 1700
		// (get) Token: 0x06002904 RID: 10500 RVA: 0x000B6769 File Offset: 0x000B4969
		// (set) Token: 0x06002905 RID: 10501 RVA: 0x000B6771 File Offset: 0x000B4971
		public bool IsMutator
		{
			get
			{
				return this.m_fMutator;
			}
			set
			{
				this.m_fMutator = value;
			}
		}

		/// <summary>Indicates whether SQL Server should invoke the method on null instances.</summary>
		/// <returns>
		///     <see langword="true" /> if SQL Server should invoke the method on null instances; otherwise, <see langword="false" />. If the method cannot be invoked (because of an attribute on the method), the SQL Server <see langword="DbNull" /> is returned.</returns>
		// Token: 0x170006A5 RID: 1701
		// (get) Token: 0x06002906 RID: 10502 RVA: 0x000B677A File Offset: 0x000B497A
		// (set) Token: 0x06002907 RID: 10503 RVA: 0x000B6782 File Offset: 0x000B4982
		public bool InvokeIfReceiverIsNull
		{
			get
			{
				return this.m_fInvokeIfReceiverIsNull;
			}
			set
			{
				this.m_fInvokeIfReceiverIsNull = value;
			}
		}

		// Token: 0x0400183C RID: 6204
		private bool m_fCallOnNullInputs;

		// Token: 0x0400183D RID: 6205
		private bool m_fMutator;

		// Token: 0x0400183E RID: 6206
		private bool m_fInvokeIfReceiverIsNull;
	}
}
