﻿using System;

namespace Microsoft.SqlServer.Server
{
	/// <summary>Describes the type of access to user data for a user-defined method or function.</summary>
	// Token: 0x0200031B RID: 795
	[Serializable]
	public enum DataAccessKind
	{
		/// <summary>The method or function does not access user data.</summary>
		// Token: 0x04001830 RID: 6192
		None,
		/// <summary>The method or function reads user data.</summary>
		// Token: 0x04001831 RID: 6193
		Read
	}
}
