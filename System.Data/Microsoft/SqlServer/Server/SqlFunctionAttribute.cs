﻿using System;

namespace Microsoft.SqlServer.Server
{
	/// <summary>Used to mark a method definition of a user-defined aggregate as a function in SQL Server. The properties on the attribute reflect the physical characteristics used when the type is registered with SQL Server.</summary>
	// Token: 0x0200031D RID: 797
	[AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = false)]
	[Serializable]
	public class SqlFunctionAttribute : Attribute
	{
		/// <summary>An optional attribute on a user-defined aggregate, used to indicate that the method should be registered in SQL Server as a function. Also used to set the <see cref="P:Microsoft.SqlServer.Server.SqlFunctionAttribute.DataAccess" />, <see cref="P:Microsoft.SqlServer.Server.SqlFunctionAttribute.FillRowMethodName" />, <see cref="P:Microsoft.SqlServer.Server.SqlFunctionAttribute.IsDeterministic" />, <see cref="P:Microsoft.SqlServer.Server.SqlFunctionAttribute.IsPrecise" />, <see cref="P:Microsoft.SqlServer.Server.SqlFunctionAttribute.Name" />, <see cref="P:Microsoft.SqlServer.Server.SqlFunctionAttribute.SystemDataAccess" />, and <see cref="P:Microsoft.SqlServer.Server.SqlFunctionAttribute.TableDefinition" /> properties of the function attribute.</summary>
		// Token: 0x060028F2 RID: 10482 RVA: 0x000B668B File Offset: 0x000B488B
		public SqlFunctionAttribute()
		{
			this.m_fDeterministic = false;
			this.m_eDataAccess = DataAccessKind.None;
			this.m_eSystemDataAccess = SystemDataAccessKind.None;
			this.m_fPrecise = false;
			this.m_fName = null;
			this.m_fTableDefinition = null;
			this.m_FillRowMethodName = null;
		}

		/// <summary>Indicates whether the user-defined function is deterministic.</summary>
		/// <returns>
		///     <see langword="true" /> if the function is deterministic; otherwise <see langword="false" />.</returns>
		// Token: 0x1700069C RID: 1692
		// (get) Token: 0x060028F3 RID: 10483 RVA: 0x000B66C4 File Offset: 0x000B48C4
		// (set) Token: 0x060028F4 RID: 10484 RVA: 0x000B66CC File Offset: 0x000B48CC
		public bool IsDeterministic
		{
			get
			{
				return this.m_fDeterministic;
			}
			set
			{
				this.m_fDeterministic = value;
			}
		}

		/// <summary>Indicates whether the function involves access to user data stored in the local instance of SQL Server.</summary>
		/// <returns>
		///     <see cref="T:Microsoft.SqlServer.Server.DataAccessKind" />.<see langword="None" />: Does not access data. <see cref="T:Microsoft.SqlServer.Server.DataAccessKind" />.<see langword="Read" />: Only reads user data.</returns>
		// Token: 0x1700069D RID: 1693
		// (get) Token: 0x060028F5 RID: 10485 RVA: 0x000B66D5 File Offset: 0x000B48D5
		// (set) Token: 0x060028F6 RID: 10486 RVA: 0x000B66DD File Offset: 0x000B48DD
		public DataAccessKind DataAccess
		{
			get
			{
				return this.m_eDataAccess;
			}
			set
			{
				this.m_eDataAccess = value;
			}
		}

		/// <summary>Indicates whether the function requires access to data stored in the system catalogs or virtual system tables of SQL Server.</summary>
		/// <returns>
		///     <see cref="T:Microsoft.SqlServer.Server.DataAccessKind" />.<see langword="None" />: Does not access system data. <see cref="T:Microsoft.SqlServer.Server.DataAccessKind" />.<see langword="Read" />: Only reads system data.</returns>
		// Token: 0x1700069E RID: 1694
		// (get) Token: 0x060028F7 RID: 10487 RVA: 0x000B66E6 File Offset: 0x000B48E6
		// (set) Token: 0x060028F8 RID: 10488 RVA: 0x000B66EE File Offset: 0x000B48EE
		public SystemDataAccessKind SystemDataAccess
		{
			get
			{
				return this.m_eSystemDataAccess;
			}
			set
			{
				this.m_eSystemDataAccess = value;
			}
		}

		/// <summary>Indicates whether the function involves imprecise computations, such as floating point operations.</summary>
		/// <returns>
		///     <see langword="true" /> if the function involves precise computations; otherwise <see langword="false" />.</returns>
		// Token: 0x1700069F RID: 1695
		// (get) Token: 0x060028F9 RID: 10489 RVA: 0x000B66F7 File Offset: 0x000B48F7
		// (set) Token: 0x060028FA RID: 10490 RVA: 0x000B66FF File Offset: 0x000B48FF
		public bool IsPrecise
		{
			get
			{
				return this.m_fPrecise;
			}
			set
			{
				this.m_fPrecise = value;
			}
		}

		/// <summary>The name under which the function should be registered in SQL Server.</summary>
		/// <returns>A <see cref="T:System.String" /> value representing the name under which the function should be registered.</returns>
		// Token: 0x170006A0 RID: 1696
		// (get) Token: 0x060028FB RID: 10491 RVA: 0x000B6708 File Offset: 0x000B4908
		// (set) Token: 0x060028FC RID: 10492 RVA: 0x000B6710 File Offset: 0x000B4910
		public string Name
		{
			get
			{
				return this.m_fName;
			}
			set
			{
				this.m_fName = value;
			}
		}

		/// <summary>A string that represents the table definition of the results, if the method is used as a table-valued function (TVF).</summary>
		/// <returns>A <see cref="T:System.String" /> value representing the table definition of the results.</returns>
		// Token: 0x170006A1 RID: 1697
		// (get) Token: 0x060028FD RID: 10493 RVA: 0x000B6719 File Offset: 0x000B4919
		// (set) Token: 0x060028FE RID: 10494 RVA: 0x000B6721 File Offset: 0x000B4921
		public string TableDefinition
		{
			get
			{
				return this.m_fTableDefinition;
			}
			set
			{
				this.m_fTableDefinition = value;
			}
		}

		/// <summary>The name of a method in the same class as the table-valued function (TVF) that is used by the TVF contract.</summary>
		/// <returns>A <see cref="T:System.String" /> value representing the name of a method used by the TVF contract.</returns>
		// Token: 0x170006A2 RID: 1698
		// (get) Token: 0x060028FF RID: 10495 RVA: 0x000B672A File Offset: 0x000B492A
		// (set) Token: 0x06002900 RID: 10496 RVA: 0x000B6732 File Offset: 0x000B4932
		public string FillRowMethodName
		{
			get
			{
				return this.m_FillRowMethodName;
			}
			set
			{
				this.m_FillRowMethodName = value;
			}
		}

		// Token: 0x04001835 RID: 6197
		private bool m_fDeterministic;

		// Token: 0x04001836 RID: 6198
		private DataAccessKind m_eDataAccess;

		// Token: 0x04001837 RID: 6199
		private SystemDataAccessKind m_eSystemDataAccess;

		// Token: 0x04001838 RID: 6200
		private bool m_fPrecise;

		// Token: 0x04001839 RID: 6201
		private string m_fName;

		// Token: 0x0400183A RID: 6202
		private string m_fTableDefinition;

		// Token: 0x0400183B RID: 6203
		private string m_FillRowMethodName;
	}
}
