﻿using System;

namespace Microsoft.SqlServer.Server
{
	/// <summary>Used to mark a method definition in an assembly as a trigger in SQL Server. The properties on the attribute reflect the physical attributes used when the type is registered with SQL Server. This class cannot be inherited.</summary>
	// Token: 0x02000320 RID: 800
	[AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = false)]
	[Serializable]
	public sealed class SqlTriggerAttribute : Attribute
	{
		/// <summary>An attribute on a method definition in an assembly, used to mark the method as a trigger in SQL Server.</summary>
		// Token: 0x0600290B RID: 10507 RVA: 0x000B67AB File Offset: 0x000B49AB
		public SqlTriggerAttribute()
		{
			this.m_fName = null;
			this.m_fTarget = null;
			this.m_fEvent = null;
		}

		/// <summary>The name of the trigger.</summary>
		/// <returns>A <see cref="T:System.String" /> value representing the name of the trigger.</returns>
		// Token: 0x170006A7 RID: 1703
		// (get) Token: 0x0600290C RID: 10508 RVA: 0x000B67C8 File Offset: 0x000B49C8
		// (set) Token: 0x0600290D RID: 10509 RVA: 0x000B67D0 File Offset: 0x000B49D0
		public string Name
		{
			get
			{
				return this.m_fName;
			}
			set
			{
				this.m_fName = value;
			}
		}

		/// <summary>The table to which the trigger applies.</summary>
		/// <returns>A <see cref="T:System.String" /> value representing the table name.</returns>
		// Token: 0x170006A8 RID: 1704
		// (get) Token: 0x0600290E RID: 10510 RVA: 0x000B67D9 File Offset: 0x000B49D9
		// (set) Token: 0x0600290F RID: 10511 RVA: 0x000B67E1 File Offset: 0x000B49E1
		public string Target
		{
			get
			{
				return this.m_fTarget;
			}
			set
			{
				this.m_fTarget = value;
			}
		}

		/// <summary>The type of trigger and what data manipulation language (DML) action activates the trigger.</summary>
		/// <returns>A <see cref="T:System.String" /> value representing the type of trigger and what data manipulation language (DML) action activates the trigger.</returns>
		// Token: 0x170006A9 RID: 1705
		// (get) Token: 0x06002910 RID: 10512 RVA: 0x000B67EA File Offset: 0x000B49EA
		// (set) Token: 0x06002911 RID: 10513 RVA: 0x000B67F2 File Offset: 0x000B49F2
		public string Event
		{
			get
			{
				return this.m_fEvent;
			}
			set
			{
				this.m_fEvent = value;
			}
		}

		// Token: 0x04001840 RID: 6208
		private string m_fName;

		// Token: 0x04001841 RID: 6209
		private string m_fTarget;

		// Token: 0x04001842 RID: 6210
		private string m_fEvent;
	}
}
