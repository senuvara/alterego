﻿using System;
using System.Data.SqlClient;
using System.IO;

namespace Microsoft.SqlServer.Server
{
	// Token: 0x02000300 RID: 768
	internal class SmiGettersStream : Stream
	{
		// Token: 0x06002697 RID: 9879 RVA: 0x000AE2B8 File Offset: 0x000AC4B8
		internal SmiGettersStream(SmiEventSink_Default sink, ITypedGettersV3 getters, int ordinal, SmiMetaData metaData)
		{
			this._sink = sink;
			this._getters = getters;
			this._ordinal = ordinal;
			this._readPosition = 0L;
			this._metaData = metaData;
		}

		// Token: 0x17000649 RID: 1609
		// (get) Token: 0x06002698 RID: 9880 RVA: 0x0000EF1B File Offset: 0x0000D11B
		public override bool CanRead
		{
			get
			{
				return true;
			}
		}

		// Token: 0x1700064A RID: 1610
		// (get) Token: 0x06002699 RID: 9881 RVA: 0x000061C5 File Offset: 0x000043C5
		public override bool CanSeek
		{
			get
			{
				return false;
			}
		}

		// Token: 0x1700064B RID: 1611
		// (get) Token: 0x0600269A RID: 9882 RVA: 0x000061C5 File Offset: 0x000043C5
		public override bool CanWrite
		{
			get
			{
				return false;
			}
		}

		// Token: 0x1700064C RID: 1612
		// (get) Token: 0x0600269B RID: 9883 RVA: 0x000AE2E8 File Offset: 0x000AC4E8
		public override long Length
		{
			get
			{
				return ValueUtilsSmi.GetBytesInternal(this._sink, this._getters, this._ordinal, this._metaData, 0L, null, 0, 0, false);
			}
		}

		// Token: 0x1700064D RID: 1613
		// (get) Token: 0x0600269C RID: 9884 RVA: 0x000AE318 File Offset: 0x000AC518
		// (set) Token: 0x0600269D RID: 9885 RVA: 0x000AE320 File Offset: 0x000AC520
		public override long Position
		{
			get
			{
				return this._readPosition;
			}
			set
			{
				throw SQL.StreamSeekNotSupported();
			}
		}

		// Token: 0x0600269E RID: 9886 RVA: 0x000AE327 File Offset: 0x000AC527
		public override void Flush()
		{
			throw SQL.StreamWriteNotSupported();
		}

		// Token: 0x0600269F RID: 9887 RVA: 0x000AE320 File Offset: 0x000AC520
		public override long Seek(long offset, SeekOrigin origin)
		{
			throw SQL.StreamSeekNotSupported();
		}

		// Token: 0x060026A0 RID: 9888 RVA: 0x000AE327 File Offset: 0x000AC527
		public override void SetLength(long value)
		{
			throw SQL.StreamWriteNotSupported();
		}

		// Token: 0x060026A1 RID: 9889 RVA: 0x000AE330 File Offset: 0x000AC530
		public override int Read(byte[] buffer, int offset, int count)
		{
			long bytesInternal = ValueUtilsSmi.GetBytesInternal(this._sink, this._getters, this._ordinal, this._metaData, this._readPosition, buffer, offset, count, false);
			this._readPosition += bytesInternal;
			return checked((int)bytesInternal);
		}

		// Token: 0x060026A2 RID: 9890 RVA: 0x000AE327 File Offset: 0x000AC527
		public override void Write(byte[] buffer, int offset, int count)
		{
			throw SQL.StreamWriteNotSupported();
		}

		// Token: 0x04001754 RID: 5972
		private SmiEventSink_Default _sink;

		// Token: 0x04001755 RID: 5973
		private ITypedGettersV3 _getters;

		// Token: 0x04001756 RID: 5974
		private int _ordinal;

		// Token: 0x04001757 RID: 5975
		private long _readPosition;

		// Token: 0x04001758 RID: 5976
		private SmiMetaData _metaData;
	}
}
