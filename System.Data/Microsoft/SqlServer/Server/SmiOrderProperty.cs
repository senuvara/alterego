﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.Diagnostics;

namespace Microsoft.SqlServer.Server
{
	// Token: 0x0200030A RID: 778
	internal class SmiOrderProperty : SmiMetaDataProperty
	{
		// Token: 0x060026CC RID: 9932 RVA: 0x000AEDCD File Offset: 0x000ACFCD
		internal SmiOrderProperty(IList<SmiOrderProperty.SmiColumnOrder> columnOrders)
		{
			this._columns = new ReadOnlyCollection<SmiOrderProperty.SmiColumnOrder>(columnOrders);
		}

		// Token: 0x17000661 RID: 1633
		internal SmiOrderProperty.SmiColumnOrder this[int ordinal]
		{
			get
			{
				if (this._columns.Count <= ordinal)
				{
					return new SmiOrderProperty.SmiColumnOrder
					{
						Order = SortOrder.Unspecified,
						SortOrdinal = -1
					};
				}
				return this._columns[ordinal];
			}
		}

		// Token: 0x060026CE RID: 9934 RVA: 0x00005E03 File Offset: 0x00004003
		[Conditional("DEBUG")]
		internal void CheckCount(int countToMatch)
		{
		}

		// Token: 0x040017AF RID: 6063
		private IList<SmiOrderProperty.SmiColumnOrder> _columns;

		// Token: 0x0200030B RID: 779
		internal struct SmiColumnOrder
		{
			// Token: 0x040017B0 RID: 6064
			internal int SortOrdinal;

			// Token: 0x040017B1 RID: 6065
			internal SortOrder Order;
		}
	}
}
