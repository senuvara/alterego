﻿using System;
using System.Data;
using System.Data.Common;
using System.Data.ProviderBase;
using System.Data.SqlClient;
using System.Data.SqlTypes;

namespace Microsoft.SqlServer.Server
{
	/// <summary>Represents a single row of data and its metadata. This class cannot be inherited.</summary>
	// Token: 0x02000312 RID: 786
	public class SqlDataRecord : IDataRecord
	{
		/// <summary>Gets the number of columns in the data row. This property is read-only.</summary>
		/// <returns>The number of columns in the data row as an integer.</returns>
		// Token: 0x1700066C RID: 1644
		// (get) Token: 0x06002758 RID: 10072 RVA: 0x000AEFA0 File Offset: 0x000AD1A0
		public virtual int FieldCount
		{
			get
			{
				this.EnsureSubclassOverride();
				return this._columnMetaData.Length;
			}
		}

		/// <summary>Returns the name of the column specified by the ordinal argument.</summary>
		/// <param name="ordinal">The zero-based ordinal of the column.</param>
		/// <returns>A <see cref="T:System.String" /> containing the column name.</returns>
		/// <exception cref="T:System.IndexOutOfRangeException">The <paramref name="ordinal" /> is less than 0 or greater than the number of columns (that is, <see cref="P:Microsoft.SqlServer.Server.SqlDataRecord.FieldCount" />). </exception>
		/// <exception cref="T:System.InvalidCastException">There is a type mismatch.</exception>
		// Token: 0x06002759 RID: 10073 RVA: 0x000AEFB0 File Offset: 0x000AD1B0
		public virtual string GetName(int ordinal)
		{
			this.EnsureSubclassOverride();
			return this.GetSqlMetaData(ordinal).Name;
		}

		/// <summary>Returns the name of the data type for the column specified by the ordinal argument.</summary>
		/// <param name="ordinal">The zero-based ordinal of the column.</param>
		/// <returns>A <see cref="T:System.String" /> that contains the data type of the column.</returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The <paramref name="ordinal" /> is less than 0 or greater than the number of columns (that is, <see cref="P:Microsoft.SqlServer.Server.SqlDataRecord.FieldCount" />). </exception>
		/// <exception cref="T:System.InvalidCastException">There is a type mismatch.</exception>
		// Token: 0x0600275A RID: 10074 RVA: 0x000AEFC4 File Offset: 0x000AD1C4
		public virtual string GetDataTypeName(int ordinal)
		{
			this.EnsureSubclassOverride();
			SqlMetaData sqlMetaData = this.GetSqlMetaData(ordinal);
			if (SqlDbType.Udt == sqlMetaData.SqlDbType)
			{
				return null;
			}
			return MetaType.GetMetaTypeFromSqlDbType(sqlMetaData.SqlDbType, false).TypeName;
		}

		/// <summary>Returns a <see cref="T:System.Type" /> object representing the common language runtime (CLR) type that maps to the SQL Server type of the column specified by the <paramref name="ordinal" /> argument.</summary>
		/// <param name="ordinal">The zero-based ordinal of the column.</param>
		/// <returns>The column type as a <see cref="T:System.Type" /> object.</returns>
		/// <exception cref="T:System.IndexOutOfRangeException">The <paramref name="ordinal" /> is less than 0 or greater than the number of columns (that is, <see cref="P:Microsoft.SqlServer.Server.SqlDataRecord.FieldCount" />). </exception>
		/// <exception cref="T:System.TypeLoadException">The column is of a user-defined type that is not available to the calling application domain.</exception>
		/// <exception cref="T:System.IO.FileNotFoundException">The column is of a user-defined type that is not available to the calling application domain.</exception>
		/// <exception cref="T:System.InvalidCastException">There is a type mismatch.</exception>
		// Token: 0x0600275B RID: 10075 RVA: 0x000AEFFC File Offset: 0x000AD1FC
		public virtual Type GetFieldType(int ordinal)
		{
			this.EnsureSubclassOverride();
			return MetaType.GetMetaTypeFromSqlDbType(this.GetSqlMetaData(ordinal).SqlDbType, false).ClassType;
		}

		/// <summary>Returns the common language runtime (CLR) type value for the column specified by the ordinal argument.</summary>
		/// <param name="ordinal">The zero-based ordinal of the column.</param>
		/// <returns>The CLR type value of the column specified by the ordinal.</returns>
		/// <exception cref="T:System.IndexOutOfRangeException">The <paramref name="ordinal" /> is less than 0 or greater than the number of columns (that is, <see cref="P:Microsoft.SqlServer.Server.SqlDataRecord.FieldCount" />).</exception>
		/// <exception cref="T:System.InvalidCastException">There is a type mismatch.</exception>
		// Token: 0x0600275C RID: 10076 RVA: 0x000AF01C File Offset: 0x000AD21C
		public virtual object GetValue(int ordinal)
		{
			this.EnsureSubclassOverride();
			SmiMetaData smiMetaData = this.GetSmiMetaData(ordinal);
			return ValueUtilsSmi.GetValue200(this._eventSink, this._recordBuffer, ordinal, smiMetaData);
		}

		/// <summary>Returns the values for all the columns in the record, expressed as common language runtime (CLR) types, in an array.</summary>
		/// <param name="values">The array into which to copy the values column values.</param>
		/// <returns>An <see cref="T:System.Int32" /> that indicates the number of columns copied.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="values" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.InvalidCastException">There is a type mismatch.</exception>
		// Token: 0x0600275D RID: 10077 RVA: 0x000AF04C File Offset: 0x000AD24C
		public virtual int GetValues(object[] values)
		{
			this.EnsureSubclassOverride();
			if (values == null)
			{
				throw ADP.ArgumentNull("values");
			}
			int num = (values.Length < this.FieldCount) ? values.Length : this.FieldCount;
			for (int i = 0; i < num; i++)
			{
				values[i] = this.GetValue(i);
			}
			return num;
		}

		/// <summary>Returns the column ordinal specified by the column name.</summary>
		/// <param name="name">The name of the column to look up.</param>
		/// <returns>The zero-based ordinal of the column as an integer.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="name" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.IndexOutOfRangeException">The column name could not be found.</exception>
		/// <exception cref="T:System.InvalidCastException">There is a type mismatch.</exception>
		// Token: 0x0600275E RID: 10078 RVA: 0x000AF09C File Offset: 0x000AD29C
		public virtual int GetOrdinal(string name)
		{
			this.EnsureSubclassOverride();
			if (this._fieldNameLookup == null)
			{
				string[] array = new string[this.FieldCount];
				for (int i = 0; i < array.Length; i++)
				{
					array[i] = this.GetSqlMetaData(i).Name;
				}
				this._fieldNameLookup = new FieldNameLookup(array, -1);
			}
			return this._fieldNameLookup.GetOrdinal(name);
		}

		/// <summary>Gets the common language runtime (CLR) type value for the column specified by the column <paramref name="ordinal" /> argument.</summary>
		/// <param name="ordinal">The zero-based ordinal of the column.</param>
		/// <returns>The CLR type value of the column specified by the <paramref name="ordinal" />.</returns>
		/// <exception cref="T:System.IndexOutOfRangeException">The <paramref name="ordinal" /> is less than 0 or greater than the number of columns (that is, <see cref="P:Microsoft.SqlServer.Server.SqlDataRecord.FieldCount" />).</exception>
		// Token: 0x1700066D RID: 1645
		public virtual object this[int ordinal]
		{
			get
			{
				this.EnsureSubclassOverride();
				return this.GetValue(ordinal);
			}
		}

		/// <summary>Gets the common language runtime (CLR) type value for the column specified by the column <paramref name="name" /> argument.</summary>
		/// <param name="name">The name of the column.</param>
		/// <returns>The CLR type value of the column specified by the <paramref name="name" />.</returns>
		// Token: 0x1700066E RID: 1646
		public virtual object this[string name]
		{
			get
			{
				this.EnsureSubclassOverride();
				return this.GetValue(this.GetOrdinal(name));
			}
		}

		/// <summary>Gets the value for the column specified by the ordinal as a <see cref="T:System.Boolean" />.</summary>
		/// <param name="ordinal">The zero-based ordinal of the column.</param>
		/// <returns>The column value as a <see cref="T:System.Boolean" />.</returns>
		/// <exception cref="T:System.IndexOutOfRangeException">The <paramref name="ordinal" /> is less than 0 or greater than the number of columns (that is, <see cref="P:Microsoft.SqlServer.Server.SqlDataRecord.FieldCount" />).</exception>
		/// <exception cref="T:System.Data.SqlTypes.SqlNullValueException">The column specified by <paramref name="ordinal" /> is null.</exception>
		/// <exception cref="T:System.InvalidCastException">There is a type mismatch.</exception>
		// Token: 0x06002761 RID: 10081 RVA: 0x000AF11D File Offset: 0x000AD31D
		public virtual bool GetBoolean(int ordinal)
		{
			this.EnsureSubclassOverride();
			return ValueUtilsSmi.GetBoolean(this._eventSink, this._recordBuffer, ordinal, this.GetSmiMetaData(ordinal));
		}

		/// <summary>Gets the value for the column specified by the ordinal as a <see cref="T:System.Byte" />.</summary>
		/// <param name="ordinal">The zero-based ordinal of the column.</param>
		/// <returns>The column value as a <see cref="T:System.Byte" />.</returns>
		/// <exception cref="T:System.IndexOutOfRangeException">The <paramref name="ordinal" /> is less than 0 or greater than the number of columns (that is, <see cref="P:Microsoft.SqlServer.Server.SqlDataRecord.FieldCount" />).</exception>
		/// <exception cref="T:System.Data.SqlTypes.SqlNullValueException">The column specified by <paramref name="ordinal" /> is null.</exception>
		/// <exception cref="T:System.InvalidCastException">There is a type mismatch.</exception>
		// Token: 0x06002762 RID: 10082 RVA: 0x000AF13E File Offset: 0x000AD33E
		public virtual byte GetByte(int ordinal)
		{
			this.EnsureSubclassOverride();
			return ValueUtilsSmi.GetByte(this._eventSink, this._recordBuffer, ordinal, this.GetSmiMetaData(ordinal));
		}

		/// <summary>Gets the value for the column specified by the ordinal as an array of <see cref="T:System.Byte" /> objects.</summary>
		/// <param name="ordinal">The zero-based ordinal of the column.</param>
		/// <param name="fieldOffset">The offset into the field value to start retrieving bytes.</param>
		/// <param name="buffer">The target buffer to which to copy bytes.</param>
		/// <param name="bufferOffset">The offset into the buffer to which to start copying bytes.</param>
		/// <param name="length">The number of bytes to copy to the buffer.</param>
		/// <returns>The number of bytes copied.</returns>
		/// <exception cref="T:System.IndexOutOfRangeException">The <paramref name="ordinal" /> is less than 0 or greater than the number of columns (that is, <see cref="P:Microsoft.SqlServer.Server.SqlDataRecord.FieldCount" />).</exception>
		/// <exception cref="T:System.Data.SqlTypes.SqlNullValueException">The column specified by <paramref name="ordinal" /> is null.</exception>
		/// <exception cref="T:System.InvalidCastException">There is a type mismatch.</exception>
		// Token: 0x06002763 RID: 10083 RVA: 0x000AF160 File Offset: 0x000AD360
		public virtual long GetBytes(int ordinal, long fieldOffset, byte[] buffer, int bufferOffset, int length)
		{
			this.EnsureSubclassOverride();
			return ValueUtilsSmi.GetBytes(this._eventSink, this._recordBuffer, ordinal, this.GetSmiMetaData(ordinal), fieldOffset, buffer, bufferOffset, length, true);
		}

		/// <summary>Gets the value for the column specified by the ordinal as a <see cref="T:System.Char" />.</summary>
		/// <param name="ordinal">The zero-based ordinal of the column.</param>
		/// <returns>The column value as a <see cref="T:System.Char" />.</returns>
		/// <exception cref="T:System.IndexOutOfRangeException">The <paramref name="ordinal" /> is less than 0 or greater than the number of columns (that is, <see cref="P:Microsoft.SqlServer.Server.SqlDataRecord.FieldCount" />).</exception>
		/// <exception cref="T:System.Data.SqlTypes.SqlNullValueException">The column specified by <paramref name="ordinal" /> is null.</exception>
		/// <exception cref="T:System.InvalidCastException">There is a type mismatch.</exception>
		// Token: 0x06002764 RID: 10084 RVA: 0x000AF193 File Offset: 0x000AD393
		public virtual char GetChar(int ordinal)
		{
			this.EnsureSubclassOverride();
			throw ADP.NotSupported();
		}

		/// <summary>Gets the value for the column specified by the ordinal as an array of <see cref="T:System.Char" /> objects.</summary>
		/// <param name="ordinal">The zero-based ordinal of the column.</param>
		/// <param name="fieldOffset">The offset into the field value to start retrieving characters.</param>
		/// <param name="buffer">The target buffer to copy chars to.</param>
		/// <param name="bufferOffset">The offset into the buffer to start copying chars to.</param>
		/// <param name="length">The number of chars to copy to the buffer.</param>
		/// <returns>The number of characters copied.</returns>
		/// <exception cref="T:System.IndexOutOfRangeException">The <paramref name="ordinal" /> is less than 0 or greater than the number of columns (that is, <see cref="P:Microsoft.SqlServer.Server.SqlDataRecord.FieldCount" />).</exception>
		/// <exception cref="T:System.Data.SqlTypes.SqlNullValueException">The column specified by <paramref name="ordinal" /> is null.</exception>
		/// <exception cref="T:System.InvalidCastException">There is a type mismatch.</exception>
		// Token: 0x06002765 RID: 10085 RVA: 0x000AF1A0 File Offset: 0x000AD3A0
		public virtual long GetChars(int ordinal, long fieldOffset, char[] buffer, int bufferOffset, int length)
		{
			this.EnsureSubclassOverride();
			return ValueUtilsSmi.GetChars(this._eventSink, this._recordBuffer, ordinal, this.GetSmiMetaData(ordinal), fieldOffset, buffer, bufferOffset, length);
		}

		/// <summary>Gets the value for the column specified by the ordinal as a <see cref="T:System.Guid" />.</summary>
		/// <param name="ordinal">The zero-based ordinal of the column.</param>
		/// <returns>The column value as a <see cref="T:System.Guid" />.</returns>
		/// <exception cref="T:System.IndexOutOfRangeException">The <paramref name="ordinal" /> is less than 0 or greater than the number of columns (that is, <see cref="P:Microsoft.SqlServer.Server.SqlDataRecord.FieldCount" />).</exception>
		/// <exception cref="T:System.Data.SqlTypes.SqlNullValueException">The column specified by <paramref name="ordinal" /> is null.</exception>
		/// <exception cref="T:System.InvalidCastException">There is a type mismatch.</exception>
		// Token: 0x06002766 RID: 10086 RVA: 0x000AF1C7 File Offset: 0x000AD3C7
		public virtual Guid GetGuid(int ordinal)
		{
			this.EnsureSubclassOverride();
			return ValueUtilsSmi.GetGuid(this._eventSink, this._recordBuffer, ordinal, this.GetSmiMetaData(ordinal));
		}

		/// <summary>Gets the value for the column specified by the ordinal as a <see cref="T:System.Int16" />.</summary>
		/// <param name="ordinal">The zero-based ordinal of the column.</param>
		/// <returns>The column value as a <see cref="T:System.Int16" />.</returns>
		/// <exception cref="T:System.IndexOutOfRangeException">The <paramref name="ordinal" /> is less than 0 or greater than the number of columns (that is, <see cref="P:Microsoft.SqlServer.Server.SqlDataRecord.FieldCount" />).</exception>
		/// <exception cref="T:System.Data.SqlTypes.SqlNullValueException">The column specified by <paramref name="ordinal" /> is null.</exception>
		/// <exception cref="T:System.InvalidCastException">There is a type mismatch.</exception>
		// Token: 0x06002767 RID: 10087 RVA: 0x000AF1E8 File Offset: 0x000AD3E8
		public virtual short GetInt16(int ordinal)
		{
			this.EnsureSubclassOverride();
			return ValueUtilsSmi.GetInt16(this._eventSink, this._recordBuffer, ordinal, this.GetSmiMetaData(ordinal));
		}

		/// <summary>Gets the value for the column specified by the ordinal as a <see cref="T:System.Int32" />.</summary>
		/// <param name="ordinal">The zero-based ordinal of the column.</param>
		/// <returns>The column value as a <see cref="T:System.Int32" />.</returns>
		/// <exception cref="T:System.IndexOutOfRangeException">The <paramref name="ordinal" /> is less than 0 or greater than the number of columns (that is, <see cref="P:Microsoft.SqlServer.Server.SqlDataRecord.FieldCount" />).</exception>
		/// <exception cref="T:System.Data.SqlTypes.SqlNullValueException">The column specified by <paramref name="ordinal" /> is null.</exception>
		/// <exception cref="T:System.InvalidCastException">There is a type mismatch.</exception>
		// Token: 0x06002768 RID: 10088 RVA: 0x000AF209 File Offset: 0x000AD409
		public virtual int GetInt32(int ordinal)
		{
			this.EnsureSubclassOverride();
			return ValueUtilsSmi.GetInt32(this._eventSink, this._recordBuffer, ordinal, this.GetSmiMetaData(ordinal));
		}

		/// <summary>Gets the value for the column specified by the ordinal as a <see cref="T:System.Int64" />.</summary>
		/// <param name="ordinal">The zero-based ordinal of the column.</param>
		/// <returns>The column value as a <see cref="T:System.Int64" />.</returns>
		/// <exception cref="T:System.IndexOutOfRangeException">The <paramref name="ordinal" /> is less than 0 or greater than the number of columns (that is, <see cref="P:Microsoft.SqlServer.Server.SqlDataRecord.FieldCount" />).</exception>
		/// <exception cref="T:System.Data.SqlTypes.SqlNullValueException">The column specified by <paramref name="ordinal" /> is null.</exception>
		/// <exception cref="T:System.InvalidCastException">There is a type mismatch.</exception>
		// Token: 0x06002769 RID: 10089 RVA: 0x000AF22A File Offset: 0x000AD42A
		public virtual long GetInt64(int ordinal)
		{
			this.EnsureSubclassOverride();
			return ValueUtilsSmi.GetInt64(this._eventSink, this._recordBuffer, ordinal, this.GetSmiMetaData(ordinal));
		}

		/// <summary>Gets the value for the column specified by the ordinal as a <see langword="float" />.</summary>
		/// <param name="ordinal">The zero-based ordinal of the column.</param>
		/// <returns>The column value as a <see langword="float" />.</returns>
		/// <exception cref="T:System.IndexOutOfRangeException">The <paramref name="ordinal" /> is less than 0 or greater than the number of columns (that is, <see cref="P:Microsoft.SqlServer.Server.SqlDataRecord.FieldCount" />).</exception>
		/// <exception cref="T:System.Data.SqlTypes.SqlNullValueException">The column specified by <paramref name="ordinal" /> is null.</exception>
		/// <exception cref="T:System.InvalidCastException">There is a type mismatch.</exception>
		// Token: 0x0600276A RID: 10090 RVA: 0x000AF24B File Offset: 0x000AD44B
		public virtual float GetFloat(int ordinal)
		{
			this.EnsureSubclassOverride();
			return ValueUtilsSmi.GetSingle(this._eventSink, this._recordBuffer, ordinal, this.GetSmiMetaData(ordinal));
		}

		/// <summary>Gets the value for the column specified by the ordinal as a <see cref="T:System.Double" />.</summary>
		/// <param name="ordinal">The zero-based ordinal of the column.</param>
		/// <returns>The column value as a <see cref="T:System.Double" />.</returns>
		/// <exception cref="T:System.IndexOutOfRangeException">The <paramref name="ordinal" /> is less than 0 or greater than the number of columns (that is, <see cref="P:Microsoft.SqlServer.Server.SqlDataRecord.FieldCount" />).</exception>
		/// <exception cref="T:System.Data.SqlTypes.SqlNullValueException">The column specified by <paramref name="ordinal" /> is null.</exception>
		/// <exception cref="T:System.InvalidCastException">There is a type mismatch.</exception>
		// Token: 0x0600276B RID: 10091 RVA: 0x000AF26C File Offset: 0x000AD46C
		public virtual double GetDouble(int ordinal)
		{
			this.EnsureSubclassOverride();
			return ValueUtilsSmi.GetDouble(this._eventSink, this._recordBuffer, ordinal, this.GetSmiMetaData(ordinal));
		}

		/// <summary>Gets the value for the column specified by the ordinal as a <see cref="T:System.String" />.</summary>
		/// <param name="ordinal">The zero-based ordinal of the column.</param>
		/// <returns>The column value as a <see cref="T:System.String" />.</returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The <paramref name="ordinal" /> is less than 0 or greater than the number of columns (that is, <see cref="P:Microsoft.SqlServer.Server.SqlDataRecord.FieldCount" />).</exception>
		/// <exception cref="T:System.InvalidCastException">There is a type mismatch.</exception>
		// Token: 0x0600276C RID: 10092 RVA: 0x000AF290 File Offset: 0x000AD490
		public virtual string GetString(int ordinal)
		{
			this.EnsureSubclassOverride();
			SmiMetaData smiMetaData = this.GetSmiMetaData(ordinal);
			if (this._usesStringStorageForXml && SqlDbType.Xml == smiMetaData.SqlDbType)
			{
				return ValueUtilsSmi.GetString(this._eventSink, this._recordBuffer, ordinal, SqlDataRecord.s_maxNVarCharForXml);
			}
			return ValueUtilsSmi.GetString(this._eventSink, this._recordBuffer, ordinal, this.GetSmiMetaData(ordinal));
		}

		/// <summary>Gets the value for the column specified by the ordinal as a <see cref="T:System.Decimal" />.</summary>
		/// <param name="ordinal">The zero-based ordinal of the column.</param>
		/// <returns>The column value as a <see cref="T:System.Decimal" />.</returns>
		/// <exception cref="T:System.IndexOutOfRangeException">The <paramref name="ordinal" /> is less than 0 or greater than the number of columns (that is, <see cref="P:Microsoft.SqlServer.Server.SqlDataRecord.FieldCount" />).</exception>
		/// <exception cref="T:System.Data.SqlTypes.SqlNullValueException">The column specified by <paramref name="ordinal" /> is null.</exception>
		/// <exception cref="T:System.InvalidCastException">There is a type mismatch.</exception>
		// Token: 0x0600276D RID: 10093 RVA: 0x000AF2EE File Offset: 0x000AD4EE
		public virtual decimal GetDecimal(int ordinal)
		{
			this.EnsureSubclassOverride();
			return ValueUtilsSmi.GetDecimal(this._eventSink, this._recordBuffer, ordinal, this.GetSmiMetaData(ordinal));
		}

		/// <summary>Gets the value for the column specified by the ordinal as a <see cref="T:System.DateTime" />.</summary>
		/// <param name="ordinal">The zero-based ordinal of the column.</param>
		/// <returns>The column value as a <see cref="T:System.DateTime" />.</returns>
		/// <exception cref="T:System.IndexOutOfRangeException">The <paramref name="ordinal" /> is less than 0 or greater than the number of columns (that is, <see cref="P:Microsoft.SqlServer.Server.SqlDataRecord.FieldCount" />).</exception>
		/// <exception cref="T:System.Data.SqlTypes.SqlNullValueException">The column specified by <paramref name="ordinal" /> is null.</exception>
		/// <exception cref="T:System.InvalidCastException">There is a type mismatch.</exception>
		// Token: 0x0600276E RID: 10094 RVA: 0x000AF30F File Offset: 0x000AD50F
		public virtual DateTime GetDateTime(int ordinal)
		{
			this.EnsureSubclassOverride();
			return ValueUtilsSmi.GetDateTime(this._eventSink, this._recordBuffer, ordinal, this.GetSmiMetaData(ordinal));
		}

		/// <summary>Returns the specified column’s data as a <see cref="T:System.DateTimeOffset" />.</summary>
		/// <param name="ordinal">The zero-based column ordinal.</param>
		/// <returns>The value of the specified column as a <see cref="T:System.DateTimeOffset" />.</returns>
		// Token: 0x0600276F RID: 10095 RVA: 0x000AF330 File Offset: 0x000AD530
		public virtual DateTimeOffset GetDateTimeOffset(int ordinal)
		{
			this.EnsureSubclassOverride();
			return ValueUtilsSmi.GetDateTimeOffset(this._eventSink, this._recordBuffer, ordinal, this.GetSmiMetaData(ordinal));
		}

		/// <summary>Returns the specified column’s data as a <see cref="T:System.TimeSpan" />.</summary>
		/// <param name="ordinal">The zero-based column ordinal.</param>
		/// <returns>The value of the specified column as a <see cref="T:System.TimeSpan" />.</returns>
		// Token: 0x06002770 RID: 10096 RVA: 0x000AF351 File Offset: 0x000AD551
		public virtual TimeSpan GetTimeSpan(int ordinal)
		{
			this.EnsureSubclassOverride();
			return ValueUtilsSmi.GetTimeSpan(this._eventSink, this._recordBuffer, ordinal, this.GetSmiMetaData(ordinal));
		}

		/// <summary>Returns true if the column specified by the column ordinal parameter is null.</summary>
		/// <param name="ordinal">The zero-based ordinal of the column.</param>
		/// <returns>
		///     <see langword="true" /> if the column is null; <see langword="false" /> otherwise.</returns>
		/// <exception cref="T:System.IndexOutOfRangeException">The <paramref name="ordinal" /> is less than 0 or greater than the number of columns (that is, <see cref="P:Microsoft.SqlServer.Server.SqlDataRecord.FieldCount" />).</exception>
		// Token: 0x06002771 RID: 10097 RVA: 0x000AF372 File Offset: 0x000AD572
		public virtual bool IsDBNull(int ordinal)
		{
			this.EnsureSubclassOverride();
			this.ThrowIfInvalidOrdinal(ordinal);
			return ValueUtilsSmi.IsDBNull(this._eventSink, this._recordBuffer, ordinal);
		}

		/// <summary>Returns a <see cref="T:Microsoft.SqlServer.Server.SqlMetaData" /> object, describing the metadata of the column specified by the column ordinal.</summary>
		/// <param name="ordinal">The zero-based ordinal of the column.</param>
		/// <returns>The column metadata as a <see cref="T:Microsoft.SqlServer.Server.SqlMetaData" /> object.</returns>
		/// <exception cref="T:System.IndexOutOfRangeException">The <paramref name="ordinal" /> is less than 0 or greater than the number of columns (that is, <see cref="P:Microsoft.SqlServer.Server.SqlDataRecord.FieldCount" />).</exception>
		/// <exception cref="T:System.InvalidCastException">There is a type mismatch.</exception>
		// Token: 0x06002772 RID: 10098 RVA: 0x000AF393 File Offset: 0x000AD593
		public virtual SqlMetaData GetSqlMetaData(int ordinal)
		{
			this.EnsureSubclassOverride();
			return this._columnMetaData[ordinal];
		}

		/// <summary>Returns a <see cref="T:System.Type" /> object that represents the type (as a SQL Server type, defined in <see cref="N:System.Data.SqlTypes" />) that maps to the SQL Server type of the column.</summary>
		/// <param name="ordinal">The zero-based ordinal of the column.</param>
		/// <returns>The column type as a <see cref="T:System.Type" /> object.</returns>
		/// <exception cref="T:System.IndexOutOfRangeException">The <paramref name="ordinal" /> is less than 0 or greater than the number of columns (that is, <see cref="P:Microsoft.SqlServer.Server.SqlDataRecord.FieldCount" />). </exception>
		/// <exception cref="T:System.TypeLoadException">The column is of a user-defined type that is not available to the calling application domain.</exception>
		/// <exception cref="T:System.IO.FileNotFoundException">The column is of a user-defined type that is not available to the calling application domain.</exception>
		/// <exception cref="T:System.InvalidCastException">There is a type mismatch.</exception>
		// Token: 0x06002773 RID: 10099 RVA: 0x000AF3A3 File Offset: 0x000AD5A3
		public virtual Type GetSqlFieldType(int ordinal)
		{
			this.EnsureSubclassOverride();
			return MetaType.GetMetaTypeFromSqlDbType(this.GetSqlMetaData(ordinal).SqlDbType, false).SqlType;
		}

		/// <summary>Returns the data value stored in the column, expressed as a SQL Server type, specified by the column ordinal.</summary>
		/// <param name="ordinal">The zero-based ordinal of the column.</param>
		/// <returns>The value of the column, expressed as a SQL Server type, as a <see cref="T:System.Object" />.</returns>
		/// <exception cref="T:System.IndexOutOfRangeException">The <paramref name="ordinal" /> is less than 0 or greater than the number of columns (that is, <see cref="P:Microsoft.SqlServer.Server.SqlDataRecord.FieldCount" />).</exception>
		/// <exception cref="T:System.InvalidCastException">There is a type mismatch.</exception>
		// Token: 0x06002774 RID: 10100 RVA: 0x000AF3C4 File Offset: 0x000AD5C4
		public virtual object GetSqlValue(int ordinal)
		{
			this.EnsureSubclassOverride();
			SmiMetaData smiMetaData = this.GetSmiMetaData(ordinal);
			return ValueUtilsSmi.GetSqlValue200(this._eventSink, this._recordBuffer, ordinal, smiMetaData);
		}

		/// <summary>Returns the values for all the columns in the record, expressed as SQL Server types, in an array.</summary>
		/// <param name="values">The array into which to copy the values column values. </param>
		/// <returns>An <see cref="T:System.Int32" /> that indicates the number of columns copied.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="values" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.InvalidCastException">There is a type mismatch.</exception>
		// Token: 0x06002775 RID: 10101 RVA: 0x000AF3F4 File Offset: 0x000AD5F4
		public virtual int GetSqlValues(object[] values)
		{
			this.EnsureSubclassOverride();
			if (values == null)
			{
				throw ADP.ArgumentNull("values");
			}
			int num = (values.Length < this.FieldCount) ? values.Length : this.FieldCount;
			for (int i = 0; i < num; i++)
			{
				values[i] = this.GetSqlValue(i);
			}
			return num;
		}

		/// <summary>Gets the value for the column specified by the ordinal as a <see cref="T:System.Data.SqlTypes.SqlBinary" />.</summary>
		/// <param name="ordinal">The zero-based ordinal of the column.</param>
		/// <returns>The column value as a <see cref="T:System.Data.SqlTypes.SqlBinary" />.</returns>
		/// <exception cref="T:System.IndexOutOfRangeException">The <paramref name="ordinal" /> is less than 0 or greater than the number of columns (that is, <see cref="P:Microsoft.SqlServer.Server.SqlDataRecord.FieldCount" />).</exception>
		/// <exception cref="T:System.InvalidCastException">There is a type mismatch.</exception>
		// Token: 0x06002776 RID: 10102 RVA: 0x000AF443 File Offset: 0x000AD643
		public virtual SqlBinary GetSqlBinary(int ordinal)
		{
			this.EnsureSubclassOverride();
			return ValueUtilsSmi.GetSqlBinary(this._eventSink, this._recordBuffer, ordinal, this.GetSmiMetaData(ordinal));
		}

		/// <summary>Gets the value for the column specified by the ordinal as a <see cref="T:System.Data.SqlTypes.SqlBytes" />.</summary>
		/// <param name="ordinal">The zero-based ordinal of the column.</param>
		/// <returns>The column value as a <see cref="T:System.Data.SqlTypes.SqlBytes" />.</returns>
		/// <exception cref="T:System.IndexOutOfRangeException">The <paramref name="ordinal" /> is less than 0 or greater than the number of columns (that is, <see cref="P:Microsoft.SqlServer.Server.SqlDataRecord.FieldCount" />).</exception>
		/// <exception cref="T:System.InvalidCastException">There is a type mismatch.</exception>
		// Token: 0x06002777 RID: 10103 RVA: 0x000AF464 File Offset: 0x000AD664
		public virtual SqlBytes GetSqlBytes(int ordinal)
		{
			this.EnsureSubclassOverride();
			return ValueUtilsSmi.GetSqlBytes(this._eventSink, this._recordBuffer, ordinal, this.GetSmiMetaData(ordinal));
		}

		/// <summary>Gets the value for the column specified by the ordinal as a <see cref="T:System.Data.SqlTypes.SqlXml" />.</summary>
		/// <param name="ordinal">The zero-based ordinal of the column.</param>
		/// <returns>The column value as a <see cref="T:System.Data.SqlTypes.SqlXml" />.</returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The <paramref name="ordinal" /> is less than 0 or greater than the number of columns (that is, <see cref="P:Microsoft.SqlServer.Server.SqlDataRecord.FieldCount" />).</exception>
		/// <exception cref="T:System.InvalidCastException">There is a type mismatch.</exception>
		// Token: 0x06002778 RID: 10104 RVA: 0x000AF485 File Offset: 0x000AD685
		public virtual SqlXml GetSqlXml(int ordinal)
		{
			this.EnsureSubclassOverride();
			return ValueUtilsSmi.GetSqlXml(this._eventSink, this._recordBuffer, ordinal, this.GetSmiMetaData(ordinal));
		}

		/// <summary>Gets the value for the column specified by the ordinal as a <see cref="T:System.Data.SqlTypes.SqlBoolean" />.</summary>
		/// <param name="ordinal">The zero-based ordinal of the column.</param>
		/// <returns>The column value as a <see cref="T:System.Data.SqlTypes.SqlBoolean" />.</returns>
		/// <exception cref="T:System.IndexOutOfRangeException">The <paramref name="ordinal" /> is less than 0 or greater than the number of columns (that is, <see cref="P:Microsoft.SqlServer.Server.SqlDataRecord.FieldCount" />).</exception>
		/// <exception cref="T:System.InvalidCastException">There is a type mismatch.</exception>
		// Token: 0x06002779 RID: 10105 RVA: 0x000AF4A6 File Offset: 0x000AD6A6
		public virtual SqlBoolean GetSqlBoolean(int ordinal)
		{
			this.EnsureSubclassOverride();
			return ValueUtilsSmi.GetSqlBoolean(this._eventSink, this._recordBuffer, ordinal, this.GetSmiMetaData(ordinal));
		}

		/// <summary>Gets the value for the column specified by the ordinal as a <see cref="T:System.Data.SqlTypes.SqlByte" />.</summary>
		/// <param name="ordinal">The zero-based ordinal of the column.</param>
		/// <returns>The column value as a <see cref="T:System.Data.SqlTypes.SqlByte" />.</returns>
		/// <exception cref="T:System.IndexOutOfRangeException">The <paramref name="ordinal" /> is less than 0 or greater than the number of columns (that is, <see cref="P:Microsoft.SqlServer.Server.SqlDataRecord.FieldCount" />).</exception>
		/// <exception cref="T:System.InvalidCastException">There is a type mismatch.</exception>
		// Token: 0x0600277A RID: 10106 RVA: 0x000AF4C7 File Offset: 0x000AD6C7
		public virtual SqlByte GetSqlByte(int ordinal)
		{
			this.EnsureSubclassOverride();
			return ValueUtilsSmi.GetSqlByte(this._eventSink, this._recordBuffer, ordinal, this.GetSmiMetaData(ordinal));
		}

		/// <summary>Gets the value for the column specified by the ordinal as a <see cref="T:System.Data.SqlTypes.SqlChars" />.</summary>
		/// <param name="ordinal">The zero-based ordinal of the column.</param>
		/// <returns>The column value as a <see cref="T:System.Data.SqlTypes.SqlChars" />.</returns>
		/// <exception cref="T:System.IndexOutOfRangeException">The <paramref name="ordinal" /> is less than 0 or greater than the number of columns (that is, <see cref="P:Microsoft.SqlServer.Server.SqlDataRecord.FieldCount" />).</exception>
		/// <exception cref="T:System.InvalidCastException">There is a type mismatch.</exception>
		// Token: 0x0600277B RID: 10107 RVA: 0x000AF4E8 File Offset: 0x000AD6E8
		public virtual SqlChars GetSqlChars(int ordinal)
		{
			this.EnsureSubclassOverride();
			return ValueUtilsSmi.GetSqlChars(this._eventSink, this._recordBuffer, ordinal, this.GetSmiMetaData(ordinal));
		}

		/// <summary>Gets the value for the column specified by the ordinal as a <see cref="T:System.Data.SqlTypes.SqlInt16" />.</summary>
		/// <param name="ordinal">The zero-based ordinal of the column.</param>
		/// <returns>The column value as a <see cref="T:System.Data.SqlTypes.SqlInt16" />.</returns>
		/// <exception cref="T:System.IndexOutOfRangeException">The <paramref name="ordinal" /> is less than 0 or greater than the number of columns (that is, <see cref="P:Microsoft.SqlServer.Server.SqlDataRecord.FieldCount" />).</exception>
		/// <exception cref="T:System.InvalidCastException">There is a type mismatch.</exception>
		// Token: 0x0600277C RID: 10108 RVA: 0x000AF509 File Offset: 0x000AD709
		public virtual SqlInt16 GetSqlInt16(int ordinal)
		{
			this.EnsureSubclassOverride();
			return ValueUtilsSmi.GetSqlInt16(this._eventSink, this._recordBuffer, ordinal, this.GetSmiMetaData(ordinal));
		}

		/// <summary>Gets the value for the column specified by the ordinal as a <see cref="T:System.Data.SqlTypes.SqlInt32" />.</summary>
		/// <param name="ordinal">The zero-based ordinal of the column.</param>
		/// <returns>The column value as a <see cref="T:System.Data.SqlTypes.SqlInt32" />.</returns>
		/// <exception cref="T:System.IndexOutOfRangeException">The <paramref name="ordinal" /> is less than 0 or greater than the number of columns (that is, <see cref="P:Microsoft.SqlServer.Server.SqlDataRecord.FieldCount" />).</exception>
		/// <exception cref="T:System.InvalidCastException">There is a type mismatch.</exception>
		// Token: 0x0600277D RID: 10109 RVA: 0x000AF52A File Offset: 0x000AD72A
		public virtual SqlInt32 GetSqlInt32(int ordinal)
		{
			this.EnsureSubclassOverride();
			return ValueUtilsSmi.GetSqlInt32(this._eventSink, this._recordBuffer, ordinal, this.GetSmiMetaData(ordinal));
		}

		/// <summary>Gets the value for the column specified by the ordinal as a <see cref="T:System.Data.SqlTypes.SqlInt64" />.</summary>
		/// <param name="ordinal">The zero-based ordinal of the column.</param>
		/// <returns>The column value as a <see cref="T:System.Data.SqlTypes.SqlInt64" />.</returns>
		/// <exception cref="T:System.IndexOutOfRangeException">The <paramref name="ordinal" /> is less than 0 or greater than the number of columns (that is, <see cref="P:Microsoft.SqlServer.Server.SqlDataRecord.FieldCount" />).</exception>
		/// <exception cref="T:System.InvalidCastException">There is a type mismatch.</exception>
		// Token: 0x0600277E RID: 10110 RVA: 0x000AF54B File Offset: 0x000AD74B
		public virtual SqlInt64 GetSqlInt64(int ordinal)
		{
			this.EnsureSubclassOverride();
			return ValueUtilsSmi.GetSqlInt64(this._eventSink, this._recordBuffer, ordinal, this.GetSmiMetaData(ordinal));
		}

		/// <summary>Gets the value for the column specified by the ordinal as a <see cref="T:System.Data.SqlTypes.SqlSingle" />.</summary>
		/// <param name="ordinal">The zero-based ordinal of the column.</param>
		/// <returns>The column value as a <see cref="T:System.Data.SqlTypes.SqlSingle" />.</returns>
		/// <exception cref="T:System.IndexOutOfRangeException">The <paramref name="ordinal" /> is less than 0 or greater than the number of columns (that is, <see cref="P:Microsoft.SqlServer.Server.SqlDataRecord.FieldCount" />).</exception>
		/// <exception cref="T:System.InvalidCastException">There is a type mismatch.</exception>
		// Token: 0x0600277F RID: 10111 RVA: 0x000AF56C File Offset: 0x000AD76C
		public virtual SqlSingle GetSqlSingle(int ordinal)
		{
			this.EnsureSubclassOverride();
			return ValueUtilsSmi.GetSqlSingle(this._eventSink, this._recordBuffer, ordinal, this.GetSmiMetaData(ordinal));
		}

		/// <summary>Gets the value for the column specified by the ordinal as a <see cref="T:System.Data.SqlTypes.SqlDouble" />.</summary>
		/// <param name="ordinal">The zero-based ordinal of the column.</param>
		/// <returns>The column value as a <see cref="T:System.Data.SqlTypes.SqlDouble" />.</returns>
		/// <exception cref="T:System.IndexOutOfRangeException">The <paramref name="ordinal" /> is less than 0 or greater than the number of columns (that is, <see cref="P:Microsoft.SqlServer.Server.SqlDataRecord.FieldCount" />).</exception>
		/// <exception cref="T:System.InvalidCastException">There is a type mismatch.</exception>
		// Token: 0x06002780 RID: 10112 RVA: 0x000AF58D File Offset: 0x000AD78D
		public virtual SqlDouble GetSqlDouble(int ordinal)
		{
			this.EnsureSubclassOverride();
			return ValueUtilsSmi.GetSqlDouble(this._eventSink, this._recordBuffer, ordinal, this.GetSmiMetaData(ordinal));
		}

		/// <summary>Gets the value for the column specified by the ordinal as a <see cref="T:System.Data.SqlTypes.SqlMoney" />.</summary>
		/// <param name="ordinal">The zero-based ordinal of the column.</param>
		/// <returns>The column value as a <see cref="T:System.Data.SqlTypes.SqlMoney" />.</returns>
		/// <exception cref="T:System.IndexOutOfRangeException">The <paramref name="ordinal" /> is less than 0 or greater than the number of columns (that is, <see cref="P:Microsoft.SqlServer.Server.SqlDataRecord.FieldCount" />).</exception>
		/// <exception cref="T:System.InvalidCastException">There is a type mismatch.</exception>
		// Token: 0x06002781 RID: 10113 RVA: 0x000AF5AE File Offset: 0x000AD7AE
		public virtual SqlMoney GetSqlMoney(int ordinal)
		{
			this.EnsureSubclassOverride();
			return ValueUtilsSmi.GetSqlMoney(this._eventSink, this._recordBuffer, ordinal, this.GetSmiMetaData(ordinal));
		}

		/// <summary>Gets the value for the column specified by the ordinal as a <see cref="T:System.Data.SqlTypes.SqlDateTime" />.</summary>
		/// <param name="ordinal">The zero-based ordinal of the column.</param>
		/// <returns>The column value as a <see cref="T:System.Data.SqlTypes.SqlDateTime" />.</returns>
		/// <exception cref="T:System.IndexOutOfRangeException">The <paramref name="ordinal" /> is less than 0 or greater than the number of columns (that is, <see cref="P:Microsoft.SqlServer.Server.SqlDataRecord.FieldCount" />).</exception>
		/// <exception cref="T:System.InvalidCastException">There is a type mismatch.</exception>
		// Token: 0x06002782 RID: 10114 RVA: 0x000AF5CF File Offset: 0x000AD7CF
		public virtual SqlDateTime GetSqlDateTime(int ordinal)
		{
			this.EnsureSubclassOverride();
			return ValueUtilsSmi.GetSqlDateTime(this._eventSink, this._recordBuffer, ordinal, this.GetSmiMetaData(ordinal));
		}

		/// <summary>Gets the value for the column specified by the ordinal as a <see cref="T:System.Data.SqlTypes.SqlDecimal" />.</summary>
		/// <param name="ordinal">The zero-based ordinal of the column.</param>
		/// <returns>The column value as a <see cref="T:System.Data.SqlTypes.SqlDecimal" />.</returns>
		/// <exception cref="T:System.IndexOutOfRangeException">The <paramref name="ordinal" /> is less than 0 or greater than the number of columns (that is, <see cref="P:Microsoft.SqlServer.Server.SqlDataRecord.FieldCount" />).</exception>
		/// <exception cref="T:System.InvalidCastException">There is a type mismatch.</exception>
		// Token: 0x06002783 RID: 10115 RVA: 0x000AF5F0 File Offset: 0x000AD7F0
		public virtual SqlDecimal GetSqlDecimal(int ordinal)
		{
			this.EnsureSubclassOverride();
			return ValueUtilsSmi.GetSqlDecimal(this._eventSink, this._recordBuffer, ordinal, this.GetSmiMetaData(ordinal));
		}

		/// <summary>Gets the value for the column specified by the ordinal as a <see cref="T:System.Data.SqlTypes.SqlString" />.</summary>
		/// <param name="ordinal">The zero-based ordinal of the column.</param>
		/// <returns>The column value as a <see cref="T:System.Data.SqlTypes.SqlString" />.</returns>
		/// <exception cref="T:System.IndexOutOfRangeException">The <paramref name="ordinal" /> is less than 0 or greater than the number of columns (that is, <see cref="P:Microsoft.SqlServer.Server.SqlDataRecord.FieldCount" />).</exception>
		/// <exception cref="T:System.InvalidCastException">There is a type mismatch.</exception>
		// Token: 0x06002784 RID: 10116 RVA: 0x000AF611 File Offset: 0x000AD811
		public virtual SqlString GetSqlString(int ordinal)
		{
			this.EnsureSubclassOverride();
			return ValueUtilsSmi.GetSqlString(this._eventSink, this._recordBuffer, ordinal, this.GetSmiMetaData(ordinal));
		}

		/// <summary>Gets the value for the column specified by the ordinal as a <see cref="T:System.Data.SqlTypes.SqlGuid" />.</summary>
		/// <param name="ordinal">The zero-based ordinal of the column.</param>
		/// <returns>The column value as a <see cref="T:System.Data.SqlTypes.SqlGuid" />.</returns>
		/// <exception cref="T:System.IndexOutOfRangeException">The <paramref name="ordinal" /> is less than 0 or greater than the number of columns (that is, <see cref="P:Microsoft.SqlServer.Server.SqlDataRecord.FieldCount" />).</exception>
		/// <exception cref="T:System.InvalidCastException">There is a type mismatch.</exception>
		// Token: 0x06002785 RID: 10117 RVA: 0x000AF632 File Offset: 0x000AD832
		public virtual SqlGuid GetSqlGuid(int ordinal)
		{
			this.EnsureSubclassOverride();
			return ValueUtilsSmi.GetSqlGuid(this._eventSink, this._recordBuffer, ordinal, this.GetSmiMetaData(ordinal));
		}

		/// <summary>Sets new values for all of the columns in the <see cref="T:Microsoft.SqlServer.Server.SqlDataRecord" />. These values are expressed as common language runtime (CLR) types.</summary>
		/// <param name="values">The array of new values, expressed as CLR types boxed as <see cref="T:System.Object" /> references, for the <see cref="T:Microsoft.SqlServer.Server.SqlDataRecord" /> instance.</param>
		/// <returns>The number of column values set as an integer.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="values" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentException">The size of values does not match the number of columns in the <see cref="T:Microsoft.SqlServer.Server.SqlDataRecord" /> instance.</exception>
		// Token: 0x06002786 RID: 10118 RVA: 0x000AF654 File Offset: 0x000AD854
		public virtual int SetValues(params object[] values)
		{
			this.EnsureSubclassOverride();
			if (values == null)
			{
				throw ADP.ArgumentNull("values");
			}
			int num = (values.Length > this.FieldCount) ? this.FieldCount : values.Length;
			ExtendedClrTypeCode[] array = new ExtendedClrTypeCode[num];
			for (int i = 0; i < num; i++)
			{
				SqlMetaData sqlMetaData = this.GetSqlMetaData(i);
				array[i] = MetaDataUtilsSmi.DetermineExtendedTypeCodeForUseWithSqlDbType(sqlMetaData.SqlDbType, false, values[i]);
				if (ExtendedClrTypeCode.Invalid == array[i])
				{
					throw ADP.InvalidCast();
				}
			}
			for (int j = 0; j < num; j++)
			{
				ValueUtilsSmi.SetCompatibleValueV200(this._eventSink, this._recordBuffer, j, this.GetSmiMetaData(j), values[j], array[j], 0, 0, null);
			}
			return num;
		}

		/// <summary>Sets a new value, expressed as a common language runtime (CLR) type, for the column specified by the column ordinal.</summary>
		/// <param name="ordinal">The zero-based ordinal of the column.</param>
		/// <param name="value">The new value for the specified column.</param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The <paramref name="ordinal" /> is less than 0 or greater than the number of columns (that is, <see cref="P:Microsoft.SqlServer.Server.SqlDataRecord.FieldCount" />).</exception>
		// Token: 0x06002787 RID: 10119 RVA: 0x000AF6FC File Offset: 0x000AD8FC
		public virtual void SetValue(int ordinal, object value)
		{
			this.EnsureSubclassOverride();
			ExtendedClrTypeCode extendedClrTypeCode = MetaDataUtilsSmi.DetermineExtendedTypeCodeForUseWithSqlDbType(this.GetSqlMetaData(ordinal).SqlDbType, false, value);
			if (ExtendedClrTypeCode.Invalid == extendedClrTypeCode)
			{
				throw ADP.InvalidCast();
			}
			ValueUtilsSmi.SetCompatibleValueV200(this._eventSink, this._recordBuffer, ordinal, this.GetSmiMetaData(ordinal), value, extendedClrTypeCode, 0, 0, null);
		}

		/// <summary>Sets the data stored in the column to the specified <see cref="T:System.Boolean" /> value.</summary>
		/// <param name="ordinal">The zero-based ordinal of the column.</param>
		/// <param name="value">The new value of the column.</param>
		// Token: 0x06002788 RID: 10120 RVA: 0x000AF74B File Offset: 0x000AD94B
		public virtual void SetBoolean(int ordinal, bool value)
		{
			this.EnsureSubclassOverride();
			ValueUtilsSmi.SetBoolean(this._eventSink, this._recordBuffer, ordinal, this.GetSmiMetaData(ordinal), value);
		}

		/// <summary>Sets the data stored in the column to the specified <see cref="T:System.Byte" /> value.</summary>
		/// <param name="ordinal">The zero-based ordinal of the column.</param>
		/// <param name="value">The new value of the column.</param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The <paramref name="ordinal" /> is less than 0 or greater than the number of columns (that is, <see cref="P:Microsoft.SqlServer.Server.SqlDataRecord.FieldCount" />).</exception>
		// Token: 0x06002789 RID: 10121 RVA: 0x000AF76D File Offset: 0x000AD96D
		public virtual void SetByte(int ordinal, byte value)
		{
			this.EnsureSubclassOverride();
			ValueUtilsSmi.SetByte(this._eventSink, this._recordBuffer, ordinal, this.GetSmiMetaData(ordinal), value);
		}

		/// <summary>Sets the data stored in the column to the specified array of <see cref="T:System.Byte" /> values.</summary>
		/// <param name="ordinal">The zero-based ordinal of the column.</param>
		/// <param name="fieldOffset">The offset into the field value to start copying bytes.</param>
		/// <param name="buffer">The target buffer from which to copy bytes.</param>
		/// <param name="bufferOffset">The offset into the buffer from which to start copying bytes.</param>
		/// <param name="length">The number of bytes to copy from the buffer.</param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The <paramref name="ordinal" /> is less than 0 or greater than the number of columns (that is, <see cref="P:Microsoft.SqlServer.Server.SqlDataRecord.FieldCount" />).</exception>
		// Token: 0x0600278A RID: 10122 RVA: 0x000AF78F File Offset: 0x000AD98F
		public virtual void SetBytes(int ordinal, long fieldOffset, byte[] buffer, int bufferOffset, int length)
		{
			this.EnsureSubclassOverride();
			ValueUtilsSmi.SetBytes(this._eventSink, this._recordBuffer, ordinal, this.GetSmiMetaData(ordinal), fieldOffset, buffer, bufferOffset, length);
		}

		/// <summary>Sets the data stored in the column to the specified <see cref="T:System.Char" /> value.</summary>
		/// <param name="ordinal">The zero-based ordinal of the column.</param>
		/// <param name="value">The new value of the column.</param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The <paramref name="ordinal" /> is less than 0 or greater than the number of columns (that is, <see cref="P:Microsoft.SqlServer.Server.SqlDataRecord.FieldCount" />).</exception>
		// Token: 0x0600278B RID: 10123 RVA: 0x000AF193 File Offset: 0x000AD393
		public virtual void SetChar(int ordinal, char value)
		{
			this.EnsureSubclassOverride();
			throw ADP.NotSupported();
		}

		/// <summary>Sets the data stored in the column to the specified array of <see cref="T:System.Char" /> values.</summary>
		/// <param name="ordinal">The zero-based ordinal of the column.</param>
		/// <param name="fieldOffset">The offset into the field value to start copying characters.</param>
		/// <param name="buffer">The target buffer from which to copy chars.</param>
		/// <param name="bufferOffset">The offset into the buffer from which to start copying chars.</param>
		/// <param name="length">The number of chars to copy from the buffer.</param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The <paramref name="ordinal" /> is less than 0 or greater than the number of columns (that is, <see cref="P:Microsoft.SqlServer.Server.SqlDataRecord.FieldCount" />).</exception>
		// Token: 0x0600278C RID: 10124 RVA: 0x000AF7B7 File Offset: 0x000AD9B7
		public virtual void SetChars(int ordinal, long fieldOffset, char[] buffer, int bufferOffset, int length)
		{
			this.EnsureSubclassOverride();
			ValueUtilsSmi.SetChars(this._eventSink, this._recordBuffer, ordinal, this.GetSmiMetaData(ordinal), fieldOffset, buffer, bufferOffset, length);
		}

		/// <summary>Sets the data stored in the column to the specified <see cref="T:System.Int16" /> value.</summary>
		/// <param name="ordinal">The zero-based ordinal of the column.</param>
		/// <param name="value">The new value of the column.</param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The <paramref name="ordinal" /> is less than 0 or greater than the number of columns (that is, <see cref="P:Microsoft.SqlServer.Server.SqlDataRecord.FieldCount" />).</exception>
		// Token: 0x0600278D RID: 10125 RVA: 0x000AF7DF File Offset: 0x000AD9DF
		public virtual void SetInt16(int ordinal, short value)
		{
			this.EnsureSubclassOverride();
			ValueUtilsSmi.SetInt16(this._eventSink, this._recordBuffer, ordinal, this.GetSmiMetaData(ordinal), value);
		}

		/// <summary>Sets the data stored in the column to the specified <see cref="T:System.Int32" /> value.</summary>
		/// <param name="ordinal">The zero-based ordinal of the column.</param>
		/// <param name="value">The new value of the column.</param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The <paramref name="ordinal" /> is less than 0 or greater than the number of columns (that is, <see cref="P:Microsoft.SqlServer.Server.SqlDataRecord.FieldCount" />).</exception>
		// Token: 0x0600278E RID: 10126 RVA: 0x000AF801 File Offset: 0x000ADA01
		public virtual void SetInt32(int ordinal, int value)
		{
			this.EnsureSubclassOverride();
			ValueUtilsSmi.SetInt32(this._eventSink, this._recordBuffer, ordinal, this.GetSmiMetaData(ordinal), value);
		}

		/// <summary>Sets the data stored in the column to the specified <see cref="T:System.Int64" /> value.</summary>
		/// <param name="ordinal">The zero-based ordinal of the column.</param>
		/// <param name="value">The new value of the column.</param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The <paramref name="ordinal" /> is less than 0 or greater than the number of columns (that is, <see cref="P:Microsoft.SqlServer.Server.SqlDataRecord.FieldCount" />).</exception>
		// Token: 0x0600278F RID: 10127 RVA: 0x000AF823 File Offset: 0x000ADA23
		public virtual void SetInt64(int ordinal, long value)
		{
			this.EnsureSubclassOverride();
			ValueUtilsSmi.SetInt64(this._eventSink, this._recordBuffer, ordinal, this.GetSmiMetaData(ordinal), value);
		}

		/// <summary>Sets the data stored in the column to the specified <see langword="float" /> value.</summary>
		/// <param name="ordinal">The zero-based ordinal of the column.</param>
		/// <param name="value">The new value of the column.</param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The <paramref name="ordinal" /> is less than 0 or greater than the number of columns (that is, <see cref="P:Microsoft.SqlServer.Server.SqlDataRecord.FieldCount" />).</exception>
		// Token: 0x06002790 RID: 10128 RVA: 0x000AF845 File Offset: 0x000ADA45
		public virtual void SetFloat(int ordinal, float value)
		{
			this.EnsureSubclassOverride();
			ValueUtilsSmi.SetSingle(this._eventSink, this._recordBuffer, ordinal, this.GetSmiMetaData(ordinal), value);
		}

		/// <summary>Sets the data stored in the column to the specified <see cref="T:System.Double" /> value.</summary>
		/// <param name="ordinal">The zero-based ordinal of the column.</param>
		/// <param name="value">The new value of the column.</param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The <paramref name="ordinal" /> is less than 0 or greater than the number of columns (that is, <see cref="P:Microsoft.SqlServer.Server.SqlDataRecord.FieldCount" />).</exception>
		// Token: 0x06002791 RID: 10129 RVA: 0x000AF867 File Offset: 0x000ADA67
		public virtual void SetDouble(int ordinal, double value)
		{
			this.EnsureSubclassOverride();
			ValueUtilsSmi.SetDouble(this._eventSink, this._recordBuffer, ordinal, this.GetSmiMetaData(ordinal), value);
		}

		/// <summary>Sets the data stored in the column to the specified <see cref="T:System.String" /> value.</summary>
		/// <param name="ordinal">The zero-based ordinal of the column.</param>
		/// <param name="value">The new value of the column.</param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The <paramref name="ordinal" /> is less than 0 or greater than the number of columns (that is, <see cref="P:Microsoft.SqlServer.Server.SqlDataRecord.FieldCount" />).</exception>
		// Token: 0x06002792 RID: 10130 RVA: 0x000AF889 File Offset: 0x000ADA89
		public virtual void SetString(int ordinal, string value)
		{
			this.EnsureSubclassOverride();
			ValueUtilsSmi.SetString(this._eventSink, this._recordBuffer, ordinal, this.GetSmiMetaData(ordinal), value);
		}

		/// <summary>Sets the data stored in the column to the specified <see cref="T:System.Decimal" /> value.</summary>
		/// <param name="ordinal">The zero-based ordinal of the column.</param>
		/// <param name="value">The new value of the column.</param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The <paramref name="ordinal" /> is less than 0 or greater than the number of columns (that is, <see cref="P:Microsoft.SqlServer.Server.SqlDataRecord.FieldCount" />).</exception>
		// Token: 0x06002793 RID: 10131 RVA: 0x000AF8AB File Offset: 0x000ADAAB
		public virtual void SetDecimal(int ordinal, decimal value)
		{
			this.EnsureSubclassOverride();
			ValueUtilsSmi.SetDecimal(this._eventSink, this._recordBuffer, ordinal, this.GetSmiMetaData(ordinal), value);
		}

		/// <summary>Sets the data stored in the column to the specified <see cref="T:System.DateTime" /> value.</summary>
		/// <param name="ordinal">The zero-based ordinal of the column.</param>
		/// <param name="value">The new value of the column.</param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The <paramref name="ordinal" /> is less than 0 or greater than the number of columns (that is, <see cref="P:Microsoft.SqlServer.Server.SqlDataRecord.FieldCount" />).</exception>
		// Token: 0x06002794 RID: 10132 RVA: 0x000AF8CD File Offset: 0x000ADACD
		public virtual void SetDateTime(int ordinal, DateTime value)
		{
			this.EnsureSubclassOverride();
			ValueUtilsSmi.SetDateTime(this._eventSink, this._recordBuffer, ordinal, this.GetSmiMetaData(ordinal), value);
		}

		/// <summary>Sets the value of the column specified to the <see cref="T:System.TimeSpan" />.</summary>
		/// <param name="ordinal">The zero-based ordinal of the column.</param>
		/// <param name="value">The new value of the column.</param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The <paramref name="ordinal" /> passed in is a negative number.</exception>
		/// <exception cref="T:System.ArgumentException">The <see cref="T:System.TimeSpan" /> value passed in is greater than 24 hours in length.</exception>
		// Token: 0x06002795 RID: 10133 RVA: 0x000AF8EF File Offset: 0x000ADAEF
		public virtual void SetTimeSpan(int ordinal, TimeSpan value)
		{
			this.EnsureSubclassOverride();
			ValueUtilsSmi.SetTimeSpan(this._eventSink, this._recordBuffer, ordinal, this.GetSmiMetaData(ordinal), value);
		}

		/// <summary>Sets the value of the column specified to the <see cref="T:System.DateTimeOffset" /> value.</summary>
		/// <param name="ordinal">The zero-based ordinal of the column.</param>
		/// <param name="value">The new value of the column.</param>
		// Token: 0x06002796 RID: 10134 RVA: 0x000AF911 File Offset: 0x000ADB11
		public virtual void SetDateTimeOffset(int ordinal, DateTimeOffset value)
		{
			this.EnsureSubclassOverride();
			ValueUtilsSmi.SetDateTimeOffset(this._eventSink, this._recordBuffer, ordinal, this.GetSmiMetaData(ordinal), value);
		}

		/// <summary>Sets the value in the specified column to <see cref="T:System.DBNull" />.</summary>
		/// <param name="ordinal">The zero-based ordinal of the column.</param>
		// Token: 0x06002797 RID: 10135 RVA: 0x000AF933 File Offset: 0x000ADB33
		public virtual void SetDBNull(int ordinal)
		{
			this.EnsureSubclassOverride();
			ValueUtilsSmi.SetDBNull(this._eventSink, this._recordBuffer, ordinal, true);
		}

		/// <summary>Sets the data stored in the column to the specified <see cref="T:System.Guid" /> value.</summary>
		/// <param name="ordinal">The zero-based ordinal of the column.</param>
		/// <param name="value">The new value of the column.</param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The <paramref name="ordinal" /> is less than 0 or greater than the number of columns (that is, <see cref="P:Microsoft.SqlServer.Server.SqlDataRecord.FieldCount" />).</exception>
		// Token: 0x06002798 RID: 10136 RVA: 0x000AF94E File Offset: 0x000ADB4E
		public virtual void SetGuid(int ordinal, Guid value)
		{
			this.EnsureSubclassOverride();
			ValueUtilsSmi.SetGuid(this._eventSink, this._recordBuffer, ordinal, this.GetSmiMetaData(ordinal), value);
		}

		/// <summary>Sets the data stored in the column to the specified <see cref="T:System.Data.SqlTypes.SqlBoolean" /> value.</summary>
		/// <param name="ordinal">The zero-based ordinal of the column.</param>
		/// <param name="value">The new value of the column.</param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The <paramref name="ordinal" /> is less than 0 or greater than the number of columns (that is, <see cref="P:Microsoft.SqlServer.Server.SqlDataRecord.FieldCount" />).</exception>
		// Token: 0x06002799 RID: 10137 RVA: 0x000AF970 File Offset: 0x000ADB70
		public virtual void SetSqlBoolean(int ordinal, SqlBoolean value)
		{
			this.EnsureSubclassOverride();
			ValueUtilsSmi.SetSqlBoolean(this._eventSink, this._recordBuffer, ordinal, this.GetSmiMetaData(ordinal), value);
		}

		/// <summary>Sets the data stored in the column to the specified <see cref="T:System.Data.SqlTypes.SqlByte" /> value.</summary>
		/// <param name="ordinal">The zero-based ordinal of the column.</param>
		/// <param name="value">The new value of the column.</param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The <paramref name="ordinal" /> is less than 0 or greater than the number of columns (that is, <see cref="P:Microsoft.SqlServer.Server.SqlDataRecord.FieldCount" />).</exception>
		// Token: 0x0600279A RID: 10138 RVA: 0x000AF992 File Offset: 0x000ADB92
		public virtual void SetSqlByte(int ordinal, SqlByte value)
		{
			this.EnsureSubclassOverride();
			ValueUtilsSmi.SetSqlByte(this._eventSink, this._recordBuffer, ordinal, this.GetSmiMetaData(ordinal), value);
		}

		/// <summary>Sets the data stored in the column to the specified <see cref="T:System.Data.SqlTypes.SqlInt16" /> value.</summary>
		/// <param name="ordinal">The zero-based ordinal of the column.</param>
		/// <param name="value">The new value of the column.</param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The <paramref name="ordinal" /> is less than 0 or greater than the number of columns (that is, <see cref="P:Microsoft.SqlServer.Server.SqlDataRecord.FieldCount" />).</exception>
		// Token: 0x0600279B RID: 10139 RVA: 0x000AF9B4 File Offset: 0x000ADBB4
		public virtual void SetSqlInt16(int ordinal, SqlInt16 value)
		{
			this.EnsureSubclassOverride();
			ValueUtilsSmi.SetSqlInt16(this._eventSink, this._recordBuffer, ordinal, this.GetSmiMetaData(ordinal), value);
		}

		/// <summary>Sets the data stored in the column to the specified <see cref="T:System.Data.SqlTypes.SqlInt32" /> value.</summary>
		/// <param name="ordinal">The zero-based ordinal of the column.</param>
		/// <param name="value">The new value of the column.</param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The <paramref name="ordinal" /> is less than 0 or greater than the number of columns (that is, <see cref="P:Microsoft.SqlServer.Server.SqlDataRecord.FieldCount" />).</exception>
		// Token: 0x0600279C RID: 10140 RVA: 0x000AF9D6 File Offset: 0x000ADBD6
		public virtual void SetSqlInt32(int ordinal, SqlInt32 value)
		{
			this.EnsureSubclassOverride();
			ValueUtilsSmi.SetSqlInt32(this._eventSink, this._recordBuffer, ordinal, this.GetSmiMetaData(ordinal), value);
		}

		/// <summary>Sets the data stored in the column to the specified <see cref="T:System.Data.SqlTypes.SqlInt64" /> value.</summary>
		/// <param name="ordinal">The zero-based ordinal of the column.</param>
		/// <param name="value">The new value of the column.</param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The <paramref name="ordinal" /> is less than 0 or greater than the number of columns (that is, <see cref="P:Microsoft.SqlServer.Server.SqlDataRecord.FieldCount" />).</exception>
		// Token: 0x0600279D RID: 10141 RVA: 0x000AF9F8 File Offset: 0x000ADBF8
		public virtual void SetSqlInt64(int ordinal, SqlInt64 value)
		{
			this.EnsureSubclassOverride();
			ValueUtilsSmi.SetSqlInt64(this._eventSink, this._recordBuffer, ordinal, this.GetSmiMetaData(ordinal), value);
		}

		/// <summary>Sets the data stored in the column to the specified <see cref="T:System.Data.SqlTypes.SqlSingle" /> value.</summary>
		/// <param name="ordinal">The zero-based ordinal of the column.</param>
		/// <param name="value">The new value of the column.</param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The <paramref name="ordinal" /> is less than 0 or greater than the number of columns (that is, <see cref="P:Microsoft.SqlServer.Server.SqlDataRecord.FieldCount" />).</exception>
		// Token: 0x0600279E RID: 10142 RVA: 0x000AFA1A File Offset: 0x000ADC1A
		public virtual void SetSqlSingle(int ordinal, SqlSingle value)
		{
			this.EnsureSubclassOverride();
			ValueUtilsSmi.SetSqlSingle(this._eventSink, this._recordBuffer, ordinal, this.GetSmiMetaData(ordinal), value);
		}

		/// <summary>Sets the data stored in the column to the specified <see cref="T:System.Data.SqlTypes.SqlDouble" /> value.</summary>
		/// <param name="ordinal">The zero-based ordinal of the column.</param>
		/// <param name="value">The new value of the column.</param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The <paramref name="ordinal" /> is less than 0 or greater than the number of columns (that is, <see cref="P:Microsoft.SqlServer.Server.SqlDataRecord.FieldCount" />).</exception>
		// Token: 0x0600279F RID: 10143 RVA: 0x000AFA3C File Offset: 0x000ADC3C
		public virtual void SetSqlDouble(int ordinal, SqlDouble value)
		{
			this.EnsureSubclassOverride();
			ValueUtilsSmi.SetSqlDouble(this._eventSink, this._recordBuffer, ordinal, this.GetSmiMetaData(ordinal), value);
		}

		/// <summary>Sets the data stored in the column to the specified <see cref="T:System.Data.SqlTypes.SqlMoney" /> value.</summary>
		/// <param name="ordinal">The zero-based ordinal of the column.</param>
		/// <param name="value">The new value of the column.</param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The <paramref name="ordinal" /> is less than 0 or greater than the number of columns (that is, <see cref="P:Microsoft.SqlServer.Server.SqlDataRecord.FieldCount" />).</exception>
		// Token: 0x060027A0 RID: 10144 RVA: 0x000AFA5E File Offset: 0x000ADC5E
		public virtual void SetSqlMoney(int ordinal, SqlMoney value)
		{
			this.EnsureSubclassOverride();
			ValueUtilsSmi.SetSqlMoney(this._eventSink, this._recordBuffer, ordinal, this.GetSmiMetaData(ordinal), value);
		}

		/// <summary>Sets the data stored in the column to the specified <see cref="T:System.Data.SqlTypes.SqlDateTime" /> value.</summary>
		/// <param name="ordinal">The zero-based ordinal of the column.</param>
		/// <param name="value">The new value of the column.</param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The <paramref name="ordinal" /> is less than 0 or greater than the number of columns (that is, <see cref="P:Microsoft.SqlServer.Server.SqlDataRecord.FieldCount" />).</exception>
		// Token: 0x060027A1 RID: 10145 RVA: 0x000AFA80 File Offset: 0x000ADC80
		public virtual void SetSqlDateTime(int ordinal, SqlDateTime value)
		{
			this.EnsureSubclassOverride();
			ValueUtilsSmi.SetSqlDateTime(this._eventSink, this._recordBuffer, ordinal, this.GetSmiMetaData(ordinal), value);
		}

		/// <summary>Sets the data stored in the column to the specified <see cref="T:System.Data.SqlTypes.SqlXml" /> value.</summary>
		/// <param name="ordinal">The zero-based ordinal of the column.</param>
		/// <param name="value">The new value of the column.</param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The <paramref name="ordinal" /> is less than 0 or greater than the number of columns (that is, <see cref="P:Microsoft.SqlServer.Server.SqlDataRecord.FieldCount" />).</exception>
		// Token: 0x060027A2 RID: 10146 RVA: 0x000AFAA2 File Offset: 0x000ADCA2
		public virtual void SetSqlXml(int ordinal, SqlXml value)
		{
			this.EnsureSubclassOverride();
			ValueUtilsSmi.SetSqlXml(this._eventSink, this._recordBuffer, ordinal, this.GetSmiMetaData(ordinal), value);
		}

		/// <summary>Sets the data stored in the column to the specified <see cref="T:System.Data.SqlTypes.SqlDecimal" /> value.</summary>
		/// <param name="ordinal">The zero-based ordinal of the column.</param>
		/// <param name="value">The new value of the column.</param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The <paramref name="ordinal" /> is less than 0 or greater than the number of columns (that is, <see cref="P:Microsoft.SqlServer.Server.SqlDataRecord.FieldCount" />).</exception>
		// Token: 0x060027A3 RID: 10147 RVA: 0x000AFAC4 File Offset: 0x000ADCC4
		public virtual void SetSqlDecimal(int ordinal, SqlDecimal value)
		{
			this.EnsureSubclassOverride();
			ValueUtilsSmi.SetSqlDecimal(this._eventSink, this._recordBuffer, ordinal, this.GetSmiMetaData(ordinal), value);
		}

		/// <summary>Sets the data stored in the column to the specified <see cref="T:System.Data.SqlTypes.SqlString" /> value.</summary>
		/// <param name="ordinal">The zero-based ordinal of the column.</param>
		/// <param name="value">The new value of the column.</param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The <paramref name="ordinal" /> is less than 0 or greater than the number of columns (that is, <see cref="P:Microsoft.SqlServer.Server.SqlDataRecord.FieldCount" />).</exception>
		// Token: 0x060027A4 RID: 10148 RVA: 0x000AFAE6 File Offset: 0x000ADCE6
		public virtual void SetSqlString(int ordinal, SqlString value)
		{
			this.EnsureSubclassOverride();
			ValueUtilsSmi.SetSqlString(this._eventSink, this._recordBuffer, ordinal, this.GetSmiMetaData(ordinal), value);
		}

		/// <summary>Sets the data stored in the column to the specified <see cref="T:System.Data.SqlTypes.SqlBinary" /> value.</summary>
		/// <param name="ordinal">The zero-based ordinal of the column.</param>
		/// <param name="value">The new value of the column.</param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The <paramref name="ordinal" /> is less than 0 or greater than the number of columns (that is, <see cref="P:Microsoft.SqlServer.Server.SqlDataRecord.FieldCount" />).</exception>
		// Token: 0x060027A5 RID: 10149 RVA: 0x000AFB08 File Offset: 0x000ADD08
		public virtual void SetSqlBinary(int ordinal, SqlBinary value)
		{
			this.EnsureSubclassOverride();
			ValueUtilsSmi.SetSqlBinary(this._eventSink, this._recordBuffer, ordinal, this.GetSmiMetaData(ordinal), value);
		}

		/// <summary>Sets the data stored in the column to the specified <see cref="T:System.Data.SqlTypes.SqlGuid" /> value.</summary>
		/// <param name="ordinal">The zero-based ordinal of the column.</param>
		/// <param name="value">The new value of the column.</param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The <paramref name="ordinal" /> is less than 0 or greater than the number of columns (that is, <see cref="P:Microsoft.SqlServer.Server.SqlDataRecord.FieldCount" />).</exception>
		// Token: 0x060027A6 RID: 10150 RVA: 0x000AFB2A File Offset: 0x000ADD2A
		public virtual void SetSqlGuid(int ordinal, SqlGuid value)
		{
			this.EnsureSubclassOverride();
			ValueUtilsSmi.SetSqlGuid(this._eventSink, this._recordBuffer, ordinal, this.GetSmiMetaData(ordinal), value);
		}

		/// <summary>Sets the data stored in the column to the specified <see cref="T:System.Data.SqlTypes.SqlChars" /> value.</summary>
		/// <param name="ordinal">The zero-based ordinal of the column.</param>
		/// <param name="value">The new value of the column.</param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The <paramref name="ordinal" /> is less than 0 or greater than the number of columns (that is, <see cref="P:Microsoft.SqlServer.Server.SqlDataRecord.FieldCount" />).</exception>
		// Token: 0x060027A7 RID: 10151 RVA: 0x000AFB4C File Offset: 0x000ADD4C
		public virtual void SetSqlChars(int ordinal, SqlChars value)
		{
			this.EnsureSubclassOverride();
			ValueUtilsSmi.SetSqlChars(this._eventSink, this._recordBuffer, ordinal, this.GetSmiMetaData(ordinal), value);
		}

		/// <summary>Sets the data stored in the column to the specified <see cref="T:System.Data.SqlTypes.SqlBytes" /> value.</summary>
		/// <param name="ordinal">The zero-based ordinal of the column.</param>
		/// <param name="value">The new value of the column.</param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The <paramref name="ordinal" /> is less than 0 or greater than the number of columns (that is, <see cref="P:Microsoft.SqlServer.Server.SqlDataRecord.FieldCount" />).</exception>
		// Token: 0x060027A8 RID: 10152 RVA: 0x000AFB6E File Offset: 0x000ADD6E
		public virtual void SetSqlBytes(int ordinal, SqlBytes value)
		{
			this.EnsureSubclassOverride();
			ValueUtilsSmi.SetSqlBytes(this._eventSink, this._recordBuffer, ordinal, this.GetSmiMetaData(ordinal), value);
		}

		/// <summary>Inititializes a new <see cref="T:Microsoft.SqlServer.Server.SqlDataRecord" /> instance with the schema based on the array of <see cref="T:Microsoft.SqlServer.Server.SqlMetaData" /> objects passed as an argument.</summary>
		/// <param name="metaData">An array of <see cref="T:Microsoft.SqlServer.Server.SqlMetaData" /> objects that describe each column in the <see cref="T:Microsoft.SqlServer.Server.SqlDataRecord" />.</param>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="metaData" /> is <see langword="null" />. </exception>
		// Token: 0x060027A9 RID: 10153 RVA: 0x000AFB90 File Offset: 0x000ADD90
		public SqlDataRecord(params SqlMetaData[] metaData)
		{
			if (metaData == null)
			{
				throw ADP.ArgumentNull("metaData");
			}
			this._columnMetaData = new SqlMetaData[metaData.Length];
			this._columnSmiMetaData = new SmiExtendedMetaData[metaData.Length];
			for (int i = 0; i < this._columnSmiMetaData.Length; i++)
			{
				if (metaData[i] == null)
				{
					throw ADP.ArgumentNull(string.Format("{0}[{1}]", "metaData", i));
				}
				this._columnMetaData[i] = metaData[i];
				this._columnSmiMetaData[i] = MetaDataUtilsSmi.SqlMetaDataToSmiExtendedMetaData(this._columnMetaData[i]);
			}
			this._eventSink = new SmiEventSink_Default();
			this._recordBuffer = new MemoryRecordBuffer(this._columnSmiMetaData);
			this._usesStringStorageForXml = true;
			this._eventSink.ProcessMessagesAndThrow();
		}

		// Token: 0x060027AA RID: 10154 RVA: 0x000AFC50 File Offset: 0x000ADE50
		internal SqlDataRecord(SmiRecordBuffer recordBuffer, params SmiExtendedMetaData[] metaData)
		{
			this._columnMetaData = new SqlMetaData[metaData.Length];
			this._columnSmiMetaData = new SmiExtendedMetaData[metaData.Length];
			for (int i = 0; i < this._columnSmiMetaData.Length; i++)
			{
				this._columnSmiMetaData[i] = metaData[i];
				this._columnMetaData[i] = MetaDataUtilsSmi.SmiExtendedMetaDataToSqlMetaData(this._columnSmiMetaData[i]);
			}
			this._eventSink = new SmiEventSink_Default();
			this._recordBuffer = recordBuffer;
			this._eventSink.ProcessMessagesAndThrow();
		}

		// Token: 0x1700066F RID: 1647
		// (get) Token: 0x060027AB RID: 10155 RVA: 0x000AFCCF File Offset: 0x000ADECF
		internal SmiRecordBuffer RecordBuffer
		{
			get
			{
				return this._recordBuffer;
			}
		}

		// Token: 0x060027AC RID: 10156 RVA: 0x000AFCD7 File Offset: 0x000ADED7
		internal SqlMetaData[] InternalGetMetaData()
		{
			return this._columnMetaData;
		}

		// Token: 0x060027AD RID: 10157 RVA: 0x000AFCDF File Offset: 0x000ADEDF
		internal SmiExtendedMetaData[] InternalGetSmiMetaData()
		{
			return this._columnSmiMetaData;
		}

		// Token: 0x060027AE RID: 10158 RVA: 0x000AFCE7 File Offset: 0x000ADEE7
		internal SmiExtendedMetaData GetSmiMetaData(int ordinal)
		{
			return this._columnSmiMetaData[ordinal];
		}

		// Token: 0x060027AF RID: 10159 RVA: 0x000AFCF1 File Offset: 0x000ADEF1
		internal void ThrowIfInvalidOrdinal(int ordinal)
		{
			if (0 > ordinal || this.FieldCount <= ordinal)
			{
				throw ADP.IndexOutOfRange(ordinal);
			}
		}

		// Token: 0x060027B0 RID: 10160 RVA: 0x000AFD07 File Offset: 0x000ADF07
		private void EnsureSubclassOverride()
		{
			if (this._recordBuffer == null)
			{
				throw SQL.SubclassMustOverride();
			}
		}

		// Token: 0x060027B1 RID: 10161 RVA: 0x0005CDBE File Offset: 0x0005AFBE
		public IDataReader GetData(int i)
		{
			throw ADP.NotSupported();
		}

		// Token: 0x060027B2 RID: 10162 RVA: 0x000AFD17 File Offset: 0x000ADF17
		// Note: this type is marked as 'beforefieldinit'.
		static SqlDataRecord()
		{
		}

		// Token: 0x040017CE RID: 6094
		private SmiRecordBuffer _recordBuffer;

		// Token: 0x040017CF RID: 6095
		private SmiExtendedMetaData[] _columnSmiMetaData;

		// Token: 0x040017D0 RID: 6096
		private SmiEventSink_Default _eventSink;

		// Token: 0x040017D1 RID: 6097
		private SqlMetaData[] _columnMetaData;

		// Token: 0x040017D2 RID: 6098
		private FieldNameLookup _fieldNameLookup;

		// Token: 0x040017D3 RID: 6099
		private bool _usesStringStorageForXml;

		// Token: 0x040017D4 RID: 6100
		private static readonly SmiMetaData s_maxNVarCharForXml = new SmiMetaData(SqlDbType.NVarChar, -1L, SmiMetaData.DefaultNVarChar_NoCollation.Precision, SmiMetaData.DefaultNVarChar_NoCollation.Scale, SmiMetaData.DefaultNVarChar.LocaleId, SmiMetaData.DefaultNVarChar.CompareOptions);
	}
}
