﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;

namespace Microsoft.SqlServer.Server
{
	// Token: 0x0200030C RID: 780
	internal class SmiDefaultFieldsProperty : SmiMetaDataProperty
	{
		// Token: 0x060026CF RID: 9935 RVA: 0x000AEE25 File Offset: 0x000AD025
		internal SmiDefaultFieldsProperty(IList<bool> defaultFields)
		{
			this._defaults = new ReadOnlyCollection<bool>(defaultFields);
		}

		// Token: 0x17000662 RID: 1634
		internal bool this[int ordinal]
		{
			get
			{
				return this._defaults.Count > ordinal && this._defaults[ordinal];
			}
		}

		// Token: 0x060026D1 RID: 9937 RVA: 0x00005E03 File Offset: 0x00004003
		[Conditional("DEBUG")]
		internal void CheckCount(int countToMatch)
		{
		}

		// Token: 0x040017B2 RID: 6066
		private IList<bool> _defaults;
	}
}
