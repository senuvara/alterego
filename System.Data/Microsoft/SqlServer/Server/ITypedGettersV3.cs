﻿using System;
using System.Data.SqlTypes;

namespace Microsoft.SqlServer.Server
{
	// Token: 0x020002F9 RID: 761
	internal interface ITypedGettersV3
	{
		// Token: 0x0600260F RID: 9743
		bool IsDBNull(SmiEventSink sink, int ordinal);

		// Token: 0x06002610 RID: 9744
		SmiMetaData GetVariantType(SmiEventSink sink, int ordinal);

		// Token: 0x06002611 RID: 9745
		bool GetBoolean(SmiEventSink sink, int ordinal);

		// Token: 0x06002612 RID: 9746
		byte GetByte(SmiEventSink sink, int ordinal);

		// Token: 0x06002613 RID: 9747
		long GetBytesLength(SmiEventSink sink, int ordinal);

		// Token: 0x06002614 RID: 9748
		int GetBytes(SmiEventSink sink, int ordinal, long fieldOffset, byte[] buffer, int bufferOffset, int length);

		// Token: 0x06002615 RID: 9749
		long GetCharsLength(SmiEventSink sink, int ordinal);

		// Token: 0x06002616 RID: 9750
		int GetChars(SmiEventSink sink, int ordinal, long fieldOffset, char[] buffer, int bufferOffset, int length);

		// Token: 0x06002617 RID: 9751
		string GetString(SmiEventSink sink, int ordinal);

		// Token: 0x06002618 RID: 9752
		short GetInt16(SmiEventSink sink, int ordinal);

		// Token: 0x06002619 RID: 9753
		int GetInt32(SmiEventSink sink, int ordinal);

		// Token: 0x0600261A RID: 9754
		long GetInt64(SmiEventSink sink, int ordinal);

		// Token: 0x0600261B RID: 9755
		float GetSingle(SmiEventSink sink, int ordinal);

		// Token: 0x0600261C RID: 9756
		double GetDouble(SmiEventSink sink, int ordinal);

		// Token: 0x0600261D RID: 9757
		SqlDecimal GetSqlDecimal(SmiEventSink sink, int ordinal);

		// Token: 0x0600261E RID: 9758
		DateTime GetDateTime(SmiEventSink sink, int ordinal);

		// Token: 0x0600261F RID: 9759
		Guid GetGuid(SmiEventSink sink, int ordinal);
	}
}
