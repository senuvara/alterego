﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlTypes;

namespace Microsoft.SqlServer.Server
{
	// Token: 0x02000302 RID: 770
	internal class SmiExtendedMetaData : SmiMetaData
	{
		// Token: 0x060026B4 RID: 9908 RVA: 0x000AEAD8 File Offset: 0x000ACCD8
		internal SmiExtendedMetaData(SqlDbType dbType, long maxLength, byte precision, byte scale, long localeId, SqlCompareOptions compareOptions, string name, string typeSpecificNamePart1, string typeSpecificNamePart2, string typeSpecificNamePart3) : this(dbType, maxLength, precision, scale, localeId, compareOptions, false, null, null, name, typeSpecificNamePart1, typeSpecificNamePart2, typeSpecificNamePart3)
		{
		}

		// Token: 0x060026B5 RID: 9909 RVA: 0x000AEB00 File Offset: 0x000ACD00
		internal SmiExtendedMetaData(SqlDbType dbType, long maxLength, byte precision, byte scale, long localeId, SqlCompareOptions compareOptions, bool isMultiValued, IList<SmiExtendedMetaData> fieldMetaData, SmiMetaDataPropertyCollection extendedProperties, string name, string typeSpecificNamePart1, string typeSpecificNamePart2, string typeSpecificNamePart3) : base(dbType, maxLength, precision, scale, localeId, compareOptions, isMultiValued, fieldMetaData, extendedProperties)
		{
			this._name = name;
			this._typeSpecificNamePart1 = typeSpecificNamePart1;
			this._typeSpecificNamePart2 = typeSpecificNamePart2;
			this._typeSpecificNamePart3 = typeSpecificNamePart3;
		}

		// Token: 0x17000658 RID: 1624
		// (get) Token: 0x060026B6 RID: 9910 RVA: 0x000AEB42 File Offset: 0x000ACD42
		internal string Name
		{
			get
			{
				return this._name;
			}
		}

		// Token: 0x17000659 RID: 1625
		// (get) Token: 0x060026B7 RID: 9911 RVA: 0x000AEB4A File Offset: 0x000ACD4A
		internal string TypeSpecificNamePart1
		{
			get
			{
				return this._typeSpecificNamePart1;
			}
		}

		// Token: 0x1700065A RID: 1626
		// (get) Token: 0x060026B8 RID: 9912 RVA: 0x000AEB52 File Offset: 0x000ACD52
		internal string TypeSpecificNamePart2
		{
			get
			{
				return this._typeSpecificNamePart2;
			}
		}

		// Token: 0x1700065B RID: 1627
		// (get) Token: 0x060026B9 RID: 9913 RVA: 0x000AEB5A File Offset: 0x000ACD5A
		internal string TypeSpecificNamePart3
		{
			get
			{
				return this._typeSpecificNamePart3;
			}
		}

		// Token: 0x04001791 RID: 6033
		private string _name;

		// Token: 0x04001792 RID: 6034
		private string _typeSpecificNamePart1;

		// Token: 0x04001793 RID: 6035
		private string _typeSpecificNamePart2;

		// Token: 0x04001794 RID: 6036
		private string _typeSpecificNamePart3;
	}
}
