﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;

namespace Microsoft.SqlServer.Server
{
	// Token: 0x02000309 RID: 777
	internal class SmiUniqueKeyProperty : SmiMetaDataProperty
	{
		// Token: 0x060026C9 RID: 9929 RVA: 0x000AED9B File Offset: 0x000ACF9B
		internal SmiUniqueKeyProperty(IList<bool> columnIsKey)
		{
			this._columns = new ReadOnlyCollection<bool>(columnIsKey);
		}

		// Token: 0x17000660 RID: 1632
		internal bool this[int ordinal]
		{
			get
			{
				return this._columns.Count > ordinal && this._columns[ordinal];
			}
		}

		// Token: 0x060026CB RID: 9931 RVA: 0x00005E03 File Offset: 0x00004003
		[Conditional("DEBUG")]
		internal void CheckCount(int countToMatch)
		{
		}

		// Token: 0x040017AE RID: 6062
		private IList<bool> _columns;
	}
}
