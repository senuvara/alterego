﻿using System;
using System.Security.Principal;
using Unity;

namespace Microsoft.SqlServer.Server
{
	/// <summary>Represents an abstraction of the caller's context, which provides access to the <see cref="T:Microsoft.SqlServer.Server.SqlPipe" />, <see cref="T:Microsoft.SqlServer.Server.SqlTriggerContext" />, and <see cref="T:System.Security.Principal.WindowsIdentity" /> objects. This class cannot be inherited.</summary>
	// Token: 0x0200038C RID: 908
	public sealed class SqlContext
	{
		// Token: 0x06002B4F RID: 11087 RVA: 0x00010458 File Offset: 0x0000E658
		internal SqlContext()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Specifies whether the calling code is running within SQL Server, and if the context connection can be accessed.</summary>
		/// <returns>
		///     <see langword="True" /> if the context connection is available and the other <see cref="T:Microsoft.SqlServer.Server.SqlContext" /> members can be accessed.</returns>
		// Token: 0x17000733 RID: 1843
		// (get) Token: 0x06002B50 RID: 11088 RVA: 0x000B778C File Offset: 0x000B598C
		public static bool IsAvailable
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
		}

		/// <summary>Gets the pipe object that allows the caller to send result sets, messages, and the results of executing commands back to the client.</summary>
		/// <returns>An instance of <see cref="T:Microsoft.SqlServer.Server.SqlPipe" /> if a pipe is available, or <see langword="null" /> if called in a context where pipe is not available (for example, in a user-defined function).</returns>
		// Token: 0x17000734 RID: 1844
		// (get) Token: 0x06002B51 RID: 11089 RVA: 0x00051759 File Offset: 0x0004F959
		public static SqlPipe Pipe
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the trigger context used to provide the caller with information about what caused the trigger to fire, and a map of the columns that were updated.</summary>
		/// <returns>An instance of <see cref="T:Microsoft.SqlServer.Server.SqlTriggerContext" /> if a trigger context is available, or <see langword="null" /> if called outside of a trigger invocation.</returns>
		// Token: 0x17000735 RID: 1845
		// (get) Token: 0x06002B52 RID: 11090 RVA: 0x00051759 File Offset: 0x0004F959
		public static SqlTriggerContext TriggerContext
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>The Microsoft Windows identity of the caller.</summary>
		/// <returns>A <see cref="T:System.Security.Principal.WindowsIdentity" /> instance representing the Windows identity of the caller, or <see langword="null" /> if the client was authenticated using SQL Server Authentication. </returns>
		// Token: 0x17000736 RID: 1846
		// (get) Token: 0x06002B53 RID: 11091 RVA: 0x00051759 File Offset: 0x0004F959
		public static WindowsIdentity WindowsIdentity
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}
	}
}
