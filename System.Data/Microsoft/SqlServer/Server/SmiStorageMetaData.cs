﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlTypes;

namespace Microsoft.SqlServer.Server
{
	// Token: 0x02000304 RID: 772
	internal class SmiStorageMetaData : SmiExtendedMetaData
	{
		// Token: 0x060026BC RID: 9916 RVA: 0x000AEBA0 File Offset: 0x000ACDA0
		internal SmiStorageMetaData(SqlDbType dbType, long maxLength, byte precision, byte scale, long localeId, SqlCompareOptions compareOptions, bool isMultiValued, IList<SmiExtendedMetaData> fieldMetaData, SmiMetaDataPropertyCollection extendedProperties, string name, string typeSpecificNamePart1, string typeSpecificNamePart2, string typeSpecificNamePart3, bool allowsDBNull, string serverName, string catalogName, string schemaName, string tableName, string columnName, SqlBoolean isKey, bool isIdentity, bool isColumnSet) : base(dbType, maxLength, precision, scale, localeId, compareOptions, isMultiValued, fieldMetaData, extendedProperties, name, typeSpecificNamePart1, typeSpecificNamePart2, typeSpecificNamePart3)
		{
			this._allowsDBNull = allowsDBNull;
			this._serverName = serverName;
			this._catalogName = catalogName;
			this._schemaName = schemaName;
			this._tableName = tableName;
			this._columnName = columnName;
			this._isKey = isKey;
			this._isIdentity = isIdentity;
			this._isColumnSet = isColumnSet;
		}

		// Token: 0x1700065D RID: 1629
		// (get) Token: 0x060026BD RID: 9917 RVA: 0x000AEC12 File Offset: 0x000ACE12
		internal SqlBoolean IsKey
		{
			get
			{
				return this._isKey;
			}
		}

		// Token: 0x04001796 RID: 6038
		private bool _allowsDBNull;

		// Token: 0x04001797 RID: 6039
		private string _serverName;

		// Token: 0x04001798 RID: 6040
		private string _catalogName;

		// Token: 0x04001799 RID: 6041
		private string _schemaName;

		// Token: 0x0400179A RID: 6042
		private string _tableName;

		// Token: 0x0400179B RID: 6043
		private string _columnName;

		// Token: 0x0400179C RID: 6044
		private SqlBoolean _isKey;

		// Token: 0x0400179D RID: 6045
		private bool _isIdentity;

		// Token: 0x0400179E RID: 6046
		private bool _isColumnSet;
	}
}
