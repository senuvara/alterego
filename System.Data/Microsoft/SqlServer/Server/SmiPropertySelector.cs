﻿using System;

namespace Microsoft.SqlServer.Server
{
	// Token: 0x02000306 RID: 774
	internal enum SmiPropertySelector
	{
		// Token: 0x040017A4 RID: 6052
		DefaultFields,
		// Token: 0x040017A5 RID: 6053
		SortOrder,
		// Token: 0x040017A6 RID: 6054
		UniqueKey
	}
}
