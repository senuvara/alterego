﻿using System;
using System.Data.Common;

namespace Microsoft.SqlServer.Server
{
	/// <summary>Used to mark a type definition in an assembly as a user-defined type (UDT) in SQL Server. The properties on the attribute reflect the physical characteristics used when the type is registered with SQL Server. This class cannot be inherited.</summary>
	// Token: 0x02000323 RID: 803
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct, AllowMultiple = false, Inherited = true)]
	public sealed class SqlUserDefinedTypeAttribute : Attribute
	{
		/// <summary>A required attribute on a user-defined type (UDT), used to confirm that the given type is a UDT and to indicate the storage format of the UDT.</summary>
		/// <param name="format">One of the <see cref="T:Microsoft.SqlServer.Server.Format" /> values representing the serialization format of the type.</param>
		// Token: 0x06002920 RID: 10528 RVA: 0x000B68C4 File Offset: 0x000B4AC4
		public SqlUserDefinedTypeAttribute(Format format)
		{
			if (format == Format.Unknown)
			{
				throw ADP.NotSupportedUserDefinedTypeSerializationFormat(format, "format");
			}
			if (format - Format.Native > 1)
			{
				throw ADP.InvalidUserDefinedTypeSerializationFormat(format);
			}
			this.m_format = format;
		}

		/// <summary>The maximum size of the instance, in bytes.</summary>
		/// <returns>An <see cref="T:System.Int32" /> value representing the maximum size of the instance.</returns>
		// Token: 0x170006B1 RID: 1713
		// (get) Token: 0x06002921 RID: 10529 RVA: 0x000B68F1 File Offset: 0x000B4AF1
		// (set) Token: 0x06002922 RID: 10530 RVA: 0x000B68F9 File Offset: 0x000B4AF9
		public int MaxByteSize
		{
			get
			{
				return this.m_MaxByteSize;
			}
			set
			{
				if (value < -1)
				{
					throw ADP.ArgumentOutOfRange("MaxByteSize");
				}
				this.m_MaxByteSize = value;
			}
		}

		/// <summary>Indicates whether all instances of this user-defined type are the same length.</summary>
		/// <returns>
		///     <see langword="true" /> if all instances of this type are the same length; otherwise <see langword="false" />.</returns>
		// Token: 0x170006B2 RID: 1714
		// (get) Token: 0x06002923 RID: 10531 RVA: 0x000B6911 File Offset: 0x000B4B11
		// (set) Token: 0x06002924 RID: 10532 RVA: 0x000B6919 File Offset: 0x000B4B19
		public bool IsFixedLength
		{
			get
			{
				return this.m_IsFixedLength;
			}
			set
			{
				this.m_IsFixedLength = value;
			}
		}

		/// <summary>Indicates whether the user-defined type is byte ordered.</summary>
		/// <returns>
		///     <see langword="true" /> if the user-defined type is byte ordered; otherwise <see langword="false" />.</returns>
		// Token: 0x170006B3 RID: 1715
		// (get) Token: 0x06002925 RID: 10533 RVA: 0x000B6922 File Offset: 0x000B4B22
		// (set) Token: 0x06002926 RID: 10534 RVA: 0x000B692A File Offset: 0x000B4B2A
		public bool IsByteOrdered
		{
			get
			{
				return this.m_IsByteOrdered;
			}
			set
			{
				this.m_IsByteOrdered = value;
			}
		}

		/// <summary>The serialization format as a <see cref="T:Microsoft.SqlServer.Server.Format" />.</summary>
		/// <returns>A <see cref="T:Microsoft.SqlServer.Server.Format" /> value representing the serialization format.</returns>
		// Token: 0x170006B4 RID: 1716
		// (get) Token: 0x06002927 RID: 10535 RVA: 0x000B6933 File Offset: 0x000B4B33
		public Format Format
		{
			get
			{
				return this.m_format;
			}
		}

		/// <summary>The name of the method used to validate instances of the user-defined type.</summary>
		/// <returns>A <see cref="T:System.String" /> representing the name of the method used to validate instances of the user-defined type.</returns>
		// Token: 0x170006B5 RID: 1717
		// (get) Token: 0x06002928 RID: 10536 RVA: 0x000B693B File Offset: 0x000B4B3B
		// (set) Token: 0x06002929 RID: 10537 RVA: 0x000B6943 File Offset: 0x000B4B43
		public string ValidationMethodName
		{
			get
			{
				return this.m_ValidationMethodName;
			}
			set
			{
				this.m_ValidationMethodName = value;
			}
		}

		/// <summary>The SQL Server name of the user-defined type.</summary>
		/// <returns>A <see cref="T:System.String" /> value representing the SQL Server name of the user-defined type.</returns>
		// Token: 0x170006B6 RID: 1718
		// (get) Token: 0x0600292A RID: 10538 RVA: 0x000B694C File Offset: 0x000B4B4C
		// (set) Token: 0x0600292B RID: 10539 RVA: 0x000B6954 File Offset: 0x000B4B54
		public string Name
		{
			get
			{
				return this.m_fName;
			}
			set
			{
				this.m_fName = value;
			}
		}

		// Token: 0x0400184F RID: 6223
		private int m_MaxByteSize;

		// Token: 0x04001850 RID: 6224
		private bool m_IsFixedLength;

		// Token: 0x04001851 RID: 6225
		private bool m_IsByteOrdered;

		// Token: 0x04001852 RID: 6226
		private Format m_format;

		// Token: 0x04001853 RID: 6227
		private string m_fName;

		// Token: 0x04001854 RID: 6228
		internal const int YukonMaxByteSizeValue = 8000;

		// Token: 0x04001855 RID: 6229
		private string m_ValidationMethodName;
	}
}
