﻿using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Globalization;
using Unity;

namespace Microsoft.SqlServer.Server
{
	/// <summary>Specifies and retrieves metadata information from parameters and columns of <see cref="T:Microsoft.SqlServer.Server.SqlDataRecord" /> objects. This class cannot be inherited.</summary>
	// Token: 0x02000317 RID: 791
	public sealed class SqlMetaData
	{
		/// <summary>Initializes a new instance of the <see cref="T:Microsoft.SqlServer.Server.SqlMetaData" /> class with the specified column name and type.</summary>
		/// <param name="name">The name of the column.</param>
		/// <param name="dbType">The SQL Server type of the parameter or column.</param>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="Name" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentException">A <see langword="SqlDbType" /> that is not allowed was passed to the constructor as <paramref name="dbType" />.</exception>
		// Token: 0x06002889 RID: 10377 RVA: 0x000B41A5 File Offset: 0x000B23A5
		public SqlMetaData(string name, SqlDbType dbType)
		{
			this.Construct(name, dbType, false, false, SortOrder.Unspecified, -1);
		}

		/// <summary>Initializes a new instance of the <see cref="T:Microsoft.SqlServer.Server.SqlMetaData" /> class with the specified column name, and default server. This form of the constructor supports table-valued parameters by allowing you to specify if the column is unique in the table-valued parameter, the sort order for the column, and the ordinal of the sort column.</summary>
		/// <param name="name">The name of the column.</param>
		/// <param name="dbType">The SQL Server type of the parameter or column.</param>
		/// <param name="useServerDefault">Specifes whether this column should use the default server value.</param>
		/// <param name="isUniqueKey">Specifies if the column in the table-valued parameter is unique.</param>
		/// <param name="columnSortOrder">Specifies the sort order for a column.</param>
		/// <param name="sortOrdinal">Specifies the ordinal of the sort column.</param>
		// Token: 0x0600288A RID: 10378 RVA: 0x000B41B9 File Offset: 0x000B23B9
		public SqlMetaData(string name, SqlDbType dbType, bool useServerDefault, bool isUniqueKey, SortOrder columnSortOrder, int sortOrdinal)
		{
			this.Construct(name, dbType, useServerDefault, isUniqueKey, columnSortOrder, sortOrdinal);
		}

		/// <summary>Initializes a new instance of the <see cref="T:Microsoft.SqlServer.Server.SqlMetaData" /> class with the specified column name, type, and maximum length.</summary>
		/// <param name="name">The name of the column.</param>
		/// <param name="dbType">The SQL Server type of the parameter or column.</param>
		/// <param name="maxLength">The maximum length of the specified type.</param>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="Name" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.ArgumentException">A SqlDbType that is not allowed was passed to the constructor as <paramref name="dbType" />.</exception>
		// Token: 0x0600288B RID: 10379 RVA: 0x000B41D0 File Offset: 0x000B23D0
		public SqlMetaData(string name, SqlDbType dbType, long maxLength)
		{
			this.Construct(name, dbType, maxLength, false, false, SortOrder.Unspecified, -1);
		}

		/// <summary>Initializes a new instance of the <see cref="T:Microsoft.SqlServer.Server.SqlMetaData" /> class with the specified column name, type, maximum length, and server default. This form of the constructor supports table-valued parameters by allowing you to specify if the column is unique in the table-valued parameter, the sort order for the column, and the ordinal of the sort column.</summary>
		/// <param name="name">The name of the column.</param>
		/// <param name="dbType">The SQL Server type of the parameter or column.</param>
		/// <param name="maxLength">The maximum length of the specified type.</param>
		/// <param name="useServerDefault">Specifes whether this column should use the default server value.</param>
		/// <param name="isUniqueKey">Specifies if the column in the table-valued parameter is unique.</param>
		/// <param name="columnSortOrder">Specifies the sort order for a column.</param>
		/// <param name="sortOrdinal">Specifies the ordinal of the sort column.</param>
		// Token: 0x0600288C RID: 10380 RVA: 0x000B41E5 File Offset: 0x000B23E5
		public SqlMetaData(string name, SqlDbType dbType, long maxLength, bool useServerDefault, bool isUniqueKey, SortOrder columnSortOrder, int sortOrdinal)
		{
			this.Construct(name, dbType, maxLength, useServerDefault, isUniqueKey, columnSortOrder, sortOrdinal);
		}

		/// <summary>Initializes a new instance of the <see cref="T:Microsoft.SqlServer.Server.SqlMetaData" /> class with the specified column name, type, precision, and scale.</summary>
		/// <param name="name">The name of the parameter or column.</param>
		/// <param name="dbType">The SQL Server type of the parameter or column.</param>
		/// <param name="precision">The precision of the parameter or column.</param>
		/// <param name="scale">The scale of the parameter or column.</param>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="Name" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.ArgumentException">A <see langword="SqlDbType" /> that is not allowed was passed to the constructor as <paramref name="dbType" />, or <paramref name="scale" /> was greater than <paramref name="precision" />. </exception>
		// Token: 0x0600288D RID: 10381 RVA: 0x000B4200 File Offset: 0x000B2400
		public SqlMetaData(string name, SqlDbType dbType, byte precision, byte scale)
		{
			this.Construct(name, dbType, precision, scale, false, false, SortOrder.Unspecified, -1);
		}

		/// <summary>Initializes a new instance of the <see cref="T:Microsoft.SqlServer.Server.SqlMetaData" /> class with the specified column name, type, precision, scale, and server default. This form of the constructor supports table-valued parameters by allowing you to specify if the column is unique in the table-valued parameter, the sort order for the column, and the ordinal of the sort column.</summary>
		/// <param name="name">The name of the column.</param>
		/// <param name="dbType">The SQL Server type of the parameter or column.</param>
		/// <param name="precision">The precision of the parameter or column.</param>
		/// <param name="scale">The scale of the parameter or column.</param>
		/// <param name="useServerDefault">Specifes whether this column should use the default server value.</param>
		/// <param name="isUniqueKey">Specifies if the column in the table-valued parameter is unique.</param>
		/// <param name="columnSortOrder">Specifies the sort order for a column.</param>
		/// <param name="sortOrdinal">Specifies the ordinal of the sort column.</param>
		// Token: 0x0600288E RID: 10382 RVA: 0x000B4224 File Offset: 0x000B2424
		public SqlMetaData(string name, SqlDbType dbType, byte precision, byte scale, bool useServerDefault, bool isUniqueKey, SortOrder columnSortOrder, int sortOrdinal)
		{
			this.Construct(name, dbType, precision, scale, useServerDefault, isUniqueKey, columnSortOrder, sortOrdinal);
		}

		/// <summary>Initializes a new instance of the <see cref="T:Microsoft.SqlServer.Server.SqlMetaData" /> class with the specified column name, type, maximum length, locale, and compare options.</summary>
		/// <param name="name">The name of the parameter or column.</param>
		/// <param name="dbType">The SQL Server type of the parameter or column.</param>
		/// <param name="maxLength">The maximum length of the specified type. </param>
		/// <param name="locale">The locale ID of the parameter or column.</param>
		/// <param name="compareOptions">The comparison rules of the parameter or column.</param>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="Name" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.ArgumentException">A SqlDbType that is not allowed was passed to the constructor as <paramref name="dbType" />.</exception>
		// Token: 0x0600288F RID: 10383 RVA: 0x000B424C File Offset: 0x000B244C
		public SqlMetaData(string name, SqlDbType dbType, long maxLength, long locale, SqlCompareOptions compareOptions)
		{
			this.Construct(name, dbType, maxLength, locale, compareOptions, false, false, SortOrder.Unspecified, -1);
		}

		/// <summary>Initializes a new instance of the <see cref="T:Microsoft.SqlServer.Server.SqlMetaData" /> class with the specified column name, type, maximum length, locale, compare options, and server default. This form of the constructor supports table-valued parameters by allowing you to specify if the column is unique in the table-valued parameter, the sort order for the column, and the ordinal of the sort column.</summary>
		/// <param name="name">The name of the column.</param>
		/// <param name="dbType">The SQL Server type of the parameter or column.</param>
		/// <param name="maxLength">The maximum length of the specified type.</param>
		/// <param name="locale">The locale ID of the parameter or column.</param>
		/// <param name="compareOptions">The comparison rules of the parameter or column.</param>
		/// <param name="useServerDefault">Specifes whether this column should use the default server value.</param>
		/// <param name="isUniqueKey">Specifies if the column in the table-valued parameter is unique.</param>
		/// <param name="columnSortOrder">Specifies the sort order for a column.</param>
		/// <param name="sortOrdinal">Specifies the ordinal of the sort column.</param>
		// Token: 0x06002890 RID: 10384 RVA: 0x000B4270 File Offset: 0x000B2470
		public SqlMetaData(string name, SqlDbType dbType, long maxLength, long locale, SqlCompareOptions compareOptions, bool useServerDefault, bool isUniqueKey, SortOrder columnSortOrder, int sortOrdinal)
		{
			this.Construct(name, dbType, maxLength, locale, compareOptions, useServerDefault, isUniqueKey, columnSortOrder, sortOrdinal);
		}

		/// <summary>Initializes a new instance of the <see cref="T:Microsoft.SqlServer.Server.SqlMetaData" /> class with the specified column name, database name, owning schema, object name, and default server. This form of the constructor supports table-valued parameters by allowing you to specify if the column is unique in the table-valued parameter, the sort order for the column, and the ordinal of the sort column.</summary>
		/// <param name="name">The name of the column.</param>
		/// <param name="dbType">The SQL Server type of the parameter or column.</param>
		/// <param name="database">The database name of the XML schema collection of a typed XML instance.</param>
		/// <param name="owningSchema">The relational schema name of the XML schema collection of a typed XML instance.</param>
		/// <param name="objectName">The name of the XML schema collection of a typed XML instance.</param>
		/// <param name="useServerDefault">Specifes whether this column should use the default server value.</param>
		/// <param name="isUniqueKey">Specifies if the column in the table-valued parameter is unique.</param>
		/// <param name="columnSortOrder">Specifies the sort order for a column.</param>
		/// <param name="sortOrdinal">Specifies the ordinal of the sort column.</param>
		// Token: 0x06002891 RID: 10385 RVA: 0x000B4298 File Offset: 0x000B2498
		public SqlMetaData(string name, SqlDbType dbType, string database, string owningSchema, string objectName, bool useServerDefault, bool isUniqueKey, SortOrder columnSortOrder, int sortOrdinal)
		{
			this.Construct(name, dbType, database, owningSchema, objectName, useServerDefault, isUniqueKey, columnSortOrder, sortOrdinal);
		}

		/// <summary>Initializes a new instance of the <see cref="T:Microsoft.SqlServer.Server.SqlMetaData" /> class with the specified column name, type, maximum length, precision, scale, locale ID, compare options, and user-defined type (UDT).</summary>
		/// <param name="name">The name of the column.</param>
		/// <param name="dbType">The SQL Server type of the parameter or column.</param>
		/// <param name="maxLength">The maximum length of the specified type.</param>
		/// <param name="precision">The precision of the parameter or column.</param>
		/// <param name="scale">The scale of the parameter or column.</param>
		/// <param name="locale">The locale ID of the parameter or column.</param>
		/// <param name="compareOptions">The comparison rules of the parameter or column.</param>
		/// <param name="userDefinedType">A <see cref="T:System.Type" /> instance that points to the UDT.</param>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="Name" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.ArgumentException">A <see langword="SqlDbType" /> that is not allowed was passed to the constructor as <paramref name="dbType" />, or <paramref name="userDefinedType" /> points to a type that does not have <see cref="T:Microsoft.SqlServer.Server.SqlUserDefinedTypeAttribute" /> declared.</exception>
		// Token: 0x06002892 RID: 10386 RVA: 0x000B42C0 File Offset: 0x000B24C0
		public SqlMetaData(string name, SqlDbType dbType, long maxLength, byte precision, byte scale, long locale, SqlCompareOptions compareOptions, Type userDefinedType) : this(name, dbType, maxLength, precision, scale, locale, compareOptions, userDefinedType, false, false, SortOrder.Unspecified, -1)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:Microsoft.SqlServer.Server.SqlMetaData" /> class with the specified column name, type, maximum length, precision, scale, locale ID, compare options, and user-defined type (UDT). This form of the constructor supports table-valued parameters by allowing you to specify if the column is unique in the table-valued parameter, the sort order for the column, and the ordinal of the sort column.</summary>
		/// <param name="name">The name of the column.</param>
		/// <param name="dbType">The SQL Server type of the parameter or column.</param>
		/// <param name="maxLength">The maximum length of the specified type.</param>
		/// <param name="precision">The precision of the parameter or column.</param>
		/// <param name="scale">The scale of the parameter or column.</param>
		/// <param name="localeId">The locale ID of the parameter or column.</param>
		/// <param name="compareOptions">The comparison rules of the parameter or column.</param>
		/// <param name="userDefinedType">A <see cref="T:System.Type" /> instance that points to the UDT.</param>
		/// <param name="useServerDefault">Specifes whether this column should use the default server value.</param>
		/// <param name="isUniqueKey">Specifies if the column in the table-valued parameter is unique.</param>
		/// <param name="columnSortOrder">Specifies the sort order for a column.</param>
		/// <param name="sortOrdinal">Specifies the ordinal of the sort column.</param>
		// Token: 0x06002893 RID: 10387 RVA: 0x000B42E4 File Offset: 0x000B24E4
		public SqlMetaData(string name, SqlDbType dbType, long maxLength, byte precision, byte scale, long localeId, SqlCompareOptions compareOptions, Type userDefinedType, bool useServerDefault, bool isUniqueKey, SortOrder columnSortOrder, int sortOrdinal)
		{
			switch (dbType)
			{
			case SqlDbType.BigInt:
			case SqlDbType.Bit:
			case SqlDbType.DateTime:
			case SqlDbType.Float:
			case SqlDbType.Image:
			case SqlDbType.Int:
			case SqlDbType.Money:
			case SqlDbType.Real:
			case SqlDbType.UniqueIdentifier:
			case SqlDbType.SmallDateTime:
			case SqlDbType.SmallInt:
			case SqlDbType.SmallMoney:
			case SqlDbType.Timestamp:
			case SqlDbType.TinyInt:
			case SqlDbType.Xml:
			case SqlDbType.Date:
				this.Construct(name, dbType, useServerDefault, isUniqueKey, columnSortOrder, sortOrdinal);
				return;
			case SqlDbType.Binary:
			case SqlDbType.VarBinary:
				this.Construct(name, dbType, maxLength, useServerDefault, isUniqueKey, columnSortOrder, sortOrdinal);
				return;
			case SqlDbType.Char:
			case SqlDbType.NChar:
			case SqlDbType.NVarChar:
			case SqlDbType.VarChar:
				this.Construct(name, dbType, maxLength, localeId, compareOptions, useServerDefault, isUniqueKey, columnSortOrder, sortOrdinal);
				return;
			case SqlDbType.Decimal:
			case SqlDbType.Time:
			case SqlDbType.DateTime2:
			case SqlDbType.DateTimeOffset:
				this.Construct(name, dbType, precision, scale, useServerDefault, isUniqueKey, columnSortOrder, sortOrdinal);
				return;
			case SqlDbType.NText:
			case SqlDbType.Text:
				this.Construct(name, dbType, SqlMetaData.Max, localeId, compareOptions, useServerDefault, isUniqueKey, columnSortOrder, sortOrdinal);
				return;
			case SqlDbType.Variant:
				this.Construct(name, dbType, useServerDefault, isUniqueKey, columnSortOrder, sortOrdinal);
				return;
			case SqlDbType.Udt:
				throw ADP.DbTypeNotSupported(SqlDbType.Udt.ToString());
			}
			SQL.InvalidSqlDbTypeForConstructor(dbType);
		}

		/// <summary>Initializes a new instance of the <see cref="T:Microsoft.SqlServer.Server.SqlMetaData" /> class with the specified column name, type, database name, owning schema, and object name.</summary>
		/// <param name="name">The name of the column.</param>
		/// <param name="dbType">The SQL Server type of the parameter or column.</param>
		/// <param name="database">The database name of the XML schema collection of a typed XML instance.</param>
		/// <param name="owningSchema">The relational schema name of the XML schema collection of a typed XML instance.</param>
		/// <param name="objectName">The name of the XML schema collection of a typed XML instance.</param>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="Name" /> is <see langword="null" />, or <paramref name="objectName" /> is <see langword="null" /> when <paramref name="database" /> and <paramref name="owningSchema" /> are non-<see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentException">A SqlDbType that is not allowed was passed to the constructor as <paramref name="dbType" />.</exception>
		// Token: 0x06002894 RID: 10388 RVA: 0x000B4424 File Offset: 0x000B2624
		public SqlMetaData(string name, SqlDbType dbType, string database, string owningSchema, string objectName)
		{
			this.Construct(name, dbType, database, owningSchema, objectName, false, false, SortOrder.Unspecified, -1);
		}

		// Token: 0x06002895 RID: 10389 RVA: 0x000B4448 File Offset: 0x000B2648
		internal SqlMetaData(string name, SqlDbType sqlDBType, long maxLength, byte precision, byte scale, long localeId, SqlCompareOptions compareOptions, string xmlSchemaCollectionDatabase, string xmlSchemaCollectionOwningSchema, string xmlSchemaCollectionName, bool partialLength)
		{
			this.AssertNameIsValid(name);
			this._strName = name;
			this._sqlDbType = sqlDBType;
			this._lMaxLength = maxLength;
			this._bPrecision = precision;
			this._bScale = scale;
			this._lLocale = localeId;
			this._eCompareOptions = compareOptions;
			this._xmlSchemaCollectionDatabase = xmlSchemaCollectionDatabase;
			this._xmlSchemaCollectionOwningSchema = xmlSchemaCollectionOwningSchema;
			this._xmlSchemaCollectionName = xmlSchemaCollectionName;
			this._bPartialLength = partialLength;
			this.ThrowIfUdt(sqlDBType);
		}

		// Token: 0x06002896 RID: 10390 RVA: 0x000B44C0 File Offset: 0x000B26C0
		private SqlMetaData(string name, SqlDbType sqlDbType, long maxLength, byte precision, byte scale, long localeId, SqlCompareOptions compareOptions, bool partialLength)
		{
			this.AssertNameIsValid(name);
			this._strName = name;
			this._sqlDbType = sqlDbType;
			this._lMaxLength = maxLength;
			this._bPrecision = precision;
			this._bScale = scale;
			this._lLocale = localeId;
			this._eCompareOptions = compareOptions;
			this._bPartialLength = partialLength;
			this.ThrowIfUdt(sqlDbType);
		}

		/// <summary>Gets the comparison rules used for the column or parameter.</summary>
		/// <returns>The comparison rules used for the column or parameter as a <see cref="T:System.Data.SqlTypes.SqlCompareOptions" />.</returns>
		// Token: 0x17000681 RID: 1665
		// (get) Token: 0x06002897 RID: 10391 RVA: 0x000B451E File Offset: 0x000B271E
		public SqlCompareOptions CompareOptions
		{
			get
			{
				return this._eCompareOptions;
			}
		}

		/// <summary>Indicates if the column in the table-valued parameter is unique.</summary>
		/// <returns>A <see langword="Boolean" /> value.</returns>
		// Token: 0x17000682 RID: 1666
		// (get) Token: 0x06002898 RID: 10392 RVA: 0x000B4526 File Offset: 0x000B2726
		public bool IsUniqueKey
		{
			get
			{
				return this._isUniqueKey;
			}
		}

		/// <summary>Gets the locale ID of the column or parameter.</summary>
		/// <returns>The locale ID of the column or parameter as a <see cref="T:System.Int64" />.</returns>
		// Token: 0x17000683 RID: 1667
		// (get) Token: 0x06002899 RID: 10393 RVA: 0x000B452E File Offset: 0x000B272E
		public long LocaleId
		{
			get
			{
				return this._lLocale;
			}
		}

		/// <summary>Gets the length of <see langword="text" />, <see langword="ntext" />, and <see langword="image" /> data types. </summary>
		/// <returns>The length of <see langword="text" />, <see langword="ntext" />, and <see langword="image" /> data types.</returns>
		// Token: 0x17000684 RID: 1668
		// (get) Token: 0x0600289A RID: 10394 RVA: 0x000B4536 File Offset: 0x000B2736
		public static long Max
		{
			get
			{
				return -1L;
			}
		}

		/// <summary>Gets the maximum length of the column or parameter.</summary>
		/// <returns>The maximum length of the column or parameter as a <see cref="T:System.Int64" />.</returns>
		// Token: 0x17000685 RID: 1669
		// (get) Token: 0x0600289B RID: 10395 RVA: 0x000B453A File Offset: 0x000B273A
		public long MaxLength
		{
			get
			{
				return this._lMaxLength;
			}
		}

		/// <summary>Gets the name of the column or parameter.</summary>
		/// <returns>The name of the column or parameter as a <see cref="T:System.String" />.</returns>
		/// <exception cref="T:System.InvalidOperationException">The <paramref name="Name" /> specified in the constructor is longer than 128 characters. </exception>
		// Token: 0x17000686 RID: 1670
		// (get) Token: 0x0600289C RID: 10396 RVA: 0x000B4542 File Offset: 0x000B2742
		public string Name
		{
			get
			{
				return this._strName;
			}
		}

		/// <summary>Gets the precision of the column or parameter.</summary>
		/// <returns>The precision of the column or parameter as a <see cref="T:System.Byte" />.</returns>
		// Token: 0x17000687 RID: 1671
		// (get) Token: 0x0600289D RID: 10397 RVA: 0x000B454A File Offset: 0x000B274A
		public byte Precision
		{
			get
			{
				return this._bPrecision;
			}
		}

		/// <summary>Gets the scale of the column or parameter.</summary>
		/// <returns>The scale of the column or parameter.</returns>
		// Token: 0x17000688 RID: 1672
		// (get) Token: 0x0600289E RID: 10398 RVA: 0x000B4552 File Offset: 0x000B2752
		public byte Scale
		{
			get
			{
				return this._bScale;
			}
		}

		/// <summary>Returns the sort order for a column.</summary>
		/// <returns>A <see cref="T:System.Data.SqlClient.SortOrder" /> object.</returns>
		// Token: 0x17000689 RID: 1673
		// (get) Token: 0x0600289F RID: 10399 RVA: 0x000B455A File Offset: 0x000B275A
		public SortOrder SortOrder
		{
			get
			{
				return this._columnSortOrder;
			}
		}

		/// <summary>Returns the ordinal of the sort column.</summary>
		/// <returns>The ordinal of the sort column.</returns>
		// Token: 0x1700068A RID: 1674
		// (get) Token: 0x060028A0 RID: 10400 RVA: 0x000B4562 File Offset: 0x000B2762
		public int SortOrdinal
		{
			get
			{
				return this._sortOrdinal;
			}
		}

		/// <summary>Gets the data type of the column or parameter.</summary>
		/// <returns>The data type of the column or parameter as a <see cref="T:System.Data.DbType" />.</returns>
		// Token: 0x1700068B RID: 1675
		// (get) Token: 0x060028A1 RID: 10401 RVA: 0x000B456A File Offset: 0x000B276A
		public SqlDbType SqlDbType
		{
			get
			{
				return this._sqlDbType;
			}
		}

		/// <summary>Gets the three-part name of the user-defined type (UDT) or the SQL Server type represented by the instance.</summary>
		/// <returns>The name of the UDT or SQL Server type as a <see cref="T:System.String" />.</returns>
		// Token: 0x1700068C RID: 1676
		// (get) Token: 0x060028A2 RID: 10402 RVA: 0x000B4572 File Offset: 0x000B2772
		public string TypeName
		{
			get
			{
				return SqlMetaData.sxm_rgDefaults[(int)this.SqlDbType].Name;
			}
		}

		/// <summary>Reports whether this column should use the default server value.</summary>
		/// <returns>A <see langword="Boolean" /> value.</returns>
		// Token: 0x1700068D RID: 1677
		// (get) Token: 0x060028A3 RID: 10403 RVA: 0x000B4585 File Offset: 0x000B2785
		public bool UseServerDefault
		{
			get
			{
				return this._useServerDefault;
			}
		}

		/// <summary>Gets the name of the database where the schema collection for this XML instance is located.</summary>
		/// <returns>The name of the database where the schema collection for this XML instance is located as a <see cref="T:System.String" />.</returns>
		// Token: 0x1700068E RID: 1678
		// (get) Token: 0x060028A4 RID: 10404 RVA: 0x000B458D File Offset: 0x000B278D
		public string XmlSchemaCollectionDatabase
		{
			get
			{
				return this._xmlSchemaCollectionDatabase;
			}
		}

		/// <summary>Gets the name of the schema collection for this XML instance.</summary>
		/// <returns>The name of the schema collection for this XML instance as a <see cref="T:System.String" />.</returns>
		// Token: 0x1700068F RID: 1679
		// (get) Token: 0x060028A5 RID: 10405 RVA: 0x000B4595 File Offset: 0x000B2795
		public string XmlSchemaCollectionName
		{
			get
			{
				return this._xmlSchemaCollectionName;
			}
		}

		/// <summary>Gets the owning relational schema where the schema collection for this XML instance is located.</summary>
		/// <returns>The owning relational schema where the schema collection for this XML instance is located as a <see cref="T:System.String" />.</returns>
		// Token: 0x17000690 RID: 1680
		// (get) Token: 0x060028A6 RID: 10406 RVA: 0x000B459D File Offset: 0x000B279D
		public string XmlSchemaCollectionOwningSchema
		{
			get
			{
				return this._xmlSchemaCollectionOwningSchema;
			}
		}

		// Token: 0x17000691 RID: 1681
		// (get) Token: 0x060028A7 RID: 10407 RVA: 0x000B45A5 File Offset: 0x000B27A5
		internal bool IsPartialLength
		{
			get
			{
				return this._bPartialLength;
			}
		}

		// Token: 0x060028A8 RID: 10408 RVA: 0x000B45B0 File Offset: 0x000B27B0
		private void Construct(string name, SqlDbType dbType, bool useServerDefault, bool isUniqueKey, SortOrder columnSortOrder, int sortOrdinal)
		{
			this.AssertNameIsValid(name);
			this.ValidateSortOrder(columnSortOrder, sortOrdinal);
			if (dbType != SqlDbType.BigInt && SqlDbType.Bit != dbType && SqlDbType.DateTime != dbType && SqlDbType.Date != dbType && SqlDbType.DateTime2 != dbType && SqlDbType.DateTimeOffset != dbType && SqlDbType.Decimal != dbType && SqlDbType.Float != dbType && SqlDbType.Image != dbType && SqlDbType.Int != dbType && SqlDbType.Money != dbType && SqlDbType.NText != dbType && SqlDbType.Real != dbType && SqlDbType.SmallDateTime != dbType && SqlDbType.SmallInt != dbType && SqlDbType.SmallMoney != dbType && SqlDbType.Text != dbType && SqlDbType.Time != dbType && SqlDbType.Timestamp != dbType && SqlDbType.TinyInt != dbType && SqlDbType.UniqueIdentifier != dbType && SqlDbType.Variant != dbType && SqlDbType.Xml != dbType)
			{
				throw SQL.InvalidSqlDbTypeForConstructor(dbType);
			}
			this.ThrowIfUdt(dbType);
			this.SetDefaultsForType(dbType);
			if (SqlDbType.NText == dbType || SqlDbType.Text == dbType)
			{
				this._lLocale = (long)CultureInfo.CurrentCulture.LCID;
			}
			this._strName = name;
			this._useServerDefault = useServerDefault;
			this._isUniqueKey = isUniqueKey;
			this._columnSortOrder = columnSortOrder;
			this._sortOrdinal = sortOrdinal;
		}

		// Token: 0x060028A9 RID: 10409 RVA: 0x000B4690 File Offset: 0x000B2890
		private void Construct(string name, SqlDbType dbType, long maxLength, bool useServerDefault, bool isUniqueKey, SortOrder columnSortOrder, int sortOrdinal)
		{
			this.AssertNameIsValid(name);
			this.ValidateSortOrder(columnSortOrder, sortOrdinal);
			long lLocale = 0L;
			if (SqlDbType.Char == dbType)
			{
				if (maxLength > 8000L || maxLength < 0L)
				{
					throw ADP.Argument(SR.GetString("Specified length '{0}' is out of range.", new object[]
					{
						maxLength.ToString(CultureInfo.InvariantCulture)
					}), "maxLength");
				}
				lLocale = (long)CultureInfo.CurrentCulture.LCID;
			}
			else if (SqlDbType.VarChar == dbType)
			{
				if ((maxLength > 8000L || maxLength < 0L) && maxLength != SqlMetaData.Max)
				{
					throw ADP.Argument(SR.GetString("Specified length '{0}' is out of range.", new object[]
					{
						maxLength.ToString(CultureInfo.InvariantCulture)
					}), "maxLength");
				}
				lLocale = (long)CultureInfo.CurrentCulture.LCID;
			}
			else if (SqlDbType.NChar == dbType)
			{
				if (maxLength > 4000L || maxLength < 0L)
				{
					throw ADP.Argument(SR.GetString("Specified length '{0}' is out of range.", new object[]
					{
						maxLength.ToString(CultureInfo.InvariantCulture)
					}), "maxLength");
				}
				lLocale = (long)CultureInfo.CurrentCulture.LCID;
			}
			else if (SqlDbType.NVarChar == dbType)
			{
				if ((maxLength > 4000L || maxLength < 0L) && maxLength != SqlMetaData.Max)
				{
					throw ADP.Argument(SR.GetString("Specified length '{0}' is out of range.", new object[]
					{
						maxLength.ToString(CultureInfo.InvariantCulture)
					}), "maxLength");
				}
				lLocale = (long)CultureInfo.CurrentCulture.LCID;
			}
			else if (SqlDbType.NText == dbType || SqlDbType.Text == dbType)
			{
				if (SqlMetaData.Max != maxLength)
				{
					throw ADP.Argument(SR.GetString("Specified length '{0}' is out of range.", new object[]
					{
						maxLength.ToString(CultureInfo.InvariantCulture)
					}), "maxLength");
				}
				lLocale = (long)CultureInfo.CurrentCulture.LCID;
			}
			else if (SqlDbType.Binary == dbType)
			{
				if (maxLength > 8000L || maxLength < 0L)
				{
					throw ADP.Argument(SR.GetString("Specified length '{0}' is out of range.", new object[]
					{
						maxLength.ToString(CultureInfo.InvariantCulture)
					}), "maxLength");
				}
			}
			else if (SqlDbType.VarBinary == dbType)
			{
				if ((maxLength > 8000L || maxLength < 0L) && maxLength != SqlMetaData.Max)
				{
					throw ADP.Argument(SR.GetString("Specified length '{0}' is out of range.", new object[]
					{
						maxLength.ToString(CultureInfo.InvariantCulture)
					}), "maxLength");
				}
			}
			else
			{
				if (SqlDbType.Image != dbType)
				{
					throw SQL.InvalidSqlDbTypeForConstructor(dbType);
				}
				if (SqlMetaData.Max != maxLength)
				{
					throw ADP.Argument(SR.GetString("Specified length '{0}' is out of range.", new object[]
					{
						maxLength.ToString(CultureInfo.InvariantCulture)
					}), "maxLength");
				}
			}
			this.SetDefaultsForType(dbType);
			this._strName = name;
			this._lMaxLength = maxLength;
			this._lLocale = lLocale;
			this._useServerDefault = useServerDefault;
			this._isUniqueKey = isUniqueKey;
			this._columnSortOrder = columnSortOrder;
			this._sortOrdinal = sortOrdinal;
		}

		// Token: 0x060028AA RID: 10410 RVA: 0x000B4944 File Offset: 0x000B2B44
		private void Construct(string name, SqlDbType dbType, long maxLength, long locale, SqlCompareOptions compareOptions, bool useServerDefault, bool isUniqueKey, SortOrder columnSortOrder, int sortOrdinal)
		{
			this.AssertNameIsValid(name);
			this.ValidateSortOrder(columnSortOrder, sortOrdinal);
			if (SqlDbType.Char == dbType)
			{
				if (maxLength > 8000L || maxLength < 0L)
				{
					throw ADP.Argument(SR.GetString("Specified length '{0}' is out of range.", new object[]
					{
						maxLength.ToString(CultureInfo.InvariantCulture)
					}), "maxLength");
				}
			}
			else if (SqlDbType.VarChar == dbType)
			{
				if ((maxLength > 8000L || maxLength < 0L) && maxLength != SqlMetaData.Max)
				{
					throw ADP.Argument(SR.GetString("Specified length '{0}' is out of range.", new object[]
					{
						maxLength.ToString(CultureInfo.InvariantCulture)
					}), "maxLength");
				}
			}
			else if (SqlDbType.NChar == dbType)
			{
				if (maxLength > 4000L || maxLength < 0L)
				{
					throw ADP.Argument(SR.GetString("Specified length '{0}' is out of range.", new object[]
					{
						maxLength.ToString(CultureInfo.InvariantCulture)
					}), "maxLength");
				}
			}
			else if (SqlDbType.NVarChar == dbType)
			{
				if ((maxLength > 4000L || maxLength < 0L) && maxLength != SqlMetaData.Max)
				{
					throw ADP.Argument(SR.GetString("Specified length '{0}' is out of range.", new object[]
					{
						maxLength.ToString(CultureInfo.InvariantCulture)
					}), "maxLength");
				}
			}
			else
			{
				if (SqlDbType.NText != dbType && SqlDbType.Text != dbType)
				{
					throw SQL.InvalidSqlDbTypeForConstructor(dbType);
				}
				if (SqlMetaData.Max != maxLength)
				{
					throw ADP.Argument(SR.GetString("Specified length '{0}' is out of range.", new object[]
					{
						maxLength.ToString(CultureInfo.InvariantCulture)
					}), "maxLength");
				}
			}
			if (SqlCompareOptions.BinarySort != compareOptions && (~(SqlCompareOptions.IgnoreCase | SqlCompareOptions.IgnoreNonSpace | SqlCompareOptions.IgnoreKanaType | SqlCompareOptions.IgnoreWidth) & compareOptions) != SqlCompareOptions.None)
			{
				throw ADP.InvalidEnumerationValue(typeof(SqlCompareOptions), (int)compareOptions);
			}
			this.SetDefaultsForType(dbType);
			this._strName = name;
			this._lMaxLength = maxLength;
			this._lLocale = locale;
			this._eCompareOptions = compareOptions;
			this._useServerDefault = useServerDefault;
			this._isUniqueKey = isUniqueKey;
			this._columnSortOrder = columnSortOrder;
			this._sortOrdinal = sortOrdinal;
		}

		// Token: 0x060028AB RID: 10411 RVA: 0x000B4B1C File Offset: 0x000B2D1C
		private void Construct(string name, SqlDbType dbType, byte precision, byte scale, bool useServerDefault, bool isUniqueKey, SortOrder columnSortOrder, int sortOrdinal)
		{
			this.AssertNameIsValid(name);
			this.ValidateSortOrder(columnSortOrder, sortOrdinal);
			if (SqlDbType.Decimal == dbType)
			{
				if (precision > SqlDecimal.MaxPrecision || scale > precision)
				{
					throw SQL.PrecisionValueOutOfRange(precision);
				}
				if (scale > SqlDecimal.MaxScale)
				{
					throw SQL.ScaleValueOutOfRange(scale);
				}
			}
			else
			{
				if (SqlDbType.Time != dbType && SqlDbType.DateTime2 != dbType && SqlDbType.DateTimeOffset != dbType)
				{
					throw SQL.InvalidSqlDbTypeForConstructor(dbType);
				}
				if (scale > 7)
				{
					throw SQL.TimeScaleValueOutOfRange(scale);
				}
			}
			this.SetDefaultsForType(dbType);
			this._strName = name;
			this._bPrecision = precision;
			this._bScale = scale;
			if (SqlDbType.Decimal == dbType)
			{
				this._lMaxLength = (long)((ulong)SqlMetaData.s_maxLenFromPrecision[(int)(precision - 1)]);
			}
			else
			{
				this._lMaxLength -= (long)((ulong)SqlMetaData.s_maxVarTimeLenOffsetFromScale[(int)scale]);
			}
			this._useServerDefault = useServerDefault;
			this._isUniqueKey = isUniqueKey;
			this._columnSortOrder = columnSortOrder;
			this._sortOrdinal = sortOrdinal;
		}

		// Token: 0x060028AC RID: 10412 RVA: 0x000B4BF0 File Offset: 0x000B2DF0
		private void Construct(string name, SqlDbType dbType, string database, string owningSchema, string objectName, bool useServerDefault, bool isUniqueKey, SortOrder columnSortOrder, int sortOrdinal)
		{
			this.AssertNameIsValid(name);
			this.ValidateSortOrder(columnSortOrder, sortOrdinal);
			if (SqlDbType.Xml != dbType)
			{
				throw SQL.InvalidSqlDbTypeForConstructor(dbType);
			}
			if ((database != null || owningSchema != null) && objectName == null)
			{
				throw ADP.ArgumentNull("objectName");
			}
			this.SetDefaultsForType(SqlDbType.Xml);
			this._strName = name;
			this._xmlSchemaCollectionDatabase = database;
			this._xmlSchemaCollectionOwningSchema = owningSchema;
			this._xmlSchemaCollectionName = objectName;
			this._useServerDefault = useServerDefault;
			this._isUniqueKey = isUniqueKey;
			this._columnSortOrder = columnSortOrder;
			this._sortOrdinal = sortOrdinal;
		}

		// Token: 0x060028AD RID: 10413 RVA: 0x000B4C76 File Offset: 0x000B2E76
		private void AssertNameIsValid(string name)
		{
			if (name == null)
			{
				throw ADP.ArgumentNull("name");
			}
			if (128L < (long)name.Length)
			{
				throw SQL.NameTooLong("name");
			}
		}

		// Token: 0x060028AE RID: 10414 RVA: 0x000B4CA0 File Offset: 0x000B2EA0
		private void ValidateSortOrder(SortOrder columnSortOrder, int sortOrdinal)
		{
			if (SortOrder.Unspecified != columnSortOrder && columnSortOrder != SortOrder.Ascending && SortOrder.Descending != columnSortOrder)
			{
				throw SQL.InvalidSortOrder(columnSortOrder);
			}
			if (SortOrder.Unspecified == columnSortOrder != (-1 == sortOrdinal))
			{
				throw SQL.MustSpecifyBothSortOrderAndOrdinal(columnSortOrder, sortOrdinal);
			}
		}

		/// <summary>Validates the specified <see cref="T:System.Int16" /> value against the metadata, and adjusts the value if necessary.</summary>
		/// <param name="value">The value to validate against the <see cref="T:Microsoft.SqlServer.Server.SqlMetaData" /> instance.</param>
		/// <returns>The adjusted value as a <see cref="T:System.Int16" />.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="Value" /> does not match the <see cref="T:Microsoft.SqlServer.Server.SqlMetaData" /> type, or <paramref name="value" /> could not be adjusted. </exception>
		// Token: 0x060028AF RID: 10415 RVA: 0x000B4CC6 File Offset: 0x000B2EC6
		public short Adjust(short value)
		{
			if (SqlDbType.SmallInt != this.SqlDbType)
			{
				SqlMetaData.ThrowInvalidType();
			}
			return value;
		}

		/// <summary>Validates the specified <see cref="T:System.Int32" /> value against the metadata, and adjusts the value if necessary.</summary>
		/// <param name="value">The value to validate against the <see cref="T:Microsoft.SqlServer.Server.SqlMetaData" /> instance.</param>
		/// <returns>The adjusted value as a <see cref="T:System.Int32" />.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="Value" /> does not match the <see cref="T:Microsoft.SqlServer.Server.SqlMetaData" /> type, or <paramref name="value" /> could not be adjusted. </exception>
		// Token: 0x060028B0 RID: 10416 RVA: 0x000B4CD8 File Offset: 0x000B2ED8
		public int Adjust(int value)
		{
			if (SqlDbType.Int != this.SqlDbType)
			{
				SqlMetaData.ThrowInvalidType();
			}
			return value;
		}

		/// <summary>Validates the specified <see cref="T:System.Int64" /> value against the metadata, and adjusts the value if necessary.</summary>
		/// <param name="value">The value to validate against the <see cref="T:Microsoft.SqlServer.Server.SqlMetaData" /> instance.</param>
		/// <returns>The adjusted value as a <see cref="T:System.Int64" />.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="Value" /> does not match the <see cref="T:Microsoft.SqlServer.Server.SqlMetaData" /> type, or <paramref name="value" /> could not be adjusted. </exception>
		// Token: 0x060028B1 RID: 10417 RVA: 0x000B4CE9 File Offset: 0x000B2EE9
		public long Adjust(long value)
		{
			if (this.SqlDbType != SqlDbType.BigInt)
			{
				SqlMetaData.ThrowInvalidType();
			}
			return value;
		}

		/// <summary>Validates the specified <see cref="T:System.Single" /> value against the metadata, and adjusts the value if necessary.</summary>
		/// <param name="value">The value to validate against the <see cref="T:Microsoft.SqlServer.Server.SqlMetaData" /> instance.</param>
		/// <returns>The adjusted value as a <see cref="T:System.Single" />.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="Value" /> does not match the <see cref="T:Microsoft.SqlServer.Server.SqlMetaData" /> type, or <paramref name="value" /> could not be adjusted. </exception>
		// Token: 0x060028B2 RID: 10418 RVA: 0x000B4CF9 File Offset: 0x000B2EF9
		public float Adjust(float value)
		{
			if (SqlDbType.Real != this.SqlDbType)
			{
				SqlMetaData.ThrowInvalidType();
			}
			return value;
		}

		/// <summary>Validates the specified <see cref="T:System.Double" /> value against the metadata, and adjusts the value if necessary.</summary>
		/// <param name="value">The value to validate against the <see cref="T:Microsoft.SqlServer.Server.SqlMetaData" /> instance.</param>
		/// <returns>The adjusted value as a <see cref="T:System.Double" />.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="Value" /> does not match the <see cref="T:Microsoft.SqlServer.Server.SqlMetaData" /> type, or <paramref name="value" /> could not be adjusted. </exception>
		// Token: 0x060028B3 RID: 10419 RVA: 0x000B4D0B File Offset: 0x000B2F0B
		public double Adjust(double value)
		{
			if (SqlDbType.Float != this.SqlDbType)
			{
				SqlMetaData.ThrowInvalidType();
			}
			return value;
		}

		/// <summary>Validates the specified <see cref="T:System.String" /> value against the metadata, and adjusts the value if necessary.</summary>
		/// <param name="value">The value to validate against the <see cref="T:Microsoft.SqlServer.Server.SqlMetaData" /> instance.</param>
		/// <returns>The adjusted value as a <see cref="T:System.String" />.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="Value" /> does not match the <see cref="T:Microsoft.SqlServer.Server.SqlMetaData" /> type, or <paramref name="value" /> could not be adjusted. </exception>
		// Token: 0x060028B4 RID: 10420 RVA: 0x000B4D1C File Offset: 0x000B2F1C
		public string Adjust(string value)
		{
			if (SqlDbType.Char == this.SqlDbType || SqlDbType.NChar == this.SqlDbType)
			{
				if (value != null && (long)value.Length < this.MaxLength)
				{
					value = value.PadRight((int)this.MaxLength);
				}
			}
			else if (SqlDbType.VarChar != this.SqlDbType && SqlDbType.NVarChar != this.SqlDbType && SqlDbType.Text != this.SqlDbType && SqlDbType.NText != this.SqlDbType)
			{
				SqlMetaData.ThrowInvalidType();
			}
			if (value == null)
			{
				return null;
			}
			if ((long)value.Length > this.MaxLength && SqlMetaData.Max != this.MaxLength)
			{
				value = value.Remove((int)this.MaxLength, (int)((long)value.Length - this.MaxLength));
			}
			return value;
		}

		/// <summary>Validates the specified <see cref="T:System.Decimal" /> value against the metadata, and adjusts the value if necessary.</summary>
		/// <param name="value">The value to validate against the <see cref="T:Microsoft.SqlServer.Server.SqlMetaData" /> instance.</param>
		/// <returns>The adjusted value as a <see cref="T:System.Decimal" />.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="Value" /> does not match the <see cref="T:Microsoft.SqlServer.Server.SqlMetaData" /> type, or <paramref name="value" /> could not be adjusted. </exception>
		// Token: 0x060028B5 RID: 10421 RVA: 0x000B4DCC File Offset: 0x000B2FCC
		public decimal Adjust(decimal value)
		{
			if (SqlDbType.Decimal != this.SqlDbType && SqlDbType.Money != this.SqlDbType && SqlDbType.SmallMoney != this.SqlDbType)
			{
				SqlMetaData.ThrowInvalidType();
			}
			if (SqlDbType.Decimal != this.SqlDbType)
			{
				this.VerifyMoneyRange(new SqlMoney(value));
				return value;
			}
			return this.InternalAdjustSqlDecimal(new SqlDecimal(value)).Value;
		}

		/// <summary>Validates the specified <see cref="T:System.DateTime" /> value against the metadata, and adjusts the value if necessary.</summary>
		/// <param name="value">The value to validate against the <see cref="T:Microsoft.SqlServer.Server.SqlMetaData" /> instance.</param>
		/// <returns>The adjusted value as a <see cref="T:System.DateTime" />.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="Value" /> does not match the <see cref="T:Microsoft.SqlServer.Server.SqlMetaData" /> type, or <paramref name="value" /> could not be adjusted. </exception>
		// Token: 0x060028B6 RID: 10422 RVA: 0x000B4E28 File Offset: 0x000B3028
		public DateTime Adjust(DateTime value)
		{
			if (SqlDbType.DateTime == this.SqlDbType || SqlDbType.SmallDateTime == this.SqlDbType)
			{
				this.VerifyDateTimeRange(value);
			}
			else
			{
				if (SqlDbType.DateTime2 == this.SqlDbType)
				{
					return new DateTime(this.InternalAdjustTimeTicks(value.Ticks));
				}
				if (SqlDbType.Date == this.SqlDbType)
				{
					return value.Date;
				}
				SqlMetaData.ThrowInvalidType();
			}
			return value;
		}

		/// <summary>Validates the specified <see cref="T:System.Guid" /> value against the metadata, and adjusts the value if necessary.</summary>
		/// <param name="value">The value to validate against the <see cref="T:Microsoft.SqlServer.Server.SqlMetaData" /> instance.</param>
		/// <returns>The adjusted value as a <see cref="T:System.Guid" />.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="Value" /> does not match the <see cref="T:Microsoft.SqlServer.Server.SqlMetaData" /> type, or <paramref name="value" /> could not be adjusted. </exception>
		// Token: 0x060028B7 RID: 10423 RVA: 0x000B4E86 File Offset: 0x000B3086
		public Guid Adjust(Guid value)
		{
			if (SqlDbType.UniqueIdentifier != this.SqlDbType)
			{
				SqlMetaData.ThrowInvalidType();
			}
			return value;
		}

		/// <summary>Validates the specified <see cref="T:System.Data.SqlTypes.SqlBoolean" /> value against the metadata, and adjusts the value if necessary.</summary>
		/// <param name="value">The value to validate against the <see cref="T:Microsoft.SqlServer.Server.SqlMetaData" /> instance.</param>
		/// <returns>The adjusted value as a <see cref="T:System.Data.SqlTypes.SqlBoolean" />.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="Value" /> does not match the <see cref="T:Microsoft.SqlServer.Server.SqlMetaData" /> type, or <paramref name="value" /> could not be adjusted. </exception>
		// Token: 0x060028B8 RID: 10424 RVA: 0x000B4E98 File Offset: 0x000B3098
		public SqlBoolean Adjust(SqlBoolean value)
		{
			if (SqlDbType.Bit != this.SqlDbType)
			{
				SqlMetaData.ThrowInvalidType();
			}
			return value;
		}

		/// <summary>Validates the specified <see cref="T:System.Data.SqlTypes.SqlByte" /> value against the metadata, and adjusts the value if necessary.</summary>
		/// <param name="value">The value to validate against the <see cref="T:Microsoft.SqlServer.Server.SqlMetaData" /> instance.</param>
		/// <returns>The adjusted value as a <see cref="T:System.Data.SqlTypes.SqlByte" />.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="Value" /> does not match the <see cref="T:Microsoft.SqlServer.Server.SqlMetaData" /> type, or <paramref name="value" /> could not be adjusted. </exception>
		// Token: 0x060028B9 RID: 10425 RVA: 0x000B4EA9 File Offset: 0x000B30A9
		public SqlByte Adjust(SqlByte value)
		{
			if (SqlDbType.TinyInt != this.SqlDbType)
			{
				SqlMetaData.ThrowInvalidType();
			}
			return value;
		}

		/// <summary>Validates the specified <see cref="T:System.Data.SqlTypes.SqlInt16" /> value against the metadata, and adjusts the value if necessary.</summary>
		/// <param name="value">The value to validate against the <see cref="T:Microsoft.SqlServer.Server.SqlMetaData" /> instance.</param>
		/// <returns>The adjusted value as a <see cref="T:System.Data.SqlTypes.SqlInt16" />.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="Value" /> does not match the <see cref="T:Microsoft.SqlServer.Server.SqlMetaData" /> type, or <paramref name="value" /> could not be adjusted. </exception>
		// Token: 0x060028BA RID: 10426 RVA: 0x000B4CC6 File Offset: 0x000B2EC6
		public SqlInt16 Adjust(SqlInt16 value)
		{
			if (SqlDbType.SmallInt != this.SqlDbType)
			{
				SqlMetaData.ThrowInvalidType();
			}
			return value;
		}

		/// <summary>Validates the specified <see cref="T:System.Data.SqlTypes.SqlInt32" /> value against the metadata, and adjusts the value if necessary.</summary>
		/// <param name="value">The value to validate against the <see cref="T:Microsoft.SqlServer.Server.SqlMetaData" /> instance.</param>
		/// <returns>The adjusted value as a <see cref="T:System.Data.SqlTypes.SqlInt32" />.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="Value" /> does not match the <see cref="T:Microsoft.SqlServer.Server.SqlMetaData" /> type, or <paramref name="value" /> could not be adjusted. </exception>
		// Token: 0x060028BB RID: 10427 RVA: 0x000B4CD8 File Offset: 0x000B2ED8
		public SqlInt32 Adjust(SqlInt32 value)
		{
			if (SqlDbType.Int != this.SqlDbType)
			{
				SqlMetaData.ThrowInvalidType();
			}
			return value;
		}

		/// <summary>Validates the specified <see cref="T:System.Data.SqlTypes.SqlInt64" /> value against the metadata, and adjusts the value if necessary.</summary>
		/// <param name="value">The value to validate against the <see cref="T:Microsoft.SqlServer.Server.SqlMetaData" /> instance.</param>
		/// <returns>The adjusted value as a <see cref="T:System.Data.SqlTypes.SqlInt64" />.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="Value" /> does not match the <see cref="T:Microsoft.SqlServer.Server.SqlMetaData" /> type, or <paramref name="value" /> could not be adjusted. </exception>
		// Token: 0x060028BC RID: 10428 RVA: 0x000B4CE9 File Offset: 0x000B2EE9
		public SqlInt64 Adjust(SqlInt64 value)
		{
			if (this.SqlDbType != SqlDbType.BigInt)
			{
				SqlMetaData.ThrowInvalidType();
			}
			return value;
		}

		/// <summary>Validates the specified <see cref="T:System.Data.SqlTypes.SqlSingle" /> value against the metadata, and adjusts the value if necessary.</summary>
		/// <param name="value">The value to validate against the <see cref="T:Microsoft.SqlServer.Server.SqlMetaData" /> instance.</param>
		/// <returns>The adjusted value as a <see cref="T:System.Data.SqlTypes.SqlSingle" />.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="Value" /> does not match the <see cref="T:Microsoft.SqlServer.Server.SqlMetaData" /> type, or <paramref name="value" /> could not be adjusted. </exception>
		// Token: 0x060028BD RID: 10429 RVA: 0x000B4CF9 File Offset: 0x000B2EF9
		public SqlSingle Adjust(SqlSingle value)
		{
			if (SqlDbType.Real != this.SqlDbType)
			{
				SqlMetaData.ThrowInvalidType();
			}
			return value;
		}

		/// <summary>Validates the specified <see cref="T:System.Data.SqlTypes.SqlDouble" /> value against the metadata, and adjusts the value if necessary.</summary>
		/// <param name="value">The value to validate against the <see cref="T:Microsoft.SqlServer.Server.SqlMetaData" /> instance.</param>
		/// <returns>The adjusted value as a <see cref="T:System.Data.SqlTypes.SqlDouble" />.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="Value" /> does not match the <see cref="T:Microsoft.SqlServer.Server.SqlMetaData" /> type, or <paramref name="value" /> could not be adjusted. </exception>
		// Token: 0x060028BE RID: 10430 RVA: 0x000B4D0B File Offset: 0x000B2F0B
		public SqlDouble Adjust(SqlDouble value)
		{
			if (SqlDbType.Float != this.SqlDbType)
			{
				SqlMetaData.ThrowInvalidType();
			}
			return value;
		}

		/// <summary>Validates the specified <see cref="T:System.Data.SqlTypes.SqlMoney" /> value against the metadata, and adjusts the value if necessary.</summary>
		/// <param name="value">The value to validate against the <see cref="T:Microsoft.SqlServer.Server.SqlMetaData" /> instance.</param>
		/// <returns>The adjusted value as a <see cref="T:System.Data.SqlTypes.SqlMoney" />.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="Value" /> does not match the <see cref="T:Microsoft.SqlServer.Server.SqlMetaData" /> type, or <paramref name="value" /> could not be adjusted. </exception>
		// Token: 0x060028BF RID: 10431 RVA: 0x000B4EBB File Offset: 0x000B30BB
		public SqlMoney Adjust(SqlMoney value)
		{
			if (SqlDbType.Money != this.SqlDbType && SqlDbType.SmallMoney != this.SqlDbType)
			{
				SqlMetaData.ThrowInvalidType();
			}
			if (!value.IsNull)
			{
				this.VerifyMoneyRange(value);
			}
			return value;
		}

		/// <summary>Validates the specified <see cref="T:System.Data.SqlTypes.SqlDateTime" /> value against the metadata, and adjusts the value if necessary.</summary>
		/// <param name="value">The value to validate against the <see cref="T:Microsoft.SqlServer.Server.SqlMetaData" /> instance.</param>
		/// <returns>The adjusted value as a <see cref="T:System.Data.SqlTypes.SqlDateTime" />.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="Value" /> does not match the <see cref="T:Microsoft.SqlServer.Server.SqlMetaData" /> type, or <paramref name="value" /> could not be adjusted. </exception>
		// Token: 0x060028C0 RID: 10432 RVA: 0x000B4EE7 File Offset: 0x000B30E7
		public SqlDateTime Adjust(SqlDateTime value)
		{
			if (SqlDbType.DateTime != this.SqlDbType && SqlDbType.SmallDateTime != this.SqlDbType)
			{
				SqlMetaData.ThrowInvalidType();
			}
			if (!value.IsNull)
			{
				this.VerifyDateTimeRange(value.Value);
			}
			return value;
		}

		/// <summary>Validates the specified <see cref="T:System.Data.SqlTypes.SqlDecimal" /> value against the metadata, and adjusts the value if necessary.</summary>
		/// <param name="value">The value to validate against the <see cref="T:Microsoft.SqlServer.Server.SqlMetaData" /> instance.</param>
		/// <returns>The adjusted value as a <see cref="T:System.Data.SqlTypes.SqlDecimal" />.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="Value" /> does not match the <see cref="T:Microsoft.SqlServer.Server.SqlMetaData" /> type, or <paramref name="value" /> could not be adjusted. </exception>
		// Token: 0x060028C1 RID: 10433 RVA: 0x000B4F18 File Offset: 0x000B3118
		public SqlDecimal Adjust(SqlDecimal value)
		{
			if (SqlDbType.Decimal != this.SqlDbType)
			{
				SqlMetaData.ThrowInvalidType();
			}
			return this.InternalAdjustSqlDecimal(value);
		}

		/// <summary>Validates the specified <see cref="T:System.Data.SqlTypes.SqlString" /> value against the metadata, and adjusts the value if necessary.</summary>
		/// <param name="value">The value to validate against the <see cref="T:Microsoft.SqlServer.Server.SqlMetaData" /> instance.</param>
		/// <returns>The adjusted value as a <see cref="T:System.Data.SqlTypes.SqlString" />.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="Value" /> does not match the <see cref="T:Microsoft.SqlServer.Server.SqlMetaData" /> type, or <paramref name="value" /> could not be adjusted. </exception>
		// Token: 0x060028C2 RID: 10434 RVA: 0x000B4F30 File Offset: 0x000B3130
		public SqlString Adjust(SqlString value)
		{
			if (SqlDbType.Char == this.SqlDbType || SqlDbType.NChar == this.SqlDbType)
			{
				if (!value.IsNull && (long)value.Value.Length < this.MaxLength)
				{
					return new SqlString(value.Value.PadRight((int)this.MaxLength));
				}
			}
			else if (SqlDbType.VarChar != this.SqlDbType && SqlDbType.NVarChar != this.SqlDbType && SqlDbType.Text != this.SqlDbType && SqlDbType.NText != this.SqlDbType)
			{
				SqlMetaData.ThrowInvalidType();
			}
			if (value.IsNull)
			{
				return value;
			}
			if ((long)value.Value.Length > this.MaxLength && SqlMetaData.Max != this.MaxLength)
			{
				value = new SqlString(value.Value.Remove((int)this.MaxLength, (int)((long)value.Value.Length - this.MaxLength)));
			}
			return value;
		}

		/// <summary>Validates the specified <see cref="T:System.Data.SqlTypes.SqlBinary" /> value against the metadata, and adjusts the value if necessary.</summary>
		/// <param name="value">The value to validate against the <see cref="T:Microsoft.SqlServer.Server.SqlMetaData" /> instance.</param>
		/// <returns>The adjusted value as a <see cref="T:System.Data.SqlTypes.SqlBinary" />.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="Value" /> does not match the <see cref="T:Microsoft.SqlServer.Server.SqlMetaData" /> type, or <paramref name="value" /> could not be adjusted. </exception>
		// Token: 0x060028C3 RID: 10435 RVA: 0x000B5014 File Offset: 0x000B3214
		public SqlBinary Adjust(SqlBinary value)
		{
			if (SqlDbType.Binary == this.SqlDbType || SqlDbType.Timestamp == this.SqlDbType)
			{
				if (!value.IsNull && (long)value.Length < this.MaxLength)
				{
					byte[] value2 = value.Value;
					byte[] array = new byte[this.MaxLength];
					Buffer.BlockCopy(value2, 0, array, 0, value2.Length);
					Array.Clear(array, value2.Length, array.Length - value2.Length);
					return new SqlBinary(array);
				}
			}
			else if (SqlDbType.VarBinary != this.SqlDbType && SqlDbType.Image != this.SqlDbType)
			{
				SqlMetaData.ThrowInvalidType();
			}
			if (value.IsNull)
			{
				return value;
			}
			if ((long)value.Length > this.MaxLength && SqlMetaData.Max != this.MaxLength)
			{
				Array value3 = value.Value;
				byte[] array2 = new byte[this.MaxLength];
				Buffer.BlockCopy(value3, 0, array2, 0, (int)this.MaxLength);
				value = new SqlBinary(array2);
			}
			return value;
		}

		/// <summary>Validates the specified <see cref="T:System.Data.SqlTypes.SqlGuid" /> value against the metadata, and adjusts the value if necessary.</summary>
		/// <param name="value">The value to validate against the <see cref="T:Microsoft.SqlServer.Server.SqlMetaData" /> instance.</param>
		/// <returns>The adjusted value as a <see cref="T:System.Data.SqlTypes.SqlGuid" />.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="Value" /> does not match the <see cref="T:Microsoft.SqlServer.Server.SqlMetaData" /> type, or <paramref name="value" /> could not be adjusted. </exception>
		// Token: 0x060028C4 RID: 10436 RVA: 0x000B4E86 File Offset: 0x000B3086
		public SqlGuid Adjust(SqlGuid value)
		{
			if (SqlDbType.UniqueIdentifier != this.SqlDbType)
			{
				SqlMetaData.ThrowInvalidType();
			}
			return value;
		}

		/// <summary>Validates the specified <see cref="T:System.Data.SqlTypes.SqlChars" /> value against the metadata, and adjusts the value if necessary.</summary>
		/// <param name="value">The value to validate against the <see cref="T:Microsoft.SqlServer.Server.SqlMetaData" /> instance.</param>
		/// <returns>The adjusted value as a <see cref="T:System.Data.SqlTypes.SqlChars" />.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="Value" /> does not match the <see cref="T:Microsoft.SqlServer.Server.SqlMetaData" /> type, or <paramref name="value" /> could not be adjusted. </exception>
		// Token: 0x060028C5 RID: 10437 RVA: 0x000B50F4 File Offset: 0x000B32F4
		public SqlChars Adjust(SqlChars value)
		{
			if (SqlDbType.Char == this.SqlDbType || SqlDbType.NChar == this.SqlDbType)
			{
				if (value != null && !value.IsNull)
				{
					long length = value.Length;
					if (length < this.MaxLength)
					{
						if (value.MaxLength < this.MaxLength)
						{
							char[] array = new char[(int)this.MaxLength];
							Array.Copy(value.Buffer, 0, array, 0, (int)length);
							value = new SqlChars(array);
						}
						char[] buffer = value.Buffer;
						for (long num = length; num < this.MaxLength; num += 1L)
						{
							buffer[(int)(checked((IntPtr)num))] = ' ';
						}
						value.SetLength(this.MaxLength);
						return value;
					}
				}
			}
			else if (SqlDbType.VarChar != this.SqlDbType && SqlDbType.NVarChar != this.SqlDbType && SqlDbType.Text != this.SqlDbType && SqlDbType.NText != this.SqlDbType)
			{
				SqlMetaData.ThrowInvalidType();
			}
			if (value == null || value.IsNull)
			{
				return value;
			}
			if (value.Length > this.MaxLength && SqlMetaData.Max != this.MaxLength)
			{
				value.SetLength(this.MaxLength);
			}
			return value;
		}

		/// <summary>Validates the specified <see cref="T:System.Data.SqlTypes.SqlBytes" /> value against the metadata, and adjusts the value if necessary.</summary>
		/// <param name="value">The value to validate against the <see cref="T:Microsoft.SqlServer.Server.SqlMetaData" /> instance.</param>
		/// <returns>The adjusted value as a <see cref="T:System.Data.SqlTypes.SqlBytes" />.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="Value" /> does not match the <see cref="T:Microsoft.SqlServer.Server.SqlMetaData" /> type, or <paramref name="value" /> could not be adjusted. </exception>
		// Token: 0x060028C6 RID: 10438 RVA: 0x000B5200 File Offset: 0x000B3400
		public SqlBytes Adjust(SqlBytes value)
		{
			if (SqlDbType.Binary == this.SqlDbType || SqlDbType.Timestamp == this.SqlDbType)
			{
				if (value != null && !value.IsNull)
				{
					int num = (int)value.Length;
					if ((long)num < this.MaxLength)
					{
						if (value.MaxLength < this.MaxLength)
						{
							byte[] array = new byte[this.MaxLength];
							Buffer.BlockCopy(value.Buffer, 0, array, 0, num);
							value = new SqlBytes(array);
						}
						byte[] buffer = value.Buffer;
						Array.Clear(buffer, num, buffer.Length - num);
						value.SetLength(this.MaxLength);
						return value;
					}
				}
			}
			else if (SqlDbType.VarBinary != this.SqlDbType && SqlDbType.Image != this.SqlDbType)
			{
				SqlMetaData.ThrowInvalidType();
			}
			if (value == null || value.IsNull)
			{
				return value;
			}
			if (value.Length > this.MaxLength && SqlMetaData.Max != this.MaxLength)
			{
				value.SetLength(this.MaxLength);
			}
			return value;
		}

		/// <summary>Validates the specified <see cref="T:System.Data.SqlTypes.SqlXml" /> value against the metadata, and adjusts the value if necessary.</summary>
		/// <param name="value">The value to validate against the <see cref="T:Microsoft.SqlServer.Server.SqlMetaData" /> instance.</param>
		/// <returns>The adjusted value as a <see cref="T:System.Data.SqlTypes.SqlXml" />.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="Value" /> does not match the <see cref="T:Microsoft.SqlServer.Server.SqlMetaData" /> type, or <paramref name="value" /> could not be adjusted. </exception>
		// Token: 0x060028C7 RID: 10439 RVA: 0x000B52E0 File Offset: 0x000B34E0
		public SqlXml Adjust(SqlXml value)
		{
			if (SqlDbType.Xml != this.SqlDbType)
			{
				SqlMetaData.ThrowInvalidType();
			}
			return value;
		}

		/// <summary>Validates the specified <see cref="T:System.TimeSpan" /> value against the metadata, and adjusts the value if necessary.</summary>
		/// <param name="value">The value to validate against the <see cref="T:Microsoft.SqlServer.Server.SqlMetaData" /> instance.</param>
		/// <returns>The adjusted value as an array of <see cref="T:System.TimeSpan" /> values.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="Value" /> does not match the <see cref="T:Microsoft.SqlServer.Server.SqlMetaData" /> type, or <paramref name="value" /> could not be adjusted. </exception>
		// Token: 0x060028C8 RID: 10440 RVA: 0x000B52F2 File Offset: 0x000B34F2
		public TimeSpan Adjust(TimeSpan value)
		{
			if (SqlDbType.Time != this.SqlDbType)
			{
				SqlMetaData.ThrowInvalidType();
			}
			this.VerifyTimeRange(value);
			return new TimeSpan(this.InternalAdjustTimeTicks(value.Ticks));
		}

		/// <summary>Validates the specified <see cref="T:System.DateTimeOffset" /> value against the metadata, and adjusts the value if necessary.</summary>
		/// <param name="value">The value to validate against the <see cref="T:Microsoft.SqlServer.Server.SqlMetaData" /> instance.</param>
		/// <returns>The adjusted value as an array of <see cref="T:System.DateTimeOffset" /> values.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="Value" /> does not match the <see cref="T:Microsoft.SqlServer.Server.SqlMetaData" /> type, or <paramref name="value" /> could not be adjusted. </exception>
		// Token: 0x060028C9 RID: 10441 RVA: 0x000B531C File Offset: 0x000B351C
		public DateTimeOffset Adjust(DateTimeOffset value)
		{
			if (SqlDbType.DateTimeOffset != this.SqlDbType)
			{
				SqlMetaData.ThrowInvalidType();
			}
			return new DateTimeOffset(this.InternalAdjustTimeTicks(value.Ticks), value.Offset);
		}

		/// <summary>Validates the specified <see cref="T:System.Object" /> value against the metadata, and adjusts the value if necessary.</summary>
		/// <param name="value">The value to validate against the <see cref="T:Microsoft.SqlServer.Server.SqlMetaData" /> instance.</param>
		/// <returns>The adjusted value as a <see cref="T:System.Object" />.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="Value" /> does not match the <see cref="T:Microsoft.SqlServer.Server.SqlMetaData" /> type, or <paramref name="value" /> could not be adjusted. </exception>
		// Token: 0x060028CA RID: 10442 RVA: 0x000B5348 File Offset: 0x000B3548
		public object Adjust(object value)
		{
			if (value == null)
			{
				return null;
			}
			if (value is bool)
			{
				value = this.Adjust((bool)value);
			}
			else if (value is byte)
			{
				value = this.Adjust((byte)value);
			}
			else if (value is char)
			{
				value = this.Adjust((char)value);
			}
			else if (value is DateTime)
			{
				value = this.Adjust((DateTime)value);
			}
			else if (!(value is DBNull))
			{
				if (value is decimal)
				{
					value = this.Adjust((decimal)value);
				}
				else if (value is double)
				{
					value = this.Adjust((double)value);
				}
				else if (value is short)
				{
					value = this.Adjust((short)value);
				}
				else if (value is int)
				{
					value = this.Adjust((int)value);
				}
				else if (value is long)
				{
					value = this.Adjust((long)value);
				}
				else
				{
					if (value is sbyte)
					{
						throw ADP.InvalidDataType("SByte");
					}
					if (value is float)
					{
						value = this.Adjust((float)value);
					}
					else if (value is string)
					{
						value = this.Adjust((string)value);
					}
					else
					{
						if (value is ushort)
						{
							throw ADP.InvalidDataType("UInt16");
						}
						if (value is uint)
						{
							throw ADP.InvalidDataType("UInt32");
						}
						if (value is ulong)
						{
							throw ADP.InvalidDataType("UInt64");
						}
						if (value is byte[])
						{
							value = this.Adjust((byte[])value);
						}
						else if (value is char[])
						{
							value = this.Adjust((char[])value);
						}
						else if (value is Guid)
						{
							value = this.Adjust((Guid)value);
						}
						else if (value is SqlBinary)
						{
							value = this.Adjust((SqlBinary)value);
						}
						else if (value is SqlBoolean)
						{
							value = this.Adjust((SqlBoolean)value);
						}
						else if (value is SqlByte)
						{
							value = this.Adjust((SqlByte)value);
						}
						else if (value is SqlDateTime)
						{
							value = this.Adjust((SqlDateTime)value);
						}
						else if (value is SqlDouble)
						{
							value = this.Adjust((SqlDouble)value);
						}
						else if (value is SqlGuid)
						{
							value = this.Adjust((SqlGuid)value);
						}
						else if (value is SqlInt16)
						{
							value = this.Adjust((SqlInt16)value);
						}
						else if (value is SqlInt32)
						{
							value = this.Adjust((SqlInt32)value);
						}
						else if (value is SqlInt64)
						{
							value = this.Adjust((SqlInt64)value);
						}
						else if (value is SqlMoney)
						{
							value = this.Adjust((SqlMoney)value);
						}
						else if (value is SqlDecimal)
						{
							value = this.Adjust((SqlDecimal)value);
						}
						else if (value is SqlSingle)
						{
							value = this.Adjust((SqlSingle)value);
						}
						else if (value is SqlString)
						{
							value = this.Adjust((SqlString)value);
						}
						else if (value is SqlChars)
						{
							value = this.Adjust((SqlChars)value);
						}
						else if (value is SqlBytes)
						{
							value = this.Adjust((SqlBytes)value);
						}
						else if (value is SqlXml)
						{
							value = this.Adjust((SqlXml)value);
						}
						else if (value is TimeSpan)
						{
							value = this.Adjust((TimeSpan)value);
						}
						else
						{
							if (!(value is DateTimeOffset))
							{
								throw ADP.UnknownDataType(value.GetType());
							}
							value = this.Adjust((DateTimeOffset)value);
						}
					}
				}
			}
			return value;
		}

		/// <summary>Infers the metadata from the specified object and returns it as a <see cref="T:Microsoft.SqlServer.Server.SqlMetaData" /> instance.</summary>
		/// <param name="value">The object used from which the metadata is inferred.</param>
		/// <param name="name">The name assigned to the returned <see cref="T:Microsoft.SqlServer.Server.SqlMetaData" /> instance.</param>
		/// <returns>The inferred metadata as a <see cref="T:Microsoft.SqlServer.Server.SqlMetaData" /> instance.</returns>
		/// <exception cref="T:System.ArgumentNullException">The v<paramref name="alue" /> is <see langword="null" />. </exception>
		// Token: 0x060028CB RID: 10443 RVA: 0x000B5794 File Offset: 0x000B3994
		public static SqlMetaData InferFromValue(object value, string name)
		{
			if (value == null)
			{
				throw ADP.ArgumentNull("value");
			}
			SqlMetaData result;
			if (value is bool)
			{
				result = new SqlMetaData(name, SqlDbType.Bit);
			}
			else if (value is byte)
			{
				result = new SqlMetaData(name, SqlDbType.TinyInt);
			}
			else if (value is char)
			{
				result = new SqlMetaData(name, SqlDbType.NVarChar, 1L);
			}
			else if (value is DateTime)
			{
				result = new SqlMetaData(name, SqlDbType.DateTime);
			}
			else
			{
				if (value is DBNull)
				{
					throw ADP.InvalidDataType("DBNull");
				}
				if (value is decimal)
				{
					SqlDecimal sqlDecimal = new SqlDecimal((decimal)value);
					result = new SqlMetaData(name, SqlDbType.Decimal, sqlDecimal.Precision, sqlDecimal.Scale);
				}
				else if (value is double)
				{
					result = new SqlMetaData(name, SqlDbType.Float);
				}
				else if (value is short)
				{
					result = new SqlMetaData(name, SqlDbType.SmallInt);
				}
				else if (value is int)
				{
					result = new SqlMetaData(name, SqlDbType.Int);
				}
				else if (value is long)
				{
					result = new SqlMetaData(name, SqlDbType.BigInt);
				}
				else
				{
					if (value is sbyte)
					{
						throw ADP.InvalidDataType("SByte");
					}
					if (value is float)
					{
						result = new SqlMetaData(name, SqlDbType.Real);
					}
					else if (value is string)
					{
						long num = (long)((string)value).Length;
						if (num < 1L)
						{
							num = 1L;
						}
						if (4000L < num)
						{
							num = SqlMetaData.Max;
						}
						result = new SqlMetaData(name, SqlDbType.NVarChar, num);
					}
					else
					{
						if (value is ushort)
						{
							throw ADP.InvalidDataType("UInt16");
						}
						if (value is uint)
						{
							throw ADP.InvalidDataType("UInt32");
						}
						if (value is ulong)
						{
							throw ADP.InvalidDataType("UInt64");
						}
						if (value is byte[])
						{
							long num2 = (long)((byte[])value).Length;
							if (num2 < 1L)
							{
								num2 = 1L;
							}
							if (8000L < num2)
							{
								num2 = SqlMetaData.Max;
							}
							result = new SqlMetaData(name, SqlDbType.VarBinary, num2);
						}
						else if (value is char[])
						{
							long num3 = (long)((char[])value).Length;
							if (num3 < 1L)
							{
								num3 = 1L;
							}
							if (4000L < num3)
							{
								num3 = SqlMetaData.Max;
							}
							result = new SqlMetaData(name, SqlDbType.NVarChar, num3);
						}
						else if (value is Guid)
						{
							result = new SqlMetaData(name, SqlDbType.UniqueIdentifier);
						}
						else if (value != null)
						{
							result = new SqlMetaData(name, SqlDbType.Variant);
						}
						else if (value is SqlBinary)
						{
							SqlBinary sqlBinary = (SqlBinary)value;
							long num4;
							if (!sqlBinary.IsNull)
							{
								num4 = (long)sqlBinary.Length;
								if (num4 < 1L)
								{
									num4 = 1L;
								}
								if (8000L < num4)
								{
									num4 = SqlMetaData.Max;
								}
							}
							else
							{
								num4 = SqlMetaData.sxm_rgDefaults[21].MaxLength;
							}
							result = new SqlMetaData(name, SqlDbType.VarBinary, num4);
						}
						else if (value is SqlBoolean)
						{
							result = new SqlMetaData(name, SqlDbType.Bit);
						}
						else if (value is SqlByte)
						{
							result = new SqlMetaData(name, SqlDbType.TinyInt);
						}
						else if (value is SqlDateTime)
						{
							result = new SqlMetaData(name, SqlDbType.DateTime);
						}
						else if (value is SqlDouble)
						{
							result = new SqlMetaData(name, SqlDbType.Float);
						}
						else if (value is SqlGuid)
						{
							result = new SqlMetaData(name, SqlDbType.UniqueIdentifier);
						}
						else if (value is SqlInt16)
						{
							result = new SqlMetaData(name, SqlDbType.SmallInt);
						}
						else if (value is SqlInt32)
						{
							result = new SqlMetaData(name, SqlDbType.Int);
						}
						else if (value is SqlInt64)
						{
							result = new SqlMetaData(name, SqlDbType.BigInt);
						}
						else if (value is SqlMoney)
						{
							result = new SqlMetaData(name, SqlDbType.Money);
						}
						else if (value is SqlDecimal)
						{
							SqlDecimal sqlDecimal2 = (SqlDecimal)value;
							byte precision;
							byte scale;
							if (!sqlDecimal2.IsNull)
							{
								precision = sqlDecimal2.Precision;
								scale = sqlDecimal2.Scale;
							}
							else
							{
								precision = SqlMetaData.sxm_rgDefaults[5].Precision;
								scale = SqlMetaData.sxm_rgDefaults[5].Scale;
							}
							result = new SqlMetaData(name, SqlDbType.Decimal, precision, scale);
						}
						else if (value is SqlSingle)
						{
							result = new SqlMetaData(name, SqlDbType.Real);
						}
						else if (value is SqlString)
						{
							SqlString sqlString = (SqlString)value;
							if (!sqlString.IsNull)
							{
								long num5 = (long)sqlString.Value.Length;
								if (num5 < 1L)
								{
									num5 = 1L;
								}
								if (num5 > 4000L)
								{
									num5 = SqlMetaData.Max;
								}
								result = new SqlMetaData(name, SqlDbType.NVarChar, num5, (long)sqlString.LCID, sqlString.SqlCompareOptions);
							}
							else
							{
								result = new SqlMetaData(name, SqlDbType.NVarChar, SqlMetaData.sxm_rgDefaults[12].MaxLength);
							}
						}
						else if (value is SqlChars)
						{
							SqlChars sqlChars = (SqlChars)value;
							long num6;
							if (!sqlChars.IsNull)
							{
								num6 = sqlChars.Length;
								if (num6 < 1L)
								{
									num6 = 1L;
								}
								if (num6 > 4000L)
								{
									num6 = SqlMetaData.Max;
								}
							}
							else
							{
								num6 = SqlMetaData.sxm_rgDefaults[12].MaxLength;
							}
							result = new SqlMetaData(name, SqlDbType.NVarChar, num6);
						}
						else if (value is SqlBytes)
						{
							SqlBytes sqlBytes = (SqlBytes)value;
							long num7;
							if (!sqlBytes.IsNull)
							{
								num7 = sqlBytes.Length;
								if (num7 < 1L)
								{
									num7 = 1L;
								}
								else if (8000L < num7)
								{
									num7 = SqlMetaData.Max;
								}
							}
							else
							{
								num7 = SqlMetaData.sxm_rgDefaults[21].MaxLength;
							}
							result = new SqlMetaData(name, SqlDbType.VarBinary, num7);
						}
						else if (value is SqlXml)
						{
							result = new SqlMetaData(name, SqlDbType.Xml);
						}
						else if (value is TimeSpan)
						{
							result = new SqlMetaData(name, SqlDbType.Time, 0, SqlMetaData.InferScaleFromTimeTicks(((TimeSpan)value).Ticks));
						}
						else
						{
							if (!(value is DateTimeOffset))
							{
								throw ADP.UnknownDataType(value.GetType());
							}
							result = new SqlMetaData(name, SqlDbType.DateTimeOffset, 0, SqlMetaData.InferScaleFromTimeTicks(((DateTimeOffset)value).Ticks));
						}
					}
				}
			}
			return result;
		}

		/// <summary>Validates the specified <see cref="T:System.Boolean" /> value against the metadata, and adjusts the value if necessary.</summary>
		/// <param name="value">The value to validate against the <see cref="T:Microsoft.SqlServer.Server.SqlMetaData" /> instance.</param>
		/// <returns>The adjusted value as a <see cref="T:System.Boolean" />.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="Value" /> does not match the <see cref="T:Microsoft.SqlServer.Server.SqlMetaData" /> type, or <paramref name="value" /> could not be adjusted. </exception>
		// Token: 0x060028CC RID: 10444 RVA: 0x000B4E98 File Offset: 0x000B3098
		public bool Adjust(bool value)
		{
			if (SqlDbType.Bit != this.SqlDbType)
			{
				SqlMetaData.ThrowInvalidType();
			}
			return value;
		}

		/// <summary>Validates the specified <see cref="T:System.Byte" /> value against the metadata, and adjusts the value if necessary.</summary>
		/// <param name="value">The value to validate against the <see cref="T:Microsoft.SqlServer.Server.SqlMetaData" /> instance.</param>
		/// <returns>The adjusted value as a <see cref="T:System.Byte" />.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="Value" /> does not match the <see cref="T:Microsoft.SqlServer.Server.SqlMetaData" /> type, or <paramref name="value" /> could not be adjusted. </exception>
		// Token: 0x060028CD RID: 10445 RVA: 0x000B4EA9 File Offset: 0x000B30A9
		public byte Adjust(byte value)
		{
			if (SqlDbType.TinyInt != this.SqlDbType)
			{
				SqlMetaData.ThrowInvalidType();
			}
			return value;
		}

		/// <summary>Validates the specified array of <see cref="T:System.Byte" /> values against the metadata, and adjusts the value if necessary.</summary>
		/// <param name="value">The value to validate against the <see cref="T:Microsoft.SqlServer.Server.SqlMetaData" /> instance.</param>
		/// <returns>The adjusted value as an array of <see cref="T:System.Byte" /> values.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="Value" /> does not match the <see cref="T:Microsoft.SqlServer.Server.SqlMetaData" /> type, or <paramref name="value" /> could not be adjusted. </exception>
		// Token: 0x060028CE RID: 10446 RVA: 0x000B5D14 File Offset: 0x000B3F14
		public byte[] Adjust(byte[] value)
		{
			if (SqlDbType.Binary == this.SqlDbType || SqlDbType.Timestamp == this.SqlDbType)
			{
				if (value != null && (long)value.Length < this.MaxLength)
				{
					byte[] array = new byte[this.MaxLength];
					Buffer.BlockCopy(value, 0, array, 0, value.Length);
					Array.Clear(array, value.Length, array.Length - value.Length);
					return array;
				}
			}
			else if (SqlDbType.VarBinary != this.SqlDbType && SqlDbType.Image != this.SqlDbType)
			{
				SqlMetaData.ThrowInvalidType();
			}
			if (value == null)
			{
				return null;
			}
			if ((long)value.Length > this.MaxLength && SqlMetaData.Max != this.MaxLength)
			{
				byte[] array2 = new byte[this.MaxLength];
				Buffer.BlockCopy(value, 0, array2, 0, (int)this.MaxLength);
				value = array2;
			}
			return value;
		}

		/// <summary>Validates the specified <see cref="T:System.Char" /> value against the metadata, and adjusts the value if necessary.</summary>
		/// <param name="value">The value to validate against the <see cref="T:Microsoft.SqlServer.Server.SqlMetaData" /> instance.</param>
		/// <returns>The adjusted value as a <see cref="T:System.Char" />.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="Value" /> does not match the <see cref="T:Microsoft.SqlServer.Server.SqlMetaData" /> type, or <paramref name="value" /> could not be adjusted. </exception>
		// Token: 0x060028CF RID: 10447 RVA: 0x000B5DC8 File Offset: 0x000B3FC8
		public char Adjust(char value)
		{
			if (SqlDbType.Char == this.SqlDbType || SqlDbType.NChar == this.SqlDbType)
			{
				if (1L != this.MaxLength)
				{
					SqlMetaData.ThrowInvalidType();
				}
			}
			else if (1L > this.MaxLength || (SqlDbType.VarChar != this.SqlDbType && SqlDbType.NVarChar != this.SqlDbType && SqlDbType.Text != this.SqlDbType && SqlDbType.NText != this.SqlDbType))
			{
				SqlMetaData.ThrowInvalidType();
			}
			return value;
		}

		/// <summary>Validates the specified array of <see cref="T:System.Char" /> values against the metadata, and adjusts the value if necessary.</summary>
		/// <param name="value">The value to validate against the <see cref="T:Microsoft.SqlServer.Server.SqlMetaData" /> instance.</param>
		/// <returns>The adjusted value as an array <see cref="T:System.Char" /> values.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="Value" /> does not match the <see cref="T:Microsoft.SqlServer.Server.SqlMetaData" /> type, or <paramref name="value" /> could not be adjusted. </exception>
		// Token: 0x060028D0 RID: 10448 RVA: 0x000B5E34 File Offset: 0x000B4034
		public char[] Adjust(char[] value)
		{
			if (SqlDbType.Char == this.SqlDbType || SqlDbType.NChar == this.SqlDbType)
			{
				if (value != null)
				{
					long num = (long)value.Length;
					if (num < this.MaxLength)
					{
						char[] array = new char[(int)this.MaxLength];
						Array.Copy(value, 0, array, 0, (int)num);
						for (long num2 = num; num2 < (long)array.Length; num2 += 1L)
						{
							array[(int)(checked((IntPtr)num2))] = ' ';
						}
						return array;
					}
				}
			}
			else if (SqlDbType.VarChar != this.SqlDbType && SqlDbType.NVarChar != this.SqlDbType && SqlDbType.Text != this.SqlDbType && SqlDbType.NText != this.SqlDbType)
			{
				SqlMetaData.ThrowInvalidType();
			}
			if (value == null)
			{
				return null;
			}
			if ((long)value.Length > this.MaxLength && SqlMetaData.Max != this.MaxLength)
			{
				char[] array2 = new char[this.MaxLength];
				Array.Copy(value, 0, array2, 0, (int)this.MaxLength);
				value = array2;
			}
			return value;
		}

		// Token: 0x060028D1 RID: 10449 RVA: 0x000B5F04 File Offset: 0x000B4104
		internal static SqlMetaData GetPartialLengthMetaData(SqlMetaData md)
		{
			if (md.IsPartialLength)
			{
				return md;
			}
			if (md.SqlDbType == SqlDbType.Xml)
			{
				SqlMetaData.ThrowInvalidType();
			}
			if (md.SqlDbType == SqlDbType.NVarChar || md.SqlDbType == SqlDbType.VarChar || md.SqlDbType == SqlDbType.VarBinary)
			{
				return new SqlMetaData(md.Name, md.SqlDbType, SqlMetaData.Max, 0, 0, md.LocaleId, md.CompareOptions, null, null, null, true);
			}
			return md;
		}

		// Token: 0x060028D2 RID: 10450 RVA: 0x000B5F72 File Offset: 0x000B4172
		private static void ThrowInvalidType()
		{
			throw ADP.InvalidMetaDataValue();
		}

		// Token: 0x060028D3 RID: 10451 RVA: 0x000B5F79 File Offset: 0x000B4179
		private void VerifyDateTimeRange(DateTime value)
		{
			if (SqlDbType.SmallDateTime == this.SqlDbType && (SqlMetaData.s_dtSmallMax < value || SqlMetaData.s_dtSmallMin > value))
			{
				SqlMetaData.ThrowInvalidType();
			}
		}

		// Token: 0x060028D4 RID: 10452 RVA: 0x000B5FA4 File Offset: 0x000B41A4
		private void VerifyMoneyRange(SqlMoney value)
		{
			if (SqlDbType.SmallMoney == this.SqlDbType && ((SqlMetaData.s_smSmallMax < value).Value || (SqlMetaData.s_smSmallMin > value).Value))
			{
				SqlMetaData.ThrowInvalidType();
			}
		}

		// Token: 0x060028D5 RID: 10453 RVA: 0x000B5FEC File Offset: 0x000B41EC
		private SqlDecimal InternalAdjustSqlDecimal(SqlDecimal value)
		{
			if (!value.IsNull && (value.Precision != this.Precision || value.Scale != this.Scale))
			{
				if (value.Scale != this.Scale)
				{
					value = SqlDecimal.AdjustScale(value, (int)(this.Scale - value.Scale), false);
				}
				return SqlDecimal.ConvertToPrecScale(value, (int)this.Precision, (int)this.Scale);
			}
			return value;
		}

		// Token: 0x060028D6 RID: 10454 RVA: 0x000B605A File Offset: 0x000B425A
		private void VerifyTimeRange(TimeSpan value)
		{
			if (SqlDbType.Time == this.SqlDbType && (SqlMetaData.s_timeMin > value || value > SqlMetaData.s_timeMax))
			{
				SqlMetaData.ThrowInvalidType();
			}
		}

		// Token: 0x060028D7 RID: 10455 RVA: 0x000B6085 File Offset: 0x000B4285
		private long InternalAdjustTimeTicks(long ticks)
		{
			return ticks / SqlMetaData.s_unitTicksFromScale[(int)this.Scale] * SqlMetaData.s_unitTicksFromScale[(int)this.Scale];
		}

		// Token: 0x060028D8 RID: 10456 RVA: 0x000B60A4 File Offset: 0x000B42A4
		private static byte InferScaleFromTimeTicks(long ticks)
		{
			for (byte b = 0; b < 7; b += 1)
			{
				if (ticks / SqlMetaData.s_unitTicksFromScale[(int)b] * SqlMetaData.s_unitTicksFromScale[(int)b] == ticks)
				{
					return b;
				}
			}
			return 7;
		}

		// Token: 0x060028D9 RID: 10457 RVA: 0x000B60D8 File Offset: 0x000B42D8
		private void SetDefaultsForType(SqlDbType dbType)
		{
			if (SqlDbType.BigInt <= dbType && SqlDbType.DateTimeOffset >= dbType)
			{
				SqlMetaData sqlMetaData = SqlMetaData.sxm_rgDefaults[(int)dbType];
				this._sqlDbType = dbType;
				this._lMaxLength = sqlMetaData.MaxLength;
				this._bPrecision = sqlMetaData.Precision;
				this._bScale = sqlMetaData.Scale;
				this._lLocale = sqlMetaData.LocaleId;
				this._eCompareOptions = sqlMetaData.CompareOptions;
			}
		}

		// Token: 0x060028DA RID: 10458 RVA: 0x000B613C File Offset: 0x000B433C
		private void ThrowIfUdt(SqlDbType dbType)
		{
			if (dbType == SqlDbType.Udt)
			{
				throw ADP.DbTypeNotSupported(SqlDbType.Udt.ToString());
			}
		}

		/// <summary>Initializes a new instance of the <see cref="T:Microsoft.SqlServer.Server.SqlMetaData" /> class with the specified column name, type, and user-defined type (UDT).</summary>
		/// <param name="name">The name of the column.</param>
		/// <param name="dbType">The SQL Server type of the parameter or column.</param>
		/// <param name="userDefinedType">A <see cref="T:System.Type" /> instance that points to the UDT.</param>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="Name" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.ArgumentException">A SqlDbType that is not allowed was passed to the constructor as <paramref name="dbType" />, or <paramref name="userDefinedType" /> points to a type that does not have <see cref="T:Microsoft.SqlServer.Server.SqlUserDefinedTypeAttribute" /> declared. </exception>
		// Token: 0x060028DB RID: 10459 RVA: 0x000B6164 File Offset: 0x000B4364
		public SqlMetaData(string name, SqlDbType dbType, Type userDefinedType) : this(name, dbType, -1L, 0, 0, 0L, SqlCompareOptions.None, userDefinedType)
		{
		}

		/// <summary>Gets the data type of the column or parameter.</summary>
		/// <returns>The data type of the column or parameter as a <see cref="T:System.Data.DbType" />.</returns>
		// Token: 0x17000692 RID: 1682
		// (get) Token: 0x060028DC RID: 10460 RVA: 0x000554AE File Offset: 0x000536AE
		[MonoTODO]
		public DbType DbType
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		// Token: 0x060028DD RID: 10461 RVA: 0x000B6184 File Offset: 0x000B4384
		// Note: this type is marked as 'beforefieldinit'.
		static SqlMetaData()
		{
		}

		/// <summary>Gets the common language runtime (CLR) type of a user-defined type (UDT).</summary>
		/// <returns>The CLR type name of a user-defined type as a <see cref="T:System.Type" />.</returns>
		// Token: 0x17000693 RID: 1683
		// (get) Token: 0x060028DE RID: 10462 RVA: 0x00051759 File Offset: 0x0004F959
		public Type Type
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		// Token: 0x04001804 RID: 6148
		private string _strName;

		// Token: 0x04001805 RID: 6149
		private long _lMaxLength;

		// Token: 0x04001806 RID: 6150
		private SqlDbType _sqlDbType;

		// Token: 0x04001807 RID: 6151
		private byte _bPrecision;

		// Token: 0x04001808 RID: 6152
		private byte _bScale;

		// Token: 0x04001809 RID: 6153
		private long _lLocale;

		// Token: 0x0400180A RID: 6154
		private SqlCompareOptions _eCompareOptions;

		// Token: 0x0400180B RID: 6155
		private string _xmlSchemaCollectionDatabase;

		// Token: 0x0400180C RID: 6156
		private string _xmlSchemaCollectionOwningSchema;

		// Token: 0x0400180D RID: 6157
		private string _xmlSchemaCollectionName;

		// Token: 0x0400180E RID: 6158
		private bool _bPartialLength;

		// Token: 0x0400180F RID: 6159
		private bool _useServerDefault;

		// Token: 0x04001810 RID: 6160
		private bool _isUniqueKey;

		// Token: 0x04001811 RID: 6161
		private SortOrder _columnSortOrder;

		// Token: 0x04001812 RID: 6162
		private int _sortOrdinal;

		// Token: 0x04001813 RID: 6163
		private const long x_lMax = -1L;

		// Token: 0x04001814 RID: 6164
		private const long x_lServerMaxUnicode = 4000L;

		// Token: 0x04001815 RID: 6165
		private const long x_lServerMaxANSI = 8000L;

		// Token: 0x04001816 RID: 6166
		private const long x_lServerMaxBinary = 8000L;

		// Token: 0x04001817 RID: 6167
		private const bool x_defaultUseServerDefault = false;

		// Token: 0x04001818 RID: 6168
		private const bool x_defaultIsUniqueKey = false;

		// Token: 0x04001819 RID: 6169
		private const SortOrder x_defaultColumnSortOrder = SortOrder.Unspecified;

		// Token: 0x0400181A RID: 6170
		private const int x_defaultSortOrdinal = -1;

		// Token: 0x0400181B RID: 6171
		private const SqlCompareOptions x_eDefaultStringCompareOptions = SqlCompareOptions.IgnoreCase | SqlCompareOptions.IgnoreKanaType | SqlCompareOptions.IgnoreWidth;

		// Token: 0x0400181C RID: 6172
		private static byte[] s_maxLenFromPrecision = new byte[]
		{
			5,
			5,
			5,
			5,
			5,
			5,
			5,
			5,
			5,
			9,
			9,
			9,
			9,
			9,
			9,
			9,
			9,
			9,
			9,
			13,
			13,
			13,
			13,
			13,
			13,
			13,
			13,
			13,
			17,
			17,
			17,
			17,
			17,
			17,
			17,
			17,
			17,
			17
		};

		// Token: 0x0400181D RID: 6173
		private const byte MaxTimeScale = 7;

		// Token: 0x0400181E RID: 6174
		private static byte[] s_maxVarTimeLenOffsetFromScale = new byte[]
		{
			2,
			2,
			2,
			1,
			1,
			0,
			0,
			0
		};

		// Token: 0x0400181F RID: 6175
		private static readonly DateTime s_dtSmallMax = new DateTime(2079, 6, 6, 23, 59, 29, 998);

		// Token: 0x04001820 RID: 6176
		private static readonly DateTime s_dtSmallMin = new DateTime(1899, 12, 31, 23, 59, 29, 999);

		// Token: 0x04001821 RID: 6177
		private static readonly SqlMoney s_smSmallMax = new SqlMoney(214748.3647m);

		// Token: 0x04001822 RID: 6178
		private static readonly SqlMoney s_smSmallMin = new SqlMoney(-214748.3648m);

		// Token: 0x04001823 RID: 6179
		private static readonly TimeSpan s_timeMin = TimeSpan.Zero;

		// Token: 0x04001824 RID: 6180
		private static readonly TimeSpan s_timeMax = new TimeSpan(863999999999L);

		// Token: 0x04001825 RID: 6181
		private static readonly long[] s_unitTicksFromScale = new long[]
		{
			10000000L,
			1000000L,
			100000L,
			10000L,
			1000L,
			100L,
			10L,
			1L
		};

		// Token: 0x04001826 RID: 6182
		internal static SqlMetaData[] sxm_rgDefaults = new SqlMetaData[]
		{
			new SqlMetaData("bigint", SqlDbType.BigInt, 8L, 19, 0, 0L, SqlCompareOptions.None, false),
			new SqlMetaData("binary", SqlDbType.Binary, 1L, 0, 0, 0L, SqlCompareOptions.None, false),
			new SqlMetaData("bit", SqlDbType.Bit, 1L, 1, 0, 0L, SqlCompareOptions.None, false),
			new SqlMetaData("char", SqlDbType.Char, 1L, 0, 0, 0L, SqlCompareOptions.IgnoreCase | SqlCompareOptions.IgnoreKanaType | SqlCompareOptions.IgnoreWidth, false),
			new SqlMetaData("datetime", SqlDbType.DateTime, 8L, 23, 3, 0L, SqlCompareOptions.None, false),
			new SqlMetaData("decimal", SqlDbType.Decimal, 9L, 18, 0, 0L, SqlCompareOptions.None, false),
			new SqlMetaData("float", SqlDbType.Float, 8L, 53, 0, 0L, SqlCompareOptions.None, false),
			new SqlMetaData("image", SqlDbType.Image, -1L, 0, 0, 0L, SqlCompareOptions.None, false),
			new SqlMetaData("int", SqlDbType.Int, 4L, 10, 0, 0L, SqlCompareOptions.None, false),
			new SqlMetaData("money", SqlDbType.Money, 8L, 19, 4, 0L, SqlCompareOptions.None, false),
			new SqlMetaData("nchar", SqlDbType.NChar, 1L, 0, 0, 0L, SqlCompareOptions.IgnoreCase | SqlCompareOptions.IgnoreKanaType | SqlCompareOptions.IgnoreWidth, false),
			new SqlMetaData("ntext", SqlDbType.NText, -1L, 0, 0, 0L, SqlCompareOptions.IgnoreCase | SqlCompareOptions.IgnoreKanaType | SqlCompareOptions.IgnoreWidth, false),
			new SqlMetaData("nvarchar", SqlDbType.NVarChar, 4000L, 0, 0, 0L, SqlCompareOptions.IgnoreCase | SqlCompareOptions.IgnoreKanaType | SqlCompareOptions.IgnoreWidth, false),
			new SqlMetaData("real", SqlDbType.Real, 4L, 24, 0, 0L, SqlCompareOptions.None, false),
			new SqlMetaData("uniqueidentifier", SqlDbType.UniqueIdentifier, 16L, 0, 0, 0L, SqlCompareOptions.None, false),
			new SqlMetaData("smalldatetime", SqlDbType.SmallDateTime, 4L, 16, 0, 0L, SqlCompareOptions.None, false),
			new SqlMetaData("smallint", SqlDbType.SmallInt, 2L, 5, 0, 0L, SqlCompareOptions.None, false),
			new SqlMetaData("smallmoney", SqlDbType.SmallMoney, 4L, 10, 4, 0L, SqlCompareOptions.None, false),
			new SqlMetaData("text", SqlDbType.Text, -1L, 0, 0, 0L, SqlCompareOptions.IgnoreCase | SqlCompareOptions.IgnoreKanaType | SqlCompareOptions.IgnoreWidth, false),
			new SqlMetaData("timestamp", SqlDbType.Timestamp, 8L, 0, 0, 0L, SqlCompareOptions.None, false),
			new SqlMetaData("tinyint", SqlDbType.TinyInt, 1L, 3, 0, 0L, SqlCompareOptions.None, false),
			new SqlMetaData("varbinary", SqlDbType.VarBinary, 8000L, 0, 0, 0L, SqlCompareOptions.None, false),
			new SqlMetaData("varchar", SqlDbType.VarChar, 8000L, 0, 0, 0L, SqlCompareOptions.IgnoreCase | SqlCompareOptions.IgnoreKanaType | SqlCompareOptions.IgnoreWidth, false),
			new SqlMetaData("sql_variant", SqlDbType.Variant, 8016L, 0, 0, 0L, SqlCompareOptions.None, false),
			new SqlMetaData("nvarchar", SqlDbType.NVarChar, 1L, 0, 0, 0L, SqlCompareOptions.IgnoreCase | SqlCompareOptions.IgnoreKanaType | SqlCompareOptions.IgnoreWidth, false),
			new SqlMetaData("xml", SqlDbType.Xml, -1L, 0, 0, 0L, SqlCompareOptions.IgnoreCase | SqlCompareOptions.IgnoreKanaType | SqlCompareOptions.IgnoreWidth, true),
			new SqlMetaData("nvarchar", SqlDbType.NVarChar, 1L, 0, 0, 0L, SqlCompareOptions.IgnoreCase | SqlCompareOptions.IgnoreKanaType | SqlCompareOptions.IgnoreWidth, false),
			new SqlMetaData("nvarchar", SqlDbType.NVarChar, 4000L, 0, 0, 0L, SqlCompareOptions.IgnoreCase | SqlCompareOptions.IgnoreKanaType | SqlCompareOptions.IgnoreWidth, false),
			new SqlMetaData("nvarchar", SqlDbType.NVarChar, 4000L, 0, 0, 0L, SqlCompareOptions.IgnoreCase | SqlCompareOptions.IgnoreKanaType | SqlCompareOptions.IgnoreWidth, false),
			new SqlMetaData("udt", SqlDbType.Structured, 0L, 0, 0, 0L, SqlCompareOptions.None, false),
			new SqlMetaData("table", SqlDbType.Structured, 0L, 0, 0, 0L, SqlCompareOptions.None, false),
			new SqlMetaData("date", SqlDbType.Date, 3L, 10, 0, 0L, SqlCompareOptions.None, false),
			new SqlMetaData("time", SqlDbType.Time, 5L, 0, 7, 0L, SqlCompareOptions.None, false),
			new SqlMetaData("datetime2", SqlDbType.DateTime2, 8L, 0, 7, 0L, SqlCompareOptions.None, false),
			new SqlMetaData("datetimeoffset", SqlDbType.DateTimeOffset, 10L, 0, 7, 0L, SqlCompareOptions.None, false)
		};
	}
}
