﻿using System;
using System.Collections.Generic;
using System.Data.Common;

namespace Microsoft.SqlServer.Server
{
	// Token: 0x02000307 RID: 775
	internal class SmiMetaDataPropertyCollection
	{
		// Token: 0x060026C0 RID: 9920 RVA: 0x000AECBC File Offset: 0x000ACEBC
		private static SmiMetaDataPropertyCollection CreateEmptyInstance()
		{
			SmiMetaDataPropertyCollection smiMetaDataPropertyCollection = new SmiMetaDataPropertyCollection();
			smiMetaDataPropertyCollection.SetReadOnly();
			return smiMetaDataPropertyCollection;
		}

		// Token: 0x060026C1 RID: 9921 RVA: 0x000AECCC File Offset: 0x000ACECC
		internal SmiMetaDataPropertyCollection()
		{
			this._properties = new SmiMetaDataProperty[3];
			this._isReadOnly = false;
			this._properties[0] = SmiMetaDataPropertyCollection.s_emptyDefaultFields;
			this._properties[1] = SmiMetaDataPropertyCollection.s_emptySortOrder;
			this._properties[2] = SmiMetaDataPropertyCollection.s_emptyUniqueKey;
		}

		// Token: 0x1700065E RID: 1630
		internal SmiMetaDataProperty this[SmiPropertySelector key]
		{
			get
			{
				return this._properties[(int)key];
			}
			set
			{
				if (value == null)
				{
					throw ADP.InternalError(ADP.InternalErrorCode.InvalidSmiCall);
				}
				this.EnsureWritable();
				this._properties[(int)key] = value;
			}
		}

		// Token: 0x1700065F RID: 1631
		// (get) Token: 0x060026C4 RID: 9924 RVA: 0x000AED3F File Offset: 0x000ACF3F
		internal bool IsReadOnly
		{
			get
			{
				return this._isReadOnly;
			}
		}

		// Token: 0x060026C5 RID: 9925 RVA: 0x000AED47 File Offset: 0x000ACF47
		internal void SetReadOnly()
		{
			this._isReadOnly = true;
		}

		// Token: 0x060026C6 RID: 9926 RVA: 0x000AED50 File Offset: 0x000ACF50
		private void EnsureWritable()
		{
			if (this.IsReadOnly)
			{
				throw ADP.InternalError(ADP.InternalErrorCode.InvalidSmiCall);
			}
		}

		// Token: 0x060026C7 RID: 9927 RVA: 0x000AED62 File Offset: 0x000ACF62
		// Note: this type is marked as 'beforefieldinit'.
		static SmiMetaDataPropertyCollection()
		{
		}

		// Token: 0x040017A7 RID: 6055
		private const int SelectorCount = 3;

		// Token: 0x040017A8 RID: 6056
		private SmiMetaDataProperty[] _properties;

		// Token: 0x040017A9 RID: 6057
		private bool _isReadOnly;

		// Token: 0x040017AA RID: 6058
		private static readonly SmiDefaultFieldsProperty s_emptyDefaultFields = new SmiDefaultFieldsProperty(new List<bool>());

		// Token: 0x040017AB RID: 6059
		private static readonly SmiOrderProperty s_emptySortOrder = new SmiOrderProperty(new List<SmiOrderProperty.SmiColumnOrder>());

		// Token: 0x040017AC RID: 6060
		private static readonly SmiUniqueKeyProperty s_emptyUniqueKey = new SmiUniqueKeyProperty(new List<bool>());

		// Token: 0x040017AD RID: 6061
		internal static readonly SmiMetaDataPropertyCollection EmptyInstance = SmiMetaDataPropertyCollection.CreateEmptyInstance();
	}
}
