﻿using System;
using System.Data.Common;

namespace Microsoft.SqlServer.Server
{
	/// <summary>Indicates that the type should be registered as a user-defined aggregate. The properties on the attribute reflect the physical attributes used when the type is registered with SQL Server. This class cannot be inherited.</summary>
	// Token: 0x02000321 RID: 801
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct, AllowMultiple = false, Inherited = false)]
	public sealed class SqlUserDefinedAggregateAttribute : Attribute
	{
		/// <summary>A required attribute on a user-defined aggregate, used to indicate that the given type is a user-defined aggregate and the storage format of the user-defined aggregate.</summary>
		/// <param name="format">One of the <see cref="T:Microsoft.SqlServer.Server.Format" /> values representing the serialization format of the aggregate.</param>
		// Token: 0x06002912 RID: 10514 RVA: 0x000B67FB File Offset: 0x000B49FB
		public SqlUserDefinedAggregateAttribute(Format format)
		{
			if (format == Format.Unknown)
			{
				throw ADP.NotSupportedUserDefinedTypeSerializationFormat(format, "format");
			}
			if (format - Format.Native > 1)
			{
				throw ADP.InvalidUserDefinedTypeSerializationFormat(format);
			}
			this.m_format = format;
		}

		/// <summary>The maximum size, in bytes, of the aggregate instance.</summary>
		/// <returns>An <see cref="T:System.Int32" /> value representing the maximum size of the aggregate instance.</returns>
		// Token: 0x170006AA RID: 1706
		// (get) Token: 0x06002913 RID: 10515 RVA: 0x000B682F File Offset: 0x000B4A2F
		// (set) Token: 0x06002914 RID: 10516 RVA: 0x000B6837 File Offset: 0x000B4A37
		public int MaxByteSize
		{
			get
			{
				return this.m_MaxByteSize;
			}
			set
			{
				if (value < -1 || value > 8000)
				{
					throw ADP.ArgumentOutOfRange(Res.GetString("range: 0-8000"), "MaxByteSize", value);
				}
				this.m_MaxByteSize = value;
			}
		}

		/// <summary>Indicates whether the aggregate is invariant to duplicates.</summary>
		/// <returns>
		///     <see langword="true" /> if the aggregate is invariant to duplicates; otherwise <see langword="false" />.</returns>
		// Token: 0x170006AB RID: 1707
		// (get) Token: 0x06002915 RID: 10517 RVA: 0x000B6867 File Offset: 0x000B4A67
		// (set) Token: 0x06002916 RID: 10518 RVA: 0x000B686F File Offset: 0x000B4A6F
		public bool IsInvariantToDuplicates
		{
			get
			{
				return this.m_fInvariantToDup;
			}
			set
			{
				this.m_fInvariantToDup = value;
			}
		}

		/// <summary>Indicates whether the aggregate is invariant to nulls.</summary>
		/// <returns>
		///     <see langword="true" /> if the aggregate is invariant to nulls; otherwise <see langword="false" />.</returns>
		// Token: 0x170006AC RID: 1708
		// (get) Token: 0x06002917 RID: 10519 RVA: 0x000B6878 File Offset: 0x000B4A78
		// (set) Token: 0x06002918 RID: 10520 RVA: 0x000B6880 File Offset: 0x000B4A80
		public bool IsInvariantToNulls
		{
			get
			{
				return this.m_fInvariantToNulls;
			}
			set
			{
				this.m_fInvariantToNulls = value;
			}
		}

		/// <summary>Indicates whether the aggregate is invariant to order.</summary>
		/// <returns>
		///     <see langword="true" /> if the aggregate is invariant to order; otherwise <see langword="false" />.</returns>
		// Token: 0x170006AD RID: 1709
		// (get) Token: 0x06002919 RID: 10521 RVA: 0x000B6889 File Offset: 0x000B4A89
		// (set) Token: 0x0600291A RID: 10522 RVA: 0x000B6891 File Offset: 0x000B4A91
		public bool IsInvariantToOrder
		{
			get
			{
				return this.m_fInvariantToOrder;
			}
			set
			{
				this.m_fInvariantToOrder = value;
			}
		}

		/// <summary>Indicates whether the aggregate returns <see langword="null" /> if no values have been accumulated.</summary>
		/// <returns>
		///     <see langword="true" /> if the aggregate returns <see langword="null" /> if no values have been accumulated; otherwise <see langword="false" />.</returns>
		// Token: 0x170006AE RID: 1710
		// (get) Token: 0x0600291B RID: 10523 RVA: 0x000B689A File Offset: 0x000B4A9A
		// (set) Token: 0x0600291C RID: 10524 RVA: 0x000B68A2 File Offset: 0x000B4AA2
		public bool IsNullIfEmpty
		{
			get
			{
				return this.m_fNullIfEmpty;
			}
			set
			{
				this.m_fNullIfEmpty = value;
			}
		}

		/// <summary>The serialization format as a <see cref="T:Microsoft.SqlServer.Server.Format" />.</summary>
		/// <returns>A <see cref="T:Microsoft.SqlServer.Server.Format" /> representing the serialization format.</returns>
		// Token: 0x170006AF RID: 1711
		// (get) Token: 0x0600291D RID: 10525 RVA: 0x000B68AB File Offset: 0x000B4AAB
		public Format Format
		{
			get
			{
				return this.m_format;
			}
		}

		/// <summary>The name of the aggregate.</summary>
		/// <returns>A <see cref="T:System.String" /> value representing the name of the aggregate.</returns>
		// Token: 0x170006B0 RID: 1712
		// (get) Token: 0x0600291E RID: 10526 RVA: 0x000B68B3 File Offset: 0x000B4AB3
		// (set) Token: 0x0600291F RID: 10527 RVA: 0x000B68BB File Offset: 0x000B4ABB
		public string Name
		{
			get
			{
				return this.m_fName;
			}
			set
			{
				this.m_fName = value;
			}
		}

		// Token: 0x04001843 RID: 6211
		private int m_MaxByteSize;

		// Token: 0x04001844 RID: 6212
		private bool m_fInvariantToDup;

		// Token: 0x04001845 RID: 6213
		private bool m_fInvariantToNulls;

		// Token: 0x04001846 RID: 6214
		private bool m_fInvariantToOrder = true;

		// Token: 0x04001847 RID: 6215
		private bool m_fNullIfEmpty;

		// Token: 0x04001848 RID: 6216
		private Format m_format;

		// Token: 0x04001849 RID: 6217
		private string m_fName;

		/// <summary>The maximum size, in bytes, required to store the state of this aggregate instance during computation.</summary>
		// Token: 0x0400184A RID: 6218
		public const int MaxByteSizeValue = 8000;
	}
}
