﻿using System;

namespace Microsoft.SqlServer.Server
{
	/// <summary>The <see cref="T:Microsoft.SqlServer.Server.TriggerAction" /> enumeration is used by the <see cref="T:Microsoft.SqlServer.Server.SqlTriggerContext" /> class to indicate what action fired the trigger. </summary>
	// Token: 0x02000325 RID: 805
	public enum TriggerAction
	{
		/// <summary>An invalid trigger action, one that is not exposed to the user, occurred.</summary>
		// Token: 0x040018B5 RID: 6325
		Invalid,
		/// <summary>An INSERT Transact-SQL statement was executed.</summary>
		// Token: 0x040018B6 RID: 6326
		Insert,
		/// <summary>An UPDATE Transact-SQL statement was executed.</summary>
		// Token: 0x040018B7 RID: 6327
		Update,
		/// <summary>A DELETE Transact-SQL statement was executed.</summary>
		// Token: 0x040018B8 RID: 6328
		Delete,
		/// <summary>A CREATE TABLE Transact-SQL statement was executed.</summary>
		// Token: 0x040018B9 RID: 6329
		CreateTable = 21,
		/// <summary>An ALTER TABLE Transact-SQL statement was executed.</summary>
		// Token: 0x040018BA RID: 6330
		AlterTable,
		/// <summary>A DROP TABLE Transact-SQL statement was executed.</summary>
		// Token: 0x040018BB RID: 6331
		DropTable,
		/// <summary>A CREATE INDEX Transact-SQL statement was executed.</summary>
		// Token: 0x040018BC RID: 6332
		CreateIndex,
		/// <summary>An ALTER INDEX Transact-SQL statement was executed.</summary>
		// Token: 0x040018BD RID: 6333
		AlterIndex,
		/// <summary>A DROP INDEX Transact-SQL statement was executed.</summary>
		// Token: 0x040018BE RID: 6334
		DropIndex,
		/// <summary>A CREATE SYNONYM Transact-SQL statement was executed.</summary>
		// Token: 0x040018BF RID: 6335
		CreateSynonym = 34,
		/// <summary>A DROP SYNONYM Transact-SQL statement was executed.</summary>
		// Token: 0x040018C0 RID: 6336
		DropSynonym = 36,
		/// <summary>Not available.</summary>
		// Token: 0x040018C1 RID: 6337
		CreateSecurityExpression = 31,
		/// <summary>Not available.</summary>
		// Token: 0x040018C2 RID: 6338
		DropSecurityExpression = 33,
		/// <summary>A CREATE VIEW Transact-SQL statement was executed.</summary>
		// Token: 0x040018C3 RID: 6339
		CreateView = 41,
		/// <summary>An ALTER VIEW Transact-SQL statement was executed.</summary>
		// Token: 0x040018C4 RID: 6340
		AlterView,
		/// <summary>A DROP VIEW Transact-SQL statement was executed.</summary>
		// Token: 0x040018C5 RID: 6341
		DropView,
		/// <summary>A CREATE PROCEDURE Transact-SQL statement was executed.</summary>
		// Token: 0x040018C6 RID: 6342
		CreateProcedure = 51,
		/// <summary>An ALTER PROCEDURE Transact-SQL statement was executed.</summary>
		// Token: 0x040018C7 RID: 6343
		AlterProcedure,
		/// <summary>A DROP PROCEDURE Transact-SQL statement was executed.</summary>
		// Token: 0x040018C8 RID: 6344
		DropProcedure,
		/// <summary>A CREATE FUNCTION Transact-SQL statement was executed.</summary>
		// Token: 0x040018C9 RID: 6345
		CreateFunction = 61,
		/// <summary>An ALTER FUNCTION Transact-SQL statement was executed.</summary>
		// Token: 0x040018CA RID: 6346
		AlterFunction,
		/// <summary>A DROP FUNCTION Transact-SQL statement was executed.</summary>
		// Token: 0x040018CB RID: 6347
		DropFunction,
		/// <summary>A CREATE TRIGGER Transact-SQL statement was executed.</summary>
		// Token: 0x040018CC RID: 6348
		CreateTrigger = 71,
		/// <summary>An ALTER TRIGGER Transact-SQL statement was executed.</summary>
		// Token: 0x040018CD RID: 6349
		AlterTrigger,
		/// <summary>A DROP TRIGGER Transact-SQL statement was executed.</summary>
		// Token: 0x040018CE RID: 6350
		DropTrigger,
		/// <summary>A CREATE EVENT NOTIFICATION Transact-SQL statement was executed.</summary>
		// Token: 0x040018CF RID: 6351
		CreateEventNotification,
		/// <summary>A DROP EVENT NOTIFICATION Transact-SQL statement was executed.</summary>
		// Token: 0x040018D0 RID: 6352
		DropEventNotification = 76,
		/// <summary>A CREATE TYPE Transact-SQL statement was executed.</summary>
		// Token: 0x040018D1 RID: 6353
		CreateType = 91,
		/// <summary>A DROP TYPE Transact-SQL statement was executed.</summary>
		// Token: 0x040018D2 RID: 6354
		DropType = 93,
		/// <summary>A CREATE ASSEMBLY Transact-SQL statement was executed.</summary>
		// Token: 0x040018D3 RID: 6355
		CreateAssembly = 101,
		/// <summary>An ALTER ASSEMBLY Transact-SQL statement was executed.</summary>
		// Token: 0x040018D4 RID: 6356
		AlterAssembly,
		/// <summary>A DROP ASSEMBLY Transact-SQL statement was executed.</summary>
		// Token: 0x040018D5 RID: 6357
		DropAssembly,
		/// <summary>A CREATE USER Transact-SQL statement was executed.</summary>
		// Token: 0x040018D6 RID: 6358
		CreateUser = 131,
		/// <summary>An ALTER USER Transact-SQL statement was executed.</summary>
		// Token: 0x040018D7 RID: 6359
		AlterUser,
		/// <summary>A DROP USER Transact-SQL statement was executed.</summary>
		// Token: 0x040018D8 RID: 6360
		DropUser,
		/// <summary>A CREATE ROLE Transact-SQL statement was executed.</summary>
		// Token: 0x040018D9 RID: 6361
		CreateRole,
		/// <summary>An ALTER ROLE Transact-SQL statement was executed.</summary>
		// Token: 0x040018DA RID: 6362
		AlterRole,
		/// <summary>A DROP ROLE Transact-SQL statement was executed.</summary>
		// Token: 0x040018DB RID: 6363
		DropRole,
		/// <summary>A CREATE APPLICATION ROLE Transact-SQL statement was executed.</summary>
		// Token: 0x040018DC RID: 6364
		CreateAppRole,
		/// <summary>An ALTER APPLICATION ROLE Transact-SQL statement was executed.</summary>
		// Token: 0x040018DD RID: 6365
		AlterAppRole,
		/// <summary>A DROP APPLICATION ROLE Transact-SQL statement was executed.</summary>
		// Token: 0x040018DE RID: 6366
		DropAppRole,
		/// <summary>A CREATE SCHEMA Transact-SQL statement was executed.</summary>
		// Token: 0x040018DF RID: 6367
		CreateSchema = 141,
		/// <summary>An ALTER SCHEMA Transact-SQL statement was executed.</summary>
		// Token: 0x040018E0 RID: 6368
		AlterSchema,
		/// <summary>A DROP SCHEMA Transact-SQL statement was executed.</summary>
		// Token: 0x040018E1 RID: 6369
		DropSchema,
		/// <summary>A CREATE LOGIN Transact-SQL statement was executed.</summary>
		// Token: 0x040018E2 RID: 6370
		CreateLogin,
		/// <summary>An ALTER LOGIN Transact-SQL statement was executed.</summary>
		// Token: 0x040018E3 RID: 6371
		AlterLogin,
		/// <summary>A DROP LOGIN Transact-SQL statement was executed.</summary>
		// Token: 0x040018E4 RID: 6372
		DropLogin,
		/// <summary>A CREATE MESSAGE TYPE Transact-SQL statement was executed.</summary>
		// Token: 0x040018E5 RID: 6373
		CreateMsgType = 151,
		/// <summary>A DROP MESSAGE TYPE Transact-SQL statement was executed.</summary>
		// Token: 0x040018E6 RID: 6374
		DropMsgType = 153,
		/// <summary>A CREATE CONTRACT Transact-SQL statement was executed.</summary>
		// Token: 0x040018E7 RID: 6375
		CreateContract,
		/// <summary>A DROP CONTRACT Transact-SQL statement was executed.</summary>
		// Token: 0x040018E8 RID: 6376
		DropContract = 156,
		/// <summary>A CREATE QUEUE Transact-SQL statement was executed.</summary>
		// Token: 0x040018E9 RID: 6377
		CreateQueue,
		/// <summary>An ALTER QUEUE Transact-SQL statement was executed.</summary>
		// Token: 0x040018EA RID: 6378
		AlterQueue,
		/// <summary>A DROP QUEUE Transact-SQL statement was executed.</summary>
		// Token: 0x040018EB RID: 6379
		DropQueue,
		/// <summary>A CREATE SERVICE Transact-SQL statement was executed.</summary>
		// Token: 0x040018EC RID: 6380
		CreateService = 161,
		/// <summary>An ALTER SERVICE Transact-SQL statement was executed.</summary>
		// Token: 0x040018ED RID: 6381
		AlterService,
		/// <summary>A DROP SERVICE Transact-SQL statement was executed.</summary>
		// Token: 0x040018EE RID: 6382
		DropService,
		/// <summary>A CREATE ROUTE Transact-SQL statement was executed.</summary>
		// Token: 0x040018EF RID: 6383
		CreateRoute,
		/// <summary>An ALTER ROUTE Transact-SQL statement was executed.</summary>
		// Token: 0x040018F0 RID: 6384
		AlterRoute,
		/// <summary>A DROP ROUTE Transact-SQL statement was executed.</summary>
		// Token: 0x040018F1 RID: 6385
		DropRoute,
		/// <summary>A GRANT Transact-SQL statement was executed.</summary>
		// Token: 0x040018F2 RID: 6386
		GrantStatement,
		/// <summary>A DENY Transact-SQL statement was executed.</summary>
		// Token: 0x040018F3 RID: 6387
		DenyStatement,
		/// <summary>A REVOKE Transact-SQL statement was executed.</summary>
		// Token: 0x040018F4 RID: 6388
		RevokeStatement,
		/// <summary>A GRANT OBJECT Transact-SQL statement was executed.</summary>
		// Token: 0x040018F5 RID: 6389
		GrantObject,
		/// <summary>A DENY Object Permissions Transact-SQL statement was executed.</summary>
		// Token: 0x040018F6 RID: 6390
		DenyObject,
		/// <summary>A REVOKE OBJECT Transact-SQL statement was executed.</summary>
		// Token: 0x040018F7 RID: 6391
		RevokeObject,
		/// <summary>A CREATE_REMOTE_SERVICE_BINDING event type was specified when an event notification was created on the database or server instance.</summary>
		// Token: 0x040018F8 RID: 6392
		CreateBinding = 174,
		/// <summary>An ALTER_REMOTE_SERVICE_BINDING event type was specified when an event notification was created on the database or server instance.</summary>
		// Token: 0x040018F9 RID: 6393
		AlterBinding,
		/// <summary>A DROP_REMOTE_SERVICE_BINDING event type was specified when an event notification was created on the database or server instance.</summary>
		// Token: 0x040018FA RID: 6394
		DropBinding,
		/// <summary>A CREATE PARTITION FUNCTION Transact-SQL statement was executed.</summary>
		// Token: 0x040018FB RID: 6395
		CreatePartitionFunction = 191,
		/// <summary>An ALTER PARTITION FUNCTION Transact-SQL statement was executed.</summary>
		// Token: 0x040018FC RID: 6396
		AlterPartitionFunction,
		/// <summary>A DROP PARTITION FUNCTION Transact-SQL statement was executed.</summary>
		// Token: 0x040018FD RID: 6397
		DropPartitionFunction,
		/// <summary>A CREATE PARTITION SCHEME Transact-SQL statement was executed.</summary>
		// Token: 0x040018FE RID: 6398
		CreatePartitionScheme,
		/// <summary>An ALTER PARTITION SCHEME Transact-SQL statement was executed.</summary>
		// Token: 0x040018FF RID: 6399
		AlterPartitionScheme,
		/// <summary>A DROP PARTITION SCHEME Transact-SQL statement was executed.</summary>
		// Token: 0x04001900 RID: 6400
		DropPartitionScheme
	}
}
