﻿using System;

namespace Microsoft.SqlServer.Server
{
	/// <summary>Describes the type of access to system data for a user-defined method or function.</summary>
	// Token: 0x0200031C RID: 796
	[Serializable]
	public enum SystemDataAccessKind
	{
		/// <summary>The method or function does not access system data. </summary>
		// Token: 0x04001833 RID: 6195
		None,
		/// <summary>The method or function reads system data.</summary>
		// Token: 0x04001834 RID: 6196
		Read
	}
}
