﻿using System;
using System.Data.SqlTypes;

namespace Microsoft.SqlServer.Server
{
	// Token: 0x020002FA RID: 762
	internal interface ITypedSetters
	{
		// Token: 0x06002620 RID: 9760
		void SetDBNull(int ordinal);

		// Token: 0x06002621 RID: 9761
		void SetBoolean(int ordinal, bool value);

		// Token: 0x06002622 RID: 9762
		void SetByte(int ordinal, byte value);

		// Token: 0x06002623 RID: 9763
		void SetBytes(int ordinal, long fieldOffset, byte[] buffer, int bufferOffset, int length);

		// Token: 0x06002624 RID: 9764
		void SetChar(int ordinal, char value);

		// Token: 0x06002625 RID: 9765
		void SetChars(int ordinal, long fieldOffset, char[] buffer, int bufferOffset, int length);

		// Token: 0x06002626 RID: 9766
		void SetInt16(int ordinal, short value);

		// Token: 0x06002627 RID: 9767
		void SetInt32(int ordinal, int value);

		// Token: 0x06002628 RID: 9768
		void SetInt64(int ordinal, long value);

		// Token: 0x06002629 RID: 9769
		void SetFloat(int ordinal, float value);

		// Token: 0x0600262A RID: 9770
		void SetDouble(int ordinal, double value);

		// Token: 0x0600262B RID: 9771
		[Obsolete("Not supported as of SMI v2.  Will be removed when v1 support dropped.  Use setter with offset.")]
		void SetString(int ordinal, string value);

		// Token: 0x0600262C RID: 9772
		void SetString(int ordinal, string value, int offset);

		// Token: 0x0600262D RID: 9773
		void SetDecimal(int ordinal, decimal value);

		// Token: 0x0600262E RID: 9774
		void SetDateTime(int ordinal, DateTime value);

		// Token: 0x0600262F RID: 9775
		void SetGuid(int ordinal, Guid value);

		// Token: 0x06002630 RID: 9776
		void SetSqlBoolean(int ordinal, SqlBoolean value);

		// Token: 0x06002631 RID: 9777
		void SetSqlByte(int ordinal, SqlByte value);

		// Token: 0x06002632 RID: 9778
		void SetSqlInt16(int ordinal, SqlInt16 value);

		// Token: 0x06002633 RID: 9779
		void SetSqlInt32(int ordinal, SqlInt32 value);

		// Token: 0x06002634 RID: 9780
		void SetSqlInt64(int ordinal, SqlInt64 value);

		// Token: 0x06002635 RID: 9781
		void SetSqlSingle(int ordinal, SqlSingle value);

		// Token: 0x06002636 RID: 9782
		void SetSqlDouble(int ordinal, SqlDouble value);

		// Token: 0x06002637 RID: 9783
		void SetSqlMoney(int ordinal, SqlMoney value);

		// Token: 0x06002638 RID: 9784
		void SetSqlDateTime(int ordinal, SqlDateTime value);

		// Token: 0x06002639 RID: 9785
		void SetSqlDecimal(int ordinal, SqlDecimal value);

		// Token: 0x0600263A RID: 9786
		[Obsolete("Not supported as of SMI v2.  Will be removed when v1 support dropped.  Use setter with offset.")]
		void SetSqlString(int ordinal, SqlString value);

		// Token: 0x0600263B RID: 9787
		void SetSqlString(int ordinal, SqlString value, int offset);

		// Token: 0x0600263C RID: 9788
		[Obsolete("Not supported as of SMI v2.  Will be removed when v1 support dropped.  Use setter with offset.")]
		void SetSqlBinary(int ordinal, SqlBinary value);

		// Token: 0x0600263D RID: 9789
		void SetSqlBinary(int ordinal, SqlBinary value, int offset);

		// Token: 0x0600263E RID: 9790
		void SetSqlGuid(int ordinal, SqlGuid value);

		// Token: 0x0600263F RID: 9791
		[Obsolete("Not supported as of SMI v2.  Will be removed when v1 support dropped.  Use setter with offset.")]
		void SetSqlChars(int ordinal, SqlChars value);

		// Token: 0x06002640 RID: 9792
		void SetSqlChars(int ordinal, SqlChars value, int offset);

		// Token: 0x06002641 RID: 9793
		[Obsolete("Not supported as of SMI v2.  Will be removed when v1 support dropped.  Use setter with offset.")]
		void SetSqlBytes(int ordinal, SqlBytes value);

		// Token: 0x06002642 RID: 9794
		void SetSqlBytes(int ordinal, SqlBytes value, int offset);

		// Token: 0x06002643 RID: 9795
		void SetSqlXml(int ordinal, SqlXml value);
	}
}
