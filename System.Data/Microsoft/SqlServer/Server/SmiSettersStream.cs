﻿using System;
using System.Data.Common;
using System.Data.SqlClient;
using System.IO;

namespace Microsoft.SqlServer.Server
{
	// Token: 0x0200030E RID: 782
	internal class SmiSettersStream : Stream
	{
		// Token: 0x0600271D RID: 10013 RVA: 0x000AEE68 File Offset: 0x000AD068
		internal SmiSettersStream(SmiEventSink_Default sink, ITypedSettersV3 setters, int ordinal, SmiMetaData metaData)
		{
			this._sink = sink;
			this._setters = setters;
			this._ordinal = ordinal;
			this._lengthWritten = 0L;
			this._metaData = metaData;
		}

		// Token: 0x17000665 RID: 1637
		// (get) Token: 0x0600271E RID: 10014 RVA: 0x000061C5 File Offset: 0x000043C5
		public override bool CanRead
		{
			get
			{
				return false;
			}
		}

		// Token: 0x17000666 RID: 1638
		// (get) Token: 0x0600271F RID: 10015 RVA: 0x000061C5 File Offset: 0x000043C5
		public override bool CanSeek
		{
			get
			{
				return false;
			}
		}

		// Token: 0x17000667 RID: 1639
		// (get) Token: 0x06002720 RID: 10016 RVA: 0x0000EF1B File Offset: 0x0000D11B
		public override bool CanWrite
		{
			get
			{
				return true;
			}
		}

		// Token: 0x17000668 RID: 1640
		// (get) Token: 0x06002721 RID: 10017 RVA: 0x000AEE95 File Offset: 0x000AD095
		public override long Length
		{
			get
			{
				return this._lengthWritten;
			}
		}

		// Token: 0x17000669 RID: 1641
		// (get) Token: 0x06002722 RID: 10018 RVA: 0x000AEE95 File Offset: 0x000AD095
		// (set) Token: 0x06002723 RID: 10019 RVA: 0x000AE320 File Offset: 0x000AC520
		public override long Position
		{
			get
			{
				return this._lengthWritten;
			}
			set
			{
				throw SQL.StreamSeekNotSupported();
			}
		}

		// Token: 0x06002724 RID: 10020 RVA: 0x000AEE9D File Offset: 0x000AD09D
		public override void Flush()
		{
			this._lengthWritten = ValueUtilsSmi.SetBytesLength(this._sink, this._setters, this._ordinal, this._metaData, this._lengthWritten);
		}

		// Token: 0x06002725 RID: 10021 RVA: 0x000AE320 File Offset: 0x000AC520
		public override long Seek(long offset, SeekOrigin origin)
		{
			throw SQL.StreamSeekNotSupported();
		}

		// Token: 0x06002726 RID: 10022 RVA: 0x000AEEC8 File Offset: 0x000AD0C8
		public override void SetLength(long value)
		{
			if (value < 0L)
			{
				throw ADP.ArgumentOutOfRange("value");
			}
			ValueUtilsSmi.SetBytesLength(this._sink, this._setters, this._ordinal, this._metaData, value);
		}

		// Token: 0x06002727 RID: 10023 RVA: 0x000AEEF9 File Offset: 0x000AD0F9
		public override int Read(byte[] buffer, int offset, int count)
		{
			throw SQL.StreamReadNotSupported();
		}

		// Token: 0x06002728 RID: 10024 RVA: 0x000AEF00 File Offset: 0x000AD100
		public override void Write(byte[] buffer, int offset, int count)
		{
			this._lengthWritten += ValueUtilsSmi.SetBytes(this._sink, this._setters, this._ordinal, this._metaData, this._lengthWritten, buffer, offset, count);
		}

		// Token: 0x040017B3 RID: 6067
		private SmiEventSink_Default _sink;

		// Token: 0x040017B4 RID: 6068
		private ITypedSettersV3 _setters;

		// Token: 0x040017B5 RID: 6069
		private int _ordinal;

		// Token: 0x040017B6 RID: 6070
		private long _lengthWritten;

		// Token: 0x040017B7 RID: 6071
		private SmiMetaData _metaData;
	}
}
