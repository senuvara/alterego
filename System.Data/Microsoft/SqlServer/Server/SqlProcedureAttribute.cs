﻿using System;

namespace Microsoft.SqlServer.Server
{
	/// <summary>Used to mark a method definition in an assembly as a stored procedure. The properties on the attribute reflect the physical characteristics used when the type is registered with SQL Server. This class cannot be inherited.</summary>
	// Token: 0x0200031F RID: 799
	[AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = false)]
	[Serializable]
	public sealed class SqlProcedureAttribute : Attribute
	{
		/// <summary>An attribute on a method definition in an assembly, used to indicate that the given method should be registered as a stored procedure in SQL Server.</summary>
		// Token: 0x06002908 RID: 10504 RVA: 0x000B678B File Offset: 0x000B498B
		public SqlProcedureAttribute()
		{
			this.m_fName = null;
		}

		/// <summary>The name of the stored procedure.</summary>
		/// <returns>A <see cref="T:System.String" /> representing the name of the stored procedure.</returns>
		// Token: 0x170006A6 RID: 1702
		// (get) Token: 0x06002909 RID: 10505 RVA: 0x000B679A File Offset: 0x000B499A
		// (set) Token: 0x0600290A RID: 10506 RVA: 0x000B67A2 File Offset: 0x000B49A2
		public string Name
		{
			get
			{
				return this.m_fName;
			}
			set
			{
				this.m_fName = value;
			}
		}

		// Token: 0x0400183F RID: 6207
		private string m_fName;
	}
}
