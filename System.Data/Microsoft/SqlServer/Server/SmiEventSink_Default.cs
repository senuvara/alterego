﻿using System;
using System.Data.SqlClient;

namespace Microsoft.SqlServer.Server
{
	// Token: 0x020002FF RID: 767
	internal class SmiEventSink_Default : SmiEventSink
	{
		// Token: 0x17000647 RID: 1607
		// (get) Token: 0x06002691 RID: 9873 RVA: 0x000AE1BE File Offset: 0x000AC3BE
		internal bool HasMessages
		{
			get
			{
				return this._errors != null || this._warnings != null;
			}
		}

		// Token: 0x17000648 RID: 1608
		// (get) Token: 0x06002692 RID: 9874 RVA: 0x00004526 File Offset: 0x00002726
		internal virtual string ServerVersion
		{
			get
			{
				return null;
			}
		}

		// Token: 0x06002693 RID: 9875 RVA: 0x000AE1D4 File Offset: 0x000AC3D4
		protected virtual void DispatchMessages()
		{
			SqlException ex = this.ProcessMessages(true);
			if (ex != null)
			{
				throw ex;
			}
		}

		// Token: 0x06002694 RID: 9876 RVA: 0x000AE1F0 File Offset: 0x000AC3F0
		protected SqlException ProcessMessages(bool ignoreWarnings)
		{
			SqlException result = null;
			SqlErrorCollection sqlErrorCollection = null;
			if (this._errors != null)
			{
				if (this._warnings != null)
				{
					foreach (object obj in this._warnings)
					{
						SqlError error = (SqlError)obj;
						this._errors.Add(error);
					}
				}
				sqlErrorCollection = this._errors;
				this._errors = null;
				this._warnings = null;
			}
			else
			{
				if (!ignoreWarnings)
				{
					sqlErrorCollection = this._warnings;
				}
				this._warnings = null;
			}
			if (sqlErrorCollection != null)
			{
				result = SqlException.CreateException(sqlErrorCollection, this.ServerVersion);
			}
			return result;
		}

		// Token: 0x06002695 RID: 9877 RVA: 0x000AE2A0 File Offset: 0x000AC4A0
		internal void ProcessMessagesAndThrow()
		{
			if (this.HasMessages)
			{
				this.DispatchMessages();
			}
		}

		// Token: 0x06002696 RID: 9878 RVA: 0x000AE2B0 File Offset: 0x000AC4B0
		internal SmiEventSink_Default()
		{
		}

		// Token: 0x04001752 RID: 5970
		private SqlErrorCollection _errors;

		// Token: 0x04001753 RID: 5971
		private SqlErrorCollection _warnings;
	}
}
