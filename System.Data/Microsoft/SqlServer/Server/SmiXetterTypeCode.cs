﻿using System;

namespace Microsoft.SqlServer.Server
{
	// Token: 0x02000311 RID: 785
	internal enum SmiXetterTypeCode
	{
		// Token: 0x040017BC RID: 6076
		XetBoolean,
		// Token: 0x040017BD RID: 6077
		XetByte,
		// Token: 0x040017BE RID: 6078
		XetBytes,
		// Token: 0x040017BF RID: 6079
		XetChars,
		// Token: 0x040017C0 RID: 6080
		XetString,
		// Token: 0x040017C1 RID: 6081
		XetInt16,
		// Token: 0x040017C2 RID: 6082
		XetInt32,
		// Token: 0x040017C3 RID: 6083
		XetInt64,
		// Token: 0x040017C4 RID: 6084
		XetSingle,
		// Token: 0x040017C5 RID: 6085
		XetDouble,
		// Token: 0x040017C6 RID: 6086
		XetSqlDecimal,
		// Token: 0x040017C7 RID: 6087
		XetDateTime,
		// Token: 0x040017C8 RID: 6088
		XetGuid,
		// Token: 0x040017C9 RID: 6089
		GetVariantMetaData,
		// Token: 0x040017CA RID: 6090
		GetXet,
		// Token: 0x040017CB RID: 6091
		XetTime,
		// Token: 0x040017CC RID: 6092
		XetTimeSpan = 15,
		// Token: 0x040017CD RID: 6093
		XetDateTimeOffset
	}
}
