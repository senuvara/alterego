﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;

// Token: 0x02000006 RID: 6
[CompilerGenerated]
internal sealed class <>f__AnonymousType4<<OperationId>j__TPar, <Operation>j__TPar, <ConnectionId>j__TPar, <Connection>j__TPar, <Statistics>j__TPar, <Timestamp>j__TPar>
{
	// Token: 0x17000015 RID: 21
	// (get) Token: 0x06000025 RID: 37 RVA: 0x00002B52 File Offset: 0x00000D52
	public <OperationId>j__TPar OperationId
	{
		get
		{
			return this.<OperationId>i__Field;
		}
	}

	// Token: 0x17000016 RID: 22
	// (get) Token: 0x06000026 RID: 38 RVA: 0x00002B5A File Offset: 0x00000D5A
	public <Operation>j__TPar Operation
	{
		get
		{
			return this.<Operation>i__Field;
		}
	}

	// Token: 0x17000017 RID: 23
	// (get) Token: 0x06000027 RID: 39 RVA: 0x00002B62 File Offset: 0x00000D62
	public <ConnectionId>j__TPar ConnectionId
	{
		get
		{
			return this.<ConnectionId>i__Field;
		}
	}

	// Token: 0x17000018 RID: 24
	// (get) Token: 0x06000028 RID: 40 RVA: 0x00002B6A File Offset: 0x00000D6A
	public <Connection>j__TPar Connection
	{
		get
		{
			return this.<Connection>i__Field;
		}
	}

	// Token: 0x17000019 RID: 25
	// (get) Token: 0x06000029 RID: 41 RVA: 0x00002B72 File Offset: 0x00000D72
	public <Statistics>j__TPar Statistics
	{
		get
		{
			return this.<Statistics>i__Field;
		}
	}

	// Token: 0x1700001A RID: 26
	// (get) Token: 0x0600002A RID: 42 RVA: 0x00002B7A File Offset: 0x00000D7A
	public <Timestamp>j__TPar Timestamp
	{
		get
		{
			return this.<Timestamp>i__Field;
		}
	}

	// Token: 0x0600002B RID: 43 RVA: 0x00002B82 File Offset: 0x00000D82
	[DebuggerHidden]
	public <>f__AnonymousType4(<OperationId>j__TPar OperationId, <Operation>j__TPar Operation, <ConnectionId>j__TPar ConnectionId, <Connection>j__TPar Connection, <Statistics>j__TPar Statistics, <Timestamp>j__TPar Timestamp)
	{
		this.<OperationId>i__Field = OperationId;
		this.<Operation>i__Field = Operation;
		this.<ConnectionId>i__Field = ConnectionId;
		this.<Connection>i__Field = Connection;
		this.<Statistics>i__Field = Statistics;
		this.<Timestamp>i__Field = Timestamp;
	}

	// Token: 0x0600002C RID: 44 RVA: 0x00002BB8 File Offset: 0x00000DB8
	[DebuggerHidden]
	public override bool Equals(object value)
	{
		var <>f__AnonymousType = value as <>f__AnonymousType4<<OperationId>j__TPar, <Operation>j__TPar, <ConnectionId>j__TPar, <Connection>j__TPar, <Statistics>j__TPar, <Timestamp>j__TPar>;
		return <>f__AnonymousType != null && EqualityComparer<<OperationId>j__TPar>.Default.Equals(this.<OperationId>i__Field, <>f__AnonymousType.<OperationId>i__Field) && EqualityComparer<<Operation>j__TPar>.Default.Equals(this.<Operation>i__Field, <>f__AnonymousType.<Operation>i__Field) && EqualityComparer<<ConnectionId>j__TPar>.Default.Equals(this.<ConnectionId>i__Field, <>f__AnonymousType.<ConnectionId>i__Field) && EqualityComparer<<Connection>j__TPar>.Default.Equals(this.<Connection>i__Field, <>f__AnonymousType.<Connection>i__Field) && EqualityComparer<<Statistics>j__TPar>.Default.Equals(this.<Statistics>i__Field, <>f__AnonymousType.<Statistics>i__Field) && EqualityComparer<<Timestamp>j__TPar>.Default.Equals(this.<Timestamp>i__Field, <>f__AnonymousType.<Timestamp>i__Field);
	}

	// Token: 0x0600002D RID: 45 RVA: 0x00002C64 File Offset: 0x00000E64
	[DebuggerHidden]
	public override int GetHashCode()
	{
		return (((((-113527307 * -1521134295 + EqualityComparer<<OperationId>j__TPar>.Default.GetHashCode(this.<OperationId>i__Field)) * -1521134295 + EqualityComparer<<Operation>j__TPar>.Default.GetHashCode(this.<Operation>i__Field)) * -1521134295 + EqualityComparer<<ConnectionId>j__TPar>.Default.GetHashCode(this.<ConnectionId>i__Field)) * -1521134295 + EqualityComparer<<Connection>j__TPar>.Default.GetHashCode(this.<Connection>i__Field)) * -1521134295 + EqualityComparer<<Statistics>j__TPar>.Default.GetHashCode(this.<Statistics>i__Field)) * -1521134295 + EqualityComparer<<Timestamp>j__TPar>.Default.GetHashCode(this.<Timestamp>i__Field);
	}

	// Token: 0x0600002E RID: 46 RVA: 0x00002D00 File Offset: 0x00000F00
	[DebuggerHidden]
	public override string ToString()
	{
		IFormatProvider provider = null;
		string format = "{{ OperationId = {0}, Operation = {1}, ConnectionId = {2}, Connection = {3}, Statistics = {4}, Timestamp = {5} }}";
		object[] array = new object[6];
		int num = 0;
		<OperationId>j__TPar <OperationId>j__TPar = this.<OperationId>i__Field;
		ref <OperationId>j__TPar ptr = ref <OperationId>j__TPar;
		<OperationId>j__TPar <OperationId>j__TPar2 = default(<OperationId>j__TPar);
		object obj;
		if (<OperationId>j__TPar2 == null)
		{
			<OperationId>j__TPar2 = <OperationId>j__TPar;
			ptr = ref <OperationId>j__TPar2;
			if (<OperationId>j__TPar2 == null)
			{
				obj = null;
				goto IL_46;
			}
		}
		obj = ptr.ToString();
		IL_46:
		array[num] = obj;
		int num2 = 1;
		<Operation>j__TPar <Operation>j__TPar = this.<Operation>i__Field;
		ref <Operation>j__TPar ptr2 = ref <Operation>j__TPar;
		<Operation>j__TPar <Operation>j__TPar2 = default(<Operation>j__TPar);
		object obj2;
		if (<Operation>j__TPar2 == null)
		{
			<Operation>j__TPar2 = <Operation>j__TPar;
			ptr2 = ref <Operation>j__TPar2;
			if (<Operation>j__TPar2 == null)
			{
				obj2 = null;
				goto IL_81;
			}
		}
		obj2 = ptr2.ToString();
		IL_81:
		array[num2] = obj2;
		int num3 = 2;
		<ConnectionId>j__TPar <ConnectionId>j__TPar = this.<ConnectionId>i__Field;
		ref <ConnectionId>j__TPar ptr3 = ref <ConnectionId>j__TPar;
		<ConnectionId>j__TPar <ConnectionId>j__TPar2 = default(<ConnectionId>j__TPar);
		object obj3;
		if (<ConnectionId>j__TPar2 == null)
		{
			<ConnectionId>j__TPar2 = <ConnectionId>j__TPar;
			ptr3 = ref <ConnectionId>j__TPar2;
			if (<ConnectionId>j__TPar2 == null)
			{
				obj3 = null;
				goto IL_C0;
			}
		}
		obj3 = ptr3.ToString();
		IL_C0:
		array[num3] = obj3;
		int num4 = 3;
		<Connection>j__TPar <Connection>j__TPar = this.<Connection>i__Field;
		ref <Connection>j__TPar ptr4 = ref <Connection>j__TPar;
		<Connection>j__TPar <Connection>j__TPar2 = default(<Connection>j__TPar);
		object obj4;
		if (<Connection>j__TPar2 == null)
		{
			<Connection>j__TPar2 = <Connection>j__TPar;
			ptr4 = ref <Connection>j__TPar2;
			if (<Connection>j__TPar2 == null)
			{
				obj4 = null;
				goto IL_FF;
			}
		}
		obj4 = ptr4.ToString();
		IL_FF:
		array[num4] = obj4;
		int num5 = 4;
		<Statistics>j__TPar <Statistics>j__TPar = this.<Statistics>i__Field;
		ref <Statistics>j__TPar ptr5 = ref <Statistics>j__TPar;
		<Statistics>j__TPar <Statistics>j__TPar2 = default(<Statistics>j__TPar);
		object obj5;
		if (<Statistics>j__TPar2 == null)
		{
			<Statistics>j__TPar2 = <Statistics>j__TPar;
			ptr5 = ref <Statistics>j__TPar2;
			if (<Statistics>j__TPar2 == null)
			{
				obj5 = null;
				goto IL_13E;
			}
		}
		obj5 = ptr5.ToString();
		IL_13E:
		array[num5] = obj5;
		int num6 = 5;
		<Timestamp>j__TPar <Timestamp>j__TPar = this.<Timestamp>i__Field;
		ref <Timestamp>j__TPar ptr6 = ref <Timestamp>j__TPar;
		<Timestamp>j__TPar <Timestamp>j__TPar2 = default(<Timestamp>j__TPar);
		object obj6;
		if (<Timestamp>j__TPar2 == null)
		{
			<Timestamp>j__TPar2 = <Timestamp>j__TPar;
			ptr6 = ref <Timestamp>j__TPar2;
			if (<Timestamp>j__TPar2 == null)
			{
				obj6 = null;
				goto IL_17D;
			}
		}
		obj6 = ptr6.ToString();
		IL_17D:
		array[num6] = obj6;
		return string.Format(provider, format, array);
	}

	// Token: 0x04000015 RID: 21
	[DebuggerBrowsable(DebuggerBrowsableState.Never)]
	private readonly <OperationId>j__TPar <OperationId>i__Field;

	// Token: 0x04000016 RID: 22
	[DebuggerBrowsable(DebuggerBrowsableState.Never)]
	private readonly <Operation>j__TPar <Operation>i__Field;

	// Token: 0x04000017 RID: 23
	[DebuggerBrowsable(DebuggerBrowsableState.Never)]
	private readonly <ConnectionId>j__TPar <ConnectionId>i__Field;

	// Token: 0x04000018 RID: 24
	[DebuggerBrowsable(DebuggerBrowsableState.Never)]
	private readonly <Connection>j__TPar <Connection>i__Field;

	// Token: 0x04000019 RID: 25
	[DebuggerBrowsable(DebuggerBrowsableState.Never)]
	private readonly <Statistics>j__TPar <Statistics>i__Field;

	// Token: 0x0400001A RID: 26
	[DebuggerBrowsable(DebuggerBrowsableState.Never)]
	private readonly <Timestamp>j__TPar <Timestamp>i__Field;
}
