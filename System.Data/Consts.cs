﻿using System;

// Token: 0x0200001A RID: 26
internal static class Consts
{
	// Token: 0x04000092 RID: 146
	public const string MonoVersion = "5.11.0.0";

	// Token: 0x04000093 RID: 147
	public const string MonoCompany = "Mono development team";

	// Token: 0x04000094 RID: 148
	public const string MonoProduct = "Mono Common Language Infrastructure";

	// Token: 0x04000095 RID: 149
	public const string MonoCopyright = "(c) Various Mono authors";

	// Token: 0x04000096 RID: 150
	public const int MonoCorlibVersion = 1051100001;

	// Token: 0x04000097 RID: 151
	public const string FxVersion = "4.0.0.0";

	// Token: 0x04000098 RID: 152
	public const string FxFileVersion = "4.0.30319.17020";

	// Token: 0x04000099 RID: 153
	public const string EnvironmentVersion = "4.0.30319.17020";

	// Token: 0x0400009A RID: 154
	public const string VsVersion = "0.0.0.0";

	// Token: 0x0400009B RID: 155
	public const string VsFileVersion = "11.0.0.0";

	// Token: 0x0400009C RID: 156
	private const string PublicKeyToken = "b77a5c561934e089";

	// Token: 0x0400009D RID: 157
	public const string AssemblyI18N = "I18N, Version=4.0.0.0, Culture=neutral, PublicKeyToken=0738eb9f132ed756";

	// Token: 0x0400009E RID: 158
	public const string AssemblyMicrosoft_JScript = "Microsoft.JScript, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a";

	// Token: 0x0400009F RID: 159
	public const string AssemblyMicrosoft_VisualStudio = "Microsoft.VisualStudio, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a";

	// Token: 0x040000A0 RID: 160
	public const string AssemblyMicrosoft_VisualStudio_Web = "Microsoft.VisualStudio.Web, Version=0.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a";

	// Token: 0x040000A1 RID: 161
	public const string AssemblyMicrosoft_VSDesigner = "Microsoft.VSDesigner, Version=0.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a";

	// Token: 0x040000A2 RID: 162
	public const string AssemblyMono_Http = "Mono.Http, Version=4.0.0.0, Culture=neutral, PublicKeyToken=0738eb9f132ed756";

	// Token: 0x040000A3 RID: 163
	public const string AssemblyMono_Posix = "Mono.Posix, Version=4.0.0.0, Culture=neutral, PublicKeyToken=0738eb9f132ed756";

	// Token: 0x040000A4 RID: 164
	public const string AssemblyMono_Security = "Mono.Security, Version=4.0.0.0, Culture=neutral, PublicKeyToken=0738eb9f132ed756";

	// Token: 0x040000A5 RID: 165
	public const string AssemblyMono_Messaging_RabbitMQ = "Mono.Messaging.RabbitMQ, Version=4.0.0.0, Culture=neutral, PublicKeyToken=0738eb9f132ed756";

	// Token: 0x040000A6 RID: 166
	public const string AssemblyCorlib = "mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089";

	// Token: 0x040000A7 RID: 167
	public const string AssemblySystem = "System, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089";

	// Token: 0x040000A8 RID: 168
	public const string AssemblySystem_Data = "System.Data, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089";

	// Token: 0x040000A9 RID: 169
	public const string AssemblySystem_Design = "System.Design, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a";

	// Token: 0x040000AA RID: 170
	public const string AssemblySystem_DirectoryServices = "System.DirectoryServices, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a";

	// Token: 0x040000AB RID: 171
	public const string AssemblySystem_Drawing = "System.Drawing, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a";

	// Token: 0x040000AC RID: 172
	public const string AssemblySystem_Drawing_Design = "System.Drawing.Design, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a";

	// Token: 0x040000AD RID: 173
	public const string AssemblySystem_Messaging = "System.Messaging, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a";

	// Token: 0x040000AE RID: 174
	public const string AssemblySystem_Security = "System.Security, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a";

	// Token: 0x040000AF RID: 175
	public const string AssemblySystem_ServiceProcess = "System.ServiceProcess, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a";

	// Token: 0x040000B0 RID: 176
	public const string AssemblySystem_Web = "System.Web, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a";

	// Token: 0x040000B1 RID: 177
	public const string AssemblySystem_Windows_Forms = "System.Windows.Forms, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089";

	// Token: 0x040000B2 RID: 178
	public const string AssemblySystem_2_0 = "System, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089";

	// Token: 0x040000B3 RID: 179
	public const string AssemblySystemCore_3_5 = "System.Core, Version=3.5.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089";

	// Token: 0x040000B4 RID: 180
	public const string AssemblySystem_Core = "System.Core, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089";

	// Token: 0x040000B5 RID: 181
	public const string WindowsBase_3_0 = "WindowsBase, Version=3.0.0.0, PublicKeyToken=31bf3856ad364e35";

	// Token: 0x040000B6 RID: 182
	public const string AssemblyWindowsBase = "WindowsBase, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35";

	// Token: 0x040000B7 RID: 183
	public const string AssemblyPresentationCore_3_5 = "PresentationCore, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35";

	// Token: 0x040000B8 RID: 184
	public const string AssemblyPresentationCore_4_0 = "PresentationCore, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35";

	// Token: 0x040000B9 RID: 185
	public const string AssemblyPresentationFramework_3_5 = "PresentationFramework, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35";

	// Token: 0x040000BA RID: 186
	public const string AssemblySystemServiceModel_3_0 = "System.ServiceModel, Version=3.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089";
}
