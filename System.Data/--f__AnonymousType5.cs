﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;

// Token: 0x02000007 RID: 7
[CompilerGenerated]
internal sealed class <>f__AnonymousType5<<OperationId>j__TPar, <Operation>j__TPar, <ConnectionId>j__TPar, <Connection>j__TPar, <Exception>j__TPar, <Timestamp>j__TPar>
{
	// Token: 0x1700001B RID: 27
	// (get) Token: 0x0600002F RID: 47 RVA: 0x00002E90 File Offset: 0x00001090
	public <OperationId>j__TPar OperationId
	{
		get
		{
			return this.<OperationId>i__Field;
		}
	}

	// Token: 0x1700001C RID: 28
	// (get) Token: 0x06000030 RID: 48 RVA: 0x00002E98 File Offset: 0x00001098
	public <Operation>j__TPar Operation
	{
		get
		{
			return this.<Operation>i__Field;
		}
	}

	// Token: 0x1700001D RID: 29
	// (get) Token: 0x06000031 RID: 49 RVA: 0x00002EA0 File Offset: 0x000010A0
	public <ConnectionId>j__TPar ConnectionId
	{
		get
		{
			return this.<ConnectionId>i__Field;
		}
	}

	// Token: 0x1700001E RID: 30
	// (get) Token: 0x06000032 RID: 50 RVA: 0x00002EA8 File Offset: 0x000010A8
	public <Connection>j__TPar Connection
	{
		get
		{
			return this.<Connection>i__Field;
		}
	}

	// Token: 0x1700001F RID: 31
	// (get) Token: 0x06000033 RID: 51 RVA: 0x00002EB0 File Offset: 0x000010B0
	public <Exception>j__TPar Exception
	{
		get
		{
			return this.<Exception>i__Field;
		}
	}

	// Token: 0x17000020 RID: 32
	// (get) Token: 0x06000034 RID: 52 RVA: 0x00002EB8 File Offset: 0x000010B8
	public <Timestamp>j__TPar Timestamp
	{
		get
		{
			return this.<Timestamp>i__Field;
		}
	}

	// Token: 0x06000035 RID: 53 RVA: 0x00002EC0 File Offset: 0x000010C0
	[DebuggerHidden]
	public <>f__AnonymousType5(<OperationId>j__TPar OperationId, <Operation>j__TPar Operation, <ConnectionId>j__TPar ConnectionId, <Connection>j__TPar Connection, <Exception>j__TPar Exception, <Timestamp>j__TPar Timestamp)
	{
		this.<OperationId>i__Field = OperationId;
		this.<Operation>i__Field = Operation;
		this.<ConnectionId>i__Field = ConnectionId;
		this.<Connection>i__Field = Connection;
		this.<Exception>i__Field = Exception;
		this.<Timestamp>i__Field = Timestamp;
	}

	// Token: 0x06000036 RID: 54 RVA: 0x00002EF8 File Offset: 0x000010F8
	[DebuggerHidden]
	public override bool Equals(object value)
	{
		var <>f__AnonymousType = value as <>f__AnonymousType5<<OperationId>j__TPar, <Operation>j__TPar, <ConnectionId>j__TPar, <Connection>j__TPar, <Exception>j__TPar, <Timestamp>j__TPar>;
		return <>f__AnonymousType != null && EqualityComparer<<OperationId>j__TPar>.Default.Equals(this.<OperationId>i__Field, <>f__AnonymousType.<OperationId>i__Field) && EqualityComparer<<Operation>j__TPar>.Default.Equals(this.<Operation>i__Field, <>f__AnonymousType.<Operation>i__Field) && EqualityComparer<<ConnectionId>j__TPar>.Default.Equals(this.<ConnectionId>i__Field, <>f__AnonymousType.<ConnectionId>i__Field) && EqualityComparer<<Connection>j__TPar>.Default.Equals(this.<Connection>i__Field, <>f__AnonymousType.<Connection>i__Field) && EqualityComparer<<Exception>j__TPar>.Default.Equals(this.<Exception>i__Field, <>f__AnonymousType.<Exception>i__Field) && EqualityComparer<<Timestamp>j__TPar>.Default.Equals(this.<Timestamp>i__Field, <>f__AnonymousType.<Timestamp>i__Field);
	}

	// Token: 0x06000037 RID: 55 RVA: 0x00002FA4 File Offset: 0x000011A4
	[DebuggerHidden]
	public override int GetHashCode()
	{
		return (((((-566581869 * -1521134295 + EqualityComparer<<OperationId>j__TPar>.Default.GetHashCode(this.<OperationId>i__Field)) * -1521134295 + EqualityComparer<<Operation>j__TPar>.Default.GetHashCode(this.<Operation>i__Field)) * -1521134295 + EqualityComparer<<ConnectionId>j__TPar>.Default.GetHashCode(this.<ConnectionId>i__Field)) * -1521134295 + EqualityComparer<<Connection>j__TPar>.Default.GetHashCode(this.<Connection>i__Field)) * -1521134295 + EqualityComparer<<Exception>j__TPar>.Default.GetHashCode(this.<Exception>i__Field)) * -1521134295 + EqualityComparer<<Timestamp>j__TPar>.Default.GetHashCode(this.<Timestamp>i__Field);
	}

	// Token: 0x06000038 RID: 56 RVA: 0x00003040 File Offset: 0x00001240
	[DebuggerHidden]
	public override string ToString()
	{
		IFormatProvider provider = null;
		string format = "{{ OperationId = {0}, Operation = {1}, ConnectionId = {2}, Connection = {3}, Exception = {4}, Timestamp = {5} }}";
		object[] array = new object[6];
		int num = 0;
		<OperationId>j__TPar <OperationId>j__TPar = this.<OperationId>i__Field;
		ref <OperationId>j__TPar ptr = ref <OperationId>j__TPar;
		<OperationId>j__TPar <OperationId>j__TPar2 = default(<OperationId>j__TPar);
		object obj;
		if (<OperationId>j__TPar2 == null)
		{
			<OperationId>j__TPar2 = <OperationId>j__TPar;
			ptr = ref <OperationId>j__TPar2;
			if (<OperationId>j__TPar2 == null)
			{
				obj = null;
				goto IL_46;
			}
		}
		obj = ptr.ToString();
		IL_46:
		array[num] = obj;
		int num2 = 1;
		<Operation>j__TPar <Operation>j__TPar = this.<Operation>i__Field;
		ref <Operation>j__TPar ptr2 = ref <Operation>j__TPar;
		<Operation>j__TPar <Operation>j__TPar2 = default(<Operation>j__TPar);
		object obj2;
		if (<Operation>j__TPar2 == null)
		{
			<Operation>j__TPar2 = <Operation>j__TPar;
			ptr2 = ref <Operation>j__TPar2;
			if (<Operation>j__TPar2 == null)
			{
				obj2 = null;
				goto IL_81;
			}
		}
		obj2 = ptr2.ToString();
		IL_81:
		array[num2] = obj2;
		int num3 = 2;
		<ConnectionId>j__TPar <ConnectionId>j__TPar = this.<ConnectionId>i__Field;
		ref <ConnectionId>j__TPar ptr3 = ref <ConnectionId>j__TPar;
		<ConnectionId>j__TPar <ConnectionId>j__TPar2 = default(<ConnectionId>j__TPar);
		object obj3;
		if (<ConnectionId>j__TPar2 == null)
		{
			<ConnectionId>j__TPar2 = <ConnectionId>j__TPar;
			ptr3 = ref <ConnectionId>j__TPar2;
			if (<ConnectionId>j__TPar2 == null)
			{
				obj3 = null;
				goto IL_C0;
			}
		}
		obj3 = ptr3.ToString();
		IL_C0:
		array[num3] = obj3;
		int num4 = 3;
		<Connection>j__TPar <Connection>j__TPar = this.<Connection>i__Field;
		ref <Connection>j__TPar ptr4 = ref <Connection>j__TPar;
		<Connection>j__TPar <Connection>j__TPar2 = default(<Connection>j__TPar);
		object obj4;
		if (<Connection>j__TPar2 == null)
		{
			<Connection>j__TPar2 = <Connection>j__TPar;
			ptr4 = ref <Connection>j__TPar2;
			if (<Connection>j__TPar2 == null)
			{
				obj4 = null;
				goto IL_FF;
			}
		}
		obj4 = ptr4.ToString();
		IL_FF:
		array[num4] = obj4;
		int num5 = 4;
		<Exception>j__TPar <Exception>j__TPar = this.<Exception>i__Field;
		ref <Exception>j__TPar ptr5 = ref <Exception>j__TPar;
		<Exception>j__TPar <Exception>j__TPar2 = default(<Exception>j__TPar);
		object obj5;
		if (<Exception>j__TPar2 == null)
		{
			<Exception>j__TPar2 = <Exception>j__TPar;
			ptr5 = ref <Exception>j__TPar2;
			if (<Exception>j__TPar2 == null)
			{
				obj5 = null;
				goto IL_13E;
			}
		}
		obj5 = ptr5.ToString();
		IL_13E:
		array[num5] = obj5;
		int num6 = 5;
		<Timestamp>j__TPar <Timestamp>j__TPar = this.<Timestamp>i__Field;
		ref <Timestamp>j__TPar ptr6 = ref <Timestamp>j__TPar;
		<Timestamp>j__TPar <Timestamp>j__TPar2 = default(<Timestamp>j__TPar);
		object obj6;
		if (<Timestamp>j__TPar2 == null)
		{
			<Timestamp>j__TPar2 = <Timestamp>j__TPar;
			ptr6 = ref <Timestamp>j__TPar2;
			if (<Timestamp>j__TPar2 == null)
			{
				obj6 = null;
				goto IL_17D;
			}
		}
		obj6 = ptr6.ToString();
		IL_17D:
		array[num6] = obj6;
		return string.Format(provider, format, array);
	}

	// Token: 0x0400001B RID: 27
	[DebuggerBrowsable(DebuggerBrowsableState.Never)]
	private readonly <OperationId>j__TPar <OperationId>i__Field;

	// Token: 0x0400001C RID: 28
	[DebuggerBrowsable(DebuggerBrowsableState.Never)]
	private readonly <Operation>j__TPar <Operation>i__Field;

	// Token: 0x0400001D RID: 29
	[DebuggerBrowsable(DebuggerBrowsableState.Never)]
	private readonly <ConnectionId>j__TPar <ConnectionId>i__Field;

	// Token: 0x0400001E RID: 30
	[DebuggerBrowsable(DebuggerBrowsableState.Never)]
	private readonly <Connection>j__TPar <Connection>i__Field;

	// Token: 0x0400001F RID: 31
	[DebuggerBrowsable(DebuggerBrowsableState.Never)]
	private readonly <Exception>j__TPar <Exception>i__Field;

	// Token: 0x04000020 RID: 32
	[DebuggerBrowsable(DebuggerBrowsableState.Never)]
	private readonly <Timestamp>j__TPar <Timestamp>i__Field;
}
