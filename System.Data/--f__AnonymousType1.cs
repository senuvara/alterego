﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;

// Token: 0x02000003 RID: 3
[CompilerGenerated]
internal sealed class <>f__AnonymousType1<<OperationId>j__TPar, <Operation>j__TPar, <ConnectionId>j__TPar, <Command>j__TPar, <Statistics>j__TPar, <Timestamp>j__TPar>
{
	// Token: 0x17000005 RID: 5
	// (get) Token: 0x06000009 RID: 9 RVA: 0x00002292 File Offset: 0x00000492
	public <OperationId>j__TPar OperationId
	{
		get
		{
			return this.<OperationId>i__Field;
		}
	}

	// Token: 0x17000006 RID: 6
	// (get) Token: 0x0600000A RID: 10 RVA: 0x0000229A File Offset: 0x0000049A
	public <Operation>j__TPar Operation
	{
		get
		{
			return this.<Operation>i__Field;
		}
	}

	// Token: 0x17000007 RID: 7
	// (get) Token: 0x0600000B RID: 11 RVA: 0x000022A2 File Offset: 0x000004A2
	public <ConnectionId>j__TPar ConnectionId
	{
		get
		{
			return this.<ConnectionId>i__Field;
		}
	}

	// Token: 0x17000008 RID: 8
	// (get) Token: 0x0600000C RID: 12 RVA: 0x000022AA File Offset: 0x000004AA
	public <Command>j__TPar Command
	{
		get
		{
			return this.<Command>i__Field;
		}
	}

	// Token: 0x17000009 RID: 9
	// (get) Token: 0x0600000D RID: 13 RVA: 0x000022B2 File Offset: 0x000004B2
	public <Statistics>j__TPar Statistics
	{
		get
		{
			return this.<Statistics>i__Field;
		}
	}

	// Token: 0x1700000A RID: 10
	// (get) Token: 0x0600000E RID: 14 RVA: 0x000022BA File Offset: 0x000004BA
	public <Timestamp>j__TPar Timestamp
	{
		get
		{
			return this.<Timestamp>i__Field;
		}
	}

	// Token: 0x0600000F RID: 15 RVA: 0x000022C2 File Offset: 0x000004C2
	[DebuggerHidden]
	public <>f__AnonymousType1(<OperationId>j__TPar OperationId, <Operation>j__TPar Operation, <ConnectionId>j__TPar ConnectionId, <Command>j__TPar Command, <Statistics>j__TPar Statistics, <Timestamp>j__TPar Timestamp)
	{
		this.<OperationId>i__Field = OperationId;
		this.<Operation>i__Field = Operation;
		this.<ConnectionId>i__Field = ConnectionId;
		this.<Command>i__Field = Command;
		this.<Statistics>i__Field = Statistics;
		this.<Timestamp>i__Field = Timestamp;
	}

	// Token: 0x06000010 RID: 16 RVA: 0x000022F8 File Offset: 0x000004F8
	[DebuggerHidden]
	public override bool Equals(object value)
	{
		var <>f__AnonymousType = value as <>f__AnonymousType1<<OperationId>j__TPar, <Operation>j__TPar, <ConnectionId>j__TPar, <Command>j__TPar, <Statistics>j__TPar, <Timestamp>j__TPar>;
		return <>f__AnonymousType != null && EqualityComparer<<OperationId>j__TPar>.Default.Equals(this.<OperationId>i__Field, <>f__AnonymousType.<OperationId>i__Field) && EqualityComparer<<Operation>j__TPar>.Default.Equals(this.<Operation>i__Field, <>f__AnonymousType.<Operation>i__Field) && EqualityComparer<<ConnectionId>j__TPar>.Default.Equals(this.<ConnectionId>i__Field, <>f__AnonymousType.<ConnectionId>i__Field) && EqualityComparer<<Command>j__TPar>.Default.Equals(this.<Command>i__Field, <>f__AnonymousType.<Command>i__Field) && EqualityComparer<<Statistics>j__TPar>.Default.Equals(this.<Statistics>i__Field, <>f__AnonymousType.<Statistics>i__Field) && EqualityComparer<<Timestamp>j__TPar>.Default.Equals(this.<Timestamp>i__Field, <>f__AnonymousType.<Timestamp>i__Field);
	}

	// Token: 0x06000011 RID: 17 RVA: 0x000023A4 File Offset: 0x000005A4
	[DebuggerHidden]
	public override int GetHashCode()
	{
		return (((((-394583926 * -1521134295 + EqualityComparer<<OperationId>j__TPar>.Default.GetHashCode(this.<OperationId>i__Field)) * -1521134295 + EqualityComparer<<Operation>j__TPar>.Default.GetHashCode(this.<Operation>i__Field)) * -1521134295 + EqualityComparer<<ConnectionId>j__TPar>.Default.GetHashCode(this.<ConnectionId>i__Field)) * -1521134295 + EqualityComparer<<Command>j__TPar>.Default.GetHashCode(this.<Command>i__Field)) * -1521134295 + EqualityComparer<<Statistics>j__TPar>.Default.GetHashCode(this.<Statistics>i__Field)) * -1521134295 + EqualityComparer<<Timestamp>j__TPar>.Default.GetHashCode(this.<Timestamp>i__Field);
	}

	// Token: 0x06000012 RID: 18 RVA: 0x00002440 File Offset: 0x00000640
	[DebuggerHidden]
	public override string ToString()
	{
		IFormatProvider provider = null;
		string format = "{{ OperationId = {0}, Operation = {1}, ConnectionId = {2}, Command = {3}, Statistics = {4}, Timestamp = {5} }}";
		object[] array = new object[6];
		int num = 0;
		<OperationId>j__TPar <OperationId>j__TPar = this.<OperationId>i__Field;
		ref <OperationId>j__TPar ptr = ref <OperationId>j__TPar;
		<OperationId>j__TPar <OperationId>j__TPar2 = default(<OperationId>j__TPar);
		object obj;
		if (<OperationId>j__TPar2 == null)
		{
			<OperationId>j__TPar2 = <OperationId>j__TPar;
			ptr = ref <OperationId>j__TPar2;
			if (<OperationId>j__TPar2 == null)
			{
				obj = null;
				goto IL_46;
			}
		}
		obj = ptr.ToString();
		IL_46:
		array[num] = obj;
		int num2 = 1;
		<Operation>j__TPar <Operation>j__TPar = this.<Operation>i__Field;
		ref <Operation>j__TPar ptr2 = ref <Operation>j__TPar;
		<Operation>j__TPar <Operation>j__TPar2 = default(<Operation>j__TPar);
		object obj2;
		if (<Operation>j__TPar2 == null)
		{
			<Operation>j__TPar2 = <Operation>j__TPar;
			ptr2 = ref <Operation>j__TPar2;
			if (<Operation>j__TPar2 == null)
			{
				obj2 = null;
				goto IL_81;
			}
		}
		obj2 = ptr2.ToString();
		IL_81:
		array[num2] = obj2;
		int num3 = 2;
		<ConnectionId>j__TPar <ConnectionId>j__TPar = this.<ConnectionId>i__Field;
		ref <ConnectionId>j__TPar ptr3 = ref <ConnectionId>j__TPar;
		<ConnectionId>j__TPar <ConnectionId>j__TPar2 = default(<ConnectionId>j__TPar);
		object obj3;
		if (<ConnectionId>j__TPar2 == null)
		{
			<ConnectionId>j__TPar2 = <ConnectionId>j__TPar;
			ptr3 = ref <ConnectionId>j__TPar2;
			if (<ConnectionId>j__TPar2 == null)
			{
				obj3 = null;
				goto IL_C0;
			}
		}
		obj3 = ptr3.ToString();
		IL_C0:
		array[num3] = obj3;
		int num4 = 3;
		<Command>j__TPar <Command>j__TPar = this.<Command>i__Field;
		ref <Command>j__TPar ptr4 = ref <Command>j__TPar;
		<Command>j__TPar <Command>j__TPar2 = default(<Command>j__TPar);
		object obj4;
		if (<Command>j__TPar2 == null)
		{
			<Command>j__TPar2 = <Command>j__TPar;
			ptr4 = ref <Command>j__TPar2;
			if (<Command>j__TPar2 == null)
			{
				obj4 = null;
				goto IL_FF;
			}
		}
		obj4 = ptr4.ToString();
		IL_FF:
		array[num4] = obj4;
		int num5 = 4;
		<Statistics>j__TPar <Statistics>j__TPar = this.<Statistics>i__Field;
		ref <Statistics>j__TPar ptr5 = ref <Statistics>j__TPar;
		<Statistics>j__TPar <Statistics>j__TPar2 = default(<Statistics>j__TPar);
		object obj5;
		if (<Statistics>j__TPar2 == null)
		{
			<Statistics>j__TPar2 = <Statistics>j__TPar;
			ptr5 = ref <Statistics>j__TPar2;
			if (<Statistics>j__TPar2 == null)
			{
				obj5 = null;
				goto IL_13E;
			}
		}
		obj5 = ptr5.ToString();
		IL_13E:
		array[num5] = obj5;
		int num6 = 5;
		<Timestamp>j__TPar <Timestamp>j__TPar = this.<Timestamp>i__Field;
		ref <Timestamp>j__TPar ptr6 = ref <Timestamp>j__TPar;
		<Timestamp>j__TPar <Timestamp>j__TPar2 = default(<Timestamp>j__TPar);
		object obj6;
		if (<Timestamp>j__TPar2 == null)
		{
			<Timestamp>j__TPar2 = <Timestamp>j__TPar;
			ptr6 = ref <Timestamp>j__TPar2;
			if (<Timestamp>j__TPar2 == null)
			{
				obj6 = null;
				goto IL_17D;
			}
		}
		obj6 = ptr6.ToString();
		IL_17D:
		array[num6] = obj6;
		return string.Format(provider, format, array);
	}

	// Token: 0x04000005 RID: 5
	[DebuggerBrowsable(DebuggerBrowsableState.Never)]
	private readonly <OperationId>j__TPar <OperationId>i__Field;

	// Token: 0x04000006 RID: 6
	[DebuggerBrowsable(DebuggerBrowsableState.Never)]
	private readonly <Operation>j__TPar <Operation>i__Field;

	// Token: 0x04000007 RID: 7
	[DebuggerBrowsable(DebuggerBrowsableState.Never)]
	private readonly <ConnectionId>j__TPar <ConnectionId>i__Field;

	// Token: 0x04000008 RID: 8
	[DebuggerBrowsable(DebuggerBrowsableState.Never)]
	private readonly <Command>j__TPar <Command>i__Field;

	// Token: 0x04000009 RID: 9
	[DebuggerBrowsable(DebuggerBrowsableState.Never)]
	private readonly <Statistics>j__TPar <Statistics>i__Field;

	// Token: 0x0400000A RID: 10
	[DebuggerBrowsable(DebuggerBrowsableState.Never)]
	private readonly <Timestamp>j__TPar <Timestamp>i__Field;
}
