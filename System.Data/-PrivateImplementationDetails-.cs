﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// Token: 0x0200032B RID: 811
[CompilerGenerated]
internal sealed class <PrivateImplementationDetails>
{
	// Token: 0x0600293F RID: 10559 RVA: 0x000B6B68 File Offset: 0x000B4D68
	internal static uint ComputeStringHash(string s)
	{
		uint num;
		if (s != null)
		{
			num = 2166136261U;
			for (int i = 0; i < s.Length; i++)
			{
				num = ((uint)s[i] ^ num) * 16777619U;
			}
		}
		return num;
	}

	// Token: 0x04001902 RID: 6402 RVA: 0x000CE5B4 File Offset: 0x000CC7B4
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=36 0EE6555EB2C89F29655BD23FAB0573D8D684331A;

	// Token: 0x04001903 RID: 6403 RVA: 0x000CE5D8 File Offset: 0x000CC7D8
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=512 17D2B80D753AA62E789780119267BDC4ED4D8F2A;

	// Token: 0x04001904 RID: 6404 RVA: 0x000CE7D8 File Offset: 0x000CC9D8
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=32 185965EC7271286178226334C22A93B4591D8E44;

	// Token: 0x04001905 RID: 6405 RVA: 0x000CE7F8 File Offset: 0x000CC9F8
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=38 1D7CD6F4EDDA127D3C963A650A77F39144235266;

	// Token: 0x04001906 RID: 6406 RVA: 0x000CE81E File Offset: 0x000CCA1E
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=152 229F48C8598232AD9236772DD710E64615D0EE51;

	// Token: 0x04001907 RID: 6407 RVA: 0x000CE8B6 File Offset: 0x000CCAB6
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=1575 24E34D04CE6D7404AF777338ABA8DF79AB49D396;

	// Token: 0x04001908 RID: 6408 RVA: 0x000CEEDD File Offset: 0x000CD0DD
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=38 2A4F1BD548EC71F652E24985361CD72F0FE1BE7D;

	// Token: 0x04001909 RID: 6409 RVA: 0x000CEF03 File Offset: 0x000CD103
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=152 547FF12759F2EA9866F3E2095113686A6379ABBF;

	// Token: 0x0400190A RID: 6410 RVA: 0x000CEF9B File Offset: 0x000CD19B
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=176 57F92A12C48A0856350D3C95C4145F2AF4C9DEFF;

	// Token: 0x0400190B RID: 6411 RVA: 0x000CF04B File Offset: 0x000CD24B
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=152 624B37B4C211942F3422DFFEAE9F44901E57339C;

	// Token: 0x0400190C RID: 6412 RVA: 0x000CF0E3 File Offset: 0x000CD2E3
	internal static readonly long 67B1A5A63D8C5CA4CCD90748A3B9D9E7D8AA3E68;

	// Token: 0x0400190D RID: 6413 RVA: 0x000CF0EB File Offset: 0x000CD2EB
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=32 8165EBA1F885AC99072C11FAE9D7B5076BF3889B;

	// Token: 0x0400190E RID: 6414 RVA: 0x000CF10B File Offset: 0x000CD30B
	internal static readonly long 948B5D74ED2E909D5E0175A9F734AE203D62579F;

	// Token: 0x0400190F RID: 6415 RVA: 0x000CF113 File Offset: 0x000CD313
	internal static readonly long 9640408DDCA98CA64B04248140FE922D1561D20E;

	// Token: 0x04001910 RID: 6416 RVA: 0x000CF11B File Offset: 0x000CD31B
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=64 B2903BCFDB294DDE6986A52B83755A2C95D3950F;

	// Token: 0x04001911 RID: 6417 RVA: 0x000CF15B File Offset: 0x000CD35B
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=32 CD8806463E49D88B3B6B737394D99018CBCF8E1B;

	// Token: 0x04001912 RID: 6418 RVA: 0x000CF17B File Offset: 0x000CD37B
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=1575 D31AAFBFA434830765ED9EBF85EE16207B96F317;

	// Token: 0x04001913 RID: 6419 RVA: 0x000CF7A2 File Offset: 0x000CD9A2
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=172 D4E5FDC29D5B32E1F527C5DE622FA7E58C342CB4;

	// Token: 0x04001914 RID: 6420 RVA: 0x000CF84E File Offset: 0x000CDA4E
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=52 DD3AEFEADB1CD615F3017763F1568179FEE640B0;

	// Token: 0x04001915 RID: 6421 RVA: 0x000CF882 File Offset: 0x000CDA82
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=52 E92B39D8233061927D9ACDE54665E68E7535635A;

	// Token: 0x04001916 RID: 6422 RVA: 0x000CF8B6 File Offset: 0x000CDAB6
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=25 F8EEE276F349A0C349216E39CF4C9875E9DF80C7;

	// Token: 0x04001917 RID: 6423 RVA: 0x000CF8CF File Offset: 0x000CDACF
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=64 FA7899481F1198B5A3F90368A998C285FCE19878;

	// Token: 0x04001918 RID: 6424 RVA: 0x000CF90F File Offset: 0x000CDB0F
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=595 FBD23B7F2208BC8A00CB826F39DC1C1A2E27CD77;

	// Token: 0x04001919 RID: 6425 RVA: 0x000CFB62 File Offset: 0x000CDD62
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=152 FF8FD0AC7542FD42A8A7C8D145E120345BA51C56;

	// Token: 0x0200032C RID: 812
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 25)]
	private struct __StaticArrayInitTypeSize=25
	{
	}

	// Token: 0x0200032D RID: 813
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 32)]
	private struct __StaticArrayInitTypeSize=32
	{
	}

	// Token: 0x0200032E RID: 814
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 36)]
	private struct __StaticArrayInitTypeSize=36
	{
	}

	// Token: 0x0200032F RID: 815
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 38)]
	private struct __StaticArrayInitTypeSize=38
	{
	}

	// Token: 0x02000330 RID: 816
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 52)]
	private struct __StaticArrayInitTypeSize=52
	{
	}

	// Token: 0x02000331 RID: 817
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 64)]
	private struct __StaticArrayInitTypeSize=64
	{
	}

	// Token: 0x02000332 RID: 818
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 152)]
	private struct __StaticArrayInitTypeSize=152
	{
	}

	// Token: 0x02000333 RID: 819
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 172)]
	private struct __StaticArrayInitTypeSize=172
	{
	}

	// Token: 0x02000334 RID: 820
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 176)]
	private struct __StaticArrayInitTypeSize=176
	{
	}

	// Token: 0x02000335 RID: 821
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 512)]
	private struct __StaticArrayInitTypeSize=512
	{
	}

	// Token: 0x02000336 RID: 822
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 595)]
	private struct __StaticArrayInitTypeSize=595
	{
	}

	// Token: 0x02000337 RID: 823
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 1575)]
	private struct __StaticArrayInitTypeSize=1575
	{
	}
}
