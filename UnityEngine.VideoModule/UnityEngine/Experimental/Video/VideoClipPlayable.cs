﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;
using UnityEngine.Playables;
using UnityEngine.Scripting;
using UnityEngine.Video;

namespace UnityEngine.Experimental.Video
{
	// Token: 0x02000007 RID: 7
	[NativeHeader("Modules/Video/Public/ScriptBindings/VideoClipPlayable.bindings.h")]
	[RequiredByNativeCode]
	[StaticAccessor("VideoClipPlayableBindings", StaticAccessorType.DoubleColon)]
	[NativeHeader("Runtime/Director/Core/HPlayable.h")]
	[NativeHeader("Modules/Video/Public/VideoClip.h")]
	[NativeHeader("Modules/Video/Public/Director/VideoClipPlayable.h")]
	public struct VideoClipPlayable : IPlayable, IEquatable<VideoClipPlayable>
	{
		// Token: 0x0600002C RID: 44 RVA: 0x00002061 File Offset: 0x00000261
		internal VideoClipPlayable(PlayableHandle handle)
		{
			if (handle.IsValid())
			{
				if (!handle.IsPlayableOfType<VideoClipPlayable>())
				{
					throw new InvalidCastException("Can't set handle: the playable is not an VideoClipPlayable.");
				}
			}
			this.m_Handle = handle;
		}

		// Token: 0x0600002D RID: 45 RVA: 0x00002090 File Offset: 0x00000290
		public static VideoClipPlayable Create(PlayableGraph graph, VideoClip clip, bool looping)
		{
			PlayableHandle handle = VideoClipPlayable.CreateHandle(graph, clip, looping);
			VideoClipPlayable videoClipPlayable = new VideoClipPlayable(handle);
			if (clip != null)
			{
				videoClipPlayable.SetDuration(clip.length);
			}
			return videoClipPlayable;
		}

		// Token: 0x0600002E RID: 46 RVA: 0x000020D0 File Offset: 0x000002D0
		private static PlayableHandle CreateHandle(PlayableGraph graph, VideoClip clip, bool looping)
		{
			PlayableHandle @null = PlayableHandle.Null;
			PlayableHandle result;
			if (!VideoClipPlayable.InternalCreateVideoClipPlayable(ref graph, clip, looping, ref @null))
			{
				result = PlayableHandle.Null;
			}
			else
			{
				result = @null;
			}
			return result;
		}

		// Token: 0x0600002F RID: 47 RVA: 0x00002108 File Offset: 0x00000308
		public PlayableHandle GetHandle()
		{
			return this.m_Handle;
		}

		// Token: 0x06000030 RID: 48 RVA: 0x00002124 File Offset: 0x00000324
		public static implicit operator Playable(VideoClipPlayable playable)
		{
			return new Playable(playable.GetHandle());
		}

		// Token: 0x06000031 RID: 49 RVA: 0x00002148 File Offset: 0x00000348
		public static explicit operator VideoClipPlayable(Playable playable)
		{
			return new VideoClipPlayable(playable.GetHandle());
		}

		// Token: 0x06000032 RID: 50 RVA: 0x0000216C File Offset: 0x0000036C
		public bool Equals(VideoClipPlayable other)
		{
			return this.GetHandle() == other.GetHandle();
		}

		// Token: 0x06000033 RID: 51 RVA: 0x00002194 File Offset: 0x00000394
		public VideoClip GetClip()
		{
			return VideoClipPlayable.GetClipInternal(ref this.m_Handle);
		}

		// Token: 0x06000034 RID: 52 RVA: 0x000021B4 File Offset: 0x000003B4
		public void SetClip(VideoClip value)
		{
			VideoClipPlayable.SetClipInternal(ref this.m_Handle, value);
		}

		// Token: 0x06000035 RID: 53 RVA: 0x000021C4 File Offset: 0x000003C4
		public bool GetLooped()
		{
			return VideoClipPlayable.GetLoopedInternal(ref this.m_Handle);
		}

		// Token: 0x06000036 RID: 54 RVA: 0x000021E4 File Offset: 0x000003E4
		public void SetLooped(bool value)
		{
			VideoClipPlayable.SetLoopedInternal(ref this.m_Handle, value);
		}

		// Token: 0x06000037 RID: 55 RVA: 0x000021F4 File Offset: 0x000003F4
		public bool IsPlaying()
		{
			return VideoClipPlayable.GetIsPlayingInternal(ref this.m_Handle);
		}

		// Token: 0x06000038 RID: 56 RVA: 0x00002214 File Offset: 0x00000414
		public double GetStartDelay()
		{
			return VideoClipPlayable.GetStartDelayInternal(ref this.m_Handle);
		}

		// Token: 0x06000039 RID: 57 RVA: 0x00002234 File Offset: 0x00000434
		internal void SetStartDelay(double value)
		{
			this.ValidateStartDelayInternal(value);
			VideoClipPlayable.SetStartDelayInternal(ref this.m_Handle, value);
		}

		// Token: 0x0600003A RID: 58 RVA: 0x0000224C File Offset: 0x0000044C
		public double GetPauseDelay()
		{
			return VideoClipPlayable.GetPauseDelayInternal(ref this.m_Handle);
		}

		// Token: 0x0600003B RID: 59 RVA: 0x0000226C File Offset: 0x0000046C
		internal void GetPauseDelay(double value)
		{
			double pauseDelayInternal = VideoClipPlayable.GetPauseDelayInternal(ref this.m_Handle);
			if (this.m_Handle.GetPlayState() == PlayState.Playing && (value < 0.05 || (pauseDelayInternal != 0.0 && pauseDelayInternal < 0.05)))
			{
				throw new ArgumentException("VideoClipPlayable.pauseDelay: Setting new delay when existing delay is too small or 0.0 (" + pauseDelayInternal + "), Video system will not be able to change in time");
			}
			VideoClipPlayable.SetPauseDelayInternal(ref this.m_Handle, value);
		}

		// Token: 0x0600003C RID: 60 RVA: 0x000022EB File Offset: 0x000004EB
		public void Seek(double startTime, double startDelay)
		{
			this.Seek(startTime, startDelay, 0.0);
		}

		// Token: 0x0600003D RID: 61 RVA: 0x00002300 File Offset: 0x00000500
		public void Seek(double startTime, double startDelay, [DefaultValue("0")] double duration)
		{
			this.ValidateStartDelayInternal(startDelay);
			VideoClipPlayable.SetStartDelayInternal(ref this.m_Handle, startDelay);
			if (duration > 0.0)
			{
				this.m_Handle.SetDuration(duration + startTime);
				VideoClipPlayable.SetPauseDelayInternal(ref this.m_Handle, startDelay + duration);
			}
			else
			{
				this.m_Handle.SetDuration(double.MaxValue);
				VideoClipPlayable.SetPauseDelayInternal(ref this.m_Handle, 0.0);
			}
			this.m_Handle.SetTime(startTime);
			this.m_Handle.Play();
		}

		// Token: 0x0600003E RID: 62 RVA: 0x00002394 File Offset: 0x00000594
		private void ValidateStartDelayInternal(double startDelay)
		{
			double startDelayInternal = VideoClipPlayable.GetStartDelayInternal(ref this.m_Handle);
			if (this.IsPlaying() && (startDelay < 0.05 || (startDelayInternal >= 1E-05 && startDelayInternal < 0.05)))
			{
				Debug.LogWarning("VideoClipPlayable.StartDelay: Setting new delay when existing delay is too small or 0.0 (" + startDelayInternal + "), Video system will not be able to change in time");
			}
		}

		// Token: 0x0600003F RID: 63
		[NativeThrows]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern VideoClip GetClipInternal(ref PlayableHandle hdl);

		// Token: 0x06000040 RID: 64
		[NativeThrows]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void SetClipInternal(ref PlayableHandle hdl, VideoClip clip);

		// Token: 0x06000041 RID: 65
		[NativeThrows]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool GetLoopedInternal(ref PlayableHandle hdl);

		// Token: 0x06000042 RID: 66
		[NativeThrows]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void SetLoopedInternal(ref PlayableHandle hdl, bool looped);

		// Token: 0x06000043 RID: 67
		[NativeThrows]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool GetIsPlayingInternal(ref PlayableHandle hdl);

		// Token: 0x06000044 RID: 68
		[NativeThrows]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern double GetStartDelayInternal(ref PlayableHandle hdl);

		// Token: 0x06000045 RID: 69
		[NativeThrows]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void SetStartDelayInternal(ref PlayableHandle hdl, double delay);

		// Token: 0x06000046 RID: 70
		[NativeThrows]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern double GetPauseDelayInternal(ref PlayableHandle hdl);

		// Token: 0x06000047 RID: 71
		[NativeThrows]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void SetPauseDelayInternal(ref PlayableHandle hdl, double delay);

		// Token: 0x06000048 RID: 72
		[NativeThrows]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool InternalCreateVideoClipPlayable(ref PlayableGraph graph, VideoClip clip, bool looping, ref PlayableHandle handle);

		// Token: 0x06000049 RID: 73
		[NativeThrows]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool ValidateType(ref PlayableHandle hdl);

		// Token: 0x04000012 RID: 18
		private PlayableHandle m_Handle;
	}
}
