﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;
using UnityEngine.Experimental.Audio;
using UnityEngine.Video;

namespace UnityEngine.Experimental.Video
{
	// Token: 0x02000017 RID: 23
	[StaticAccessor("VideoPlayerExtensionsBindings", StaticAccessorType.DoubleColon)]
	[NativeHeader("VideoScriptingClasses.h")]
	[NativeHeader("Modules/Video/Public/ScriptBindings/VideoPlayerExtensions.bindings.h")]
	[NativeHeader("Modules/Video/Public/VideoPlayer.h")]
	public static class VideoPlayerExtensions
	{
		// Token: 0x060000DA RID: 218 RVA: 0x00002904 File Offset: 0x00000B04
		public static AudioSampleProvider GetAudioSampleProvider(this VideoPlayer vp, ushort trackIndex)
		{
			ushort controlledAudioTrackCount = vp.controlledAudioTrackCount;
			if (trackIndex >= controlledAudioTrackCount)
			{
				throw new ArgumentOutOfRangeException("trackIndex", trackIndex, "VideoPlayer is currently configured with " + controlledAudioTrackCount + " tracks.");
			}
			VideoAudioOutputMode audioOutputMode = vp.audioOutputMode;
			if (audioOutputMode != VideoAudioOutputMode.APIOnly)
			{
				throw new InvalidOperationException("VideoPlayer.GetAudioSampleProvider requires audioOutputMode to be APIOnly. Current: " + audioOutputMode);
			}
			AudioSampleProvider audioSampleProvider = AudioSampleProvider.Lookup(VideoPlayerExtensions.InternalGetAudioSampleProviderId(vp, trackIndex), vp, trackIndex);
			if (audioSampleProvider == null)
			{
				throw new InvalidOperationException("VideoPlayer.GetAudioSampleProvider got null provider.");
			}
			if (audioSampleProvider.owner != vp)
			{
				throw new InvalidOperationException("Internal error: VideoPlayer.GetAudioSampleProvider got provider used by another object.");
			}
			if (audioSampleProvider.trackIndex != trackIndex)
			{
				throw new InvalidOperationException(string.Concat(new object[]
				{
					"Internal error: VideoPlayer.GetAudioSampleProvider got provider for track ",
					audioSampleProvider.trackIndex,
					" instead of ",
					trackIndex
				}));
			}
			return audioSampleProvider;
		}

		// Token: 0x060000DB RID: 219
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern uint InternalGetAudioSampleProviderId(VideoPlayer vp, ushort trackIndex);
	}
}
