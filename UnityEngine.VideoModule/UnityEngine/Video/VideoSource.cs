﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine.Video
{
	// Token: 0x02000010 RID: 16
	[RequiredByNativeCode]
	public enum VideoSource
	{
		// Token: 0x0400002D RID: 45
		VideoClip,
		// Token: 0x0400002E RID: 46
		Url
	}
}
