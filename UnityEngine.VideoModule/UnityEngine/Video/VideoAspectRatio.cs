﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine.Video
{
	// Token: 0x0200000D RID: 13
	[RequiredByNativeCode]
	public enum VideoAspectRatio
	{
		// Token: 0x0400001F RID: 31
		NoScaling,
		// Token: 0x04000020 RID: 32
		FitVertically,
		// Token: 0x04000021 RID: 33
		FitHorizontally,
		// Token: 0x04000022 RID: 34
		FitInside,
		// Token: 0x04000023 RID: 35
		FitOutside,
		// Token: 0x04000024 RID: 36
		Stretch
	}
}
