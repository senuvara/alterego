﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine.Video
{
	// Token: 0x0200000B RID: 11
	[RequiredByNativeCode]
	public enum VideoRenderMode
	{
		// Token: 0x04000015 RID: 21
		CameraFarPlane,
		// Token: 0x04000016 RID: 22
		CameraNearPlane,
		// Token: 0x04000017 RID: 23
		RenderTexture,
		// Token: 0x04000018 RID: 24
		MaterialOverride,
		// Token: 0x04000019 RID: 25
		APIOnly
	}
}
