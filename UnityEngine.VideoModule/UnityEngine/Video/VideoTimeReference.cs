﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine.Video
{
	// Token: 0x0200000F RID: 15
	[RequiredByNativeCode]
	public enum VideoTimeReference
	{
		// Token: 0x04000029 RID: 41
		Freerun,
		// Token: 0x0400002A RID: 42
		InternalTime,
		// Token: 0x0400002B RID: 43
		ExternalTime
	}
}
