﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine.Video
{
	// Token: 0x02000011 RID: 17
	[RequiredByNativeCode]
	public enum VideoAudioOutputMode
	{
		// Token: 0x04000030 RID: 48
		None,
		// Token: 0x04000031 RID: 49
		AudioSource,
		// Token: 0x04000032 RID: 50
		Direct,
		// Token: 0x04000033 RID: 51
		APIOnly
	}
}
