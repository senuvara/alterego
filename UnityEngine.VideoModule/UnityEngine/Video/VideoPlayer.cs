﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine.Video
{
	// Token: 0x02000012 RID: 18
	[RequiredByNativeCode]
	[RequireComponent(typeof(Transform))]
	[NativeHeader("Modules/Video/Public/VideoPlayer.h")]
	public sealed class VideoPlayer : Behaviour
	{
		// Token: 0x0600005B RID: 91 RVA: 0x0000244C File Offset: 0x0000064C
		public VideoPlayer()
		{
		}

		// Token: 0x1700000B RID: 11
		// (get) Token: 0x0600005C RID: 92
		// (set) Token: 0x0600005D RID: 93
		public extern VideoSource source { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700000C RID: 12
		// (get) Token: 0x0600005E RID: 94
		// (set) Token: 0x0600005F RID: 95
		[NativeName("VideoUrl")]
		public extern string url { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700000D RID: 13
		// (get) Token: 0x06000060 RID: 96
		// (set) Token: 0x06000061 RID: 97
		[NativeName("VideoClip")]
		public extern VideoClip clip { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700000E RID: 14
		// (get) Token: 0x06000062 RID: 98
		// (set) Token: 0x06000063 RID: 99
		public extern VideoRenderMode renderMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700000F RID: 15
		// (get) Token: 0x06000064 RID: 100
		// (set) Token: 0x06000065 RID: 101
		[NativeHeader("Runtime/Camera/Camera.h")]
		public extern Camera targetCamera { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000010 RID: 16
		// (get) Token: 0x06000066 RID: 102
		// (set) Token: 0x06000067 RID: 103
		[NativeHeader("Runtime/Graphics/RenderTexture.h")]
		public extern RenderTexture targetTexture { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000011 RID: 17
		// (get) Token: 0x06000068 RID: 104
		// (set) Token: 0x06000069 RID: 105
		[NativeHeader("Runtime/Graphics/Renderer.h")]
		public extern Renderer targetMaterialRenderer { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000012 RID: 18
		// (get) Token: 0x0600006A RID: 106
		// (set) Token: 0x0600006B RID: 107
		public extern string targetMaterialProperty { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000013 RID: 19
		// (get) Token: 0x0600006C RID: 108
		// (set) Token: 0x0600006D RID: 109
		public extern VideoAspectRatio aspectRatio { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000014 RID: 20
		// (get) Token: 0x0600006E RID: 110
		// (set) Token: 0x0600006F RID: 111
		public extern float targetCameraAlpha { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000015 RID: 21
		// (get) Token: 0x06000070 RID: 112
		// (set) Token: 0x06000071 RID: 113
		public extern Video3DLayout targetCamera3DLayout { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000016 RID: 22
		// (get) Token: 0x06000072 RID: 114
		[NativeHeader("Runtime/Graphics/Texture.h")]
		public extern Texture texture { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x06000073 RID: 115
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void Prepare();

		// Token: 0x17000017 RID: 23
		// (get) Token: 0x06000074 RID: 116
		public extern bool isPrepared { [NativeName("IsPrepared")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000018 RID: 24
		// (get) Token: 0x06000075 RID: 117
		// (set) Token: 0x06000076 RID: 118
		public extern bool waitForFirstFrame { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000019 RID: 25
		// (get) Token: 0x06000077 RID: 119
		// (set) Token: 0x06000078 RID: 120
		public extern bool playOnAwake { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x06000079 RID: 121
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void Play();

		// Token: 0x0600007A RID: 122
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void Pause();

		// Token: 0x0600007B RID: 123
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void Stop();

		// Token: 0x1700001A RID: 26
		// (get) Token: 0x0600007C RID: 124
		public extern bool isPlaying { [NativeName("IsPlaying")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700001B RID: 27
		// (get) Token: 0x0600007D RID: 125
		public extern bool isPaused { [NativeName("IsPaused")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700001C RID: 28
		// (get) Token: 0x0600007E RID: 126
		public extern bool canSetTime { [NativeName("CanSetTime")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700001D RID: 29
		// (get) Token: 0x0600007F RID: 127
		// (set) Token: 0x06000080 RID: 128
		[NativeName("SecPosition")]
		public extern double time { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700001E RID: 30
		// (get) Token: 0x06000081 RID: 129
		// (set) Token: 0x06000082 RID: 130
		[NativeName("FramePosition")]
		public extern long frame { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700001F RID: 31
		// (get) Token: 0x06000083 RID: 131
		public extern double clockTime { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000020 RID: 32
		// (get) Token: 0x06000084 RID: 132
		public extern bool canStep { [NativeName("CanStep")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x06000085 RID: 133
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void StepForward();

		// Token: 0x17000021 RID: 33
		// (get) Token: 0x06000086 RID: 134
		public extern bool canSetPlaybackSpeed { [NativeName("CanSetPlaybackSpeed")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000022 RID: 34
		// (get) Token: 0x06000087 RID: 135
		// (set) Token: 0x06000088 RID: 136
		public extern float playbackSpeed { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000023 RID: 35
		// (get) Token: 0x06000089 RID: 137
		// (set) Token: 0x0600008A RID: 138
		[NativeName("Loop")]
		public extern bool isLooping { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000024 RID: 36
		// (get) Token: 0x0600008B RID: 139
		public extern bool canSetTimeSource { [NativeName("CanSetTimeSource")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000025 RID: 37
		// (get) Token: 0x0600008C RID: 140
		// (set) Token: 0x0600008D RID: 141
		public extern VideoTimeSource timeSource { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000026 RID: 38
		// (get) Token: 0x0600008E RID: 142
		// (set) Token: 0x0600008F RID: 143
		public extern VideoTimeReference timeReference { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000027 RID: 39
		// (get) Token: 0x06000090 RID: 144
		// (set) Token: 0x06000091 RID: 145
		public extern double externalReferenceTime { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000028 RID: 40
		// (get) Token: 0x06000092 RID: 146
		public extern bool canSetSkipOnDrop { [NativeName("CanSetSkipOnDrop")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000029 RID: 41
		// (get) Token: 0x06000093 RID: 147
		// (set) Token: 0x06000094 RID: 148
		public extern bool skipOnDrop { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700002A RID: 42
		// (get) Token: 0x06000095 RID: 149
		public extern ulong frameCount { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700002B RID: 43
		// (get) Token: 0x06000096 RID: 150
		public extern float frameRate { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700002C RID: 44
		// (get) Token: 0x06000097 RID: 151
		[NativeName("Duration")]
		public extern double length { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700002D RID: 45
		// (get) Token: 0x06000098 RID: 152
		public extern uint width { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700002E RID: 46
		// (get) Token: 0x06000099 RID: 153
		public extern uint height { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700002F RID: 47
		// (get) Token: 0x0600009A RID: 154
		public extern uint pixelAspectRatioNumerator { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000030 RID: 48
		// (get) Token: 0x0600009B RID: 155
		public extern uint pixelAspectRatioDenominator { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000031 RID: 49
		// (get) Token: 0x0600009C RID: 156
		public extern ushort audioTrackCount { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x0600009D RID: 157
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern string GetAudioLanguageCode(ushort trackIndex);

		// Token: 0x0600009E RID: 158
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern ushort GetAudioChannelCount(ushort trackIndex);

		// Token: 0x0600009F RID: 159
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern uint GetAudioSampleRate(ushort trackIndex);

		// Token: 0x17000032 RID: 50
		// (get) Token: 0x060000A0 RID: 160
		public static extern ushort controlledAudioTrackMaxCount { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000033 RID: 51
		// (get) Token: 0x060000A1 RID: 161 RVA: 0x00002454 File Offset: 0x00000654
		// (set) Token: 0x060000A2 RID: 162 RVA: 0x00002470 File Offset: 0x00000670
		public ushort controlledAudioTrackCount
		{
			get
			{
				return this.GetControlledAudioTrackCount();
			}
			set
			{
				int controlledAudioTrackMaxCount = (int)VideoPlayer.controlledAudioTrackMaxCount;
				if ((int)value > controlledAudioTrackMaxCount)
				{
					throw new ArgumentException(string.Format("Cannot control more than {0} tracks.", controlledAudioTrackMaxCount), "value");
				}
				this.SetControlledAudioTrackCount(value);
			}
		}

		// Token: 0x060000A3 RID: 163
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern ushort GetControlledAudioTrackCount();

		// Token: 0x060000A4 RID: 164
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void SetControlledAudioTrackCount(ushort value);

		// Token: 0x060000A5 RID: 165
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void EnableAudioTrack(ushort trackIndex, bool enabled);

		// Token: 0x060000A6 RID: 166
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern bool IsAudioTrackEnabled(ushort trackIndex);

		// Token: 0x17000034 RID: 52
		// (get) Token: 0x060000A7 RID: 167
		// (set) Token: 0x060000A8 RID: 168
		public extern VideoAudioOutputMode audioOutputMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000035 RID: 53
		// (get) Token: 0x060000A9 RID: 169
		public extern bool canSetDirectAudioVolume { [NativeName("CanSetDirectAudioVolume")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x060000AA RID: 170
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern float GetDirectAudioVolume(ushort trackIndex);

		// Token: 0x060000AB RID: 171
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SetDirectAudioVolume(ushort trackIndex, float volume);

		// Token: 0x060000AC RID: 172
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern bool GetDirectAudioMute(ushort trackIndex);

		// Token: 0x060000AD RID: 173
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SetDirectAudioMute(ushort trackIndex, bool mute);

		// Token: 0x060000AE RID: 174
		[NativeHeader("Modules/Audio/Public/AudioSource.h")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern AudioSource GetTargetAudioSource(ushort trackIndex);

		// Token: 0x060000AF RID: 175
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SetTargetAudioSource(ushort trackIndex, AudioSource source);

		// Token: 0x14000001 RID: 1
		// (add) Token: 0x060000B0 RID: 176 RVA: 0x000024B0 File Offset: 0x000006B0
		// (remove) Token: 0x060000B1 RID: 177 RVA: 0x000024E8 File Offset: 0x000006E8
		public event VideoPlayer.EventHandler prepareCompleted
		{
			add
			{
				VideoPlayer.EventHandler eventHandler = this.prepareCompleted;
				VideoPlayer.EventHandler eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					eventHandler = Interlocked.CompareExchange<VideoPlayer.EventHandler>(ref this.prepareCompleted, (VideoPlayer.EventHandler)Delegate.Combine(eventHandler2, value), eventHandler);
				}
				while (eventHandler != eventHandler2);
			}
			remove
			{
				VideoPlayer.EventHandler eventHandler = this.prepareCompleted;
				VideoPlayer.EventHandler eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					eventHandler = Interlocked.CompareExchange<VideoPlayer.EventHandler>(ref this.prepareCompleted, (VideoPlayer.EventHandler)Delegate.Remove(eventHandler2, value), eventHandler);
				}
				while (eventHandler != eventHandler2);
			}
		}

		// Token: 0x14000002 RID: 2
		// (add) Token: 0x060000B2 RID: 178 RVA: 0x00002520 File Offset: 0x00000720
		// (remove) Token: 0x060000B3 RID: 179 RVA: 0x00002558 File Offset: 0x00000758
		public event VideoPlayer.EventHandler loopPointReached
		{
			add
			{
				VideoPlayer.EventHandler eventHandler = this.loopPointReached;
				VideoPlayer.EventHandler eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					eventHandler = Interlocked.CompareExchange<VideoPlayer.EventHandler>(ref this.loopPointReached, (VideoPlayer.EventHandler)Delegate.Combine(eventHandler2, value), eventHandler);
				}
				while (eventHandler != eventHandler2);
			}
			remove
			{
				VideoPlayer.EventHandler eventHandler = this.loopPointReached;
				VideoPlayer.EventHandler eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					eventHandler = Interlocked.CompareExchange<VideoPlayer.EventHandler>(ref this.loopPointReached, (VideoPlayer.EventHandler)Delegate.Remove(eventHandler2, value), eventHandler);
				}
				while (eventHandler != eventHandler2);
			}
		}

		// Token: 0x14000003 RID: 3
		// (add) Token: 0x060000B4 RID: 180 RVA: 0x00002590 File Offset: 0x00000790
		// (remove) Token: 0x060000B5 RID: 181 RVA: 0x000025C8 File Offset: 0x000007C8
		public event VideoPlayer.EventHandler started
		{
			add
			{
				VideoPlayer.EventHandler eventHandler = this.started;
				VideoPlayer.EventHandler eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					eventHandler = Interlocked.CompareExchange<VideoPlayer.EventHandler>(ref this.started, (VideoPlayer.EventHandler)Delegate.Combine(eventHandler2, value), eventHandler);
				}
				while (eventHandler != eventHandler2);
			}
			remove
			{
				VideoPlayer.EventHandler eventHandler = this.started;
				VideoPlayer.EventHandler eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					eventHandler = Interlocked.CompareExchange<VideoPlayer.EventHandler>(ref this.started, (VideoPlayer.EventHandler)Delegate.Remove(eventHandler2, value), eventHandler);
				}
				while (eventHandler != eventHandler2);
			}
		}

		// Token: 0x14000004 RID: 4
		// (add) Token: 0x060000B6 RID: 182 RVA: 0x00002600 File Offset: 0x00000800
		// (remove) Token: 0x060000B7 RID: 183 RVA: 0x00002638 File Offset: 0x00000838
		public event VideoPlayer.EventHandler frameDropped
		{
			add
			{
				VideoPlayer.EventHandler eventHandler = this.frameDropped;
				VideoPlayer.EventHandler eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					eventHandler = Interlocked.CompareExchange<VideoPlayer.EventHandler>(ref this.frameDropped, (VideoPlayer.EventHandler)Delegate.Combine(eventHandler2, value), eventHandler);
				}
				while (eventHandler != eventHandler2);
			}
			remove
			{
				VideoPlayer.EventHandler eventHandler = this.frameDropped;
				VideoPlayer.EventHandler eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					eventHandler = Interlocked.CompareExchange<VideoPlayer.EventHandler>(ref this.frameDropped, (VideoPlayer.EventHandler)Delegate.Remove(eventHandler2, value), eventHandler);
				}
				while (eventHandler != eventHandler2);
			}
		}

		// Token: 0x14000005 RID: 5
		// (add) Token: 0x060000B8 RID: 184 RVA: 0x00002670 File Offset: 0x00000870
		// (remove) Token: 0x060000B9 RID: 185 RVA: 0x000026A8 File Offset: 0x000008A8
		public event VideoPlayer.ErrorEventHandler errorReceived
		{
			add
			{
				VideoPlayer.ErrorEventHandler errorEventHandler = this.errorReceived;
				VideoPlayer.ErrorEventHandler errorEventHandler2;
				do
				{
					errorEventHandler2 = errorEventHandler;
					errorEventHandler = Interlocked.CompareExchange<VideoPlayer.ErrorEventHandler>(ref this.errorReceived, (VideoPlayer.ErrorEventHandler)Delegate.Combine(errorEventHandler2, value), errorEventHandler);
				}
				while (errorEventHandler != errorEventHandler2);
			}
			remove
			{
				VideoPlayer.ErrorEventHandler errorEventHandler = this.errorReceived;
				VideoPlayer.ErrorEventHandler errorEventHandler2;
				do
				{
					errorEventHandler2 = errorEventHandler;
					errorEventHandler = Interlocked.CompareExchange<VideoPlayer.ErrorEventHandler>(ref this.errorReceived, (VideoPlayer.ErrorEventHandler)Delegate.Remove(errorEventHandler2, value), errorEventHandler);
				}
				while (errorEventHandler != errorEventHandler2);
			}
		}

		// Token: 0x14000006 RID: 6
		// (add) Token: 0x060000BA RID: 186 RVA: 0x000026E0 File Offset: 0x000008E0
		// (remove) Token: 0x060000BB RID: 187 RVA: 0x00002718 File Offset: 0x00000918
		public event VideoPlayer.EventHandler seekCompleted
		{
			add
			{
				VideoPlayer.EventHandler eventHandler = this.seekCompleted;
				VideoPlayer.EventHandler eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					eventHandler = Interlocked.CompareExchange<VideoPlayer.EventHandler>(ref this.seekCompleted, (VideoPlayer.EventHandler)Delegate.Combine(eventHandler2, value), eventHandler);
				}
				while (eventHandler != eventHandler2);
			}
			remove
			{
				VideoPlayer.EventHandler eventHandler = this.seekCompleted;
				VideoPlayer.EventHandler eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					eventHandler = Interlocked.CompareExchange<VideoPlayer.EventHandler>(ref this.seekCompleted, (VideoPlayer.EventHandler)Delegate.Remove(eventHandler2, value), eventHandler);
				}
				while (eventHandler != eventHandler2);
			}
		}

		// Token: 0x14000007 RID: 7
		// (add) Token: 0x060000BC RID: 188 RVA: 0x00002750 File Offset: 0x00000950
		// (remove) Token: 0x060000BD RID: 189 RVA: 0x00002788 File Offset: 0x00000988
		public event VideoPlayer.TimeEventHandler clockResyncOccurred
		{
			add
			{
				VideoPlayer.TimeEventHandler timeEventHandler = this.clockResyncOccurred;
				VideoPlayer.TimeEventHandler timeEventHandler2;
				do
				{
					timeEventHandler2 = timeEventHandler;
					timeEventHandler = Interlocked.CompareExchange<VideoPlayer.TimeEventHandler>(ref this.clockResyncOccurred, (VideoPlayer.TimeEventHandler)Delegate.Combine(timeEventHandler2, value), timeEventHandler);
				}
				while (timeEventHandler != timeEventHandler2);
			}
			remove
			{
				VideoPlayer.TimeEventHandler timeEventHandler = this.clockResyncOccurred;
				VideoPlayer.TimeEventHandler timeEventHandler2;
				do
				{
					timeEventHandler2 = timeEventHandler;
					timeEventHandler = Interlocked.CompareExchange<VideoPlayer.TimeEventHandler>(ref this.clockResyncOccurred, (VideoPlayer.TimeEventHandler)Delegate.Remove(timeEventHandler2, value), timeEventHandler);
				}
				while (timeEventHandler != timeEventHandler2);
			}
		}

		// Token: 0x17000036 RID: 54
		// (get) Token: 0x060000BE RID: 190
		// (set) Token: 0x060000BF RID: 191
		public extern bool sendFrameReadyEvents { [NativeName("AreFrameReadyEventsEnabled")] [MethodImpl(MethodImplOptions.InternalCall)] get; [NativeName("EnableFrameReadyEvents")] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x14000008 RID: 8
		// (add) Token: 0x060000C0 RID: 192 RVA: 0x000027C0 File Offset: 0x000009C0
		// (remove) Token: 0x060000C1 RID: 193 RVA: 0x000027F8 File Offset: 0x000009F8
		public event VideoPlayer.FrameReadyEventHandler frameReady
		{
			add
			{
				VideoPlayer.FrameReadyEventHandler frameReadyEventHandler = this.frameReady;
				VideoPlayer.FrameReadyEventHandler frameReadyEventHandler2;
				do
				{
					frameReadyEventHandler2 = frameReadyEventHandler;
					frameReadyEventHandler = Interlocked.CompareExchange<VideoPlayer.FrameReadyEventHandler>(ref this.frameReady, (VideoPlayer.FrameReadyEventHandler)Delegate.Combine(frameReadyEventHandler2, value), frameReadyEventHandler);
				}
				while (frameReadyEventHandler != frameReadyEventHandler2);
			}
			remove
			{
				VideoPlayer.FrameReadyEventHandler frameReadyEventHandler = this.frameReady;
				VideoPlayer.FrameReadyEventHandler frameReadyEventHandler2;
				do
				{
					frameReadyEventHandler2 = frameReadyEventHandler;
					frameReadyEventHandler = Interlocked.CompareExchange<VideoPlayer.FrameReadyEventHandler>(ref this.frameReady, (VideoPlayer.FrameReadyEventHandler)Delegate.Remove(frameReadyEventHandler2, value), frameReadyEventHandler);
				}
				while (frameReadyEventHandler != frameReadyEventHandler2);
			}
		}

		// Token: 0x060000C2 RID: 194 RVA: 0x0000282E File Offset: 0x00000A2E
		[RequiredByNativeCode]
		private static void InvokePrepareCompletedCallback_Internal(VideoPlayer source)
		{
			if (source.prepareCompleted != null)
			{
				source.prepareCompleted(source);
			}
		}

		// Token: 0x060000C3 RID: 195 RVA: 0x00002848 File Offset: 0x00000A48
		[RequiredByNativeCode]
		private static void InvokeFrameReadyCallback_Internal(VideoPlayer source, long frameIdx)
		{
			if (source.frameReady != null)
			{
				source.frameReady(source, frameIdx);
			}
		}

		// Token: 0x060000C4 RID: 196 RVA: 0x00002863 File Offset: 0x00000A63
		[RequiredByNativeCode]
		private static void InvokeLoopPointReachedCallback_Internal(VideoPlayer source)
		{
			if (source.loopPointReached != null)
			{
				source.loopPointReached(source);
			}
		}

		// Token: 0x060000C5 RID: 197 RVA: 0x0000287D File Offset: 0x00000A7D
		[RequiredByNativeCode]
		private static void InvokeStartedCallback_Internal(VideoPlayer source)
		{
			if (source.started != null)
			{
				source.started(source);
			}
		}

		// Token: 0x060000C6 RID: 198 RVA: 0x00002897 File Offset: 0x00000A97
		[RequiredByNativeCode]
		private static void InvokeFrameDroppedCallback_Internal(VideoPlayer source)
		{
			if (source.frameDropped != null)
			{
				source.frameDropped(source);
			}
		}

		// Token: 0x060000C7 RID: 199 RVA: 0x000028B1 File Offset: 0x00000AB1
		[RequiredByNativeCode]
		private static void InvokeErrorReceivedCallback_Internal(VideoPlayer source, string errorStr)
		{
			if (source.errorReceived != null)
			{
				source.errorReceived(source, errorStr);
			}
		}

		// Token: 0x060000C8 RID: 200 RVA: 0x000028CC File Offset: 0x00000ACC
		[RequiredByNativeCode]
		private static void InvokeSeekCompletedCallback_Internal(VideoPlayer source)
		{
			if (source.seekCompleted != null)
			{
				source.seekCompleted(source);
			}
		}

		// Token: 0x060000C9 RID: 201 RVA: 0x000028E6 File Offset: 0x00000AE6
		[RequiredByNativeCode]
		private static void InvokeClockResyncOccurredCallback_Internal(VideoPlayer source, double seconds)
		{
			if (source.clockResyncOccurred != null)
			{
				source.clockResyncOccurred(source, seconds);
			}
		}

		// Token: 0x04000034 RID: 52
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private VideoPlayer.EventHandler prepareCompleted;

		// Token: 0x04000035 RID: 53
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private VideoPlayer.EventHandler loopPointReached;

		// Token: 0x04000036 RID: 54
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private VideoPlayer.EventHandler started;

		// Token: 0x04000037 RID: 55
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private VideoPlayer.EventHandler frameDropped;

		// Token: 0x04000038 RID: 56
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private VideoPlayer.ErrorEventHandler errorReceived;

		// Token: 0x04000039 RID: 57
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private VideoPlayer.EventHandler seekCompleted;

		// Token: 0x0400003A RID: 58
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private VideoPlayer.TimeEventHandler clockResyncOccurred;

		// Token: 0x0400003B RID: 59
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private VideoPlayer.FrameReadyEventHandler frameReady;

		// Token: 0x02000013 RID: 19
		// (Invoke) Token: 0x060000CB RID: 203
		public delegate void EventHandler(VideoPlayer source);

		// Token: 0x02000014 RID: 20
		// (Invoke) Token: 0x060000CF RID: 207
		public delegate void ErrorEventHandler(VideoPlayer source, string message);

		// Token: 0x02000015 RID: 21
		// (Invoke) Token: 0x060000D3 RID: 211
		public delegate void FrameReadyEventHandler(VideoPlayer source, long frameIdx);

		// Token: 0x02000016 RID: 22
		// (Invoke) Token: 0x060000D7 RID: 215
		public delegate void TimeEventHandler(VideoPlayer source, double seconds);
	}
}
