﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine.Video
{
	// Token: 0x0200000E RID: 14
	[RequiredByNativeCode]
	public enum VideoTimeSource
	{
		// Token: 0x04000026 RID: 38
		AudioDSPTimeSource,
		// Token: 0x04000027 RID: 39
		GameTimeSource
	}
}
