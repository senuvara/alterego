﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine.Video
{
	// Token: 0x02000006 RID: 6
	[NativeHeader("Modules/Video/Public/VideoClip.h")]
	[RequiredByNativeCode]
	public sealed class VideoClip : Object
	{
		// Token: 0x0600001F RID: 31 RVA: 0x00002058 File Offset: 0x00000258
		private VideoClip()
		{
		}

		// Token: 0x17000001 RID: 1
		// (get) Token: 0x06000020 RID: 32
		public extern string originalPath { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000002 RID: 2
		// (get) Token: 0x06000021 RID: 33
		public extern ulong frameCount { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000003 RID: 3
		// (get) Token: 0x06000022 RID: 34
		public extern double frameRate { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000004 RID: 4
		// (get) Token: 0x06000023 RID: 35
		[NativeName("Duration")]
		public extern double length { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000005 RID: 5
		// (get) Token: 0x06000024 RID: 36
		public extern uint width { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000006 RID: 6
		// (get) Token: 0x06000025 RID: 37
		public extern uint height { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000007 RID: 7
		// (get) Token: 0x06000026 RID: 38
		public extern uint pixelAspectRatioNumerator { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000008 RID: 8
		// (get) Token: 0x06000027 RID: 39
		public extern uint pixelAspectRatioDenominator { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000009 RID: 9
		// (get) Token: 0x06000028 RID: 40
		public extern ushort audioTrackCount { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x06000029 RID: 41
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern ushort GetAudioChannelCount(ushort audioTrackIdx);

		// Token: 0x0600002A RID: 42
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern uint GetAudioSampleRate(ushort audioTrackIdx);

		// Token: 0x0600002B RID: 43
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern string GetAudioLanguage(ushort audioTrackIdx);
	}
}
