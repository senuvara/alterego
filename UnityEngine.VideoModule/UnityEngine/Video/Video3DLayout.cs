﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine.Video
{
	// Token: 0x0200000C RID: 12
	[RequiredByNativeCode]
	public enum Video3DLayout
	{
		// Token: 0x0400001B RID: 27
		No3D,
		// Token: 0x0400001C RID: 28
		SideBySide3D,
		// Token: 0x0400001D RID: 29
		OverUnder3D
	}
}
