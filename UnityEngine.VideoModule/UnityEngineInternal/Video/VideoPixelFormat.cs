﻿using System;
using UnityEngine.Scripting;

namespace UnityEngineInternal.Video
{
	// Token: 0x02000003 RID: 3
	[UsedByNativeCode]
	internal enum VideoPixelFormat
	{
		// Token: 0x0400000D RID: 13
		RGB,
		// Token: 0x0400000E RID: 14
		RGBA,
		// Token: 0x0400000F RID: 15
		YUV,
		// Token: 0x04000010 RID: 16
		YUVA
	}
}
