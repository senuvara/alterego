﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngineInternal.Video
{
	// Token: 0x02000004 RID: 4
	[NativeHeader("Modules/Video/Public/Base/MediaComponent.h")]
	[UsedByNativeCode]
	internal class VideoPlayback
	{
		// Token: 0x06000001 RID: 1 RVA: 0x00002050 File Offset: 0x00000250
		public VideoPlayback()
		{
		}

		// Token: 0x06000002 RID: 2
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void StartPlayback();

		// Token: 0x06000003 RID: 3
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void PausePlayback();

		// Token: 0x06000004 RID: 4
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void StopPlayback();

		// Token: 0x06000005 RID: 5
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern VideoError GetStatus();

		// Token: 0x06000006 RID: 6
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern bool IsReady();

		// Token: 0x06000007 RID: 7
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern bool IsPlaying();

		// Token: 0x06000008 RID: 8
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void Step();

		// Token: 0x06000009 RID: 9
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern bool CanStep();

		// Token: 0x0600000A RID: 10
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern uint GetWidth();

		// Token: 0x0600000B RID: 11
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern uint GetHeight();

		// Token: 0x0600000C RID: 12
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern float GetFrameRate();

		// Token: 0x0600000D RID: 13
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern float GetDuration();

		// Token: 0x0600000E RID: 14
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern ulong GetFrameCount();

		// Token: 0x0600000F RID: 15
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern uint GetPixelAspectRatioNumerator();

		// Token: 0x06000010 RID: 16
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern uint GetPixelAspectRatioDenominator();

		// Token: 0x06000011 RID: 17
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern VideoPixelFormat GetPixelFormat();

		// Token: 0x06000012 RID: 18
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern bool CanNotSkipOnDrop();

		// Token: 0x06000013 RID: 19
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SetSkipOnDrop(bool skipOnDrop);

		// Token: 0x06000014 RID: 20
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern bool GetTexture(Texture texture, out long outputFrameNum);

		// Token: 0x06000015 RID: 21
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SeekToFrame(long frameIndex, VideoPlayback.Callback seekCompletedCallback);

		// Token: 0x06000016 RID: 22
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SeekToTime(double secs, VideoPlayback.Callback seekCompletedCallback);

		// Token: 0x06000017 RID: 23
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern float GetPlaybackSpeed();

		// Token: 0x06000018 RID: 24
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SetPlaybackSpeed(float value);

		// Token: 0x06000019 RID: 25
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern bool GetLoop();

		// Token: 0x0600001A RID: 26
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SetLoop(bool value);

		// Token: 0x04000011 RID: 17
		internal IntPtr m_Ptr;

		// Token: 0x02000005 RID: 5
		// (Invoke) Token: 0x0600001C RID: 28
		public delegate void Callback();
	}
}
