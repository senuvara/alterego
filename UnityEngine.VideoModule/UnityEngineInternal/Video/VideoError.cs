﻿using System;
using UnityEngine.Scripting;

namespace UnityEngineInternal.Video
{
	// Token: 0x02000002 RID: 2
	[UsedByNativeCode]
	internal enum VideoError
	{
		// Token: 0x04000002 RID: 2
		NoErr,
		// Token: 0x04000003 RID: 3
		OutOfMemoryErr,
		// Token: 0x04000004 RID: 4
		CantReadFile,
		// Token: 0x04000005 RID: 5
		CantWriteFile,
		// Token: 0x04000006 RID: 6
		BadParams,
		// Token: 0x04000007 RID: 7
		NoData,
		// Token: 0x04000008 RID: 8
		BadPermissions,
		// Token: 0x04000009 RID: 9
		DeviceNotAvailable,
		// Token: 0x0400000A RID: 10
		ResourceNotAvailable,
		// Token: 0x0400000B RID: 11
		NetworkErr
	}
}
