﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngineInternal.Video
{
	// Token: 0x02000008 RID: 8
	[UsedByNativeCode]
	[NativeHeader("Modules/Video/Public/Base/VideoMediaPlayback.h")]
	internal class VideoPlaybackMgr : IDisposable
	{
		// Token: 0x0600004A RID: 74 RVA: 0x00002402 File Offset: 0x00000602
		public VideoPlaybackMgr()
		{
			this.m_Ptr = VideoPlaybackMgr.Internal_Create();
		}

		// Token: 0x0600004B RID: 75 RVA: 0x00002416 File Offset: 0x00000616
		public void Dispose()
		{
			if (this.m_Ptr != IntPtr.Zero)
			{
				VideoPlaybackMgr.Internal_Destroy(this.m_Ptr);
				this.m_Ptr = IntPtr.Zero;
			}
			GC.SuppressFinalize(this);
		}

		// Token: 0x0600004C RID: 76
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern IntPtr Internal_Create();

		// Token: 0x0600004D RID: 77
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void Internal_Destroy(IntPtr ptr);

		// Token: 0x0600004E RID: 78
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern VideoPlayback CreateVideoPlayback(string fileName, VideoPlaybackMgr.MessageCallback errorCallback, VideoPlaybackMgr.Callback readyCallback, VideoPlaybackMgr.Callback reachedEndCallback);

		// Token: 0x0600004F RID: 79
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void ReleaseVideoPlayback(VideoPlayback playback);

		// Token: 0x1700000A RID: 10
		// (get) Token: 0x06000050 RID: 80
		public extern ulong videoPlaybackCount { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x06000051 RID: 81
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void Update();

		// Token: 0x06000052 RID: 82
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern void ProcessOSMainLoopMessagesForTesting();

		// Token: 0x04000013 RID: 19
		internal IntPtr m_Ptr;

		// Token: 0x02000009 RID: 9
		// (Invoke) Token: 0x06000054 RID: 84
		public delegate void Callback();

		// Token: 0x0200000A RID: 10
		// (Invoke) Token: 0x06000058 RID: 88
		public delegate void MessageCallback(string message);
	}
}
