﻿using System;
using UnityEngine.UI;

namespace UnityEngine.Experimental.UI
{
	// Token: 0x020000AB RID: 171
	public static class VertexHelperExtension
	{
		// Token: 0x06000633 RID: 1587 RVA: 0x0001F194 File Offset: 0x0001D594
		public static void AddVert(this VertexHelper obj, Vector3 position, Color32 color, Vector2 uv0, Vector2 uv1, Vector2 uv2, Vector2 uv3, Vector3 normal, Vector4 tangent)
		{
			obj.AddVert(position, color, uv0, uv1, uv2, uv3, normal, tangent);
		}
	}
}
