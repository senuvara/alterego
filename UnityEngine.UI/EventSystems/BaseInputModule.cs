﻿using System;
using System.Collections.Generic;

namespace UnityEngine.EventSystems
{
	// Token: 0x02000027 RID: 39
	[RequireComponent(typeof(EventSystem))]
	public abstract class BaseInputModule : UIBehaviour
	{
		// Token: 0x060000DA RID: 218 RVA: 0x00003E37 File Offset: 0x00002237
		protected BaseInputModule()
		{
		}

		// Token: 0x17000042 RID: 66
		// (get) Token: 0x060000DB RID: 219 RVA: 0x00003E4C File Offset: 0x0000224C
		public BaseInput input
		{
			get
			{
				BaseInput result;
				if (this.m_InputOverride != null)
				{
					result = this.m_InputOverride;
				}
				else
				{
					if (this.m_DefaultInput == null)
					{
						BaseInput[] components = base.GetComponents<BaseInput>();
						foreach (BaseInput baseInput in components)
						{
							if (baseInput != null && baseInput.GetType() == typeof(BaseInput))
							{
								this.m_DefaultInput = baseInput;
								break;
							}
						}
						if (this.m_DefaultInput == null)
						{
							this.m_DefaultInput = base.gameObject.AddComponent<BaseInput>();
						}
					}
					result = this.m_DefaultInput;
				}
				return result;
			}
		}

		// Token: 0x17000043 RID: 67
		// (get) Token: 0x060000DC RID: 220 RVA: 0x00003F10 File Offset: 0x00002310
		// (set) Token: 0x060000DD RID: 221 RVA: 0x00003F2B File Offset: 0x0000232B
		public BaseInput inputOverride
		{
			get
			{
				return this.m_InputOverride;
			}
			set
			{
				this.m_InputOverride = value;
			}
		}

		// Token: 0x17000044 RID: 68
		// (get) Token: 0x060000DE RID: 222 RVA: 0x00003F38 File Offset: 0x00002338
		protected EventSystem eventSystem
		{
			get
			{
				return this.m_EventSystem;
			}
		}

		// Token: 0x060000DF RID: 223 RVA: 0x00003F53 File Offset: 0x00002353
		protected override void OnEnable()
		{
			base.OnEnable();
			this.m_EventSystem = base.GetComponent<EventSystem>();
			this.m_EventSystem.UpdateModules();
		}

		// Token: 0x060000E0 RID: 224 RVA: 0x00003F73 File Offset: 0x00002373
		protected override void OnDisable()
		{
			this.m_EventSystem.UpdateModules();
			base.OnDisable();
		}

		// Token: 0x060000E1 RID: 225
		public abstract void Process();

		// Token: 0x060000E2 RID: 226 RVA: 0x00003F88 File Offset: 0x00002388
		protected static RaycastResult FindFirstRaycast(List<RaycastResult> candidates)
		{
			for (int i = 0; i < candidates.Count; i++)
			{
				if (!(candidates[i].gameObject == null))
				{
					return candidates[i];
				}
			}
			return default(RaycastResult);
		}

		// Token: 0x060000E3 RID: 227 RVA: 0x00003FEC File Offset: 0x000023EC
		protected static MoveDirection DetermineMoveDirection(float x, float y)
		{
			return BaseInputModule.DetermineMoveDirection(x, y, 0.6f);
		}

		// Token: 0x060000E4 RID: 228 RVA: 0x00004010 File Offset: 0x00002410
		protected static MoveDirection DetermineMoveDirection(float x, float y, float deadZone)
		{
			Vector2 vector = new Vector2(x, y);
			MoveDirection result;
			if (vector.sqrMagnitude < deadZone * deadZone)
			{
				result = MoveDirection.None;
			}
			else if (Mathf.Abs(x) > Mathf.Abs(y))
			{
				if (x > 0f)
				{
					result = MoveDirection.Right;
				}
				else
				{
					result = MoveDirection.Left;
				}
			}
			else if (y > 0f)
			{
				result = MoveDirection.Up;
			}
			else
			{
				result = MoveDirection.Down;
			}
			return result;
		}

		// Token: 0x060000E5 RID: 229 RVA: 0x00004084 File Offset: 0x00002484
		protected static GameObject FindCommonRoot(GameObject g1, GameObject g2)
		{
			GameObject result;
			if (g1 == null || g2 == null)
			{
				result = null;
			}
			else
			{
				Transform transform = g1.transform;
				while (transform != null)
				{
					Transform transform2 = g2.transform;
					while (transform2 != null)
					{
						if (transform == transform2)
						{
							return transform.gameObject;
						}
						transform2 = transform2.parent;
					}
					transform = transform.parent;
				}
				result = null;
			}
			return result;
		}

		// Token: 0x060000E6 RID: 230 RVA: 0x00004114 File Offset: 0x00002514
		protected void HandlePointerExitAndEnter(PointerEventData currentPointerData, GameObject newEnterTarget)
		{
			if (newEnterTarget == null || currentPointerData.pointerEnter == null)
			{
				for (int i = 0; i < currentPointerData.hovered.Count; i++)
				{
					ExecuteEvents.Execute<IPointerExitHandler>(currentPointerData.hovered[i], currentPointerData, ExecuteEvents.pointerExitHandler);
				}
				currentPointerData.hovered.Clear();
				if (newEnterTarget == null)
				{
					currentPointerData.pointerEnter = null;
					return;
				}
			}
			if (!(currentPointerData.pointerEnter == newEnterTarget) || !newEnterTarget)
			{
				GameObject gameObject = BaseInputModule.FindCommonRoot(currentPointerData.pointerEnter, newEnterTarget);
				if (currentPointerData.pointerEnter != null)
				{
					Transform transform = currentPointerData.pointerEnter.transform;
					while (transform != null)
					{
						if (gameObject != null && gameObject.transform == transform)
						{
							break;
						}
						ExecuteEvents.Execute<IPointerExitHandler>(transform.gameObject, currentPointerData, ExecuteEvents.pointerExitHandler);
						currentPointerData.hovered.Remove(transform.gameObject);
						transform = transform.parent;
					}
				}
				currentPointerData.pointerEnter = newEnterTarget;
				if (newEnterTarget != null)
				{
					Transform transform2 = newEnterTarget.transform;
					while (transform2 != null && transform2.gameObject != gameObject)
					{
						ExecuteEvents.Execute<IPointerEnterHandler>(transform2.gameObject, currentPointerData, ExecuteEvents.pointerEnterHandler);
						currentPointerData.hovered.Add(transform2.gameObject);
						transform2 = transform2.parent;
					}
				}
			}
		}

		// Token: 0x060000E7 RID: 231 RVA: 0x000042B0 File Offset: 0x000026B0
		protected virtual AxisEventData GetAxisEventData(float x, float y, float moveDeadZone)
		{
			if (this.m_AxisEventData == null)
			{
				this.m_AxisEventData = new AxisEventData(this.eventSystem);
			}
			this.m_AxisEventData.Reset();
			this.m_AxisEventData.moveVector = new Vector2(x, y);
			this.m_AxisEventData.moveDir = BaseInputModule.DetermineMoveDirection(x, y, moveDeadZone);
			return this.m_AxisEventData;
		}

		// Token: 0x060000E8 RID: 232 RVA: 0x00004318 File Offset: 0x00002718
		protected virtual BaseEventData GetBaseEventData()
		{
			if (this.m_BaseEventData == null)
			{
				this.m_BaseEventData = new BaseEventData(this.eventSystem);
			}
			this.m_BaseEventData.Reset();
			return this.m_BaseEventData;
		}

		// Token: 0x060000E9 RID: 233 RVA: 0x0000435C File Offset: 0x0000275C
		public virtual bool IsPointerOverGameObject(int pointerId)
		{
			return false;
		}

		// Token: 0x060000EA RID: 234 RVA: 0x00004374 File Offset: 0x00002774
		public virtual bool ShouldActivateModule()
		{
			return base.enabled && base.gameObject.activeInHierarchy;
		}

		// Token: 0x060000EB RID: 235 RVA: 0x000043A2 File Offset: 0x000027A2
		public virtual void DeactivateModule()
		{
		}

		// Token: 0x060000EC RID: 236 RVA: 0x000043A5 File Offset: 0x000027A5
		public virtual void ActivateModule()
		{
		}

		// Token: 0x060000ED RID: 237 RVA: 0x000043A8 File Offset: 0x000027A8
		public virtual void UpdateModule()
		{
		}

		// Token: 0x060000EE RID: 238 RVA: 0x000043AC File Offset: 0x000027AC
		public virtual bool IsModuleSupported()
		{
			return true;
		}

		// Token: 0x0400007D RID: 125
		[NonSerialized]
		protected List<RaycastResult> m_RaycastResultCache = new List<RaycastResult>();

		// Token: 0x0400007E RID: 126
		private AxisEventData m_AxisEventData;

		// Token: 0x0400007F RID: 127
		private EventSystem m_EventSystem;

		// Token: 0x04000080 RID: 128
		private BaseEventData m_BaseEventData;

		// Token: 0x04000081 RID: 129
		protected BaseInput m_InputOverride;

		// Token: 0x04000082 RID: 130
		private BaseInput m_DefaultInput;
	}
}
