﻿using System;
using System.Collections.Generic;

namespace UnityEngine.EventSystems
{
	// Token: 0x0200002F RID: 47
	public abstract class BaseRaycaster : UIBehaviour
	{
		// Token: 0x0600013F RID: 319 RVA: 0x000061B4 File Offset: 0x000045B4
		protected BaseRaycaster()
		{
		}

		// Token: 0x06000140 RID: 320
		public abstract void Raycast(PointerEventData eventData, List<RaycastResult> resultAppendList);

		// Token: 0x17000052 RID: 82
		// (get) Token: 0x06000141 RID: 321
		public abstract Camera eventCamera { get; }

		// Token: 0x17000053 RID: 83
		// (get) Token: 0x06000142 RID: 322 RVA: 0x000061BC File Offset: 0x000045BC
		[Obsolete("Please use sortOrderPriority and renderOrderPriority", false)]
		public virtual int priority
		{
			get
			{
				return 0;
			}
		}

		// Token: 0x17000054 RID: 84
		// (get) Token: 0x06000143 RID: 323 RVA: 0x000061D4 File Offset: 0x000045D4
		public virtual int sortOrderPriority
		{
			get
			{
				return int.MinValue;
			}
		}

		// Token: 0x17000055 RID: 85
		// (get) Token: 0x06000144 RID: 324 RVA: 0x000061F0 File Offset: 0x000045F0
		public virtual int renderOrderPriority
		{
			get
			{
				return int.MinValue;
			}
		}

		// Token: 0x06000145 RID: 325 RVA: 0x0000620C File Offset: 0x0000460C
		public override string ToString()
		{
			return string.Concat(new object[]
			{
				"Name: ",
				base.gameObject,
				"\neventCamera: ",
				this.eventCamera,
				"\nsortOrderPriority: ",
				this.sortOrderPriority,
				"\nrenderOrderPriority: ",
				this.renderOrderPriority
			});
		}

		// Token: 0x06000146 RID: 326 RVA: 0x0000627A File Offset: 0x0000467A
		protected override void OnEnable()
		{
			base.OnEnable();
			RaycasterManager.AddRaycaster(this);
		}

		// Token: 0x06000147 RID: 327 RVA: 0x00006289 File Offset: 0x00004689
		protected override void OnDisable()
		{
			RaycasterManager.RemoveRaycasters(this);
			base.OnDisable();
		}
	}
}
