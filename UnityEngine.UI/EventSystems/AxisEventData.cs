﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace UnityEngine.EventSystems
{
	// Token: 0x02000020 RID: 32
	public class AxisEventData : BaseEventData
	{
		// Token: 0x0600008E RID: 142 RVA: 0x00003694 File Offset: 0x00001A94
		public AxisEventData(EventSystem eventSystem) : base(eventSystem)
		{
			this.moveVector = Vector2.zero;
			this.moveDir = MoveDirection.None;
		}

		// Token: 0x1700001F RID: 31
		// (get) Token: 0x0600008F RID: 143 RVA: 0x000036B0 File Offset: 0x00001AB0
		// (set) Token: 0x06000090 RID: 144 RVA: 0x000036CA File Offset: 0x00001ACA
		public Vector2 moveVector
		{
			[CompilerGenerated]
			get
			{
				return this.<moveVector>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<moveVector>k__BackingField = value;
			}
		}

		// Token: 0x17000020 RID: 32
		// (get) Token: 0x06000091 RID: 145 RVA: 0x000036D4 File Offset: 0x00001AD4
		// (set) Token: 0x06000092 RID: 146 RVA: 0x000036EE File Offset: 0x00001AEE
		public MoveDirection moveDir
		{
			[CompilerGenerated]
			get
			{
				return this.<moveDir>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<moveDir>k__BackingField = value;
			}
		}

		// Token: 0x0400005B RID: 91
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private Vector2 <moveVector>k__BackingField;

		// Token: 0x0400005C RID: 92
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private MoveDirection <moveDir>k__BackingField;
	}
}
