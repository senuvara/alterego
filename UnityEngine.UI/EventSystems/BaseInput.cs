﻿using System;

namespace UnityEngine.EventSystems
{
	// Token: 0x02000026 RID: 38
	public class BaseInput : UIBehaviour
	{
		// Token: 0x060000C9 RID: 201 RVA: 0x00003C93 File Offset: 0x00002093
		public BaseInput()
		{
		}

		// Token: 0x1700003A RID: 58
		// (get) Token: 0x060000CA RID: 202 RVA: 0x00003C9C File Offset: 0x0000209C
		public virtual string compositionString
		{
			get
			{
				return Input.compositionString;
			}
		}

		// Token: 0x1700003B RID: 59
		// (get) Token: 0x060000CB RID: 203 RVA: 0x00003CB8 File Offset: 0x000020B8
		// (set) Token: 0x060000CC RID: 204 RVA: 0x00003CD2 File Offset: 0x000020D2
		public virtual IMECompositionMode imeCompositionMode
		{
			get
			{
				return Input.imeCompositionMode;
			}
			set
			{
				Input.imeCompositionMode = value;
			}
		}

		// Token: 0x1700003C RID: 60
		// (get) Token: 0x060000CD RID: 205 RVA: 0x00003CDC File Offset: 0x000020DC
		// (set) Token: 0x060000CE RID: 206 RVA: 0x00003CF6 File Offset: 0x000020F6
		public virtual Vector2 compositionCursorPos
		{
			get
			{
				return Input.compositionCursorPos;
			}
			set
			{
				Input.compositionCursorPos = value;
			}
		}

		// Token: 0x1700003D RID: 61
		// (get) Token: 0x060000CF RID: 207 RVA: 0x00003D00 File Offset: 0x00002100
		public virtual bool mousePresent
		{
			get
			{
				return Input.mousePresent;
			}
		}

		// Token: 0x060000D0 RID: 208 RVA: 0x00003D1C File Offset: 0x0000211C
		public virtual bool GetMouseButtonDown(int button)
		{
			return Input.GetMouseButtonDown(button);
		}

		// Token: 0x060000D1 RID: 209 RVA: 0x00003D38 File Offset: 0x00002138
		public virtual bool GetMouseButtonUp(int button)
		{
			return Input.GetMouseButtonUp(button);
		}

		// Token: 0x060000D2 RID: 210 RVA: 0x00003D54 File Offset: 0x00002154
		public virtual bool GetMouseButton(int button)
		{
			return Input.GetMouseButton(button);
		}

		// Token: 0x1700003E RID: 62
		// (get) Token: 0x060000D3 RID: 211 RVA: 0x00003D70 File Offset: 0x00002170
		public virtual Vector2 mousePosition
		{
			get
			{
				return Input.mousePosition;
			}
		}

		// Token: 0x1700003F RID: 63
		// (get) Token: 0x060000D4 RID: 212 RVA: 0x00003D90 File Offset: 0x00002190
		public virtual Vector2 mouseScrollDelta
		{
			get
			{
				return Input.mouseScrollDelta;
			}
		}

		// Token: 0x17000040 RID: 64
		// (get) Token: 0x060000D5 RID: 213 RVA: 0x00003DAC File Offset: 0x000021AC
		public virtual bool touchSupported
		{
			get
			{
				return Input.touchSupported;
			}
		}

		// Token: 0x17000041 RID: 65
		// (get) Token: 0x060000D6 RID: 214 RVA: 0x00003DC8 File Offset: 0x000021C8
		public virtual int touchCount
		{
			get
			{
				return Input.touchCount;
			}
		}

		// Token: 0x060000D7 RID: 215 RVA: 0x00003DE4 File Offset: 0x000021E4
		public virtual Touch GetTouch(int index)
		{
			return Input.GetTouch(index);
		}

		// Token: 0x060000D8 RID: 216 RVA: 0x00003E00 File Offset: 0x00002200
		public virtual float GetAxisRaw(string axisName)
		{
			return Input.GetAxisRaw(axisName);
		}

		// Token: 0x060000D9 RID: 217 RVA: 0x00003E1C File Offset: 0x0000221C
		public virtual bool GetButtonDown(string buttonName)
		{
			return Input.GetButtonDown(buttonName);
		}
	}
}
