﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine.UI;

namespace UnityEngine.EventSystems
{
	// Token: 0x02000031 RID: 49
	[AddComponentMenu("Event/Physics Raycaster")]
	[RequireComponent(typeof(Camera))]
	public class PhysicsRaycaster : BaseRaycaster
	{
		// Token: 0x0600014A RID: 330 RVA: 0x00006298 File Offset: 0x00004698
		protected PhysicsRaycaster()
		{
		}

		// Token: 0x17000056 RID: 86
		// (get) Token: 0x0600014B RID: 331 RVA: 0x000062BC File Offset: 0x000046BC
		public override Camera eventCamera
		{
			get
			{
				if (this.m_EventCamera == null)
				{
					this.m_EventCamera = base.GetComponent<Camera>();
				}
				return this.m_EventCamera ?? Camera.main;
			}
		}

		// Token: 0x17000057 RID: 87
		// (get) Token: 0x0600014C RID: 332 RVA: 0x00006300 File Offset: 0x00004700
		public virtual int depth
		{
			get
			{
				return (!(this.eventCamera != null)) ? 16777215 : ((int)this.eventCamera.depth);
			}
		}

		// Token: 0x17000058 RID: 88
		// (get) Token: 0x0600014D RID: 333 RVA: 0x0000633C File Offset: 0x0000473C
		public int finalEventMask
		{
			get
			{
				return (!(this.eventCamera != null)) ? -1 : (this.eventCamera.cullingMask & this.m_EventMask);
			}
		}

		// Token: 0x17000059 RID: 89
		// (get) Token: 0x0600014E RID: 334 RVA: 0x00006380 File Offset: 0x00004780
		// (set) Token: 0x0600014F RID: 335 RVA: 0x0000639B File Offset: 0x0000479B
		public LayerMask eventMask
		{
			get
			{
				return this.m_EventMask;
			}
			set
			{
				this.m_EventMask = value;
			}
		}

		// Token: 0x1700005A RID: 90
		// (get) Token: 0x06000150 RID: 336 RVA: 0x000063A8 File Offset: 0x000047A8
		// (set) Token: 0x06000151 RID: 337 RVA: 0x000063C3 File Offset: 0x000047C3
		public int maxRayIntersections
		{
			get
			{
				return this.m_MaxRayIntersections;
			}
			set
			{
				this.m_MaxRayIntersections = value;
			}
		}

		// Token: 0x06000152 RID: 338 RVA: 0x000063D0 File Offset: 0x000047D0
		protected bool ComputeRayAndDistance(PointerEventData eventData, ref Ray ray, ref float distanceToClipPlane)
		{
			bool result;
			if (this.eventCamera == null)
			{
				result = false;
			}
			else
			{
				Vector3 vector = Display.RelativeMouseAt(eventData.position);
				if (vector != Vector3.zero)
				{
					int num = (int)vector.z;
					if (num != this.eventCamera.targetDisplay)
					{
						return false;
					}
				}
				else
				{
					vector = eventData.position;
				}
				if (!this.eventCamera.pixelRect.Contains(vector))
				{
					result = false;
				}
				else
				{
					ray = this.eventCamera.ScreenPointToRay(vector);
					float z = ray.direction.z;
					distanceToClipPlane = ((!Mathf.Approximately(0f, z)) ? Mathf.Abs((this.eventCamera.farClipPlane - this.eventCamera.nearClipPlane) / z) : float.PositiveInfinity);
					result = true;
				}
			}
			return result;
		}

		// Token: 0x06000153 RID: 339 RVA: 0x000064D4 File Offset: 0x000048D4
		public override void Raycast(PointerEventData eventData, List<RaycastResult> resultAppendList)
		{
			Ray r = default(Ray);
			float f = 0f;
			if (this.ComputeRayAndDistance(eventData, ref r, ref f))
			{
				int num;
				if (this.m_MaxRayIntersections == 0)
				{
					if (ReflectionMethodsCache.Singleton.raycast3DAll == null)
					{
						return;
					}
					this.m_Hits = ReflectionMethodsCache.Singleton.raycast3DAll(r, f, this.finalEventMask);
					num = this.m_Hits.Length;
				}
				else
				{
					if (ReflectionMethodsCache.Singleton.getRaycastNonAlloc == null)
					{
						return;
					}
					if (this.m_LastMaxRayIntersections != this.m_MaxRayIntersections)
					{
						this.m_Hits = new RaycastHit[this.m_MaxRayIntersections];
						this.m_LastMaxRayIntersections = this.m_MaxRayIntersections;
					}
					num = ReflectionMethodsCache.Singleton.getRaycastNonAlloc(r, this.m_Hits, f, this.finalEventMask);
				}
				if (num > 1)
				{
					Array.Sort<RaycastHit>(this.m_Hits, (RaycastHit r1, RaycastHit r2) => r1.distance.CompareTo(r2.distance));
				}
				if (num != 0)
				{
					int i = 0;
					int num2 = num;
					while (i < num2)
					{
						RaycastResult item = new RaycastResult
						{
							gameObject = this.m_Hits[i].collider.gameObject,
							module = this,
							distance = this.m_Hits[i].distance,
							worldPosition = this.m_Hits[i].point,
							worldNormal = this.m_Hits[i].normal,
							screenPosition = eventData.position,
							index = (float)resultAppendList.Count,
							sortingLayer = 0,
							sortingOrder = 0
						};
						resultAppendList.Add(item);
						i++;
					}
				}
			}
		}

		// Token: 0x06000154 RID: 340 RVA: 0x000066B4 File Offset: 0x00004AB4
		[CompilerGenerated]
		private static int <Raycast>m__0(RaycastHit r1, RaycastHit r2)
		{
			return r1.distance.CompareTo(r2.distance);
		}

		// Token: 0x040000A4 RID: 164
		protected const int kNoEventMaskSet = -1;

		// Token: 0x040000A5 RID: 165
		protected Camera m_EventCamera;

		// Token: 0x040000A6 RID: 166
		[SerializeField]
		protected LayerMask m_EventMask = -1;

		// Token: 0x040000A7 RID: 167
		[SerializeField]
		protected int m_MaxRayIntersections = 0;

		// Token: 0x040000A8 RID: 168
		protected int m_LastMaxRayIntersections = 0;

		// Token: 0x040000A9 RID: 169
		private RaycastHit[] m_Hits;

		// Token: 0x040000AA RID: 170
		[CompilerGenerated]
		private static Comparison<RaycastHit> <>f__am$cache0;
	}
}
