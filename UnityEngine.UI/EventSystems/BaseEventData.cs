﻿using System;

namespace UnityEngine.EventSystems
{
	// Token: 0x02000022 RID: 34
	public class BaseEventData : AbstractEventData
	{
		// Token: 0x06000097 RID: 151 RVA: 0x00003633 File Offset: 0x00001A33
		public BaseEventData(EventSystem eventSystem)
		{
			this.m_EventSystem = eventSystem;
		}

		// Token: 0x17000022 RID: 34
		// (get) Token: 0x06000098 RID: 152 RVA: 0x00003644 File Offset: 0x00001A44
		public BaseInputModule currentInputModule
		{
			get
			{
				return this.m_EventSystem.currentInputModule;
			}
		}

		// Token: 0x17000023 RID: 35
		// (get) Token: 0x06000099 RID: 153 RVA: 0x00003664 File Offset: 0x00001A64
		// (set) Token: 0x0600009A RID: 154 RVA: 0x00003684 File Offset: 0x00001A84
		public GameObject selectedObject
		{
			get
			{
				return this.m_EventSystem.currentSelectedGameObject;
			}
			set
			{
				this.m_EventSystem.SetSelectedGameObject(value, this);
			}
		}

		// Token: 0x0400005E RID: 94
		private readonly EventSystem m_EventSystem;
	}
}
