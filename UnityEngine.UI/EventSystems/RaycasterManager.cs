﻿using System;
using System.Collections.Generic;

namespace UnityEngine.EventSystems
{
	// Token: 0x0200001E RID: 30
	internal static class RaycasterManager
	{
		// Token: 0x0600007C RID: 124 RVA: 0x0000358A File Offset: 0x0000198A
		public static void AddRaycaster(BaseRaycaster baseRaycaster)
		{
			if (!RaycasterManager.s_Raycasters.Contains(baseRaycaster))
			{
				RaycasterManager.s_Raycasters.Add(baseRaycaster);
			}
		}

		// Token: 0x0600007D RID: 125 RVA: 0x000035B0 File Offset: 0x000019B0
		public static List<BaseRaycaster> GetRaycasters()
		{
			return RaycasterManager.s_Raycasters;
		}

		// Token: 0x0600007E RID: 126 RVA: 0x000035CA File Offset: 0x000019CA
		public static void RemoveRaycasters(BaseRaycaster baseRaycaster)
		{
			if (RaycasterManager.s_Raycasters.Contains(baseRaycaster))
			{
				RaycasterManager.s_Raycasters.Remove(baseRaycaster);
			}
		}

		// Token: 0x0600007F RID: 127 RVA: 0x000035EE File Offset: 0x000019EE
		// Note: this type is marked as 'beforefieldinit'.
		static RaycasterManager()
		{
		}

		// Token: 0x0400005A RID: 90
		private static readonly List<BaseRaycaster> s_Raycasters = new List<BaseRaycaster>();
	}
}
