﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Text;

namespace UnityEngine.EventSystems
{
	// Token: 0x02000023 RID: 35
	public class PointerEventData : BaseEventData
	{
		// Token: 0x0600009B RID: 155 RVA: 0x000036F8 File Offset: 0x00001AF8
		public PointerEventData(EventSystem eventSystem) : base(eventSystem)
		{
			this.eligibleForClick = false;
			this.pointerId = -1;
			this.position = Vector2.zero;
			this.delta = Vector2.zero;
			this.pressPosition = Vector2.zero;
			this.clickTime = 0f;
			this.clickCount = 0;
			this.scrollDelta = Vector2.zero;
			this.useDragThreshold = true;
			this.dragging = false;
			this.button = PointerEventData.InputButton.Left;
		}

		// Token: 0x17000024 RID: 36
		// (get) Token: 0x0600009C RID: 156 RVA: 0x0000377C File Offset: 0x00001B7C
		// (set) Token: 0x0600009D RID: 157 RVA: 0x00003796 File Offset: 0x00001B96
		public GameObject pointerEnter
		{
			[CompilerGenerated]
			get
			{
				return this.<pointerEnter>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<pointerEnter>k__BackingField = value;
			}
		}

		// Token: 0x17000025 RID: 37
		// (get) Token: 0x0600009E RID: 158 RVA: 0x000037A0 File Offset: 0x00001BA0
		// (set) Token: 0x0600009F RID: 159 RVA: 0x000037BA File Offset: 0x00001BBA
		public GameObject lastPress
		{
			[CompilerGenerated]
			get
			{
				return this.<lastPress>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<lastPress>k__BackingField = value;
			}
		}

		// Token: 0x17000026 RID: 38
		// (get) Token: 0x060000A0 RID: 160 RVA: 0x000037C4 File Offset: 0x00001BC4
		// (set) Token: 0x060000A1 RID: 161 RVA: 0x000037DE File Offset: 0x00001BDE
		public GameObject rawPointerPress
		{
			[CompilerGenerated]
			get
			{
				return this.<rawPointerPress>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<rawPointerPress>k__BackingField = value;
			}
		}

		// Token: 0x17000027 RID: 39
		// (get) Token: 0x060000A2 RID: 162 RVA: 0x000037E8 File Offset: 0x00001BE8
		// (set) Token: 0x060000A3 RID: 163 RVA: 0x00003802 File Offset: 0x00001C02
		public GameObject pointerDrag
		{
			[CompilerGenerated]
			get
			{
				return this.<pointerDrag>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<pointerDrag>k__BackingField = value;
			}
		}

		// Token: 0x17000028 RID: 40
		// (get) Token: 0x060000A4 RID: 164 RVA: 0x0000380C File Offset: 0x00001C0C
		// (set) Token: 0x060000A5 RID: 165 RVA: 0x00003826 File Offset: 0x00001C26
		public RaycastResult pointerCurrentRaycast
		{
			[CompilerGenerated]
			get
			{
				return this.<pointerCurrentRaycast>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<pointerCurrentRaycast>k__BackingField = value;
			}
		}

		// Token: 0x17000029 RID: 41
		// (get) Token: 0x060000A6 RID: 166 RVA: 0x00003830 File Offset: 0x00001C30
		// (set) Token: 0x060000A7 RID: 167 RVA: 0x0000384A File Offset: 0x00001C4A
		public RaycastResult pointerPressRaycast
		{
			[CompilerGenerated]
			get
			{
				return this.<pointerPressRaycast>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<pointerPressRaycast>k__BackingField = value;
			}
		}

		// Token: 0x1700002A RID: 42
		// (get) Token: 0x060000A8 RID: 168 RVA: 0x00003854 File Offset: 0x00001C54
		// (set) Token: 0x060000A9 RID: 169 RVA: 0x0000386E File Offset: 0x00001C6E
		public bool eligibleForClick
		{
			[CompilerGenerated]
			get
			{
				return this.<eligibleForClick>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<eligibleForClick>k__BackingField = value;
			}
		}

		// Token: 0x1700002B RID: 43
		// (get) Token: 0x060000AA RID: 170 RVA: 0x00003878 File Offset: 0x00001C78
		// (set) Token: 0x060000AB RID: 171 RVA: 0x00003892 File Offset: 0x00001C92
		public int pointerId
		{
			[CompilerGenerated]
			get
			{
				return this.<pointerId>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<pointerId>k__BackingField = value;
			}
		}

		// Token: 0x1700002C RID: 44
		// (get) Token: 0x060000AC RID: 172 RVA: 0x0000389C File Offset: 0x00001C9C
		// (set) Token: 0x060000AD RID: 173 RVA: 0x000038B6 File Offset: 0x00001CB6
		public Vector2 position
		{
			[CompilerGenerated]
			get
			{
				return this.<position>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<position>k__BackingField = value;
			}
		}

		// Token: 0x1700002D RID: 45
		// (get) Token: 0x060000AE RID: 174 RVA: 0x000038C0 File Offset: 0x00001CC0
		// (set) Token: 0x060000AF RID: 175 RVA: 0x000038DA File Offset: 0x00001CDA
		public Vector2 delta
		{
			[CompilerGenerated]
			get
			{
				return this.<delta>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<delta>k__BackingField = value;
			}
		}

		// Token: 0x1700002E RID: 46
		// (get) Token: 0x060000B0 RID: 176 RVA: 0x000038E4 File Offset: 0x00001CE4
		// (set) Token: 0x060000B1 RID: 177 RVA: 0x000038FE File Offset: 0x00001CFE
		public Vector2 pressPosition
		{
			[CompilerGenerated]
			get
			{
				return this.<pressPosition>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<pressPosition>k__BackingField = value;
			}
		}

		// Token: 0x1700002F RID: 47
		// (get) Token: 0x060000B2 RID: 178 RVA: 0x00003908 File Offset: 0x00001D08
		// (set) Token: 0x060000B3 RID: 179 RVA: 0x00003922 File Offset: 0x00001D22
		[Obsolete("Use either pointerCurrentRaycast.worldPosition or pointerPressRaycast.worldPosition")]
		public Vector3 worldPosition
		{
			[CompilerGenerated]
			get
			{
				return this.<worldPosition>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<worldPosition>k__BackingField = value;
			}
		}

		// Token: 0x17000030 RID: 48
		// (get) Token: 0x060000B4 RID: 180 RVA: 0x0000392C File Offset: 0x00001D2C
		// (set) Token: 0x060000B5 RID: 181 RVA: 0x00003946 File Offset: 0x00001D46
		[Obsolete("Use either pointerCurrentRaycast.worldNormal or pointerPressRaycast.worldNormal")]
		public Vector3 worldNormal
		{
			[CompilerGenerated]
			get
			{
				return this.<worldNormal>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<worldNormal>k__BackingField = value;
			}
		}

		// Token: 0x17000031 RID: 49
		// (get) Token: 0x060000B6 RID: 182 RVA: 0x00003950 File Offset: 0x00001D50
		// (set) Token: 0x060000B7 RID: 183 RVA: 0x0000396A File Offset: 0x00001D6A
		public float clickTime
		{
			[CompilerGenerated]
			get
			{
				return this.<clickTime>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<clickTime>k__BackingField = value;
			}
		}

		// Token: 0x17000032 RID: 50
		// (get) Token: 0x060000B8 RID: 184 RVA: 0x00003974 File Offset: 0x00001D74
		// (set) Token: 0x060000B9 RID: 185 RVA: 0x0000398E File Offset: 0x00001D8E
		public int clickCount
		{
			[CompilerGenerated]
			get
			{
				return this.<clickCount>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<clickCount>k__BackingField = value;
			}
		}

		// Token: 0x17000033 RID: 51
		// (get) Token: 0x060000BA RID: 186 RVA: 0x00003998 File Offset: 0x00001D98
		// (set) Token: 0x060000BB RID: 187 RVA: 0x000039B2 File Offset: 0x00001DB2
		public Vector2 scrollDelta
		{
			[CompilerGenerated]
			get
			{
				return this.<scrollDelta>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<scrollDelta>k__BackingField = value;
			}
		}

		// Token: 0x17000034 RID: 52
		// (get) Token: 0x060000BC RID: 188 RVA: 0x000039BC File Offset: 0x00001DBC
		// (set) Token: 0x060000BD RID: 189 RVA: 0x000039D6 File Offset: 0x00001DD6
		public bool useDragThreshold
		{
			[CompilerGenerated]
			get
			{
				return this.<useDragThreshold>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<useDragThreshold>k__BackingField = value;
			}
		}

		// Token: 0x17000035 RID: 53
		// (get) Token: 0x060000BE RID: 190 RVA: 0x000039E0 File Offset: 0x00001DE0
		// (set) Token: 0x060000BF RID: 191 RVA: 0x000039FA File Offset: 0x00001DFA
		public bool dragging
		{
			[CompilerGenerated]
			get
			{
				return this.<dragging>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<dragging>k__BackingField = value;
			}
		}

		// Token: 0x17000036 RID: 54
		// (get) Token: 0x060000C0 RID: 192 RVA: 0x00003A04 File Offset: 0x00001E04
		// (set) Token: 0x060000C1 RID: 193 RVA: 0x00003A1E File Offset: 0x00001E1E
		public PointerEventData.InputButton button
		{
			[CompilerGenerated]
			get
			{
				return this.<button>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<button>k__BackingField = value;
			}
		}

		// Token: 0x060000C2 RID: 194 RVA: 0x00003A28 File Offset: 0x00001E28
		public bool IsPointerMoving()
		{
			return this.delta.sqrMagnitude > 0f;
		}

		// Token: 0x060000C3 RID: 195 RVA: 0x00003A54 File Offset: 0x00001E54
		public bool IsScrolling()
		{
			return this.scrollDelta.sqrMagnitude > 0f;
		}

		// Token: 0x17000037 RID: 55
		// (get) Token: 0x060000C4 RID: 196 RVA: 0x00003A80 File Offset: 0x00001E80
		public Camera enterEventCamera
		{
			get
			{
				return (!(this.pointerCurrentRaycast.module == null)) ? this.pointerCurrentRaycast.module.eventCamera : null;
			}
		}

		// Token: 0x17000038 RID: 56
		// (get) Token: 0x060000C5 RID: 197 RVA: 0x00003AC8 File Offset: 0x00001EC8
		public Camera pressEventCamera
		{
			get
			{
				return (!(this.pointerPressRaycast.module == null)) ? this.pointerPressRaycast.module.eventCamera : null;
			}
		}

		// Token: 0x17000039 RID: 57
		// (get) Token: 0x060000C6 RID: 198 RVA: 0x00003B10 File Offset: 0x00001F10
		// (set) Token: 0x060000C7 RID: 199 RVA: 0x00003B2B File Offset: 0x00001F2B
		public GameObject pointerPress
		{
			get
			{
				return this.m_PointerPress;
			}
			set
			{
				if (!(this.m_PointerPress == value))
				{
					this.lastPress = this.m_PointerPress;
					this.m_PointerPress = value;
				}
			}
		}

		// Token: 0x060000C8 RID: 200 RVA: 0x00003B58 File Offset: 0x00001F58
		public override string ToString()
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.AppendLine("<b>Position</b>: " + this.position);
			stringBuilder.AppendLine("<b>delta</b>: " + this.delta);
			stringBuilder.AppendLine("<b>eligibleForClick</b>: " + this.eligibleForClick);
			stringBuilder.AppendLine("<b>pointerEnter</b>: " + this.pointerEnter);
			stringBuilder.AppendLine("<b>pointerPress</b>: " + this.pointerPress);
			stringBuilder.AppendLine("<b>lastPointerPress</b>: " + this.lastPress);
			stringBuilder.AppendLine("<b>pointerDrag</b>: " + this.pointerDrag);
			stringBuilder.AppendLine("<b>Use Drag Threshold</b>: " + this.useDragThreshold);
			stringBuilder.AppendLine("<b>Current Rayast:</b>");
			stringBuilder.AppendLine(this.pointerCurrentRaycast.ToString());
			stringBuilder.AppendLine("<b>Press Rayast:</b>");
			stringBuilder.AppendLine(this.pointerPressRaycast.ToString());
			return stringBuilder.ToString();
		}

		// Token: 0x0400005F RID: 95
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private GameObject <pointerEnter>k__BackingField;

		// Token: 0x04000060 RID: 96
		private GameObject m_PointerPress;

		// Token: 0x04000061 RID: 97
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private GameObject <lastPress>k__BackingField;

		// Token: 0x04000062 RID: 98
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private GameObject <rawPointerPress>k__BackingField;

		// Token: 0x04000063 RID: 99
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private GameObject <pointerDrag>k__BackingField;

		// Token: 0x04000064 RID: 100
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private RaycastResult <pointerCurrentRaycast>k__BackingField;

		// Token: 0x04000065 RID: 101
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private RaycastResult <pointerPressRaycast>k__BackingField;

		// Token: 0x04000066 RID: 102
		public List<GameObject> hovered = new List<GameObject>();

		// Token: 0x04000067 RID: 103
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private bool <eligibleForClick>k__BackingField;

		// Token: 0x04000068 RID: 104
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private int <pointerId>k__BackingField;

		// Token: 0x04000069 RID: 105
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private Vector2 <position>k__BackingField;

		// Token: 0x0400006A RID: 106
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private Vector2 <delta>k__BackingField;

		// Token: 0x0400006B RID: 107
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private Vector2 <pressPosition>k__BackingField;

		// Token: 0x0400006C RID: 108
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private Vector3 <worldPosition>k__BackingField;

		// Token: 0x0400006D RID: 109
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private Vector3 <worldNormal>k__BackingField;

		// Token: 0x0400006E RID: 110
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private float <clickTime>k__BackingField;

		// Token: 0x0400006F RID: 111
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private int <clickCount>k__BackingField;

		// Token: 0x04000070 RID: 112
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private Vector2 <scrollDelta>k__BackingField;

		// Token: 0x04000071 RID: 113
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private bool <useDragThreshold>k__BackingField;

		// Token: 0x04000072 RID: 114
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private bool <dragging>k__BackingField;

		// Token: 0x04000073 RID: 115
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private PointerEventData.InputButton <button>k__BackingField;

		// Token: 0x02000024 RID: 36
		public enum InputButton
		{
			// Token: 0x04000075 RID: 117
			Left,
			// Token: 0x04000076 RID: 118
			Right,
			// Token: 0x04000077 RID: 119
			Middle
		}

		// Token: 0x02000025 RID: 37
		public enum FramePressState
		{
			// Token: 0x04000079 RID: 121
			Pressed,
			// Token: 0x0400007A RID: 122
			Released,
			// Token: 0x0400007B RID: 123
			PressedAndReleased,
			// Token: 0x0400007C RID: 124
			NotChanged
		}
	}
}
