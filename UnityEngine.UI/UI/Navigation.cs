﻿using System;

namespace UnityEngine.UI
{
	// Token: 0x02000066 RID: 102
	[Serializable]
	public struct Navigation : IEquatable<Navigation>
	{
		// Token: 0x170000F0 RID: 240
		// (get) Token: 0x06000387 RID: 903 RVA: 0x0001507C File Offset: 0x0001347C
		// (set) Token: 0x06000388 RID: 904 RVA: 0x00015097 File Offset: 0x00013497
		public Navigation.Mode mode
		{
			get
			{
				return this.m_Mode;
			}
			set
			{
				this.m_Mode = value;
			}
		}

		// Token: 0x170000F1 RID: 241
		// (get) Token: 0x06000389 RID: 905 RVA: 0x000150A4 File Offset: 0x000134A4
		// (set) Token: 0x0600038A RID: 906 RVA: 0x000150BF File Offset: 0x000134BF
		public Selectable selectOnUp
		{
			get
			{
				return this.m_SelectOnUp;
			}
			set
			{
				this.m_SelectOnUp = value;
			}
		}

		// Token: 0x170000F2 RID: 242
		// (get) Token: 0x0600038B RID: 907 RVA: 0x000150CC File Offset: 0x000134CC
		// (set) Token: 0x0600038C RID: 908 RVA: 0x000150E7 File Offset: 0x000134E7
		public Selectable selectOnDown
		{
			get
			{
				return this.m_SelectOnDown;
			}
			set
			{
				this.m_SelectOnDown = value;
			}
		}

		// Token: 0x170000F3 RID: 243
		// (get) Token: 0x0600038D RID: 909 RVA: 0x000150F4 File Offset: 0x000134F4
		// (set) Token: 0x0600038E RID: 910 RVA: 0x0001510F File Offset: 0x0001350F
		public Selectable selectOnLeft
		{
			get
			{
				return this.m_SelectOnLeft;
			}
			set
			{
				this.m_SelectOnLeft = value;
			}
		}

		// Token: 0x170000F4 RID: 244
		// (get) Token: 0x0600038F RID: 911 RVA: 0x0001511C File Offset: 0x0001351C
		// (set) Token: 0x06000390 RID: 912 RVA: 0x00015137 File Offset: 0x00013537
		public Selectable selectOnRight
		{
			get
			{
				return this.m_SelectOnRight;
			}
			set
			{
				this.m_SelectOnRight = value;
			}
		}

		// Token: 0x170000F5 RID: 245
		// (get) Token: 0x06000391 RID: 913 RVA: 0x00015144 File Offset: 0x00013544
		public static Navigation defaultNavigation
		{
			get
			{
				return new Navigation
				{
					m_Mode = Navigation.Mode.Automatic
				};
			}
		}

		// Token: 0x06000392 RID: 914 RVA: 0x0001516C File Offset: 0x0001356C
		public bool Equals(Navigation other)
		{
			return this.mode == other.mode && this.selectOnUp == other.selectOnUp && this.selectOnDown == other.selectOnDown && this.selectOnLeft == other.selectOnLeft && this.selectOnRight == other.selectOnRight;
		}

		// Token: 0x040001BE RID: 446
		[SerializeField]
		private Navigation.Mode m_Mode;

		// Token: 0x040001BF RID: 447
		[SerializeField]
		private Selectable m_SelectOnUp;

		// Token: 0x040001C0 RID: 448
		[SerializeField]
		private Selectable m_SelectOnDown;

		// Token: 0x040001C1 RID: 449
		[SerializeField]
		private Selectable m_SelectOnLeft;

		// Token: 0x040001C2 RID: 450
		[SerializeField]
		private Selectable m_SelectOnRight;

		// Token: 0x02000067 RID: 103
		[Flags]
		public enum Mode
		{
			// Token: 0x040001C4 RID: 452
			None = 0,
			// Token: 0x040001C5 RID: 453
			Horizontal = 1,
			// Token: 0x040001C6 RID: 454
			Vertical = 2,
			// Token: 0x040001C7 RID: 455
			Automatic = 3,
			// Token: 0x040001C8 RID: 456
			Explicit = 4
		}
	}
}
