﻿using System;
using System.Reflection;
using UnityEngineInternal;

namespace UnityEngine.UI
{
	// Token: 0x020000A3 RID: 163
	internal class ReflectionMethodsCache
	{
		// Token: 0x06000604 RID: 1540 RVA: 0x0001E68C File Offset: 0x0001CA8C
		public ReflectionMethodsCache()
		{
			MethodInfo method = typeof(Physics).GetMethod("Raycast", new Type[]
			{
				typeof(Ray),
				typeof(RaycastHit).MakeByRefType(),
				typeof(float),
				typeof(int)
			});
			if (method != null)
			{
				this.raycast3D = (ReflectionMethodsCache.Raycast3DCallback)ScriptingUtils.CreateDelegate(typeof(ReflectionMethodsCache.Raycast3DCallback), method);
			}
			MethodInfo method2 = typeof(Physics2D).GetMethod("Raycast", new Type[]
			{
				typeof(Vector2),
				typeof(Vector2),
				typeof(float),
				typeof(int)
			});
			if (method2 != null)
			{
				this.raycast2D = (ReflectionMethodsCache.Raycast2DCallback)ScriptingUtils.CreateDelegate(typeof(ReflectionMethodsCache.Raycast2DCallback), method2);
			}
			MethodInfo method3 = typeof(Physics).GetMethod("RaycastAll", new Type[]
			{
				typeof(Ray),
				typeof(float),
				typeof(int)
			});
			if (method3 != null)
			{
				this.raycast3DAll = (ReflectionMethodsCache.RaycastAllCallback)ScriptingUtils.CreateDelegate(typeof(ReflectionMethodsCache.RaycastAllCallback), method3);
			}
			MethodInfo method4 = typeof(Physics2D).GetMethod("GetRayIntersectionAll", new Type[]
			{
				typeof(Ray),
				typeof(float),
				typeof(int)
			});
			if (method4 != null)
			{
				this.getRayIntersectionAll = (ReflectionMethodsCache.GetRayIntersectionAllCallback)ScriptingUtils.CreateDelegate(typeof(ReflectionMethodsCache.GetRayIntersectionAllCallback), method4);
			}
			MethodInfo method5 = typeof(Physics2D).GetMethod("GetRayIntersectionNonAlloc", new Type[]
			{
				typeof(Ray),
				typeof(RaycastHit2D[]),
				typeof(float),
				typeof(int)
			});
			if (method5 != null)
			{
				this.getRayIntersectionAllNonAlloc = (ReflectionMethodsCache.GetRayIntersectionAllNonAllocCallback)ScriptingUtils.CreateDelegate(typeof(ReflectionMethodsCache.GetRayIntersectionAllNonAllocCallback), method5);
			}
			MethodInfo method6 = typeof(Physics).GetMethod("RaycastNonAlloc", new Type[]
			{
				typeof(Ray),
				typeof(RaycastHit[]),
				typeof(float),
				typeof(int)
			});
			if (method6 != null)
			{
				this.getRaycastNonAlloc = (ReflectionMethodsCache.GetRaycastNonAllocCallback)ScriptingUtils.CreateDelegate(typeof(ReflectionMethodsCache.GetRaycastNonAllocCallback), method6);
			}
		}

		// Token: 0x1700019E RID: 414
		// (get) Token: 0x06000605 RID: 1541 RVA: 0x0001E95C File Offset: 0x0001CD5C
		public static ReflectionMethodsCache Singleton
		{
			get
			{
				if (ReflectionMethodsCache.s_ReflectionMethodsCache == null)
				{
					ReflectionMethodsCache.s_ReflectionMethodsCache = new ReflectionMethodsCache();
				}
				return ReflectionMethodsCache.s_ReflectionMethodsCache;
			}
		}

		// Token: 0x06000606 RID: 1542 RVA: 0x0001E98A File Offset: 0x0001CD8A
		// Note: this type is marked as 'beforefieldinit'.
		static ReflectionMethodsCache()
		{
		}

		// Token: 0x040002D7 RID: 727
		public ReflectionMethodsCache.Raycast3DCallback raycast3D = null;

		// Token: 0x040002D8 RID: 728
		public ReflectionMethodsCache.RaycastAllCallback raycast3DAll = null;

		// Token: 0x040002D9 RID: 729
		public ReflectionMethodsCache.Raycast2DCallback raycast2D = null;

		// Token: 0x040002DA RID: 730
		public ReflectionMethodsCache.GetRayIntersectionAllCallback getRayIntersectionAll = null;

		// Token: 0x040002DB RID: 731
		public ReflectionMethodsCache.GetRayIntersectionAllNonAllocCallback getRayIntersectionAllNonAlloc = null;

		// Token: 0x040002DC RID: 732
		public ReflectionMethodsCache.GetRaycastNonAllocCallback getRaycastNonAlloc = null;

		// Token: 0x040002DD RID: 733
		private static ReflectionMethodsCache s_ReflectionMethodsCache = null;

		// Token: 0x020000A4 RID: 164
		// (Invoke) Token: 0x06000608 RID: 1544
		public delegate bool Raycast3DCallback(Ray r, out RaycastHit hit, float f, int i);

		// Token: 0x020000A5 RID: 165
		// (Invoke) Token: 0x0600060C RID: 1548
		public delegate RaycastHit2D Raycast2DCallback(Vector2 p1, Vector2 p2, float f, int i);

		// Token: 0x020000A6 RID: 166
		// (Invoke) Token: 0x06000610 RID: 1552
		public delegate RaycastHit[] RaycastAllCallback(Ray r, float f, int i);

		// Token: 0x020000A7 RID: 167
		// (Invoke) Token: 0x06000614 RID: 1556
		public delegate RaycastHit2D[] GetRayIntersectionAllCallback(Ray r, float f, int i);

		// Token: 0x020000A8 RID: 168
		// (Invoke) Token: 0x06000618 RID: 1560
		public delegate int GetRayIntersectionAllNonAllocCallback(Ray r, RaycastHit2D[] results, float f, int i);

		// Token: 0x020000A9 RID: 169
		// (Invoke) Token: 0x0600061C RID: 1564
		public delegate int GetRaycastNonAllocCallback(Ray r, RaycastHit[] results, float f, int i);
	}
}
