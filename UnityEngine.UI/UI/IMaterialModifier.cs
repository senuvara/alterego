﻿using System;

namespace UnityEngine.UI
{
	// Token: 0x0200009F RID: 159
	public interface IMaterialModifier
	{
		// Token: 0x060005E6 RID: 1510
		Material GetModifiedMaterial(Material baseMaterial);
	}
}
