﻿using System;
using UnityEngine.Serialization;

namespace UnityEngine.UI
{
	// Token: 0x02000068 RID: 104
	[AddComponentMenu("UI/Raw Image", 12)]
	public class RawImage : MaskableGraphic
	{
		// Token: 0x06000393 RID: 915 RVA: 0x000151ED File Offset: 0x000135ED
		protected RawImage()
		{
			base.useLegacyMeshGeneration = false;
		}

		// Token: 0x170000F6 RID: 246
		// (get) Token: 0x06000394 RID: 916 RVA: 0x0001521C File Offset: 0x0001361C
		public override Texture mainTexture
		{
			get
			{
				Texture result;
				if (this.m_Texture == null)
				{
					if (this.material != null && this.material.mainTexture != null)
					{
						result = this.material.mainTexture;
					}
					else
					{
						result = Graphic.s_WhiteTexture;
					}
				}
				else
				{
					result = this.m_Texture;
				}
				return result;
			}
		}

		// Token: 0x170000F7 RID: 247
		// (get) Token: 0x06000395 RID: 917 RVA: 0x00015290 File Offset: 0x00013690
		// (set) Token: 0x06000396 RID: 918 RVA: 0x000152AB File Offset: 0x000136AB
		public Texture texture
		{
			get
			{
				return this.m_Texture;
			}
			set
			{
				if (!(this.m_Texture == value))
				{
					this.m_Texture = value;
					this.SetVerticesDirty();
					this.SetMaterialDirty();
				}
			}
		}

		// Token: 0x170000F8 RID: 248
		// (get) Token: 0x06000397 RID: 919 RVA: 0x000152D8 File Offset: 0x000136D8
		// (set) Token: 0x06000398 RID: 920 RVA: 0x000152F3 File Offset: 0x000136F3
		public Rect uvRect
		{
			get
			{
				return this.m_UVRect;
			}
			set
			{
				if (!(this.m_UVRect == value))
				{
					this.m_UVRect = value;
					this.SetVerticesDirty();
				}
			}
		}

		// Token: 0x06000399 RID: 921 RVA: 0x0001531C File Offset: 0x0001371C
		public override void SetNativeSize()
		{
			Texture mainTexture = this.mainTexture;
			if (mainTexture != null)
			{
				int num = Mathf.RoundToInt((float)mainTexture.width * this.uvRect.width);
				int num2 = Mathf.RoundToInt((float)mainTexture.height * this.uvRect.height);
				base.rectTransform.anchorMax = base.rectTransform.anchorMin;
				base.rectTransform.sizeDelta = new Vector2((float)num, (float)num2);
			}
		}

		// Token: 0x0600039A RID: 922 RVA: 0x000153A4 File Offset: 0x000137A4
		protected override void OnPopulateMesh(VertexHelper vh)
		{
			Texture mainTexture = this.mainTexture;
			vh.Clear();
			if (mainTexture != null)
			{
				Rect pixelAdjustedRect = base.GetPixelAdjustedRect();
				Vector4 vector = new Vector4(pixelAdjustedRect.x, pixelAdjustedRect.y, pixelAdjustedRect.x + pixelAdjustedRect.width, pixelAdjustedRect.y + pixelAdjustedRect.height);
				float num = (float)mainTexture.width * mainTexture.texelSize.x;
				float num2 = (float)mainTexture.height * mainTexture.texelSize.y;
				Color color = this.color;
				vh.AddVert(new Vector3(vector.x, vector.y), color, new Vector2(this.m_UVRect.xMin * num, this.m_UVRect.yMin * num2));
				vh.AddVert(new Vector3(vector.x, vector.w), color, new Vector2(this.m_UVRect.xMin * num, this.m_UVRect.yMax * num2));
				vh.AddVert(new Vector3(vector.z, vector.w), color, new Vector2(this.m_UVRect.xMax * num, this.m_UVRect.yMax * num2));
				vh.AddVert(new Vector3(vector.z, vector.y), color, new Vector2(this.m_UVRect.xMax * num, this.m_UVRect.yMin * num2));
				vh.AddTriangle(0, 1, 2);
				vh.AddTriangle(2, 3, 0);
			}
		}

		// Token: 0x040001C9 RID: 457
		[FormerlySerializedAs("m_Tex")]
		[SerializeField]
		private Texture m_Texture;

		// Token: 0x040001CA RID: 458
		[SerializeField]
		private Rect m_UVRect = new Rect(0f, 0f, 1f, 1f);
	}
}
