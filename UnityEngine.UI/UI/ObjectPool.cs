﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine.Events;

namespace UnityEngine.UI
{
	// Token: 0x020000A2 RID: 162
	internal class ObjectPool<T> where T : new()
	{
		// Token: 0x060005FD RID: 1533 RVA: 0x0001E52B File Offset: 0x0001C92B
		public ObjectPool(UnityAction<T> actionOnGet, UnityAction<T> actionOnRelease)
		{
			this.m_ActionOnGet = actionOnGet;
			this.m_ActionOnRelease = actionOnRelease;
		}

		// Token: 0x1700019B RID: 411
		// (get) Token: 0x060005FE RID: 1534 RVA: 0x0001E550 File Offset: 0x0001C950
		// (set) Token: 0x060005FF RID: 1535 RVA: 0x0001E56A File Offset: 0x0001C96A
		public int countAll
		{
			[CompilerGenerated]
			get
			{
				return this.<countAll>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<countAll>k__BackingField = value;
			}
		}

		// Token: 0x1700019C RID: 412
		// (get) Token: 0x06000600 RID: 1536 RVA: 0x0001E574 File Offset: 0x0001C974
		public int countActive
		{
			get
			{
				return this.countAll - this.countInactive;
			}
		}

		// Token: 0x1700019D RID: 413
		// (get) Token: 0x06000601 RID: 1537 RVA: 0x0001E598 File Offset: 0x0001C998
		public int countInactive
		{
			get
			{
				return this.m_Stack.Count;
			}
		}

		// Token: 0x06000602 RID: 1538 RVA: 0x0001E5B8 File Offset: 0x0001C9B8
		public T Get()
		{
			T t;
			if (this.m_Stack.Count == 0)
			{
				t = Activator.CreateInstance<T>();
				this.countAll++;
			}
			else
			{
				t = this.m_Stack.Pop();
			}
			if (this.m_ActionOnGet != null)
			{
				this.m_ActionOnGet(t);
			}
			return t;
		}

		// Token: 0x06000603 RID: 1539 RVA: 0x0001E620 File Offset: 0x0001CA20
		public void Release(T element)
		{
			if (this.m_Stack.Count > 0 && object.ReferenceEquals(this.m_Stack.Peek(), element))
			{
				Debug.LogError("Internal error. Trying to destroy object that is already released to pool.");
			}
			if (this.m_ActionOnRelease != null)
			{
				this.m_ActionOnRelease(element);
			}
			this.m_Stack.Push(element);
		}

		// Token: 0x040002D3 RID: 723
		private readonly Stack<T> m_Stack = new Stack<T>();

		// Token: 0x040002D4 RID: 724
		private readonly UnityAction<T> m_ActionOnGet;

		// Token: 0x040002D5 RID: 725
		private readonly UnityAction<T> m_ActionOnRelease;

		// Token: 0x040002D6 RID: 726
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private int <countAll>k__BackingField;
	}
}
