﻿using System;

namespace UnityEngine.UI
{
	// Token: 0x020000AF RID: 175
	public interface IMeshModifier
	{
		// Token: 0x0600063E RID: 1598
		[Obsolete("use IMeshModifier.ModifyMesh (VertexHelper verts) instead", false)]
		void ModifyMesh(Mesh mesh);

		// Token: 0x0600063F RID: 1599
		void ModifyMesh(VertexHelper verts);
	}
}
