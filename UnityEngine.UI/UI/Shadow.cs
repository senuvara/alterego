﻿using System;
using System.Collections.Generic;

namespace UnityEngine.UI
{
	// Token: 0x020000B2 RID: 178
	[AddComponentMenu("UI/Effects/Shadow", 14)]
	public class Shadow : BaseMeshEffect
	{
		// Token: 0x06000644 RID: 1604 RVA: 0x0001F2B8 File Offset: 0x0001D6B8
		protected Shadow()
		{
		}

		// Token: 0x170001A2 RID: 418
		// (get) Token: 0x06000645 RID: 1605 RVA: 0x0001F308 File Offset: 0x0001D708
		// (set) Token: 0x06000646 RID: 1606 RVA: 0x0001F323 File Offset: 0x0001D723
		public Color effectColor
		{
			get
			{
				return this.m_EffectColor;
			}
			set
			{
				this.m_EffectColor = value;
				if (base.graphic != null)
				{
					base.graphic.SetVerticesDirty();
				}
			}
		}

		// Token: 0x170001A3 RID: 419
		// (get) Token: 0x06000647 RID: 1607 RVA: 0x0001F34C File Offset: 0x0001D74C
		// (set) Token: 0x06000648 RID: 1608 RVA: 0x0001F368 File Offset: 0x0001D768
		public Vector2 effectDistance
		{
			get
			{
				return this.m_EffectDistance;
			}
			set
			{
				if (value.x > 600f)
				{
					value.x = 600f;
				}
				if (value.x < -600f)
				{
					value.x = -600f;
				}
				if (value.y > 600f)
				{
					value.y = 600f;
				}
				if (value.y < -600f)
				{
					value.y = -600f;
				}
				if (!(this.m_EffectDistance == value))
				{
					this.m_EffectDistance = value;
					if (base.graphic != null)
					{
						base.graphic.SetVerticesDirty();
					}
				}
			}
		}

		// Token: 0x170001A4 RID: 420
		// (get) Token: 0x06000649 RID: 1609 RVA: 0x0001F424 File Offset: 0x0001D824
		// (set) Token: 0x0600064A RID: 1610 RVA: 0x0001F43F File Offset: 0x0001D83F
		public bool useGraphicAlpha
		{
			get
			{
				return this.m_UseGraphicAlpha;
			}
			set
			{
				this.m_UseGraphicAlpha = value;
				if (base.graphic != null)
				{
					base.graphic.SetVerticesDirty();
				}
			}
		}

		// Token: 0x0600064B RID: 1611 RVA: 0x0001F468 File Offset: 0x0001D868
		protected void ApplyShadowZeroAlloc(List<UIVertex> verts, Color32 color, int start, int end, float x, float y)
		{
			int num = verts.Count + end - start;
			if (verts.Capacity < num)
			{
				verts.Capacity = num;
			}
			for (int i = start; i < end; i++)
			{
				UIVertex uivertex = verts[i];
				verts.Add(uivertex);
				Vector3 position = uivertex.position;
				position.x += x;
				position.y += y;
				uivertex.position = position;
				Color32 color2 = color;
				if (this.m_UseGraphicAlpha)
				{
					color2.a = color2.a * verts[i].color.a / byte.MaxValue;
				}
				uivertex.color = color2;
				verts[i] = uivertex;
			}
		}

		// Token: 0x0600064C RID: 1612 RVA: 0x0001F533 File Offset: 0x0001D933
		protected void ApplyShadow(List<UIVertex> verts, Color32 color, int start, int end, float x, float y)
		{
			this.ApplyShadowZeroAlloc(verts, color, start, end, x, y);
		}

		// Token: 0x0600064D RID: 1613 RVA: 0x0001F548 File Offset: 0x0001D948
		public override void ModifyMesh(VertexHelper vh)
		{
			if (this.IsActive())
			{
				List<UIVertex> list = ListPool<UIVertex>.Get();
				vh.GetUIVertexStream(list);
				this.ApplyShadow(list, this.effectColor, 0, list.Count, this.effectDistance.x, this.effectDistance.y);
				vh.Clear();
				vh.AddUIVertexTriangleStream(list);
				ListPool<UIVertex>.Release(list);
			}
		}

		// Token: 0x040002EB RID: 747
		[SerializeField]
		private Color m_EffectColor = new Color(0f, 0f, 0f, 0.5f);

		// Token: 0x040002EC RID: 748
		[SerializeField]
		private Vector2 m_EffectDistance = new Vector2(1f, -1f);

		// Token: 0x040002ED RID: 749
		[SerializeField]
		private bool m_UseGraphicAlpha = true;

		// Token: 0x040002EE RID: 750
		private const float kMaxEffectDistance = 600f;
	}
}
