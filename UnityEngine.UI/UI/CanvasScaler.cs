﻿using System;
using UnityEngine.EventSystems;

namespace UnityEngine.UI
{
	// Token: 0x02000089 RID: 137
	[RequireComponent(typeof(Canvas))]
	[ExecuteAlways]
	[AddComponentMenu("Layout/Canvas Scaler", 101)]
	public class CanvasScaler : UIBehaviour
	{
		// Token: 0x0600051B RID: 1307 RVA: 0x0001B6B4 File Offset: 0x00019AB4
		protected CanvasScaler()
		{
		}

		// Token: 0x17000163 RID: 355
		// (get) Token: 0x0600051C RID: 1308 RVA: 0x0001B74C File Offset: 0x00019B4C
		// (set) Token: 0x0600051D RID: 1309 RVA: 0x0001B767 File Offset: 0x00019B67
		public CanvasScaler.ScaleMode uiScaleMode
		{
			get
			{
				return this.m_UiScaleMode;
			}
			set
			{
				this.m_UiScaleMode = value;
			}
		}

		// Token: 0x17000164 RID: 356
		// (get) Token: 0x0600051E RID: 1310 RVA: 0x0001B774 File Offset: 0x00019B74
		// (set) Token: 0x0600051F RID: 1311 RVA: 0x0001B78F File Offset: 0x00019B8F
		public float referencePixelsPerUnit
		{
			get
			{
				return this.m_ReferencePixelsPerUnit;
			}
			set
			{
				this.m_ReferencePixelsPerUnit = value;
			}
		}

		// Token: 0x17000165 RID: 357
		// (get) Token: 0x06000520 RID: 1312 RVA: 0x0001B79C File Offset: 0x00019B9C
		// (set) Token: 0x06000521 RID: 1313 RVA: 0x0001B7B7 File Offset: 0x00019BB7
		public float scaleFactor
		{
			get
			{
				return this.m_ScaleFactor;
			}
			set
			{
				this.m_ScaleFactor = Mathf.Max(0.01f, value);
			}
		}

		// Token: 0x17000166 RID: 358
		// (get) Token: 0x06000522 RID: 1314 RVA: 0x0001B7CC File Offset: 0x00019BCC
		// (set) Token: 0x06000523 RID: 1315 RVA: 0x0001B7E8 File Offset: 0x00019BE8
		public Vector2 referenceResolution
		{
			get
			{
				return this.m_ReferenceResolution;
			}
			set
			{
				this.m_ReferenceResolution = value;
				if (this.m_ReferenceResolution.x > -1E-05f && this.m_ReferenceResolution.x < 1E-05f)
				{
					this.m_ReferenceResolution.x = 1E-05f * Mathf.Sign(this.m_ReferenceResolution.x);
				}
				if (this.m_ReferenceResolution.y > -1E-05f && this.m_ReferenceResolution.y < 1E-05f)
				{
					this.m_ReferenceResolution.y = 1E-05f * Mathf.Sign(this.m_ReferenceResolution.y);
				}
			}
		}

		// Token: 0x17000167 RID: 359
		// (get) Token: 0x06000524 RID: 1316 RVA: 0x0001B894 File Offset: 0x00019C94
		// (set) Token: 0x06000525 RID: 1317 RVA: 0x0001B8AF File Offset: 0x00019CAF
		public CanvasScaler.ScreenMatchMode screenMatchMode
		{
			get
			{
				return this.m_ScreenMatchMode;
			}
			set
			{
				this.m_ScreenMatchMode = value;
			}
		}

		// Token: 0x17000168 RID: 360
		// (get) Token: 0x06000526 RID: 1318 RVA: 0x0001B8BC File Offset: 0x00019CBC
		// (set) Token: 0x06000527 RID: 1319 RVA: 0x0001B8D7 File Offset: 0x00019CD7
		public float matchWidthOrHeight
		{
			get
			{
				return this.m_MatchWidthOrHeight;
			}
			set
			{
				this.m_MatchWidthOrHeight = value;
			}
		}

		// Token: 0x17000169 RID: 361
		// (get) Token: 0x06000528 RID: 1320 RVA: 0x0001B8E4 File Offset: 0x00019CE4
		// (set) Token: 0x06000529 RID: 1321 RVA: 0x0001B8FF File Offset: 0x00019CFF
		public CanvasScaler.Unit physicalUnit
		{
			get
			{
				return this.m_PhysicalUnit;
			}
			set
			{
				this.m_PhysicalUnit = value;
			}
		}

		// Token: 0x1700016A RID: 362
		// (get) Token: 0x0600052A RID: 1322 RVA: 0x0001B90C File Offset: 0x00019D0C
		// (set) Token: 0x0600052B RID: 1323 RVA: 0x0001B927 File Offset: 0x00019D27
		public float fallbackScreenDPI
		{
			get
			{
				return this.m_FallbackScreenDPI;
			}
			set
			{
				this.m_FallbackScreenDPI = value;
			}
		}

		// Token: 0x1700016B RID: 363
		// (get) Token: 0x0600052C RID: 1324 RVA: 0x0001B934 File Offset: 0x00019D34
		// (set) Token: 0x0600052D RID: 1325 RVA: 0x0001B94F File Offset: 0x00019D4F
		public float defaultSpriteDPI
		{
			get
			{
				return this.m_DefaultSpriteDPI;
			}
			set
			{
				this.m_DefaultSpriteDPI = Mathf.Max(1f, value);
			}
		}

		// Token: 0x1700016C RID: 364
		// (get) Token: 0x0600052E RID: 1326 RVA: 0x0001B964 File Offset: 0x00019D64
		// (set) Token: 0x0600052F RID: 1327 RVA: 0x0001B97F File Offset: 0x00019D7F
		public float dynamicPixelsPerUnit
		{
			get
			{
				return this.m_DynamicPixelsPerUnit;
			}
			set
			{
				this.m_DynamicPixelsPerUnit = value;
			}
		}

		// Token: 0x06000530 RID: 1328 RVA: 0x0001B989 File Offset: 0x00019D89
		protected override void OnEnable()
		{
			base.OnEnable();
			this.m_Canvas = base.GetComponent<Canvas>();
			this.Handle();
		}

		// Token: 0x06000531 RID: 1329 RVA: 0x0001B9A4 File Offset: 0x00019DA4
		protected override void OnDisable()
		{
			this.SetScaleFactor(1f);
			this.SetReferencePixelsPerUnit(100f);
			base.OnDisable();
		}

		// Token: 0x06000532 RID: 1330 RVA: 0x0001B9C3 File Offset: 0x00019DC3
		protected virtual void Update()
		{
			this.Handle();
		}

		// Token: 0x06000533 RID: 1331 RVA: 0x0001B9CC File Offset: 0x00019DCC
		protected virtual void Handle()
		{
			if (!(this.m_Canvas == null) && this.m_Canvas.isRootCanvas)
			{
				if (this.m_Canvas.renderMode == RenderMode.WorldSpace)
				{
					this.HandleWorldCanvas();
				}
				else
				{
					CanvasScaler.ScaleMode uiScaleMode = this.m_UiScaleMode;
					if (uiScaleMode != CanvasScaler.ScaleMode.ConstantPixelSize)
					{
						if (uiScaleMode != CanvasScaler.ScaleMode.ScaleWithScreenSize)
						{
							if (uiScaleMode == CanvasScaler.ScaleMode.ConstantPhysicalSize)
							{
								this.HandleConstantPhysicalSize();
							}
						}
						else
						{
							this.HandleScaleWithScreenSize();
						}
					}
					else
					{
						this.HandleConstantPixelSize();
					}
				}
			}
		}

		// Token: 0x06000534 RID: 1332 RVA: 0x0001BA5E File Offset: 0x00019E5E
		protected virtual void HandleWorldCanvas()
		{
			this.SetScaleFactor(this.m_DynamicPixelsPerUnit);
			this.SetReferencePixelsPerUnit(this.m_ReferencePixelsPerUnit);
		}

		// Token: 0x06000535 RID: 1333 RVA: 0x0001BA79 File Offset: 0x00019E79
		protected virtual void HandleConstantPixelSize()
		{
			this.SetScaleFactor(this.m_ScaleFactor);
			this.SetReferencePixelsPerUnit(this.m_ReferencePixelsPerUnit);
		}

		// Token: 0x06000536 RID: 1334 RVA: 0x0001BA94 File Offset: 0x00019E94
		protected virtual void HandleScaleWithScreenSize()
		{
			Vector2 vector = new Vector2((float)Screen.width, (float)Screen.height);
			int targetDisplay = this.m_Canvas.targetDisplay;
			if (targetDisplay > 0 && targetDisplay < Display.displays.Length)
			{
				Display display = Display.displays[targetDisplay];
				vector = new Vector2((float)display.renderingWidth, (float)display.renderingHeight);
			}
			float scaleFactor = 0f;
			CanvasScaler.ScreenMatchMode screenMatchMode = this.m_ScreenMatchMode;
			if (screenMatchMode != CanvasScaler.ScreenMatchMode.MatchWidthOrHeight)
			{
				if (screenMatchMode != CanvasScaler.ScreenMatchMode.Expand)
				{
					if (screenMatchMode == CanvasScaler.ScreenMatchMode.Shrink)
					{
						scaleFactor = Mathf.Max(vector.x / this.m_ReferenceResolution.x, vector.y / this.m_ReferenceResolution.y);
					}
				}
				else
				{
					scaleFactor = Mathf.Min(vector.x / this.m_ReferenceResolution.x, vector.y / this.m_ReferenceResolution.y);
				}
			}
			else
			{
				float a = Mathf.Log(vector.x / this.m_ReferenceResolution.x, 2f);
				float b = Mathf.Log(vector.y / this.m_ReferenceResolution.y, 2f);
				float p = Mathf.Lerp(a, b, this.m_MatchWidthOrHeight);
				scaleFactor = Mathf.Pow(2f, p);
			}
			this.SetScaleFactor(scaleFactor);
			this.SetReferencePixelsPerUnit(this.m_ReferencePixelsPerUnit);
		}

		// Token: 0x06000537 RID: 1335 RVA: 0x0001BBF8 File Offset: 0x00019FF8
		protected virtual void HandleConstantPhysicalSize()
		{
			float dpi = Screen.dpi;
			float num = (dpi != 0f) ? dpi : this.m_FallbackScreenDPI;
			float num2 = 1f;
			switch (this.m_PhysicalUnit)
			{
			case CanvasScaler.Unit.Centimeters:
				num2 = 2.54f;
				break;
			case CanvasScaler.Unit.Millimeters:
				num2 = 25.4f;
				break;
			case CanvasScaler.Unit.Inches:
				num2 = 1f;
				break;
			case CanvasScaler.Unit.Points:
				num2 = 72f;
				break;
			case CanvasScaler.Unit.Picas:
				num2 = 6f;
				break;
			}
			this.SetScaleFactor(num / num2);
			this.SetReferencePixelsPerUnit(this.m_ReferencePixelsPerUnit * num2 / this.m_DefaultSpriteDPI);
		}

		// Token: 0x06000538 RID: 1336 RVA: 0x0001BCA5 File Offset: 0x0001A0A5
		protected void SetScaleFactor(float scaleFactor)
		{
			if (scaleFactor != this.m_PrevScaleFactor)
			{
				this.m_Canvas.scaleFactor = scaleFactor;
				this.m_PrevScaleFactor = scaleFactor;
			}
		}

		// Token: 0x06000539 RID: 1337 RVA: 0x0001BCCC File Offset: 0x0001A0CC
		protected void SetReferencePixelsPerUnit(float referencePixelsPerUnit)
		{
			if (referencePixelsPerUnit != this.m_PrevReferencePixelsPerUnit)
			{
				this.m_Canvas.referencePixelsPerUnit = referencePixelsPerUnit;
				this.m_PrevReferencePixelsPerUnit = referencePixelsPerUnit;
			}
		}

		// Token: 0x04000273 RID: 627
		[Tooltip("Determines how UI elements in the Canvas are scaled.")]
		[SerializeField]
		private CanvasScaler.ScaleMode m_UiScaleMode = CanvasScaler.ScaleMode.ConstantPixelSize;

		// Token: 0x04000274 RID: 628
		[Tooltip("If a sprite has this 'Pixels Per Unit' setting, then one pixel in the sprite will cover one unit in the UI.")]
		[SerializeField]
		protected float m_ReferencePixelsPerUnit = 100f;

		// Token: 0x04000275 RID: 629
		[Tooltip("Scales all UI elements in the Canvas by this factor.")]
		[SerializeField]
		protected float m_ScaleFactor = 1f;

		// Token: 0x04000276 RID: 630
		[Tooltip("The resolution the UI layout is designed for. If the screen resolution is larger, the UI will be scaled up, and if it's smaller, the UI will be scaled down. This is done in accordance with the Screen Match Mode.")]
		[SerializeField]
		protected Vector2 m_ReferenceResolution = new Vector2(800f, 600f);

		// Token: 0x04000277 RID: 631
		[Tooltip("A mode used to scale the canvas area if the aspect ratio of the current resolution doesn't fit the reference resolution.")]
		[SerializeField]
		protected CanvasScaler.ScreenMatchMode m_ScreenMatchMode = CanvasScaler.ScreenMatchMode.MatchWidthOrHeight;

		// Token: 0x04000278 RID: 632
		[Tooltip("Determines if the scaling is using the width or height as reference, or a mix in between.")]
		[Range(0f, 1f)]
		[SerializeField]
		protected float m_MatchWidthOrHeight = 0f;

		// Token: 0x04000279 RID: 633
		private const float kLogBase = 2f;

		// Token: 0x0400027A RID: 634
		[Tooltip("The physical unit to specify positions and sizes in.")]
		[SerializeField]
		protected CanvasScaler.Unit m_PhysicalUnit = CanvasScaler.Unit.Points;

		// Token: 0x0400027B RID: 635
		[Tooltip("The DPI to assume if the screen DPI is not known.")]
		[SerializeField]
		protected float m_FallbackScreenDPI = 96f;

		// Token: 0x0400027C RID: 636
		[Tooltip("The pixels per inch to use for sprites that have a 'Pixels Per Unit' setting that matches the 'Reference Pixels Per Unit' setting.")]
		[SerializeField]
		protected float m_DefaultSpriteDPI = 96f;

		// Token: 0x0400027D RID: 637
		[Tooltip("The amount of pixels per unit to use for dynamically created bitmaps in the UI, such as Text.")]
		[SerializeField]
		protected float m_DynamicPixelsPerUnit = 1f;

		// Token: 0x0400027E RID: 638
		private Canvas m_Canvas;

		// Token: 0x0400027F RID: 639
		[NonSerialized]
		private float m_PrevScaleFactor = 1f;

		// Token: 0x04000280 RID: 640
		[NonSerialized]
		private float m_PrevReferencePixelsPerUnit = 100f;

		// Token: 0x0200008A RID: 138
		public enum ScaleMode
		{
			// Token: 0x04000282 RID: 642
			ConstantPixelSize,
			// Token: 0x04000283 RID: 643
			ScaleWithScreenSize,
			// Token: 0x04000284 RID: 644
			ConstantPhysicalSize
		}

		// Token: 0x0200008B RID: 139
		public enum ScreenMatchMode
		{
			// Token: 0x04000286 RID: 646
			MatchWidthOrHeight,
			// Token: 0x04000287 RID: 647
			Expand,
			// Token: 0x04000288 RID: 648
			Shrink
		}

		// Token: 0x0200008C RID: 140
		public enum Unit
		{
			// Token: 0x0400028A RID: 650
			Centimeters,
			// Token: 0x0400028B RID: 651
			Millimeters,
			// Token: 0x0400028C RID: 652
			Inches,
			// Token: 0x0400028D RID: 653
			Points,
			// Token: 0x0400028E RID: 654
			Picas
		}
	}
}
