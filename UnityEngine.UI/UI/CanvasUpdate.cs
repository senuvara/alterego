﻿using System;

namespace UnityEngine.UI
{
	// Token: 0x0200003C RID: 60
	public enum CanvasUpdate
	{
		// Token: 0x040000C6 RID: 198
		Prelayout,
		// Token: 0x040000C7 RID: 199
		Layout,
		// Token: 0x040000C8 RID: 200
		PostLayout,
		// Token: 0x040000C9 RID: 201
		PreRender,
		// Token: 0x040000CA RID: 202
		LatePreRender,
		// Token: 0x040000CB RID: 203
		MaxUpdateValue
	}
}
