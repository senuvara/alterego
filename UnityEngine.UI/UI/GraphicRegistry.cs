﻿using System;
using System.Collections.Generic;
using UnityEngine.UI.Collections;

namespace UnityEngine.UI
{
	// Token: 0x0200004C RID: 76
	public class GraphicRegistry
	{
		// Token: 0x06000272 RID: 626 RVA: 0x0000CA90 File Offset: 0x0000AE90
		protected GraphicRegistry()
		{
			GC.KeepAlive(new Dictionary<Graphic, int>());
			GC.KeepAlive(new Dictionary<ICanvasElement, int>());
			GC.KeepAlive(new Dictionary<IClipper, int>());
		}

		// Token: 0x170000A3 RID: 163
		// (get) Token: 0x06000273 RID: 627 RVA: 0x0000CAC4 File Offset: 0x0000AEC4
		public static GraphicRegistry instance
		{
			get
			{
				if (GraphicRegistry.s_Instance == null)
				{
					GraphicRegistry.s_Instance = new GraphicRegistry();
				}
				return GraphicRegistry.s_Instance;
			}
		}

		// Token: 0x06000274 RID: 628 RVA: 0x0000CAF4 File Offset: 0x0000AEF4
		public static void RegisterGraphicForCanvas(Canvas c, Graphic graphic)
		{
			if (!(c == null))
			{
				IndexedSet<Graphic> indexedSet;
				GraphicRegistry.instance.m_Graphics.TryGetValue(c, out indexedSet);
				if (indexedSet != null)
				{
					indexedSet.AddUnique(graphic);
				}
				else
				{
					indexedSet = new IndexedSet<Graphic>();
					indexedSet.Add(graphic);
					GraphicRegistry.instance.m_Graphics.Add(c, indexedSet);
				}
			}
		}

		// Token: 0x06000275 RID: 629 RVA: 0x0000CB58 File Offset: 0x0000AF58
		public static void UnregisterGraphicForCanvas(Canvas c, Graphic graphic)
		{
			if (!(c == null))
			{
				IndexedSet<Graphic> indexedSet;
				if (GraphicRegistry.instance.m_Graphics.TryGetValue(c, out indexedSet))
				{
					indexedSet.Remove(graphic);
					if (indexedSet.Count == 0)
					{
						GraphicRegistry.instance.m_Graphics.Remove(c);
					}
				}
			}
		}

		// Token: 0x06000276 RID: 630 RVA: 0x0000CBB4 File Offset: 0x0000AFB4
		public static IList<Graphic> GetGraphicsForCanvas(Canvas canvas)
		{
			IndexedSet<Graphic> indexedSet;
			IList<Graphic> result;
			if (GraphicRegistry.instance.m_Graphics.TryGetValue(canvas, out indexedSet))
			{
				result = indexedSet;
			}
			else
			{
				result = GraphicRegistry.s_EmptyList;
			}
			return result;
		}

		// Token: 0x06000277 RID: 631 RVA: 0x0000CBEC File Offset: 0x0000AFEC
		// Note: this type is marked as 'beforefieldinit'.
		static GraphicRegistry()
		{
		}

		// Token: 0x0400012B RID: 299
		private static GraphicRegistry s_Instance;

		// Token: 0x0400012C RID: 300
		private readonly Dictionary<Canvas, IndexedSet<Graphic>> m_Graphics = new Dictionary<Canvas, IndexedSet<Graphic>>();

		// Token: 0x0400012D RID: 301
		private static readonly List<Graphic> s_EmptyList = new List<Graphic>();
	}
}
