﻿using System;

namespace UnityEngine.UI
{
	// Token: 0x02000084 RID: 132
	public interface IClipper
	{
		// Token: 0x06000503 RID: 1283
		void PerformClipping();
	}
}
