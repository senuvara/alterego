﻿using System;
using System.Collections.Generic;
using UnityEngine.EventSystems;

namespace UnityEngine.UI
{
	// Token: 0x02000069 RID: 105
	[AddComponentMenu("UI/Rect Mask 2D", 13)]
	[ExecuteAlways]
	[DisallowMultipleComponent]
	[RequireComponent(typeof(RectTransform))]
	public class RectMask2D : UIBehaviour, IClipper, ICanvasRaycastFilter
	{
		// Token: 0x0600039B RID: 923 RVA: 0x00015554 File Offset: 0x00013954
		protected RectMask2D()
		{
		}

		// Token: 0x170000F9 RID: 249
		// (get) Token: 0x0600039C RID: 924 RVA: 0x0001558C File Offset: 0x0001398C
		private Canvas Canvas
		{
			get
			{
				if (this.m_Canvas == null)
				{
					List<Canvas> list = ListPool<Canvas>.Get();
					base.gameObject.GetComponentsInParent<Canvas>(false, list);
					if (list.Count > 0)
					{
						this.m_Canvas = list[list.Count - 1];
					}
					else
					{
						this.m_Canvas = null;
					}
					ListPool<Canvas>.Release(list);
				}
				return this.m_Canvas;
			}
		}

		// Token: 0x170000FA RID: 250
		// (get) Token: 0x0600039D RID: 925 RVA: 0x00015600 File Offset: 0x00013A00
		public Rect canvasRect
		{
			get
			{
				return this.m_VertexClipper.GetCanvasRect(this.rectTransform, this.Canvas);
			}
		}

		// Token: 0x170000FB RID: 251
		// (get) Token: 0x0600039E RID: 926 RVA: 0x0001562C File Offset: 0x00013A2C
		public RectTransform rectTransform
		{
			get
			{
				RectTransform result;
				if ((result = this.m_RectTransform) == null)
				{
					result = (this.m_RectTransform = base.GetComponent<RectTransform>());
				}
				return result;
			}
		}

		// Token: 0x0600039F RID: 927 RVA: 0x0001565D File Offset: 0x00013A5D
		protected override void OnEnable()
		{
			base.OnEnable();
			this.m_ShouldRecalculateClipRects = true;
			ClipperRegistry.Register(this);
			MaskUtilities.Notify2DMaskStateChanged(this);
		}

		// Token: 0x060003A0 RID: 928 RVA: 0x00015679 File Offset: 0x00013A79
		protected override void OnDisable()
		{
			base.OnDisable();
			this.m_ClipTargets.Clear();
			this.m_Clippers.Clear();
			ClipperRegistry.Unregister(this);
			MaskUtilities.Notify2DMaskStateChanged(this);
		}

		// Token: 0x060003A1 RID: 929 RVA: 0x000156A4 File Offset: 0x00013AA4
		public virtual bool IsRaycastLocationValid(Vector2 sp, Camera eventCamera)
		{
			return !base.isActiveAndEnabled || RectTransformUtility.RectangleContainsScreenPoint(this.rectTransform, sp, eventCamera);
		}

		// Token: 0x170000FC RID: 252
		// (get) Token: 0x060003A2 RID: 930 RVA: 0x000156D8 File Offset: 0x00013AD8
		private Rect rootCanvasRect
		{
			get
			{
				this.rectTransform.GetWorldCorners(this.m_Corners);
				if (!object.ReferenceEquals(this.Canvas, null))
				{
					Canvas rootCanvas = this.Canvas.rootCanvas;
					for (int i = 0; i < 4; i++)
					{
						this.m_Corners[i] = rootCanvas.transform.InverseTransformPoint(this.m_Corners[i]);
					}
				}
				return new Rect(this.m_Corners[0].x, this.m_Corners[0].y, this.m_Corners[2].x - this.m_Corners[0].x, this.m_Corners[2].y - this.m_Corners[0].y);
			}
		}

		// Token: 0x060003A3 RID: 931 RVA: 0x000157CC File Offset: 0x00013BCC
		public virtual void PerformClipping()
		{
			if (!object.ReferenceEquals(this.Canvas, null))
			{
				if (this.m_ShouldRecalculateClipRects)
				{
					MaskUtilities.GetRectMasksForClip(this, this.m_Clippers);
					this.m_ShouldRecalculateClipRects = false;
				}
				bool flag = true;
				Rect rect = Clipping.FindCullAndClipWorldRect(this.m_Clippers, out flag);
				RenderMode renderMode = this.Canvas.rootCanvas.renderMode;
				bool flag2 = (renderMode == RenderMode.ScreenSpaceCamera || renderMode == RenderMode.ScreenSpaceOverlay) && !rect.Overlaps(this.rootCanvasRect, true);
				bool flag3 = rect != this.m_LastClipRectCanvasSpace;
				bool forceClip = this.m_ForceClip;
				foreach (IClippable clippable in this.m_ClipTargets)
				{
					if (flag3 || forceClip)
					{
						clippable.SetClipRect(rect, flag);
					}
					MaskableGraphic maskableGraphic = clippable as MaskableGraphic;
					if (!(maskableGraphic != null) || maskableGraphic.canvasRenderer.hasMoved || flag3)
					{
						clippable.Cull((!flag2) ? rect : Rect.zero, !flag2 && flag);
					}
				}
				this.m_LastClipRectCanvasSpace = rect;
				this.m_ForceClip = false;
			}
		}

		// Token: 0x060003A4 RID: 932 RVA: 0x00015938 File Offset: 0x00013D38
		public void AddClippable(IClippable clippable)
		{
			if (clippable != null)
			{
				this.m_ShouldRecalculateClipRects = true;
				if (!this.m_ClipTargets.Contains(clippable))
				{
					this.m_ClipTargets.Add(clippable);
				}
				this.m_ForceClip = true;
			}
		}

		// Token: 0x060003A5 RID: 933 RVA: 0x00015974 File Offset: 0x00013D74
		public void RemoveClippable(IClippable clippable)
		{
			if (clippable != null)
			{
				this.m_ShouldRecalculateClipRects = true;
				clippable.SetClipRect(default(Rect), false);
				this.m_ClipTargets.Remove(clippable);
				this.m_ForceClip = true;
			}
		}

		// Token: 0x060003A6 RID: 934 RVA: 0x000159B8 File Offset: 0x00013DB8
		protected override void OnTransformParentChanged()
		{
			base.OnTransformParentChanged();
			this.m_ShouldRecalculateClipRects = true;
		}

		// Token: 0x060003A7 RID: 935 RVA: 0x000159C8 File Offset: 0x00013DC8
		protected override void OnCanvasHierarchyChanged()
		{
			this.m_Canvas = null;
			base.OnCanvasHierarchyChanged();
			this.m_ShouldRecalculateClipRects = true;
		}

		// Token: 0x040001CB RID: 459
		[NonSerialized]
		private readonly RectangularVertexClipper m_VertexClipper = new RectangularVertexClipper();

		// Token: 0x040001CC RID: 460
		[NonSerialized]
		private RectTransform m_RectTransform;

		// Token: 0x040001CD RID: 461
		[NonSerialized]
		private HashSet<IClippable> m_ClipTargets = new HashSet<IClippable>();

		// Token: 0x040001CE RID: 462
		[NonSerialized]
		private bool m_ShouldRecalculateClipRects;

		// Token: 0x040001CF RID: 463
		[NonSerialized]
		private List<RectMask2D> m_Clippers = new List<RectMask2D>();

		// Token: 0x040001D0 RID: 464
		[NonSerialized]
		private Rect m_LastClipRectCanvasSpace;

		// Token: 0x040001D1 RID: 465
		[NonSerialized]
		private bool m_ForceClip;

		// Token: 0x040001D2 RID: 466
		[NonSerialized]
		private Canvas m_Canvas;

		// Token: 0x040001D3 RID: 467
		private Vector3[] m_Corners = new Vector3[4];
	}
}
