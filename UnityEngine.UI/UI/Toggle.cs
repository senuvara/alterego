﻿using System;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace UnityEngine.UI
{
	// Token: 0x0200007E RID: 126
	[AddComponentMenu("UI/Toggle", 31)]
	[RequireComponent(typeof(RectTransform))]
	public class Toggle : Selectable, IPointerClickHandler, ISubmitHandler, ICanvasElement, IEventSystemHandler
	{
		// Token: 0x060004DB RID: 1243 RVA: 0x0001AA1E File Offset: 0x00018E1E
		protected Toggle()
		{
		}

		// Token: 0x1700015A RID: 346
		// (get) Token: 0x060004DC RID: 1244 RVA: 0x0001AA3C File Offset: 0x00018E3C
		// (set) Token: 0x060004DD RID: 1245 RVA: 0x0001AA57 File Offset: 0x00018E57
		public ToggleGroup group
		{
			get
			{
				return this.m_Group;
			}
			set
			{
				this.m_Group = value;
				this.SetToggleGroup(this.m_Group, true);
				this.PlayEffect(true);
			}
		}

		// Token: 0x060004DE RID: 1246 RVA: 0x0001AA77 File Offset: 0x00018E77
		public virtual void Rebuild(CanvasUpdate executing)
		{
		}

		// Token: 0x060004DF RID: 1247 RVA: 0x0001AA7A File Offset: 0x00018E7A
		public virtual void LayoutComplete()
		{
		}

		// Token: 0x060004E0 RID: 1248 RVA: 0x0001AA7D File Offset: 0x00018E7D
		public virtual void GraphicUpdateComplete()
		{
		}

		// Token: 0x060004E1 RID: 1249 RVA: 0x0001AA80 File Offset: 0x00018E80
		protected override void OnEnable()
		{
			base.OnEnable();
			this.SetToggleGroup(this.m_Group, false);
			this.PlayEffect(true);
		}

		// Token: 0x060004E2 RID: 1250 RVA: 0x0001AA9D File Offset: 0x00018E9D
		protected override void OnDisable()
		{
			this.SetToggleGroup(null, false);
			base.OnDisable();
		}

		// Token: 0x060004E3 RID: 1251 RVA: 0x0001AAB0 File Offset: 0x00018EB0
		protected override void OnDidApplyAnimationProperties()
		{
			if (this.graphic != null)
			{
				bool flag = !Mathf.Approximately(this.graphic.canvasRenderer.GetColor().a, 0f);
				if (this.m_IsOn != flag)
				{
					this.m_IsOn = flag;
					this.Set(!flag);
				}
			}
			base.OnDidApplyAnimationProperties();
		}

		// Token: 0x060004E4 RID: 1252 RVA: 0x0001AB1C File Offset: 0x00018F1C
		private void SetToggleGroup(ToggleGroup newGroup, bool setMemberValue)
		{
			ToggleGroup group = this.m_Group;
			if (this.m_Group != null)
			{
				this.m_Group.UnregisterToggle(this);
			}
			if (setMemberValue)
			{
				this.m_Group = newGroup;
			}
			if (newGroup != null && this.IsActive())
			{
				newGroup.RegisterToggle(this);
			}
			if (newGroup != null && newGroup != group && this.isOn && this.IsActive())
			{
				newGroup.NotifyToggleOn(this);
			}
		}

		// Token: 0x1700015B RID: 347
		// (get) Token: 0x060004E5 RID: 1253 RVA: 0x0001ABB0 File Offset: 0x00018FB0
		// (set) Token: 0x060004E6 RID: 1254 RVA: 0x0001ABCB File Offset: 0x00018FCB
		public bool isOn
		{
			get
			{
				return this.m_IsOn;
			}
			set
			{
				this.Set(value);
			}
		}

		// Token: 0x060004E7 RID: 1255 RVA: 0x0001ABD5 File Offset: 0x00018FD5
		private void Set(bool value)
		{
			this.Set(value, true);
		}

		// Token: 0x060004E8 RID: 1256 RVA: 0x0001ABE0 File Offset: 0x00018FE0
		private void Set(bool value, bool sendCallback)
		{
			if (this.m_IsOn != value)
			{
				this.m_IsOn = value;
				if (this.m_Group != null && this.IsActive())
				{
					if (this.m_IsOn || (!this.m_Group.AnyTogglesOn() && !this.m_Group.allowSwitchOff))
					{
						this.m_IsOn = true;
						this.m_Group.NotifyToggleOn(this);
					}
				}
				this.PlayEffect(this.toggleTransition == Toggle.ToggleTransition.None);
				if (sendCallback)
				{
					UISystemProfilerApi.AddMarker("Toggle.value", this);
					this.onValueChanged.Invoke(this.m_IsOn);
				}
			}
		}

		// Token: 0x060004E9 RID: 1257 RVA: 0x0001AC98 File Offset: 0x00019098
		private void PlayEffect(bool instant)
		{
			if (!(this.graphic == null))
			{
				this.graphic.CrossFadeAlpha((!this.m_IsOn) ? 0f : 1f, (!instant) ? 0.1f : 0f, true);
			}
		}

		// Token: 0x060004EA RID: 1258 RVA: 0x0001ACF7 File Offset: 0x000190F7
		protected override void Start()
		{
			this.PlayEffect(true);
		}

		// Token: 0x060004EB RID: 1259 RVA: 0x0001AD01 File Offset: 0x00019101
		private void InternalToggle()
		{
			if (this.IsActive() && this.IsInteractable())
			{
				this.isOn = !this.isOn;
			}
		}

		// Token: 0x060004EC RID: 1260 RVA: 0x0001AD2E File Offset: 0x0001912E
		public virtual void OnPointerClick(PointerEventData eventData)
		{
			if (eventData.button == PointerEventData.InputButton.Left)
			{
				this.InternalToggle();
			}
		}

		// Token: 0x060004ED RID: 1261 RVA: 0x0001AD47 File Offset: 0x00019147
		public virtual void OnSubmit(BaseEventData eventData)
		{
			this.InternalToggle();
		}

		// Token: 0x060004EE RID: 1262 RVA: 0x0001AD50 File Offset: 0x00019150
		Transform ICanvasElement.get_transform()
		{
			return base.transform;
		}

		// Token: 0x04000258 RID: 600
		public Toggle.ToggleTransition toggleTransition = Toggle.ToggleTransition.Fade;

		// Token: 0x04000259 RID: 601
		public Graphic graphic;

		// Token: 0x0400025A RID: 602
		[SerializeField]
		private ToggleGroup m_Group;

		// Token: 0x0400025B RID: 603
		public Toggle.ToggleEvent onValueChanged = new Toggle.ToggleEvent();

		// Token: 0x0400025C RID: 604
		[Tooltip("Is the toggle currently on or off?")]
		[SerializeField]
		private bool m_IsOn;

		// Token: 0x0200007F RID: 127
		public enum ToggleTransition
		{
			// Token: 0x0400025E RID: 606
			None,
			// Token: 0x0400025F RID: 607
			Fade
		}

		// Token: 0x02000080 RID: 128
		[Serializable]
		public class ToggleEvent : UnityEvent<bool>
		{
			// Token: 0x060004EF RID: 1263 RVA: 0x0001AD58 File Offset: 0x00019158
			public ToggleEvent()
			{
			}
		}
	}
}
