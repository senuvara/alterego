﻿using System;

namespace UnityEngine.UI
{
	// Token: 0x0200003D RID: 61
	public interface ICanvasElement
	{
		// Token: 0x0600018D RID: 397
		void Rebuild(CanvasUpdate executing);

		// Token: 0x1700006B RID: 107
		// (get) Token: 0x0600018E RID: 398
		Transform transform { get; }

		// Token: 0x0600018F RID: 399
		void LayoutComplete();

		// Token: 0x06000190 RID: 400
		void GraphicUpdateComplete();

		// Token: 0x06000191 RID: 401
		bool IsDestroyed();
	}
}
