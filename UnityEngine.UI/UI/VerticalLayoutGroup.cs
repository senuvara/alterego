﻿using System;

namespace UnityEngine.UI
{
	// Token: 0x0200009E RID: 158
	[AddComponentMenu("Layout/Vertical Layout Group", 151)]
	public class VerticalLayoutGroup : HorizontalOrVerticalLayoutGroup
	{
		// Token: 0x060005E1 RID: 1505 RVA: 0x0001E13E File Offset: 0x0001C53E
		protected VerticalLayoutGroup()
		{
		}

		// Token: 0x060005E2 RID: 1506 RVA: 0x0001E147 File Offset: 0x0001C547
		public override void CalculateLayoutInputHorizontal()
		{
			base.CalculateLayoutInputHorizontal();
			base.CalcAlongAxis(0, true);
		}

		// Token: 0x060005E3 RID: 1507 RVA: 0x0001E158 File Offset: 0x0001C558
		public override void CalculateLayoutInputVertical()
		{
			base.CalcAlongAxis(1, true);
		}

		// Token: 0x060005E4 RID: 1508 RVA: 0x0001E163 File Offset: 0x0001C563
		public override void SetLayoutHorizontal()
		{
			base.SetChildrenAlongAxis(0, true);
		}

		// Token: 0x060005E5 RID: 1509 RVA: 0x0001E16E File Offset: 0x0001C56E
		public override void SetLayoutVertical()
		{
			base.SetChildrenAlongAxis(1, true);
		}
	}
}
