﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.Serialization;
using UnityEngine.UI.CoroutineTween;

namespace UnityEngine.UI
{
	// Token: 0x02000049 RID: 73
	[DisallowMultipleComponent]
	[RequireComponent(typeof(CanvasRenderer))]
	[RequireComponent(typeof(RectTransform))]
	[ExecuteAlways]
	public abstract class Graphic : UIBehaviour, ICanvasElement
	{
		// Token: 0x0600022C RID: 556 RVA: 0x0000B478 File Offset: 0x00009878
		protected Graphic()
		{
			if (this.m_ColorTweenRunner == null)
			{
				this.m_ColorTweenRunner = new TweenRunner<ColorTween>();
			}
			this.m_ColorTweenRunner.Init(this);
			this.useLegacyMeshGeneration = true;
		}

		// Token: 0x17000090 RID: 144
		// (get) Token: 0x0600022D RID: 557 RVA: 0x0000B4C8 File Offset: 0x000098C8
		public static Material defaultGraphicMaterial
		{
			get
			{
				if (Graphic.s_DefaultUI == null)
				{
					Graphic.s_DefaultUI = Canvas.GetDefaultCanvasMaterial();
				}
				return Graphic.s_DefaultUI;
			}
		}

		// Token: 0x17000091 RID: 145
		// (get) Token: 0x0600022E RID: 558 RVA: 0x0000B4FC File Offset: 0x000098FC
		// (set) Token: 0x0600022F RID: 559 RVA: 0x0000B517 File Offset: 0x00009917
		public virtual Color color
		{
			get
			{
				return this.m_Color;
			}
			set
			{
				if (SetPropertyUtility.SetColor(ref this.m_Color, value))
				{
					this.SetVerticesDirty();
				}
			}
		}

		// Token: 0x17000092 RID: 146
		// (get) Token: 0x06000230 RID: 560 RVA: 0x0000B534 File Offset: 0x00009934
		// (set) Token: 0x06000231 RID: 561 RVA: 0x0000B54F File Offset: 0x0000994F
		public virtual bool raycastTarget
		{
			get
			{
				return this.m_RaycastTarget;
			}
			set
			{
				this.m_RaycastTarget = value;
			}
		}

		// Token: 0x17000093 RID: 147
		// (get) Token: 0x06000232 RID: 562 RVA: 0x0000B55C File Offset: 0x0000995C
		// (set) Token: 0x06000233 RID: 563 RVA: 0x0000B576 File Offset: 0x00009976
		protected bool useLegacyMeshGeneration
		{
			[CompilerGenerated]
			get
			{
				return this.<useLegacyMeshGeneration>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<useLegacyMeshGeneration>k__BackingField = value;
			}
		}

		// Token: 0x06000234 RID: 564 RVA: 0x0000B57F File Offset: 0x0000997F
		public virtual void SetAllDirty()
		{
			this.SetLayoutDirty();
			this.SetVerticesDirty();
			this.SetMaterialDirty();
		}

		// Token: 0x06000235 RID: 565 RVA: 0x0000B594 File Offset: 0x00009994
		public virtual void SetLayoutDirty()
		{
			if (this.IsActive())
			{
				LayoutRebuilder.MarkLayoutForRebuild(this.rectTransform);
				if (this.m_OnDirtyLayoutCallback != null)
				{
					this.m_OnDirtyLayoutCallback();
				}
			}
		}

		// Token: 0x06000236 RID: 566 RVA: 0x0000B5C8 File Offset: 0x000099C8
		public virtual void SetVerticesDirty()
		{
			if (this.IsActive())
			{
				this.m_VertsDirty = true;
				CanvasUpdateRegistry.RegisterCanvasElementForGraphicRebuild(this);
				if (this.m_OnDirtyVertsCallback != null)
				{
					this.m_OnDirtyVertsCallback();
				}
			}
		}

		// Token: 0x06000237 RID: 567 RVA: 0x0000B5FE File Offset: 0x000099FE
		public virtual void SetMaterialDirty()
		{
			if (this.IsActive())
			{
				this.m_MaterialDirty = true;
				CanvasUpdateRegistry.RegisterCanvasElementForGraphicRebuild(this);
				if (this.m_OnDirtyMaterialCallback != null)
				{
					this.m_OnDirtyMaterialCallback();
				}
			}
		}

		// Token: 0x06000238 RID: 568 RVA: 0x0000B634 File Offset: 0x00009A34
		protected override void OnRectTransformDimensionsChange()
		{
			if (base.gameObject.activeInHierarchy)
			{
				if (CanvasUpdateRegistry.IsRebuildingLayout())
				{
					this.SetVerticesDirty();
				}
				else
				{
					this.SetVerticesDirty();
					this.SetLayoutDirty();
				}
			}
		}

		// Token: 0x06000239 RID: 569 RVA: 0x0000B66C File Offset: 0x00009A6C
		protected override void OnBeforeTransformParentChanged()
		{
			GraphicRegistry.UnregisterGraphicForCanvas(this.canvas, this);
			LayoutRebuilder.MarkLayoutForRebuild(this.rectTransform);
		}

		// Token: 0x0600023A RID: 570 RVA: 0x0000B686 File Offset: 0x00009A86
		protected override void OnTransformParentChanged()
		{
			base.OnTransformParentChanged();
			this.m_Canvas = null;
			if (this.IsActive())
			{
				this.CacheCanvas();
				GraphicRegistry.RegisterGraphicForCanvas(this.canvas, this);
				this.SetAllDirty();
			}
		}

		// Token: 0x17000094 RID: 148
		// (get) Token: 0x0600023B RID: 571 RVA: 0x0000B6C0 File Offset: 0x00009AC0
		public int depth
		{
			get
			{
				return this.canvasRenderer.absoluteDepth;
			}
		}

		// Token: 0x17000095 RID: 149
		// (get) Token: 0x0600023C RID: 572 RVA: 0x0000B6E0 File Offset: 0x00009AE0
		public RectTransform rectTransform
		{
			get
			{
				if (object.ReferenceEquals(this.m_RectTransform, null))
				{
					this.m_RectTransform = base.GetComponent<RectTransform>();
				}
				return this.m_RectTransform;
			}
		}

		// Token: 0x17000096 RID: 150
		// (get) Token: 0x0600023D RID: 573 RVA: 0x0000B71C File Offset: 0x00009B1C
		public Canvas canvas
		{
			get
			{
				if (this.m_Canvas == null)
				{
					this.CacheCanvas();
				}
				return this.m_Canvas;
			}
		}

		// Token: 0x0600023E RID: 574 RVA: 0x0000B750 File Offset: 0x00009B50
		private void CacheCanvas()
		{
			List<Canvas> list = ListPool<Canvas>.Get();
			base.gameObject.GetComponentsInParent<Canvas>(false, list);
			if (list.Count > 0)
			{
				for (int i = 0; i < list.Count; i++)
				{
					if (list[i].isActiveAndEnabled)
					{
						this.m_Canvas = list[i];
						break;
					}
				}
			}
			else
			{
				this.m_Canvas = null;
			}
			ListPool<Canvas>.Release(list);
		}

		// Token: 0x17000097 RID: 151
		// (get) Token: 0x0600023F RID: 575 RVA: 0x0000B7D0 File Offset: 0x00009BD0
		public CanvasRenderer canvasRenderer
		{
			get
			{
				if (object.ReferenceEquals(this.m_CanvasRenderer, null))
				{
					this.m_CanvasRenderer = base.GetComponent<CanvasRenderer>();
				}
				return this.m_CanvasRenderer;
			}
		}

		// Token: 0x17000098 RID: 152
		// (get) Token: 0x06000240 RID: 576 RVA: 0x0000B80C File Offset: 0x00009C0C
		public virtual Material defaultMaterial
		{
			get
			{
				return Graphic.defaultGraphicMaterial;
			}
		}

		// Token: 0x17000099 RID: 153
		// (get) Token: 0x06000241 RID: 577 RVA: 0x0000B828 File Offset: 0x00009C28
		// (set) Token: 0x06000242 RID: 578 RVA: 0x0000B85F File Offset: 0x00009C5F
		public virtual Material material
		{
			get
			{
				return (!(this.m_Material != null)) ? this.defaultMaterial : this.m_Material;
			}
			set
			{
				if (!(this.m_Material == value))
				{
					this.m_Material = value;
					this.SetMaterialDirty();
				}
			}
		}

		// Token: 0x1700009A RID: 154
		// (get) Token: 0x06000243 RID: 579 RVA: 0x0000B888 File Offset: 0x00009C88
		public virtual Material materialForRendering
		{
			get
			{
				List<Component> list = ListPool<Component>.Get();
				base.GetComponents(typeof(IMaterialModifier), list);
				Material material = this.material;
				for (int i = 0; i < list.Count; i++)
				{
					material = (list[i] as IMaterialModifier).GetModifiedMaterial(material);
				}
				ListPool<Component>.Release(list);
				return material;
			}
		}

		// Token: 0x1700009B RID: 155
		// (get) Token: 0x06000244 RID: 580 RVA: 0x0000B8EC File Offset: 0x00009CEC
		public virtual Texture mainTexture
		{
			get
			{
				return Graphic.s_WhiteTexture;
			}
		}

		// Token: 0x06000245 RID: 581 RVA: 0x0000B906 File Offset: 0x00009D06
		protected override void OnEnable()
		{
			base.OnEnable();
			this.CacheCanvas();
			GraphicRegistry.RegisterGraphicForCanvas(this.canvas, this);
			if (Graphic.s_WhiteTexture == null)
			{
				Graphic.s_WhiteTexture = Texture2D.whiteTexture;
			}
			this.SetAllDirty();
		}

		// Token: 0x06000246 RID: 582 RVA: 0x0000B944 File Offset: 0x00009D44
		protected override void OnDisable()
		{
			GraphicRegistry.UnregisterGraphicForCanvas(this.canvas, this);
			CanvasUpdateRegistry.UnRegisterCanvasElementForRebuild(this);
			if (this.canvasRenderer != null)
			{
				this.canvasRenderer.Clear();
			}
			LayoutRebuilder.MarkLayoutForRebuild(this.rectTransform);
			base.OnDisable();
		}

		// Token: 0x06000247 RID: 583 RVA: 0x0000B994 File Offset: 0x00009D94
		protected override void OnCanvasHierarchyChanged()
		{
			Canvas canvas = this.m_Canvas;
			this.m_Canvas = null;
			if (this.IsActive())
			{
				this.CacheCanvas();
				if (canvas != this.m_Canvas)
				{
					GraphicRegistry.UnregisterGraphicForCanvas(canvas, this);
					if (this.IsActive())
					{
						GraphicRegistry.RegisterGraphicForCanvas(this.canvas, this);
					}
				}
			}
		}

		// Token: 0x06000248 RID: 584 RVA: 0x0000B9F7 File Offset: 0x00009DF7
		public virtual void OnCullingChanged()
		{
			if (!this.canvasRenderer.cull && (this.m_VertsDirty || this.m_MaterialDirty))
			{
				CanvasUpdateRegistry.RegisterCanvasElementForGraphicRebuild(this);
			}
		}

		// Token: 0x06000249 RID: 585 RVA: 0x0000BA28 File Offset: 0x00009E28
		public virtual void Rebuild(CanvasUpdate update)
		{
			if (!this.canvasRenderer.cull)
			{
				if (update == CanvasUpdate.PreRender)
				{
					if (this.m_VertsDirty)
					{
						this.UpdateGeometry();
						this.m_VertsDirty = false;
					}
					if (this.m_MaterialDirty)
					{
						this.UpdateMaterial();
						this.m_MaterialDirty = false;
					}
				}
			}
		}

		// Token: 0x0600024A RID: 586 RVA: 0x0000BA90 File Offset: 0x00009E90
		public virtual void LayoutComplete()
		{
		}

		// Token: 0x0600024B RID: 587 RVA: 0x0000BA93 File Offset: 0x00009E93
		public virtual void GraphicUpdateComplete()
		{
		}

		// Token: 0x0600024C RID: 588 RVA: 0x0000BA98 File Offset: 0x00009E98
		protected virtual void UpdateMaterial()
		{
			if (this.IsActive())
			{
				this.canvasRenderer.materialCount = 1;
				this.canvasRenderer.SetMaterial(this.materialForRendering, 0);
				this.canvasRenderer.SetTexture(this.mainTexture);
			}
		}

		// Token: 0x0600024D RID: 589 RVA: 0x0000BAE5 File Offset: 0x00009EE5
		protected virtual void UpdateGeometry()
		{
			if (this.useLegacyMeshGeneration)
			{
				this.DoLegacyMeshGeneration();
			}
			else
			{
				this.DoMeshGeneration();
			}
		}

		// Token: 0x0600024E RID: 590 RVA: 0x0000BB04 File Offset: 0x00009F04
		private void DoMeshGeneration()
		{
			if (this.rectTransform != null && this.rectTransform.rect.width >= 0f && this.rectTransform.rect.height >= 0f)
			{
				this.OnPopulateMesh(Graphic.s_VertexHelper);
			}
			else
			{
				Graphic.s_VertexHelper.Clear();
			}
			List<Component> list = ListPool<Component>.Get();
			base.GetComponents(typeof(IMeshModifier), list);
			for (int i = 0; i < list.Count; i++)
			{
				((IMeshModifier)list[i]).ModifyMesh(Graphic.s_VertexHelper);
			}
			ListPool<Component>.Release(list);
			Graphic.s_VertexHelper.FillMesh(Graphic.workerMesh);
			this.canvasRenderer.SetMesh(Graphic.workerMesh);
		}

		// Token: 0x0600024F RID: 591 RVA: 0x0000BBE0 File Offset: 0x00009FE0
		private void DoLegacyMeshGeneration()
		{
			if (this.rectTransform != null && this.rectTransform.rect.width >= 0f && this.rectTransform.rect.height >= 0f)
			{
				this.OnPopulateMesh(Graphic.workerMesh);
			}
			else
			{
				Graphic.workerMesh.Clear();
			}
			List<Component> list = ListPool<Component>.Get();
			base.GetComponents(typeof(IMeshModifier), list);
			for (int i = 0; i < list.Count; i++)
			{
				((IMeshModifier)list[i]).ModifyMesh(Graphic.workerMesh);
			}
			ListPool<Component>.Release(list);
			this.canvasRenderer.SetMesh(Graphic.workerMesh);
		}

		// Token: 0x1700009C RID: 156
		// (get) Token: 0x06000250 RID: 592 RVA: 0x0000BCB4 File Offset: 0x0000A0B4
		protected static Mesh workerMesh
		{
			get
			{
				if (Graphic.s_Mesh == null)
				{
					Graphic.s_Mesh = new Mesh();
					Graphic.s_Mesh.name = "Shared UI Mesh";
					Graphic.s_Mesh.hideFlags = HideFlags.HideAndDontSave;
				}
				return Graphic.s_Mesh;
			}
		}

		// Token: 0x06000251 RID: 593 RVA: 0x0000BD05 File Offset: 0x0000A105
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Obsolete("Use OnPopulateMesh instead.", true)]
		protected virtual void OnFillVBO(List<UIVertex> vbo)
		{
		}

		// Token: 0x06000252 RID: 594 RVA: 0x0000BD08 File Offset: 0x0000A108
		[Obsolete("Use OnPopulateMesh(VertexHelper vh) instead.", false)]
		protected virtual void OnPopulateMesh(Mesh m)
		{
			this.OnPopulateMesh(Graphic.s_VertexHelper);
			Graphic.s_VertexHelper.FillMesh(m);
		}

		// Token: 0x06000253 RID: 595 RVA: 0x0000BD24 File Offset: 0x0000A124
		protected virtual void OnPopulateMesh(VertexHelper vh)
		{
			Rect pixelAdjustedRect = this.GetPixelAdjustedRect();
			Vector4 vector = new Vector4(pixelAdjustedRect.x, pixelAdjustedRect.y, pixelAdjustedRect.x + pixelAdjustedRect.width, pixelAdjustedRect.y + pixelAdjustedRect.height);
			Color32 color = this.color;
			vh.Clear();
			vh.AddVert(new Vector3(vector.x, vector.y), color, new Vector2(0f, 0f));
			vh.AddVert(new Vector3(vector.x, vector.w), color, new Vector2(0f, 1f));
			vh.AddVert(new Vector3(vector.z, vector.w), color, new Vector2(1f, 1f));
			vh.AddVert(new Vector3(vector.z, vector.y), color, new Vector2(1f, 0f));
			vh.AddTriangle(0, 1, 2);
			vh.AddTriangle(2, 3, 0);
		}

		// Token: 0x06000254 RID: 596 RVA: 0x0000BE34 File Offset: 0x0000A234
		protected override void OnDidApplyAnimationProperties()
		{
			this.SetAllDirty();
		}

		// Token: 0x06000255 RID: 597 RVA: 0x0000BE3D File Offset: 0x0000A23D
		public virtual void SetNativeSize()
		{
		}

		// Token: 0x06000256 RID: 598 RVA: 0x0000BE40 File Offset: 0x0000A240
		public virtual bool Raycast(Vector2 sp, Camera eventCamera)
		{
			bool result;
			if (!base.isActiveAndEnabled)
			{
				result = false;
			}
			else
			{
				Transform transform = base.transform;
				List<Component> list = ListPool<Component>.Get();
				bool flag = false;
				bool flag2 = true;
				while (transform != null)
				{
					transform.GetComponents<Component>(list);
					for (int i = 0; i < list.Count; i++)
					{
						Canvas canvas = list[i] as Canvas;
						if (canvas != null && canvas.overrideSorting)
						{
							flag2 = false;
						}
						ICanvasRaycastFilter canvasRaycastFilter = list[i] as ICanvasRaycastFilter;
						if (canvasRaycastFilter != null)
						{
							bool flag3 = true;
							CanvasGroup canvasGroup = list[i] as CanvasGroup;
							if (canvasGroup != null)
							{
								if (!flag && canvasGroup.ignoreParentGroups)
								{
									flag = true;
									flag3 = canvasRaycastFilter.IsRaycastLocationValid(sp, eventCamera);
								}
								else if (!flag)
								{
									flag3 = canvasRaycastFilter.IsRaycastLocationValid(sp, eventCamera);
								}
							}
							else
							{
								flag3 = canvasRaycastFilter.IsRaycastLocationValid(sp, eventCamera);
							}
							if (!flag3)
							{
								ListPool<Component>.Release(list);
								return false;
							}
						}
					}
					transform = ((!flag2) ? null : transform.parent);
				}
				ListPool<Component>.Release(list);
				result = true;
			}
			return result;
		}

		// Token: 0x06000257 RID: 599 RVA: 0x0000BF90 File Offset: 0x0000A390
		public Vector2 PixelAdjustPoint(Vector2 point)
		{
			Vector2 result;
			if (!this.canvas || this.canvas.renderMode == RenderMode.WorldSpace || this.canvas.scaleFactor == 0f || !this.canvas.pixelPerfect)
			{
				result = point;
			}
			else
			{
				result = RectTransformUtility.PixelAdjustPoint(point, base.transform, this.canvas);
			}
			return result;
		}

		// Token: 0x06000258 RID: 600 RVA: 0x0000C008 File Offset: 0x0000A408
		public Rect GetPixelAdjustedRect()
		{
			Rect result;
			if (!this.canvas || this.canvas.renderMode == RenderMode.WorldSpace || this.canvas.scaleFactor == 0f || !this.canvas.pixelPerfect)
			{
				result = this.rectTransform.rect;
			}
			else
			{
				result = RectTransformUtility.PixelAdjustRect(this.rectTransform, this.canvas);
			}
			return result;
		}

		// Token: 0x06000259 RID: 601 RVA: 0x0000C085 File Offset: 0x0000A485
		public virtual void CrossFadeColor(Color targetColor, float duration, bool ignoreTimeScale, bool useAlpha)
		{
			this.CrossFadeColor(targetColor, duration, ignoreTimeScale, useAlpha, true);
		}

		// Token: 0x0600025A RID: 602 RVA: 0x0000C094 File Offset: 0x0000A494
		public virtual void CrossFadeColor(Color targetColor, float duration, bool ignoreTimeScale, bool useAlpha, bool useRGB)
		{
			if (!(this.canvasRenderer == null) && (useRGB || useAlpha))
			{
				if (this.canvasRenderer.GetColor().Equals(targetColor))
				{
					this.m_ColorTweenRunner.StopTween();
				}
				else
				{
					ColorTween.ColorTweenMode tweenMode = (!useRGB || !useAlpha) ? ((!useRGB) ? ColorTween.ColorTweenMode.Alpha : ColorTween.ColorTweenMode.RGB) : ColorTween.ColorTweenMode.All;
					ColorTween info = new ColorTween
					{
						duration = duration,
						startColor = this.canvasRenderer.GetColor(),
						targetColor = targetColor
					};
					info.AddOnChangedCallback(new UnityAction<Color>(this.canvasRenderer.SetColor));
					info.ignoreTimeScale = ignoreTimeScale;
					info.tweenMode = tweenMode;
					this.m_ColorTweenRunner.StartTween(info);
				}
			}
		}

		// Token: 0x0600025B RID: 603 RVA: 0x0000C174 File Offset: 0x0000A574
		private static Color CreateColorFromAlpha(float alpha)
		{
			Color black = Color.black;
			black.a = alpha;
			return black;
		}

		// Token: 0x0600025C RID: 604 RVA: 0x0000C198 File Offset: 0x0000A598
		public virtual void CrossFadeAlpha(float alpha, float duration, bool ignoreTimeScale)
		{
			this.CrossFadeColor(Graphic.CreateColorFromAlpha(alpha), duration, ignoreTimeScale, true, false);
		}

		// Token: 0x0600025D RID: 605 RVA: 0x0000C1AB File Offset: 0x0000A5AB
		public void RegisterDirtyLayoutCallback(UnityAction action)
		{
			this.m_OnDirtyLayoutCallback = (UnityAction)Delegate.Combine(this.m_OnDirtyLayoutCallback, action);
		}

		// Token: 0x0600025E RID: 606 RVA: 0x0000C1C5 File Offset: 0x0000A5C5
		public void UnregisterDirtyLayoutCallback(UnityAction action)
		{
			this.m_OnDirtyLayoutCallback = (UnityAction)Delegate.Remove(this.m_OnDirtyLayoutCallback, action);
		}

		// Token: 0x0600025F RID: 607 RVA: 0x0000C1DF File Offset: 0x0000A5DF
		public void RegisterDirtyVerticesCallback(UnityAction action)
		{
			this.m_OnDirtyVertsCallback = (UnityAction)Delegate.Combine(this.m_OnDirtyVertsCallback, action);
		}

		// Token: 0x06000260 RID: 608 RVA: 0x0000C1F9 File Offset: 0x0000A5F9
		public void UnregisterDirtyVerticesCallback(UnityAction action)
		{
			this.m_OnDirtyVertsCallback = (UnityAction)Delegate.Remove(this.m_OnDirtyVertsCallback, action);
		}

		// Token: 0x06000261 RID: 609 RVA: 0x0000C213 File Offset: 0x0000A613
		public void RegisterDirtyMaterialCallback(UnityAction action)
		{
			this.m_OnDirtyMaterialCallback = (UnityAction)Delegate.Combine(this.m_OnDirtyMaterialCallback, action);
		}

		// Token: 0x06000262 RID: 610 RVA: 0x0000C22D File Offset: 0x0000A62D
		public void UnregisterDirtyMaterialCallback(UnityAction action)
		{
			this.m_OnDirtyMaterialCallback = (UnityAction)Delegate.Remove(this.m_OnDirtyMaterialCallback, action);
		}

		// Token: 0x06000263 RID: 611 RVA: 0x0000C247 File Offset: 0x0000A647
		// Note: this type is marked as 'beforefieldinit'.
		static Graphic()
		{
		}

		// Token: 0x06000264 RID: 612 RVA: 0x0000C25F File Offset: 0x0000A65F
		Transform ICanvasElement.get_transform()
		{
			return base.transform;
		}

		// Token: 0x0400010D RID: 269
		protected static Material s_DefaultUI = null;

		// Token: 0x0400010E RID: 270
		protected static Texture2D s_WhiteTexture = null;

		// Token: 0x0400010F RID: 271
		[FormerlySerializedAs("m_Mat")]
		[SerializeField]
		protected Material m_Material;

		// Token: 0x04000110 RID: 272
		[SerializeField]
		private Color m_Color = Color.white;

		// Token: 0x04000111 RID: 273
		[SerializeField]
		private bool m_RaycastTarget = true;

		// Token: 0x04000112 RID: 274
		[NonSerialized]
		private RectTransform m_RectTransform;

		// Token: 0x04000113 RID: 275
		[NonSerialized]
		private CanvasRenderer m_CanvasRenderer;

		// Token: 0x04000114 RID: 276
		[NonSerialized]
		private Canvas m_Canvas;

		// Token: 0x04000115 RID: 277
		[NonSerialized]
		private bool m_VertsDirty;

		// Token: 0x04000116 RID: 278
		[NonSerialized]
		private bool m_MaterialDirty;

		// Token: 0x04000117 RID: 279
		[NonSerialized]
		protected UnityAction m_OnDirtyLayoutCallback;

		// Token: 0x04000118 RID: 280
		[NonSerialized]
		protected UnityAction m_OnDirtyVertsCallback;

		// Token: 0x04000119 RID: 281
		[NonSerialized]
		protected UnityAction m_OnDirtyMaterialCallback;

		// Token: 0x0400011A RID: 282
		[NonSerialized]
		protected static Mesh s_Mesh;

		// Token: 0x0400011B RID: 283
		[NonSerialized]
		private static readonly VertexHelper s_VertexHelper = new VertexHelper();

		// Token: 0x0400011C RID: 284
		[NonSerialized]
		private readonly TweenRunner<ColorTween> m_ColorTweenRunner;

		// Token: 0x0400011D RID: 285
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private bool <useLegacyMeshGeneration>k__BackingField;
	}
}
