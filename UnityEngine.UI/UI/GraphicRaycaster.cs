﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine.EventSystems;
using UnityEngine.Serialization;

namespace UnityEngine.UI
{
	// Token: 0x0200004A RID: 74
	[AddComponentMenu("Event/Graphic Raycaster")]
	[RequireComponent(typeof(Canvas))]
	public class GraphicRaycaster : BaseRaycaster
	{
		// Token: 0x06000265 RID: 613 RVA: 0x0000C267 File Offset: 0x0000A667
		protected GraphicRaycaster()
		{
		}

		// Token: 0x1700009D RID: 157
		// (get) Token: 0x06000266 RID: 614 RVA: 0x0000C298 File Offset: 0x0000A698
		public override int sortOrderPriority
		{
			get
			{
				int result;
				if (this.canvas.renderMode == RenderMode.ScreenSpaceOverlay)
				{
					result = this.canvas.sortingOrder;
				}
				else
				{
					result = base.sortOrderPriority;
				}
				return result;
			}
		}

		// Token: 0x1700009E RID: 158
		// (get) Token: 0x06000267 RID: 615 RVA: 0x0000C2D4 File Offset: 0x0000A6D4
		public override int renderOrderPriority
		{
			get
			{
				int result;
				if (this.canvas.renderMode == RenderMode.ScreenSpaceOverlay)
				{
					result = this.canvas.rootCanvas.renderOrder;
				}
				else
				{
					result = base.renderOrderPriority;
				}
				return result;
			}
		}

		// Token: 0x1700009F RID: 159
		// (get) Token: 0x06000268 RID: 616 RVA: 0x0000C318 File Offset: 0x0000A718
		// (set) Token: 0x06000269 RID: 617 RVA: 0x0000C333 File Offset: 0x0000A733
		public bool ignoreReversedGraphics
		{
			get
			{
				return this.m_IgnoreReversedGraphics;
			}
			set
			{
				this.m_IgnoreReversedGraphics = value;
			}
		}

		// Token: 0x170000A0 RID: 160
		// (get) Token: 0x0600026A RID: 618 RVA: 0x0000C340 File Offset: 0x0000A740
		// (set) Token: 0x0600026B RID: 619 RVA: 0x0000C35B File Offset: 0x0000A75B
		public GraphicRaycaster.BlockingObjects blockingObjects
		{
			get
			{
				return this.m_BlockingObjects;
			}
			set
			{
				this.m_BlockingObjects = value;
			}
		}

		// Token: 0x170000A1 RID: 161
		// (get) Token: 0x0600026C RID: 620 RVA: 0x0000C368 File Offset: 0x0000A768
		private Canvas canvas
		{
			get
			{
				Canvas canvas;
				if (this.m_Canvas != null)
				{
					canvas = this.m_Canvas;
				}
				else
				{
					this.m_Canvas = base.GetComponent<Canvas>();
					canvas = this.m_Canvas;
				}
				return canvas;
			}
		}

		// Token: 0x0600026D RID: 621 RVA: 0x0000C3AC File Offset: 0x0000A7AC
		public override void Raycast(PointerEventData eventData, List<RaycastResult> resultAppendList)
		{
			if (!(this.canvas == null))
			{
				IList<Graphic> graphicsForCanvas = GraphicRegistry.GetGraphicsForCanvas(this.canvas);
				if (graphicsForCanvas != null && graphicsForCanvas.Count != 0)
				{
					Camera eventCamera = this.eventCamera;
					int targetDisplay;
					if (this.canvas.renderMode == RenderMode.ScreenSpaceOverlay || eventCamera == null)
					{
						targetDisplay = this.canvas.targetDisplay;
					}
					else
					{
						targetDisplay = eventCamera.targetDisplay;
					}
					Vector3 vector = Display.RelativeMouseAt(eventData.position);
					if (vector != Vector3.zero)
					{
						int num = (int)vector.z;
						if (num != targetDisplay)
						{
							return;
						}
					}
					else
					{
						vector = eventData.position;
					}
					Vector2 vector2;
					if (eventCamera == null)
					{
						float num2 = (float)Screen.width;
						float num3 = (float)Screen.height;
						if (targetDisplay > 0 && targetDisplay < Display.displays.Length)
						{
							num2 = (float)Display.displays[targetDisplay].systemWidth;
							num3 = (float)Display.displays[targetDisplay].systemHeight;
						}
						vector2 = new Vector2(vector.x / num2, vector.y / num3);
					}
					else
					{
						vector2 = eventCamera.ScreenToViewportPoint(vector);
					}
					if (vector2.x >= 0f && vector2.x <= 1f && vector2.y >= 0f && vector2.y <= 1f)
					{
						float num4 = float.MaxValue;
						Ray r = default(Ray);
						if (eventCamera != null)
						{
							r = eventCamera.ScreenPointToRay(vector);
						}
						if (this.canvas.renderMode != RenderMode.ScreenSpaceOverlay && this.blockingObjects != GraphicRaycaster.BlockingObjects.None)
						{
							float f = 100f;
							if (eventCamera != null)
							{
								float z = r.direction.z;
								f = ((!Mathf.Approximately(0f, z)) ? Mathf.Abs((eventCamera.farClipPlane - eventCamera.nearClipPlane) / z) : float.PositiveInfinity);
							}
							if (this.blockingObjects == GraphicRaycaster.BlockingObjects.ThreeD || this.blockingObjects == GraphicRaycaster.BlockingObjects.All)
							{
								if (ReflectionMethodsCache.Singleton.raycast3D != null)
								{
									RaycastHit[] array = ReflectionMethodsCache.Singleton.raycast3DAll(r, f, this.m_BlockingMask);
									if (array.Length > 0)
									{
										num4 = array[0].distance;
									}
								}
							}
							if (this.blockingObjects == GraphicRaycaster.BlockingObjects.TwoD || this.blockingObjects == GraphicRaycaster.BlockingObjects.All)
							{
								if (ReflectionMethodsCache.Singleton.raycast2D != null)
								{
									RaycastHit2D[] array2 = ReflectionMethodsCache.Singleton.getRayIntersectionAll(r, f, this.m_BlockingMask);
									if (array2.Length > 0)
									{
										num4 = array2[0].distance;
									}
								}
							}
						}
						this.m_RaycastResults.Clear();
						GraphicRaycaster.Raycast(this.canvas, eventCamera, vector, graphicsForCanvas, this.m_RaycastResults);
						int count = this.m_RaycastResults.Count;
						int i = 0;
						while (i < count)
						{
							GameObject gameObject = this.m_RaycastResults[i].gameObject;
							bool flag = true;
							if (this.ignoreReversedGraphics)
							{
								if (eventCamera == null)
								{
									Vector3 rhs = gameObject.transform.rotation * Vector3.forward;
									flag = (Vector3.Dot(Vector3.forward, rhs) > 0f);
								}
								else
								{
									Vector3 lhs = eventCamera.transform.rotation * Vector3.forward;
									Vector3 rhs2 = gameObject.transform.rotation * Vector3.forward;
									flag = (Vector3.Dot(lhs, rhs2) > 0f);
								}
							}
							if (flag)
							{
								float num5;
								if (eventCamera == null || this.canvas.renderMode == RenderMode.ScreenSpaceOverlay)
								{
									num5 = 0f;
								}
								else
								{
									Transform transform = gameObject.transform;
									Vector3 forward = transform.forward;
									num5 = Vector3.Dot(forward, transform.position - r.origin) / Vector3.Dot(forward, r.direction);
									if (num5 < 0f)
									{
										goto IL_4EA;
									}
								}
								if (num5 < num4)
								{
									RaycastResult item = new RaycastResult
									{
										gameObject = gameObject,
										module = this,
										distance = num5,
										screenPosition = vector,
										index = (float)resultAppendList.Count,
										depth = this.m_RaycastResults[i].depth,
										sortingLayer = this.canvas.sortingLayerID,
										sortingOrder = this.canvas.sortingOrder
									};
									resultAppendList.Add(item);
								}
							}
							IL_4EA:
							i++;
							continue;
							goto IL_4EA;
						}
					}
				}
			}
		}

		// Token: 0x170000A2 RID: 162
		// (get) Token: 0x0600026E RID: 622 RVA: 0x0000C8B4 File Offset: 0x0000ACB4
		public override Camera eventCamera
		{
			get
			{
				Camera result;
				if (this.canvas.renderMode == RenderMode.ScreenSpaceOverlay || (this.canvas.renderMode == RenderMode.ScreenSpaceCamera && this.canvas.worldCamera == null))
				{
					result = null;
				}
				else
				{
					result = ((!(this.canvas.worldCamera != null)) ? Camera.main : this.canvas.worldCamera);
				}
				return result;
			}
		}

		// Token: 0x0600026F RID: 623 RVA: 0x0000C934 File Offset: 0x0000AD34
		private static void Raycast(Canvas canvas, Camera eventCamera, Vector2 pointerPosition, IList<Graphic> foundGraphics, List<Graphic> results)
		{
			int count = foundGraphics.Count;
			for (int i = 0; i < count; i++)
			{
				Graphic graphic = foundGraphics[i];
				if (graphic.depth != -1 && graphic.raycastTarget && !graphic.canvasRenderer.cull)
				{
					if (RectTransformUtility.RectangleContainsScreenPoint(graphic.rectTransform, pointerPosition, eventCamera))
					{
						if (!(eventCamera != null) || eventCamera.WorldToScreenPoint(graphic.rectTransform.position).z <= eventCamera.farClipPlane)
						{
							if (graphic.Raycast(pointerPosition, eventCamera))
							{
								GraphicRaycaster.s_SortedGraphics.Add(graphic);
							}
						}
					}
				}
			}
			GraphicRaycaster.s_SortedGraphics.Sort((Graphic g1, Graphic g2) => g2.depth.CompareTo(g1.depth));
			count = GraphicRaycaster.s_SortedGraphics.Count;
			for (int j = 0; j < count; j++)
			{
				results.Add(GraphicRaycaster.s_SortedGraphics[j]);
			}
			GraphicRaycaster.s_SortedGraphics.Clear();
		}

		// Token: 0x06000270 RID: 624 RVA: 0x0000CA5C File Offset: 0x0000AE5C
		// Note: this type is marked as 'beforefieldinit'.
		static GraphicRaycaster()
		{
		}

		// Token: 0x06000271 RID: 625 RVA: 0x0000CA68 File Offset: 0x0000AE68
		[CompilerGenerated]
		private static int <Raycast>m__0(Graphic g1, Graphic g2)
		{
			return g2.depth.CompareTo(g1.depth);
		}

		// Token: 0x0400011E RID: 286
		protected const int kNoEventMaskSet = -1;

		// Token: 0x0400011F RID: 287
		[FormerlySerializedAs("ignoreReversedGraphics")]
		[SerializeField]
		private bool m_IgnoreReversedGraphics = true;

		// Token: 0x04000120 RID: 288
		[FormerlySerializedAs("blockingObjects")]
		[SerializeField]
		private GraphicRaycaster.BlockingObjects m_BlockingObjects = GraphicRaycaster.BlockingObjects.None;

		// Token: 0x04000121 RID: 289
		[SerializeField]
		protected LayerMask m_BlockingMask = -1;

		// Token: 0x04000122 RID: 290
		private Canvas m_Canvas;

		// Token: 0x04000123 RID: 291
		[NonSerialized]
		private List<Graphic> m_RaycastResults = new List<Graphic>();

		// Token: 0x04000124 RID: 292
		[NonSerialized]
		private static readonly List<Graphic> s_SortedGraphics = new List<Graphic>();

		// Token: 0x04000125 RID: 293
		[CompilerGenerated]
		private static Comparison<Graphic> <>f__am$cache0;

		// Token: 0x0200004B RID: 75
		public enum BlockingObjects
		{
			// Token: 0x04000127 RID: 295
			None,
			// Token: 0x04000128 RID: 296
			TwoD,
			// Token: 0x04000129 RID: 297
			ThreeD,
			// Token: 0x0400012A RID: 298
			All
		}
	}
}
