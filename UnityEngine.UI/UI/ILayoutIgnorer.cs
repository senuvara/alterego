﻿using System;

namespace UnityEngine.UI
{
	// Token: 0x02000099 RID: 153
	public interface ILayoutIgnorer
	{
		// Token: 0x17000182 RID: 386
		// (get) Token: 0x06000577 RID: 1399
		bool ignoreLayout { get; }
	}
}
