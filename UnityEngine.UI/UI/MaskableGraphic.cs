﻿using System;
using System.ComponentModel;
using UnityEngine.Events;
using UnityEngine.Rendering;

namespace UnityEngine.UI
{
	// Token: 0x02000063 RID: 99
	public abstract class MaskableGraphic : Graphic, IClippable, IMaskable, IMaterialModifier
	{
		// Token: 0x06000371 RID: 881 RVA: 0x0000CBF8 File Offset: 0x0000AFF8
		protected MaskableGraphic()
		{
		}

		// Token: 0x170000ED RID: 237
		// (get) Token: 0x06000372 RID: 882 RVA: 0x0000CC34 File Offset: 0x0000B034
		// (set) Token: 0x06000373 RID: 883 RVA: 0x0000CC4F File Offset: 0x0000B04F
		public MaskableGraphic.CullStateChangedEvent onCullStateChanged
		{
			get
			{
				return this.m_OnCullStateChanged;
			}
			set
			{
				this.m_OnCullStateChanged = value;
			}
		}

		// Token: 0x170000EE RID: 238
		// (get) Token: 0x06000374 RID: 884 RVA: 0x0000CC5C File Offset: 0x0000B05C
		// (set) Token: 0x06000375 RID: 885 RVA: 0x0000CC77 File Offset: 0x0000B077
		public bool maskable
		{
			get
			{
				return this.m_Maskable;
			}
			set
			{
				if (value != this.m_Maskable)
				{
					this.m_Maskable = value;
					this.m_ShouldRecalculateStencil = true;
					this.SetMaterialDirty();
				}
			}
		}

		// Token: 0x06000376 RID: 886 RVA: 0x0000CCA0 File Offset: 0x0000B0A0
		public virtual Material GetModifiedMaterial(Material baseMaterial)
		{
			Material material = baseMaterial;
			if (this.m_ShouldRecalculateStencil)
			{
				Transform stopAfter = MaskUtilities.FindRootSortOverrideCanvas(base.transform);
				this.m_StencilValue = ((!this.maskable) ? 0 : MaskUtilities.GetStencilDepth(base.transform, stopAfter));
				this.m_ShouldRecalculateStencil = false;
			}
			Mask component = base.GetComponent<Mask>();
			if (this.m_StencilValue > 0 && (component == null || !component.IsActive()))
			{
				Material maskMaterial = StencilMaterial.Add(material, (1 << this.m_StencilValue) - 1, StencilOp.Keep, CompareFunction.Equal, ColorWriteMask.All, (1 << this.m_StencilValue) - 1, 0);
				StencilMaterial.Remove(this.m_MaskMaterial);
				this.m_MaskMaterial = maskMaterial;
				material = this.m_MaskMaterial;
			}
			return material;
		}

		// Token: 0x06000377 RID: 887 RVA: 0x0000CD68 File Offset: 0x0000B168
		public virtual void Cull(Rect clipRect, bool validRect)
		{
			bool cull = !validRect || !clipRect.Overlaps(this.rootCanvasRect, true);
			this.UpdateCull(cull);
		}

		// Token: 0x06000378 RID: 888 RVA: 0x0000CD98 File Offset: 0x0000B198
		private void UpdateCull(bool cull)
		{
			if (base.canvasRenderer.cull != cull)
			{
				base.canvasRenderer.cull = cull;
				UISystemProfilerApi.AddMarker("MaskableGraphic.cullingChanged", this);
				this.m_OnCullStateChanged.Invoke(cull);
				this.OnCullingChanged();
			}
		}

		// Token: 0x06000379 RID: 889 RVA: 0x0000CDD7 File Offset: 0x0000B1D7
		public virtual void SetClipRect(Rect clipRect, bool validRect)
		{
			if (validRect)
			{
				base.canvasRenderer.EnableRectClipping(clipRect);
			}
			else
			{
				base.canvasRenderer.DisableRectClipping();
			}
		}

		// Token: 0x0600037A RID: 890 RVA: 0x0000CDFC File Offset: 0x0000B1FC
		protected override void OnEnable()
		{
			base.OnEnable();
			this.m_ShouldRecalculateStencil = true;
			this.UpdateClipParent();
			this.SetMaterialDirty();
			if (base.GetComponent<Mask>() != null)
			{
				MaskUtilities.NotifyStencilStateChanged(this);
			}
		}

		// Token: 0x0600037B RID: 891 RVA: 0x0000CE34 File Offset: 0x0000B234
		protected override void OnDisable()
		{
			base.OnDisable();
			this.m_ShouldRecalculateStencil = true;
			this.SetMaterialDirty();
			this.UpdateClipParent();
			StencilMaterial.Remove(this.m_MaskMaterial);
			this.m_MaskMaterial = null;
			if (base.GetComponent<Mask>() != null)
			{
				MaskUtilities.NotifyStencilStateChanged(this);
			}
		}

		// Token: 0x0600037C RID: 892 RVA: 0x0000CE86 File Offset: 0x0000B286
		protected override void OnTransformParentChanged()
		{
			base.OnTransformParentChanged();
			if (base.isActiveAndEnabled)
			{
				this.m_ShouldRecalculateStencil = true;
				this.UpdateClipParent();
				this.SetMaterialDirty();
			}
		}

		// Token: 0x0600037D RID: 893 RVA: 0x0000CEB2 File Offset: 0x0000B2B2
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Obsolete("Not used anymore.", true)]
		public virtual void ParentMaskStateChanged()
		{
		}

		// Token: 0x0600037E RID: 894 RVA: 0x0000CEB5 File Offset: 0x0000B2B5
		protected override void OnCanvasHierarchyChanged()
		{
			base.OnCanvasHierarchyChanged();
			if (base.isActiveAndEnabled)
			{
				this.m_ShouldRecalculateStencil = true;
				this.UpdateClipParent();
				this.SetMaterialDirty();
			}
		}

		// Token: 0x170000EF RID: 239
		// (get) Token: 0x0600037F RID: 895 RVA: 0x0000CEE4 File Offset: 0x0000B2E4
		private Rect rootCanvasRect
		{
			get
			{
				base.rectTransform.GetWorldCorners(this.m_Corners);
				if (base.canvas)
				{
					Matrix4x4 worldToLocalMatrix = base.canvas.rootCanvas.transform.worldToLocalMatrix;
					for (int i = 0; i < 4; i++)
					{
						this.m_Corners[i] = worldToLocalMatrix.MultiplyPoint(this.m_Corners[i]);
					}
				}
				return new Rect(this.m_Corners[0].x, this.m_Corners[0].y, this.m_Corners[2].x - this.m_Corners[0].x, this.m_Corners[2].y - this.m_Corners[0].y);
			}
		}

		// Token: 0x06000380 RID: 896 RVA: 0x0000CFDC File Offset: 0x0000B3DC
		private void UpdateClipParent()
		{
			RectMask2D rectMask2D = (!this.maskable || !this.IsActive()) ? null : MaskUtilities.GetRectMaskForClippable(this);
			if (this.m_ParentMask != null && (rectMask2D != this.m_ParentMask || !rectMask2D.IsActive()))
			{
				this.m_ParentMask.RemoveClippable(this);
				this.UpdateCull(false);
			}
			if (rectMask2D != null && rectMask2D.IsActive())
			{
				rectMask2D.AddClippable(this);
			}
			this.m_ParentMask = rectMask2D;
		}

		// Token: 0x06000381 RID: 897 RVA: 0x0000D074 File Offset: 0x0000B474
		public virtual void RecalculateClipping()
		{
			this.UpdateClipParent();
		}

		// Token: 0x06000382 RID: 898 RVA: 0x0000D07D File Offset: 0x0000B47D
		public virtual void RecalculateMasking()
		{
			StencilMaterial.Remove(this.m_MaskMaterial);
			this.m_MaskMaterial = null;
			this.m_ShouldRecalculateStencil = true;
			this.SetMaterialDirty();
		}

		// Token: 0x06000383 RID: 899 RVA: 0x0000D09F File Offset: 0x0000B49F
		GameObject IClippable.get_gameObject()
		{
			return base.gameObject;
		}

		// Token: 0x040001B5 RID: 437
		[NonSerialized]
		protected bool m_ShouldRecalculateStencil = true;

		// Token: 0x040001B6 RID: 438
		[NonSerialized]
		protected Material m_MaskMaterial;

		// Token: 0x040001B7 RID: 439
		[NonSerialized]
		private RectMask2D m_ParentMask;

		// Token: 0x040001B8 RID: 440
		[NonSerialized]
		private bool m_Maskable = true;

		// Token: 0x040001B9 RID: 441
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Obsolete("Not used anymore.", true)]
		[NonSerialized]
		protected bool m_IncludeForMasking = false;

		// Token: 0x040001BA RID: 442
		[SerializeField]
		private MaskableGraphic.CullStateChangedEvent m_OnCullStateChanged = new MaskableGraphic.CullStateChangedEvent();

		// Token: 0x040001BB RID: 443
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Obsolete("Not used anymore", true)]
		[NonSerialized]
		protected bool m_ShouldRecalculate = true;

		// Token: 0x040001BC RID: 444
		[NonSerialized]
		protected int m_StencilValue;

		// Token: 0x040001BD RID: 445
		private readonly Vector3[] m_Corners = new Vector3[4];

		// Token: 0x02000064 RID: 100
		[Serializable]
		public class CullStateChangedEvent : UnityEvent<bool>
		{
			// Token: 0x06000384 RID: 900 RVA: 0x0000D0A7 File Offset: 0x0000B4A7
			public CullStateChangedEvent()
			{
			}
		}
	}
}
