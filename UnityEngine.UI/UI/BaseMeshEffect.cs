﻿using System;
using UnityEngine.EventSystems;

namespace UnityEngine.UI
{
	// Token: 0x020000AD RID: 173
	[ExecuteAlways]
	public abstract class BaseMeshEffect : UIBehaviour, IMeshModifier
	{
		// Token: 0x06000636 RID: 1590 RVA: 0x0001F1BD File Offset: 0x0001D5BD
		protected BaseMeshEffect()
		{
		}

		// Token: 0x170001A1 RID: 417
		// (get) Token: 0x06000637 RID: 1591 RVA: 0x0001F1C8 File Offset: 0x0001D5C8
		protected Graphic graphic
		{
			get
			{
				if (this.m_Graphic == null)
				{
					this.m_Graphic = base.GetComponent<Graphic>();
				}
				return this.m_Graphic;
			}
		}

		// Token: 0x06000638 RID: 1592 RVA: 0x0001F200 File Offset: 0x0001D600
		protected override void OnEnable()
		{
			base.OnEnable();
			if (this.graphic != null)
			{
				this.graphic.SetVerticesDirty();
			}
		}

		// Token: 0x06000639 RID: 1593 RVA: 0x0001F225 File Offset: 0x0001D625
		protected override void OnDisable()
		{
			if (this.graphic != null)
			{
				this.graphic.SetVerticesDirty();
			}
			base.OnDisable();
		}

		// Token: 0x0600063A RID: 1594 RVA: 0x0001F24A File Offset: 0x0001D64A
		protected override void OnDidApplyAnimationProperties()
		{
			if (this.graphic != null)
			{
				this.graphic.SetVerticesDirty();
			}
			base.OnDidApplyAnimationProperties();
		}

		// Token: 0x0600063B RID: 1595 RVA: 0x0001F270 File Offset: 0x0001D670
		public virtual void ModifyMesh(Mesh mesh)
		{
			using (VertexHelper vertexHelper = new VertexHelper(mesh))
			{
				this.ModifyMesh(vertexHelper);
				vertexHelper.FillMesh(mesh);
			}
		}

		// Token: 0x0600063C RID: 1596
		public abstract void ModifyMesh(VertexHelper vh);

		// Token: 0x040002EA RID: 746
		[NonSerialized]
		private Graphic m_Graphic;
	}
}
