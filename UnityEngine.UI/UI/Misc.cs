﻿using System;

namespace UnityEngine.UI
{
	// Token: 0x02000065 RID: 101
	internal static class Misc
	{
		// Token: 0x06000385 RID: 901 RVA: 0x00014FF4 File Offset: 0x000133F4
		public static void Destroy(Object obj)
		{
			if (obj != null)
			{
				if (Application.isPlaying)
				{
					if (obj is GameObject)
					{
						GameObject gameObject = obj as GameObject;
						gameObject.transform.parent = null;
					}
					Object.Destroy(obj);
				}
				else
				{
					Object.DestroyImmediate(obj);
				}
			}
		}

		// Token: 0x06000386 RID: 902 RVA: 0x0001504D File Offset: 0x0001344D
		public static void DestroyImmediate(Object obj)
		{
			if (obj != null)
			{
				if (Application.isEditor)
				{
					Object.DestroyImmediate(obj);
				}
				else
				{
					Object.Destroy(obj);
				}
			}
		}
	}
}
