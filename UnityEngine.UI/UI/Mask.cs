﻿using System;
using UnityEngine.EventSystems;
using UnityEngine.Rendering;

namespace UnityEngine.UI
{
	// Token: 0x02000061 RID: 97
	[AddComponentMenu("UI/Mask", 13)]
	[ExecuteAlways]
	[RequireComponent(typeof(RectTransform))]
	[DisallowMultipleComponent]
	public class Mask : UIBehaviour, ICanvasRaycastFilter, IMaterialModifier
	{
		// Token: 0x0600035E RID: 862 RVA: 0x00014789 File Offset: 0x00012B89
		protected Mask()
		{
		}

		// Token: 0x170000EA RID: 234
		// (get) Token: 0x0600035F RID: 863 RVA: 0x0001479C File Offset: 0x00012B9C
		public RectTransform rectTransform
		{
			get
			{
				RectTransform result;
				if ((result = this.m_RectTransform) == null)
				{
					result = (this.m_RectTransform = base.GetComponent<RectTransform>());
				}
				return result;
			}
		}

		// Token: 0x170000EB RID: 235
		// (get) Token: 0x06000360 RID: 864 RVA: 0x000147D0 File Offset: 0x00012BD0
		// (set) Token: 0x06000361 RID: 865 RVA: 0x000147EB File Offset: 0x00012BEB
		public bool showMaskGraphic
		{
			get
			{
				return this.m_ShowMaskGraphic;
			}
			set
			{
				if (this.m_ShowMaskGraphic != value)
				{
					this.m_ShowMaskGraphic = value;
					if (this.graphic != null)
					{
						this.graphic.SetMaterialDirty();
					}
				}
			}
		}

		// Token: 0x170000EC RID: 236
		// (get) Token: 0x06000362 RID: 866 RVA: 0x00014824 File Offset: 0x00012C24
		public Graphic graphic
		{
			get
			{
				Graphic result;
				if ((result = this.m_Graphic) == null)
				{
					result = (this.m_Graphic = base.GetComponent<Graphic>());
				}
				return result;
			}
		}

		// Token: 0x06000363 RID: 867 RVA: 0x00014858 File Offset: 0x00012C58
		public virtual bool MaskEnabled()
		{
			return this.IsActive() && this.graphic != null;
		}

		// Token: 0x06000364 RID: 868 RVA: 0x00014887 File Offset: 0x00012C87
		[Obsolete("Not used anymore.")]
		public virtual void OnSiblingGraphicEnabledDisabled()
		{
		}

		// Token: 0x06000365 RID: 869 RVA: 0x0001488A File Offset: 0x00012C8A
		protected override void OnEnable()
		{
			base.OnEnable();
			if (this.graphic != null)
			{
				this.graphic.canvasRenderer.hasPopInstruction = true;
				this.graphic.SetMaterialDirty();
			}
			MaskUtilities.NotifyStencilStateChanged(this);
		}

		// Token: 0x06000366 RID: 870 RVA: 0x000148C8 File Offset: 0x00012CC8
		protected override void OnDisable()
		{
			base.OnDisable();
			if (this.graphic != null)
			{
				this.graphic.SetMaterialDirty();
				this.graphic.canvasRenderer.hasPopInstruction = false;
				this.graphic.canvasRenderer.popMaterialCount = 0;
			}
			StencilMaterial.Remove(this.m_MaskMaterial);
			this.m_MaskMaterial = null;
			StencilMaterial.Remove(this.m_UnmaskMaterial);
			this.m_UnmaskMaterial = null;
			MaskUtilities.NotifyStencilStateChanged(this);
		}

		// Token: 0x06000367 RID: 871 RVA: 0x00014948 File Offset: 0x00012D48
		public virtual bool IsRaycastLocationValid(Vector2 sp, Camera eventCamera)
		{
			return !base.isActiveAndEnabled || RectTransformUtility.RectangleContainsScreenPoint(this.rectTransform, sp, eventCamera);
		}

		// Token: 0x06000368 RID: 872 RVA: 0x0001497C File Offset: 0x00012D7C
		public virtual Material GetModifiedMaterial(Material baseMaterial)
		{
			Material result;
			if (!this.MaskEnabled())
			{
				result = baseMaterial;
			}
			else
			{
				Transform stopAfter = MaskUtilities.FindRootSortOverrideCanvas(base.transform);
				int stencilDepth = MaskUtilities.GetStencilDepth(base.transform, stopAfter);
				if (stencilDepth >= 8)
				{
					Debug.LogError("Attempting to use a stencil mask with depth > 8", base.gameObject);
					result = baseMaterial;
				}
				else
				{
					int num = 1 << stencilDepth;
					if (num == 1)
					{
						Material maskMaterial = StencilMaterial.Add(baseMaterial, 1, StencilOp.Replace, CompareFunction.Always, (!this.m_ShowMaskGraphic) ? ((ColorWriteMask)0) : ColorWriteMask.All);
						StencilMaterial.Remove(this.m_MaskMaterial);
						this.m_MaskMaterial = maskMaterial;
						Material unmaskMaterial = StencilMaterial.Add(baseMaterial, 1, StencilOp.Zero, CompareFunction.Always, (ColorWriteMask)0);
						StencilMaterial.Remove(this.m_UnmaskMaterial);
						this.m_UnmaskMaterial = unmaskMaterial;
						this.graphic.canvasRenderer.popMaterialCount = 1;
						this.graphic.canvasRenderer.SetPopMaterial(this.m_UnmaskMaterial, 0);
						result = this.m_MaskMaterial;
					}
					else
					{
						Material maskMaterial2 = StencilMaterial.Add(baseMaterial, num | num - 1, StencilOp.Replace, CompareFunction.Equal, (!this.m_ShowMaskGraphic) ? ((ColorWriteMask)0) : ColorWriteMask.All, num - 1, num | num - 1);
						StencilMaterial.Remove(this.m_MaskMaterial);
						this.m_MaskMaterial = maskMaterial2;
						this.graphic.canvasRenderer.hasPopInstruction = true;
						Material unmaskMaterial2 = StencilMaterial.Add(baseMaterial, num - 1, StencilOp.Replace, CompareFunction.Equal, (ColorWriteMask)0, num - 1, num | num - 1);
						StencilMaterial.Remove(this.m_UnmaskMaterial);
						this.m_UnmaskMaterial = unmaskMaterial2;
						this.graphic.canvasRenderer.popMaterialCount = 1;
						this.graphic.canvasRenderer.SetPopMaterial(this.m_UnmaskMaterial, 0);
						result = this.m_MaskMaterial;
					}
				}
			}
			return result;
		}

		// Token: 0x040001B0 RID: 432
		[NonSerialized]
		private RectTransform m_RectTransform;

		// Token: 0x040001B1 RID: 433
		[SerializeField]
		private bool m_ShowMaskGraphic = true;

		// Token: 0x040001B2 RID: 434
		[NonSerialized]
		private Graphic m_Graphic;

		// Token: 0x040001B3 RID: 435
		[NonSerialized]
		private Material m_MaskMaterial;

		// Token: 0x040001B4 RID: 436
		[NonSerialized]
		private Material m_UnmaskMaterial;
	}
}
