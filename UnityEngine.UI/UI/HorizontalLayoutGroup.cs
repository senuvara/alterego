﻿using System;

namespace UnityEngine.UI
{
	// Token: 0x02000093 RID: 147
	[AddComponentMenu("Layout/Horizontal Layout Group", 150)]
	public class HorizontalLayoutGroup : HorizontalOrVerticalLayoutGroup
	{
		// Token: 0x06000559 RID: 1369 RVA: 0x0001D3C6 File Offset: 0x0001B7C6
		protected HorizontalLayoutGroup()
		{
		}

		// Token: 0x0600055A RID: 1370 RVA: 0x0001D3CF File Offset: 0x0001B7CF
		public override void CalculateLayoutInputHorizontal()
		{
			base.CalculateLayoutInputHorizontal();
			base.CalcAlongAxis(0, false);
		}

		// Token: 0x0600055B RID: 1371 RVA: 0x0001D3E0 File Offset: 0x0001B7E0
		public override void CalculateLayoutInputVertical()
		{
			base.CalcAlongAxis(1, false);
		}

		// Token: 0x0600055C RID: 1372 RVA: 0x0001D3EB File Offset: 0x0001B7EB
		public override void SetLayoutHorizontal()
		{
			base.SetChildrenAlongAxis(0, false);
		}

		// Token: 0x0600055D RID: 1373 RVA: 0x0001D3F6 File Offset: 0x0001B7F6
		public override void SetLayoutVertical()
		{
			base.SetChildrenAlongAxis(1, false);
		}
	}
}
