﻿using System;

namespace UnityEngine.UI
{
	// Token: 0x02000096 RID: 150
	public interface ILayoutController
	{
		// Token: 0x06000575 RID: 1397
		void SetLayoutHorizontal();

		// Token: 0x06000576 RID: 1398
		void SetLayoutVertical();
	}
}
