﻿using System;
using System.Collections.Generic;

namespace UnityEngine.UI
{
	// Token: 0x02000075 RID: 117
	internal static class SetPropertyUtility
	{
		// Token: 0x0600046A RID: 1130 RVA: 0x000188E0 File Offset: 0x00016CE0
		public static bool SetColor(ref Color currentValue, Color newValue)
		{
			bool result;
			if (currentValue.r == newValue.r && currentValue.g == newValue.g && currentValue.b == newValue.b && currentValue.a == newValue.a)
			{
				result = false;
			}
			else
			{
				currentValue = newValue;
				result = true;
			}
			return result;
		}

		// Token: 0x0600046B RID: 1131 RVA: 0x0001894C File Offset: 0x00016D4C
		public static bool SetStruct<T>(ref T currentValue, T newValue) where T : struct
		{
			bool result;
			if (EqualityComparer<T>.Default.Equals(currentValue, newValue))
			{
				result = false;
			}
			else
			{
				currentValue = newValue;
				result = true;
			}
			return result;
		}

		// Token: 0x0600046C RID: 1132 RVA: 0x00018988 File Offset: 0x00016D88
		public static bool SetClass<T>(ref T currentValue, T newValue) where T : class
		{
			bool result;
			if ((currentValue == null && newValue == null) || (currentValue != null && currentValue.Equals(newValue)))
			{
				result = false;
			}
			else
			{
				currentValue = newValue;
				result = true;
			}
			return result;
		}
	}
}
