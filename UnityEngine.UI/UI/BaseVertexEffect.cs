﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace UnityEngine.UI
{
	// Token: 0x020000AC RID: 172
	[Obsolete("Use BaseMeshEffect instead", true)]
	public abstract class BaseVertexEffect
	{
		// Token: 0x06000634 RID: 1588 RVA: 0x0001F1B5 File Offset: 0x0001D5B5
		protected BaseVertexEffect()
		{
		}

		// Token: 0x06000635 RID: 1589
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Obsolete("Use BaseMeshEffect.ModifyMeshes instead", true)]
		public abstract void ModifyVertices(List<UIVertex> vertices);
	}
}
