﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace UnityEngine.UI
{
	// Token: 0x0200009D RID: 157
	public static class LayoutUtility
	{
		// Token: 0x060005CE RID: 1486 RVA: 0x0001DCD8 File Offset: 0x0001C0D8
		public static float GetMinSize(RectTransform rect, int axis)
		{
			float result;
			if (axis == 0)
			{
				result = LayoutUtility.GetMinWidth(rect);
			}
			else
			{
				result = LayoutUtility.GetMinHeight(rect);
			}
			return result;
		}

		// Token: 0x060005CF RID: 1487 RVA: 0x0001DD08 File Offset: 0x0001C108
		public static float GetPreferredSize(RectTransform rect, int axis)
		{
			float result;
			if (axis == 0)
			{
				result = LayoutUtility.GetPreferredWidth(rect);
			}
			else
			{
				result = LayoutUtility.GetPreferredHeight(rect);
			}
			return result;
		}

		// Token: 0x060005D0 RID: 1488 RVA: 0x0001DD38 File Offset: 0x0001C138
		public static float GetFlexibleSize(RectTransform rect, int axis)
		{
			float result;
			if (axis == 0)
			{
				result = LayoutUtility.GetFlexibleWidth(rect);
			}
			else
			{
				result = LayoutUtility.GetFlexibleHeight(rect);
			}
			return result;
		}

		// Token: 0x060005D1 RID: 1489 RVA: 0x0001DD68 File Offset: 0x0001C168
		public static float GetMinWidth(RectTransform rect)
		{
			return LayoutUtility.GetLayoutProperty(rect, (ILayoutElement e) => e.minWidth, 0f);
		}

		// Token: 0x060005D2 RID: 1490 RVA: 0x0001DDA8 File Offset: 0x0001C1A8
		public static float GetPreferredWidth(RectTransform rect)
		{
			return Mathf.Max(LayoutUtility.GetLayoutProperty(rect, (ILayoutElement e) => e.minWidth, 0f), LayoutUtility.GetLayoutProperty(rect, (ILayoutElement e) => e.preferredWidth, 0f));
		}

		// Token: 0x060005D3 RID: 1491 RVA: 0x0001DE14 File Offset: 0x0001C214
		public static float GetFlexibleWidth(RectTransform rect)
		{
			return LayoutUtility.GetLayoutProperty(rect, (ILayoutElement e) => e.flexibleWidth, 0f);
		}

		// Token: 0x060005D4 RID: 1492 RVA: 0x0001DE54 File Offset: 0x0001C254
		public static float GetMinHeight(RectTransform rect)
		{
			return LayoutUtility.GetLayoutProperty(rect, (ILayoutElement e) => e.minHeight, 0f);
		}

		// Token: 0x060005D5 RID: 1493 RVA: 0x0001DE94 File Offset: 0x0001C294
		public static float GetPreferredHeight(RectTransform rect)
		{
			return Mathf.Max(LayoutUtility.GetLayoutProperty(rect, (ILayoutElement e) => e.minHeight, 0f), LayoutUtility.GetLayoutProperty(rect, (ILayoutElement e) => e.preferredHeight, 0f));
		}

		// Token: 0x060005D6 RID: 1494 RVA: 0x0001DF00 File Offset: 0x0001C300
		public static float GetFlexibleHeight(RectTransform rect)
		{
			return LayoutUtility.GetLayoutProperty(rect, (ILayoutElement e) => e.flexibleHeight, 0f);
		}

		// Token: 0x060005D7 RID: 1495 RVA: 0x0001DF40 File Offset: 0x0001C340
		public static float GetLayoutProperty(RectTransform rect, Func<ILayoutElement, float> property, float defaultValue)
		{
			ILayoutElement layoutElement;
			return LayoutUtility.GetLayoutProperty(rect, property, defaultValue, out layoutElement);
		}

		// Token: 0x060005D8 RID: 1496 RVA: 0x0001DF60 File Offset: 0x0001C360
		public static float GetLayoutProperty(RectTransform rect, Func<ILayoutElement, float> property, float defaultValue, out ILayoutElement source)
		{
			source = null;
			float result;
			if (rect == null)
			{
				result = 0f;
			}
			else
			{
				float num = defaultValue;
				int num2 = int.MinValue;
				List<Component> list = ListPool<Component>.Get();
				rect.GetComponents(typeof(ILayoutElement), list);
				for (int i = 0; i < list.Count; i++)
				{
					ILayoutElement layoutElement = list[i] as ILayoutElement;
					if (!(layoutElement is Behaviour) || ((Behaviour)layoutElement).isActiveAndEnabled)
					{
						int layoutPriority = layoutElement.layoutPriority;
						if (layoutPriority >= num2)
						{
							float num3 = property(layoutElement);
							if (num3 >= 0f)
							{
								if (layoutPriority > num2)
								{
									num = num3;
									num2 = layoutPriority;
									source = layoutElement;
								}
								else if (num3 > num)
								{
									num = num3;
									source = layoutElement;
								}
							}
						}
					}
				}
				ListPool<Component>.Release(list);
				result = num;
			}
			return result;
		}

		// Token: 0x060005D9 RID: 1497 RVA: 0x0001E060 File Offset: 0x0001C460
		[CompilerGenerated]
		private static float <GetMinWidth>m__0(ILayoutElement e)
		{
			return e.minWidth;
		}

		// Token: 0x060005DA RID: 1498 RVA: 0x0001E07C File Offset: 0x0001C47C
		[CompilerGenerated]
		private static float <GetPreferredWidth>m__1(ILayoutElement e)
		{
			return e.minWidth;
		}

		// Token: 0x060005DB RID: 1499 RVA: 0x0001E098 File Offset: 0x0001C498
		[CompilerGenerated]
		private static float <GetPreferredWidth>m__2(ILayoutElement e)
		{
			return e.preferredWidth;
		}

		// Token: 0x060005DC RID: 1500 RVA: 0x0001E0B4 File Offset: 0x0001C4B4
		[CompilerGenerated]
		private static float <GetFlexibleWidth>m__3(ILayoutElement e)
		{
			return e.flexibleWidth;
		}

		// Token: 0x060005DD RID: 1501 RVA: 0x0001E0D0 File Offset: 0x0001C4D0
		[CompilerGenerated]
		private static float <GetMinHeight>m__4(ILayoutElement e)
		{
			return e.minHeight;
		}

		// Token: 0x060005DE RID: 1502 RVA: 0x0001E0EC File Offset: 0x0001C4EC
		[CompilerGenerated]
		private static float <GetPreferredHeight>m__5(ILayoutElement e)
		{
			return e.minHeight;
		}

		// Token: 0x060005DF RID: 1503 RVA: 0x0001E108 File Offset: 0x0001C508
		[CompilerGenerated]
		private static float <GetPreferredHeight>m__6(ILayoutElement e)
		{
			return e.preferredHeight;
		}

		// Token: 0x060005E0 RID: 1504 RVA: 0x0001E124 File Offset: 0x0001C524
		[CompilerGenerated]
		private static float <GetFlexibleHeight>m__7(ILayoutElement e)
		{
			return e.flexibleHeight;
		}

		// Token: 0x040002C7 RID: 711
		[CompilerGenerated]
		private static Func<ILayoutElement, float> <>f__am$cache0;

		// Token: 0x040002C8 RID: 712
		[CompilerGenerated]
		private static Func<ILayoutElement, float> <>f__am$cache1;

		// Token: 0x040002C9 RID: 713
		[CompilerGenerated]
		private static Func<ILayoutElement, float> <>f__am$cache2;

		// Token: 0x040002CA RID: 714
		[CompilerGenerated]
		private static Func<ILayoutElement, float> <>f__am$cache3;

		// Token: 0x040002CB RID: 715
		[CompilerGenerated]
		private static Func<ILayoutElement, float> <>f__am$cache4;

		// Token: 0x040002CC RID: 716
		[CompilerGenerated]
		private static Func<ILayoutElement, float> <>f__am$cache5;

		// Token: 0x040002CD RID: 717
		[CompilerGenerated]
		private static Func<ILayoutElement, float> <>f__am$cache6;

		// Token: 0x040002CE RID: 718
		[CompilerGenerated]
		private static Func<ILayoutElement, float> <>f__am$cache7;
	}
}
