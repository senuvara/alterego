﻿using System;
using UnityEngine.EventSystems;

namespace UnityEngine.UI
{
	// Token: 0x0200009A RID: 154
	[AddComponentMenu("Layout/Layout Element", 140)]
	[RequireComponent(typeof(RectTransform))]
	[ExecuteAlways]
	public class LayoutElement : UIBehaviour, ILayoutElement, ILayoutIgnorer
	{
		// Token: 0x06000578 RID: 1400 RVA: 0x0001D404 File Offset: 0x0001B804
		protected LayoutElement()
		{
		}

		// Token: 0x17000183 RID: 387
		// (get) Token: 0x06000579 RID: 1401 RVA: 0x0001D468 File Offset: 0x0001B868
		// (set) Token: 0x0600057A RID: 1402 RVA: 0x0001D483 File Offset: 0x0001B883
		public virtual bool ignoreLayout
		{
			get
			{
				return this.m_IgnoreLayout;
			}
			set
			{
				if (SetPropertyUtility.SetStruct<bool>(ref this.m_IgnoreLayout, value))
				{
					this.SetDirty();
				}
			}
		}

		// Token: 0x0600057B RID: 1403 RVA: 0x0001D49D File Offset: 0x0001B89D
		public virtual void CalculateLayoutInputHorizontal()
		{
		}

		// Token: 0x0600057C RID: 1404 RVA: 0x0001D4A0 File Offset: 0x0001B8A0
		public virtual void CalculateLayoutInputVertical()
		{
		}

		// Token: 0x17000184 RID: 388
		// (get) Token: 0x0600057D RID: 1405 RVA: 0x0001D4A4 File Offset: 0x0001B8A4
		// (set) Token: 0x0600057E RID: 1406 RVA: 0x0001D4BF File Offset: 0x0001B8BF
		public virtual float minWidth
		{
			get
			{
				return this.m_MinWidth;
			}
			set
			{
				if (SetPropertyUtility.SetStruct<float>(ref this.m_MinWidth, value))
				{
					this.SetDirty();
				}
			}
		}

		// Token: 0x17000185 RID: 389
		// (get) Token: 0x0600057F RID: 1407 RVA: 0x0001D4DC File Offset: 0x0001B8DC
		// (set) Token: 0x06000580 RID: 1408 RVA: 0x0001D4F7 File Offset: 0x0001B8F7
		public virtual float minHeight
		{
			get
			{
				return this.m_MinHeight;
			}
			set
			{
				if (SetPropertyUtility.SetStruct<float>(ref this.m_MinHeight, value))
				{
					this.SetDirty();
				}
			}
		}

		// Token: 0x17000186 RID: 390
		// (get) Token: 0x06000581 RID: 1409 RVA: 0x0001D514 File Offset: 0x0001B914
		// (set) Token: 0x06000582 RID: 1410 RVA: 0x0001D52F File Offset: 0x0001B92F
		public virtual float preferredWidth
		{
			get
			{
				return this.m_PreferredWidth;
			}
			set
			{
				if (SetPropertyUtility.SetStruct<float>(ref this.m_PreferredWidth, value))
				{
					this.SetDirty();
				}
			}
		}

		// Token: 0x17000187 RID: 391
		// (get) Token: 0x06000583 RID: 1411 RVA: 0x0001D54C File Offset: 0x0001B94C
		// (set) Token: 0x06000584 RID: 1412 RVA: 0x0001D567 File Offset: 0x0001B967
		public virtual float preferredHeight
		{
			get
			{
				return this.m_PreferredHeight;
			}
			set
			{
				if (SetPropertyUtility.SetStruct<float>(ref this.m_PreferredHeight, value))
				{
					this.SetDirty();
				}
			}
		}

		// Token: 0x17000188 RID: 392
		// (get) Token: 0x06000585 RID: 1413 RVA: 0x0001D584 File Offset: 0x0001B984
		// (set) Token: 0x06000586 RID: 1414 RVA: 0x0001D59F File Offset: 0x0001B99F
		public virtual float flexibleWidth
		{
			get
			{
				return this.m_FlexibleWidth;
			}
			set
			{
				if (SetPropertyUtility.SetStruct<float>(ref this.m_FlexibleWidth, value))
				{
					this.SetDirty();
				}
			}
		}

		// Token: 0x17000189 RID: 393
		// (get) Token: 0x06000587 RID: 1415 RVA: 0x0001D5BC File Offset: 0x0001B9BC
		// (set) Token: 0x06000588 RID: 1416 RVA: 0x0001D5D7 File Offset: 0x0001B9D7
		public virtual float flexibleHeight
		{
			get
			{
				return this.m_FlexibleHeight;
			}
			set
			{
				if (SetPropertyUtility.SetStruct<float>(ref this.m_FlexibleHeight, value))
				{
					this.SetDirty();
				}
			}
		}

		// Token: 0x1700018A RID: 394
		// (get) Token: 0x06000589 RID: 1417 RVA: 0x0001D5F4 File Offset: 0x0001B9F4
		// (set) Token: 0x0600058A RID: 1418 RVA: 0x0001D60F File Offset: 0x0001BA0F
		public virtual int layoutPriority
		{
			get
			{
				return this.m_LayoutPriority;
			}
			set
			{
				if (SetPropertyUtility.SetStruct<int>(ref this.m_LayoutPriority, value))
				{
					this.SetDirty();
				}
			}
		}

		// Token: 0x0600058B RID: 1419 RVA: 0x0001D629 File Offset: 0x0001BA29
		protected override void OnEnable()
		{
			base.OnEnable();
			this.SetDirty();
		}

		// Token: 0x0600058C RID: 1420 RVA: 0x0001D638 File Offset: 0x0001BA38
		protected override void OnTransformParentChanged()
		{
			this.SetDirty();
		}

		// Token: 0x0600058D RID: 1421 RVA: 0x0001D641 File Offset: 0x0001BA41
		protected override void OnDisable()
		{
			this.SetDirty();
			base.OnDisable();
		}

		// Token: 0x0600058E RID: 1422 RVA: 0x0001D650 File Offset: 0x0001BA50
		protected override void OnDidApplyAnimationProperties()
		{
			this.SetDirty();
		}

		// Token: 0x0600058F RID: 1423 RVA: 0x0001D659 File Offset: 0x0001BA59
		protected override void OnBeforeTransformParentChanged()
		{
			this.SetDirty();
		}

		// Token: 0x06000590 RID: 1424 RVA: 0x0001D662 File Offset: 0x0001BA62
		protected void SetDirty()
		{
			if (this.IsActive())
			{
				LayoutRebuilder.MarkLayoutForRebuild(base.transform as RectTransform);
			}
		}

		// Token: 0x040002AE RID: 686
		[SerializeField]
		private bool m_IgnoreLayout = false;

		// Token: 0x040002AF RID: 687
		[SerializeField]
		private float m_MinWidth = -1f;

		// Token: 0x040002B0 RID: 688
		[SerializeField]
		private float m_MinHeight = -1f;

		// Token: 0x040002B1 RID: 689
		[SerializeField]
		private float m_PreferredWidth = -1f;

		// Token: 0x040002B2 RID: 690
		[SerializeField]
		private float m_PreferredHeight = -1f;

		// Token: 0x040002B3 RID: 691
		[SerializeField]
		private float m_FlexibleWidth = -1f;

		// Token: 0x040002B4 RID: 692
		[SerializeField]
		private float m_FlexibleHeight = -1f;

		// Token: 0x040002B5 RID: 693
		[SerializeField]
		private int m_LayoutPriority = 1;
	}
}
