﻿using System;

namespace UnityEngine.UI
{
	// Token: 0x0200004F RID: 79
	public interface IMaskable
	{
		// Token: 0x0600027B RID: 635
		void RecalculateMasking();
	}
}
