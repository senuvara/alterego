﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine.Events;

namespace UnityEngine.UI
{
	// Token: 0x020000A1 RID: 161
	internal static class ListPool<T>
	{
		// Token: 0x060005F9 RID: 1529 RVA: 0x0001E4CA File Offset: 0x0001C8CA
		private static void Clear(List<T> l)
		{
			l.Clear();
		}

		// Token: 0x060005FA RID: 1530 RVA: 0x0001E4D4 File Offset: 0x0001C8D4
		public static List<T> Get()
		{
			return ListPool<T>.s_ListPool.Get();
		}

		// Token: 0x060005FB RID: 1531 RVA: 0x0001E4F3 File Offset: 0x0001C8F3
		public static void Release(List<T> toRelease)
		{
			ListPool<T>.s_ListPool.Release(toRelease);
		}

		// Token: 0x060005FC RID: 1532 RVA: 0x0001E501 File Offset: 0x0001C901
		// Note: this type is marked as 'beforefieldinit'.
		static ListPool()
		{
			UnityAction<List<T>> actionOnGet = null;
			if (ListPool<T>.<>f__mg$cache0 == null)
			{
				ListPool<T>.<>f__mg$cache0 = new UnityAction<List<T>>(ListPool<T>.Clear);
			}
			ListPool<T>.s_ListPool = new ObjectPool<List<T>>(actionOnGet, ListPool<T>.<>f__mg$cache0);
		}

		// Token: 0x040002D1 RID: 721
		private static readonly ObjectPool<List<T>> s_ListPool;

		// Token: 0x040002D2 RID: 722
		[CompilerGenerated]
		private static UnityAction<List<T>> <>f__mg$cache0;
	}
}
