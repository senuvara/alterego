﻿using System;
using UnityEngine.EventSystems;

namespace UnityEngine.UI
{
	// Token: 0x02000087 RID: 135
	[AddComponentMenu("Layout/Aspect Ratio Fitter", 142)]
	[ExecuteAlways]
	[RequireComponent(typeof(RectTransform))]
	[DisallowMultipleComponent]
	public class AspectRatioFitter : UIBehaviour, ILayoutSelfController, ILayoutController
	{
		// Token: 0x0600050B RID: 1291 RVA: 0x0001B346 File Offset: 0x00019746
		protected AspectRatioFitter()
		{
		}

		// Token: 0x17000160 RID: 352
		// (get) Token: 0x0600050C RID: 1292 RVA: 0x0001B368 File Offset: 0x00019768
		// (set) Token: 0x0600050D RID: 1293 RVA: 0x0001B383 File Offset: 0x00019783
		public AspectRatioFitter.AspectMode aspectMode
		{
			get
			{
				return this.m_AspectMode;
			}
			set
			{
				if (SetPropertyUtility.SetStruct<AspectRatioFitter.AspectMode>(ref this.m_AspectMode, value))
				{
					this.SetDirty();
				}
			}
		}

		// Token: 0x17000161 RID: 353
		// (get) Token: 0x0600050E RID: 1294 RVA: 0x0001B3A0 File Offset: 0x000197A0
		// (set) Token: 0x0600050F RID: 1295 RVA: 0x0001B3BB File Offset: 0x000197BB
		public float aspectRatio
		{
			get
			{
				return this.m_AspectRatio;
			}
			set
			{
				if (SetPropertyUtility.SetStruct<float>(ref this.m_AspectRatio, value))
				{
					this.SetDirty();
				}
			}
		}

		// Token: 0x17000162 RID: 354
		// (get) Token: 0x06000510 RID: 1296 RVA: 0x0001B3D8 File Offset: 0x000197D8
		private RectTransform rectTransform
		{
			get
			{
				if (this.m_Rect == null)
				{
					this.m_Rect = base.GetComponent<RectTransform>();
				}
				return this.m_Rect;
			}
		}

		// Token: 0x06000511 RID: 1297 RVA: 0x0001B410 File Offset: 0x00019810
		protected override void OnEnable()
		{
			base.OnEnable();
			this.SetDirty();
		}

		// Token: 0x06000512 RID: 1298 RVA: 0x0001B41F File Offset: 0x0001981F
		protected override void OnDisable()
		{
			this.m_Tracker.Clear();
			LayoutRebuilder.MarkLayoutForRebuild(this.rectTransform);
			base.OnDisable();
		}

		// Token: 0x06000513 RID: 1299 RVA: 0x0001B43E File Offset: 0x0001983E
		protected virtual void Update()
		{
			if (this.m_DelayedSetDirty)
			{
				this.m_DelayedSetDirty = false;
				this.SetDirty();
			}
		}

		// Token: 0x06000514 RID: 1300 RVA: 0x0001B45B File Offset: 0x0001985B
		protected override void OnRectTransformDimensionsChange()
		{
			this.UpdateRect();
		}

		// Token: 0x06000515 RID: 1301 RVA: 0x0001B464 File Offset: 0x00019864
		private void UpdateRect()
		{
			if (this.IsActive())
			{
				this.m_Tracker.Clear();
				switch (this.m_AspectMode)
				{
				case AspectRatioFitter.AspectMode.WidthControlsHeight:
					this.m_Tracker.Add(this, this.rectTransform, DrivenTransformProperties.SizeDeltaY);
					this.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, this.rectTransform.rect.width / this.m_AspectRatio);
					break;
				case AspectRatioFitter.AspectMode.HeightControlsWidth:
					this.m_Tracker.Add(this, this.rectTransform, DrivenTransformProperties.SizeDeltaX);
					this.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, this.rectTransform.rect.height * this.m_AspectRatio);
					break;
				case AspectRatioFitter.AspectMode.FitInParent:
				case AspectRatioFitter.AspectMode.EnvelopeParent:
				{
					this.m_Tracker.Add(this, this.rectTransform, DrivenTransformProperties.AnchoredPositionX | DrivenTransformProperties.AnchoredPositionY | DrivenTransformProperties.AnchorMinX | DrivenTransformProperties.AnchorMinY | DrivenTransformProperties.AnchorMaxX | DrivenTransformProperties.AnchorMaxY | DrivenTransformProperties.SizeDeltaX | DrivenTransformProperties.SizeDeltaY);
					this.rectTransform.anchorMin = Vector2.zero;
					this.rectTransform.anchorMax = Vector2.one;
					this.rectTransform.anchoredPosition = Vector2.zero;
					Vector2 zero = Vector2.zero;
					Vector2 parentSize = this.GetParentSize();
					if (parentSize.y * this.aspectRatio < parentSize.x ^ this.m_AspectMode == AspectRatioFitter.AspectMode.FitInParent)
					{
						zero.y = this.GetSizeDeltaToProduceSize(parentSize.x / this.aspectRatio, 1);
					}
					else
					{
						zero.x = this.GetSizeDeltaToProduceSize(parentSize.y * this.aspectRatio, 0);
					}
					this.rectTransform.sizeDelta = zero;
					break;
				}
				}
			}
		}

		// Token: 0x06000516 RID: 1302 RVA: 0x0001B608 File Offset: 0x00019A08
		private float GetSizeDeltaToProduceSize(float size, int axis)
		{
			return size - this.GetParentSize()[axis] * (this.rectTransform.anchorMax[axis] - this.rectTransform.anchorMin[axis]);
		}

		// Token: 0x06000517 RID: 1303 RVA: 0x0001B658 File Offset: 0x00019A58
		private Vector2 GetParentSize()
		{
			RectTransform rectTransform = this.rectTransform.parent as RectTransform;
			Vector2 result;
			if (!rectTransform)
			{
				result = Vector2.zero;
			}
			else
			{
				result = rectTransform.rect.size;
			}
			return result;
		}

		// Token: 0x06000518 RID: 1304 RVA: 0x0001B6A2 File Offset: 0x00019AA2
		public virtual void SetLayoutHorizontal()
		{
		}

		// Token: 0x06000519 RID: 1305 RVA: 0x0001B6A5 File Offset: 0x00019AA5
		public virtual void SetLayoutVertical()
		{
		}

		// Token: 0x0600051A RID: 1306 RVA: 0x0001B6A8 File Offset: 0x00019AA8
		protected void SetDirty()
		{
			this.UpdateRect();
		}

		// Token: 0x04000268 RID: 616
		[SerializeField]
		private AspectRatioFitter.AspectMode m_AspectMode = AspectRatioFitter.AspectMode.None;

		// Token: 0x04000269 RID: 617
		[SerializeField]
		private float m_AspectRatio = 1f;

		// Token: 0x0400026A RID: 618
		[NonSerialized]
		private RectTransform m_Rect;

		// Token: 0x0400026B RID: 619
		private bool m_DelayedSetDirty = false;

		// Token: 0x0400026C RID: 620
		private DrivenRectTransformTracker m_Tracker;

		// Token: 0x02000088 RID: 136
		public enum AspectMode
		{
			// Token: 0x0400026E RID: 622
			None,
			// Token: 0x0400026F RID: 623
			WidthControlsHeight,
			// Token: 0x04000270 RID: 624
			HeightControlsWidth,
			// Token: 0x04000271 RID: 625
			FitInParent,
			// Token: 0x04000272 RID: 626
			EnvelopeParent
		}
	}
}
