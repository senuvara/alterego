﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace UnityEngine.UI.Collections
{
	// Token: 0x020000A0 RID: 160
	internal class IndexedSet<T> : IList<T>, ICollection<T>, IEnumerable<T>, IEnumerable
	{
		// Token: 0x060005E7 RID: 1511 RVA: 0x0001E179 File Offset: 0x0001C579
		public IndexedSet()
		{
		}

		// Token: 0x060005E8 RID: 1512 RVA: 0x0001E197 File Offset: 0x0001C597
		public void Add(T item)
		{
			this.m_List.Add(item);
			this.m_Dictionary.Add(item, this.m_List.Count - 1);
		}

		// Token: 0x060005E9 RID: 1513 RVA: 0x0001E1C0 File Offset: 0x0001C5C0
		public bool AddUnique(T item)
		{
			bool result;
			if (this.m_Dictionary.ContainsKey(item))
			{
				result = false;
			}
			else
			{
				this.m_List.Add(item);
				this.m_Dictionary.Add(item, this.m_List.Count - 1);
				result = true;
			}
			return result;
		}

		// Token: 0x060005EA RID: 1514 RVA: 0x0001E214 File Offset: 0x0001C614
		public bool Remove(T item)
		{
			int index = -1;
			bool result;
			if (!this.m_Dictionary.TryGetValue(item, out index))
			{
				result = false;
			}
			else
			{
				this.RemoveAt(index);
				result = true;
			}
			return result;
		}

		// Token: 0x060005EB RID: 1515 RVA: 0x0001E24D File Offset: 0x0001C64D
		public IEnumerator<T> GetEnumerator()
		{
			throw new NotImplementedException();
		}

		// Token: 0x060005EC RID: 1516 RVA: 0x0001E258 File Offset: 0x0001C658
		IEnumerator IEnumerable.GetEnumerator()
		{
			return this.GetEnumerator();
		}

		// Token: 0x060005ED RID: 1517 RVA: 0x0001E273 File Offset: 0x0001C673
		public void Clear()
		{
			this.m_List.Clear();
			this.m_Dictionary.Clear();
		}

		// Token: 0x060005EE RID: 1518 RVA: 0x0001E28C File Offset: 0x0001C68C
		public bool Contains(T item)
		{
			return this.m_Dictionary.ContainsKey(item);
		}

		// Token: 0x060005EF RID: 1519 RVA: 0x0001E2AD File Offset: 0x0001C6AD
		public void CopyTo(T[] array, int arrayIndex)
		{
			this.m_List.CopyTo(array, arrayIndex);
		}

		// Token: 0x17000198 RID: 408
		// (get) Token: 0x060005F0 RID: 1520 RVA: 0x0001E2C0 File Offset: 0x0001C6C0
		public int Count
		{
			get
			{
				return this.m_List.Count;
			}
		}

		// Token: 0x17000199 RID: 409
		// (get) Token: 0x060005F1 RID: 1521 RVA: 0x0001E2E0 File Offset: 0x0001C6E0
		public bool IsReadOnly
		{
			get
			{
				return false;
			}
		}

		// Token: 0x060005F2 RID: 1522 RVA: 0x0001E2F8 File Offset: 0x0001C6F8
		public int IndexOf(T item)
		{
			int result = -1;
			this.m_Dictionary.TryGetValue(item, out result);
			return result;
		}

		// Token: 0x060005F3 RID: 1523 RVA: 0x0001E31F File Offset: 0x0001C71F
		public void Insert(int index, T item)
		{
			throw new NotSupportedException("Random Insertion is semantically invalid, since this structure does not guarantee ordering.");
		}

		// Token: 0x060005F4 RID: 1524 RVA: 0x0001E32C File Offset: 0x0001C72C
		public void RemoveAt(int index)
		{
			T key = this.m_List[index];
			this.m_Dictionary.Remove(key);
			if (index == this.m_List.Count - 1)
			{
				this.m_List.RemoveAt(index);
			}
			else
			{
				int index2 = this.m_List.Count - 1;
				T t = this.m_List[index2];
				this.m_List[index] = t;
				this.m_Dictionary[t] = index;
				this.m_List.RemoveAt(index2);
			}
		}

		// Token: 0x1700019A RID: 410
		public T this[int index]
		{
			get
			{
				return this.m_List[index];
			}
			set
			{
				T key = this.m_List[index];
				this.m_Dictionary.Remove(key);
				this.m_List[index] = value;
				this.m_Dictionary.Add(key, index);
			}
		}

		// Token: 0x060005F7 RID: 1527 RVA: 0x0001E424 File Offset: 0x0001C824
		public void RemoveAll(Predicate<T> match)
		{
			int i = 0;
			while (i < this.m_List.Count)
			{
				T t = this.m_List[i];
				if (match(t))
				{
					this.Remove(t);
				}
				else
				{
					i++;
				}
			}
		}

		// Token: 0x060005F8 RID: 1528 RVA: 0x0001E478 File Offset: 0x0001C878
		public void Sort(Comparison<T> sortLayoutFunction)
		{
			this.m_List.Sort(sortLayoutFunction);
			for (int i = 0; i < this.m_List.Count; i++)
			{
				T key = this.m_List[i];
				this.m_Dictionary[key] = i;
			}
		}

		// Token: 0x040002CF RID: 719
		private readonly List<T> m_List = new List<T>();

		// Token: 0x040002D0 RID: 720
		private Dictionary<T, int> m_Dictionary = new Dictionary<T, int>();
	}
}
