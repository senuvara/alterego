﻿using System;
using UnityEngine.UI.Collections;

namespace UnityEngine.UI
{
	// Token: 0x02000082 RID: 130
	public class ClipperRegistry
	{
		// Token: 0x060004FC RID: 1276 RVA: 0x0001AF94 File Offset: 0x00019394
		protected ClipperRegistry()
		{
		}

		// Token: 0x1700015D RID: 349
		// (get) Token: 0x060004FD RID: 1277 RVA: 0x0001AFB4 File Offset: 0x000193B4
		public static ClipperRegistry instance
		{
			get
			{
				if (ClipperRegistry.s_Instance == null)
				{
					ClipperRegistry.s_Instance = new ClipperRegistry();
				}
				return ClipperRegistry.s_Instance;
			}
		}

		// Token: 0x060004FE RID: 1278 RVA: 0x0001AFE4 File Offset: 0x000193E4
		public void Cull()
		{
			for (int i = 0; i < this.m_Clippers.Count; i++)
			{
				this.m_Clippers[i].PerformClipping();
			}
		}

		// Token: 0x060004FF RID: 1279 RVA: 0x0001B021 File Offset: 0x00019421
		public static void Register(IClipper c)
		{
			if (c != null)
			{
				ClipperRegistry.instance.m_Clippers.AddUnique(c);
			}
		}

		// Token: 0x06000500 RID: 1280 RVA: 0x0001B040 File Offset: 0x00019440
		public static void Unregister(IClipper c)
		{
			ClipperRegistry.instance.m_Clippers.Remove(c);
		}

		// Token: 0x04000264 RID: 612
		private static ClipperRegistry s_Instance;

		// Token: 0x04000265 RID: 613
		private readonly IndexedSet<IClipper> m_Clippers = new IndexedSet<IClipper>();
	}
}
