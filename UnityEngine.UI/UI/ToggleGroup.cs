﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using UnityEngine.EventSystems;

namespace UnityEngine.UI
{
	// Token: 0x02000081 RID: 129
	[AddComponentMenu("UI/Toggle Group", 32)]
	[DisallowMultipleComponent]
	public class ToggleGroup : UIBehaviour
	{
		// Token: 0x060004F0 RID: 1264 RVA: 0x0001AD60 File Offset: 0x00019160
		protected ToggleGroup()
		{
		}

		// Token: 0x1700015C RID: 348
		// (get) Token: 0x060004F1 RID: 1265 RVA: 0x0001AD7C File Offset: 0x0001917C
		// (set) Token: 0x060004F2 RID: 1266 RVA: 0x0001AD97 File Offset: 0x00019197
		public bool allowSwitchOff
		{
			get
			{
				return this.m_AllowSwitchOff;
			}
			set
			{
				this.m_AllowSwitchOff = value;
			}
		}

		// Token: 0x060004F3 RID: 1267 RVA: 0x0001ADA1 File Offset: 0x000191A1
		private void ValidateToggleIsInGroup(Toggle toggle)
		{
			if (toggle == null || !this.m_Toggles.Contains(toggle))
			{
				throw new ArgumentException(string.Format("Toggle {0} is not part of ToggleGroup {1}", new object[]
				{
					toggle,
					this
				}));
			}
		}

		// Token: 0x060004F4 RID: 1268 RVA: 0x0001ADE0 File Offset: 0x000191E0
		public void NotifyToggleOn(Toggle toggle)
		{
			this.ValidateToggleIsInGroup(toggle);
			for (int i = 0; i < this.m_Toggles.Count; i++)
			{
				if (!(this.m_Toggles[i] == toggle))
				{
					this.m_Toggles[i].isOn = false;
				}
			}
		}

		// Token: 0x060004F5 RID: 1269 RVA: 0x0001AE41 File Offset: 0x00019241
		public void UnregisterToggle(Toggle toggle)
		{
			if (this.m_Toggles.Contains(toggle))
			{
				this.m_Toggles.Remove(toggle);
			}
		}

		// Token: 0x060004F6 RID: 1270 RVA: 0x0001AE62 File Offset: 0x00019262
		public void RegisterToggle(Toggle toggle)
		{
			if (!this.m_Toggles.Contains(toggle))
			{
				this.m_Toggles.Add(toggle);
			}
		}

		// Token: 0x060004F7 RID: 1271 RVA: 0x0001AE84 File Offset: 0x00019284
		public bool AnyTogglesOn()
		{
			return this.m_Toggles.Find((Toggle x) => x.isOn) != null;
		}

		// Token: 0x060004F8 RID: 1272 RVA: 0x0001AEC8 File Offset: 0x000192C8
		public IEnumerable<Toggle> ActiveToggles()
		{
			return from x in this.m_Toggles
			where x.isOn
			select x;
		}

		// Token: 0x060004F9 RID: 1273 RVA: 0x0001AF08 File Offset: 0x00019308
		public void SetAllTogglesOff()
		{
			bool allowSwitchOff = this.m_AllowSwitchOff;
			this.m_AllowSwitchOff = true;
			for (int i = 0; i < this.m_Toggles.Count; i++)
			{
				this.m_Toggles[i].isOn = false;
			}
			this.m_AllowSwitchOff = allowSwitchOff;
		}

		// Token: 0x060004FA RID: 1274 RVA: 0x0001AF5C File Offset: 0x0001935C
		[CompilerGenerated]
		private static bool <AnyTogglesOn>m__0(Toggle x)
		{
			return x.isOn;
		}

		// Token: 0x060004FB RID: 1275 RVA: 0x0001AF78 File Offset: 0x00019378
		[CompilerGenerated]
		private static bool <ActiveToggles>m__1(Toggle x)
		{
			return x.isOn;
		}

		// Token: 0x04000260 RID: 608
		[SerializeField]
		private bool m_AllowSwitchOff = false;

		// Token: 0x04000261 RID: 609
		private List<Toggle> m_Toggles = new List<Toggle>();

		// Token: 0x04000262 RID: 610
		[CompilerGenerated]
		private static Predicate<Toggle> <>f__am$cache0;

		// Token: 0x04000263 RID: 611
		[CompilerGenerated]
		private static Func<Toggle, bool> <>f__am$cache1;
	}
}
