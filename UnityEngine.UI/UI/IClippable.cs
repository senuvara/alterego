﻿using System;

namespace UnityEngine.UI
{
	// Token: 0x02000085 RID: 133
	public interface IClippable
	{
		// Token: 0x1700015E RID: 350
		// (get) Token: 0x06000504 RID: 1284
		GameObject gameObject { get; }

		// Token: 0x06000505 RID: 1285
		void RecalculateClipping();

		// Token: 0x1700015F RID: 351
		// (get) Token: 0x06000506 RID: 1286
		RectTransform rectTransform { get; }

		// Token: 0x06000507 RID: 1287
		void Cull(Rect clipRect, bool validRect);

		// Token: 0x06000508 RID: 1288
		void SetClipRect(Rect value, bool validRect);
	}
}
