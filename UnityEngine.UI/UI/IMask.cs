﻿using System;
using System.ComponentModel;

namespace UnityEngine.UI
{
	// Token: 0x0200004E RID: 78
	[EditorBrowsable(EditorBrowsableState.Never)]
	[Obsolete("Not supported anymore.", true)]
	public interface IMask
	{
		// Token: 0x06000279 RID: 633
		bool Enabled();

		// Token: 0x170000A4 RID: 164
		// (get) Token: 0x0600027A RID: 634
		RectTransform rectTransform { get; }
	}
}
