﻿using System;

namespace UnityEngine.UI
{
	// Token: 0x0200007A RID: 122
	[Serializable]
	public struct SpriteState : IEquatable<SpriteState>
	{
		// Token: 0x1700013F RID: 319
		// (get) Token: 0x0600049C RID: 1180 RVA: 0x00019750 File Offset: 0x00017B50
		// (set) Token: 0x0600049D RID: 1181 RVA: 0x0001976B File Offset: 0x00017B6B
		public Sprite highlightedSprite
		{
			get
			{
				return this.m_HighlightedSprite;
			}
			set
			{
				this.m_HighlightedSprite = value;
			}
		}

		// Token: 0x17000140 RID: 320
		// (get) Token: 0x0600049E RID: 1182 RVA: 0x00019778 File Offset: 0x00017B78
		// (set) Token: 0x0600049F RID: 1183 RVA: 0x00019793 File Offset: 0x00017B93
		public Sprite pressedSprite
		{
			get
			{
				return this.m_PressedSprite;
			}
			set
			{
				this.m_PressedSprite = value;
			}
		}

		// Token: 0x17000141 RID: 321
		// (get) Token: 0x060004A0 RID: 1184 RVA: 0x000197A0 File Offset: 0x00017BA0
		// (set) Token: 0x060004A1 RID: 1185 RVA: 0x000197BB File Offset: 0x00017BBB
		public Sprite disabledSprite
		{
			get
			{
				return this.m_DisabledSprite;
			}
			set
			{
				this.m_DisabledSprite = value;
			}
		}

		// Token: 0x060004A2 RID: 1186 RVA: 0x000197C8 File Offset: 0x00017BC8
		public bool Equals(SpriteState other)
		{
			return this.highlightedSprite == other.highlightedSprite && this.pressedSprite == other.pressedSprite && this.disabledSprite == other.disabledSprite;
		}

		// Token: 0x04000243 RID: 579
		[SerializeField]
		private Sprite m_HighlightedSprite;

		// Token: 0x04000244 RID: 580
		[SerializeField]
		private Sprite m_PressedSprite;

		// Token: 0x04000245 RID: 581
		[SerializeField]
		private Sprite m_DisabledSprite;
	}
}
