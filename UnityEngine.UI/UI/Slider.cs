﻿using System;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace UnityEngine.UI
{
	// Token: 0x02000076 RID: 118
	[AddComponentMenu("UI/Slider", 33)]
	[RequireComponent(typeof(RectTransform))]
	public class Slider : Selectable, IDragHandler, IInitializePotentialDragHandler, ICanvasElement, IEventSystemHandler
	{
		// Token: 0x0600046D RID: 1133 RVA: 0x000189F0 File Offset: 0x00016DF0
		protected Slider()
		{
		}

		// Token: 0x17000133 RID: 307
		// (get) Token: 0x0600046E RID: 1134 RVA: 0x00018A40 File Offset: 0x00016E40
		// (set) Token: 0x0600046F RID: 1135 RVA: 0x00018A5B File Offset: 0x00016E5B
		public RectTransform fillRect
		{
			get
			{
				return this.m_FillRect;
			}
			set
			{
				if (SetPropertyUtility.SetClass<RectTransform>(ref this.m_FillRect, value))
				{
					this.UpdateCachedReferences();
					this.UpdateVisuals();
				}
			}
		}

		// Token: 0x17000134 RID: 308
		// (get) Token: 0x06000470 RID: 1136 RVA: 0x00018A80 File Offset: 0x00016E80
		// (set) Token: 0x06000471 RID: 1137 RVA: 0x00018A9B File Offset: 0x00016E9B
		public RectTransform handleRect
		{
			get
			{
				return this.m_HandleRect;
			}
			set
			{
				if (SetPropertyUtility.SetClass<RectTransform>(ref this.m_HandleRect, value))
				{
					this.UpdateCachedReferences();
					this.UpdateVisuals();
				}
			}
		}

		// Token: 0x17000135 RID: 309
		// (get) Token: 0x06000472 RID: 1138 RVA: 0x00018AC0 File Offset: 0x00016EC0
		// (set) Token: 0x06000473 RID: 1139 RVA: 0x00018ADB File Offset: 0x00016EDB
		public Slider.Direction direction
		{
			get
			{
				return this.m_Direction;
			}
			set
			{
				if (SetPropertyUtility.SetStruct<Slider.Direction>(ref this.m_Direction, value))
				{
					this.UpdateVisuals();
				}
			}
		}

		// Token: 0x17000136 RID: 310
		// (get) Token: 0x06000474 RID: 1140 RVA: 0x00018AF8 File Offset: 0x00016EF8
		// (set) Token: 0x06000475 RID: 1141 RVA: 0x00018B13 File Offset: 0x00016F13
		public float minValue
		{
			get
			{
				return this.m_MinValue;
			}
			set
			{
				if (SetPropertyUtility.SetStruct<float>(ref this.m_MinValue, value))
				{
					this.Set(this.m_Value);
					this.UpdateVisuals();
				}
			}
		}

		// Token: 0x17000137 RID: 311
		// (get) Token: 0x06000476 RID: 1142 RVA: 0x00018B3C File Offset: 0x00016F3C
		// (set) Token: 0x06000477 RID: 1143 RVA: 0x00018B57 File Offset: 0x00016F57
		public float maxValue
		{
			get
			{
				return this.m_MaxValue;
			}
			set
			{
				if (SetPropertyUtility.SetStruct<float>(ref this.m_MaxValue, value))
				{
					this.Set(this.m_Value);
					this.UpdateVisuals();
				}
			}
		}

		// Token: 0x17000138 RID: 312
		// (get) Token: 0x06000478 RID: 1144 RVA: 0x00018B80 File Offset: 0x00016F80
		// (set) Token: 0x06000479 RID: 1145 RVA: 0x00018B9B File Offset: 0x00016F9B
		public bool wholeNumbers
		{
			get
			{
				return this.m_WholeNumbers;
			}
			set
			{
				if (SetPropertyUtility.SetStruct<bool>(ref this.m_WholeNumbers, value))
				{
					this.Set(this.m_Value);
					this.UpdateVisuals();
				}
			}
		}

		// Token: 0x17000139 RID: 313
		// (get) Token: 0x0600047A RID: 1146 RVA: 0x00018BC4 File Offset: 0x00016FC4
		// (set) Token: 0x0600047B RID: 1147 RVA: 0x00018BFB File Offset: 0x00016FFB
		public virtual float value
		{
			get
			{
				float result;
				if (this.wholeNumbers)
				{
					result = Mathf.Round(this.m_Value);
				}
				else
				{
					result = this.m_Value;
				}
				return result;
			}
			set
			{
				this.Set(value);
			}
		}

		// Token: 0x1700013A RID: 314
		// (get) Token: 0x0600047C RID: 1148 RVA: 0x00018C08 File Offset: 0x00017008
		// (set) Token: 0x0600047D RID: 1149 RVA: 0x00018C55 File Offset: 0x00017055
		public float normalizedValue
		{
			get
			{
				float result;
				if (Mathf.Approximately(this.minValue, this.maxValue))
				{
					result = 0f;
				}
				else
				{
					result = Mathf.InverseLerp(this.minValue, this.maxValue, this.value);
				}
				return result;
			}
			set
			{
				this.value = Mathf.Lerp(this.minValue, this.maxValue, value);
			}
		}

		// Token: 0x1700013B RID: 315
		// (get) Token: 0x0600047E RID: 1150 RVA: 0x00018C70 File Offset: 0x00017070
		// (set) Token: 0x0600047F RID: 1151 RVA: 0x00018C8B File Offset: 0x0001708B
		public Slider.SliderEvent onValueChanged
		{
			get
			{
				return this.m_OnValueChanged;
			}
			set
			{
				this.m_OnValueChanged = value;
			}
		}

		// Token: 0x1700013C RID: 316
		// (get) Token: 0x06000480 RID: 1152 RVA: 0x00018C98 File Offset: 0x00017098
		private float stepSize
		{
			get
			{
				return (!this.wholeNumbers) ? ((this.maxValue - this.minValue) * 0.1f) : 1f;
			}
		}

		// Token: 0x06000481 RID: 1153 RVA: 0x00018CD5 File Offset: 0x000170D5
		public virtual void Rebuild(CanvasUpdate executing)
		{
		}

		// Token: 0x06000482 RID: 1154 RVA: 0x00018CD8 File Offset: 0x000170D8
		public virtual void LayoutComplete()
		{
		}

		// Token: 0x06000483 RID: 1155 RVA: 0x00018CDB File Offset: 0x000170DB
		public virtual void GraphicUpdateComplete()
		{
		}

		// Token: 0x06000484 RID: 1156 RVA: 0x00018CDE File Offset: 0x000170DE
		protected override void OnEnable()
		{
			base.OnEnable();
			this.UpdateCachedReferences();
			this.Set(this.m_Value, false);
			this.UpdateVisuals();
		}

		// Token: 0x06000485 RID: 1157 RVA: 0x00018D00 File Offset: 0x00017100
		protected override void OnDisable()
		{
			this.m_Tracker.Clear();
			base.OnDisable();
		}

		// Token: 0x06000486 RID: 1158 RVA: 0x00018D14 File Offset: 0x00017114
		protected override void OnDidApplyAnimationProperties()
		{
			this.m_Value = this.ClampValue(this.m_Value);
			float num = this.normalizedValue;
			if (this.m_FillContainerRect != null)
			{
				if (this.m_FillImage != null && this.m_FillImage.type == Image.Type.Filled)
				{
					num = this.m_FillImage.fillAmount;
				}
				else
				{
					num = ((!this.reverseValue) ? this.m_FillRect.anchorMax[(int)this.axis] : (1f - this.m_FillRect.anchorMin[(int)this.axis]));
				}
			}
			else if (this.m_HandleContainerRect != null)
			{
				num = ((!this.reverseValue) ? this.m_HandleRect.anchorMin[(int)this.axis] : (1f - this.m_HandleRect.anchorMin[(int)this.axis]));
			}
			this.UpdateVisuals();
			if (num != this.normalizedValue)
			{
				UISystemProfilerApi.AddMarker("Slider.value", this);
				this.onValueChanged.Invoke(this.m_Value);
			}
		}

		// Token: 0x06000487 RID: 1159 RVA: 0x00018E5C File Offset: 0x0001725C
		private void UpdateCachedReferences()
		{
			if (this.m_FillRect && this.m_FillRect != (RectTransform)base.transform)
			{
				this.m_FillTransform = this.m_FillRect.transform;
				this.m_FillImage = this.m_FillRect.GetComponent<Image>();
				if (this.m_FillTransform.parent != null)
				{
					this.m_FillContainerRect = this.m_FillTransform.parent.GetComponent<RectTransform>();
				}
			}
			else
			{
				this.m_FillRect = null;
				this.m_FillContainerRect = null;
				this.m_FillImage = null;
			}
			if (this.m_HandleRect && this.m_HandleRect != (RectTransform)base.transform)
			{
				this.m_HandleTransform = this.m_HandleRect.transform;
				if (this.m_HandleTransform.parent != null)
				{
					this.m_HandleContainerRect = this.m_HandleTransform.parent.GetComponent<RectTransform>();
				}
			}
			else
			{
				this.m_HandleRect = null;
				this.m_HandleContainerRect = null;
			}
		}

		// Token: 0x06000488 RID: 1160 RVA: 0x00018F80 File Offset: 0x00017380
		private float ClampValue(float input)
		{
			float num = Mathf.Clamp(input, this.minValue, this.maxValue);
			if (this.wholeNumbers)
			{
				num = Mathf.Round(num);
			}
			return num;
		}

		// Token: 0x06000489 RID: 1161 RVA: 0x00018FBB File Offset: 0x000173BB
		private void Set(float input)
		{
			this.Set(input, true);
		}

		// Token: 0x0600048A RID: 1162 RVA: 0x00018FC8 File Offset: 0x000173C8
		protected virtual void Set(float input, bool sendCallback)
		{
			float num = this.ClampValue(input);
			if (this.m_Value != num)
			{
				this.m_Value = num;
				this.UpdateVisuals();
				if (sendCallback)
				{
					UISystemProfilerApi.AddMarker("Slider.value", this);
					this.m_OnValueChanged.Invoke(num);
				}
			}
		}

		// Token: 0x0600048B RID: 1163 RVA: 0x0001901B File Offset: 0x0001741B
		protected override void OnRectTransformDimensionsChange()
		{
			base.OnRectTransformDimensionsChange();
			if (this.IsActive())
			{
				this.UpdateVisuals();
			}
		}

		// Token: 0x1700013D RID: 317
		// (get) Token: 0x0600048C RID: 1164 RVA: 0x0001903C File Offset: 0x0001743C
		private Slider.Axis axis
		{
			get
			{
				return (this.m_Direction != Slider.Direction.LeftToRight && this.m_Direction != Slider.Direction.RightToLeft) ? Slider.Axis.Vertical : Slider.Axis.Horizontal;
			}
		}

		// Token: 0x1700013E RID: 318
		// (get) Token: 0x0600048D RID: 1165 RVA: 0x00019070 File Offset: 0x00017470
		private bool reverseValue
		{
			get
			{
				return this.m_Direction == Slider.Direction.RightToLeft || this.m_Direction == Slider.Direction.TopToBottom;
			}
		}

		// Token: 0x0600048E RID: 1166 RVA: 0x000190A0 File Offset: 0x000174A0
		private void UpdateVisuals()
		{
			this.m_Tracker.Clear();
			if (this.m_FillContainerRect != null)
			{
				this.m_Tracker.Add(this, this.m_FillRect, DrivenTransformProperties.Anchors);
				Vector2 zero = Vector2.zero;
				Vector2 one = Vector2.one;
				if (this.m_FillImage != null && this.m_FillImage.type == Image.Type.Filled)
				{
					this.m_FillImage.fillAmount = this.normalizedValue;
				}
				else if (this.reverseValue)
				{
					zero[(int)this.axis] = 1f - this.normalizedValue;
				}
				else
				{
					one[(int)this.axis] = this.normalizedValue;
				}
				this.m_FillRect.anchorMin = zero;
				this.m_FillRect.anchorMax = one;
			}
			if (this.m_HandleContainerRect != null)
			{
				this.m_Tracker.Add(this, this.m_HandleRect, DrivenTransformProperties.Anchors);
				Vector2 zero2 = Vector2.zero;
				Vector2 one2 = Vector2.one;
				int axis = (int)this.axis;
				float value = (!this.reverseValue) ? this.normalizedValue : (1f - this.normalizedValue);
				one2[(int)this.axis] = value;
				zero2[axis] = value;
				this.m_HandleRect.anchorMin = zero2;
				this.m_HandleRect.anchorMax = one2;
			}
		}

		// Token: 0x0600048F RID: 1167 RVA: 0x00019210 File Offset: 0x00017610
		private void UpdateDrag(PointerEventData eventData, Camera cam)
		{
			RectTransform rectTransform = this.m_HandleContainerRect ?? this.m_FillContainerRect;
			if (rectTransform != null && rectTransform.rect.size[(int)this.axis] > 0f)
			{
				Vector2 a;
				if (RectTransformUtility.ScreenPointToLocalPointInRectangle(rectTransform, eventData.position, cam, out a))
				{
					a -= rectTransform.rect.position;
					float num = Mathf.Clamp01((a - this.m_Offset)[(int)this.axis] / rectTransform.rect.size[(int)this.axis]);
					this.normalizedValue = ((!this.reverseValue) ? num : (1f - num));
				}
			}
		}

		// Token: 0x06000490 RID: 1168 RVA: 0x000192F8 File Offset: 0x000176F8
		private bool MayDrag(PointerEventData eventData)
		{
			return this.IsActive() && this.IsInteractable() && eventData.button == PointerEventData.InputButton.Left;
		}

		// Token: 0x06000491 RID: 1169 RVA: 0x00019330 File Offset: 0x00017730
		public override void OnPointerDown(PointerEventData eventData)
		{
			if (this.MayDrag(eventData))
			{
				base.OnPointerDown(eventData);
				this.m_Offset = Vector2.zero;
				if (this.m_HandleContainerRect != null && RectTransformUtility.RectangleContainsScreenPoint(this.m_HandleRect, eventData.position, eventData.enterEventCamera))
				{
					Vector2 offset;
					if (RectTransformUtility.ScreenPointToLocalPointInRectangle(this.m_HandleRect, eventData.position, eventData.pressEventCamera, out offset))
					{
						this.m_Offset = offset;
					}
				}
				else
				{
					this.UpdateDrag(eventData, eventData.pressEventCamera);
				}
			}
		}

		// Token: 0x06000492 RID: 1170 RVA: 0x000193C9 File Offset: 0x000177C9
		public virtual void OnDrag(PointerEventData eventData)
		{
			if (this.MayDrag(eventData))
			{
				this.UpdateDrag(eventData, eventData.pressEventCamera);
			}
		}

		// Token: 0x06000493 RID: 1171 RVA: 0x000193EC File Offset: 0x000177EC
		public override void OnMove(AxisEventData eventData)
		{
			if (!this.IsActive() || !this.IsInteractable())
			{
				base.OnMove(eventData);
			}
			else
			{
				switch (eventData.moveDir)
				{
				case MoveDirection.Left:
					if (this.axis == Slider.Axis.Horizontal && this.FindSelectableOnLeft() == null)
					{
						this.Set((!this.reverseValue) ? (this.value - this.stepSize) : (this.value + this.stepSize));
					}
					else
					{
						base.OnMove(eventData);
					}
					break;
				case MoveDirection.Up:
					if (this.axis == Slider.Axis.Vertical && this.FindSelectableOnUp() == null)
					{
						this.Set((!this.reverseValue) ? (this.value + this.stepSize) : (this.value - this.stepSize));
					}
					else
					{
						base.OnMove(eventData);
					}
					break;
				case MoveDirection.Right:
					if (this.axis == Slider.Axis.Horizontal && this.FindSelectableOnRight() == null)
					{
						this.Set((!this.reverseValue) ? (this.value + this.stepSize) : (this.value - this.stepSize));
					}
					else
					{
						base.OnMove(eventData);
					}
					break;
				case MoveDirection.Down:
					if (this.axis == Slider.Axis.Vertical && this.FindSelectableOnDown() == null)
					{
						this.Set((!this.reverseValue) ? (this.value - this.stepSize) : (this.value + this.stepSize));
					}
					else
					{
						base.OnMove(eventData);
					}
					break;
				}
			}
		}

		// Token: 0x06000494 RID: 1172 RVA: 0x000195B8 File Offset: 0x000179B8
		public override Selectable FindSelectableOnLeft()
		{
			Selectable result;
			if (base.navigation.mode == Navigation.Mode.Automatic && this.axis == Slider.Axis.Horizontal)
			{
				result = null;
			}
			else
			{
				result = base.FindSelectableOnLeft();
			}
			return result;
		}

		// Token: 0x06000495 RID: 1173 RVA: 0x000195FC File Offset: 0x000179FC
		public override Selectable FindSelectableOnRight()
		{
			Selectable result;
			if (base.navigation.mode == Navigation.Mode.Automatic && this.axis == Slider.Axis.Horizontal)
			{
				result = null;
			}
			else
			{
				result = base.FindSelectableOnRight();
			}
			return result;
		}

		// Token: 0x06000496 RID: 1174 RVA: 0x00019640 File Offset: 0x00017A40
		public override Selectable FindSelectableOnUp()
		{
			Selectable result;
			if (base.navigation.mode == Navigation.Mode.Automatic && this.axis == Slider.Axis.Vertical)
			{
				result = null;
			}
			else
			{
				result = base.FindSelectableOnUp();
			}
			return result;
		}

		// Token: 0x06000497 RID: 1175 RVA: 0x00019684 File Offset: 0x00017A84
		public override Selectable FindSelectableOnDown()
		{
			Selectable result;
			if (base.navigation.mode == Navigation.Mode.Automatic && this.axis == Slider.Axis.Vertical)
			{
				result = null;
			}
			else
			{
				result = base.FindSelectableOnDown();
			}
			return result;
		}

		// Token: 0x06000498 RID: 1176 RVA: 0x000196C6 File Offset: 0x00017AC6
		public virtual void OnInitializePotentialDrag(PointerEventData eventData)
		{
			eventData.useDragThreshold = false;
		}

		// Token: 0x06000499 RID: 1177 RVA: 0x000196D0 File Offset: 0x00017AD0
		public void SetDirection(Slider.Direction direction, bool includeRectLayouts)
		{
			Slider.Axis axis = this.axis;
			bool reverseValue = this.reverseValue;
			this.direction = direction;
			if (includeRectLayouts)
			{
				if (this.axis != axis)
				{
					RectTransformUtility.FlipLayoutAxes(base.transform as RectTransform, true, true);
				}
				if (this.reverseValue != reverseValue)
				{
					RectTransformUtility.FlipLayoutOnAxis(base.transform as RectTransform, (int)this.axis, true, true);
				}
			}
		}

		// Token: 0x0600049A RID: 1178 RVA: 0x00019740 File Offset: 0x00017B40
		Transform ICanvasElement.get_transform()
		{
			return base.transform;
		}

		// Token: 0x0400022C RID: 556
		[SerializeField]
		private RectTransform m_FillRect;

		// Token: 0x0400022D RID: 557
		[SerializeField]
		private RectTransform m_HandleRect;

		// Token: 0x0400022E RID: 558
		[Space]
		[SerializeField]
		private Slider.Direction m_Direction = Slider.Direction.LeftToRight;

		// Token: 0x0400022F RID: 559
		[SerializeField]
		private float m_MinValue = 0f;

		// Token: 0x04000230 RID: 560
		[SerializeField]
		private float m_MaxValue = 1f;

		// Token: 0x04000231 RID: 561
		[SerializeField]
		private bool m_WholeNumbers = false;

		// Token: 0x04000232 RID: 562
		[SerializeField]
		protected float m_Value;

		// Token: 0x04000233 RID: 563
		[Space]
		[SerializeField]
		private Slider.SliderEvent m_OnValueChanged = new Slider.SliderEvent();

		// Token: 0x04000234 RID: 564
		private Image m_FillImage;

		// Token: 0x04000235 RID: 565
		private Transform m_FillTransform;

		// Token: 0x04000236 RID: 566
		private RectTransform m_FillContainerRect;

		// Token: 0x04000237 RID: 567
		private Transform m_HandleTransform;

		// Token: 0x04000238 RID: 568
		private RectTransform m_HandleContainerRect;

		// Token: 0x04000239 RID: 569
		private Vector2 m_Offset = Vector2.zero;

		// Token: 0x0400023A RID: 570
		private DrivenRectTransformTracker m_Tracker;

		// Token: 0x02000077 RID: 119
		public enum Direction
		{
			// Token: 0x0400023C RID: 572
			LeftToRight,
			// Token: 0x0400023D RID: 573
			RightToLeft,
			// Token: 0x0400023E RID: 574
			BottomToTop,
			// Token: 0x0400023F RID: 575
			TopToBottom
		}

		// Token: 0x02000078 RID: 120
		[Serializable]
		public class SliderEvent : UnityEvent<float>
		{
			// Token: 0x0600049B RID: 1179 RVA: 0x00019748 File Offset: 0x00017B48
			public SliderEvent()
			{
			}
		}

		// Token: 0x02000079 RID: 121
		private enum Axis
		{
			// Token: 0x04000241 RID: 577
			Horizontal,
			// Token: 0x04000242 RID: 578
			Vertical
		}
	}
}
