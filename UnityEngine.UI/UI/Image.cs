﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine.Serialization;
using UnityEngine.Sprites;
using UnityEngine.U2D;

namespace UnityEngine.UI
{
	// Token: 0x02000050 RID: 80
	[AddComponentMenu("UI/Image", 11)]
	public class Image : MaskableGraphic, ISerializationCallbackReceiver, ILayoutElement, ICanvasRaycastFilter
	{
		// Token: 0x0600027C RID: 636 RVA: 0x0000D0B0 File Offset: 0x0000B4B0
		protected Image()
		{
			base.useLegacyMeshGeneration = false;
		}

		// Token: 0x170000A5 RID: 165
		// (get) Token: 0x0600027D RID: 637 RVA: 0x0000D10C File Offset: 0x0000B50C
		// (set) Token: 0x0600027E RID: 638 RVA: 0x0000D127 File Offset: 0x0000B527
		public Sprite sprite
		{
			get
			{
				return this.m_Sprite;
			}
			set
			{
				if (SetPropertyUtility.SetClass<Sprite>(ref this.m_Sprite, value))
				{
					this.SetAllDirty();
					this.TrackSprite();
				}
			}
		}

		// Token: 0x170000A6 RID: 166
		// (get) Token: 0x0600027F RID: 639 RVA: 0x0000D14C File Offset: 0x0000B54C
		// (set) Token: 0x06000280 RID: 640 RVA: 0x0000D167 File Offset: 0x0000B567
		public Sprite overrideSprite
		{
			get
			{
				return this.activeSprite;
			}
			set
			{
				if (SetPropertyUtility.SetClass<Sprite>(ref this.m_OverrideSprite, value))
				{
					this.SetAllDirty();
					this.TrackSprite();
				}
			}
		}

		// Token: 0x170000A7 RID: 167
		// (get) Token: 0x06000281 RID: 641 RVA: 0x0000D18C File Offset: 0x0000B58C
		private Sprite activeSprite
		{
			get
			{
				return (!(this.m_OverrideSprite != null)) ? this.sprite : this.m_OverrideSprite;
			}
		}

		// Token: 0x170000A8 RID: 168
		// (get) Token: 0x06000282 RID: 642 RVA: 0x0000D1C4 File Offset: 0x0000B5C4
		// (set) Token: 0x06000283 RID: 643 RVA: 0x0000D1DF File Offset: 0x0000B5DF
		public Image.Type type
		{
			get
			{
				return this.m_Type;
			}
			set
			{
				if (SetPropertyUtility.SetStruct<Image.Type>(ref this.m_Type, value))
				{
					this.SetVerticesDirty();
				}
			}
		}

		// Token: 0x170000A9 RID: 169
		// (get) Token: 0x06000284 RID: 644 RVA: 0x0000D1FC File Offset: 0x0000B5FC
		// (set) Token: 0x06000285 RID: 645 RVA: 0x0000D217 File Offset: 0x0000B617
		public bool preserveAspect
		{
			get
			{
				return this.m_PreserveAspect;
			}
			set
			{
				if (SetPropertyUtility.SetStruct<bool>(ref this.m_PreserveAspect, value))
				{
					this.SetVerticesDirty();
				}
			}
		}

		// Token: 0x170000AA RID: 170
		// (get) Token: 0x06000286 RID: 646 RVA: 0x0000D234 File Offset: 0x0000B634
		// (set) Token: 0x06000287 RID: 647 RVA: 0x0000D24F File Offset: 0x0000B64F
		public bool fillCenter
		{
			get
			{
				return this.m_FillCenter;
			}
			set
			{
				if (SetPropertyUtility.SetStruct<bool>(ref this.m_FillCenter, value))
				{
					this.SetVerticesDirty();
				}
			}
		}

		// Token: 0x170000AB RID: 171
		// (get) Token: 0x06000288 RID: 648 RVA: 0x0000D26C File Offset: 0x0000B66C
		// (set) Token: 0x06000289 RID: 649 RVA: 0x0000D287 File Offset: 0x0000B687
		public Image.FillMethod fillMethod
		{
			get
			{
				return this.m_FillMethod;
			}
			set
			{
				if (SetPropertyUtility.SetStruct<Image.FillMethod>(ref this.m_FillMethod, value))
				{
					this.SetVerticesDirty();
					this.m_FillOrigin = 0;
				}
			}
		}

		// Token: 0x170000AC RID: 172
		// (get) Token: 0x0600028A RID: 650 RVA: 0x0000D2AC File Offset: 0x0000B6AC
		// (set) Token: 0x0600028B RID: 651 RVA: 0x0000D2C7 File Offset: 0x0000B6C7
		public float fillAmount
		{
			get
			{
				return this.m_FillAmount;
			}
			set
			{
				if (SetPropertyUtility.SetStruct<float>(ref this.m_FillAmount, Mathf.Clamp01(value)))
				{
					this.SetVerticesDirty();
				}
			}
		}

		// Token: 0x170000AD RID: 173
		// (get) Token: 0x0600028C RID: 652 RVA: 0x0000D2E8 File Offset: 0x0000B6E8
		// (set) Token: 0x0600028D RID: 653 RVA: 0x0000D303 File Offset: 0x0000B703
		public bool fillClockwise
		{
			get
			{
				return this.m_FillClockwise;
			}
			set
			{
				if (SetPropertyUtility.SetStruct<bool>(ref this.m_FillClockwise, value))
				{
					this.SetVerticesDirty();
				}
			}
		}

		// Token: 0x170000AE RID: 174
		// (get) Token: 0x0600028E RID: 654 RVA: 0x0000D320 File Offset: 0x0000B720
		// (set) Token: 0x0600028F RID: 655 RVA: 0x0000D33B File Offset: 0x0000B73B
		public int fillOrigin
		{
			get
			{
				return this.m_FillOrigin;
			}
			set
			{
				if (SetPropertyUtility.SetStruct<int>(ref this.m_FillOrigin, value))
				{
					this.SetVerticesDirty();
				}
			}
		}

		// Token: 0x170000AF RID: 175
		// (get) Token: 0x06000290 RID: 656 RVA: 0x0000D358 File Offset: 0x0000B758
		// (set) Token: 0x06000291 RID: 657 RVA: 0x0000D379 File Offset: 0x0000B779
		[Obsolete("eventAlphaThreshold has been deprecated. Use eventMinimumAlphaThreshold instead (UnityUpgradable) -> alphaHitTestMinimumThreshold")]
		public float eventAlphaThreshold
		{
			get
			{
				return 1f - this.alphaHitTestMinimumThreshold;
			}
			set
			{
				this.alphaHitTestMinimumThreshold = 1f - value;
			}
		}

		// Token: 0x170000B0 RID: 176
		// (get) Token: 0x06000292 RID: 658 RVA: 0x0000D38C File Offset: 0x0000B78C
		// (set) Token: 0x06000293 RID: 659 RVA: 0x0000D3A7 File Offset: 0x0000B7A7
		public float alphaHitTestMinimumThreshold
		{
			get
			{
				return this.m_AlphaHitTestMinimumThreshold;
			}
			set
			{
				this.m_AlphaHitTestMinimumThreshold = value;
			}
		}

		// Token: 0x170000B1 RID: 177
		// (get) Token: 0x06000294 RID: 660 RVA: 0x0000D3B4 File Offset: 0x0000B7B4
		// (set) Token: 0x06000295 RID: 661 RVA: 0x0000D3CF File Offset: 0x0000B7CF
		public bool useSpriteMesh
		{
			get
			{
				return this.m_UseSpriteMesh;
			}
			set
			{
				if (SetPropertyUtility.SetStruct<bool>(ref this.m_UseSpriteMesh, value))
				{
					this.SetVerticesDirty();
				}
			}
		}

		// Token: 0x170000B2 RID: 178
		// (get) Token: 0x06000296 RID: 662 RVA: 0x0000D3EC File Offset: 0x0000B7EC
		public static Material defaultETC1GraphicMaterial
		{
			get
			{
				if (Image.s_ETC1DefaultUI == null)
				{
					Image.s_ETC1DefaultUI = Canvas.GetETC1SupportedCanvasMaterial();
				}
				return Image.s_ETC1DefaultUI;
			}
		}

		// Token: 0x170000B3 RID: 179
		// (get) Token: 0x06000297 RID: 663 RVA: 0x0000D420 File Offset: 0x0000B820
		public override Texture mainTexture
		{
			get
			{
				Texture result;
				if (this.activeSprite == null)
				{
					if (this.material != null && this.material.mainTexture != null)
					{
						result = this.material.mainTexture;
					}
					else
					{
						result = Graphic.s_WhiteTexture;
					}
				}
				else
				{
					result = this.activeSprite.texture;
				}
				return result;
			}
		}

		// Token: 0x170000B4 RID: 180
		// (get) Token: 0x06000298 RID: 664 RVA: 0x0000D498 File Offset: 0x0000B898
		public bool hasBorder
		{
			get
			{
				return this.activeSprite != null && this.activeSprite.border.sqrMagnitude > 0f;
			}
		}

		// Token: 0x170000B5 RID: 181
		// (get) Token: 0x06000299 RID: 665 RVA: 0x0000D4E0 File Offset: 0x0000B8E0
		public float pixelsPerUnit
		{
			get
			{
				float num = 100f;
				if (this.activeSprite)
				{
					num = this.activeSprite.pixelsPerUnit;
				}
				float num2 = 100f;
				if (base.canvas)
				{
					num2 = base.canvas.referencePixelsPerUnit;
				}
				return num / num2;
			}
		}

		// Token: 0x170000B6 RID: 182
		// (get) Token: 0x0600029A RID: 666 RVA: 0x0000D53C File Offset: 0x0000B93C
		// (set) Token: 0x0600029B RID: 667 RVA: 0x0000D5A5 File Offset: 0x0000B9A5
		public override Material material
		{
			get
			{
				Material result;
				if (this.m_Material != null)
				{
					result = this.m_Material;
				}
				else if (this.activeSprite && this.activeSprite.associatedAlphaSplitTexture != null)
				{
					result = Image.defaultETC1GraphicMaterial;
				}
				else
				{
					result = this.defaultMaterial;
				}
				return result;
			}
			set
			{
				base.material = value;
			}
		}

		// Token: 0x0600029C RID: 668 RVA: 0x0000D5AF File Offset: 0x0000B9AF
		public virtual void OnBeforeSerialize()
		{
		}

		// Token: 0x0600029D RID: 669 RVA: 0x0000D5B4 File Offset: 0x0000B9B4
		public virtual void OnAfterDeserialize()
		{
			if (this.m_FillOrigin < 0)
			{
				this.m_FillOrigin = 0;
			}
			else if (this.m_FillMethod == Image.FillMethod.Horizontal && this.m_FillOrigin > 1)
			{
				this.m_FillOrigin = 0;
			}
			else if (this.m_FillMethod == Image.FillMethod.Vertical && this.m_FillOrigin > 1)
			{
				this.m_FillOrigin = 0;
			}
			else if (this.m_FillOrigin > 3)
			{
				this.m_FillOrigin = 0;
			}
			this.m_FillAmount = Mathf.Clamp(this.m_FillAmount, 0f, 1f);
		}

		// Token: 0x0600029E RID: 670 RVA: 0x0000D650 File Offset: 0x0000BA50
		private void PreserveSpriteAspectRatio(ref Rect rect, Vector2 spriteSize)
		{
			float num = spriteSize.x / spriteSize.y;
			float num2 = rect.width / rect.height;
			if (num > num2)
			{
				float height = rect.height;
				rect.height = rect.width * (1f / num);
				rect.y += (height - rect.height) * base.rectTransform.pivot.y;
			}
			else
			{
				float width = rect.width;
				rect.width = rect.height * num;
				rect.x += (width - rect.width) * base.rectTransform.pivot.x;
			}
		}

		// Token: 0x0600029F RID: 671 RVA: 0x0000D714 File Offset: 0x0000BB14
		private Vector4 GetDrawingDimensions(bool shouldPreserveAspect)
		{
			Vector4 vector = (!(this.activeSprite == null)) ? DataUtility.GetPadding(this.activeSprite) : Vector4.zero;
			Vector2 spriteSize = (!(this.activeSprite == null)) ? new Vector2(this.activeSprite.rect.width, this.activeSprite.rect.height) : Vector2.zero;
			Rect pixelAdjustedRect = base.GetPixelAdjustedRect();
			int num = Mathf.RoundToInt(spriteSize.x);
			int num2 = Mathf.RoundToInt(spriteSize.y);
			Vector4 result = new Vector4(vector.x / (float)num, vector.y / (float)num2, ((float)num - vector.z) / (float)num, ((float)num2 - vector.w) / (float)num2);
			if (shouldPreserveAspect && spriteSize.sqrMagnitude > 0f)
			{
				this.PreserveSpriteAspectRatio(ref pixelAdjustedRect, spriteSize);
			}
			result = new Vector4(pixelAdjustedRect.x + pixelAdjustedRect.width * result.x, pixelAdjustedRect.y + pixelAdjustedRect.height * result.y, pixelAdjustedRect.x + pixelAdjustedRect.width * result.z, pixelAdjustedRect.y + pixelAdjustedRect.height * result.w);
			return result;
		}

		// Token: 0x060002A0 RID: 672 RVA: 0x0000D880 File Offset: 0x0000BC80
		public override void SetNativeSize()
		{
			if (this.activeSprite != null)
			{
				float x = this.activeSprite.rect.width / this.pixelsPerUnit;
				float y = this.activeSprite.rect.height / this.pixelsPerUnit;
				base.rectTransform.anchorMax = base.rectTransform.anchorMin;
				base.rectTransform.sizeDelta = new Vector2(x, y);
				this.SetAllDirty();
			}
		}

		// Token: 0x060002A1 RID: 673 RVA: 0x0000D908 File Offset: 0x0000BD08
		protected override void OnPopulateMesh(VertexHelper toFill)
		{
			if (this.activeSprite == null)
			{
				base.OnPopulateMesh(toFill);
			}
			else
			{
				switch (this.type)
				{
				case Image.Type.Simple:
					if (!this.useSpriteMesh)
					{
						this.GenerateSimpleSprite(toFill, this.m_PreserveAspect);
					}
					else
					{
						this.GenerateSprite(toFill, this.m_PreserveAspect);
					}
					break;
				case Image.Type.Sliced:
					this.GenerateSlicedSprite(toFill);
					break;
				case Image.Type.Tiled:
					this.GenerateTiledSprite(toFill);
					break;
				case Image.Type.Filled:
					this.GenerateFilledSprite(toFill, this.m_PreserveAspect);
					break;
				}
			}
		}

		// Token: 0x060002A2 RID: 674 RVA: 0x0000D9AF File Offset: 0x0000BDAF
		private void TrackSprite()
		{
			if (this.activeSprite != null && this.activeSprite.texture == null)
			{
				Image.TrackImage(this);
				this.m_Tracked = true;
			}
		}

		// Token: 0x060002A3 RID: 675 RVA: 0x0000D9E8 File Offset: 0x0000BDE8
		protected override void OnEnable()
		{
			base.OnEnable();
			this.TrackSprite();
		}

		// Token: 0x060002A4 RID: 676 RVA: 0x0000D9F7 File Offset: 0x0000BDF7
		protected override void OnDisable()
		{
			base.OnDisable();
			if (this.m_Tracked)
			{
				Image.UnTrackImage(this);
			}
		}

		// Token: 0x060002A5 RID: 677 RVA: 0x0000DA14 File Offset: 0x0000BE14
		protected override void UpdateMaterial()
		{
			base.UpdateMaterial();
			if (this.activeSprite == null)
			{
				base.canvasRenderer.SetAlphaTexture(null);
			}
			else
			{
				Texture2D associatedAlphaSplitTexture = this.activeSprite.associatedAlphaSplitTexture;
				if (associatedAlphaSplitTexture != null)
				{
					base.canvasRenderer.SetAlphaTexture(associatedAlphaSplitTexture);
				}
			}
		}

		// Token: 0x060002A6 RID: 678 RVA: 0x0000DA74 File Offset: 0x0000BE74
		private void GenerateSimpleSprite(VertexHelper vh, bool lPreserveAspect)
		{
			Vector4 drawingDimensions = this.GetDrawingDimensions(lPreserveAspect);
			Vector4 vector = (!(this.activeSprite != null)) ? Vector4.zero : DataUtility.GetOuterUV(this.activeSprite);
			Color color = this.color;
			vh.Clear();
			vh.AddVert(new Vector3(drawingDimensions.x, drawingDimensions.y), color, new Vector2(vector.x, vector.y));
			vh.AddVert(new Vector3(drawingDimensions.x, drawingDimensions.w), color, new Vector2(vector.x, vector.w));
			vh.AddVert(new Vector3(drawingDimensions.z, drawingDimensions.w), color, new Vector2(vector.z, vector.w));
			vh.AddVert(new Vector3(drawingDimensions.z, drawingDimensions.y), color, new Vector2(vector.z, vector.y));
			vh.AddTriangle(0, 1, 2);
			vh.AddTriangle(2, 3, 0);
		}

		// Token: 0x060002A7 RID: 679 RVA: 0x0000DB98 File Offset: 0x0000BF98
		private void GenerateSprite(VertexHelper vh, bool lPreserveAspect)
		{
			Vector2 vector = new Vector2(this.activeSprite.rect.width, this.activeSprite.rect.height);
			Vector2 b = this.activeSprite.pivot / vector;
			Vector2 pivot = base.rectTransform.pivot;
			Rect pixelAdjustedRect = base.GetPixelAdjustedRect();
			if (lPreserveAspect & vector.sqrMagnitude > 0f)
			{
				this.PreserveSpriteAspectRatio(ref pixelAdjustedRect, vector);
			}
			Vector2 b2 = new Vector2(pixelAdjustedRect.width, pixelAdjustedRect.height);
			Vector3 size = this.activeSprite.bounds.size;
			Vector2 vector2 = (pivot - b) * b2;
			Color color = this.color;
			vh.Clear();
			Vector2[] vertices = this.activeSprite.vertices;
			Vector2[] uv = this.activeSprite.uv;
			for (int i = 0; i < vertices.Length; i++)
			{
				vh.AddVert(new Vector3(vertices[i].x / size.x * b2.x - vector2.x, vertices[i].y / size.y * b2.y - vector2.y), color, new Vector2(uv[i].x, uv[i].y));
			}
			ushort[] triangles = this.activeSprite.triangles;
			for (int j = 0; j < triangles.Length; j += 3)
			{
				vh.AddTriangle((int)triangles[j], (int)triangles[j + 1], (int)triangles[j + 2]);
			}
		}

		// Token: 0x060002A8 RID: 680 RVA: 0x0000DD5C File Offset: 0x0000C15C
		private void GenerateSlicedSprite(VertexHelper toFill)
		{
			if (!this.hasBorder)
			{
				this.GenerateSimpleSprite(toFill, false);
			}
			else
			{
				Vector4 vector;
				Vector4 vector2;
				Vector4 a;
				Vector4 a2;
				if (this.activeSprite != null)
				{
					vector = DataUtility.GetOuterUV(this.activeSprite);
					vector2 = DataUtility.GetInnerUV(this.activeSprite);
					a = DataUtility.GetPadding(this.activeSprite);
					a2 = this.activeSprite.border;
				}
				else
				{
					vector = Vector4.zero;
					vector2 = Vector4.zero;
					a = Vector4.zero;
					a2 = Vector4.zero;
				}
				Rect pixelAdjustedRect = base.GetPixelAdjustedRect();
				Vector4 adjustedBorders = this.GetAdjustedBorders(a2 / this.pixelsPerUnit, pixelAdjustedRect);
				a /= this.pixelsPerUnit;
				Image.s_VertScratch[0] = new Vector2(a.x, a.y);
				Image.s_VertScratch[3] = new Vector2(pixelAdjustedRect.width - a.z, pixelAdjustedRect.height - a.w);
				Image.s_VertScratch[1].x = adjustedBorders.x;
				Image.s_VertScratch[1].y = adjustedBorders.y;
				Image.s_VertScratch[2].x = pixelAdjustedRect.width - adjustedBorders.z;
				Image.s_VertScratch[2].y = pixelAdjustedRect.height - adjustedBorders.w;
				for (int i = 0; i < 4; i++)
				{
					Vector2[] array = Image.s_VertScratch;
					int num = i;
					array[num].x = array[num].x + pixelAdjustedRect.x;
					Vector2[] array2 = Image.s_VertScratch;
					int num2 = i;
					array2[num2].y = array2[num2].y + pixelAdjustedRect.y;
				}
				Image.s_UVScratch[0] = new Vector2(vector.x, vector.y);
				Image.s_UVScratch[1] = new Vector2(vector2.x, vector2.y);
				Image.s_UVScratch[2] = new Vector2(vector2.z, vector2.w);
				Image.s_UVScratch[3] = new Vector2(vector.z, vector.w);
				toFill.Clear();
				for (int j = 0; j < 3; j++)
				{
					int num3 = j + 1;
					for (int k = 0; k < 3; k++)
					{
						if (this.m_FillCenter || j != 1 || k != 1)
						{
							int num4 = k + 1;
							Image.AddQuad(toFill, new Vector2(Image.s_VertScratch[j].x, Image.s_VertScratch[k].y), new Vector2(Image.s_VertScratch[num3].x, Image.s_VertScratch[num4].y), this.color, new Vector2(Image.s_UVScratch[j].x, Image.s_UVScratch[k].y), new Vector2(Image.s_UVScratch[num3].x, Image.s_UVScratch[num4].y));
						}
					}
				}
			}
		}

		// Token: 0x060002A9 RID: 681 RVA: 0x0000E0C4 File Offset: 0x0000C4C4
		private void GenerateTiledSprite(VertexHelper toFill)
		{
			Vector4 vector;
			Vector4 vector2;
			Vector4 a;
			Vector2 vector3;
			if (this.activeSprite != null)
			{
				vector = DataUtility.GetOuterUV(this.activeSprite);
				vector2 = DataUtility.GetInnerUV(this.activeSprite);
				a = this.activeSprite.border;
				vector3 = this.activeSprite.rect.size;
			}
			else
			{
				vector = Vector4.zero;
				vector2 = Vector4.zero;
				a = Vector4.zero;
				vector3 = Vector2.one * 100f;
			}
			Rect pixelAdjustedRect = base.GetPixelAdjustedRect();
			float num = (vector3.x - a.x - a.z) / this.pixelsPerUnit;
			float num2 = (vector3.y - a.y - a.w) / this.pixelsPerUnit;
			a = this.GetAdjustedBorders(a / this.pixelsPerUnit, pixelAdjustedRect);
			Vector2 vector4 = new Vector2(vector2.x, vector2.y);
			Vector2 vector5 = new Vector2(vector2.z, vector2.w);
			float x = a.x;
			float num3 = pixelAdjustedRect.width - a.z;
			float y = a.y;
			float num4 = pixelAdjustedRect.height - a.w;
			toFill.Clear();
			Vector2 uvMax = vector5;
			if (num <= 0f)
			{
				num = num3 - x;
			}
			if (num2 <= 0f)
			{
				num2 = num4 - y;
			}
			if (this.activeSprite != null && (this.hasBorder || this.activeSprite.packed || this.activeSprite.texture.wrapMode != TextureWrapMode.Repeat))
			{
				long num5;
				long num6;
				if (this.m_FillCenter)
				{
					num5 = (long)Math.Ceiling((double)((num3 - x) / num));
					num6 = (long)Math.Ceiling((double)((num4 - y) / num2));
					double num7;
					if (this.hasBorder)
					{
						num7 = ((double)num5 + 2.0) * ((double)num6 + 2.0) * 4.0;
					}
					else
					{
						num7 = (double)(num5 * num6) * 4.0;
					}
					if (num7 > 65000.0)
					{
						Debug.LogError("Too many sprite tiles on Image \"" + base.name + "\". The tile size will be increased. To remove the limit on the number of tiles, convert the Sprite to an Advanced texture, remove the borders, clear the Packing tag and set the Wrap mode to Repeat.", this);
						double num8 = 16250.0;
						double num9;
						if (this.hasBorder)
						{
							num9 = ((double)num5 + 2.0) / ((double)num6 + 2.0);
						}
						else
						{
							num9 = (double)num5 / (double)num6;
						}
						double num10 = Math.Sqrt(num8 / num9);
						double num11 = num10 * num9;
						if (this.hasBorder)
						{
							num10 -= 2.0;
							num11 -= 2.0;
						}
						num5 = (long)Math.Floor(num10);
						num6 = (long)Math.Floor(num11);
						num = (num3 - x) / (float)num5;
						num2 = (num4 - y) / (float)num6;
					}
				}
				else if (this.hasBorder)
				{
					num5 = (long)Math.Ceiling((double)((num3 - x) / num));
					num6 = (long)Math.Ceiling((double)((num4 - y) / num2));
					double num12 = ((double)(num6 + num5) + 2.0) * 2.0 * 4.0;
					if (num12 > 65000.0)
					{
						Debug.LogError("Too many sprite tiles on Image \"" + base.name + "\". The tile size will be increased. To remove the limit on the number of tiles, convert the Sprite to an Advanced texture, remove the borders, clear the Packing tag and set the Wrap mode to Repeat.", this);
						double num13 = 16250.0;
						double num14 = (double)num5 / (double)num6;
						double num15 = (num13 - 4.0) / (2.0 * (1.0 + num14));
						double d = num15 * num14;
						num5 = (long)Math.Floor(num15);
						num6 = (long)Math.Floor(d);
						num = (num3 - x) / (float)num5;
						num2 = (num4 - y) / (float)num6;
					}
				}
				else
				{
					num5 = (num6 = 0L);
				}
				if (this.m_FillCenter)
				{
					for (long num16 = 0L; num16 < num6; num16 += 1L)
					{
						float num17 = y + (float)num16 * num2;
						float num18 = y + (float)(num16 + 1L) * num2;
						if (num18 > num4)
						{
							uvMax.y = vector4.y + (vector5.y - vector4.y) * (num4 - num17) / (num18 - num17);
							num18 = num4;
						}
						uvMax.x = vector5.x;
						for (long num19 = 0L; num19 < num5; num19 += 1L)
						{
							float num20 = x + (float)num19 * num;
							float num21 = x + (float)(num19 + 1L) * num;
							if (num21 > num3)
							{
								uvMax.x = vector4.x + (vector5.x - vector4.x) * (num3 - num20) / (num21 - num20);
								num21 = num3;
							}
							Image.AddQuad(toFill, new Vector2(num20, num17) + pixelAdjustedRect.position, new Vector2(num21, num18) + pixelAdjustedRect.position, this.color, vector4, uvMax);
						}
					}
				}
				if (this.hasBorder)
				{
					uvMax = vector5;
					for (long num22 = 0L; num22 < num6; num22 += 1L)
					{
						float num23 = y + (float)num22 * num2;
						float num24 = y + (float)(num22 + 1L) * num2;
						if (num24 > num4)
						{
							uvMax.y = vector4.y + (vector5.y - vector4.y) * (num4 - num23) / (num24 - num23);
							num24 = num4;
						}
						Image.AddQuad(toFill, new Vector2(0f, num23) + pixelAdjustedRect.position, new Vector2(x, num24) + pixelAdjustedRect.position, this.color, new Vector2(vector.x, vector4.y), new Vector2(vector4.x, uvMax.y));
						Image.AddQuad(toFill, new Vector2(num3, num23) + pixelAdjustedRect.position, new Vector2(pixelAdjustedRect.width, num24) + pixelAdjustedRect.position, this.color, new Vector2(vector5.x, vector4.y), new Vector2(vector.z, uvMax.y));
					}
					uvMax = vector5;
					for (long num25 = 0L; num25 < num5; num25 += 1L)
					{
						float num26 = x + (float)num25 * num;
						float num27 = x + (float)(num25 + 1L) * num;
						if (num27 > num3)
						{
							uvMax.x = vector4.x + (vector5.x - vector4.x) * (num3 - num26) / (num27 - num26);
							num27 = num3;
						}
						Image.AddQuad(toFill, new Vector2(num26, 0f) + pixelAdjustedRect.position, new Vector2(num27, y) + pixelAdjustedRect.position, this.color, new Vector2(vector4.x, vector.y), new Vector2(uvMax.x, vector4.y));
						Image.AddQuad(toFill, new Vector2(num26, num4) + pixelAdjustedRect.position, new Vector2(num27, pixelAdjustedRect.height) + pixelAdjustedRect.position, this.color, new Vector2(vector4.x, vector5.y), new Vector2(uvMax.x, vector.w));
					}
					Image.AddQuad(toFill, new Vector2(0f, 0f) + pixelAdjustedRect.position, new Vector2(x, y) + pixelAdjustedRect.position, this.color, new Vector2(vector.x, vector.y), new Vector2(vector4.x, vector4.y));
					Image.AddQuad(toFill, new Vector2(num3, 0f) + pixelAdjustedRect.position, new Vector2(pixelAdjustedRect.width, y) + pixelAdjustedRect.position, this.color, new Vector2(vector5.x, vector.y), new Vector2(vector.z, vector4.y));
					Image.AddQuad(toFill, new Vector2(0f, num4) + pixelAdjustedRect.position, new Vector2(x, pixelAdjustedRect.height) + pixelAdjustedRect.position, this.color, new Vector2(vector.x, vector5.y), new Vector2(vector4.x, vector.w));
					Image.AddQuad(toFill, new Vector2(num3, num4) + pixelAdjustedRect.position, new Vector2(pixelAdjustedRect.width, pixelAdjustedRect.height) + pixelAdjustedRect.position, this.color, new Vector2(vector5.x, vector5.y), new Vector2(vector.z, vector.w));
				}
			}
			else
			{
				Vector2 b = new Vector2((num3 - x) / num, (num4 - y) / num2);
				if (this.m_FillCenter)
				{
					Image.AddQuad(toFill, new Vector2(x, y) + pixelAdjustedRect.position, new Vector2(num3, num4) + pixelAdjustedRect.position, this.color, Vector2.Scale(vector4, b), Vector2.Scale(vector5, b));
				}
			}
		}

		// Token: 0x060002AA RID: 682 RVA: 0x0000EAB0 File Offset: 0x0000CEB0
		private static void AddQuad(VertexHelper vertexHelper, Vector3[] quadPositions, Color32 color, Vector3[] quadUVs)
		{
			int currentVertCount = vertexHelper.currentVertCount;
			for (int i = 0; i < 4; i++)
			{
				vertexHelper.AddVert(quadPositions[i], color, quadUVs[i]);
			}
			vertexHelper.AddTriangle(currentVertCount, currentVertCount + 1, currentVertCount + 2);
			vertexHelper.AddTriangle(currentVertCount + 2, currentVertCount + 3, currentVertCount);
		}

		// Token: 0x060002AB RID: 683 RVA: 0x0000EB18 File Offset: 0x0000CF18
		private static void AddQuad(VertexHelper vertexHelper, Vector2 posMin, Vector2 posMax, Color32 color, Vector2 uvMin, Vector2 uvMax)
		{
			int currentVertCount = vertexHelper.currentVertCount;
			vertexHelper.AddVert(new Vector3(posMin.x, posMin.y, 0f), color, new Vector2(uvMin.x, uvMin.y));
			vertexHelper.AddVert(new Vector3(posMin.x, posMax.y, 0f), color, new Vector2(uvMin.x, uvMax.y));
			vertexHelper.AddVert(new Vector3(posMax.x, posMax.y, 0f), color, new Vector2(uvMax.x, uvMax.y));
			vertexHelper.AddVert(new Vector3(posMax.x, posMin.y, 0f), color, new Vector2(uvMax.x, uvMin.y));
			vertexHelper.AddTriangle(currentVertCount, currentVertCount + 1, currentVertCount + 2);
			vertexHelper.AddTriangle(currentVertCount + 2, currentVertCount + 3, currentVertCount);
		}

		// Token: 0x060002AC RID: 684 RVA: 0x0000EC10 File Offset: 0x0000D010
		private Vector4 GetAdjustedBorders(Vector4 border, Rect adjustedRect)
		{
			Rect rect = base.rectTransform.rect;
			for (int i = 0; i <= 1; i++)
			{
				if (rect.size[i] != 0f)
				{
					float num = adjustedRect.size[i] / rect.size[i];
					ref Vector4 ptr = ref border;
					int index;
					border[index = i] = ptr[index] * num;
					ptr = ref border;
					int index2;
					border[index2 = i + 2] = ptr[index2] * num;
				}
				float num2 = border[i] + border[i + 2];
				if (adjustedRect.size[i] < num2 && num2 != 0f)
				{
					float num = adjustedRect.size[i] / num2;
					ref Vector4 ptr = ref border;
					int index3;
					border[index3 = i] = ptr[index3] * num;
					ptr = ref border;
					int index4;
					border[index4 = i + 2] = ptr[index4] * num;
				}
			}
			return border;
		}

		// Token: 0x060002AD RID: 685 RVA: 0x0000ED40 File Offset: 0x0000D140
		private void GenerateFilledSprite(VertexHelper toFill, bool preserveAspect)
		{
			toFill.Clear();
			if (this.m_FillAmount >= 0.001f)
			{
				Vector4 drawingDimensions = this.GetDrawingDimensions(preserveAspect);
				Vector4 vector = (!(this.activeSprite != null)) ? Vector4.zero : DataUtility.GetOuterUV(this.activeSprite);
				UIVertex simpleVert = UIVertex.simpleVert;
				simpleVert.color = this.color;
				float num = vector.x;
				float num2 = vector.y;
				float num3 = vector.z;
				float num4 = vector.w;
				if (this.m_FillMethod == Image.FillMethod.Horizontal || this.m_FillMethod == Image.FillMethod.Vertical)
				{
					if (this.fillMethod == Image.FillMethod.Horizontal)
					{
						float num5 = (num3 - num) * this.m_FillAmount;
						if (this.m_FillOrigin == 1)
						{
							drawingDimensions.x = drawingDimensions.z - (drawingDimensions.z - drawingDimensions.x) * this.m_FillAmount;
							num = num3 - num5;
						}
						else
						{
							drawingDimensions.z = drawingDimensions.x + (drawingDimensions.z - drawingDimensions.x) * this.m_FillAmount;
							num3 = num + num5;
						}
					}
					else if (this.fillMethod == Image.FillMethod.Vertical)
					{
						float num6 = (num4 - num2) * this.m_FillAmount;
						if (this.m_FillOrigin == 1)
						{
							drawingDimensions.y = drawingDimensions.w - (drawingDimensions.w - drawingDimensions.y) * this.m_FillAmount;
							num2 = num4 - num6;
						}
						else
						{
							drawingDimensions.w = drawingDimensions.y + (drawingDimensions.w - drawingDimensions.y) * this.m_FillAmount;
							num4 = num2 + num6;
						}
					}
				}
				Image.s_Xy[0] = new Vector2(drawingDimensions.x, drawingDimensions.y);
				Image.s_Xy[1] = new Vector2(drawingDimensions.x, drawingDimensions.w);
				Image.s_Xy[2] = new Vector2(drawingDimensions.z, drawingDimensions.w);
				Image.s_Xy[3] = new Vector2(drawingDimensions.z, drawingDimensions.y);
				Image.s_Uv[0] = new Vector2(num, num2);
				Image.s_Uv[1] = new Vector2(num, num4);
				Image.s_Uv[2] = new Vector2(num3, num4);
				Image.s_Uv[3] = new Vector2(num3, num2);
				if (this.m_FillAmount < 1f && this.m_FillMethod != Image.FillMethod.Horizontal && this.m_FillMethod != Image.FillMethod.Vertical)
				{
					if (this.fillMethod == Image.FillMethod.Radial90)
					{
						if (Image.RadialCut(Image.s_Xy, Image.s_Uv, this.m_FillAmount, this.m_FillClockwise, this.m_FillOrigin))
						{
							Image.AddQuad(toFill, Image.s_Xy, this.color, Image.s_Uv);
						}
					}
					else if (this.fillMethod == Image.FillMethod.Radial180)
					{
						for (int i = 0; i < 2; i++)
						{
							int num7 = (this.m_FillOrigin <= 1) ? 0 : 1;
							float t;
							float t2;
							float t3;
							float t4;
							if (this.m_FillOrigin == 0 || this.m_FillOrigin == 2)
							{
								t = 0f;
								t2 = 1f;
								if (i == num7)
								{
									t3 = 0f;
									t4 = 0.5f;
								}
								else
								{
									t3 = 0.5f;
									t4 = 1f;
								}
							}
							else
							{
								t3 = 0f;
								t4 = 1f;
								if (i == num7)
								{
									t = 0.5f;
									t2 = 1f;
								}
								else
								{
									t = 0f;
									t2 = 0.5f;
								}
							}
							Image.s_Xy[0].x = Mathf.Lerp(drawingDimensions.x, drawingDimensions.z, t3);
							Image.s_Xy[1].x = Image.s_Xy[0].x;
							Image.s_Xy[2].x = Mathf.Lerp(drawingDimensions.x, drawingDimensions.z, t4);
							Image.s_Xy[3].x = Image.s_Xy[2].x;
							Image.s_Xy[0].y = Mathf.Lerp(drawingDimensions.y, drawingDimensions.w, t);
							Image.s_Xy[1].y = Mathf.Lerp(drawingDimensions.y, drawingDimensions.w, t2);
							Image.s_Xy[2].y = Image.s_Xy[1].y;
							Image.s_Xy[3].y = Image.s_Xy[0].y;
							Image.s_Uv[0].x = Mathf.Lerp(num, num3, t3);
							Image.s_Uv[1].x = Image.s_Uv[0].x;
							Image.s_Uv[2].x = Mathf.Lerp(num, num3, t4);
							Image.s_Uv[3].x = Image.s_Uv[2].x;
							Image.s_Uv[0].y = Mathf.Lerp(num2, num4, t);
							Image.s_Uv[1].y = Mathf.Lerp(num2, num4, t2);
							Image.s_Uv[2].y = Image.s_Uv[1].y;
							Image.s_Uv[3].y = Image.s_Uv[0].y;
							float value = (!this.m_FillClockwise) ? (this.m_FillAmount * 2f - (float)(1 - i)) : (this.fillAmount * 2f - (float)i);
							if (Image.RadialCut(Image.s_Xy, Image.s_Uv, Mathf.Clamp01(value), this.m_FillClockwise, (i + this.m_FillOrigin + 3) % 4))
							{
								Image.AddQuad(toFill, Image.s_Xy, this.color, Image.s_Uv);
							}
						}
					}
					else if (this.fillMethod == Image.FillMethod.Radial360)
					{
						for (int j = 0; j < 4; j++)
						{
							float t5;
							float t6;
							if (j < 2)
							{
								t5 = 0f;
								t6 = 0.5f;
							}
							else
							{
								t5 = 0.5f;
								t6 = 1f;
							}
							float t7;
							float t8;
							if (j == 0 || j == 3)
							{
								t7 = 0f;
								t8 = 0.5f;
							}
							else
							{
								t7 = 0.5f;
								t8 = 1f;
							}
							Image.s_Xy[0].x = Mathf.Lerp(drawingDimensions.x, drawingDimensions.z, t5);
							Image.s_Xy[1].x = Image.s_Xy[0].x;
							Image.s_Xy[2].x = Mathf.Lerp(drawingDimensions.x, drawingDimensions.z, t6);
							Image.s_Xy[3].x = Image.s_Xy[2].x;
							Image.s_Xy[0].y = Mathf.Lerp(drawingDimensions.y, drawingDimensions.w, t7);
							Image.s_Xy[1].y = Mathf.Lerp(drawingDimensions.y, drawingDimensions.w, t8);
							Image.s_Xy[2].y = Image.s_Xy[1].y;
							Image.s_Xy[3].y = Image.s_Xy[0].y;
							Image.s_Uv[0].x = Mathf.Lerp(num, num3, t5);
							Image.s_Uv[1].x = Image.s_Uv[0].x;
							Image.s_Uv[2].x = Mathf.Lerp(num, num3, t6);
							Image.s_Uv[3].x = Image.s_Uv[2].x;
							Image.s_Uv[0].y = Mathf.Lerp(num2, num4, t7);
							Image.s_Uv[1].y = Mathf.Lerp(num2, num4, t8);
							Image.s_Uv[2].y = Image.s_Uv[1].y;
							Image.s_Uv[3].y = Image.s_Uv[0].y;
							float value2 = (!this.m_FillClockwise) ? (this.m_FillAmount * 4f - (float)(3 - (j + this.m_FillOrigin) % 4)) : (this.m_FillAmount * 4f - (float)((j + this.m_FillOrigin) % 4));
							if (Image.RadialCut(Image.s_Xy, Image.s_Uv, Mathf.Clamp01(value2), this.m_FillClockwise, (j + 2) % 4))
							{
								Image.AddQuad(toFill, Image.s_Xy, this.color, Image.s_Uv);
							}
						}
					}
				}
				else
				{
					Image.AddQuad(toFill, Image.s_Xy, this.color, Image.s_Uv);
				}
			}
		}

		// Token: 0x060002AE RID: 686 RVA: 0x0000F718 File Offset: 0x0000DB18
		private static bool RadialCut(Vector3[] xy, Vector3[] uv, float fill, bool invert, int corner)
		{
			bool result;
			if (fill < 0.001f)
			{
				result = false;
			}
			else
			{
				if ((corner & 1) == 1)
				{
					invert = !invert;
				}
				if (!invert && fill > 0.999f)
				{
					result = true;
				}
				else
				{
					float num = Mathf.Clamp01(fill);
					if (invert)
					{
						num = 1f - num;
					}
					num *= 1.5707964f;
					float cos = Mathf.Cos(num);
					float sin = Mathf.Sin(num);
					Image.RadialCut(xy, cos, sin, invert, corner);
					Image.RadialCut(uv, cos, sin, invert, corner);
					result = true;
				}
			}
			return result;
		}

		// Token: 0x060002AF RID: 687 RVA: 0x0000F7AC File Offset: 0x0000DBAC
		private static void RadialCut(Vector3[] xy, float cos, float sin, bool invert, int corner)
		{
			int num = (corner + 1) % 4;
			int num2 = (corner + 2) % 4;
			int num3 = (corner + 3) % 4;
			if ((corner & 1) == 1)
			{
				if (sin > cos)
				{
					cos /= sin;
					sin = 1f;
					if (invert)
					{
						xy[num].x = Mathf.Lerp(xy[corner].x, xy[num2].x, cos);
						xy[num2].x = xy[num].x;
					}
				}
				else if (cos > sin)
				{
					sin /= cos;
					cos = 1f;
					if (!invert)
					{
						xy[num2].y = Mathf.Lerp(xy[corner].y, xy[num2].y, sin);
						xy[num3].y = xy[num2].y;
					}
				}
				else
				{
					cos = 1f;
					sin = 1f;
				}
				if (!invert)
				{
					xy[num3].x = Mathf.Lerp(xy[corner].x, xy[num2].x, cos);
				}
				else
				{
					xy[num].y = Mathf.Lerp(xy[corner].y, xy[num2].y, sin);
				}
			}
			else
			{
				if (cos > sin)
				{
					sin /= cos;
					cos = 1f;
					if (!invert)
					{
						xy[num].y = Mathf.Lerp(xy[corner].y, xy[num2].y, sin);
						xy[num2].y = xy[num].y;
					}
				}
				else if (sin > cos)
				{
					cos /= sin;
					sin = 1f;
					if (invert)
					{
						xy[num2].x = Mathf.Lerp(xy[corner].x, xy[num2].x, cos);
						xy[num3].x = xy[num2].x;
					}
				}
				else
				{
					cos = 1f;
					sin = 1f;
				}
				if (invert)
				{
					xy[num3].y = Mathf.Lerp(xy[corner].y, xy[num2].y, sin);
				}
				else
				{
					xy[num].x = Mathf.Lerp(xy[corner].x, xy[num2].x, cos);
				}
			}
		}

		// Token: 0x060002B0 RID: 688 RVA: 0x0000FA53 File Offset: 0x0000DE53
		public virtual void CalculateLayoutInputHorizontal()
		{
		}

		// Token: 0x060002B1 RID: 689 RVA: 0x0000FA56 File Offset: 0x0000DE56
		public virtual void CalculateLayoutInputVertical()
		{
		}

		// Token: 0x170000B7 RID: 183
		// (get) Token: 0x060002B2 RID: 690 RVA: 0x0000FA5C File Offset: 0x0000DE5C
		public virtual float minWidth
		{
			get
			{
				return 0f;
			}
		}

		// Token: 0x170000B8 RID: 184
		// (get) Token: 0x060002B3 RID: 691 RVA: 0x0000FA78 File Offset: 0x0000DE78
		public virtual float preferredWidth
		{
			get
			{
				float result;
				if (this.activeSprite == null)
				{
					result = 0f;
				}
				else if (this.type == Image.Type.Sliced || this.type == Image.Type.Tiled)
				{
					result = DataUtility.GetMinSize(this.activeSprite).x / this.pixelsPerUnit;
				}
				else
				{
					result = this.activeSprite.rect.size.x / this.pixelsPerUnit;
				}
				return result;
			}
		}

		// Token: 0x170000B9 RID: 185
		// (get) Token: 0x060002B4 RID: 692 RVA: 0x0000FB04 File Offset: 0x0000DF04
		public virtual float flexibleWidth
		{
			get
			{
				return -1f;
			}
		}

		// Token: 0x170000BA RID: 186
		// (get) Token: 0x060002B5 RID: 693 RVA: 0x0000FB20 File Offset: 0x0000DF20
		public virtual float minHeight
		{
			get
			{
				return 0f;
			}
		}

		// Token: 0x170000BB RID: 187
		// (get) Token: 0x060002B6 RID: 694 RVA: 0x0000FB3C File Offset: 0x0000DF3C
		public virtual float preferredHeight
		{
			get
			{
				float result;
				if (this.activeSprite == null)
				{
					result = 0f;
				}
				else if (this.type == Image.Type.Sliced || this.type == Image.Type.Tiled)
				{
					result = DataUtility.GetMinSize(this.activeSprite).y / this.pixelsPerUnit;
				}
				else
				{
					result = this.activeSprite.rect.size.y / this.pixelsPerUnit;
				}
				return result;
			}
		}

		// Token: 0x170000BC RID: 188
		// (get) Token: 0x060002B7 RID: 695 RVA: 0x0000FBC8 File Offset: 0x0000DFC8
		public virtual float flexibleHeight
		{
			get
			{
				return -1f;
			}
		}

		// Token: 0x170000BD RID: 189
		// (get) Token: 0x060002B8 RID: 696 RVA: 0x0000FBE4 File Offset: 0x0000DFE4
		public virtual int layoutPriority
		{
			get
			{
				return 0;
			}
		}

		// Token: 0x060002B9 RID: 697 RVA: 0x0000FBFC File Offset: 0x0000DFFC
		public virtual bool IsRaycastLocationValid(Vector2 screenPoint, Camera eventCamera)
		{
			bool result;
			Vector2 local;
			if (this.alphaHitTestMinimumThreshold <= 0f)
			{
				result = true;
			}
			else if (this.alphaHitTestMinimumThreshold > 1f)
			{
				result = false;
			}
			else if (this.activeSprite == null)
			{
				result = true;
			}
			else if (!RectTransformUtility.ScreenPointToLocalPointInRectangle(base.rectTransform, screenPoint, eventCamera, out local))
			{
				result = false;
			}
			else
			{
				Rect pixelAdjustedRect = base.GetPixelAdjustedRect();
				local.x += base.rectTransform.pivot.x * pixelAdjustedRect.width;
				local.y += base.rectTransform.pivot.y * pixelAdjustedRect.height;
				local = this.MapCoordinate(local, pixelAdjustedRect);
				Rect textureRect = this.activeSprite.textureRect;
				Vector2 vector = new Vector2(local.x / textureRect.width, local.y / textureRect.height);
				float x = Mathf.Lerp(textureRect.x, textureRect.xMax, vector.x) / (float)this.activeSprite.texture.width;
				float y = Mathf.Lerp(textureRect.y, textureRect.yMax, vector.y) / (float)this.activeSprite.texture.height;
				try
				{
					result = (this.activeSprite.texture.GetPixelBilinear(x, y).a >= this.alphaHitTestMinimumThreshold);
				}
				catch (UnityException ex)
				{
					Debug.LogError("Using alphaHitTestMinimumThreshold greater than 0 on Image whose sprite texture cannot be read. " + ex.Message + " Also make sure to disable sprite packing for this sprite.", this);
					result = true;
				}
			}
			return result;
		}

		// Token: 0x060002BA RID: 698 RVA: 0x0000FDC4 File Offset: 0x0000E1C4
		private Vector2 MapCoordinate(Vector2 local, Rect rect)
		{
			Rect rect2 = this.activeSprite.rect;
			Vector2 result;
			if (this.type == Image.Type.Simple || this.type == Image.Type.Filled)
			{
				result = new Vector2(local.x * rect2.width / rect.width, local.y * rect2.height / rect.height);
			}
			else
			{
				Vector4 border = this.activeSprite.border;
				Vector4 adjustedBorders = this.GetAdjustedBorders(border / this.pixelsPerUnit, rect);
				for (int i = 0; i < 2; i++)
				{
					if (local[i] > adjustedBorders[i])
					{
						if (rect.size[i] - local[i] <= adjustedBorders[i + 2])
						{
							ref Vector2 ptr = ref local;
							int index;
							local[index = i] = ptr[index] - (rect.size[i] - rect2.size[i]);
						}
						else if (this.type == Image.Type.Sliced)
						{
							float t = Mathf.InverseLerp(adjustedBorders[i], rect.size[i] - adjustedBorders[i + 2], local[i]);
							local[i] = Mathf.Lerp(border[i], rect2.size[i] - border[i + 2], t);
						}
						else
						{
							ref Vector2 ptr = ref local;
							int index2;
							local[index2 = i] = ptr[index2] - adjustedBorders[i];
							local[i] = Mathf.Repeat(local[i], rect2.size[i] - border[i] - border[i + 2]);
							ptr = ref local;
							int index3;
							local[index3 = i] = ptr[index3] + border[i];
						}
					}
				}
				result = local;
			}
			return result;
		}

		// Token: 0x060002BB RID: 699 RVA: 0x00010004 File Offset: 0x0000E404
		private static void RebuildImage(SpriteAtlas spriteAtlas)
		{
			for (int i = Image.m_TrackedTexturelessImages.Count - 1; i >= 0; i--)
			{
				Image image = Image.m_TrackedTexturelessImages[i];
				if (spriteAtlas.CanBindTo(image.activeSprite))
				{
					image.SetAllDirty();
					Image.m_TrackedTexturelessImages.RemoveAt(i);
				}
			}
		}

		// Token: 0x060002BC RID: 700 RVA: 0x00010064 File Offset: 0x0000E464
		private static void TrackImage(Image g)
		{
			if (!Image.s_Initialized)
			{
				if (Image.<>f__mg$cache0 == null)
				{
					Image.<>f__mg$cache0 = new Action<SpriteAtlas>(Image.RebuildImage);
				}
				SpriteAtlasManager.atlasRegistered += Image.<>f__mg$cache0;
				Image.s_Initialized = true;
			}
			Image.m_TrackedTexturelessImages.Add(g);
		}

		// Token: 0x060002BD RID: 701 RVA: 0x000100B1 File Offset: 0x0000E4B1
		private static void UnTrackImage(Image g)
		{
			Image.m_TrackedTexturelessImages.Remove(g);
		}

		// Token: 0x060002BE RID: 702 RVA: 0x000100C0 File Offset: 0x0000E4C0
		// Note: this type is marked as 'beforefieldinit'.
		static Image()
		{
		}

		// Token: 0x0400012E RID: 302
		protected static Material s_ETC1DefaultUI = null;

		// Token: 0x0400012F RID: 303
		[FormerlySerializedAs("m_Frame")]
		[SerializeField]
		private Sprite m_Sprite;

		// Token: 0x04000130 RID: 304
		[NonSerialized]
		private Sprite m_OverrideSprite;

		// Token: 0x04000131 RID: 305
		[SerializeField]
		private Image.Type m_Type = Image.Type.Simple;

		// Token: 0x04000132 RID: 306
		[SerializeField]
		private bool m_PreserveAspect = false;

		// Token: 0x04000133 RID: 307
		[SerializeField]
		private bool m_FillCenter = true;

		// Token: 0x04000134 RID: 308
		[SerializeField]
		private Image.FillMethod m_FillMethod = Image.FillMethod.Radial360;

		// Token: 0x04000135 RID: 309
		[Range(0f, 1f)]
		[SerializeField]
		private float m_FillAmount = 1f;

		// Token: 0x04000136 RID: 310
		[SerializeField]
		private bool m_FillClockwise = true;

		// Token: 0x04000137 RID: 311
		[SerializeField]
		private int m_FillOrigin;

		// Token: 0x04000138 RID: 312
		private float m_AlphaHitTestMinimumThreshold = 0f;

		// Token: 0x04000139 RID: 313
		private bool m_Tracked = false;

		// Token: 0x0400013A RID: 314
		[SerializeField]
		private bool m_UseSpriteMesh;

		// Token: 0x0400013B RID: 315
		private static readonly Vector2[] s_VertScratch = new Vector2[4];

		// Token: 0x0400013C RID: 316
		private static readonly Vector2[] s_UVScratch = new Vector2[4];

		// Token: 0x0400013D RID: 317
		private static readonly Vector3[] s_Xy = new Vector3[4];

		// Token: 0x0400013E RID: 318
		private static readonly Vector3[] s_Uv = new Vector3[4];

		// Token: 0x0400013F RID: 319
		private static List<Image> m_TrackedTexturelessImages = new List<Image>();

		// Token: 0x04000140 RID: 320
		private static bool s_Initialized;

		// Token: 0x04000141 RID: 321
		[CompilerGenerated]
		private static Action<SpriteAtlas> <>f__mg$cache0;

		// Token: 0x02000051 RID: 81
		public enum Type
		{
			// Token: 0x04000143 RID: 323
			Simple,
			// Token: 0x04000144 RID: 324
			Sliced,
			// Token: 0x04000145 RID: 325
			Tiled,
			// Token: 0x04000146 RID: 326
			Filled
		}

		// Token: 0x02000052 RID: 82
		public enum FillMethod
		{
			// Token: 0x04000148 RID: 328
			Horizontal,
			// Token: 0x04000149 RID: 329
			Vertical,
			// Token: 0x0400014A RID: 330
			Radial90,
			// Token: 0x0400014B RID: 331
			Radial180,
			// Token: 0x0400014C RID: 332
			Radial360
		}

		// Token: 0x02000053 RID: 83
		public enum OriginHorizontal
		{
			// Token: 0x0400014E RID: 334
			Left,
			// Token: 0x0400014F RID: 335
			Right
		}

		// Token: 0x02000054 RID: 84
		public enum OriginVertical
		{
			// Token: 0x04000151 RID: 337
			Bottom,
			// Token: 0x04000152 RID: 338
			Top
		}

		// Token: 0x02000055 RID: 85
		public enum Origin90
		{
			// Token: 0x04000154 RID: 340
			BottomLeft,
			// Token: 0x04000155 RID: 341
			TopLeft,
			// Token: 0x04000156 RID: 342
			TopRight,
			// Token: 0x04000157 RID: 343
			BottomRight
		}

		// Token: 0x02000056 RID: 86
		public enum Origin180
		{
			// Token: 0x04000159 RID: 345
			Bottom,
			// Token: 0x0400015A RID: 346
			Left,
			// Token: 0x0400015B RID: 347
			Top,
			// Token: 0x0400015C RID: 348
			Right
		}

		// Token: 0x02000057 RID: 87
		public enum Origin360
		{
			// Token: 0x0400015E RID: 350
			Bottom,
			// Token: 0x0400015F RID: 351
			Right,
			// Token: 0x04000160 RID: 352
			Top,
			// Token: 0x04000161 RID: 353
			Left
		}
	}
}
