﻿using System;

namespace UnityEngine.UI
{
	// Token: 0x02000095 RID: 149
	public interface ILayoutElement
	{
		// Token: 0x0600056C RID: 1388
		void CalculateLayoutInputHorizontal();

		// Token: 0x0600056D RID: 1389
		void CalculateLayoutInputVertical();

		// Token: 0x1700017B RID: 379
		// (get) Token: 0x0600056E RID: 1390
		float minWidth { get; }

		// Token: 0x1700017C RID: 380
		// (get) Token: 0x0600056F RID: 1391
		float preferredWidth { get; }

		// Token: 0x1700017D RID: 381
		// (get) Token: 0x06000570 RID: 1392
		float flexibleWidth { get; }

		// Token: 0x1700017E RID: 382
		// (get) Token: 0x06000571 RID: 1393
		float minHeight { get; }

		// Token: 0x1700017F RID: 383
		// (get) Token: 0x06000572 RID: 1394
		float preferredHeight { get; }

		// Token: 0x17000180 RID: 384
		// (get) Token: 0x06000573 RID: 1395
		float flexibleHeight { get; }

		// Token: 0x17000181 RID: 385
		// (get) Token: 0x06000574 RID: 1396
		int layoutPriority { get; }
	}
}
