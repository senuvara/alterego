﻿using System;
using UnityEngine.EventSystems;

namespace UnityEngine.UI
{
	// Token: 0x0200008D RID: 141
	[AddComponentMenu("Layout/Content Size Fitter", 141)]
	[ExecuteAlways]
	[RequireComponent(typeof(RectTransform))]
	public class ContentSizeFitter : UIBehaviour, ILayoutSelfController, ILayoutController
	{
		// Token: 0x0600053A RID: 1338 RVA: 0x0001BCF3 File Offset: 0x0001A0F3
		protected ContentSizeFitter()
		{
		}

		// Token: 0x1700016D RID: 365
		// (get) Token: 0x0600053B RID: 1339 RVA: 0x0001BD0C File Offset: 0x0001A10C
		// (set) Token: 0x0600053C RID: 1340 RVA: 0x0001BD27 File Offset: 0x0001A127
		public ContentSizeFitter.FitMode horizontalFit
		{
			get
			{
				return this.m_HorizontalFit;
			}
			set
			{
				if (SetPropertyUtility.SetStruct<ContentSizeFitter.FitMode>(ref this.m_HorizontalFit, value))
				{
					this.SetDirty();
				}
			}
		}

		// Token: 0x1700016E RID: 366
		// (get) Token: 0x0600053D RID: 1341 RVA: 0x0001BD44 File Offset: 0x0001A144
		// (set) Token: 0x0600053E RID: 1342 RVA: 0x0001BD5F File Offset: 0x0001A15F
		public ContentSizeFitter.FitMode verticalFit
		{
			get
			{
				return this.m_VerticalFit;
			}
			set
			{
				if (SetPropertyUtility.SetStruct<ContentSizeFitter.FitMode>(ref this.m_VerticalFit, value))
				{
					this.SetDirty();
				}
			}
		}

		// Token: 0x1700016F RID: 367
		// (get) Token: 0x0600053F RID: 1343 RVA: 0x0001BD7C File Offset: 0x0001A17C
		private RectTransform rectTransform
		{
			get
			{
				if (this.m_Rect == null)
				{
					this.m_Rect = base.GetComponent<RectTransform>();
				}
				return this.m_Rect;
			}
		}

		// Token: 0x06000540 RID: 1344 RVA: 0x0001BDB4 File Offset: 0x0001A1B4
		protected override void OnEnable()
		{
			base.OnEnable();
			this.SetDirty();
		}

		// Token: 0x06000541 RID: 1345 RVA: 0x0001BDC3 File Offset: 0x0001A1C3
		protected override void OnDisable()
		{
			this.m_Tracker.Clear();
			LayoutRebuilder.MarkLayoutForRebuild(this.rectTransform);
			base.OnDisable();
		}

		// Token: 0x06000542 RID: 1346 RVA: 0x0001BDE2 File Offset: 0x0001A1E2
		protected override void OnRectTransformDimensionsChange()
		{
			this.SetDirty();
		}

		// Token: 0x06000543 RID: 1347 RVA: 0x0001BDEC File Offset: 0x0001A1EC
		private void HandleSelfFittingAlongAxis(int axis)
		{
			ContentSizeFitter.FitMode fitMode = (axis != 0) ? this.verticalFit : this.horizontalFit;
			if (fitMode == ContentSizeFitter.FitMode.Unconstrained)
			{
				this.m_Tracker.Add(this, this.rectTransform, DrivenTransformProperties.None);
			}
			else
			{
				this.m_Tracker.Add(this, this.rectTransform, (axis != 0) ? DrivenTransformProperties.SizeDeltaY : DrivenTransformProperties.SizeDeltaX);
				if (fitMode == ContentSizeFitter.FitMode.MinSize)
				{
					this.rectTransform.SetSizeWithCurrentAnchors((RectTransform.Axis)axis, LayoutUtility.GetMinSize(this.m_Rect, axis));
				}
				else
				{
					this.rectTransform.SetSizeWithCurrentAnchors((RectTransform.Axis)axis, LayoutUtility.GetPreferredSize(this.m_Rect, axis));
				}
			}
		}

		// Token: 0x06000544 RID: 1348 RVA: 0x0001BE94 File Offset: 0x0001A294
		public virtual void SetLayoutHorizontal()
		{
			this.m_Tracker.Clear();
			this.HandleSelfFittingAlongAxis(0);
		}

		// Token: 0x06000545 RID: 1349 RVA: 0x0001BEA9 File Offset: 0x0001A2A9
		public virtual void SetLayoutVertical()
		{
			this.HandleSelfFittingAlongAxis(1);
		}

		// Token: 0x06000546 RID: 1350 RVA: 0x0001BEB3 File Offset: 0x0001A2B3
		protected void SetDirty()
		{
			if (this.IsActive())
			{
				LayoutRebuilder.MarkLayoutForRebuild(this.rectTransform);
			}
		}

		// Token: 0x0400028F RID: 655
		[SerializeField]
		protected ContentSizeFitter.FitMode m_HorizontalFit = ContentSizeFitter.FitMode.Unconstrained;

		// Token: 0x04000290 RID: 656
		[SerializeField]
		protected ContentSizeFitter.FitMode m_VerticalFit = ContentSizeFitter.FitMode.Unconstrained;

		// Token: 0x04000291 RID: 657
		[NonSerialized]
		private RectTransform m_Rect;

		// Token: 0x04000292 RID: 658
		private DrivenRectTransformTracker m_Tracker;

		// Token: 0x0200008E RID: 142
		public enum FitMode
		{
			// Token: 0x04000294 RID: 660
			Unconstrained,
			// Token: 0x04000295 RID: 661
			MinSize,
			// Token: 0x04000296 RID: 662
			PreferredSize
		}
	}
}
