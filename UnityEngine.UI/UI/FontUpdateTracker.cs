﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace UnityEngine.UI
{
	// Token: 0x02000048 RID: 72
	public static class FontUpdateTracker
	{
		// Token: 0x06000228 RID: 552 RVA: 0x0000B2C0 File Offset: 0x000096C0
		public static void TrackText(Text t)
		{
			if (!(t.font == null))
			{
				HashSet<Text> hashSet;
				FontUpdateTracker.m_Tracked.TryGetValue(t.font, out hashSet);
				if (hashSet == null)
				{
					if (FontUpdateTracker.m_Tracked.Count == 0)
					{
						if (FontUpdateTracker.<>f__mg$cache0 == null)
						{
							FontUpdateTracker.<>f__mg$cache0 = new Action<Font>(FontUpdateTracker.RebuildForFont);
						}
						Font.textureRebuilt += FontUpdateTracker.<>f__mg$cache0;
					}
					hashSet = new HashSet<Text>();
					FontUpdateTracker.m_Tracked.Add(t.font, hashSet);
				}
				if (!hashSet.Contains(t))
				{
					hashSet.Add(t);
				}
			}
		}

		// Token: 0x06000229 RID: 553 RVA: 0x0000B35C File Offset: 0x0000975C
		private static void RebuildForFont(Font f)
		{
			HashSet<Text> hashSet;
			FontUpdateTracker.m_Tracked.TryGetValue(f, out hashSet);
			if (hashSet != null)
			{
				foreach (Text text in hashSet)
				{
					text.FontTextureChanged();
				}
			}
		}

		// Token: 0x0600022A RID: 554 RVA: 0x0000B3D0 File Offset: 0x000097D0
		public static void UntrackText(Text t)
		{
			if (!(t.font == null))
			{
				HashSet<Text> hashSet;
				FontUpdateTracker.m_Tracked.TryGetValue(t.font, out hashSet);
				if (hashSet != null)
				{
					hashSet.Remove(t);
					if (hashSet.Count == 0)
					{
						FontUpdateTracker.m_Tracked.Remove(t.font);
						if (FontUpdateTracker.m_Tracked.Count == 0)
						{
							if (FontUpdateTracker.<>f__mg$cache1 == null)
							{
								FontUpdateTracker.<>f__mg$cache1 = new Action<Font>(FontUpdateTracker.RebuildForFont);
							}
							Font.textureRebuilt -= FontUpdateTracker.<>f__mg$cache1;
						}
					}
				}
			}
		}

		// Token: 0x0600022B RID: 555 RVA: 0x0000B469 File Offset: 0x00009869
		// Note: this type is marked as 'beforefieldinit'.
		static FontUpdateTracker()
		{
		}

		// Token: 0x0400010A RID: 266
		private static Dictionary<Font, HashSet<Text>> m_Tracked = new Dictionary<Font, HashSet<Text>>();

		// Token: 0x0400010B RID: 267
		[CompilerGenerated]
		private static Action<Font> <>f__mg$cache0;

		// Token: 0x0400010C RID: 268
		[CompilerGenerated]
		private static Action<Font> <>f__mg$cache1;
	}
}
