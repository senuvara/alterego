﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine.EventSystems;
using UnityEngine.Serialization;

namespace UnityEngine.UI
{
	// Token: 0x02000072 RID: 114
	[AddComponentMenu("UI/Selectable", 70)]
	[ExecuteAlways]
	[SelectionBase]
	[DisallowMultipleComponent]
	public class Selectable : UIBehaviour, IMoveHandler, IPointerDownHandler, IPointerUpHandler, IPointerEnterHandler, IPointerExitHandler, ISelectHandler, IDeselectHandler, IEventSystemHandler
	{
		// Token: 0x0600042E RID: 1070 RVA: 0x00006F94 File Offset: 0x00005394
		protected Selectable()
		{
		}

		// Token: 0x17000125 RID: 293
		// (get) Token: 0x0600042F RID: 1071 RVA: 0x00006FEC File Offset: 0x000053EC
		public static List<Selectable> allSelectables
		{
			get
			{
				return Selectable.s_List;
			}
		}

		// Token: 0x17000126 RID: 294
		// (get) Token: 0x06000430 RID: 1072 RVA: 0x00007008 File Offset: 0x00005408
		// (set) Token: 0x06000431 RID: 1073 RVA: 0x00007023 File Offset: 0x00005423
		public Navigation navigation
		{
			get
			{
				return this.m_Navigation;
			}
			set
			{
				if (SetPropertyUtility.SetStruct<Navigation>(ref this.m_Navigation, value))
				{
					this.OnSetProperty();
				}
			}
		}

		// Token: 0x17000127 RID: 295
		// (get) Token: 0x06000432 RID: 1074 RVA: 0x00007040 File Offset: 0x00005440
		// (set) Token: 0x06000433 RID: 1075 RVA: 0x0000705B File Offset: 0x0000545B
		public Selectable.Transition transition
		{
			get
			{
				return this.m_Transition;
			}
			set
			{
				if (SetPropertyUtility.SetStruct<Selectable.Transition>(ref this.m_Transition, value))
				{
					this.OnSetProperty();
				}
			}
		}

		// Token: 0x17000128 RID: 296
		// (get) Token: 0x06000434 RID: 1076 RVA: 0x00007078 File Offset: 0x00005478
		// (set) Token: 0x06000435 RID: 1077 RVA: 0x00007093 File Offset: 0x00005493
		public ColorBlock colors
		{
			get
			{
				return this.m_Colors;
			}
			set
			{
				if (SetPropertyUtility.SetStruct<ColorBlock>(ref this.m_Colors, value))
				{
					this.OnSetProperty();
				}
			}
		}

		// Token: 0x17000129 RID: 297
		// (get) Token: 0x06000436 RID: 1078 RVA: 0x000070B0 File Offset: 0x000054B0
		// (set) Token: 0x06000437 RID: 1079 RVA: 0x000070CB File Offset: 0x000054CB
		public SpriteState spriteState
		{
			get
			{
				return this.m_SpriteState;
			}
			set
			{
				if (SetPropertyUtility.SetStruct<SpriteState>(ref this.m_SpriteState, value))
				{
					this.OnSetProperty();
				}
			}
		}

		// Token: 0x1700012A RID: 298
		// (get) Token: 0x06000438 RID: 1080 RVA: 0x000070E8 File Offset: 0x000054E8
		// (set) Token: 0x06000439 RID: 1081 RVA: 0x00007103 File Offset: 0x00005503
		public AnimationTriggers animationTriggers
		{
			get
			{
				return this.m_AnimationTriggers;
			}
			set
			{
				if (SetPropertyUtility.SetClass<AnimationTriggers>(ref this.m_AnimationTriggers, value))
				{
					this.OnSetProperty();
				}
			}
		}

		// Token: 0x1700012B RID: 299
		// (get) Token: 0x0600043A RID: 1082 RVA: 0x00007120 File Offset: 0x00005520
		// (set) Token: 0x0600043B RID: 1083 RVA: 0x0000713B File Offset: 0x0000553B
		public Graphic targetGraphic
		{
			get
			{
				return this.m_TargetGraphic;
			}
			set
			{
				if (SetPropertyUtility.SetClass<Graphic>(ref this.m_TargetGraphic, value))
				{
					this.OnSetProperty();
				}
			}
		}

		// Token: 0x1700012C RID: 300
		// (get) Token: 0x0600043C RID: 1084 RVA: 0x00007158 File Offset: 0x00005558
		// (set) Token: 0x0600043D RID: 1085 RVA: 0x00007174 File Offset: 0x00005574
		public bool interactable
		{
			get
			{
				return this.m_Interactable;
			}
			set
			{
				if (SetPropertyUtility.SetStruct<bool>(ref this.m_Interactable, value))
				{
					if (!this.m_Interactable && EventSystem.current != null && EventSystem.current.currentSelectedGameObject == base.gameObject)
					{
						EventSystem.current.SetSelectedGameObject(null);
					}
					if (this.m_Interactable)
					{
						this.UpdateSelectionState(null);
					}
					this.OnSetProperty();
				}
			}
		}

		// Token: 0x1700012D RID: 301
		// (get) Token: 0x0600043E RID: 1086 RVA: 0x000071F0 File Offset: 0x000055F0
		// (set) Token: 0x0600043F RID: 1087 RVA: 0x0000720A File Offset: 0x0000560A
		private bool isPointerInside
		{
			[CompilerGenerated]
			get
			{
				return this.<isPointerInside>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<isPointerInside>k__BackingField = value;
			}
		}

		// Token: 0x1700012E RID: 302
		// (get) Token: 0x06000440 RID: 1088 RVA: 0x00007214 File Offset: 0x00005614
		// (set) Token: 0x06000441 RID: 1089 RVA: 0x0000722E File Offset: 0x0000562E
		private bool isPointerDown
		{
			[CompilerGenerated]
			get
			{
				return this.<isPointerDown>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<isPointerDown>k__BackingField = value;
			}
		}

		// Token: 0x1700012F RID: 303
		// (get) Token: 0x06000442 RID: 1090 RVA: 0x00007238 File Offset: 0x00005638
		// (set) Token: 0x06000443 RID: 1091 RVA: 0x00007252 File Offset: 0x00005652
		private bool hasSelection
		{
			[CompilerGenerated]
			get
			{
				return this.<hasSelection>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<hasSelection>k__BackingField = value;
			}
		}

		// Token: 0x17000130 RID: 304
		// (get) Token: 0x06000444 RID: 1092 RVA: 0x0000725C File Offset: 0x0000565C
		// (set) Token: 0x06000445 RID: 1093 RVA: 0x0000727C File Offset: 0x0000567C
		public Image image
		{
			get
			{
				return this.m_TargetGraphic as Image;
			}
			set
			{
				this.m_TargetGraphic = value;
			}
		}

		// Token: 0x17000131 RID: 305
		// (get) Token: 0x06000446 RID: 1094 RVA: 0x00007288 File Offset: 0x00005688
		public Animator animator
		{
			get
			{
				return base.GetComponent<Animator>();
			}
		}

		// Token: 0x06000447 RID: 1095 RVA: 0x000072A3 File Offset: 0x000056A3
		protected override void Awake()
		{
			if (this.m_TargetGraphic == null)
			{
				this.m_TargetGraphic = base.GetComponent<Graphic>();
			}
		}

		// Token: 0x06000448 RID: 1096 RVA: 0x000072C4 File Offset: 0x000056C4
		protected override void OnCanvasGroupChanged()
		{
			bool flag = true;
			Transform transform = base.transform;
			while (transform != null)
			{
				transform.GetComponents<CanvasGroup>(this.m_CanvasGroupCache);
				bool flag2 = false;
				for (int i = 0; i < this.m_CanvasGroupCache.Count; i++)
				{
					if (!this.m_CanvasGroupCache[i].interactable)
					{
						flag = false;
						flag2 = true;
					}
					if (this.m_CanvasGroupCache[i].ignoreParentGroups)
					{
						flag2 = true;
					}
				}
				if (flag2)
				{
					break;
				}
				transform = transform.parent;
			}
			if (flag != this.m_GroupsAllowInteraction)
			{
				this.m_GroupsAllowInteraction = flag;
				this.OnSetProperty();
			}
		}

		// Token: 0x06000449 RID: 1097 RVA: 0x0000737C File Offset: 0x0000577C
		public virtual bool IsInteractable()
		{
			return this.m_GroupsAllowInteraction && this.m_Interactable;
		}

		// Token: 0x0600044A RID: 1098 RVA: 0x000073A5 File Offset: 0x000057A5
		protected override void OnDidApplyAnimationProperties()
		{
			this.OnSetProperty();
		}

		// Token: 0x0600044B RID: 1099 RVA: 0x000073B0 File Offset: 0x000057B0
		protected override void OnEnable()
		{
			base.OnEnable();
			Selectable.s_List.Add(this);
			Selectable.SelectionState currentSelectionState = Selectable.SelectionState.Normal;
			if (this.hasSelection)
			{
				currentSelectionState = Selectable.SelectionState.Highlighted;
			}
			this.m_CurrentSelectionState = currentSelectionState;
			this.InternalEvaluateAndTransitionToSelectionState(true);
		}

		// Token: 0x0600044C RID: 1100 RVA: 0x000073EC File Offset: 0x000057EC
		private void OnSetProperty()
		{
			this.InternalEvaluateAndTransitionToSelectionState(false);
		}

		// Token: 0x0600044D RID: 1101 RVA: 0x000073F6 File Offset: 0x000057F6
		protected override void OnDisable()
		{
			Selectable.s_List.Remove(this);
			this.InstantClearState();
			base.OnDisable();
		}

		// Token: 0x17000132 RID: 306
		// (get) Token: 0x0600044E RID: 1102 RVA: 0x00007414 File Offset: 0x00005814
		protected Selectable.SelectionState currentSelectionState
		{
			get
			{
				return this.m_CurrentSelectionState;
			}
		}

		// Token: 0x0600044F RID: 1103 RVA: 0x00007430 File Offset: 0x00005830
		protected virtual void InstantClearState()
		{
			string normalTrigger = this.m_AnimationTriggers.normalTrigger;
			this.isPointerInside = false;
			this.isPointerDown = false;
			this.hasSelection = false;
			Selectable.Transition transition = this.m_Transition;
			if (transition != Selectable.Transition.ColorTint)
			{
				if (transition != Selectable.Transition.SpriteSwap)
				{
					if (transition == Selectable.Transition.Animation)
					{
						this.TriggerAnimation(normalTrigger);
					}
				}
				else
				{
					this.DoSpriteSwap(null);
				}
			}
			else
			{
				this.StartColorTween(Color.white, true);
			}
		}

		// Token: 0x06000450 RID: 1104 RVA: 0x000074AC File Offset: 0x000058AC
		protected virtual void DoStateTransition(Selectable.SelectionState state, bool instant)
		{
			Color a;
			Sprite newSprite;
			string triggername;
			switch (state)
			{
			case Selectable.SelectionState.Normal:
				a = this.m_Colors.normalColor;
				newSprite = null;
				triggername = this.m_AnimationTriggers.normalTrigger;
				break;
			case Selectable.SelectionState.Highlighted:
				a = this.m_Colors.highlightedColor;
				newSprite = this.m_SpriteState.highlightedSprite;
				triggername = this.m_AnimationTriggers.highlightedTrigger;
				break;
			case Selectable.SelectionState.Pressed:
				a = this.m_Colors.pressedColor;
				newSprite = this.m_SpriteState.pressedSprite;
				triggername = this.m_AnimationTriggers.pressedTrigger;
				break;
			case Selectable.SelectionState.Disabled:
				a = this.m_Colors.disabledColor;
				newSprite = this.m_SpriteState.disabledSprite;
				triggername = this.m_AnimationTriggers.disabledTrigger;
				break;
			default:
				a = Color.black;
				newSprite = null;
				triggername = string.Empty;
				break;
			}
			if (base.gameObject.activeInHierarchy)
			{
				Selectable.Transition transition = this.m_Transition;
				if (transition != Selectable.Transition.ColorTint)
				{
					if (transition != Selectable.Transition.SpriteSwap)
					{
						if (transition == Selectable.Transition.Animation)
						{
							this.TriggerAnimation(triggername);
						}
					}
					else
					{
						this.DoSpriteSwap(newSprite);
					}
				}
				else
				{
					this.StartColorTween(a * this.m_Colors.colorMultiplier, instant);
				}
			}
		}

		// Token: 0x06000451 RID: 1105 RVA: 0x000075EC File Offset: 0x000059EC
		public Selectable FindSelectable(Vector3 dir)
		{
			dir = dir.normalized;
			Vector3 v = Quaternion.Inverse(base.transform.rotation) * dir;
			Vector3 b = base.transform.TransformPoint(Selectable.GetPointOnRectEdge(base.transform as RectTransform, v));
			float num = float.NegativeInfinity;
			Selectable result = null;
			for (int i = 0; i < Selectable.s_List.Count; i++)
			{
				Selectable selectable = Selectable.s_List[i];
				if (!(selectable == this) && !(selectable == null))
				{
					if (selectable.IsInteractable() && selectable.navigation.mode != Navigation.Mode.None)
					{
						RectTransform rectTransform = selectable.transform as RectTransform;
						Vector3 position = (!(rectTransform != null)) ? Vector3.zero : rectTransform.rect.center;
						Vector3 rhs = selectable.transform.TransformPoint(position) - b;
						float num2 = Vector3.Dot(dir, rhs);
						if (num2 > 0f)
						{
							float num3 = num2 / rhs.sqrMagnitude;
							if (num3 > num)
							{
								num = num3;
								result = selectable;
							}
						}
					}
				}
			}
			return result;
		}

		// Token: 0x06000452 RID: 1106 RVA: 0x0000774C File Offset: 0x00005B4C
		private static Vector3 GetPointOnRectEdge(RectTransform rect, Vector2 dir)
		{
			Vector3 result;
			if (rect == null)
			{
				result = Vector3.zero;
			}
			else
			{
				if (dir != Vector2.zero)
				{
					dir /= Mathf.Max(Mathf.Abs(dir.x), Mathf.Abs(dir.y));
				}
				dir = rect.rect.center + Vector2.Scale(rect.rect.size, dir * 0.5f);
				result = dir;
			}
			return result;
		}

		// Token: 0x06000453 RID: 1107 RVA: 0x000077E6 File Offset: 0x00005BE6
		private void Navigate(AxisEventData eventData, Selectable sel)
		{
			if (sel != null && sel.IsActive())
			{
				eventData.selectedObject = sel.gameObject;
			}
		}

		// Token: 0x06000454 RID: 1108 RVA: 0x0000780C File Offset: 0x00005C0C
		public virtual Selectable FindSelectableOnLeft()
		{
			Selectable result;
			if (this.m_Navigation.mode == Navigation.Mode.Explicit)
			{
				result = this.m_Navigation.selectOnLeft;
			}
			else if ((this.m_Navigation.mode & Navigation.Mode.Horizontal) != Navigation.Mode.None)
			{
				result = this.FindSelectable(base.transform.rotation * Vector3.left);
			}
			else
			{
				result = null;
			}
			return result;
		}

		// Token: 0x06000455 RID: 1109 RVA: 0x0000787C File Offset: 0x00005C7C
		public virtual Selectable FindSelectableOnRight()
		{
			Selectable result;
			if (this.m_Navigation.mode == Navigation.Mode.Explicit)
			{
				result = this.m_Navigation.selectOnRight;
			}
			else if ((this.m_Navigation.mode & Navigation.Mode.Horizontal) != Navigation.Mode.None)
			{
				result = this.FindSelectable(base.transform.rotation * Vector3.right);
			}
			else
			{
				result = null;
			}
			return result;
		}

		// Token: 0x06000456 RID: 1110 RVA: 0x000078EC File Offset: 0x00005CEC
		public virtual Selectable FindSelectableOnUp()
		{
			Selectable result;
			if (this.m_Navigation.mode == Navigation.Mode.Explicit)
			{
				result = this.m_Navigation.selectOnUp;
			}
			else if ((this.m_Navigation.mode & Navigation.Mode.Vertical) != Navigation.Mode.None)
			{
				result = this.FindSelectable(base.transform.rotation * Vector3.up);
			}
			else
			{
				result = null;
			}
			return result;
		}

		// Token: 0x06000457 RID: 1111 RVA: 0x0000795C File Offset: 0x00005D5C
		public virtual Selectable FindSelectableOnDown()
		{
			Selectable result;
			if (this.m_Navigation.mode == Navigation.Mode.Explicit)
			{
				result = this.m_Navigation.selectOnDown;
			}
			else if ((this.m_Navigation.mode & Navigation.Mode.Vertical) != Navigation.Mode.None)
			{
				result = this.FindSelectable(base.transform.rotation * Vector3.down);
			}
			else
			{
				result = null;
			}
			return result;
		}

		// Token: 0x06000458 RID: 1112 RVA: 0x000079CC File Offset: 0x00005DCC
		public virtual void OnMove(AxisEventData eventData)
		{
			switch (eventData.moveDir)
			{
			case MoveDirection.Left:
				this.Navigate(eventData, this.FindSelectableOnLeft());
				break;
			case MoveDirection.Up:
				this.Navigate(eventData, this.FindSelectableOnUp());
				break;
			case MoveDirection.Right:
				this.Navigate(eventData, this.FindSelectableOnRight());
				break;
			case MoveDirection.Down:
				this.Navigate(eventData, this.FindSelectableOnDown());
				break;
			}
		}

		// Token: 0x06000459 RID: 1113 RVA: 0x00007A44 File Offset: 0x00005E44
		private void StartColorTween(Color targetColor, bool instant)
		{
			if (!(this.m_TargetGraphic == null))
			{
				this.m_TargetGraphic.CrossFadeColor(targetColor, (!instant) ? this.m_Colors.fadeDuration : 0f, true, true);
			}
		}

		// Token: 0x0600045A RID: 1114 RVA: 0x00007A91 File Offset: 0x00005E91
		private void DoSpriteSwap(Sprite newSprite)
		{
			if (!(this.image == null))
			{
				this.image.overrideSprite = newSprite;
			}
		}

		// Token: 0x0600045B RID: 1115 RVA: 0x00007AB8 File Offset: 0x00005EB8
		private void TriggerAnimation(string triggername)
		{
			if (this.transition == Selectable.Transition.Animation && !(this.animator == null) && this.animator.isActiveAndEnabled && this.animator.hasBoundPlayables && !string.IsNullOrEmpty(triggername))
			{
				this.animator.ResetTrigger(this.m_AnimationTriggers.normalTrigger);
				this.animator.ResetTrigger(this.m_AnimationTriggers.pressedTrigger);
				this.animator.ResetTrigger(this.m_AnimationTriggers.highlightedTrigger);
				this.animator.ResetTrigger(this.m_AnimationTriggers.disabledTrigger);
				this.animator.SetTrigger(triggername);
			}
		}

		// Token: 0x0600045C RID: 1116 RVA: 0x00007B78 File Offset: 0x00005F78
		protected bool IsHighlighted(BaseEventData eventData)
		{
			bool result;
			if (!this.IsActive())
			{
				result = false;
			}
			else if (this.IsPressed())
			{
				result = false;
			}
			else
			{
				bool flag = this.hasSelection;
				if (eventData is PointerEventData)
				{
					PointerEventData pointerEventData = eventData as PointerEventData;
					flag |= ((this.isPointerDown && !this.isPointerInside && pointerEventData.pointerPress == base.gameObject) || (!this.isPointerDown && this.isPointerInside && pointerEventData.pointerPress == base.gameObject) || (!this.isPointerDown && this.isPointerInside && pointerEventData.pointerPress == null));
				}
				else
				{
					flag |= this.isPointerInside;
				}
				result = flag;
			}
			return result;
		}

		// Token: 0x0600045D RID: 1117 RVA: 0x00007C60 File Offset: 0x00006060
		[Obsolete("Is Pressed no longer requires eventData", false)]
		protected bool IsPressed(BaseEventData eventData)
		{
			return this.IsPressed();
		}

		// Token: 0x0600045E RID: 1118 RVA: 0x00007C7C File Offset: 0x0000607C
		protected bool IsPressed()
		{
			return this.IsActive() && this.isPointerInside && this.isPointerDown;
		}

		// Token: 0x0600045F RID: 1119 RVA: 0x00007CB7 File Offset: 0x000060B7
		protected void UpdateSelectionState(BaseEventData eventData)
		{
			if (this.IsPressed())
			{
				this.m_CurrentSelectionState = Selectable.SelectionState.Pressed;
			}
			else if (this.IsHighlighted(eventData))
			{
				this.m_CurrentSelectionState = Selectable.SelectionState.Highlighted;
			}
			else
			{
				this.m_CurrentSelectionState = Selectable.SelectionState.Normal;
			}
		}

		// Token: 0x06000460 RID: 1120 RVA: 0x00007CF2 File Offset: 0x000060F2
		private void EvaluateAndTransitionToSelectionState(BaseEventData eventData)
		{
			if (this.IsActive() && this.IsInteractable())
			{
				this.UpdateSelectionState(eventData);
				this.InternalEvaluateAndTransitionToSelectionState(false);
			}
		}

		// Token: 0x06000461 RID: 1121 RVA: 0x00007D20 File Offset: 0x00006120
		private void InternalEvaluateAndTransitionToSelectionState(bool instant)
		{
			Selectable.SelectionState state = this.m_CurrentSelectionState;
			if (this.IsActive() && !this.IsInteractable())
			{
				state = Selectable.SelectionState.Disabled;
			}
			this.DoStateTransition(state, instant);
		}

		// Token: 0x06000462 RID: 1122 RVA: 0x00007D58 File Offset: 0x00006158
		public virtual void OnPointerDown(PointerEventData eventData)
		{
			if (eventData.button == PointerEventData.InputButton.Left)
			{
				if (this.IsInteractable() && this.navigation.mode != Navigation.Mode.None && EventSystem.current != null)
				{
					EventSystem.current.SetSelectedGameObject(base.gameObject, eventData);
				}
				this.isPointerDown = true;
				this.EvaluateAndTransitionToSelectionState(eventData);
			}
		}

		// Token: 0x06000463 RID: 1123 RVA: 0x00007DC3 File Offset: 0x000061C3
		public virtual void OnPointerUp(PointerEventData eventData)
		{
			if (eventData.button == PointerEventData.InputButton.Left)
			{
				this.isPointerDown = false;
				this.EvaluateAndTransitionToSelectionState(eventData);
			}
		}

		// Token: 0x06000464 RID: 1124 RVA: 0x00007DE4 File Offset: 0x000061E4
		public virtual void OnPointerEnter(PointerEventData eventData)
		{
			this.isPointerInside = true;
			this.EvaluateAndTransitionToSelectionState(eventData);
		}

		// Token: 0x06000465 RID: 1125 RVA: 0x00007DF5 File Offset: 0x000061F5
		public virtual void OnPointerExit(PointerEventData eventData)
		{
			this.isPointerInside = false;
			this.EvaluateAndTransitionToSelectionState(eventData);
		}

		// Token: 0x06000466 RID: 1126 RVA: 0x00007E06 File Offset: 0x00006206
		public virtual void OnSelect(BaseEventData eventData)
		{
			this.hasSelection = true;
			this.EvaluateAndTransitionToSelectionState(eventData);
		}

		// Token: 0x06000467 RID: 1127 RVA: 0x00007E17 File Offset: 0x00006217
		public virtual void OnDeselect(BaseEventData eventData)
		{
			this.hasSelection = false;
			this.EvaluateAndTransitionToSelectionState(eventData);
		}

		// Token: 0x06000468 RID: 1128 RVA: 0x00007E28 File Offset: 0x00006228
		public virtual void Select()
		{
			if (!(EventSystem.current == null) && !EventSystem.current.alreadySelecting)
			{
				EventSystem.current.SetSelectedGameObject(base.gameObject);
			}
		}

		// Token: 0x06000469 RID: 1129 RVA: 0x00007E5F File Offset: 0x0000625F
		// Note: this type is marked as 'beforefieldinit'.
		static Selectable()
		{
		}

		// Token: 0x04000214 RID: 532
		private static List<Selectable> s_List = new List<Selectable>();

		// Token: 0x04000215 RID: 533
		[FormerlySerializedAs("navigation")]
		[SerializeField]
		private Navigation m_Navigation = Navigation.defaultNavigation;

		// Token: 0x04000216 RID: 534
		[FormerlySerializedAs("transition")]
		[SerializeField]
		private Selectable.Transition m_Transition = Selectable.Transition.ColorTint;

		// Token: 0x04000217 RID: 535
		[FormerlySerializedAs("colors")]
		[SerializeField]
		private ColorBlock m_Colors = ColorBlock.defaultColorBlock;

		// Token: 0x04000218 RID: 536
		[FormerlySerializedAs("spriteState")]
		[SerializeField]
		private SpriteState m_SpriteState;

		// Token: 0x04000219 RID: 537
		[FormerlySerializedAs("animationTriggers")]
		[SerializeField]
		private AnimationTriggers m_AnimationTriggers = new AnimationTriggers();

		// Token: 0x0400021A RID: 538
		[Tooltip("Can the Selectable be interacted with?")]
		[SerializeField]
		private bool m_Interactable = true;

		// Token: 0x0400021B RID: 539
		[FormerlySerializedAs("highlightGraphic")]
		[FormerlySerializedAs("m_HighlightGraphic")]
		[SerializeField]
		private Graphic m_TargetGraphic;

		// Token: 0x0400021C RID: 540
		private bool m_GroupsAllowInteraction = true;

		// Token: 0x0400021D RID: 541
		private Selectable.SelectionState m_CurrentSelectionState;

		// Token: 0x0400021E RID: 542
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private bool <isPointerInside>k__BackingField;

		// Token: 0x0400021F RID: 543
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private bool <isPointerDown>k__BackingField;

		// Token: 0x04000220 RID: 544
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private bool <hasSelection>k__BackingField;

		// Token: 0x04000221 RID: 545
		private readonly List<CanvasGroup> m_CanvasGroupCache = new List<CanvasGroup>();

		// Token: 0x02000073 RID: 115
		public enum Transition
		{
			// Token: 0x04000223 RID: 547
			None,
			// Token: 0x04000224 RID: 548
			ColorTint,
			// Token: 0x04000225 RID: 549
			SpriteSwap,
			// Token: 0x04000226 RID: 550
			Animation
		}

		// Token: 0x02000074 RID: 116
		protected enum SelectionState
		{
			// Token: 0x04000228 RID: 552
			Normal,
			// Token: 0x04000229 RID: 553
			Highlighted,
			// Token: 0x0400022A RID: 554
			Pressed,
			// Token: 0x0400022B RID: 555
			Disabled
		}
	}
}
