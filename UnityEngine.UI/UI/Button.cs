﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.Serialization;

namespace UnityEngine.UI
{
	// Token: 0x0200003A RID: 58
	[AddComponentMenu("UI/Button", 30)]
	public class Button : Selectable, IPointerClickHandler, ISubmitHandler, IEventSystemHandler
	{
		// Token: 0x06000185 RID: 389 RVA: 0x00007E6B File Offset: 0x0000626B
		protected Button()
		{
		}

		// Token: 0x1700006A RID: 106
		// (get) Token: 0x06000186 RID: 390 RVA: 0x00007E80 File Offset: 0x00006280
		// (set) Token: 0x06000187 RID: 391 RVA: 0x00007E9B File Offset: 0x0000629B
		public Button.ButtonClickedEvent onClick
		{
			get
			{
				return this.m_OnClick;
			}
			set
			{
				this.m_OnClick = value;
			}
		}

		// Token: 0x06000188 RID: 392 RVA: 0x00007EA5 File Offset: 0x000062A5
		private void Press()
		{
			if (this.IsActive() && this.IsInteractable())
			{
				UISystemProfilerApi.AddMarker("Button.onClick", this);
				this.m_OnClick.Invoke();
			}
		}

		// Token: 0x06000189 RID: 393 RVA: 0x00007ED9 File Offset: 0x000062D9
		public virtual void OnPointerClick(PointerEventData eventData)
		{
			if (eventData.button == PointerEventData.InputButton.Left)
			{
				this.Press();
			}
		}

		// Token: 0x0600018A RID: 394 RVA: 0x00007EF2 File Offset: 0x000062F2
		public virtual void OnSubmit(BaseEventData eventData)
		{
			this.Press();
			if (this.IsActive() && this.IsInteractable())
			{
				this.DoStateTransition(Selectable.SelectionState.Pressed, false);
				base.StartCoroutine(this.OnFinishSubmit());
			}
		}

		// Token: 0x0600018B RID: 395 RVA: 0x00007F2C File Offset: 0x0000632C
		private IEnumerator OnFinishSubmit()
		{
			float fadeTime = base.colors.fadeDuration;
			float elapsedTime = 0f;
			while (elapsedTime < fadeTime)
			{
				elapsedTime += Time.unscaledDeltaTime;
				yield return null;
			}
			this.DoStateTransition(base.currentSelectionState, false);
			yield break;
		}

		// Token: 0x040000C4 RID: 196
		[FormerlySerializedAs("onClick")]
		[SerializeField]
		private Button.ButtonClickedEvent m_OnClick = new Button.ButtonClickedEvent();

		// Token: 0x0200003B RID: 59
		[Serializable]
		public class ButtonClickedEvent : UnityEvent
		{
			// Token: 0x0600018C RID: 396 RVA: 0x00007F4E File Offset: 0x0000634E
			public ButtonClickedEvent()
			{
			}
		}

		// Token: 0x020000B4 RID: 180
		[CompilerGenerated]
		private sealed class <OnFinishSubmit>c__Iterator0 : IEnumerator, IDisposable, IEnumerator<object>
		{
			// Token: 0x06000654 RID: 1620 RVA: 0x00007F56 File Offset: 0x00006356
			[DebuggerHidden]
			public <OnFinishSubmit>c__Iterator0()
			{
			}

			// Token: 0x06000655 RID: 1621 RVA: 0x00007F60 File Offset: 0x00006360
			public bool MoveNext()
			{
				uint num = (uint)this.$PC;
				this.$PC = -1;
				switch (num)
				{
				case 0U:
					fadeTime = base.colors.fadeDuration;
					elapsedTime = 0f;
					break;
				case 1U:
					break;
				default:
					return false;
				}
				if (elapsedTime < fadeTime)
				{
					elapsedTime += Time.unscaledDeltaTime;
					this.$current = null;
					if (!this.$disposing)
					{
						this.$PC = 1;
					}
					return true;
				}
				this.DoStateTransition(base.currentSelectionState, false);
				this.$PC = -1;
				return false;
			}

			// Token: 0x170001A7 RID: 423
			// (get) Token: 0x06000656 RID: 1622 RVA: 0x0000801C File Offset: 0x0000641C
			object IEnumerator<object>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.$current;
				}
			}

			// Token: 0x170001A8 RID: 424
			// (get) Token: 0x06000657 RID: 1623 RVA: 0x00008038 File Offset: 0x00006438
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.$current;
				}
			}

			// Token: 0x06000658 RID: 1624 RVA: 0x00008052 File Offset: 0x00006452
			[DebuggerHidden]
			public void Dispose()
			{
				this.$disposing = true;
				this.$PC = -1;
			}

			// Token: 0x06000659 RID: 1625 RVA: 0x00008062 File Offset: 0x00006462
			[DebuggerHidden]
			public void Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x040002F5 RID: 757
			internal float <fadeTime>__0;

			// Token: 0x040002F6 RID: 758
			internal float <elapsedTime>__0;

			// Token: 0x040002F7 RID: 759
			internal Button $this;

			// Token: 0x040002F8 RID: 760
			internal object $current;

			// Token: 0x040002F9 RID: 761
			internal bool $disposing;

			// Token: 0x040002FA RID: 762
			internal int $PC;
		}
	}
}
