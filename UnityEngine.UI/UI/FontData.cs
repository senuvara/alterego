﻿using System;
using UnityEngine.Serialization;

namespace UnityEngine.UI
{
	// Token: 0x02000047 RID: 71
	[Serializable]
	public class FontData : ISerializationCallbackReceiver
	{
		// Token: 0x0600020C RID: 524 RVA: 0x0000B005 File Offset: 0x00009405
		public FontData()
		{
		}

		// Token: 0x17000083 RID: 131
		// (get) Token: 0x0600020D RID: 525 RVA: 0x0000B010 File Offset: 0x00009410
		public static FontData defaultFontData
		{
			get
			{
				return new FontData
				{
					m_FontSize = 14,
					m_LineSpacing = 1f,
					m_FontStyle = FontStyle.Normal,
					m_BestFit = false,
					m_MinSize = 10,
					m_MaxSize = 40,
					m_Alignment = TextAnchor.UpperLeft,
					m_HorizontalOverflow = HorizontalWrapMode.Wrap,
					m_VerticalOverflow = VerticalWrapMode.Truncate,
					m_RichText = true,
					m_AlignByGeometry = false
				};
			}
		}

		// Token: 0x17000084 RID: 132
		// (get) Token: 0x0600020E RID: 526 RVA: 0x0000B084 File Offset: 0x00009484
		// (set) Token: 0x0600020F RID: 527 RVA: 0x0000B09F File Offset: 0x0000949F
		public Font font
		{
			get
			{
				return this.m_Font;
			}
			set
			{
				this.m_Font = value;
			}
		}

		// Token: 0x17000085 RID: 133
		// (get) Token: 0x06000210 RID: 528 RVA: 0x0000B0AC File Offset: 0x000094AC
		// (set) Token: 0x06000211 RID: 529 RVA: 0x0000B0C7 File Offset: 0x000094C7
		public int fontSize
		{
			get
			{
				return this.m_FontSize;
			}
			set
			{
				this.m_FontSize = value;
			}
		}

		// Token: 0x17000086 RID: 134
		// (get) Token: 0x06000212 RID: 530 RVA: 0x0000B0D4 File Offset: 0x000094D4
		// (set) Token: 0x06000213 RID: 531 RVA: 0x0000B0EF File Offset: 0x000094EF
		public FontStyle fontStyle
		{
			get
			{
				return this.m_FontStyle;
			}
			set
			{
				this.m_FontStyle = value;
			}
		}

		// Token: 0x17000087 RID: 135
		// (get) Token: 0x06000214 RID: 532 RVA: 0x0000B0FC File Offset: 0x000094FC
		// (set) Token: 0x06000215 RID: 533 RVA: 0x0000B117 File Offset: 0x00009517
		public bool bestFit
		{
			get
			{
				return this.m_BestFit;
			}
			set
			{
				this.m_BestFit = value;
			}
		}

		// Token: 0x17000088 RID: 136
		// (get) Token: 0x06000216 RID: 534 RVA: 0x0000B124 File Offset: 0x00009524
		// (set) Token: 0x06000217 RID: 535 RVA: 0x0000B13F File Offset: 0x0000953F
		public int minSize
		{
			get
			{
				return this.m_MinSize;
			}
			set
			{
				this.m_MinSize = value;
			}
		}

		// Token: 0x17000089 RID: 137
		// (get) Token: 0x06000218 RID: 536 RVA: 0x0000B14C File Offset: 0x0000954C
		// (set) Token: 0x06000219 RID: 537 RVA: 0x0000B167 File Offset: 0x00009567
		public int maxSize
		{
			get
			{
				return this.m_MaxSize;
			}
			set
			{
				this.m_MaxSize = value;
			}
		}

		// Token: 0x1700008A RID: 138
		// (get) Token: 0x0600021A RID: 538 RVA: 0x0000B174 File Offset: 0x00009574
		// (set) Token: 0x0600021B RID: 539 RVA: 0x0000B18F File Offset: 0x0000958F
		public TextAnchor alignment
		{
			get
			{
				return this.m_Alignment;
			}
			set
			{
				this.m_Alignment = value;
			}
		}

		// Token: 0x1700008B RID: 139
		// (get) Token: 0x0600021C RID: 540 RVA: 0x0000B19C File Offset: 0x0000959C
		// (set) Token: 0x0600021D RID: 541 RVA: 0x0000B1B7 File Offset: 0x000095B7
		public bool alignByGeometry
		{
			get
			{
				return this.m_AlignByGeometry;
			}
			set
			{
				this.m_AlignByGeometry = value;
			}
		}

		// Token: 0x1700008C RID: 140
		// (get) Token: 0x0600021E RID: 542 RVA: 0x0000B1C4 File Offset: 0x000095C4
		// (set) Token: 0x0600021F RID: 543 RVA: 0x0000B1DF File Offset: 0x000095DF
		public bool richText
		{
			get
			{
				return this.m_RichText;
			}
			set
			{
				this.m_RichText = value;
			}
		}

		// Token: 0x1700008D RID: 141
		// (get) Token: 0x06000220 RID: 544 RVA: 0x0000B1EC File Offset: 0x000095EC
		// (set) Token: 0x06000221 RID: 545 RVA: 0x0000B207 File Offset: 0x00009607
		public HorizontalWrapMode horizontalOverflow
		{
			get
			{
				return this.m_HorizontalOverflow;
			}
			set
			{
				this.m_HorizontalOverflow = value;
			}
		}

		// Token: 0x1700008E RID: 142
		// (get) Token: 0x06000222 RID: 546 RVA: 0x0000B214 File Offset: 0x00009614
		// (set) Token: 0x06000223 RID: 547 RVA: 0x0000B22F File Offset: 0x0000962F
		public VerticalWrapMode verticalOverflow
		{
			get
			{
				return this.m_VerticalOverflow;
			}
			set
			{
				this.m_VerticalOverflow = value;
			}
		}

		// Token: 0x1700008F RID: 143
		// (get) Token: 0x06000224 RID: 548 RVA: 0x0000B23C File Offset: 0x0000963C
		// (set) Token: 0x06000225 RID: 549 RVA: 0x0000B257 File Offset: 0x00009657
		public float lineSpacing
		{
			get
			{
				return this.m_LineSpacing;
			}
			set
			{
				this.m_LineSpacing = value;
			}
		}

		// Token: 0x06000226 RID: 550 RVA: 0x0000B261 File Offset: 0x00009661
		void ISerializationCallbackReceiver.OnBeforeSerialize()
		{
		}

		// Token: 0x06000227 RID: 551 RVA: 0x0000B264 File Offset: 0x00009664
		void ISerializationCallbackReceiver.OnAfterDeserialize()
		{
			this.m_FontSize = Mathf.Clamp(this.m_FontSize, 0, 300);
			this.m_MinSize = Mathf.Clamp(this.m_MinSize, 0, this.m_FontSize);
			this.m_MaxSize = Mathf.Clamp(this.m_MaxSize, this.m_FontSize, 300);
		}

		// Token: 0x040000FE RID: 254
		[SerializeField]
		[FormerlySerializedAs("font")]
		private Font m_Font;

		// Token: 0x040000FF RID: 255
		[SerializeField]
		[FormerlySerializedAs("fontSize")]
		private int m_FontSize;

		// Token: 0x04000100 RID: 256
		[SerializeField]
		[FormerlySerializedAs("fontStyle")]
		private FontStyle m_FontStyle;

		// Token: 0x04000101 RID: 257
		[SerializeField]
		private bool m_BestFit;

		// Token: 0x04000102 RID: 258
		[SerializeField]
		private int m_MinSize;

		// Token: 0x04000103 RID: 259
		[SerializeField]
		private int m_MaxSize;

		// Token: 0x04000104 RID: 260
		[SerializeField]
		[FormerlySerializedAs("alignment")]
		private TextAnchor m_Alignment;

		// Token: 0x04000105 RID: 261
		[SerializeField]
		private bool m_AlignByGeometry;

		// Token: 0x04000106 RID: 262
		[SerializeField]
		[FormerlySerializedAs("richText")]
		private bool m_RichText;

		// Token: 0x04000107 RID: 263
		[SerializeField]
		private HorizontalWrapMode m_HorizontalOverflow;

		// Token: 0x04000108 RID: 264
		[SerializeField]
		private VerticalWrapMode m_VerticalOverflow;

		// Token: 0x04000109 RID: 265
		[SerializeField]
		private float m_LineSpacing;
	}
}
