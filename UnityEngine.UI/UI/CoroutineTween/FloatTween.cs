﻿using System;
using UnityEngine.Events;

namespace UnityEngine.UI.CoroutineTween
{
	// Token: 0x02000036 RID: 54
	internal struct FloatTween : ITweenValue
	{
		// Token: 0x17000062 RID: 98
		// (get) Token: 0x06000169 RID: 361 RVA: 0x00006B04 File Offset: 0x00004F04
		// (set) Token: 0x0600016A RID: 362 RVA: 0x00006B1F File Offset: 0x00004F1F
		public float startValue
		{
			get
			{
				return this.m_StartValue;
			}
			set
			{
				this.m_StartValue = value;
			}
		}

		// Token: 0x17000063 RID: 99
		// (get) Token: 0x0600016B RID: 363 RVA: 0x00006B2C File Offset: 0x00004F2C
		// (set) Token: 0x0600016C RID: 364 RVA: 0x00006B47 File Offset: 0x00004F47
		public float targetValue
		{
			get
			{
				return this.m_TargetValue;
			}
			set
			{
				this.m_TargetValue = value;
			}
		}

		// Token: 0x17000064 RID: 100
		// (get) Token: 0x0600016D RID: 365 RVA: 0x00006B54 File Offset: 0x00004F54
		// (set) Token: 0x0600016E RID: 366 RVA: 0x00006B6F File Offset: 0x00004F6F
		public float duration
		{
			get
			{
				return this.m_Duration;
			}
			set
			{
				this.m_Duration = value;
			}
		}

		// Token: 0x17000065 RID: 101
		// (get) Token: 0x0600016F RID: 367 RVA: 0x00006B7C File Offset: 0x00004F7C
		// (set) Token: 0x06000170 RID: 368 RVA: 0x00006B97 File Offset: 0x00004F97
		public bool ignoreTimeScale
		{
			get
			{
				return this.m_IgnoreTimeScale;
			}
			set
			{
				this.m_IgnoreTimeScale = value;
			}
		}

		// Token: 0x06000171 RID: 369 RVA: 0x00006BA4 File Offset: 0x00004FA4
		public void TweenValue(float floatPercentage)
		{
			if (this.ValidTarget())
			{
				float arg = Mathf.Lerp(this.m_StartValue, this.m_TargetValue, floatPercentage);
				this.m_Target.Invoke(arg);
			}
		}

		// Token: 0x06000172 RID: 370 RVA: 0x00006BE1 File Offset: 0x00004FE1
		public void AddOnChangedCallback(UnityAction<float> callback)
		{
			if (this.m_Target == null)
			{
				this.m_Target = new FloatTween.FloatTweenCallback();
			}
			this.m_Target.AddListener(callback);
		}

		// Token: 0x06000173 RID: 371 RVA: 0x00006C08 File Offset: 0x00005008
		public bool GetIgnoreTimescale()
		{
			return this.m_IgnoreTimeScale;
		}

		// Token: 0x06000174 RID: 372 RVA: 0x00006C24 File Offset: 0x00005024
		public float GetDuration()
		{
			return this.m_Duration;
		}

		// Token: 0x06000175 RID: 373 RVA: 0x00006C40 File Offset: 0x00005040
		public bool ValidTarget()
		{
			return this.m_Target != null;
		}

		// Token: 0x040000B5 RID: 181
		private FloatTween.FloatTweenCallback m_Target;

		// Token: 0x040000B6 RID: 182
		private float m_StartValue;

		// Token: 0x040000B7 RID: 183
		private float m_TargetValue;

		// Token: 0x040000B8 RID: 184
		private float m_Duration;

		// Token: 0x040000B9 RID: 185
		private bool m_IgnoreTimeScale;

		// Token: 0x02000037 RID: 55
		public class FloatTweenCallback : UnityEvent<float>
		{
			// Token: 0x06000176 RID: 374 RVA: 0x00006C61 File Offset: 0x00005061
			public FloatTweenCallback()
			{
			}
		}
	}
}
