﻿using System;

namespace UnityEngine.UI.CoroutineTween
{
	// Token: 0x02000032 RID: 50
	internal interface ITweenValue
	{
		// Token: 0x06000155 RID: 341
		void TweenValue(float floatPercentage);

		// Token: 0x1700005B RID: 91
		// (get) Token: 0x06000156 RID: 342
		bool ignoreTimeScale { get; }

		// Token: 0x1700005C RID: 92
		// (get) Token: 0x06000157 RID: 343
		float duration { get; }

		// Token: 0x06000158 RID: 344
		bool ValidTarget();
	}
}
