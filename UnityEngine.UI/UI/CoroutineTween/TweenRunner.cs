﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace UnityEngine.UI.CoroutineTween
{
	// Token: 0x02000038 RID: 56
	internal class TweenRunner<T> where T : struct, ITweenValue
	{
		// Token: 0x06000177 RID: 375 RVA: 0x00006C69 File Offset: 0x00005069
		public TweenRunner()
		{
		}

		// Token: 0x06000178 RID: 376 RVA: 0x00006C74 File Offset: 0x00005074
		private static IEnumerator Start(T tweenInfo)
		{
			if (!tweenInfo.ValidTarget())
			{
				yield break;
			}
			float elapsedTime = 0f;
			while (elapsedTime < tweenInfo.duration)
			{
				elapsedTime += ((!tweenInfo.ignoreTimeScale) ? Time.deltaTime : Time.unscaledDeltaTime);
				float percentage = Mathf.Clamp01(elapsedTime / tweenInfo.duration);
				tweenInfo.TweenValue(percentage);
				yield return null;
			}
			tweenInfo.TweenValue(1f);
			yield break;
		}

		// Token: 0x06000179 RID: 377 RVA: 0x00006C96 File Offset: 0x00005096
		public void Init(MonoBehaviour coroutineContainer)
		{
			this.m_CoroutineContainer = coroutineContainer;
		}

		// Token: 0x0600017A RID: 378 RVA: 0x00006CA0 File Offset: 0x000050A0
		public void StartTween(T info)
		{
			if (this.m_CoroutineContainer == null)
			{
				Debug.LogWarning("Coroutine container not configured... did you forget to call Init?");
			}
			else
			{
				this.StopTween();
				if (!this.m_CoroutineContainer.gameObject.activeInHierarchy)
				{
					info.TweenValue(1f);
				}
				else
				{
					this.m_Tween = TweenRunner<T>.Start(info);
					this.m_CoroutineContainer.StartCoroutine(this.m_Tween);
				}
			}
		}

		// Token: 0x0600017B RID: 379 RVA: 0x00006D20 File Offset: 0x00005120
		public void StopTween()
		{
			if (this.m_Tween != null)
			{
				this.m_CoroutineContainer.StopCoroutine(this.m_Tween);
				this.m_Tween = null;
			}
		}

		// Token: 0x040000BA RID: 186
		protected MonoBehaviour m_CoroutineContainer;

		// Token: 0x040000BB RID: 187
		protected IEnumerator m_Tween;

		// Token: 0x020000B3 RID: 179
		[CompilerGenerated]
		private sealed class <Start>c__Iterator0 : IEnumerator, IDisposable, IEnumerator<object>
		{
			// Token: 0x0600064E RID: 1614 RVA: 0x00006D48 File Offset: 0x00005148
			[DebuggerHidden]
			public <Start>c__Iterator0()
			{
			}

			// Token: 0x0600064F RID: 1615 RVA: 0x00006D50 File Offset: 0x00005150
			public bool MoveNext()
			{
				uint num = (uint)this.$PC;
				this.$PC = -1;
				switch (num)
				{
				case 0U:
					if (!tweenInfo.ValidTarget())
					{
						return false;
					}
					elapsedTime = 0f;
					break;
				case 1U:
					break;
				default:
					return false;
				}
				if (elapsedTime < tweenInfo.duration)
				{
					elapsedTime += ((!tweenInfo.ignoreTimeScale) ? Time.deltaTime : Time.unscaledDeltaTime);
					percentage = Mathf.Clamp01(elapsedTime / tweenInfo.duration);
					tweenInfo.TweenValue(percentage);
					this.$current = null;
					if (!this.$disposing)
					{
						this.$PC = 1;
					}
					return true;
				}
				tweenInfo.TweenValue(1f);
				this.$PC = -1;
				return false;
			}

			// Token: 0x170001A5 RID: 421
			// (get) Token: 0x06000650 RID: 1616 RVA: 0x00006E70 File Offset: 0x00005270
			object IEnumerator<object>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.$current;
				}
			}

			// Token: 0x170001A6 RID: 422
			// (get) Token: 0x06000651 RID: 1617 RVA: 0x00006E8C File Offset: 0x0000528C
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.$current;
				}
			}

			// Token: 0x06000652 RID: 1618 RVA: 0x00006EA6 File Offset: 0x000052A6
			[DebuggerHidden]
			public void Dispose()
			{
				this.$disposing = true;
				this.$PC = -1;
			}

			// Token: 0x06000653 RID: 1619 RVA: 0x00006EB6 File Offset: 0x000052B6
			[DebuggerHidden]
			public void Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x040002EF RID: 751
			internal T tweenInfo;

			// Token: 0x040002F0 RID: 752
			internal float <elapsedTime>__0;

			// Token: 0x040002F1 RID: 753
			internal float <percentage>__1;

			// Token: 0x040002F2 RID: 754
			internal object $current;

			// Token: 0x040002F3 RID: 755
			internal bool $disposing;

			// Token: 0x040002F4 RID: 756
			internal int $PC;
		}
	}
}
