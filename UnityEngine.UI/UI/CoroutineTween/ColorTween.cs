﻿using System;
using UnityEngine.Events;

namespace UnityEngine.UI.CoroutineTween
{
	// Token: 0x02000033 RID: 51
	internal struct ColorTween : ITweenValue
	{
		// Token: 0x1700005D RID: 93
		// (get) Token: 0x06000159 RID: 345 RVA: 0x0000690C File Offset: 0x00004D0C
		// (set) Token: 0x0600015A RID: 346 RVA: 0x00006927 File Offset: 0x00004D27
		public Color startColor
		{
			get
			{
				return this.m_StartColor;
			}
			set
			{
				this.m_StartColor = value;
			}
		}

		// Token: 0x1700005E RID: 94
		// (get) Token: 0x0600015B RID: 347 RVA: 0x00006934 File Offset: 0x00004D34
		// (set) Token: 0x0600015C RID: 348 RVA: 0x0000694F File Offset: 0x00004D4F
		public Color targetColor
		{
			get
			{
				return this.m_TargetColor;
			}
			set
			{
				this.m_TargetColor = value;
			}
		}

		// Token: 0x1700005F RID: 95
		// (get) Token: 0x0600015D RID: 349 RVA: 0x0000695C File Offset: 0x00004D5C
		// (set) Token: 0x0600015E RID: 350 RVA: 0x00006977 File Offset: 0x00004D77
		public ColorTween.ColorTweenMode tweenMode
		{
			get
			{
				return this.m_TweenMode;
			}
			set
			{
				this.m_TweenMode = value;
			}
		}

		// Token: 0x17000060 RID: 96
		// (get) Token: 0x0600015F RID: 351 RVA: 0x00006984 File Offset: 0x00004D84
		// (set) Token: 0x06000160 RID: 352 RVA: 0x0000699F File Offset: 0x00004D9F
		public float duration
		{
			get
			{
				return this.m_Duration;
			}
			set
			{
				this.m_Duration = value;
			}
		}

		// Token: 0x17000061 RID: 97
		// (get) Token: 0x06000161 RID: 353 RVA: 0x000069AC File Offset: 0x00004DAC
		// (set) Token: 0x06000162 RID: 354 RVA: 0x000069C7 File Offset: 0x00004DC7
		public bool ignoreTimeScale
		{
			get
			{
				return this.m_IgnoreTimeScale;
			}
			set
			{
				this.m_IgnoreTimeScale = value;
			}
		}

		// Token: 0x06000163 RID: 355 RVA: 0x000069D4 File Offset: 0x00004DD4
		public void TweenValue(float floatPercentage)
		{
			if (this.ValidTarget())
			{
				Color arg = Color.Lerp(this.m_StartColor, this.m_TargetColor, floatPercentage);
				if (this.m_TweenMode == ColorTween.ColorTweenMode.Alpha)
				{
					arg.r = this.m_StartColor.r;
					arg.g = this.m_StartColor.g;
					arg.b = this.m_StartColor.b;
				}
				else if (this.m_TweenMode == ColorTween.ColorTweenMode.RGB)
				{
					arg.a = this.m_StartColor.a;
				}
				this.m_Target.Invoke(arg);
			}
		}

		// Token: 0x06000164 RID: 356 RVA: 0x00006A7A File Offset: 0x00004E7A
		public void AddOnChangedCallback(UnityAction<Color> callback)
		{
			if (this.m_Target == null)
			{
				this.m_Target = new ColorTween.ColorTweenCallback();
			}
			this.m_Target.AddListener(callback);
		}

		// Token: 0x06000165 RID: 357 RVA: 0x00006AA0 File Offset: 0x00004EA0
		public bool GetIgnoreTimescale()
		{
			return this.m_IgnoreTimeScale;
		}

		// Token: 0x06000166 RID: 358 RVA: 0x00006ABC File Offset: 0x00004EBC
		public float GetDuration()
		{
			return this.m_Duration;
		}

		// Token: 0x06000167 RID: 359 RVA: 0x00006AD8 File Offset: 0x00004ED8
		public bool ValidTarget()
		{
			return this.m_Target != null;
		}

		// Token: 0x040000AB RID: 171
		private ColorTween.ColorTweenCallback m_Target;

		// Token: 0x040000AC RID: 172
		private Color m_StartColor;

		// Token: 0x040000AD RID: 173
		private Color m_TargetColor;

		// Token: 0x040000AE RID: 174
		private ColorTween.ColorTweenMode m_TweenMode;

		// Token: 0x040000AF RID: 175
		private float m_Duration;

		// Token: 0x040000B0 RID: 176
		private bool m_IgnoreTimeScale;

		// Token: 0x02000034 RID: 52
		public enum ColorTweenMode
		{
			// Token: 0x040000B2 RID: 178
			All,
			// Token: 0x040000B3 RID: 179
			RGB,
			// Token: 0x040000B4 RID: 180
			Alpha
		}

		// Token: 0x02000035 RID: 53
		public class ColorTweenCallback : UnityEvent<Color>
		{
			// Token: 0x06000168 RID: 360 RVA: 0x00006AF9 File Offset: 0x00004EF9
			public ColorTweenCallback()
			{
			}
		}
	}
}
