﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.UI.Collections;

namespace UnityEngine.UI
{
	// Token: 0x0200003E RID: 62
	public class CanvasUpdateRegistry
	{
		// Token: 0x06000192 RID: 402 RVA: 0x00008069 File Offset: 0x00006469
		protected CanvasUpdateRegistry()
		{
			Canvas.willRenderCanvases += this.PerformUpdate;
		}

		// Token: 0x1700006C RID: 108
		// (get) Token: 0x06000193 RID: 403 RVA: 0x0000809C File Offset: 0x0000649C
		public static CanvasUpdateRegistry instance
		{
			get
			{
				if (CanvasUpdateRegistry.s_Instance == null)
				{
					CanvasUpdateRegistry.s_Instance = new CanvasUpdateRegistry();
				}
				return CanvasUpdateRegistry.s_Instance;
			}
		}

		// Token: 0x06000194 RID: 404 RVA: 0x000080CC File Offset: 0x000064CC
		private bool ObjectValidForUpdate(ICanvasElement element)
		{
			bool result = element != null;
			bool flag = element is Object;
			if (flag)
			{
				result = (element as Object != null);
			}
			return result;
		}

		// Token: 0x06000195 RID: 405 RVA: 0x00008108 File Offset: 0x00006508
		private void CleanInvalidItems()
		{
			for (int i = this.m_LayoutRebuildQueue.Count - 1; i >= 0; i--)
			{
				ICanvasElement canvasElement = this.m_LayoutRebuildQueue[i];
				if (canvasElement == null)
				{
					this.m_LayoutRebuildQueue.RemoveAt(i);
				}
				else if (canvasElement.IsDestroyed())
				{
					this.m_LayoutRebuildQueue.RemoveAt(i);
					canvasElement.LayoutComplete();
				}
			}
			for (int j = this.m_GraphicRebuildQueue.Count - 1; j >= 0; j--)
			{
				ICanvasElement canvasElement2 = this.m_GraphicRebuildQueue[j];
				if (canvasElement2 == null)
				{
					this.m_GraphicRebuildQueue.RemoveAt(j);
				}
				else if (canvasElement2.IsDestroyed())
				{
					this.m_GraphicRebuildQueue.RemoveAt(j);
					canvasElement2.GraphicUpdateComplete();
				}
			}
		}

		// Token: 0x06000196 RID: 406 RVA: 0x000081E0 File Offset: 0x000065E0
		private void PerformUpdate()
		{
			UISystemProfilerApi.BeginSample(UISystemProfilerApi.SampleType.Layout);
			this.CleanInvalidItems();
			this.m_PerformingLayoutUpdate = true;
			this.m_LayoutRebuildQueue.Sort(CanvasUpdateRegistry.s_SortLayoutFunction);
			for (int i = 0; i <= 2; i++)
			{
				for (int j = 0; j < this.m_LayoutRebuildQueue.Count; j++)
				{
					ICanvasElement canvasElement = CanvasUpdateRegistry.instance.m_LayoutRebuildQueue[j];
					try
					{
						if (this.ObjectValidForUpdate(canvasElement))
						{
							canvasElement.Rebuild((CanvasUpdate)i);
						}
					}
					catch (Exception exception)
					{
						Debug.LogException(exception, canvasElement.transform);
					}
				}
			}
			for (int k = 0; k < this.m_LayoutRebuildQueue.Count; k++)
			{
				this.m_LayoutRebuildQueue[k].LayoutComplete();
			}
			CanvasUpdateRegistry.instance.m_LayoutRebuildQueue.Clear();
			this.m_PerformingLayoutUpdate = false;
			ClipperRegistry.instance.Cull();
			this.m_PerformingGraphicUpdate = true;
			for (int l = 3; l < 5; l++)
			{
				for (int m = 0; m < CanvasUpdateRegistry.instance.m_GraphicRebuildQueue.Count; m++)
				{
					try
					{
						ICanvasElement canvasElement2 = CanvasUpdateRegistry.instance.m_GraphicRebuildQueue[m];
						if (this.ObjectValidForUpdate(canvasElement2))
						{
							canvasElement2.Rebuild((CanvasUpdate)l);
						}
					}
					catch (Exception exception2)
					{
						Debug.LogException(exception2, CanvasUpdateRegistry.instance.m_GraphicRebuildQueue[m].transform);
					}
				}
			}
			for (int n = 0; n < this.m_GraphicRebuildQueue.Count; n++)
			{
				this.m_GraphicRebuildQueue[n].GraphicUpdateComplete();
			}
			CanvasUpdateRegistry.instance.m_GraphicRebuildQueue.Clear();
			this.m_PerformingGraphicUpdate = false;
			UISystemProfilerApi.EndSample(UISystemProfilerApi.SampleType.Layout);
		}

		// Token: 0x06000197 RID: 407 RVA: 0x000083DC File Offset: 0x000067DC
		private static int ParentCount(Transform child)
		{
			int result;
			if (child == null)
			{
				result = 0;
			}
			else
			{
				Transform parent = child.parent;
				int num = 0;
				while (parent != null)
				{
					num++;
					parent = parent.parent;
				}
				result = num;
			}
			return result;
		}

		// Token: 0x06000198 RID: 408 RVA: 0x0000842C File Offset: 0x0000682C
		private static int SortLayoutList(ICanvasElement x, ICanvasElement y)
		{
			Transform transform = x.transform;
			Transform transform2 = y.transform;
			return CanvasUpdateRegistry.ParentCount(transform) - CanvasUpdateRegistry.ParentCount(transform2);
		}

		// Token: 0x06000199 RID: 409 RVA: 0x0000845C File Offset: 0x0000685C
		public static void RegisterCanvasElementForLayoutRebuild(ICanvasElement element)
		{
			CanvasUpdateRegistry.instance.InternalRegisterCanvasElementForLayoutRebuild(element);
		}

		// Token: 0x0600019A RID: 410 RVA: 0x0000846C File Offset: 0x0000686C
		public static bool TryRegisterCanvasElementForLayoutRebuild(ICanvasElement element)
		{
			return CanvasUpdateRegistry.instance.InternalRegisterCanvasElementForLayoutRebuild(element);
		}

		// Token: 0x0600019B RID: 411 RVA: 0x0000848C File Offset: 0x0000688C
		private bool InternalRegisterCanvasElementForLayoutRebuild(ICanvasElement element)
		{
			return !this.m_LayoutRebuildQueue.Contains(element) && this.m_LayoutRebuildQueue.AddUnique(element);
		}

		// Token: 0x0600019C RID: 412 RVA: 0x000084C5 File Offset: 0x000068C5
		public static void RegisterCanvasElementForGraphicRebuild(ICanvasElement element)
		{
			CanvasUpdateRegistry.instance.InternalRegisterCanvasElementForGraphicRebuild(element);
		}

		// Token: 0x0600019D RID: 413 RVA: 0x000084D4 File Offset: 0x000068D4
		public static bool TryRegisterCanvasElementForGraphicRebuild(ICanvasElement element)
		{
			return CanvasUpdateRegistry.instance.InternalRegisterCanvasElementForGraphicRebuild(element);
		}

		// Token: 0x0600019E RID: 414 RVA: 0x000084F4 File Offset: 0x000068F4
		private bool InternalRegisterCanvasElementForGraphicRebuild(ICanvasElement element)
		{
			bool result;
			if (this.m_PerformingGraphicUpdate)
			{
				Debug.LogError(string.Format("Trying to add {0} for graphic rebuild while we are already inside a graphic rebuild loop. This is not supported.", element));
				result = false;
			}
			else
			{
				result = this.m_GraphicRebuildQueue.AddUnique(element);
			}
			return result;
		}

		// Token: 0x0600019F RID: 415 RVA: 0x00008538 File Offset: 0x00006938
		public static void UnRegisterCanvasElementForRebuild(ICanvasElement element)
		{
			CanvasUpdateRegistry.instance.InternalUnRegisterCanvasElementForLayoutRebuild(element);
			CanvasUpdateRegistry.instance.InternalUnRegisterCanvasElementForGraphicRebuild(element);
		}

		// Token: 0x060001A0 RID: 416 RVA: 0x00008551 File Offset: 0x00006951
		private void InternalUnRegisterCanvasElementForLayoutRebuild(ICanvasElement element)
		{
			if (this.m_PerformingLayoutUpdate)
			{
				Debug.LogError(string.Format("Trying to remove {0} from rebuild list while we are already inside a rebuild loop. This is not supported.", element));
			}
			else
			{
				element.LayoutComplete();
				CanvasUpdateRegistry.instance.m_LayoutRebuildQueue.Remove(element);
			}
		}

		// Token: 0x060001A1 RID: 417 RVA: 0x0000858C File Offset: 0x0000698C
		private void InternalUnRegisterCanvasElementForGraphicRebuild(ICanvasElement element)
		{
			if (this.m_PerformingGraphicUpdate)
			{
				Debug.LogError(string.Format("Trying to remove {0} from rebuild list while we are already inside a rebuild loop. This is not supported.", element));
			}
			else
			{
				element.GraphicUpdateComplete();
				CanvasUpdateRegistry.instance.m_GraphicRebuildQueue.Remove(element);
			}
		}

		// Token: 0x060001A2 RID: 418 RVA: 0x000085C8 File Offset: 0x000069C8
		public static bool IsRebuildingLayout()
		{
			return CanvasUpdateRegistry.instance.m_PerformingLayoutUpdate;
		}

		// Token: 0x060001A3 RID: 419 RVA: 0x000085E8 File Offset: 0x000069E8
		public static bool IsRebuildingGraphics()
		{
			return CanvasUpdateRegistry.instance.m_PerformingGraphicUpdate;
		}

		// Token: 0x060001A4 RID: 420 RVA: 0x00008607 File Offset: 0x00006A07
		// Note: this type is marked as 'beforefieldinit'.
		static CanvasUpdateRegistry()
		{
			if (CanvasUpdateRegistry.<>f__mg$cache0 == null)
			{
				CanvasUpdateRegistry.<>f__mg$cache0 = new Comparison<ICanvasElement>(CanvasUpdateRegistry.SortLayoutList);
			}
			CanvasUpdateRegistry.s_SortLayoutFunction = CanvasUpdateRegistry.<>f__mg$cache0;
		}

		// Token: 0x040000CC RID: 204
		private static CanvasUpdateRegistry s_Instance;

		// Token: 0x040000CD RID: 205
		private bool m_PerformingLayoutUpdate;

		// Token: 0x040000CE RID: 206
		private bool m_PerformingGraphicUpdate;

		// Token: 0x040000CF RID: 207
		private readonly IndexedSet<ICanvasElement> m_LayoutRebuildQueue = new IndexedSet<ICanvasElement>();

		// Token: 0x040000D0 RID: 208
		private readonly IndexedSet<ICanvasElement> m_GraphicRebuildQueue = new IndexedSet<ICanvasElement>();

		// Token: 0x040000D1 RID: 209
		private static readonly Comparison<ICanvasElement> s_SortLayoutFunction;

		// Token: 0x040000D2 RID: 210
		[CompilerGenerated]
		private static Comparison<ICanvasElement> <>f__mg$cache0;
	}
}
