﻿using System;
using System.Collections.Generic;

namespace UnityEngine.UI
{
	// Token: 0x020000AA RID: 170
	public class VertexHelper : IDisposable
	{
		// Token: 0x0600061F RID: 1567 RVA: 0x0001E992 File Offset: 0x0001CD92
		public VertexHelper()
		{
		}

		// Token: 0x06000620 RID: 1568 RVA: 0x0001E9A4 File Offset: 0x0001CDA4
		public VertexHelper(Mesh m)
		{
			this.InitializeListIfRequired();
			this.m_Positions.AddRange(m.vertices);
			this.m_Colors.AddRange(m.colors32);
			this.m_Uv0S.AddRange(m.uv);
			this.m_Uv1S.AddRange(m.uv2);
			this.m_Uv2S.AddRange(m.uv3);
			this.m_Uv3S.AddRange(m.uv4);
			this.m_Normals.AddRange(m.normals);
			this.m_Tangents.AddRange(m.tangents);
			this.m_Indices.AddRange(m.GetIndices(0));
		}

		// Token: 0x06000621 RID: 1569 RVA: 0x0001EA60 File Offset: 0x0001CE60
		private void InitializeListIfRequired()
		{
			if (!this.m_ListsInitalized)
			{
				this.m_Positions = ListPool<Vector3>.Get();
				this.m_Colors = ListPool<Color32>.Get();
				this.m_Uv0S = ListPool<Vector2>.Get();
				this.m_Uv1S = ListPool<Vector2>.Get();
				this.m_Uv2S = ListPool<Vector2>.Get();
				this.m_Uv3S = ListPool<Vector2>.Get();
				this.m_Normals = ListPool<Vector3>.Get();
				this.m_Tangents = ListPool<Vector4>.Get();
				this.m_Indices = ListPool<int>.Get();
				this.m_ListsInitalized = true;
			}
		}

		// Token: 0x06000622 RID: 1570 RVA: 0x0001EAE8 File Offset: 0x0001CEE8
		public void Dispose()
		{
			if (this.m_ListsInitalized)
			{
				ListPool<Vector3>.Release(this.m_Positions);
				ListPool<Color32>.Release(this.m_Colors);
				ListPool<Vector2>.Release(this.m_Uv0S);
				ListPool<Vector2>.Release(this.m_Uv1S);
				ListPool<Vector2>.Release(this.m_Uv2S);
				ListPool<Vector2>.Release(this.m_Uv3S);
				ListPool<Vector3>.Release(this.m_Normals);
				ListPool<Vector4>.Release(this.m_Tangents);
				ListPool<int>.Release(this.m_Indices);
				this.m_Positions = null;
				this.m_Colors = null;
				this.m_Uv0S = null;
				this.m_Uv1S = null;
				this.m_Uv2S = null;
				this.m_Uv3S = null;
				this.m_Normals = null;
				this.m_Tangents = null;
				this.m_Indices = null;
				this.m_ListsInitalized = false;
			}
		}

		// Token: 0x06000623 RID: 1571 RVA: 0x0001EBAC File Offset: 0x0001CFAC
		public void Clear()
		{
			if (this.m_ListsInitalized)
			{
				this.m_Positions.Clear();
				this.m_Colors.Clear();
				this.m_Uv0S.Clear();
				this.m_Uv1S.Clear();
				this.m_Uv2S.Clear();
				this.m_Uv3S.Clear();
				this.m_Normals.Clear();
				this.m_Tangents.Clear();
				this.m_Indices.Clear();
			}
		}

		// Token: 0x1700019F RID: 415
		// (get) Token: 0x06000624 RID: 1572 RVA: 0x0001EC2C File Offset: 0x0001D02C
		public int currentVertCount
		{
			get
			{
				return (this.m_Positions == null) ? 0 : this.m_Positions.Count;
			}
		}

		// Token: 0x170001A0 RID: 416
		// (get) Token: 0x06000625 RID: 1573 RVA: 0x0001EC60 File Offset: 0x0001D060
		public int currentIndexCount
		{
			get
			{
				return (this.m_Indices == null) ? 0 : this.m_Indices.Count;
			}
		}

		// Token: 0x06000626 RID: 1574 RVA: 0x0001EC94 File Offset: 0x0001D094
		public void PopulateUIVertex(ref UIVertex vertex, int i)
		{
			this.InitializeListIfRequired();
			vertex.position = this.m_Positions[i];
			vertex.color = this.m_Colors[i];
			vertex.uv0 = this.m_Uv0S[i];
			vertex.uv1 = this.m_Uv1S[i];
			vertex.uv2 = this.m_Uv2S[i];
			vertex.uv3 = this.m_Uv3S[i];
			vertex.normal = this.m_Normals[i];
			vertex.tangent = this.m_Tangents[i];
		}

		// Token: 0x06000627 RID: 1575 RVA: 0x0001ED38 File Offset: 0x0001D138
		public void SetUIVertex(UIVertex vertex, int i)
		{
			this.InitializeListIfRequired();
			this.m_Positions[i] = vertex.position;
			this.m_Colors[i] = vertex.color;
			this.m_Uv0S[i] = vertex.uv0;
			this.m_Uv1S[i] = vertex.uv1;
			this.m_Uv2S[i] = vertex.uv2;
			this.m_Uv3S[i] = vertex.uv3;
			this.m_Normals[i] = vertex.normal;
			this.m_Tangents[i] = vertex.tangent;
		}

		// Token: 0x06000628 RID: 1576 RVA: 0x0001EDE4 File Offset: 0x0001D1E4
		public void FillMesh(Mesh mesh)
		{
			this.InitializeListIfRequired();
			mesh.Clear();
			if (this.m_Positions.Count >= 65000)
			{
				throw new ArgumentException("Mesh can not have more than 65000 vertices");
			}
			mesh.SetVertices(this.m_Positions);
			mesh.SetColors(this.m_Colors);
			mesh.SetUVs(0, this.m_Uv0S);
			mesh.SetUVs(1, this.m_Uv1S);
			mesh.SetUVs(2, this.m_Uv2S);
			mesh.SetUVs(3, this.m_Uv3S);
			mesh.SetNormals(this.m_Normals);
			mesh.SetTangents(this.m_Tangents);
			mesh.SetTriangles(this.m_Indices, 0);
			mesh.RecalculateBounds();
		}

		// Token: 0x06000629 RID: 1577 RVA: 0x0001EE98 File Offset: 0x0001D298
		internal void AddVert(Vector3 position, Color32 color, Vector2 uv0, Vector2 uv1, Vector2 uv2, Vector2 uv3, Vector3 normal, Vector4 tangent)
		{
			this.InitializeListIfRequired();
			this.m_Positions.Add(position);
			this.m_Colors.Add(color);
			this.m_Uv0S.Add(uv0);
			this.m_Uv1S.Add(uv1);
			this.m_Uv2S.Add(uv2);
			this.m_Uv3S.Add(uv3);
			this.m_Normals.Add(normal);
			this.m_Tangents.Add(tangent);
		}

		// Token: 0x0600062A RID: 1578 RVA: 0x0001EF14 File Offset: 0x0001D314
		public void AddVert(Vector3 position, Color32 color, Vector2 uv0, Vector2 uv1, Vector3 normal, Vector4 tangent)
		{
			this.AddVert(position, color, uv0, uv1, Vector2.zero, Vector2.zero, normal, tangent);
		}

		// Token: 0x0600062B RID: 1579 RVA: 0x0001EF3B File Offset: 0x0001D33B
		public void AddVert(Vector3 position, Color32 color, Vector2 uv0)
		{
			this.AddVert(position, color, uv0, Vector2.zero, VertexHelper.s_DefaultNormal, VertexHelper.s_DefaultTangent);
		}

		// Token: 0x0600062C RID: 1580 RVA: 0x0001EF56 File Offset: 0x0001D356
		public void AddVert(UIVertex v)
		{
			this.AddVert(v.position, v.color, v.uv0, v.uv1, v.normal, v.tangent);
		}

		// Token: 0x0600062D RID: 1581 RVA: 0x0001EF89 File Offset: 0x0001D389
		public void AddTriangle(int idx0, int idx1, int idx2)
		{
			this.InitializeListIfRequired();
			this.m_Indices.Add(idx0);
			this.m_Indices.Add(idx1);
			this.m_Indices.Add(idx2);
		}

		// Token: 0x0600062E RID: 1582 RVA: 0x0001EFB8 File Offset: 0x0001D3B8
		public void AddUIVertexQuad(UIVertex[] verts)
		{
			int currentVertCount = this.currentVertCount;
			for (int i = 0; i < 4; i++)
			{
				this.AddVert(verts[i].position, verts[i].color, verts[i].uv0, verts[i].uv1, verts[i].normal, verts[i].tangent);
			}
			this.AddTriangle(currentVertCount, currentVertCount + 1, currentVertCount + 2);
			this.AddTriangle(currentVertCount + 2, currentVertCount + 3, currentVertCount);
		}

		// Token: 0x0600062F RID: 1583 RVA: 0x0001F048 File Offset: 0x0001D448
		public void AddUIVertexStream(List<UIVertex> verts, List<int> indices)
		{
			this.InitializeListIfRequired();
			if (verts != null)
			{
				CanvasRenderer.AddUIVertexStream(verts, this.m_Positions, this.m_Colors, this.m_Uv0S, this.m_Uv1S, this.m_Uv2S, this.m_Uv3S, this.m_Normals, this.m_Tangents);
			}
			if (indices != null)
			{
				this.m_Indices.AddRange(indices);
			}
		}

		// Token: 0x06000630 RID: 1584 RVA: 0x0001F0B0 File Offset: 0x0001D4B0
		public void AddUIVertexTriangleStream(List<UIVertex> verts)
		{
			if (verts != null)
			{
				this.InitializeListIfRequired();
				CanvasRenderer.SplitUIVertexStreams(verts, this.m_Positions, this.m_Colors, this.m_Uv0S, this.m_Uv1S, this.m_Uv2S, this.m_Uv3S, this.m_Normals, this.m_Tangents, this.m_Indices);
			}
		}

		// Token: 0x06000631 RID: 1585 RVA: 0x0001F10C File Offset: 0x0001D50C
		public void GetUIVertexStream(List<UIVertex> stream)
		{
			if (stream != null)
			{
				this.InitializeListIfRequired();
				CanvasRenderer.CreateUIVertexStream(stream, this.m_Positions, this.m_Colors, this.m_Uv0S, this.m_Uv1S, this.m_Uv2S, this.m_Uv3S, this.m_Normals, this.m_Tangents, this.m_Indices);
			}
		}

		// Token: 0x06000632 RID: 1586 RVA: 0x0001F167 File Offset: 0x0001D567
		// Note: this type is marked as 'beforefieldinit'.
		static VertexHelper()
		{
		}

		// Token: 0x040002DE RID: 734
		private List<Vector3> m_Positions;

		// Token: 0x040002DF RID: 735
		private List<Color32> m_Colors;

		// Token: 0x040002E0 RID: 736
		private List<Vector2> m_Uv0S;

		// Token: 0x040002E1 RID: 737
		private List<Vector2> m_Uv1S;

		// Token: 0x040002E2 RID: 738
		private List<Vector2> m_Uv2S;

		// Token: 0x040002E3 RID: 739
		private List<Vector2> m_Uv3S;

		// Token: 0x040002E4 RID: 740
		private List<Vector3> m_Normals;

		// Token: 0x040002E5 RID: 741
		private List<Vector4> m_Tangents;

		// Token: 0x040002E6 RID: 742
		private List<int> m_Indices;

		// Token: 0x040002E7 RID: 743
		private static readonly Vector4 s_DefaultTangent = new Vector4(1f, 0f, 0f, -1f);

		// Token: 0x040002E8 RID: 744
		private static readonly Vector3 s_DefaultNormal = Vector3.back;

		// Token: 0x040002E9 RID: 745
		private bool m_ListsInitalized = false;
	}
}
