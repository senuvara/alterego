﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine.EventSystems;

namespace UnityEngine.UI
{
	// Token: 0x0200009B RID: 155
	[DisallowMultipleComponent]
	[ExecuteAlways]
	[RequireComponent(typeof(RectTransform))]
	public abstract class LayoutGroup : UIBehaviour, ILayoutElement, ILayoutGroup, ILayoutController
	{
		// Token: 0x06000591 RID: 1425 RVA: 0x0001BED4 File Offset: 0x0001A2D4
		protected LayoutGroup()
		{
			if (this.m_Padding == null)
			{
				this.m_Padding = new RectOffset();
			}
		}

		// Token: 0x1700018B RID: 395
		// (get) Token: 0x06000592 RID: 1426 RVA: 0x0001BF3C File Offset: 0x0001A33C
		// (set) Token: 0x06000593 RID: 1427 RVA: 0x0001BF57 File Offset: 0x0001A357
		public RectOffset padding
		{
			get
			{
				return this.m_Padding;
			}
			set
			{
				this.SetProperty<RectOffset>(ref this.m_Padding, value);
			}
		}

		// Token: 0x1700018C RID: 396
		// (get) Token: 0x06000594 RID: 1428 RVA: 0x0001BF68 File Offset: 0x0001A368
		// (set) Token: 0x06000595 RID: 1429 RVA: 0x0001BF83 File Offset: 0x0001A383
		public TextAnchor childAlignment
		{
			get
			{
				return this.m_ChildAlignment;
			}
			set
			{
				this.SetProperty<TextAnchor>(ref this.m_ChildAlignment, value);
			}
		}

		// Token: 0x1700018D RID: 397
		// (get) Token: 0x06000596 RID: 1430 RVA: 0x0001BF94 File Offset: 0x0001A394
		protected RectTransform rectTransform
		{
			get
			{
				if (this.m_Rect == null)
				{
					this.m_Rect = base.GetComponent<RectTransform>();
				}
				return this.m_Rect;
			}
		}

		// Token: 0x1700018E RID: 398
		// (get) Token: 0x06000597 RID: 1431 RVA: 0x0001BFCC File Offset: 0x0001A3CC
		protected List<RectTransform> rectChildren
		{
			get
			{
				return this.m_RectChildren;
			}
		}

		// Token: 0x06000598 RID: 1432 RVA: 0x0001BFE8 File Offset: 0x0001A3E8
		public virtual void CalculateLayoutInputHorizontal()
		{
			this.m_RectChildren.Clear();
			List<Component> list = ListPool<Component>.Get();
			for (int i = 0; i < this.rectTransform.childCount; i++)
			{
				RectTransform rectTransform = this.rectTransform.GetChild(i) as RectTransform;
				if (!(rectTransform == null) && rectTransform.gameObject.activeInHierarchy)
				{
					rectTransform.GetComponents(typeof(ILayoutIgnorer), list);
					if (list.Count == 0)
					{
						this.m_RectChildren.Add(rectTransform);
					}
					else
					{
						for (int j = 0; j < list.Count; j++)
						{
							ILayoutIgnorer layoutIgnorer = (ILayoutIgnorer)list[j];
							if (!layoutIgnorer.ignoreLayout)
							{
								this.m_RectChildren.Add(rectTransform);
								break;
							}
						}
					}
				}
			}
			ListPool<Component>.Release(list);
			this.m_Tracker.Clear();
		}

		// Token: 0x06000599 RID: 1433
		public abstract void CalculateLayoutInputVertical();

		// Token: 0x1700018F RID: 399
		// (get) Token: 0x0600059A RID: 1434 RVA: 0x0001C0DC File Offset: 0x0001A4DC
		public virtual float minWidth
		{
			get
			{
				return this.GetTotalMinSize(0);
			}
		}

		// Token: 0x17000190 RID: 400
		// (get) Token: 0x0600059B RID: 1435 RVA: 0x0001C0F8 File Offset: 0x0001A4F8
		public virtual float preferredWidth
		{
			get
			{
				return this.GetTotalPreferredSize(0);
			}
		}

		// Token: 0x17000191 RID: 401
		// (get) Token: 0x0600059C RID: 1436 RVA: 0x0001C114 File Offset: 0x0001A514
		public virtual float flexibleWidth
		{
			get
			{
				return this.GetTotalFlexibleSize(0);
			}
		}

		// Token: 0x17000192 RID: 402
		// (get) Token: 0x0600059D RID: 1437 RVA: 0x0001C130 File Offset: 0x0001A530
		public virtual float minHeight
		{
			get
			{
				return this.GetTotalMinSize(1);
			}
		}

		// Token: 0x17000193 RID: 403
		// (get) Token: 0x0600059E RID: 1438 RVA: 0x0001C14C File Offset: 0x0001A54C
		public virtual float preferredHeight
		{
			get
			{
				return this.GetTotalPreferredSize(1);
			}
		}

		// Token: 0x17000194 RID: 404
		// (get) Token: 0x0600059F RID: 1439 RVA: 0x0001C168 File Offset: 0x0001A568
		public virtual float flexibleHeight
		{
			get
			{
				return this.GetTotalFlexibleSize(1);
			}
		}

		// Token: 0x17000195 RID: 405
		// (get) Token: 0x060005A0 RID: 1440 RVA: 0x0001C184 File Offset: 0x0001A584
		public virtual int layoutPriority
		{
			get
			{
				return 0;
			}
		}

		// Token: 0x060005A1 RID: 1441
		public abstract void SetLayoutHorizontal();

		// Token: 0x060005A2 RID: 1442
		public abstract void SetLayoutVertical();

		// Token: 0x060005A3 RID: 1443 RVA: 0x0001C19A File Offset: 0x0001A59A
		protected override void OnEnable()
		{
			base.OnEnable();
			this.SetDirty();
		}

		// Token: 0x060005A4 RID: 1444 RVA: 0x0001C1A9 File Offset: 0x0001A5A9
		protected override void OnDisable()
		{
			this.m_Tracker.Clear();
			LayoutRebuilder.MarkLayoutForRebuild(this.rectTransform);
			base.OnDisable();
		}

		// Token: 0x060005A5 RID: 1445 RVA: 0x0001C1C8 File Offset: 0x0001A5C8
		protected override void OnDidApplyAnimationProperties()
		{
			this.SetDirty();
		}

		// Token: 0x060005A6 RID: 1446 RVA: 0x0001C1D4 File Offset: 0x0001A5D4
		protected float GetTotalMinSize(int axis)
		{
			return this.m_TotalMinSize[axis];
		}

		// Token: 0x060005A7 RID: 1447 RVA: 0x0001C1F8 File Offset: 0x0001A5F8
		protected float GetTotalPreferredSize(int axis)
		{
			return this.m_TotalPreferredSize[axis];
		}

		// Token: 0x060005A8 RID: 1448 RVA: 0x0001C21C File Offset: 0x0001A61C
		protected float GetTotalFlexibleSize(int axis)
		{
			return this.m_TotalFlexibleSize[axis];
		}

		// Token: 0x060005A9 RID: 1449 RVA: 0x0001C240 File Offset: 0x0001A640
		protected float GetStartOffset(int axis, float requiredSpaceWithoutPadding)
		{
			float num = requiredSpaceWithoutPadding + (float)((axis != 0) ? this.padding.vertical : this.padding.horizontal);
			float num2 = this.rectTransform.rect.size[axis];
			float num3 = num2 - num;
			float alignmentOnAxis = this.GetAlignmentOnAxis(axis);
			return (float)((axis != 0) ? this.padding.top : this.padding.left) + num3 * alignmentOnAxis;
		}

		// Token: 0x060005AA RID: 1450 RVA: 0x0001C2D0 File Offset: 0x0001A6D0
		protected float GetAlignmentOnAxis(int axis)
		{
			float result;
			if (axis == 0)
			{
				result = (float)(this.childAlignment % TextAnchor.MiddleLeft) * 0.5f;
			}
			else
			{
				result = (float)(this.childAlignment / TextAnchor.MiddleLeft) * 0.5f;
			}
			return result;
		}

		// Token: 0x060005AB RID: 1451 RVA: 0x0001C30F File Offset: 0x0001A70F
		protected void SetLayoutInputForAxis(float totalMin, float totalPreferred, float totalFlexible, int axis)
		{
			this.m_TotalMinSize[axis] = totalMin;
			this.m_TotalPreferredSize[axis] = totalPreferred;
			this.m_TotalFlexibleSize[axis] = totalFlexible;
		}

		// Token: 0x060005AC RID: 1452 RVA: 0x0001C33C File Offset: 0x0001A73C
		protected void SetChildAlongAxis(RectTransform rect, int axis, float pos)
		{
			if (!(rect == null))
			{
				this.m_Tracker.Add(this, rect, DrivenTransformProperties.Anchors | ((axis != 0) ? DrivenTransformProperties.AnchoredPositionY : DrivenTransformProperties.AnchoredPositionX));
				rect.SetInsetAndSizeFromParentEdge((axis != 0) ? RectTransform.Edge.Top : RectTransform.Edge.Left, pos, rect.sizeDelta[axis]);
			}
		}

		// Token: 0x060005AD RID: 1453 RVA: 0x0001C3A0 File Offset: 0x0001A7A0
		protected void SetChildAlongAxis(RectTransform rect, int axis, float pos, float size)
		{
			if (!(rect == null))
			{
				this.m_Tracker.Add(this, rect, DrivenTransformProperties.Anchors | ((axis != 0) ? (DrivenTransformProperties.AnchoredPositionY | DrivenTransformProperties.SizeDeltaY) : (DrivenTransformProperties.AnchoredPositionX | DrivenTransformProperties.SizeDeltaX)));
				rect.SetInsetAndSizeFromParentEdge((axis != 0) ? RectTransform.Edge.Top : RectTransform.Edge.Left, pos, size);
			}
		}

		// Token: 0x17000196 RID: 406
		// (get) Token: 0x060005AE RID: 1454 RVA: 0x0001C400 File Offset: 0x0001A800
		private bool isRootLayoutGroup
		{
			get
			{
				Transform parent = base.transform.parent;
				return parent == null || base.transform.parent.GetComponent(typeof(ILayoutGroup)) == null;
			}
		}

		// Token: 0x060005AF RID: 1455 RVA: 0x0001C454 File Offset: 0x0001A854
		protected override void OnRectTransformDimensionsChange()
		{
			base.OnRectTransformDimensionsChange();
			if (this.isRootLayoutGroup)
			{
				this.SetDirty();
			}
		}

		// Token: 0x060005B0 RID: 1456 RVA: 0x0001C46E File Offset: 0x0001A86E
		protected virtual void OnTransformChildrenChanged()
		{
			this.SetDirty();
		}

		// Token: 0x060005B1 RID: 1457 RVA: 0x0001C478 File Offset: 0x0001A878
		protected void SetProperty<T>(ref T currentValue, T newValue)
		{
			if ((currentValue != null || newValue != null) && (currentValue == null || !currentValue.Equals(newValue)))
			{
				currentValue = newValue;
				this.SetDirty();
			}
		}

		// Token: 0x060005B2 RID: 1458 RVA: 0x0001C4DA File Offset: 0x0001A8DA
		protected void SetDirty()
		{
			if (this.IsActive())
			{
				if (!CanvasUpdateRegistry.IsRebuildingLayout())
				{
					LayoutRebuilder.MarkLayoutForRebuild(this.rectTransform);
				}
				else
				{
					base.StartCoroutine(this.DelayedSetDirty(this.rectTransform));
				}
			}
		}

		// Token: 0x060005B3 RID: 1459 RVA: 0x0001C51C File Offset: 0x0001A91C
		private IEnumerator DelayedSetDirty(RectTransform rectTransform)
		{
			yield return null;
			LayoutRebuilder.MarkLayoutForRebuild(rectTransform);
			yield break;
		}

		// Token: 0x040002B6 RID: 694
		[SerializeField]
		protected RectOffset m_Padding = new RectOffset();

		// Token: 0x040002B7 RID: 695
		[SerializeField]
		protected TextAnchor m_ChildAlignment = TextAnchor.UpperLeft;

		// Token: 0x040002B8 RID: 696
		[NonSerialized]
		private RectTransform m_Rect;

		// Token: 0x040002B9 RID: 697
		protected DrivenRectTransformTracker m_Tracker;

		// Token: 0x040002BA RID: 698
		private Vector2 m_TotalMinSize = Vector2.zero;

		// Token: 0x040002BB RID: 699
		private Vector2 m_TotalPreferredSize = Vector2.zero;

		// Token: 0x040002BC RID: 700
		private Vector2 m_TotalFlexibleSize = Vector2.zero;

		// Token: 0x040002BD RID: 701
		[NonSerialized]
		private List<RectTransform> m_RectChildren = new List<RectTransform>();

		// Token: 0x020000BC RID: 188
		[CompilerGenerated]
		private sealed class <DelayedSetDirty>c__Iterator0 : IEnumerator, IDisposable, IEnumerator<object>
		{
			// Token: 0x06000674 RID: 1652 RVA: 0x0001C53E File Offset: 0x0001A93E
			[DebuggerHidden]
			public <DelayedSetDirty>c__Iterator0()
			{
			}

			// Token: 0x06000675 RID: 1653 RVA: 0x0001C548 File Offset: 0x0001A948
			public bool MoveNext()
			{
				uint num = (uint)this.$PC;
				this.$PC = -1;
				switch (num)
				{
				case 0U:
					this.$current = null;
					if (!this.$disposing)
					{
						this.$PC = 1;
					}
					return true;
				case 1U:
					LayoutRebuilder.MarkLayoutForRebuild(rectTransform);
					this.$PC = -1;
					break;
				}
				return false;
			}

			// Token: 0x170001B1 RID: 433
			// (get) Token: 0x06000676 RID: 1654 RVA: 0x0001C5A8 File Offset: 0x0001A9A8
			object IEnumerator<object>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.$current;
				}
			}

			// Token: 0x170001B2 RID: 434
			// (get) Token: 0x06000677 RID: 1655 RVA: 0x0001C5C4 File Offset: 0x0001A9C4
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.$current;
				}
			}

			// Token: 0x06000678 RID: 1656 RVA: 0x0001C5DE File Offset: 0x0001A9DE
			[DebuggerHidden]
			public void Dispose()
			{
				this.$disposing = true;
				this.$PC = -1;
			}

			// Token: 0x06000679 RID: 1657 RVA: 0x0001C5EE File Offset: 0x0001A9EE
			[DebuggerHidden]
			public void Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x04000316 RID: 790
			internal RectTransform rectTransform;

			// Token: 0x04000317 RID: 791
			internal object $current;

			// Token: 0x04000318 RID: 792
			internal bool $disposing;

			// Token: 0x04000319 RID: 793
			internal int $PC;
		}
	}
}
