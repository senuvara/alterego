﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace UnityEngine.UI
{
	// Token: 0x0200006E RID: 110
	[AddComponentMenu("UI/Scrollbar", 34)]
	[RequireComponent(typeof(RectTransform))]
	public class Scrollbar : Selectable, IBeginDragHandler, IDragHandler, IInitializePotentialDragHandler, ICanvasElement, IEventSystemHandler
	{
		// Token: 0x06000404 RID: 1028 RVA: 0x00017C72 File Offset: 0x00016072
		protected Scrollbar()
		{
		}

		// Token: 0x1700011C RID: 284
		// (get) Token: 0x06000405 RID: 1029 RVA: 0x00017CB4 File Offset: 0x000160B4
		// (set) Token: 0x06000406 RID: 1030 RVA: 0x00017CCF File Offset: 0x000160CF
		public RectTransform handleRect
		{
			get
			{
				return this.m_HandleRect;
			}
			set
			{
				if (SetPropertyUtility.SetClass<RectTransform>(ref this.m_HandleRect, value))
				{
					this.UpdateCachedReferences();
					this.UpdateVisuals();
				}
			}
		}

		// Token: 0x1700011D RID: 285
		// (get) Token: 0x06000407 RID: 1031 RVA: 0x00017CF4 File Offset: 0x000160F4
		// (set) Token: 0x06000408 RID: 1032 RVA: 0x00017D0F File Offset: 0x0001610F
		public Scrollbar.Direction direction
		{
			get
			{
				return this.m_Direction;
			}
			set
			{
				if (SetPropertyUtility.SetStruct<Scrollbar.Direction>(ref this.m_Direction, value))
				{
					this.UpdateVisuals();
				}
			}
		}

		// Token: 0x1700011E RID: 286
		// (get) Token: 0x06000409 RID: 1033 RVA: 0x00017D2C File Offset: 0x0001612C
		// (set) Token: 0x0600040A RID: 1034 RVA: 0x00017D70 File Offset: 0x00016170
		public float value
		{
			get
			{
				float num = this.m_Value;
				if (this.m_NumberOfSteps > 1)
				{
					num = Mathf.Round(num * (float)(this.m_NumberOfSteps - 1)) / (float)(this.m_NumberOfSteps - 1);
				}
				return num;
			}
			set
			{
				this.Set(value);
			}
		}

		// Token: 0x1700011F RID: 287
		// (get) Token: 0x0600040B RID: 1035 RVA: 0x00017D7C File Offset: 0x0001617C
		// (set) Token: 0x0600040C RID: 1036 RVA: 0x00017D97 File Offset: 0x00016197
		public float size
		{
			get
			{
				return this.m_Size;
			}
			set
			{
				if (SetPropertyUtility.SetStruct<float>(ref this.m_Size, Mathf.Clamp01(value)))
				{
					this.UpdateVisuals();
				}
			}
		}

		// Token: 0x17000120 RID: 288
		// (get) Token: 0x0600040D RID: 1037 RVA: 0x00017DB8 File Offset: 0x000161B8
		// (set) Token: 0x0600040E RID: 1038 RVA: 0x00017DD3 File Offset: 0x000161D3
		public int numberOfSteps
		{
			get
			{
				return this.m_NumberOfSteps;
			}
			set
			{
				if (SetPropertyUtility.SetStruct<int>(ref this.m_NumberOfSteps, value))
				{
					this.Set(this.m_Value);
					this.UpdateVisuals();
				}
			}
		}

		// Token: 0x17000121 RID: 289
		// (get) Token: 0x0600040F RID: 1039 RVA: 0x00017DFC File Offset: 0x000161FC
		// (set) Token: 0x06000410 RID: 1040 RVA: 0x00017E17 File Offset: 0x00016217
		public Scrollbar.ScrollEvent onValueChanged
		{
			get
			{
				return this.m_OnValueChanged;
			}
			set
			{
				this.m_OnValueChanged = value;
			}
		}

		// Token: 0x17000122 RID: 290
		// (get) Token: 0x06000411 RID: 1041 RVA: 0x00017E24 File Offset: 0x00016224
		private float stepSize
		{
			get
			{
				return (this.m_NumberOfSteps <= 1) ? 0.1f : (1f / (float)(this.m_NumberOfSteps - 1));
			}
		}

		// Token: 0x06000412 RID: 1042 RVA: 0x00017E5E File Offset: 0x0001625E
		public virtual void Rebuild(CanvasUpdate executing)
		{
		}

		// Token: 0x06000413 RID: 1043 RVA: 0x00017E61 File Offset: 0x00016261
		public virtual void LayoutComplete()
		{
		}

		// Token: 0x06000414 RID: 1044 RVA: 0x00017E64 File Offset: 0x00016264
		public virtual void GraphicUpdateComplete()
		{
		}

		// Token: 0x06000415 RID: 1045 RVA: 0x00017E67 File Offset: 0x00016267
		protected override void OnEnable()
		{
			base.OnEnable();
			this.UpdateCachedReferences();
			this.Set(this.m_Value, false);
			this.UpdateVisuals();
		}

		// Token: 0x06000416 RID: 1046 RVA: 0x00017E89 File Offset: 0x00016289
		protected override void OnDisable()
		{
			this.m_Tracker.Clear();
			base.OnDisable();
		}

		// Token: 0x06000417 RID: 1047 RVA: 0x00017EA0 File Offset: 0x000162A0
		private void UpdateCachedReferences()
		{
			if (this.m_HandleRect && this.m_HandleRect.parent != null)
			{
				this.m_ContainerRect = this.m_HandleRect.parent.GetComponent<RectTransform>();
			}
			else
			{
				this.m_ContainerRect = null;
			}
		}

		// Token: 0x06000418 RID: 1048 RVA: 0x00017EF6 File Offset: 0x000162F6
		private void Set(float input)
		{
			this.Set(input, true);
		}

		// Token: 0x06000419 RID: 1049 RVA: 0x00017F04 File Offset: 0x00016304
		private void Set(float input, bool sendCallback)
		{
			float value = this.m_Value;
			this.m_Value = Mathf.Clamp01(input);
			if (value != this.value)
			{
				this.UpdateVisuals();
				if (sendCallback)
				{
					UISystemProfilerApi.AddMarker("Scrollbar.value", this);
					this.m_OnValueChanged.Invoke(this.value);
				}
			}
		}

		// Token: 0x0600041A RID: 1050 RVA: 0x00017F60 File Offset: 0x00016360
		protected override void OnRectTransformDimensionsChange()
		{
			base.OnRectTransformDimensionsChange();
			if (this.IsActive())
			{
				this.UpdateVisuals();
			}
		}

		// Token: 0x17000123 RID: 291
		// (get) Token: 0x0600041B RID: 1051 RVA: 0x00017F80 File Offset: 0x00016380
		private Scrollbar.Axis axis
		{
			get
			{
				return (this.m_Direction != Scrollbar.Direction.LeftToRight && this.m_Direction != Scrollbar.Direction.RightToLeft) ? Scrollbar.Axis.Vertical : Scrollbar.Axis.Horizontal;
			}
		}

		// Token: 0x17000124 RID: 292
		// (get) Token: 0x0600041C RID: 1052 RVA: 0x00017FB4 File Offset: 0x000163B4
		private bool reverseValue
		{
			get
			{
				return this.m_Direction == Scrollbar.Direction.RightToLeft || this.m_Direction == Scrollbar.Direction.TopToBottom;
			}
		}

		// Token: 0x0600041D RID: 1053 RVA: 0x00017FE4 File Offset: 0x000163E4
		private void UpdateVisuals()
		{
			this.m_Tracker.Clear();
			if (this.m_ContainerRect != null)
			{
				this.m_Tracker.Add(this, this.m_HandleRect, DrivenTransformProperties.Anchors);
				Vector2 zero = Vector2.zero;
				Vector2 one = Vector2.one;
				float num = this.value * (1f - this.size);
				if (this.reverseValue)
				{
					zero[(int)this.axis] = 1f - num - this.size;
					one[(int)this.axis] = 1f - num;
				}
				else
				{
					zero[(int)this.axis] = num;
					one[(int)this.axis] = num + this.size;
				}
				this.m_HandleRect.anchorMin = zero;
				this.m_HandleRect.anchorMax = one;
			}
		}

		// Token: 0x0600041E RID: 1054 RVA: 0x000180C8 File Offset: 0x000164C8
		private void UpdateDrag(PointerEventData eventData)
		{
			if (eventData.button == PointerEventData.InputButton.Left)
			{
				if (!(this.m_ContainerRect == null))
				{
					Vector2 a;
					if (RectTransformUtility.ScreenPointToLocalPointInRectangle(this.m_ContainerRect, eventData.position, eventData.pressEventCamera, out a))
					{
						Vector2 a2 = a - this.m_Offset - this.m_ContainerRect.rect.position;
						Vector2 vector = a2 - (this.m_HandleRect.rect.size - this.m_HandleRect.sizeDelta) * 0.5f;
						float num = (this.axis != Scrollbar.Axis.Horizontal) ? this.m_ContainerRect.rect.height : this.m_ContainerRect.rect.width;
						float num2 = num * (1f - this.size);
						if (num2 > 0f)
						{
							switch (this.m_Direction)
							{
							case Scrollbar.Direction.LeftToRight:
								this.Set(vector.x / num2);
								break;
							case Scrollbar.Direction.RightToLeft:
								this.Set(1f - vector.x / num2);
								break;
							case Scrollbar.Direction.BottomToTop:
								this.Set(vector.y / num2);
								break;
							case Scrollbar.Direction.TopToBottom:
								this.Set(1f - vector.y / num2);
								break;
							}
						}
					}
				}
			}
		}

		// Token: 0x0600041F RID: 1055 RVA: 0x0001825C File Offset: 0x0001665C
		private bool MayDrag(PointerEventData eventData)
		{
			return this.IsActive() && this.IsInteractable() && eventData.button == PointerEventData.InputButton.Left;
		}

		// Token: 0x06000420 RID: 1056 RVA: 0x00018294 File Offset: 0x00016694
		public virtual void OnBeginDrag(PointerEventData eventData)
		{
			this.isPointerDownAndNotDragging = false;
			if (this.MayDrag(eventData))
			{
				if (!(this.m_ContainerRect == null))
				{
					this.m_Offset = Vector2.zero;
					if (RectTransformUtility.RectangleContainsScreenPoint(this.m_HandleRect, eventData.position, eventData.enterEventCamera))
					{
						Vector2 a;
						if (RectTransformUtility.ScreenPointToLocalPointInRectangle(this.m_HandleRect, eventData.position, eventData.pressEventCamera, out a))
						{
							this.m_Offset = a - this.m_HandleRect.rect.center;
						}
					}
				}
			}
		}

		// Token: 0x06000421 RID: 1057 RVA: 0x00018336 File Offset: 0x00016736
		public virtual void OnDrag(PointerEventData eventData)
		{
			if (this.MayDrag(eventData))
			{
				if (this.m_ContainerRect != null)
				{
					this.UpdateDrag(eventData);
				}
			}
		}

		// Token: 0x06000422 RID: 1058 RVA: 0x00018362 File Offset: 0x00016762
		public override void OnPointerDown(PointerEventData eventData)
		{
			if (this.MayDrag(eventData))
			{
				base.OnPointerDown(eventData);
				this.isPointerDownAndNotDragging = true;
				this.m_PointerDownRepeat = base.StartCoroutine(this.ClickRepeat(eventData));
			}
		}

		// Token: 0x06000423 RID: 1059 RVA: 0x00018398 File Offset: 0x00016798
		protected IEnumerator ClickRepeat(PointerEventData eventData)
		{
			while (this.isPointerDownAndNotDragging)
			{
				if (!RectTransformUtility.RectangleContainsScreenPoint(this.m_HandleRect, eventData.position, eventData.enterEventCamera))
				{
					Vector2 vector;
					if (RectTransformUtility.ScreenPointToLocalPointInRectangle(this.m_HandleRect, eventData.position, eventData.pressEventCamera, out vector))
					{
						float num = (this.axis != Scrollbar.Axis.Horizontal) ? vector.y : vector.x;
						if (num < 0f)
						{
							this.value -= this.size;
						}
						else
						{
							this.value += this.size;
						}
					}
				}
				yield return new WaitForEndOfFrame();
			}
			base.StopCoroutine(this.m_PointerDownRepeat);
			yield break;
		}

		// Token: 0x06000424 RID: 1060 RVA: 0x000183C1 File Offset: 0x000167C1
		public override void OnPointerUp(PointerEventData eventData)
		{
			base.OnPointerUp(eventData);
			this.isPointerDownAndNotDragging = false;
		}

		// Token: 0x06000425 RID: 1061 RVA: 0x000183D4 File Offset: 0x000167D4
		public override void OnMove(AxisEventData eventData)
		{
			if (!this.IsActive() || !this.IsInteractable())
			{
				base.OnMove(eventData);
			}
			else
			{
				switch (eventData.moveDir)
				{
				case MoveDirection.Left:
					if (this.axis == Scrollbar.Axis.Horizontal && this.FindSelectableOnLeft() == null)
					{
						this.Set((!this.reverseValue) ? (this.value - this.stepSize) : (this.value + this.stepSize));
					}
					else
					{
						base.OnMove(eventData);
					}
					break;
				case MoveDirection.Up:
					if (this.axis == Scrollbar.Axis.Vertical && this.FindSelectableOnUp() == null)
					{
						this.Set((!this.reverseValue) ? (this.value + this.stepSize) : (this.value - this.stepSize));
					}
					else
					{
						base.OnMove(eventData);
					}
					break;
				case MoveDirection.Right:
					if (this.axis == Scrollbar.Axis.Horizontal && this.FindSelectableOnRight() == null)
					{
						this.Set((!this.reverseValue) ? (this.value + this.stepSize) : (this.value - this.stepSize));
					}
					else
					{
						base.OnMove(eventData);
					}
					break;
				case MoveDirection.Down:
					if (this.axis == Scrollbar.Axis.Vertical && this.FindSelectableOnDown() == null)
					{
						this.Set((!this.reverseValue) ? (this.value - this.stepSize) : (this.value + this.stepSize));
					}
					else
					{
						base.OnMove(eventData);
					}
					break;
				}
			}
		}

		// Token: 0x06000426 RID: 1062 RVA: 0x000185A0 File Offset: 0x000169A0
		public override Selectable FindSelectableOnLeft()
		{
			Selectable result;
			if (base.navigation.mode == Navigation.Mode.Automatic && this.axis == Scrollbar.Axis.Horizontal)
			{
				result = null;
			}
			else
			{
				result = base.FindSelectableOnLeft();
			}
			return result;
		}

		// Token: 0x06000427 RID: 1063 RVA: 0x000185E4 File Offset: 0x000169E4
		public override Selectable FindSelectableOnRight()
		{
			Selectable result;
			if (base.navigation.mode == Navigation.Mode.Automatic && this.axis == Scrollbar.Axis.Horizontal)
			{
				result = null;
			}
			else
			{
				result = base.FindSelectableOnRight();
			}
			return result;
		}

		// Token: 0x06000428 RID: 1064 RVA: 0x00018628 File Offset: 0x00016A28
		public override Selectable FindSelectableOnUp()
		{
			Selectable result;
			if (base.navigation.mode == Navigation.Mode.Automatic && this.axis == Scrollbar.Axis.Vertical)
			{
				result = null;
			}
			else
			{
				result = base.FindSelectableOnUp();
			}
			return result;
		}

		// Token: 0x06000429 RID: 1065 RVA: 0x0001866C File Offset: 0x00016A6C
		public override Selectable FindSelectableOnDown()
		{
			Selectable result;
			if (base.navigation.mode == Navigation.Mode.Automatic && this.axis == Scrollbar.Axis.Vertical)
			{
				result = null;
			}
			else
			{
				result = base.FindSelectableOnDown();
			}
			return result;
		}

		// Token: 0x0600042A RID: 1066 RVA: 0x000186AE File Offset: 0x00016AAE
		public virtual void OnInitializePotentialDrag(PointerEventData eventData)
		{
			eventData.useDragThreshold = false;
		}

		// Token: 0x0600042B RID: 1067 RVA: 0x000186B8 File Offset: 0x00016AB8
		public void SetDirection(Scrollbar.Direction direction, bool includeRectLayouts)
		{
			Scrollbar.Axis axis = this.axis;
			bool reverseValue = this.reverseValue;
			this.direction = direction;
			if (includeRectLayouts)
			{
				if (this.axis != axis)
				{
					RectTransformUtility.FlipLayoutAxes(base.transform as RectTransform, true, true);
				}
				if (this.reverseValue != reverseValue)
				{
					RectTransformUtility.FlipLayoutOnAxis(base.transform as RectTransform, (int)this.axis, true, true);
				}
			}
		}

		// Token: 0x0600042C RID: 1068 RVA: 0x00018728 File Offset: 0x00016B28
		Transform ICanvasElement.get_transform()
		{
			return base.transform;
		}

		// Token: 0x04000201 RID: 513
		[SerializeField]
		private RectTransform m_HandleRect;

		// Token: 0x04000202 RID: 514
		[SerializeField]
		private Scrollbar.Direction m_Direction = Scrollbar.Direction.LeftToRight;

		// Token: 0x04000203 RID: 515
		[Range(0f, 1f)]
		[SerializeField]
		private float m_Value;

		// Token: 0x04000204 RID: 516
		[Range(0f, 1f)]
		[SerializeField]
		private float m_Size = 0.2f;

		// Token: 0x04000205 RID: 517
		[Range(0f, 11f)]
		[SerializeField]
		private int m_NumberOfSteps = 0;

		// Token: 0x04000206 RID: 518
		[Space(6f)]
		[SerializeField]
		private Scrollbar.ScrollEvent m_OnValueChanged = new Scrollbar.ScrollEvent();

		// Token: 0x04000207 RID: 519
		private RectTransform m_ContainerRect;

		// Token: 0x04000208 RID: 520
		private Vector2 m_Offset = Vector2.zero;

		// Token: 0x04000209 RID: 521
		private DrivenRectTransformTracker m_Tracker;

		// Token: 0x0400020A RID: 522
		private Coroutine m_PointerDownRepeat;

		// Token: 0x0400020B RID: 523
		private bool isPointerDownAndNotDragging = false;

		// Token: 0x0200006F RID: 111
		public enum Direction
		{
			// Token: 0x0400020D RID: 525
			LeftToRight,
			// Token: 0x0400020E RID: 526
			RightToLeft,
			// Token: 0x0400020F RID: 527
			BottomToTop,
			// Token: 0x04000210 RID: 528
			TopToBottom
		}

		// Token: 0x02000070 RID: 112
		[Serializable]
		public class ScrollEvent : UnityEvent<float>
		{
			// Token: 0x0600042D RID: 1069 RVA: 0x00018730 File Offset: 0x00016B30
			public ScrollEvent()
			{
			}
		}

		// Token: 0x02000071 RID: 113
		private enum Axis
		{
			// Token: 0x04000212 RID: 530
			Horizontal,
			// Token: 0x04000213 RID: 531
			Vertical
		}

		// Token: 0x020000BB RID: 187
		[CompilerGenerated]
		private sealed class <ClickRepeat>c__Iterator0 : IEnumerator, IDisposable, IEnumerator<object>
		{
			// Token: 0x0600066E RID: 1646 RVA: 0x00018738 File Offset: 0x00016B38
			[DebuggerHidden]
			public <ClickRepeat>c__Iterator0()
			{
			}

			// Token: 0x0600066F RID: 1647 RVA: 0x00018740 File Offset: 0x00016B40
			public bool MoveNext()
			{
				uint num = (uint)this.$PC;
				this.$PC = -1;
				switch (num)
				{
				case 0U:
					break;
				case 1U:
					break;
				default:
					return false;
				}
				if (this.isPointerDownAndNotDragging)
				{
					if (!RectTransformUtility.RectangleContainsScreenPoint(this.m_HandleRect, eventData.position, eventData.enterEventCamera))
					{
						Vector2 vector;
						if (RectTransformUtility.ScreenPointToLocalPointInRectangle(this.m_HandleRect, eventData.position, eventData.pressEventCamera, out vector))
						{
							float num2 = (base.axis != Scrollbar.Axis.Horizontal) ? vector.y : vector.x;
							if (num2 < 0f)
							{
								base.value -= base.size;
							}
							else
							{
								base.value += base.size;
							}
						}
					}
					this.$current = new WaitForEndOfFrame();
					if (!this.$disposing)
					{
						this.$PC = 1;
					}
					return true;
				}
				base.StopCoroutine(this.m_PointerDownRepeat);
				this.$PC = -1;
				return false;
			}

			// Token: 0x170001AF RID: 431
			// (get) Token: 0x06000670 RID: 1648 RVA: 0x00018890 File Offset: 0x00016C90
			object IEnumerator<object>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.$current;
				}
			}

			// Token: 0x170001B0 RID: 432
			// (get) Token: 0x06000671 RID: 1649 RVA: 0x000188AC File Offset: 0x00016CAC
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.$current;
				}
			}

			// Token: 0x06000672 RID: 1650 RVA: 0x000188C6 File Offset: 0x00016CC6
			[DebuggerHidden]
			public void Dispose()
			{
				this.$disposing = true;
				this.$PC = -1;
			}

			// Token: 0x06000673 RID: 1651 RVA: 0x000188D6 File Offset: 0x00016CD6
			[DebuggerHidden]
			public void Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x04000311 RID: 785
			internal PointerEventData eventData;

			// Token: 0x04000312 RID: 786
			internal Scrollbar $this;

			// Token: 0x04000313 RID: 787
			internal object $current;

			// Token: 0x04000314 RID: 788
			internal bool $disposing;

			// Token: 0x04000315 RID: 789
			internal int $PC;
		}
	}
}
