﻿using System;

namespace UnityEngine.UI
{
	// Token: 0x02000086 RID: 134
	internal class RectangularVertexClipper
	{
		// Token: 0x06000509 RID: 1289 RVA: 0x0001B23A File Offset: 0x0001963A
		public RectangularVertexClipper()
		{
		}

		// Token: 0x0600050A RID: 1290 RVA: 0x0001B25C File Offset: 0x0001965C
		public Rect GetCanvasRect(RectTransform t, Canvas c)
		{
			Rect result;
			if (c == null)
			{
				result = default(Rect);
			}
			else
			{
				t.GetWorldCorners(this.m_WorldCorners);
				Transform component = c.GetComponent<Transform>();
				for (int i = 0; i < 4; i++)
				{
					this.m_CanvasCorners[i] = component.InverseTransformPoint(this.m_WorldCorners[i]);
				}
				result = new Rect(this.m_CanvasCorners[0].x, this.m_CanvasCorners[0].y, this.m_CanvasCorners[2].x - this.m_CanvasCorners[0].x, this.m_CanvasCorners[2].y - this.m_CanvasCorners[0].y);
			}
			return result;
		}

		// Token: 0x04000266 RID: 614
		private readonly Vector3[] m_WorldCorners = new Vector3[4];

		// Token: 0x04000267 RID: 615
		private readonly Vector3[] m_CanvasCorners = new Vector3[4];
	}
}
