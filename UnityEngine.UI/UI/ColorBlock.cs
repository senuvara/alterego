﻿using System;
using UnityEngine.Serialization;

namespace UnityEngine.UI
{
	// Token: 0x0200003F RID: 63
	[Serializable]
	public struct ColorBlock : IEquatable<ColorBlock>
	{
		// Token: 0x1700006D RID: 109
		// (get) Token: 0x060001A5 RID: 421 RVA: 0x0000862C File Offset: 0x00006A2C
		// (set) Token: 0x060001A6 RID: 422 RVA: 0x00008647 File Offset: 0x00006A47
		public Color normalColor
		{
			get
			{
				return this.m_NormalColor;
			}
			set
			{
				this.m_NormalColor = value;
			}
		}

		// Token: 0x1700006E RID: 110
		// (get) Token: 0x060001A7 RID: 423 RVA: 0x00008654 File Offset: 0x00006A54
		// (set) Token: 0x060001A8 RID: 424 RVA: 0x0000866F File Offset: 0x00006A6F
		public Color highlightedColor
		{
			get
			{
				return this.m_HighlightedColor;
			}
			set
			{
				this.m_HighlightedColor = value;
			}
		}

		// Token: 0x1700006F RID: 111
		// (get) Token: 0x060001A9 RID: 425 RVA: 0x0000867C File Offset: 0x00006A7C
		// (set) Token: 0x060001AA RID: 426 RVA: 0x00008697 File Offset: 0x00006A97
		public Color pressedColor
		{
			get
			{
				return this.m_PressedColor;
			}
			set
			{
				this.m_PressedColor = value;
			}
		}

		// Token: 0x17000070 RID: 112
		// (get) Token: 0x060001AB RID: 427 RVA: 0x000086A4 File Offset: 0x00006AA4
		// (set) Token: 0x060001AC RID: 428 RVA: 0x000086BF File Offset: 0x00006ABF
		public Color disabledColor
		{
			get
			{
				return this.m_DisabledColor;
			}
			set
			{
				this.m_DisabledColor = value;
			}
		}

		// Token: 0x17000071 RID: 113
		// (get) Token: 0x060001AD RID: 429 RVA: 0x000086CC File Offset: 0x00006ACC
		// (set) Token: 0x060001AE RID: 430 RVA: 0x000086E7 File Offset: 0x00006AE7
		public float colorMultiplier
		{
			get
			{
				return this.m_ColorMultiplier;
			}
			set
			{
				this.m_ColorMultiplier = value;
			}
		}

		// Token: 0x17000072 RID: 114
		// (get) Token: 0x060001AF RID: 431 RVA: 0x000086F4 File Offset: 0x00006AF4
		// (set) Token: 0x060001B0 RID: 432 RVA: 0x0000870F File Offset: 0x00006B0F
		public float fadeDuration
		{
			get
			{
				return this.m_FadeDuration;
			}
			set
			{
				this.m_FadeDuration = value;
			}
		}

		// Token: 0x17000073 RID: 115
		// (get) Token: 0x060001B1 RID: 433 RVA: 0x0000871C File Offset: 0x00006B1C
		public static ColorBlock defaultColorBlock
		{
			get
			{
				return new ColorBlock
				{
					m_NormalColor = new Color32(byte.MaxValue, byte.MaxValue, byte.MaxValue, byte.MaxValue),
					m_HighlightedColor = new Color32(245, 245, 245, byte.MaxValue),
					m_PressedColor = new Color32(200, 200, 200, byte.MaxValue),
					m_DisabledColor = new Color32(200, 200, 200, 128),
					colorMultiplier = 1f,
					fadeDuration = 0.1f
				};
			}
		}

		// Token: 0x060001B2 RID: 434 RVA: 0x000087E8 File Offset: 0x00006BE8
		public override bool Equals(object obj)
		{
			return obj is ColorBlock && this.Equals((ColorBlock)obj);
		}

		// Token: 0x060001B3 RID: 435 RVA: 0x0000881C File Offset: 0x00006C1C
		public bool Equals(ColorBlock other)
		{
			return this.normalColor == other.normalColor && this.highlightedColor == other.highlightedColor && this.pressedColor == other.pressedColor && this.disabledColor == other.disabledColor && this.colorMultiplier == other.colorMultiplier && this.fadeDuration == other.fadeDuration;
		}

		// Token: 0x060001B4 RID: 436 RVA: 0x000088B4 File Offset: 0x00006CB4
		public static bool operator ==(ColorBlock point1, ColorBlock point2)
		{
			return point1.Equals(point2);
		}

		// Token: 0x060001B5 RID: 437 RVA: 0x000088D4 File Offset: 0x00006CD4
		public static bool operator !=(ColorBlock point1, ColorBlock point2)
		{
			return !point1.Equals(point2);
		}

		// Token: 0x060001B6 RID: 438 RVA: 0x000088F4 File Offset: 0x00006CF4
		public override int GetHashCode()
		{
			return base.GetHashCode();
		}

		// Token: 0x040000D3 RID: 211
		[FormerlySerializedAs("normalColor")]
		[SerializeField]
		private Color m_NormalColor;

		// Token: 0x040000D4 RID: 212
		[FormerlySerializedAs("highlightedColor")]
		[FormerlySerializedAs("m_SelectedColor")]
		[SerializeField]
		private Color m_HighlightedColor;

		// Token: 0x040000D5 RID: 213
		[FormerlySerializedAs("pressedColor")]
		[SerializeField]
		private Color m_PressedColor;

		// Token: 0x040000D6 RID: 214
		[FormerlySerializedAs("disabledColor")]
		[SerializeField]
		private Color m_DisabledColor;

		// Token: 0x040000D7 RID: 215
		[Range(1f, 5f)]
		[SerializeField]
		private float m_ColorMultiplier;

		// Token: 0x040000D8 RID: 216
		[FormerlySerializedAs("fadeDuration")]
		[SerializeField]
		private float m_FadeDuration;
	}
}
