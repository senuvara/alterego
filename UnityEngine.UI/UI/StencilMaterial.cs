﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine.Rendering;

namespace UnityEngine.UI
{
	// Token: 0x0200007B RID: 123
	public static class StencilMaterial
	{
		// Token: 0x060004A3 RID: 1187 RVA: 0x00019820 File Offset: 0x00017C20
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Obsolete("Use Material.Add instead.", true)]
		public static Material Add(Material baseMat, int stencilID)
		{
			return null;
		}

		// Token: 0x060004A4 RID: 1188 RVA: 0x00019838 File Offset: 0x00017C38
		public static Material Add(Material baseMat, int stencilID, StencilOp operation, CompareFunction compareFunction, ColorWriteMask colorWriteMask)
		{
			return StencilMaterial.Add(baseMat, stencilID, operation, compareFunction, colorWriteMask, 255, 255);
		}

		// Token: 0x060004A5 RID: 1189 RVA: 0x00019864 File Offset: 0x00017C64
		public static Material Add(Material baseMat, int stencilID, StencilOp operation, CompareFunction compareFunction, ColorWriteMask colorWriteMask, int readMask, int writeMask)
		{
			Material result;
			if ((stencilID <= 0 && colorWriteMask == ColorWriteMask.All) || baseMat == null)
			{
				result = baseMat;
			}
			else if (!baseMat.HasProperty("_Stencil"))
			{
				Debug.LogWarning("Material " + baseMat.name + " doesn't have _Stencil property", baseMat);
				result = baseMat;
			}
			else if (!baseMat.HasProperty("_StencilOp"))
			{
				Debug.LogWarning("Material " + baseMat.name + " doesn't have _StencilOp property", baseMat);
				result = baseMat;
			}
			else if (!baseMat.HasProperty("_StencilComp"))
			{
				Debug.LogWarning("Material " + baseMat.name + " doesn't have _StencilComp property", baseMat);
				result = baseMat;
			}
			else if (!baseMat.HasProperty("_StencilReadMask"))
			{
				Debug.LogWarning("Material " + baseMat.name + " doesn't have _StencilReadMask property", baseMat);
				result = baseMat;
			}
			else if (!baseMat.HasProperty("_StencilWriteMask"))
			{
				Debug.LogWarning("Material " + baseMat.name + " doesn't have _StencilWriteMask property", baseMat);
				result = baseMat;
			}
			else if (!baseMat.HasProperty("_ColorMask"))
			{
				Debug.LogWarning("Material " + baseMat.name + " doesn't have _ColorMask property", baseMat);
				result = baseMat;
			}
			else
			{
				for (int i = 0; i < StencilMaterial.m_List.Count; i++)
				{
					StencilMaterial.MatEntry matEntry = StencilMaterial.m_List[i];
					if (matEntry.baseMat == baseMat && matEntry.stencilId == stencilID && matEntry.operation == operation && matEntry.compareFunction == compareFunction && matEntry.readMask == readMask && matEntry.writeMask == writeMask && matEntry.colorMask == colorWriteMask)
					{
						matEntry.count++;
						return matEntry.customMat;
					}
				}
				StencilMaterial.MatEntry matEntry2 = new StencilMaterial.MatEntry();
				matEntry2.count = 1;
				matEntry2.baseMat = baseMat;
				matEntry2.customMat = new Material(baseMat);
				matEntry2.customMat.hideFlags = HideFlags.HideAndDontSave;
				matEntry2.stencilId = stencilID;
				matEntry2.operation = operation;
				matEntry2.compareFunction = compareFunction;
				matEntry2.readMask = readMask;
				matEntry2.writeMask = writeMask;
				matEntry2.colorMask = colorWriteMask;
				matEntry2.useAlphaClip = (operation != StencilOp.Keep && writeMask > 0);
				matEntry2.customMat.name = string.Format("Stencil Id:{0}, Op:{1}, Comp:{2}, WriteMask:{3}, ReadMask:{4}, ColorMask:{5} AlphaClip:{6} ({7})", new object[]
				{
					stencilID,
					operation,
					compareFunction,
					writeMask,
					readMask,
					colorWriteMask,
					matEntry2.useAlphaClip,
					baseMat.name
				});
				matEntry2.customMat.SetInt("_Stencil", stencilID);
				matEntry2.customMat.SetInt("_StencilOp", (int)operation);
				matEntry2.customMat.SetInt("_StencilComp", (int)compareFunction);
				matEntry2.customMat.SetInt("_StencilReadMask", readMask);
				matEntry2.customMat.SetInt("_StencilWriteMask", writeMask);
				matEntry2.customMat.SetInt("_ColorMask", (int)colorWriteMask);
				if (matEntry2.customMat.HasProperty("_UseAlphaClip"))
				{
					matEntry2.customMat.SetInt("_UseAlphaClip", (!matEntry2.useAlphaClip) ? 0 : 1);
				}
				if (matEntry2.useAlphaClip)
				{
					matEntry2.customMat.EnableKeyword("UNITY_UI_ALPHACLIP");
				}
				else
				{
					matEntry2.customMat.DisableKeyword("UNITY_UI_ALPHACLIP");
				}
				StencilMaterial.m_List.Add(matEntry2);
				result = matEntry2.customMat;
			}
			return result;
		}

		// Token: 0x060004A6 RID: 1190 RVA: 0x00019C28 File Offset: 0x00018028
		public static void Remove(Material customMat)
		{
			if (!(customMat == null))
			{
				for (int i = 0; i < StencilMaterial.m_List.Count; i++)
				{
					StencilMaterial.MatEntry matEntry = StencilMaterial.m_List[i];
					if (!(matEntry.customMat != customMat))
					{
						if (--matEntry.count == 0)
						{
							Misc.DestroyImmediate(matEntry.customMat);
							matEntry.baseMat = null;
							StencilMaterial.m_List.RemoveAt(i);
						}
						break;
					}
				}
			}
		}

		// Token: 0x060004A7 RID: 1191 RVA: 0x00019CC0 File Offset: 0x000180C0
		public static void ClearAll()
		{
			for (int i = 0; i < StencilMaterial.m_List.Count; i++)
			{
				StencilMaterial.MatEntry matEntry = StencilMaterial.m_List[i];
				Misc.DestroyImmediate(matEntry.customMat);
				matEntry.baseMat = null;
			}
			StencilMaterial.m_List.Clear();
		}

		// Token: 0x060004A8 RID: 1192 RVA: 0x00019D13 File Offset: 0x00018113
		// Note: this type is marked as 'beforefieldinit'.
		static StencilMaterial()
		{
		}

		// Token: 0x04000246 RID: 582
		private static List<StencilMaterial.MatEntry> m_List = new List<StencilMaterial.MatEntry>();

		// Token: 0x0200007C RID: 124
		private class MatEntry
		{
			// Token: 0x060004A9 RID: 1193 RVA: 0x00019D1F File Offset: 0x0001811F
			public MatEntry()
			{
			}

			// Token: 0x04000247 RID: 583
			public Material baseMat;

			// Token: 0x04000248 RID: 584
			public Material customMat;

			// Token: 0x04000249 RID: 585
			public int count;

			// Token: 0x0400024A RID: 586
			public int stencilId;

			// Token: 0x0400024B RID: 587
			public StencilOp operation = StencilOp.Keep;

			// Token: 0x0400024C RID: 588
			public CompareFunction compareFunction = CompareFunction.Always;

			// Token: 0x0400024D RID: 589
			public int readMask;

			// Token: 0x0400024E RID: 590
			public int writeMask;

			// Token: 0x0400024F RID: 591
			public bool useAlphaClip;

			// Token: 0x04000250 RID: 592
			public ColorWriteMask colorMask;
		}
	}
}
