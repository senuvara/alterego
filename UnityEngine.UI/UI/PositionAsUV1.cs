﻿using System;

namespace UnityEngine.UI
{
	// Token: 0x020000B1 RID: 177
	[AddComponentMenu("UI/Effects/Position As UV1", 16)]
	public class PositionAsUV1 : BaseMeshEffect
	{
		// Token: 0x06000642 RID: 1602 RVA: 0x0001F722 File Offset: 0x0001DB22
		protected PositionAsUV1()
		{
		}

		// Token: 0x06000643 RID: 1603 RVA: 0x0001F72C File Offset: 0x0001DB2C
		public override void ModifyMesh(VertexHelper vh)
		{
			UIVertex vertex = default(UIVertex);
			for (int i = 0; i < vh.currentVertCount; i++)
			{
				vh.PopulateUIVertex(ref vertex, i);
				vertex.uv1 = new Vector2(vertex.position.x, vertex.position.y);
				vh.SetUIVertex(vertex, i);
			}
		}
	}
}
