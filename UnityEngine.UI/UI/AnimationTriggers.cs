﻿using System;
using UnityEngine.Serialization;

namespace UnityEngine.UI
{
	// Token: 0x02000039 RID: 57
	[Serializable]
	public class AnimationTriggers
	{
		// Token: 0x0600017C RID: 380 RVA: 0x00006EBD File Offset: 0x000052BD
		public AnimationTriggers()
		{
		}

		// Token: 0x17000066 RID: 102
		// (get) Token: 0x0600017D RID: 381 RVA: 0x00006EF4 File Offset: 0x000052F4
		// (set) Token: 0x0600017E RID: 382 RVA: 0x00006F0F File Offset: 0x0000530F
		public string normalTrigger
		{
			get
			{
				return this.m_NormalTrigger;
			}
			set
			{
				this.m_NormalTrigger = value;
			}
		}

		// Token: 0x17000067 RID: 103
		// (get) Token: 0x0600017F RID: 383 RVA: 0x00006F1C File Offset: 0x0000531C
		// (set) Token: 0x06000180 RID: 384 RVA: 0x00006F37 File Offset: 0x00005337
		public string highlightedTrigger
		{
			get
			{
				return this.m_HighlightedTrigger;
			}
			set
			{
				this.m_HighlightedTrigger = value;
			}
		}

		// Token: 0x17000068 RID: 104
		// (get) Token: 0x06000181 RID: 385 RVA: 0x00006F44 File Offset: 0x00005344
		// (set) Token: 0x06000182 RID: 386 RVA: 0x00006F5F File Offset: 0x0000535F
		public string pressedTrigger
		{
			get
			{
				return this.m_PressedTrigger;
			}
			set
			{
				this.m_PressedTrigger = value;
			}
		}

		// Token: 0x17000069 RID: 105
		// (get) Token: 0x06000183 RID: 387 RVA: 0x00006F6C File Offset: 0x0000536C
		// (set) Token: 0x06000184 RID: 388 RVA: 0x00006F87 File Offset: 0x00005387
		public string disabledTrigger
		{
			get
			{
				return this.m_DisabledTrigger;
			}
			set
			{
				this.m_DisabledTrigger = value;
			}
		}

		// Token: 0x040000BC RID: 188
		private const string kDefaultNormalAnimName = "Normal";

		// Token: 0x040000BD RID: 189
		private const string kDefaultSelectedAnimName = "Highlighted";

		// Token: 0x040000BE RID: 190
		private const string kDefaultPressedAnimName = "Pressed";

		// Token: 0x040000BF RID: 191
		private const string kDefaultDisabledAnimName = "Disabled";

		// Token: 0x040000C0 RID: 192
		[FormerlySerializedAs("normalTrigger")]
		[SerializeField]
		private string m_NormalTrigger = "Normal";

		// Token: 0x040000C1 RID: 193
		[FormerlySerializedAs("highlightedTrigger")]
		[FormerlySerializedAs("m_SelectedTrigger")]
		[SerializeField]
		private string m_HighlightedTrigger = "Highlighted";

		// Token: 0x040000C2 RID: 194
		[FormerlySerializedAs("pressedTrigger")]
		[SerializeField]
		private string m_PressedTrigger = "Pressed";

		// Token: 0x040000C3 RID: 195
		[FormerlySerializedAs("disabledTrigger")]
		[SerializeField]
		private string m_DisabledTrigger = "Disabled";
	}
}
