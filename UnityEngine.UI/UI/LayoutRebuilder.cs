﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine.Events;

namespace UnityEngine.UI
{
	// Token: 0x0200009C RID: 156
	public class LayoutRebuilder : ICanvasElement
	{
		// Token: 0x060005B4 RID: 1460 RVA: 0x0001D685 File Offset: 0x0001BA85
		static LayoutRebuilder()
		{
			if (LayoutRebuilder.<>f__mg$cache0 == null)
			{
				LayoutRebuilder.<>f__mg$cache0 = new RectTransform.ReapplyDrivenProperties(LayoutRebuilder.ReapplyDrivenProperties);
			}
			RectTransform.reapplyDrivenProperties += LayoutRebuilder.<>f__mg$cache0;
		}

		// Token: 0x060005B5 RID: 1461 RVA: 0x0001D6C1 File Offset: 0x0001BAC1
		public LayoutRebuilder()
		{
		}

		// Token: 0x060005B6 RID: 1462 RVA: 0x0001D6C9 File Offset: 0x0001BAC9
		private void Initialize(RectTransform controller)
		{
			this.m_ToRebuild = controller;
			this.m_CachedHashFromTransform = controller.GetHashCode();
		}

		// Token: 0x060005B7 RID: 1463 RVA: 0x0001D6DF File Offset: 0x0001BADF
		private void Clear()
		{
			this.m_ToRebuild = null;
			this.m_CachedHashFromTransform = 0;
		}

		// Token: 0x060005B8 RID: 1464 RVA: 0x0001D6F0 File Offset: 0x0001BAF0
		private static void ReapplyDrivenProperties(RectTransform driven)
		{
			LayoutRebuilder.MarkLayoutForRebuild(driven);
		}

		// Token: 0x17000197 RID: 407
		// (get) Token: 0x060005B9 RID: 1465 RVA: 0x0001D6FC File Offset: 0x0001BAFC
		public Transform transform
		{
			get
			{
				return this.m_ToRebuild;
			}
		}

		// Token: 0x060005BA RID: 1466 RVA: 0x0001D718 File Offset: 0x0001BB18
		public bool IsDestroyed()
		{
			return this.m_ToRebuild == null;
		}

		// Token: 0x060005BB RID: 1467 RVA: 0x0001D739 File Offset: 0x0001BB39
		private static void StripDisabledBehavioursFromList(List<Component> components)
		{
			components.RemoveAll((Component e) => e is Behaviour && !((Behaviour)e).isActiveAndEnabled);
		}

		// Token: 0x060005BC RID: 1468 RVA: 0x0001D760 File Offset: 0x0001BB60
		public static void ForceRebuildLayoutImmediate(RectTransform layoutRoot)
		{
			LayoutRebuilder layoutRebuilder = LayoutRebuilder.s_Rebuilders.Get();
			layoutRebuilder.Initialize(layoutRoot);
			layoutRebuilder.Rebuild(CanvasUpdate.Layout);
			LayoutRebuilder.s_Rebuilders.Release(layoutRebuilder);
		}

		// Token: 0x060005BD RID: 1469 RVA: 0x0001D794 File Offset: 0x0001BB94
		public void Rebuild(CanvasUpdate executing)
		{
			if (executing == CanvasUpdate.Layout)
			{
				this.PerformLayoutCalculation(this.m_ToRebuild, delegate(Component e)
				{
					(e as ILayoutElement).CalculateLayoutInputHorizontal();
				});
				this.PerformLayoutControl(this.m_ToRebuild, delegate(Component e)
				{
					(e as ILayoutController).SetLayoutHorizontal();
				});
				this.PerformLayoutCalculation(this.m_ToRebuild, delegate(Component e)
				{
					(e as ILayoutElement).CalculateLayoutInputVertical();
				});
				this.PerformLayoutControl(this.m_ToRebuild, delegate(Component e)
				{
					(e as ILayoutController).SetLayoutVertical();
				});
			}
		}

		// Token: 0x060005BE RID: 1470 RVA: 0x0001D858 File Offset: 0x0001BC58
		private void PerformLayoutControl(RectTransform rect, UnityAction<Component> action)
		{
			if (!(rect == null))
			{
				List<Component> list = ListPool<Component>.Get();
				rect.GetComponents(typeof(ILayoutController), list);
				LayoutRebuilder.StripDisabledBehavioursFromList(list);
				if (list.Count > 0)
				{
					for (int i = 0; i < list.Count; i++)
					{
						if (list[i] is ILayoutSelfController)
						{
							action(list[i]);
						}
					}
					for (int j = 0; j < list.Count; j++)
					{
						if (!(list[j] is ILayoutSelfController))
						{
							action(list[j]);
						}
					}
					for (int k = 0; k < rect.childCount; k++)
					{
						this.PerformLayoutControl(rect.GetChild(k) as RectTransform, action);
					}
				}
				ListPool<Component>.Release(list);
			}
		}

		// Token: 0x060005BF RID: 1471 RVA: 0x0001D93C File Offset: 0x0001BD3C
		private void PerformLayoutCalculation(RectTransform rect, UnityAction<Component> action)
		{
			if (!(rect == null))
			{
				List<Component> list = ListPool<Component>.Get();
				rect.GetComponents(typeof(ILayoutElement), list);
				LayoutRebuilder.StripDisabledBehavioursFromList(list);
				if (list.Count > 0 || rect.GetComponent(typeof(ILayoutGroup)))
				{
					for (int i = 0; i < rect.childCount; i++)
					{
						this.PerformLayoutCalculation(rect.GetChild(i) as RectTransform, action);
					}
					for (int j = 0; j < list.Count; j++)
					{
						action(list[j]);
					}
				}
				ListPool<Component>.Release(list);
			}
		}

		// Token: 0x060005C0 RID: 1472 RVA: 0x0001D9F4 File Offset: 0x0001BDF4
		public static void MarkLayoutForRebuild(RectTransform rect)
		{
			if (!(rect == null) && !(rect.gameObject == null))
			{
				List<Component> list = ListPool<Component>.Get();
				bool flag = true;
				RectTransform rectTransform = rect;
				RectTransform rectTransform2 = rectTransform.parent as RectTransform;
				while (flag && !(rectTransform2 == null) && !(rectTransform2.gameObject == null))
				{
					flag = false;
					rectTransform2.GetComponents(typeof(ILayoutGroup), list);
					for (int i = 0; i < list.Count; i++)
					{
						Component component = list[i];
						if (component != null && component is Behaviour && ((Behaviour)component).isActiveAndEnabled)
						{
							flag = true;
							rectTransform = rectTransform2;
							break;
						}
					}
					rectTransform2 = (rectTransform2.parent as RectTransform);
				}
				if (rectTransform == rect && !LayoutRebuilder.ValidController(rectTransform, list))
				{
					ListPool<Component>.Release(list);
				}
				else
				{
					LayoutRebuilder.MarkLayoutRootForRebuild(rectTransform);
					ListPool<Component>.Release(list);
				}
			}
		}

		// Token: 0x060005C1 RID: 1473 RVA: 0x0001DB10 File Offset: 0x0001BF10
		private static bool ValidController(RectTransform layoutRoot, List<Component> comps)
		{
			bool result;
			if (layoutRoot == null || layoutRoot.gameObject == null)
			{
				result = false;
			}
			else
			{
				layoutRoot.GetComponents(typeof(ILayoutController), comps);
				for (int i = 0; i < comps.Count; i++)
				{
					Component component = comps[i];
					if (component != null && component is Behaviour && ((Behaviour)component).isActiveAndEnabled)
					{
						return true;
					}
				}
				result = false;
			}
			return result;
		}

		// Token: 0x060005C2 RID: 1474 RVA: 0x0001DBAC File Offset: 0x0001BFAC
		private static void MarkLayoutRootForRebuild(RectTransform controller)
		{
			if (!(controller == null))
			{
				LayoutRebuilder layoutRebuilder = LayoutRebuilder.s_Rebuilders.Get();
				layoutRebuilder.Initialize(controller);
				if (!CanvasUpdateRegistry.TryRegisterCanvasElementForLayoutRebuild(layoutRebuilder))
				{
					LayoutRebuilder.s_Rebuilders.Release(layoutRebuilder);
				}
			}
		}

		// Token: 0x060005C3 RID: 1475 RVA: 0x0001DBF3 File Offset: 0x0001BFF3
		public void LayoutComplete()
		{
			LayoutRebuilder.s_Rebuilders.Release(this);
		}

		// Token: 0x060005C4 RID: 1476 RVA: 0x0001DC01 File Offset: 0x0001C001
		public void GraphicUpdateComplete()
		{
		}

		// Token: 0x060005C5 RID: 1477 RVA: 0x0001DC04 File Offset: 0x0001C004
		public override int GetHashCode()
		{
			return this.m_CachedHashFromTransform;
		}

		// Token: 0x060005C6 RID: 1478 RVA: 0x0001DC20 File Offset: 0x0001C020
		public override bool Equals(object obj)
		{
			return obj.GetHashCode() == this.GetHashCode();
		}

		// Token: 0x060005C7 RID: 1479 RVA: 0x0001DC44 File Offset: 0x0001C044
		public override string ToString()
		{
			return "(Layout Rebuilder for) " + this.m_ToRebuild;
		}

		// Token: 0x060005C8 RID: 1480 RVA: 0x0001DC69 File Offset: 0x0001C069
		[CompilerGenerated]
		private static void <s_Rebuilders>m__0(LayoutRebuilder x)
		{
			x.Clear();
		}

		// Token: 0x060005C9 RID: 1481 RVA: 0x0001DC74 File Offset: 0x0001C074
		[CompilerGenerated]
		private static bool <StripDisabledBehavioursFromList>m__1(Component e)
		{
			return e is Behaviour && !((Behaviour)e).isActiveAndEnabled;
		}

		// Token: 0x060005CA RID: 1482 RVA: 0x0001DCA4 File Offset: 0x0001C0A4
		[CompilerGenerated]
		private static void <Rebuild>m__2(Component e)
		{
			(e as ILayoutElement).CalculateLayoutInputHorizontal();
		}

		// Token: 0x060005CB RID: 1483 RVA: 0x0001DCB1 File Offset: 0x0001C0B1
		[CompilerGenerated]
		private static void <Rebuild>m__3(Component e)
		{
			(e as ILayoutController).SetLayoutHorizontal();
		}

		// Token: 0x060005CC RID: 1484 RVA: 0x0001DCBE File Offset: 0x0001C0BE
		[CompilerGenerated]
		private static void <Rebuild>m__4(Component e)
		{
			(e as ILayoutElement).CalculateLayoutInputVertical();
		}

		// Token: 0x060005CD RID: 1485 RVA: 0x0001DCCB File Offset: 0x0001C0CB
		[CompilerGenerated]
		private static void <Rebuild>m__5(Component e)
		{
			(e as ILayoutController).SetLayoutVertical();
		}

		// Token: 0x040002BE RID: 702
		private RectTransform m_ToRebuild;

		// Token: 0x040002BF RID: 703
		private int m_CachedHashFromTransform;

		// Token: 0x040002C0 RID: 704
		private static ObjectPool<LayoutRebuilder> s_Rebuilders = new ObjectPool<LayoutRebuilder>(null, delegate(LayoutRebuilder x)
		{
			x.Clear();
		});

		// Token: 0x040002C1 RID: 705
		[CompilerGenerated]
		private static RectTransform.ReapplyDrivenProperties <>f__mg$cache0;

		// Token: 0x040002C2 RID: 706
		[CompilerGenerated]
		private static Predicate<Component> <>f__am$cache0;

		// Token: 0x040002C3 RID: 707
		[CompilerGenerated]
		private static UnityAction<Component> <>f__am$cache1;

		// Token: 0x040002C4 RID: 708
		[CompilerGenerated]
		private static UnityAction<Component> <>f__am$cache2;

		// Token: 0x040002C5 RID: 709
		[CompilerGenerated]
		private static UnityAction<Component> <>f__am$cache3;

		// Token: 0x040002C6 RID: 710
		[CompilerGenerated]
		private static UnityAction<Component> <>f__am$cache4;
	}
}
