﻿using System;
using System.Collections.Generic;

namespace UnityEngine.UI
{
	// Token: 0x020000B0 RID: 176
	[AddComponentMenu("UI/Effects/Outline", 15)]
	public class Outline : Shadow
	{
		// Token: 0x06000640 RID: 1600 RVA: 0x0001F5BB File Offset: 0x0001D9BB
		protected Outline()
		{
		}

		// Token: 0x06000641 RID: 1601 RVA: 0x0001F5C4 File Offset: 0x0001D9C4
		public override void ModifyMesh(VertexHelper vh)
		{
			if (this.IsActive())
			{
				List<UIVertex> list = ListPool<UIVertex>.Get();
				vh.GetUIVertexStream(list);
				int num = list.Count * 5;
				if (list.Capacity < num)
				{
					list.Capacity = num;
				}
				int start = 0;
				int count = list.Count;
				base.ApplyShadowZeroAlloc(list, base.effectColor, start, list.Count, base.effectDistance.x, base.effectDistance.y);
				start = count;
				count = list.Count;
				base.ApplyShadowZeroAlloc(list, base.effectColor, start, list.Count, base.effectDistance.x, -base.effectDistance.y);
				start = count;
				count = list.Count;
				base.ApplyShadowZeroAlloc(list, base.effectColor, start, list.Count, -base.effectDistance.x, base.effectDistance.y);
				start = count;
				count = list.Count;
				base.ApplyShadowZeroAlloc(list, base.effectColor, start, list.Count, -base.effectDistance.x, -base.effectDistance.y);
				vh.Clear();
				vh.AddUIVertexTriangleStream(list);
				ListPool<UIVertex>.Release(list);
			}
		}
	}
}
