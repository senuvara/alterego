﻿using System;
using System.Collections.Generic;

namespace UnityEngine.UI
{
	// Token: 0x0200007D RID: 125
	[AddComponentMenu("UI/Text", 10)]
	public class Text : MaskableGraphic, ILayoutElement
	{
		// Token: 0x060004AA RID: 1194 RVA: 0x00019D35 File Offset: 0x00018135
		protected Text()
		{
			base.useLegacyMeshGeneration = false;
		}

		// Token: 0x17000142 RID: 322
		// (get) Token: 0x060004AB RID: 1195 RVA: 0x00019D70 File Offset: 0x00018170
		public TextGenerator cachedTextGenerator
		{
			get
			{
				TextGenerator result;
				if ((result = this.m_TextCache) == null)
				{
					result = (this.m_TextCache = ((this.m_Text.Length == 0) ? new TextGenerator() : new TextGenerator(this.m_Text.Length)));
				}
				return result;
			}
		}

		// Token: 0x17000143 RID: 323
		// (get) Token: 0x060004AC RID: 1196 RVA: 0x00019DC8 File Offset: 0x000181C8
		public TextGenerator cachedTextGeneratorForLayout
		{
			get
			{
				TextGenerator result;
				if ((result = this.m_TextCacheForLayout) == null)
				{
					result = (this.m_TextCacheForLayout = new TextGenerator());
				}
				return result;
			}
		}

		// Token: 0x17000144 RID: 324
		// (get) Token: 0x060004AD RID: 1197 RVA: 0x00019DF8 File Offset: 0x000181F8
		public override Texture mainTexture
		{
			get
			{
				Texture mainTexture;
				if (this.font != null && this.font.material != null && this.font.material.mainTexture != null)
				{
					mainTexture = this.font.material.mainTexture;
				}
				else if (this.m_Material != null)
				{
					mainTexture = this.m_Material.mainTexture;
				}
				else
				{
					mainTexture = base.mainTexture;
				}
				return mainTexture;
			}
		}

		// Token: 0x060004AE RID: 1198 RVA: 0x00019E90 File Offset: 0x00018290
		public void FontTextureChanged()
		{
			if (this)
			{
				if (!this.m_DisableFontTextureRebuiltCallback)
				{
					this.cachedTextGenerator.Invalidate();
					if (this.IsActive())
					{
						if (CanvasUpdateRegistry.IsRebuildingGraphics() || CanvasUpdateRegistry.IsRebuildingLayout())
						{
							this.UpdateGeometry();
						}
						else
						{
							this.SetAllDirty();
						}
					}
				}
			}
		}

		// Token: 0x17000145 RID: 325
		// (get) Token: 0x060004AF RID: 1199 RVA: 0x00019F00 File Offset: 0x00018300
		// (set) Token: 0x060004B0 RID: 1200 RVA: 0x00019F20 File Offset: 0x00018320
		public Font font
		{
			get
			{
				return this.m_FontData.font;
			}
			set
			{
				if (!(this.m_FontData.font == value))
				{
					FontUpdateTracker.UntrackText(this);
					this.m_FontData.font = value;
					FontUpdateTracker.TrackText(this);
					this.SetAllDirty();
				}
			}
		}

		// Token: 0x17000146 RID: 326
		// (get) Token: 0x060004B1 RID: 1201 RVA: 0x00019F5C File Offset: 0x0001835C
		// (set) Token: 0x060004B2 RID: 1202 RVA: 0x00019F78 File Offset: 0x00018378
		public virtual string text
		{
			get
			{
				return this.m_Text;
			}
			set
			{
				if (string.IsNullOrEmpty(value))
				{
					if (!string.IsNullOrEmpty(this.m_Text))
					{
						this.m_Text = "";
						this.SetVerticesDirty();
					}
				}
				else if (this.m_Text != value)
				{
					this.m_Text = value;
					this.SetVerticesDirty();
					this.SetLayoutDirty();
				}
			}
		}

		// Token: 0x17000147 RID: 327
		// (get) Token: 0x060004B3 RID: 1203 RVA: 0x00019FE4 File Offset: 0x000183E4
		// (set) Token: 0x060004B4 RID: 1204 RVA: 0x0001A004 File Offset: 0x00018404
		public bool supportRichText
		{
			get
			{
				return this.m_FontData.richText;
			}
			set
			{
				if (this.m_FontData.richText != value)
				{
					this.m_FontData.richText = value;
					this.SetVerticesDirty();
					this.SetLayoutDirty();
				}
			}
		}

		// Token: 0x17000148 RID: 328
		// (get) Token: 0x060004B5 RID: 1205 RVA: 0x0001A038 File Offset: 0x00018438
		// (set) Token: 0x060004B6 RID: 1206 RVA: 0x0001A058 File Offset: 0x00018458
		public bool resizeTextForBestFit
		{
			get
			{
				return this.m_FontData.bestFit;
			}
			set
			{
				if (this.m_FontData.bestFit != value)
				{
					this.m_FontData.bestFit = value;
					this.SetVerticesDirty();
					this.SetLayoutDirty();
				}
			}
		}

		// Token: 0x17000149 RID: 329
		// (get) Token: 0x060004B7 RID: 1207 RVA: 0x0001A08C File Offset: 0x0001848C
		// (set) Token: 0x060004B8 RID: 1208 RVA: 0x0001A0AC File Offset: 0x000184AC
		public int resizeTextMinSize
		{
			get
			{
				return this.m_FontData.minSize;
			}
			set
			{
				if (this.m_FontData.minSize != value)
				{
					this.m_FontData.minSize = value;
					this.SetVerticesDirty();
					this.SetLayoutDirty();
				}
			}
		}

		// Token: 0x1700014A RID: 330
		// (get) Token: 0x060004B9 RID: 1209 RVA: 0x0001A0E0 File Offset: 0x000184E0
		// (set) Token: 0x060004BA RID: 1210 RVA: 0x0001A100 File Offset: 0x00018500
		public int resizeTextMaxSize
		{
			get
			{
				return this.m_FontData.maxSize;
			}
			set
			{
				if (this.m_FontData.maxSize != value)
				{
					this.m_FontData.maxSize = value;
					this.SetVerticesDirty();
					this.SetLayoutDirty();
				}
			}
		}

		// Token: 0x1700014B RID: 331
		// (get) Token: 0x060004BB RID: 1211 RVA: 0x0001A134 File Offset: 0x00018534
		// (set) Token: 0x060004BC RID: 1212 RVA: 0x0001A154 File Offset: 0x00018554
		public TextAnchor alignment
		{
			get
			{
				return this.m_FontData.alignment;
			}
			set
			{
				if (this.m_FontData.alignment != value)
				{
					this.m_FontData.alignment = value;
					this.SetVerticesDirty();
					this.SetLayoutDirty();
				}
			}
		}

		// Token: 0x1700014C RID: 332
		// (get) Token: 0x060004BD RID: 1213 RVA: 0x0001A188 File Offset: 0x00018588
		// (set) Token: 0x060004BE RID: 1214 RVA: 0x0001A1A8 File Offset: 0x000185A8
		public bool alignByGeometry
		{
			get
			{
				return this.m_FontData.alignByGeometry;
			}
			set
			{
				if (this.m_FontData.alignByGeometry != value)
				{
					this.m_FontData.alignByGeometry = value;
					this.SetVerticesDirty();
				}
			}
		}

		// Token: 0x1700014D RID: 333
		// (get) Token: 0x060004BF RID: 1215 RVA: 0x0001A1D4 File Offset: 0x000185D4
		// (set) Token: 0x060004C0 RID: 1216 RVA: 0x0001A1F4 File Offset: 0x000185F4
		public int fontSize
		{
			get
			{
				return this.m_FontData.fontSize;
			}
			set
			{
				if (this.m_FontData.fontSize != value)
				{
					this.m_FontData.fontSize = value;
					this.SetVerticesDirty();
					this.SetLayoutDirty();
				}
			}
		}

		// Token: 0x1700014E RID: 334
		// (get) Token: 0x060004C1 RID: 1217 RVA: 0x0001A228 File Offset: 0x00018628
		// (set) Token: 0x060004C2 RID: 1218 RVA: 0x0001A248 File Offset: 0x00018648
		public HorizontalWrapMode horizontalOverflow
		{
			get
			{
				return this.m_FontData.horizontalOverflow;
			}
			set
			{
				if (this.m_FontData.horizontalOverflow != value)
				{
					this.m_FontData.horizontalOverflow = value;
					this.SetVerticesDirty();
					this.SetLayoutDirty();
				}
			}
		}

		// Token: 0x1700014F RID: 335
		// (get) Token: 0x060004C3 RID: 1219 RVA: 0x0001A27C File Offset: 0x0001867C
		// (set) Token: 0x060004C4 RID: 1220 RVA: 0x0001A29C File Offset: 0x0001869C
		public VerticalWrapMode verticalOverflow
		{
			get
			{
				return this.m_FontData.verticalOverflow;
			}
			set
			{
				if (this.m_FontData.verticalOverflow != value)
				{
					this.m_FontData.verticalOverflow = value;
					this.SetVerticesDirty();
					this.SetLayoutDirty();
				}
			}
		}

		// Token: 0x17000150 RID: 336
		// (get) Token: 0x060004C5 RID: 1221 RVA: 0x0001A2D0 File Offset: 0x000186D0
		// (set) Token: 0x060004C6 RID: 1222 RVA: 0x0001A2F0 File Offset: 0x000186F0
		public float lineSpacing
		{
			get
			{
				return this.m_FontData.lineSpacing;
			}
			set
			{
				if (this.m_FontData.lineSpacing != value)
				{
					this.m_FontData.lineSpacing = value;
					this.SetVerticesDirty();
					this.SetLayoutDirty();
				}
			}
		}

		// Token: 0x17000151 RID: 337
		// (get) Token: 0x060004C7 RID: 1223 RVA: 0x0001A324 File Offset: 0x00018724
		// (set) Token: 0x060004C8 RID: 1224 RVA: 0x0001A344 File Offset: 0x00018744
		public FontStyle fontStyle
		{
			get
			{
				return this.m_FontData.fontStyle;
			}
			set
			{
				if (this.m_FontData.fontStyle != value)
				{
					this.m_FontData.fontStyle = value;
					this.SetVerticesDirty();
					this.SetLayoutDirty();
				}
			}
		}

		// Token: 0x17000152 RID: 338
		// (get) Token: 0x060004C9 RID: 1225 RVA: 0x0001A378 File Offset: 0x00018778
		public float pixelsPerUnit
		{
			get
			{
				Canvas canvas = base.canvas;
				float result;
				if (!canvas)
				{
					result = 1f;
				}
				else if (!this.font || this.font.dynamic)
				{
					result = canvas.scaleFactor;
				}
				else if (this.m_FontData.fontSize <= 0 || this.font.fontSize <= 0)
				{
					result = 1f;
				}
				else
				{
					result = (float)this.font.fontSize / (float)this.m_FontData.fontSize;
				}
				return result;
			}
		}

		// Token: 0x060004CA RID: 1226 RVA: 0x0001A41C File Offset: 0x0001881C
		protected override void OnEnable()
		{
			base.OnEnable();
			this.cachedTextGenerator.Invalidate();
			FontUpdateTracker.TrackText(this);
		}

		// Token: 0x060004CB RID: 1227 RVA: 0x0001A436 File Offset: 0x00018836
		protected override void OnDisable()
		{
			FontUpdateTracker.UntrackText(this);
			base.OnDisable();
		}

		// Token: 0x060004CC RID: 1228 RVA: 0x0001A445 File Offset: 0x00018845
		protected override void UpdateGeometry()
		{
			if (this.font != null)
			{
				base.UpdateGeometry();
			}
		}

		// Token: 0x060004CD RID: 1229 RVA: 0x0001A461 File Offset: 0x00018861
		internal void AssignDefaultFont()
		{
			this.font = Resources.GetBuiltinResource<Font>("Arial.ttf");
		}

		// Token: 0x060004CE RID: 1230 RVA: 0x0001A474 File Offset: 0x00018874
		public TextGenerationSettings GetGenerationSettings(Vector2 extents)
		{
			TextGenerationSettings result = default(TextGenerationSettings);
			result.generationExtents = extents;
			if (this.font != null && this.font.dynamic)
			{
				result.fontSize = this.m_FontData.fontSize;
				result.resizeTextMinSize = this.m_FontData.minSize;
				result.resizeTextMaxSize = this.m_FontData.maxSize;
			}
			result.textAnchor = this.m_FontData.alignment;
			result.alignByGeometry = this.m_FontData.alignByGeometry;
			result.scaleFactor = this.pixelsPerUnit;
			result.color = this.color;
			result.font = this.font;
			result.pivot = base.rectTransform.pivot;
			result.richText = this.m_FontData.richText;
			result.lineSpacing = this.m_FontData.lineSpacing;
			result.fontStyle = this.m_FontData.fontStyle;
			result.resizeTextForBestFit = this.m_FontData.bestFit;
			result.updateBounds = false;
			result.horizontalOverflow = this.m_FontData.horizontalOverflow;
			result.verticalOverflow = this.m_FontData.verticalOverflow;
			return result;
		}

		// Token: 0x060004CF RID: 1231 RVA: 0x0001A5C4 File Offset: 0x000189C4
		public static Vector2 GetTextAnchorPivot(TextAnchor anchor)
		{
			Vector2 result;
			switch (anchor)
			{
			case TextAnchor.UpperLeft:
				result = new Vector2(0f, 1f);
				break;
			case TextAnchor.UpperCenter:
				result = new Vector2(0.5f, 1f);
				break;
			case TextAnchor.UpperRight:
				result = new Vector2(1f, 1f);
				break;
			case TextAnchor.MiddleLeft:
				result = new Vector2(0f, 0.5f);
				break;
			case TextAnchor.MiddleCenter:
				result = new Vector2(0.5f, 0.5f);
				break;
			case TextAnchor.MiddleRight:
				result = new Vector2(1f, 0.5f);
				break;
			case TextAnchor.LowerLeft:
				result = new Vector2(0f, 0f);
				break;
			case TextAnchor.LowerCenter:
				result = new Vector2(0.5f, 0f);
				break;
			case TextAnchor.LowerRight:
				result = new Vector2(1f, 0f);
				break;
			default:
				result = Vector2.zero;
				break;
			}
			return result;
		}

		// Token: 0x060004D0 RID: 1232 RVA: 0x0001A6CC File Offset: 0x00018ACC
		protected override void OnPopulateMesh(VertexHelper toFill)
		{
			if (!(this.font == null))
			{
				this.m_DisableFontTextureRebuiltCallback = true;
				Vector2 size = base.rectTransform.rect.size;
				TextGenerationSettings generationSettings = this.GetGenerationSettings(size);
				this.cachedTextGenerator.PopulateWithErrors(this.text, generationSettings, base.gameObject);
				IList<UIVertex> verts = this.cachedTextGenerator.verts;
				float d = 1f / this.pixelsPerUnit;
				int num = verts.Count - 4;
				if (num <= 0)
				{
					toFill.Clear();
				}
				else
				{
					Vector2 vector = new Vector2(verts[0].position.x, verts[0].position.y) * d;
					vector = base.PixelAdjustPoint(vector) - vector;
					toFill.Clear();
					if (vector != Vector2.zero)
					{
						for (int i = 0; i < num; i++)
						{
							int num2 = i & 3;
							this.m_TempVerts[num2] = verts[i];
							UIVertex[] tempVerts = this.m_TempVerts;
							int num3 = num2;
							tempVerts[num3].position = tempVerts[num3].position * d;
							UIVertex[] tempVerts2 = this.m_TempVerts;
							int num4 = num2;
							tempVerts2[num4].position.x = tempVerts2[num4].position.x + vector.x;
							UIVertex[] tempVerts3 = this.m_TempVerts;
							int num5 = num2;
							tempVerts3[num5].position.y = tempVerts3[num5].position.y + vector.y;
							if (num2 == 3)
							{
								toFill.AddUIVertexQuad(this.m_TempVerts);
							}
						}
					}
					else
					{
						for (int j = 0; j < num; j++)
						{
							int num6 = j & 3;
							this.m_TempVerts[num6] = verts[j];
							UIVertex[] tempVerts4 = this.m_TempVerts;
							int num7 = num6;
							tempVerts4[num7].position = tempVerts4[num7].position * d;
							if (num6 == 3)
							{
								toFill.AddUIVertexQuad(this.m_TempVerts);
							}
						}
					}
					this.m_DisableFontTextureRebuiltCallback = false;
				}
			}
		}

		// Token: 0x060004D1 RID: 1233 RVA: 0x0001A8F6 File Offset: 0x00018CF6
		public virtual void CalculateLayoutInputHorizontal()
		{
		}

		// Token: 0x060004D2 RID: 1234 RVA: 0x0001A8F9 File Offset: 0x00018CF9
		public virtual void CalculateLayoutInputVertical()
		{
		}

		// Token: 0x17000153 RID: 339
		// (get) Token: 0x060004D3 RID: 1235 RVA: 0x0001A8FC File Offset: 0x00018CFC
		public virtual float minWidth
		{
			get
			{
				return 0f;
			}
		}

		// Token: 0x17000154 RID: 340
		// (get) Token: 0x060004D4 RID: 1236 RVA: 0x0001A918 File Offset: 0x00018D18
		public virtual float preferredWidth
		{
			get
			{
				TextGenerationSettings generationSettings = this.GetGenerationSettings(Vector2.zero);
				return this.cachedTextGeneratorForLayout.GetPreferredWidth(this.m_Text, generationSettings) / this.pixelsPerUnit;
			}
		}

		// Token: 0x17000155 RID: 341
		// (get) Token: 0x060004D5 RID: 1237 RVA: 0x0001A954 File Offset: 0x00018D54
		public virtual float flexibleWidth
		{
			get
			{
				return -1f;
			}
		}

		// Token: 0x17000156 RID: 342
		// (get) Token: 0x060004D6 RID: 1238 RVA: 0x0001A970 File Offset: 0x00018D70
		public virtual float minHeight
		{
			get
			{
				return 0f;
			}
		}

		// Token: 0x17000157 RID: 343
		// (get) Token: 0x060004D7 RID: 1239 RVA: 0x0001A98C File Offset: 0x00018D8C
		public virtual float preferredHeight
		{
			get
			{
				TextGenerationSettings generationSettings = this.GetGenerationSettings(new Vector2(base.GetPixelAdjustedRect().size.x, 0f));
				return this.cachedTextGeneratorForLayout.GetPreferredHeight(this.m_Text, generationSettings) / this.pixelsPerUnit;
			}
		}

		// Token: 0x17000158 RID: 344
		// (get) Token: 0x060004D8 RID: 1240 RVA: 0x0001A9E4 File Offset: 0x00018DE4
		public virtual float flexibleHeight
		{
			get
			{
				return -1f;
			}
		}

		// Token: 0x17000159 RID: 345
		// (get) Token: 0x060004D9 RID: 1241 RVA: 0x0001AA00 File Offset: 0x00018E00
		public virtual int layoutPriority
		{
			get
			{
				return 0;
			}
		}

		// Token: 0x060004DA RID: 1242 RVA: 0x0001AA16 File Offset: 0x00018E16
		// Note: this type is marked as 'beforefieldinit'.
		static Text()
		{
		}

		// Token: 0x04000251 RID: 593
		[SerializeField]
		private FontData m_FontData = FontData.defaultFontData;

		// Token: 0x04000252 RID: 594
		[TextArea(3, 10)]
		[SerializeField]
		protected string m_Text = string.Empty;

		// Token: 0x04000253 RID: 595
		private TextGenerator m_TextCache;

		// Token: 0x04000254 RID: 596
		private TextGenerator m_TextCacheForLayout;

		// Token: 0x04000255 RID: 597
		protected static Material s_DefaultText = null;

		// Token: 0x04000256 RID: 598
		[NonSerialized]
		protected bool m_DisableFontTextureRebuiltCallback = false;

		// Token: 0x04000257 RID: 599
		private readonly UIVertex[] m_TempVerts = new UIVertex[4];
	}
}
