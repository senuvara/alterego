﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI.CoroutineTween;

namespace UnityEngine.UI
{
	// Token: 0x02000042 RID: 66
	[AddComponentMenu("UI/Dropdown", 35)]
	[RequireComponent(typeof(RectTransform))]
	public class Dropdown : Selectable, IPointerClickHandler, ISubmitHandler, ICancelHandler, IEventSystemHandler
	{
		// Token: 0x060001C9 RID: 457 RVA: 0x00009C8F File Offset: 0x0000808F
		protected Dropdown()
		{
		}

		// Token: 0x17000074 RID: 116
		// (get) Token: 0x060001CA RID: 458 RVA: 0x00009CC0 File Offset: 0x000080C0
		// (set) Token: 0x060001CB RID: 459 RVA: 0x00009CDB File Offset: 0x000080DB
		public RectTransform template
		{
			get
			{
				return this.m_Template;
			}
			set
			{
				this.m_Template = value;
				this.RefreshShownValue();
			}
		}

		// Token: 0x17000075 RID: 117
		// (get) Token: 0x060001CC RID: 460 RVA: 0x00009CEC File Offset: 0x000080EC
		// (set) Token: 0x060001CD RID: 461 RVA: 0x00009D07 File Offset: 0x00008107
		public Text captionText
		{
			get
			{
				return this.m_CaptionText;
			}
			set
			{
				this.m_CaptionText = value;
				this.RefreshShownValue();
			}
		}

		// Token: 0x17000076 RID: 118
		// (get) Token: 0x060001CE RID: 462 RVA: 0x00009D18 File Offset: 0x00008118
		// (set) Token: 0x060001CF RID: 463 RVA: 0x00009D33 File Offset: 0x00008133
		public Image captionImage
		{
			get
			{
				return this.m_CaptionImage;
			}
			set
			{
				this.m_CaptionImage = value;
				this.RefreshShownValue();
			}
		}

		// Token: 0x17000077 RID: 119
		// (get) Token: 0x060001D0 RID: 464 RVA: 0x00009D44 File Offset: 0x00008144
		// (set) Token: 0x060001D1 RID: 465 RVA: 0x00009D5F File Offset: 0x0000815F
		public Text itemText
		{
			get
			{
				return this.m_ItemText;
			}
			set
			{
				this.m_ItemText = value;
				this.RefreshShownValue();
			}
		}

		// Token: 0x17000078 RID: 120
		// (get) Token: 0x060001D2 RID: 466 RVA: 0x00009D70 File Offset: 0x00008170
		// (set) Token: 0x060001D3 RID: 467 RVA: 0x00009D8B File Offset: 0x0000818B
		public Image itemImage
		{
			get
			{
				return this.m_ItemImage;
			}
			set
			{
				this.m_ItemImage = value;
				this.RefreshShownValue();
			}
		}

		// Token: 0x17000079 RID: 121
		// (get) Token: 0x060001D4 RID: 468 RVA: 0x00009D9C File Offset: 0x0000819C
		// (set) Token: 0x060001D5 RID: 469 RVA: 0x00009DBC File Offset: 0x000081BC
		public List<Dropdown.OptionData> options
		{
			get
			{
				return this.m_Options.options;
			}
			set
			{
				this.m_Options.options = value;
				this.RefreshShownValue();
			}
		}

		// Token: 0x1700007A RID: 122
		// (get) Token: 0x060001D6 RID: 470 RVA: 0x00009DD4 File Offset: 0x000081D4
		// (set) Token: 0x060001D7 RID: 471 RVA: 0x00009DEF File Offset: 0x000081EF
		public Dropdown.DropdownEvent onValueChanged
		{
			get
			{
				return this.m_OnValueChanged;
			}
			set
			{
				this.m_OnValueChanged = value;
			}
		}

		// Token: 0x1700007B RID: 123
		// (get) Token: 0x060001D8 RID: 472 RVA: 0x00009DFC File Offset: 0x000081FC
		// (set) Token: 0x060001D9 RID: 473 RVA: 0x00009E18 File Offset: 0x00008218
		public int value
		{
			get
			{
				return this.m_Value;
			}
			set
			{
				if (!Application.isPlaying || (value != this.m_Value && this.options.Count != 0))
				{
					this.m_Value = Mathf.Clamp(value, 0, this.options.Count - 1);
					this.RefreshShownValue();
					UISystemProfilerApi.AddMarker("Dropdown.value", this);
					this.m_OnValueChanged.Invoke(this.m_Value);
				}
			}
		}

		// Token: 0x060001DA RID: 474 RVA: 0x00009E90 File Offset: 0x00008290
		protected override void Awake()
		{
			this.m_AlphaTweenRunner = new TweenRunner<FloatTween>();
			this.m_AlphaTweenRunner.Init(this);
			if (this.m_CaptionImage)
			{
				this.m_CaptionImage.enabled = (this.m_CaptionImage.sprite != null);
			}
			if (this.m_Template)
			{
				this.m_Template.gameObject.SetActive(false);
			}
		}

		// Token: 0x060001DB RID: 475 RVA: 0x00009F02 File Offset: 0x00008302
		protected override void Start()
		{
			base.Start();
			this.RefreshShownValue();
		}

		// Token: 0x060001DC RID: 476 RVA: 0x00009F14 File Offset: 0x00008314
		public void RefreshShownValue()
		{
			Dropdown.OptionData optionData = Dropdown.s_NoOptionData;
			if (this.options.Count > 0)
			{
				optionData = this.options[Mathf.Clamp(this.m_Value, 0, this.options.Count - 1)];
			}
			if (this.m_CaptionText)
			{
				if (optionData != null && optionData.text != null)
				{
					this.m_CaptionText.text = optionData.text;
				}
				else
				{
					this.m_CaptionText.text = "";
				}
			}
			if (this.m_CaptionImage)
			{
				if (optionData != null)
				{
					this.m_CaptionImage.sprite = optionData.image;
				}
				else
				{
					this.m_CaptionImage.sprite = null;
				}
				this.m_CaptionImage.enabled = (this.m_CaptionImage.sprite != null);
			}
		}

		// Token: 0x060001DD RID: 477 RVA: 0x00009FFD File Offset: 0x000083FD
		public void AddOptions(List<Dropdown.OptionData> options)
		{
			this.options.AddRange(options);
			this.RefreshShownValue();
		}

		// Token: 0x060001DE RID: 478 RVA: 0x0000A014 File Offset: 0x00008414
		public void AddOptions(List<string> options)
		{
			for (int i = 0; i < options.Count; i++)
			{
				this.options.Add(new Dropdown.OptionData(options[i]));
			}
			this.RefreshShownValue();
		}

		// Token: 0x060001DF RID: 479 RVA: 0x0000A058 File Offset: 0x00008458
		public void AddOptions(List<Sprite> options)
		{
			for (int i = 0; i < options.Count; i++)
			{
				this.options.Add(new Dropdown.OptionData(options[i]));
			}
			this.RefreshShownValue();
		}

		// Token: 0x060001E0 RID: 480 RVA: 0x0000A09A File Offset: 0x0000849A
		public void ClearOptions()
		{
			this.options.Clear();
			this.RefreshShownValue();
		}

		// Token: 0x060001E1 RID: 481 RVA: 0x0000A0B0 File Offset: 0x000084B0
		private void SetupTemplate()
		{
			this.validTemplate = false;
			if (!this.m_Template)
			{
				Debug.LogError("The dropdown template is not assigned. The template needs to be assigned and must have a child GameObject with a Toggle component serving as the item.", this);
			}
			else
			{
				GameObject gameObject = this.m_Template.gameObject;
				gameObject.SetActive(true);
				Toggle componentInChildren = this.m_Template.GetComponentInChildren<Toggle>();
				this.validTemplate = true;
				if (!componentInChildren || componentInChildren.transform == this.template)
				{
					this.validTemplate = false;
					Debug.LogError("The dropdown template is not valid. The template must have a child GameObject with a Toggle component serving as the item.", this.template);
				}
				else if (!(componentInChildren.transform.parent is RectTransform))
				{
					this.validTemplate = false;
					Debug.LogError("The dropdown template is not valid. The child GameObject with a Toggle component (the item) must have a RectTransform on its parent.", this.template);
				}
				else if (this.itemText != null && !this.itemText.transform.IsChildOf(componentInChildren.transform))
				{
					this.validTemplate = false;
					Debug.LogError("The dropdown template is not valid. The Item Text must be on the item GameObject or children of it.", this.template);
				}
				else if (this.itemImage != null && !this.itemImage.transform.IsChildOf(componentInChildren.transform))
				{
					this.validTemplate = false;
					Debug.LogError("The dropdown template is not valid. The Item Image must be on the item GameObject or children of it.", this.template);
				}
				if (!this.validTemplate)
				{
					gameObject.SetActive(false);
				}
				else
				{
					Dropdown.DropdownItem dropdownItem = componentInChildren.gameObject.AddComponent<Dropdown.DropdownItem>();
					dropdownItem.text = this.m_ItemText;
					dropdownItem.image = this.m_ItemImage;
					dropdownItem.toggle = componentInChildren;
					dropdownItem.rectTransform = (RectTransform)componentInChildren.transform;
					Canvas orAddComponent = Dropdown.GetOrAddComponent<Canvas>(gameObject);
					orAddComponent.overrideSorting = true;
					orAddComponent.sortingOrder = 30000;
					Dropdown.GetOrAddComponent<GraphicRaycaster>(gameObject);
					Dropdown.GetOrAddComponent<CanvasGroup>(gameObject);
					gameObject.SetActive(false);
					this.validTemplate = true;
				}
			}
		}

		// Token: 0x060001E2 RID: 482 RVA: 0x0000A298 File Offset: 0x00008698
		private static T GetOrAddComponent<T>(GameObject go) where T : Component
		{
			T t = go.GetComponent<T>();
			if (!t)
			{
				t = go.AddComponent<T>();
			}
			return t;
		}

		// Token: 0x060001E3 RID: 483 RVA: 0x0000A2CC File Offset: 0x000086CC
		public virtual void OnPointerClick(PointerEventData eventData)
		{
			this.Show();
		}

		// Token: 0x060001E4 RID: 484 RVA: 0x0000A2D5 File Offset: 0x000086D5
		public virtual void OnSubmit(BaseEventData eventData)
		{
			this.Show();
		}

		// Token: 0x060001E5 RID: 485 RVA: 0x0000A2DE File Offset: 0x000086DE
		public virtual void OnCancel(BaseEventData eventData)
		{
			this.Hide();
		}

		// Token: 0x060001E6 RID: 486 RVA: 0x0000A2E8 File Offset: 0x000086E8
		public void Show()
		{
			if (this.IsActive() && this.IsInteractable() && !(this.m_Dropdown != null))
			{
				if (!this.validTemplate)
				{
					this.SetupTemplate();
					if (!this.validTemplate)
					{
						return;
					}
				}
				List<Canvas> list = ListPool<Canvas>.Get();
				base.gameObject.GetComponentsInParent<Canvas>(false, list);
				if (list.Count != 0)
				{
					Canvas canvas = list[0];
					ListPool<Canvas>.Release(list);
					this.m_Template.gameObject.SetActive(true);
					this.m_Dropdown = this.CreateDropdownList(this.m_Template.gameObject);
					this.m_Dropdown.name = "Dropdown List";
					this.m_Dropdown.SetActive(true);
					RectTransform rectTransform = this.m_Dropdown.transform as RectTransform;
					rectTransform.SetParent(this.m_Template.transform.parent, false);
					Dropdown.DropdownItem componentInChildren = this.m_Dropdown.GetComponentInChildren<Dropdown.DropdownItem>();
					GameObject gameObject = componentInChildren.rectTransform.parent.gameObject;
					RectTransform rectTransform2 = gameObject.transform as RectTransform;
					componentInChildren.rectTransform.gameObject.SetActive(true);
					Rect rect = rectTransform2.rect;
					Rect rect2 = componentInChildren.rectTransform.rect;
					Vector2 vector = rect2.min - rect.min + componentInChildren.rectTransform.localPosition;
					Vector2 vector2 = rect2.max - rect.max + componentInChildren.rectTransform.localPosition;
					Vector2 size = rect2.size;
					this.m_Items.Clear();
					Toggle toggle = null;
					for (int i = 0; i < this.options.Count; i++)
					{
						Dropdown.OptionData data = this.options[i];
						Dropdown.DropdownItem item = this.AddItem(data, this.value == i, componentInChildren, this.m_Items);
						if (!(item == null))
						{
							item.toggle.isOn = (this.value == i);
							item.toggle.onValueChanged.AddListener(delegate(bool x)
							{
								this.OnSelectItem(item.toggle);
							});
							if (item.toggle.isOn)
							{
								item.toggle.Select();
							}
							if (toggle != null)
							{
								Navigation navigation = toggle.navigation;
								Navigation navigation2 = item.toggle.navigation;
								navigation.mode = Navigation.Mode.Explicit;
								navigation2.mode = Navigation.Mode.Explicit;
								navigation.selectOnDown = item.toggle;
								navigation.selectOnRight = item.toggle;
								navigation2.selectOnLeft = toggle;
								navigation2.selectOnUp = toggle;
								toggle.navigation = navigation;
								item.toggle.navigation = navigation2;
							}
							toggle = item.toggle;
						}
					}
					Vector2 sizeDelta = rectTransform2.sizeDelta;
					sizeDelta.y = size.y * (float)this.m_Items.Count + vector.y - vector2.y;
					rectTransform2.sizeDelta = sizeDelta;
					float num = rectTransform.rect.height - rectTransform2.rect.height;
					if (num > 0f)
					{
						rectTransform.sizeDelta = new Vector2(rectTransform.sizeDelta.x, rectTransform.sizeDelta.y - num);
					}
					Vector3[] array = new Vector3[4];
					rectTransform.GetWorldCorners(array);
					RectTransform rectTransform3 = canvas.transform as RectTransform;
					Rect rect3 = rectTransform3.rect;
					for (int j = 0; j < 2; j++)
					{
						bool flag = false;
						for (int k = 0; k < 4; k++)
						{
							Vector3 vector3 = rectTransform3.InverseTransformPoint(array[k]);
							if (vector3[j] < rect3.min[j] || vector3[j] > rect3.max[j])
							{
								flag = true;
								break;
							}
						}
						if (flag)
						{
							RectTransformUtility.FlipLayoutOnAxis(rectTransform, j, false, false);
						}
					}
					for (int l = 0; l < this.m_Items.Count; l++)
					{
						RectTransform rectTransform4 = this.m_Items[l].rectTransform;
						rectTransform4.anchorMin = new Vector2(rectTransform4.anchorMin.x, 0f);
						rectTransform4.anchorMax = new Vector2(rectTransform4.anchorMax.x, 0f);
						rectTransform4.anchoredPosition = new Vector2(rectTransform4.anchoredPosition.x, vector.y + size.y * (float)(this.m_Items.Count - 1 - l) + size.y * rectTransform4.pivot.y);
						rectTransform4.sizeDelta = new Vector2(rectTransform4.sizeDelta.x, size.y);
					}
					this.AlphaFadeList(0.15f, 0f, 1f);
					this.m_Template.gameObject.SetActive(false);
					componentInChildren.gameObject.SetActive(false);
					this.m_Blocker = this.CreateBlocker(canvas);
				}
			}
		}

		// Token: 0x060001E7 RID: 487 RVA: 0x0000A8AC File Offset: 0x00008CAC
		protected virtual GameObject CreateBlocker(Canvas rootCanvas)
		{
			GameObject gameObject = new GameObject("Blocker");
			RectTransform rectTransform = gameObject.AddComponent<RectTransform>();
			rectTransform.SetParent(rootCanvas.transform, false);
			rectTransform.anchorMin = Vector3.zero;
			rectTransform.anchorMax = Vector3.one;
			rectTransform.sizeDelta = Vector2.zero;
			Canvas canvas = gameObject.AddComponent<Canvas>();
			canvas.overrideSorting = true;
			Canvas component = this.m_Dropdown.GetComponent<Canvas>();
			canvas.sortingLayerID = component.sortingLayerID;
			canvas.sortingOrder = component.sortingOrder - 1;
			gameObject.AddComponent<GraphicRaycaster>();
			Image image = gameObject.AddComponent<Image>();
			image.color = Color.clear;
			Button button = gameObject.AddComponent<Button>();
			button.onClick.AddListener(new UnityAction(this.Hide));
			return gameObject;
		}

		// Token: 0x060001E8 RID: 488 RVA: 0x0000A97D File Offset: 0x00008D7D
		protected virtual void DestroyBlocker(GameObject blocker)
		{
			Object.Destroy(blocker);
		}

		// Token: 0x060001E9 RID: 489 RVA: 0x0000A988 File Offset: 0x00008D88
		protected virtual GameObject CreateDropdownList(GameObject template)
		{
			return Object.Instantiate<GameObject>(template);
		}

		// Token: 0x060001EA RID: 490 RVA: 0x0000A9A3 File Offset: 0x00008DA3
		protected virtual void DestroyDropdownList(GameObject dropdownList)
		{
			Object.Destroy(dropdownList);
		}

		// Token: 0x060001EB RID: 491 RVA: 0x0000A9AC File Offset: 0x00008DAC
		protected virtual Dropdown.DropdownItem CreateItem(Dropdown.DropdownItem itemTemplate)
		{
			return Object.Instantiate<Dropdown.DropdownItem>(itemTemplate);
		}

		// Token: 0x060001EC RID: 492 RVA: 0x0000A9C7 File Offset: 0x00008DC7
		protected virtual void DestroyItem(Dropdown.DropdownItem item)
		{
		}

		// Token: 0x060001ED RID: 493 RVA: 0x0000A9CC File Offset: 0x00008DCC
		private Dropdown.DropdownItem AddItem(Dropdown.OptionData data, bool selected, Dropdown.DropdownItem itemTemplate, List<Dropdown.DropdownItem> items)
		{
			Dropdown.DropdownItem dropdownItem = this.CreateItem(itemTemplate);
			dropdownItem.rectTransform.SetParent(itemTemplate.rectTransform.parent, false);
			dropdownItem.gameObject.SetActive(true);
			dropdownItem.gameObject.name = "Item " + items.Count + ((data.text == null) ? "" : (": " + data.text));
			if (dropdownItem.toggle != null)
			{
				dropdownItem.toggle.isOn = false;
			}
			if (dropdownItem.text)
			{
				dropdownItem.text.text = data.text;
			}
			if (dropdownItem.image)
			{
				dropdownItem.image.sprite = data.image;
				dropdownItem.image.enabled = (dropdownItem.image.sprite != null);
			}
			items.Add(dropdownItem);
			return dropdownItem;
		}

		// Token: 0x060001EE RID: 494 RVA: 0x0000AADC File Offset: 0x00008EDC
		private void AlphaFadeList(float duration, float alpha)
		{
			CanvasGroup component = this.m_Dropdown.GetComponent<CanvasGroup>();
			this.AlphaFadeList(duration, component.alpha, alpha);
		}

		// Token: 0x060001EF RID: 495 RVA: 0x0000AB04 File Offset: 0x00008F04
		private void AlphaFadeList(float duration, float start, float end)
		{
			if (!end.Equals(start))
			{
				FloatTween info = new FloatTween
				{
					duration = duration,
					startValue = start,
					targetValue = end
				};
				info.AddOnChangedCallback(new UnityAction<float>(this.SetAlpha));
				info.ignoreTimeScale = true;
				this.m_AlphaTweenRunner.StartTween(info);
			}
		}

		// Token: 0x060001F0 RID: 496 RVA: 0x0000AB70 File Offset: 0x00008F70
		private void SetAlpha(float alpha)
		{
			if (this.m_Dropdown)
			{
				CanvasGroup component = this.m_Dropdown.GetComponent<CanvasGroup>();
				component.alpha = alpha;
			}
		}

		// Token: 0x060001F1 RID: 497 RVA: 0x0000ABA8 File Offset: 0x00008FA8
		public void Hide()
		{
			if (this.m_Dropdown != null)
			{
				this.AlphaFadeList(0.15f, 0f);
				if (this.IsActive())
				{
					base.StartCoroutine(this.DelayedDestroyDropdownList(0.15f));
				}
			}
			if (this.m_Blocker != null)
			{
				this.DestroyBlocker(this.m_Blocker);
			}
			this.m_Blocker = null;
			this.Select();
		}

		// Token: 0x060001F2 RID: 498 RVA: 0x0000AC20 File Offset: 0x00009020
		private IEnumerator DelayedDestroyDropdownList(float delay)
		{
			yield return new WaitForSecondsRealtime(delay);
			for (int i = 0; i < this.m_Items.Count; i++)
			{
				if (this.m_Items[i] != null)
				{
					this.DestroyItem(this.m_Items[i]);
				}
			}
			this.m_Items.Clear();
			if (this.m_Dropdown != null)
			{
				this.DestroyDropdownList(this.m_Dropdown);
			}
			this.m_Dropdown = null;
			yield break;
		}

		// Token: 0x060001F3 RID: 499 RVA: 0x0000AC4C File Offset: 0x0000904C
		private void OnSelectItem(Toggle toggle)
		{
			if (!toggle.isOn)
			{
				toggle.isOn = true;
			}
			int num = -1;
			Transform transform = toggle.transform;
			Transform parent = transform.parent;
			for (int i = 0; i < parent.childCount; i++)
			{
				if (parent.GetChild(i) == transform)
				{
					num = i - 1;
					break;
				}
			}
			if (num >= 0)
			{
				this.value = num;
				this.Hide();
			}
		}

		// Token: 0x060001F4 RID: 500 RVA: 0x0000ACCA File Offset: 0x000090CA
		// Note: this type is marked as 'beforefieldinit'.
		static Dropdown()
		{
		}

		// Token: 0x040000E9 RID: 233
		[SerializeField]
		private RectTransform m_Template;

		// Token: 0x040000EA RID: 234
		[SerializeField]
		private Text m_CaptionText;

		// Token: 0x040000EB RID: 235
		[SerializeField]
		private Image m_CaptionImage;

		// Token: 0x040000EC RID: 236
		[Space]
		[SerializeField]
		private Text m_ItemText;

		// Token: 0x040000ED RID: 237
		[SerializeField]
		private Image m_ItemImage;

		// Token: 0x040000EE RID: 238
		[Space]
		[SerializeField]
		private int m_Value;

		// Token: 0x040000EF RID: 239
		[Space]
		[SerializeField]
		private Dropdown.OptionDataList m_Options = new Dropdown.OptionDataList();

		// Token: 0x040000F0 RID: 240
		[Space]
		[SerializeField]
		private Dropdown.DropdownEvent m_OnValueChanged = new Dropdown.DropdownEvent();

		// Token: 0x040000F1 RID: 241
		private GameObject m_Dropdown;

		// Token: 0x040000F2 RID: 242
		private GameObject m_Blocker;

		// Token: 0x040000F3 RID: 243
		private List<Dropdown.DropdownItem> m_Items = new List<Dropdown.DropdownItem>();

		// Token: 0x040000F4 RID: 244
		private TweenRunner<FloatTween> m_AlphaTweenRunner;

		// Token: 0x040000F5 RID: 245
		private bool validTemplate = false;

		// Token: 0x040000F6 RID: 246
		private static Dropdown.OptionData s_NoOptionData = new Dropdown.OptionData();

		// Token: 0x02000043 RID: 67
		protected internal class DropdownItem : MonoBehaviour, IPointerEnterHandler, ICancelHandler, IEventSystemHandler
		{
			// Token: 0x060001F5 RID: 501 RVA: 0x0000ACD6 File Offset: 0x000090D6
			public DropdownItem()
			{
			}

			// Token: 0x1700007C RID: 124
			// (get) Token: 0x060001F6 RID: 502 RVA: 0x0000ACE0 File Offset: 0x000090E0
			// (set) Token: 0x060001F7 RID: 503 RVA: 0x0000ACFB File Offset: 0x000090FB
			public Text text
			{
				get
				{
					return this.m_Text;
				}
				set
				{
					this.m_Text = value;
				}
			}

			// Token: 0x1700007D RID: 125
			// (get) Token: 0x060001F8 RID: 504 RVA: 0x0000AD08 File Offset: 0x00009108
			// (set) Token: 0x060001F9 RID: 505 RVA: 0x0000AD23 File Offset: 0x00009123
			public Image image
			{
				get
				{
					return this.m_Image;
				}
				set
				{
					this.m_Image = value;
				}
			}

			// Token: 0x1700007E RID: 126
			// (get) Token: 0x060001FA RID: 506 RVA: 0x0000AD30 File Offset: 0x00009130
			// (set) Token: 0x060001FB RID: 507 RVA: 0x0000AD4B File Offset: 0x0000914B
			public RectTransform rectTransform
			{
				get
				{
					return this.m_RectTransform;
				}
				set
				{
					this.m_RectTransform = value;
				}
			}

			// Token: 0x1700007F RID: 127
			// (get) Token: 0x060001FC RID: 508 RVA: 0x0000AD58 File Offset: 0x00009158
			// (set) Token: 0x060001FD RID: 509 RVA: 0x0000AD73 File Offset: 0x00009173
			public Toggle toggle
			{
				get
				{
					return this.m_Toggle;
				}
				set
				{
					this.m_Toggle = value;
				}
			}

			// Token: 0x060001FE RID: 510 RVA: 0x0000AD7D File Offset: 0x0000917D
			public virtual void OnPointerEnter(PointerEventData eventData)
			{
				EventSystem.current.SetSelectedGameObject(base.gameObject);
			}

			// Token: 0x060001FF RID: 511 RVA: 0x0000AD90 File Offset: 0x00009190
			public virtual void OnCancel(BaseEventData eventData)
			{
				Dropdown componentInParent = base.GetComponentInParent<Dropdown>();
				if (componentInParent)
				{
					componentInParent.Hide();
				}
			}

			// Token: 0x040000F7 RID: 247
			[SerializeField]
			private Text m_Text;

			// Token: 0x040000F8 RID: 248
			[SerializeField]
			private Image m_Image;

			// Token: 0x040000F9 RID: 249
			[SerializeField]
			private RectTransform m_RectTransform;

			// Token: 0x040000FA RID: 250
			[SerializeField]
			private Toggle m_Toggle;
		}

		// Token: 0x02000044 RID: 68
		[Serializable]
		public class OptionData
		{
			// Token: 0x06000200 RID: 512 RVA: 0x0000ADB6 File Offset: 0x000091B6
			public OptionData()
			{
			}

			// Token: 0x06000201 RID: 513 RVA: 0x0000ADBF File Offset: 0x000091BF
			public OptionData(string text)
			{
				this.text = text;
			}

			// Token: 0x06000202 RID: 514 RVA: 0x0000ADCF File Offset: 0x000091CF
			public OptionData(Sprite image)
			{
				this.image = image;
			}

			// Token: 0x06000203 RID: 515 RVA: 0x0000ADDF File Offset: 0x000091DF
			public OptionData(string text, Sprite image)
			{
				this.text = text;
				this.image = image;
			}

			// Token: 0x17000080 RID: 128
			// (get) Token: 0x06000204 RID: 516 RVA: 0x0000ADF8 File Offset: 0x000091F8
			// (set) Token: 0x06000205 RID: 517 RVA: 0x0000AE13 File Offset: 0x00009213
			public string text
			{
				get
				{
					return this.m_Text;
				}
				set
				{
					this.m_Text = value;
				}
			}

			// Token: 0x17000081 RID: 129
			// (get) Token: 0x06000206 RID: 518 RVA: 0x0000AE20 File Offset: 0x00009220
			// (set) Token: 0x06000207 RID: 519 RVA: 0x0000AE3B File Offset: 0x0000923B
			public Sprite image
			{
				get
				{
					return this.m_Image;
				}
				set
				{
					this.m_Image = value;
				}
			}

			// Token: 0x040000FB RID: 251
			[SerializeField]
			private string m_Text;

			// Token: 0x040000FC RID: 252
			[SerializeField]
			private Sprite m_Image;
		}

		// Token: 0x02000045 RID: 69
		[Serializable]
		public class OptionDataList
		{
			// Token: 0x06000208 RID: 520 RVA: 0x0000AE45 File Offset: 0x00009245
			public OptionDataList()
			{
				this.options = new List<Dropdown.OptionData>();
			}

			// Token: 0x17000082 RID: 130
			// (get) Token: 0x06000209 RID: 521 RVA: 0x0000AE5C File Offset: 0x0000925C
			// (set) Token: 0x0600020A RID: 522 RVA: 0x0000AE77 File Offset: 0x00009277
			public List<Dropdown.OptionData> options
			{
				get
				{
					return this.m_Options;
				}
				set
				{
					this.m_Options = value;
				}
			}

			// Token: 0x040000FD RID: 253
			[SerializeField]
			private List<Dropdown.OptionData> m_Options;
		}

		// Token: 0x02000046 RID: 70
		[Serializable]
		public class DropdownEvent : UnityEvent<int>
		{
			// Token: 0x0600020B RID: 523 RVA: 0x0000AE81 File Offset: 0x00009281
			public DropdownEvent()
			{
			}
		}

		// Token: 0x020000B5 RID: 181
		[CompilerGenerated]
		private sealed class <Show>c__AnonStorey1
		{
			// Token: 0x0600065A RID: 1626 RVA: 0x0000AE89 File Offset: 0x00009289
			public <Show>c__AnonStorey1()
			{
			}

			// Token: 0x0600065B RID: 1627 RVA: 0x0000AE91 File Offset: 0x00009291
			internal void <>m__0(bool x)
			{
				this.$this.OnSelectItem(this.item.toggle);
			}

			// Token: 0x040002FB RID: 763
			internal Dropdown.DropdownItem item;

			// Token: 0x040002FC RID: 764
			internal Dropdown $this;
		}

		// Token: 0x020000B6 RID: 182
		[CompilerGenerated]
		private sealed class <DelayedDestroyDropdownList>c__Iterator0 : IEnumerator, IDisposable, IEnumerator<object>
		{
			// Token: 0x0600065C RID: 1628 RVA: 0x0000AEA9 File Offset: 0x000092A9
			[DebuggerHidden]
			public <DelayedDestroyDropdownList>c__Iterator0()
			{
			}

			// Token: 0x0600065D RID: 1629 RVA: 0x0000AEB4 File Offset: 0x000092B4
			public bool MoveNext()
			{
				uint num = (uint)this.$PC;
				this.$PC = -1;
				switch (num)
				{
				case 0U:
					this.$current = new WaitForSecondsRealtime(delay);
					if (!this.$disposing)
					{
						this.$PC = 1;
					}
					return true;
				case 1U:
					for (int i = 0; i < this.m_Items.Count; i++)
					{
						if (this.m_Items[i] != null)
						{
							this.DestroyItem(this.m_Items[i]);
						}
					}
					this.m_Items.Clear();
					if (this.m_Dropdown != null)
					{
						this.DestroyDropdownList(this.m_Dropdown);
					}
					this.m_Dropdown = null;
					this.$PC = -1;
					break;
				}
				return false;
			}

			// Token: 0x170001A9 RID: 425
			// (get) Token: 0x0600065E RID: 1630 RVA: 0x0000AFB8 File Offset: 0x000093B8
			object IEnumerator<object>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.$current;
				}
			}

			// Token: 0x170001AA RID: 426
			// (get) Token: 0x0600065F RID: 1631 RVA: 0x0000AFD4 File Offset: 0x000093D4
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.$current;
				}
			}

			// Token: 0x06000660 RID: 1632 RVA: 0x0000AFEE File Offset: 0x000093EE
			[DebuggerHidden]
			public void Dispose()
			{
				this.$disposing = true;
				this.$PC = -1;
			}

			// Token: 0x06000661 RID: 1633 RVA: 0x0000AFFE File Offset: 0x000093FE
			[DebuggerHidden]
			public void Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x040002FD RID: 765
			internal float delay;

			// Token: 0x040002FE RID: 766
			internal Dropdown $this;

			// Token: 0x040002FF RID: 767
			internal object $current;

			// Token: 0x04000300 RID: 768
			internal bool $disposing;

			// Token: 0x04000301 RID: 769
			internal int $PC;
		}
	}
}
