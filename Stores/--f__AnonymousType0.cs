﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;

// Token: 0x02000002 RID: 2
[DebuggerDisplay("\\{ product = {product}, metadata = {metadata} }", Type = "<Anonymous Type>")]
[CompilerGenerated]
internal sealed class <>f__AnonymousType0<<product>j__TPar, <metadata>j__TPar>
{
	// Token: 0x17000001 RID: 1
	// (get) Token: 0x06000001 RID: 1 RVA: 0x00002050 File Offset: 0x00000250
	public <product>j__TPar product
	{
		get
		{
			return this.<product>i__Field;
		}
	}

	// Token: 0x17000002 RID: 2
	// (get) Token: 0x06000002 RID: 2 RVA: 0x00002058 File Offset: 0x00000258
	public <metadata>j__TPar metadata
	{
		get
		{
			return this.<metadata>i__Field;
		}
	}

	// Token: 0x06000003 RID: 3 RVA: 0x00002060 File Offset: 0x00000260
	[DebuggerHidden]
	public <>f__AnonymousType0(<product>j__TPar product, <metadata>j__TPar metadata)
	{
		this.<product>i__Field = product;
		this.<metadata>i__Field = metadata;
	}

	// Token: 0x06000004 RID: 4 RVA: 0x00002078 File Offset: 0x00000278
	[DebuggerHidden]
	public override bool Equals(object value)
	{
		var <>f__AnonymousType = value as <>f__AnonymousType0<<product>j__TPar, <metadata>j__TPar>;
		return <>f__AnonymousType != null && EqualityComparer<<product>j__TPar>.Default.Equals(this.<product>i__Field, <>f__AnonymousType.<product>i__Field) && EqualityComparer<<metadata>j__TPar>.Default.Equals(this.<metadata>i__Field, <>f__AnonymousType.<metadata>i__Field);
	}

	// Token: 0x06000005 RID: 5 RVA: 0x000020C0 File Offset: 0x000002C0
	[DebuggerHidden]
	public override int GetHashCode()
	{
		return (1514440194 * -1521134295 + EqualityComparer<<product>j__TPar>.Default.GetHashCode(this.<product>i__Field)) * -1521134295 + EqualityComparer<<metadata>j__TPar>.Default.GetHashCode(this.<metadata>i__Field);
	}

	// Token: 0x06000006 RID: 6 RVA: 0x000020F8 File Offset: 0x000002F8
	[DebuggerHidden]
	public override string ToString()
	{
		IFormatProvider provider = null;
		string format = "{{ product = {0}, metadata = {1} }}";
		object[] array = new object[2];
		int num = 0;
		<product>j__TPar <product>j__TPar = this.<product>i__Field;
		ref <product>j__TPar ptr = ref <product>j__TPar;
		<product>j__TPar <product>j__TPar2 = default(<product>j__TPar);
		object obj;
		if (<product>j__TPar2 == null)
		{
			<product>j__TPar2 = <product>j__TPar;
			ptr = ref <product>j__TPar2;
			if (<product>j__TPar2 == null)
			{
				obj = null;
				goto IL_46;
			}
		}
		obj = ptr.ToString();
		IL_46:
		array[num] = obj;
		int num2 = 1;
		<metadata>j__TPar <metadata>j__TPar = this.<metadata>i__Field;
		ref <metadata>j__TPar ptr2 = ref <metadata>j__TPar;
		<metadata>j__TPar <metadata>j__TPar2 = default(<metadata>j__TPar);
		object obj2;
		if (<metadata>j__TPar2 == null)
		{
			<metadata>j__TPar2 = <metadata>j__TPar;
			ptr2 = ref <metadata>j__TPar2;
			if (<metadata>j__TPar2 == null)
			{
				obj2 = null;
				goto IL_81;
			}
		}
		obj2 = ptr2.ToString();
		IL_81:
		array[num2] = obj2;
		return string.Format(provider, format, array);
	}

	// Token: 0x04000001 RID: 1
	[DebuggerBrowsable(DebuggerBrowsableState.Never)]
	private readonly <product>j__TPar <product>i__Field;

	// Token: 0x04000002 RID: 2
	[DebuggerBrowsable(DebuggerBrowsableState.Never)]
	private readonly <metadata>j__TPar <metadata>i__Field;
}
