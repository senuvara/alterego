﻿using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.CompilerServices;
using UnityEngine;

[assembly: AssemblyVersion("1.0.7016.24194")]
[assembly: InternalsVisibleTo("DynamicProxyGenAssembly2")]
[assembly: InternalsVisibleTo("specs")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCopyright("alex")]
[assembly: AssemblyProduct("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyTitle("Stores")]
[assembly: InternalsVisibleTo("Editor")]
[assembly: UnityAPICompatibilityVersion("2018.3.0f2")]
