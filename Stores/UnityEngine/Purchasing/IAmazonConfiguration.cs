﻿using System;
using System.Collections.Generic;
using UnityEngine.Purchasing.Extension;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000014 RID: 20
	public interface IAmazonConfiguration : IStoreConfiguration
	{
		// Token: 0x06000055 RID: 85
		void WriteSandboxJSON(HashSet<ProductDefinition> products);
	}
}
