﻿using System;
using Uniject;
using UnityEngine.XR;

namespace UnityEngine.Purchasing
{
	// Token: 0x0200002E RID: 46
	internal class GooglePlayAndroidJavaStore : AndroidJavaStore
	{
		// Token: 0x060000C3 RID: 195 RVA: 0x00004CFC File Offset: 0x00002EFC
		public GooglePlayAndroidJavaStore(AndroidJavaObject store, IUtil util) : base(store)
		{
			this.m_Util = util;
			string text = "";
			bool flag = Enum.IsDefined(typeof(PurchaseFailureReason), "DuplicateTransaction");
			if (flag)
			{
				text += "supportsPurchaseFailureReasonDuplicateTransaction";
			}
			base.GetStore().Call("SetFeatures", new object[]
			{
				text
			});
		}

		// Token: 0x060000C4 RID: 196 RVA: 0x00004D60 File Offset: 0x00002F60
		public override void Purchase(string productJSON, string developerPayload)
		{
			bool flag = !developerPayload.Contains("iapPromo");
			if (flag)
			{
				base.GetStore().Call("SetUnityVrEnabled", new object[]
				{
					XRSettings.enabled
				});
			}
			base.Purchase(productJSON, developerPayload);
		}

		// Token: 0x040000A6 RID: 166
		private IUtil m_Util;
	}
}
