﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.CompilerServices;
using Uniject;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace UnityEngine.Purchasing
{
	// Token: 0x020000BC RID: 188
	internal class UIFakeStore : FakeStore
	{
		// Token: 0x06000367 RID: 871 RVA: 0x0000E0FA File Offset: 0x0000C2FA
		public UIFakeStore()
		{
		}

		// Token: 0x06000368 RID: 872 RVA: 0x0000E104 File Offset: 0x0000C304
		public UIFakeStore(IUtil util)
		{
			this.m_Util = util;
		}

		// Token: 0x06000369 RID: 873 RVA: 0x0000E118 File Offset: 0x0000C318
		protected override bool StartUI<T>(object model, FakeStore.DialogType dialogType, Action<bool, T> callback)
		{
			List<string> list = new List<string>();
			list.Add("Success");
			foreach (object obj in Enum.GetValues(typeof(T)))
			{
				T t = (T)((object)obj);
				list.Add(t.ToString());
			}
			Action<bool, int> callback2 = delegate(bool result, int codeValue)
			{
				T arg = (T)((object)codeValue);
				callback(result, arg);
			};
			string queryText = null;
			string okayButtonText = null;
			bool flag = dialogType == FakeStore.DialogType.Purchase;
			if (flag)
			{
				queryText = this.CreatePurchaseQuestion((ProductDefinition)model);
				bool flag2 = this.UIMode == FakeStoreUIMode.DeveloperUser;
				if (flag2)
				{
					okayButtonText = "OK";
				}
				else
				{
					okayButtonText = "Buy";
				}
			}
			else
			{
				bool flag3 = dialogType == FakeStore.DialogType.RetrieveProducts;
				if (flag3)
				{
					queryText = this.CreateRetrieveProductsQuestion((ReadOnlyCollection<ProductDefinition>)model);
					okayButtonText = "OK";
				}
				else
				{
					Debug.LogError("Unrecognized DialogType " + dialogType);
				}
			}
			string cancelButtonText = "Cancel";
			return this.StartUI(queryText, okayButtonText, cancelButtonText, list, callback2);
		}

		// Token: 0x0600036A RID: 874 RVA: 0x0000E25C File Offset: 0x0000C45C
		private bool StartUI(string queryText, string okayButtonText, string cancelButtonText, List<string> options, Action<bool, int> callback)
		{
			bool flag = this.IsShowingDialog();
			bool result;
			if (flag)
			{
				result = false;
			}
			else
			{
				this.m_CurrentDialog = new UIFakeStore.DialogRequest
				{
					QueryText = queryText,
					OkayButtonText = okayButtonText,
					CancelButtonText = cancelButtonText,
					Options = options,
					Callback = callback
				};
				this.InstantiateDialog();
				result = true;
			}
			return result;
		}

		// Token: 0x0600036B RID: 875 RVA: 0x0000E2B8 File Offset: 0x0000C4B8
		private void InstantiateDialog()
		{
			bool flag = this.m_CurrentDialog == null;
			if (flag)
			{
				Debug.LogError(this + " requires m_CurrentDialog. Not showing dialog.");
			}
			else
			{
				bool flag2 = this.UIFakeStoreCanvasPrefab == null;
				if (flag2)
				{
					this.UIFakeStoreCanvasPrefab = (Resources.Load("UIFakeStoreCanvas") as GameObject);
				}
				Canvas component = this.UIFakeStoreCanvasPrefab.GetComponent<Canvas>();
				this.m_Canvas = Object.Instantiate<Canvas>(component);
				UIFakeStore.LifecycleNotifier lifecycleNotifier = this.m_Canvas.gameObject.AddComponent<UIFakeStore.LifecycleNotifier>();
				lifecycleNotifier.OnDestroyCallback = delegate()
				{
					this.m_CurrentDialog = null;
				};
				this.m_ParentGameObjectPath = this.m_Canvas.name + "/Panel/";
				bool flag3 = Object.FindObjectOfType<EventSystem>() == null;
				if (flag3)
				{
					this.m_EventSystem = new GameObject("EventSystem", new Type[]
					{
						typeof(EventSystem)
					});
					this.m_EventSystem.AddComponent<StandaloneInputModule>();
					this.m_EventSystem.transform.parent = this.m_Canvas.transform;
				}
				GameObject gameObject = GameObject.Find(this.m_ParentGameObjectPath + "HeaderText");
				Text component2 = gameObject.GetComponent<Text>();
				component2.text = this.m_CurrentDialog.QueryText;
				Text okayButtonText = this.GetOkayButtonText();
				okayButtonText.text = this.m_CurrentDialog.OkayButtonText;
				Text cancelButtonText = this.GetCancelButtonText();
				cancelButtonText.text = this.m_CurrentDialog.CancelButtonText;
				this.GetDropdown().options.Clear();
				foreach (string text in this.m_CurrentDialog.Options)
				{
					this.GetDropdown().options.Add(new Dropdown.OptionData(text));
				}
				bool flag4 = this.m_CurrentDialog.Options.Count > 0;
				if (flag4)
				{
					this.m_LastSelectedDropdownIndex = 0;
				}
				this.GetDropdown().RefreshShownValue();
				this.GetOkayButton().onClick.AddListener(delegate()
				{
					this.OkayButtonClicked();
				});
				this.GetCancelButton().onClick.AddListener(delegate()
				{
					this.CancelButtonClicked();
				});
				this.GetDropdown().onValueChanged.AddListener(delegate(int selectedItem)
				{
					this.DropdownValueChanged(selectedItem);
				});
				bool flag5 = this.UIMode == FakeStoreUIMode.StandardUser;
				if (flag5)
				{
					this.GetDropdown().onValueChanged.RemoveAllListeners();
					Object.Destroy(this.GetDropdownContainerGameObject());
				}
				else
				{
					bool flag6 = this.UIMode == FakeStoreUIMode.DeveloperUser;
					if (flag6)
					{
						this.GetCancelButton().onClick.RemoveAllListeners();
						Object.Destroy(this.GetCancelButtonGameObject());
					}
				}
			}
		}

		// Token: 0x0600036C RID: 876 RVA: 0x0000E584 File Offset: 0x0000C784
		private string CreatePurchaseQuestion(ProductDefinition definition)
		{
			return "Do you want to Purchase " + definition.id + "?\n\n[Environment: FakeStore]";
		}

		// Token: 0x0600036D RID: 877 RVA: 0x0000E5AC File Offset: 0x0000C7AC
		private string CreateRetrieveProductsQuestion(ReadOnlyCollection<ProductDefinition> definitions)
		{
			string str = "Do you want to initialize purchasing for products {";
			str += string.Join(", ", (from pid in definitions.Take(2)
			select pid.id).ToArray<string>());
			bool flag = definitions.Count > 2;
			if (flag)
			{
				str += ", ...";
			}
			return str + "}?\n\n[Environment: FakeStore]";
		}

		// Token: 0x0600036E RID: 878 RVA: 0x0000E630 File Offset: 0x0000C830
		private Button GetOkayButton()
		{
			return GameObject.Find(this.m_ParentGameObjectPath + "Button1").GetComponent<Button>();
		}

		// Token: 0x0600036F RID: 879 RVA: 0x0000E65C File Offset: 0x0000C85C
		private Button GetCancelButton()
		{
			GameObject gameObject = GameObject.Find(this.m_ParentGameObjectPath + "Button2");
			bool flag = gameObject != null;
			Button result;
			if (flag)
			{
				result = gameObject.GetComponent<Button>();
			}
			else
			{
				result = null;
			}
			return result;
		}

		// Token: 0x06000370 RID: 880 RVA: 0x0000E69C File Offset: 0x0000C89C
		private GameObject GetCancelButtonGameObject()
		{
			return GameObject.Find(this.m_ParentGameObjectPath + "Button2");
		}

		// Token: 0x06000371 RID: 881 RVA: 0x0000E6C4 File Offset: 0x0000C8C4
		private Text GetOkayButtonText()
		{
			return GameObject.Find(this.m_ParentGameObjectPath + "Button1/Text").GetComponent<Text>();
		}

		// Token: 0x06000372 RID: 882 RVA: 0x0000E6F0 File Offset: 0x0000C8F0
		private Text GetCancelButtonText()
		{
			return GameObject.Find(this.m_ParentGameObjectPath + "Button2/Text").GetComponent<Text>();
		}

		// Token: 0x06000373 RID: 883 RVA: 0x0000E71C File Offset: 0x0000C91C
		private Dropdown GetDropdown()
		{
			GameObject gameObject = GameObject.Find(this.m_ParentGameObjectPath + "Panel2/Panel3/Dropdown");
			bool flag = gameObject != null;
			Dropdown result;
			if (flag)
			{
				result = gameObject.GetComponent<Dropdown>();
			}
			else
			{
				result = null;
			}
			return result;
		}

		// Token: 0x06000374 RID: 884 RVA: 0x0000E75C File Offset: 0x0000C95C
		private GameObject GetDropdownContainerGameObject()
		{
			return GameObject.Find(this.m_ParentGameObjectPath + "Panel2");
		}

		// Token: 0x06000375 RID: 885 RVA: 0x0000E784 File Offset: 0x0000C984
		private void OkayButtonClicked()
		{
			bool arg = false;
			bool flag = this.m_LastSelectedDropdownIndex == 0 || this.UIMode != FakeStoreUIMode.DeveloperUser;
			if (flag)
			{
				arg = true;
			}
			int arg2 = Math.Max(0, this.m_LastSelectedDropdownIndex - 1);
			this.m_CurrentDialog.Callback(arg, arg2);
			this.CloseDialog();
		}

		// Token: 0x06000376 RID: 886 RVA: 0x0000E7DC File Offset: 0x0000C9DC
		private void CancelButtonClicked()
		{
			int arg = Math.Max(0, this.m_LastSelectedDropdownIndex - 1);
			this.m_CurrentDialog.Callback(false, arg);
			this.CloseDialog();
		}

		// Token: 0x06000377 RID: 887 RVA: 0x0000E813 File Offset: 0x0000CA13
		private void DropdownValueChanged(int selectedItem)
		{
			this.m_LastSelectedDropdownIndex = selectedItem;
		}

		// Token: 0x06000378 RID: 888 RVA: 0x0000E820 File Offset: 0x0000CA20
		private void CloseDialog()
		{
			this.m_CurrentDialog = null;
			this.GetOkayButton().onClick.RemoveAllListeners();
			bool flag = this.GetCancelButton();
			if (flag)
			{
				this.GetCancelButton().onClick.RemoveAllListeners();
			}
			bool flag2 = this.GetDropdown() != null;
			if (flag2)
			{
				this.GetDropdown().onValueChanged.RemoveAllListeners();
			}
			Object.Destroy(this.m_Canvas.gameObject);
		}

		// Token: 0x06000379 RID: 889 RVA: 0x0000E89C File Offset: 0x0000CA9C
		public bool IsShowingDialog()
		{
			return this.m_CurrentDialog != null;
		}

		// Token: 0x0600037A RID: 890 RVA: 0x0000E8B7 File Offset: 0x0000CAB7
		[CompilerGenerated]
		private void <InstantiateDialog>b__16_0()
		{
			this.m_CurrentDialog = null;
		}

		// Token: 0x0600037B RID: 891 RVA: 0x0000E8C1 File Offset: 0x0000CAC1
		[CompilerGenerated]
		private void <InstantiateDialog>b__16_1()
		{
			this.OkayButtonClicked();
		}

		// Token: 0x0600037C RID: 892 RVA: 0x0000E8CB File Offset: 0x0000CACB
		[CompilerGenerated]
		private void <InstantiateDialog>b__16_2()
		{
			this.CancelButtonClicked();
		}

		// Token: 0x0600037D RID: 893 RVA: 0x0000E8D5 File Offset: 0x0000CAD5
		[CompilerGenerated]
		private void <InstantiateDialog>b__16_3(int selectedItem)
		{
			this.DropdownValueChanged(selectedItem);
		}

		// Token: 0x0400029C RID: 668
		private const string EnvironmentDescriptionPostfix = "\n\n[Environment: FakeStore]";

		// Token: 0x0400029D RID: 669
		private const string SuccessString = "Success";

		// Token: 0x0400029E RID: 670
		private const int RetrieveProductsDescriptionCount = 2;

		// Token: 0x0400029F RID: 671
		private UIFakeStore.DialogRequest m_CurrentDialog;

		// Token: 0x040002A0 RID: 672
		private int m_LastSelectedDropdownIndex;

		// Token: 0x040002A1 RID: 673
		private GameObject UIFakeStoreCanvasPrefab;

		// Token: 0x040002A2 RID: 674
		private Canvas m_Canvas;

		// Token: 0x040002A3 RID: 675
		private GameObject m_EventSystem;

		// Token: 0x040002A4 RID: 676
		private string m_ParentGameObjectPath;

		// Token: 0x040002A5 RID: 677
		private IUtil m_Util;

		// Token: 0x020000BD RID: 189
		protected class DialogRequest
		{
			// Token: 0x0600037E RID: 894 RVA: 0x00002CA1 File Offset: 0x00000EA1
			public DialogRequest()
			{
			}

			// Token: 0x040002A6 RID: 678
			public string QueryText;

			// Token: 0x040002A7 RID: 679
			public string OkayButtonText;

			// Token: 0x040002A8 RID: 680
			public string CancelButtonText;

			// Token: 0x040002A9 RID: 681
			public List<string> Options;

			// Token: 0x040002AA RID: 682
			public Action<bool, int> Callback;
		}

		// Token: 0x020000BE RID: 190
		protected class LifecycleNotifier : MonoBehaviour
		{
			// Token: 0x0600037F RID: 895 RVA: 0x0000E8E0 File Offset: 0x0000CAE0
			private void OnDestroy()
			{
				bool flag = this.OnDestroyCallback != null;
				if (flag)
				{
					this.OnDestroyCallback();
				}
			}

			// Token: 0x06000380 RID: 896 RVA: 0x00009344 File Offset: 0x00007544
			public LifecycleNotifier()
			{
			}

			// Token: 0x040002AB RID: 683
			public Action OnDestroyCallback;
		}

		// Token: 0x020000BF RID: 191
		[CompilerGenerated]
		private sealed class <>c__DisplayClass14_0<T>
		{
			// Token: 0x06000381 RID: 897 RVA: 0x00002CA1 File Offset: 0x00000EA1
			public <>c__DisplayClass14_0()
			{
			}

			// Token: 0x06000382 RID: 898 RVA: 0x0000E90C File Offset: 0x0000CB0C
			internal void <StartUI>b__0(bool result, int codeValue)
			{
				T arg = (T)((object)codeValue);
				this.callback(result, arg);
			}

			// Token: 0x040002AC RID: 684
			public Action<bool, T> callback;
		}

		// Token: 0x020000C0 RID: 192
		[CompilerGenerated]
		[Serializable]
		private sealed class <>c
		{
			// Token: 0x06000383 RID: 899 RVA: 0x0000E934 File Offset: 0x0000CB34
			// Note: this type is marked as 'beforefieldinit'.
			static <>c()
			{
			}

			// Token: 0x06000384 RID: 900 RVA: 0x00002CA1 File Offset: 0x00000EA1
			public <>c()
			{
			}

			// Token: 0x06000385 RID: 901 RVA: 0x0000E940 File Offset: 0x0000CB40
			internal string <CreateRetrieveProductsQuestion>b__18_0(ProductDefinition pid)
			{
				return pid.id;
			}

			// Token: 0x040002AD RID: 685
			public static readonly UIFakeStore.<>c <>9 = new UIFakeStore.<>c();

			// Token: 0x040002AE RID: 686
			public static Func<ProductDefinition, string> <>9__18_0;
		}
	}
}
