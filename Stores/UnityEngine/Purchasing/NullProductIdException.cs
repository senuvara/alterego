﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x0200009C RID: 156
	public class NullProductIdException : ReceiptParserException
	{
		// Token: 0x06000312 RID: 786 RVA: 0x0000D2EE File Offset: 0x0000B4EE
		public NullProductIdException()
		{
		}
	}
}
