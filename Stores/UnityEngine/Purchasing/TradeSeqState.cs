﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000020 RID: 32
	internal enum TradeSeqState
	{
		// Token: 0x04000041 RID: 65
		PAY_FAILED,
		// Token: 0x04000042 RID: 66
		PAY_CONFIRM,
		// Token: 0x04000043 RID: 67
		ORDER_SUCCEED,
		// Token: 0x04000044 RID: 68
		PAY_INIT,
		// Token: 0x04000045 RID: 69
		PAY_SELECTED_CHANNEL,
		// Token: 0x04000046 RID: 70
		NOTKNOWN
	}
}
