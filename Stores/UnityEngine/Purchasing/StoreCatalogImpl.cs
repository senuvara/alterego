﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using Uniject;

namespace UnityEngine.Purchasing
{
	// Token: 0x0200006C RID: 108
	internal class StoreCatalogImpl
	{
		// Token: 0x060001EC RID: 492 RVA: 0x000089C8 File Offset: 0x00006BC8
		public static StoreCatalogImpl CreateInstance(string storeName, string baseUrl, IAsyncWebUtil webUtil, ILogger logger, IUtil util)
		{
			bool flag = string.IsNullOrEmpty(storeName) || string.IsNullOrEmpty(baseUrl);
			StoreCatalogImpl result;
			if (flag)
			{
				result = null;
			}
			else
			{
				bool flag2 = logger == null;
				if (flag2)
				{
					logger = Debug.unityLogger;
				}
				StoreCatalogImpl.profile = ProfileData.Instance(util);
				Dictionary<string, object> profileIds = StoreCatalogImpl.profile.GetProfileIds();
				string catalogURL = baseUrl + "/catalog" + profileIds.ToQueryString();
				FileReference fileReference = FileReference.CreateInstance("store.json", logger, util);
				result = new StoreCatalogImpl(webUtil, logger, catalogURL, storeName, fileReference);
			}
			return result;
		}

		// Token: 0x060001ED RID: 493 RVA: 0x00008A4B File Offset: 0x00006C4B
		internal StoreCatalogImpl(IAsyncWebUtil util, ILogger logger, string catalogURL, string storeName, FileReference fileReference)
		{
			this.m_AsyncUtil = util;
			this.m_Logger = logger;
			this.m_CatalogURL = catalogURL;
			this.m_StoreName = storeName;
			this.m_cachedStoreCatalogReference = fileReference;
		}

		// Token: 0x060001EE RID: 494 RVA: 0x00008A7C File Offset: 0x00006C7C
		internal void FetchProducts(Action<List<ProductDefinition>> callback)
		{
			this.m_AsyncUtil.Get(this.m_CatalogURL, delegate(string response)
			{
				List<ProductDefinition> list = StoreCatalogImpl.ParseProductsFromJSON(response, this.m_StoreName, this.m_Logger);
				bool flag = list == null;
				if (flag)
				{
					this.m_Logger.LogError("Failed to fetch IAP catalog due to malformed response for " + this.m_StoreName, "response: " + response);
					this.handleCachedCatalog(callback);
				}
				else
				{
					this.m_Logger.Log("Fetched catalog successfully");
					bool flag2 = this.m_cachedStoreCatalogReference != null;
					if (flag2)
					{
						this.m_cachedStoreCatalogReference.Save(response);
					}
					callback(list);
				}
			}, delegate(string error)
			{
				this.handleCachedCatalog(callback);
			}, 30);
		}

		// Token: 0x060001EF RID: 495 RVA: 0x00008ACC File Offset: 0x00006CCC
		internal static List<ProductDefinition> ParseProductsFromJSON(string json, string storeName, ILogger logger)
		{
			bool flag = string.IsNullOrEmpty(json);
			List<ProductDefinition> result;
			if (flag)
			{
				result = null;
			}
			else
			{
				HashSet<ProductDefinition> hashSet = new HashSet<ProductDefinition>();
				try
				{
					Dictionary<string, object> dictionary = (Dictionary<string, object>)MiniJson.JsonDecode(json);
					object obj;
					dictionary.TryGetValue("catalog", out obj);
					object value;
					bool flag2 = dictionary.TryGetValue("abGroup", out value);
					if (flag2)
					{
						bool flag3 = StoreCatalogImpl.profile != null;
						if (flag3)
						{
							StoreCatalogImpl.profile.SetStoreABGroup(new int?(Convert.ToInt32(value)));
						}
					}
					Dictionary<string, object> dictionary2 = obj as Dictionary<string, object>;
					object obj2;
					bool flag4 = dictionary2.TryGetValue("id", out obj2);
					if (flag4)
					{
						bool flag5 = StoreCatalogImpl.profile != null;
						if (flag5)
						{
							string text = obj2 as string;
							bool flag6 = text == "";
							if (flag6)
							{
								text = "empty-catalog-id";
							}
							StoreCatalogImpl.profile.SetCatalogId(text);
						}
					}
					else
					{
						bool flag7 = StoreCatalogImpl.profile != null;
						if (flag7)
						{
							StoreCatalogImpl.profile.SetCatalogId("no-catalog-id-present");
						}
					}
					object obj3;
					dictionary2.TryGetValue("products", out obj3);
					List<object> productsList = (List<object>)obj3;
					result = productsList.DecodeJSON(storeName);
				}
				catch (Exception arg)
				{
					bool flag8 = logger != null;
					if (flag8)
					{
						logger.LogWarning("UnityIAP", "Error parsing catalog, exception " + arg);
					}
					result = null;
				}
			}
			return result;
		}

		// Token: 0x060001F0 RID: 496 RVA: 0x00008C2C File Offset: 0x00006E2C
		private void handleCachedCatalog(Action<List<ProductDefinition>> callback)
		{
			List<ProductDefinition> list = null;
			bool flag = this.m_cachedStoreCatalogReference != null;
			if (flag)
			{
				list = StoreCatalogImpl.ParseProductsFromJSON(this.m_cachedStoreCatalogReference.Load(), this.m_StoreName, this.m_Logger);
				bool flag2 = list == null || list.Count == 0;
				if (flag2)
				{
					this.m_Logger.Log("Using configuration builder objects");
				}
				else
				{
					this.m_Logger.Log("Using cached IAP catalog");
				}
			}
			else
			{
				this.m_Logger.Log("Using registered configuration builder objects");
			}
			callback(list);
		}

		// Token: 0x04000136 RID: 310
		private IAsyncWebUtil m_AsyncUtil;

		// Token: 0x04000137 RID: 311
		private ILogger m_Logger;

		// Token: 0x04000138 RID: 312
		private string m_CatalogURL;

		// Token: 0x04000139 RID: 313
		private string m_StoreName;

		// Token: 0x0400013A RID: 314
		private FileReference m_cachedStoreCatalogReference;

		// Token: 0x0400013B RID: 315
		private const string kFileName = "store.json";

		// Token: 0x0400013C RID: 316
		private static ProfileData profile;

		// Token: 0x0400013D RID: 317
		private const string kCatalogURL = "https://ecommerce.iap.unity3d.com";

		// Token: 0x0200006D RID: 109
		[CompilerGenerated]
		private sealed class <>c__DisplayClass10_0
		{
			// Token: 0x060001F1 RID: 497 RVA: 0x00002CA1 File Offset: 0x00000EA1
			public <>c__DisplayClass10_0()
			{
			}

			// Token: 0x060001F2 RID: 498 RVA: 0x00008CC0 File Offset: 0x00006EC0
			internal void <FetchProducts>b__0(string response)
			{
				List<ProductDefinition> list = StoreCatalogImpl.ParseProductsFromJSON(response, this.<>4__this.m_StoreName, this.<>4__this.m_Logger);
				bool flag = list == null;
				if (flag)
				{
					this.<>4__this.m_Logger.LogError("Failed to fetch IAP catalog due to malformed response for " + this.<>4__this.m_StoreName, "response: " + response);
					this.<>4__this.handleCachedCatalog(this.callback);
				}
				else
				{
					this.<>4__this.m_Logger.Log("Fetched catalog successfully");
					bool flag2 = this.<>4__this.m_cachedStoreCatalogReference != null;
					if (flag2)
					{
						this.<>4__this.m_cachedStoreCatalogReference.Save(response);
					}
					this.callback(list);
				}
			}

			// Token: 0x060001F3 RID: 499 RVA: 0x00008D85 File Offset: 0x00006F85
			internal void <FetchProducts>b__1(string error)
			{
				this.<>4__this.handleCachedCatalog(this.callback);
			}

			// Token: 0x0400013E RID: 318
			public Action<List<ProductDefinition>> callback;

			// Token: 0x0400013F RID: 319
			public StoreCatalogImpl <>4__this;
		}
	}
}
