﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Reflection;
using UnityEngine.Purchasing.Extension;
using UnityEngine.Purchasing.MiniJSON;
using UnityEngine.UDP;

namespace UnityEngine.Purchasing
{
	// Token: 0x0200004E RID: 78
	internal class UDPBindings : IInitListener, IPurchaseListener, INativeUDPStore, INativeStore
	{
		// Token: 0x0600012D RID: 301 RVA: 0x00005AE0 File Offset: 0x00003CE0
		public void Initialize(Action<bool, string> callback)
		{
			bool flag = callback == null;
			if (!flag)
			{
				bool flag2 = this.m_InitCallback != null;
				if (flag2)
				{
					callback(false, "{ \"error\" : \"already initializing\" }");
				}
				else
				{
					this.m_InitCallback = callback;
					this.m_LocalPurchasesCache = new Dictionary<string, PurchaseInfo>();
					StoreService.Initialize(this, null);
				}
			}
		}

		// Token: 0x0600012E RID: 302 RVA: 0x00005B30 File Offset: 0x00003D30
		public void Purchase(string productId, Action<bool, string> callback, string developerPayload = null)
		{
			bool flag = callback == null;
			if (!flag)
			{
				bool flag2 = this.m_PurchaseCallback != null;
				if (flag2)
				{
					callback(false, "{ \"error\" : \"already purchasing\" }");
				}
				else
				{
					this.m_PurchaseCallback = callback;
					StoreService.Purchase(productId, developerPayload, this);
				}
			}
		}

		// Token: 0x0600012F RID: 303 RVA: 0x00005B78 File Offset: 0x00003D78
		public void RetrieveProducts(ReadOnlyCollection<ProductDefinition> products, Action<bool, string> callback)
		{
			bool flag = callback == null;
			if (!flag)
			{
				bool flag2 = this.m_RetrieveProductsCallback != null;
				if (flag2)
				{
					callback(false, "{ \"error\" : \"already retrieving products\" }");
				}
				else
				{
					this.m_RetrieveProductsCallback = callback;
					List<string> list = new List<string>();
					foreach (ProductDefinition productDefinition in products)
					{
						list.Add(productDefinition.storeSpecificId);
					}
					StoreService.QueryInventory(list, this);
				}
			}
		}

		// Token: 0x06000130 RID: 304 RVA: 0x00005C0C File Offset: 0x00003E0C
		public void FinishTransaction(ProductDefinition productDefinition, string transactionID)
		{
			PurchaseInfo purchaseInfo = this.FindPurchaseInfo(transactionID);
			bool flag = purchaseInfo != null;
			if (flag)
			{
				StoreService.ConsumePurchase(purchaseInfo, this);
			}
		}

		// Token: 0x06000131 RID: 305 RVA: 0x00005C34 File Offset: 0x00003E34
		private PurchaseInfo FindPurchaseInfo(string transactionId)
		{
			List<PurchaseInfo> purchaseList = this.m_Inventory.GetPurchaseList();
			foreach (PurchaseInfo purchaseInfo in purchaseList)
			{
				bool flag = purchaseInfo.GameOrderId.Equals(transactionId);
				if (flag)
				{
					this.m_Inventory.GetPurchaseDictionary().Remove(purchaseInfo.ProductId);
					return purchaseInfo;
				}
			}
			foreach (KeyValuePair<string, PurchaseInfo> keyValuePair in this.m_LocalPurchasesCache)
			{
				PurchaseInfo value = keyValuePair.Value;
				bool flag2 = value.GameOrderId.Equals(transactionId);
				if (flag2)
				{
					this.m_LocalPurchasesCache.Remove(value.GameOrderId);
					return value;
				}
			}
			return null;
		}

		// Token: 0x06000132 RID: 306 RVA: 0x00005D3C File Offset: 0x00003F3C
		public void OnInitialized(UserInfo userInfo)
		{
			Dictionary<string, string> obj = UDPBindings.StringPropertyToDictionary(userInfo);
			string arg = obj.toJson();
			this.m_InitCallback(true, arg);
			this.m_InitCallback = null;
		}

		// Token: 0x06000133 RID: 307 RVA: 0x00005D70 File Offset: 0x00003F70
		public void OnInitializeFailed(string message)
		{
			Dictionary<string, string> dictionary = new Dictionary<string, string>();
			dictionary["error"] = message;
			string arg = dictionary.toJson();
			this.m_InitCallback(false, arg);
			this.m_InitCallback = null;
		}

		// Token: 0x06000134 RID: 308 RVA: 0x00005DB0 File Offset: 0x00003FB0
		public void OnPurchase(PurchaseInfo purchaseInfo)
		{
			Dictionary<string, string> obj = UDPBindings.StringPropertyToDictionary(purchaseInfo);
			string arg = obj.toJson();
			this.m_LocalPurchasesCache[purchaseInfo.GameOrderId] = purchaseInfo;
			this.m_PurchaseCallback(true, arg);
			this.m_PurchaseCallback = null;
		}

		// Token: 0x06000135 RID: 309 RVA: 0x00005DF4 File Offset: 0x00003FF4
		public void OnPurchaseFailed(string message, PurchaseInfo purchaseInfo)
		{
			Dictionary<string, object> dictionary = new Dictionary<string, object>();
			dictionary["error"] = message;
			bool flag = purchaseInfo != null;
			if (flag)
			{
				dictionary["purchaseInfo"] = UDPBindings.StringPropertyToDictionary(purchaseInfo);
			}
			string arg = dictionary.toJson();
			this.m_PurchaseCallback(false, arg);
			this.m_PurchaseCallback = null;
		}

		// Token: 0x06000136 RID: 310 RVA: 0x00005E50 File Offset: 0x00004050
		public void OnPurchaseRepeated(string productId)
		{
			Dictionary<string, object> dictionary = new Dictionary<string, object>();
			dictionary["error"] = "repeat";
			dictionary["isRepeat"] = true;
			string arg = dictionary.toJson();
			this.m_PurchaseCallback(false, arg);
			this.m_PurchaseCallback = null;
		}

		// Token: 0x06000137 RID: 311 RVA: 0x00005EA3 File Offset: 0x000040A3
		public void OnPurchaseConsume(PurchaseInfo purchaseInfo)
		{
			Debug.Log("Consume Success for " + purchaseInfo.GameOrderId);
		}

		// Token: 0x06000138 RID: 312 RVA: 0x00005EBC File Offset: 0x000040BC
		public void OnPurchaseConsumeFailed(string message, PurchaseInfo purchaseInfo)
		{
			Debug.Log("Consume Failed: " + message);
		}

		// Token: 0x06000139 RID: 313 RVA: 0x00005ED0 File Offset: 0x000040D0
		public void OnQueryInventory(Inventory inventory)
		{
			this.m_Inventory = inventory;
			HashSet<ProductDescription> hashSet = new HashSet<ProductDescription>();
			IList<ProductInfo> productList = inventory.GetProductList();
			foreach (ProductInfo productInfo in productList)
			{
				ProductMetadata metadata = new ProductMetadata(productInfo.Price, productInfo.Title, productInfo.Description, productInfo.Currency, Convert.ToDecimal(productInfo.PriceAmountMicros) / 1000000m);
				ProductDescription item = new ProductDescription(productInfo.ProductId, metadata);
				bool flag = inventory.HasPurchase(productInfo.ProductId);
				if (flag)
				{
					PurchaseInfo purchaseInfo = inventory.GetPurchaseInfo(productInfo.ProductId);
					Dictionary<string, string> dictionary = UDPBindings.StringPropertyToDictionary(purchaseInfo);
					string value = dictionary["GameOrderId"];
					string value2 = dictionary["ProductId"];
					bool flag2 = !string.IsNullOrEmpty(value);
					if (flag2)
					{
						dictionary["transactionId"] = value;
					}
					bool flag3 = !string.IsNullOrEmpty(value2);
					if (flag3)
					{
						dictionary["storeSpecificId"] = value2;
					}
					item = new ProductDescription(productInfo.ProductId, metadata, dictionary.toJson(), purchaseInfo.GameOrderId);
				}
				hashSet.Add(item);
			}
			string arg = JSONSerializer.SerializeProductDescs(hashSet);
			this.m_RetrieveProductsCallback(true, arg);
			this.m_RetrieveProductsCallback = null;
		}

		// Token: 0x0600013A RID: 314 RVA: 0x0000605C File Offset: 0x0000425C
		public void OnQueryInventoryFailed(string message)
		{
			Dictionary<string, string> dictionary = new Dictionary<string, string>();
			dictionary["error"] = message;
			this.m_RetrieveProductsCallback(false, dictionary.toJson());
		}

		// Token: 0x0600013B RID: 315 RVA: 0x000054B3 File Offset: 0x000036B3
		public void RetrieveProducts(string json)
		{
			throw new NotImplementedException();
		}

		// Token: 0x0600013C RID: 316 RVA: 0x000054B3 File Offset: 0x000036B3
		public void Purchase(string productJSON, string developerPayload)
		{
			throw new NotImplementedException();
		}

		// Token: 0x0600013D RID: 317 RVA: 0x000054B3 File Offset: 0x000036B3
		public void FinishTransaction(string productJSON, string transactionID)
		{
			throw new NotImplementedException();
		}

		// Token: 0x0600013E RID: 318 RVA: 0x00006090 File Offset: 0x00004290
		internal static Dictionary<string, string> StringPropertyToDictionary(object info)
		{
			Dictionary<string, string> dictionary = new Dictionary<string, string>();
			PropertyInfo[] properties = info.GetType().GetProperties();
			foreach (PropertyInfo propertyInfo in properties)
			{
				bool flag = propertyInfo.PropertyType == typeof(string);
				if (flag)
				{
					string value = (string)propertyInfo.GetValue(info, null);
					bool flag2 = !string.IsNullOrEmpty(value);
					if (flag2)
					{
						dictionary[propertyInfo.Name] = value;
					}
				}
			}
			return dictionary;
		}

		// Token: 0x0600013F RID: 319 RVA: 0x00002CA1 File Offset: 0x00000EA1
		public UDPBindings()
		{
		}

		// Token: 0x040000CC RID: 204
		private Action<bool, string> m_PurchaseCallback;

		// Token: 0x040000CD RID: 205
		private Action<bool, string> m_InitCallback;

		// Token: 0x040000CE RID: 206
		private Action<bool, string> m_RetrieveProductsCallback;

		// Token: 0x040000CF RID: 207
		private Inventory m_Inventory;

		// Token: 0x040000D0 RID: 208
		private Dictionary<string, PurchaseInfo> m_LocalPurchasesCache;
	}
}
