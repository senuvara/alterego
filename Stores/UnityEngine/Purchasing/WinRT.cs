﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x020000A5 RID: 165
	[Obsolete("Use WindowsStore.Name for Universal Windows Apps")]
	public class WinRT
	{
		// Token: 0x06000320 RID: 800 RVA: 0x00002CA1 File Offset: 0x00000EA1
		public WinRT()
		{
		}

		// Token: 0x04000264 RID: 612
		public const string Name = "WinRT";
	}
}
