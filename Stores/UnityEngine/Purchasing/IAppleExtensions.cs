﻿using System;
using System.Collections.Generic;

namespace UnityEngine.Purchasing
{
	// Token: 0x0200005B RID: 91
	public interface IAppleExtensions : IStoreExtension
	{
		// Token: 0x0600018B RID: 395
		void RefreshAppReceipt(Action<string> successCallback, Action errorCallback);

		// Token: 0x0600018C RID: 396
		string GetTransactionReceiptForProduct(Product product);

		// Token: 0x0600018D RID: 397
		void RestoreTransactions(Action<bool> callback);

		// Token: 0x0600018E RID: 398
		void RegisterPurchaseDeferredListener(Action<Product> callback);

		// Token: 0x0600018F RID: 399
		void SetApplicationUsername(string applicationUsername);

		// Token: 0x17000039 RID: 57
		// (get) Token: 0x06000190 RID: 400
		// (set) Token: 0x06000191 RID: 401
		bool simulateAskToBuy { get; set; }

		// Token: 0x06000192 RID: 402
		void SetStorePromotionOrder(List<Product> products);

		// Token: 0x06000193 RID: 403
		void SetStorePromotionVisibility(Product product, AppleStorePromotionVisibility visible);

		// Token: 0x06000194 RID: 404
		void ContinuePromotionalPurchases();

		// Token: 0x06000195 RID: 405
		Dictionary<string, string> GetIntroductoryPriceDictionary();

		// Token: 0x06000196 RID: 406
		Dictionary<string, string> GetProductDetails();
	}
}
