﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000078 RID: 120
	internal interface IAsyncWebUtil
	{
		// Token: 0x0600021F RID: 543
		void Get(string url, Action<string> responseHandler, Action<string> errorHandler, int maxTimeoutInSeconds = 30);

		// Token: 0x06000220 RID: 544
		void Post(string url, string body, Action<string> responseHandler, Action<string> errorHandler, int maxTimeoutInSeconds = 30);

		// Token: 0x06000221 RID: 545
		void Schedule(Action a, int delayInSeconds);
	}
}
