﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000099 RID: 153
	internal enum AppleStoreProductType
	{
		// Token: 0x0400025D RID: 605
		NonConsumable,
		// Token: 0x0400025E RID: 606
		Consumable,
		// Token: 0x0400025F RID: 607
		NonRenewingSubscription,
		// Token: 0x04000260 RID: 608
		AutoRenewingSubscription
	}
}
