﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using Uniject;

namespace UnityEngine.Purchasing
{
	// Token: 0x0200008C RID: 140
	internal class ProfileData
	{
		// Token: 0x1700005E RID: 94
		// (get) Token: 0x0600026C RID: 620 RVA: 0x0000A43D File Offset: 0x0000863D
		// (set) Token: 0x0600026D RID: 621 RVA: 0x0000A445 File Offset: 0x00008645
		public string AppId
		{
			[CompilerGenerated]
			get
			{
				return this.<AppId>k__BackingField;
			}
			[CompilerGenerated]
			internal set
			{
				this.<AppId>k__BackingField = value;
			}
		}

		// Token: 0x1700005F RID: 95
		// (get) Token: 0x0600026E RID: 622 RVA: 0x0000A44E File Offset: 0x0000864E
		// (set) Token: 0x0600026F RID: 623 RVA: 0x0000A456 File Offset: 0x00008656
		public string UserId
		{
			[CompilerGenerated]
			get
			{
				return this.<UserId>k__BackingField;
			}
			[CompilerGenerated]
			internal set
			{
				this.<UserId>k__BackingField = value;
			}
		}

		// Token: 0x17000060 RID: 96
		// (get) Token: 0x06000270 RID: 624 RVA: 0x0000A45F File Offset: 0x0000865F
		// (set) Token: 0x06000271 RID: 625 RVA: 0x0000A467 File Offset: 0x00008667
		public ulong SessionId
		{
			[CompilerGenerated]
			get
			{
				return this.<SessionId>k__BackingField;
			}
			[CompilerGenerated]
			internal set
			{
				this.<SessionId>k__BackingField = value;
			}
		}

		// Token: 0x17000061 RID: 97
		// (get) Token: 0x06000272 RID: 626 RVA: 0x0000A470 File Offset: 0x00008670
		// (set) Token: 0x06000273 RID: 627 RVA: 0x0000A478 File Offset: 0x00008678
		public string Platform
		{
			[CompilerGenerated]
			get
			{
				return this.<Platform>k__BackingField;
			}
			[CompilerGenerated]
			internal set
			{
				this.<Platform>k__BackingField = value;
			}
		}

		// Token: 0x17000062 RID: 98
		// (get) Token: 0x06000274 RID: 628 RVA: 0x0000A481 File Offset: 0x00008681
		// (set) Token: 0x06000275 RID: 629 RVA: 0x0000A489 File Offset: 0x00008689
		public int PlatformId
		{
			[CompilerGenerated]
			get
			{
				return this.<PlatformId>k__BackingField;
			}
			[CompilerGenerated]
			internal set
			{
				this.<PlatformId>k__BackingField = value;
			}
		}

		// Token: 0x17000063 RID: 99
		// (get) Token: 0x06000276 RID: 630 RVA: 0x0000A492 File Offset: 0x00008692
		// (set) Token: 0x06000277 RID: 631 RVA: 0x0000A49A File Offset: 0x0000869A
		public string SdkVer
		{
			[CompilerGenerated]
			get
			{
				return this.<SdkVer>k__BackingField;
			}
			[CompilerGenerated]
			internal set
			{
				this.<SdkVer>k__BackingField = value;
			}
		}

		// Token: 0x17000064 RID: 100
		// (get) Token: 0x06000278 RID: 632 RVA: 0x0000A4A3 File Offset: 0x000086A3
		// (set) Token: 0x06000279 RID: 633 RVA: 0x0000A4AB File Offset: 0x000086AB
		public string OsVer
		{
			[CompilerGenerated]
			get
			{
				return this.<OsVer>k__BackingField;
			}
			[CompilerGenerated]
			internal set
			{
				this.<OsVer>k__BackingField = value;
			}
		}

		// Token: 0x17000065 RID: 101
		// (get) Token: 0x0600027A RID: 634 RVA: 0x0000A4B4 File Offset: 0x000086B4
		// (set) Token: 0x0600027B RID: 635 RVA: 0x0000A4BC File Offset: 0x000086BC
		public int ScreenWidth
		{
			[CompilerGenerated]
			get
			{
				return this.<ScreenWidth>k__BackingField;
			}
			[CompilerGenerated]
			internal set
			{
				this.<ScreenWidth>k__BackingField = value;
			}
		}

		// Token: 0x17000066 RID: 102
		// (get) Token: 0x0600027C RID: 636 RVA: 0x0000A4C5 File Offset: 0x000086C5
		// (set) Token: 0x0600027D RID: 637 RVA: 0x0000A4CD File Offset: 0x000086CD
		public int ScreenHeight
		{
			[CompilerGenerated]
			get
			{
				return this.<ScreenHeight>k__BackingField;
			}
			[CompilerGenerated]
			internal set
			{
				this.<ScreenHeight>k__BackingField = value;
			}
		}

		// Token: 0x17000067 RID: 103
		// (get) Token: 0x0600027E RID: 638 RVA: 0x0000A4D6 File Offset: 0x000086D6
		// (set) Token: 0x0600027F RID: 639 RVA: 0x0000A4DE File Offset: 0x000086DE
		public float ScreenDpi
		{
			[CompilerGenerated]
			get
			{
				return this.<ScreenDpi>k__BackingField;
			}
			[CompilerGenerated]
			internal set
			{
				this.<ScreenDpi>k__BackingField = value;
			}
		}

		// Token: 0x17000068 RID: 104
		// (get) Token: 0x06000280 RID: 640 RVA: 0x0000A4E7 File Offset: 0x000086E7
		// (set) Token: 0x06000281 RID: 641 RVA: 0x0000A4EF File Offset: 0x000086EF
		public string ScreenOrientation
		{
			[CompilerGenerated]
			get
			{
				return this.<ScreenOrientation>k__BackingField;
			}
			[CompilerGenerated]
			internal set
			{
				this.<ScreenOrientation>k__BackingField = value;
			}
		}

		// Token: 0x17000069 RID: 105
		// (get) Token: 0x06000282 RID: 642 RVA: 0x0000A4F8 File Offset: 0x000086F8
		// (set) Token: 0x06000283 RID: 643 RVA: 0x0000A500 File Offset: 0x00008700
		public string DeviceId
		{
			[CompilerGenerated]
			get
			{
				return this.<DeviceId>k__BackingField;
			}
			[CompilerGenerated]
			internal set
			{
				this.<DeviceId>k__BackingField = value;
			}
		}

		// Token: 0x1700006A RID: 106
		// (get) Token: 0x06000284 RID: 644 RVA: 0x0000A509 File Offset: 0x00008709
		// (set) Token: 0x06000285 RID: 645 RVA: 0x0000A511 File Offset: 0x00008711
		public string BuildGUID
		{
			[CompilerGenerated]
			get
			{
				return this.<BuildGUID>k__BackingField;
			}
			[CompilerGenerated]
			internal set
			{
				this.<BuildGUID>k__BackingField = value;
			}
		}

		// Token: 0x1700006B RID: 107
		// (get) Token: 0x06000286 RID: 646 RVA: 0x0000A51A File Offset: 0x0000871A
		// (set) Token: 0x06000287 RID: 647 RVA: 0x0000A522 File Offset: 0x00008722
		public string IapVer
		{
			[CompilerGenerated]
			get
			{
				return this.<IapVer>k__BackingField;
			}
			[CompilerGenerated]
			internal set
			{
				this.<IapVer>k__BackingField = value;
			}
		}

		// Token: 0x1700006C RID: 108
		// (get) Token: 0x06000288 RID: 648 RVA: 0x0000A52B File Offset: 0x0000872B
		// (set) Token: 0x06000289 RID: 649 RVA: 0x0000A533 File Offset: 0x00008733
		public string AdsGamerToken
		{
			[CompilerGenerated]
			get
			{
				return this.<AdsGamerToken>k__BackingField;
			}
			[CompilerGenerated]
			internal set
			{
				this.<AdsGamerToken>k__BackingField = value;
			}
		}

		// Token: 0x1700006D RID: 109
		// (get) Token: 0x0600028A RID: 650 RVA: 0x0000A53C File Offset: 0x0000873C
		// (set) Token: 0x0600028B RID: 651 RVA: 0x0000A544 File Offset: 0x00008744
		public bool? TrackingOptOut
		{
			[CompilerGenerated]
			get
			{
				return this.<TrackingOptOut>k__BackingField;
			}
			[CompilerGenerated]
			internal set
			{
				this.<TrackingOptOut>k__BackingField = value;
			}
		}

		// Token: 0x1700006E RID: 110
		// (get) Token: 0x0600028C RID: 652 RVA: 0x0000A54D File Offset: 0x0000874D
		// (set) Token: 0x0600028D RID: 653 RVA: 0x0000A555 File Offset: 0x00008755
		public int? AdsABGroup
		{
			[CompilerGenerated]
			get
			{
				return this.<AdsABGroup>k__BackingField;
			}
			[CompilerGenerated]
			internal set
			{
				this.<AdsABGroup>k__BackingField = value;
			}
		}

		// Token: 0x1700006F RID: 111
		// (get) Token: 0x0600028E RID: 654 RVA: 0x0000A55E File Offset: 0x0000875E
		// (set) Token: 0x0600028F RID: 655 RVA: 0x0000A566 File Offset: 0x00008766
		public string AdsGameId
		{
			[CompilerGenerated]
			get
			{
				return this.<AdsGameId>k__BackingField;
			}
			[CompilerGenerated]
			internal set
			{
				this.<AdsGameId>k__BackingField = value;
			}
		}

		// Token: 0x17000070 RID: 112
		// (get) Token: 0x06000290 RID: 656 RVA: 0x0000A56F File Offset: 0x0000876F
		// (set) Token: 0x06000291 RID: 657 RVA: 0x0000A577 File Offset: 0x00008777
		public int? StoreABGroup
		{
			[CompilerGenerated]
			get
			{
				return this.<StoreABGroup>k__BackingField;
			}
			[CompilerGenerated]
			internal set
			{
				this.<StoreABGroup>k__BackingField = value;
			}
		}

		// Token: 0x17000071 RID: 113
		// (get) Token: 0x06000292 RID: 658 RVA: 0x0000A580 File Offset: 0x00008780
		// (set) Token: 0x06000293 RID: 659 RVA: 0x0000A588 File Offset: 0x00008788
		public string CatalogId
		{
			[CompilerGenerated]
			get
			{
				return this.<CatalogId>k__BackingField;
			}
			[CompilerGenerated]
			internal set
			{
				this.<CatalogId>k__BackingField = value;
			}
		}

		// Token: 0x17000072 RID: 114
		// (get) Token: 0x06000294 RID: 660 RVA: 0x0000A591 File Offset: 0x00008791
		// (set) Token: 0x06000295 RID: 661 RVA: 0x0000A599 File Offset: 0x00008799
		public string MonetizationId
		{
			[CompilerGenerated]
			get
			{
				return this.<MonetizationId>k__BackingField;
			}
			[CompilerGenerated]
			internal set
			{
				this.<MonetizationId>k__BackingField = value;
			}
		}

		// Token: 0x17000073 RID: 115
		// (get) Token: 0x06000296 RID: 662 RVA: 0x0000A5A2 File Offset: 0x000087A2
		// (set) Token: 0x06000297 RID: 663 RVA: 0x0000A5AA File Offset: 0x000087AA
		public string StoreName
		{
			[CompilerGenerated]
			get
			{
				return this.<StoreName>k__BackingField;
			}
			[CompilerGenerated]
			internal set
			{
				this.<StoreName>k__BackingField = value;
			}
		}

		// Token: 0x17000074 RID: 116
		// (get) Token: 0x06000298 RID: 664 RVA: 0x0000A5B3 File Offset: 0x000087B3
		// (set) Token: 0x06000299 RID: 665 RVA: 0x0000A5BB File Offset: 0x000087BB
		public string GameVersion
		{
			[CompilerGenerated]
			get
			{
				return this.<GameVersion>k__BackingField;
			}
			[CompilerGenerated]
			internal set
			{
				this.<GameVersion>k__BackingField = value;
			}
		}

		// Token: 0x17000075 RID: 117
		// (get) Token: 0x0600029A RID: 666 RVA: 0x0000A5C4 File Offset: 0x000087C4
		// (set) Token: 0x0600029B RID: 667 RVA: 0x0000A5CC File Offset: 0x000087CC
		public bool? StoreTestEnabled
		{
			[CompilerGenerated]
			get
			{
				return this.<StoreTestEnabled>k__BackingField;
			}
			[CompilerGenerated]
			internal set
			{
				this.<StoreTestEnabled>k__BackingField = value;
			}
		}

		// Token: 0x0600029C RID: 668 RVA: 0x0000A5D8 File Offset: 0x000087D8
		private ProfileData(IUtil util)
		{
			this.m_Util = util;
			this.AppId = util.cloudProjectId;
			this.Platform = util.platform.ToString();
			this.PlatformId = (int)util.platform;
			this.SdkVer = util.unityVersion;
			this.OsVer = util.operatingSystem;
			this.DeviceId = util.deviceUniqueIdentifier;
			this.GameVersion = util.gameVersion;
			this.IapVer = Promo.Version();
			this.UserId = util.userId;
			this.SessionId = util.sessionId;
			this.ScreenWidth = util.screenWidth;
			this.ScreenHeight = util.screenHeight;
			this.ScreenDpi = util.screenDpi;
			this.ScreenOrientation = util.screenOrientation;
		}

		// Token: 0x0600029D RID: 669 RVA: 0x0000A6B8 File Offset: 0x000088B8
		internal Dictionary<string, object> GetProfileDict()
		{
			Dictionary<string, object> dictionary = new Dictionary<string, object>();
			dictionary.Add("appid", this.AppId);
			dictionary.Add("platform", this.Platform);
			dictionary.Add("platformid", this.PlatformId);
			bool flag = !string.IsNullOrEmpty(this.AdsGameId);
			if (flag)
			{
				dictionary.Add("gameId", this.AdsGameId);
			}
			dictionary.Add("sdk_ver", this.SdkVer);
			bool flag2 = this.DeviceId != "n/a";
			if (flag2)
			{
				dictionary.Add("deviceid", this.DeviceId);
			}
			bool flag3 = !string.IsNullOrEmpty(this.UserId);
			if (flag3)
			{
				dictionary.Add("userid", this.UserId);
			}
			bool flag4 = this.SessionId > 0UL;
			if (flag4)
			{
				dictionary.Add("sessionid", this.SessionId);
			}
			bool flag5 = !string.IsNullOrEmpty(this.BuildGUID);
			if (flag5)
			{
				dictionary.Add("build_guid", this.BuildGUID);
			}
			bool flag6 = !string.IsNullOrEmpty(this.IapVer);
			if (flag6)
			{
				dictionary.Add("iap_ver", this.IapVer);
			}
			bool flag7 = !string.IsNullOrEmpty(this.AdsGamerToken);
			if (flag7)
			{
				dictionary.Add("gamerToken", this.AdsGamerToken);
			}
			bool flag8 = this.TrackingOptOut != null;
			if (flag8)
			{
				dictionary.Add("trackingOptOut", this.TrackingOptOut);
			}
			bool flag9 = this.AdsABGroup != null;
			if (flag9)
			{
				dictionary.Add("abGroup", this.AdsABGroup);
			}
			bool flag10 = this.StoreABGroup != null;
			if (flag10)
			{
				dictionary.Add("store_abgroup", this.StoreABGroup);
			}
			bool flag11 = !string.IsNullOrEmpty(this.CatalogId);
			if (flag11)
			{
				dictionary.Add("catalogid", this.CatalogId);
			}
			bool flag12 = this.StoreTestEnabled != null;
			if (flag12)
			{
				dictionary.Add("iap_test", this.StoreTestEnabled);
			}
			bool flag13 = !string.IsNullOrEmpty(this.StoreName);
			if (flag13)
			{
				dictionary.Add("store", this.StoreName);
			}
			bool flag14 = !string.IsNullOrEmpty(this.GameVersion);
			if (flag14)
			{
				dictionary.Add("game_ver", this.GameVersion);
			}
			bool flag15 = !string.IsNullOrEmpty(this.OsVer);
			if (flag15)
			{
				dictionary.Add("osv", this.OsVer);
			}
			dictionary.Add("w", this.ScreenWidth);
			dictionary.Add("h", this.ScreenHeight);
			dictionary.Add("ppi", this.ScreenDpi);
			this.ScreenOrientation = this.m_Util.screenOrientation;
			bool flag16 = !string.IsNullOrEmpty(this.ScreenOrientation);
			if (flag16)
			{
				dictionary.Add("orient", this.ScreenOrientation);
			}
			return dictionary;
		}

		// Token: 0x0600029E RID: 670 RVA: 0x0000AA0C File Offset: 0x00008C0C
		internal Dictionary<string, object> GetProfileIds()
		{
			Dictionary<string, object> dictionary = new Dictionary<string, object>();
			dictionary.Add("appid", this.AppId);
			bool flag = this.DeviceId != "n/a";
			if (flag)
			{
				dictionary.Add("deviceid", this.DeviceId);
			}
			bool flag2 = !string.IsNullOrEmpty(this.UserId);
			if (flag2)
			{
				dictionary.Add("userid", this.UserId);
			}
			bool flag3 = !string.IsNullOrEmpty(this.AdsGamerToken);
			if (flag3)
			{
				dictionary.Add("gamerToken", this.AdsGamerToken);
			}
			bool flag4 = this.TrackingOptOut != null;
			if (flag4)
			{
				dictionary.Add("trackingOptOut", this.TrackingOptOut);
			}
			bool flag5 = !string.IsNullOrEmpty(this.MonetizationId);
			if (flag5)
			{
				dictionary.Add("umpid", this.MonetizationId);
			}
			bool flag6 = this.SessionId > 0UL;
			if (flag6)
			{
				string value = Convert.ToString(this.SessionId);
				bool flag7 = !string.IsNullOrEmpty(value);
				if (flag7)
				{
					dictionary.Add("sessionid", value);
				}
			}
			return dictionary;
		}

		// Token: 0x0600029F RID: 671 RVA: 0x0000AB44 File Offset: 0x00008D44
		internal static ProfileData Instance(IUtil util)
		{
			bool flag = ProfileData.ProfileInstance == null;
			if (flag)
			{
				ProfileData.ProfileInstance = new ProfileData(util);
			}
			return ProfileData.ProfileInstance;
		}

		// Token: 0x060002A0 RID: 672 RVA: 0x0000AB74 File Offset: 0x00008D74
		internal void SetGamerToken(string gamerToken)
		{
			bool flag = !string.IsNullOrEmpty(gamerToken);
			if (flag)
			{
				this.AdsGamerToken = gamerToken;
			}
		}

		// Token: 0x060002A1 RID: 673 RVA: 0x0000AB9C File Offset: 0x00008D9C
		internal void SetTrackingOptOut(bool? trackingOptOut)
		{
			bool flag = trackingOptOut != null;
			if (flag)
			{
				this.TrackingOptOut = trackingOptOut;
			}
		}

		// Token: 0x060002A2 RID: 674 RVA: 0x0000ABC0 File Offset: 0x00008DC0
		internal void SetGameId(string gameid)
		{
			bool flag = !string.IsNullOrEmpty(gameid);
			if (flag)
			{
				this.AdsGameId = gameid;
			}
		}

		// Token: 0x060002A3 RID: 675 RVA: 0x0000ABE8 File Offset: 0x00008DE8
		internal void SetABGroup(int? abgroup)
		{
			bool flag = abgroup != null && abgroup > 0;
			if (flag)
			{
				this.AdsABGroup = abgroup;
			}
		}

		// Token: 0x060002A4 RID: 676 RVA: 0x0000AC28 File Offset: 0x00008E28
		internal void SetStoreABGroup(int? abgroup)
		{
			bool flag = abgroup != null;
			if (flag)
			{
				this.StoreABGroup = abgroup;
			}
		}

		// Token: 0x060002A5 RID: 677 RVA: 0x0000AC4C File Offset: 0x00008E4C
		internal void SetCatalogId(string storeid)
		{
			bool flag = storeid != null;
			if (flag)
			{
				this.CatalogId = storeid;
			}
		}

		// Token: 0x060002A6 RID: 678 RVA: 0x0000AC6C File Offset: 0x00008E6C
		internal void SetMonetizationId(string umpid)
		{
			bool flag = !string.IsNullOrEmpty(umpid);
			if (flag)
			{
				this.MonetizationId = umpid;
			}
		}

		// Token: 0x060002A7 RID: 679 RVA: 0x0000AC91 File Offset: 0x00008E91
		internal void SetStoreTestEnabled(bool enable)
		{
			this.StoreTestEnabled = new bool?(enable);
		}

		// Token: 0x060002A8 RID: 680 RVA: 0x0000ACA4 File Offset: 0x00008EA4
		internal void SetStoreName(string storename)
		{
			bool flag = !string.IsNullOrEmpty(storename);
			if (flag)
			{
				this.StoreName = storename;
			}
		}

		// Token: 0x040001C5 RID: 453
		private IUtil m_Util;

		// Token: 0x040001C6 RID: 454
		private static ProfileData ProfileInstance;

		// Token: 0x040001C7 RID: 455
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private string <AppId>k__BackingField;

		// Token: 0x040001C8 RID: 456
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private string <UserId>k__BackingField;

		// Token: 0x040001C9 RID: 457
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private ulong <SessionId>k__BackingField;

		// Token: 0x040001CA RID: 458
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private string <Platform>k__BackingField;

		// Token: 0x040001CB RID: 459
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private int <PlatformId>k__BackingField;

		// Token: 0x040001CC RID: 460
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private string <SdkVer>k__BackingField;

		// Token: 0x040001CD RID: 461
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private string <OsVer>k__BackingField;

		// Token: 0x040001CE RID: 462
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private int <ScreenWidth>k__BackingField;

		// Token: 0x040001CF RID: 463
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private int <ScreenHeight>k__BackingField;

		// Token: 0x040001D0 RID: 464
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private float <ScreenDpi>k__BackingField;

		// Token: 0x040001D1 RID: 465
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private string <ScreenOrientation>k__BackingField;

		// Token: 0x040001D2 RID: 466
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private string <DeviceId>k__BackingField;

		// Token: 0x040001D3 RID: 467
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private string <BuildGUID>k__BackingField;

		// Token: 0x040001D4 RID: 468
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private string <IapVer>k__BackingField;

		// Token: 0x040001D5 RID: 469
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private string <AdsGamerToken>k__BackingField;

		// Token: 0x040001D6 RID: 470
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private bool? <TrackingOptOut>k__BackingField;

		// Token: 0x040001D7 RID: 471
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private int? <AdsABGroup>k__BackingField;

		// Token: 0x040001D8 RID: 472
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private string <AdsGameId>k__BackingField;

		// Token: 0x040001D9 RID: 473
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private int? <StoreABGroup>k__BackingField;

		// Token: 0x040001DA RID: 474
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private string <CatalogId>k__BackingField;

		// Token: 0x040001DB RID: 475
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private string <MonetizationId>k__BackingField;

		// Token: 0x040001DC RID: 476
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private string <StoreName>k__BackingField;

		// Token: 0x040001DD RID: 477
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private string <GameVersion>k__BackingField;

		// Token: 0x040001DE RID: 478
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private bool? <StoreTestEnabled>k__BackingField;

		// Token: 0x040001DF RID: 479
		private const string kConnectSessionInfoApplicationId = "appid";

		// Token: 0x040001E0 RID: 480
		private const string kConnectSessionInfoUserId = "userid";

		// Token: 0x040001E1 RID: 481
		private const string kConnectSessionInfoSessionId = "sessionid";

		// Token: 0x040001E2 RID: 482
		private const string kConnectSessionInfoPlatformName = "platform";

		// Token: 0x040001E3 RID: 483
		private const string kConnectSessionInfoPlatformId = "platformid";

		// Token: 0x040001E4 RID: 484
		private const string kConnectSessionInfoSdkVersion = "sdk_ver";

		// Token: 0x040001E5 RID: 485
		private const string kConnectSessionInfoDeviceId = "deviceid";

		// Token: 0x040001E6 RID: 486
		private const string kConnectSessionInfoBuildGuid = "build_guid";

		// Token: 0x040001E7 RID: 487
		private const string kConnectSessionInfoIapVersion = "iap_ver";

		// Token: 0x040001E8 RID: 488
		private const string kConnectSessionInfoAdsGamerToken = "gamerToken";

		// Token: 0x040001E9 RID: 489
		private const string kConnectSessionInfoAdsTrackingOptOut = "trackingOptOut";

		// Token: 0x040001EA RID: 490
		private const string kConnectSessionInfoAdsGameId = "gameId";

		// Token: 0x040001EB RID: 491
		private const string kConnectSessionInfoAdsABGroup = "abGroup";

		// Token: 0x040001EC RID: 492
		private const string kConnectSessionInfoStoreABGroup = "store_abgroup";

		// Token: 0x040001ED RID: 493
		private const string kConnectSessionInfoCatalogId = "catalogid";

		// Token: 0x040001EE RID: 494
		private const string kConnectSessionInfoMonetizationId = "umpid";

		// Token: 0x040001EF RID: 495
		private const string kConnectSessionInfoStoreTest = "iap_test";

		// Token: 0x040001F0 RID: 496
		private const string kConnectSessionInfoStoreName = "store";

		// Token: 0x040001F1 RID: 497
		private const string kConnectSessionInfoGameVersion = "game_ver";

		// Token: 0x040001F2 RID: 498
		private const string kOsVersion = "osv";

		// Token: 0x040001F3 RID: 499
		private const string kDpi = "ppi";

		// Token: 0x040001F4 RID: 500
		private const string kWidth = "w";

		// Token: 0x040001F5 RID: 501
		private const string kHeight = "h";

		// Token: 0x040001F6 RID: 502
		private const string kOrient = "orient";
	}
}
