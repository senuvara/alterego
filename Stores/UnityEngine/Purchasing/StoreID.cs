﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x0200007B RID: 123
	[Serializable]
	public class StoreID
	{
		// Token: 0x06000226 RID: 550 RVA: 0x000095EF File Offset: 0x000077EF
		public StoreID(string store_, string id_)
		{
			this.store = store_;
			this.id = id_;
		}

		// Token: 0x04000172 RID: 370
		public string store;

		// Token: 0x04000173 RID: 371
		public string id;
	}
}
