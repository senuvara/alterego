﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x0200005C RID: 92
	public enum AppleStorePromotionVisibility
	{
		// Token: 0x040000F1 RID: 241
		Default,
		// Token: 0x040000F2 RID: 242
		Hide,
		// Token: 0x040000F3 RID: 243
		Show
	}
}
