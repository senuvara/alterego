﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000064 RID: 100
	public class CloudCatalogImpl
	{
		// Token: 0x060001C2 RID: 450 RVA: 0x0000828C File Offset: 0x0000648C
		public static CloudCatalogImpl CreateInstance(string storeName)
		{
			GameObject gameObject = new GameObject();
			Object.DontDestroyOnLoad(gameObject);
			gameObject.name = "Unity IAP";
			gameObject.hideFlags = (HideFlags.HideInHierarchy | HideFlags.HideInInspector);
			AsyncWebUtil util = gameObject.AddComponent<AsyncWebUtil>();
			string text = Path.Combine(Path.Combine(Application.persistentDataPath, "Unity"), Path.Combine(Application.cloudProjectId, "IAP"));
			string cacheFile = null;
			try
			{
				Directory.CreateDirectory(text);
				cacheFile = Path.Combine(text, "catalog.json");
			}
			catch (Exception message)
			{
				Debug.unityLogger.Log("Unable to cache IAP catalog", message);
			}
			string catalogURL = string.Format("{0}/{1}", "https://catalog.iap.cloud.unity3d.com", Application.cloudProjectId);
			return new CloudCatalogImpl(util, cacheFile, Debug.unityLogger, catalogURL, storeName);
		}

		// Token: 0x060001C3 RID: 451 RVA: 0x00008354 File Offset: 0x00006554
		internal CloudCatalogImpl(IAsyncWebUtil util, string cacheFile, ILogger logger, string catalogURL, string storeName)
		{
			this.m_AsyncUtil = util;
			this.m_CacheFileName = cacheFile;
			this.m_Logger = logger;
			this.m_CatalogURL = catalogURL;
			this.m_StoreName = storeName;
		}

		// Token: 0x060001C4 RID: 452 RVA: 0x00008383 File Offset: 0x00006583
		public void FetchProducts(Action<HashSet<ProductDefinition>> callback)
		{
			this.FetchProducts(callback, 0);
		}

		// Token: 0x060001C5 RID: 453 RVA: 0x00008390 File Offset: 0x00006590
		internal void FetchProducts(Action<HashSet<ProductDefinition>> callback, int delayInSeconds)
		{
			this.m_Logger.Log("Fetching IAP cloud catalog...");
			Action <>9__2;
			this.m_AsyncUtil.Get(this.m_CatalogURL, delegate(string response)
			{
				this.m_Logger.Log("Fetched catalog");
				try
				{
					HashSet<ProductDefinition> obj = CloudCatalogImpl.ParseProductsFromJSON(response, this.m_StoreName);
					this.TryPersistCatalog(response);
					callback(obj);
				}
				catch (SerializationException message)
				{
					this.m_Logger.LogError("Error parsing IAP catalog", message);
					this.m_Logger.Log(response);
					callback(this.TryLoadCachedCatalog());
				}
			}, delegate(string error)
			{
				HashSet<ProductDefinition> hashSet = this.TryLoadCachedCatalog();
				bool flag = hashSet != null && hashSet.Count > 0;
				if (flag)
				{
					this.m_Logger.Log("Failed to fetch IAP catalog, using cache.");
					callback(hashSet);
				}
				else
				{
					delayInSeconds = Math.Max(5, delayInSeconds * 2);
					delayInSeconds = Math.Min(300, delayInSeconds);
					IAsyncWebUtil asyncUtil = this.m_AsyncUtil;
					Action a;
					if ((a = <>9__2) == null)
					{
						a = (<>9__2 = delegate()
						{
							this.FetchProducts(callback, delayInSeconds);
						});
					}
					asyncUtil.Schedule(a, delayInSeconds);
				}
			}, 30);
		}

		// Token: 0x060001C6 RID: 454 RVA: 0x000083F8 File Offset: 0x000065F8
		internal static HashSet<ProductDefinition> ParseProductsFromJSON(string json, string storeName)
		{
			HashSet<ProductDefinition> hashSet = new HashSet<ProductDefinition>();
			HashSet<ProductDefinition> result;
			try
			{
				Dictionary<string, object> dictionary = (Dictionary<string, object>)MiniJson.JsonDecode(json);
				object obj;
				dictionary.TryGetValue("products", out obj);
				List<object> list = obj as List<object>;
				string key = CloudCatalogImpl.CamelCaseToSnakeCase(storeName);
				foreach (object obj2 in list)
				{
					Dictionary<string, object> dictionary2 = (Dictionary<string, object>)obj2;
					object obj3;
					dictionary2.TryGetValue("id", out obj3);
					object obj4;
					dictionary2.TryGetValue("store_ids", out obj4);
					object obj5;
					dictionary2.TryGetValue("type", out obj5);
					Dictionary<string, object> dictionary3 = obj4 as Dictionary<string, object>;
					string storeSpecificId = (string)obj3;
					bool flag = dictionary3 != null && dictionary3.ContainsKey(key);
					if (flag)
					{
						object obj6 = null;
						dictionary3.TryGetValue(key, out obj6);
						bool flag2 = obj6 != null;
						if (flag2)
						{
							storeSpecificId = (string)obj6;
						}
					}
					ProductType type = (ProductType)Enum.Parse(typeof(ProductType), (string)obj5);
					ProductDefinition item = new ProductDefinition((string)obj3, storeSpecificId, type);
					hashSet.Add(item);
				}
				result = hashSet;
			}
			catch (Exception innerException)
			{
				throw new SerializationException("Error parsing JSON", innerException);
			}
			return result;
		}

		// Token: 0x060001C7 RID: 455 RVA: 0x00008574 File Offset: 0x00006774
		internal static string CamelCaseToSnakeCase(string s)
		{
			IEnumerable<string> source = s.Select((char a, int b) => (char.IsUpper(a) && b > 0) ? ("_" + char.ToLower(a).ToString()) : char.ToLower(a).ToString());
			return source.Aggregate((string a, string b) => a + b);
		}

		// Token: 0x060001C8 RID: 456 RVA: 0x000085D4 File Offset: 0x000067D4
		private void TryPersistCatalog(string response)
		{
			bool flag = this.m_CacheFileName == null;
			if (!flag)
			{
				try
				{
					File.WriteAllText(this.m_CacheFileName, response);
				}
				catch (Exception message)
				{
					this.m_Logger.LogError("Failed persisting IAP catalog", message);
				}
			}
		}

		// Token: 0x060001C9 RID: 457 RVA: 0x0000862C File Offset: 0x0000682C
		private HashSet<ProductDefinition> TryLoadCachedCatalog()
		{
			bool flag = this.m_CacheFileName != null && File.Exists(this.m_CacheFileName);
			if (flag)
			{
				try
				{
					string json = File.ReadAllText(this.m_CacheFileName);
					return CloudCatalogImpl.ParseProductsFromJSON(json, this.m_StoreName);
				}
				catch (Exception message)
				{
					this.m_Logger.LogError("Error loading cached catalog", message);
				}
			}
			return new HashSet<ProductDefinition>();
		}

		// Token: 0x0400011D RID: 285
		private IAsyncWebUtil m_AsyncUtil;

		// Token: 0x0400011E RID: 286
		private string m_CacheFileName;

		// Token: 0x0400011F RID: 287
		private ILogger m_Logger;

		// Token: 0x04000120 RID: 288
		private string m_CatalogURL;

		// Token: 0x04000121 RID: 289
		private string m_StoreName;

		// Token: 0x04000122 RID: 290
		private const int kMaxRetryDelayInSeconds = 300;

		// Token: 0x04000123 RID: 291
		private const string kCatalogURL = "https://catalog.iap.cloud.unity3d.com";

		// Token: 0x02000065 RID: 101
		[CompilerGenerated]
		private sealed class <>c__DisplayClass10_0
		{
			// Token: 0x060001CA RID: 458 RVA: 0x00002CA1 File Offset: 0x00000EA1
			public <>c__DisplayClass10_0()
			{
			}

			// Token: 0x060001CB RID: 459 RVA: 0x000086A4 File Offset: 0x000068A4
			internal void <FetchProducts>b__0(string response)
			{
				this.<>4__this.m_Logger.Log("Fetched catalog");
				try
				{
					HashSet<ProductDefinition> obj = CloudCatalogImpl.ParseProductsFromJSON(response, this.<>4__this.m_StoreName);
					this.<>4__this.TryPersistCatalog(response);
					this.callback(obj);
				}
				catch (SerializationException message)
				{
					this.<>4__this.m_Logger.LogError("Error parsing IAP catalog", message);
					this.<>4__this.m_Logger.Log(response);
					this.callback(this.<>4__this.TryLoadCachedCatalog());
				}
			}

			// Token: 0x060001CC RID: 460 RVA: 0x00008750 File Offset: 0x00006950
			internal void <FetchProducts>b__1(string error)
			{
				HashSet<ProductDefinition> hashSet = this.<>4__this.TryLoadCachedCatalog();
				bool flag = hashSet != null && hashSet.Count > 0;
				if (flag)
				{
					this.<>4__this.m_Logger.Log("Failed to fetch IAP catalog, using cache.");
					this.callback(hashSet);
				}
				else
				{
					this.delayInSeconds = Math.Max(5, this.delayInSeconds * 2);
					this.delayInSeconds = Math.Min(300, this.delayInSeconds);
					IAsyncWebUtil asyncUtil = this.<>4__this.m_AsyncUtil;
					Action a;
					if ((a = this.<>9__2) == null)
					{
						a = (this.<>9__2 = delegate()
						{
							this.<>4__this.FetchProducts(this.callback, this.delayInSeconds);
						});
					}
					asyncUtil.Schedule(a, this.delayInSeconds);
				}
			}

			// Token: 0x060001CD RID: 461 RVA: 0x00008806 File Offset: 0x00006A06
			internal void <FetchProducts>b__2()
			{
				this.<>4__this.FetchProducts(this.callback, this.delayInSeconds);
			}

			// Token: 0x04000124 RID: 292
			public Action<HashSet<ProductDefinition>> callback;

			// Token: 0x04000125 RID: 293
			public int delayInSeconds;

			// Token: 0x04000126 RID: 294
			public CloudCatalogImpl <>4__this;

			// Token: 0x04000127 RID: 295
			public Action <>9__2;
		}

		// Token: 0x02000066 RID: 102
		[CompilerGenerated]
		[Serializable]
		private sealed class <>c
		{
			// Token: 0x060001CE RID: 462 RVA: 0x00008820 File Offset: 0x00006A20
			// Note: this type is marked as 'beforefieldinit'.
			static <>c()
			{
			}

			// Token: 0x060001CF RID: 463 RVA: 0x00002CA1 File Offset: 0x00000EA1
			public <>c()
			{
			}

			// Token: 0x060001D0 RID: 464 RVA: 0x0000882C File Offset: 0x00006A2C
			internal string <CamelCaseToSnakeCase>b__12_0(char a, int b)
			{
				return (char.IsUpper(a) && b > 0) ? ("_" + char.ToLower(a).ToString()) : char.ToLower(a).ToString();
			}

			// Token: 0x060001D1 RID: 465 RVA: 0x0000886D File Offset: 0x00006A6D
			internal string <CamelCaseToSnakeCase>b__12_1(string a, string b)
			{
				return a + b;
			}

			// Token: 0x04000128 RID: 296
			public static readonly CloudCatalogImpl.<>c <>9 = new CloudCatalogImpl.<>c();

			// Token: 0x04000129 RID: 297
			public static Func<char, int, string> <>9__12_0;

			// Token: 0x0400012A RID: 298
			public static Func<string, string, string> <>9__12_1;
		}
	}
}
