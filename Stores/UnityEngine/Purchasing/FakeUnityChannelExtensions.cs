﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x0200003D RID: 61
	public class FakeUnityChannelExtensions : IUnityChannelExtensions, IStoreExtension
	{
		// Token: 0x060000E6 RID: 230 RVA: 0x00004FD7 File Offset: 0x000031D7
		public void ConfirmPurchase(string transactionId, Action<bool, string, string> callback)
		{
			callback(true, "fakeTransactionId", "fakeMessage");
		}

		// Token: 0x060000E7 RID: 231 RVA: 0x00004FEC File Offset: 0x000031EC
		public void ValidateReceipt(string transactionId, Action<bool, string, string> callback)
		{
			callback(true, "fakeSignData", "fakeSignature");
		}

		// Token: 0x060000E8 RID: 232 RVA: 0x00005004 File Offset: 0x00003204
		public string GetLastPurchaseError()
		{
			return "{ \"error\": \"DuplicateTransaction\" }";
		}

		// Token: 0x060000E9 RID: 233 RVA: 0x00002CA1 File Offset: 0x00000EA1
		public FakeUnityChannelExtensions()
		{
		}
	}
}
