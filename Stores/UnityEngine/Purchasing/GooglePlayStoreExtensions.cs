﻿using System;
using System.Collections.Generic;
using UnityEngine.Purchasing.Extension;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000031 RID: 49
	internal class GooglePlayStoreExtensions : AndroidJavaProxy, IGooglePlayStoreExtensions, IStoreExtension, IGooglePlayConfiguration, IStoreConfiguration
	{
		// Token: 0x060000C9 RID: 201 RVA: 0x00004DEE File Offset: 0x00002FEE
		public GooglePlayStoreExtensions() : base("com.unity.purchasing.googleplay.GooglePlayPurchasing")
		{
		}

		// Token: 0x060000CA RID: 202 RVA: 0x00004DFD File Offset: 0x00002FFD
		public void SetAndroidJavaObject(AndroidJavaObject java)
		{
			this.m_Java = java;
		}

		// Token: 0x060000CB RID: 203 RVA: 0x00002EA8 File Offset: 0x000010A8
		public void SetPublicKey(string key)
		{
		}

		// Token: 0x060000CC RID: 204 RVA: 0x00004E07 File Offset: 0x00003007
		public void UpgradeDowngradeSubscription(string oldSku, string newSku)
		{
			this.m_Java.Call("UpgradeDowngradeSubscription", new object[]
			{
				oldSku,
				newSku
			});
		}

		// Token: 0x060000CD RID: 205 RVA: 0x00004E2C File Offset: 0x0000302C
		public Dictionary<string, string> GetProductJSONDictionary()
		{
			string json = this.m_Java.Get<string>("productJSON");
			Dictionary<string, string> dictionary = new Dictionary<string, string>();
			Dictionary<string, object> dictionary2 = (Dictionary<string, object>)MiniJson.JsonDecode(json);
			foreach (KeyValuePair<string, object> keyValuePair in dictionary2)
			{
				dictionary.Add(keyValuePair.Key, (string)keyValuePair.Value);
			}
			return dictionary;
		}

		// Token: 0x060000CE RID: 206 RVA: 0x00004EC0 File Offset: 0x000030C0
		public void RestoreTransactions(Action<bool> callback)
		{
			this.m_Java.Call("RestoreTransactions", new object[]
			{
				new GooglePlayStoreCallback(callback)
			});
		}

		// Token: 0x060000CF RID: 207 RVA: 0x00004EE3 File Offset: 0x000030E3
		public void FinishAdditionalTransaction(string productId, string transactionId)
		{
			this.m_Java.Call("FinishAdditionalTransaction", new object[]
			{
				productId,
				transactionId
			});
		}

		// Token: 0x040000A8 RID: 168
		private AndroidJavaObject m_Java;
	}
}
