﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.CompilerServices;
using Uniject;
using UnityEngine.Purchasing.Default;
using UnityEngine.Purchasing.Extension;

namespace UnityEngine.Purchasing
{
	// Token: 0x020000A7 RID: 167
	internal class WinRTStore : AbstractStore, IWindowsIAPCallback, IMicrosoftExtensions, IStoreExtension
	{
		// Token: 0x06000322 RID: 802 RVA: 0x0000D32C File Offset: 0x0000B52C
		public WinRTStore(IWindowsIAP win8, IUtil util, ILogger logger)
		{
			this.win8 = win8;
			this.util = util;
			this.logger = logger;
		}

		// Token: 0x06000323 RID: 803 RVA: 0x0000D352 File Offset: 0x0000B552
		public void SetWindowsIAP(IWindowsIAP iap)
		{
			this.win8 = iap;
		}

		// Token: 0x06000324 RID: 804 RVA: 0x0000D35C File Offset: 0x0000B55C
		public override void Initialize(IStoreCallback biller)
		{
			this.callback = biller;
		}

		// Token: 0x06000325 RID: 805 RVA: 0x0000D368 File Offset: 0x0000B568
		public override void RetrieveProducts(ReadOnlyCollection<ProductDefinition> productDefs)
		{
			IEnumerable<WinProductDescription> source = from def in productDefs
			where def.type != ProductType.Subscription
			select new WinProductDescription(def.storeSpecificId, "$0.01", "Fake title - " + def.storeSpecificId, "Fake description - " + def.storeSpecificId, "USD", 0.01m, null, null, def.type == ProductType.Consumable);
			this.win8.BuildDummyProducts(source.ToList<WinProductDescription>());
			this.init(0);
		}

		// Token: 0x06000326 RID: 806 RVA: 0x0000D3DA File Offset: 0x0000B5DA
		public override void FinishTransaction(ProductDefinition product, string transactionId)
		{
			this.win8.FinaliseTransaction(transactionId);
		}

		// Token: 0x06000327 RID: 807 RVA: 0x0000D3EA File Offset: 0x0000B5EA
		private void init(int delay)
		{
			this.win8.Initialize(this);
			this.win8.RetrieveProducts(true);
		}

		// Token: 0x06000328 RID: 808 RVA: 0x0000D407 File Offset: 0x0000B607
		public override void Purchase(ProductDefinition product, string developerPayload)
		{
			this.win8.Purchase(product.storeSpecificId);
		}

		// Token: 0x06000329 RID: 809 RVA: 0x0000D41C File Offset: 0x0000B61C
		public void restoreTransactions(bool pausing)
		{
			bool flag = !pausing;
			if (flag)
			{
				bool canReceivePurchases = this.m_CanReceivePurchases;
				if (canReceivePurchases)
				{
					this.win8.RetrieveProducts(false);
				}
			}
		}

		// Token: 0x0600032A RID: 810 RVA: 0x0000D44D File Offset: 0x0000B64D
		public void RestoreTransactions()
		{
			this.logger.Log("Explicit RestoreTransactions()");
			this.win8.RetrieveProducts(false);
			this.m_CanReceivePurchases = true;
		}

		// Token: 0x0600032B RID: 811 RVA: 0x0000D475 File Offset: 0x0000B675
		public void logError(string error)
		{
			this.logger.LogError("Unity Purchasing", error);
		}

		// Token: 0x0600032C RID: 812 RVA: 0x0000D48C File Offset: 0x0000B68C
		public void OnProductListReceived(WinProductDescription[] winProducts)
		{
			this.util.RunOnMainThread(delegate
			{
				IEnumerable<ProductDescription> source = from product in winProducts
				let metadata = new ProductMetadata(product.price, product.title, product.description, product.ISOCurrencyCode, product.priceDecimal)
				select new ProductDescription(product.platformSpecificID, metadata, product.receipt, product.transactionID);
				this.callback.OnProductsRetrieved(source.ToList<ProductDescription>());
			});
		}

		// Token: 0x0600032D RID: 813 RVA: 0x0000D4C8 File Offset: 0x0000B6C8
		public void log(string message)
		{
			this.util.RunOnMainThread(delegate
			{
				this.logger.Log(message);
			});
		}

		// Token: 0x0600032E RID: 814 RVA: 0x0000D504 File Offset: 0x0000B704
		public void OnPurchaseFailed(string productId, string error)
		{
			this.util.RunOnMainThread(delegate
			{
				this.logger.LogFormat(LogType.Error, "Purchase failed: {0}, {1}", new object[]
				{
					productId,
					error
				});
				bool flag = "AlreadyPurchased" == error;
				if (flag)
				{
					try
					{
						this.callback.OnPurchaseFailed(new PurchaseFailureDescription(productId, (PurchaseFailureReason)Enum.Parse(typeof(PurchaseFailureReason), "DuplicateTransaction"), error));
					}
					catch
					{
						this.callback.OnPurchaseFailed(new PurchaseFailureDescription(productId, (PurchaseFailureReason)Enum.Parse(typeof(PurchaseFailureReason), "Unknown"), error));
					}
				}
				else
				{
					bool flag2 = "NotPurchased" == error;
					if (flag2)
					{
						this.callback.OnPurchaseFailed(new PurchaseFailureDescription(productId, (PurchaseFailureReason)Enum.Parse(typeof(PurchaseFailureReason), "UserCancelled"), error));
					}
					else
					{
						this.callback.OnPurchaseFailed(new PurchaseFailureDescription(productId, (PurchaseFailureReason)Enum.Parse(typeof(PurchaseFailureReason), "Unknown"), error));
					}
				}
			});
		}

		// Token: 0x0600032F RID: 815 RVA: 0x0000D548 File Offset: 0x0000B748
		public void OnPurchaseSucceeded(string productId, string receipt, string tranId)
		{
			this.util.RunOnMainThread(delegate
			{
				this.logger.Log("PURCHASE SUCCEEDED!:{0}", WinRTStore.count++);
				this.m_CanReceivePurchases = true;
				this.callback.OnPurchaseSucceeded(productId, receipt, tranId);
			});
		}

		// Token: 0x06000330 RID: 816 RVA: 0x0000D590 File Offset: 0x0000B790
		public void OnProductListError(string message)
		{
			this.util.RunOnMainThread(delegate
			{
				bool flag = message.Contains("801900CC");
				if (flag)
				{
					this.callback.OnSetupFailed(InitializationFailureReason.AppNotKnown);
				}
				else
				{
					this.logError("Unable to retrieve product listings. UnityIAP will automatically retry...");
					this.logError(message);
					this.init(3000);
				}
			});
		}

		// Token: 0x04000266 RID: 614
		private IWindowsIAP win8;

		// Token: 0x04000267 RID: 615
		private IStoreCallback callback;

		// Token: 0x04000268 RID: 616
		private IUtil util;

		// Token: 0x04000269 RID: 617
		private ILogger logger;

		// Token: 0x0400026A RID: 618
		private bool m_CanReceivePurchases = false;

		// Token: 0x0400026B RID: 619
		private static int count;

		// Token: 0x020000A8 RID: 168
		[CompilerGenerated]
		[Serializable]
		private sealed class <>c
		{
			// Token: 0x06000331 RID: 817 RVA: 0x0000D5CA File Offset: 0x0000B7CA
			// Note: this type is marked as 'beforefieldinit'.
			static <>c()
			{
			}

			// Token: 0x06000332 RID: 818 RVA: 0x00002CA1 File Offset: 0x00000EA1
			public <>c()
			{
			}

			// Token: 0x06000333 RID: 819 RVA: 0x0000D5D6 File Offset: 0x0000B7D6
			internal bool <RetrieveProducts>b__8_0(ProductDefinition def)
			{
				return def.type != ProductType.Subscription;
			}

			// Token: 0x06000334 RID: 820 RVA: 0x0000D5E4 File Offset: 0x0000B7E4
			internal WinProductDescription <RetrieveProducts>b__8_1(ProductDefinition def)
			{
				return new WinProductDescription(def.storeSpecificId, "$0.01", "Fake title - " + def.storeSpecificId, "Fake description - " + def.storeSpecificId, "USD", 0.01m, null, null, def.type == ProductType.Consumable);
			}

			// Token: 0x06000335 RID: 821 RVA: 0x0000D63B File Offset: 0x0000B83B
			internal <>f__AnonymousType0<WinProductDescription, ProductMetadata> <OnProductListReceived>b__15_1(WinProductDescription product)
			{
				return new
				{
					product = product,
					metadata = new ProductMetadata(product.price, product.title, product.description, product.ISOCurrencyCode, product.priceDecimal)
				};
			}

			// Token: 0x06000336 RID: 822 RVA: 0x0000D666 File Offset: 0x0000B866
			internal ProductDescription <OnProductListReceived>b__15_2(<>f__AnonymousType0<WinProductDescription, ProductMetadata> <>h__TransparentIdentifier0)
			{
				return new ProductDescription(<>h__TransparentIdentifier0.product.platformSpecificID, <>h__TransparentIdentifier0.metadata, <>h__TransparentIdentifier0.product.receipt, <>h__TransparentIdentifier0.product.transactionID);
			}

			// Token: 0x0400026C RID: 620
			public static readonly WinRTStore.<>c <>9 = new WinRTStore.<>c();

			// Token: 0x0400026D RID: 621
			public static Func<ProductDefinition, bool> <>9__8_0;

			// Token: 0x0400026E RID: 622
			public static Func<ProductDefinition, WinProductDescription> <>9__8_1;

			// Token: 0x0400026F RID: 623
			public static Func<WinProductDescription, <>f__AnonymousType0<WinProductDescription, ProductMetadata>> <>9__15_1;

			// Token: 0x04000270 RID: 624
			public static Func<<>f__AnonymousType0<WinProductDescription, ProductMetadata>, ProductDescription> <>9__15_2;
		}

		// Token: 0x020000A9 RID: 169
		[CompilerGenerated]
		private sealed class <>c__DisplayClass15_0
		{
			// Token: 0x06000337 RID: 823 RVA: 0x00002CA1 File Offset: 0x00000EA1
			public <>c__DisplayClass15_0()
			{
			}

			// Token: 0x06000338 RID: 824 RVA: 0x0000D694 File Offset: 0x0000B894
			internal void <OnProductListReceived>b__0()
			{
				IEnumerable<ProductDescription> source = this.winProducts.Select(new Func<WinProductDescription, <>f__AnonymousType0<WinProductDescription, ProductMetadata>>(WinRTStore.<>c.<>9.<OnProductListReceived>b__15_1)).Select(new Func<<>f__AnonymousType0<WinProductDescription, ProductMetadata>, ProductDescription>(WinRTStore.<>c.<>9.<OnProductListReceived>b__15_2));
				this.<>4__this.callback.OnProductsRetrieved(source.ToList<ProductDescription>());
			}

			// Token: 0x04000271 RID: 625
			public WinProductDescription[] winProducts;

			// Token: 0x04000272 RID: 626
			public WinRTStore <>4__this;
		}

		// Token: 0x020000AA RID: 170
		[CompilerGenerated]
		private sealed class <>c__DisplayClass16_0
		{
			// Token: 0x06000339 RID: 825 RVA: 0x00002CA1 File Offset: 0x00000EA1
			public <>c__DisplayClass16_0()
			{
			}

			// Token: 0x0600033A RID: 826 RVA: 0x0000D708 File Offset: 0x0000B908
			internal void <log>b__0()
			{
				this.<>4__this.logger.Log(this.message);
			}

			// Token: 0x04000273 RID: 627
			public string message;

			// Token: 0x04000274 RID: 628
			public WinRTStore <>4__this;
		}

		// Token: 0x020000AB RID: 171
		[CompilerGenerated]
		private sealed class <>c__DisplayClass17_0
		{
			// Token: 0x0600033B RID: 827 RVA: 0x00002CA1 File Offset: 0x00000EA1
			public <>c__DisplayClass17_0()
			{
			}

			// Token: 0x0600033C RID: 828 RVA: 0x0000D724 File Offset: 0x0000B924
			internal void <OnPurchaseFailed>b__0()
			{
				this.<>4__this.logger.LogFormat(LogType.Error, "Purchase failed: {0}, {1}", new object[]
				{
					this.productId,
					this.error
				});
				bool flag = "AlreadyPurchased" == this.error;
				if (flag)
				{
					try
					{
						this.<>4__this.callback.OnPurchaseFailed(new PurchaseFailureDescription(this.productId, (PurchaseFailureReason)Enum.Parse(typeof(PurchaseFailureReason), "DuplicateTransaction"), this.error));
					}
					catch
					{
						this.<>4__this.callback.OnPurchaseFailed(new PurchaseFailureDescription(this.productId, (PurchaseFailureReason)Enum.Parse(typeof(PurchaseFailureReason), "Unknown"), this.error));
					}
				}
				else
				{
					bool flag2 = "NotPurchased" == this.error;
					if (flag2)
					{
						this.<>4__this.callback.OnPurchaseFailed(new PurchaseFailureDescription(this.productId, (PurchaseFailureReason)Enum.Parse(typeof(PurchaseFailureReason), "UserCancelled"), this.error));
					}
					else
					{
						this.<>4__this.callback.OnPurchaseFailed(new PurchaseFailureDescription(this.productId, (PurchaseFailureReason)Enum.Parse(typeof(PurchaseFailureReason), "Unknown"), this.error));
					}
				}
			}

			// Token: 0x04000275 RID: 629
			public string productId;

			// Token: 0x04000276 RID: 630
			public string error;

			// Token: 0x04000277 RID: 631
			public WinRTStore <>4__this;
		}

		// Token: 0x020000AC RID: 172
		[CompilerGenerated]
		private sealed class <>c__DisplayClass19_0
		{
			// Token: 0x0600033D RID: 829 RVA: 0x00002CA1 File Offset: 0x00000EA1
			public <>c__DisplayClass19_0()
			{
			}

			// Token: 0x0600033E RID: 830 RVA: 0x0000D8A0 File Offset: 0x0000BAA0
			internal void <OnPurchaseSucceeded>b__0()
			{
				this.<>4__this.logger.Log("PURCHASE SUCCEEDED!:{0}", WinRTStore.count++);
				this.<>4__this.m_CanReceivePurchases = true;
				this.<>4__this.callback.OnPurchaseSucceeded(this.productId, this.receipt, this.tranId);
			}

			// Token: 0x04000278 RID: 632
			public string productId;

			// Token: 0x04000279 RID: 633
			public string receipt;

			// Token: 0x0400027A RID: 634
			public string tranId;

			// Token: 0x0400027B RID: 635
			public WinRTStore <>4__this;
		}

		// Token: 0x020000AD RID: 173
		[CompilerGenerated]
		private sealed class <>c__DisplayClass20_0
		{
			// Token: 0x0600033F RID: 831 RVA: 0x00002CA1 File Offset: 0x00000EA1
			public <>c__DisplayClass20_0()
			{
			}

			// Token: 0x06000340 RID: 832 RVA: 0x0000D908 File Offset: 0x0000BB08
			internal void <OnProductListError>b__0()
			{
				bool flag = this.message.Contains("801900CC");
				if (flag)
				{
					this.<>4__this.callback.OnSetupFailed(InitializationFailureReason.AppNotKnown);
				}
				else
				{
					this.<>4__this.logError("Unable to retrieve product listings. UnityIAP will automatically retry...");
					this.<>4__this.logError(this.message);
					this.<>4__this.init(3000);
				}
			}

			// Token: 0x0400027C RID: 636
			public string message;

			// Token: 0x0400027D RID: 637
			public WinRTStore <>4__this;
		}
	}
}
