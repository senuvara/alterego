﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x0200006B RID: 107
	public interface IManagedStoreExtensions : IStoreExtension
	{
		// Token: 0x1700004B RID: 75
		// (get) Token: 0x060001EA RID: 490
		Product[] storeCatalog { get; }

		// Token: 0x060001EB RID: 491
		void RefreshCatalog(Action callback);
	}
}
