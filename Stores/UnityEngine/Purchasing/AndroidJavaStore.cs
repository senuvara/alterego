﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000004 RID: 4
	internal class AndroidJavaStore : INativeStore
	{
		// Token: 0x06000020 RID: 32 RVA: 0x0000218C File Offset: 0x0000038C
		protected AndroidJavaObject GetStore()
		{
			return this.m_Store;
		}

		// Token: 0x06000021 RID: 33 RVA: 0x000021A4 File Offset: 0x000003A4
		public AndroidJavaStore(AndroidJavaObject store)
		{
			this.m_Store = store;
		}

		// Token: 0x06000022 RID: 34 RVA: 0x000021B5 File Offset: 0x000003B5
		public void RetrieveProducts(string json)
		{
			this.m_Store.Call("RetrieveProducts", new object[]
			{
				json
			});
		}

		// Token: 0x06000023 RID: 35 RVA: 0x000021D3 File Offset: 0x000003D3
		public virtual void Purchase(string productJSON, string developerPayload)
		{
			this.m_Store.Call("Purchase", new object[]
			{
				productJSON,
				developerPayload
			});
		}

		// Token: 0x06000024 RID: 36 RVA: 0x000021F5 File Offset: 0x000003F5
		public void FinishTransaction(string productJSON, string transactionID)
		{
			this.m_Store.Call("FinishTransaction", new object[]
			{
				productJSON,
				transactionID
			});
		}

		// Token: 0x04000003 RID: 3
		private AndroidJavaObject m_Store;
	}
}
