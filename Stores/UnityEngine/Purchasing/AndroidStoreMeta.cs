﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000006 RID: 6
	public enum AndroidStoreMeta
	{
		// Token: 0x0400000D RID: 13
		AndroidStoreStart,
		// Token: 0x0400000E RID: 14
		AndroidStoreEnd = 5
	}
}
