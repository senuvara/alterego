﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x0200001D RID: 29
	public enum ValidateReceiptState
	{
		// Token: 0x04000033 RID: 51
		ValidateSucceed,
		// Token: 0x04000034 RID: 52
		ValidateFailed,
		// Token: 0x04000035 RID: 53
		NotKnown
	}
}
