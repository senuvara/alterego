﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000097 RID: 151
	public enum Result
	{
		// Token: 0x04000253 RID: 595
		True,
		// Token: 0x04000254 RID: 596
		False,
		// Token: 0x04000255 RID: 597
		Unsupported
	}
}
