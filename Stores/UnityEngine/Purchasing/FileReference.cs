﻿using System;
using System.IO;
using Uniject;

namespace UnityEngine.Purchasing
{
	// Token: 0x020000C1 RID: 193
	internal class FileReference
	{
		// Token: 0x06000386 RID: 902 RVA: 0x0000E948 File Offset: 0x0000CB48
		internal static FileReference CreateInstance(string filename, ILogger logger, IUtil util)
		{
			FileReference result;
			try
			{
				string path = Path.Combine(util.persistentDataPath, "Unity");
				string path2 = Path.Combine(util.cloudProjectId, "IAP");
				string text = Path.Combine(path, path2);
				Directory.CreateDirectory(text);
				string filePath = Path.Combine(text, filename);
				result = new FileReference(filePath, logger);
			}
			catch
			{
				result = null;
			}
			return result;
		}

		// Token: 0x06000387 RID: 903 RVA: 0x0000E9B4 File Offset: 0x0000CBB4
		internal FileReference(string filePath, ILogger logger)
		{
			this.m_FilePath = filePath;
			this.m_Logger = logger;
		}

		// Token: 0x06000388 RID: 904 RVA: 0x0000E9CC File Offset: 0x0000CBCC
		internal void Save(string payload)
		{
			try
			{
				File.WriteAllText(this.m_FilePath, payload);
			}
			catch (Exception message)
			{
				this.m_Logger.LogError("Failed persisting content", message);
			}
		}

		// Token: 0x06000389 RID: 905 RVA: 0x0000EA14 File Offset: 0x0000CC14
		internal string Load()
		{
			string result;
			try
			{
				result = File.ReadAllText(this.m_FilePath);
			}
			catch
			{
				result = null;
			}
			return result;
		}

		// Token: 0x0600038A RID: 906 RVA: 0x0000EA48 File Offset: 0x0000CC48
		internal void Delete()
		{
			try
			{
				File.Delete(this.m_FilePath);
			}
			catch (Exception message)
			{
				this.m_Logger.Log("Failed deleting cached content", message);
			}
		}

		// Token: 0x040002AF RID: 687
		private string m_FilePath;

		// Token: 0x040002B0 RID: 688
		private ILogger m_Logger;
	}
}
