﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine.Purchasing.Extension;
using UnityEngine.Purchasing.MiniJSON;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000062 RID: 98
	internal class JSONStore : AbstractStore, IUnityCallback, IManagedStoreExtensions, IStoreExtension, IStoreInternal, IManagedStoreConfig, IStoreConfiguration, ITransactionHistoryExtensions
	{
		// Token: 0x1700003A RID: 58
		// (get) Token: 0x0600019E RID: 414 RVA: 0x00007148 File Offset: 0x00005348
		public Product[] storeCatalog
		{
			get
			{
				List<Product> list = new List<Product>();
				bool flag = this.m_storeCatalog != null && this.unity.products.all != null;
				if (flag)
				{
					foreach (ProductDefinition productDefinition in this.m_storeCatalog)
					{
						foreach (Product product in this.unity.products.all)
						{
							bool flag2 = false;
							bool flag3 = product.definition.type > ProductType.Consumable;
							if (flag3)
							{
								bool flag4 = product.hasReceipt || !string.IsNullOrEmpty(product.transactionID);
								if (flag4)
								{
									flag2 = true;
								}
							}
							bool flag5 = product.availableToPurchase && !flag2 && product.definition.storeSpecificId == productDefinition.storeSpecificId;
							if (flag5)
							{
								list.Add(product);
							}
						}
					}
				}
				return list.ToArray();
			}
		}

		// Token: 0x1700003B RID: 59
		// (get) Token: 0x0600019F RID: 415 RVA: 0x00007288 File Offset: 0x00005488
		// (set) Token: 0x060001A0 RID: 416 RVA: 0x000072A0 File Offset: 0x000054A0
		public bool disableStoreCatalog
		{
			get
			{
				return this.catalogDisabled;
			}
			set
			{
				if (value)
				{
					this.catalogDisabled = true;
					this.isManagedStoreEnabled = false;
					bool flag = this.m_Logger != null;
					if (flag)
					{
						this.m_Logger.LogWarning("UnityIAP", "Disabling store optimization");
					}
				}
				else
				{
					this.catalogDisabled = false;
					this.isManagedStoreEnabled = true;
					bool flag2 = this.m_Logger != null;
					if (flag2)
					{
						this.m_Logger.Log("UnityIAP", "Enabling store optimization");
					}
				}
			}
		}

		// Token: 0x1700003C RID: 60
		// (get) Token: 0x060001A1 RID: 417 RVA: 0x00007320 File Offset: 0x00005520
		// (set) Token: 0x060001A2 RID: 418 RVA: 0x0000734C File Offset: 0x0000554C
		public bool? trackingOptOut
		{
			get
			{
				ProfileData profileData = ProfileData.Instance(this.m_Module.util);
				return profileData.TrackingOptOut;
			}
			set
			{
				ProfileData profileData = ProfileData.Instance(this.m_Module.util);
				profileData.SetTrackingOptOut(value);
			}
		}

		// Token: 0x1700003D RID: 61
		// (get) Token: 0x060001A3 RID: 419 RVA: 0x00007374 File Offset: 0x00005574
		// (set) Token: 0x060001A4 RID: 420 RVA: 0x0000738C File Offset: 0x0000558C
		public bool storeTestEnabled
		{
			get
			{
				return this.testStore;
			}
			set
			{
				bool flag = !this.testStore;
				if (flag)
				{
					this.testStore = value;
					ProfileData profileData = ProfileData.Instance(this.m_Module.util);
					profileData.SetStoreTestEnabled(value);
				}
			}
		}

		// Token: 0x1700003E RID: 62
		// (get) Token: 0x060001A5 RID: 421 RVA: 0x000073CC File Offset: 0x000055CC
		// (set) Token: 0x060001A6 RID: 422 RVA: 0x000073E4 File Offset: 0x000055E4
		public string baseIapUrl
		{
			get
			{
				return this.iapBaseUrl;
			}
			set
			{
				bool flag = this.iapBaseUrl == null && !string.IsNullOrEmpty(value);
				if (flag)
				{
					this.storeTestEnabled = true;
					this.iapBaseUrl = value;
				}
			}
		}

		// Token: 0x1700003F RID: 63
		// (get) Token: 0x060001A7 RID: 423 RVA: 0x0000741C File Offset: 0x0000561C
		// (set) Token: 0x060001A8 RID: 424 RVA: 0x00007434 File Offset: 0x00005634
		public string baseEventUrl
		{
			get
			{
				return this.eventBaseUrl;
			}
			set
			{
				bool flag = !string.IsNullOrEmpty(value);
				if (flag)
				{
					this.storeTestEnabled = true;
					this.eventBaseUrl = value;
				}
			}
		}

		// Token: 0x060001A9 RID: 425 RVA: 0x00007460 File Offset: 0x00005660
		public JSONStore()
		{
		}

		// Token: 0x060001AA RID: 426 RVA: 0x000074CF File Offset: 0x000056CF
		public void SetNativeStore(INativeStore native)
		{
			this.store = native;
		}

		// Token: 0x060001AB RID: 427 RVA: 0x000074DC File Offset: 0x000056DC
		void IStoreInternal.SetModule(StandardPurchasingModule module)
		{
			bool flag = module == null;
			if (!flag)
			{
				this.m_Module = module;
				bool flag2 = module.logger != null;
				if (flag2)
				{
					this.m_Logger = module.logger;
				}
				else
				{
					this.m_Logger = Debug.unityLogger;
				}
			}
		}

		// Token: 0x060001AC RID: 428 RVA: 0x00007528 File Offset: 0x00005728
		public override void Initialize(IStoreCallback callback)
		{
			this.unity = callback;
			this.m_EventQueue = EventQueue.Instance(this.m_Module.util, this.m_Module.webUtil);
			this.m_profileData = ProfileData.Instance(this.m_Module.util);
			bool flag = this.m_Module != null;
			if (flag)
			{
				string storeName = this.m_Module.storeInstance.storeName;
				this.m_profileData.SetStoreName(storeName);
				bool flag2 = string.IsNullOrEmpty(this.iapBaseUrl);
				if (flag2)
				{
					this.iapBaseUrl = "https://ecommerce.iap.unity3d.com";
				}
				this.m_managedStore = StoreCatalogImpl.CreateInstance(storeName, this.iapBaseUrl, this.m_Module.webUtil, this.m_Module.logger, this.m_Module.util);
			}
			else
			{
				bool flag3 = this.m_Logger != null;
				if (flag3)
				{
					this.m_Logger.LogWarning("UnityIAP", "JSONStore init has no reference to SPM, can't start managed store");
				}
			}
		}

		// Token: 0x060001AD RID: 429 RVA: 0x0000761C File Offset: 0x0000581C
		public override void RetrieveProducts(ReadOnlyCollection<ProductDefinition> products)
		{
			bool flag = this.isManagedStoreEnabled && this.m_managedStore != null && (this.isRefreshing || this.isFirstTimeRetrievingProducts);
			if (flag)
			{
				this.m_BuilderProducts = new HashSet<ProductDefinition>(products);
				this.m_managedStore.FetchProducts(new Action<List<ProductDefinition>>(this.ProcessManagedStoreResponse));
			}
			else
			{
				this.store.RetrieveProducts(JSONSerializer.SerializeProductDefs(products));
			}
			this.isFirstTimeRetrievingProducts = false;
		}

		// Token: 0x060001AE RID: 430 RVA: 0x00007698 File Offset: 0x00005898
		internal void ProcessManagedStoreResponse(List<ProductDefinition> storeProducts)
		{
			this.m_storeCatalog = storeProducts;
			bool flag = this.isRefreshing;
			if (flag)
			{
				this.isRefreshing = false;
				bool flag2 = this.storeCatalog.Length == 0 && this.refreshCallback != null;
				if (flag2)
				{
					this.refreshCallback();
					this.refreshCallback = null;
					return;
				}
			}
			HashSet<ProductDefinition> hashSet = new HashSet<ProductDefinition>(this.m_BuilderProducts);
			bool flag3 = storeProducts != null;
			if (flag3)
			{
				hashSet.UnionWith(storeProducts);
			}
			this.store.RetrieveProducts(JSONSerializer.SerializeProductDefs(hashSet));
		}

		// Token: 0x060001AF RID: 431 RVA: 0x00007720 File Offset: 0x00005920
		public override void Purchase(ProductDefinition product, string developerPayload)
		{
			bool flag = !string.IsNullOrEmpty(developerPayload);
			if (flag)
			{
				try
				{
					Dictionary<string, object> dictionary = (Dictionary<string, object>)Json.Deserialize(developerPayload);
					object obj;
					bool flag2 = dictionary != null && dictionary.ContainsKey("iapPromo") && dictionary.TryGetValue("productId", out obj);
					if (flag2)
					{
						this.m_Logger.Log("UnityIAP: Promo Purchase(" + obj + ")");
						this.promoPayload = dictionary;
						this.promoPayload.Add("type", "iap.purchase");
						this.promoPayload.Add("iap_service", true);
						Product product2 = this.unity.products.WithID(obj as string);
						this.promoPayload.Add("amount", product2.metadata.localizedPrice);
						this.promoPayload.Add("currency", product2.metadata.isoCurrencyCode);
						developerPayload = "iapPromo";
					}
				}
				catch (Exception arg)
				{
					this.m_Logger.LogWarning("UnityIAP", "JSONStore exception handling developerPayload: " + arg);
				}
			}
			this.store.Purchase(JSONSerializer.SerializeProductDef(product), developerPayload);
		}

		// Token: 0x060001B0 RID: 432 RVA: 0x00007870 File Offset: 0x00005A70
		public override void FinishTransaction(ProductDefinition product, string transactionId)
		{
			string productJSON = (product == null) ? null : JSONSerializer.SerializeProductDef(product);
			this.store.FinishTransaction(productJSON, transactionId);
		}

		// Token: 0x060001B1 RID: 433 RVA: 0x0000789C File Offset: 0x00005A9C
		public void OnSetupFailed(string reason)
		{
			InitializationFailureReason reason2 = (InitializationFailureReason)Enum.Parse(typeof(InitializationFailureReason), reason, true);
			this.unity.OnSetupFailed(reason2);
		}

		// Token: 0x060001B2 RID: 434 RVA: 0x000078CE File Offset: 0x00005ACE
		public virtual void OnProductsRetrieved(string json)
		{
			this.unity.OnProductsRetrieved(JSONSerializer.DeserializeProductDescriptions(json));
			Promo.ProvideProductsToAds(this, this.unity);
		}

		// Token: 0x060001B3 RID: 435 RVA: 0x000078F0 File Offset: 0x00005AF0
		public virtual void OnPurchaseSucceeded(string id, string receipt, string transactionID)
		{
			Product product = this.unity.products.WithStoreSpecificID(id);
			bool flag = this.promoPayload != null && (id == (string)this.promoPayload["productId"] || id == (string)this.promoPayload["storeSpecificId"]);
			if (flag)
			{
				this.promoPayload.Add("purchase", "OK");
				bool flag2 = product != null;
				if (flag2)
				{
					this.promoPayload.Add("productType", product.definition.type.ToString());
				}
				Dictionary<string, string> dictionary = new Dictionary<string, string>();
				dictionary.Add("data", this.FormatUnifiedReceipt(receipt, transactionID));
				this.promoPayload.Add("receipt", dictionary);
				PurchasingEvent purchasingEvent = new PurchasingEvent(this.promoPayload);
				Dictionary<string, object> profileDict = this.m_profileData.GetProfileDict();
				string json = purchasingEvent.FlatJSON(profileDict);
				this.m_EventQueue.SendEvent(json);
				this.promoPayload.Clear();
				this.promoPayload = null;
			}
			else
			{
				bool flag3 = product != null;
				if (flag3)
				{
					PurchasingEvent purchasingEvent2 = new PurchasingEvent(new Dictionary<string, object>
					{
						{
							"type",
							"iap.purchase"
						},
						{
							"iap_service",
							true
						},
						{
							"iapPromo",
							false
						},
						{
							"purchase",
							"OK"
						},
						{
							"productId",
							product.definition.id
						},
						{
							"storeSpecificId",
							product.definition.storeSpecificId
						},
						{
							"amount",
							product.metadata.localizedPrice
						},
						{
							"currency",
							product.metadata.isoCurrencyCode
						},
						{
							"productType",
							product.definition.type.ToString()
						},
						{
							"receipt",
							new Dictionary<string, string>
							{
								{
									"data",
									this.FormatUnifiedReceipt(receipt, transactionID)
								}
							}
						}
					});
					Dictionary<string, object> profileDict2 = this.m_profileData.GetProfileDict();
					string json2 = purchasingEvent2.FlatJSON(profileDict2);
					this.m_EventQueue.SendEvent(EventDestType.IAP, json2, this.eventBaseUrl + "/v1/organic_purchase", null);
				}
			}
			this.unity.OnPurchaseSucceeded(id, receipt, transactionID);
			Promo.ProvideProductsToAds(this, this.unity);
		}

		// Token: 0x060001B4 RID: 436 RVA: 0x00007BA0 File Offset: 0x00005DA0
		public void OnPurchaseFailed(string json)
		{
			this.OnPurchaseFailed(JSONSerializer.DeserializeFailureReason(json), json);
		}

		// Token: 0x060001B5 RID: 437 RVA: 0x00007BB4 File Offset: 0x00005DB4
		public void OnPurchaseFailed(PurchaseFailureDescription failure, string json = null)
		{
			bool flag = this.promoPayload != null;
			if (flag)
			{
				this.promoPayload["type"] = "iap.purchasefailed";
				this.promoPayload.Add("purchase", "FAILED");
				bool flag2 = json != null;
				if (flag2)
				{
					this.promoPayload.Add("failureJSON", json);
				}
				PurchasingEvent purchasingEvent = new PurchasingEvent(this.promoPayload);
				Dictionary<string, object> profileDict = this.m_profileData.GetProfileDict();
				string json2 = purchasingEvent.FlatJSON(profileDict);
				this.m_EventQueue.SendEvent(EventDestType.IAP, json2, null, null);
				this.promoPayload.Clear();
				this.promoPayload = null;
			}
			else
			{
				Product product = this.unity.products.WithStoreSpecificID(failure.productId);
				bool flag3 = product != null;
				if (flag3)
				{
					Dictionary<string, object> dictionary = new Dictionary<string, object>();
					dictionary.Add("type", "iap.purchasefailed");
					dictionary.Add("iap_service", true);
					dictionary.Add("iapPromo", false);
					dictionary.Add("purchase", "FAILED");
					dictionary.Add("productId", product.definition.id);
					dictionary.Add("storeSpecificId", product.definition.storeSpecificId);
					dictionary.Add("amount", product.metadata.localizedPrice);
					dictionary.Add("currency", product.metadata.isoCurrencyCode);
					bool flag4 = json != null;
					if (flag4)
					{
						dictionary.Add("failureJSON", json);
					}
					PurchasingEvent purchasingEvent2 = new PurchasingEvent(dictionary);
					Dictionary<string, object> profileDict2 = ProfileData.Instance(this.m_Module.util).GetProfileDict();
					string json3 = purchasingEvent2.FlatJSON(profileDict2);
					this.m_EventQueue.SendEvent(EventDestType.IAP, json3, this.eventBaseUrl + "/v1/organic_purchase", null);
				}
			}
			this.lastPurchaseFailureDescription = failure;
			this._lastPurchaseErrorCode = this.ParseStoreSpecificPurchaseErrorCode(json);
			this.unity.OnPurchaseFailed(failure);
		}

		// Token: 0x060001B6 RID: 438 RVA: 0x00007DE4 File Offset: 0x00005FE4
		public void RefreshCatalog(Action callback)
		{
			bool flag = this.isManagedStoreEnabled;
			if (flag)
			{
				this.isRefreshing = true;
				this.refreshCallback = callback;
				PurchasingManager purchasingManager = this.unity as PurchasingManager;
				purchasingManager.FetchAdditionalProducts(this.m_BuilderProducts, callback, null);
			}
			else
			{
				this.isRefreshing = false;
				this.refreshCallback = null;
				this.m_Logger.LogWarning("UnityIAP", "Unable to refresh catalog because managed store is disabled.");
				callback();
			}
		}

		// Token: 0x060001B7 RID: 439 RVA: 0x00007E58 File Offset: 0x00006058
		public PurchaseFailureDescription GetLastPurchaseFailureDescription()
		{
			return this.lastPurchaseFailureDescription;
		}

		// Token: 0x060001B8 RID: 440 RVA: 0x00007E70 File Offset: 0x00006070
		public StoreSpecificPurchaseErrorCode GetLastStoreSpecificPurchaseErrorCode()
		{
			return this._lastPurchaseErrorCode;
		}

		// Token: 0x060001B9 RID: 441 RVA: 0x00007E88 File Offset: 0x00006088
		private string FormatUnifiedReceipt(string platformReceipt, string transactionId)
		{
			Dictionary<string, object> dictionary = new Dictionary<string, object>();
			bool flag = this.m_Module != null;
			if (flag)
			{
				dictionary["Store"] = this.m_Module.storeInstance.storeName;
			}
			else
			{
				dictionary["Store"] = "unknown";
			}
			dictionary["TransactionID"] = (transactionId ?? string.Empty);
			dictionary["Payload"] = (platformReceipt ?? string.Empty);
			return Json.Serialize(dictionary);
		}

		// Token: 0x060001BA RID: 442 RVA: 0x00007F14 File Offset: 0x00006114
		private StoreSpecificPurchaseErrorCode ParseStoreSpecificPurchaseErrorCode(string json)
		{
			bool flag = json == null;
			StoreSpecificPurchaseErrorCode result;
			if (flag)
			{
				result = StoreSpecificPurchaseErrorCode.Unknown;
			}
			else
			{
				Dictionary<string, object> dictionary = MiniJson.JsonDecode(json) as Dictionary<string, object>;
				bool flag2 = dictionary != null && dictionary.ContainsKey(this.kStoreSpecificErrorCodeKey) && Enum.IsDefined(typeof(StoreSpecificPurchaseErrorCode), (string)dictionary[this.kStoreSpecificErrorCodeKey]);
				if (flag2)
				{
					string value = (string)dictionary[this.kStoreSpecificErrorCodeKey];
					result = (StoreSpecificPurchaseErrorCode)Enum.Parse(typeof(StoreSpecificPurchaseErrorCode), value);
				}
				else
				{
					result = StoreSpecificPurchaseErrorCode.Unknown;
				}
			}
			return result;
		}

		// Token: 0x04000106 RID: 262
		private StoreCatalogImpl m_managedStore;

		// Token: 0x04000107 RID: 263
		protected IStoreCallback unity;

		// Token: 0x04000108 RID: 264
		private INativeStore store;

		// Token: 0x04000109 RID: 265
		private List<ProductDefinition> m_storeCatalog;

		// Token: 0x0400010A RID: 266
		private bool isManagedStoreEnabled = true;

		// Token: 0x0400010B RID: 267
		private ProfileData m_profileData;

		// Token: 0x0400010C RID: 268
		private bool isRefreshing = false;

		// Token: 0x0400010D RID: 269
		private bool isFirstTimeRetrievingProducts = true;

		// Token: 0x0400010E RID: 270
		private Action refreshCallback;

		// Token: 0x0400010F RID: 271
		private StandardPurchasingModule m_Module;

		// Token: 0x04000110 RID: 272
		private HashSet<ProductDefinition> m_BuilderProducts = new HashSet<ProductDefinition>();

		// Token: 0x04000111 RID: 273
		protected ILogger m_Logger;

		// Token: 0x04000112 RID: 274
		private EventQueue m_EventQueue;

		// Token: 0x04000113 RID: 275
		private Dictionary<string, object> promoPayload = null;

		// Token: 0x04000114 RID: 276
		private const string kIapEventsBase = "https://events.iap.unity3d.com/events";

		// Token: 0x04000115 RID: 277
		private const string kIecCatalogBase = "https://ecommerce.iap.unity3d.com";

		// Token: 0x04000116 RID: 278
		private bool catalogDisabled = false;

		// Token: 0x04000117 RID: 279
		private bool testStore = false;

		// Token: 0x04000118 RID: 280
		private string iapBaseUrl = null;

		// Token: 0x04000119 RID: 281
		private string eventBaseUrl = "https://events.iap.unity3d.com/events";

		// Token: 0x0400011A RID: 282
		protected PurchaseFailureDescription lastPurchaseFailureDescription;

		// Token: 0x0400011B RID: 283
		private StoreSpecificPurchaseErrorCode _lastPurchaseErrorCode = StoreSpecificPurchaseErrorCode.Unknown;

		// Token: 0x0400011C RID: 284
		private string kStoreSpecificErrorCodeKey = "storeSpecificErrorCode";
	}
}
