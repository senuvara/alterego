﻿using System;
using System.Collections.Generic;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000033 RID: 51
	public interface IGooglePlayStoreExtensions : IStoreExtension
	{
		// Token: 0x060000D1 RID: 209
		void UpgradeDowngradeSubscription(string oldSku, string newSku);

		// Token: 0x060000D2 RID: 210
		Dictionary<string, string> GetProductJSONDictionary();

		// Token: 0x060000D3 RID: 211
		void RestoreTransactions(Action<bool> callback);

		// Token: 0x060000D4 RID: 212
		void FinishAdditionalTransaction(string productId, string transactionId);
	}
}
