﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine.Purchasing.Extension;
using UnityEngine.Purchasing.MiniJSON;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000045 RID: 69
	internal class UnityChannelImpl : JSONStore, IUnityChannelExtensions, IStoreExtension, IUnityChannelConfiguration, IStoreConfiguration
	{
		// Token: 0x0600010D RID: 269 RVA: 0x00005566 File Offset: 0x00003766
		public UnityChannelImpl()
		{
		}

		// Token: 0x0600010E RID: 270 RVA: 0x0000557B File Offset: 0x0000377B
		public void SetNativeStore(INativeUnityChannelStore unityChannelBindings)
		{
			base.SetNativeStore(unityChannelBindings);
			this.m_Bindings = unityChannelBindings;
		}

		// Token: 0x0600010F RID: 271 RVA: 0x0000558D File Offset: 0x0000378D
		public override void RetrieveProducts(ReadOnlyCollection<ProductDefinition> products)
		{
			this.m_Bindings.RetrieveProducts(products, delegate(bool result, string json)
			{
				this.OnProductsRetrieved(json);
			});
		}

		// Token: 0x06000110 RID: 272 RVA: 0x000055AC File Offset: 0x000037AC
		public override void Purchase(ProductDefinition product, string developerPayload)
		{
			this.m_Bindings.Purchase(product.storeSpecificId, delegate(bool purchaseSuccess, string message)
			{
				this.m_LastPurchaseError = "";
				if (purchaseSuccess)
				{
					Dictionary<string, object> dic = message.HashtableFromJson();
					string transactionId = dic.GetString("gameOrderId", "");
					string @string = dic.GetString("productCode", "");
					bool flag = !string.IsNullOrEmpty(transactionId);
					if (flag)
					{
						dic["transactionId"] = transactionId;
					}
					bool flag2 = !string.IsNullOrEmpty(@string);
					if (flag2)
					{
						dic["storeSpecificId"] = @string;
					}
					bool flag3 = !product.storeSpecificId.Equals(@string);
					if (flag3)
					{
						Debug.LogWarningFormat("UnityChannelImpl received mismatching product code for purchase. Expected {0}, received {1}.", new object[]
						{
							product.storeSpecificId,
							@string
						});
					}
					bool fetchReceiptPayloadOnPurchase = this.fetchReceiptPayloadOnPurchase;
					if (fetchReceiptPayloadOnPurchase)
					{
						this.ValidateReceipt(transactionId, delegate(bool success, string signData, string signature)
						{
							if (success)
							{
								dic["json"] = signData;
								dic["signature"] = signature;
								this.extractDeveloperPayload(dic, signData);
							}
							else
							{
								dic["json"] = (signData ?? "ValidateReceipt error");
								dic["signature"] = (signature ?? "ValidateReceipt error");
								dic["error"] = "ValidateReceipt";
							}
							string receipt2 = dic.toJson();
							this.unity.OnPurchaseSucceeded(product.storeSpecificId, receipt2, transactionId);
						});
					}
					else
					{
						string receipt = dic.toJson();
						this.unity.OnPurchaseSucceeded(product.storeSpecificId, receipt, transactionId);
					}
				}
				else
				{
					PurchaseFailureReason reason = (PurchaseFailureReason)Enum.Parse(typeof(PurchaseFailureReason), "Unknown");
					string value = reason.ToString();
					Dictionary<string, object> dictionary = message.HashtableFromJson();
					string string2 = dictionary.GetString("isRepeat", "");
					bool flag4;
					bool.TryParse(string2, out flag4);
					bool flag5 = flag4;
					if (flag5)
					{
						bool flag6 = Enum.IsDefined(typeof(PurchaseFailureReason), "DuplicateTransaction");
						if (flag6)
						{
							reason = (PurchaseFailureReason)Enum.Parse(typeof(PurchaseFailureReason), "DuplicateTransaction");
						}
						value = "DuplicateTransaction";
					}
					Dictionary<string, object> dictionary2 = new Dictionary<string, object>();
					dictionary2["error"] = value;
					bool flag7 = dictionary.ContainsKey("purchaseInfo");
					if (flag7)
					{
						dictionary2["purchaseInfo"] = dictionary["purchaseInfo"];
					}
					string lastPurchaseError = dictionary2.toJson();
					this.m_LastPurchaseError = lastPurchaseError;
					PurchaseFailureDescription desc = new PurchaseFailureDescription(product.storeSpecificId, reason, message);
					this.unity.OnPurchaseFailed(desc);
				}
			}, developerPayload);
		}

		// Token: 0x06000111 RID: 273 RVA: 0x000055F4 File Offset: 0x000037F4
		private void extractDeveloperPayload(Dictionary<string, object> dic, string signData)
		{
			Dictionary<string, object> dictionary = (Dictionary<string, object>)MiniJson.JsonDecode(signData);
			bool flag = !dictionary.ContainsKey("extension");
			if (!flag)
			{
				string text = (string)dictionary["extension"];
				bool flag2 = string.IsNullOrEmpty(text);
				if (!flag2)
				{
					Dictionary<string, object> dictionary2 = (Dictionary<string, object>)MiniJson.JsonDecode(text);
					bool flag3 = !dictionary2.ContainsKey("cpUserInfo");
					if (!flag3)
					{
						dic["developerPayload"] = (string)dictionary2["cpUserInfo"];
					}
				}
			}
		}

		// Token: 0x06000112 RID: 274 RVA: 0x00002EA8 File Offset: 0x000010A8
		public override void FinishTransaction(ProductDefinition product, string transactionId)
		{
		}

		// Token: 0x1700002F RID: 47
		// (get) Token: 0x06000113 RID: 275 RVA: 0x0000567F File Offset: 0x0000387F
		// (set) Token: 0x06000114 RID: 276 RVA: 0x00005687 File Offset: 0x00003887
		public bool fetchReceiptPayloadOnPurchase
		{
			[CompilerGenerated]
			get
			{
				return this.<fetchReceiptPayloadOnPurchase>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<fetchReceiptPayloadOnPurchase>k__BackingField = value;
			}
		}

		// Token: 0x06000115 RID: 277 RVA: 0x00005690 File Offset: 0x00003890
		public void ConfirmPurchase(string transactionId, Action<bool, string, string> callback)
		{
			this.m_Bindings.ConfirmPurchase(transactionId, callback);
		}

		// Token: 0x06000116 RID: 278 RVA: 0x000056A1 File Offset: 0x000038A1
		public void ValidateReceipt(string transactionIdentifier, Action<bool, string, string> callback)
		{
			this.m_Bindings.ValidateReceipt(transactionIdentifier, callback);
		}

		// Token: 0x06000117 RID: 279 RVA: 0x000056B4 File Offset: 0x000038B4
		public string GetLastPurchaseError()
		{
			return this.m_LastPurchaseError;
		}

		// Token: 0x06000118 RID: 280 RVA: 0x000056CC File Offset: 0x000038CC
		[CompilerGenerated]
		private void <RetrieveProducts>b__6_0(bool result, string json)
		{
			this.OnProductsRetrieved(json);
		}

		// Token: 0x040000BB RID: 187
		private const string k_DuplicateTransaction = "DuplicateTransaction";

		// Token: 0x040000BC RID: 188
		private const string k_Unknown = "Unknown";

		// Token: 0x040000BD RID: 189
		protected INativeUnityChannelStore m_Bindings;

		// Token: 0x040000BE RID: 190
		protected string m_LastPurchaseError = "";

		// Token: 0x040000BF RID: 191
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private bool <fetchReceiptPayloadOnPurchase>k__BackingField;

		// Token: 0x02000046 RID: 70
		[CompilerGenerated]
		private sealed class <>c__DisplayClass7_0
		{
			// Token: 0x06000119 RID: 281 RVA: 0x00002CA1 File Offset: 0x00000EA1
			public <>c__DisplayClass7_0()
			{
			}

			// Token: 0x0600011A RID: 282 RVA: 0x000056D8 File Offset: 0x000038D8
			internal void <Purchase>b__0(bool purchaseSuccess, string message)
			{
				this.<>4__this.m_LastPurchaseError = "";
				if (purchaseSuccess)
				{
					UnityChannelImpl.<>c__DisplayClass7_1 CS$<>8__locals1 = new UnityChannelImpl.<>c__DisplayClass7_1();
					CS$<>8__locals1.CS$<>8__locals1 = this;
					CS$<>8__locals1.dic = message.HashtableFromJson();
					CS$<>8__locals1.transactionId = CS$<>8__locals1.dic.GetString("gameOrderId", "");
					string @string = CS$<>8__locals1.dic.GetString("productCode", "");
					bool flag = !string.IsNullOrEmpty(CS$<>8__locals1.transactionId);
					if (flag)
					{
						CS$<>8__locals1.dic["transactionId"] = CS$<>8__locals1.transactionId;
					}
					bool flag2 = !string.IsNullOrEmpty(@string);
					if (flag2)
					{
						CS$<>8__locals1.dic["storeSpecificId"] = @string;
					}
					bool flag3 = !this.product.storeSpecificId.Equals(@string);
					if (flag3)
					{
						Debug.LogWarningFormat("UnityChannelImpl received mismatching product code for purchase. Expected {0}, received {1}.", new object[]
						{
							this.product.storeSpecificId,
							@string
						});
					}
					bool fetchReceiptPayloadOnPurchase = this.<>4__this.fetchReceiptPayloadOnPurchase;
					if (fetchReceiptPayloadOnPurchase)
					{
						this.<>4__this.ValidateReceipt(CS$<>8__locals1.transactionId, new Action<bool, string, string>(CS$<>8__locals1.<Purchase>b__1));
					}
					else
					{
						string receipt = CS$<>8__locals1.dic.toJson();
						this.<>4__this.unity.OnPurchaseSucceeded(this.product.storeSpecificId, receipt, CS$<>8__locals1.transactionId);
					}
				}
				else
				{
					PurchaseFailureReason reason = (PurchaseFailureReason)Enum.Parse(typeof(PurchaseFailureReason), "Unknown");
					string value = reason.ToString();
					Dictionary<string, object> dictionary = message.HashtableFromJson();
					string string2 = dictionary.GetString("isRepeat", "");
					bool flag4;
					bool.TryParse(string2, out flag4);
					bool flag5 = flag4;
					if (flag5)
					{
						bool flag6 = Enum.IsDefined(typeof(PurchaseFailureReason), "DuplicateTransaction");
						if (flag6)
						{
							reason = (PurchaseFailureReason)Enum.Parse(typeof(PurchaseFailureReason), "DuplicateTransaction");
						}
						value = "DuplicateTransaction";
					}
					Dictionary<string, object> dictionary2 = new Dictionary<string, object>();
					dictionary2["error"] = value;
					bool flag7 = dictionary.ContainsKey("purchaseInfo");
					if (flag7)
					{
						dictionary2["purchaseInfo"] = dictionary["purchaseInfo"];
					}
					string lastPurchaseError = dictionary2.toJson();
					this.<>4__this.m_LastPurchaseError = lastPurchaseError;
					PurchaseFailureDescription desc = new PurchaseFailureDescription(this.product.storeSpecificId, reason, message);
					this.<>4__this.unity.OnPurchaseFailed(desc);
				}
			}

			// Token: 0x040000C0 RID: 192
			public ProductDefinition product;

			// Token: 0x040000C1 RID: 193
			public UnityChannelImpl <>4__this;
		}

		// Token: 0x02000047 RID: 71
		[CompilerGenerated]
		private sealed class <>c__DisplayClass7_1
		{
			// Token: 0x0600011B RID: 283 RVA: 0x00002CA1 File Offset: 0x00000EA1
			public <>c__DisplayClass7_1()
			{
			}

			// Token: 0x0600011C RID: 284 RVA: 0x0000595C File Offset: 0x00003B5C
			internal void <Purchase>b__1(bool success, string signData, string signature)
			{
				if (success)
				{
					this.dic["json"] = signData;
					this.dic["signature"] = signature;
					this.CS$<>8__locals1.<>4__this.extractDeveloperPayload(this.dic, signData);
				}
				else
				{
					this.dic["json"] = (signData ?? "ValidateReceipt error");
					this.dic["signature"] = (signature ?? "ValidateReceipt error");
					this.dic["error"] = "ValidateReceipt";
				}
				string receipt = this.dic.toJson();
				this.CS$<>8__locals1.<>4__this.unity.OnPurchaseSucceeded(this.CS$<>8__locals1.product.storeSpecificId, receipt, this.transactionId);
			}

			// Token: 0x040000C2 RID: 194
			public Dictionary<string, object> dic;

			// Token: 0x040000C3 RID: 195
			public string transactionId;

			// Token: 0x040000C4 RID: 196
			public UnityChannelImpl.<>c__DisplayClass7_0 CS$<>8__locals1;
		}
	}
}
