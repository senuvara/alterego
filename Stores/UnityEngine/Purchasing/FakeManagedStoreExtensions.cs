﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000068 RID: 104
	internal class FakeManagedStoreExtensions : IManagedStoreExtensions, IStoreExtension
	{
		// Token: 0x17000045 RID: 69
		// (get) Token: 0x060001DD RID: 477 RVA: 0x000089A4 File Offset: 0x00006BA4
		public Product[] storeCatalog
		{
			get
			{
				return new Product[0];
			}
		}

		// Token: 0x060001DE RID: 478 RVA: 0x000089BC File Offset: 0x00006BBC
		public void RefreshCatalog(Action a)
		{
			a();
		}

		// Token: 0x060001DF RID: 479 RVA: 0x00002CA1 File Offset: 0x00000EA1
		public FakeManagedStoreExtensions()
		{
		}
	}
}
