﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000091 RID: 145
	internal class StoreConfiguration
	{
		// Token: 0x17000082 RID: 130
		// (get) Token: 0x060002E0 RID: 736 RVA: 0x0000BF73 File Offset: 0x0000A173
		// (set) Token: 0x060002E1 RID: 737 RVA: 0x0000BF7B File Offset: 0x0000A17B
		public AppStore androidStore
		{
			[CompilerGenerated]
			get
			{
				return this.<androidStore>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<androidStore>k__BackingField = value;
			}
		}

		// Token: 0x060002E2 RID: 738 RVA: 0x0000BF84 File Offset: 0x0000A184
		public StoreConfiguration(AppStore store)
		{
			this.androidStore = store;
		}

		// Token: 0x060002E3 RID: 739 RVA: 0x0000BF98 File Offset: 0x0000A198
		public static string Serialize(StoreConfiguration store)
		{
			Dictionary<string, object> json = new Dictionary<string, object>
			{
				{
					"androidStore",
					store.androidStore.ToString()
				}
			};
			return MiniJson.JsonEncode(json);
		}

		// Token: 0x060002E4 RID: 740 RVA: 0x0000BFD8 File Offset: 0x0000A1D8
		public static StoreConfiguration Deserialize(string json)
		{
			Dictionary<string, object> dictionary = (Dictionary<string, object>)MiniJson.JsonDecode(json);
			string value = (string)dictionary["androidStore"];
			bool flag = !Enum.IsDefined(typeof(AppStore), value);
			AppStore store;
			if (flag)
			{
				store = AppStore.GooglePlay;
			}
			else
			{
				store = (AppStore)Enum.Parse(typeof(AppStore), (string)dictionary["androidStore"], true);
			}
			return new StoreConfiguration(store);
		}

		// Token: 0x04000214 RID: 532
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private AppStore <androidStore>k__BackingField;
	}
}
