﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x0200007C RID: 124
	public enum TranslationLocale
	{
		// Token: 0x04000175 RID: 373
		zh_TW,
		// Token: 0x04000176 RID: 374
		cs_CZ,
		// Token: 0x04000177 RID: 375
		da_DK,
		// Token: 0x04000178 RID: 376
		nl_NL,
		// Token: 0x04000179 RID: 377
		en_US,
		// Token: 0x0400017A RID: 378
		fr_FR,
		// Token: 0x0400017B RID: 379
		fi_FI,
		// Token: 0x0400017C RID: 380
		de_DE,
		// Token: 0x0400017D RID: 381
		iw_IL,
		// Token: 0x0400017E RID: 382
		hi_IN,
		// Token: 0x0400017F RID: 383
		it_IT,
		// Token: 0x04000180 RID: 384
		ja_JP,
		// Token: 0x04000181 RID: 385
		ko_KR,
		// Token: 0x04000182 RID: 386
		no_NO,
		// Token: 0x04000183 RID: 387
		pl_PL,
		// Token: 0x04000184 RID: 388
		pt_PT,
		// Token: 0x04000185 RID: 389
		ru_RU,
		// Token: 0x04000186 RID: 390
		es_ES,
		// Token: 0x04000187 RID: 391
		sv_SE,
		// Token: 0x04000188 RID: 392
		zh_CN,
		// Token: 0x04000189 RID: 393
		en_AU,
		// Token: 0x0400018A RID: 394
		en_CA,
		// Token: 0x0400018B RID: 395
		en_GB,
		// Token: 0x0400018C RID: 396
		fr_CA,
		// Token: 0x0400018D RID: 397
		el_GR,
		// Token: 0x0400018E RID: 398
		id_ID,
		// Token: 0x0400018F RID: 399
		ms_MY,
		// Token: 0x04000190 RID: 400
		pt_BR,
		// Token: 0x04000191 RID: 401
		es_MX,
		// Token: 0x04000192 RID: 402
		th_TH,
		// Token: 0x04000193 RID: 403
		tr_TR,
		// Token: 0x04000194 RID: 404
		vi_VN
	}
}
