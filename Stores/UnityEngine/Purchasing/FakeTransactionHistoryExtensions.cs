﻿using System;
using UnityEngine.Purchasing.Extension;

namespace UnityEngine.Purchasing
{
	// Token: 0x0200009F RID: 159
	internal class FakeTransactionHistoryExtensions : ITransactionHistoryExtensions, IStoreExtension
	{
		// Token: 0x06000315 RID: 789 RVA: 0x0000D304 File Offset: 0x0000B504
		public PurchaseFailureDescription GetLastPurchaseFailureDescription()
		{
			return null;
		}

		// Token: 0x06000316 RID: 790 RVA: 0x0000D318 File Offset: 0x0000B518
		public StoreSpecificPurchaseErrorCode GetLastStoreSpecificPurchaseErrorCode()
		{
			return StoreSpecificPurchaseErrorCode.Unknown;
		}

		// Token: 0x06000317 RID: 791 RVA: 0x00002CA1 File Offset: 0x00000EA1
		public FakeTransactionHistoryExtensions()
		{
		}
	}
}
