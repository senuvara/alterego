﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000037 RID: 55
	public interface ISamsungAppsExtensions : IStoreExtension
	{
		// Token: 0x060000DA RID: 218
		void RestoreTransactions(Action<bool> callback);
	}
}
