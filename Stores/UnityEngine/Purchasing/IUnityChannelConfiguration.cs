﻿using System;
using UnityEngine.Purchasing.Extension;

namespace UnityEngine.Purchasing
{
	// Token: 0x0200003F RID: 63
	public interface IUnityChannelConfiguration : IStoreConfiguration
	{
		// Token: 0x1700002E RID: 46
		// (get) Token: 0x060000EE RID: 238
		// (set) Token: 0x060000EF RID: 239
		bool fetchReceiptPayloadOnPurchase { get; set; }
	}
}
