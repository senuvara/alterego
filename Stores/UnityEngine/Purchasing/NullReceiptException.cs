﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x0200009D RID: 157
	public class NullReceiptException : ReceiptParserException
	{
		// Token: 0x06000313 RID: 787 RVA: 0x0000D2EE File Offset: 0x0000B4EE
		public NullReceiptException()
		{
		}
	}
}
