﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000059 RID: 89
	internal class FakeAppleExtensions : IAppleExtensions, IStoreExtension
	{
		// Token: 0x0600017B RID: 379 RVA: 0x000070B0 File Offset: 0x000052B0
		public void RefreshAppReceipt(Action<string> successCallback, Action errorCallback)
		{
			bool failRefresh = this.m_FailRefresh;
			if (failRefresh)
			{
				errorCallback();
			}
			else
			{
				successCallback("A fake refreshed receipt!");
			}
			this.m_FailRefresh = !this.m_FailRefresh;
		}

		// Token: 0x0600017C RID: 380 RVA: 0x00004F05 File Offset: 0x00003105
		public void RestoreTransactions(Action<bool> callback)
		{
			callback(true);
		}

		// Token: 0x0600017D RID: 381 RVA: 0x00002EA8 File Offset: 0x000010A8
		public void RegisterPurchaseDeferredListener(Action<Product> callback)
		{
		}

		// Token: 0x17000036 RID: 54
		// (get) Token: 0x0600017E RID: 382 RVA: 0x000070EC File Offset: 0x000052EC
		// (set) Token: 0x0600017F RID: 383 RVA: 0x000070F4 File Offset: 0x000052F4
		public bool simulateAskToBuy
		{
			[CompilerGenerated]
			get
			{
				return this.<simulateAskToBuy>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<simulateAskToBuy>k__BackingField = value;
			}
		}

		// Token: 0x06000180 RID: 384 RVA: 0x00002EA8 File Offset: 0x000010A8
		public void SetStorePromotionOrder(List<Product> products)
		{
		}

		// Token: 0x06000181 RID: 385 RVA: 0x00002EA8 File Offset: 0x000010A8
		public void SetStorePromotionVisibility(Product product, AppleStorePromotionVisibility visible)
		{
		}

		// Token: 0x06000182 RID: 386 RVA: 0x00002EA8 File Offset: 0x000010A8
		public void SetApplicationUsername(string applicationUsername)
		{
		}

		// Token: 0x06000183 RID: 387 RVA: 0x00007100 File Offset: 0x00005300
		public string GetTransactionReceiptForProduct(Product product)
		{
			return "";
		}

		// Token: 0x06000184 RID: 388 RVA: 0x00002EA8 File Offset: 0x000010A8
		public void ContinuePromotionalPurchases()
		{
		}

		// Token: 0x06000185 RID: 389 RVA: 0x00007118 File Offset: 0x00005318
		public Dictionary<string, string> GetIntroductoryPriceDictionary()
		{
			return new Dictionary<string, string>();
		}

		// Token: 0x06000186 RID: 390 RVA: 0x00007130 File Offset: 0x00005330
		public Dictionary<string, string> GetProductDetails()
		{
			return new Dictionary<string, string>();
		}

		// Token: 0x06000187 RID: 391 RVA: 0x00002CA1 File Offset: 0x00000EA1
		public FakeAppleExtensions()
		{
		}

		// Token: 0x040000EE RID: 238
		private bool m_FailRefresh;

		// Token: 0x040000EF RID: 239
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private bool <simulateAskToBuy>k__BackingField;
	}
}
