﻿using System;
using UnityEngine.UDP;

namespace UnityEngine.Purchasing
{
	// Token: 0x0200004D RID: 77
	public class UDP
	{
		// Token: 0x17000030 RID: 48
		// (get) Token: 0x0600012B RID: 299 RVA: 0x00005AAC File Offset: 0x00003CAC
		public static string Name
		{
			get
			{
				string result;
				try
				{
					result = StoreService.StoreName;
				}
				catch (Exception)
				{
					result = "UDP";
				}
				return result;
			}
		}

		// Token: 0x0600012C RID: 300 RVA: 0x00002CA1 File Offset: 0x00000EA1
		public UDP()
		{
		}
	}
}
