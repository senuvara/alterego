﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using AOT;
using Uniject;
using UnityEngine.Purchasing.Extension;
using UnityEngine.Purchasing.Security;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000053 RID: 83
	internal class AppleStoreImpl : JSONStore, IAppleExtensions, IStoreExtension, IAppleConfiguration, IStoreConfiguration
	{
		// Token: 0x06000150 RID: 336 RVA: 0x00006593 File Offset: 0x00004793
		public AppleStoreImpl(IUtil util)
		{
			AppleStoreImpl.util = util;
			AppleStoreImpl.instance = this;
		}

		// Token: 0x06000151 RID: 337 RVA: 0x000065A9 File Offset: 0x000047A9
		public void SetNativeStore(INativeAppleStore apple)
		{
			base.SetNativeStore(apple);
			this.m_Native = apple;
			apple.SetUnityPurchasingCallback(new UnityPurchasingCallback(AppleStoreImpl.MessageCallback));
		}

		// Token: 0x17000031 RID: 49
		// (get) Token: 0x06000152 RID: 338 RVA: 0x000065D0 File Offset: 0x000047D0
		public string appReceipt
		{
			get
			{
				return this.m_Native.appReceipt;
			}
		}

		// Token: 0x17000032 RID: 50
		// (get) Token: 0x06000153 RID: 339 RVA: 0x000065F0 File Offset: 0x000047F0
		public bool canMakePayments
		{
			get
			{
				return this.m_Native.canMakePayments;
			}
		}

		// Token: 0x06000154 RID: 340 RVA: 0x0000660D File Offset: 0x0000480D
		public void SetApplePromotionalPurchaseInterceptorCallback(Action<Product> callback)
		{
			this.m_PromotionalPurchaseCallback = callback;
		}

		// Token: 0x17000033 RID: 51
		// (get) Token: 0x06000155 RID: 341 RVA: 0x00006618 File Offset: 0x00004818
		// (set) Token: 0x06000156 RID: 342 RVA: 0x00006635 File Offset: 0x00004835
		public bool simulateAskToBuy
		{
			get
			{
				return this.m_Native.simulateAskToBuy;
			}
			set
			{
				this.m_Native.simulateAskToBuy = value;
			}
		}

		// Token: 0x06000157 RID: 343 RVA: 0x00006648 File Offset: 0x00004848
		public void SetStorePromotionOrder(List<Product> products)
		{
			List<string> list = new List<string>();
			foreach (Product product in products)
			{
				bool flag = product != null && !string.IsNullOrEmpty(product.definition.storeSpecificId);
				if (flag)
				{
					list.Add(product.definition.storeSpecificId);
				}
			}
			Dictionary<string, object> json = new Dictionary<string, object>
			{
				{
					"products",
					list
				}
			};
			this.m_Native.SetStorePromotionOrder(MiniJson.JsonEncode(json));
		}

		// Token: 0x06000158 RID: 344 RVA: 0x000066F0 File Offset: 0x000048F0
		public void SetStorePromotionVisibility(Product product, AppleStorePromotionVisibility visibility)
		{
			bool flag = product == null;
			if (flag)
			{
				throw new ArgumentNullException("product");
			}
			this.m_Native.SetStorePromotionVisibility(product.definition.storeSpecificId, visibility.ToString());
		}

		// Token: 0x06000159 RID: 345 RVA: 0x00006738 File Offset: 0x00004938
		public string GetTransactionReceiptForProduct(Product product)
		{
			return this.m_Native.GetTransactionReceiptForProductId(product.definition.storeSpecificId);
		}

		// Token: 0x0600015A RID: 346 RVA: 0x00006760 File Offset: 0x00004960
		public void SetApplicationUsername(string applicationUsername)
		{
			this.m_Native.SetApplicationUsername(applicationUsername);
		}

		// Token: 0x0600015B RID: 347 RVA: 0x00006770 File Offset: 0x00004970
		public override void OnProductsRetrieved(string json)
		{
			List<ProductDescription> list = JSONSerializer.DeserializeProductDescriptions(json);
			List<ProductDescription> list2 = null;
			this.products_json = json;
			bool flag = this.m_Native != null;
			if (flag)
			{
				string appReceipt = this.m_Native.appReceipt;
				bool flag2 = !string.IsNullOrEmpty(appReceipt);
				if (flag2)
				{
					AppleReceipt appleReceiptFromBase64String = this.getAppleReceiptFromBase64String(appReceipt);
					bool flag3 = appleReceiptFromBase64String != null && appleReceiptFromBase64String.inAppPurchaseReceipts != null && appleReceiptFromBase64String.inAppPurchaseReceipts.Length != 0;
					if (flag3)
					{
						list2 = new List<ProductDescription>();
						using (List<ProductDescription>.Enumerator enumerator = list.GetEnumerator())
						{
							while (enumerator.MoveNext())
							{
								ProductDescription productDescription = enumerator.Current;
								AppleInAppPurchaseReceipt[] array = Array.FindAll<AppleInAppPurchaseReceipt>(appleReceiptFromBase64String.inAppPurchaseReceipts, (AppleInAppPurchaseReceipt r) => r.productID == productDescription.storeSpecificId);
								bool flag4 = array == null || array.Length == 0;
								if (flag4)
								{
									list2.Add(productDescription);
								}
								else
								{
									Array.Sort<AppleInAppPurchaseReceipt>(array, (AppleInAppPurchaseReceipt b, AppleInAppPurchaseReceipt a) => a.purchaseDate.CompareTo(b.purchaseDate));
									AppleInAppPurchaseReceipt appleInAppPurchaseReceipt = array[0];
									AppleStoreProductType appleStoreProductType = (AppleStoreProductType)Enum.Parse(typeof(AppleStoreProductType), appleInAppPurchaseReceipt.productType.ToString());
									bool flag5 = appleStoreProductType == AppleStoreProductType.AutoRenewingSubscription;
									if (flag5)
									{
										bool flag6 = new SubscriptionInfo(appleInAppPurchaseReceipt, null).isExpired() == Result.True;
										if (flag6)
										{
											list2.Add(productDescription);
										}
										else
										{
											list2.Add(new ProductDescription(productDescription.storeSpecificId, productDescription.metadata, appReceipt, appleInAppPurchaseReceipt.transactionID));
										}
									}
									else
									{
										bool flag7 = appleStoreProductType == AppleStoreProductType.Consumable;
										if (flag7)
										{
											list2.Add(productDescription);
										}
										else
										{
											list2.Add(new ProductDescription(productDescription.storeSpecificId, productDescription.metadata, appReceipt, appleInAppPurchaseReceipt.transactionID));
										}
									}
								}
							}
						}
						foreach (AppleInAppPurchaseReceipt appleInAppPurchaseReceipt2 in appleReceiptFromBase64String.inAppPurchaseReceipts)
						{
							Console.WriteLine("                    productID: {0}", appleInAppPurchaseReceipt2.productID);
							Console.WriteLine("                transactionID: {0}", appleInAppPurchaseReceipt2.transactionID);
							Console.WriteLine("originalTransactionIdentifier: {0}", appleInAppPurchaseReceipt2.originalTransactionIdentifier);
							Console.WriteLine("                 purchaseDate: {0}", appleInAppPurchaseReceipt2.purchaseDate);
							Console.WriteLine("         originalPurchaseDate: {0}", appleInAppPurchaseReceipt2.originalPurchaseDate);
							Console.WriteLine("   subscriptionExpirationDate: {0}", appleInAppPurchaseReceipt2.subscriptionExpirationDate);
						}
					}
				}
			}
			this.unity.OnProductsRetrieved(list2 ?? list);
			Promo.ProvideProductsToAds(this, this.unity);
			bool flag8 = this.m_PromotionalPurchaseCallback != null;
			if (flag8)
			{
				this.m_Native.InterceptPromotionalPurchases();
			}
			this.m_Native.AddTransactionObserver();
		}

		// Token: 0x0600015C RID: 348 RVA: 0x00006A8C File Offset: 0x00004C8C
		public void RestoreTransactions(Action<bool> callback)
		{
			this.m_RestoreCallback = callback;
			this.m_Native.RestoreTransactions();
		}

		// Token: 0x0600015D RID: 349 RVA: 0x00006AA2 File Offset: 0x00004CA2
		public void RefreshAppReceipt(Action<string> successCallback, Action errorCallback)
		{
			this.m_RefreshReceiptSuccess = successCallback;
			this.m_RefreshReceiptError = errorCallback;
			this.m_Native.RefreshAppReceipt();
		}

		// Token: 0x0600015E RID: 350 RVA: 0x00006ABF File Offset: 0x00004CBF
		public void RegisterPurchaseDeferredListener(Action<Product> callback)
		{
			this.m_DeferredCallback = callback;
		}

		// Token: 0x0600015F RID: 351 RVA: 0x00006AC9 File Offset: 0x00004CC9
		public void ContinuePromotionalPurchases()
		{
			this.m_Native.ContinuePromotionalPurchases();
		}

		// Token: 0x06000160 RID: 352 RVA: 0x00006AD8 File Offset: 0x00004CD8
		public Dictionary<string, string> GetIntroductoryPriceDictionary()
		{
			return JSONSerializer.DeserializeSubscriptionDescriptions(this.products_json);
		}

		// Token: 0x06000161 RID: 353 RVA: 0x00006AF8 File Offset: 0x00004CF8
		public Dictionary<string, string> GetProductDetails()
		{
			return JSONSerializer.DeserializeProductDetails(this.products_json);
		}

		// Token: 0x06000162 RID: 354 RVA: 0x00006B18 File Offset: 0x00004D18
		public void OnPurchaseDeferred(string productId)
		{
			bool flag = this.m_DeferredCallback != null;
			if (flag)
			{
				Product product = this.unity.products.WithStoreSpecificID(productId);
				bool flag2 = product != null;
				if (flag2)
				{
					this.m_DeferredCallback(product);
				}
			}
		}

		// Token: 0x06000163 RID: 355 RVA: 0x00006B5C File Offset: 0x00004D5C
		public void OnPromotionalPurchaseAttempted(string productId)
		{
			bool flag = this.m_PromotionalPurchaseCallback != null;
			if (flag)
			{
				Product product = this.unity.products.WithStoreSpecificID(productId);
				bool flag2 = product != null;
				if (flag2)
				{
					this.m_PromotionalPurchaseCallback(product);
				}
			}
		}

		// Token: 0x06000164 RID: 356 RVA: 0x00006BA4 File Offset: 0x00004DA4
		public void OnTransactionsRestoredSuccess()
		{
			bool flag = this.m_RestoreCallback != null;
			if (flag)
			{
				this.m_RestoreCallback(true);
			}
		}

		// Token: 0x06000165 RID: 357 RVA: 0x00006BCC File Offset: 0x00004DCC
		public void OnTransactionsRestoredFail(string error)
		{
			bool flag = this.m_RestoreCallback != null;
			if (flag)
			{
				this.m_RestoreCallback(false);
			}
		}

		// Token: 0x06000166 RID: 358 RVA: 0x00006BF4 File Offset: 0x00004DF4
		public void OnAppReceiptRetrieved(string receipt)
		{
			bool flag = receipt != null;
			if (flag)
			{
				bool flag2 = this.m_RefreshReceiptSuccess != null;
				if (flag2)
				{
					this.m_RefreshReceiptSuccess(receipt);
				}
			}
		}

		// Token: 0x06000167 RID: 359 RVA: 0x00006C28 File Offset: 0x00004E28
		public void OnAppReceiptRefreshedFailed()
		{
			bool flag = this.m_RefreshReceiptError != null;
			if (flag)
			{
				this.m_RefreshReceiptError();
			}
		}

		// Token: 0x06000168 RID: 360 RVA: 0x00006C50 File Offset: 0x00004E50
		[MonoPInvokeCallback(typeof(UnityPurchasingCallback))]
		private static void MessageCallback(string subject, string payload, string receipt, string transactionId)
		{
			AppleStoreImpl.util.RunOnMainThread(delegate
			{
				AppleStoreImpl.instance.ProcessMessage(subject, payload, receipt, transactionId);
			});
		}

		// Token: 0x06000169 RID: 361 RVA: 0x00006C98 File Offset: 0x00004E98
		private void ProcessMessage(string subject, string payload, string receipt, string transactionId)
		{
			uint num = <PrivateImplementationDetails>.ComputeStringHash(subject);
			if (num <= 2411276062U)
			{
				if (num <= 1093263071U)
				{
					if (num != 430974421U)
					{
						if (num == 1093263071U)
						{
							if (subject == "onTransactionsRestoredFail")
							{
								this.OnTransactionsRestoredFail(payload);
							}
						}
					}
					else if (subject == "onPromotionalPurchaseAttempted")
					{
						this.OnPromotionalPurchaseAttempted(payload);
					}
				}
				else if (num != 1523640433U)
				{
					if (num != 1674652619U)
					{
						if (num == 2411276062U)
						{
							if (subject == "onTransactionsRestoredSuccess")
							{
								this.OnTransactionsRestoredSuccess();
							}
						}
					}
					else if (subject == "onProductPurchaseDeferred")
					{
						this.OnPurchaseDeferred(payload);
					}
				}
				else if (subject == "onAppReceiptRefreshFailed")
				{
					this.OnAppReceiptRefreshedFailed();
				}
			}
			else if (num <= 3385076384U)
			{
				if (num != 3320864102U)
				{
					if (num == 3385076384U)
					{
						if (subject == "OnPurchaseSucceeded")
						{
							this.OnPurchaseSucceeded(payload, receipt, transactionId);
						}
					}
				}
				else if (subject == "OnPurchaseFailed")
				{
					base.OnPurchaseFailed(payload);
				}
			}
			else if (num != 3622431981U)
			{
				if (num != 3685547548U)
				{
					if (num == 3750884092U)
					{
						if (subject == "OnSetupFailed")
						{
							base.OnSetupFailed(payload);
						}
					}
				}
				else if (subject == "OnProductsRetrieved")
				{
					this.OnProductsRetrieved(payload);
				}
			}
			else if (subject == "onAppReceiptRefreshed")
			{
				this.OnAppReceiptRetrieved(payload);
			}
		}

		// Token: 0x0600016A RID: 362 RVA: 0x00006E6C File Offset: 0x0000506C
		public override void OnPurchaseSucceeded(string id, string receipt, string transactionId)
		{
			bool flag = this.isValidPurchaseState(this.getAppleReceiptFromBase64String(receipt), id);
			if (flag)
			{
				base.OnPurchaseSucceeded(id, receipt, transactionId);
			}
			else
			{
				base.FinishTransaction(null, transactionId);
			}
		}

		// Token: 0x0600016B RID: 363 RVA: 0x00006EA8 File Offset: 0x000050A8
		internal AppleReceipt getAppleReceiptFromBase64String(string receipt)
		{
			AppleReceipt result = null;
			bool flag = !string.IsNullOrEmpty(receipt);
			if (flag)
			{
				AppleReceiptParser appleReceiptParser = new AppleReceiptParser();
				try
				{
					result = appleReceiptParser.Parse(Convert.FromBase64String(receipt));
				}
				catch (Exception)
				{
				}
			}
			return result;
		}

		// Token: 0x0600016C RID: 364 RVA: 0x00006EF8 File Offset: 0x000050F8
		internal bool isValidPurchaseState(AppleReceipt appleReceipt, string id)
		{
			bool result = true;
			bool flag = appleReceipt != null && appleReceipt.inAppPurchaseReceipts != null && appleReceipt.inAppPurchaseReceipts.Length != 0;
			if (flag)
			{
				AppleInAppPurchaseReceipt[] array = Array.FindAll<AppleInAppPurchaseReceipt>(appleReceipt.inAppPurchaseReceipts, (AppleInAppPurchaseReceipt r) => r.productID == id);
				bool flag2 = array != null && array.Length != 0;
				if (flag2)
				{
					Array.Sort<AppleInAppPurchaseReceipt>(array, (AppleInAppPurchaseReceipt b, AppleInAppPurchaseReceipt a) => a.purchaseDate.CompareTo(b.purchaseDate));
					AppleInAppPurchaseReceipt appleInAppPurchaseReceipt = array[0];
					AppleStoreProductType appleStoreProductType = (AppleStoreProductType)Enum.Parse(typeof(AppleStoreProductType), appleInAppPurchaseReceipt.productType.ToString());
					bool flag3 = appleStoreProductType == AppleStoreProductType.AutoRenewingSubscription;
					if (flag3)
					{
						bool flag4 = new SubscriptionInfo(appleInAppPurchaseReceipt, null).isExpired() == Result.True;
						if (flag4)
						{
							result = false;
						}
					}
				}
			}
			return result;
		}

		// Token: 0x040000DC RID: 220
		private Action<Product> m_DeferredCallback;

		// Token: 0x040000DD RID: 221
		private Action m_RefreshReceiptError;

		// Token: 0x040000DE RID: 222
		private Action<string> m_RefreshReceiptSuccess;

		// Token: 0x040000DF RID: 223
		private Action<bool> m_RestoreCallback;

		// Token: 0x040000E0 RID: 224
		private Action<Product> m_PromotionalPurchaseCallback;

		// Token: 0x040000E1 RID: 225
		private INativeAppleStore m_Native;

		// Token: 0x040000E2 RID: 226
		private static IUtil util;

		// Token: 0x040000E3 RID: 227
		private static AppleStoreImpl instance;

		// Token: 0x040000E4 RID: 228
		private string products_json;

		// Token: 0x02000054 RID: 84
		[CompilerGenerated]
		private sealed class <>c__DisplayClass23_0
		{
			// Token: 0x0600016D RID: 365 RVA: 0x00002CA1 File Offset: 0x00000EA1
			public <>c__DisplayClass23_0()
			{
			}

			// Token: 0x0600016E RID: 366 RVA: 0x00006FE0 File Offset: 0x000051E0
			internal bool <OnProductsRetrieved>b__0(AppleInAppPurchaseReceipt r)
			{
				return r.productID == this.productDescription.storeSpecificId;
			}

			// Token: 0x040000E5 RID: 229
			public ProductDescription productDescription;
		}

		// Token: 0x02000055 RID: 85
		[CompilerGenerated]
		[Serializable]
		private sealed class <>c
		{
			// Token: 0x0600016F RID: 367 RVA: 0x00006FF8 File Offset: 0x000051F8
			// Note: this type is marked as 'beforefieldinit'.
			static <>c()
			{
			}

			// Token: 0x06000170 RID: 368 RVA: 0x00002CA1 File Offset: 0x00000EA1
			public <>c()
			{
			}

			// Token: 0x06000171 RID: 369 RVA: 0x00007004 File Offset: 0x00005204
			internal int <OnProductsRetrieved>b__23_1(AppleInAppPurchaseReceipt b, AppleInAppPurchaseReceipt a)
			{
				return a.purchaseDate.CompareTo(b.purchaseDate);
			}

			// Token: 0x06000172 RID: 370 RVA: 0x00007028 File Offset: 0x00005228
			internal int <isValidPurchaseState>b__40_1(AppleInAppPurchaseReceipt b, AppleInAppPurchaseReceipt a)
			{
				return a.purchaseDate.CompareTo(b.purchaseDate);
			}

			// Token: 0x040000E6 RID: 230
			public static readonly AppleStoreImpl.<>c <>9 = new AppleStoreImpl.<>c();

			// Token: 0x040000E7 RID: 231
			public static Comparison<AppleInAppPurchaseReceipt> <>9__23_1;

			// Token: 0x040000E8 RID: 232
			public static Comparison<AppleInAppPurchaseReceipt> <>9__40_1;
		}

		// Token: 0x02000056 RID: 86
		[CompilerGenerated]
		private sealed class <>c__DisplayClass36_0
		{
			// Token: 0x06000173 RID: 371 RVA: 0x00002CA1 File Offset: 0x00000EA1
			public <>c__DisplayClass36_0()
			{
			}

			// Token: 0x06000174 RID: 372 RVA: 0x00007049 File Offset: 0x00005249
			internal void <MessageCallback>b__0()
			{
				AppleStoreImpl.instance.ProcessMessage(this.subject, this.payload, this.receipt, this.transactionId);
			}

			// Token: 0x040000E9 RID: 233
			public string subject;

			// Token: 0x040000EA RID: 234
			public string payload;

			// Token: 0x040000EB RID: 235
			public string receipt;

			// Token: 0x040000EC RID: 236
			public string transactionId;
		}

		// Token: 0x02000057 RID: 87
		[CompilerGenerated]
		private sealed class <>c__DisplayClass40_0
		{
			// Token: 0x06000175 RID: 373 RVA: 0x00002CA1 File Offset: 0x00000EA1
			public <>c__DisplayClass40_0()
			{
			}

			// Token: 0x06000176 RID: 374 RVA: 0x0000706F File Offset: 0x0000526F
			internal bool <isValidPurchaseState>b__0(AppleInAppPurchaseReceipt r)
			{
				return r.productID == this.id;
			}

			// Token: 0x040000ED RID: 237
			public string id;
		}
	}
}
