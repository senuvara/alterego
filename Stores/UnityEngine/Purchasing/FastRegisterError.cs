﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x0200001C RID: 28
	public enum FastRegisterError
	{
		// Token: 0x0400002E RID: 46
		PasswordEmpty,
		// Token: 0x0400002F RID: 47
		FastRegisterCallBackIsNull,
		// Token: 0x04000030 RID: 48
		NetworkError,
		// Token: 0x04000031 RID: 49
		NotKnown
	}
}
