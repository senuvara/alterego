﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x0200008A RID: 138
	public interface IProductCatalogImpl
	{
		// Token: 0x06000269 RID: 617
		ProductCatalog LoadDefaultCatalog();
	}
}
