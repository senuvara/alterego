﻿using System;
using UnityEngine.Purchasing.Extension;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000034 RID: 52
	public class FakeSamsungAppsExtensions : ISamsungAppsExtensions, IStoreExtension, ISamsungAppsConfiguration, IStoreConfiguration
	{
		// Token: 0x060000D5 RID: 213 RVA: 0x00002EA8 File Offset: 0x000010A8
		public void SetMode(SamsungAppsMode mode)
		{
		}

		// Token: 0x060000D6 RID: 214 RVA: 0x00004F05 File Offset: 0x00003105
		public void RestoreTransactions(Action<bool> callback)
		{
			callback(true);
		}

		// Token: 0x060000D7 RID: 215 RVA: 0x00002CA1 File Offset: 0x00000EA1
		public FakeSamsungAppsExtensions()
		{
		}
	}
}
