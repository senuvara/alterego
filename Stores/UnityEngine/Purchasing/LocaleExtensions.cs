﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace UnityEngine.Purchasing
{
	// Token: 0x0200007D RID: 125
	public static class LocaleExtensions
	{
		// Token: 0x06000227 RID: 551 RVA: 0x00009608 File Offset: 0x00007808
		public static string[] GetLabelsWithSupportedPlatforms()
		{
			bool flag = LocaleExtensions.LabelsWithSupportedPlatforms != null;
			string[] labelsWithSupportedPlatforms;
			if (flag)
			{
				labelsWithSupportedPlatforms = LocaleExtensions.LabelsWithSupportedPlatforms;
			}
			else
			{
				LocaleExtensions.LabelsWithSupportedPlatforms = new string[Enum.GetValues(typeof(TranslationLocale)).Length];
				List<TranslationLocale> list = LocaleExtensions.GoogleLocales.ToList<TranslationLocale>();
				List<TranslationLocale> list2 = LocaleExtensions.AppleLocales.ToList<TranslationLocale>();
				List<TranslationLocale> list3 = LocaleExtensions.XiaomiLocales.ToList<TranslationLocale>();
				int num = 0;
				foreach (object obj in Enum.GetValues(typeof(TranslationLocale)))
				{
					TranslationLocale item = (TranslationLocale)obj;
					List<string> list4 = new List<string>();
					bool flag2 = list.Contains(item);
					if (flag2)
					{
						list4.Add("Google Play");
					}
					bool flag3 = list2.Contains(item);
					if (flag3)
					{
						list4.Add("Apple");
					}
					bool flag4 = list3.Contains(item);
					if (flag4)
					{
						list4.Add(LocaleExtensions.XiaomiMiGamePay);
					}
					string str = string.Join(", ", list4.ToArray());
					LocaleExtensions.LabelsWithSupportedPlatforms[num] = LocaleExtensions.Labels[num] + " (" + str + ")";
					num++;
				}
				labelsWithSupportedPlatforms = LocaleExtensions.LabelsWithSupportedPlatforms;
			}
			return labelsWithSupportedPlatforms;
		}

		// Token: 0x06000228 RID: 552 RVA: 0x0000976C File Offset: 0x0000796C
		public static bool SupportedOnApple(this TranslationLocale locale)
		{
			return LocaleExtensions.AppleLocales.Contains(locale);
		}

		// Token: 0x06000229 RID: 553 RVA: 0x0000978C File Offset: 0x0000798C
		public static bool SupportedOnGoogle(this TranslationLocale locale)
		{
			return LocaleExtensions.GoogleLocales.Contains(locale);
		}

		// Token: 0x0600022A RID: 554 RVA: 0x000097AC File Offset: 0x000079AC
		// Note: this type is marked as 'beforefieldinit'.
		static LocaleExtensions()
		{
		}

		// Token: 0x04000195 RID: 405
		public static readonly string XiaomiMiGamePay = "Xiaomi Mi Game Pay";

		// Token: 0x04000196 RID: 406
		private static readonly string[] Labels = new string[]
		{
			"Chinese (Traditional)",
			"Czech",
			"Danish",
			"Dutch",
			"English (U.S.)",
			"French",
			"Finnish",
			"German",
			"Hebrew",
			"Hindi",
			"Italian",
			"Japanese",
			"Korean",
			"Norwegian",
			"Polish",
			"Portuguese (Portugal)",
			"Russian",
			"Spanish (Spain)",
			"Swedish",
			"Chinese (Simplified)",
			"English (Australia)",
			"English (Canada)",
			"English (U.K.)",
			"French (Canada)",
			"Greek",
			"Indonesian",
			"Malay",
			"Portuguese (Brazil)",
			"Spanish (Mexico)",
			"Thai",
			"Turkish",
			"Vietnamese"
		};

		// Token: 0x04000197 RID: 407
		private static readonly TranslationLocale[] GoogleLocales = new TranslationLocale[]
		{
			TranslationLocale.zh_TW,
			TranslationLocale.cs_CZ,
			TranslationLocale.da_DK,
			TranslationLocale.nl_NL,
			TranslationLocale.en_US,
			TranslationLocale.fr_FR,
			TranslationLocale.fi_FI,
			TranslationLocale.de_DE,
			TranslationLocale.iw_IL,
			TranslationLocale.hi_IN,
			TranslationLocale.it_IT,
			TranslationLocale.ja_JP,
			TranslationLocale.ko_KR,
			TranslationLocale.no_NO,
			TranslationLocale.pl_PL,
			TranslationLocale.pt_PT,
			TranslationLocale.ru_RU,
			TranslationLocale.es_ES,
			TranslationLocale.sv_SE
		};

		// Token: 0x04000198 RID: 408
		private static readonly TranslationLocale[] AppleLocales = new TranslationLocale[]
		{
			TranslationLocale.zh_CN,
			TranslationLocale.zh_TW,
			TranslationLocale.da_DK,
			TranslationLocale.nl_NL,
			TranslationLocale.en_AU,
			TranslationLocale.en_CA,
			TranslationLocale.en_GB,
			TranslationLocale.en_US,
			TranslationLocale.fi_FI,
			TranslationLocale.fr_FR,
			TranslationLocale.fr_CA,
			TranslationLocale.de_DE,
			TranslationLocale.el_GR,
			TranslationLocale.id_ID,
			TranslationLocale.it_IT,
			TranslationLocale.ja_JP,
			TranslationLocale.ko_KR,
			TranslationLocale.ms_MY,
			TranslationLocale.no_NO,
			TranslationLocale.pt_BR,
			TranslationLocale.pt_PT,
			TranslationLocale.ru_RU,
			TranslationLocale.es_MX,
			TranslationLocale.es_ES,
			TranslationLocale.sv_SE,
			TranslationLocale.th_TH,
			TranslationLocale.tr_TR,
			TranslationLocale.vi_VN
		};

		// Token: 0x04000199 RID: 409
		private static readonly TranslationLocale[] XiaomiLocales = new TranslationLocale[]
		{
			TranslationLocale.zh_CN
		};

		// Token: 0x0400019A RID: 410
		private static string[] LabelsWithSupportedPlatforms;
	}
}
