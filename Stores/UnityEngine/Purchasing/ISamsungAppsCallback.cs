﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000035 RID: 53
	internal interface ISamsungAppsCallback
	{
		// Token: 0x060000D8 RID: 216
		void OnTransactionsRestored(bool result);
	}
}
