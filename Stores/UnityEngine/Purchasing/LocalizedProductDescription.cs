﻿using System;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.RegularExpressions;

namespace UnityEngine.Purchasing
{
	// Token: 0x0200007E RID: 126
	[Serializable]
	public class LocalizedProductDescription
	{
		// Token: 0x0600022B RID: 555 RVA: 0x00009A00 File Offset: 0x00007C00
		public LocalizedProductDescription Clone()
		{
			return new LocalizedProductDescription
			{
				googleLocale = this.googleLocale,
				Title = this.Title,
				Description = this.Description
			};
		}

		// Token: 0x17000050 RID: 80
		// (get) Token: 0x0600022C RID: 556 RVA: 0x00009A40 File Offset: 0x00007C40
		// (set) Token: 0x0600022D RID: 557 RVA: 0x00009A5D File Offset: 0x00007C5D
		public string Title
		{
			get
			{
				return LocalizedProductDescription.DecodeNonLatinCharacters(this.title);
			}
			set
			{
				this.title = LocalizedProductDescription.EncodeNonLatinCharacters(value);
			}
		}

		// Token: 0x17000051 RID: 81
		// (get) Token: 0x0600022E RID: 558 RVA: 0x00009A6C File Offset: 0x00007C6C
		// (set) Token: 0x0600022F RID: 559 RVA: 0x00009A89 File Offset: 0x00007C89
		public string Description
		{
			get
			{
				return LocalizedProductDescription.DecodeNonLatinCharacters(this.description);
			}
			set
			{
				this.description = LocalizedProductDescription.EncodeNonLatinCharacters(value);
			}
		}

		// Token: 0x06000230 RID: 560 RVA: 0x00009A98 File Offset: 0x00007C98
		private static string EncodeNonLatinCharacters(string s)
		{
			bool flag = s == null;
			string result;
			if (flag)
			{
				result = s;
			}
			else
			{
				StringBuilder stringBuilder = new StringBuilder();
				foreach (char c in s)
				{
					bool flag2 = c > '\u007f';
					if (flag2)
					{
						string str = "\\u";
						int num = (int)c;
						string value = str + num.ToString("x4");
						stringBuilder.Append(value);
					}
					else
					{
						stringBuilder.Append(c);
					}
				}
				result = stringBuilder.ToString();
			}
			return result;
		}

		// Token: 0x06000231 RID: 561 RVA: 0x00009B28 File Offset: 0x00007D28
		private static string DecodeNonLatinCharacters(string s)
		{
			bool flag = s == null;
			string result;
			if (flag)
			{
				result = s;
			}
			else
			{
				result = Regex.Replace(s, "\\\\u(?<Value>[a-zA-Z0-9]{4})", (Match m) => ((char)int.Parse(m.Groups["Value"].Value, NumberStyles.HexNumber)).ToString());
			}
			return result;
		}

		// Token: 0x06000232 RID: 562 RVA: 0x00009B70 File Offset: 0x00007D70
		public LocalizedProductDescription()
		{
		}

		// Token: 0x0400019B RID: 411
		public TranslationLocale googleLocale = TranslationLocale.en_US;

		// Token: 0x0400019C RID: 412
		[SerializeField]
		private string title;

		// Token: 0x0400019D RID: 413
		[SerializeField]
		private string description;

		// Token: 0x0200007F RID: 127
		[CompilerGenerated]
		[Serializable]
		private sealed class <>c
		{
			// Token: 0x06000233 RID: 563 RVA: 0x00009B80 File Offset: 0x00007D80
			// Note: this type is marked as 'beforefieldinit'.
			static <>c()
			{
			}

			// Token: 0x06000234 RID: 564 RVA: 0x00002CA1 File Offset: 0x00000EA1
			public <>c()
			{
			}

			// Token: 0x06000235 RID: 565 RVA: 0x00009B8C File Offset: 0x00007D8C
			internal string <DecodeNonLatinCharacters>b__11_0(Match m)
			{
				return ((char)int.Parse(m.Groups["Value"].Value, NumberStyles.HexNumber)).ToString();
			}

			// Token: 0x0400019E RID: 414
			public static readonly LocalizedProductDescription.<>c <>9 = new LocalizedProductDescription.<>c();

			// Token: 0x0400019F RID: 415
			public static MatchEvaluator <>9__11_0;
		}
	}
}
