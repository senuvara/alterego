﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x0200003A RID: 58
	public enum SamsungAppsMode
	{
		// Token: 0x040000AC RID: 172
		Production,
		// Token: 0x040000AD RID: 173
		AlwaysSucceed,
		// Token: 0x040000AE RID: 174
		AlwaysFail
	}
}
