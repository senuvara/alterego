﻿using System;
using UnityEngine.Purchasing.Extension;

namespace UnityEngine.Purchasing
{
	// Token: 0x0200001A RID: 26
	public interface IMoolahConfiguration : IStoreConfiguration
	{
		// Token: 0x1700001D RID: 29
		// (get) Token: 0x06000067 RID: 103
		// (set) Token: 0x06000068 RID: 104
		string appKey { get; set; }

		// Token: 0x1700001E RID: 30
		// (get) Token: 0x06000069 RID: 105
		// (set) Token: 0x0600006A RID: 106
		string hashKey { get; set; }

		// Token: 0x1700001F RID: 31
		// (get) Token: 0x0600006B RID: 107
		// (set) Token: 0x0600006C RID: 108
		string notificationURL { get; set; }

		// Token: 0x0600006D RID: 109
		void SetMode(CloudMoolahMode mode);
	}
}
