﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x020000B6 RID: 182
	public enum FakeStoreUIMode
	{
		// Token: 0x0400028B RID: 651
		Default,
		// Token: 0x0400028C RID: 652
		StandardUser,
		// Token: 0x0400028D RID: 653
		DeveloperUser
	}
}
