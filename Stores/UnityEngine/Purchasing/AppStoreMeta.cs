﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x0200005F RID: 95
	public enum AppStoreMeta
	{
		// Token: 0x04000104 RID: 260
		AndroidStoreStart = 1,
		// Token: 0x04000105 RID: 261
		AndroidStoreEnd = 6
	}
}
