﻿using System;
using System.Collections.Generic;
using UnityEngine.Purchasing.Extension;

namespace UnityEngine.Purchasing
{
	// Token: 0x0200002C RID: 44
	public class FakeGooglePlayStoreExtensions : IGooglePlayStoreExtensions, IStoreExtension, IGooglePlayConfiguration, IStoreConfiguration
	{
		// Token: 0x060000BC RID: 188 RVA: 0x00002EA8 File Offset: 0x000010A8
		public void SetPublicKey(string s)
		{
		}

		// Token: 0x060000BD RID: 189 RVA: 0x00002EA8 File Offset: 0x000010A8
		public void UpgradeDowngradeSubscription(string oldSku, string newSku)
		{
		}

		// Token: 0x060000BE RID: 190 RVA: 0x00002EA8 File Offset: 0x000010A8
		public void RestoreTransactions(Action<bool> callback)
		{
		}

		// Token: 0x060000BF RID: 191 RVA: 0x00004CE8 File Offset: 0x00002EE8
		public Dictionary<string, string> GetProductJSONDictionary()
		{
			return null;
		}

		// Token: 0x060000C0 RID: 192 RVA: 0x00002EA8 File Offset: 0x000010A8
		public void FinishAdditionalTransaction(string productId, string transactionId)
		{
		}

		// Token: 0x060000C1 RID: 193 RVA: 0x00002CA1 File Offset: 0x00000EA1
		public FakeGooglePlayStoreExtensions()
		{
		}
	}
}
