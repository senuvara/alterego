﻿using System;
using System.Reflection;
using Uniject;

namespace UnityEngine.Purchasing
{
	// Token: 0x0200006E RID: 110
	internal class AdsIPC
	{
		// Token: 0x060001F4 RID: 500 RVA: 0x00008D9C File Offset: 0x00006F9C
		internal static bool InitAdsIPC(IUtil util)
		{
			bool flag = util.platform == RuntimePlatform.IPhonePlayer;
			if (flag)
			{
				AdsIPC.adsAdvertisementClassName += "iOS";
			}
			else
			{
				bool flag2 = util.platform == RuntimePlatform.Android;
				if (!flag2)
				{
					return false;
				}
				AdsIPC.adsAdvertisementClassName += "Android";
			}
			bool flag3 = AdsIPC.VerifyMethodExists();
			bool result;
			if (flag3)
			{
				result = true;
			}
			else
			{
				AdsIPC.adsAdvertisementClassName = "UnityEngine.Advertisements.Purchasing,UnityEngine.Advertisements";
				result = AdsIPC.VerifyMethodExists();
			}
			return result;
		}

		// Token: 0x060001F5 RID: 501 RVA: 0x00008E1C File Offset: 0x0000701C
		internal static bool VerifyMethodExists()
		{
			try
			{
				AdsIPC.adsAdvertisementType = Type.GetType(AdsIPC.adsAdvertisementClassName);
				bool flag = AdsIPC.adsAdvertisementType != null;
				if (flag)
				{
					AdsIPC.adsMessageSend = AdsIPC.adsAdvertisementType.GetMethod(AdsIPC.adsMessageSendName);
					bool flag2 = AdsIPC.adsMessageSend != null;
					if (flag2)
					{
						return true;
					}
				}
			}
			catch
			{
			}
			return false;
		}

		// Token: 0x060001F6 RID: 502 RVA: 0x00008E8C File Offset: 0x0000708C
		internal static bool SendEvent(string json)
		{
			bool flag = AdsIPC.adsMessageSend != null;
			bool result;
			if (flag)
			{
				AdsIPC.adsMessageSend.Invoke(null, new string[]
				{
					json
				});
				result = true;
			}
			else
			{
				result = false;
			}
			return result;
		}

		// Token: 0x060001F7 RID: 503 RVA: 0x00002CA1 File Offset: 0x00000EA1
		public AdsIPC()
		{
		}

		// Token: 0x060001F8 RID: 504 RVA: 0x00008EC6 File Offset: 0x000070C6
		// Note: this type is marked as 'beforefieldinit'.
		static AdsIPC()
		{
		}

		// Token: 0x04000140 RID: 320
		private static string adsAdvertisementClassName = "UnityEngine.Advertisements.Purchasing,UnityEngine.Advertisements.";

		// Token: 0x04000141 RID: 321
		private static string adsMessageSendName = "SendEvent";

		// Token: 0x04000142 RID: 322
		private static Type adsAdvertisementType = null;

		// Token: 0x04000143 RID: 323
		private static MethodInfo adsMessageSend = null;
	}
}
