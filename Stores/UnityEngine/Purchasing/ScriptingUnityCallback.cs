﻿using System;
using System.Runtime.CompilerServices;
using Uniject;

namespace UnityEngine.Purchasing
{
	// Token: 0x0200000C RID: 12
	internal class ScriptingUnityCallback : IUnityCallback
	{
		// Token: 0x0600003F RID: 63 RVA: 0x00002CAA File Offset: 0x00000EAA
		public ScriptingUnityCallback(IUnityCallback forwardTo, IUtil util)
		{
			this.forwardTo = forwardTo;
			this.util = util;
		}

		// Token: 0x06000040 RID: 64 RVA: 0x00002CC4 File Offset: 0x00000EC4
		public void OnSetupFailed(string json)
		{
			this.util.RunOnMainThread(delegate
			{
				this.forwardTo.OnSetupFailed(json);
			});
		}

		// Token: 0x06000041 RID: 65 RVA: 0x00002D00 File Offset: 0x00000F00
		public void OnProductsRetrieved(string json)
		{
			this.util.RunOnMainThread(delegate
			{
				this.forwardTo.OnProductsRetrieved(json);
			});
		}

		// Token: 0x06000042 RID: 66 RVA: 0x00002D3C File Offset: 0x00000F3C
		public void OnPurchaseSucceeded(string id, string receipt, string transactionID)
		{
			this.util.RunOnMainThread(delegate
			{
				this.forwardTo.OnPurchaseSucceeded(id, receipt, transactionID);
			});
		}

		// Token: 0x06000043 RID: 67 RVA: 0x00002D84 File Offset: 0x00000F84
		public void OnPurchaseFailed(string json)
		{
			this.util.RunOnMainThread(delegate
			{
				this.forwardTo.OnPurchaseFailed(json);
			});
		}

		// Token: 0x04000010 RID: 16
		private IUnityCallback forwardTo;

		// Token: 0x04000011 RID: 17
		private IUtil util;

		// Token: 0x0200000D RID: 13
		[CompilerGenerated]
		private sealed class <>c__DisplayClass3_0
		{
			// Token: 0x06000044 RID: 68 RVA: 0x00002CA1 File Offset: 0x00000EA1
			public <>c__DisplayClass3_0()
			{
			}

			// Token: 0x06000045 RID: 69 RVA: 0x00002DBE File Offset: 0x00000FBE
			internal void <OnSetupFailed>b__0()
			{
				this.<>4__this.forwardTo.OnSetupFailed(this.json);
			}

			// Token: 0x04000012 RID: 18
			public string json;

			// Token: 0x04000013 RID: 19
			public ScriptingUnityCallback <>4__this;
		}

		// Token: 0x0200000E RID: 14
		[CompilerGenerated]
		private sealed class <>c__DisplayClass4_0
		{
			// Token: 0x06000046 RID: 70 RVA: 0x00002CA1 File Offset: 0x00000EA1
			public <>c__DisplayClass4_0()
			{
			}

			// Token: 0x06000047 RID: 71 RVA: 0x00002DD7 File Offset: 0x00000FD7
			internal void <OnProductsRetrieved>b__0()
			{
				this.<>4__this.forwardTo.OnProductsRetrieved(this.json);
			}

			// Token: 0x04000014 RID: 20
			public string json;

			// Token: 0x04000015 RID: 21
			public ScriptingUnityCallback <>4__this;
		}

		// Token: 0x0200000F RID: 15
		[CompilerGenerated]
		private sealed class <>c__DisplayClass5_0
		{
			// Token: 0x06000048 RID: 72 RVA: 0x00002CA1 File Offset: 0x00000EA1
			public <>c__DisplayClass5_0()
			{
			}

			// Token: 0x06000049 RID: 73 RVA: 0x00002DF0 File Offset: 0x00000FF0
			internal void <OnPurchaseSucceeded>b__0()
			{
				this.<>4__this.forwardTo.OnPurchaseSucceeded(this.id, this.receipt, this.transactionID);
			}

			// Token: 0x04000016 RID: 22
			public string id;

			// Token: 0x04000017 RID: 23
			public string receipt;

			// Token: 0x04000018 RID: 24
			public string transactionID;

			// Token: 0x04000019 RID: 25
			public ScriptingUnityCallback <>4__this;
		}

		// Token: 0x02000010 RID: 16
		[CompilerGenerated]
		private sealed class <>c__DisplayClass6_0
		{
			// Token: 0x0600004A RID: 74 RVA: 0x00002CA1 File Offset: 0x00000EA1
			public <>c__DisplayClass6_0()
			{
			}

			// Token: 0x0600004B RID: 75 RVA: 0x00002E15 File Offset: 0x00001015
			internal void <OnPurchaseFailed>b__0()
			{
				this.<>4__this.forwardTo.OnPurchaseFailed(this.json);
			}

			// Token: 0x0400001A RID: 26
			public string json;

			// Token: 0x0400001B RID: 27
			public ScriptingUnityCallback <>4__this;
		}
	}
}
