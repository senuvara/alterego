﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000040 RID: 64
	public interface IUnityChannelExtensions : IStoreExtension
	{
		// Token: 0x060000F0 RID: 240
		void ConfirmPurchase(string transactionId, Action<bool, string, string> callback);

		// Token: 0x060000F1 RID: 241
		void ValidateReceipt(string transactionId, Action<bool, string, string> callback);

		// Token: 0x060000F2 RID: 242
		string GetLastPurchaseError();
	}
}
