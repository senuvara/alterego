﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x020000A4 RID: 164
	public interface IMicrosoftExtensions : IStoreExtension
	{
		// Token: 0x0600031F RID: 799
		void RestoreTransactions();
	}
}
