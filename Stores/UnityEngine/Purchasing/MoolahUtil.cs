﻿using System;
using System.IO;
using System.Net;
using System.Text;

namespace UnityEngine.Purchasing
{
	// Token: 0x0200002A RID: 42
	internal class MoolahUtil
	{
		// Token: 0x060000B6 RID: 182 RVA: 0x00004B24 File Offset: 0x00002D24
		public static string GetResponseString(HttpWebResponse webresponse)
		{
			bool flag = webresponse == null;
			string result;
			if (flag)
			{
				result = null;
			}
			else
			{
				using (Stream responseStream = webresponse.GetResponseStream())
				{
					StreamReader streamReader = new StreamReader(responseStream, Encoding.UTF8);
					result = streamReader.ReadToEnd();
				}
			}
			return result;
		}

		// Token: 0x060000B7 RID: 183 RVA: 0x00002CA1 File Offset: 0x00000EA1
		public MoolahUtil()
		{
		}
	}
}
