﻿using System;
using Uniject;
using UnityEngine.Purchasing.Extension;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000063 RID: 99
	internal class NativeStoreProvider : INativeStoreProvider
	{
		// Token: 0x060001BB RID: 443 RVA: 0x00007FA8 File Offset: 0x000061A8
		public INativeStore GetAndroidStore(IUnityCallback callback, AppStore store, IPurchasingBinder binder, IUtil util)
		{
			INativeStore androidStoreHelper;
			try
			{
				androidStoreHelper = this.GetAndroidStoreHelper(callback, store, binder, util);
			}
			catch (Exception ex)
			{
				throw new NotSupportedException("Failed to bind to native store: " + ex.ToString());
			}
			bool flag = androidStoreHelper != null;
			if (flag)
			{
				return androidStoreHelper;
			}
			throw new NotImplementedException();
		}

		// Token: 0x060001BC RID: 444 RVA: 0x00008004 File Offset: 0x00006204
		private INativeStore GetAndroidStoreHelper(IUnityCallback callback, AppStore store, IPurchasingBinder binder, IUtil util)
		{
			switch (store)
			{
			case AppStore.GooglePlay:
				break;
			case AppStore.AmazonAppStore:
				using (AndroidJavaClass androidJavaClass = new AndroidJavaClass("com.unity.purchasing.amazon.AmazonPurchasing"))
				{
					JavaBridge javaBridge = new JavaBridge(new ScriptingUnityCallback(callback, util));
					AndroidJavaObject androidJavaObject = androidJavaClass.CallStatic<AndroidJavaObject>("instance", new object[]
					{
						javaBridge
					});
					AmazonAppStoreStoreExtensions instance = new AmazonAppStoreStoreExtensions(androidJavaObject);
					binder.RegisterExtension<IAmazonExtensions>(instance);
					binder.RegisterConfiguration<IAmazonConfiguration>(instance);
					return new AndroidJavaStore(androidJavaObject);
				}
				break;
			case AppStore.CloudMoolah:
				goto IL_1CA;
			case AppStore.SamsungApps:
				goto IL_108;
			case AppStore.XiaomiMiPay:
				goto IL_17D;
			case AppStore.UDP:
			{
				UDPImpl udpimpl = new UDPImpl();
				UDPBindings udpbindings = new UDPBindings();
				udpimpl.SetNativeStore(udpbindings);
				binder.RegisterExtension<IUDPExtensions>(udpimpl);
				return udpbindings;
			}
			default:
				goto IL_1CA;
			}
			using (AndroidJavaClass androidJavaClass2 = new AndroidJavaClass("com.unity.purchasing.googleplay.GooglePlayPurchasing"))
			{
				GooglePlayStoreExtensions googlePlayStoreExtensions = new GooglePlayStoreExtensions();
				JavaBridge javaBridge2 = new JavaBridge(new ScriptingUnityCallback(callback, util));
				AndroidJavaObject androidJavaObject2 = androidJavaClass2.CallStatic<AndroidJavaObject>("instance", new object[]
				{
					javaBridge2
				});
				googlePlayStoreExtensions.SetAndroidJavaObject(androidJavaObject2);
				binder.RegisterExtension<IGooglePlayStoreExtensions>(googlePlayStoreExtensions);
				binder.RegisterConfiguration<IGooglePlayConfiguration>(googlePlayStoreExtensions);
				return new GooglePlayAndroidJavaStore(androidJavaObject2, util);
			}
			IL_108:
			using (AndroidJavaClass androidJavaClass3 = new AndroidJavaClass("com.unity.purchasing.samsung.SamsungPurchasing"))
			{
				SamsungAppsStoreExtensions samsungAppsStoreExtensions = new SamsungAppsStoreExtensions();
				JavaBridge javaBridge3 = new JavaBridge(new ScriptingUnityCallback(callback, util));
				AndroidJavaObject androidJavaObject3 = androidJavaClass3.CallStatic<AndroidJavaObject>("instance", new object[]
				{
					javaBridge3,
					samsungAppsStoreExtensions
				});
				samsungAppsStoreExtensions.SetAndroidJavaObject(androidJavaObject3);
				binder.RegisterExtension<ISamsungAppsExtensions>(samsungAppsStoreExtensions);
				binder.RegisterConfiguration<ISamsungAppsConfiguration>(samsungAppsStoreExtensions);
				return new AndroidJavaStore(androidJavaObject3);
			}
			IL_17D:
			UnityChannelImpl unityChannelImpl = new UnityChannelImpl();
			UnityChannelBindings unityChannelBindings = new UnityChannelBindings();
			unityChannelImpl.SetNativeStore(unityChannelBindings);
			binder.RegisterExtension<IUnityChannelExtensions>(unityChannelImpl);
			binder.RegisterConfiguration<IUnityChannelConfiguration>(unityChannelImpl);
			return unityChannelBindings;
			IL_1CA:
			throw new NotImplementedException();
		}

		// Token: 0x060001BD RID: 445 RVA: 0x0000820C File Offset: 0x0000640C
		public INativeAppleStore GetStorekit(IUnityCallback callback)
		{
			bool flag = Application.platform == RuntimePlatform.IPhonePlayer || Application.platform == RuntimePlatform.tvOS;
			INativeAppleStore result;
			if (flag)
			{
				result = new iOSStoreBindings();
			}
			else
			{
				result = new OSXStoreBindings();
			}
			return result;
		}

		// Token: 0x060001BE RID: 446 RVA: 0x00008244 File Offset: 0x00006444
		public INativeTizenStore GetTizenStore(IUnityCallback callback, IPurchasingBinder binder)
		{
			return new TizenStoreBindings();
		}

		// Token: 0x060001BF RID: 447 RVA: 0x0000825C File Offset: 0x0000645C
		public INativeFacebookStore GetFacebookStore()
		{
			return new FacebookStoreBindings();
		}

		// Token: 0x060001C0 RID: 448 RVA: 0x00008274 File Offset: 0x00006474
		public INativeFacebookStore GetFacebookStore(IUnityCallback callback, IPurchasingBinder binder)
		{
			return new FacebookStoreBindings();
		}

		// Token: 0x060001C1 RID: 449 RVA: 0x00002CA1 File Offset: 0x00000EA1
		public NativeStoreProvider()
		{
		}
	}
}
