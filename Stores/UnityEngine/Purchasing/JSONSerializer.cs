﻿using System;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine.Purchasing.Extension;

namespace UnityEngine.Purchasing
{
	// Token: 0x0200000B RID: 11
	internal class JSONSerializer
	{
		// Token: 0x06000032 RID: 50 RVA: 0x000022C4 File Offset: 0x000004C4
		public static string SerializeProductDef(ProductDefinition product)
		{
			return MiniJson.JsonEncode(JSONSerializer.EncodeProductDef(product));
		}

		// Token: 0x06000033 RID: 51 RVA: 0x000022E4 File Offset: 0x000004E4
		public static string SerializeProductDefs(IEnumerable<ProductDefinition> products)
		{
			List<object> list = new List<object>();
			foreach (ProductDefinition product in products)
			{
				list.Add(JSONSerializer.EncodeProductDef(product));
			}
			return MiniJson.JsonEncode(list);
		}

		// Token: 0x06000034 RID: 52 RVA: 0x00002348 File Offset: 0x00000548
		public static string SerializeProductDescs(ProductDescription product)
		{
			return MiniJson.JsonEncode(JSONSerializer.EncodeProductDesc(product));
		}

		// Token: 0x06000035 RID: 53 RVA: 0x00002368 File Offset: 0x00000568
		public static string SerializeProductDescs(IEnumerable<ProductDescription> products)
		{
			List<object> list = new List<object>();
			foreach (ProductDescription product in products)
			{
				list.Add(JSONSerializer.EncodeProductDesc(product));
			}
			return MiniJson.JsonEncode(list);
		}

		// Token: 0x06000036 RID: 54 RVA: 0x000023CC File Offset: 0x000005CC
		public static List<ProductDescription> DeserializeProductDescriptions(string json)
		{
			List<object> list = (List<object>)MiniJson.JsonDecode(json);
			List<ProductDescription> list2 = new List<ProductDescription>();
			foreach (object obj in list)
			{
				Dictionary<string, object> dictionary = (Dictionary<string, object>)obj;
				ProductMetadata metadata = JSONSerializer.DeserializeMetadata((Dictionary<string, object>)dictionary["metadata"]);
				ProductDescription item = new ProductDescription((string)dictionary["storeSpecificId"], metadata, dictionary.TryGetString("receipt"), dictionary.TryGetString("transactionId"), ProductType.NonConsumable);
				list2.Add(item);
			}
			return list2;
		}

		// Token: 0x06000037 RID: 55 RVA: 0x00002488 File Offset: 0x00000688
		public static Dictionary<string, string> DeserializeSubscriptionDescriptions(string json)
		{
			List<object> list = (List<object>)MiniJson.JsonDecode(json);
			Dictionary<string, string> dictionary = new Dictionary<string, string>();
			foreach (object obj in list)
			{
				Dictionary<string, object> dictionary2 = (Dictionary<string, object>)obj;
				Dictionary<string, object> dic = (Dictionary<string, object>)dictionary2["metadata"];
				string key = (string)dictionary2["storeSpecificId"];
				Dictionary<string, string> dictionary3 = new Dictionary<string, string>();
				dictionary3["introductoryPrice"] = dic.TryGetString("introductoryPrice");
				dictionary3["introductoryPriceLocale"] = dic.TryGetString("introductoryPriceLocale");
				dictionary3["introductoryPriceNumberOfPeriods"] = dic.TryGetString("introductoryPriceNumberOfPeriods");
				dictionary3["numberOfUnits"] = dic.TryGetString("numberOfUnits");
				dictionary3["unit"] = dic.TryGetString("unit");
				bool flag = !string.IsNullOrEmpty(dictionary3["numberOfUnits"]) && string.IsNullOrEmpty(dictionary3["unit"]);
				if (flag)
				{
					dictionary3["unit"] = "0";
				}
				dictionary.Add(key, MiniJson.JsonEncode(dictionary3));
			}
			return dictionary;
		}

		// Token: 0x06000038 RID: 56 RVA: 0x00002600 File Offset: 0x00000800
		public static Dictionary<string, string> DeserializeProductDetails(string json)
		{
			List<object> list = (List<object>)MiniJson.JsonDecode(json);
			Dictionary<string, string> dictionary = new Dictionary<string, string>();
			foreach (object obj in list)
			{
				Dictionary<string, object> dictionary2 = (Dictionary<string, object>)obj;
				Dictionary<string, object> dic = (Dictionary<string, object>)dictionary2["metadata"];
				string key = (string)dictionary2["storeSpecificId"];
				Dictionary<string, string> dictionary3 = new Dictionary<string, string>();
				dictionary3["subscriptionNumberOfUnits"] = dic.TryGetString("subscriptionNumberOfUnits");
				dictionary3["subscriptionPeriodUnit"] = dic.TryGetString("subscriptionPeriodUnit");
				dictionary3["localizedPrice"] = dic.TryGetString("localizedPrice");
				dictionary3["isoCurrencyCode"] = dic.TryGetString("isoCurrencyCode");
				dictionary3["localizedPriceString"] = dic.TryGetString("localizedPriceString");
				dictionary3["localizedTitle"] = dic.TryGetString("localizedTitle");
				dictionary3["localizedDescription"] = dic.TryGetString("localizedDescription");
				dictionary3["introductoryPrice"] = dic.TryGetString("introductoryPrice");
				dictionary3["introductoryPriceLocale"] = dic.TryGetString("introductoryPriceLocale");
				dictionary3["introductoryPriceNumberOfPeriods"] = dic.TryGetString("introductoryPriceNumberOfPeriods");
				dictionary3["numberOfUnits"] = dic.TryGetString("numberOfUnits");
				dictionary3["unit"] = dic.TryGetString("unit");
				bool flag = !string.IsNullOrEmpty(dictionary3["subscriptionNumberOfUnits"]) && string.IsNullOrEmpty(dictionary3["subscriptionPeriodUnit"]);
				if (flag)
				{
					dictionary3["subscriptionPeriodUnit"] = "0";
				}
				bool flag2 = !string.IsNullOrEmpty(dictionary3["numberOfUnits"]) && string.IsNullOrEmpty(dictionary3["unit"]);
				if (flag2)
				{
					dictionary3["unit"] = "0";
				}
				dictionary.Add(key, MiniJson.JsonEncode(dictionary3));
			}
			return dictionary;
		}

		// Token: 0x06000039 RID: 57 RVA: 0x00002868 File Offset: 0x00000A68
		public static PurchaseFailureDescription DeserializeFailureReason(string json)
		{
			Dictionary<string, object> dictionary = (Dictionary<string, object>)MiniJson.JsonDecode(json);
			PurchaseFailureReason reason = PurchaseFailureReason.Unknown;
			bool flag = Enum.IsDefined(typeof(PurchaseFailureReason), (string)dictionary["reason"]);
			if (flag)
			{
				reason = (PurchaseFailureReason)Enum.Parse(typeof(PurchaseFailureReason), (string)dictionary["reason"]);
			}
			return new PurchaseFailureDescription((string)dictionary["productId"], reason, dictionary.TryGetString("message"));
		}

		// Token: 0x0600003A RID: 58 RVA: 0x000028F4 File Offset: 0x00000AF4
		private static ProductMetadata DeserializeMetadata(Dictionary<string, object> data)
		{
			decimal localizedPrice = 0.0m;
			try
			{
				localizedPrice = Convert.ToDecimal(data["localizedPrice"]);
			}
			catch
			{
				localizedPrice = 0.0m;
			}
			return new ProductMetadata(data.TryGetString("localizedPriceString"), data.TryGetString("localizedTitle"), data.TryGetString("localizedDescription"), data.TryGetString("isoCurrencyCode"), localizedPrice);
		}

		// Token: 0x0600003B RID: 59 RVA: 0x0000297C File Offset: 0x00000B7C
		private static Dictionary<string, object> EncodeProductDef(ProductDefinition product)
		{
			Dictionary<string, object> dictionary = new Dictionary<string, object>();
			dictionary.Add("id", product.id);
			dictionary.Add("storeSpecificId", product.storeSpecificId);
			dictionary.Add("type", product.type.ToString());
			bool flag = true;
			PropertyInfo property = typeof(ProductDefinition).GetProperty("enabled");
			bool flag2 = property != null;
			if (flag2)
			{
				try
				{
					flag = Convert.ToBoolean(property.GetValue(product, null));
				}
				catch
				{
					flag = true;
				}
			}
			dictionary.Add("enabled", flag);
			List<object> list = new List<object>();
			PropertyInfo property2 = typeof(ProductDefinition).GetProperty("payouts");
			bool flag3 = property2 != null;
			if (flag3)
			{
				object value = property2.GetValue(product, null);
				Array array = value as Array;
				bool flag4 = array != null;
				if (flag4)
				{
					foreach (object obj in array)
					{
						Dictionary<string, object> dictionary2 = new Dictionary<string, object>();
						Type type = obj.GetType();
						dictionary2["t"] = type.GetField("typeString").GetValue(obj);
						dictionary2["st"] = type.GetField("subtype").GetValue(obj);
						dictionary2["q"] = type.GetField("quantity").GetValue(obj);
						dictionary2["d"] = type.GetField("data").GetValue(obj);
						list.Add(dictionary2);
					}
				}
			}
			dictionary.Add("payouts", list);
			return dictionary;
		}

		// Token: 0x0600003C RID: 60 RVA: 0x00002B7C File Offset: 0x00000D7C
		private static Dictionary<string, object> EncodeProductDesc(ProductDescription product)
		{
			Dictionary<string, object> dictionary = new Dictionary<string, object>();
			dictionary.Add("storeSpecificId", product.storeSpecificId);
			Type typeFromHandle = typeof(ProductDescription);
			FieldInfo field = typeFromHandle.GetField("type");
			bool flag = field != null;
			if (flag)
			{
				object value = field.GetValue(product);
				dictionary.Add("type", value.ToString());
			}
			dictionary.Add("metadata", JSONSerializer.EncodeProductMeta(product.metadata));
			dictionary.Add("receipt", product.receipt);
			dictionary.Add("transactionId", product.transactionId);
			return dictionary;
		}

		// Token: 0x0600003D RID: 61 RVA: 0x00002C24 File Offset: 0x00000E24
		private static Dictionary<string, object> EncodeProductMeta(ProductMetadata product)
		{
			return new Dictionary<string, object>
			{
				{
					"localizedPriceString",
					product.localizedPriceString
				},
				{
					"localizedTitle",
					product.localizedTitle
				},
				{
					"localizedDescription",
					product.localizedDescription
				},
				{
					"isoCurrencyCode",
					product.isoCurrencyCode
				},
				{
					"localizedPrice",
					Convert.ToDouble(product.localizedPrice)
				}
			};
		}

		// Token: 0x0600003E RID: 62 RVA: 0x00002CA1 File Offset: 0x00000EA1
		public JSONSerializer()
		{
		}
	}
}
