﻿using System;
using System.Collections.ObjectModel;

namespace UnityEngine.Purchasing
{
	// Token: 0x0200004B RID: 75
	internal interface INativeUDPStore : INativeStore
	{
		// Token: 0x06000124 RID: 292
		void Initialize(Action<bool, string> callback);

		// Token: 0x06000125 RID: 293
		void Purchase(string productId, Action<bool, string> callback, string developerPayload = null);

		// Token: 0x06000126 RID: 294
		void RetrieveProducts(ReadOnlyCollection<ProductDefinition> products, Action<bool, string> callback);

		// Token: 0x06000127 RID: 295
		void FinishTransaction(ProductDefinition productDefinition, string transactionID);
	}
}
