﻿using System;
using UnityEngine.Purchasing.Extension;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000032 RID: 50
	public interface IGooglePlayConfiguration : IStoreConfiguration
	{
		// Token: 0x060000D0 RID: 208
		void SetPublicKey(string key);
	}
}
