﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.CompilerServices;
using UnityEngine.ChannelPurchase;
using UnityEngine.Purchasing.Extension;
using UnityEngine.Purchasing.MiniJSON;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000042 RID: 66
	internal class UnityChannelBindings : IPurchaseListener, INativeUnityChannelStore, INativeStore
	{
		// Token: 0x060000F5 RID: 245 RVA: 0x00005034 File Offset: 0x00003234
		public void OnPurchase(PurchaseInfo purchaseInfo)
		{
			Dictionary<string, string> obj = UnityChannelBindings.PurchaseInfoToDictionary(purchaseInfo);
			string arg = obj.toJson();
			this.m_PurchaseCallback(true, arg);
			this.m_PurchaseCallback = null;
		}

		// Token: 0x060000F6 RID: 246 RVA: 0x00005068 File Offset: 0x00003268
		public void OnPurchaseFailed(string message, PurchaseInfo purchaseInfo)
		{
			Dictionary<string, object> dictionary = new Dictionary<string, object>();
			dictionary["error"] = message;
			bool flag = purchaseInfo != null;
			if (flag)
			{
				dictionary["purchaseInfo"] = UnityChannelBindings.PurchaseInfoToDictionary(purchaseInfo);
			}
			string arg = dictionary.toJson();
			this.m_PurchaseCallback(false, arg);
			this.m_PurchaseCallback = null;
		}

		// Token: 0x060000F7 RID: 247 RVA: 0x000050C4 File Offset: 0x000032C4
		public void OnPurchaseRepeated(string productCode)
		{
			Dictionary<string, object> dictionary = new Dictionary<string, object>();
			dictionary["error"] = "repeat";
			dictionary["isRepeat"] = true;
			string arg = dictionary.toJson();
			this.m_PurchaseCallback(false, arg);
			this.m_PurchaseCallback = null;
		}

		// Token: 0x060000F8 RID: 248 RVA: 0x00005117 File Offset: 0x00003317
		public void OnPurchaseConfirm(string transactionId)
		{
			this.OnPurchaseConfirmCallbackDispatcher(transactionId, true, transactionId, "");
		}

		// Token: 0x060000F9 RID: 249 RVA: 0x00005129 File Offset: 0x00003329
		public void OnPurchaseConfirmFailed(string transactionId, string message)
		{
			this.OnPurchaseConfirmCallbackDispatcher(transactionId, false, transactionId, message);
		}

		// Token: 0x060000FA RID: 250 RVA: 0x00005137 File Offset: 0x00003337
		protected void OnPurchaseConfirmCallbackDispatcher(string transactionId, bool result, string param1, string param2)
		{
			this.OnResponseCallbackDispatcher(transactionId, result, param1, param2, this.m_PurchaseConfirmCallbacks);
		}

		// Token: 0x060000FB RID: 251 RVA: 0x0000514C File Offset: 0x0000334C
		public void OnReceiptValidate(ReceiptInfo receiptInfo)
		{
			this.OnReceiptValidateCallbackDispatcher(receiptInfo.gameOrderId, true, receiptInfo.signData, receiptInfo.signature);
		}

		// Token: 0x060000FC RID: 252 RVA: 0x00005169 File Offset: 0x00003369
		public void OnReceiptValidateFailed(string transactionId, string message)
		{
			this.OnReceiptValidateCallbackDispatcher(transactionId, false, message, null);
		}

		// Token: 0x060000FD RID: 253 RVA: 0x00005177 File Offset: 0x00003377
		protected void OnReceiptValidateCallbackDispatcher(string transactionId, bool result, string param1, string param2)
		{
			this.OnResponseCallbackDispatcher(transactionId, result, param1, param2, this.m_ValidateCallbacks);
		}

		// Token: 0x060000FE RID: 254 RVA: 0x0000518C File Offset: 0x0000338C
		protected void OnResponseCallbackDispatcher(string transactionId, bool result, string param1, string param2, Dictionary<string, List<Action<bool, string, string>>> callbackDictionary)
		{
			bool flag = !callbackDictionary.ContainsKey(transactionId);
			if (!flag)
			{
				List<Action<bool, string, string>> list = callbackDictionary[transactionId];
				callbackDictionary.Remove(transactionId);
				foreach (Action<bool, string, string> action in list)
				{
					action(result, param1, param2);
				}
			}
		}

		// Token: 0x060000FF RID: 255 RVA: 0x00005208 File Offset: 0x00003408
		public void Purchase(string productId, Action<bool, string> callback, string developerPayload = null)
		{
			bool flag = callback == null;
			if (!flag)
			{
				bool flag2 = this.m_PurchaseCallback != null;
				if (flag2)
				{
					callback(false, "{ \"error\" : \"already purchasing\" }");
				}
				else
				{
					this.m_PurchaseCallback = callback;
					this.m_PurchaseGuid = Guid.NewGuid().ToString();
					PurchaseService.Purchase(productId, this.m_PurchaseGuid, this, developerPayload);
				}
			}
		}

		// Token: 0x06000100 RID: 256 RVA: 0x00005270 File Offset: 0x00003470
		public void RetrieveProducts(ReadOnlyCollection<ProductDefinition> products, Action<bool, string> callback)
		{
			HashSet<ProductDescription> hashSet = new HashSet<ProductDescription>();
			ProductCatalog productCatalog = ProductCatalog.LoadDefaultCatalog();
			foreach (ProductCatalogItem productCatalogItem in productCatalog.allValidProducts)
			{
				foreach (ProductDefinition productDefinition in products)
				{
					bool flag = string.Equals(productCatalogItem.id, productDefinition.id);
					if (flag)
					{
						int num = XiaomiPriceTiers.XiaomiPriceTierPrices[productCatalogItem.xiaomiPriceTier];
						string priceString = string.Format("¥{0:0.00}", num);
						LocalizedProductDescription localizedProductDescription = productCatalogItem.defaultDescription;
						LocalizedProductDescription description = productCatalogItem.GetDescription(TranslationLocale.zh_CN);
						localizedProductDescription = (description ?? localizedProductDescription);
						ProductMetadata metadata = new ProductMetadata(priceString, localizedProductDescription.Title, localizedProductDescription.Description, "CNY", num);
						ProductDescription item = new ProductDescription(productDefinition.storeSpecificId, metadata);
						hashSet.Add(item);
					}
				}
			}
			string arg = JSONSerializer.SerializeProductDescs(hashSet);
			callback(true, arg);
		}

		// Token: 0x06000101 RID: 257 RVA: 0x000053B8 File Offset: 0x000035B8
		public void ValidateReceipt(string transactionId, Action<bool, string, string> callback)
		{
			this.RequestUniquely(transactionId, callback, this.m_ValidateCallbacks, delegate
			{
				PurchaseService.ValidateReceipt(transactionId, this);
			});
		}

		// Token: 0x06000102 RID: 258 RVA: 0x000053FC File Offset: 0x000035FC
		public void ConfirmPurchase(string transactionId, Action<bool, string, string> callback)
		{
			this.RequestUniquely(transactionId, callback, this.m_PurchaseConfirmCallbacks, delegate
			{
				PurchaseService.ConfirmPurchase(transactionId, this);
			});
		}

		// Token: 0x06000103 RID: 259 RVA: 0x00005440 File Offset: 0x00003640
		protected void RequestUniquely(string transactionId, Action<bool, string, string> callback, Dictionary<string, List<Action<bool, string, string>>> callbackDictionary, Action requestAction)
		{
			bool flag = callback == null;
			if (!flag)
			{
				bool flag2 = string.IsNullOrEmpty(transactionId);
				if (flag2)
				{
					callback(false, "{ \"error\" : \"transactionId missing\" }", null);
				}
				else
				{
					bool flag3 = !callbackDictionary.ContainsKey(transactionId);
					if (flag3)
					{
						callbackDictionary.Add(transactionId, new List<Action<bool, string, string>>
						{
							callback
						});
						requestAction();
					}
					else
					{
						callbackDictionary[transactionId].Add(callback);
					}
				}
			}
		}

		// Token: 0x06000104 RID: 260 RVA: 0x000054B3 File Offset: 0x000036B3
		public void RetrieveProducts(string json)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06000105 RID: 261 RVA: 0x000054B3 File Offset: 0x000036B3
		public void Purchase(string productJSON, string developerPayload)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06000106 RID: 262 RVA: 0x000054B3 File Offset: 0x000036B3
		public void FinishTransaction(string productJSON, string transactionID)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06000107 RID: 263 RVA: 0x000054BC File Offset: 0x000036BC
		internal static Dictionary<string, string> PurchaseInfoToDictionary(PurchaseInfo purchaseInfo)
		{
			Dictionary<string, string> dictionary = new Dictionary<string, string>();
			dictionary["gameOrderId"] = purchaseInfo.gameOrderId;
			dictionary["productCode"] = purchaseInfo.productCode;
			dictionary["orderQueryToken"] = purchaseInfo.orderQueryToken;
			dictionary["developerPayload"] = purchaseInfo.developerPayload;
			return dictionary;
		}

		// Token: 0x06000108 RID: 264 RVA: 0x0000551D File Offset: 0x0000371D
		public UnityChannelBindings()
		{
		}

		// Token: 0x040000B3 RID: 179
		protected Action<bool, string> m_PurchaseCallback;

		// Token: 0x040000B4 RID: 180
		protected string m_PurchaseGuid;

		// Token: 0x040000B5 RID: 181
		protected Dictionary<string, List<Action<bool, string, string>>> m_ValidateCallbacks = new Dictionary<string, List<Action<bool, string, string>>>();

		// Token: 0x040000B6 RID: 182
		protected Dictionary<string, List<Action<bool, string, string>>> m_PurchaseConfirmCallbacks = new Dictionary<string, List<Action<bool, string, string>>>();

		// Token: 0x02000043 RID: 67
		[CompilerGenerated]
		private sealed class <>c__DisplayClass16_0
		{
			// Token: 0x06000109 RID: 265 RVA: 0x00002CA1 File Offset: 0x00000EA1
			public <>c__DisplayClass16_0()
			{
			}

			// Token: 0x0600010A RID: 266 RVA: 0x0000553C File Offset: 0x0000373C
			internal void <ValidateReceipt>b__0()
			{
				PurchaseService.ValidateReceipt(this.transactionId, this.<>4__this);
			}

			// Token: 0x040000B7 RID: 183
			public string transactionId;

			// Token: 0x040000B8 RID: 184
			public UnityChannelBindings <>4__this;
		}

		// Token: 0x02000044 RID: 68
		[CompilerGenerated]
		private sealed class <>c__DisplayClass17_0
		{
			// Token: 0x0600010B RID: 267 RVA: 0x00002CA1 File Offset: 0x00000EA1
			public <>c__DisplayClass17_0()
			{
			}

			// Token: 0x0600010C RID: 268 RVA: 0x00005551 File Offset: 0x00003751
			internal void <ConfirmPurchase>b__0()
			{
				PurchaseService.ConfirmPurchase(this.transactionId, this.<>4__this);
			}

			// Token: 0x040000B9 RID: 185
			public string transactionId;

			// Token: 0x040000BA RID: 186
			public UnityChannelBindings <>4__this;
		}
	}
}
