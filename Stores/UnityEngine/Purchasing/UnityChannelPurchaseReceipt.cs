﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000048 RID: 72
	[Serializable]
	public class UnityChannelPurchaseReceipt
	{
		// Token: 0x0600011D RID: 285 RVA: 0x00002CA1 File Offset: 0x00000EA1
		public UnityChannelPurchaseReceipt()
		{
		}

		// Token: 0x040000C5 RID: 197
		public string storeSpecificId;

		// Token: 0x040000C6 RID: 198
		public string transactionId;

		// Token: 0x040000C7 RID: 199
		public string orderQueryToken;

		// Token: 0x040000C8 RID: 200
		public string json;

		// Token: 0x040000C9 RID: 201
		public string signature;

		// Token: 0x040000CA RID: 202
		public string error;
	}
}
