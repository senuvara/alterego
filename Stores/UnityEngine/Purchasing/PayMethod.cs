﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x0200002B RID: 43
	internal class PayMethod
	{
		// Token: 0x060000B8 RID: 184 RVA: 0x00004B78 File Offset: 0x00002D78
		public static void showPayWebView(string paymentURL, string authGlobal, string transactionId, string hashKey, string customID)
		{
			Debug.Log("CloudMoolah PayWebView is being opened");
			bool flag = Application.platform == RuntimePlatform.Android;
			if (flag)
			{
				using (AndroidJavaClass androidJavaClass = new AndroidJavaClass("com.cm.androidforunity.PaymentActivity"))
				{
					AndroidJavaObject androidJavaObject = androidJavaClass.CallStatic<AndroidJavaObject>("instance", new object[0]);
					androidJavaObject.Call("JavaShowPayWebView", new object[]
					{
						paymentURL,
						authGlobal,
						transactionId,
						hashKey,
						customID
					});
				}
			}
		}

		// Token: 0x060000B9 RID: 185 RVA: 0x00004C04 File Offset: 0x00002E04
		public static void showPaySuccess(string title, string msg)
		{
			bool flag = Application.platform == RuntimePlatform.Android;
			if (flag)
			{
				using (AndroidJavaClass androidJavaClass = new AndroidJavaClass("com.cm.androidforunity.PaymentActivity"))
				{
					AndroidJavaObject androidJavaObject = androidJavaClass.CallStatic<AndroidJavaObject>("instance", new object[0]);
					androidJavaObject.Call("showPaySuccess", new object[]
					{
						title,
						msg
					});
				}
			}
		}

		// Token: 0x060000BA RID: 186 RVA: 0x00004C78 File Offset: 0x00002E78
		public static string getDeviceID()
		{
			bool flag = Application.platform == RuntimePlatform.Android;
			if (flag)
			{
				using (AndroidJavaClass androidJavaClass = new AndroidJavaClass("com.cm.androidforunity.PaymentActivity"))
				{
					AndroidJavaObject androidJavaObject = androidJavaClass.CallStatic<AndroidJavaObject>("instance", new object[0]);
					return androidJavaObject.Call<string>("getDeviceID", new object[0]);
				}
			}
			return null;
		}

		// Token: 0x060000BB RID: 187 RVA: 0x00002CA1 File Offset: 0x00000EA1
		public PayMethod()
		{
		}
	}
}
