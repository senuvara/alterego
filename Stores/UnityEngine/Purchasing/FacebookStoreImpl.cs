﻿using System;
using System.Runtime.CompilerServices;
using AOT;
using Uniject;

namespace UnityEngine.Purchasing
{
	// Token: 0x020000B4 RID: 180
	internal class FacebookStoreImpl : JSONStore
	{
		// Token: 0x0600034C RID: 844 RVA: 0x0000DA87 File Offset: 0x0000BC87
		public FacebookStoreImpl(IUtil util)
		{
			FacebookStoreImpl.util = util;
			FacebookStoreImpl.instance = this;
		}

		// Token: 0x0600034D RID: 845 RVA: 0x0000DA9D File Offset: 0x0000BC9D
		public void SetNativeStore(INativeFacebookStore facebook)
		{
			base.SetNativeStore(facebook);
			this.m_Native = facebook;
			facebook.Init();
			facebook.SetUnityPurchasingCallback(new UnityPurchasingCallback(FacebookStoreImpl.MessageCallback));
		}

		// Token: 0x0600034E RID: 846 RVA: 0x0000DACC File Offset: 0x0000BCCC
		public bool consumeItem(string item)
		{
			return this.m_Native.ConsumeItem(item);
		}

		// Token: 0x0600034F RID: 847 RVA: 0x0000DAEC File Offset: 0x0000BCEC
		[MonoPInvokeCallback(typeof(UnityPurchasingCallback))]
		private static void MessageCallback(string subject, string payload, string receipt, string transactionId)
		{
			FacebookStoreImpl.util.RunOnMainThread(delegate
			{
				FacebookStoreImpl.instance.ProcessMessage(subject, payload, receipt, transactionId);
			});
		}

		// Token: 0x06000350 RID: 848 RVA: 0x0000DB34 File Offset: 0x0000BD34
		private void ProcessMessage(string subject, string payload, string receipt, string transactionId)
		{
			if (!(subject == "OnSetupFailed"))
			{
				if (!(subject == "OnProductsRetrieved"))
				{
					if (!(subject == "OnPurchaseSucceeded"))
					{
						if (!(subject == "OnPurchaseFailed"))
						{
							if (!(subject == "SendPurchasingEvent"))
							{
							}
						}
						else
						{
							base.OnPurchaseFailed(payload);
						}
					}
					else
					{
						this.OnPurchaseSucceeded(payload, receipt, transactionId);
					}
				}
				else
				{
					this.OnProductsRetrieved(payload);
				}
			}
			else
			{
				base.OnSetupFailed(payload);
			}
		}

		// Token: 0x04000283 RID: 643
		private INativeFacebookStore m_Native;

		// Token: 0x04000284 RID: 644
		private static IUtil util;

		// Token: 0x04000285 RID: 645
		private static FacebookStoreImpl instance;

		// Token: 0x020000B5 RID: 181
		[CompilerGenerated]
		private sealed class <>c__DisplayClass6_0
		{
			// Token: 0x06000351 RID: 849 RVA: 0x00002CA1 File Offset: 0x00000EA1
			public <>c__DisplayClass6_0()
			{
			}

			// Token: 0x06000352 RID: 850 RVA: 0x0000DBB4 File Offset: 0x0000BDB4
			internal void <MessageCallback>b__0()
			{
				FacebookStoreImpl.instance.ProcessMessage(this.subject, this.payload, this.receipt, this.transactionId);
			}

			// Token: 0x04000286 RID: 646
			public string subject;

			// Token: 0x04000287 RID: 647
			public string payload;

			// Token: 0x04000288 RID: 648
			public string receipt;

			// Token: 0x04000289 RID: 649
			public string transactionId;
		}
	}
}
