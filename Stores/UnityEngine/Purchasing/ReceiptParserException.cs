﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x0200009A RID: 154
	public class ReceiptParserException : Exception
	{
		// Token: 0x0600030F RID: 783 RVA: 0x0000D2D9 File Offset: 0x0000B4D9
		public ReceiptParserException()
		{
		}

		// Token: 0x06000310 RID: 784 RVA: 0x0000D2E3 File Offset: 0x0000B4E3
		public ReceiptParserException(string message) : base(message)
		{
		}
	}
}
