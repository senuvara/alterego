﻿using System;
using UnityEngine.Purchasing.Extension;

namespace UnityEngine.Purchasing
{
	// Token: 0x0200002F RID: 47
	internal class FakeGooglePlayConfiguration : IGooglePlayConfiguration, IStoreConfiguration
	{
		// Token: 0x060000C5 RID: 197 RVA: 0x00002EA8 File Offset: 0x000010A8
		public void SetPublicKey(string key)
		{
		}

		// Token: 0x060000C6 RID: 198 RVA: 0x00002CA1 File Offset: 0x00000EA1
		public FakeGooglePlayConfiguration()
		{
		}
	}
}
