﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000061 RID: 97
	internal interface IStoreInternal
	{
		// Token: 0x0600019D RID: 413
		void SetModule(StandardPurchasingModule module);
	}
}
