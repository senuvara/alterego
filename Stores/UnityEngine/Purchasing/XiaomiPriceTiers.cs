﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000041 RID: 65
	internal class XiaomiPriceTiers
	{
		// Token: 0x060000F3 RID: 243 RVA: 0x00002CA1 File Offset: 0x00000EA1
		public XiaomiPriceTiers()
		{
		}

		// Token: 0x060000F4 RID: 244 RVA: 0x0000501B File Offset: 0x0000321B
		// Note: this type is marked as 'beforefieldinit'.
		static XiaomiPriceTiers()
		{
		}

		// Token: 0x040000B2 RID: 178
		public static int[] XiaomiPriceTierPrices = new int[]
		{
			0,
			1,
			3,
			6,
			8,
			12,
			18,
			25,
			28,
			30,
			40,
			45,
			50,
			60,
			68,
			73,
			78,
			88,
			93,
			98,
			108,
			113,
			118,
			123,
			128,
			138,
			148,
			153,
			158,
			163,
			168,
			178,
			188,
			193,
			198,
			208,
			218,
			223,
			228,
			233,
			238,
			243,
			248,
			253,
			258,
			263,
			268,
			273,
			278,
			283,
			288,
			298,
			308,
			318,
			328,
			348,
			388,
			418,
			448,
			488,
			518,
			548,
			588,
			618,
			648,
			698,
			798,
			818,
			848,
			898,
			998,
			1048,
			1098,
			1148,
			1198,
			1248,
			1298,
			1398,
			1448,
			1498,
			1598,
			1648,
			1998,
			2298,
			2598,
			2998,
			3298,
			3998,
			4498,
			4998,
			5898,
			6498
		};
	}
}
