﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000009 RID: 9
	internal class JavaBridge : AndroidJavaProxy, IUnityCallback
	{
		// Token: 0x0600002B RID: 43 RVA: 0x00002217 File Offset: 0x00000417
		public JavaBridge(IUnityCallback forwardTo) : base("com.unity.purchasing.common.IUnityCallback")
		{
			this.forwardTo = forwardTo;
		}

		// Token: 0x0600002C RID: 44 RVA: 0x0000222D File Offset: 0x0000042D
		public JavaBridge(IUnityCallback forwardTo, string javaInterface) : base(javaInterface)
		{
			this.forwardTo = forwardTo;
		}

		// Token: 0x0600002D RID: 45 RVA: 0x0000223F File Offset: 0x0000043F
		public void OnSetupFailed(string json)
		{
			this.forwardTo.OnSetupFailed(json);
		}

		// Token: 0x0600002E RID: 46 RVA: 0x0000224F File Offset: 0x0000044F
		public void OnProductsRetrieved(string json)
		{
			this.forwardTo.OnProductsRetrieved(json);
		}

		// Token: 0x0600002F RID: 47 RVA: 0x0000225F File Offset: 0x0000045F
		public void OnPurchaseSucceeded(string id, string receipt, string transactionID)
		{
			this.forwardTo.OnPurchaseSucceeded(id, receipt, transactionID);
		}

		// Token: 0x06000030 RID: 48 RVA: 0x00002271 File Offset: 0x00000471
		public void OnPurchaseFailed(string json)
		{
			this.forwardTo.OnPurchaseFailed(json);
		}

		// Token: 0x0400000F RID: 15
		private IUnityCallback forwardTo;
	}
}
