﻿using System;
using System.Collections.Generic;
using System.Xml;
using UnityEngine.Purchasing.Security;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000096 RID: 150
	public class SubscriptionInfo
	{
		// Token: 0x060002F3 RID: 755 RVA: 0x0000C64C File Offset: 0x0000A84C
		public SubscriptionInfo(AppleInAppPurchaseReceipt r, string intro_json)
		{
			AppleStoreProductType appleStoreProductType = (AppleStoreProductType)Enum.Parse(typeof(AppleStoreProductType), r.productType.ToString());
			bool flag = appleStoreProductType == AppleStoreProductType.Consumable || appleStoreProductType == AppleStoreProductType.NonConsumable;
			if (flag)
			{
				throw new InvalidProductTypeException();
			}
			bool flag2 = !string.IsNullOrEmpty(intro_json);
			if (flag2)
			{
				Dictionary<string, object> dic = (Dictionary<string, object>)MiniJson.JsonDecode(intro_json);
				int num = -1;
				SubscriptionPeriodUnit subscriptionPeriodUnit = SubscriptionPeriodUnit.NotAvailable;
				this.introductory_price = dic.TryGetString("introductoryPrice") + dic.TryGetString("introductoryPriceLocale");
				bool flag3 = string.IsNullOrEmpty(this.introductory_price);
				if (flag3)
				{
					this.introductory_price = "not available";
				}
				else
				{
					try
					{
						this.introductory_price_cycles = Convert.ToInt64(dic.TryGetString("introductoryPriceNumberOfPeriods"));
						num = Convert.ToInt32(dic.TryGetString("numberOfUnits"));
						subscriptionPeriodUnit = (SubscriptionPeriodUnit)Convert.ToInt32(dic.TryGetString("unit"));
					}
					catch (Exception message)
					{
						Debug.unityLogger.Log("Unable to parse introductory period cycles and duration, this product does not have configuration of introductory price period", message);
						subscriptionPeriodUnit = SubscriptionPeriodUnit.NotAvailable;
					}
				}
				DateTime now = DateTime.Now;
				switch (subscriptionPeriodUnit)
				{
				case SubscriptionPeriodUnit.Day:
					this.introductory_price_period = TimeSpan.FromTicks(TimeSpan.FromDays(1.0).Ticks * (long)num);
					break;
				case SubscriptionPeriodUnit.Month:
					this.introductory_price_period = TimeSpan.FromTicks((now.AddMonths(1) - now).Ticks * (long)num);
					break;
				case SubscriptionPeriodUnit.Week:
					this.introductory_price_period = TimeSpan.FromTicks(TimeSpan.FromDays(7.0).Ticks * (long)num);
					break;
				case SubscriptionPeriodUnit.Year:
					this.introductory_price_period = TimeSpan.FromTicks((now.AddYears(1) - now).Ticks * (long)num);
					break;
				case SubscriptionPeriodUnit.NotAvailable:
					this.introductory_price_period = TimeSpan.Zero;
					this.introductory_price_cycles = 0L;
					break;
				}
			}
			else
			{
				this.introductory_price = "not available";
				this.introductory_price_period = TimeSpan.Zero;
				this.introductory_price_cycles = 0L;
			}
			DateTime utcNow = DateTime.UtcNow;
			this.purchaseDate = r.purchaseDate;
			this.productId = r.productID;
			this.subscriptionExpireDate = r.subscriptionExpirationDate;
			this.subscriptionCancelDate = r.cancellationDate;
			bool flag4 = appleStoreProductType == AppleStoreProductType.NonRenewingSubscription;
			if (flag4)
			{
				this.is_subscribed = Result.Unsupported;
				this.is_expired = Result.Unsupported;
				this.is_cancelled = Result.Unsupported;
				this.is_free_trial = Result.Unsupported;
				this.is_auto_renewing = Result.Unsupported;
				this.is_introductory_price_period = Result.Unsupported;
			}
			else
			{
				this.is_cancelled = ((r.cancellationDate.Ticks > 0L && r.cancellationDate.Ticks < utcNow.Ticks) ? Result.True : Result.False);
				this.is_subscribed = ((r.subscriptionExpirationDate.Ticks >= utcNow.Ticks) ? Result.True : Result.False);
				this.is_expired = ((r.subscriptionExpirationDate.Ticks > 0L && r.subscriptionExpirationDate.Ticks < utcNow.Ticks) ? Result.True : Result.False);
				this.is_free_trial = ((r.isFreeTrial == 1) ? Result.True : Result.False);
				this.is_auto_renewing = ((appleStoreProductType == AppleStoreProductType.AutoRenewingSubscription && this.is_cancelled == Result.False && this.is_expired == Result.False) ? Result.True : Result.False);
				this.is_introductory_price_period = ((r.isIntroductoryPricePeriod == 1) ? Result.True : Result.False);
			}
			bool flag5 = this.is_subscribed == Result.True;
			if (flag5)
			{
				this.remainedTime = r.subscriptionExpirationDate.Subtract(utcNow);
			}
			else
			{
				this.remainedTime = TimeSpan.Zero;
			}
		}

		// Token: 0x060002F4 RID: 756 RVA: 0x0000C9F4 File Offset: 0x0000ABF4
		public SubscriptionInfo(string skuDetails, bool isAutoRenewing, DateTime purchaseDate, bool isFreeTrial, bool hasIntroductoryPriceTrial, bool purchaseHistorySupported, string updateMetadata)
		{
			Dictionary<string, object> dictionary = (Dictionary<string, object>)MiniJson.JsonDecode(skuDetails);
			bool flag = (string)dictionary["type"] == "inapp";
			if (flag)
			{
				throw new InvalidProductTypeException();
			}
			this.productId = (string)dictionary["productId"];
			this.purchaseDate = purchaseDate;
			this.is_subscribed = Result.True;
			this.is_auto_renewing = (isAutoRenewing ? Result.True : Result.False);
			this.is_expired = Result.False;
			this.is_cancelled = (isAutoRenewing ? Result.False : Result.True);
			this.is_free_trial = Result.False;
			string text = null;
			bool flag2 = dictionary.ContainsKey("subscriptionPeriod");
			if (flag2)
			{
				text = (string)dictionary["subscriptionPeriod"];
			}
			string period_string = null;
			bool flag3 = dictionary.ContainsKey("freeTrialPeriod");
			if (flag3)
			{
				period_string = (string)dictionary["freeTrialPeriod"];
			}
			string text2 = null;
			bool flag4 = dictionary.ContainsKey("introductoryPrice");
			if (flag4)
			{
				text2 = (string)dictionary["introductoryPrice"];
			}
			string text3 = null;
			bool flag5 = dictionary.ContainsKey("introductoryPricePeriod");
			if (flag5)
			{
				text3 = (string)dictionary["introductoryPricePeriod"];
			}
			long num = 0L;
			bool flag6 = dictionary.ContainsKey("introductoryPriceCycles");
			if (flag6)
			{
				num = (long)dictionary["introductoryPriceCycles"];
			}
			this.free_trial_period_string = period_string;
			this.subscriptionPeriod = this.computePeriodTimeSpan(this.parsePeriodTimeSpanUnits(text));
			this.freeTrialPeriod = TimeSpan.Zero;
			if (isFreeTrial)
			{
				this.freeTrialPeriod = this.parseTimeSpan(period_string);
			}
			this.introductory_price = text2;
			this.introductory_price_cycles = num;
			this.introductory_price_period = TimeSpan.Zero;
			this.is_introductory_price_period = Result.False;
			TimeSpan ts = TimeSpan.Zero;
			if (hasIntroductoryPriceTrial)
			{
				bool flag7 = text3 != null && text3.Equals(text);
				if (flag7)
				{
					this.introductory_price_period = this.subscriptionPeriod;
				}
				else
				{
					this.introductory_price_period = this.parseTimeSpan(text3);
				}
				ts = this.accumulateIntroductoryDuration(this.parsePeriodTimeSpanUnits(text3), this.introductory_price_cycles);
			}
			TimeSpan timeSpan = TimeSpan.FromSeconds((updateMetadata == null) ? 0.0 : this.computeExtraTime(updateMetadata, this.subscriptionPeriod.TotalSeconds));
			TimeSpan t = DateTime.UtcNow.Subtract(purchaseDate);
			bool flag8 = t <= timeSpan;
			if (flag8)
			{
				this.subscriptionExpireDate = purchaseDate.Add(timeSpan);
			}
			else
			{
				bool flag9 = t <= this.freeTrialPeriod.Add(timeSpan);
				if (flag9)
				{
					this.is_free_trial = Result.True;
					this.subscriptionExpireDate = purchaseDate.Add(this.freeTrialPeriod.Add(timeSpan));
				}
				else
				{
					bool flag10 = t < this.freeTrialPeriod.Add(timeSpan).Add(ts);
					if (flag10)
					{
						this.is_introductory_price_period = Result.True;
						DateTime billing_begin_date = this.purchaseDate.Add(this.freeTrialPeriod.Add(timeSpan));
						this.subscriptionExpireDate = this.nextBillingDate(billing_begin_date, this.parsePeriodTimeSpanUnits(text3));
					}
					else
					{
						DateTime billing_begin_date2 = this.purchaseDate.Add(this.freeTrialPeriod.Add(timeSpan).Add(ts));
						this.subscriptionExpireDate = this.nextBillingDate(billing_begin_date2, this.parsePeriodTimeSpanUnits(text));
					}
				}
			}
			this.remainedTime = this.subscriptionExpireDate.Subtract(DateTime.UtcNow);
			this.sku_details = skuDetails;
			bool flag11 = !purchaseHistorySupported;
			if (flag11)
			{
				this.is_free_trial = Result.Unsupported;
				this.subscriptionExpireDate = DateTime.MaxValue;
				this.remainedTime = TimeSpan.MaxValue;
				this.is_introductory_price_period = Result.Unsupported;
			}
		}

		// Token: 0x060002F5 RID: 757 RVA: 0x0000CD90 File Offset: 0x0000AF90
		public SubscriptionInfo(string productId)
		{
			this.productId = productId;
			this.is_subscribed = Result.True;
			this.is_expired = Result.False;
			this.is_cancelled = Result.Unsupported;
			this.is_free_trial = Result.Unsupported;
			this.is_auto_renewing = Result.Unsupported;
			this.remainedTime = TimeSpan.MaxValue;
			this.is_introductory_price_period = Result.Unsupported;
			this.introductory_price_period = TimeSpan.MaxValue;
			this.introductory_price = null;
			this.introductory_price_cycles = 0L;
		}

		// Token: 0x060002F6 RID: 758 RVA: 0x0000CDFC File Offset: 0x0000AFFC
		public string getProductId()
		{
			return this.productId;
		}

		// Token: 0x060002F7 RID: 759 RVA: 0x0000CE14 File Offset: 0x0000B014
		public DateTime getPurchaseDate()
		{
			return this.purchaseDate;
		}

		// Token: 0x060002F8 RID: 760 RVA: 0x0000CE2C File Offset: 0x0000B02C
		public Result isSubscribed()
		{
			return this.is_subscribed;
		}

		// Token: 0x060002F9 RID: 761 RVA: 0x0000CE44 File Offset: 0x0000B044
		public Result isExpired()
		{
			return this.is_expired;
		}

		// Token: 0x060002FA RID: 762 RVA: 0x0000CE5C File Offset: 0x0000B05C
		public Result isCancelled()
		{
			return this.is_cancelled;
		}

		// Token: 0x060002FB RID: 763 RVA: 0x0000CE74 File Offset: 0x0000B074
		public Result isFreeTrial()
		{
			return this.is_free_trial;
		}

		// Token: 0x060002FC RID: 764 RVA: 0x0000CE8C File Offset: 0x0000B08C
		public Result isAutoRenewing()
		{
			return this.is_auto_renewing;
		}

		// Token: 0x060002FD RID: 765 RVA: 0x0000CEA4 File Offset: 0x0000B0A4
		public TimeSpan getRemainingTime()
		{
			return this.remainedTime;
		}

		// Token: 0x060002FE RID: 766 RVA: 0x0000CEBC File Offset: 0x0000B0BC
		public Result isIntroductoryPricePeriod()
		{
			return this.is_introductory_price_period;
		}

		// Token: 0x060002FF RID: 767 RVA: 0x0000CED4 File Offset: 0x0000B0D4
		public TimeSpan getIntroductoryPricePeriod()
		{
			return this.introductory_price_period;
		}

		// Token: 0x06000300 RID: 768 RVA: 0x0000CEEC File Offset: 0x0000B0EC
		public string getIntroductoryPrice()
		{
			return string.IsNullOrEmpty(this.introductory_price) ? "not available" : this.introductory_price;
		}

		// Token: 0x06000301 RID: 769 RVA: 0x0000CF18 File Offset: 0x0000B118
		public long getIntroductoryPricePeriodCycles()
		{
			return this.introductory_price_cycles;
		}

		// Token: 0x06000302 RID: 770 RVA: 0x0000CF30 File Offset: 0x0000B130
		public DateTime getExpireDate()
		{
			return this.subscriptionExpireDate;
		}

		// Token: 0x06000303 RID: 771 RVA: 0x0000CF48 File Offset: 0x0000B148
		public DateTime getCancelDate()
		{
			return this.subscriptionCancelDate;
		}

		// Token: 0x06000304 RID: 772 RVA: 0x0000CF60 File Offset: 0x0000B160
		public TimeSpan getFreeTrialPeriod()
		{
			return this.freeTrialPeriod;
		}

		// Token: 0x06000305 RID: 773 RVA: 0x0000CF78 File Offset: 0x0000B178
		public TimeSpan getSubscriptionPeriod()
		{
			return this.subscriptionPeriod;
		}

		// Token: 0x06000306 RID: 774 RVA: 0x0000CF90 File Offset: 0x0000B190
		public string getFreeTrialPeriodString()
		{
			return this.free_trial_period_string;
		}

		// Token: 0x06000307 RID: 775 RVA: 0x0000CFA8 File Offset: 0x0000B1A8
		public string getSkuDetails()
		{
			return this.sku_details;
		}

		// Token: 0x06000308 RID: 776 RVA: 0x0000CFC0 File Offset: 0x0000B1C0
		public string getSubscriptionInfoJsonString()
		{
			return MiniJson.JsonEncode(new Dictionary<string, object>
			{
				{
					"productId",
					this.productId
				},
				{
					"is_free_trial",
					this.is_free_trial
				},
				{
					"is_introductory_price_period",
					this.is_introductory_price_period == Result.True
				},
				{
					"remaining_time_in_seconds",
					this.remainedTime.TotalSeconds
				}
			});
		}

		// Token: 0x06000309 RID: 777 RVA: 0x0000D040 File Offset: 0x0000B240
		private DateTime nextBillingDate(DateTime billing_begin_date, TimeSpanUnits units)
		{
			DateTime dateTime = billing_begin_date;
			while (DateTime.Compare(dateTime, DateTime.UtcNow) <= 0)
			{
				dateTime = dateTime.AddDays(units.days).AddMonths(units.months).AddYears(units.years);
			}
			return dateTime;
		}

		// Token: 0x0600030A RID: 778 RVA: 0x0000D098 File Offset: 0x0000B298
		private TimeSpan accumulateIntroductoryDuration(TimeSpanUnits units, long cycles)
		{
			TimeSpan result = TimeSpan.Zero;
			for (long num = 0L; num < cycles; num += 1L)
			{
				result = result.Add(this.computePeriodTimeSpan(units));
			}
			return result;
		}

		// Token: 0x0600030B RID: 779 RVA: 0x0000D0D4 File Offset: 0x0000B2D4
		private TimeSpan computePeriodTimeSpan(TimeSpanUnits units)
		{
			DateTime now = DateTime.Now;
			return now.AddDays(units.days).AddMonths(units.months).AddYears(units.years).Subtract(now);
		}

		// Token: 0x0600030C RID: 780 RVA: 0x0000D120 File Offset: 0x0000B320
		private double computeExtraTime(string metadata, double new_sku_period_in_seconds)
		{
			Dictionary<string, object> dictionary = (Dictionary<string, object>)MiniJson.JsonDecode(metadata);
			long num = (long)dictionary["old_sku_remaining_seconds"];
			long num2 = (long)dictionary["old_sku_price_in_micros"];
			double totalSeconds = this.parseTimeSpan((string)dictionary["old_sku_period_string"]).TotalSeconds;
			long num3 = (long)dictionary["new_sku_price_in_micros"];
			return (double)num / totalSeconds * (double)num2 / (double)num3 * new_sku_period_in_seconds;
		}

		// Token: 0x0600030D RID: 781 RVA: 0x0000D1A8 File Offset: 0x0000B3A8
		private TimeSpan parseTimeSpan(string period_string)
		{
			TimeSpan result = TimeSpan.Zero;
			try
			{
				result = XmlConvert.ToTimeSpan(period_string);
			}
			catch (Exception)
			{
				bool flag = period_string == null || period_string.Length == 0;
				if (flag)
				{
					result = TimeSpan.Zero;
				}
				else
				{
					result = new TimeSpan(7, 0, 0, 0);
				}
			}
			return result;
		}

		// Token: 0x0600030E RID: 782 RVA: 0x0000D20C File Offset: 0x0000B40C
		private TimeSpanUnits parsePeriodTimeSpanUnits(string time_span)
		{
			TimeSpanUnits result;
			if (!(time_span == "P1W"))
			{
				if (!(time_span == "P1M"))
				{
					if (!(time_span == "P3M"))
					{
						if (!(time_span == "P6M"))
						{
							if (!(time_span == "P1Y"))
							{
								result = new TimeSpanUnits((double)this.parseTimeSpan(time_span).Days, 0, 0);
							}
							else
							{
								result = new TimeSpanUnits(0.0, 0, 1);
							}
						}
						else
						{
							result = new TimeSpanUnits(0.0, 6, 0);
						}
					}
					else
					{
						result = new TimeSpanUnits(0.0, 3, 0);
					}
				}
				else
				{
					result = new TimeSpanUnits(0.0, 1, 0);
				}
			}
			else
			{
				result = new TimeSpanUnits(7.0, 0, 0);
			}
			return result;
		}

		// Token: 0x04000240 RID: 576
		private Result is_subscribed;

		// Token: 0x04000241 RID: 577
		private Result is_expired;

		// Token: 0x04000242 RID: 578
		private Result is_cancelled;

		// Token: 0x04000243 RID: 579
		private Result is_free_trial;

		// Token: 0x04000244 RID: 580
		private Result is_auto_renewing;

		// Token: 0x04000245 RID: 581
		private Result is_introductory_price_period;

		// Token: 0x04000246 RID: 582
		private string productId;

		// Token: 0x04000247 RID: 583
		private DateTime purchaseDate;

		// Token: 0x04000248 RID: 584
		private DateTime subscriptionExpireDate;

		// Token: 0x04000249 RID: 585
		private DateTime subscriptionCancelDate;

		// Token: 0x0400024A RID: 586
		private TimeSpan remainedTime;

		// Token: 0x0400024B RID: 587
		private string introductory_price;

		// Token: 0x0400024C RID: 588
		private TimeSpan introductory_price_period;

		// Token: 0x0400024D RID: 589
		private long introductory_price_cycles;

		// Token: 0x0400024E RID: 590
		private TimeSpan freeTrialPeriod;

		// Token: 0x0400024F RID: 591
		private TimeSpan subscriptionPeriod;

		// Token: 0x04000250 RID: 592
		private string free_trial_period_string;

		// Token: 0x04000251 RID: 593
		private string sku_details;
	}
}
