﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using UnityEngine.Purchasing.MiniJSON;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000073 RID: 115
	internal class PurchasingEvent
	{
		// Token: 0x06000207 RID: 519 RVA: 0x000091E6 File Offset: 0x000073E6
		public PurchasingEvent(Dictionary<string, object> eventDict)
		{
			this.EventDict = eventDict;
		}

		// Token: 0x06000208 RID: 520 RVA: 0x000091F8 File Offset: 0x000073F8
		public string FlatJSON(Dictionary<string, object> profileDict)
		{
			Dictionary<string, object> obj = profileDict.Concat(this.EventDict).ToDictionary((KeyValuePair<string, object> s) => s.Key, (KeyValuePair<string, object> s) => s.Value);
			string text = Json.Serialize(obj);
			return (text != null) ? text : string.Empty;
		}

		// Token: 0x0400015D RID: 349
		private Dictionary<string, object> EventDict;

		// Token: 0x02000074 RID: 116
		[CompilerGenerated]
		[Serializable]
		private sealed class <>c
		{
			// Token: 0x06000209 RID: 521 RVA: 0x0000926C File Offset: 0x0000746C
			// Note: this type is marked as 'beforefieldinit'.
			static <>c()
			{
			}

			// Token: 0x0600020A RID: 522 RVA: 0x00002CA1 File Offset: 0x00000EA1
			public <>c()
			{
			}

			// Token: 0x0600020B RID: 523 RVA: 0x00009278 File Offset: 0x00007478
			internal string <FlatJSON>b__2_0(KeyValuePair<string, object> s)
			{
				return s.Key;
			}

			// Token: 0x0600020C RID: 524 RVA: 0x00009281 File Offset: 0x00007481
			internal object <FlatJSON>b__2_1(KeyValuePair<string, object> s)
			{
				return s.Value;
			}

			// Token: 0x0400015E RID: 350
			public static readonly PurchasingEvent.<>c <>9 = new PurchasingEvent.<>c();

			// Token: 0x0400015F RID: 351
			public static Func<KeyValuePair<string, object>, string> <>9__2_0;

			// Token: 0x04000160 RID: 352
			public static Func<KeyValuePair<string, object>, object> <>9__2_1;
		}
	}
}
