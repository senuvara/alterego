﻿using System;
using UnityEngine.Purchasing.Extension;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000036 RID: 54
	public interface ISamsungAppsConfiguration : IStoreConfiguration
	{
		// Token: 0x060000D9 RID: 217
		void SetMode(SamsungAppsMode mode);
	}
}
