﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x0200001B RID: 27
	public enum LoginResultState
	{
		// Token: 0x04000026 RID: 38
		LoginSucceed,
		// Token: 0x04000027 RID: 39
		UserNotExists,
		// Token: 0x04000028 RID: 40
		PasswordError,
		// Token: 0x04000029 RID: 41
		UserOrPasswordEmpty,
		// Token: 0x0400002A RID: 42
		LoginCallBackIsNull,
		// Token: 0x0400002B RID: 43
		NetworkError,
		// Token: 0x0400002C RID: 44
		NotKnown
	}
}
