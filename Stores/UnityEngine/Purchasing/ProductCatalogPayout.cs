﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000080 RID: 128
	[Serializable]
	public class ProductCatalogPayout
	{
		// Token: 0x17000052 RID: 82
		// (get) Token: 0x06000236 RID: 566 RVA: 0x00009BC8 File Offset: 0x00007DC8
		// (set) Token: 0x06000237 RID: 567 RVA: 0x00009C11 File Offset: 0x00007E11
		public ProductCatalogPayout.ProductCatalogPayoutType type
		{
			get
			{
				ProductCatalogPayout.ProductCatalogPayoutType result = ProductCatalogPayout.ProductCatalogPayoutType.Other;
				bool flag = Enum.IsDefined(typeof(ProductCatalogPayout.ProductCatalogPayoutType), this.t);
				if (flag)
				{
					result = (ProductCatalogPayout.ProductCatalogPayoutType)Enum.Parse(typeof(ProductCatalogPayout.ProductCatalogPayoutType), this.t);
				}
				return result;
			}
			set
			{
				this.t = value.ToString();
			}
		}

		// Token: 0x17000053 RID: 83
		// (get) Token: 0x06000238 RID: 568 RVA: 0x00009C28 File Offset: 0x00007E28
		public string typeString
		{
			get
			{
				return this.t;
			}
		}

		// Token: 0x17000054 RID: 84
		// (get) Token: 0x06000239 RID: 569 RVA: 0x00009C40 File Offset: 0x00007E40
		// (set) Token: 0x0600023A RID: 570 RVA: 0x00009C58 File Offset: 0x00007E58
		public string subtype
		{
			get
			{
				return this.st;
			}
			set
			{
				bool flag = value.Length > 64;
				if (flag)
				{
					throw new ArgumentException(string.Format("subtype should be no longer than {0} characters", 64));
				}
				this.st = value;
			}
		}

		// Token: 0x17000055 RID: 85
		// (get) Token: 0x0600023B RID: 571 RVA: 0x00009C94 File Offset: 0x00007E94
		// (set) Token: 0x0600023C RID: 572 RVA: 0x00009CAC File Offset: 0x00007EAC
		public double quantity
		{
			get
			{
				return this.q;
			}
			set
			{
				this.q = value;
			}
		}

		// Token: 0x17000056 RID: 86
		// (get) Token: 0x0600023D RID: 573 RVA: 0x00009CB8 File Offset: 0x00007EB8
		// (set) Token: 0x0600023E RID: 574 RVA: 0x00009CD0 File Offset: 0x00007ED0
		public string data
		{
			get
			{
				return this.d;
			}
			set
			{
				bool flag = value.Length > 1024;
				if (flag)
				{
					throw new ArgumentException(string.Format("data should be no longer than {0} characters", 1024));
				}
				this.d = value;
			}
		}

		// Token: 0x0600023F RID: 575 RVA: 0x00009D10 File Offset: 0x00007F10
		public ProductCatalogPayout()
		{
		}

		// Token: 0x040001A0 RID: 416
		[SerializeField]
		private string t = ProductCatalogPayout.ProductCatalogPayoutType.Other.ToString();

		// Token: 0x040001A1 RID: 417
		public const int MaxSubtypeLength = 64;

		// Token: 0x040001A2 RID: 418
		[SerializeField]
		private string st = string.Empty;

		// Token: 0x040001A3 RID: 419
		[SerializeField]
		private double q;

		// Token: 0x040001A4 RID: 420
		public const int MaxDataLength = 1024;

		// Token: 0x040001A5 RID: 421
		[SerializeField]
		private string d = string.Empty;

		// Token: 0x02000081 RID: 129
		public enum ProductCatalogPayoutType
		{
			// Token: 0x040001A7 RID: 423
			Other,
			// Token: 0x040001A8 RID: 424
			Currency,
			// Token: 0x040001A9 RID: 425
			Item,
			// Token: 0x040001AA RID: 426
			Resource
		}
	}
}
