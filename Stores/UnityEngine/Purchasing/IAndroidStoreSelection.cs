﻿using System;
using UnityEngine.Purchasing.Extension;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000007 RID: 7
	public interface IAndroidStoreSelection : IStoreConfiguration
	{
		// Token: 0x17000015 RID: 21
		// (get) Token: 0x06000025 RID: 37
		AndroidStore androidStore { get; }

		// Token: 0x17000016 RID: 22
		// (get) Token: 0x06000026 RID: 38
		AppStore appStore { get; }
	}
}
