﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Reflection;
using System.Runtime.CompilerServices;
using UnityEngine.Purchasing.Extension;
using UnityEngine.Purchasing.MiniJSON;
using UnityEngine.UDP;

namespace UnityEngine.Purchasing
{
	// Token: 0x0200004F RID: 79
	internal class UDPImpl : JSONStore, IUDPExtensions, IStoreExtension
	{
		// Token: 0x06000140 RID: 320 RVA: 0x0000611A File Offset: 0x0000431A
		public void SetNativeStore(INativeUDPStore nativeUdpStore)
		{
			this.m_Bindings = nativeUdpStore;
		}

		// Token: 0x06000141 RID: 321 RVA: 0x00006124 File Offset: 0x00004324
		public override void Initialize(IStoreCallback callback)
		{
			this.unity = callback;
		}

		// Token: 0x06000142 RID: 322 RVA: 0x00006130 File Offset: 0x00004330
		public override void RetrieveProducts(ReadOnlyCollection<ProductDefinition> products)
		{
			Action<bool, string> retrieveCallback = delegate(bool success, string json)
			{
				bool flag2 = success && !string.IsNullOrEmpty(json);
				if (flag2)
				{
					this.OnProductsRetrieved(json);
				}
				else
				{
					this.m_Logger.Log("RetrieveProducts failed: " + json);
				}
			};
			bool flag = !this.m_Initialized;
			if (flag)
			{
				this.m_Bindings.Initialize(delegate(bool success, string message)
				{
					this.m_LastInitError = "";
					this.m_UserInfo = null;
					if (success)
					{
						bool flag2 = !string.IsNullOrEmpty(message);
						if (flag2)
						{
							Dictionary<string, object> dictionary = message.HashtableFromJson();
							bool flag3 = dictionary.ContainsKey("Channel");
							if (flag3)
							{
								this.m_UserInfo = new UserInfo();
								UDPImpl.DictionaryToStringProperty(dictionary, this.m_UserInfo);
							}
						}
						this.m_Initialized = true;
						this.m_Bindings.RetrieveProducts(products, retrieveCallback);
					}
					else
					{
						this.m_LastInitError = message;
						this.unity.OnSetupFailed(InitializationFailureReason.AppNotKnown);
					}
				});
			}
			else
			{
				this.m_Bindings.RetrieveProducts(products, retrieveCallback);
			}
		}

		// Token: 0x06000143 RID: 323 RVA: 0x000061A8 File Offset: 0x000043A8
		public override void Purchase(ProductDefinition product, string developerPayload)
		{
			this.m_Bindings.Purchase(product.storeSpecificId, delegate(bool success, string message)
			{
				Dictionary<string, object> dictionary = message.HashtableFromJson();
				if (success)
				{
					string @string = dictionary.GetString("GameOrderId", "");
					string string2 = dictionary.GetString("ProductId", "");
					bool flag = !string.IsNullOrEmpty(@string);
					if (flag)
					{
						dictionary["transactionId"] = @string;
					}
					bool flag2 = !string.IsNullOrEmpty(string2);
					if (flag2)
					{
						dictionary["storeSpecificId"] = string2;
					}
					bool flag3 = !product.storeSpecificId.Equals(string2);
					if (flag3)
					{
						this.m_Logger.LogFormat(LogType.Error, "UDPImpl received mismatching product Id for purchase. Excpected {0}, received {1}", new object[]
						{
							product.storeSpecificId,
							string2
						});
					}
					string receipt = dictionary.toJson();
					this.unity.OnPurchaseSucceeded(product.storeSpecificId, receipt, @string);
				}
				else
				{
					PurchaseFailureReason reason = (PurchaseFailureReason)Enum.Parse(typeof(PurchaseFailureReason), "Unknown");
					string value = reason.ToString();
					Dictionary<string, object> dictionary2 = new Dictionary<string, object>();
					dictionary2["error"] = value;
					bool flag4 = dictionary.ContainsKey("purchaseInfo");
					if (flag4)
					{
						dictionary2["purchaseInfo"] = dictionary["purchaseInfo"];
					}
					string text = dictionary2.toJson();
					PurchaseFailureDescription purchaseFailureDescription = new PurchaseFailureDescription(product.storeSpecificId, reason, message);
					this.lastPurchaseFailureDescription = purchaseFailureDescription;
					this.unity.OnPurchaseFailed(purchaseFailureDescription);
				}
			}, developerPayload);
		}

		// Token: 0x06000144 RID: 324 RVA: 0x000061F0 File Offset: 0x000043F0
		public override void FinishTransaction(ProductDefinition product, string transactionId)
		{
			bool flag = product == null || transactionId == null;
			if (!flag)
			{
				bool flag2 = product.type == ProductType.Consumable;
				if (flag2)
				{
					this.m_Bindings.FinishTransaction(product, transactionId);
				}
			}
		}

		// Token: 0x06000145 RID: 325 RVA: 0x0000622C File Offset: 0x0000442C
		public string GetLastInitializationError()
		{
			return this.m_LastInitError;
		}

		// Token: 0x06000146 RID: 326 RVA: 0x00006244 File Offset: 0x00004444
		public UserInfo GetUserInfo()
		{
			return this.m_UserInfo;
		}

		// Token: 0x06000147 RID: 327 RVA: 0x0000625C File Offset: 0x0000445C
		public void EnableDebugLog(bool enable)
		{
			StoreService.EnableDebugLogging(enable);
		}

		// Token: 0x06000148 RID: 328 RVA: 0x00006268 File Offset: 0x00004468
		private static void DictionaryToStringProperty(Dictionary<string, object> dic, object info)
		{
			PropertyInfo[] properties = info.GetType().GetProperties();
			foreach (PropertyInfo propertyInfo in properties)
			{
				bool flag = propertyInfo.PropertyType == typeof(string);
				if (flag)
				{
					propertyInfo.SetValue(info, dic.GetString(propertyInfo.Name, ""), null);
				}
			}
		}

		// Token: 0x06000149 RID: 329 RVA: 0x000062CB File Offset: 0x000044CB
		public UDPImpl()
		{
		}

		// Token: 0x0600014A RID: 330 RVA: 0x000062E4 File Offset: 0x000044E4
		[CompilerGenerated]
		private void <RetrieveProducts>b__7_0(bool success, string json)
		{
			bool flag = success && !string.IsNullOrEmpty(json);
			if (flag)
			{
				this.OnProductsRetrieved(json);
			}
			else
			{
				this.m_Logger.Log("RetrieveProducts failed: " + json);
			}
		}

		// Token: 0x040000D1 RID: 209
		private INativeUDPStore m_Bindings;

		// Token: 0x040000D2 RID: 210
		private UserInfo m_UserInfo = null;

		// Token: 0x040000D3 RID: 211
		private string m_LastInitError;

		// Token: 0x040000D4 RID: 212
		private const string k_Unknown = "Unknown";

		// Token: 0x040000D5 RID: 213
		private bool m_Initialized = false;

		// Token: 0x02000050 RID: 80
		[CompilerGenerated]
		private sealed class <>c__DisplayClass7_0
		{
			// Token: 0x0600014B RID: 331 RVA: 0x00002CA1 File Offset: 0x00000EA1
			public <>c__DisplayClass7_0()
			{
			}

			// Token: 0x0600014C RID: 332 RVA: 0x0000632C File Offset: 0x0000452C
			internal void <RetrieveProducts>b__1(bool success, string message)
			{
				this.<>4__this.m_LastInitError = "";
				this.<>4__this.m_UserInfo = null;
				if (success)
				{
					bool flag = !string.IsNullOrEmpty(message);
					if (flag)
					{
						Dictionary<string, object> dictionary = message.HashtableFromJson();
						bool flag2 = dictionary.ContainsKey("Channel");
						if (flag2)
						{
							this.<>4__this.m_UserInfo = new UserInfo();
							UDPImpl.DictionaryToStringProperty(dictionary, this.<>4__this.m_UserInfo);
						}
					}
					this.<>4__this.m_Initialized = true;
					this.<>4__this.m_Bindings.RetrieveProducts(this.products, this.retrieveCallback);
				}
				else
				{
					this.<>4__this.m_LastInitError = message;
					this.<>4__this.unity.OnSetupFailed(InitializationFailureReason.AppNotKnown);
				}
			}

			// Token: 0x040000D6 RID: 214
			public ReadOnlyCollection<ProductDefinition> products;

			// Token: 0x040000D7 RID: 215
			public Action<bool, string> retrieveCallback;

			// Token: 0x040000D8 RID: 216
			public UDPImpl <>4__this;
		}

		// Token: 0x02000051 RID: 81
		[CompilerGenerated]
		private sealed class <>c__DisplayClass8_0
		{
			// Token: 0x0600014D RID: 333 RVA: 0x00002CA1 File Offset: 0x00000EA1
			public <>c__DisplayClass8_0()
			{
			}

			// Token: 0x0600014E RID: 334 RVA: 0x000063F4 File Offset: 0x000045F4
			internal void <Purchase>b__0(bool success, string message)
			{
				Dictionary<string, object> dictionary = message.HashtableFromJson();
				if (success)
				{
					string @string = dictionary.GetString("GameOrderId", "");
					string string2 = dictionary.GetString("ProductId", "");
					bool flag = !string.IsNullOrEmpty(@string);
					if (flag)
					{
						dictionary["transactionId"] = @string;
					}
					bool flag2 = !string.IsNullOrEmpty(string2);
					if (flag2)
					{
						dictionary["storeSpecificId"] = string2;
					}
					bool flag3 = !this.product.storeSpecificId.Equals(string2);
					if (flag3)
					{
						this.<>4__this.m_Logger.LogFormat(LogType.Error, "UDPImpl received mismatching product Id for purchase. Excpected {0}, received {1}", new object[]
						{
							this.product.storeSpecificId,
							string2
						});
					}
					string receipt = dictionary.toJson();
					this.<>4__this.unity.OnPurchaseSucceeded(this.product.storeSpecificId, receipt, @string);
				}
				else
				{
					PurchaseFailureReason reason = (PurchaseFailureReason)Enum.Parse(typeof(PurchaseFailureReason), "Unknown");
					string value = reason.ToString();
					Dictionary<string, object> dictionary2 = new Dictionary<string, object>();
					dictionary2["error"] = value;
					bool flag4 = dictionary.ContainsKey("purchaseInfo");
					if (flag4)
					{
						dictionary2["purchaseInfo"] = dictionary["purchaseInfo"];
					}
					string text = dictionary2.toJson();
					PurchaseFailureDescription purchaseFailureDescription = new PurchaseFailureDescription(this.product.storeSpecificId, reason, message);
					this.<>4__this.lastPurchaseFailureDescription = purchaseFailureDescription;
					this.<>4__this.unity.OnPurchaseFailed(purchaseFailureDescription);
				}
			}

			// Token: 0x040000D9 RID: 217
			public ProductDefinition product;

			// Token: 0x040000DA RID: 218
			public UDPImpl <>4__this;
		}
	}
}
