﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x0200009E RID: 158
	public class StoreSubscriptionInfoNotSupportedException : ReceiptParserException
	{
		// Token: 0x06000314 RID: 788 RVA: 0x0000D2F7 File Offset: 0x0000B4F7
		public StoreSubscriptionInfoNotSupportedException(string message) : base(message)
		{
		}
	}
}
