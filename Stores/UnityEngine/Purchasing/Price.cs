﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x0200007A RID: 122
	[Serializable]
	public class Price : ISerializationCallbackReceiver
	{
		// Token: 0x06000223 RID: 547 RVA: 0x0000958C File Offset: 0x0000778C
		public void OnBeforeSerialize()
		{
			this.data = decimal.GetBits(this.value);
			this.num = decimal.ToDouble(this.value);
		}

		// Token: 0x06000224 RID: 548 RVA: 0x000095B4 File Offset: 0x000077B4
		public void OnAfterDeserialize()
		{
			bool flag = this.data != null && this.data.Length == 4;
			if (flag)
			{
				this.value = new decimal(this.data);
			}
		}

		// Token: 0x06000225 RID: 549 RVA: 0x00002CA1 File Offset: 0x00000EA1
		public Price()
		{
		}

		// Token: 0x0400016F RID: 367
		public decimal value;

		// Token: 0x04000170 RID: 368
		[SerializeField]
		private int[] data;

		// Token: 0x04000171 RID: 369
		[SerializeField]
		private double num;
	}
}
