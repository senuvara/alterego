﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000005 RID: 5
	public enum AndroidStore
	{
		// Token: 0x04000005 RID: 5
		GooglePlay,
		// Token: 0x04000006 RID: 6
		AmazonAppStore,
		// Token: 0x04000007 RID: 7
		CloudMoolah,
		// Token: 0x04000008 RID: 8
		SamsungApps,
		// Token: 0x04000009 RID: 9
		XiaomiMiPay,
		// Token: 0x0400000A RID: 10
		UDP,
		// Token: 0x0400000B RID: 11
		NotSpecified
	}
}
