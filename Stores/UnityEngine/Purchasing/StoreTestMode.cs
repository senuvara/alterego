﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000069 RID: 105
	public enum StoreTestMode
	{
		// Token: 0x04000131 RID: 305
		Normal,
		// Token: 0x04000132 RID: 306
		Sandbox,
		// Token: 0x04000133 RID: 307
		TestMode,
		// Token: 0x04000134 RID: 308
		ServerTest,
		// Token: 0x04000135 RID: 309
		Unknown
	}
}
