﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Security.Cryptography;
using System.Text;
using UnityEngine.Purchasing.Extension;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000023 RID: 35
	[AddComponentMenu("")]
	internal class MoolahStoreImpl : MonoBehaviour, IStore, IMoolahExtension, IStoreExtension, IMoolahConfiguration, IStoreConfiguration
	{
		// Token: 0x06000071 RID: 113 RVA: 0x00002FD8 File Offset: 0x000011D8
		public void Initialize(IStoreCallback m_callback)
		{
			Debug.Log("CloudMoolah Initialize");
			this.m_callback = m_callback;
			bool flag = string.IsNullOrEmpty(this.m_appKey);
			if (flag)
			{
				throw new Exception("IMoolahConfiguration.appkey is null!");
			}
			bool flag2 = string.IsNullOrEmpty(this.m_hashKey);
			if (flag2)
			{
				throw new Exception("IMoolahConfiguration.hashKey is null!");
			}
		}

		// Token: 0x06000072 RID: 114 RVA: 0x00003030 File Offset: 0x00001230
		public void RetrieveProducts(ReadOnlyCollection<ProductDefinition> productDefinitions)
		{
			bool flag = this.GetMode() > CloudMoolahMode.Production;
			if (flag)
			{
				List<ProductDescription> list = new List<ProductDescription>();
				foreach (ProductDefinition productDefinition in productDefinitions)
				{
					ProductMetadata metadata = new ProductMetadata("$0.01", "CloudMoolah title for " + productDefinition.storeSpecificId, "CloudMoolah description", "USD", 0.01m);
					list.Add(new ProductDescription(productDefinition.storeSpecificId, metadata));
				}
				this.RetrieveProductsSucceeded(list);
			}
			else
			{
				List<object> list2 = new List<object>();
				foreach (ProductDefinition productDefinition2 in productDefinitions)
				{
					list2.Add(new Dictionary<string, object>
					{
						{
							"pid",
							productDefinition2.storeSpecificId
						},
						{
							"productType",
							this.GetProductTypeIndex(productDefinition2.type)
						}
					});
				}
				string productInfo = MiniJson.JsonEncode(list2);
				base.StartCoroutine(this.VaildateProduct(this.m_appKey, productInfo, delegate(bool state, string result)
				{
					Debug.Log("CloudMoolah VaildateProduct state: " + state.ToString());
					this.VaildateProductProcess(state, result);
				}));
			}
		}

		// Token: 0x06000073 RID: 115 RVA: 0x00003190 File Offset: 0x00001390
		private int GetProductTypeIndex(ProductType type)
		{
			int result;
			switch (type)
			{
			case ProductType.Consumable:
				result = 1;
				break;
			case ProductType.NonConsumable:
				result = 2;
				break;
			case ProductType.Subscription:
				result = 3;
				break;
			default:
				result = 0;
				break;
			}
			return result;
		}

		// Token: 0x06000074 RID: 116 RVA: 0x000031C8 File Offset: 0x000013C8
		private void VaildateProductProcess(bool state, string result)
		{
			if (state)
			{
				Dictionary<string, object> dictionary = MiniJson.JsonDecode(result) as Dictionary<string, object>;
				string a = dictionary["code"].ToString();
				bool flag = a == "1" || a == "2";
				if (flag)
				{
					List<object> list = dictionary["values"] as List<object>;
					List<ProductDescription> list2 = new List<ProductDescription>();
					foreach (object obj in list)
					{
						Dictionary<string, object> dictionary2 = obj as Dictionary<string, object>;
						string currentString = this.GetCurrentString(dictionary2["pid"]);
						bool flag2 = string.IsNullOrEmpty(currentString);
						if (!flag2)
						{
							string currentString2 = this.GetCurrentString(dictionary2["priceString"]);
							bool flag3 = string.IsNullOrEmpty(currentString2);
							if (!flag3)
							{
								string text = Convert.ToDouble(currentString2).ToString("f2");
								string currentString3 = this.GetCurrentString(dictionary2["title"]);
								string currentString4 = this.GetCurrentString(dictionary2["description"]);
								string currentString5 = this.GetCurrentString(dictionary2["currencyCode"]);
								string value = "0.00";
								bool flag4 = dictionary2["localizedPrice"] == null;
								if (flag4)
								{
									value = text;
								}
								decimal localizedPrice = Convert.ToDecimal(value);
								ProductMetadata metadata = new ProductMetadata(text, currentString3, currentString4, currentString5, localizedPrice);
								list2.Add(new ProductDescription(currentString, metadata));
							}
						}
					}
					this.RetrieveProductsSucceeded(list2);
					Debug.Log("CloudMoolah ProductList.length: " + list2.Count.ToString());
				}
				else
				{
					this.RetrieveProductsFailed(InitializationFailureReason.NoProductsAvailable);
				}
			}
			else
			{
				this.RetrieveProductsFailed(InitializationFailureReason.PurchasingUnavailable);
			}
		}

		// Token: 0x06000075 RID: 117 RVA: 0x000033C4 File Offset: 0x000015C4
		private string GetCurrentString(object obj)
		{
			bool flag = obj == null;
			string result;
			if (flag)
			{
				result = null;
			}
			else
			{
				result = obj.ToString();
			}
			return result;
		}

		// Token: 0x06000076 RID: 118 RVA: 0x000033E9 File Offset: 0x000015E9
		private IEnumerator VaildateProduct(string appkey, string productInfo, Action<bool, string> result)
		{
			string sign = this.GetStringMD5(appkey + this.m_hashKey);
			WWWForm wf = new WWWForm();
			wf.AddField("appId", appkey);
			wf.AddField("productInfo", productInfo);
			wf.AddField("sign", sign);
			WWW w = new WWW(MoolahStoreImpl.requestProductValidateUrl, wf);
			yield return w;
			bool flag = !string.IsNullOrEmpty(w.error);
			if (flag)
			{
				Debug.Log("CloudMoolah ValidateProduct w.error: " + w.error);
				result(false, w.error);
			}
			else
			{
				result(true, w.text);
				Debug.Log("CloudMoolah ValidateProduct w.text: " + w.text);
			}
			yield break;
		}

		// Token: 0x06000077 RID: 119 RVA: 0x0000340D File Offset: 0x0000160D
		private void RetrieveProductsSucceeded(List<ProductDescription> products)
		{
			this.m_callback.OnProductsRetrieved(products);
		}

		// Token: 0x06000078 RID: 120 RVA: 0x0000341D File Offset: 0x0000161D
		private void RetrieveProductsFailed(InitializationFailureReason reason)
		{
			this.m_callback.OnSetupFailed(reason);
		}

		// Token: 0x06000079 RID: 121 RVA: 0x00003430 File Offset: 0x00001630
		public void ClosePayWebView(string result)
		{
			Debug.Log("CloudMoolah ClosePayWebView");
			bool flag = this.isNeedPolling;
			if (flag)
			{
				this.isNeedPolling = false;
				this.PurchaseFailed(this.m_CurrentStoreProductID, PurchaseFailureReason.UserCancelled, "UserCancelled");
			}
		}

		// Token: 0x0600007A RID: 122 RVA: 0x00003470 File Offset: 0x00001670
		private void PurchaseRusult(string resultJson)
		{
			this.isNeedPolling = false;
			Dictionary<string, object> dictionary = MiniJson.JsonDecode(resultJson) as Dictionary<string, object>;
			Debug.Log("CloudMoolah PurchaseResult resultJson: " + resultJson);
			string a = dictionary["code"].ToString();
			bool flag = a == "1";
			if (flag)
			{
				Dictionary<string, object> dictionary2 = dictionary["values"] as Dictionary<string, object>;
				string a2 = dictionary2["state"].ToString();
				string transactionId = dictionary2["tradeSeq"].ToString();
				string storeSpecificId = dictionary2["productId"].ToString();
				string msg = dictionary["msg"].ToString();
				bool flag2 = a2 == TradeSeqState.PAY_CONFIRM.ToString() || a2 == TradeSeqState.ORDER_SUCCEED.ToString();
				if (flag2)
				{
					string receipt = dictionary2["receipt"].ToString();
					this.PurchaseSucceed(storeSpecificId, receipt, transactionId);
				}
				else
				{
					bool flag3 = a2 == TradeSeqState.PAY_FAILED.ToString();
					if (flag3)
					{
						this.PurchaseFailed(storeSpecificId, (PurchaseFailureReason)Enum.Parse(typeof(PurchaseFailureReason), "Unknown"), msg);
					}
				}
			}
		}

		// Token: 0x0600007B RID: 123 RVA: 0x000035C0 File Offset: 0x000017C0
		public void Purchase(ProductDefinition product, string developerPayload)
		{
			Debug.Log("CloudMoolah Purchase: " + product.storeSpecificId);
			bool flag = this.GetMode() == CloudMoolahMode.AlwaysSucceed;
			if (flag)
			{
				this.PurchaseSucceed(product.storeSpecificId, "CloudMoolah TestMode receipt", Guid.NewGuid().ToString());
			}
			else
			{
				bool flag2 = this.GetMode() == CloudMoolahMode.AlwaysFailed;
				if (flag2)
				{
					this.PurchaseFailed(product.storeSpecificId, PurchaseFailureReason.UserCancelled, "TestMode UserCancelled");
				}
				else
				{
					bool flag3 = this.isNeedPolling;
					if (flag3)
					{
						throw new Exception("CloudMoolah Aborting this purchase. Pending purchase detected.");
					}
					Action<string, string, string> purchaseSucceed = delegate(string productid, string receipt, string transactionId)
					{
						this.PurchaseSucceed(productid, receipt, transactionId);
					};
					Action<string, PurchaseFailureReason, string> purchaseFailed = delegate(string storeSpecificId, PurchaseFailureReason failureReason, string msg)
					{
						bool flag4 = failureReason == (PurchaseFailureReason)Enum.Parse(typeof(PurchaseFailureReason), "Unknown");
						if (flag4)
						{
							failureReason = PurchaseFailureReason.UserCancelled;
						}
						this.PurchaseFailed(storeSpecificId, failureReason, msg);
					};
					Action<string, string, string> succeed = delegate(string transactionId, string authGlobal, string paymentUrl)
					{
						bool flag4 = string.IsNullOrEmpty(paymentUrl);
						if (flag4)
						{
							throw new Exception("authGlobal is null!");
						}
						bool flag5 = string.IsNullOrEmpty(authGlobal);
						if (flag5)
						{
							throw new Exception("authGlobal is null! ");
						}
						bool flag6 = string.IsNullOrEmpty(transactionId);
						if (flag6)
						{
							throw new Exception("transactionId is null! ");
						}
						this.isNeedPolling = true;
						string text = this.m_CustomerID;
						bool flag7 = string.IsNullOrEmpty(text);
						if (flag7)
						{
							text = this.DeviceUniqueIdentifier();
						}
						bool flag8 = Application.platform == RuntimePlatform.Android;
						if (flag8)
						{
							PayMethod.showPayWebView(paymentUrl, authGlobal, transactionId, this.m_hashKey, text);
						}
						else
						{
							Application.OpenURL(paymentUrl);
							this.StartCoroutine(this.StartPurchasePolling(authGlobal, transactionId, purchaseSucceed, purchaseFailed));
						}
					};
					Action<string, string> failed = delegate(string productID, string msg)
					{
						this.PurchaseFailed(productID, PurchaseFailureReason.PaymentDeclined, "request MoolahStoreAuthCode failed !");
					};
					this.m_CurrentStoreProductID = product.storeSpecificId;
					this.RequestAuthCode(product.storeSpecificId, developerPayload, succeed, failed);
				}
			}
		}

		// Token: 0x0600007C RID: 124 RVA: 0x000036C4 File Offset: 0x000018C4
		private string DeviceUniqueIdentifier()
		{
			string deviceID = PayMethod.getDeviceID();
			Debug.Log("CloudMoolah getDeviceID: " + deviceID);
			return deviceID;
		}

		// Token: 0x0600007D RID: 125 RVA: 0x000036F0 File Offset: 0x000018F0
		private void RequestAuthCode(string productID, string payload, Action<string, string, string> succeed, Action<string, string> failed)
		{
			bool flag = this.isRequestAuthCodeing;
			if (flag)
			{
				failed(productID, "RequestAuthCode repeat");
				throw new Exception("RequestAuthCode repeat");
			}
			string text = this.m_CustomerID;
			bool flag2 = string.IsNullOrEmpty(text);
			if (flag2)
			{
				text = this.DeviceUniqueIdentifier();
			}
			bool flag3 = string.IsNullOrEmpty(text);
			if (flag3)
			{
				failed(productID, "customerId is null");
				throw new Exception("customerId or m_UniqueID is null!");
			}
			string text2 = "1";
			string text3 = Guid.NewGuid().ToString();
			string md5String = string.Concat(new string[]
			{
				this.m_appKey,
				text,
				productID,
				text3,
				text2,
				this.m_hashKey
			});
			string stringMD = this.GetStringMD5(md5String);
			string str = string.Concat(new string[]
			{
				"?APPId=",
				this.m_appKey,
				"&customerId=",
				text,
				"&productId=",
				productID,
				"&tradeSeq=",
				text3,
				"&tradeType=",
				text2,
				"&payLoad=",
				payload,
				"&sign=",
				stringMD
			});
			WWWForm wwwform = new WWWForm();
			wwwform.AddField("APPId", this.m_appKey);
			wwwform.AddField("customerId", text);
			wwwform.AddField("productId", productID);
			wwwform.AddField("tradeSeq", text3);
			wwwform.AddField("tradeType", text2);
			wwwform.AddField("payload", payload);
			bool flag4 = this.m_notificationURL != null && this.m_notificationURL != "";
			if (flag4)
			{
				wwwform.AddField("notificationURL", this.m_notificationURL);
				Debug.Log("CloudMoolah notificationURL: " + this.notificationURL);
			}
			wwwform.AddField("sign", stringMD);
			this.isRequestAuthCodeing = true;
			Debug.Log("CloudMoolah: " + MoolahStoreImpl.requestAuthCodePath + str);
			base.StartCoroutine(this.RequestAuthCode(wwwform, productID, text3, succeed, failed));
		}

		// Token: 0x0600007E RID: 126 RVA: 0x00003912 File Offset: 0x00001B12
		private IEnumerator RequestAuthCode(WWWForm wf, string productID, string transactionId, Action<string, string, string> succeed, Action<string, string> failed)
		{
			WWW w = new WWW(MoolahStoreImpl.requestAuthCodePath, wf);
			yield return w;
			this.isRequestAuthCodeing = false;
			bool flag = !string.IsNullOrEmpty(w.error);
			if (flag)
			{
				Debug.Log("CloudMoolah RequestAuthCode error: " + w.error);
				failed(productID, w.error);
			}
			else
			{
				Debug.Log("CloudMoolah RequestAuthCode w.text: " + w.text);
				Dictionary<string, object> authCodeResult = MiniJson.JsonDecode(w.text) as Dictionary<string, object>;
				bool flag2 = authCodeResult["code"].ToString() != "1";
				if (flag2)
				{
					failed(productID, this.GetCurrentString(authCodeResult["msg"]));
				}
				else
				{
					Dictionary<string, object> authCodeValues = authCodeResult["values"] as Dictionary<string, object>;
					string authCode = authCodeValues["authCode"].ToString();
					string paymentURL = authCodeValues["requestUrl"].ToString();
					succeed(transactionId, authCode, paymentURL);
					authCodeValues = null;
					authCode = null;
					paymentURL = null;
				}
				authCodeResult = null;
			}
			yield break;
		}

		// Token: 0x0600007F RID: 127 RVA: 0x00003946 File Offset: 0x00001B46
		private IEnumerator StartPurchasePolling(string authGlobal, string transactionId, Action<string, string, string> purchaseSucceed, Action<string, PurchaseFailureReason, string> purchaseFailed)
		{
			yield return new WaitForSeconds(6f);
			bool flag = !this.isNeedPolling;
			if (flag)
			{
				yield break;
			}
			string orderSuccess = "0";
			string signstr = authGlobal + orderSuccess + transactionId + this.m_hashKey;
			string sign = this.GetStringMD5(signstr);
			string param = string.Concat(new string[]
			{
				"?authCode=",
				authGlobal,
				"&orderSuccess=",
				orderSuccess,
				"&tradeSeq=",
				transactionId,
				"&sign=",
				sign
			});
			string url = MoolahStoreImpl.pollingPath + ((param == null) ? "" : param);
			WWW pollingstr = new WWW(url);
			yield return pollingstr;
			bool flag2 = !string.IsNullOrEmpty(pollingstr.error);
			if (flag2)
			{
				Debug.Log("CloudMoolah StartPurchasePolling PC error: " + pollingstr.error);
			}
			else
			{
				Dictionary<string, object> jsonPollingObjects = MiniJson.JsonDecode(pollingstr.text) as Dictionary<string, object>;
				Debug.Log("CloudMoolah StartPurchasePolling PC resultJson: " + pollingstr.text);
				string code = jsonPollingObjects["code"].ToString();
				bool flag3 = code == "1";
				if (flag3)
				{
					Dictionary<string, object> pollingValues = jsonPollingObjects["values"] as Dictionary<string, object>;
					string tradeSeq = pollingValues["tradeSeq"].ToString();
					string tradeState = pollingValues["state"].ToString();
					string productId = pollingValues["productId"].ToString();
					string Msg = jsonPollingObjects["msg"].ToString();
					bool flag4 = tradeState == TradeSeqState.PAY_CONFIRM.ToString() || tradeState == TradeSeqState.ORDER_SUCCEED.ToString();
					if (flag4)
					{
						this.isNeedPolling = false;
						string receipt = pollingValues["receipt"].ToString();
						purchaseSucceed(productId, receipt, tradeSeq);
						yield break;
					}
					bool flag5 = tradeState == TradeSeqState.PAY_FAILED.ToString();
					if (flag5)
					{
						this.isNeedPolling = false;
						purchaseFailed(productId, PurchaseFailureReason.UserCancelled, Msg);
						yield break;
					}
					pollingValues = null;
					tradeSeq = null;
					tradeState = null;
					productId = null;
					Msg = null;
				}
				jsonPollingObjects = null;
				code = null;
			}
			base.StartCoroutine(this.StartPurchasePolling(authGlobal, transactionId, purchaseSucceed, purchaseFailed));
			yield break;
		}

		// Token: 0x06000080 RID: 128 RVA: 0x00003972 File Offset: 0x00001B72
		public void PurchaseSucceed(string storeSpecificId, string receipt, string transactionId)
		{
			Debug.Log("CloudMoolah PurchaseSucceed");
			this.m_callback.OnPurchaseSucceeded(storeSpecificId, receipt, transactionId);
		}

		// Token: 0x06000081 RID: 129 RVA: 0x00003990 File Offset: 0x00001B90
		public void PurchaseFailed(string storeSpecificId, PurchaseFailureReason reason, string msg)
		{
			PurchaseFailureDescription desc = new PurchaseFailureDescription(storeSpecificId, reason, msg);
			this.m_callback.OnPurchaseFailed(desc);
		}

		// Token: 0x06000082 RID: 130 RVA: 0x000039B4 File Offset: 0x00001BB4
		public void FinishTransaction(ProductDefinition product, string transactionId)
		{
			Debug.Log("CloudMoolah FinishTransaction");
		}

		// Token: 0x06000083 RID: 131 RVA: 0x000039C4 File Offset: 0x00001BC4
		private string GetStringMD5(string md5String)
		{
			byte[] bytes = Encoding.UTF8.GetBytes(md5String);
			MD5CryptoServiceProvider md5CryptoServiceProvider = new MD5CryptoServiceProvider();
			byte[] array = md5CryptoServiceProvider.ComputeHash(bytes);
			string text = "";
			for (int i = 0; i < array.Length; i++)
			{
				text += Convert.ToString(array[i], 16).PadLeft(2, '0');
			}
			return text.PadLeft(32, '0');
		}

		// Token: 0x17000020 RID: 32
		// (get) Token: 0x06000084 RID: 132 RVA: 0x00003A38 File Offset: 0x00001C38
		// (set) Token: 0x06000085 RID: 133 RVA: 0x00003A50 File Offset: 0x00001C50
		public string appKey
		{
			get
			{
				return this.m_appKey;
			}
			set
			{
				this.m_appKey = value;
			}
		}

		// Token: 0x17000021 RID: 33
		// (get) Token: 0x06000086 RID: 134 RVA: 0x00003A5C File Offset: 0x00001C5C
		// (set) Token: 0x06000087 RID: 135 RVA: 0x00003A74 File Offset: 0x00001C74
		public string hashKey
		{
			get
			{
				return this.m_hashKey;
			}
			set
			{
				this.m_hashKey = value;
			}
		}

		// Token: 0x17000022 RID: 34
		// (get) Token: 0x06000088 RID: 136 RVA: 0x00003A80 File Offset: 0x00001C80
		// (set) Token: 0x06000089 RID: 137 RVA: 0x00003A98 File Offset: 0x00001C98
		public string notificationURL
		{
			get
			{
				return this.m_notificationURL;
			}
			set
			{
				this.m_notificationURL = value;
			}
		}

		// Token: 0x0600008A RID: 138 RVA: 0x00003AA2 File Offset: 0x00001CA2
		public void SetMode(CloudMoolahMode mode)
		{
			this.m_mode = mode;
		}

		// Token: 0x0600008B RID: 139 RVA: 0x00003AAC File Offset: 0x00001CAC
		private CloudMoolahMode GetMode()
		{
			return this.m_mode;
		}

		// Token: 0x0600008C RID: 140 RVA: 0x00003AC4 File Offset: 0x00001CC4
		public void RestoreTransactionID(Action<RestoreTransactionIDState> result)
		{
			bool flag = this.GetMode() == CloudMoolahMode.AlwaysSucceed;
			if (flag)
			{
				result(RestoreTransactionIDState.RestoreSucceed);
			}
			else
			{
				bool flag2 = this.GetMode() == CloudMoolahMode.AlwaysFailed;
				if (flag2)
				{
					result(RestoreTransactionIDState.RestoreFailed);
				}
				else
				{
					base.StartCoroutine(this.RestoreTransactionIDProcess(result));
				}
			}
		}

		// Token: 0x0600008D RID: 141 RVA: 0x00003B10 File Offset: 0x00001D10
		private IEnumerator RestoreTransactionIDProcess(Action<RestoreTransactionIDState> result)
		{
			string customID = this.m_CustomerID;
			bool flag = string.IsNullOrEmpty(customID);
			if (flag)
			{
				customID = this.DeviceUniqueIdentifier();
			}
			WWWForm wf = new WWWForm();
			wf.AddField("appId", this.m_appKey);
			wf.AddField("customerId", customID);
			DateTime now = DateTime.Now;
			string endDate = now.ToString("yyyy/MM/dd");
			string startDate = now.AddDays(-7.0).ToString("yyyy/MM/dd");
			wf.AddField("startDate", startDate);
			wf.AddField("endDate", endDate);
			string sign = this.GetStringMD5(this.m_appKey + customID + this.m_hashKey);
			wf.AddField("Sign", sign);
			WWW w = new WWW(MoolahStoreImpl.requestRestoreTransactionUrl, wf);
			yield return w;
			bool flag2 = !string.IsNullOrEmpty(w.error);
			if (flag2)
			{
				Debug.LogError("CloudMoolah RestoreTransactionID error: " + w.error);
				result(RestoreTransactionIDState.NotKnown);
			}
			else
			{
				Debug.LogError("CloudMoolah RestoreTransactionIDProcess text: " + w.text);
				Dictionary<string, object> restoreObjects = MiniJson.JsonDecode(w.text) as Dictionary<string, object>;
				string code = restoreObjects["code"].ToString();
				bool flag3 = code == "1";
				if (flag3)
				{
					List<object> restoreValues = restoreObjects["values"] as List<object>;
					foreach (object obj in restoreValues)
					{
						Dictionary<string, object> restoreObjectElem = (Dictionary<string, object>)obj;
						string productId = restoreObjectElem["productId"].ToString();
						string tradeSeq = restoreObjectElem["tradeSeq"].ToString();
						string receipt = restoreObjectElem["receipt"].ToString();
						this.PurchaseSucceed(productId, receipt, tradeSeq);
						productId = null;
						tradeSeq = null;
						receipt = null;
						restoreObjectElem = null;
					}
					List<object>.Enumerator enumerator = default(List<object>.Enumerator);
					result(RestoreTransactionIDState.RestoreSucceed);
					restoreValues = null;
				}
				else
				{
					bool flag4 = code == "CMB0000147";
					if (flag4)
					{
						result(RestoreTransactionIDState.NoTransactionRestore);
					}
					else
					{
						result(RestoreTransactionIDState.RestoreFailed);
					}
				}
				restoreObjects = null;
				code = null;
			}
			yield break;
		}

		// Token: 0x0600008E RID: 142 RVA: 0x00003B28 File Offset: 0x00001D28
		public void ValidateReceipt(string transactionId, string receipt, Action<string, ValidateReceiptState, string> result)
		{
			bool flag = string.IsNullOrEmpty(transactionId) || string.IsNullOrEmpty(receipt);
			if (flag)
			{
				result(transactionId, ValidateReceiptState.ValidateFailed, "transactionId or receipt is null");
			}
			else
			{
				bool flag2 = this.GetMode() == CloudMoolahMode.AlwaysSucceed;
				if (flag2)
				{
					result(transactionId, ValidateReceiptState.ValidateSucceed, "TestMode ValidateSucceed");
				}
				else
				{
					bool flag3 = this.GetMode() == CloudMoolahMode.AlwaysFailed;
					if (flag3)
					{
						result(transactionId, ValidateReceiptState.ValidateFailed, "TestMode ValidateFailed");
					}
					else
					{
						base.StartCoroutine(this.ValidateReceiptProcess(transactionId, receipt, result));
					}
				}
			}
		}

		// Token: 0x0600008F RID: 143 RVA: 0x00003BA8 File Offset: 0x00001DA8
		private IEnumerator ValidateReceiptProcess(string transactionId, string receipt, Action<string, ValidateReceiptState, string> result)
		{
			Dictionary<string, object> tempJson = MiniJson.JsonDecode(receipt) as Dictionary<string, object>;
			bool flag = tempJson != null;
			if (flag)
			{
				bool flag2 = tempJson["Payload"] != null;
				if (flag2)
				{
					receipt = tempJson["Payload"].ToString();
				}
			}
			WWWForm wf = new WWWForm();
			wf.AddField("appId", this.m_appKey);
			wf.AddField("receipt", receipt);
			string sign = this.GetStringMD5(this.m_appKey + receipt + this.m_hashKey);
			wf.AddField("sign", sign);
			WWW w = new WWW(MoolahStoreImpl.requestValidateReceiptUrl, wf);
			yield return w;
			bool flag3 = !string.IsNullOrEmpty(w.error);
			if (flag3)
			{
				Debug.LogError("CloudMoolah ValidateReceipt error: " + w.error);
				result(transactionId, ValidateReceiptState.NotKnown, "ValidateReceiptState NotKnown");
			}
			else
			{
				Debug.LogError("CloudMoolah validateReceiptProcess text: " + w.text);
				Dictionary<string, object> jsonObjects = MiniJson.JsonDecode(w.text) as Dictionary<string, object>;
				string code = jsonObjects["code"].ToString();
				string msg = jsonObjects["msg"].ToString();
				bool flag4 = code == "1";
				if (flag4)
				{
					result(transactionId, ValidateReceiptState.ValidateSucceed, "ValidateReceiptState ValidateSucceeded");
				}
				else
				{
					result(transactionId, ValidateReceiptState.ValidateFailed, msg);
				}
				jsonObjects = null;
				code = null;
				msg = null;
			}
			yield break;
		}

		// Token: 0x06000090 RID: 144 RVA: 0x00003BCC File Offset: 0x00001DCC
		public MoolahStoreImpl()
		{
		}

		// Token: 0x06000091 RID: 145 RVA: 0x00003BF5 File Offset: 0x00001DF5
		// Note: this type is marked as 'beforefieldinit'.
		static MoolahStoreImpl()
		{
		}

		// Token: 0x06000092 RID: 146 RVA: 0x00003C29 File Offset: 0x00001E29
		[CompilerGenerated]
		private void <RetrieveProducts>b__9_0(bool state, string result)
		{
			Debug.Log("CloudMoolah VaildateProduct state: " + state.ToString());
			this.VaildateProductProcess(state, result);
		}

		// Token: 0x06000093 RID: 147 RVA: 0x00003C4C File Offset: 0x00001E4C
		[CompilerGenerated]
		private void <Purchase>b__18_0(string productid, string receipt, string transactionId)
		{
			this.PurchaseSucceed(productid, receipt, transactionId);
		}

		// Token: 0x06000094 RID: 148 RVA: 0x00003C5C File Offset: 0x00001E5C
		[CompilerGenerated]
		private void <Purchase>b__18_1(string storeSpecificId, PurchaseFailureReason failureReason, string msg)
		{
			bool flag = failureReason == (PurchaseFailureReason)Enum.Parse(typeof(PurchaseFailureReason), "Unknown");
			if (flag)
			{
				failureReason = PurchaseFailureReason.UserCancelled;
			}
			this.PurchaseFailed(storeSpecificId, failureReason, msg);
		}

		// Token: 0x06000095 RID: 149 RVA: 0x00003C97 File Offset: 0x00001E97
		[CompilerGenerated]
		private void <Purchase>b__18_3(string productID, string msg)
		{
			this.PurchaseFailed(productID, PurchaseFailureReason.PaymentDeclined, "request MoolahStoreAuthCode failed !");
		}

		// Token: 0x04000048 RID: 72
		private static readonly string pollingPath = "https://api.cloudmoolah.com/CMPayment/api/polling.ashx";

		// Token: 0x04000049 RID: 73
		private static readonly string requestAuthCodePath = "https://api.cloudmoolah.com/CMPayment/api/authGlobal.ashx";

		// Token: 0x0400004A RID: 74
		private static readonly string requestRestoreTransactionUrl = "https://api.cloudmoolah.com/CMPayment/receipt/recover.ashx";

		// Token: 0x0400004B RID: 75
		private static readonly string requestValidateReceiptUrl = "https://api.cloudmoolah.com/CMPayment/receipt/validate.ashx";

		// Token: 0x0400004C RID: 76
		private static readonly string requestProductValidateUrl = "https://api.cloudmoolah.com/CMPayment/product/validate.ashx";

		// Token: 0x0400004D RID: 77
		private IStoreCallback m_callback;

		// Token: 0x0400004E RID: 78
		private bool isNeedPolling = false;

		// Token: 0x0400004F RID: 79
		private string m_CurrentStoreProductID;

		// Token: 0x04000050 RID: 80
		private bool isRequestAuthCodeing = false;

		// Token: 0x04000051 RID: 81
		private string m_appKey;

		// Token: 0x04000052 RID: 82
		private string m_hashKey;

		// Token: 0x04000053 RID: 83
		private string m_notificationURL;

		// Token: 0x04000054 RID: 84
		private CloudMoolahMode m_mode = CloudMoolahMode.Production;

		// Token: 0x04000055 RID: 85
		private string m_CustomerID = "";

		// Token: 0x02000024 RID: 36
		[CompilerGenerated]
		private sealed class <VaildateProduct>d__13 : IEnumerator<object>, IEnumerator, IDisposable
		{
			// Token: 0x06000096 RID: 150 RVA: 0x00003CA8 File Offset: 0x00001EA8
			[DebuggerHidden]
			public <VaildateProduct>d__13(int <>1__state)
			{
				this.<>1__state = <>1__state;
			}

			// Token: 0x06000097 RID: 151 RVA: 0x00003CB8 File Offset: 0x00001EB8
			[DebuggerHidden]
			void IDisposable.Dispose()
			{
			}

			// Token: 0x06000098 RID: 152 RVA: 0x00003CBC File Offset: 0x00001EBC
			bool IEnumerator.MoveNext()
			{
				int num = this.<>1__state;
				if (num == 0)
				{
					this.<>1__state = -1;
					sign = base.GetStringMD5(appkey + this.m_hashKey);
					wf = new WWWForm();
					wf.AddField("appId", appkey);
					wf.AddField("productInfo", productInfo);
					wf.AddField("sign", sign);
					w = new WWW(MoolahStoreImpl.requestProductValidateUrl, wf);
					this.<>2__current = w;
					this.<>1__state = 1;
					return true;
				}
				if (num != 1)
				{
					return false;
				}
				this.<>1__state = -1;
				bool flag = !string.IsNullOrEmpty(w.error);
				if (flag)
				{
					Debug.Log("CloudMoolah ValidateProduct w.error: " + w.error);
					result(false, w.error);
				}
				else
				{
					result(true, w.text);
					Debug.Log("CloudMoolah ValidateProduct w.text: " + w.text);
				}
				return false;
			}

			// Token: 0x17000023 RID: 35
			// (get) Token: 0x06000099 RID: 153 RVA: 0x00003E19 File Offset: 0x00002019
			object IEnumerator<object>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x0600009A RID: 154 RVA: 0x00003E21 File Offset: 0x00002021
			[DebuggerHidden]
			void IEnumerator.Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x17000024 RID: 36
			// (get) Token: 0x0600009B RID: 155 RVA: 0x00003E19 File Offset: 0x00002019
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x04000056 RID: 86
			private int <>1__state;

			// Token: 0x04000057 RID: 87
			private object <>2__current;

			// Token: 0x04000058 RID: 88
			public string appkey;

			// Token: 0x04000059 RID: 89
			public string productInfo;

			// Token: 0x0400005A RID: 90
			public Action<bool, string> result;

			// Token: 0x0400005B RID: 91
			public MoolahStoreImpl <>4__this;

			// Token: 0x0400005C RID: 92
			private string <sign>5__1;

			// Token: 0x0400005D RID: 93
			private WWWForm <wf>5__2;

			// Token: 0x0400005E RID: 94
			private WWW <w>5__3;
		}

		// Token: 0x02000025 RID: 37
		[CompilerGenerated]
		private sealed class <>c__DisplayClass18_0
		{
			// Token: 0x0600009C RID: 156 RVA: 0x00002CA1 File Offset: 0x00000EA1
			public <>c__DisplayClass18_0()
			{
			}

			// Token: 0x0600009D RID: 157 RVA: 0x00003E28 File Offset: 0x00002028
			internal void <Purchase>b__2(string transactionId, string authGlobal, string paymentUrl)
			{
				bool flag = string.IsNullOrEmpty(paymentUrl);
				if (flag)
				{
					throw new Exception("authGlobal is null!");
				}
				bool flag2 = string.IsNullOrEmpty(authGlobal);
				if (flag2)
				{
					throw new Exception("authGlobal is null! ");
				}
				bool flag3 = string.IsNullOrEmpty(transactionId);
				if (flag3)
				{
					throw new Exception("transactionId is null! ");
				}
				this.<>4__this.isNeedPolling = true;
				string text = this.<>4__this.m_CustomerID;
				bool flag4 = string.IsNullOrEmpty(text);
				if (flag4)
				{
					text = this.<>4__this.DeviceUniqueIdentifier();
				}
				bool flag5 = Application.platform == RuntimePlatform.Android;
				if (flag5)
				{
					PayMethod.showPayWebView(paymentUrl, authGlobal, transactionId, this.<>4__this.m_hashKey, text);
				}
				else
				{
					Application.OpenURL(paymentUrl);
					this.<>4__this.StartCoroutine(this.<>4__this.StartPurchasePolling(authGlobal, transactionId, this.purchaseSucceed, this.purchaseFailed));
				}
			}

			// Token: 0x0400005F RID: 95
			public Action<string, string, string> purchaseSucceed;

			// Token: 0x04000060 RID: 96
			public Action<string, PurchaseFailureReason, string> purchaseFailed;

			// Token: 0x04000061 RID: 97
			public MoolahStoreImpl <>4__this;
		}

		// Token: 0x02000026 RID: 38
		[CompilerGenerated]
		private sealed class <RequestAuthCode>d__22 : IEnumerator<object>, IEnumerator, IDisposable
		{
			// Token: 0x0600009E RID: 158 RVA: 0x00003F00 File Offset: 0x00002100
			[DebuggerHidden]
			public <RequestAuthCode>d__22(int <>1__state)
			{
				this.<>1__state = <>1__state;
			}

			// Token: 0x0600009F RID: 159 RVA: 0x00003CB8 File Offset: 0x00001EB8
			[DebuggerHidden]
			void IDisposable.Dispose()
			{
			}

			// Token: 0x060000A0 RID: 160 RVA: 0x00003F10 File Offset: 0x00002110
			bool IEnumerator.MoveNext()
			{
				int num = this.<>1__state;
				if (num == 0)
				{
					this.<>1__state = -1;
					w = new WWW(MoolahStoreImpl.requestAuthCodePath, wf);
					this.<>2__current = w;
					this.<>1__state = 1;
					return true;
				}
				if (num != 1)
				{
					return false;
				}
				this.<>1__state = -1;
				this.isRequestAuthCodeing = false;
				bool flag = !string.IsNullOrEmpty(w.error);
				if (flag)
				{
					Debug.Log("CloudMoolah RequestAuthCode error: " + w.error);
					failed(productID, w.error);
				}
				else
				{
					Debug.Log("CloudMoolah RequestAuthCode w.text: " + w.text);
					authCodeResult = (MiniJson.JsonDecode(w.text) as Dictionary<string, object>);
					bool flag2 = authCodeResult["code"].ToString() != "1";
					if (flag2)
					{
						failed(productID, base.GetCurrentString(authCodeResult["msg"]));
					}
					else
					{
						authCodeValues = (authCodeResult["values"] as Dictionary<string, object>);
						authCode = authCodeValues["authCode"].ToString();
						paymentURL = authCodeValues["requestUrl"].ToString();
						succeed(transactionId, authCode, paymentURL);
						authCodeValues = null;
						authCode = null;
						paymentURL = null;
					}
					authCodeResult = null;
				}
				return false;
			}

			// Token: 0x17000025 RID: 37
			// (get) Token: 0x060000A1 RID: 161 RVA: 0x000040EE File Offset: 0x000022EE
			object IEnumerator<object>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x060000A2 RID: 162 RVA: 0x00003E21 File Offset: 0x00002021
			[DebuggerHidden]
			void IEnumerator.Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x17000026 RID: 38
			// (get) Token: 0x060000A3 RID: 163 RVA: 0x000040EE File Offset: 0x000022EE
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x04000062 RID: 98
			private int <>1__state;

			// Token: 0x04000063 RID: 99
			private object <>2__current;

			// Token: 0x04000064 RID: 100
			public WWWForm wf;

			// Token: 0x04000065 RID: 101
			public string productID;

			// Token: 0x04000066 RID: 102
			public string transactionId;

			// Token: 0x04000067 RID: 103
			public Action<string, string, string> succeed;

			// Token: 0x04000068 RID: 104
			public Action<string, string> failed;

			// Token: 0x04000069 RID: 105
			public MoolahStoreImpl <>4__this;

			// Token: 0x0400006A RID: 106
			private WWW <w>5__1;

			// Token: 0x0400006B RID: 107
			private Dictionary<string, object> <authCodeResult>5__2;

			// Token: 0x0400006C RID: 108
			private Dictionary<string, object> <authCodeValues>5__3;

			// Token: 0x0400006D RID: 109
			private string <authCode>5__4;

			// Token: 0x0400006E RID: 110
			private string <paymentURL>5__5;
		}

		// Token: 0x02000027 RID: 39
		[CompilerGenerated]
		private sealed class <StartPurchasePolling>d__23 : IEnumerator<object>, IEnumerator, IDisposable
		{
			// Token: 0x060000A4 RID: 164 RVA: 0x000040F6 File Offset: 0x000022F6
			[DebuggerHidden]
			public <StartPurchasePolling>d__23(int <>1__state)
			{
				this.<>1__state = <>1__state;
			}

			// Token: 0x060000A5 RID: 165 RVA: 0x00003CB8 File Offset: 0x00001EB8
			[DebuggerHidden]
			void IDisposable.Dispose()
			{
			}

			// Token: 0x060000A6 RID: 166 RVA: 0x00004108 File Offset: 0x00002308
			bool IEnumerator.MoveNext()
			{
				switch (this.<>1__state)
				{
				case 0:
					this.<>1__state = -1;
					this.<>2__current = new WaitForSeconds(6f);
					this.<>1__state = 1;
					return true;
				case 1:
				{
					this.<>1__state = -1;
					bool flag = !this.isNeedPolling;
					if (flag)
					{
						return false;
					}
					orderSuccess = "0";
					signstr = authGlobal + orderSuccess + transactionId + this.m_hashKey;
					sign = base.GetStringMD5(signstr);
					param = string.Concat(new string[]
					{
						"?authCode=",
						authGlobal,
						"&orderSuccess=",
						orderSuccess,
						"&tradeSeq=",
						transactionId,
						"&sign=",
						sign
					});
					url = MoolahStoreImpl.pollingPath + ((param == null) ? "" : param);
					pollingstr = new WWW(url);
					this.<>2__current = pollingstr;
					this.<>1__state = 2;
					return true;
				}
				case 2:
				{
					this.<>1__state = -1;
					bool flag2 = !string.IsNullOrEmpty(pollingstr.error);
					if (flag2)
					{
						Debug.Log("CloudMoolah StartPurchasePolling PC error: " + pollingstr.error);
					}
					else
					{
						jsonPollingObjects = (MiniJson.JsonDecode(pollingstr.text) as Dictionary<string, object>);
						Debug.Log("CloudMoolah StartPurchasePolling PC resultJson: " + pollingstr.text);
						code = jsonPollingObjects["code"].ToString();
						bool flag3 = code == "1";
						if (flag3)
						{
							pollingValues = (jsonPollingObjects["values"] as Dictionary<string, object>);
							tradeSeq = pollingValues["tradeSeq"].ToString();
							tradeState = pollingValues["state"].ToString();
							productId = pollingValues["productId"].ToString();
							Msg = jsonPollingObjects["msg"].ToString();
							bool flag4 = tradeState == TradeSeqState.PAY_CONFIRM.ToString() || tradeState == TradeSeqState.ORDER_SUCCEED.ToString();
							if (flag4)
							{
								this.isNeedPolling = false;
								receipt = pollingValues["receipt"].ToString();
								purchaseSucceed(productId, receipt, tradeSeq);
								return false;
							}
							bool flag5 = tradeState == TradeSeqState.PAY_FAILED.ToString();
							if (flag5)
							{
								this.isNeedPolling = false;
								purchaseFailed(productId, PurchaseFailureReason.UserCancelled, Msg);
								return false;
							}
							pollingValues = null;
							tradeSeq = null;
							tradeState = null;
							productId = null;
							Msg = null;
						}
						jsonPollingObjects = null;
						code = null;
					}
					base.StartCoroutine(base.StartPurchasePolling(authGlobal, transactionId, purchaseSucceed, purchaseFailed));
					return false;
				}
				default:
					return false;
				}
			}

			// Token: 0x17000027 RID: 39
			// (get) Token: 0x060000A7 RID: 167 RVA: 0x000044C8 File Offset: 0x000026C8
			object IEnumerator<object>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x060000A8 RID: 168 RVA: 0x00003E21 File Offset: 0x00002021
			[DebuggerHidden]
			void IEnumerator.Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x17000028 RID: 40
			// (get) Token: 0x060000A9 RID: 169 RVA: 0x000044C8 File Offset: 0x000026C8
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x0400006F RID: 111
			private int <>1__state;

			// Token: 0x04000070 RID: 112
			private object <>2__current;

			// Token: 0x04000071 RID: 113
			public string authGlobal;

			// Token: 0x04000072 RID: 114
			public string transactionId;

			// Token: 0x04000073 RID: 115
			public Action<string, string, string> purchaseSucceed;

			// Token: 0x04000074 RID: 116
			public Action<string, PurchaseFailureReason, string> purchaseFailed;

			// Token: 0x04000075 RID: 117
			public MoolahStoreImpl <>4__this;

			// Token: 0x04000076 RID: 118
			private string <orderSuccess>5__1;

			// Token: 0x04000077 RID: 119
			private string <signstr>5__2;

			// Token: 0x04000078 RID: 120
			private string <sign>5__3;

			// Token: 0x04000079 RID: 121
			private string <param>5__4;

			// Token: 0x0400007A RID: 122
			private string <url>5__5;

			// Token: 0x0400007B RID: 123
			private WWW <pollingstr>5__6;

			// Token: 0x0400007C RID: 124
			private Dictionary<string, object> <jsonPollingObjects>5__7;

			// Token: 0x0400007D RID: 125
			private string <code>5__8;

			// Token: 0x0400007E RID: 126
			private Dictionary<string, object> <pollingValues>5__9;

			// Token: 0x0400007F RID: 127
			private string <tradeSeq>5__10;

			// Token: 0x04000080 RID: 128
			private string <tradeState>5__11;

			// Token: 0x04000081 RID: 129
			private string <productId>5__12;

			// Token: 0x04000082 RID: 130
			private string <Msg>5__13;

			// Token: 0x04000083 RID: 131
			private string <receipt>5__14;
		}

		// Token: 0x02000028 RID: 40
		[CompilerGenerated]
		private sealed class <RestoreTransactionIDProcess>d__45 : IEnumerator<object>, IEnumerator, IDisposable
		{
			// Token: 0x060000AA RID: 170 RVA: 0x000044D0 File Offset: 0x000026D0
			[DebuggerHidden]
			public <RestoreTransactionIDProcess>d__45(int <>1__state)
			{
				this.<>1__state = <>1__state;
			}

			// Token: 0x060000AB RID: 171 RVA: 0x00003CB8 File Offset: 0x00001EB8
			[DebuggerHidden]
			void IDisposable.Dispose()
			{
			}

			// Token: 0x060000AC RID: 172 RVA: 0x000044E0 File Offset: 0x000026E0
			bool IEnumerator.MoveNext()
			{
				int num = this.<>1__state;
				if (num == 0)
				{
					this.<>1__state = -1;
					customID = this.m_CustomerID;
					bool flag = string.IsNullOrEmpty(customID);
					if (flag)
					{
						customID = base.DeviceUniqueIdentifier();
					}
					wf = new WWWForm();
					wf.AddField("appId", this.m_appKey);
					wf.AddField("customerId", customID);
					now = DateTime.Now;
					endDate = now.ToString("yyyy/MM/dd");
					DateTime upperWeek = now.AddDays(-7.0);
					startDate = upperWeek.ToString("yyyy/MM/dd");
					wf.AddField("startDate", startDate);
					wf.AddField("endDate", endDate);
					sign = base.GetStringMD5(this.m_appKey + customID + this.m_hashKey);
					wf.AddField("Sign", sign);
					w = new WWW(MoolahStoreImpl.requestRestoreTransactionUrl, wf);
					this.<>2__current = w;
					this.<>1__state = 1;
					return true;
				}
				if (num != 1)
				{
					return false;
				}
				this.<>1__state = -1;
				bool flag2 = !string.IsNullOrEmpty(w.error);
				if (flag2)
				{
					Debug.LogError("CloudMoolah RestoreTransactionID error: " + w.error);
					result(RestoreTransactionIDState.NotKnown);
				}
				else
				{
					Debug.LogError("CloudMoolah RestoreTransactionIDProcess text: " + w.text);
					restoreObjects = (MiniJson.JsonDecode(w.text) as Dictionary<string, object>);
					code = restoreObjects["code"].ToString();
					bool flag3 = code == "1";
					if (flag3)
					{
						restoreValues = (restoreObjects["values"] as List<object>);
						enumerator = restoreValues.GetEnumerator();
						try
						{
							while (enumerator.MoveNext())
							{
								restoreObjectElem = (Dictionary<string, object>)enumerator.Current;
								productId = restoreObjectElem["productId"].ToString();
								tradeSeq = restoreObjectElem["tradeSeq"].ToString();
								receipt = restoreObjectElem["receipt"].ToString();
								base.PurchaseSucceed(productId, receipt, tradeSeq);
								productId = null;
								tradeSeq = null;
								receipt = null;
								restoreObjectElem = null;
							}
						}
						finally
						{
							((IDisposable)enumerator).Dispose();
						}
						enumerator = default(List<object>.Enumerator);
						result(RestoreTransactionIDState.RestoreSucceed);
						restoreValues = null;
					}
					else
					{
						bool flag4 = code == "CMB0000147";
						if (flag4)
						{
							result(RestoreTransactionIDState.NoTransactionRestore);
						}
						else
						{
							result(RestoreTransactionIDState.RestoreFailed);
						}
					}
					restoreObjects = null;
					code = null;
				}
				return false;
			}

			// Token: 0x17000029 RID: 41
			// (get) Token: 0x060000AD RID: 173 RVA: 0x000048A0 File Offset: 0x00002AA0
			object IEnumerator<object>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x060000AE RID: 174 RVA: 0x00003E21 File Offset: 0x00002021
			[DebuggerHidden]
			void IEnumerator.Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x1700002A RID: 42
			// (get) Token: 0x060000AF RID: 175 RVA: 0x000048A0 File Offset: 0x00002AA0
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x04000084 RID: 132
			private int <>1__state;

			// Token: 0x04000085 RID: 133
			private object <>2__current;

			// Token: 0x04000086 RID: 134
			public Action<RestoreTransactionIDState> result;

			// Token: 0x04000087 RID: 135
			public MoolahStoreImpl <>4__this;

			// Token: 0x04000088 RID: 136
			private string <customID>5__1;

			// Token: 0x04000089 RID: 137
			private WWWForm <wf>5__2;

			// Token: 0x0400008A RID: 138
			private DateTime <now>5__3;

			// Token: 0x0400008B RID: 139
			private string <endDate>5__4;

			// Token: 0x0400008C RID: 140
			private DateTime <upperWeek>5__5;

			// Token: 0x0400008D RID: 141
			private string <startDate>5__6;

			// Token: 0x0400008E RID: 142
			private string <sign>5__7;

			// Token: 0x0400008F RID: 143
			private WWW <w>5__8;

			// Token: 0x04000090 RID: 144
			private Dictionary<string, object> <restoreObjects>5__9;

			// Token: 0x04000091 RID: 145
			private string <code>5__10;

			// Token: 0x04000092 RID: 146
			private List<object> <restoreValues>5__11;

			// Token: 0x04000093 RID: 147
			private List<object>.Enumerator <>s__12;

			// Token: 0x04000094 RID: 148
			private Dictionary<string, object> <restoreObjectElem>5__13;

			// Token: 0x04000095 RID: 149
			private string <productId>5__14;

			// Token: 0x04000096 RID: 150
			private string <tradeSeq>5__15;

			// Token: 0x04000097 RID: 151
			private string <receipt>5__16;
		}

		// Token: 0x02000029 RID: 41
		[CompilerGenerated]
		private sealed class <ValidateReceiptProcess>d__47 : IEnumerator<object>, IEnumerator, IDisposable
		{
			// Token: 0x060000B0 RID: 176 RVA: 0x000048A8 File Offset: 0x00002AA8
			[DebuggerHidden]
			public <ValidateReceiptProcess>d__47(int <>1__state)
			{
				this.<>1__state = <>1__state;
			}

			// Token: 0x060000B1 RID: 177 RVA: 0x00003CB8 File Offset: 0x00001EB8
			[DebuggerHidden]
			void IDisposable.Dispose()
			{
			}

			// Token: 0x060000B2 RID: 178 RVA: 0x000048B8 File Offset: 0x00002AB8
			bool IEnumerator.MoveNext()
			{
				int num = this.<>1__state;
				if (num == 0)
				{
					this.<>1__state = -1;
					tempJson = (MiniJson.JsonDecode(receipt) as Dictionary<string, object>);
					bool flag = tempJson != null;
					if (flag)
					{
						bool flag2 = tempJson["Payload"] != null;
						if (flag2)
						{
							receipt = tempJson["Payload"].ToString();
						}
					}
					wf = new WWWForm();
					wf.AddField("appId", this.m_appKey);
					wf.AddField("receipt", receipt);
					sign = base.GetStringMD5(this.m_appKey + receipt + this.m_hashKey);
					wf.AddField("sign", sign);
					w = new WWW(MoolahStoreImpl.requestValidateReceiptUrl, wf);
					this.<>2__current = w;
					this.<>1__state = 1;
					return true;
				}
				if (num != 1)
				{
					return false;
				}
				this.<>1__state = -1;
				bool flag3 = !string.IsNullOrEmpty(w.error);
				if (flag3)
				{
					Debug.LogError("CloudMoolah ValidateReceipt error: " + w.error);
					result(transactionId, ValidateReceiptState.NotKnown, "ValidateReceiptState NotKnown");
				}
				else
				{
					Debug.LogError("CloudMoolah validateReceiptProcess text: " + w.text);
					jsonObjects = (MiniJson.JsonDecode(w.text) as Dictionary<string, object>);
					code = jsonObjects["code"].ToString();
					msg = jsonObjects["msg"].ToString();
					bool flag4 = code == "1";
					if (flag4)
					{
						result(transactionId, ValidateReceiptState.ValidateSucceed, "ValidateReceiptState ValidateSucceeded");
					}
					else
					{
						result(transactionId, ValidateReceiptState.ValidateFailed, msg);
					}
					jsonObjects = null;
					code = null;
					msg = null;
				}
				return false;
			}

			// Token: 0x1700002B RID: 43
			// (get) Token: 0x060000B3 RID: 179 RVA: 0x00004B1A File Offset: 0x00002D1A
			object IEnumerator<object>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x060000B4 RID: 180 RVA: 0x00003E21 File Offset: 0x00002021
			[DebuggerHidden]
			void IEnumerator.Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x1700002C RID: 44
			// (get) Token: 0x060000B5 RID: 181 RVA: 0x00004B1A File Offset: 0x00002D1A
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x04000098 RID: 152
			private int <>1__state;

			// Token: 0x04000099 RID: 153
			private object <>2__current;

			// Token: 0x0400009A RID: 154
			public string transactionId;

			// Token: 0x0400009B RID: 155
			public string receipt;

			// Token: 0x0400009C RID: 156
			public Action<string, ValidateReceiptState, string> result;

			// Token: 0x0400009D RID: 157
			public MoolahStoreImpl <>4__this;

			// Token: 0x0400009E RID: 158
			private Dictionary<string, object> <tempJson>5__1;

			// Token: 0x0400009F RID: 159
			private WWWForm <wf>5__2;

			// Token: 0x040000A0 RID: 160
			private string <sign>5__3;

			// Token: 0x040000A1 RID: 161
			private WWW <w>5__4;

			// Token: 0x040000A2 RID: 162
			private Dictionary<string, object> <jsonObjects>5__5;

			// Token: 0x040000A3 RID: 163
			private string <code>5__6;

			// Token: 0x040000A4 RID: 164
			private string <msg>5__7;
		}
	}
}
