﻿using System;
using UnityEngine.Purchasing.Extension;

namespace UnityEngine.Purchasing
{
	// Token: 0x0200005A RID: 90
	public interface IAppleConfiguration : IStoreConfiguration
	{
		// Token: 0x17000037 RID: 55
		// (get) Token: 0x06000188 RID: 392
		string appReceipt { get; }

		// Token: 0x17000038 RID: 56
		// (get) Token: 0x06000189 RID: 393
		bool canMakePayments { get; }

		// Token: 0x0600018A RID: 394
		void SetApplePromotionalPurchaseInterceptorCallback(Action<Product> callback);
	}
}
