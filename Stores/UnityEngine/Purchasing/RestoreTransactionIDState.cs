﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x0200001E RID: 30
	public enum RestoreTransactionIDState
	{
		// Token: 0x04000037 RID: 55
		NoTransactionRestore,
		// Token: 0x04000038 RID: 56
		RestoreSucceed,
		// Token: 0x04000039 RID: 57
		RestoreFailed,
		// Token: 0x0400003A RID: 58
		NotKnown
	}
}
