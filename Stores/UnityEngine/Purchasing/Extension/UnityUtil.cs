﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using Uniject;

namespace UnityEngine.Purchasing.Extension
{
	// Token: 0x020000C3 RID: 195
	[AddComponentMenu("")]
	[HideInInspector]
	internal class UnityUtil : MonoBehaviour, IUtil
	{
		// Token: 0x0600038C RID: 908 RVA: 0x0000EC5C File Offset: 0x0000CE5C
		public T[] GetAnyComponentsOfType<T>() where T : class
		{
			GameObject[] array = (GameObject[])Object.FindObjectsOfType(typeof(GameObject));
			List<T> list = new List<T>();
			foreach (GameObject gameObject in array)
			{
				foreach (MonoBehaviour monoBehaviour in gameObject.GetComponents<MonoBehaviour>())
				{
					bool flag = monoBehaviour is T;
					if (flag)
					{
						list.Add(monoBehaviour as T);
					}
				}
			}
			return list.ToArray();
		}

		// Token: 0x17000086 RID: 134
		// (get) Token: 0x0600038D RID: 909 RVA: 0x0000ECF4 File Offset: 0x0000CEF4
		public DateTime currentTime
		{
			get
			{
				return DateTime.Now;
			}
		}

		// Token: 0x17000087 RID: 135
		// (get) Token: 0x0600038E RID: 910 RVA: 0x0000ED0C File Offset: 0x0000CF0C
		public string persistentDataPath
		{
			get
			{
				return Application.persistentDataPath;
			}
		}

		// Token: 0x17000088 RID: 136
		// (get) Token: 0x0600038F RID: 911 RVA: 0x0000ED24 File Offset: 0x0000CF24
		public string deviceUniqueIdentifier
		{
			get
			{
				return SystemInfo.deviceUniqueIdentifier;
			}
		}

		// Token: 0x17000089 RID: 137
		// (get) Token: 0x06000390 RID: 912 RVA: 0x0000ED3C File Offset: 0x0000CF3C
		public string unityVersion
		{
			get
			{
				return Application.unityVersion;
			}
		}

		// Token: 0x1700008A RID: 138
		// (get) Token: 0x06000391 RID: 913 RVA: 0x0000ED54 File Offset: 0x0000CF54
		public string cloudProjectId
		{
			get
			{
				return Application.cloudProjectId;
			}
		}

		// Token: 0x1700008B RID: 139
		// (get) Token: 0x06000392 RID: 914 RVA: 0x0000ED6C File Offset: 0x0000CF6C
		public string userId
		{
			get
			{
				return PlayerPrefs.GetString("unity.cloud_userid", string.Empty);
			}
		}

		// Token: 0x1700008C RID: 140
		// (get) Token: 0x06000393 RID: 915 RVA: 0x0000ED90 File Offset: 0x0000CF90
		public string gameVersion
		{
			get
			{
				return Application.version;
			}
		}

		// Token: 0x1700008D RID: 141
		// (get) Token: 0x06000394 RID: 916 RVA: 0x0000EDA8 File Offset: 0x0000CFA8
		public ulong sessionId
		{
			get
			{
				return ulong.Parse(PlayerPrefs.GetString("unity.player_sessionid", "0"));
			}
		}

		// Token: 0x1700008E RID: 142
		// (get) Token: 0x06000395 RID: 917 RVA: 0x0000EDD0 File Offset: 0x0000CFD0
		public RuntimePlatform platform
		{
			get
			{
				return Application.platform;
			}
		}

		// Token: 0x1700008F RID: 143
		// (get) Token: 0x06000396 RID: 918 RVA: 0x0000EDE8 File Offset: 0x0000CFE8
		public bool isEditor
		{
			get
			{
				return Application.isEditor;
			}
		}

		// Token: 0x17000090 RID: 144
		// (get) Token: 0x06000397 RID: 919 RVA: 0x0000EE00 File Offset: 0x0000D000
		public string deviceModel
		{
			get
			{
				return SystemInfo.deviceModel;
			}
		}

		// Token: 0x17000091 RID: 145
		// (get) Token: 0x06000398 RID: 920 RVA: 0x0000EE18 File Offset: 0x0000D018
		public string deviceName
		{
			get
			{
				return SystemInfo.deviceName;
			}
		}

		// Token: 0x17000092 RID: 146
		// (get) Token: 0x06000399 RID: 921 RVA: 0x0000EE30 File Offset: 0x0000D030
		public DeviceType deviceType
		{
			get
			{
				return SystemInfo.deviceType;
			}
		}

		// Token: 0x17000093 RID: 147
		// (get) Token: 0x0600039A RID: 922 RVA: 0x0000EE48 File Offset: 0x0000D048
		public string operatingSystem
		{
			get
			{
				return SystemInfo.operatingSystem;
			}
		}

		// Token: 0x17000094 RID: 148
		// (get) Token: 0x0600039B RID: 923 RVA: 0x0000EE60 File Offset: 0x0000D060
		public int screenWidth
		{
			get
			{
				return Screen.width;
			}
		}

		// Token: 0x17000095 RID: 149
		// (get) Token: 0x0600039C RID: 924 RVA: 0x0000EE78 File Offset: 0x0000D078
		public int screenHeight
		{
			get
			{
				return Screen.height;
			}
		}

		// Token: 0x17000096 RID: 150
		// (get) Token: 0x0600039D RID: 925 RVA: 0x0000EE90 File Offset: 0x0000D090
		public float screenDpi
		{
			get
			{
				return Screen.dpi;
			}
		}

		// Token: 0x17000097 RID: 151
		// (get) Token: 0x0600039E RID: 926 RVA: 0x0000EEA8 File Offset: 0x0000D0A8
		public string screenOrientation
		{
			get
			{
				return Screen.orientation.ToString();
			}
		}

		// Token: 0x0600039F RID: 927 RVA: 0x0000EED0 File Offset: 0x0000D0D0
		object IUtil.InitiateCoroutine(IEnumerator start)
		{
			return base.StartCoroutine(start);
		}

		// Token: 0x060003A0 RID: 928 RVA: 0x0000EEE9 File Offset: 0x0000D0E9
		void IUtil.InitiateCoroutine(IEnumerator start, int delay)
		{
			this.DelayedCoroutine(start, delay);
		}

		// Token: 0x060003A1 RID: 929 RVA: 0x0000EEF8 File Offset: 0x0000D0F8
		public void RunOnMainThread(Action runnable)
		{
			List<Action> obj = UnityUtil.s_Callbacks;
			lock (obj)
			{
				UnityUtil.s_Callbacks.Add(runnable);
				UnityUtil.s_CallbacksPending = true;
			}
		}

		// Token: 0x060003A2 RID: 930 RVA: 0x0000EF44 File Offset: 0x0000D144
		public object GetWaitForSeconds(int seconds)
		{
			return new WaitForSeconds((float)seconds);
		}

		// Token: 0x060003A3 RID: 931 RVA: 0x0000EF5D File Offset: 0x0000D15D
		private void Start()
		{
			Object.DontDestroyOnLoad(base.gameObject);
		}

		// Token: 0x060003A4 RID: 932 RVA: 0x0000EF6C File Offset: 0x0000D16C
		public static T FindInstanceOfType<T>() where T : MonoBehaviour
		{
			return (T)((object)Object.FindObjectOfType(typeof(T)));
		}

		// Token: 0x060003A5 RID: 933 RVA: 0x0000EF94 File Offset: 0x0000D194
		public static T LoadResourceInstanceOfType<T>() where T : MonoBehaviour
		{
			return ((GameObject)Object.Instantiate(Resources.Load(typeof(T).ToString()))).GetComponent<T>();
		}

		// Token: 0x060003A6 RID: 934 RVA: 0x0000EFCC File Offset: 0x0000D1CC
		public static bool PcPlatform()
		{
			return UnityUtil.s_PcControlledPlatforms.Contains(Application.platform);
		}

		// Token: 0x060003A7 RID: 935 RVA: 0x0000EFF0 File Offset: 0x0000D1F0
		public static void DebugLog(string message, params object[] args)
		{
			try
			{
				Debug.Log(string.Format("com.ballatergames.debug - {0}", string.Format(message, args)));
			}
			catch (ArgumentNullException message2)
			{
				Debug.Log(message2);
			}
			catch (FormatException message3)
			{
				Debug.Log(message3);
			}
		}

		// Token: 0x060003A8 RID: 936 RVA: 0x0000F050 File Offset: 0x0000D250
		private IEnumerator DelayedCoroutine(IEnumerator coroutine, int delay)
		{
			yield return new WaitForSeconds((float)delay);
			base.StartCoroutine(coroutine);
			yield break;
		}

		// Token: 0x060003A9 RID: 937 RVA: 0x0000F070 File Offset: 0x0000D270
		private void Update()
		{
			bool flag = !UnityUtil.s_CallbacksPending;
			if (!flag)
			{
				List<Action> obj = UnityUtil.s_Callbacks;
				Action[] array;
				lock (obj)
				{
					bool flag2 = UnityUtil.s_Callbacks.Count == 0;
					if (flag2)
					{
						return;
					}
					array = new Action[UnityUtil.s_Callbacks.Count];
					UnityUtil.s_Callbacks.CopyTo(array);
					UnityUtil.s_Callbacks.Clear();
					UnityUtil.s_CallbacksPending = false;
				}
				foreach (Action action in array)
				{
					action();
				}
			}
		}

		// Token: 0x060003AA RID: 938 RVA: 0x0000F124 File Offset: 0x0000D324
		public void AddPauseListener(Action<bool> runnable)
		{
			this.pauseListeners.Add(runnable);
		}

		// Token: 0x060003AB RID: 939 RVA: 0x0000F134 File Offset: 0x0000D334
		public void OnApplicationPause(bool paused)
		{
			foreach (Action<bool> action in this.pauseListeners)
			{
				action(paused);
			}
		}

		// Token: 0x060003AC RID: 940 RVA: 0x0000F190 File Offset: 0x0000D390
		public bool IsClassOrSubclass(Type potentialBase, Type potentialDescendant)
		{
			return potentialDescendant.IsSubclassOf(potentialBase) || potentialDescendant == potentialBase;
		}

		// Token: 0x060003AD RID: 941 RVA: 0x0000F1B2 File Offset: 0x0000D3B2
		public UnityUtil()
		{
		}

		// Token: 0x060003AE RID: 942 RVA: 0x0000F1C8 File Offset: 0x0000D3C8
		// Note: this type is marked as 'beforefieldinit'.
		static UnityUtil()
		{
		}

		// Token: 0x040002B1 RID: 689
		private static List<Action> s_Callbacks = new List<Action>();

		// Token: 0x040002B2 RID: 690
		private static volatile bool s_CallbacksPending;

		// Token: 0x040002B3 RID: 691
		private static List<RuntimePlatform> s_PcControlledPlatforms = new List<RuntimePlatform>
		{
			RuntimePlatform.LinuxPlayer,
			RuntimePlatform.OSXDashboardPlayer,
			RuntimePlatform.OSXEditor,
			RuntimePlatform.OSXPlayer,
			RuntimePlatform.WindowsEditor,
			RuntimePlatform.WindowsPlayer
		};

		// Token: 0x040002B4 RID: 692
		private List<Action<bool>> pauseListeners = new List<Action<bool>>();

		// Token: 0x020000C4 RID: 196
		[CompilerGenerated]
		private sealed class <DelayedCoroutine>d__49 : IEnumerator<object>, IEnumerator, IDisposable
		{
			// Token: 0x060003AF RID: 943 RVA: 0x0000F21A File Offset: 0x0000D41A
			[DebuggerHidden]
			public <DelayedCoroutine>d__49(int <>1__state)
			{
				this.<>1__state = <>1__state;
			}

			// Token: 0x060003B0 RID: 944 RVA: 0x00003CB8 File Offset: 0x00001EB8
			[DebuggerHidden]
			void IDisposable.Dispose()
			{
			}

			// Token: 0x060003B1 RID: 945 RVA: 0x0000F22C File Offset: 0x0000D42C
			bool IEnumerator.MoveNext()
			{
				int num = this.<>1__state;
				if (num == 0)
				{
					this.<>1__state = -1;
					this.<>2__current = new WaitForSeconds((float)delay);
					this.<>1__state = 1;
					return true;
				}
				if (num != 1)
				{
					return false;
				}
				this.<>1__state = -1;
				base.StartCoroutine(coroutine);
				return false;
			}

			// Token: 0x17000098 RID: 152
			// (get) Token: 0x060003B2 RID: 946 RVA: 0x0000F28E File Offset: 0x0000D48E
			object IEnumerator<object>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x060003B3 RID: 947 RVA: 0x00003E21 File Offset: 0x00002021
			[DebuggerHidden]
			void IEnumerator.Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x17000099 RID: 153
			// (get) Token: 0x060003B4 RID: 948 RVA: 0x0000F28E File Offset: 0x0000D48E
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x040002B5 RID: 693
			private int <>1__state;

			// Token: 0x040002B6 RID: 694
			private object <>2__current;

			// Token: 0x040002B7 RID: 695
			public IEnumerator coroutine;

			// Token: 0x040002B8 RID: 696
			public int delay;

			// Token: 0x040002B9 RID: 697
			public UnityUtil <>4__this;
		}
	}
}
