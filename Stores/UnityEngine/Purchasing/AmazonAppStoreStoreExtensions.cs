﻿using System;
using System.Collections.Generic;
using UnityEngine.Purchasing.Extension;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000012 RID: 18
	public class AmazonAppStoreStoreExtensions : IAmazonExtensions, IStoreExtension, IAmazonConfiguration, IStoreConfiguration
	{
		// Token: 0x0600004D RID: 77 RVA: 0x00002E2E File Offset: 0x0000102E
		public AmazonAppStoreStoreExtensions(AndroidJavaObject a)
		{
			this.android = a;
		}

		// Token: 0x0600004E RID: 78 RVA: 0x00002E3F File Offset: 0x0000103F
		public void WriteSandboxJSON(HashSet<ProductDefinition> products)
		{
			this.android.Call("writeSandboxJSON", new object[]
			{
				JSONSerializer.SerializeProductDefs(products)
			});
		}

		// Token: 0x0600004F RID: 79 RVA: 0x00002E62 File Offset: 0x00001062
		public void NotifyUnableToFulfillUnavailableProduct(string transactionID)
		{
			this.android.Call("notifyUnableToFulfillUnavailableProduct", new object[]
			{
				transactionID
			});
		}

		// Token: 0x17000017 RID: 23
		// (get) Token: 0x06000050 RID: 80 RVA: 0x00002E80 File Offset: 0x00001080
		public string amazonUserId
		{
			get
			{
				return this.android.Call<string>("getAmazonUserId", new object[0]);
			}
		}

		// Token: 0x0400001D RID: 29
		private AndroidJavaObject android;
	}
}
