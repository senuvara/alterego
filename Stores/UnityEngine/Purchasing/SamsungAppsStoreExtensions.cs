﻿using System;
using UnityEngine.Purchasing.Extension;

namespace UnityEngine.Purchasing
{
	// Token: 0x0200003B RID: 59
	internal class SamsungAppsStoreExtensions : AndroidJavaProxy, ISamsungAppsCallback, ISamsungAppsExtensions, IStoreExtension, ISamsungAppsConfiguration, IStoreConfiguration
	{
		// Token: 0x060000DE RID: 222 RVA: 0x00004F36 File Offset: 0x00003136
		public SamsungAppsStoreExtensions() : base("com.unity.purchasing.samsung.ISamsungAppsStoreCallback")
		{
		}

		// Token: 0x060000DF RID: 223 RVA: 0x00004F45 File Offset: 0x00003145
		public void SetAndroidJavaObject(AndroidJavaObject java)
		{
			this.m_Java = java;
		}

		// Token: 0x060000E0 RID: 224 RVA: 0x00004F4F File Offset: 0x0000314F
		public void SetMode(SamsungAppsMode mode)
		{
			this.m_Java.Call("setMode", new object[]
			{
				mode.ToString()
			});
		}

		// Token: 0x060000E1 RID: 225 RVA: 0x00004F79 File Offset: 0x00003179
		public void RestoreTransactions(Action<bool> callback)
		{
			this.m_RestoreCallback = callback;
			this.m_Java.Call("restoreTransactions", new object[0]);
		}

		// Token: 0x060000E2 RID: 226 RVA: 0x00004F9C File Offset: 0x0000319C
		public void OnTransactionsRestored(bool result)
		{
			bool flag = this.m_RestoreCallback != null;
			if (flag)
			{
				this.m_RestoreCallback(result);
			}
		}

		// Token: 0x040000AF RID: 175
		private Action<bool> m_RestoreCallback;

		// Token: 0x040000B0 RID: 176
		private AndroidJavaObject m_Java;
	}
}
