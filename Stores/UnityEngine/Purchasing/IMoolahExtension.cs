﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000021 RID: 33
	public interface IMoolahExtension : IStoreExtension
	{
		// Token: 0x0600006E RID: 110
		void RestoreTransactionID(Action<RestoreTransactionIDState> result);

		// Token: 0x0600006F RID: 111
		void ValidateReceipt(string transactionId, string receipt, Action<string, ValidateReceiptState, string> result);
	}
}
