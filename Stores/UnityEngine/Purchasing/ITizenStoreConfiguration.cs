﻿using System;
using UnityEngine.Purchasing.Extension;

namespace UnityEngine.Purchasing
{
	// Token: 0x020000AE RID: 174
	public interface ITizenStoreConfiguration : IStoreConfiguration
	{
		// Token: 0x06000341 RID: 833
		void SetGroupId(string group);
	}
}
