﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x0200008B RID: 139
	internal class ProductCatalogImpl : IProductCatalogImpl
	{
		// Token: 0x0600026A RID: 618 RVA: 0x0000A400 File Offset: 0x00008600
		public ProductCatalog LoadDefaultCatalog()
		{
			TextAsset textAsset = Resources.Load("IAPProductCatalog") as TextAsset;
			bool flag = textAsset != null;
			ProductCatalog result;
			if (flag)
			{
				result = ProductCatalog.FromTextAsset(textAsset);
			}
			else
			{
				result = new ProductCatalog();
			}
			return result;
		}

		// Token: 0x0600026B RID: 619 RVA: 0x00002CA1 File Offset: 0x00000EA1
		public ProductCatalogImpl()
		{
		}
	}
}
