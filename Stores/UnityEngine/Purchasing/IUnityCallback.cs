﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000008 RID: 8
	internal interface IUnityCallback
	{
		// Token: 0x06000027 RID: 39
		void OnSetupFailed(string json);

		// Token: 0x06000028 RID: 40
		void OnProductsRetrieved(string json);

		// Token: 0x06000029 RID: 41
		void OnPurchaseSucceeded(string id, string receipt, string transactionID);

		// Token: 0x0600002A RID: 42
		void OnPurchaseFailed(string json);
	}
}
