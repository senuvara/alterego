﻿using System;
using System.Collections.Generic;

namespace UnityEngine.Purchasing
{
	// Token: 0x020000C2 RID: 194
	internal static class ProductDefinitionExtensions
	{
		// Token: 0x0600038B RID: 907 RVA: 0x0000EA90 File Offset: 0x0000CC90
		internal static List<ProductDefinition> DecodeJSON(this List<object> productsList, string storeName)
		{
			List<ProductDefinition> list = new List<ProductDefinition>();
			List<ProductDefinition> result;
			try
			{
				foreach (object obj in productsList)
				{
					Dictionary<string, object> dictionary = (Dictionary<string, object>)obj;
					object obj2;
					dictionary.TryGetValue("id", out obj2);
					object obj3;
					dictionary.TryGetValue("store_ids", out obj3);
					object obj4;
					dictionary.TryGetValue("type", out obj4);
					Dictionary<string, object> dictionary2 = obj3 as Dictionary<string, object>;
					string storeSpecificId = (string)obj2;
					bool flag = dictionary2 != null;
					if (flag)
					{
						foreach (KeyValuePair<string, object> keyValuePair in dictionary2)
						{
							string b = keyValuePair.Key.ToLower();
							string text = (string)keyValuePair.Value;
							bool flag2 = !string.IsNullOrEmpty(text) && storeName.ToLower() == b;
							if (flag2)
							{
								storeSpecificId = text;
							}
						}
					}
					else
					{
						object obj5;
						dictionary.TryGetValue("storeSpecificId", out obj5);
						string text2 = (string)obj5;
						bool flag3 = text2 != null;
						if (flag3)
						{
							storeSpecificId = text2;
						}
					}
					ProductType type = (ProductType)Enum.Parse(typeof(ProductType), (string)obj4);
					ProductDefinition item = new ProductDefinition((string)obj2, storeSpecificId, type);
					list.Add(item);
				}
				result = list;
			}
			catch
			{
				result = null;
			}
			return result;
		}
	}
}
