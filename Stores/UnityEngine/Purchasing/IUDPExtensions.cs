﻿using System;
using UnityEngine.UDP;

namespace UnityEngine.Purchasing
{
	// Token: 0x0200004C RID: 76
	public interface IUDPExtensions : IStoreExtension
	{
		// Token: 0x06000128 RID: 296
		UserInfo GetUserInfo();

		// Token: 0x06000129 RID: 297
		string GetLastInitializationError();

		// Token: 0x0600012A RID: 298
		void EnableDebugLog(bool enable);
	}
}
