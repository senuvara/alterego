﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine.Purchasing.Extension;

namespace UnityEngine.Purchasing
{
	// Token: 0x0200003C RID: 60
	internal class FakeUnityChannelConfiguration : IUnityChannelConfiguration, IStoreConfiguration
	{
		// Token: 0x1700002D RID: 45
		// (get) Token: 0x060000E3 RID: 227 RVA: 0x00004FC6 File Offset: 0x000031C6
		// (set) Token: 0x060000E4 RID: 228 RVA: 0x00004FCE File Offset: 0x000031CE
		public bool fetchReceiptPayloadOnPurchase
		{
			[CompilerGenerated]
			get
			{
				return this.<fetchReceiptPayloadOnPurchase>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<fetchReceiptPayloadOnPurchase>k__BackingField = value;
			}
		}

		// Token: 0x060000E5 RID: 229 RVA: 0x00002CA1 File Offset: 0x00000EA1
		public FakeUnityChannelConfiguration()
		{
		}

		// Token: 0x040000B1 RID: 177
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private bool <fetchReceiptPayloadOnPurchase>k__BackingField;
	}
}
