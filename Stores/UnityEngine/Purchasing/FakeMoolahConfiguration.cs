﻿using System;
using UnityEngine.Purchasing.Extension;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000017 RID: 23
	internal class FakeMoolahConfiguration : IMoolahConfiguration, IStoreConfiguration
	{
		// Token: 0x1700001A RID: 26
		// (get) Token: 0x0600005C RID: 92 RVA: 0x00002F50 File Offset: 0x00001150
		// (set) Token: 0x0600005D RID: 93 RVA: 0x00002F68 File Offset: 0x00001168
		public string appKey
		{
			get
			{
				return this.m_appKey;
			}
			set
			{
				this.m_appKey = value;
			}
		}

		// Token: 0x1700001B RID: 27
		// (get) Token: 0x0600005E RID: 94 RVA: 0x00002F74 File Offset: 0x00001174
		// (set) Token: 0x0600005F RID: 95 RVA: 0x00002F8C File Offset: 0x0000118C
		public string hashKey
		{
			get
			{
				return this.m_hashKey;
			}
			set
			{
				this.m_hashKey = value;
			}
		}

		// Token: 0x1700001C RID: 28
		// (get) Token: 0x06000060 RID: 96 RVA: 0x00002F98 File Offset: 0x00001198
		// (set) Token: 0x06000061 RID: 97 RVA: 0x00002FB0 File Offset: 0x000011B0
		public string notificationURL
		{
			get
			{
				return this.m_notificationURL;
			}
			set
			{
				this.m_notificationURL = value;
			}
		}

		// Token: 0x06000062 RID: 98 RVA: 0x00002EA8 File Offset: 0x000010A8
		public void SetMode(CloudMoolahMode mode)
		{
		}

		// Token: 0x06000063 RID: 99 RVA: 0x00002CA1 File Offset: 0x00000EA1
		public FakeMoolahConfiguration()
		{
		}

		// Token: 0x0400001E RID: 30
		private string m_appKey;

		// Token: 0x0400001F RID: 31
		private string m_hashKey;

		// Token: 0x04000020 RID: 32
		private string m_notificationURL;
	}
}
