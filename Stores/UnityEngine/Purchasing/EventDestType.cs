﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x0200006F RID: 111
	public enum EventDestType
	{
		// Token: 0x04000145 RID: 325
		Unknown,
		// Token: 0x04000146 RID: 326
		AdsTracking,
		// Token: 0x04000147 RID: 327
		IAP,
		// Token: 0x04000148 RID: 328
		Analytics,
		// Token: 0x04000149 RID: 329
		CDP,
		// Token: 0x0400014A RID: 330
		CDPDirect,
		// Token: 0x0400014B RID: 331
		AdsIPC
	}
}
