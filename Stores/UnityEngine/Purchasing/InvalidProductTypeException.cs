﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x0200009B RID: 155
	public class InvalidProductTypeException : ReceiptParserException
	{
		// Token: 0x06000311 RID: 785 RVA: 0x0000D2EE File Offset: 0x0000B4EE
		public InvalidProductTypeException()
		{
		}
	}
}
