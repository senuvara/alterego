﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000088 RID: 136
	[Serializable]
	public class ProductCatalog
	{
		// Token: 0x1700005C RID: 92
		// (get) Token: 0x0600025A RID: 602 RVA: 0x0000A232 File Offset: 0x00008432
		public ICollection<ProductCatalogItem> allProducts
		{
			get
			{
				return this.products;
			}
		}

		// Token: 0x1700005D RID: 93
		// (get) Token: 0x0600025B RID: 603 RVA: 0x0000A23C File Offset: 0x0000843C
		public ICollection<ProductCatalogItem> allValidProducts
		{
			get
			{
				return (from x in this.products
				where !string.IsNullOrEmpty(x.id) && x.id.Trim().Length != 0
				select x).ToList<ProductCatalogItem>();
			}
		}

		// Token: 0x0600025C RID: 604 RVA: 0x0000A280 File Offset: 0x00008480
		internal static void Initialize()
		{
			bool flag = ProductCatalog.instance == null;
			if (flag)
			{
				ProductCatalog.Initialize(new ProductCatalogImpl());
			}
		}

		// Token: 0x0600025D RID: 605 RVA: 0x0000A2A7 File Offset: 0x000084A7
		public static void Initialize(IProductCatalogImpl productCatalogImpl)
		{
			ProductCatalog.instance = productCatalogImpl;
		}

		// Token: 0x0600025E RID: 606 RVA: 0x0000A2B0 File Offset: 0x000084B0
		public void Add(ProductCatalogItem item)
		{
			this.products.Add(item);
		}

		// Token: 0x0600025F RID: 607 RVA: 0x0000A2C0 File Offset: 0x000084C0
		public void Remove(ProductCatalogItem item)
		{
			this.products.Remove(item);
		}

		// Token: 0x06000260 RID: 608 RVA: 0x0000A2D0 File Offset: 0x000084D0
		public bool IsEmpty()
		{
			foreach (ProductCatalogItem productCatalogItem in this.products)
			{
				bool flag = !string.IsNullOrEmpty(productCatalogItem.id);
				if (flag)
				{
					return false;
				}
			}
			return true;
		}

		// Token: 0x06000261 RID: 609 RVA: 0x0000A340 File Offset: 0x00008540
		public static string Serialize(ProductCatalog catalog)
		{
			return JsonUtility.ToJson(catalog);
		}

		// Token: 0x06000262 RID: 610 RVA: 0x0000A358 File Offset: 0x00008558
		public static ProductCatalog Deserialize(string catalogJSON)
		{
			return JsonUtility.FromJson<ProductCatalog>(catalogJSON);
		}

		// Token: 0x06000263 RID: 611 RVA: 0x0000A370 File Offset: 0x00008570
		public static ProductCatalog FromTextAsset(TextAsset asset)
		{
			return ProductCatalog.Deserialize(asset.text);
		}

		// Token: 0x06000264 RID: 612 RVA: 0x0000A390 File Offset: 0x00008590
		public static ProductCatalog LoadDefaultCatalog()
		{
			ProductCatalog.Initialize();
			return ProductCatalog.instance.LoadDefaultCatalog();
		}

		// Token: 0x06000265 RID: 613 RVA: 0x0000A3B2 File Offset: 0x000085B2
		public ProductCatalog()
		{
		}

		// Token: 0x040001BC RID: 444
		private static IProductCatalogImpl instance;

		// Token: 0x040001BD RID: 445
		public string appleSKU;

		// Token: 0x040001BE RID: 446
		public string appleTeamID;

		// Token: 0x040001BF RID: 447
		public bool enableCodelessAutoInitialization = false;

		// Token: 0x040001C0 RID: 448
		[SerializeField]
		private List<ProductCatalogItem> products = new List<ProductCatalogItem>();

		// Token: 0x040001C1 RID: 449
		public const string kCatalogPath = "Assets/Resources/IAPProductCatalog.json";

		// Token: 0x040001C2 RID: 450
		public const string prevkCatalogPath = "Assets/Plugins/UnityPurchasing/Resources/IAPProductCatalog.json";

		// Token: 0x02000089 RID: 137
		[CompilerGenerated]
		[Serializable]
		private sealed class <>c
		{
			// Token: 0x06000266 RID: 614 RVA: 0x0000A3CD File Offset: 0x000085CD
			// Note: this type is marked as 'beforefieldinit'.
			static <>c()
			{
			}

			// Token: 0x06000267 RID: 615 RVA: 0x00002CA1 File Offset: 0x00000EA1
			public <>c()
			{
			}

			// Token: 0x06000268 RID: 616 RVA: 0x0000A3D9 File Offset: 0x000085D9
			internal bool <get_allValidProducts>b__8_0(ProductCatalogItem x)
			{
				return !string.IsNullOrEmpty(x.id) && x.id.Trim().Length != 0;
			}

			// Token: 0x040001C3 RID: 451
			public static readonly ProductCatalog.<>c <>9 = new ProductCatalog.<>c();

			// Token: 0x040001C4 RID: 452
			public static Func<ProductCatalogItem, bool> <>9__8_0;
		}
	}
}
