﻿using System;
using AOT;
using Uniject;
using UnityEngine.Purchasing.Extension;

namespace UnityEngine.Purchasing
{
	// Token: 0x020000B0 RID: 176
	internal class TizenStoreImpl : JSONStore, ITizenStoreConfiguration, IStoreConfiguration
	{
		// Token: 0x06000343 RID: 835 RVA: 0x0000D976 File Offset: 0x0000BB76
		public TizenStoreImpl(IUtil util)
		{
			TizenStoreImpl.instance = this;
		}

		// Token: 0x06000344 RID: 836 RVA: 0x0000D986 File Offset: 0x0000BB86
		public void SetNativeStore(INativeTizenStore tizen)
		{
			base.SetNativeStore(tizen);
			this.m_Native = tizen;
			this.m_Native.SetUnityPurchasingCallback(new UnityNativePurchasingCallback(TizenStoreImpl.MessageCallback));
		}

		// Token: 0x06000345 RID: 837 RVA: 0x0000D9B0 File Offset: 0x0000BBB0
		public void SetGroupId(string group)
		{
			this.m_Native.SetGroupId(group);
		}

		// Token: 0x06000346 RID: 838 RVA: 0x0000D9C0 File Offset: 0x0000BBC0
		[MonoPInvokeCallback(typeof(UnityNativePurchasingCallback))]
		private static void MessageCallback(string subject, string payload, string receipt, string transactionId)
		{
			TizenStoreImpl.instance.ProcessMessage(subject, payload, receipt, transactionId);
		}

		// Token: 0x06000347 RID: 839 RVA: 0x0000D9D4 File Offset: 0x0000BBD4
		private void ProcessMessage(string subject, string payload, string receipt, string transactionId)
		{
			Debug.Log(string.Concat(new string[]
			{
				"[UnityIAP] ProcessMessage subject: ",
				subject,
				" payload: ",
				payload,
				" receipt: ",
				receipt,
				" transactionId: ",
				transactionId
			}));
			if (!(subject == "OnSetupFailed"))
			{
				if (!(subject == "OnProductsRetrieved"))
				{
					if (!(subject == "OnPurchaseSucceeded"))
					{
						if (subject == "OnPurchaseFailed")
						{
							base.OnPurchaseFailed(payload);
						}
					}
					else
					{
						this.OnPurchaseSucceeded(payload, receipt, transactionId);
					}
				}
				else
				{
					this.OnProductsRetrieved(payload);
				}
			}
			else
			{
				base.OnSetupFailed(payload);
			}
		}

		// Token: 0x0400027F RID: 639
		private static TizenStoreImpl instance;

		// Token: 0x04000280 RID: 640
		private INativeTizenStore m_Native;
	}
}
