﻿using System;
using System.Collections.Generic;

namespace UnityEngine.Purchasing
{
	// Token: 0x0200000A RID: 10
	internal static class SerializationExtensions
	{
		// Token: 0x06000031 RID: 49 RVA: 0x00002284 File Offset: 0x00000484
		public static string TryGetString(this Dictionary<string, object> dic, string key)
		{
			bool flag = dic.ContainsKey(key);
			if (flag)
			{
				bool flag2 = dic[key] != null;
				if (flag2)
				{
					return dic[key].ToString();
				}
			}
			return null;
		}
	}
}
