﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine.Purchasing.Security;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000094 RID: 148
	public class SubscriptionManager
	{
		// Token: 0x060002E6 RID: 742 RVA: 0x0000C070 File Offset: 0x0000A270
		public static void UpdateSubscription(Product newProduct, Product oldProduct, string developerPayload, Action<Product, string> appleStore, Action<string, string> googleStore)
		{
			bool flag = oldProduct.receipt == null;
			if (flag)
			{
				Debug.Log("The product has not been purchased, a subscription can only be upgrade/downgrade when has already been purchased");
			}
			else
			{
				Dictionary<string, object> dictionary = (Dictionary<string, object>)MiniJson.JsonDecode(oldProduct.receipt);
				bool flag2 = dictionary == null || !dictionary.ContainsKey("Store") || !dictionary.ContainsKey("Payload");
				if (flag2)
				{
					Debug.Log("The product receipt does not contain enough information");
				}
				else
				{
					string text = (string)dictionary["Store"];
					string text2 = (string)dictionary["Payload"];
					bool flag3 = text2 != null;
					if (flag3)
					{
						string a = text;
						if (!(a == "GooglePlay"))
						{
							if (!(a == "AppleAppStore") && !(a == "MacAppStore"))
							{
								Debug.Log("This store does not support update subscriptions");
							}
							else
							{
								appleStore(newProduct, developerPayload);
							}
						}
						else
						{
							SubscriptionManager subscriptionManager = new SubscriptionManager(oldProduct, null);
							SubscriptionInfo subscriptionInfo = null;
							try
							{
								subscriptionInfo = subscriptionManager.getSubscriptionInfo();
							}
							catch (Exception message)
							{
								Debug.unityLogger.Log("Error: the product that will be updated does not have a valid receipt", message);
								return;
							}
							string storeSpecificId = newProduct.definition.storeSpecificId;
							googleStore(subscriptionInfo.getSubscriptionInfoJsonString(), storeSpecificId);
						}
					}
				}
			}
		}

		// Token: 0x060002E7 RID: 743 RVA: 0x0000C1C0 File Offset: 0x0000A3C0
		public static void UpdateSubscriptionInGooglePlayStore(Product oldProduct, Product newProduct, Action<string, string> googlePlayUpdateCallback)
		{
			SubscriptionManager subscriptionManager = new SubscriptionManager(oldProduct, null);
			SubscriptionInfo subscriptionInfo = null;
			try
			{
				subscriptionInfo = subscriptionManager.getSubscriptionInfo();
			}
			catch (Exception message)
			{
				Debug.unityLogger.Log("Error: the product that will be updated does not have a valid receipt", message);
				return;
			}
			string storeSpecificId = newProduct.definition.storeSpecificId;
			googlePlayUpdateCallback(subscriptionInfo.getSubscriptionInfoJsonString(), storeSpecificId);
		}

		// Token: 0x060002E8 RID: 744 RVA: 0x0000C224 File Offset: 0x0000A424
		public static void UpdateSubscriptionInAppleStore(Product newProduct, string developerPayload, Action<Product, string> appleStoreUpdateCallback)
		{
			appleStoreUpdateCallback(newProduct, developerPayload);
		}

		// Token: 0x060002E9 RID: 745 RVA: 0x0000C230 File Offset: 0x0000A430
		public SubscriptionManager(Product product, string intro_json)
		{
			this.receipt = product.receipt;
			this.productId = product.definition.storeSpecificId;
			this.intro_json = intro_json;
		}

		// Token: 0x060002EA RID: 746 RVA: 0x0000C25E File Offset: 0x0000A45E
		public SubscriptionManager(string receipt, string id, string intro_json)
		{
			this.receipt = receipt;
			this.productId = id;
			this.intro_json = intro_json;
		}

		// Token: 0x060002EB RID: 747 RVA: 0x0000C280 File Offset: 0x0000A480
		public SubscriptionInfo getSubscriptionInfo()
		{
			bool flag = this.receipt != null;
			if (flag)
			{
				Dictionary<string, object> dictionary = (Dictionary<string, object>)MiniJson.JsonDecode(this.receipt);
				string text = (string)dictionary["Store"];
				string text2 = (string)dictionary["Payload"];
				bool flag2 = text2 != null;
				if (flag2)
				{
					string a = text;
					SubscriptionInfo result;
					if (!(a == "GooglePlay"))
					{
						if (!(a == "AppleAppStore") && !(a == "MacAppStore"))
						{
							if (!(a == "AmazonApps"))
							{
								throw new StoreSubscriptionInfoNotSupportedException("Store not supported: " + text);
							}
							result = this.getAmazonAppStoreSubInfo(this.productId);
						}
						else
						{
							bool flag3 = this.productId == null;
							if (flag3)
							{
								throw new NullProductIdException();
							}
							result = this.getAppleAppStoreSubInfo(text2, this.productId);
						}
					}
					else
					{
						result = this.getGooglePlayStoreSubInfo(text2);
					}
					return result;
				}
			}
			throw new NullReceiptException();
		}

		// Token: 0x060002EC RID: 748 RVA: 0x0000C380 File Offset: 0x0000A580
		private SubscriptionInfo getAmazonAppStoreSubInfo(string productId)
		{
			return new SubscriptionInfo(productId);
		}

		// Token: 0x060002ED RID: 749 RVA: 0x0000C398 File Offset: 0x0000A598
		private SubscriptionInfo getAppleAppStoreSubInfo(string payload, string productId)
		{
			AppleReceipt appleReceipt = null;
			try
			{
				appleReceipt = new AppleReceiptParser().Parse(Convert.FromBase64String(payload));
			}
			catch (ArgumentException message)
			{
				Debug.unityLogger.Log("Unable to parse Apple receipt", message);
			}
			catch (IAPSecurityException message2)
			{
				Debug.unityLogger.Log("Unable to parse Apple receipt", message2);
			}
			catch (NullReferenceException message3)
			{
				Debug.unityLogger.Log("Unable to parse Apple receipt", message3);
			}
			List<AppleInAppPurchaseReceipt> list = new List<AppleInAppPurchaseReceipt>();
			bool flag = appleReceipt != null && appleReceipt.inAppPurchaseReceipts != null && appleReceipt.inAppPurchaseReceipts.Length != 0;
			if (flag)
			{
				foreach (AppleInAppPurchaseReceipt appleInAppPurchaseReceipt in appleReceipt.inAppPurchaseReceipts)
				{
					bool flag2 = appleInAppPurchaseReceipt.productID.Equals(productId);
					if (flag2)
					{
						list.Add(appleInAppPurchaseReceipt);
					}
				}
			}
			return (list.Count == 0) ? null : new SubscriptionInfo(this.findMostRecentReceipt(list), this.intro_json);
		}

		// Token: 0x060002EE RID: 750 RVA: 0x0000C4B8 File Offset: 0x0000A6B8
		private AppleInAppPurchaseReceipt findMostRecentReceipt(List<AppleInAppPurchaseReceipt> receipts)
		{
			receipts.Sort((AppleInAppPurchaseReceipt b, AppleInAppPurchaseReceipt a) => a.purchaseDate.CompareTo(b.purchaseDate));
			return receipts[0];
		}

		// Token: 0x060002EF RID: 751 RVA: 0x0000C4F8 File Offset: 0x0000A6F8
		private SubscriptionInfo getGooglePlayStoreSubInfo(string payload)
		{
			Dictionary<string, object> dictionary = (Dictionary<string, object>)MiniJson.JsonDecode(payload);
			string skuDetails = (string)dictionary["skuDetails"];
			bool purchaseHistorySupported = (bool)dictionary["isPurchaseHistorySupported"];
			Dictionary<string, object> dictionary2 = (Dictionary<string, object>)MiniJson.JsonDecode((string)dictionary["json"]);
			bool isAutoRenewing = (bool)dictionary2["autoRenewing"];
			DateTime dateTime = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
			DateTime purchaseDate = dateTime.AddMilliseconds((double)((long)dictionary2["purchaseTime"]));
			string json = (string)dictionary2["developerPayload"];
			Dictionary<string, object> dictionary3 = (Dictionary<string, object>)MiniJson.JsonDecode(json);
			bool isFreeTrial = (bool)dictionary3["is_free_trial"];
			bool hasIntroductoryPriceTrial = (bool)dictionary3["has_introductory_price_trial"];
			string updateMetadata = null;
			bool flag = (bool)dictionary3["is_updated"];
			if (flag)
			{
				updateMetadata = (string)dictionary3["update_subscription_metadata"];
			}
			return new SubscriptionInfo(skuDetails, isAutoRenewing, purchaseDate, isFreeTrial, hasIntroductoryPriceTrial, purchaseHistorySupported, updateMetadata);
		}

		// Token: 0x0400023B RID: 571
		private string receipt;

		// Token: 0x0400023C RID: 572
		private string productId;

		// Token: 0x0400023D RID: 573
		private string intro_json;

		// Token: 0x02000095 RID: 149
		[CompilerGenerated]
		[Serializable]
		private sealed class <>c
		{
			// Token: 0x060002F0 RID: 752 RVA: 0x0000C619 File Offset: 0x0000A819
			// Note: this type is marked as 'beforefieldinit'.
			static <>c()
			{
			}

			// Token: 0x060002F1 RID: 753 RVA: 0x00002CA1 File Offset: 0x00000EA1
			public <>c()
			{
			}

			// Token: 0x060002F2 RID: 754 RVA: 0x0000C628 File Offset: 0x0000A828
			internal int <findMostRecentReceipt>b__11_0(AppleInAppPurchaseReceipt b, AppleInAppPurchaseReceipt a)
			{
				return a.purchaseDate.CompareTo(b.purchaseDate);
			}

			// Token: 0x0400023E RID: 574
			public static readonly SubscriptionManager.<>c <>9 = new SubscriptionManager.<>c();

			// Token: 0x0400023F RID: 575
			public static Comparison<AppleInAppPurchaseReceipt> <>9__11_0;
		}
	}
}
