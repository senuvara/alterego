﻿using System;
using System.Collections.ObjectModel;

namespace UnityEngine.Purchasing
{
	// Token: 0x0200003E RID: 62
	internal interface INativeUnityChannelStore : INativeStore
	{
		// Token: 0x060000EA RID: 234
		void Purchase(string productId, Action<bool, string> callback, string developerPayload = null);

		// Token: 0x060000EB RID: 235
		void RetrieveProducts(ReadOnlyCollection<ProductDefinition> products, Action<bool, string> callback);

		// Token: 0x060000EC RID: 236
		void ConfirmPurchase(string gameOrderId, Action<bool, string, string> callback);

		// Token: 0x060000ED RID: 237
		void ValidateReceipt(string gameOrderId, Action<bool, string, string> callback);
	}
}
