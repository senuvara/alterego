﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000079 RID: 121
	internal static class QueryHelper
	{
		// Token: 0x06000222 RID: 546 RVA: 0x000094DC File Offset: 0x000076DC
		internal static string ToQueryString(this Dictionary<string, object> parameters)
		{
			StringBuilder stringBuilder = new StringBuilder();
			foreach (string text in parameters.Keys)
			{
				string text2 = parameters[text].ToString();
				bool flag = text2 == null;
				if (!flag)
				{
					stringBuilder.Append((stringBuilder.Length == 0) ? "?" : "&");
					stringBuilder.AppendFormat("{0}={1}", Uri.EscapeDataString(text), Uri.EscapeDataString(text2));
				}
			}
			return stringBuilder.ToString();
		}
	}
}
