﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x020000A1 RID: 161
	[Serializable]
	public class UnifiedReceipt
	{
		// Token: 0x0600031A RID: 794 RVA: 0x00002CA1 File Offset: 0x00000EA1
		public UnifiedReceipt()
		{
		}

		// Token: 0x04000261 RID: 609
		public string Payload;

		// Token: 0x04000262 RID: 610
		public string Store;

		// Token: 0x04000263 RID: 611
		public string TransactionID;
	}
}
