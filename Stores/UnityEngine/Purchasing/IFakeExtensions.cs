﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x020000BB RID: 187
	internal interface IFakeExtensions : IStoreExtension
	{
		// Token: 0x17000085 RID: 133
		// (get) Token: 0x06000365 RID: 869
		// (set) Token: 0x06000366 RID: 870
		string unavailableProductId { get; set; }
	}
}
