﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using UnityEngine.Purchasing.Extension;

namespace UnityEngine.Purchasing
{
	// Token: 0x020000B7 RID: 183
	internal class FakeStore : JSONStore, IFakeExtensions, IStoreExtension, INativeStore
	{
		// Token: 0x17000084 RID: 132
		// (get) Token: 0x06000353 RID: 851 RVA: 0x0000DBDA File Offset: 0x0000BDDA
		// (set) Token: 0x06000354 RID: 852 RVA: 0x0000DBE2 File Offset: 0x0000BDE2
		public string unavailableProductId
		{
			[CompilerGenerated]
			get
			{
				return this.<unavailableProductId>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<unavailableProductId>k__BackingField = value;
			}
		}

		// Token: 0x06000355 RID: 853 RVA: 0x0000DBEB File Offset: 0x0000BDEB
		public override void Initialize(IStoreCallback biller)
		{
			this.m_Biller = biller;
			base.Initialize(biller);
			base.SetNativeStore(this);
		}

		// Token: 0x06000356 RID: 854 RVA: 0x0000DC08 File Offset: 0x0000BE08
		public void RetrieveProducts(string json)
		{
			List<object> productsList = (List<object>)MiniJson.JsonDecode(json);
			List<ProductDefinition> source = productsList.DecodeJSON("fake");
			this.StoreRetrieveProducts(new ReadOnlyCollection<ProductDefinition>(source.ToList<ProductDefinition>()));
		}

		// Token: 0x06000357 RID: 855 RVA: 0x0000DC40 File Offset: 0x0000BE40
		public void StoreRetrieveProducts(ReadOnlyCollection<ProductDefinition> productDefinitions)
		{
			List<ProductDescription> products = new List<ProductDescription>();
			foreach (ProductDefinition productDefinition in productDefinitions)
			{
				bool flag = this.unavailableProductId != productDefinition.id;
				if (flag)
				{
					ProductMetadata metadata = new ProductMetadata("$0.01", "Fake title for " + productDefinition.id, "Fake description", "USD", 0.01m);
					ProductCatalog productCatalog = ProductCatalog.LoadDefaultCatalog();
					bool flag2 = productCatalog != null;
					if (flag2)
					{
						foreach (ProductCatalogItem productCatalogItem in productCatalog.allProducts)
						{
							bool flag3 = productCatalogItem.id == productDefinition.id;
							if (flag3)
							{
								metadata = new ProductMetadata(productCatalogItem.googlePrice.value.ToString(), productCatalogItem.defaultDescription.Title, productCatalogItem.defaultDescription.Description, "USD", productCatalogItem.googlePrice.value);
							}
						}
					}
					products.Add(new ProductDescription(productDefinition.storeSpecificId, metadata));
				}
			}
			Action<bool, InitializationFailureReason> action = delegate(bool allow, InitializationFailureReason failureReason)
			{
				if (allow)
				{
					this.m_Biller.OnProductsRetrieved(products);
					Promo.ProvideProductsToAds(this, this.m_Biller);
				}
				else
				{
					this.m_Biller.OnSetupFailed(failureReason);
				}
			};
			bool flag4 = this.UIMode != FakeStoreUIMode.DeveloperUser || !this.StartUI<InitializationFailureReason>(productDefinitions, FakeStore.DialogType.RetrieveProducts, action);
			if (flag4)
			{
				action(true, InitializationFailureReason.AppNotKnown);
			}
		}

		// Token: 0x06000358 RID: 856 RVA: 0x0000DE0C File Offset: 0x0000C00C
		public void Purchase(string productJSON, string developerPayload)
		{
			Dictionary<string, object> dictionary = (Dictionary<string, object>)MiniJson.JsonDecode(productJSON);
			object obj;
			dictionary.TryGetValue("id", out obj);
			string id = obj.ToString();
			dictionary.TryGetValue("storeSpecificId", out obj);
			string storeSpecificId = obj.ToString();
			dictionary.TryGetValue("type", out obj);
			string value = obj.ToString();
			bool flag = Enum.IsDefined(typeof(ProductType), value);
			ProductType type;
			if (flag)
			{
				type = (ProductType)Enum.Parse(typeof(ProductType), value);
			}
			else
			{
				type = ProductType.Consumable;
			}
			ProductDefinition product = new ProductDefinition(id, storeSpecificId, type);
			this.FakePurchase(product, developerPayload);
		}

		// Token: 0x06000359 RID: 857 RVA: 0x0000DEB0 File Offset: 0x0000C0B0
		private void FakePurchase(ProductDefinition product, string developerPayload)
		{
			this.purchaseCalled = true;
			bool flag = product.type > ProductType.Consumable;
			if (flag)
			{
				this.m_PurchasedProducts.Add(product.storeSpecificId);
			}
			Action<bool, PurchaseFailureReason> action = delegate(bool allow, PurchaseFailureReason failureReason)
			{
				if (allow)
				{
					this.<>n__0(product.storeSpecificId, "{ \"this\" : \"is a fake receipt\" }", Guid.NewGuid().ToString());
				}
				else
				{
					bool flag3 = failureReason == (PurchaseFailureReason)Enum.Parse(typeof(PurchaseFailureReason), "Unknown");
					if (flag3)
					{
						failureReason = PurchaseFailureReason.UserCancelled;
					}
					PurchaseFailureDescription failure = new PurchaseFailureDescription(product.storeSpecificId, failureReason, "failed a fake store purchase");
					this.OnPurchaseFailed(failure, null);
				}
			};
			bool flag2 = !this.StartUI<PurchaseFailureReason>(product, FakeStore.DialogType.Purchase, action);
			if (flag2)
			{
				action(true, (PurchaseFailureReason)Enum.Parse(typeof(PurchaseFailureReason), "Unknown"));
			}
		}

		// Token: 0x0600035A RID: 858 RVA: 0x0000DF48 File Offset: 0x0000C148
		public void RestoreTransactions(Action<bool> callback)
		{
			this.restoreCalled = true;
			foreach (string storeSpecificId in this.m_PurchasedProducts)
			{
				this.m_Biller.OnPurchaseSucceeded(storeSpecificId, "{ \"this\" : \"is a fake receipt\" }", "1");
			}
			callback(true);
		}

		// Token: 0x0600035B RID: 859 RVA: 0x00002EA8 File Offset: 0x000010A8
		public void FinishTransaction(string productJSON, string transactionID)
		{
		}

		// Token: 0x0600035C RID: 860 RVA: 0x00002EA8 File Offset: 0x000010A8
		public override void FinishTransaction(ProductDefinition product, string transactionId)
		{
		}

		// Token: 0x0600035D RID: 861 RVA: 0x0000DFC0 File Offset: 0x0000C1C0
		public void RegisterPurchaseForRestore(string productId)
		{
			this.m_PurchasedProducts.Add(productId);
		}

		// Token: 0x0600035E RID: 862 RVA: 0x0000DFD0 File Offset: 0x0000C1D0
		protected virtual bool StartUI<T>(object model, FakeStore.DialogType dialogType, Action<bool, T> callback)
		{
			return false;
		}

		// Token: 0x0600035F RID: 863 RVA: 0x0000DFE3 File Offset: 0x0000C1E3
		public FakeStore()
		{
		}

		// Token: 0x06000360 RID: 864 RVA: 0x0000DFFE File Offset: 0x0000C1FE
		[CompilerGenerated]
		[DebuggerHidden]
		private void <>n__0(string id, string receipt, string transactionID)
		{
			base.OnPurchaseSucceeded(id, receipt, transactionID);
		}

		// Token: 0x0400028E RID: 654
		public const string Name = "fake";

		// Token: 0x0400028F RID: 655
		private IStoreCallback m_Biller;

		// Token: 0x04000290 RID: 656
		private List<string> m_PurchasedProducts = new List<string>();

		// Token: 0x04000291 RID: 657
		public bool purchaseCalled;

		// Token: 0x04000292 RID: 658
		public bool restoreCalled;

		// Token: 0x04000293 RID: 659
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private string <unavailableProductId>k__BackingField;

		// Token: 0x04000294 RID: 660
		public FakeStoreUIMode UIMode = FakeStoreUIMode.Default;

		// Token: 0x020000B8 RID: 184
		protected enum DialogType
		{
			// Token: 0x04000296 RID: 662
			Purchase,
			// Token: 0x04000297 RID: 663
			RetrieveProducts
		}

		// Token: 0x020000B9 RID: 185
		[CompilerGenerated]
		private sealed class <>c__DisplayClass13_0
		{
			// Token: 0x06000361 RID: 865 RVA: 0x00002CA1 File Offset: 0x00000EA1
			public <>c__DisplayClass13_0()
			{
			}

			// Token: 0x06000362 RID: 866 RVA: 0x0000E00C File Offset: 0x0000C20C
			internal void <StoreRetrieveProducts>b__0(bool allow, InitializationFailureReason failureReason)
			{
				if (allow)
				{
					this.<>4__this.m_Biller.OnProductsRetrieved(this.products);
					Promo.ProvideProductsToAds(this.<>4__this, this.<>4__this.m_Biller);
				}
				else
				{
					this.<>4__this.m_Biller.OnSetupFailed(failureReason);
				}
			}

			// Token: 0x04000298 RID: 664
			public List<ProductDescription> products;

			// Token: 0x04000299 RID: 665
			public FakeStore <>4__this;
		}

		// Token: 0x020000BA RID: 186
		[CompilerGenerated]
		private sealed class <>c__DisplayClass15_0
		{
			// Token: 0x06000363 RID: 867 RVA: 0x00002CA1 File Offset: 0x00000EA1
			public <>c__DisplayClass15_0()
			{
			}

			// Token: 0x06000364 RID: 868 RVA: 0x0000E068 File Offset: 0x0000C268
			internal void <FakePurchase>b__0(bool allow, PurchaseFailureReason failureReason)
			{
				if (allow)
				{
					this.<>4__this.<>n__0(this.product.storeSpecificId, "{ \"this\" : \"is a fake receipt\" }", Guid.NewGuid().ToString());
				}
				else
				{
					bool flag = failureReason == (PurchaseFailureReason)Enum.Parse(typeof(PurchaseFailureReason), "Unknown");
					if (flag)
					{
						failureReason = PurchaseFailureReason.UserCancelled;
					}
					PurchaseFailureDescription failure = new PurchaseFailureDescription(this.product.storeSpecificId, failureReason, "failed a fake store purchase");
					this.<>4__this.OnPurchaseFailed(failure, null);
				}
			}

			// Token: 0x0400029A RID: 666
			public ProductDefinition product;

			// Token: 0x0400029B RID: 667
			public FakeStore <>4__this;
		}
	}
}
