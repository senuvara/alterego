﻿using System;
using UnityEngine.Purchasing.Extension;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000058 RID: 88
	internal class FakeAppleConfiguation : IAppleConfiguration, IStoreConfiguration
	{
		// Token: 0x17000034 RID: 52
		// (get) Token: 0x06000177 RID: 375 RVA: 0x00007084 File Offset: 0x00005284
		public string appReceipt
		{
			get
			{
				return "This is a fake receipt. When running on an Apple store, a base64 encoded App Receipt would be returned";
			}
		}

		// Token: 0x17000035 RID: 53
		// (get) Token: 0x06000178 RID: 376 RVA: 0x0000709C File Offset: 0x0000529C
		public bool canMakePayments
		{
			get
			{
				return true;
			}
		}

		// Token: 0x06000179 RID: 377 RVA: 0x00002EA8 File Offset: 0x000010A8
		public void SetApplePromotionalPurchaseInterceptorCallback(Action<Product> callback)
		{
		}

		// Token: 0x0600017A RID: 378 RVA: 0x00002CA1 File Offset: 0x00000EA1
		public FakeAppleConfiguation()
		{
		}
	}
}
