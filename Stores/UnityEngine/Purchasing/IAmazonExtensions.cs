﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000015 RID: 21
	public interface IAmazonExtensions : IStoreExtension
	{
		// Token: 0x17000019 RID: 25
		// (get) Token: 0x06000056 RID: 86
		string amazonUserId { get; }

		// Token: 0x06000057 RID: 87
		void NotifyUnableToFulfillUnavailableProduct(string transactionID);
	}
}
