﻿using System;
using UnityEngine.Purchasing.Extension;

namespace UnityEngine.Purchasing
{
	// Token: 0x0200006A RID: 106
	public interface IManagedStoreConfig : IStoreConfiguration
	{
		// Token: 0x17000046 RID: 70
		// (get) Token: 0x060001E0 RID: 480
		// (set) Token: 0x060001E1 RID: 481
		bool disableStoreCatalog { get; set; }

		// Token: 0x17000047 RID: 71
		// (get) Token: 0x060001E2 RID: 482
		// (set) Token: 0x060001E3 RID: 483
		bool? trackingOptOut { get; set; }

		// Token: 0x17000048 RID: 72
		// (get) Token: 0x060001E4 RID: 484
		// (set) Token: 0x060001E5 RID: 485
		bool storeTestEnabled { get; set; }

		// Token: 0x17000049 RID: 73
		// (get) Token: 0x060001E6 RID: 486
		// (set) Token: 0x060001E7 RID: 487
		string baseIapUrl { get; set; }

		// Token: 0x1700004A RID: 74
		// (get) Token: 0x060001E8 RID: 488
		// (set) Token: 0x060001E9 RID: 489
		string baseEventUrl { get; set; }
	}
}
