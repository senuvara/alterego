﻿using System;
using UnityEngine.Purchasing.Extension;

namespace UnityEngine.Purchasing
{
	// Token: 0x020000B1 RID: 177
	internal class FakeTizenStoreConfiguration : ITizenStoreConfiguration, IStoreConfiguration
	{
		// Token: 0x06000348 RID: 840 RVA: 0x00002EA8 File Offset: 0x000010A8
		public void SetGroupId(string group)
		{
		}

		// Token: 0x06000349 RID: 841 RVA: 0x00002CA1 File Offset: 0x00000EA1
		public FakeTizenStoreConfiguration()
		{
		}
	}
}
