﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x0200001F RID: 31
	public enum RequestPayOutState
	{
		// Token: 0x0400003C RID: 60
		RequestPayOutSucceed,
		// Token: 0x0400003D RID: 61
		RequestPayOutNetworkError,
		// Token: 0x0400003E RID: 62
		RequestPayOutFailed,
		// Token: 0x0400003F RID: 63
		NotKnown
	}
}
