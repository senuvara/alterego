﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000039 RID: 57
	internal class SamsungAppsJavaBridge : AndroidJavaProxy, ISamsungAppsCallback
	{
		// Token: 0x060000DC RID: 220 RVA: 0x00004F10 File Offset: 0x00003110
		public SamsungAppsJavaBridge(ISamsungAppsCallback forwardTo) : base("com.unity.purchasing.samsung.ISamsungAppsCallback")
		{
			this.forwardTo = forwardTo;
		}

		// Token: 0x060000DD RID: 221 RVA: 0x00004F26 File Offset: 0x00003126
		public void OnTransactionsRestored(bool result)
		{
			this.forwardTo.OnTransactionsRestored(result);
		}

		// Token: 0x040000AA RID: 170
		private ISamsungAppsCallback forwardTo;
	}
}
