﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Text;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000075 RID: 117
	[AddComponentMenu("")]
	internal class AsyncWebUtil : MonoBehaviour, IAsyncWebUtil
	{
		// Token: 0x0600020D RID: 525 RVA: 0x0000928C File Offset: 0x0000748C
		public void Get(string url, Action<string> responseHandler, Action<string> errorHandler, int maxTimeoutInSeconds = 5)
		{
			WWW request = new WWW(url);
			base.StartCoroutine(this.Process(request, responseHandler, errorHandler, maxTimeoutInSeconds));
		}

		// Token: 0x0600020E RID: 526 RVA: 0x000092B4 File Offset: 0x000074B4
		public void Post(string url, string body, Action<string> responseHandler, Action<string> errorHandler, int maxTimeoutInSeconds = 5)
		{
			Encoding utf = Encoding.UTF8;
			WWW request = new WWW(url, utf.GetBytes(body));
			base.StartCoroutine(this.Process(request, responseHandler, errorHandler, maxTimeoutInSeconds));
		}

		// Token: 0x0600020F RID: 527 RVA: 0x000092E9 File Offset: 0x000074E9
		public void Schedule(Action a, int delayInSeconds)
		{
			base.StartCoroutine(this.DoInvoke(a, delayInSeconds));
		}

		// Token: 0x06000210 RID: 528 RVA: 0x000092FB File Offset: 0x000074FB
		private IEnumerator DoInvoke(Action a, int delayInSeconds)
		{
			yield return new WaitForSeconds((float)delayInSeconds);
			a();
			yield break;
		}

		// Token: 0x06000211 RID: 529 RVA: 0x00009318 File Offset: 0x00007518
		private IEnumerator Process(WWW request, Action<string> responseHandler, Action<string> errorHandler, int maxTimeoutInSeconds)
		{
			float timer = 0f;
			bool hasTimedOut = false;
			while (!request.isDone)
			{
				bool flag = timer > (float)maxTimeoutInSeconds;
				if (flag)
				{
					hasTimedOut = true;
					break;
				}
				timer += Time.deltaTime;
				yield return null;
			}
			bool flag2 = hasTimedOut || !string.IsNullOrEmpty(request.error);
			if (flag2)
			{
				errorHandler(request.error);
			}
			else
			{
				responseHandler(request.text);
			}
			request.Dispose();
			yield break;
		}

		// Token: 0x06000212 RID: 530 RVA: 0x00009344 File Offset: 0x00007544
		public AsyncWebUtil()
		{
		}

		// Token: 0x02000076 RID: 118
		[CompilerGenerated]
		private sealed class <DoInvoke>d__3 : IEnumerator<object>, IEnumerator, IDisposable
		{
			// Token: 0x06000213 RID: 531 RVA: 0x0000934D File Offset: 0x0000754D
			[DebuggerHidden]
			public <DoInvoke>d__3(int <>1__state)
			{
				this.<>1__state = <>1__state;
			}

			// Token: 0x06000214 RID: 532 RVA: 0x00003CB8 File Offset: 0x00001EB8
			[DebuggerHidden]
			void IDisposable.Dispose()
			{
			}

			// Token: 0x06000215 RID: 533 RVA: 0x00009360 File Offset: 0x00007560
			bool IEnumerator.MoveNext()
			{
				int num = this.<>1__state;
				if (num == 0)
				{
					this.<>1__state = -1;
					this.<>2__current = new WaitForSeconds((float)delayInSeconds);
					this.<>1__state = 1;
					return true;
				}
				if (num != 1)
				{
					return false;
				}
				this.<>1__state = -1;
				a();
				return false;
			}

			// Token: 0x1700004C RID: 76
			// (get) Token: 0x06000216 RID: 534 RVA: 0x000093BC File Offset: 0x000075BC
			object IEnumerator<object>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x06000217 RID: 535 RVA: 0x00003E21 File Offset: 0x00002021
			[DebuggerHidden]
			void IEnumerator.Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x1700004D RID: 77
			// (get) Token: 0x06000218 RID: 536 RVA: 0x000093BC File Offset: 0x000075BC
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x04000161 RID: 353
			private int <>1__state;

			// Token: 0x04000162 RID: 354
			private object <>2__current;

			// Token: 0x04000163 RID: 355
			public Action a;

			// Token: 0x04000164 RID: 356
			public int delayInSeconds;

			// Token: 0x04000165 RID: 357
			public AsyncWebUtil <>4__this;
		}

		// Token: 0x02000077 RID: 119
		[CompilerGenerated]
		private sealed class <Process>d__4 : IEnumerator<object>, IEnumerator, IDisposable
		{
			// Token: 0x06000219 RID: 537 RVA: 0x000093C4 File Offset: 0x000075C4
			[DebuggerHidden]
			public <Process>d__4(int <>1__state)
			{
				this.<>1__state = <>1__state;
			}

			// Token: 0x0600021A RID: 538 RVA: 0x00003CB8 File Offset: 0x00001EB8
			[DebuggerHidden]
			void IDisposable.Dispose()
			{
			}

			// Token: 0x0600021B RID: 539 RVA: 0x000093D4 File Offset: 0x000075D4
			bool IEnumerator.MoveNext()
			{
				int num = this.<>1__state;
				if (num != 0)
				{
					if (num != 1)
					{
						return false;
					}
					this.<>1__state = -1;
				}
				else
				{
					this.<>1__state = -1;
					timer = 0f;
					hasTimedOut = false;
				}
				if (!request.isDone)
				{
					bool flag = timer > (float)maxTimeoutInSeconds;
					if (!flag)
					{
						timer += Time.deltaTime;
						this.<>2__current = null;
						this.<>1__state = 1;
						return true;
					}
					hasTimedOut = true;
				}
				bool flag2 = hasTimedOut || !string.IsNullOrEmpty(request.error);
				if (flag2)
				{
					errorHandler(request.error);
				}
				else
				{
					responseHandler(request.text);
				}
				request.Dispose();
				return false;
			}

			// Token: 0x1700004E RID: 78
			// (get) Token: 0x0600021C RID: 540 RVA: 0x000094D2 File Offset: 0x000076D2
			object IEnumerator<object>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x0600021D RID: 541 RVA: 0x00003E21 File Offset: 0x00002021
			[DebuggerHidden]
			void IEnumerator.Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x1700004F RID: 79
			// (get) Token: 0x0600021E RID: 542 RVA: 0x000094D2 File Offset: 0x000076D2
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x04000166 RID: 358
			private int <>1__state;

			// Token: 0x04000167 RID: 359
			private object <>2__current;

			// Token: 0x04000168 RID: 360
			public WWW request;

			// Token: 0x04000169 RID: 361
			public Action<string> responseHandler;

			// Token: 0x0400016A RID: 362
			public Action<string> errorHandler;

			// Token: 0x0400016B RID: 363
			public int maxTimeoutInSeconds;

			// Token: 0x0400016C RID: 364
			public AsyncWebUtil <>4__this;

			// Token: 0x0400016D RID: 365
			private float <timer>5__1;

			// Token: 0x0400016E RID: 366
			private bool <hasTimedOut>5__2;
		}
	}
}
