﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000098 RID: 152
	public enum SubscriptionPeriodUnit
	{
		// Token: 0x04000257 RID: 599
		Day,
		// Token: 0x04000258 RID: 600
		Month,
		// Token: 0x04000259 RID: 601
		Week,
		// Token: 0x0400025A RID: 602
		Year,
		// Token: 0x0400025B RID: 603
		NotAvailable
	}
}
