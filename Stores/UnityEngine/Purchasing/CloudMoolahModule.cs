﻿using System;
using UnityEngine.Purchasing.Extension;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000016 RID: 22
	public class CloudMoolahModule : IPurchasingModule
	{
		// Token: 0x06000058 RID: 88 RVA: 0x00002EC3 File Offset: 0x000010C3
		public void Configure(IPurchasingBinder binder)
		{
			Debug.Log("CloudMoolah Configure");
			binder.RegisterStore("MoolahAppStore", this.InstantiateMoolahAppStore(binder));
		}

		// Token: 0x06000059 RID: 89 RVA: 0x00002EE4 File Offset: 0x000010E4
		private IStore InstantiateMoolahAppStore(IPurchasingBinder binder)
		{
			bool flag = this.IsSupportPlatform();
			IStore result;
			if (flag)
			{
				GameObject gameObject = GameObject.Find("IAPUtil");
				MoolahStoreImpl moolahStoreImpl = gameObject.AddComponent<MoolahStoreImpl>();
				binder.RegisterExtension<IMoolahExtension>(moolahStoreImpl);
				binder.RegisterConfiguration<IMoolahConfiguration>(moolahStoreImpl);
				result = moolahStoreImpl;
			}
			else
			{
				result = null;
			}
			return result;
		}

		// Token: 0x0600005A RID: 90 RVA: 0x00002F28 File Offset: 0x00001128
		private bool IsSupportPlatform()
		{
			return Application.platform == RuntimePlatform.Android;
		}

		// Token: 0x0600005B RID: 91 RVA: 0x00002CA1 File Offset: 0x00000EA1
		public CloudMoolahModule()
		{
		}
	}
}
