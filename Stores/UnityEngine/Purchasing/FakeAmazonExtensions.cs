﻿using System;
using System.Collections.Generic;
using UnityEngine.Purchasing.Extension;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000013 RID: 19
	public class FakeAmazonExtensions : IAmazonExtensions, IStoreExtension, IAmazonConfiguration, IStoreConfiguration
	{
		// Token: 0x06000051 RID: 81 RVA: 0x00002EA8 File Offset: 0x000010A8
		public void WriteSandboxJSON(HashSet<ProductDefinition> products)
		{
		}

		// Token: 0x06000052 RID: 82 RVA: 0x00002EA8 File Offset: 0x000010A8
		public void NotifyUnableToFulfillUnavailableProduct(string transactionID)
		{
		}

		// Token: 0x17000018 RID: 24
		// (get) Token: 0x06000053 RID: 83 RVA: 0x00002EAC File Offset: 0x000010AC
		public string amazonUserId
		{
			get
			{
				return "fakeid";
			}
		}

		// Token: 0x06000054 RID: 84 RVA: 0x00002CA1 File Offset: 0x00000EA1
		public FakeAmazonExtensions()
		{
		}
	}
}
