﻿using System;
using UnityEngine.Purchasing.Extension;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000067 RID: 103
	internal class FakeManagedStoreConfig : IManagedStoreConfig, IStoreConfiguration
	{
		// Token: 0x060001D2 RID: 466 RVA: 0x00008876 File Offset: 0x00006A76
		public FakeManagedStoreConfig()
		{
		}

		// Token: 0x17000040 RID: 64
		// (get) Token: 0x060001D3 RID: 467 RVA: 0x000088A8 File Offset: 0x00006AA8
		// (set) Token: 0x060001D4 RID: 468 RVA: 0x000088C0 File Offset: 0x00006AC0
		public bool disableStoreCatalog
		{
			get
			{
				return this.catalogDisabled;
			}
			set
			{
				this.catalogDisabled = value;
			}
		}

		// Token: 0x17000041 RID: 65
		// (get) Token: 0x060001D5 RID: 469 RVA: 0x000088CC File Offset: 0x00006ACC
		// (set) Token: 0x060001D6 RID: 470 RVA: 0x000088E4 File Offset: 0x00006AE4
		public bool? trackingOptOut
		{
			get
			{
				return this.trackingOptedOut;
			}
			set
			{
				this.trackingOptedOut = value;
			}
		}

		// Token: 0x17000042 RID: 66
		// (get) Token: 0x060001D7 RID: 471 RVA: 0x000088F0 File Offset: 0x00006AF0
		// (set) Token: 0x060001D8 RID: 472 RVA: 0x00008908 File Offset: 0x00006B08
		public bool storeTestEnabled
		{
			get
			{
				return this.testStore;
			}
			set
			{
				bool flag = !this.testStore;
				if (flag)
				{
					this.testStore = value;
				}
			}
		}

		// Token: 0x17000043 RID: 67
		// (get) Token: 0x060001D9 RID: 473 RVA: 0x0000892C File Offset: 0x00006B2C
		// (set) Token: 0x060001DA RID: 474 RVA: 0x00008944 File Offset: 0x00006B44
		public string baseIapUrl
		{
			get
			{
				return this.iapBaseUrl;
			}
			set
			{
				bool flag = this.iapBaseUrl == null && value != null;
				if (flag)
				{
					this.storeTestEnabled = true;
					this.iapBaseUrl = value;
				}
			}
		}

		// Token: 0x17000044 RID: 68
		// (get) Token: 0x060001DB RID: 475 RVA: 0x00008978 File Offset: 0x00006B78
		// (set) Token: 0x060001DC RID: 476 RVA: 0x00008990 File Offset: 0x00006B90
		public string baseEventUrl
		{
			get
			{
				return this.eventBaseUrl;
			}
			set
			{
				this.storeTestEnabled = true;
				this.eventBaseUrl = value;
			}
		}

		// Token: 0x0400012B RID: 299
		private bool catalogDisabled = false;

		// Token: 0x0400012C RID: 300
		private bool testStore = false;

		// Token: 0x0400012D RID: 301
		private string iapBaseUrl = null;

		// Token: 0x0400012E RID: 302
		private string eventBaseUrl = null;

		// Token: 0x0400012F RID: 303
		private bool? trackingOptedOut = null;
	}
}
