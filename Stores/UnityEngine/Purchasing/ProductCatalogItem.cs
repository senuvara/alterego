﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000082 RID: 130
	[Serializable]
	public class ProductCatalogItem
	{
		// Token: 0x06000240 RID: 576 RVA: 0x00009D4F File Offset: 0x00007F4F
		public void AddPayout()
		{
			this.payouts.Add(new ProductCatalogPayout());
		}

		// Token: 0x06000241 RID: 577 RVA: 0x00009D63 File Offset: 0x00007F63
		public void RemovePayout(ProductCatalogPayout payout)
		{
			this.payouts.Remove(payout);
		}

		// Token: 0x17000057 RID: 87
		// (get) Token: 0x06000242 RID: 578 RVA: 0x00009D74 File Offset: 0x00007F74
		public IList<ProductCatalogPayout> Payouts
		{
			get
			{
				return this.payouts;
			}
		}

		// Token: 0x06000243 RID: 579 RVA: 0x00009D8C File Offset: 0x00007F8C
		public ProductCatalogItem Clone()
		{
			ProductCatalogItem productCatalogItem = new ProductCatalogItem();
			productCatalogItem.id = this.id;
			productCatalogItem.type = this.type;
			productCatalogItem.SetStoreIDs(this.allStoreIDs);
			productCatalogItem.defaultDescription = this.defaultDescription.Clone();
			productCatalogItem.screenshotPath = this.screenshotPath;
			productCatalogItem.applePriceTier = this.applePriceTier;
			productCatalogItem.googlePrice.value = this.googlePrice.value;
			productCatalogItem.pricingTemplateID = this.pricingTemplateID;
			foreach (LocalizedProductDescription localizedProductDescription in this.descriptions)
			{
				productCatalogItem.descriptions.Add(localizedProductDescription.Clone());
			}
			return productCatalogItem;
		}

		// Token: 0x06000244 RID: 580 RVA: 0x00009E6C File Offset: 0x0000806C
		public void SetStoreID(string aStore, string aId)
		{
			this.storeIDs.RemoveAll((StoreID obj) => obj.store == aStore);
			bool flag = !string.IsNullOrEmpty(aId);
			if (flag)
			{
				this.storeIDs.Add(new StoreID(aStore, aId));
			}
		}

		// Token: 0x06000245 RID: 581 RVA: 0x00009EC4 File Offset: 0x000080C4
		public string GetStoreID(string store)
		{
			StoreID storeID = this.storeIDs.Find((StoreID obj) => obj.store == store);
			return (storeID == null) ? null : storeID.id;
		}

		// Token: 0x17000058 RID: 88
		// (get) Token: 0x06000246 RID: 582 RVA: 0x00009F08 File Offset: 0x00008108
		public ICollection<StoreID> allStoreIDs
		{
			get
			{
				return this.storeIDs;
			}
		}

		// Token: 0x06000247 RID: 583 RVA: 0x00009F20 File Offset: 0x00008120
		public void SetStoreIDs(ICollection<StoreID> storeIds)
		{
			using (IEnumerator<StoreID> enumerator = storeIds.GetEnumerator())
			{
				while (enumerator.MoveNext())
				{
					StoreID storeId = enumerator.Current;
					this.storeIDs.RemoveAll((StoreID obj) => obj.store == storeId.store);
					bool flag = !string.IsNullOrEmpty(storeId.id);
					if (flag)
					{
						this.storeIDs.Add(new StoreID(storeId.store, storeId.id));
					}
				}
			}
		}

		// Token: 0x06000248 RID: 584 RVA: 0x00009FC8 File Offset: 0x000081C8
		public LocalizedProductDescription GetDescription(TranslationLocale locale)
		{
			return this.descriptions.Find((LocalizedProductDescription obj) => obj.googleLocale == locale);
		}

		// Token: 0x06000249 RID: 585 RVA: 0x0000A000 File Offset: 0x00008200
		public LocalizedProductDescription GetOrCreateDescription(TranslationLocale locale)
		{
			return this.GetDescription(locale) ?? this.AddDescription(locale);
		}

		// Token: 0x0600024A RID: 586 RVA: 0x0000A024 File Offset: 0x00008224
		public LocalizedProductDescription AddDescription(TranslationLocale locale)
		{
			this.RemoveDescription(locale);
			LocalizedProductDescription localizedProductDescription = new LocalizedProductDescription();
			localizedProductDescription.googleLocale = locale;
			this.descriptions.Add(localizedProductDescription);
			return localizedProductDescription;
		}

		// Token: 0x0600024B RID: 587 RVA: 0x0000A05C File Offset: 0x0000825C
		public void RemoveDescription(TranslationLocale locale)
		{
			this.descriptions.RemoveAll((LocalizedProductDescription obj) => obj.googleLocale == locale);
		}

		// Token: 0x17000059 RID: 89
		// (get) Token: 0x0600024C RID: 588 RVA: 0x0000A090 File Offset: 0x00008290
		public bool HasAvailableLocale
		{
			get
			{
				return Enum.GetValues(typeof(TranslationLocale)).Length > this.descriptions.Count + 1;
			}
		}

		// Token: 0x1700005A RID: 90
		// (get) Token: 0x0600024D RID: 589 RVA: 0x0000A0C8 File Offset: 0x000082C8
		public TranslationLocale NextAvailableLocale
		{
			get
			{
				foreach (object obj in Enum.GetValues(typeof(TranslationLocale)))
				{
					TranslationLocale translationLocale = (TranslationLocale)obj;
					bool flag = this.GetDescription(translationLocale) == null && this.defaultDescription.googleLocale != translationLocale;
					if (flag)
					{
						return translationLocale;
					}
				}
				return TranslationLocale.en_US;
			}
		}

		// Token: 0x1700005B RID: 91
		// (get) Token: 0x0600024E RID: 590 RVA: 0x0000A158 File Offset: 0x00008358
		public ICollection<LocalizedProductDescription> translatedDescriptions
		{
			get
			{
				return this.descriptions;
			}
		}

		// Token: 0x0600024F RID: 591 RVA: 0x0000A170 File Offset: 0x00008370
		public ProductCatalogItem()
		{
		}

		// Token: 0x040001AB RID: 427
		public string id;

		// Token: 0x040001AC RID: 428
		public ProductType type;

		// Token: 0x040001AD RID: 429
		[SerializeField]
		private List<StoreID> storeIDs = new List<StoreID>();

		// Token: 0x040001AE RID: 430
		public LocalizedProductDescription defaultDescription = new LocalizedProductDescription();

		// Token: 0x040001AF RID: 431
		public string screenshotPath;

		// Token: 0x040001B0 RID: 432
		public int applePriceTier = 0;

		// Token: 0x040001B1 RID: 433
		public int xiaomiPriceTier = 0;

		// Token: 0x040001B2 RID: 434
		public Price googlePrice = new Price();

		// Token: 0x040001B3 RID: 435
		public string pricingTemplateID;

		// Token: 0x040001B4 RID: 436
		[SerializeField]
		private List<LocalizedProductDescription> descriptions = new List<LocalizedProductDescription>();

		// Token: 0x040001B5 RID: 437
		public Price udpPrice = new Price();

		// Token: 0x040001B6 RID: 438
		[SerializeField]
		private List<ProductCatalogPayout> payouts = new List<ProductCatalogPayout>();

		// Token: 0x02000083 RID: 131
		[CompilerGenerated]
		private sealed class <>c__DisplayClass17_0
		{
			// Token: 0x06000250 RID: 592 RVA: 0x00002CA1 File Offset: 0x00000EA1
			public <>c__DisplayClass17_0()
			{
			}

			// Token: 0x06000251 RID: 593 RVA: 0x0000A1D4 File Offset: 0x000083D4
			internal bool <SetStoreID>b__0(StoreID obj)
			{
				return obj.store == this.aStore;
			}

			// Token: 0x040001B7 RID: 439
			public string aStore;
		}

		// Token: 0x02000084 RID: 132
		[CompilerGenerated]
		private sealed class <>c__DisplayClass18_0
		{
			// Token: 0x06000252 RID: 594 RVA: 0x00002CA1 File Offset: 0x00000EA1
			public <>c__DisplayClass18_0()
			{
			}

			// Token: 0x06000253 RID: 595 RVA: 0x0000A1E7 File Offset: 0x000083E7
			internal bool <GetStoreID>b__0(StoreID obj)
			{
				return obj.store == this.store;
			}

			// Token: 0x040001B8 RID: 440
			public string store;
		}

		// Token: 0x02000085 RID: 133
		[CompilerGenerated]
		private sealed class <>c__DisplayClass21_0
		{
			// Token: 0x06000254 RID: 596 RVA: 0x00002CA1 File Offset: 0x00000EA1
			public <>c__DisplayClass21_0()
			{
			}

			// Token: 0x06000255 RID: 597 RVA: 0x0000A1FA File Offset: 0x000083FA
			internal bool <SetStoreIDs>b__0(StoreID obj)
			{
				return obj.store == this.storeId.store;
			}

			// Token: 0x040001B9 RID: 441
			public StoreID storeId;
		}

		// Token: 0x02000086 RID: 134
		[CompilerGenerated]
		private sealed class <>c__DisplayClass22_0
		{
			// Token: 0x06000256 RID: 598 RVA: 0x00002CA1 File Offset: 0x00000EA1
			public <>c__DisplayClass22_0()
			{
			}

			// Token: 0x06000257 RID: 599 RVA: 0x0000A212 File Offset: 0x00008412
			internal bool <GetDescription>b__0(LocalizedProductDescription obj)
			{
				return obj.googleLocale == this.locale;
			}

			// Token: 0x040001BA RID: 442
			public TranslationLocale locale;
		}

		// Token: 0x02000087 RID: 135
		[CompilerGenerated]
		private sealed class <>c__DisplayClass25_0
		{
			// Token: 0x06000258 RID: 600 RVA: 0x00002CA1 File Offset: 0x00000EA1
			public <>c__DisplayClass25_0()
			{
			}

			// Token: 0x06000259 RID: 601 RVA: 0x0000A222 File Offset: 0x00008422
			internal bool <RemoveDescription>b__0(LocalizedProductDescription obj)
			{
				return obj.googleLocale == this.locale;
			}

			// Token: 0x040001BB RID: 443
			public TranslationLocale locale;
		}
	}
}
