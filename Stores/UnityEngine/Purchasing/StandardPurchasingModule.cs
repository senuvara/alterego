﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.CompilerServices;
using Uniject;
using UnityEngine.Purchasing.Default;
using UnityEngine.Purchasing.Extension;

namespace UnityEngine.Purchasing
{
	// Token: 0x0200008E RID: 142
	public class StandardPurchasingModule : AbstractPurchasingModule, IAndroidStoreSelection, IStoreConfiguration
	{
		// Token: 0x17000076 RID: 118
		// (get) Token: 0x060002B6 RID: 694 RVA: 0x0000B4DC File Offset: 0x000096DC
		// (set) Token: 0x060002B7 RID: 695 RVA: 0x0000B4E4 File Offset: 0x000096E4
		internal IUtil util
		{
			[CompilerGenerated]
			get
			{
				return this.<util>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<util>k__BackingField = value;
			}
		}

		// Token: 0x17000077 RID: 119
		// (get) Token: 0x060002B8 RID: 696 RVA: 0x0000B4ED File Offset: 0x000096ED
		// (set) Token: 0x060002B9 RID: 697 RVA: 0x0000B4F5 File Offset: 0x000096F5
		internal ILogger logger
		{
			[CompilerGenerated]
			get
			{
				return this.<logger>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<logger>k__BackingField = value;
			}
		}

		// Token: 0x17000078 RID: 120
		// (get) Token: 0x060002BA RID: 698 RVA: 0x0000B4FE File Offset: 0x000096FE
		// (set) Token: 0x060002BB RID: 699 RVA: 0x0000B506 File Offset: 0x00009706
		internal IAsyncWebUtil webUtil
		{
			[CompilerGenerated]
			get
			{
				return this.<webUtil>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<webUtil>k__BackingField = value;
			}
		}

		// Token: 0x17000079 RID: 121
		// (get) Token: 0x060002BC RID: 700 RVA: 0x0000B50F File Offset: 0x0000970F
		// (set) Token: 0x060002BD RID: 701 RVA: 0x0000B517 File Offset: 0x00009717
		internal StandardPurchasingModule.StoreInstance storeInstance
		{
			[CompilerGenerated]
			get
			{
				return this.<storeInstance>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<storeInstance>k__BackingField = value;
			}
		}

		// Token: 0x060002BE RID: 702 RVA: 0x0000B520 File Offset: 0x00009720
		internal StandardPurchasingModule(IUtil util, IAsyncWebUtil webUtil, ILogger logger, INativeStoreProvider nativeStoreProvider, RuntimePlatform platform, AppStore android, bool useCloudCatalog)
		{
			this.util = util;
			this.webUtil = webUtil;
			this.logger = logger;
			this.m_NativeStoreProvider = nativeStoreProvider;
			this.m_RuntimePlatform = platform;
			this.useFakeStoreUIMode = FakeStoreUIMode.Default;
			this.useFakeStoreAlways = false;
			this.m_AppStorePlatform = android;
			this.m_UseCloudCatalog = useCloudCatalog;
			Promo.InitPromo(platform, logger, "1.22.0", util, webUtil);
		}

		// Token: 0x1700007A RID: 122
		// (get) Token: 0x060002BF RID: 703 RVA: 0x0000B590 File Offset: 0x00009790
		[Obsolete("Use StandardPurchasingModule.appStore instead")]
		public AndroidStore androidStore
		{
			get
			{
				AndroidStore result = AndroidStore.NotSpecified;
				try
				{
					result = (AndroidStore)Enum.Parse(typeof(AndroidStore), this.m_AppStorePlatform.ToString());
				}
				catch (Exception)
				{
				}
				return result;
			}
		}

		// Token: 0x1700007B RID: 123
		// (get) Token: 0x060002C0 RID: 704 RVA: 0x0000B5E4 File Offset: 0x000097E4
		public AppStore appStore
		{
			get
			{
				return this.m_AppStorePlatform;
			}
		}

		// Token: 0x1700007C RID: 124
		// (get) Token: 0x060002C1 RID: 705 RVA: 0x0000B5FC File Offset: 0x000097FC
		// (set) Token: 0x060002C2 RID: 706 RVA: 0x0000B614 File Offset: 0x00009814
		[Obsolete("Use IMicrosoftConfiguration to toggle use of the Microsoft IAP simulator.")]
		public bool useMockBillingSystem
		{
			get
			{
				return this.usingMockMicrosoft;
			}
			set
			{
				this.UseMockWindowsStore(value);
				this.usingMockMicrosoft = value;
			}
		}

		// Token: 0x1700007D RID: 125
		// (get) Token: 0x060002C3 RID: 707 RVA: 0x0000B626 File Offset: 0x00009826
		// (set) Token: 0x060002C4 RID: 708 RVA: 0x0000B62E File Offset: 0x0000982E
		public FakeStoreUIMode useFakeStoreUIMode
		{
			[CompilerGenerated]
			get
			{
				return this.<useFakeStoreUIMode>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<useFakeStoreUIMode>k__BackingField = value;
			}
		}

		// Token: 0x1700007E RID: 126
		// (get) Token: 0x060002C5 RID: 709 RVA: 0x0000B637 File Offset: 0x00009837
		// (set) Token: 0x060002C6 RID: 710 RVA: 0x0000B63F File Offset: 0x0000983F
		public bool useFakeStoreAlways
		{
			[CompilerGenerated]
			get
			{
				return this.<useFakeStoreAlways>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<useFakeStoreAlways>k__BackingField = value;
			}
		}

		// Token: 0x060002C7 RID: 711 RVA: 0x0000B648 File Offset: 0x00009848
		public static StandardPurchasingModule Instance()
		{
			return StandardPurchasingModule.Instance(AppStore.NotSpecified);
		}

		// Token: 0x060002C8 RID: 712 RVA: 0x0000B660 File Offset: 0x00009860
		[Obsolete("Use StandardPurchasingModule.Instance(AppStore) instead")]
		public static StandardPurchasingModule Instance(AndroidStore androidStore)
		{
			AppStore androidStore2 = AppStore.NotSpecified;
			try
			{
				androidStore2 = (AppStore)Enum.Parse(typeof(AppStore), androidStore.ToString());
			}
			catch (Exception)
			{
			}
			return StandardPurchasingModule.Instance(androidStore2);
		}

		// Token: 0x060002C9 RID: 713 RVA: 0x0000B6B8 File Offset: 0x000098B8
		public static StandardPurchasingModule Instance(AppStore androidStore)
		{
			bool flag = StandardPurchasingModule.ModuleInstance == null;
			if (flag)
			{
				ILogger unityLogger = Debug.unityLogger;
				unityLogger.Log("UnityIAP Version: 1.22.0");
				GameObject gameObject = new GameObject("IAPUtil");
				Object.DontDestroyOnLoad(gameObject);
				gameObject.hideFlags = (HideFlags.HideInHierarchy | HideFlags.HideInInspector);
				UnityUtil util = gameObject.AddComponent<UnityUtil>();
				AsyncWebUtil webUtil = gameObject.AddComponent<AsyncWebUtil>();
				TextAsset textAsset = Resources.Load("BillingMode") as TextAsset;
				StoreConfiguration storeConfiguration = null;
				bool flag2 = null != textAsset;
				if (flag2)
				{
					storeConfiguration = StoreConfiguration.Deserialize(textAsset.text);
				}
				bool flag3 = androidStore == AppStore.NotSpecified;
				if (flag3)
				{
					androidStore = AppStore.GooglePlay;
					bool flag4 = storeConfiguration != null;
					if (flag4)
					{
						AppStore androidStore2 = storeConfiguration.androidStore;
						bool flag5 = androidStore2 > AppStore.NotSpecified;
						if (flag5)
						{
							androidStore = androidStore2;
						}
					}
				}
				StandardPurchasingModule.ModuleInstance = new StandardPurchasingModule(util, webUtil, unityLogger, new NativeStoreProvider(), Application.platform, androidStore, false);
			}
			return StandardPurchasingModule.ModuleInstance;
		}

		// Token: 0x060002CA RID: 714 RVA: 0x0000B7A0 File Offset: 0x000099A0
		public override void Configure()
		{
			base.BindConfiguration<IGooglePlayConfiguration>(new FakeGooglePlayConfiguration());
			base.BindConfiguration<IAppleConfiguration>(new FakeAppleConfiguation());
			base.BindExtension<IAppleExtensions>(new FakeAppleExtensions());
			base.BindConfiguration<IAmazonConfiguration>(new FakeAmazonExtensions());
			base.BindExtension<IAmazonExtensions>(new FakeAmazonExtensions());
			base.BindConfiguration<ISamsungAppsConfiguration>(new FakeSamsungAppsExtensions());
			base.BindExtension<ISamsungAppsExtensions>(new FakeSamsungAppsExtensions());
			base.BindConfiguration<IGooglePlayConfiguration>(new FakeGooglePlayStoreExtensions());
			base.BindExtension<IGooglePlayStoreExtensions>(new FakeGooglePlayStoreExtensions());
			base.BindConfiguration<IMoolahConfiguration>(new FakeMoolahConfiguration());
			base.BindExtension<IMoolahExtension>(new FakeMoolahExtensions());
			base.BindConfiguration<IUnityChannelConfiguration>(new FakeUnityChannelConfiguration());
			base.BindExtension<IUnityChannelExtensions>(new FakeUnityChannelExtensions());
			base.BindConfiguration<IMicrosoftConfiguration>(new StandardPurchasingModule.MicrosoftConfiguration(this));
			base.BindExtension<IMicrosoftExtensions>(new FakeMicrosoftExtensions());
			base.BindConfiguration<ITizenStoreConfiguration>(new FakeTizenStoreConfiguration());
			base.BindConfiguration<IAndroidStoreSelection>(this);
			base.BindConfiguration<IManagedStoreConfig>(new FakeManagedStoreConfig());
			base.BindExtension<IManagedStoreExtensions>(new FakeManagedStoreExtensions());
			base.BindExtension<IUDPExtensions>(new FakeUDPExtension());
			base.BindExtension<ITransactionHistoryExtensions>(new FakeTransactionHistoryExtensions());
			bool flag = this.storeInstance == null;
			if (flag)
			{
				this.storeInstance = this.InstantiateStore();
			}
			base.RegisterStore(this.storeInstance.storeName, this.storeInstance.instance);
			bool useCloudCatalog = this.m_UseCloudCatalog;
			if (useCloudCatalog)
			{
				MethodInfo method = this.m_Binder.GetType().GetMethod("SetCatalogProviderFunction");
				bool flag2 = method != null;
				if (flag2)
				{
					this.m_CloudCatalog = CloudCatalogImpl.CreateInstance(this.storeInstance.storeName);
					Action<Action<HashSet<ProductDefinition>>> action = delegate(Action<HashSet<ProductDefinition>> callback)
					{
						MethodInfo method2 = typeof(CloudCatalogImpl).GetMethod("FetchProducts");
						bool flag6 = method2 != null;
						if (flag6)
						{
							method2.Invoke(this.m_CloudCatalog, new object[]
							{
								callback
							});
						}
						else
						{
							callback(new HashSet<ProductDefinition>());
						}
					};
					method.Invoke(this.m_Binder, new object[]
					{
						action
					});
				}
			}
			IStoreInternal storeInternal = this.storeInstance.instance as IStoreInternal;
			bool flag3 = storeInternal != null;
			if (flag3)
			{
				storeInternal.SetModule(this);
			}
			IManagedStoreExtensions managedStoreExtensions = this.storeInstance.instance as IManagedStoreExtensions;
			bool flag4 = managedStoreExtensions != null;
			if (flag4)
			{
				base.BindExtension<IManagedStoreExtensions>(managedStoreExtensions);
			}
			bool flag5 = this.util != null && this.util.IsClassOrSubclass(typeof(JSONStore), this.storeInstance.instance.GetType());
			if (flag5)
			{
				JSONStore instance = (JSONStore)this.storeInstance.instance;
				base.BindExtension<ITransactionHistoryExtensions>(instance);
			}
		}

		// Token: 0x060002CB RID: 715 RVA: 0x0000B9EC File Offset: 0x00009BEC
		private StandardPurchasingModule.StoreInstance InstantiateStore()
		{
			bool useFakeStoreAlways = this.useFakeStoreAlways;
			StandardPurchasingModule.StoreInstance result;
			if (useFakeStoreAlways)
			{
				result = new StandardPurchasingModule.StoreInstance("fake", this.InstantiateFakeStore());
			}
			else
			{
				RuntimePlatform runtimePlatform = this.m_RuntimePlatform;
				if (runtimePlatform <= RuntimePlatform.IPhonePlayer)
				{
					if (runtimePlatform == RuntimePlatform.OSXPlayer)
					{
						return new StandardPurchasingModule.StoreInstance("MacAppStore", this.InstantiateApple());
					}
					if (runtimePlatform == RuntimePlatform.WindowsPlayer)
					{
						goto IL_165;
					}
					if (runtimePlatform != RuntimePlatform.IPhonePlayer)
					{
						goto IL_187;
					}
				}
				else
				{
					if (runtimePlatform == RuntimePlatform.Android)
					{
						switch (this.m_AppStorePlatform)
						{
						case AppStore.CloudMoolah:
							return new StandardPurchasingModule.StoreInstance("MoolahAppStore", this.InstantiateCloudMoolah());
						case AppStore.XiaomiMiPay:
							return new StandardPurchasingModule.StoreInstance(StandardPurchasingModule.AndroidStoreNameMap[this.m_AppStorePlatform], this.InstantiateUnityChannel());
						case AppStore.UDP:
							return new StandardPurchasingModule.StoreInstance(StandardPurchasingModule.AndroidStoreNameMap[this.m_AppStorePlatform], this.InstantiateUDP());
						}
						return new StandardPurchasingModule.StoreInstance(StandardPurchasingModule.AndroidStoreNameMap[this.m_AppStorePlatform], this.InstantiateAndroid());
					}
					switch (runtimePlatform)
					{
					case RuntimePlatform.WebGLPlayer:
						goto IL_165;
					case RuntimePlatform.MetroPlayerX86:
					case RuntimePlatform.MetroPlayerX64:
					case RuntimePlatform.MetroPlayerARM:
						return new StandardPurchasingModule.StoreInstance("WinRT", this.instantiateWindowsStore());
					case RuntimePlatform.WP8Player:
					case RuntimePlatform.BlackBerryPlayer:
						goto IL_187;
					case RuntimePlatform.TizenPlayer:
						return new StandardPurchasingModule.StoreInstance("TizenStore", this.InstantiateTizen());
					default:
						if (runtimePlatform != RuntimePlatform.tvOS)
						{
							goto IL_187;
						}
						break;
					}
				}
				return new StandardPurchasingModule.StoreInstance("AppleAppStore", this.InstantiateApple());
				IL_165:
				IStore store = this.InstantiateFacebook();
				bool flag = store != null;
				if (flag)
				{
					return new StandardPurchasingModule.StoreInstance("FacebookStore", store);
				}
				IL_187:
				result = new StandardPurchasingModule.StoreInstance("fake", this.InstantiateFakeStore());
			}
			return result;
		}

		// Token: 0x060002CC RID: 716 RVA: 0x0000BB94 File Offset: 0x00009D94
		private IStore InstantiateAndroid()
		{
			JSONStore store = new JSONStore();
			return this.InstantiateAndroidHelper(store);
		}

		// Token: 0x060002CD RID: 717 RVA: 0x0000BBB4 File Offset: 0x00009DB4
		private IStore InstantiateUnityChannel()
		{
			UnityChannelImpl unityChannelImpl = new UnityChannelImpl();
			base.BindExtension<IUnityChannelExtensions>(unityChannelImpl);
			INativeUnityChannelStore nativeStore = (INativeUnityChannelStore)this.GetAndroidNativeStore(unityChannelImpl);
			unityChannelImpl.SetNativeStore(nativeStore);
			return unityChannelImpl;
		}

		// Token: 0x060002CE RID: 718 RVA: 0x0000BBEC File Offset: 0x00009DEC
		private IStore InstantiateUDP()
		{
			UDPImpl udpimpl = new UDPImpl();
			base.BindExtension<IUDPExtensions>(udpimpl);
			INativeUDPStore nativeStore = (INativeUDPStore)this.GetAndroidNativeStore(udpimpl);
			udpimpl.SetNativeStore(nativeStore);
			return udpimpl;
		}

		// Token: 0x060002CF RID: 719 RVA: 0x0000BC24 File Offset: 0x00009E24
		private IStore InstantiateAndroidHelper(JSONStore store)
		{
			store.SetNativeStore(this.GetAndroidNativeStore(store));
			return store;
		}

		// Token: 0x060002D0 RID: 720 RVA: 0x0000BC48 File Offset: 0x00009E48
		private INativeStore GetAndroidNativeStore(JSONStore store)
		{
			return this.m_NativeStoreProvider.GetAndroidStore(store, this.m_AppStorePlatform, this.m_Binder, this.util);
		}

		// Token: 0x060002D1 RID: 721 RVA: 0x0000BC78 File Offset: 0x00009E78
		private IStore InstantiateCloudMoolah()
		{
			GameObject gameObject = GameObject.Find("IAPUtil");
			MoolahStoreImpl moolahStoreImpl = gameObject.AddComponent<MoolahStoreImpl>();
			base.BindConfiguration<IMoolahConfiguration>(moolahStoreImpl);
			base.BindExtension<IMoolahExtension>(moolahStoreImpl);
			return moolahStoreImpl;
		}

		// Token: 0x060002D2 RID: 722 RVA: 0x0000BCB0 File Offset: 0x00009EB0
		private IStore InstantiateApple()
		{
			AppleStoreImpl appleStoreImpl = new AppleStoreImpl(this.util);
			INativeAppleStore storekit = this.m_NativeStoreProvider.GetStorekit(appleStoreImpl);
			appleStoreImpl.SetNativeStore(storekit);
			base.BindExtension<IAppleExtensions>(appleStoreImpl);
			return appleStoreImpl;
		}

		// Token: 0x060002D3 RID: 723 RVA: 0x0000BCEC File Offset: 0x00009EEC
		private void UseMockWindowsStore(bool value)
		{
			bool flag = this.windowsStore != null;
			if (flag)
			{
				IWindowsIAP windowsIAP = Factory.Create(value);
				this.windowsStore.SetWindowsIAP(windowsIAP);
			}
		}

		// Token: 0x060002D4 RID: 724 RVA: 0x0000BD20 File Offset: 0x00009F20
		private IStore instantiateWindowsStore()
		{
			IWindowsIAP win = Factory.Create(false);
			this.windowsStore = new WinRTStore(win, this.util, this.logger);
			this.util.AddPauseListener(new Action<bool>(this.windowsStore.restoreTransactions));
			return this.windowsStore;
		}

		// Token: 0x060002D5 RID: 725 RVA: 0x0000BD74 File Offset: 0x00009F74
		private IStore InstantiateTizen()
		{
			TizenStoreImpl tizenStoreImpl = new TizenStoreImpl(this.util);
			tizenStoreImpl.SetNativeStore(this.m_NativeStoreProvider.GetTizenStore(tizenStoreImpl, this.m_Binder));
			base.BindConfiguration<ITizenStoreConfiguration>(tizenStoreImpl);
			return tizenStoreImpl;
		}

		// Token: 0x060002D6 RID: 726 RVA: 0x0000BDB4 File Offset: 0x00009FB4
		private IStore InstantiateFacebook()
		{
			INativeFacebookStore facebookStore = this.m_NativeStoreProvider.GetFacebookStore();
			bool flag = facebookStore.Check();
			IStore result;
			if (flag)
			{
				FacebookStoreImpl facebookStoreImpl = new FacebookStoreImpl(this.util);
				facebookStoreImpl.SetNativeStore(facebookStore);
				result = facebookStoreImpl;
			}
			else
			{
				result = null;
			}
			return result;
		}

		// Token: 0x060002D7 RID: 727 RVA: 0x0000BDF8 File Offset: 0x00009FF8
		private IStore InstantiateFakeStore()
		{
			FakeStore fakeStore = null;
			bool flag = this.useFakeStoreUIMode > FakeStoreUIMode.Default;
			if (flag)
			{
				fakeStore = new UIFakeStore();
				fakeStore.UIMode = this.useFakeStoreUIMode;
			}
			bool flag2 = fakeStore == null;
			if (flag2)
			{
				fakeStore = new FakeStore();
			}
			return fakeStore;
		}

		// Token: 0x060002D8 RID: 728 RVA: 0x0000BE40 File Offset: 0x0000A040
		// Note: this type is marked as 'beforefieldinit'.
		static StandardPurchasingModule()
		{
		}

		// Token: 0x060002D9 RID: 729 RVA: 0x0000BEB4 File Offset: 0x0000A0B4
		[CompilerGenerated]
		private void <Configure>b__45_0(Action<HashSet<ProductDefinition>> callback)
		{
			MethodInfo method = typeof(CloudCatalogImpl).GetMethod("FetchProducts");
			bool flag = method != null;
			if (flag)
			{
				method.Invoke(this.m_CloudCatalog, new object[]
				{
					callback
				});
			}
			else
			{
				callback(new HashSet<ProductDefinition>());
			}
		}

		// Token: 0x04000200 RID: 512
		public const string k_PackageVersion = "1.22.0";

		// Token: 0x04000201 RID: 513
		private AppStore m_AppStorePlatform;

		// Token: 0x04000202 RID: 514
		private INativeStoreProvider m_NativeStoreProvider;

		// Token: 0x04000203 RID: 515
		private RuntimePlatform m_RuntimePlatform;

		// Token: 0x04000204 RID: 516
		private bool m_UseCloudCatalog;

		// Token: 0x04000205 RID: 517
		private static StandardPurchasingModule ModuleInstance;

		// Token: 0x04000206 RID: 518
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private IUtil <util>k__BackingField;

		// Token: 0x04000207 RID: 519
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private ILogger <logger>k__BackingField;

		// Token: 0x04000208 RID: 520
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private IAsyncWebUtil <webUtil>k__BackingField;

		// Token: 0x04000209 RID: 521
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private StandardPurchasingModule.StoreInstance <storeInstance>k__BackingField;

		// Token: 0x0400020A RID: 522
		private static Dictionary<AppStore, string> AndroidStoreNameMap = new Dictionary<AppStore, string>
		{
			{
				AppStore.AmazonAppStore,
				"AmazonApps"
			},
			{
				AppStore.GooglePlay,
				"GooglePlay"
			},
			{
				AppStore.SamsungApps,
				"SamsungApps"
			},
			{
				AppStore.CloudMoolah,
				"MoolahAppStore"
			},
			{
				AppStore.XiaomiMiPay,
				"XiaomiMiPay"
			},
			{
				AppStore.UDP,
				UDP.Name
			},
			{
				AppStore.NotSpecified,
				"GooglePlay"
			}
		};

		// Token: 0x0400020B RID: 523
		private CloudCatalogImpl m_CloudCatalog;

		// Token: 0x0400020C RID: 524
		private bool usingMockMicrosoft;

		// Token: 0x0400020D RID: 525
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private FakeStoreUIMode <useFakeStoreUIMode>k__BackingField;

		// Token: 0x0400020E RID: 526
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private bool <useFakeStoreAlways>k__BackingField;

		// Token: 0x0400020F RID: 527
		private WinRTStore windowsStore;

		// Token: 0x0200008F RID: 143
		internal class StoreInstance
		{
			// Token: 0x1700007F RID: 127
			// (get) Token: 0x060002DA RID: 730 RVA: 0x0000BF08 File Offset: 0x0000A108
			internal string storeName
			{
				[CompilerGenerated]
				get
				{
					return this.<storeName>k__BackingField;
				}
			}

			// Token: 0x17000080 RID: 128
			// (get) Token: 0x060002DB RID: 731 RVA: 0x0000BF10 File Offset: 0x0000A110
			internal IStore instance
			{
				[CompilerGenerated]
				get
				{
					return this.<instance>k__BackingField;
				}
			}

			// Token: 0x060002DC RID: 732 RVA: 0x0000BF18 File Offset: 0x0000A118
			internal StoreInstance(string name, IStore instance)
			{
				this.storeName = name;
				this.instance = instance;
			}

			// Token: 0x04000210 RID: 528
			[CompilerGenerated]
			[DebuggerBrowsable(DebuggerBrowsableState.Never)]
			private readonly string <storeName>k__BackingField;

			// Token: 0x04000211 RID: 529
			[CompilerGenerated]
			[DebuggerBrowsable(DebuggerBrowsableState.Never)]
			private readonly IStore <instance>k__BackingField;
		}

		// Token: 0x02000090 RID: 144
		private class MicrosoftConfiguration : IMicrosoftConfiguration, IStoreConfiguration
		{
			// Token: 0x060002DD RID: 733 RVA: 0x0000BF30 File Offset: 0x0000A130
			public MicrosoftConfiguration(StandardPurchasingModule module)
			{
				this.module = module;
			}

			// Token: 0x17000081 RID: 129
			// (get) Token: 0x060002DE RID: 734 RVA: 0x0000BF44 File Offset: 0x0000A144
			// (set) Token: 0x060002DF RID: 735 RVA: 0x0000BF5C File Offset: 0x0000A15C
			public bool useMockBillingSystem
			{
				get
				{
					return this.useMock;
				}
				set
				{
					this.module.UseMockWindowsStore(value);
					this.useMock = value;
				}
			}

			// Token: 0x04000212 RID: 530
			private bool useMock;

			// Token: 0x04000213 RID: 531
			private StandardPurchasingModule module;
		}
	}
}
