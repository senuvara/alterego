﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000030 RID: 48
	internal class GooglePlayStoreCallback : AndroidJavaProxy
	{
		// Token: 0x060000C7 RID: 199 RVA: 0x00004DAD File Offset: 0x00002FAD
		public GooglePlayStoreCallback(Action<bool> callback) : base("com.unity.purchasing.googleplay.IGooglePlayStoreCallback")
		{
			this.callback = callback;
		}

		// Token: 0x060000C8 RID: 200 RVA: 0x00004DC4 File Offset: 0x00002FC4
		public void OnTransactionsRestored(bool result)
		{
			bool flag = this.callback != null;
			if (flag)
			{
				this.callback(result);
			}
		}

		// Token: 0x040000A7 RID: 167
		private Action<bool> callback;
	}
}
