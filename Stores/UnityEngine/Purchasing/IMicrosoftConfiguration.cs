﻿using System;
using UnityEngine.Purchasing.Extension;

namespace UnityEngine.Purchasing
{
	// Token: 0x020000A3 RID: 163
	public interface IMicrosoftConfiguration : IStoreConfiguration
	{
		// Token: 0x17000083 RID: 131
		// (get) Token: 0x0600031D RID: 797
		// (set) Token: 0x0600031E RID: 798
		bool useMockBillingSystem { get; set; }
	}
}
