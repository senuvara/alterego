﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x020000B2 RID: 178
	public class WindowsPhone8
	{
		// Token: 0x0600034A RID: 842 RVA: 0x00002CA1 File Offset: 0x00000EA1
		public WindowsPhone8()
		{
		}

		// Token: 0x04000281 RID: 641
		[Obsolete("Use WindowsStore.Name for Universal Windows Builds")]
		public const string Name = "WinRT";
	}
}
