﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x0200005E RID: 94
	public enum AppStore
	{
		// Token: 0x040000F6 RID: 246
		NotSpecified,
		// Token: 0x040000F7 RID: 247
		GooglePlay,
		// Token: 0x040000F8 RID: 248
		AmazonAppStore,
		// Token: 0x040000F9 RID: 249
		CloudMoolah,
		// Token: 0x040000FA RID: 250
		SamsungApps,
		// Token: 0x040000FB RID: 251
		XiaomiMiPay,
		// Token: 0x040000FC RID: 252
		UDP,
		// Token: 0x040000FD RID: 253
		MacAppStore,
		// Token: 0x040000FE RID: 254
		AppleAppStore,
		// Token: 0x040000FF RID: 255
		WinRT,
		// Token: 0x04000100 RID: 256
		TizenStore,
		// Token: 0x04000101 RID: 257
		FacebookStore,
		// Token: 0x04000102 RID: 258
		fake
	}
}
