﻿using System;
using UnityEngine.UDP;

namespace UnityEngine.Purchasing
{
	// Token: 0x0200004A RID: 74
	public class FakeUDPExtension : IUDPExtensions, IStoreExtension
	{
		// Token: 0x0600011F RID: 287 RVA: 0x00005A38 File Offset: 0x00003C38
		public UserInfo GetUserInfo()
		{
			return new UserInfo
			{
				Channel = "Fake_Channel",
				UserId = "Fake_User_Id_123456",
				UserLoginToken = "Fake_Login_Token"
			};
		}

		// Token: 0x06000120 RID: 288 RVA: 0x00005A78 File Offset: 0x00003C78
		public string GetLastInitializationError()
		{
			return "Fake Initialization Error";
		}

		// Token: 0x06000121 RID: 289 RVA: 0x00005A90 File Offset: 0x00003C90
		public string GetLastPurchaseError()
		{
			return "Fake Purchase Error";
		}

		// Token: 0x06000122 RID: 290 RVA: 0x00005AA7 File Offset: 0x00003CA7
		public void EnableDebugLog(bool enable)
		{
		}

		// Token: 0x06000123 RID: 291 RVA: 0x00002CA1 File Offset: 0x00000EA1
		public FakeUDPExtension()
		{
		}
	}
}
