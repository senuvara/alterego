﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x020000A2 RID: 162
	internal class FakeMicrosoftExtensions : IMicrosoftExtensions, IStoreExtension
	{
		// Token: 0x0600031B RID: 795 RVA: 0x00005AA7 File Offset: 0x00003CA7
		public void RestoreTransactions()
		{
		}

		// Token: 0x0600031C RID: 796 RVA: 0x00002CA1 File Offset: 0x00000EA1
		public FakeMicrosoftExtensions()
		{
		}
	}
}
