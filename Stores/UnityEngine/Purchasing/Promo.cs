﻿using System;
using System.Collections.Generic;
using Uniject;
using UnityEngine.Purchasing.Extension;
using UnityEngine.Purchasing.MiniJSON;
using UnityEngine.Scripting;

namespace UnityEngine.Purchasing
{
	// Token: 0x0200008D RID: 141
	public class Promo
	{
		// Token: 0x060002A9 RID: 681 RVA: 0x0000ACCC File Offset: 0x00008ECC
		[Preserve]
		public static bool IsReady()
		{
			return Promo.s_IsReady;
		}

		// Token: 0x060002AA RID: 682 RVA: 0x0000ACE4 File Offset: 0x00008EE4
		[Preserve]
		public static string Version()
		{
			return Promo.s_Version;
		}

		// Token: 0x060002AB RID: 683 RVA: 0x0000ACFB File Offset: 0x00008EFB
		public Promo()
		{
		}

		// Token: 0x060002AC RID: 684 RVA: 0x0000AD05 File Offset: 0x00008F05
		internal static void InitPromo(RuntimePlatform platform, ILogger logger, IUtil util, IAsyncWebUtil webUtil)
		{
			Promo.InitPromo(platform, logger, "Unknown", util, webUtil);
		}

		// Token: 0x060002AD RID: 685 RVA: 0x0000AD18 File Offset: 0x00008F18
		internal static void InitPromo(RuntimePlatform platform, ILogger logger, string version, IUtil util, IAsyncWebUtil webUtil)
		{
			Promo.s_RuntimePlatform = platform;
			bool flag = logger != null;
			if (flag)
			{
				Promo.s_Logger = logger;
				Promo.s_Version = version;
				Promo.s_Util = util;
				Promo.s_WebUtil = webUtil;
				return;
			}
			throw new ArgumentException("UnityIAP: Promo initialized with null logger!");
		}

		// Token: 0x060002AE RID: 686 RVA: 0x0000AD60 File Offset: 0x00008F60
		private static HashSet<Product> UpdatePromoProductList()
		{
			bool flag = Promo.s_Unity == null || Promo.s_Unity.products == null;
			HashSet<Product> result;
			if (flag)
			{
				Promo.s_Logger.LogError("UnityIAP Promo", "Trying to update list without manager or products ready");
				result = null;
			}
			else
			{
				HashSet<Product> hashSet = new HashSet<Product>();
				Product[] all = Promo.s_Unity.products.all;
				foreach (Product product in all)
				{
					bool flag2 = product.availableToPurchase && (product.definition.type == ProductType.Consumable || string.IsNullOrEmpty(product.transactionID));
					if (flag2)
					{
						hashSet.Add(product);
					}
				}
				result = ((hashSet.Count > 0) ? hashSet : null);
			}
			return result;
		}

		// Token: 0x060002AF RID: 687 RVA: 0x0000AE28 File Offset: 0x00009028
		internal static void ProvideProductsToAds(JSONStore purchaser, IStoreCallback manager)
		{
			bool flag = purchaser == null || manager == null;
			if (flag)
			{
				Promo.s_Logger.LogError("UnityIAP Promo", "Attempt to set promo products without a valid purchaser!");
			}
			else
			{
				Promo.s_PromoPurchaser = purchaser;
				Promo.s_Unity = manager;
				Promo.ProvideProductsToAds(Promo.UpdatePromoProductList());
			}
		}

		// Token: 0x060002B0 RID: 688 RVA: 0x0000AE74 File Offset: 0x00009074
		private static void ProvideProductsToAds(HashSet<Product> productsForAds)
		{
			List<Dictionary<string, object>> list = new List<Dictionary<string, object>>();
			bool flag = productsForAds != null;
			if (flag)
			{
				foreach (Product product in productsForAds)
				{
					list.Add(new Dictionary<string, object>
					{
						{
							"productId",
							product.definition.id
						},
						{
							"iapProductId",
							product.definition.id
						},
						{
							"productType",
							product.definition.type.ToString()
						},
						{
							"localizedPriceString",
							product.metadata.localizedPriceString
						},
						{
							"localizedTitle",
							product.metadata.localizedTitle
						},
						{
							"imageUrl",
							null
						},
						{
							"isoCurrencyCode",
							product.metadata.isoCurrencyCode
						},
						{
							"localizedPrice",
							product.metadata.localizedPrice
						}
					});
				}
			}
			else
			{
				Promo.s_Logger.Log("UnityIAP Promo", "Clearing promo product metadata");
			}
			Promo.s_ProductJSON = Json.Serialize(list);
			bool flag2 = list.Count > 0;
			if (flag2)
			{
				Promo.s_IsReady = true;
				Promo.s_Logger.Log("UnityIAP: Promo interface is available for " + list.Count + " items");
				EventQueue eventQueue = EventQueue.Instance(Promo.s_Util, Promo.s_WebUtil);
				eventQueue.SendEvent(EventDestType.AdsIPC, "{\"type\":\"CatalogUpdated\"}", null, null);
			}
		}

		// Token: 0x060002B1 RID: 689 RVA: 0x0000B040 File Offset: 0x00009240
		[Preserve]
		public static string QueryPromoProducts()
		{
			return Promo.s_ProductJSON;
		}

		// Token: 0x060002B2 RID: 690 RVA: 0x0000B058 File Offset: 0x00009258
		[Preserve]
		public static bool InitiatePromoPurchase(string itemRequest)
		{
			return Promo.InitiatePurchasingCommand(itemRequest);
		}

		// Token: 0x060002B3 RID: 691 RVA: 0x0000B070 File Offset: 0x00009270
		[Preserve]
		public static bool InitiatePurchasingCommand(string command)
		{
			bool flag = string.IsNullOrEmpty(command);
			bool result;
			if (flag)
			{
				bool flag2 = Promo.s_Logger != null;
				if (flag2)
				{
					Promo.s_Logger.LogFormat(LogType.Warning, "Promo received null or empty command", new object[0]);
				}
				result = false;
			}
			else
			{
				try
				{
					Dictionary<string, object> dictionary = (Dictionary<string, object>)Json.Deserialize(command);
					bool flag3 = dictionary == null;
					if (flag3)
					{
						result = false;
					}
					else
					{
						object obj;
						bool flag4 = dictionary.TryGetValue("purchaseTrackingUrls", out obj);
						if (flag4)
						{
							bool flag5 = obj != null;
							if (flag5)
							{
								List<object> list = obj as List<object>;
								EventQueue eventQueue = EventQueue.Instance(Promo.s_Util, Promo.s_WebUtil);
								bool flag6 = list.Count > 0;
								if (flag6)
								{
									eventQueue.SetIapUrl(list[0] as string);
								}
								bool flag7 = list.Count > 1;
								if (flag7)
								{
									eventQueue.SetAdsUrl(list[1] as string);
								}
							}
							dictionary.Remove("purchaseTrackingUrls");
						}
						command = Json.Serialize(dictionary);
						object obj2;
						bool flag8 = !dictionary.TryGetValue("request", out obj2);
						if (flag8)
						{
							result = Promo.ExecPromoPurchase(command);
						}
						else
						{
							string text = ((string)obj2).ToLower();
							string a = text;
							if (!(a == "purchase"))
							{
								if (!(a == "setids"))
								{
									if (!(a == "close"))
									{
										bool flag9 = Promo.s_Logger != null;
										if (flag9)
										{
											Promo.s_Logger.LogWarning("UnityIAP Promo", "Unknown request received: " + text);
										}
										result = false;
									}
									else
									{
										bool flag10 = Promo.s_Logger != null;
										if (flag10)
										{
											Promo.s_Logger.Log("UnityIAP Promo: AdUnit closed without purchase");
										}
										result = true;
									}
								}
								else
								{
									ProfileData profileData = ProfileData.Instance(Promo.s_Util);
									object obj3;
									bool flag11 = dictionary.TryGetValue("gamerToken", out obj3);
									if (flag11)
									{
										profileData.SetGamerToken(obj3 as string);
									}
									bool flag12 = dictionary.TryGetValue("trackingOptOut", out obj3);
									if (flag12)
									{
										profileData.SetTrackingOptOut(obj3 as bool?);
									}
									bool flag13 = dictionary.TryGetValue("gameId", out obj3);
									if (flag13)
									{
										profileData.SetGameId(obj3 as string);
									}
									bool flag14 = dictionary.TryGetValue("abGroup", out obj3);
									if (flag14)
									{
										profileData.SetABGroup(obj3 as int?);
									}
									result = true;
								}
							}
							else
							{
								result = Promo.ExecPromoPurchase(command);
							}
						}
					}
				}
				catch (Exception ex)
				{
					bool flag15 = Promo.s_Logger != null;
					if (flag15)
					{
						Promo.s_Logger.LogError("UnityIAP Promo", string.Concat(new object[]
						{
							"Exception while processing incoming request: ",
							ex,
							"\n",
							command
						}));
					}
					result = false;
				}
			}
			return result;
		}

		// Token: 0x060002B4 RID: 692 RVA: 0x0000B35C File Offset: 0x0000955C
		internal static bool ExecPromoPurchase(string itemRequest)
		{
			bool flag = !Promo.s_IsReady || Promo.s_PromoPurchaser == null;
			bool result;
			if (flag)
			{
				bool flag2 = Promo.s_Logger != null;
				if (flag2)
				{
					Promo.s_Logger.LogError("UnityIAP Promo", "Promo purchase attempted without proper configuration");
				}
				result = false;
			}
			else
			{
				object obj = null;
				Dictionary<string, object> dictionary = null;
				try
				{
					dictionary = (Dictionary<string, object>)Json.Deserialize(itemRequest);
					bool flag3 = !dictionary.TryGetValue("productId", out obj);
					if (flag3)
					{
						Promo.s_Logger.LogError("UnityIAP", "Promo purchase unable to determine Product ID");
						return false;
					}
				}
				catch
				{
					Promo.s_Logger.LogError("UnityIAP", "Promo purchase argument exception");
					return false;
				}
				bool flag4 = string.IsNullOrEmpty(obj as string);
				if (flag4)
				{
					Promo.s_Logger.LogError("UnityIAP", "Promo product is null or empty!");
					result = false;
				}
				else
				{
					Product product = Promo.s_Unity.products.WithID((string)obj);
					bool flag5 = product == null;
					if (flag5)
					{
						Promo.s_Logger.LogError("UnityIAP", "Promo product lookup failed");
						result = false;
					}
					else
					{
						dictionary.Add("storeSpecificId", product.definition.storeSpecificId);
						string developerPayload = Json.Serialize(dictionary);
						Promo.s_PromoPurchaser.Purchase(product.definition, developerPayload);
						result = true;
					}
				}
			}
			return result;
		}

		// Token: 0x060002B5 RID: 693 RVA: 0x0000B4C8 File Offset: 0x000096C8
		// Note: this type is marked as 'beforefieldinit'.
		static Promo()
		{
		}

		// Token: 0x040001F7 RID: 503
		private static JSONStore s_PromoPurchaser = null;

		// Token: 0x040001F8 RID: 504
		private static IStoreCallback s_Unity = null;

		// Token: 0x040001F9 RID: 505
		private static RuntimePlatform s_RuntimePlatform;

		// Token: 0x040001FA RID: 506
		private static ILogger s_Logger;

		// Token: 0x040001FB RID: 507
		private static string s_Version;

		// Token: 0x040001FC RID: 508
		private static IUtil s_Util;

		// Token: 0x040001FD RID: 509
		private static IAsyncWebUtil s_WebUtil;

		// Token: 0x040001FE RID: 510
		private static bool s_IsReady = false;

		// Token: 0x040001FF RID: 511
		private static string s_ProductJSON;
	}
}
