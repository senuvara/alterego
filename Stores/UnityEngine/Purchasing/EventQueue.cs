﻿using System;
using System.Runtime.CompilerServices;
using Uniject;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000070 RID: 112
	internal class EventQueue
	{
		// Token: 0x060001F9 RID: 505 RVA: 0x00008EE8 File Offset: 0x000070E8
		private EventQueue(IUtil util, IAsyncWebUtil webUtil)
		{
			this.m_AsyncUtil = webUtil;
			this.Profile = ProfileData.Instance(util);
			this.ProfileDict = this.Profile.GetProfileDict();
			AdsIPC.InitAdsIPC(util);
		}

		// Token: 0x060001FA RID: 506 RVA: 0x00008F20 File Offset: 0x00007120
		public static EventQueue Instance(IUtil util, IAsyncWebUtil webUtil)
		{
			bool flag = EventQueue.QueueInstance == null;
			if (flag)
			{
				EventQueue.QueueInstance = new EventQueue(util, webUtil);
			}
			return EventQueue.QueueInstance;
		}

		// Token: 0x060001FB RID: 507 RVA: 0x00008F51 File Offset: 0x00007151
		internal void SetAdsUrl(string url)
		{
			this.TrackingUrl = url;
		}

		// Token: 0x060001FC RID: 508 RVA: 0x00008F5B File Offset: 0x0000715B
		internal void SetIapUrl(string url)
		{
			this.EventUrl = url;
		}

		// Token: 0x060001FD RID: 509 RVA: 0x00008F68 File Offset: 0x00007168
		internal bool SendEvent(EventDestType dest, string json, string url = null, int? delayInSeconds = null)
		{
			bool flag = this.m_AsyncUtil == null;
			bool result;
			if (flag)
			{
				result = false;
			}
			else
			{
				EventDestType dest2 = dest;
				if (dest2 != EventDestType.AdsTracking)
				{
					if (dest2 != EventDestType.IAP)
					{
						return dest2 == EventDestType.AdsIPC && AdsIPC.SendEvent(json);
					}
					string target = (url != null) ? url : this.EventUrl;
					bool flag2 = target == null || json == null;
					if (!flag2)
					{
						Action <>9__2;
						this.m_AsyncUtil.Post(target, json, delegate(string response)
						{
						}, delegate(string error)
						{
							bool flag4 = delayInSeconds != null;
							if (flag4)
							{
								delayInSeconds = new int?(Math.Max(5, delayInSeconds.Value * 2));
								delayInSeconds = new int?(Math.Min(300, delayInSeconds.Value));
								IAsyncWebUtil asyncUtil = this.m_AsyncUtil;
								Action a;
								if ((a = <>9__2) == null)
								{
									a = (<>9__2 = delegate()
									{
										this.SendEvent(dest, json, target, delayInSeconds);
									});
								}
								asyncUtil.Schedule(a, delayInSeconds.Value);
							}
						}, 30);
						return true;
					}
				}
				else
				{
					string target = (url != null) ? url : this.TrackingUrl;
					bool flag3 = target == null;
					if (!flag3)
					{
						this.m_AsyncUtil.Get(target, delegate(string response)
						{
						}, delegate(string error)
						{
						}, 30);
						return true;
					}
				}
				result = false;
			}
			return result;
		}

		// Token: 0x060001FE RID: 510 RVA: 0x000090E0 File Offset: 0x000072E0
		internal bool SendEvent(string json)
		{
			this.SendEvent(EventDestType.AdsTracking, null, null, null);
			this.SendEvent(EventDestType.IAP, json, null, null);
			return false;
		}

		// Token: 0x0400014C RID: 332
		private IAsyncWebUtil m_AsyncUtil;

		// Token: 0x0400014D RID: 333
		private static EventQueue QueueInstance;

		// Token: 0x0400014E RID: 334
		internal ProfileData Profile;

		// Token: 0x0400014F RID: 335
		private string TrackingUrl;

		// Token: 0x04000150 RID: 336
		private string EventUrl;

		// Token: 0x04000151 RID: 337
		internal object ProfileDict;

		// Token: 0x04000152 RID: 338
		private const int kMaxRetryDelayInSeconds = 300;

		// Token: 0x02000071 RID: 113
		[CompilerGenerated]
		private sealed class <>c__DisplayClass11_0
		{
			// Token: 0x060001FF RID: 511 RVA: 0x00002CA1 File Offset: 0x00000EA1
			public <>c__DisplayClass11_0()
			{
			}

			// Token: 0x06000200 RID: 512 RVA: 0x0000911C File Offset: 0x0000731C
			internal void <SendEvent>b__1(string error)
			{
				bool flag = this.delayInSeconds != null;
				if (flag)
				{
					this.delayInSeconds = new int?(Math.Max(5, this.delayInSeconds.Value * 2));
					this.delayInSeconds = new int?(Math.Min(300, this.delayInSeconds.Value));
					IAsyncWebUtil asyncUtil = this.<>4__this.m_AsyncUtil;
					Action a;
					if ((a = this.<>9__2) == null)
					{
						a = (this.<>9__2 = delegate()
						{
							this.<>4__this.SendEvent(this.dest, this.json, this.target, this.delayInSeconds);
						});
					}
					asyncUtil.Schedule(a, this.delayInSeconds.Value);
				}
			}

			// Token: 0x06000201 RID: 513 RVA: 0x000091B4 File Offset: 0x000073B4
			internal void <SendEvent>b__2()
			{
				this.<>4__this.SendEvent(this.dest, this.json, this.target, this.delayInSeconds);
			}

			// Token: 0x04000153 RID: 339
			public int? delayInSeconds;

			// Token: 0x04000154 RID: 340
			public EventDestType dest;

			// Token: 0x04000155 RID: 341
			public string json;

			// Token: 0x04000156 RID: 342
			public string target;

			// Token: 0x04000157 RID: 343
			public EventQueue <>4__this;

			// Token: 0x04000158 RID: 344
			public Action <>9__2;
		}

		// Token: 0x02000072 RID: 114
		[CompilerGenerated]
		[Serializable]
		private sealed class <>c
		{
			// Token: 0x06000202 RID: 514 RVA: 0x000091DA File Offset: 0x000073DA
			// Note: this type is marked as 'beforefieldinit'.
			static <>c()
			{
			}

			// Token: 0x06000203 RID: 515 RVA: 0x00002CA1 File Offset: 0x00000EA1
			public <>c()
			{
			}

			// Token: 0x06000204 RID: 516 RVA: 0x00002EA8 File Offset: 0x000010A8
			internal void <SendEvent>b__11_0(string response)
			{
			}

			// Token: 0x06000205 RID: 517 RVA: 0x00002EA8 File Offset: 0x000010A8
			internal void <SendEvent>b__11_3(string response)
			{
			}

			// Token: 0x06000206 RID: 518 RVA: 0x00002EA8 File Offset: 0x000010A8
			internal void <SendEvent>b__11_4(string error)
			{
			}

			// Token: 0x04000159 RID: 345
			public static readonly EventQueue.<>c <>9 = new EventQueue.<>c();

			// Token: 0x0400015A RID: 346
			public static Action<string> <>9__11_0;

			// Token: 0x0400015B RID: 347
			public static Action<string> <>9__11_3;

			// Token: 0x0400015C RID: 348
			public static Action<string> <>9__11_4;
		}
	}
}
