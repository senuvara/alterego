﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000019 RID: 25
	public enum CloudMoolahMode
	{
		// Token: 0x04000022 RID: 34
		Production,
		// Token: 0x04000023 RID: 35
		AlwaysSucceed,
		// Token: 0x04000024 RID: 36
		AlwaysFailed
	}
}
