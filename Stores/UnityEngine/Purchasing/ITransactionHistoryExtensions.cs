﻿using System;
using UnityEngine.Purchasing.Extension;

namespace UnityEngine.Purchasing
{
	// Token: 0x020000A0 RID: 160
	public interface ITransactionHistoryExtensions : IStoreExtension
	{
		// Token: 0x06000318 RID: 792
		PurchaseFailureDescription GetLastPurchaseFailureDescription();

		// Token: 0x06000319 RID: 793
		StoreSpecificPurchaseErrorCode GetLastStoreSpecificPurchaseErrorCode();
	}
}
