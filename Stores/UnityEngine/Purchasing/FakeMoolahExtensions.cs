﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000018 RID: 24
	internal class FakeMoolahExtensions : IMoolahExtension, IStoreExtension
	{
		// Token: 0x06000064 RID: 100 RVA: 0x00002FBA File Offset: 0x000011BA
		public void RestoreTransactionID(Action<RestoreTransactionIDState> result)
		{
			result(RestoreTransactionIDState.RestoreSucceed);
		}

		// Token: 0x06000065 RID: 101 RVA: 0x00002FC5 File Offset: 0x000011C5
		public void ValidateReceipt(string transactionId, string receipt, Action<string, ValidateReceiptState, string> result)
		{
			result(transactionId, ValidateReceiptState.ValidateSucceed, "Fake Validate Receipt Succeed");
		}

		// Token: 0x06000066 RID: 102 RVA: 0x00002CA1 File Offset: 0x00000EA1
		public FakeMoolahExtensions()
		{
		}
	}
}
