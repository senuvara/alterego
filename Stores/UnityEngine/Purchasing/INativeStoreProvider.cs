﻿using System;
using Uniject;
using UnityEngine.Purchasing.Extension;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000060 RID: 96
	internal interface INativeStoreProvider
	{
		// Token: 0x06000198 RID: 408
		INativeStore GetAndroidStore(IUnityCallback callback, AppStore store, IPurchasingBinder binder, IUtil util);

		// Token: 0x06000199 RID: 409
		INativeAppleStore GetStorekit(IUnityCallback callback);

		// Token: 0x0600019A RID: 410
		INativeTizenStore GetTizenStore(IUnityCallback callback, IPurchasingBinder binder);

		// Token: 0x0600019B RID: 411
		INativeFacebookStore GetFacebookStore();

		// Token: 0x0600019C RID: 412
		INativeFacebookStore GetFacebookStore(IUnityCallback callback, IPurchasingBinder binder);
	}
}
