﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000093 RID: 147
	public class TimeSpanUnits
	{
		// Token: 0x060002E5 RID: 741 RVA: 0x0000C050 File Offset: 0x0000A250
		public TimeSpanUnits(double d, int m, int y)
		{
			this.days = d;
			this.months = m;
			this.years = y;
		}

		// Token: 0x04000238 RID: 568
		public double days;

		// Token: 0x04000239 RID: 569
		public int months;

		// Token: 0x0400023A RID: 570
		public int years;
	}
}
