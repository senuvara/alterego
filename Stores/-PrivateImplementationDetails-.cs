﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// Token: 0x020000C5 RID: 197
[CompilerGenerated]
internal sealed class <PrivateImplementationDetails>
{
	// Token: 0x060003B5 RID: 949 RVA: 0x0000F298 File Offset: 0x0000D498
	internal static uint ComputeStringHash(string s)
	{
		uint num;
		if (s != null)
		{
			num = 2166136261U;
			for (int i = 0; i < s.Length; i++)
			{
				num = ((uint)s[i] ^ num) * 16777619U;
			}
		}
		return num;
	}

	// Token: 0x040002BA RID: 698 RVA: 0x0000F2D4 File Offset: 0x0000D4D4
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=368 8ACBA6DEDC6FDA919C1CE7B29030BCA705F0CF41;

	// Token: 0x020000C6 RID: 198
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 368)]
	private struct __StaticArrayInitTypeSize=368
	{
	}
}
