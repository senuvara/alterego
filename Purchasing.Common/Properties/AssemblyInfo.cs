﻿using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.CompilerServices;

[assembly: AssemblyVersion("1.0.7016.24185")]
[assembly: AssemblyTitle("Purchasing.Common")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("")]
[assembly: AssemblyCopyright("Unity Technologies")]
[assembly: AssemblyTrademark("")]
[assembly: InternalsVisibleTo("Stores")]
[assembly: InternalsVisibleTo("specs")]
[assembly: InternalsVisibleTo("Security")]
[assembly: InternalsVisibleTo("Editor")]
[assembly: InternalsVisibleTo("Apple")]
[assembly: InternalsVisibleTo("Tizen")]
[assembly: InternalsVisibleTo("TizenStub")]
[assembly: InternalsVisibleTo("XiaomiMiPay")]
[assembly: InternalsVisibleTo("FacebookStore")]
[assembly: InternalsVisibleTo("FacebookStub")]
