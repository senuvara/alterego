﻿using System;
using UnityEngine.Purchasing.MiniJSON;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000002 RID: 2
	public class MiniJson
	{
		// Token: 0x06000001 RID: 1 RVA: 0x00002050 File Offset: 0x00000250
		public static string JsonEncode(object json)
		{
			return Json.Serialize(json);
		}

		// Token: 0x06000002 RID: 2 RVA: 0x00002068 File Offset: 0x00000268
		public static object JsonDecode(string json)
		{
			return Json.Deserialize(json);
		}

		// Token: 0x06000003 RID: 3 RVA: 0x00002080 File Offset: 0x00000280
		public MiniJson()
		{
		}
	}
}
