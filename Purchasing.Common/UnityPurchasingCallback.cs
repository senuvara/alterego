﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000004 RID: 4
	// (Invoke) Token: 0x06000008 RID: 8
	internal delegate void UnityPurchasingCallback(string subject, string payload, string receipt, string transactionId);
}
