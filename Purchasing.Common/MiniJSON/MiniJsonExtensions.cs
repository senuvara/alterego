﻿using System;
using System.Collections.Generic;

namespace UnityEngine.Purchasing.MiniJSON
{
	// Token: 0x02000007 RID: 7
	public static class MiniJsonExtensions
	{
		// Token: 0x06000017 RID: 23 RVA: 0x0000230C File Offset: 0x0000050C
		public static Dictionary<string, object> GetHash(this Dictionary<string, object> dic, string key)
		{
			return (Dictionary<string, object>)dic[key];
		}

		// Token: 0x06000018 RID: 24 RVA: 0x0000232C File Offset: 0x0000052C
		public static T GetEnum<T>(this Dictionary<string, object> dic, string key)
		{
			bool flag = dic.ContainsKey(key);
			T result;
			if (flag)
			{
				result = (T)((object)Enum.Parse(typeof(T), dic[key].ToString(), true));
			}
			else
			{
				result = default(T);
			}
			return result;
		}

		// Token: 0x06000019 RID: 25 RVA: 0x00002378 File Offset: 0x00000578
		public static string GetString(this Dictionary<string, object> dic, string key, string defaultValue = "")
		{
			bool flag = dic.ContainsKey(key);
			string result;
			if (flag)
			{
				result = dic[key].ToString();
			}
			else
			{
				result = defaultValue;
			}
			return result;
		}

		// Token: 0x0600001A RID: 26 RVA: 0x000023A8 File Offset: 0x000005A8
		public static long GetLong(this Dictionary<string, object> dic, string key)
		{
			bool flag = dic.ContainsKey(key);
			long result;
			if (flag)
			{
				result = long.Parse(dic[key].ToString());
			}
			else
			{
				result = 0L;
			}
			return result;
		}

		// Token: 0x0600001B RID: 27 RVA: 0x000023DC File Offset: 0x000005DC
		public static List<string> GetStringList(this Dictionary<string, object> dic, string key)
		{
			bool flag = dic.ContainsKey(key);
			List<string> result;
			if (flag)
			{
				List<string> list = new List<string>();
				List<object> list2 = (List<object>)dic[key];
				foreach (object obj in list2)
				{
					list.Add(obj.ToString());
				}
				result = list;
			}
			else
			{
				result = new List<string>();
			}
			return result;
		}

		// Token: 0x0600001C RID: 28 RVA: 0x00002464 File Offset: 0x00000664
		public static bool GetBool(this Dictionary<string, object> dic, string key)
		{
			bool flag = dic.ContainsKey(key);
			return flag && bool.Parse(dic[key].ToString());
		}

		// Token: 0x0600001D RID: 29 RVA: 0x00002498 File Offset: 0x00000698
		public static T Get<T>(this Dictionary<string, object> dic, string key)
		{
			bool flag = dic.ContainsKey(key);
			T result;
			if (flag)
			{
				result = (T)((object)dic[key]);
			}
			else
			{
				result = default(T);
			}
			return result;
		}

		// Token: 0x0600001E RID: 30 RVA: 0x000024D0 File Offset: 0x000006D0
		public static string toJson(this Dictionary<string, object> obj)
		{
			return MiniJson.JsonEncode(obj);
		}

		// Token: 0x0600001F RID: 31 RVA: 0x000024E8 File Offset: 0x000006E8
		public static string toJson(this Dictionary<string, string> obj)
		{
			return MiniJson.JsonEncode(obj);
		}

		// Token: 0x06000020 RID: 32 RVA: 0x00002500 File Offset: 0x00000700
		public static string toJson(this string[] array)
		{
			List<object> list = new List<object>();
			foreach (string item in array)
			{
				list.Add(item);
			}
			return MiniJson.JsonEncode(list);
		}

		// Token: 0x06000021 RID: 33 RVA: 0x00002540 File Offset: 0x00000740
		public static List<object> ArrayListFromJson(this string json)
		{
			return MiniJson.JsonDecode(json) as List<object>;
		}

		// Token: 0x06000022 RID: 34 RVA: 0x00002560 File Offset: 0x00000760
		public static Dictionary<string, object> HashtableFromJson(this string json)
		{
			return MiniJson.JsonDecode(json) as Dictionary<string, object>;
		}
	}
}
