﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;

namespace UnityEngine.Purchasing.MiniJSON
{
	// Token: 0x02000006 RID: 6
	public static class Json
	{
		// Token: 0x06000015 RID: 21 RVA: 0x000022CC File Offset: 0x000004CC
		public static object Deserialize(string json)
		{
			bool flag = json == null;
			object result;
			if (flag)
			{
				result = null;
			}
			else
			{
				result = Json.Parser.Parse(json);
			}
			return result;
		}

		// Token: 0x06000016 RID: 22 RVA: 0x000022F4 File Offset: 0x000004F4
		public static string Serialize(object obj)
		{
			return Json.Serializer.Serialize(obj);
		}

		// Token: 0x0200000A RID: 10
		private sealed class Parser : IDisposable
		{
			// Token: 0x06000023 RID: 35 RVA: 0x00002580 File Offset: 0x00000780
			public static bool IsWordBreak(char c)
			{
				return char.IsWhiteSpace(c) || "{}[],:\"".IndexOf(c) != -1;
			}

			// Token: 0x06000024 RID: 36 RVA: 0x000025AE File Offset: 0x000007AE
			private Parser(string jsonString)
			{
				this.json = new StringReader(jsonString);
			}

			// Token: 0x06000025 RID: 37 RVA: 0x000025C4 File Offset: 0x000007C4
			public static object Parse(string jsonString)
			{
				object result;
				using (Json.Parser parser = new Json.Parser(jsonString))
				{
					result = parser.ParseValue();
				}
				return result;
			}

			// Token: 0x06000026 RID: 38 RVA: 0x00002600 File Offset: 0x00000800
			public void Dispose()
			{
				this.json.Dispose();
				this.json = null;
			}

			// Token: 0x06000027 RID: 39 RVA: 0x00002618 File Offset: 0x00000818
			private Dictionary<string, object> ParseObject()
			{
				Dictionary<string, object> dictionary = new Dictionary<string, object>();
				this.json.Read();
				for (;;)
				{
					Json.Parser.TOKEN nextToken = this.NextToken;
					if (nextToken == Json.Parser.TOKEN.NONE)
					{
						break;
					}
					if (nextToken == Json.Parser.TOKEN.CURLY_CLOSE)
					{
						goto IL_34;
					}
					if (nextToken != Json.Parser.TOKEN.COMMA)
					{
						string text = this.ParseString();
						bool flag = text == null;
						if (flag)
						{
							goto Block_4;
						}
						bool flag2 = this.NextToken != Json.Parser.TOKEN.COLON;
						if (flag2)
						{
							goto Block_5;
						}
						this.json.Read();
						dictionary[text] = this.ParseValue();
					}
				}
				return null;
				IL_34:
				return dictionary;
				Block_4:
				return null;
				Block_5:
				return null;
			}

			// Token: 0x06000028 RID: 40 RVA: 0x000026B0 File Offset: 0x000008B0
			private List<object> ParseArray()
			{
				List<object> list = new List<object>();
				this.json.Read();
				bool flag = true;
				while (flag)
				{
					Json.Parser.TOKEN nextToken = this.NextToken;
					Json.Parser.TOKEN token = nextToken;
					if (token == Json.Parser.TOKEN.NONE)
					{
						return null;
					}
					if (token != Json.Parser.TOKEN.SQUARED_CLOSE)
					{
						if (token != Json.Parser.TOKEN.COMMA)
						{
							object item = this.ParseByToken(nextToken);
							list.Add(item);
						}
					}
					else
					{
						flag = false;
					}
				}
				return list;
			}

			// Token: 0x06000029 RID: 41 RVA: 0x00002720 File Offset: 0x00000920
			private object ParseValue()
			{
				Json.Parser.TOKEN nextToken = this.NextToken;
				return this.ParseByToken(nextToken);
			}

			// Token: 0x0600002A RID: 42 RVA: 0x00002740 File Offset: 0x00000940
			private object ParseByToken(Json.Parser.TOKEN token)
			{
				switch (token)
				{
				case Json.Parser.TOKEN.CURLY_OPEN:
					return this.ParseObject();
				case Json.Parser.TOKEN.SQUARED_OPEN:
					return this.ParseArray();
				case Json.Parser.TOKEN.STRING:
					return this.ParseString();
				case Json.Parser.TOKEN.NUMBER:
					return this.ParseNumber();
				case Json.Parser.TOKEN.TRUE:
					return true;
				case Json.Parser.TOKEN.FALSE:
					return false;
				case Json.Parser.TOKEN.NULL:
					return null;
				}
				return null;
			}

			// Token: 0x0600002B RID: 43 RVA: 0x000027C8 File Offset: 0x000009C8
			private string ParseString()
			{
				StringBuilder stringBuilder = new StringBuilder();
				this.json.Read();
				bool flag = true;
				while (flag)
				{
					bool flag2 = this.json.Peek() == -1;
					if (flag2)
					{
						break;
					}
					char nextChar = this.NextChar;
					char c = nextChar;
					if (c != '"')
					{
						if (c != '\\')
						{
							stringBuilder.Append(nextChar);
						}
						else
						{
							bool flag3 = this.json.Peek() == -1;
							if (flag3)
							{
								flag = false;
							}
							else
							{
								nextChar = this.NextChar;
								char c2 = nextChar;
								if (c2 <= '\\')
								{
									if (c2 == '"' || c2 == '/' || c2 == '\\')
									{
										stringBuilder.Append(nextChar);
									}
								}
								else if (c2 <= 'f')
								{
									if (c2 != 'b')
									{
										if (c2 == 'f')
										{
											stringBuilder.Append('\f');
										}
									}
									else
									{
										stringBuilder.Append('\b');
									}
								}
								else if (c2 != 'n')
								{
									switch (c2)
									{
									case 'r':
										stringBuilder.Append('\r');
										break;
									case 't':
										stringBuilder.Append('\t');
										break;
									case 'u':
									{
										char[] array = new char[4];
										for (int i = 0; i < 4; i++)
										{
											array[i] = this.NextChar;
										}
										stringBuilder.Append((char)Convert.ToInt32(new string(array), 16));
										break;
									}
									}
								}
								else
								{
									stringBuilder.Append('\n');
								}
							}
						}
					}
					else
					{
						flag = false;
					}
				}
				return stringBuilder.ToString();
			}

			// Token: 0x0600002C RID: 44 RVA: 0x0000295C File Offset: 0x00000B5C
			private object ParseNumber()
			{
				string nextWord = this.NextWord;
				bool flag = nextWord.IndexOf('.') == -1 && nextWord.IndexOf('e') == -1 && nextWord.IndexOf('E') == -1;
				object result;
				if (flag)
				{
					long num;
					long.TryParse(nextWord, NumberStyles.Any, CultureInfo.InvariantCulture, out num);
					result = num;
				}
				else
				{
					double num2;
					double.TryParse(nextWord, NumberStyles.Any, CultureInfo.InvariantCulture, out num2);
					result = num2;
				}
				return result;
			}

			// Token: 0x0600002D RID: 45 RVA: 0x000029D8 File Offset: 0x00000BD8
			private void EatWhitespace()
			{
				while (char.IsWhiteSpace(this.PeekChar))
				{
					this.json.Read();
					bool flag = this.json.Peek() == -1;
					if (flag)
					{
						break;
					}
				}
			}

			// Token: 0x17000001 RID: 1
			// (get) Token: 0x0600002E RID: 46 RVA: 0x00002A1C File Offset: 0x00000C1C
			private char PeekChar
			{
				get
				{
					return Convert.ToChar(this.json.Peek());
				}
			}

			// Token: 0x17000002 RID: 2
			// (get) Token: 0x0600002F RID: 47 RVA: 0x00002A40 File Offset: 0x00000C40
			private char NextChar
			{
				get
				{
					return Convert.ToChar(this.json.Read());
				}
			}

			// Token: 0x17000003 RID: 3
			// (get) Token: 0x06000030 RID: 48 RVA: 0x00002A64 File Offset: 0x00000C64
			private string NextWord
			{
				get
				{
					StringBuilder stringBuilder = new StringBuilder();
					while (!Json.Parser.IsWordBreak(this.PeekChar))
					{
						stringBuilder.Append(this.NextChar);
						bool flag = this.json.Peek() == -1;
						if (flag)
						{
							break;
						}
					}
					return stringBuilder.ToString();
				}
			}

			// Token: 0x17000004 RID: 4
			// (get) Token: 0x06000031 RID: 49 RVA: 0x00002ABC File Offset: 0x00000CBC
			private Json.Parser.TOKEN NextToken
			{
				get
				{
					this.EatWhitespace();
					bool flag = this.json.Peek() == -1;
					Json.Parser.TOKEN result;
					if (flag)
					{
						result = Json.Parser.TOKEN.NONE;
					}
					else
					{
						char peekChar = this.PeekChar;
						if (peekChar <= '[')
						{
							switch (peekChar)
							{
							case '"':
								return Json.Parser.TOKEN.STRING;
							case '#':
							case '$':
							case '%':
							case '&':
							case '\'':
							case '(':
							case ')':
							case '*':
							case '+':
							case '.':
							case '/':
								break;
							case ',':
								this.json.Read();
								return Json.Parser.TOKEN.COMMA;
							case '-':
							case '0':
							case '1':
							case '2':
							case '3':
							case '4':
							case '5':
							case '6':
							case '7':
							case '8':
							case '9':
								return Json.Parser.TOKEN.NUMBER;
							case ':':
								return Json.Parser.TOKEN.COLON;
							default:
								if (peekChar == '[')
								{
									return Json.Parser.TOKEN.SQUARED_OPEN;
								}
								break;
							}
						}
						else
						{
							if (peekChar == ']')
							{
								this.json.Read();
								return Json.Parser.TOKEN.SQUARED_CLOSE;
							}
							if (peekChar == '{')
							{
								return Json.Parser.TOKEN.CURLY_OPEN;
							}
							if (peekChar == '}')
							{
								this.json.Read();
								return Json.Parser.TOKEN.CURLY_CLOSE;
							}
						}
						string nextWord = this.NextWord;
						if (!(nextWord == "false"))
						{
							if (!(nextWord == "true"))
							{
								if (!(nextWord == "null"))
								{
									result = Json.Parser.TOKEN.NONE;
								}
								else
								{
									result = Json.Parser.TOKEN.NULL;
								}
							}
							else
							{
								result = Json.Parser.TOKEN.TRUE;
							}
						}
						else
						{
							result = Json.Parser.TOKEN.FALSE;
						}
					}
					return result;
				}
			}

			// Token: 0x04000005 RID: 5
			private const string WORD_BREAK = "{}[],:\"";

			// Token: 0x04000006 RID: 6
			private StringReader json;

			// Token: 0x0200000C RID: 12
			private enum TOKEN
			{
				// Token: 0x04000009 RID: 9
				NONE,
				// Token: 0x0400000A RID: 10
				CURLY_OPEN,
				// Token: 0x0400000B RID: 11
				CURLY_CLOSE,
				// Token: 0x0400000C RID: 12
				SQUARED_OPEN,
				// Token: 0x0400000D RID: 13
				SQUARED_CLOSE,
				// Token: 0x0400000E RID: 14
				COLON,
				// Token: 0x0400000F RID: 15
				COMMA,
				// Token: 0x04000010 RID: 16
				STRING,
				// Token: 0x04000011 RID: 17
				NUMBER,
				// Token: 0x04000012 RID: 18
				TRUE,
				// Token: 0x04000013 RID: 19
				FALSE,
				// Token: 0x04000014 RID: 20
				NULL
			}
		}

		// Token: 0x0200000B RID: 11
		private sealed class Serializer
		{
			// Token: 0x06000032 RID: 50 RVA: 0x00002C0D File Offset: 0x00000E0D
			private Serializer()
			{
				this.builder = new StringBuilder();
			}

			// Token: 0x06000033 RID: 51 RVA: 0x00002C24 File Offset: 0x00000E24
			public static string Serialize(object obj)
			{
				Json.Serializer serializer = new Json.Serializer();
				serializer.SerializeValue(obj);
				return serializer.builder.ToString();
			}

			// Token: 0x06000034 RID: 52 RVA: 0x00002C50 File Offset: 0x00000E50
			private void SerializeValue(object value)
			{
				bool flag = value == null;
				if (flag)
				{
					this.builder.Append("null");
				}
				else
				{
					string str;
					bool flag2 = (str = (value as string)) != null;
					if (flag2)
					{
						this.SerializeString(str);
					}
					else
					{
						bool flag3 = value is bool;
						if (flag3)
						{
							this.builder.Append(((bool)value) ? "true" : "false");
						}
						else
						{
							IList anArray;
							bool flag4 = (anArray = (value as IList)) != null;
							if (flag4)
							{
								this.SerializeArray(anArray);
							}
							else
							{
								IDictionary obj;
								bool flag5 = (obj = (value as IDictionary)) != null;
								if (flag5)
								{
									this.SerializeObject(obj);
								}
								else
								{
									bool flag6 = value is char;
									if (flag6)
									{
										this.SerializeString(new string((char)value, 1));
									}
									else
									{
										this.SerializeOther(value);
									}
								}
							}
						}
					}
				}
			}

			// Token: 0x06000035 RID: 53 RVA: 0x00002D3C File Offset: 0x00000F3C
			private void SerializeObject(IDictionary obj)
			{
				bool flag = true;
				this.builder.Append('{');
				foreach (object obj2 in obj.Keys)
				{
					bool flag2 = !flag;
					if (flag2)
					{
						this.builder.Append(',');
					}
					this.SerializeString(obj2.ToString());
					this.builder.Append(':');
					this.SerializeValue(obj[obj2]);
					flag = false;
				}
				this.builder.Append('}');
			}

			// Token: 0x06000036 RID: 54 RVA: 0x00002DF4 File Offset: 0x00000FF4
			private void SerializeArray(IList anArray)
			{
				this.builder.Append('[');
				bool flag = true;
				foreach (object value in anArray)
				{
					bool flag2 = !flag;
					if (flag2)
					{
						this.builder.Append(',');
					}
					this.SerializeValue(value);
					flag = false;
				}
				this.builder.Append(']');
			}

			// Token: 0x06000037 RID: 55 RVA: 0x00002E84 File Offset: 0x00001084
			private void SerializeString(string str)
			{
				this.builder.Append('"');
				char[] array = str.ToCharArray();
				char[] array2 = array;
				int i = 0;
				while (i < array2.Length)
				{
					char c = array2[i];
					char c2 = c;
					switch (c2)
					{
					case '\b':
						this.builder.Append("\\b");
						break;
					case '\t':
						this.builder.Append("\\t");
						break;
					case '\n':
						this.builder.Append("\\n");
						break;
					case '\v':
						goto IL_F2;
					case '\f':
						this.builder.Append("\\f");
						break;
					case '\r':
						this.builder.Append("\\r");
						break;
					default:
						if (c2 != '"')
						{
							if (c2 != '\\')
							{
								goto IL_F2;
							}
							this.builder.Append("\\\\");
						}
						else
						{
							this.builder.Append("\\\"");
						}
						break;
					}
					IL_150:
					i++;
					continue;
					IL_F2:
					int num = Convert.ToInt32(c);
					bool flag = num >= 32 && num <= 126;
					if (flag)
					{
						this.builder.Append(c);
					}
					else
					{
						this.builder.Append("\\u");
						this.builder.Append(num.ToString("x4"));
					}
					goto IL_150;
				}
				this.builder.Append('"');
			}

			// Token: 0x06000038 RID: 56 RVA: 0x00003000 File Offset: 0x00001200
			private void SerializeOther(object value)
			{
				bool flag = value is float;
				if (flag)
				{
					this.builder.Append(((float)value).ToString("R"));
				}
				else
				{
					bool flag2 = value is int || value is uint || value is long || value is sbyte || value is byte || value is short || value is ushort || value is ulong;
					if (flag2)
					{
						this.builder.Append(value);
					}
					else
					{
						bool flag3 = value is double || value is decimal;
						if (flag3)
						{
							this.builder.Append(Convert.ToDouble(value).ToString("R"));
						}
						else
						{
							this.SerializeString(value.ToString());
						}
					}
				}
			}

			// Token: 0x04000007 RID: 7
			private StringBuilder builder;
		}
	}
}
