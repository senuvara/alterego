﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000005 RID: 5
	internal static class VersionCheck
	{
		// Token: 0x0600000B RID: 11 RVA: 0x0000208C File Offset: 0x0000028C
		public static bool GreaterThanOrEqual(string versionA, string versionB)
		{
			return !VersionCheck.LessThan(versionA, versionB);
		}

		// Token: 0x0600000C RID: 12 RVA: 0x000020A8 File Offset: 0x000002A8
		public static bool GreaterThan(string versionA, string versionB)
		{
			return !VersionCheck.LessThanOrEqual(versionA, versionB);
		}

		// Token: 0x0600000D RID: 13 RVA: 0x000020C4 File Offset: 0x000002C4
		public static bool LessThan(string versionA, string versionB)
		{
			VersionCheck.Version version = VersionCheck.Parse(versionA);
			VersionCheck.Version version2 = VersionCheck.Parse(versionB);
			bool flag = version.major > version2.major;
			bool result;
			if (flag)
			{
				result = false;
			}
			else
			{
				bool flag2 = version.major < version2.major;
				if (flag2)
				{
					result = true;
				}
				else
				{
					bool flag3 = version.minor > version2.minor;
					if (flag3)
					{
						result = false;
					}
					else
					{
						bool flag4 = version.minor < version2.minor;
						if (flag4)
						{
							result = true;
						}
						else
						{
							bool flag5 = version.patch > version2.patch;
							if (flag5)
							{
								result = false;
							}
							else
							{
								bool flag6 = version.patch < version2.patch;
								result = flag6;
							}
						}
					}
				}
			}
			return result;
		}

		// Token: 0x0600000E RID: 14 RVA: 0x00002180 File Offset: 0x00000380
		public static bool LessThanOrEqual(string versionA, string versionB)
		{
			return VersionCheck.LessThan(versionA, versionB) || !VersionCheck.LessThan(versionB, versionA);
		}

		// Token: 0x0600000F RID: 15 RVA: 0x000021A8 File Offset: 0x000003A8
		public static bool Equal(string versionA, string versionB)
		{
			return !VersionCheck.LessThan(versionA, versionB) && !VersionCheck.LessThan(versionB, versionA);
		}

		// Token: 0x06000010 RID: 16 RVA: 0x000021D0 File Offset: 0x000003D0
		public static int MajorVersion(string version)
		{
			return VersionCheck.PartialVersion(version, 0);
		}

		// Token: 0x06000011 RID: 17 RVA: 0x000021EC File Offset: 0x000003EC
		public static int MinorVersion(string version)
		{
			return VersionCheck.PartialVersion(version, 1);
		}

		// Token: 0x06000012 RID: 18 RVA: 0x00002208 File Offset: 0x00000408
		public static int PatchVersion(string version)
		{
			return VersionCheck.PartialVersion(version, 2);
		}

		// Token: 0x06000013 RID: 19 RVA: 0x00002224 File Offset: 0x00000424
		public static VersionCheck.Version Parse(string version)
		{
			return new VersionCheck.Version
			{
				major = VersionCheck.MajorVersion(version),
				minor = VersionCheck.MinorVersion(version),
				patch = VersionCheck.PatchVersion(version)
			};
		}

		// Token: 0x06000014 RID: 20 RVA: 0x00002268 File Offset: 0x00000468
		private static int PartialVersion(string version, int index)
		{
			string[] array = version.Split(new char[]
			{
				'a',
				'b',
				'f',
				'p'
			});
			string text = array[0];
			int num = 0;
			string[] array2 = text.Split(new char[]
			{
				'.'
			});
			bool flag = array2.Length < index + 1;
			int result;
			if (flag)
			{
				result = num;
			}
			else
			{
				int.TryParse(array2[index], out num);
				result = num;
			}
			return result;
		}

		// Token: 0x02000009 RID: 9
		internal struct Version
		{
			// Token: 0x04000002 RID: 2
			public int major;

			// Token: 0x04000003 RID: 3
			public int minor;

			// Token: 0x04000004 RID: 4
			public int patch;
		}
	}
}
