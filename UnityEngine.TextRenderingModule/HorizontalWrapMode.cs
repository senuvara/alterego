﻿using System;

namespace UnityEngine
{
	// Token: 0x02000008 RID: 8
	public enum HorizontalWrapMode
	{
		// Token: 0x04000037 RID: 55
		Wrap,
		// Token: 0x04000038 RID: 56
		Overflow
	}
}
