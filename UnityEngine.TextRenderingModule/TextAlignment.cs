﻿using System;

namespace UnityEngine
{
	// Token: 0x02000006 RID: 6
	public enum TextAlignment
	{
		// Token: 0x04000029 RID: 41
		Left,
		// Token: 0x0400002A RID: 42
		Center,
		// Token: 0x0400002B RID: 43
		Right
	}
}
