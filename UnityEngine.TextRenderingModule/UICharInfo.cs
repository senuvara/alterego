﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x0200000D RID: 13
	[UsedByNativeCode]
	public struct UICharInfo
	{
		// Token: 0x04000043 RID: 67
		public Vector2 cursorPos;

		// Token: 0x04000044 RID: 68
		public float charWidth;
	}
}
