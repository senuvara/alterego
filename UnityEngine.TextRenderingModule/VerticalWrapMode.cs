﻿using System;

namespace UnityEngine
{
	// Token: 0x02000009 RID: 9
	public enum VerticalWrapMode
	{
		// Token: 0x0400003A RID: 58
		Truncate,
		// Token: 0x0400003B RID: 59
		Overflow
	}
}
