﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x0200000E RID: 14
	[UsedByNativeCode]
	public struct UILineInfo
	{
		// Token: 0x04000045 RID: 69
		public int startCharIdx;

		// Token: 0x04000046 RID: 70
		public int height;

		// Token: 0x04000047 RID: 71
		public float topY;

		// Token: 0x04000048 RID: 72
		public float leading;
	}
}
