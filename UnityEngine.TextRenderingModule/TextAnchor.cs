﻿using System;

namespace UnityEngine
{
	// Token: 0x02000007 RID: 7
	public enum TextAnchor
	{
		// Token: 0x0400002D RID: 45
		UpperLeft,
		// Token: 0x0400002E RID: 46
		UpperCenter,
		// Token: 0x0400002F RID: 47
		UpperRight,
		// Token: 0x04000030 RID: 48
		MiddleLeft,
		// Token: 0x04000031 RID: 49
		MiddleCenter,
		// Token: 0x04000032 RID: 50
		MiddleRight,
		// Token: 0x04000033 RID: 51
		LowerLeft,
		// Token: 0x04000034 RID: 52
		LowerCenter,
		// Token: 0x04000035 RID: 53
		LowerRight
	}
}
