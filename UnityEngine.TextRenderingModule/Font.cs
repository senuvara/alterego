﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading;
using UnityEngine.Bindings;
using UnityEngine.Internal;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000010 RID: 16
	[NativeHeader("Modules/TextRendering/Public/FontImpl.h")]
	[NativeHeader("Modules/TextRendering/Public/Font.h")]
	[StaticAccessor("TextRenderingPrivate", StaticAccessorType.DoubleColon)]
	[NativeClass("TextRendering::Font")]
	public sealed class Font : Object
	{
		// Token: 0x06000081 RID: 129 RVA: 0x00002F5F File Offset: 0x0000115F
		public Font()
		{
			Font.Internal_CreateFont(this, null);
		}

		// Token: 0x06000082 RID: 130 RVA: 0x00002F6F File Offset: 0x0000116F
		public Font(string name)
		{
			Font.Internal_CreateFont(this, name);
		}

		// Token: 0x06000083 RID: 131 RVA: 0x00002F7F File Offset: 0x0000117F
		private Font(string[] names, int size)
		{
			Font.Internal_CreateDynamicFont(this, names, size);
		}

		// Token: 0x14000001 RID: 1
		// (add) Token: 0x06000084 RID: 132 RVA: 0x00002F90 File Offset: 0x00001190
		// (remove) Token: 0x06000085 RID: 133 RVA: 0x00002FC4 File Offset: 0x000011C4
		public static event Action<Font> textureRebuilt
		{
			add
			{
				Action<Font> action = Font.textureRebuilt;
				Action<Font> action2;
				do
				{
					action2 = action;
					action = Interlocked.CompareExchange<Action<Font>>(ref Font.textureRebuilt, (Action<Font>)Delegate.Combine(action2, value), action);
				}
				while (action != action2);
			}
			remove
			{
				Action<Font> action = Font.textureRebuilt;
				Action<Font> action2;
				do
				{
					action2 = action;
					action = Interlocked.CompareExchange<Action<Font>>(ref Font.textureRebuilt, (Action<Font>)Delegate.Remove(action2, value), action);
				}
				while (action != action2);
			}
		}

		// Token: 0x14000002 RID: 2
		// (add) Token: 0x06000086 RID: 134 RVA: 0x00002FF8 File Offset: 0x000011F8
		// (remove) Token: 0x06000087 RID: 135 RVA: 0x00003030 File Offset: 0x00001230
		private event Font.FontTextureRebuildCallback m_FontTextureRebuildCallback
		{
			add
			{
				Font.FontTextureRebuildCallback fontTextureRebuildCallback = this.m_FontTextureRebuildCallback;
				Font.FontTextureRebuildCallback fontTextureRebuildCallback2;
				do
				{
					fontTextureRebuildCallback2 = fontTextureRebuildCallback;
					fontTextureRebuildCallback = Interlocked.CompareExchange<Font.FontTextureRebuildCallback>(ref this.m_FontTextureRebuildCallback, (Font.FontTextureRebuildCallback)Delegate.Combine(fontTextureRebuildCallback2, value), fontTextureRebuildCallback);
				}
				while (fontTextureRebuildCallback != fontTextureRebuildCallback2);
			}
			remove
			{
				Font.FontTextureRebuildCallback fontTextureRebuildCallback = this.m_FontTextureRebuildCallback;
				Font.FontTextureRebuildCallback fontTextureRebuildCallback2;
				do
				{
					fontTextureRebuildCallback2 = fontTextureRebuildCallback;
					fontTextureRebuildCallback = Interlocked.CompareExchange<Font.FontTextureRebuildCallback>(ref this.m_FontTextureRebuildCallback, (Font.FontTextureRebuildCallback)Delegate.Remove(fontTextureRebuildCallback2, value), fontTextureRebuildCallback);
				}
				while (fontTextureRebuildCallback != fontTextureRebuildCallback2);
			}
		}

		// Token: 0x17000032 RID: 50
		// (get) Token: 0x06000088 RID: 136
		// (set) Token: 0x06000089 RID: 137
		public extern Material material { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000033 RID: 51
		// (get) Token: 0x0600008A RID: 138
		// (set) Token: 0x0600008B RID: 139
		public extern string[] fontNames { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000034 RID: 52
		// (get) Token: 0x0600008C RID: 140
		public extern bool dynamic { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000035 RID: 53
		// (get) Token: 0x0600008D RID: 141
		public extern int ascent { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000036 RID: 54
		// (get) Token: 0x0600008E RID: 142
		public extern int fontSize { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000037 RID: 55
		// (get) Token: 0x0600008F RID: 143
		// (set) Token: 0x06000090 RID: 144
		public extern CharacterInfo[] characterInfo { [FreeFunction("TextRenderingPrivate::GetFontCharacterInfo", HasExplicitThis = true)] [MethodImpl(MethodImplOptions.InternalCall)] get; [FreeFunction("TextRenderingPrivate::SetFontCharacterInfo", HasExplicitThis = true)] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000038 RID: 56
		// (get) Token: 0x06000091 RID: 145
		[NativeProperty("LineSpacing", false, TargetType.Function)]
		public extern int lineHeight { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000039 RID: 57
		// (get) Token: 0x06000092 RID: 146 RVA: 0x00003068 File Offset: 0x00001268
		// (set) Token: 0x06000093 RID: 147 RVA: 0x00003083 File Offset: 0x00001283
		[Obsolete("Font.textureRebuildCallback has been deprecated. Use Font.textureRebuilt instead.")]
		public Font.FontTextureRebuildCallback textureRebuildCallback
		{
			get
			{
				return this.m_FontTextureRebuildCallback;
			}
			set
			{
				this.m_FontTextureRebuildCallback = value;
			}
		}

		// Token: 0x06000094 RID: 148 RVA: 0x00003090 File Offset: 0x00001290
		public static Font CreateDynamicFontFromOSFont(string fontname, int size)
		{
			return new Font(new string[]
			{
				fontname
			}, size);
		}

		// Token: 0x06000095 RID: 149 RVA: 0x000030B8 File Offset: 0x000012B8
		public static Font CreateDynamicFontFromOSFont(string[] fontnames, int size)
		{
			return new Font(fontnames, size);
		}

		// Token: 0x06000096 RID: 150 RVA: 0x000030D4 File Offset: 0x000012D4
		[RequiredByNativeCode]
		internal static void InvokeTextureRebuilt_Internal(Font font)
		{
			if (Font.textureRebuilt != null)
			{
				Font.textureRebuilt(font);
			}
			Font.FontTextureRebuildCallback fontTextureRebuildCallback = font.m_FontTextureRebuildCallback;
			if (fontTextureRebuildCallback != null)
			{
				fontTextureRebuildCallback();
			}
		}

		// Token: 0x06000097 RID: 151 RVA: 0x00003104 File Offset: 0x00001304
		public static int GetMaxVertsForString(string str)
		{
			return str.Length * 4 + 4;
		}

		// Token: 0x06000098 RID: 152
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern Font GetDefault();

		// Token: 0x06000099 RID: 153 RVA: 0x00003124 File Offset: 0x00001324
		public bool HasCharacter(char c)
		{
			return this.HasCharacter((int)c);
		}

		// Token: 0x0600009A RID: 154
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern bool HasCharacter(int c);

		// Token: 0x0600009B RID: 155
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern string[] GetOSInstalledFontNames();

		// Token: 0x0600009C RID: 156
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void Internal_CreateFont([Writable] Font self, string name);

		// Token: 0x0600009D RID: 157
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void Internal_CreateDynamicFont([Writable] Font self, string[] _names, int size);

		// Token: 0x0600009E RID: 158
		[FreeFunction("TextRenderingPrivate::GetCharacterInfo", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern bool GetCharacterInfo(char ch, out CharacterInfo info, [DefaultValue("0")] int size, [DefaultValue("FontStyle.Normal")] FontStyle style);

		// Token: 0x0600009F RID: 159 RVA: 0x00003140 File Offset: 0x00001340
		[ExcludeFromDocs]
		public bool GetCharacterInfo(char ch, out CharacterInfo info, int size)
		{
			return this.GetCharacterInfo(ch, out info, size, FontStyle.Normal);
		}

		// Token: 0x060000A0 RID: 160 RVA: 0x00003160 File Offset: 0x00001360
		[ExcludeFromDocs]
		public bool GetCharacterInfo(char ch, out CharacterInfo info)
		{
			return this.GetCharacterInfo(ch, out info, 0, FontStyle.Normal);
		}

		// Token: 0x060000A1 RID: 161
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void RequestCharactersInTexture(string characters, [DefaultValue("0")] int size, [DefaultValue("FontStyle.Normal")] FontStyle style);

		// Token: 0x060000A2 RID: 162 RVA: 0x0000317F File Offset: 0x0000137F
		[ExcludeFromDocs]
		public void RequestCharactersInTexture(string characters, int size)
		{
			this.RequestCharactersInTexture(characters, size, FontStyle.Normal);
		}

		// Token: 0x060000A3 RID: 163 RVA: 0x0000318B File Offset: 0x0000138B
		[ExcludeFromDocs]
		public void RequestCharactersInTexture(string characters)
		{
			this.RequestCharactersInTexture(characters, 0, FontStyle.Normal);
		}

		// Token: 0x04000054 RID: 84
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private static Action<Font> textureRebuilt;

		// Token: 0x04000055 RID: 85
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private Font.FontTextureRebuildCallback m_FontTextureRebuildCallback;

		// Token: 0x02000011 RID: 17
		// (Invoke) Token: 0x060000A5 RID: 165
		public delegate void FontTextureRebuildCallback();
	}
}
