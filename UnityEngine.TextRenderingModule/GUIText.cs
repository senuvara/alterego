﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;

namespace UnityEngine
{
	// Token: 0x0200000A RID: 10
	[Obsolete("This component is part of the legacy UI system and will be removed in a future release.", false)]
	[NativeClass("TextRenderingPrivate::GUIText")]
	[NativeHeader("Runtime/Shaders/Material.h")]
	[NativeHeader("Modules/TextRendering/Public/GUIText.h")]
	public sealed class GUIText : GUIElement
	{
		// Token: 0x06000028 RID: 40 RVA: 0x0000285C File Offset: 0x00000A5C
		public GUIText()
		{
		}

		// Token: 0x1700000A RID: 10
		// (get) Token: 0x06000029 RID: 41
		// (set) Token: 0x0600002A RID: 42
		public extern string text { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700000B RID: 11
		// (get) Token: 0x0600002B RID: 43
		// (set) Token: 0x0600002C RID: 44
		public extern Material material { [FreeFunction("TextRenderingPrivate::GetGUITextMaterialWithFallback", HasExplicitThis = true)] [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700000C RID: 12
		// (get) Token: 0x0600002D RID: 45
		// (set) Token: 0x0600002E RID: 46
		public extern Font font { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700000D RID: 13
		// (get) Token: 0x0600002F RID: 47
		// (set) Token: 0x06000030 RID: 48
		public extern TextAlignment alignment { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700000E RID: 14
		// (get) Token: 0x06000031 RID: 49
		// (set) Token: 0x06000032 RID: 50
		public extern TextAnchor anchor { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700000F RID: 15
		// (get) Token: 0x06000033 RID: 51
		// (set) Token: 0x06000034 RID: 52
		public extern float lineSpacing { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000010 RID: 16
		// (get) Token: 0x06000035 RID: 53
		// (set) Token: 0x06000036 RID: 54
		public extern float tabSize { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000011 RID: 17
		// (get) Token: 0x06000037 RID: 55
		// (set) Token: 0x06000038 RID: 56
		public extern int fontSize { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000012 RID: 18
		// (get) Token: 0x06000039 RID: 57
		// (set) Token: 0x0600003A RID: 58
		public extern FontStyle fontStyle { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000013 RID: 19
		// (get) Token: 0x0600003B RID: 59
		// (set) Token: 0x0600003C RID: 60
		public extern bool richText { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000014 RID: 20
		// (get) Token: 0x0600003D RID: 61 RVA: 0x00002864 File Offset: 0x00000A64
		// (set) Token: 0x0600003E RID: 62 RVA: 0x0000287A File Offset: 0x00000A7A
		public Color color
		{
			get
			{
				Color result;
				this.get_color_Injected(out result);
				return result;
			}
			set
			{
				this.set_color_Injected(ref value);
			}
		}

		// Token: 0x17000015 RID: 21
		// (get) Token: 0x0600003F RID: 63 RVA: 0x00002884 File Offset: 0x00000A84
		// (set) Token: 0x06000040 RID: 64 RVA: 0x0000289A File Offset: 0x00000A9A
		public Vector2 pixelOffset
		{
			get
			{
				Vector2 result;
				this.get_pixelOffset_Injected(out result);
				return result;
			}
			set
			{
				this.set_pixelOffset_Injected(ref value);
			}
		}

		// Token: 0x06000041 RID: 65
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_color_Injected(out Color ret);

		// Token: 0x06000042 RID: 66
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_color_Injected(ref Color value);

		// Token: 0x06000043 RID: 67
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_pixelOffset_Injected(out Vector2 ret);

		// Token: 0x06000044 RID: 68
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_pixelOffset_Injected(ref Vector2 value);
	}
}
