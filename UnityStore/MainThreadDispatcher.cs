﻿using System;
using System.Collections.Generic;

namespace UnityEngine.Store
{
	// Token: 0x02000002 RID: 2
	[HideInInspector]
	internal class MainThreadDispatcher : MonoBehaviour
	{
		// Token: 0x06000001 RID: 1 RVA: 0x00002050 File Offset: 0x00000250
		public static void RunOnMainThread(Action runnable)
		{
			List<Action> obj = MainThreadDispatcher.s_Callbacks;
			lock (obj)
			{
				MainThreadDispatcher.s_Callbacks.Add(runnable);
				MainThreadDispatcher.s_CallbacksPending = true;
			}
		}

		// Token: 0x06000002 RID: 2 RVA: 0x0000209C File Offset: 0x0000029C
		private void Start()
		{
			Object.DontDestroyOnLoad(base.gameObject);
		}

		// Token: 0x06000003 RID: 3 RVA: 0x000020AC File Offset: 0x000002AC
		private void Update()
		{
			bool flag = !MainThreadDispatcher.s_CallbacksPending;
			if (!flag)
			{
				List<Action> obj = MainThreadDispatcher.s_Callbacks;
				Action[] array;
				lock (obj)
				{
					bool flag2 = MainThreadDispatcher.s_Callbacks.Count == 0;
					if (flag2)
					{
						return;
					}
					array = new Action[MainThreadDispatcher.s_Callbacks.Count];
					MainThreadDispatcher.s_Callbacks.CopyTo(array);
					MainThreadDispatcher.s_Callbacks.Clear();
					MainThreadDispatcher.s_CallbacksPending = false;
				}
				foreach (Action action in array)
				{
					action();
				}
			}
		}

		// Token: 0x06000004 RID: 4 RVA: 0x00002160 File Offset: 0x00000360
		public MainThreadDispatcher()
		{
		}

		// Token: 0x06000005 RID: 5 RVA: 0x00002169 File Offset: 0x00000369
		// Note: this type is marked as 'beforefieldinit'.
		static MainThreadDispatcher()
		{
		}

		// Token: 0x04000001 RID: 1
		public static readonly string OBJECT_NAME = "UnityChannelMainThreadDispatcher";

		// Token: 0x04000002 RID: 2
		private static List<Action> s_Callbacks = new List<Action>();

		// Token: 0x04000003 RID: 3
		private static volatile bool s_CallbacksPending;
	}
}
