﻿using System;

namespace UnityEngine.Store
{
	// Token: 0x02000003 RID: 3
	internal class ResultCode
	{
		// Token: 0x06000006 RID: 6 RVA: 0x0000217F File Offset: 0x0000037F
		public ResultCode()
		{
		}

		// Token: 0x06000007 RID: 7 RVA: 0x00002188 File Offset: 0x00000388
		// Note: this type is marked as 'beforefieldinit'.
		static ResultCode()
		{
		}

		// Token: 0x04000004 RID: 4
		public static readonly int SDK_INIT_SUCCESS = 0;

		// Token: 0x04000005 RID: 5
		public static readonly int SDK_INIT_ERROR = -100;

		// Token: 0x04000006 RID: 6
		public static readonly int SDK_NOT_INIT = -101;

		// Token: 0x04000007 RID: 7
		public static readonly int SDK_LOGIN_SUCCESS = 0;

		// Token: 0x04000008 RID: 8
		public static readonly int SDK_LOGIN_CANCEL = -200;

		// Token: 0x04000009 RID: 9
		public static readonly int SDK_LOGIN_FAILED = -201;

		// Token: 0x0400000A RID: 10
		public static readonly int SDK_NOT_LOGIN = -202;

		// Token: 0x0400000B RID: 11
		public static readonly int SDK_PURCHASE_SUCCESS = 0;

		// Token: 0x0400000C RID: 12
		public static readonly int SDK_PURCHASE_FAILED = -300;

		// Token: 0x0400000D RID: 13
		public static readonly int SDK_PURCHASE_CANCEL = -301;

		// Token: 0x0400000E RID: 14
		public static readonly int SDK_PURCHASE_REPEAT = -302;

		// Token: 0x0400000F RID: 15
		public static readonly int SDK_RECEIPT_VALIDATE_SUCCESS = 0;

		// Token: 0x04000010 RID: 16
		public static readonly int SDK_RECEIPT_VALIDATE_FAILED = -400;

		// Token: 0x04000011 RID: 17
		public static readonly int SDK_RECEIPT_VALIDATE_ERROR = -401;

		// Token: 0x04000012 RID: 18
		public static readonly int SDK_CONFIRM_PURCHASE_SUCCESS = 0;

		// Token: 0x04000013 RID: 19
		public static readonly int SDK_CONFIRM_PURCHASE_FAILED = -501;

		// Token: 0x04000014 RID: 20
		public static readonly int SDK_CONFIRM_PURCHASE_ERROR = -502;

		// Token: 0x04000015 RID: 21
		public static readonly int SDK_SERVER_INVALID = -999;
	}
}
