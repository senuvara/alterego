﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace UnityEngine.Store
{
	// Token: 0x02000004 RID: 4
	public class AppInfo
	{
		// Token: 0x17000001 RID: 1
		// (get) Token: 0x06000008 RID: 8 RVA: 0x0000222F File Offset: 0x0000042F
		// (set) Token: 0x06000009 RID: 9 RVA: 0x00002237 File Offset: 0x00000437
		public string appId
		{
			[CompilerGenerated]
			get
			{
				return this.<appId>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<appId>k__BackingField = value;
			}
		}

		// Token: 0x17000002 RID: 2
		// (get) Token: 0x0600000A RID: 10 RVA: 0x00002240 File Offset: 0x00000440
		// (set) Token: 0x0600000B RID: 11 RVA: 0x00002248 File Offset: 0x00000448
		public string appKey
		{
			[CompilerGenerated]
			get
			{
				return this.<appKey>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<appKey>k__BackingField = value;
			}
		}

		// Token: 0x17000003 RID: 3
		// (get) Token: 0x0600000C RID: 12 RVA: 0x00002251 File Offset: 0x00000451
		// (set) Token: 0x0600000D RID: 13 RVA: 0x00002259 File Offset: 0x00000459
		public string clientId
		{
			[CompilerGenerated]
			get
			{
				return this.<clientId>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<clientId>k__BackingField = value;
			}
		}

		// Token: 0x17000004 RID: 4
		// (get) Token: 0x0600000E RID: 14 RVA: 0x00002262 File Offset: 0x00000462
		// (set) Token: 0x0600000F RID: 15 RVA: 0x0000226A File Offset: 0x0000046A
		public string clientKey
		{
			[CompilerGenerated]
			get
			{
				return this.<clientKey>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<clientKey>k__BackingField = value;
			}
		}

		// Token: 0x17000005 RID: 5
		// (get) Token: 0x06000010 RID: 16 RVA: 0x00002273 File Offset: 0x00000473
		// (set) Token: 0x06000011 RID: 17 RVA: 0x0000227B File Offset: 0x0000047B
		public bool debug
		{
			[CompilerGenerated]
			get
			{
				return this.<debug>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<debug>k__BackingField = value;
			}
		}

		// Token: 0x06000012 RID: 18 RVA: 0x0000217F File Offset: 0x0000037F
		public AppInfo()
		{
		}

		// Token: 0x04000016 RID: 22
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private string <appId>k__BackingField;

		// Token: 0x04000017 RID: 23
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private string <appKey>k__BackingField;

		// Token: 0x04000018 RID: 24
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private string <clientId>k__BackingField;

		// Token: 0x04000019 RID: 25
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private string <clientKey>k__BackingField;

		// Token: 0x0400001A RID: 26
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private bool <debug>k__BackingField;
	}
}
