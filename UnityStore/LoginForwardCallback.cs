﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine.Store
{
	// Token: 0x02000007 RID: 7
	public class LoginForwardCallback : AndroidJavaProxy
	{
		// Token: 0x0600001B RID: 27 RVA: 0x000023BC File Offset: 0x000005BC
		public LoginForwardCallback(ILoginListener loginListener) : base("com.unity.channel.sdk.LoginCallback")
		{
			this.loginListener = loginListener;
		}

		// Token: 0x0600001C RID: 28 RVA: 0x000023D4 File Offset: 0x000005D4
		public void onInitFinished(int resultCode)
		{
			bool flag = this.loginListener == null;
			if (!flag)
			{
				bool flag2 = resultCode == ResultCode.SDK_INIT_SUCCESS;
				if (flag2)
				{
					MainThreadDispatcher.RunOnMainThread(delegate
					{
						this.loginListener.OnInitialized();
					});
				}
				else
				{
					MainThreadDispatcher.RunOnMainThread(delegate
					{
						this.loginListener.OnInitializeFailed("Init Failed: " + resultCode.ToString());
					});
				}
			}
		}

		// Token: 0x0600001D RID: 29 RVA: 0x00002444 File Offset: 0x00000644
		public void onLoginFinished(int resultCode, AndroidJavaObject jo)
		{
			bool flag = this.loginListener == null;
			if (!flag)
			{
				bool flag2 = resultCode == ResultCode.SDK_LOGIN_SUCCESS;
				if (flag2)
				{
					UserInfo userInfo = new UserInfo();
					userInfo.channel = jo.Call<string>("getChannel", new object[0]);
					string str = jo.Call<string>("getUserId", new object[0]);
					userInfo.userId = userInfo.channel + "_" + str;
					userInfo.userLoginToken = jo.Call<string>("getLoginReceipt", new object[0]);
					MainThreadDispatcher.RunOnMainThread(delegate
					{
						this.loginListener.OnLogin(userInfo);
					});
				}
				else
				{
					bool flag3 = resultCode == ResultCode.SDK_LOGIN_CANCEL;
					if (flag3)
					{
						MainThreadDispatcher.RunOnMainThread(delegate
						{
							this.loginListener.OnLoginFailed("User Login Cancel");
						});
					}
					else
					{
						MainThreadDispatcher.RunOnMainThread(delegate
						{
							this.loginListener.OnLoginFailed("User Login Failed: " + resultCode.ToString());
						});
					}
				}
			}
		}

		// Token: 0x0600001E RID: 30 RVA: 0x00002568 File Offset: 0x00000768
		[CompilerGenerated]
		private void <onInitFinished>b__2_0()
		{
			this.loginListener.OnInitialized();
		}

		// Token: 0x0600001F RID: 31 RVA: 0x00002577 File Offset: 0x00000777
		[CompilerGenerated]
		private void <onLoginFinished>b__3_1()
		{
			this.loginListener.OnLoginFailed("User Login Cancel");
		}

		// Token: 0x0400001C RID: 28
		private ILoginListener loginListener;

		// Token: 0x02000009 RID: 9
		[CompilerGenerated]
		private sealed class <>c__DisplayClass2_0
		{
			// Token: 0x06000027 RID: 39 RVA: 0x0000217F File Offset: 0x0000037F
			public <>c__DisplayClass2_0()
			{
			}

			// Token: 0x06000028 RID: 40 RVA: 0x000025BE File Offset: 0x000007BE
			internal void <onInitFinished>b__1()
			{
				this.<>4__this.loginListener.OnInitializeFailed("Init Failed: " + this.resultCode.ToString());
			}

			// Token: 0x04000020 RID: 32
			public int resultCode;

			// Token: 0x04000021 RID: 33
			public LoginForwardCallback <>4__this;
		}

		// Token: 0x0200000A RID: 10
		[CompilerGenerated]
		private sealed class <>c__DisplayClass3_0
		{
			// Token: 0x06000029 RID: 41 RVA: 0x0000217F File Offset: 0x0000037F
			public <>c__DisplayClass3_0()
			{
			}

			// Token: 0x0600002A RID: 42 RVA: 0x000025E7 File Offset: 0x000007E7
			internal void <onLoginFinished>b__0()
			{
				this.CS$<>8__locals1.<>4__this.loginListener.OnLogin(this.userInfo);
			}

			// Token: 0x04000022 RID: 34
			public UserInfo userInfo;

			// Token: 0x04000023 RID: 35
			public LoginForwardCallback.<>c__DisplayClass3_1 CS$<>8__locals1;
		}

		// Token: 0x0200000B RID: 11
		[CompilerGenerated]
		private sealed class <>c__DisplayClass3_1
		{
			// Token: 0x0600002B RID: 43 RVA: 0x0000217F File Offset: 0x0000037F
			public <>c__DisplayClass3_1()
			{
			}

			// Token: 0x0600002C RID: 44 RVA: 0x00002606 File Offset: 0x00000806
			internal void <onLoginFinished>b__2()
			{
				this.<>4__this.loginListener.OnLoginFailed("User Login Failed: " + this.resultCode.ToString());
			}

			// Token: 0x04000024 RID: 36
			public int resultCode;

			// Token: 0x04000025 RID: 37
			public LoginForwardCallback <>4__this;
		}
	}
}
