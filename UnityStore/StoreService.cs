﻿using System;

namespace UnityEngine.Store
{
	// Token: 0x02000005 RID: 5
	public class StoreService
	{
		// Token: 0x06000013 RID: 19 RVA: 0x00002284 File Offset: 0x00000484
		public static void Initialize(AppInfo appInfo, ILoginListener listener)
		{
			bool flag = GameObject.Find(MainThreadDispatcher.OBJECT_NAME) == null;
			if (flag)
			{
				GameObject gameObject = new GameObject(MainThreadDispatcher.OBJECT_NAME);
				Object.DontDestroyOnLoad(gameObject);
				gameObject.hideFlags = (HideFlags.HideInHierarchy | HideFlags.HideInInspector);
				gameObject.AddComponent<MainThreadDispatcher>();
			}
			AndroidJavaClass androidJavaClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
			AndroidJavaObject @static = androidJavaClass.GetStatic<AndroidJavaObject>("currentActivity");
			LoginForwardCallback loginForwardCallback = new LoginForwardCallback(listener);
			AndroidJavaObject androidJavaObject = new AndroidJavaObject("com.unity.channel.sdk.AppInfo", new object[0]);
			androidJavaObject.Set<string>("appId", appInfo.appId);
			androidJavaObject.Set<string>("appKey", appInfo.appKey);
			androidJavaObject.Set<string>("clientId", appInfo.clientId);
			androidJavaObject.Set<string>("clientSecret", appInfo.clientKey);
			androidJavaObject.Set<bool>("debug", appInfo.debug);
			StoreService.serviceClass.CallStatic("init", new object[]
			{
				@static,
				androidJavaObject,
				loginForwardCallback
			});
		}

		// Token: 0x06000014 RID: 20 RVA: 0x0000237C File Offset: 0x0000057C
		public static void Login(ILoginListener listener)
		{
			LoginForwardCallback loginForwardCallback = new LoginForwardCallback(listener);
			StoreService.serviceClass.CallStatic("login", new object[]
			{
				loginForwardCallback
			});
		}

		// Token: 0x06000015 RID: 21 RVA: 0x0000217F File Offset: 0x0000037F
		public StoreService()
		{
		}

		// Token: 0x06000016 RID: 22 RVA: 0x000023AB File Offset: 0x000005AB
		// Note: this type is marked as 'beforefieldinit'.
		static StoreService()
		{
		}

		// Token: 0x0400001B RID: 27
		private static AndroidJavaClass serviceClass = new AndroidJavaClass("com.unity.channel.sdk.ChannelService");
	}
}
