﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace UnityEngine.Store
{
	// Token: 0x02000008 RID: 8
	public class UserInfo
	{
		// Token: 0x17000006 RID: 6
		// (get) Token: 0x06000020 RID: 32 RVA: 0x0000258B File Offset: 0x0000078B
		// (set) Token: 0x06000021 RID: 33 RVA: 0x00002593 File Offset: 0x00000793
		public string channel
		{
			[CompilerGenerated]
			get
			{
				return this.<channel>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<channel>k__BackingField = value;
			}
		}

		// Token: 0x17000007 RID: 7
		// (get) Token: 0x06000022 RID: 34 RVA: 0x0000259C File Offset: 0x0000079C
		// (set) Token: 0x06000023 RID: 35 RVA: 0x000025A4 File Offset: 0x000007A4
		public string userId
		{
			[CompilerGenerated]
			get
			{
				return this.<userId>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<userId>k__BackingField = value;
			}
		}

		// Token: 0x17000008 RID: 8
		// (get) Token: 0x06000024 RID: 36 RVA: 0x000025AD File Offset: 0x000007AD
		// (set) Token: 0x06000025 RID: 37 RVA: 0x000025B5 File Offset: 0x000007B5
		public string userLoginToken
		{
			[CompilerGenerated]
			get
			{
				return this.<userLoginToken>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<userLoginToken>k__BackingField = value;
			}
		}

		// Token: 0x06000026 RID: 38 RVA: 0x0000217F File Offset: 0x0000037F
		public UserInfo()
		{
		}

		// Token: 0x0400001D RID: 29
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private string <channel>k__BackingField;

		// Token: 0x0400001E RID: 30
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private string <userId>k__BackingField;

		// Token: 0x0400001F RID: 31
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private string <userLoginToken>k__BackingField;
	}
}
