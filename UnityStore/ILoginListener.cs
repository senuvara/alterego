﻿using System;

namespace UnityEngine.Store
{
	// Token: 0x02000006 RID: 6
	public interface ILoginListener
	{
		// Token: 0x06000017 RID: 23
		void OnInitialized();

		// Token: 0x06000018 RID: 24
		void OnInitializeFailed(string message);

		// Token: 0x06000019 RID: 25
		void OnLogin(UserInfo userInfo);

		// Token: 0x0600001A RID: 26
		void OnLoginFailed(string message);
	}
}
