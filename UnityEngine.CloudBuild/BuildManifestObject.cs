﻿using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine.CloudBuild.MiniJSON;

namespace UnityEngine.CloudBuild
{
	// Token: 0x02000002 RID: 2
	[CLSCompliant(false)]
	public class BuildManifestObject : ScriptableObject
	{
		// Token: 0x06000001 RID: 1 RVA: 0x00002050 File Offset: 0x00000250
		private void Awake()
		{
			Dictionary<string, object> dictionary = Json.Deserialize(this.m_manifestJson) as Dictionary<string, object>;
			if (dictionary != null)
			{
				this.m_manifestDict = dictionary;
			}
		}

		// Token: 0x06000002 RID: 2 RVA: 0x00002078 File Offset: 0x00000278
		public static BuildManifestObject LoadCloudBuildManifest()
		{
			return BuildManifestObject.LoadFromResourcesOrCreateNew("UnityCloudBuildManifest.scriptable");
		}

		// Token: 0x06000003 RID: 3 RVA: 0x00002084 File Offset: 0x00000284
		public static BuildManifestObject LoadFromResourcesOrCreateNew(string resourcesPath)
		{
			BuildManifestObject buildManifestObject = (BuildManifestObject)Resources.Load(resourcesPath, typeof(BuildManifestObject));
			if (buildManifestObject == null)
			{
				Debug.Log(string.Format("WARNING: Manifest not found at {0}, returning empty manifest object", resourcesPath));
				buildManifestObject = ScriptableObject.CreateInstance<BuildManifestObject>();
			}
			return buildManifestObject;
		}

		// Token: 0x06000004 RID: 4 RVA: 0x000020C7 File Offset: 0x000002C7
		public void SetValue(string key, object value)
		{
			this.m_manifestDict[key] = value;
			this.m_manifestJson = Json.Serialize(this.m_manifestDict);
		}

		// Token: 0x06000005 RID: 5 RVA: 0x000020E8 File Offset: 0x000002E8
		public void SetValues(Dictionary<string, object> sourceDict)
		{
			if (sourceDict == null)
			{
				return;
			}
			foreach (KeyValuePair<string, object> keyValuePair in sourceDict)
			{
				this.SetValue(keyValuePair.Key, keyValuePair.Value);
			}
		}

		// Token: 0x06000006 RID: 6 RVA: 0x00002148 File Offset: 0x00000348
		public bool TryGetValue<T>(string key, out T result)
		{
			result = default(T);
			if (this.m_manifestDict.ContainsKey(key))
			{
				try
				{
					object value = this.m_manifestDict[key];
					result = (T)((object)Convert.ChangeType(value, typeof(T)));
					return true;
				}
				catch
				{
				}
				return false;
			}
			return false;
		}

		// Token: 0x06000007 RID: 7 RVA: 0x000021AC File Offset: 0x000003AC
		public T GetValue<T>(string key)
		{
			T result = default(T);
			if (!this.TryGetValue<T>(key, out result))
			{
				throw new KeyNotFoundException(string.Format("'{0}' not found.", key));
			}
			return result;
		}

		// Token: 0x06000008 RID: 8 RVA: 0x000021E0 File Offset: 0x000003E0
		[Obsolete("Use GetValue<type>(string, object) instead.")]
		public string GetValue(string key, string defaultValue)
		{
			string result = defaultValue;
			if (this.m_manifestDict.ContainsKey(key))
			{
				result = this.m_manifestDict[key].ToString();
			}
			return result;
		}

		// Token: 0x06000009 RID: 9 RVA: 0x00002210 File Offset: 0x00000410
		[Obsolete("Use GetValue<type>(string) instead.")]
		public string GetValue(string key)
		{
			string value = this.GetValue(key, null);
			if (value == null)
			{
				throw new KeyNotFoundException(string.Format("'{0}' not found.", key));
			}
			return value;
		}

		// Token: 0x0600000A RID: 10 RVA: 0x0000222E File Offset: 0x0000042E
		public void ClearValues()
		{
			this.m_manifestDict.Clear();
			this.m_manifestJson = "{}";
		}

		// Token: 0x0600000B RID: 11 RVA: 0x00002246 File Offset: 0x00000446
		public Dictionary<string, object> ToDictionary()
		{
			return this.m_manifestDict;
		}

		// Token: 0x0600000C RID: 12 RVA: 0x0000224E File Offset: 0x0000044E
		public string ToJson()
		{
			return Json.Serialize(this.m_manifestDict);
		}

		// Token: 0x0600000D RID: 13 RVA: 0x0000225C File Offset: 0x0000045C
		public override string ToString()
		{
			StringBuilder stringBuilder = new StringBuilder("[BuildManifestObject]\n");
			foreach (KeyValuePair<string, object> keyValuePair in this.m_manifestDict)
			{
				stringBuilder.AppendFormat("{0}={1}\n", keyValuePair.Key, keyValuePair.Value);
			}
			return stringBuilder.ToString();
		}

		// Token: 0x0600000E RID: 14 RVA: 0x000022D4 File Offset: 0x000004D4
		public BuildManifestObject()
		{
		}

		// Token: 0x04000001 RID: 1
		private const string MANIFEST_PATH = "UnityCloudBuildManifest.scriptable";

		// Token: 0x04000002 RID: 2
		[SerializeField]
		private string m_manifestJson = "{}";

		// Token: 0x04000003 RID: 3
		private Dictionary<string, object> m_manifestDict = new Dictionary<string, object>();
	}
}
