﻿using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyVersion("1.0.7062.34603")]
[assembly: AssemblyTitle("UnityEngine.CloudBuild")]
[assembly: AssemblyDescription("Internal Cloud Build assembly for supporting legacy Unity versions")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Unity Technologies")]
[assembly: AssemblyProduct("Cloud Build")]
[assembly: AssemblyCopyright("Copyright © Unity Technologies 2015")]
[assembly: AssemblyTrademark("")]
[assembly: ComVisible(false)]
[assembly: Guid("ed69fe13-4633-4d96-8abb-5526ce114353")]
[assembly: CLSCompliant(true)]
