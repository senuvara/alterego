﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000002 RID: 2
	internal interface INativeAppleStore : INativeStore
	{
		// Token: 0x06000001 RID: 1
		void SetUnityPurchasingCallback(UnityPurchasingCallback AsyncCallback);

		// Token: 0x06000002 RID: 2
		void RestoreTransactions();

		// Token: 0x06000003 RID: 3
		void RefreshAppReceipt();

		// Token: 0x06000004 RID: 4
		void AddTransactionObserver();

		// Token: 0x06000005 RID: 5
		void SetApplicationUsername(string applicationUsername);

		// Token: 0x17000001 RID: 1
		// (get) Token: 0x06000006 RID: 6
		string appReceipt { get; }

		// Token: 0x17000002 RID: 2
		// (get) Token: 0x06000007 RID: 7
		bool canMakePayments { get; }

		// Token: 0x17000003 RID: 3
		// (get) Token: 0x06000008 RID: 8
		// (set) Token: 0x06000009 RID: 9
		bool simulateAskToBuy { get; set; }

		// Token: 0x0600000A RID: 10
		void SetStorePromotionOrder(string json);

		// Token: 0x0600000B RID: 11
		void SetStorePromotionVisibility(string productId, string visibility);

		// Token: 0x0600000C RID: 12
		string GetTransactionReceiptForProductId(string productId);

		// Token: 0x0600000D RID: 13
		void InterceptPromotionalPurchases();

		// Token: 0x0600000E RID: 14
		void ContinuePromotionalPurchases();
	}
}
