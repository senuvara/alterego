﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000004 RID: 4
	internal class OSXStoreBindings : INativeAppleStore, INativeStore
	{
		// Token: 0x06000021 RID: 33 RVA: 0x00002050 File Offset: 0x00000250
		public void SetUnityPurchasingCallback(UnityPurchasingCallback AsyncCallback)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06000022 RID: 34 RVA: 0x00002050 File Offset: 0x00000250
		public void RestoreTransactions()
		{
			throw new NotImplementedException();
		}

		// Token: 0x06000023 RID: 35 RVA: 0x00002050 File Offset: 0x00000250
		public void RefreshAppReceipt()
		{
			throw new NotImplementedException();
		}

		// Token: 0x06000024 RID: 36 RVA: 0x00002050 File Offset: 0x00000250
		public void AddTransactionObserver()
		{
			throw new NotImplementedException();
		}

		// Token: 0x06000025 RID: 37 RVA: 0x00002050 File Offset: 0x00000250
		public void SetApplicationUsername(string applicationUsername)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06000026 RID: 38 RVA: 0x00002050 File Offset: 0x00000250
		public void RetrieveProducts(string json)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06000027 RID: 39 RVA: 0x00002050 File Offset: 0x00000250
		public void Purchase(string productJSON, string developerPayload)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06000028 RID: 40 RVA: 0x00002050 File Offset: 0x00000250
		public void FinishTransaction(string productJSON, string transactionID)
		{
			throw new NotImplementedException();
		}

		// Token: 0x17000007 RID: 7
		// (get) Token: 0x06000029 RID: 41 RVA: 0x00002050 File Offset: 0x00000250
		public string appReceipt
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		// Token: 0x17000008 RID: 8
		// (get) Token: 0x0600002A RID: 42 RVA: 0x00002050 File Offset: 0x00000250
		public bool canMakePayments
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		// Token: 0x17000009 RID: 9
		// (get) Token: 0x0600002B RID: 43 RVA: 0x00002050 File Offset: 0x00000250
		// (set) Token: 0x0600002C RID: 44 RVA: 0x00002050 File Offset: 0x00000250
		public bool simulateAskToBuy
		{
			get
			{
				throw new NotImplementedException();
			}
			set
			{
				throw new NotImplementedException();
			}
		}

		// Token: 0x0600002D RID: 45 RVA: 0x00002050 File Offset: 0x00000250
		public void SetStorePromotionOrder(string json)
		{
			throw new NotImplementedException();
		}

		// Token: 0x0600002E RID: 46 RVA: 0x00002050 File Offset: 0x00000250
		public void SetStorePromotionVisibility(string productId, string visibility)
		{
			throw new NotImplementedException();
		}

		// Token: 0x0600002F RID: 47 RVA: 0x00002050 File Offset: 0x00000250
		public string GetTransactionReceiptForProductId(string productId)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06000030 RID: 48 RVA: 0x00002050 File Offset: 0x00000250
		public void InterceptPromotionalPurchases()
		{
			throw new NotImplementedException();
		}

		// Token: 0x06000031 RID: 49 RVA: 0x00002050 File Offset: 0x00000250
		public void ContinuePromotionalPurchases()
		{
			throw new NotImplementedException();
		}

		// Token: 0x06000032 RID: 50 RVA: 0x00002058 File Offset: 0x00000258
		public OSXStoreBindings()
		{
		}
	}
}
