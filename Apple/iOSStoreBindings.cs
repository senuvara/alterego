﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000003 RID: 3
	internal class iOSStoreBindings : INativeAppleStore, INativeStore
	{
		// Token: 0x0600000F RID: 15 RVA: 0x00002050 File Offset: 0x00000250
		public void SetUnityPurchasingCallback(UnityPurchasingCallback AsyncCallback)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06000010 RID: 16 RVA: 0x00002050 File Offset: 0x00000250
		public void RestoreTransactions()
		{
			throw new NotImplementedException();
		}

		// Token: 0x06000011 RID: 17 RVA: 0x00002050 File Offset: 0x00000250
		public void RefreshAppReceipt()
		{
			throw new NotImplementedException();
		}

		// Token: 0x06000012 RID: 18 RVA: 0x00002050 File Offset: 0x00000250
		public void AddTransactionObserver()
		{
			throw new NotImplementedException();
		}

		// Token: 0x06000013 RID: 19 RVA: 0x00002050 File Offset: 0x00000250
		public void SetApplicationUsername(string applicationUsername)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06000014 RID: 20 RVA: 0x00002050 File Offset: 0x00000250
		public void RetrieveProducts(string json)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06000015 RID: 21 RVA: 0x00002050 File Offset: 0x00000250
		public void Purchase(string productJSON, string developerPayload)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06000016 RID: 22 RVA: 0x00002050 File Offset: 0x00000250
		public void FinishTransaction(string productJSON, string transactionID)
		{
			throw new NotImplementedException();
		}

		// Token: 0x17000004 RID: 4
		// (get) Token: 0x06000017 RID: 23 RVA: 0x00002050 File Offset: 0x00000250
		public string appReceipt
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		// Token: 0x17000005 RID: 5
		// (get) Token: 0x06000018 RID: 24 RVA: 0x00002050 File Offset: 0x00000250
		public bool canMakePayments
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		// Token: 0x17000006 RID: 6
		// (get) Token: 0x06000019 RID: 25 RVA: 0x00002050 File Offset: 0x00000250
		// (set) Token: 0x0600001A RID: 26 RVA: 0x00002050 File Offset: 0x00000250
		public bool simulateAskToBuy
		{
			get
			{
				throw new NotImplementedException();
			}
			set
			{
				throw new NotImplementedException();
			}
		}

		// Token: 0x0600001B RID: 27 RVA: 0x00002050 File Offset: 0x00000250
		public void SetStorePromotionOrder(string json)
		{
			throw new NotImplementedException();
		}

		// Token: 0x0600001C RID: 28 RVA: 0x00002050 File Offset: 0x00000250
		public void SetStorePromotionVisibility(string productId, string visibility)
		{
			throw new NotImplementedException();
		}

		// Token: 0x0600001D RID: 29 RVA: 0x00002050 File Offset: 0x00000250
		public string GetTransactionReceiptForProductId(string productId)
		{
			throw new NotImplementedException();
		}

		// Token: 0x0600001E RID: 30 RVA: 0x00002050 File Offset: 0x00000250
		public void InterceptPromotionalPurchases()
		{
			throw new NotImplementedException();
		}

		// Token: 0x0600001F RID: 31 RVA: 0x00002050 File Offset: 0x00000250
		public void ContinuePromotionalPurchases()
		{
			throw new NotImplementedException();
		}

		// Token: 0x06000020 RID: 32 RVA: 0x00002058 File Offset: 0x00000258
		public iOSStoreBindings()
		{
		}
	}
}
