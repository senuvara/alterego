﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000025 RID: 37
	[NativeHeader("Modules/IMGUI/GUIStyle.bindings.h")]
	[NativeHeader("IMGUIScriptingClasses.h")]
	[RequiredByNativeCode]
	[Serializable]
	[StructLayout(LayoutKind.Sequential)]
	public sealed class GUIStyle
	{
		// Token: 0x060002AB RID: 683 RVA: 0x0000A27C File Offset: 0x0000847C
		public GUIStyle()
		{
			this.m_Ptr = GUIStyle.Internal_Create(this);
		}

		// Token: 0x060002AC RID: 684 RVA: 0x0000A291 File Offset: 0x00008491
		public GUIStyle(GUIStyle other)
		{
			if (other == null)
			{
				Debug.LogError("Copied style is null. Using StyleNotFound instead.");
				other = GUISkin.error;
			}
			this.m_Ptr = GUIStyle.Internal_Copy(this, other);
		}

		// Token: 0x17000059 RID: 89
		// (get) Token: 0x060002AD RID: 685
		// (set) Token: 0x060002AE RID: 686
		[NativeProperty("Name", false, TargetType.Function)]
		public extern string name { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700005A RID: 90
		// (get) Token: 0x060002AF RID: 687
		// (set) Token: 0x060002B0 RID: 688
		[NativeProperty("Font", false, TargetType.Function)]
		public extern Font font { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700005B RID: 91
		// (get) Token: 0x060002B1 RID: 689
		// (set) Token: 0x060002B2 RID: 690
		[NativeProperty("m_ImagePosition", false, TargetType.Field)]
		public extern ImagePosition imagePosition { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700005C RID: 92
		// (get) Token: 0x060002B3 RID: 691
		// (set) Token: 0x060002B4 RID: 692
		[NativeProperty("m_Alignment", false, TargetType.Field)]
		public extern TextAnchor alignment { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700005D RID: 93
		// (get) Token: 0x060002B5 RID: 693
		// (set) Token: 0x060002B6 RID: 694
		[NativeProperty("m_WordWrap", false, TargetType.Field)]
		public extern bool wordWrap { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700005E RID: 94
		// (get) Token: 0x060002B7 RID: 695
		// (set) Token: 0x060002B8 RID: 696
		[NativeProperty("m_Clipping", false, TargetType.Field)]
		public extern TextClipping clipping { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700005F RID: 95
		// (get) Token: 0x060002B9 RID: 697 RVA: 0x0000A2C0 File Offset: 0x000084C0
		// (set) Token: 0x060002BA RID: 698 RVA: 0x0000A2D6 File Offset: 0x000084D6
		[NativeProperty("m_ContentOffset", false, TargetType.Field)]
		public Vector2 contentOffset
		{
			get
			{
				Vector2 result;
				this.get_contentOffset_Injected(out result);
				return result;
			}
			set
			{
				this.set_contentOffset_Injected(ref value);
			}
		}

		// Token: 0x17000060 RID: 96
		// (get) Token: 0x060002BB RID: 699
		// (set) Token: 0x060002BC RID: 700
		[NativeProperty("m_FixedWidth", false, TargetType.Field)]
		public extern float fixedWidth { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000061 RID: 97
		// (get) Token: 0x060002BD RID: 701
		// (set) Token: 0x060002BE RID: 702
		[NativeProperty("m_FixedHeight", false, TargetType.Field)]
		public extern float fixedHeight { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000062 RID: 98
		// (get) Token: 0x060002BF RID: 703
		// (set) Token: 0x060002C0 RID: 704
		[NativeProperty("m_StretchWidth", false, TargetType.Field)]
		public extern bool stretchWidth { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000063 RID: 99
		// (get) Token: 0x060002C1 RID: 705
		// (set) Token: 0x060002C2 RID: 706
		[NativeProperty("m_StretchHeight", false, TargetType.Field)]
		public extern bool stretchHeight { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000064 RID: 100
		// (get) Token: 0x060002C3 RID: 707
		// (set) Token: 0x060002C4 RID: 708
		[NativeProperty("m_FontSize", false, TargetType.Field)]
		public extern int fontSize { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000065 RID: 101
		// (get) Token: 0x060002C5 RID: 709
		// (set) Token: 0x060002C6 RID: 710
		[NativeProperty("m_FontStyle", false, TargetType.Field)]
		public extern FontStyle fontStyle { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000066 RID: 102
		// (get) Token: 0x060002C7 RID: 711
		// (set) Token: 0x060002C8 RID: 712
		[NativeProperty("m_RichText", false, TargetType.Field)]
		public extern bool richText { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000067 RID: 103
		// (get) Token: 0x060002C9 RID: 713 RVA: 0x0000A2E0 File Offset: 0x000084E0
		// (set) Token: 0x060002CA RID: 714 RVA: 0x0000A2F6 File Offset: 0x000084F6
		[NativeProperty("m_ClipOffset", false, TargetType.Field)]
		[Obsolete("Don't use clipOffset - put things inside BeginGroup instead. This functionality will be removed in a later version.", false)]
		public Vector2 clipOffset
		{
			get
			{
				Vector2 result;
				this.get_clipOffset_Injected(out result);
				return result;
			}
			set
			{
				this.set_clipOffset_Injected(ref value);
			}
		}

		// Token: 0x17000068 RID: 104
		// (get) Token: 0x060002CB RID: 715 RVA: 0x0000A300 File Offset: 0x00008500
		// (set) Token: 0x060002CC RID: 716 RVA: 0x0000A316 File Offset: 0x00008516
		[NativeProperty("m_ClipOffset", false, TargetType.Field)]
		internal Vector2 Internal_clipOffset
		{
			get
			{
				Vector2 result;
				this.get_Internal_clipOffset_Injected(out result);
				return result;
			}
			set
			{
				this.set_Internal_clipOffset_Injected(ref value);
			}
		}

		// Token: 0x060002CD RID: 717
		[FreeFunction(Name = "GUIStyle_Bindings::Internal_Create", IsThreadSafe = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern IntPtr Internal_Create(GUIStyle self);

		// Token: 0x060002CE RID: 718
		[FreeFunction(Name = "GUIStyle_Bindings::Internal_Copy", IsThreadSafe = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern IntPtr Internal_Copy(GUIStyle self, GUIStyle other);

		// Token: 0x060002CF RID: 719
		[FreeFunction(Name = "GUIStyle_Bindings::Internal_Destroy", IsThreadSafe = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void Internal_Destroy(IntPtr self);

		// Token: 0x060002D0 RID: 720
		[FreeFunction(Name = "GUIStyle_Bindings::GetStyleStatePtr", IsThreadSafe = true, HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern IntPtr GetStyleStatePtr(int idx);

		// Token: 0x060002D1 RID: 721
		[FreeFunction(Name = "GUIStyle_Bindings::AssignStyleState", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void AssignStyleState(int idx, IntPtr srcStyleState);

		// Token: 0x060002D2 RID: 722
		[FreeFunction(Name = "GUIStyle_Bindings::GetRectOffsetPtr", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern IntPtr GetRectOffsetPtr(int idx);

		// Token: 0x060002D3 RID: 723
		[FreeFunction(Name = "GUIStyle_Bindings::AssignRectOffset", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void AssignRectOffset(int idx, IntPtr srcRectOffset);

		// Token: 0x060002D4 RID: 724
		[FreeFunction(Name = "GUIStyle_Bindings::Internal_GetLineHeight")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern float Internal_GetLineHeight(IntPtr target);

		// Token: 0x060002D5 RID: 725 RVA: 0x0000A320 File Offset: 0x00008520
		[FreeFunction(Name = "GUIStyle_Bindings::Internal_Draw", HasExplicitThis = true)]
		private void Internal_Draw(Rect screenRect, GUIContent content, bool isHover, bool isActive, bool on, bool hasKeyboardFocus)
		{
			this.Internal_Draw_Injected(ref screenRect, content, isHover, isActive, on, hasKeyboardFocus);
		}

		// Token: 0x060002D6 RID: 726 RVA: 0x0000A332 File Offset: 0x00008532
		[FreeFunction(Name = "GUIStyle_Bindings::Internal_Draw2", HasExplicitThis = true)]
		private void Internal_Draw2(Rect position, GUIContent content, int controlID, bool on)
		{
			this.Internal_Draw2_Injected(ref position, content, controlID, on);
		}

		// Token: 0x060002D7 RID: 727 RVA: 0x0000A340 File Offset: 0x00008540
		[FreeFunction(Name = "GUIStyle_Bindings::Internal_DrawCursor", HasExplicitThis = true)]
		private void Internal_DrawCursor(Rect position, GUIContent content, int pos, Color cursorColor)
		{
			this.Internal_DrawCursor_Injected(ref position, content, pos, ref cursorColor);
		}

		// Token: 0x060002D8 RID: 728 RVA: 0x0000A350 File Offset: 0x00008550
		[FreeFunction(Name = "GUIStyle_Bindings::Internal_DrawWithTextSelection", HasExplicitThis = true)]
		private void Internal_DrawWithTextSelection(Rect screenRect, GUIContent content, bool isHover, bool isActive, bool on, bool hasKeyboardFocus, bool drawSelectionAsComposition, int cursorFirst, int cursorLast, Color cursorColor, Color selectionColor)
		{
			this.Internal_DrawWithTextSelection_Injected(ref screenRect, content, isHover, isActive, on, hasKeyboardFocus, drawSelectionAsComposition, cursorFirst, cursorLast, ref cursorColor, ref selectionColor);
		}

		// Token: 0x060002D9 RID: 729 RVA: 0x0000A378 File Offset: 0x00008578
		[FreeFunction(Name = "GUIStyle_Bindings::Internal_GetCursorPixelPosition", HasExplicitThis = true)]
		internal Vector2 Internal_GetCursorPixelPosition(Rect position, GUIContent content, int cursorStringIndex)
		{
			Vector2 result;
			this.Internal_GetCursorPixelPosition_Injected(ref position, content, cursorStringIndex, out result);
			return result;
		}

		// Token: 0x060002DA RID: 730 RVA: 0x0000A392 File Offset: 0x00008592
		[FreeFunction(Name = "GUIStyle_Bindings::Internal_GetCursorStringIndex", HasExplicitThis = true)]
		internal int Internal_GetCursorStringIndex(Rect position, GUIContent content, Vector2 cursorPixelPosition)
		{
			return this.Internal_GetCursorStringIndex_Injected(ref position, content, ref cursorPixelPosition);
		}

		// Token: 0x060002DB RID: 731
		[FreeFunction(Name = "GUIStyle_Bindings::Internal_GetNumCharactersThatFitWithinWidth", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern int Internal_GetNumCharactersThatFitWithinWidth(string text, float width);

		// Token: 0x060002DC RID: 732 RVA: 0x0000A3A0 File Offset: 0x000085A0
		[FreeFunction(Name = "GUIStyle_Bindings::Internal_CalcSize", HasExplicitThis = true)]
		internal Vector2 Internal_CalcSize(GUIContent content)
		{
			Vector2 result;
			this.Internal_CalcSize_Injected(content, out result);
			return result;
		}

		// Token: 0x060002DD RID: 733 RVA: 0x0000A3B8 File Offset: 0x000085B8
		[FreeFunction(Name = "GUIStyle_Bindings::Internal_CalcSizeWithConstraints", HasExplicitThis = true)]
		internal Vector2 Internal_CalcSizeWithConstraints(GUIContent content, Vector2 maxSize)
		{
			Vector2 result;
			this.Internal_CalcSizeWithConstraints_Injected(content, ref maxSize, out result);
			return result;
		}

		// Token: 0x060002DE RID: 734
		[FreeFunction(Name = "GUIStyle_Bindings::Internal_CalcHeight", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern float Internal_CalcHeight(GUIContent content, float width);

		// Token: 0x060002DF RID: 735 RVA: 0x0000A3D4 File Offset: 0x000085D4
		[FreeFunction(Name = "GUIStyle_Bindings::Internal_CalcMinMaxWidth", HasExplicitThis = true)]
		private Vector2 Internal_CalcMinMaxWidth(GUIContent content)
		{
			Vector2 result;
			this.Internal_CalcMinMaxWidth_Injected(content, out result);
			return result;
		}

		// Token: 0x060002E0 RID: 736 RVA: 0x0000A3EB File Offset: 0x000085EB
		[FreeFunction(Name = "GUIStyle_Bindings::SetMouseTooltip")]
		internal static void SetMouseTooltip(string tooltip, Rect screenRect)
		{
			GUIStyle.SetMouseTooltip_Injected(tooltip, ref screenRect);
		}

		// Token: 0x060002E1 RID: 737
		[FreeFunction(Name = "GUIStyle_Bindings::Internal_GetCursorFlashOffset")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern float Internal_GetCursorFlashOffset();

		// Token: 0x060002E2 RID: 738
		[FreeFunction(Name = "GUIStyle::SetDefaultFont")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern void SetDefaultFont(Font font);

		// Token: 0x060002E3 RID: 739 RVA: 0x0000A3F8 File Offset: 0x000085F8
		~GUIStyle()
		{
			if (this.m_Ptr != IntPtr.Zero)
			{
				GUIStyle.Internal_Destroy(this.m_Ptr);
				this.m_Ptr = IntPtr.Zero;
			}
		}

		// Token: 0x060002E4 RID: 740 RVA: 0x0000A450 File Offset: 0x00008650
		internal static void CleanupRoots()
		{
			GUIStyle.s_None = null;
		}

		// Token: 0x060002E5 RID: 741 RVA: 0x0000A45C File Offset: 0x0000865C
		internal void InternalOnAfterDeserialize()
		{
			this.m_Normal = GUIStyleState.ProduceGUIStyleStateFromDeserialization(this, this.GetStyleStatePtr(0));
			this.m_Hover = GUIStyleState.ProduceGUIStyleStateFromDeserialization(this, this.GetStyleStatePtr(1));
			this.m_Active = GUIStyleState.ProduceGUIStyleStateFromDeserialization(this, this.GetStyleStatePtr(2));
			this.m_Focused = GUIStyleState.ProduceGUIStyleStateFromDeserialization(this, this.GetStyleStatePtr(3));
			this.m_OnNormal = GUIStyleState.ProduceGUIStyleStateFromDeserialization(this, this.GetStyleStatePtr(4));
			this.m_OnHover = GUIStyleState.ProduceGUIStyleStateFromDeserialization(this, this.GetStyleStatePtr(5));
			this.m_OnActive = GUIStyleState.ProduceGUIStyleStateFromDeserialization(this, this.GetStyleStatePtr(6));
			this.m_OnFocused = GUIStyleState.ProduceGUIStyleStateFromDeserialization(this, this.GetStyleStatePtr(7));
		}

		// Token: 0x17000069 RID: 105
		// (get) Token: 0x060002E6 RID: 742 RVA: 0x0000A504 File Offset: 0x00008704
		// (set) Token: 0x060002E7 RID: 743 RVA: 0x0000A53C File Offset: 0x0000873C
		public GUIStyleState normal
		{
			get
			{
				GUIStyleState result;
				if ((result = this.m_Normal) == null)
				{
					result = (this.m_Normal = GUIStyleState.GetGUIStyleState(this, this.GetStyleStatePtr(0)));
				}
				return result;
			}
			set
			{
				this.AssignStyleState(0, value.m_Ptr);
			}
		}

		// Token: 0x1700006A RID: 106
		// (get) Token: 0x060002E8 RID: 744 RVA: 0x0000A54C File Offset: 0x0000874C
		// (set) Token: 0x060002E9 RID: 745 RVA: 0x0000A584 File Offset: 0x00008784
		public GUIStyleState hover
		{
			get
			{
				GUIStyleState result;
				if ((result = this.m_Hover) == null)
				{
					result = (this.m_Hover = GUIStyleState.GetGUIStyleState(this, this.GetStyleStatePtr(1)));
				}
				return result;
			}
			set
			{
				this.AssignStyleState(1, value.m_Ptr);
			}
		}

		// Token: 0x1700006B RID: 107
		// (get) Token: 0x060002EA RID: 746 RVA: 0x0000A594 File Offset: 0x00008794
		// (set) Token: 0x060002EB RID: 747 RVA: 0x0000A5CC File Offset: 0x000087CC
		public GUIStyleState active
		{
			get
			{
				GUIStyleState result;
				if ((result = this.m_Active) == null)
				{
					result = (this.m_Active = GUIStyleState.GetGUIStyleState(this, this.GetStyleStatePtr(2)));
				}
				return result;
			}
			set
			{
				this.AssignStyleState(2, value.m_Ptr);
			}
		}

		// Token: 0x1700006C RID: 108
		// (get) Token: 0x060002EC RID: 748 RVA: 0x0000A5DC File Offset: 0x000087DC
		// (set) Token: 0x060002ED RID: 749 RVA: 0x0000A614 File Offset: 0x00008814
		public GUIStyleState onNormal
		{
			get
			{
				GUIStyleState result;
				if ((result = this.m_OnNormal) == null)
				{
					result = (this.m_OnNormal = GUIStyleState.GetGUIStyleState(this, this.GetStyleStatePtr(4)));
				}
				return result;
			}
			set
			{
				this.AssignStyleState(4, value.m_Ptr);
			}
		}

		// Token: 0x1700006D RID: 109
		// (get) Token: 0x060002EE RID: 750 RVA: 0x0000A624 File Offset: 0x00008824
		// (set) Token: 0x060002EF RID: 751 RVA: 0x0000A65C File Offset: 0x0000885C
		public GUIStyleState onHover
		{
			get
			{
				GUIStyleState result;
				if ((result = this.m_OnHover) == null)
				{
					result = (this.m_OnHover = GUIStyleState.GetGUIStyleState(this, this.GetStyleStatePtr(5)));
				}
				return result;
			}
			set
			{
				this.AssignStyleState(5, value.m_Ptr);
			}
		}

		// Token: 0x1700006E RID: 110
		// (get) Token: 0x060002F0 RID: 752 RVA: 0x0000A66C File Offset: 0x0000886C
		// (set) Token: 0x060002F1 RID: 753 RVA: 0x0000A6A4 File Offset: 0x000088A4
		public GUIStyleState onActive
		{
			get
			{
				GUIStyleState result;
				if ((result = this.m_OnActive) == null)
				{
					result = (this.m_OnActive = GUIStyleState.GetGUIStyleState(this, this.GetStyleStatePtr(6)));
				}
				return result;
			}
			set
			{
				this.AssignStyleState(6, value.m_Ptr);
			}
		}

		// Token: 0x1700006F RID: 111
		// (get) Token: 0x060002F2 RID: 754 RVA: 0x0000A6B4 File Offset: 0x000088B4
		// (set) Token: 0x060002F3 RID: 755 RVA: 0x0000A6EC File Offset: 0x000088EC
		public GUIStyleState focused
		{
			get
			{
				GUIStyleState result;
				if ((result = this.m_Focused) == null)
				{
					result = (this.m_Focused = GUIStyleState.GetGUIStyleState(this, this.GetStyleStatePtr(3)));
				}
				return result;
			}
			set
			{
				this.AssignStyleState(3, value.m_Ptr);
			}
		}

		// Token: 0x17000070 RID: 112
		// (get) Token: 0x060002F4 RID: 756 RVA: 0x0000A6FC File Offset: 0x000088FC
		// (set) Token: 0x060002F5 RID: 757 RVA: 0x0000A734 File Offset: 0x00008934
		public GUIStyleState onFocused
		{
			get
			{
				GUIStyleState result;
				if ((result = this.m_OnFocused) == null)
				{
					result = (this.m_OnFocused = GUIStyleState.GetGUIStyleState(this, this.GetStyleStatePtr(7)));
				}
				return result;
			}
			set
			{
				this.AssignStyleState(7, value.m_Ptr);
			}
		}

		// Token: 0x17000071 RID: 113
		// (get) Token: 0x060002F6 RID: 758 RVA: 0x0000A744 File Offset: 0x00008944
		// (set) Token: 0x060002F7 RID: 759 RVA: 0x0000A77C File Offset: 0x0000897C
		public RectOffset border
		{
			get
			{
				RectOffset result;
				if ((result = this.m_Border) == null)
				{
					result = (this.m_Border = new RectOffset(this, this.GetRectOffsetPtr(0)));
				}
				return result;
			}
			set
			{
				this.AssignRectOffset(0, value.m_Ptr);
			}
		}

		// Token: 0x17000072 RID: 114
		// (get) Token: 0x060002F8 RID: 760 RVA: 0x0000A78C File Offset: 0x0000898C
		// (set) Token: 0x060002F9 RID: 761 RVA: 0x0000A7C4 File Offset: 0x000089C4
		public RectOffset margin
		{
			get
			{
				RectOffset result;
				if ((result = this.m_Margin) == null)
				{
					result = (this.m_Margin = new RectOffset(this, this.GetRectOffsetPtr(1)));
				}
				return result;
			}
			set
			{
				this.AssignRectOffset(1, value.m_Ptr);
			}
		}

		// Token: 0x17000073 RID: 115
		// (get) Token: 0x060002FA RID: 762 RVA: 0x0000A7D4 File Offset: 0x000089D4
		// (set) Token: 0x060002FB RID: 763 RVA: 0x0000A80C File Offset: 0x00008A0C
		public RectOffset padding
		{
			get
			{
				RectOffset result;
				if ((result = this.m_Padding) == null)
				{
					result = (this.m_Padding = new RectOffset(this, this.GetRectOffsetPtr(2)));
				}
				return result;
			}
			set
			{
				this.AssignRectOffset(2, value.m_Ptr);
			}
		}

		// Token: 0x17000074 RID: 116
		// (get) Token: 0x060002FC RID: 764 RVA: 0x0000A81C File Offset: 0x00008A1C
		// (set) Token: 0x060002FD RID: 765 RVA: 0x0000A854 File Offset: 0x00008A54
		public RectOffset overflow
		{
			get
			{
				RectOffset result;
				if ((result = this.m_Overflow) == null)
				{
					result = (this.m_Overflow = new RectOffset(this, this.GetRectOffsetPtr(3)));
				}
				return result;
			}
			set
			{
				this.AssignRectOffset(3, value.m_Ptr);
			}
		}

		// Token: 0x17000075 RID: 117
		// (get) Token: 0x060002FE RID: 766 RVA: 0x0000A864 File Offset: 0x00008A64
		public float lineHeight
		{
			[CompilerGenerated]
			get
			{
				return Mathf.Round(GUIStyle.Internal_GetLineHeight(this.m_Ptr));
			}
		}

		// Token: 0x060002FF RID: 767 RVA: 0x0000A888 File Offset: 0x00008A88
		public void Draw(Rect position, bool isHover, bool isActive, bool on, bool hasKeyboardFocus)
		{
			this.Draw(position, GUIContent.none, -1, isHover, isActive, on, hasKeyboardFocus);
		}

		// Token: 0x06000300 RID: 768 RVA: 0x0000A89E File Offset: 0x00008A9E
		public void Draw(Rect position, string text, bool isHover, bool isActive, bool on, bool hasKeyboardFocus)
		{
			this.Draw(position, GUIContent.Temp(text), -1, isHover, isActive, on, hasKeyboardFocus);
		}

		// Token: 0x06000301 RID: 769 RVA: 0x0000A8B6 File Offset: 0x00008AB6
		public void Draw(Rect position, Texture image, bool isHover, bool isActive, bool on, bool hasKeyboardFocus)
		{
			this.Draw(position, GUIContent.Temp(image), -1, isHover, isActive, on, hasKeyboardFocus);
		}

		// Token: 0x06000302 RID: 770 RVA: 0x0000A8CE File Offset: 0x00008ACE
		public void Draw(Rect position, GUIContent content, bool isHover, bool isActive, bool on, bool hasKeyboardFocus)
		{
			this.Draw(position, content, -1, isHover, isActive, on, hasKeyboardFocus);
		}

		// Token: 0x06000303 RID: 771 RVA: 0x0000A8E1 File Offset: 0x00008AE1
		public void Draw(Rect position, GUIContent content, int controlID)
		{
			this.Draw(position, content, controlID, false, false, false, false);
		}

		// Token: 0x06000304 RID: 772 RVA: 0x0000A8F1 File Offset: 0x00008AF1
		public void Draw(Rect position, GUIContent content, int controlID, bool on)
		{
			this.Draw(position, content, controlID, false, false, on, false);
		}

		// Token: 0x06000305 RID: 773 RVA: 0x0000A902 File Offset: 0x00008B02
		private void Draw(Rect position, GUIContent content, int controlId, bool isHover, bool isActive, bool on, bool hasKeyboardFocus)
		{
			if (controlId == -1)
			{
				this.Internal_Draw(position, content, isHover, isActive, on, hasKeyboardFocus);
			}
			else
			{
				this.Internal_Draw2(position, content, controlId, on);
			}
		}

		// Token: 0x06000306 RID: 774 RVA: 0x0000A92C File Offset: 0x00008B2C
		public void DrawCursor(Rect position, GUIContent content, int controlID, int character)
		{
			Event current = Event.current;
			if (current.type == EventType.Repaint)
			{
				Color cursorColor = new Color(0f, 0f, 0f, 0f);
				float cursorFlashSpeed = GUI.skin.settings.cursorFlashSpeed;
				float num = (Time.realtimeSinceStartup - GUIStyle.Internal_GetCursorFlashOffset()) % cursorFlashSpeed / cursorFlashSpeed;
				if (cursorFlashSpeed == 0f || num < 0.5f)
				{
					cursorColor = GUI.skin.settings.cursorColor;
				}
				this.Internal_DrawCursor(position, content, character, cursorColor);
			}
		}

		// Token: 0x06000307 RID: 775 RVA: 0x0000A9BC File Offset: 0x00008BBC
		internal void DrawWithTextSelection(Rect position, GUIContent content, bool isActive, bool hasKeyboardFocus, int firstSelectedCharacter, int lastSelectedCharacter, bool drawSelectionAsComposition, Color selectionColor)
		{
			Color cursorColor = new Color(0f, 0f, 0f, 0f);
			float cursorFlashSpeed = GUI.skin.settings.cursorFlashSpeed;
			float num = (Time.realtimeSinceStartup - GUIStyle.Internal_GetCursorFlashOffset()) % cursorFlashSpeed / cursorFlashSpeed;
			if (cursorFlashSpeed == 0f || num < 0.5f)
			{
				cursorColor = GUI.skin.settings.cursorColor;
			}
			this.Internal_DrawWithTextSelection(position, content, position.Contains(Event.current.mousePosition), isActive, false, hasKeyboardFocus, drawSelectionAsComposition, firstSelectedCharacter, lastSelectedCharacter, cursorColor, selectionColor);
		}

		// Token: 0x06000308 RID: 776 RVA: 0x0000AA54 File Offset: 0x00008C54
		internal void DrawWithTextSelection(Rect position, GUIContent content, bool isActive, bool hasKeyboardFocus, int firstSelectedCharacter, int lastSelectedCharacter, bool drawSelectionAsComposition)
		{
			this.DrawWithTextSelection(position, content, isActive, hasKeyboardFocus, firstSelectedCharacter, lastSelectedCharacter, drawSelectionAsComposition, GUI.skin.settings.selectionColor);
		}

		// Token: 0x06000309 RID: 777 RVA: 0x0000AA82 File Offset: 0x00008C82
		internal void DrawWithTextSelection(Rect position, GUIContent content, int controlID, int firstSelectedCharacter, int lastSelectedCharacter, bool drawSelectionAsComposition)
		{
			this.DrawWithTextSelection(position, content, controlID == GUIUtility.hotControl, controlID == GUIUtility.keyboardControl && GUIStyle.showKeyboardFocus, firstSelectedCharacter, lastSelectedCharacter, drawSelectionAsComposition);
		}

		// Token: 0x0600030A RID: 778 RVA: 0x0000AAAE File Offset: 0x00008CAE
		public void DrawWithTextSelection(Rect position, GUIContent content, int controlID, int firstSelectedCharacter, int lastSelectedCharacter)
		{
			this.DrawWithTextSelection(position, content, controlID, firstSelectedCharacter, lastSelectedCharacter, false);
		}

		// Token: 0x0600030B RID: 779 RVA: 0x0000AAC0 File Offset: 0x00008CC0
		public static implicit operator GUIStyle(string str)
		{
			GUIStyle result;
			if (GUISkin.current == null)
			{
				Debug.LogError("Unable to use a named GUIStyle without a current skin. Most likely you need to move your GUIStyle initialization code to OnGUI");
				result = GUISkin.error;
			}
			else
			{
				result = GUISkin.current.GetStyle(str);
			}
			return result;
		}

		// Token: 0x17000076 RID: 118
		// (get) Token: 0x0600030C RID: 780 RVA: 0x0000AB08 File Offset: 0x00008D08
		public static GUIStyle none
		{
			[CompilerGenerated]
			get
			{
				GUIStyle result;
				if ((result = GUIStyle.s_None) == null)
				{
					result = (GUIStyle.s_None = new GUIStyle());
				}
				return result;
			}
		}

		// Token: 0x0600030D RID: 781 RVA: 0x0000AB34 File Offset: 0x00008D34
		public Vector2 GetCursorPixelPosition(Rect position, GUIContent content, int cursorStringIndex)
		{
			return this.Internal_GetCursorPixelPosition(position, content, cursorStringIndex);
		}

		// Token: 0x0600030E RID: 782 RVA: 0x0000AB54 File Offset: 0x00008D54
		public int GetCursorStringIndex(Rect position, GUIContent content, Vector2 cursorPixelPosition)
		{
			return this.Internal_GetCursorStringIndex(position, content, cursorPixelPosition);
		}

		// Token: 0x0600030F RID: 783 RVA: 0x0000AB74 File Offset: 0x00008D74
		internal int GetNumCharactersThatFitWithinWidth(string text, float width)
		{
			return this.Internal_GetNumCharactersThatFitWithinWidth(text, width);
		}

		// Token: 0x06000310 RID: 784 RVA: 0x0000AB94 File Offset: 0x00008D94
		public Vector2 CalcSize(GUIContent content)
		{
			return this.Internal_CalcSize(content);
		}

		// Token: 0x06000311 RID: 785 RVA: 0x0000ABB0 File Offset: 0x00008DB0
		internal Vector2 CalcSizeWithConstraints(GUIContent content, Vector2 constraints)
		{
			return this.Internal_CalcSizeWithConstraints(content, constraints);
		}

		// Token: 0x06000312 RID: 786 RVA: 0x0000ABD0 File Offset: 0x00008DD0
		public Vector2 CalcScreenSize(Vector2 contentSize)
		{
			return new Vector2((this.fixedWidth == 0f) ? Mathf.Ceil(contentSize.x + (float)this.padding.left + (float)this.padding.right) : this.fixedWidth, (this.fixedHeight == 0f) ? Mathf.Ceil(contentSize.y + (float)this.padding.top + (float)this.padding.bottom) : this.fixedHeight);
		}

		// Token: 0x06000313 RID: 787 RVA: 0x0000AC6C File Offset: 0x00008E6C
		public float CalcHeight(GUIContent content, float width)
		{
			return this.Internal_CalcHeight(content, width);
		}

		// Token: 0x17000077 RID: 119
		// (get) Token: 0x06000314 RID: 788 RVA: 0x0000AC8C File Offset: 0x00008E8C
		public bool isHeightDependantOnWidth
		{
			[CompilerGenerated]
			get
			{
				return this.fixedHeight == 0f && this.wordWrap && this.imagePosition != ImagePosition.ImageOnly;
			}
		}

		// Token: 0x06000315 RID: 789 RVA: 0x0000ACD0 File Offset: 0x00008ED0
		public void CalcMinMaxWidth(GUIContent content, out float minWidth, out float maxWidth)
		{
			Vector2 vector = this.Internal_CalcMinMaxWidth(content);
			minWidth = vector.x;
			maxWidth = vector.y;
		}

		// Token: 0x06000316 RID: 790 RVA: 0x0000ACF8 File Offset: 0x00008EF8
		public override string ToString()
		{
			return UnityString.Format("GUIStyle '{0}'", new object[]
			{
				this.name
			});
		}

		// Token: 0x06000317 RID: 791 RVA: 0x0000AD26 File Offset: 0x00008F26
		// Note: this type is marked as 'beforefieldinit'.
		static GUIStyle()
		{
		}

		// Token: 0x06000318 RID: 792
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_contentOffset_Injected(out Vector2 ret);

		// Token: 0x06000319 RID: 793
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_contentOffset_Injected(ref Vector2 value);

		// Token: 0x0600031A RID: 794
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_clipOffset_Injected(out Vector2 ret);

		// Token: 0x0600031B RID: 795
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_clipOffset_Injected(ref Vector2 value);

		// Token: 0x0600031C RID: 796
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_Internal_clipOffset_Injected(out Vector2 ret);

		// Token: 0x0600031D RID: 797
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_Internal_clipOffset_Injected(ref Vector2 value);

		// Token: 0x0600031E RID: 798
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void Internal_Draw_Injected(ref Rect screenRect, GUIContent content, bool isHover, bool isActive, bool on, bool hasKeyboardFocus);

		// Token: 0x0600031F RID: 799
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void Internal_Draw2_Injected(ref Rect position, GUIContent content, int controlID, bool on);

		// Token: 0x06000320 RID: 800
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void Internal_DrawCursor_Injected(ref Rect position, GUIContent content, int pos, ref Color cursorColor);

		// Token: 0x06000321 RID: 801
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void Internal_DrawWithTextSelection_Injected(ref Rect screenRect, GUIContent content, bool isHover, bool isActive, bool on, bool hasKeyboardFocus, bool drawSelectionAsComposition, int cursorFirst, int cursorLast, ref Color cursorColor, ref Color selectionColor);

		// Token: 0x06000322 RID: 802
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void Internal_GetCursorPixelPosition_Injected(ref Rect position, GUIContent content, int cursorStringIndex, out Vector2 ret);

		// Token: 0x06000323 RID: 803
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern int Internal_GetCursorStringIndex_Injected(ref Rect position, GUIContent content, ref Vector2 cursorPixelPosition);

		// Token: 0x06000324 RID: 804
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void Internal_CalcSize_Injected(GUIContent content, out Vector2 ret);

		// Token: 0x06000325 RID: 805
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void Internal_CalcSizeWithConstraints_Injected(GUIContent content, ref Vector2 maxSize, out Vector2 ret);

		// Token: 0x06000326 RID: 806
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void Internal_CalcMinMaxWidth_Injected(GUIContent content, out Vector2 ret);

		// Token: 0x06000327 RID: 807
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void SetMouseTooltip_Injected(string tooltip, ref Rect screenRect);

		// Token: 0x040000B1 RID: 177
		[NonSerialized]
		internal IntPtr m_Ptr;

		// Token: 0x040000B2 RID: 178
		[NonSerialized]
		private GUIStyleState m_Normal;

		// Token: 0x040000B3 RID: 179
		[NonSerialized]
		private GUIStyleState m_Hover;

		// Token: 0x040000B4 RID: 180
		[NonSerialized]
		private GUIStyleState m_Active;

		// Token: 0x040000B5 RID: 181
		[NonSerialized]
		private GUIStyleState m_Focused;

		// Token: 0x040000B6 RID: 182
		[NonSerialized]
		private GUIStyleState m_OnNormal;

		// Token: 0x040000B7 RID: 183
		[NonSerialized]
		private GUIStyleState m_OnHover;

		// Token: 0x040000B8 RID: 184
		[NonSerialized]
		private GUIStyleState m_OnActive;

		// Token: 0x040000B9 RID: 185
		[NonSerialized]
		private GUIStyleState m_OnFocused;

		// Token: 0x040000BA RID: 186
		[NonSerialized]
		private RectOffset m_Border;

		// Token: 0x040000BB RID: 187
		[NonSerialized]
		private RectOffset m_Padding;

		// Token: 0x040000BC RID: 188
		[NonSerialized]
		private RectOffset m_Margin;

		// Token: 0x040000BD RID: 189
		[NonSerialized]
		private RectOffset m_Overflow;

		// Token: 0x040000BE RID: 190
		internal static bool showKeyboardFocus = true;

		// Token: 0x040000BF RID: 191
		private static GUIStyle s_None;
	}
}
