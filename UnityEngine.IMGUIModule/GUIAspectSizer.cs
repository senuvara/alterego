﻿using System;

namespace UnityEngine
{
	// Token: 0x0200002C RID: 44
	internal sealed class GUIAspectSizer : GUILayoutEntry
	{
		// Token: 0x06000392 RID: 914 RVA: 0x0000BADF File Offset: 0x00009CDF
		public GUIAspectSizer(float aspect, GUILayoutOption[] options) : base(0f, 0f, 0f, 0f, GUIStyle.none)
		{
			this.aspect = aspect;
			this.ApplyOptions(options);
		}

		// Token: 0x06000393 RID: 915 RVA: 0x0000BB10 File Offset: 0x00009D10
		public override void CalcHeight()
		{
			this.minHeight = (this.maxHeight = this.rect.width / this.aspect);
		}

		// Token: 0x040000DD RID: 221
		private float aspect;
	}
}
