﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000021 RID: 33
	[AssetFileNameExtension("guiskin", new string[]
	{

	})]
	[RequiredByNativeCode]
	[ExecuteInEditMode]
	[Serializable]
	public sealed class GUISkin : ScriptableObject
	{
		// Token: 0x0600025E RID: 606 RVA: 0x000095CD File Offset: 0x000077CD
		public GUISkin()
		{
			this.m_CustomStyles = new GUIStyle[1];
		}

		// Token: 0x0600025F RID: 607 RVA: 0x000095F4 File Offset: 0x000077F4
		internal void OnEnable()
		{
			this.Apply();
		}

		// Token: 0x06000260 RID: 608 RVA: 0x000095FD File Offset: 0x000077FD
		internal static void CleanupRoots()
		{
			GUISkin.current = null;
			GUISkin.ms_Error = null;
		}

		// Token: 0x1700003F RID: 63
		// (get) Token: 0x06000261 RID: 609 RVA: 0x0000960C File Offset: 0x0000780C
		// (set) Token: 0x06000262 RID: 610 RVA: 0x00009627 File Offset: 0x00007827
		public Font font
		{
			get
			{
				return this.m_Font;
			}
			set
			{
				this.m_Font = value;
				if (GUISkin.current == this)
				{
					GUIStyle.SetDefaultFont(this.m_Font);
				}
				this.Apply();
			}
		}

		// Token: 0x17000040 RID: 64
		// (get) Token: 0x06000263 RID: 611 RVA: 0x00009654 File Offset: 0x00007854
		// (set) Token: 0x06000264 RID: 612 RVA: 0x0000966F File Offset: 0x0000786F
		public GUIStyle box
		{
			get
			{
				return this.m_box;
			}
			set
			{
				this.m_box = value;
				this.Apply();
			}
		}

		// Token: 0x17000041 RID: 65
		// (get) Token: 0x06000265 RID: 613 RVA: 0x00009680 File Offset: 0x00007880
		// (set) Token: 0x06000266 RID: 614 RVA: 0x0000969B File Offset: 0x0000789B
		public GUIStyle label
		{
			get
			{
				return this.m_label;
			}
			set
			{
				this.m_label = value;
				this.Apply();
			}
		}

		// Token: 0x17000042 RID: 66
		// (get) Token: 0x06000267 RID: 615 RVA: 0x000096AC File Offset: 0x000078AC
		// (set) Token: 0x06000268 RID: 616 RVA: 0x000096C7 File Offset: 0x000078C7
		public GUIStyle textField
		{
			get
			{
				return this.m_textField;
			}
			set
			{
				this.m_textField = value;
				this.Apply();
			}
		}

		// Token: 0x17000043 RID: 67
		// (get) Token: 0x06000269 RID: 617 RVA: 0x000096D8 File Offset: 0x000078D8
		// (set) Token: 0x0600026A RID: 618 RVA: 0x000096F3 File Offset: 0x000078F3
		public GUIStyle textArea
		{
			get
			{
				return this.m_textArea;
			}
			set
			{
				this.m_textArea = value;
				this.Apply();
			}
		}

		// Token: 0x17000044 RID: 68
		// (get) Token: 0x0600026B RID: 619 RVA: 0x00009704 File Offset: 0x00007904
		// (set) Token: 0x0600026C RID: 620 RVA: 0x0000971F File Offset: 0x0000791F
		public GUIStyle button
		{
			get
			{
				return this.m_button;
			}
			set
			{
				this.m_button = value;
				this.Apply();
			}
		}

		// Token: 0x17000045 RID: 69
		// (get) Token: 0x0600026D RID: 621 RVA: 0x00009730 File Offset: 0x00007930
		// (set) Token: 0x0600026E RID: 622 RVA: 0x0000974B File Offset: 0x0000794B
		public GUIStyle toggle
		{
			get
			{
				return this.m_toggle;
			}
			set
			{
				this.m_toggle = value;
				this.Apply();
			}
		}

		// Token: 0x17000046 RID: 70
		// (get) Token: 0x0600026F RID: 623 RVA: 0x0000975C File Offset: 0x0000795C
		// (set) Token: 0x06000270 RID: 624 RVA: 0x00009777 File Offset: 0x00007977
		public GUIStyle window
		{
			get
			{
				return this.m_window;
			}
			set
			{
				this.m_window = value;
				this.Apply();
			}
		}

		// Token: 0x17000047 RID: 71
		// (get) Token: 0x06000271 RID: 625 RVA: 0x00009788 File Offset: 0x00007988
		// (set) Token: 0x06000272 RID: 626 RVA: 0x000097A3 File Offset: 0x000079A3
		public GUIStyle horizontalSlider
		{
			get
			{
				return this.m_horizontalSlider;
			}
			set
			{
				this.m_horizontalSlider = value;
				this.Apply();
			}
		}

		// Token: 0x17000048 RID: 72
		// (get) Token: 0x06000273 RID: 627 RVA: 0x000097B4 File Offset: 0x000079B4
		// (set) Token: 0x06000274 RID: 628 RVA: 0x000097CF File Offset: 0x000079CF
		public GUIStyle horizontalSliderThumb
		{
			get
			{
				return this.m_horizontalSliderThumb;
			}
			set
			{
				this.m_horizontalSliderThumb = value;
				this.Apply();
			}
		}

		// Token: 0x17000049 RID: 73
		// (get) Token: 0x06000275 RID: 629 RVA: 0x000097E0 File Offset: 0x000079E0
		// (set) Token: 0x06000276 RID: 630 RVA: 0x000097FB File Offset: 0x000079FB
		public GUIStyle verticalSlider
		{
			get
			{
				return this.m_verticalSlider;
			}
			set
			{
				this.m_verticalSlider = value;
				this.Apply();
			}
		}

		// Token: 0x1700004A RID: 74
		// (get) Token: 0x06000277 RID: 631 RVA: 0x0000980C File Offset: 0x00007A0C
		// (set) Token: 0x06000278 RID: 632 RVA: 0x00009827 File Offset: 0x00007A27
		public GUIStyle verticalSliderThumb
		{
			get
			{
				return this.m_verticalSliderThumb;
			}
			set
			{
				this.m_verticalSliderThumb = value;
				this.Apply();
			}
		}

		// Token: 0x1700004B RID: 75
		// (get) Token: 0x06000279 RID: 633 RVA: 0x00009838 File Offset: 0x00007A38
		// (set) Token: 0x0600027A RID: 634 RVA: 0x00009853 File Offset: 0x00007A53
		public GUIStyle horizontalScrollbar
		{
			get
			{
				return this.m_horizontalScrollbar;
			}
			set
			{
				this.m_horizontalScrollbar = value;
				this.Apply();
			}
		}

		// Token: 0x1700004C RID: 76
		// (get) Token: 0x0600027B RID: 635 RVA: 0x00009864 File Offset: 0x00007A64
		// (set) Token: 0x0600027C RID: 636 RVA: 0x0000987F File Offset: 0x00007A7F
		public GUIStyle horizontalScrollbarThumb
		{
			get
			{
				return this.m_horizontalScrollbarThumb;
			}
			set
			{
				this.m_horizontalScrollbarThumb = value;
				this.Apply();
			}
		}

		// Token: 0x1700004D RID: 77
		// (get) Token: 0x0600027D RID: 637 RVA: 0x00009890 File Offset: 0x00007A90
		// (set) Token: 0x0600027E RID: 638 RVA: 0x000098AB File Offset: 0x00007AAB
		public GUIStyle horizontalScrollbarLeftButton
		{
			get
			{
				return this.m_horizontalScrollbarLeftButton;
			}
			set
			{
				this.m_horizontalScrollbarLeftButton = value;
				this.Apply();
			}
		}

		// Token: 0x1700004E RID: 78
		// (get) Token: 0x0600027F RID: 639 RVA: 0x000098BC File Offset: 0x00007ABC
		// (set) Token: 0x06000280 RID: 640 RVA: 0x000098D7 File Offset: 0x00007AD7
		public GUIStyle horizontalScrollbarRightButton
		{
			get
			{
				return this.m_horizontalScrollbarRightButton;
			}
			set
			{
				this.m_horizontalScrollbarRightButton = value;
				this.Apply();
			}
		}

		// Token: 0x1700004F RID: 79
		// (get) Token: 0x06000281 RID: 641 RVA: 0x000098E8 File Offset: 0x00007AE8
		// (set) Token: 0x06000282 RID: 642 RVA: 0x00009903 File Offset: 0x00007B03
		public GUIStyle verticalScrollbar
		{
			get
			{
				return this.m_verticalScrollbar;
			}
			set
			{
				this.m_verticalScrollbar = value;
				this.Apply();
			}
		}

		// Token: 0x17000050 RID: 80
		// (get) Token: 0x06000283 RID: 643 RVA: 0x00009914 File Offset: 0x00007B14
		// (set) Token: 0x06000284 RID: 644 RVA: 0x0000992F File Offset: 0x00007B2F
		public GUIStyle verticalScrollbarThumb
		{
			get
			{
				return this.m_verticalScrollbarThumb;
			}
			set
			{
				this.m_verticalScrollbarThumb = value;
				this.Apply();
			}
		}

		// Token: 0x17000051 RID: 81
		// (get) Token: 0x06000285 RID: 645 RVA: 0x00009940 File Offset: 0x00007B40
		// (set) Token: 0x06000286 RID: 646 RVA: 0x0000995B File Offset: 0x00007B5B
		public GUIStyle verticalScrollbarUpButton
		{
			get
			{
				return this.m_verticalScrollbarUpButton;
			}
			set
			{
				this.m_verticalScrollbarUpButton = value;
				this.Apply();
			}
		}

		// Token: 0x17000052 RID: 82
		// (get) Token: 0x06000287 RID: 647 RVA: 0x0000996C File Offset: 0x00007B6C
		// (set) Token: 0x06000288 RID: 648 RVA: 0x00009987 File Offset: 0x00007B87
		public GUIStyle verticalScrollbarDownButton
		{
			get
			{
				return this.m_verticalScrollbarDownButton;
			}
			set
			{
				this.m_verticalScrollbarDownButton = value;
				this.Apply();
			}
		}

		// Token: 0x17000053 RID: 83
		// (get) Token: 0x06000289 RID: 649 RVA: 0x00009998 File Offset: 0x00007B98
		// (set) Token: 0x0600028A RID: 650 RVA: 0x000099B3 File Offset: 0x00007BB3
		public GUIStyle scrollView
		{
			get
			{
				return this.m_ScrollView;
			}
			set
			{
				this.m_ScrollView = value;
				this.Apply();
			}
		}

		// Token: 0x17000054 RID: 84
		// (get) Token: 0x0600028B RID: 651 RVA: 0x000099C4 File Offset: 0x00007BC4
		// (set) Token: 0x0600028C RID: 652 RVA: 0x000099DF File Offset: 0x00007BDF
		public GUIStyle[] customStyles
		{
			get
			{
				return this.m_CustomStyles;
			}
			set
			{
				this.m_CustomStyles = value;
				this.Apply();
			}
		}

		// Token: 0x17000055 RID: 85
		// (get) Token: 0x0600028D RID: 653 RVA: 0x000099F0 File Offset: 0x00007BF0
		public GUISettings settings
		{
			get
			{
				return this.m_Settings;
			}
		}

		// Token: 0x17000056 RID: 86
		// (get) Token: 0x0600028E RID: 654 RVA: 0x00009A0C File Offset: 0x00007C0C
		internal static GUIStyle error
		{
			get
			{
				if (GUISkin.ms_Error == null)
				{
					GUISkin.ms_Error = new GUIStyle();
					GUISkin.ms_Error.name = "StyleNotFoundError";
				}
				return GUISkin.ms_Error;
			}
		}

		// Token: 0x0600028F RID: 655 RVA: 0x00009A4B File Offset: 0x00007C4B
		internal void Apply()
		{
			if (this.m_CustomStyles == null)
			{
				Debug.Log("custom styles is null");
			}
			this.BuildStyleCache();
		}

		// Token: 0x06000290 RID: 656 RVA: 0x00009A6C File Offset: 0x00007C6C
		private void BuildStyleCache()
		{
			if (this.m_box == null)
			{
				this.m_box = new GUIStyle();
			}
			if (this.m_button == null)
			{
				this.m_button = new GUIStyle();
			}
			if (this.m_toggle == null)
			{
				this.m_toggle = new GUIStyle();
			}
			if (this.m_label == null)
			{
				this.m_label = new GUIStyle();
			}
			if (this.m_window == null)
			{
				this.m_window = new GUIStyle();
			}
			if (this.m_textField == null)
			{
				this.m_textField = new GUIStyle();
			}
			if (this.m_textArea == null)
			{
				this.m_textArea = new GUIStyle();
			}
			if (this.m_horizontalSlider == null)
			{
				this.m_horizontalSlider = new GUIStyle();
			}
			if (this.m_horizontalSliderThumb == null)
			{
				this.m_horizontalSliderThumb = new GUIStyle();
			}
			if (this.m_verticalSlider == null)
			{
				this.m_verticalSlider = new GUIStyle();
			}
			if (this.m_verticalSliderThumb == null)
			{
				this.m_verticalSliderThumb = new GUIStyle();
			}
			if (this.m_horizontalScrollbar == null)
			{
				this.m_horizontalScrollbar = new GUIStyle();
			}
			if (this.m_horizontalScrollbarThumb == null)
			{
				this.m_horizontalScrollbarThumb = new GUIStyle();
			}
			if (this.m_horizontalScrollbarLeftButton == null)
			{
				this.m_horizontalScrollbarLeftButton = new GUIStyle();
			}
			if (this.m_horizontalScrollbarRightButton == null)
			{
				this.m_horizontalScrollbarRightButton = new GUIStyle();
			}
			if (this.m_verticalScrollbar == null)
			{
				this.m_verticalScrollbar = new GUIStyle();
			}
			if (this.m_verticalScrollbarThumb == null)
			{
				this.m_verticalScrollbarThumb = new GUIStyle();
			}
			if (this.m_verticalScrollbarUpButton == null)
			{
				this.m_verticalScrollbarUpButton = new GUIStyle();
			}
			if (this.m_verticalScrollbarDownButton == null)
			{
				this.m_verticalScrollbarDownButton = new GUIStyle();
			}
			if (this.m_ScrollView == null)
			{
				this.m_ScrollView = new GUIStyle();
			}
			this.m_Styles = new Dictionary<string, GUIStyle>(StringComparer.OrdinalIgnoreCase);
			this.m_Styles["box"] = this.m_box;
			this.m_box.name = "box";
			this.m_Styles["button"] = this.m_button;
			this.m_button.name = "button";
			this.m_Styles["toggle"] = this.m_toggle;
			this.m_toggle.name = "toggle";
			this.m_Styles["label"] = this.m_label;
			this.m_label.name = "label";
			this.m_Styles["window"] = this.m_window;
			this.m_window.name = "window";
			this.m_Styles["textfield"] = this.m_textField;
			this.m_textField.name = "textfield";
			this.m_Styles["textarea"] = this.m_textArea;
			this.m_textArea.name = "textarea";
			this.m_Styles["horizontalslider"] = this.m_horizontalSlider;
			this.m_horizontalSlider.name = "horizontalslider";
			this.m_Styles["horizontalsliderthumb"] = this.m_horizontalSliderThumb;
			this.m_horizontalSliderThumb.name = "horizontalsliderthumb";
			this.m_Styles["verticalslider"] = this.m_verticalSlider;
			this.m_verticalSlider.name = "verticalslider";
			this.m_Styles["verticalsliderthumb"] = this.m_verticalSliderThumb;
			this.m_verticalSliderThumb.name = "verticalsliderthumb";
			this.m_Styles["horizontalscrollbar"] = this.m_horizontalScrollbar;
			this.m_horizontalScrollbar.name = "horizontalscrollbar";
			this.m_Styles["horizontalscrollbarthumb"] = this.m_horizontalScrollbarThumb;
			this.m_horizontalScrollbarThumb.name = "horizontalscrollbarthumb";
			this.m_Styles["horizontalscrollbarleftbutton"] = this.m_horizontalScrollbarLeftButton;
			this.m_horizontalScrollbarLeftButton.name = "horizontalscrollbarleftbutton";
			this.m_Styles["horizontalscrollbarrightbutton"] = this.m_horizontalScrollbarRightButton;
			this.m_horizontalScrollbarRightButton.name = "horizontalscrollbarrightbutton";
			this.m_Styles["verticalscrollbar"] = this.m_verticalScrollbar;
			this.m_verticalScrollbar.name = "verticalscrollbar";
			this.m_Styles["verticalscrollbarthumb"] = this.m_verticalScrollbarThumb;
			this.m_verticalScrollbarThumb.name = "verticalscrollbarthumb";
			this.m_Styles["verticalscrollbarupbutton"] = this.m_verticalScrollbarUpButton;
			this.m_verticalScrollbarUpButton.name = "verticalscrollbarupbutton";
			this.m_Styles["verticalscrollbardownbutton"] = this.m_verticalScrollbarDownButton;
			this.m_verticalScrollbarDownButton.name = "verticalscrollbardownbutton";
			this.m_Styles["scrollview"] = this.m_ScrollView;
			this.m_ScrollView.name = "scrollview";
			if (this.m_CustomStyles != null)
			{
				for (int i = 0; i < this.m_CustomStyles.Length; i++)
				{
					if (this.m_CustomStyles[i] != null)
					{
						this.m_Styles[this.m_CustomStyles[i].name] = this.m_CustomStyles[i];
					}
				}
			}
			GUISkin.error.stretchHeight = true;
			GUISkin.error.normal.textColor = Color.red;
		}

		// Token: 0x06000291 RID: 657 RVA: 0x00009FB4 File Offset: 0x000081B4
		public GUIStyle GetStyle(string styleName)
		{
			GUIStyle guistyle = this.FindStyle(styleName);
			GUIStyle result;
			if (guistyle != null)
			{
				result = guistyle;
			}
			else
			{
				Debug.LogWarning(string.Concat(new string[]
				{
					"Unable to find style '",
					styleName,
					"' in skin '",
					base.name,
					"' ",
					(Event.current == null) ? "<called outside OnGUI>" : Event.current.type.ToString()
				}));
				result = GUISkin.error;
			}
			return result;
		}

		// Token: 0x06000292 RID: 658 RVA: 0x0000A048 File Offset: 0x00008248
		public GUIStyle FindStyle(string styleName)
		{
			GUIStyle result;
			if (this == null)
			{
				Debug.LogError("GUISkin is NULL");
				result = null;
			}
			else
			{
				if (this.m_Styles == null)
				{
					this.BuildStyleCache();
				}
				GUIStyle guistyle;
				if (this.m_Styles.TryGetValue(styleName, out guistyle))
				{
					result = guistyle;
				}
				else
				{
					result = null;
				}
			}
			return result;
		}

		// Token: 0x06000293 RID: 659 RVA: 0x0000A0A7 File Offset: 0x000082A7
		internal void MakeCurrent()
		{
			GUISkin.current = this;
			GUIStyle.SetDefaultFont(this.font);
			if (GUISkin.m_SkinChanged != null)
			{
				GUISkin.m_SkinChanged();
			}
		}

		// Token: 0x06000294 RID: 660 RVA: 0x0000A0D0 File Offset: 0x000082D0
		public IEnumerator GetEnumerator()
		{
			if (this.m_Styles == null)
			{
				this.BuildStyleCache();
			}
			return this.m_Styles.Values.GetEnumerator();
		}

		// Token: 0x04000093 RID: 147
		[SerializeField]
		private Font m_Font;

		// Token: 0x04000094 RID: 148
		[SerializeField]
		private GUIStyle m_box;

		// Token: 0x04000095 RID: 149
		[SerializeField]
		private GUIStyle m_button;

		// Token: 0x04000096 RID: 150
		[SerializeField]
		private GUIStyle m_toggle;

		// Token: 0x04000097 RID: 151
		[SerializeField]
		private GUIStyle m_label;

		// Token: 0x04000098 RID: 152
		[SerializeField]
		private GUIStyle m_textField;

		// Token: 0x04000099 RID: 153
		[SerializeField]
		private GUIStyle m_textArea;

		// Token: 0x0400009A RID: 154
		[SerializeField]
		private GUIStyle m_window;

		// Token: 0x0400009B RID: 155
		[SerializeField]
		private GUIStyle m_horizontalSlider;

		// Token: 0x0400009C RID: 156
		[SerializeField]
		private GUIStyle m_horizontalSliderThumb;

		// Token: 0x0400009D RID: 157
		[SerializeField]
		private GUIStyle m_verticalSlider;

		// Token: 0x0400009E RID: 158
		[SerializeField]
		private GUIStyle m_verticalSliderThumb;

		// Token: 0x0400009F RID: 159
		[SerializeField]
		private GUIStyle m_horizontalScrollbar;

		// Token: 0x040000A0 RID: 160
		[SerializeField]
		private GUIStyle m_horizontalScrollbarThumb;

		// Token: 0x040000A1 RID: 161
		[SerializeField]
		private GUIStyle m_horizontalScrollbarLeftButton;

		// Token: 0x040000A2 RID: 162
		[SerializeField]
		private GUIStyle m_horizontalScrollbarRightButton;

		// Token: 0x040000A3 RID: 163
		[SerializeField]
		private GUIStyle m_verticalScrollbar;

		// Token: 0x040000A4 RID: 164
		[SerializeField]
		private GUIStyle m_verticalScrollbarThumb;

		// Token: 0x040000A5 RID: 165
		[SerializeField]
		private GUIStyle m_verticalScrollbarUpButton;

		// Token: 0x040000A6 RID: 166
		[SerializeField]
		private GUIStyle m_verticalScrollbarDownButton;

		// Token: 0x040000A7 RID: 167
		[SerializeField]
		private GUIStyle m_ScrollView;

		// Token: 0x040000A8 RID: 168
		[SerializeField]
		internal GUIStyle[] m_CustomStyles;

		// Token: 0x040000A9 RID: 169
		[SerializeField]
		private GUISettings m_Settings = new GUISettings();

		// Token: 0x040000AA RID: 170
		internal static GUIStyle ms_Error;

		// Token: 0x040000AB RID: 171
		private Dictionary<string, GUIStyle> m_Styles = null;

		// Token: 0x040000AC RID: 172
		internal static GUISkin.SkinChangedDelegate m_SkinChanged;

		// Token: 0x040000AD RID: 173
		internal static GUISkin current;

		// Token: 0x02000022 RID: 34
		// (Invoke) Token: 0x06000296 RID: 662
		internal delegate void SkinChangedDelegate();
	}
}
