﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000033 RID: 51
	internal class SliderState
	{
		// Token: 0x060003BB RID: 955 RVA: 0x0000DCD8 File Offset: 0x0000BED8
		[RequiredByNativeCode]
		public SliderState()
		{
		}

		// Token: 0x0400010D RID: 269
		public float dragStartPos;

		// Token: 0x0400010E RID: 270
		public float dragStartValue;

		// Token: 0x0400010F RID: 271
		public bool isDragging;
	}
}
