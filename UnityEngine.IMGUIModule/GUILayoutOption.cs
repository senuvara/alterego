﻿using System;

namespace UnityEngine
{
	// Token: 0x0200001B RID: 27
	public sealed class GUILayoutOption
	{
		// Token: 0x06000221 RID: 545 RVA: 0x0000851D File Offset: 0x0000671D
		internal GUILayoutOption(GUILayoutOption.Type type, object value)
		{
			this.type = type;
			this.value = value;
		}

		// Token: 0x04000071 RID: 113
		internal GUILayoutOption.Type type;

		// Token: 0x04000072 RID: 114
		internal object value;

		// Token: 0x0200001C RID: 28
		internal enum Type
		{
			// Token: 0x04000074 RID: 116
			fixedWidth,
			// Token: 0x04000075 RID: 117
			fixedHeight,
			// Token: 0x04000076 RID: 118
			minWidth,
			// Token: 0x04000077 RID: 119
			maxWidth,
			// Token: 0x04000078 RID: 120
			minHeight,
			// Token: 0x04000079 RID: 121
			maxHeight,
			// Token: 0x0400007A RID: 122
			stretchWidth,
			// Token: 0x0400007B RID: 123
			stretchHeight,
			// Token: 0x0400007C RID: 124
			alignStart,
			// Token: 0x0400007D RID: 125
			alignMiddle,
			// Token: 0x0400007E RID: 126
			alignEnd,
			// Token: 0x0400007F RID: 127
			alignJustify,
			// Token: 0x04000080 RID: 128
			equalSize,
			// Token: 0x04000081 RID: 129
			spacing
		}
	}
}
