﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;

namespace UnityEngine
{
	// Token: 0x02000006 RID: 6
	[VisibleToOtherModules(new string[]
	{
		"UnityEngine.UIElementsModule"
	})]
	internal struct EventInterests
	{
		// Token: 0x17000019 RID: 25
		// (get) Token: 0x06000045 RID: 69 RVA: 0x00003258 File Offset: 0x00001458
		// (set) Token: 0x06000046 RID: 70 RVA: 0x00003272 File Offset: 0x00001472
		public bool wantsMouseMove
		{
			[CompilerGenerated]
			get
			{
				return this.<wantsMouseMove>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<wantsMouseMove>k__BackingField = value;
			}
		}

		// Token: 0x1700001A RID: 26
		// (get) Token: 0x06000047 RID: 71 RVA: 0x0000327C File Offset: 0x0000147C
		// (set) Token: 0x06000048 RID: 72 RVA: 0x00003296 File Offset: 0x00001496
		public bool wantsMouseEnterLeaveWindow
		{
			[CompilerGenerated]
			get
			{
				return this.<wantsMouseEnterLeaveWindow>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<wantsMouseEnterLeaveWindow>k__BackingField = value;
			}
		}

		// Token: 0x06000049 RID: 73 RVA: 0x000032A0 File Offset: 0x000014A0
		public bool WantsEvent(EventType type)
		{
			bool result;
			if (type != EventType.MouseEnterWindow && type != EventType.MouseLeaveWindow)
			{
				result = (type != EventType.MouseMove || this.wantsMouseMove);
			}
			else
			{
				result = this.wantsMouseEnterLeaveWindow;
			}
			return result;
		}

		// Token: 0x04000041 RID: 65
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private bool <wantsMouseMove>k__BackingField;

		// Token: 0x04000042 RID: 66
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private bool <wantsMouseEnterLeaveWindow>k__BackingField;
	}
}
