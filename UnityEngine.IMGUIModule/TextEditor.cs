﻿using System;
using System.Collections.Generic;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000035 RID: 53
	public class TextEditor
	{
		// Token: 0x060003D7 RID: 983 RVA: 0x0000EC24 File Offset: 0x0000CE24
		[RequiredByNativeCode]
		public TextEditor()
		{
		}

		// Token: 0x1700008F RID: 143
		// (get) Token: 0x060003D8 RID: 984 RVA: 0x0000ECB4 File Offset: 0x0000CEB4
		// (set) Token: 0x060003D9 RID: 985 RVA: 0x0000ECCF File Offset: 0x0000CECF
		[Obsolete("Please use 'text' instead of 'content'", false)]
		public GUIContent content
		{
			get
			{
				return this.m_Content;
			}
			set
			{
				this.m_Content = value;
			}
		}

		// Token: 0x17000090 RID: 144
		// (get) Token: 0x060003DA RID: 986 RVA: 0x0000ECDC File Offset: 0x0000CEDC
		// (set) Token: 0x060003DB RID: 987 RVA: 0x0000ECFC File Offset: 0x0000CEFC
		public string text
		{
			get
			{
				return this.m_Content.text;
			}
			set
			{
				this.m_Content.text = (value ?? string.Empty);
				this.EnsureValidCodePointIndex(ref this.m_CursorIndex);
				this.EnsureValidCodePointIndex(ref this.m_SelectIndex);
			}
		}

		// Token: 0x17000091 RID: 145
		// (get) Token: 0x060003DC RID: 988 RVA: 0x0000ED30 File Offset: 0x0000CF30
		// (set) Token: 0x060003DD RID: 989 RVA: 0x0000ED4B File Offset: 0x0000CF4B
		public Rect position
		{
			get
			{
				return this.m_Position;
			}
			set
			{
				if (!(this.m_Position == value))
				{
					this.m_Position = value;
					this.UpdateScrollOffset();
				}
			}
		}

		// Token: 0x17000092 RID: 146
		// (get) Token: 0x060003DE RID: 990 RVA: 0x0000ED74 File Offset: 0x0000CF74
		internal virtual Rect localPosition
		{
			[VisibleToOtherModules(new string[]
			{
				"UnityEngine.UIElementsModule"
			})]
			get
			{
				return this.position;
			}
		}

		// Token: 0x17000093 RID: 147
		// (get) Token: 0x060003DF RID: 991 RVA: 0x0000ED90 File Offset: 0x0000CF90
		// (set) Token: 0x060003E0 RID: 992 RVA: 0x0000EDAC File Offset: 0x0000CFAC
		public int cursorIndex
		{
			get
			{
				return this.m_CursorIndex;
			}
			set
			{
				int cursorIndex = this.m_CursorIndex;
				this.m_CursorIndex = value;
				this.EnsureValidCodePointIndex(ref this.m_CursorIndex);
				if (this.m_CursorIndex != cursorIndex)
				{
					this.m_RevealCursor = true;
					this.OnCursorIndexChange();
				}
			}
		}

		// Token: 0x17000094 RID: 148
		// (get) Token: 0x060003E1 RID: 993 RVA: 0x0000EDF0 File Offset: 0x0000CFF0
		// (set) Token: 0x060003E2 RID: 994 RVA: 0x0000EE0C File Offset: 0x0000D00C
		public int selectIndex
		{
			get
			{
				return this.m_SelectIndex;
			}
			set
			{
				int selectIndex = this.m_SelectIndex;
				this.m_SelectIndex = value;
				this.EnsureValidCodePointIndex(ref this.m_SelectIndex);
				if (this.m_SelectIndex != selectIndex)
				{
					this.OnSelectIndexChange();
				}
			}
		}

		// Token: 0x060003E3 RID: 995 RVA: 0x0000EE46 File Offset: 0x0000D046
		private void ClearCursorPos()
		{
			this.hasHorizontalCursorPos = false;
			this.m_iAltCursorPos = -1;
		}

		// Token: 0x17000095 RID: 149
		// (get) Token: 0x060003E4 RID: 996 RVA: 0x0000EE58 File Offset: 0x0000D058
		// (set) Token: 0x060003E5 RID: 997 RVA: 0x0000EE73 File Offset: 0x0000D073
		public TextEditor.DblClickSnapping doubleClickSnapping
		{
			get
			{
				return this.m_DblClickSnap;
			}
			set
			{
				this.m_DblClickSnap = value;
			}
		}

		// Token: 0x17000096 RID: 150
		// (get) Token: 0x060003E6 RID: 998 RVA: 0x0000EE80 File Offset: 0x0000D080
		// (set) Token: 0x060003E7 RID: 999 RVA: 0x0000EE9B File Offset: 0x0000D09B
		public int altCursorPosition
		{
			get
			{
				return this.m_iAltCursorPos;
			}
			set
			{
				this.m_iAltCursorPos = value;
			}
		}

		// Token: 0x060003E8 RID: 1000 RVA: 0x0000EEA8 File Offset: 0x0000D0A8
		public void OnFocus()
		{
			if (this.multiline)
			{
				int num = 0;
				this.selectIndex = num;
				this.cursorIndex = num;
			}
			else
			{
				this.SelectAll();
			}
			this.m_HasFocus = true;
		}

		// Token: 0x060003E9 RID: 1001 RVA: 0x0000EEE3 File Offset: 0x0000D0E3
		public void OnLostFocus()
		{
			this.m_HasFocus = false;
			this.scrollOffset = Vector2.zero;
		}

		// Token: 0x060003EA RID: 1002 RVA: 0x0000EEF8 File Offset: 0x0000D0F8
		private void GrabGraphicalCursorPos()
		{
			if (!this.hasHorizontalCursorPos)
			{
				this.graphicalCursorPos = this.style.GetCursorPixelPosition(this.localPosition, this.m_Content, this.cursorIndex);
				this.graphicalSelectCursorPos = this.style.GetCursorPixelPosition(this.localPosition, this.m_Content, this.selectIndex);
				this.hasHorizontalCursorPos = false;
			}
		}

		// Token: 0x060003EB RID: 1003 RVA: 0x0000EF60 File Offset: 0x0000D160
		public bool HandleKeyEvent(Event e)
		{
			this.InitKeyActions();
			EventModifiers modifiers = e.modifiers;
			e.modifiers &= ~EventModifiers.CapsLock;
			bool result;
			if (TextEditor.s_Keyactions.ContainsKey(e))
			{
				TextEditor.TextEditOp operation = TextEditor.s_Keyactions[e];
				this.PerformOperation(operation);
				e.modifiers = modifiers;
				result = true;
			}
			else
			{
				e.modifiers = modifiers;
				result = false;
			}
			return result;
		}

		// Token: 0x060003EC RID: 1004 RVA: 0x0000EFCC File Offset: 0x0000D1CC
		public bool DeleteLineBack()
		{
			bool result;
			if (this.hasSelection)
			{
				this.DeleteSelection();
				result = true;
			}
			else
			{
				int num = this.cursorIndex;
				int num2 = num;
				while (num2-- != 0)
				{
					if (this.text[num2] == '\n')
					{
						num = num2 + 1;
						break;
					}
				}
				if (num2 == -1)
				{
					num = 0;
				}
				if (this.cursorIndex != num)
				{
					this.m_Content.text = this.text.Remove(num, this.cursorIndex - num);
					int num3 = num;
					this.cursorIndex = num3;
					this.selectIndex = num3;
					result = true;
				}
				else
				{
					result = false;
				}
			}
			return result;
		}

		// Token: 0x060003ED RID: 1005 RVA: 0x0000F080 File Offset: 0x0000D280
		public bool DeleteWordBack()
		{
			bool result;
			if (this.hasSelection)
			{
				this.DeleteSelection();
				result = true;
			}
			else
			{
				int num = this.FindEndOfPreviousWord(this.cursorIndex);
				if (this.cursorIndex != num)
				{
					this.m_Content.text = this.text.Remove(num, this.cursorIndex - num);
					int num2 = num;
					this.cursorIndex = num2;
					this.selectIndex = num2;
					result = true;
				}
				else
				{
					result = false;
				}
			}
			return result;
		}

		// Token: 0x060003EE RID: 1006 RVA: 0x0000F100 File Offset: 0x0000D300
		public bool DeleteWordForward()
		{
			bool result;
			if (this.hasSelection)
			{
				this.DeleteSelection();
				result = true;
			}
			else
			{
				int num = this.FindStartOfNextWord(this.cursorIndex);
				if (this.cursorIndex < this.text.Length)
				{
					this.m_Content.text = this.text.Remove(this.cursorIndex, num - this.cursorIndex);
					result = true;
				}
				else
				{
					result = false;
				}
			}
			return result;
		}

		// Token: 0x060003EF RID: 1007 RVA: 0x0000F180 File Offset: 0x0000D380
		public bool Delete()
		{
			bool result;
			if (this.hasSelection)
			{
				this.DeleteSelection();
				result = true;
			}
			else if (this.cursorIndex < this.text.Length)
			{
				this.m_Content.text = this.text.Remove(this.cursorIndex, this.NextCodePointIndex(this.cursorIndex) - this.cursorIndex);
				result = true;
			}
			else
			{
				result = false;
			}
			return result;
		}

		// Token: 0x060003F0 RID: 1008 RVA: 0x0000F200 File Offset: 0x0000D400
		public bool CanPaste()
		{
			return GUIUtility.systemCopyBuffer.Length != 0;
		}

		// Token: 0x060003F1 RID: 1009 RVA: 0x0000F228 File Offset: 0x0000D428
		public bool Backspace()
		{
			bool result;
			if (this.hasSelection)
			{
				this.DeleteSelection();
				result = true;
			}
			else if (this.cursorIndex > 0)
			{
				int num = this.PreviousCodePointIndex(this.cursorIndex);
				this.m_Content.text = this.text.Remove(num, this.cursorIndex - num);
				int num2 = num;
				this.cursorIndex = num2;
				this.selectIndex = num2;
				this.ClearCursorPos();
				result = true;
			}
			else
			{
				result = false;
			}
			return result;
		}

		// Token: 0x060003F2 RID: 1010 RVA: 0x0000F2AE File Offset: 0x0000D4AE
		public void SelectAll()
		{
			this.cursorIndex = 0;
			this.selectIndex = this.text.Length;
			this.ClearCursorPos();
		}

		// Token: 0x060003F3 RID: 1011 RVA: 0x0000F2CF File Offset: 0x0000D4CF
		public void SelectNone()
		{
			this.selectIndex = this.cursorIndex;
			this.ClearCursorPos();
		}

		// Token: 0x17000097 RID: 151
		// (get) Token: 0x060003F4 RID: 1012 RVA: 0x0000F2E4 File Offset: 0x0000D4E4
		public bool hasSelection
		{
			get
			{
				return this.cursorIndex != this.selectIndex;
			}
		}

		// Token: 0x17000098 RID: 152
		// (get) Token: 0x060003F5 RID: 1013 RVA: 0x0000F30C File Offset: 0x0000D50C
		public string SelectedText
		{
			get
			{
				string result;
				if (this.cursorIndex == this.selectIndex)
				{
					result = "";
				}
				else if (this.cursorIndex < this.selectIndex)
				{
					result = this.text.Substring(this.cursorIndex, this.selectIndex - this.cursorIndex);
				}
				else
				{
					result = this.text.Substring(this.selectIndex, this.cursorIndex - this.selectIndex);
				}
				return result;
			}
		}

		// Token: 0x060003F6 RID: 1014 RVA: 0x0000F390 File Offset: 0x0000D590
		public bool DeleteSelection()
		{
			bool result;
			if (this.cursorIndex == this.selectIndex)
			{
				result = false;
			}
			else
			{
				if (this.cursorIndex < this.selectIndex)
				{
					this.m_Content.text = this.text.Substring(0, this.cursorIndex) + this.text.Substring(this.selectIndex, this.text.Length - this.selectIndex);
					this.selectIndex = this.cursorIndex;
				}
				else
				{
					this.m_Content.text = this.text.Substring(0, this.selectIndex) + this.text.Substring(this.cursorIndex, this.text.Length - this.cursorIndex);
					this.cursorIndex = this.selectIndex;
				}
				this.ClearCursorPos();
				result = true;
			}
			return result;
		}

		// Token: 0x060003F7 RID: 1015 RVA: 0x0000F480 File Offset: 0x0000D680
		public void ReplaceSelection(string replace)
		{
			this.DeleteSelection();
			this.m_Content.text = this.text.Insert(this.cursorIndex, replace);
			this.selectIndex = (this.cursorIndex += replace.Length);
			this.ClearCursorPos();
		}

		// Token: 0x060003F8 RID: 1016 RVA: 0x0000F4D4 File Offset: 0x0000D6D4
		public void Insert(char c)
		{
			this.ReplaceSelection(c.ToString());
		}

		// Token: 0x060003F9 RID: 1017 RVA: 0x0000F4EC File Offset: 0x0000D6EC
		public void MoveSelectionToAltCursor()
		{
			if (this.m_iAltCursorPos != -1)
			{
				int iAltCursorPos = this.m_iAltCursorPos;
				string selectedText = this.SelectedText;
				this.m_Content.text = this.text.Insert(iAltCursorPos, selectedText);
				if (iAltCursorPos < this.cursorIndex)
				{
					this.cursorIndex += selectedText.Length;
					this.selectIndex += selectedText.Length;
				}
				this.DeleteSelection();
				int num = iAltCursorPos;
				this.cursorIndex = num;
				this.selectIndex = num;
				this.ClearCursorPos();
			}
		}

		// Token: 0x060003FA RID: 1018 RVA: 0x0000F584 File Offset: 0x0000D784
		public void MoveRight()
		{
			this.ClearCursorPos();
			if (this.selectIndex == this.cursorIndex)
			{
				this.cursorIndex = this.NextCodePointIndex(this.cursorIndex);
				this.DetectFocusChange();
				this.selectIndex = this.cursorIndex;
			}
			else if (this.selectIndex > this.cursorIndex)
			{
				this.cursorIndex = this.selectIndex;
			}
			else
			{
				this.selectIndex = this.cursorIndex;
			}
		}

		// Token: 0x060003FB RID: 1019 RVA: 0x0000F604 File Offset: 0x0000D804
		public void MoveLeft()
		{
			if (this.selectIndex == this.cursorIndex)
			{
				this.cursorIndex = this.PreviousCodePointIndex(this.cursorIndex);
				this.selectIndex = this.cursorIndex;
			}
			else if (this.selectIndex > this.cursorIndex)
			{
				this.selectIndex = this.cursorIndex;
			}
			else
			{
				this.cursorIndex = this.selectIndex;
			}
			this.ClearCursorPos();
		}

		// Token: 0x060003FC RID: 1020 RVA: 0x0000F680 File Offset: 0x0000D880
		public void MoveUp()
		{
			if (this.selectIndex < this.cursorIndex)
			{
				this.selectIndex = this.cursorIndex;
			}
			else
			{
				this.cursorIndex = this.selectIndex;
			}
			this.GrabGraphicalCursorPos();
			this.graphicalCursorPos.y = this.graphicalCursorPos.y - 1f;
			int cursorStringIndex = this.style.GetCursorStringIndex(this.localPosition, this.m_Content, this.graphicalCursorPos);
			this.selectIndex = cursorStringIndex;
			this.cursorIndex = cursorStringIndex;
			if (this.cursorIndex <= 0)
			{
				this.ClearCursorPos();
			}
		}

		// Token: 0x060003FD RID: 1021 RVA: 0x0000F718 File Offset: 0x0000D918
		public void MoveDown()
		{
			if (this.selectIndex > this.cursorIndex)
			{
				this.selectIndex = this.cursorIndex;
			}
			else
			{
				this.cursorIndex = this.selectIndex;
			}
			this.GrabGraphicalCursorPos();
			this.graphicalCursorPos.y = this.graphicalCursorPos.y + (this.style.lineHeight + 5f);
			int cursorStringIndex = this.style.GetCursorStringIndex(this.localPosition, this.m_Content, this.graphicalCursorPos);
			this.selectIndex = cursorStringIndex;
			this.cursorIndex = cursorStringIndex;
			if (this.cursorIndex == this.text.Length)
			{
				this.ClearCursorPos();
			}
		}

		// Token: 0x060003FE RID: 1022 RVA: 0x0000F7C8 File Offset: 0x0000D9C8
		public void MoveLineStart()
		{
			int num = (this.selectIndex >= this.cursorIndex) ? this.cursorIndex : this.selectIndex;
			int num2 = num;
			int num3;
			while (num2-- != 0)
			{
				if (this.text[num2] == '\n')
				{
					num3 = num2 + 1;
					this.cursorIndex = num3;
					this.selectIndex = num3;
					return;
				}
			}
			num3 = 0;
			this.cursorIndex = num3;
			this.selectIndex = num3;
		}

		// Token: 0x060003FF RID: 1023 RVA: 0x0000F848 File Offset: 0x0000DA48
		public void MoveLineEnd()
		{
			int num = (this.selectIndex <= this.cursorIndex) ? this.cursorIndex : this.selectIndex;
			int i = num;
			int length = this.text.Length;
			int num2;
			while (i < length)
			{
				if (this.text[i] == '\n')
				{
					num2 = i;
					this.cursorIndex = num2;
					this.selectIndex = num2;
					return;
				}
				i++;
			}
			num2 = length;
			this.cursorIndex = num2;
			this.selectIndex = num2;
		}

		// Token: 0x06000400 RID: 1024 RVA: 0x0000F8D4 File Offset: 0x0000DAD4
		public void MoveGraphicalLineStart()
		{
			int graphicalLineStart = this.GetGraphicalLineStart((this.cursorIndex >= this.selectIndex) ? this.selectIndex : this.cursorIndex);
			this.selectIndex = graphicalLineStart;
			this.cursorIndex = graphicalLineStart;
		}

		// Token: 0x06000401 RID: 1025 RVA: 0x0000F91C File Offset: 0x0000DB1C
		public void MoveGraphicalLineEnd()
		{
			int graphicalLineEnd = this.GetGraphicalLineEnd((this.cursorIndex <= this.selectIndex) ? this.selectIndex : this.cursorIndex);
			this.selectIndex = graphicalLineEnd;
			this.cursorIndex = graphicalLineEnd;
		}

		// Token: 0x06000402 RID: 1026 RVA: 0x0000F964 File Offset: 0x0000DB64
		public void MoveTextStart()
		{
			int num = 0;
			this.cursorIndex = num;
			this.selectIndex = num;
		}

		// Token: 0x06000403 RID: 1027 RVA: 0x0000F984 File Offset: 0x0000DB84
		public void MoveTextEnd()
		{
			int length = this.text.Length;
			this.cursorIndex = length;
			this.selectIndex = length;
		}

		// Token: 0x06000404 RID: 1028 RVA: 0x0000F9AC File Offset: 0x0000DBAC
		private int IndexOfEndOfLine(int startIndex)
		{
			int num = this.text.IndexOf('\n', startIndex);
			return (num == -1) ? this.text.Length : num;
		}

		// Token: 0x06000405 RID: 1029 RVA: 0x0000F9E8 File Offset: 0x0000DBE8
		public void MoveParagraphForward()
		{
			this.cursorIndex = ((this.cursorIndex <= this.selectIndex) ? this.selectIndex : this.cursorIndex);
			if (this.cursorIndex < this.text.Length)
			{
				int num = this.IndexOfEndOfLine(this.cursorIndex + 1);
				this.cursorIndex = num;
				this.selectIndex = num;
			}
		}

		// Token: 0x06000406 RID: 1030 RVA: 0x0000FA54 File Offset: 0x0000DC54
		public void MoveParagraphBackward()
		{
			this.cursorIndex = ((this.cursorIndex >= this.selectIndex) ? this.selectIndex : this.cursorIndex);
			if (this.cursorIndex > 1)
			{
				int num = this.text.LastIndexOf('\n', this.cursorIndex - 2) + 1;
				this.cursorIndex = num;
				this.selectIndex = num;
			}
			else
			{
				int num = 0;
				this.cursorIndex = num;
				this.selectIndex = num;
			}
		}

		// Token: 0x06000407 RID: 1031 RVA: 0x0000FAD3 File Offset: 0x0000DCD3
		public void MoveCursorToPosition(Vector2 cursorPosition)
		{
			this.MoveCursorToPosition_Internal(cursorPosition, Event.current.shift);
		}

		// Token: 0x06000408 RID: 1032 RVA: 0x0000FAE8 File Offset: 0x0000DCE8
		protected internal void MoveCursorToPosition_Internal(Vector2 cursorPosition, bool shift)
		{
			this.selectIndex = this.style.GetCursorStringIndex(this.localPosition, this.m_Content, cursorPosition + this.scrollOffset);
			if (!shift)
			{
				this.cursorIndex = this.selectIndex;
			}
			this.DetectFocusChange();
		}

		// Token: 0x06000409 RID: 1033 RVA: 0x0000FB3C File Offset: 0x0000DD3C
		public void MoveAltCursorToPosition(Vector2 cursorPosition)
		{
			int cursorStringIndex = this.style.GetCursorStringIndex(this.localPosition, this.m_Content, cursorPosition + this.scrollOffset);
			this.m_iAltCursorPos = Mathf.Min(this.text.Length, cursorStringIndex);
			this.DetectFocusChange();
		}

		// Token: 0x0600040A RID: 1034 RVA: 0x0000FB8C File Offset: 0x0000DD8C
		public bool IsOverSelection(Vector2 cursorPosition)
		{
			int cursorStringIndex = this.style.GetCursorStringIndex(this.localPosition, this.m_Content, cursorPosition + this.scrollOffset);
			return cursorStringIndex < Mathf.Max(this.cursorIndex, this.selectIndex) && cursorStringIndex > Mathf.Min(this.cursorIndex, this.selectIndex);
		}

		// Token: 0x0600040B RID: 1035 RVA: 0x0000FBF4 File Offset: 0x0000DDF4
		public void SelectToPosition(Vector2 cursorPosition)
		{
			if (!this.m_MouseDragSelectsWholeWords)
			{
				this.cursorIndex = this.style.GetCursorStringIndex(this.localPosition, this.m_Content, cursorPosition + this.scrollOffset);
			}
			else
			{
				int cursorStringIndex = this.style.GetCursorStringIndex(this.localPosition, this.m_Content, cursorPosition + this.scrollOffset);
				this.EnsureValidCodePointIndex(ref cursorStringIndex);
				this.EnsureValidCodePointIndex(ref this.m_DblClickInitPos);
				if (this.m_DblClickSnap == TextEditor.DblClickSnapping.WORDS)
				{
					if (cursorStringIndex < this.m_DblClickInitPos)
					{
						this.cursorIndex = this.FindEndOfClassification(cursorStringIndex, TextEditor.Direction.Backward);
						this.selectIndex = this.FindEndOfClassification(this.m_DblClickInitPos, TextEditor.Direction.Forward);
					}
					else
					{
						this.cursorIndex = this.FindEndOfClassification(cursorStringIndex, TextEditor.Direction.Forward);
						this.selectIndex = this.FindEndOfClassification(this.m_DblClickInitPos, TextEditor.Direction.Backward);
					}
				}
				else if (cursorStringIndex < this.m_DblClickInitPos)
				{
					if (cursorStringIndex > 0)
					{
						this.cursorIndex = this.text.LastIndexOf('\n', Mathf.Max(0, cursorStringIndex - 2)) + 1;
					}
					else
					{
						this.cursorIndex = 0;
					}
					this.selectIndex = this.text.LastIndexOf('\n', this.m_DblClickInitPos);
				}
				else
				{
					if (cursorStringIndex < this.text.Length)
					{
						this.cursorIndex = this.IndexOfEndOfLine(cursorStringIndex);
					}
					else
					{
						this.cursorIndex = this.text.Length;
					}
					this.selectIndex = this.text.LastIndexOf('\n', Mathf.Max(0, this.m_DblClickInitPos - 2)) + 1;
				}
			}
		}

		// Token: 0x0600040C RID: 1036 RVA: 0x0000FD98 File Offset: 0x0000DF98
		public void SelectLeft()
		{
			if (this.m_bJustSelected && this.cursorIndex > this.selectIndex)
			{
				int cursorIndex = this.cursorIndex;
				this.cursorIndex = this.selectIndex;
				this.selectIndex = cursorIndex;
			}
			this.m_bJustSelected = false;
			this.cursorIndex = this.PreviousCodePointIndex(this.cursorIndex);
		}

		// Token: 0x0600040D RID: 1037 RVA: 0x0000FDF8 File Offset: 0x0000DFF8
		public void SelectRight()
		{
			if (this.m_bJustSelected && this.cursorIndex < this.selectIndex)
			{
				int cursorIndex = this.cursorIndex;
				this.cursorIndex = this.selectIndex;
				this.selectIndex = cursorIndex;
			}
			this.m_bJustSelected = false;
			this.cursorIndex = this.NextCodePointIndex(this.cursorIndex);
		}

		// Token: 0x0600040E RID: 1038 RVA: 0x0000FE58 File Offset: 0x0000E058
		public void SelectUp()
		{
			this.GrabGraphicalCursorPos();
			this.graphicalCursorPos.y = this.graphicalCursorPos.y - 1f;
			this.cursorIndex = this.style.GetCursorStringIndex(this.localPosition, this.m_Content, this.graphicalCursorPos);
		}

		// Token: 0x0600040F RID: 1039 RVA: 0x0000FEA8 File Offset: 0x0000E0A8
		public void SelectDown()
		{
			this.GrabGraphicalCursorPos();
			this.graphicalCursorPos.y = this.graphicalCursorPos.y + (this.style.lineHeight + 5f);
			this.cursorIndex = this.style.GetCursorStringIndex(this.localPosition, this.m_Content, this.graphicalCursorPos);
		}

		// Token: 0x06000410 RID: 1040 RVA: 0x0000FF02 File Offset: 0x0000E102
		public void SelectTextEnd()
		{
			this.cursorIndex = this.text.Length;
		}

		// Token: 0x06000411 RID: 1041 RVA: 0x0000FF16 File Offset: 0x0000E116
		public void SelectTextStart()
		{
			this.cursorIndex = 0;
		}

		// Token: 0x06000412 RID: 1042 RVA: 0x0000FF20 File Offset: 0x0000E120
		public void MouseDragSelectsWholeWords(bool on)
		{
			this.m_MouseDragSelectsWholeWords = on;
			this.m_DblClickInitPos = this.cursorIndex;
		}

		// Token: 0x06000413 RID: 1043 RVA: 0x0000EE73 File Offset: 0x0000D073
		public void DblClickSnap(TextEditor.DblClickSnapping snapping)
		{
			this.m_DblClickSnap = snapping;
		}

		// Token: 0x06000414 RID: 1044 RVA: 0x0000FF38 File Offset: 0x0000E138
		private int GetGraphicalLineStart(int p)
		{
			Vector2 cursorPixelPosition = this.style.GetCursorPixelPosition(this.localPosition, this.m_Content, p);
			cursorPixelPosition.x = 0f;
			return this.style.GetCursorStringIndex(this.localPosition, this.m_Content, cursorPixelPosition);
		}

		// Token: 0x06000415 RID: 1045 RVA: 0x0000FF8C File Offset: 0x0000E18C
		private int GetGraphicalLineEnd(int p)
		{
			Vector2 cursorPixelPosition = this.style.GetCursorPixelPosition(this.localPosition, this.m_Content, p);
			cursorPixelPosition.x += 5000f;
			return this.style.GetCursorStringIndex(this.localPosition, this.m_Content, cursorPixelPosition);
		}

		// Token: 0x06000416 RID: 1046 RVA: 0x0000FFE8 File Offset: 0x0000E1E8
		private int FindNextSeperator(int startPos)
		{
			int length = this.text.Length;
			while (startPos < length && this.ClassifyChar(startPos) != TextEditor.CharacterType.LetterLike)
			{
				startPos = this.NextCodePointIndex(startPos);
			}
			while (startPos < length && this.ClassifyChar(startPos) == TextEditor.CharacterType.LetterLike)
			{
				startPos = this.NextCodePointIndex(startPos);
			}
			return startPos;
		}

		// Token: 0x06000417 RID: 1047 RVA: 0x0001004C File Offset: 0x0000E24C
		private int FindPrevSeperator(int startPos)
		{
			startPos = this.PreviousCodePointIndex(startPos);
			while (startPos > 0 && this.ClassifyChar(startPos) != TextEditor.CharacterType.LetterLike)
			{
				startPos = this.PreviousCodePointIndex(startPos);
			}
			int result;
			if (startPos == 0)
			{
				result = 0;
			}
			else
			{
				while (startPos > 0 && this.ClassifyChar(startPos) == TextEditor.CharacterType.LetterLike)
				{
					startPos = this.PreviousCodePointIndex(startPos);
				}
				if (this.ClassifyChar(startPos) == TextEditor.CharacterType.LetterLike)
				{
					result = startPos;
				}
				else
				{
					result = this.NextCodePointIndex(startPos);
				}
			}
			return result;
		}

		// Token: 0x06000418 RID: 1048 RVA: 0x000100D4 File Offset: 0x0000E2D4
		public void MoveWordRight()
		{
			this.cursorIndex = ((this.cursorIndex <= this.selectIndex) ? this.selectIndex : this.cursorIndex);
			int num = this.FindNextSeperator(this.cursorIndex);
			this.selectIndex = num;
			this.cursorIndex = num;
			this.ClearCursorPos();
		}

		// Token: 0x06000419 RID: 1049 RVA: 0x0001012C File Offset: 0x0000E32C
		public void MoveToStartOfNextWord()
		{
			this.ClearCursorPos();
			if (this.cursorIndex != this.selectIndex)
			{
				this.MoveRight();
			}
			else
			{
				int num = this.FindStartOfNextWord(this.cursorIndex);
				this.selectIndex = num;
				this.cursorIndex = num;
			}
		}

		// Token: 0x0600041A RID: 1050 RVA: 0x00010178 File Offset: 0x0000E378
		public void MoveToEndOfPreviousWord()
		{
			this.ClearCursorPos();
			if (this.cursorIndex != this.selectIndex)
			{
				this.MoveLeft();
			}
			else
			{
				int num = this.FindEndOfPreviousWord(this.cursorIndex);
				this.selectIndex = num;
				this.cursorIndex = num;
			}
		}

		// Token: 0x0600041B RID: 1051 RVA: 0x000101C4 File Offset: 0x0000E3C4
		public void SelectToStartOfNextWord()
		{
			this.ClearCursorPos();
			this.cursorIndex = this.FindStartOfNextWord(this.cursorIndex);
		}

		// Token: 0x0600041C RID: 1052 RVA: 0x000101DF File Offset: 0x0000E3DF
		public void SelectToEndOfPreviousWord()
		{
			this.ClearCursorPos();
			this.cursorIndex = this.FindEndOfPreviousWord(this.cursorIndex);
		}

		// Token: 0x0600041D RID: 1053 RVA: 0x000101FC File Offset: 0x0000E3FC
		private TextEditor.CharacterType ClassifyChar(int index)
		{
			TextEditor.CharacterType result;
			if (char.IsWhiteSpace(this.text, index))
			{
				result = TextEditor.CharacterType.WhiteSpace;
			}
			else if (char.IsLetterOrDigit(this.text, index) || this.text[index] == '\'')
			{
				result = TextEditor.CharacterType.LetterLike;
			}
			else
			{
				result = TextEditor.CharacterType.Symbol;
			}
			return result;
		}

		// Token: 0x0600041E RID: 1054 RVA: 0x00010258 File Offset: 0x0000E458
		public int FindStartOfNextWord(int p)
		{
			int length = this.text.Length;
			int result;
			if (p == length)
			{
				result = p;
			}
			else
			{
				TextEditor.CharacterType characterType = this.ClassifyChar(p);
				if (characterType != TextEditor.CharacterType.WhiteSpace)
				{
					p = this.NextCodePointIndex(p);
					while (p < length && this.ClassifyChar(p) == characterType)
					{
						p = this.NextCodePointIndex(p);
					}
				}
				else if (this.text[p] == '\t' || this.text[p] == '\n')
				{
					return this.NextCodePointIndex(p);
				}
				if (p == length)
				{
					result = p;
				}
				else
				{
					if (this.text[p] == ' ')
					{
						while (p < length && this.ClassifyChar(p) == TextEditor.CharacterType.WhiteSpace)
						{
							p = this.NextCodePointIndex(p);
						}
					}
					else if (this.text[p] == '\t' || this.text[p] == '\n')
					{
						return p;
					}
					result = p;
				}
			}
			return result;
		}

		// Token: 0x0600041F RID: 1055 RVA: 0x00010378 File Offset: 0x0000E578
		private int FindEndOfPreviousWord(int p)
		{
			int result;
			if (p == 0)
			{
				result = p;
			}
			else
			{
				p = this.PreviousCodePointIndex(p);
				while (p > 0 && this.text[p] == ' ')
				{
					p = this.PreviousCodePointIndex(p);
				}
				TextEditor.CharacterType characterType = this.ClassifyChar(p);
				if (characterType != TextEditor.CharacterType.WhiteSpace)
				{
					while (p > 0 && this.ClassifyChar(this.PreviousCodePointIndex(p)) == characterType)
					{
						p = this.PreviousCodePointIndex(p);
					}
				}
				result = p;
			}
			return result;
		}

		// Token: 0x06000420 RID: 1056 RVA: 0x00010408 File Offset: 0x0000E608
		public void MoveWordLeft()
		{
			this.cursorIndex = ((this.cursorIndex >= this.selectIndex) ? this.selectIndex : this.cursorIndex);
			this.cursorIndex = this.FindPrevSeperator(this.cursorIndex);
			this.selectIndex = this.cursorIndex;
		}

		// Token: 0x06000421 RID: 1057 RVA: 0x0001045C File Offset: 0x0000E65C
		public void SelectWordRight()
		{
			this.ClearCursorPos();
			int selectIndex = this.selectIndex;
			if (this.cursorIndex < this.selectIndex)
			{
				this.selectIndex = this.cursorIndex;
				this.MoveWordRight();
				this.selectIndex = selectIndex;
				this.cursorIndex = ((this.cursorIndex >= this.selectIndex) ? this.selectIndex : this.cursorIndex);
			}
			else
			{
				this.selectIndex = this.cursorIndex;
				this.MoveWordRight();
				this.selectIndex = selectIndex;
			}
		}

		// Token: 0x06000422 RID: 1058 RVA: 0x000104E8 File Offset: 0x0000E6E8
		public void SelectWordLeft()
		{
			this.ClearCursorPos();
			int selectIndex = this.selectIndex;
			if (this.cursorIndex > this.selectIndex)
			{
				this.selectIndex = this.cursorIndex;
				this.MoveWordLeft();
				this.selectIndex = selectIndex;
				this.cursorIndex = ((this.cursorIndex <= this.selectIndex) ? this.selectIndex : this.cursorIndex);
			}
			else
			{
				this.selectIndex = this.cursorIndex;
				this.MoveWordLeft();
				this.selectIndex = selectIndex;
			}
		}

		// Token: 0x06000423 RID: 1059 RVA: 0x00010574 File Offset: 0x0000E774
		public void ExpandSelectGraphicalLineStart()
		{
			this.ClearCursorPos();
			if (this.cursorIndex < this.selectIndex)
			{
				this.cursorIndex = this.GetGraphicalLineStart(this.cursorIndex);
			}
			else
			{
				int cursorIndex = this.cursorIndex;
				this.cursorIndex = this.GetGraphicalLineStart(this.selectIndex);
				this.selectIndex = cursorIndex;
			}
		}

		// Token: 0x06000424 RID: 1060 RVA: 0x000105D4 File Offset: 0x0000E7D4
		public void ExpandSelectGraphicalLineEnd()
		{
			this.ClearCursorPos();
			if (this.cursorIndex > this.selectIndex)
			{
				this.cursorIndex = this.GetGraphicalLineEnd(this.cursorIndex);
			}
			else
			{
				int cursorIndex = this.cursorIndex;
				this.cursorIndex = this.GetGraphicalLineEnd(this.selectIndex);
				this.selectIndex = cursorIndex;
			}
		}

		// Token: 0x06000425 RID: 1061 RVA: 0x00010632 File Offset: 0x0000E832
		public void SelectGraphicalLineStart()
		{
			this.ClearCursorPos();
			this.cursorIndex = this.GetGraphicalLineStart(this.cursorIndex);
		}

		// Token: 0x06000426 RID: 1062 RVA: 0x0001064D File Offset: 0x0000E84D
		public void SelectGraphicalLineEnd()
		{
			this.ClearCursorPos();
			this.cursorIndex = this.GetGraphicalLineEnd(this.cursorIndex);
		}

		// Token: 0x06000427 RID: 1063 RVA: 0x00010668 File Offset: 0x0000E868
		public void SelectParagraphForward()
		{
			this.ClearCursorPos();
			bool flag = this.cursorIndex < this.selectIndex;
			if (this.cursorIndex < this.text.Length)
			{
				this.cursorIndex = this.IndexOfEndOfLine(this.cursorIndex + 1);
				if (flag && this.cursorIndex > this.selectIndex)
				{
					this.cursorIndex = this.selectIndex;
				}
			}
		}

		// Token: 0x06000428 RID: 1064 RVA: 0x000106DC File Offset: 0x0000E8DC
		public void SelectParagraphBackward()
		{
			this.ClearCursorPos();
			bool flag = this.cursorIndex > this.selectIndex;
			if (this.cursorIndex > 1)
			{
				this.cursorIndex = this.text.LastIndexOf('\n', this.cursorIndex - 2) + 1;
				if (flag && this.cursorIndex < this.selectIndex)
				{
					this.cursorIndex = this.selectIndex;
				}
			}
			else
			{
				int num = 0;
				this.cursorIndex = num;
				this.selectIndex = num;
			}
		}

		// Token: 0x06000429 RID: 1065 RVA: 0x00010764 File Offset: 0x0000E964
		public void SelectCurrentWord()
		{
			int cursorIndex = this.cursorIndex;
			if (this.cursorIndex < this.selectIndex)
			{
				this.cursorIndex = this.FindEndOfClassification(cursorIndex, TextEditor.Direction.Backward);
				this.selectIndex = this.FindEndOfClassification(cursorIndex, TextEditor.Direction.Forward);
			}
			else
			{
				this.cursorIndex = this.FindEndOfClassification(cursorIndex, TextEditor.Direction.Forward);
				this.selectIndex = this.FindEndOfClassification(cursorIndex, TextEditor.Direction.Backward);
			}
			this.ClearCursorPos();
			this.m_bJustSelected = true;
		}

		// Token: 0x0600042A RID: 1066 RVA: 0x000107D8 File Offset: 0x0000E9D8
		private int FindEndOfClassification(int p, TextEditor.Direction dir)
		{
			int result;
			if (this.text.Length == 0)
			{
				result = 0;
			}
			else
			{
				if (p == this.text.Length)
				{
					p = this.PreviousCodePointIndex(p);
				}
				TextEditor.CharacterType characterType = this.ClassifyChar(p);
				for (;;)
				{
					if (dir != TextEditor.Direction.Backward)
					{
						if (dir == TextEditor.Direction.Forward)
						{
							p = this.NextCodePointIndex(p);
							if (p == this.text.Length)
							{
								goto Block_7;
							}
						}
					}
					else
					{
						p = this.PreviousCodePointIndex(p);
						if (p == 0)
						{
							break;
						}
					}
					if (this.ClassifyChar(p) != characterType)
					{
						goto Block_8;
					}
				}
				return (this.ClassifyChar(0) != characterType) ? this.NextCodePointIndex(0) : 0;
				Block_7:
				return this.text.Length;
				Block_8:
				if (dir == TextEditor.Direction.Forward)
				{
					result = p;
				}
				else
				{
					result = this.NextCodePointIndex(p);
				}
			}
			return result;
		}

		// Token: 0x0600042B RID: 1067 RVA: 0x000108C0 File Offset: 0x0000EAC0
		public void SelectCurrentParagraph()
		{
			this.ClearCursorPos();
			int length = this.text.Length;
			if (this.cursorIndex < length)
			{
				this.cursorIndex = this.IndexOfEndOfLine(this.cursorIndex) + 1;
			}
			if (this.selectIndex != 0)
			{
				this.selectIndex = this.text.LastIndexOf('\n', this.selectIndex - 1) + 1;
			}
		}

		// Token: 0x0600042C RID: 1068 RVA: 0x0001092A File Offset: 0x0000EB2A
		public void UpdateScrollOffsetIfNeeded(Event evt)
		{
			if (evt.type != EventType.Repaint && evt.type != EventType.Layout)
			{
				this.UpdateScrollOffset();
			}
		}

		// Token: 0x0600042D RID: 1069 RVA: 0x00010950 File Offset: 0x0000EB50
		[VisibleToOtherModules]
		internal void UpdateScrollOffset()
		{
			int cursorIndex = this.cursorIndex;
			this.graphicalCursorPos = this.style.GetCursorPixelPosition(new Rect(0f, 0f, this.position.width, this.position.height), this.m_Content, cursorIndex);
			Rect rect = this.style.padding.Remove(this.position);
			Vector2 vector = new Vector2(this.style.CalcSize(this.m_Content).x, this.style.CalcHeight(this.m_Content, this.position.width));
			if (vector.x < this.position.width)
			{
				this.scrollOffset.x = 0f;
			}
			else if (this.m_RevealCursor)
			{
				if (this.graphicalCursorPos.x + 1f > this.scrollOffset.x + rect.width)
				{
					this.scrollOffset.x = this.graphicalCursorPos.x - rect.width;
				}
				if (this.graphicalCursorPos.x < this.scrollOffset.x + (float)this.style.padding.left)
				{
					this.scrollOffset.x = this.graphicalCursorPos.x - (float)this.style.padding.left;
				}
			}
			if (vector.y < rect.height)
			{
				this.scrollOffset.y = 0f;
			}
			else if (this.m_RevealCursor)
			{
				if (this.graphicalCursorPos.y + this.style.lineHeight > this.scrollOffset.y + rect.height + (float)this.style.padding.top)
				{
					this.scrollOffset.y = this.graphicalCursorPos.y - rect.height - (float)this.style.padding.top + this.style.lineHeight;
				}
				if (this.graphicalCursorPos.y < this.scrollOffset.y + (float)this.style.padding.top)
				{
					this.scrollOffset.y = this.graphicalCursorPos.y - (float)this.style.padding.top;
				}
			}
			if (this.scrollOffset.y > 0f && vector.y - this.scrollOffset.y < rect.height + (float)this.style.padding.top + (float)this.style.padding.bottom)
			{
				this.scrollOffset.y = vector.y - rect.height - (float)this.style.padding.top - (float)this.style.padding.bottom;
			}
			this.scrollOffset.y = ((this.scrollOffset.y >= 0f) ? this.scrollOffset.y : 0f);
			this.m_RevealCursor = false;
		}

		// Token: 0x0600042E RID: 1070 RVA: 0x00010CB8 File Offset: 0x0000EEB8
		public void DrawCursor(string newText)
		{
			string text = this.text;
			int num = this.cursorIndex;
			if (Input.compositionString.Length > 0)
			{
				this.m_Content.text = newText.Substring(0, this.cursorIndex) + Input.compositionString + newText.Substring(this.selectIndex);
				num += Input.compositionString.Length;
			}
			else
			{
				this.m_Content.text = newText;
			}
			this.graphicalCursorPos = this.style.GetCursorPixelPosition(new Rect(0f, 0f, this.position.width, this.position.height), this.m_Content, num);
			Vector2 contentOffset = this.style.contentOffset;
			this.style.contentOffset -= this.scrollOffset;
			this.style.Internal_clipOffset = this.scrollOffset;
			Input.compositionCursorPos = this.graphicalCursorPos + new Vector2(this.position.x, this.position.y + this.style.lineHeight) - this.scrollOffset;
			if (Input.compositionString.Length > 0)
			{
				this.style.DrawWithTextSelection(this.position, this.m_Content, this.controlID, this.cursorIndex, this.cursorIndex + Input.compositionString.Length, true);
			}
			else
			{
				this.style.DrawWithTextSelection(this.position, this.m_Content, this.controlID, this.cursorIndex, this.selectIndex);
			}
			if (this.m_iAltCursorPos != -1)
			{
				this.style.DrawCursor(this.position, this.m_Content, this.controlID, this.m_iAltCursorPos);
			}
			this.style.contentOffset = contentOffset;
			this.style.Internal_clipOffset = Vector2.zero;
			this.m_Content.text = text;
		}

		// Token: 0x0600042F RID: 1071 RVA: 0x00010EC4 File Offset: 0x0000F0C4
		private bool PerformOperation(TextEditor.TextEditOp operation)
		{
			this.m_RevealCursor = true;
			switch (operation)
			{
			case TextEditor.TextEditOp.MoveLeft:
				this.MoveLeft();
				goto IL_2BA;
			case TextEditor.TextEditOp.MoveRight:
				this.MoveRight();
				goto IL_2BA;
			case TextEditor.TextEditOp.MoveUp:
				this.MoveUp();
				goto IL_2BA;
			case TextEditor.TextEditOp.MoveDown:
				this.MoveDown();
				goto IL_2BA;
			case TextEditor.TextEditOp.MoveLineStart:
				this.MoveLineStart();
				goto IL_2BA;
			case TextEditor.TextEditOp.MoveLineEnd:
				this.MoveLineEnd();
				goto IL_2BA;
			case TextEditor.TextEditOp.MoveTextStart:
				this.MoveTextStart();
				goto IL_2BA;
			case TextEditor.TextEditOp.MoveTextEnd:
				this.MoveTextEnd();
				goto IL_2BA;
			case TextEditor.TextEditOp.MoveGraphicalLineStart:
				this.MoveGraphicalLineStart();
				goto IL_2BA;
			case TextEditor.TextEditOp.MoveGraphicalLineEnd:
				this.MoveGraphicalLineEnd();
				goto IL_2BA;
			case TextEditor.TextEditOp.MoveWordLeft:
				this.MoveWordLeft();
				goto IL_2BA;
			case TextEditor.TextEditOp.MoveWordRight:
				this.MoveWordRight();
				goto IL_2BA;
			case TextEditor.TextEditOp.MoveParagraphForward:
				this.MoveParagraphForward();
				goto IL_2BA;
			case TextEditor.TextEditOp.MoveParagraphBackward:
				this.MoveParagraphBackward();
				goto IL_2BA;
			case TextEditor.TextEditOp.MoveToStartOfNextWord:
				this.MoveToStartOfNextWord();
				goto IL_2BA;
			case TextEditor.TextEditOp.MoveToEndOfPreviousWord:
				this.MoveToEndOfPreviousWord();
				goto IL_2BA;
			case TextEditor.TextEditOp.SelectLeft:
				this.SelectLeft();
				goto IL_2BA;
			case TextEditor.TextEditOp.SelectRight:
				this.SelectRight();
				goto IL_2BA;
			case TextEditor.TextEditOp.SelectUp:
				this.SelectUp();
				goto IL_2BA;
			case TextEditor.TextEditOp.SelectDown:
				this.SelectDown();
				goto IL_2BA;
			case TextEditor.TextEditOp.SelectTextStart:
				this.SelectTextStart();
				goto IL_2BA;
			case TextEditor.TextEditOp.SelectTextEnd:
				this.SelectTextEnd();
				goto IL_2BA;
			case TextEditor.TextEditOp.ExpandSelectGraphicalLineStart:
				this.ExpandSelectGraphicalLineStart();
				goto IL_2BA;
			case TextEditor.TextEditOp.ExpandSelectGraphicalLineEnd:
				this.ExpandSelectGraphicalLineEnd();
				goto IL_2BA;
			case TextEditor.TextEditOp.SelectGraphicalLineStart:
				this.SelectGraphicalLineStart();
				goto IL_2BA;
			case TextEditor.TextEditOp.SelectGraphicalLineEnd:
				this.SelectGraphicalLineEnd();
				goto IL_2BA;
			case TextEditor.TextEditOp.SelectWordLeft:
				this.SelectWordLeft();
				goto IL_2BA;
			case TextEditor.TextEditOp.SelectWordRight:
				this.SelectWordRight();
				goto IL_2BA;
			case TextEditor.TextEditOp.SelectToEndOfPreviousWord:
				this.SelectToEndOfPreviousWord();
				goto IL_2BA;
			case TextEditor.TextEditOp.SelectToStartOfNextWord:
				this.SelectToStartOfNextWord();
				goto IL_2BA;
			case TextEditor.TextEditOp.SelectParagraphBackward:
				this.SelectParagraphBackward();
				goto IL_2BA;
			case TextEditor.TextEditOp.SelectParagraphForward:
				this.SelectParagraphForward();
				goto IL_2BA;
			case TextEditor.TextEditOp.Delete:
				return this.Delete();
			case TextEditor.TextEditOp.Backspace:
				return this.Backspace();
			case TextEditor.TextEditOp.DeleteWordBack:
				return this.DeleteWordBack();
			case TextEditor.TextEditOp.DeleteWordForward:
				return this.DeleteWordForward();
			case TextEditor.TextEditOp.DeleteLineBack:
				return this.DeleteLineBack();
			case TextEditor.TextEditOp.Cut:
				return this.Cut();
			case TextEditor.TextEditOp.Copy:
				this.Copy();
				goto IL_2BA;
			case TextEditor.TextEditOp.Paste:
				return this.Paste();
			case TextEditor.TextEditOp.SelectAll:
				this.SelectAll();
				goto IL_2BA;
			case TextEditor.TextEditOp.SelectNone:
				this.SelectNone();
				goto IL_2BA;
			}
			Debug.Log("Unimplemented: " + operation);
			IL_2BA:
			return false;
		}

		// Token: 0x06000430 RID: 1072 RVA: 0x00011193 File Offset: 0x0000F393
		public void SaveBackup()
		{
			this.oldText = this.text;
			this.oldPos = this.cursorIndex;
			this.oldSelectPos = this.selectIndex;
		}

		// Token: 0x06000431 RID: 1073 RVA: 0x000111BA File Offset: 0x0000F3BA
		public void Undo()
		{
			this.m_Content.text = this.oldText;
			this.cursorIndex = this.oldPos;
			this.selectIndex = this.oldSelectPos;
		}

		// Token: 0x06000432 RID: 1074 RVA: 0x000111E8 File Offset: 0x0000F3E8
		public bool Cut()
		{
			bool result;
			if (this.isPasswordField)
			{
				result = false;
			}
			else
			{
				this.Copy();
				result = this.DeleteSelection();
			}
			return result;
		}

		// Token: 0x06000433 RID: 1075 RVA: 0x0001121C File Offset: 0x0000F41C
		public void Copy()
		{
			if (this.selectIndex != this.cursorIndex)
			{
				if (!this.isPasswordField)
				{
					string systemCopyBuffer;
					if (this.cursorIndex < this.selectIndex)
					{
						systemCopyBuffer = this.text.Substring(this.cursorIndex, this.selectIndex - this.cursorIndex);
					}
					else
					{
						systemCopyBuffer = this.text.Substring(this.selectIndex, this.cursorIndex - this.selectIndex);
					}
					GUIUtility.systemCopyBuffer = systemCopyBuffer;
				}
			}
		}

		// Token: 0x06000434 RID: 1076 RVA: 0x000112AC File Offset: 0x0000F4AC
		private static string ReplaceNewlinesWithSpaces(string value)
		{
			value = value.Replace("\r\n", " ");
			value = value.Replace('\n', ' ');
			value = value.Replace('\r', ' ');
			return value;
		}

		// Token: 0x06000435 RID: 1077 RVA: 0x000112EC File Offset: 0x0000F4EC
		public bool Paste()
		{
			string text = GUIUtility.systemCopyBuffer;
			bool result;
			if (text != "")
			{
				if (!this.multiline)
				{
					text = TextEditor.ReplaceNewlinesWithSpaces(text);
				}
				this.ReplaceSelection(text);
				result = true;
			}
			else
			{
				result = false;
			}
			return result;
		}

		// Token: 0x06000436 RID: 1078 RVA: 0x00011339 File Offset: 0x0000F539
		private static void MapKey(string key, TextEditor.TextEditOp action)
		{
			TextEditor.s_Keyactions[Event.KeyboardEvent(key)] = action;
		}

		// Token: 0x06000437 RID: 1079 RVA: 0x00011350 File Offset: 0x0000F550
		private void InitKeyActions()
		{
			if (TextEditor.s_Keyactions == null)
			{
				TextEditor.s_Keyactions = new Dictionary<Event, TextEditor.TextEditOp>();
				TextEditor.MapKey("left", TextEditor.TextEditOp.MoveLeft);
				TextEditor.MapKey("right", TextEditor.TextEditOp.MoveRight);
				TextEditor.MapKey("up", TextEditor.TextEditOp.MoveUp);
				TextEditor.MapKey("down", TextEditor.TextEditOp.MoveDown);
				TextEditor.MapKey("#left", TextEditor.TextEditOp.SelectLeft);
				TextEditor.MapKey("#right", TextEditor.TextEditOp.SelectRight);
				TextEditor.MapKey("#up", TextEditor.TextEditOp.SelectUp);
				TextEditor.MapKey("#down", TextEditor.TextEditOp.SelectDown);
				TextEditor.MapKey("delete", TextEditor.TextEditOp.Delete);
				TextEditor.MapKey("backspace", TextEditor.TextEditOp.Backspace);
				TextEditor.MapKey("#backspace", TextEditor.TextEditOp.Backspace);
				if (SystemInfo.operatingSystemFamily == OperatingSystemFamily.MacOSX)
				{
					TextEditor.MapKey("^left", TextEditor.TextEditOp.MoveGraphicalLineStart);
					TextEditor.MapKey("^right", TextEditor.TextEditOp.MoveGraphicalLineEnd);
					TextEditor.MapKey("&left", TextEditor.TextEditOp.MoveWordLeft);
					TextEditor.MapKey("&right", TextEditor.TextEditOp.MoveWordRight);
					TextEditor.MapKey("&up", TextEditor.TextEditOp.MoveParagraphBackward);
					TextEditor.MapKey("&down", TextEditor.TextEditOp.MoveParagraphForward);
					TextEditor.MapKey("%left", TextEditor.TextEditOp.MoveGraphicalLineStart);
					TextEditor.MapKey("%right", TextEditor.TextEditOp.MoveGraphicalLineEnd);
					TextEditor.MapKey("%up", TextEditor.TextEditOp.MoveTextStart);
					TextEditor.MapKey("%down", TextEditor.TextEditOp.MoveTextEnd);
					TextEditor.MapKey("#home", TextEditor.TextEditOp.SelectTextStart);
					TextEditor.MapKey("#end", TextEditor.TextEditOp.SelectTextEnd);
					TextEditor.MapKey("#^left", TextEditor.TextEditOp.ExpandSelectGraphicalLineStart);
					TextEditor.MapKey("#^right", TextEditor.TextEditOp.ExpandSelectGraphicalLineEnd);
					TextEditor.MapKey("#^up", TextEditor.TextEditOp.SelectParagraphBackward);
					TextEditor.MapKey("#^down", TextEditor.TextEditOp.SelectParagraphForward);
					TextEditor.MapKey("#&left", TextEditor.TextEditOp.SelectWordLeft);
					TextEditor.MapKey("#&right", TextEditor.TextEditOp.SelectWordRight);
					TextEditor.MapKey("#&up", TextEditor.TextEditOp.SelectParagraphBackward);
					TextEditor.MapKey("#&down", TextEditor.TextEditOp.SelectParagraphForward);
					TextEditor.MapKey("#%left", TextEditor.TextEditOp.ExpandSelectGraphicalLineStart);
					TextEditor.MapKey("#%right", TextEditor.TextEditOp.ExpandSelectGraphicalLineEnd);
					TextEditor.MapKey("#%up", TextEditor.TextEditOp.SelectTextStart);
					TextEditor.MapKey("#%down", TextEditor.TextEditOp.SelectTextEnd);
					TextEditor.MapKey("%a", TextEditor.TextEditOp.SelectAll);
					TextEditor.MapKey("%x", TextEditor.TextEditOp.Cut);
					TextEditor.MapKey("%c", TextEditor.TextEditOp.Copy);
					TextEditor.MapKey("%v", TextEditor.TextEditOp.Paste);
					TextEditor.MapKey("^d", TextEditor.TextEditOp.Delete);
					TextEditor.MapKey("^h", TextEditor.TextEditOp.Backspace);
					TextEditor.MapKey("^b", TextEditor.TextEditOp.MoveLeft);
					TextEditor.MapKey("^f", TextEditor.TextEditOp.MoveRight);
					TextEditor.MapKey("^a", TextEditor.TextEditOp.MoveLineStart);
					TextEditor.MapKey("^e", TextEditor.TextEditOp.MoveLineEnd);
					TextEditor.MapKey("&delete", TextEditor.TextEditOp.DeleteWordForward);
					TextEditor.MapKey("&backspace", TextEditor.TextEditOp.DeleteWordBack);
					TextEditor.MapKey("%backspace", TextEditor.TextEditOp.DeleteLineBack);
				}
				else
				{
					TextEditor.MapKey("home", TextEditor.TextEditOp.MoveGraphicalLineStart);
					TextEditor.MapKey("end", TextEditor.TextEditOp.MoveGraphicalLineEnd);
					TextEditor.MapKey("%left", TextEditor.TextEditOp.MoveWordLeft);
					TextEditor.MapKey("%right", TextEditor.TextEditOp.MoveWordRight);
					TextEditor.MapKey("%up", TextEditor.TextEditOp.MoveParagraphBackward);
					TextEditor.MapKey("%down", TextEditor.TextEditOp.MoveParagraphForward);
					TextEditor.MapKey("^left", TextEditor.TextEditOp.MoveToEndOfPreviousWord);
					TextEditor.MapKey("^right", TextEditor.TextEditOp.MoveToStartOfNextWord);
					TextEditor.MapKey("^up", TextEditor.TextEditOp.MoveParagraphBackward);
					TextEditor.MapKey("^down", TextEditor.TextEditOp.MoveParagraphForward);
					TextEditor.MapKey("#^left", TextEditor.TextEditOp.SelectToEndOfPreviousWord);
					TextEditor.MapKey("#^right", TextEditor.TextEditOp.SelectToStartOfNextWord);
					TextEditor.MapKey("#^up", TextEditor.TextEditOp.SelectParagraphBackward);
					TextEditor.MapKey("#^down", TextEditor.TextEditOp.SelectParagraphForward);
					TextEditor.MapKey("#home", TextEditor.TextEditOp.SelectGraphicalLineStart);
					TextEditor.MapKey("#end", TextEditor.TextEditOp.SelectGraphicalLineEnd);
					TextEditor.MapKey("^delete", TextEditor.TextEditOp.DeleteWordForward);
					TextEditor.MapKey("^backspace", TextEditor.TextEditOp.DeleteWordBack);
					TextEditor.MapKey("%backspace", TextEditor.TextEditOp.DeleteLineBack);
					TextEditor.MapKey("^a", TextEditor.TextEditOp.SelectAll);
					TextEditor.MapKey("^x", TextEditor.TextEditOp.Cut);
					TextEditor.MapKey("^c", TextEditor.TextEditOp.Copy);
					TextEditor.MapKey("^v", TextEditor.TextEditOp.Paste);
					TextEditor.MapKey("#delete", TextEditor.TextEditOp.Cut);
					TextEditor.MapKey("^insert", TextEditor.TextEditOp.Copy);
					TextEditor.MapKey("#insert", TextEditor.TextEditOp.Paste);
				}
			}
		}

		// Token: 0x06000438 RID: 1080 RVA: 0x000116F9 File Offset: 0x0000F8F9
		public void DetectFocusChange()
		{
			this.OnDetectFocusChange();
		}

		// Token: 0x06000439 RID: 1081 RVA: 0x00011704 File Offset: 0x0000F904
		internal virtual void OnDetectFocusChange()
		{
			if (this.m_HasFocus && this.controlID != GUIUtility.keyboardControl)
			{
				this.OnLostFocus();
			}
			if (!this.m_HasFocus && this.controlID == GUIUtility.keyboardControl)
			{
				this.OnFocus();
			}
		}

		// Token: 0x0600043A RID: 1082 RVA: 0x000021D8 File Offset: 0x000003D8
		internal virtual void OnCursorIndexChange()
		{
		}

		// Token: 0x0600043B RID: 1083 RVA: 0x000021D8 File Offset: 0x000003D8
		internal virtual void OnSelectIndexChange()
		{
		}

		// Token: 0x0600043C RID: 1084 RVA: 0x00011754 File Offset: 0x0000F954
		private void ClampTextIndex(ref int index)
		{
			index = Mathf.Clamp(index, 0, this.text.Length);
		}

		// Token: 0x0600043D RID: 1085 RVA: 0x0001176C File Offset: 0x0000F96C
		private void EnsureValidCodePointIndex(ref int index)
		{
			this.ClampTextIndex(ref index);
			if (!this.IsValidCodePointIndex(index))
			{
				index = this.NextCodePointIndex(index);
			}
		}

		// Token: 0x0600043E RID: 1086 RVA: 0x00011790 File Offset: 0x0000F990
		private bool IsValidCodePointIndex(int index)
		{
			return index >= 0 && index <= this.text.Length && (index == 0 || index == this.text.Length || !char.IsLowSurrogate(this.text[index]));
		}

		// Token: 0x0600043F RID: 1087 RVA: 0x000117F8 File Offset: 0x0000F9F8
		private int PreviousCodePointIndex(int index)
		{
			if (index > 0)
			{
				index--;
			}
			while (index > 0 && char.IsLowSurrogate(this.text[index]))
			{
				index--;
			}
			return index;
		}

		// Token: 0x06000440 RID: 1088 RVA: 0x00011844 File Offset: 0x0000FA44
		private int NextCodePointIndex(int index)
		{
			if (index < this.text.Length)
			{
				index++;
			}
			while (index < this.text.Length && char.IsLowSurrogate(this.text[index]))
			{
				index++;
			}
			return index;
		}

		// Token: 0x04000119 RID: 281
		public TouchScreenKeyboard keyboardOnScreen = null;

		// Token: 0x0400011A RID: 282
		public int controlID = 0;

		// Token: 0x0400011B RID: 283
		public GUIStyle style = GUIStyle.none;

		// Token: 0x0400011C RID: 284
		public bool multiline = false;

		// Token: 0x0400011D RID: 285
		public bool hasHorizontalCursorPos = false;

		// Token: 0x0400011E RID: 286
		public bool isPasswordField = false;

		// Token: 0x0400011F RID: 287
		[VisibleToOtherModules(new string[]
		{
			"UnityEngine.UIElementsModule"
		})]
		internal bool m_HasFocus;

		// Token: 0x04000120 RID: 288
		public Vector2 scrollOffset = Vector2.zero;

		// Token: 0x04000121 RID: 289
		private GUIContent m_Content = new GUIContent();

		// Token: 0x04000122 RID: 290
		private Rect m_Position;

		// Token: 0x04000123 RID: 291
		private int m_CursorIndex = 0;

		// Token: 0x04000124 RID: 292
		private int m_SelectIndex = 0;

		// Token: 0x04000125 RID: 293
		private bool m_RevealCursor = false;

		// Token: 0x04000126 RID: 294
		public Vector2 graphicalCursorPos;

		// Token: 0x04000127 RID: 295
		public Vector2 graphicalSelectCursorPos;

		// Token: 0x04000128 RID: 296
		private bool m_MouseDragSelectsWholeWords = false;

		// Token: 0x04000129 RID: 297
		private int m_DblClickInitPos = 0;

		// Token: 0x0400012A RID: 298
		private TextEditor.DblClickSnapping m_DblClickSnap = TextEditor.DblClickSnapping.WORDS;

		// Token: 0x0400012B RID: 299
		private bool m_bJustSelected = false;

		// Token: 0x0400012C RID: 300
		private int m_iAltCursorPos = -1;

		// Token: 0x0400012D RID: 301
		private string oldText;

		// Token: 0x0400012E RID: 302
		private int oldPos;

		// Token: 0x0400012F RID: 303
		private int oldSelectPos;

		// Token: 0x04000130 RID: 304
		private static Dictionary<Event, TextEditor.TextEditOp> s_Keyactions;

		// Token: 0x02000036 RID: 54
		public enum DblClickSnapping : byte
		{
			// Token: 0x04000132 RID: 306
			WORDS,
			// Token: 0x04000133 RID: 307
			PARAGRAPHS
		}

		// Token: 0x02000037 RID: 55
		private enum CharacterType
		{
			// Token: 0x04000135 RID: 309
			LetterLike,
			// Token: 0x04000136 RID: 310
			Symbol,
			// Token: 0x04000137 RID: 311
			Symbol2,
			// Token: 0x04000138 RID: 312
			WhiteSpace
		}

		// Token: 0x02000038 RID: 56
		private enum Direction
		{
			// Token: 0x0400013A RID: 314
			Forward,
			// Token: 0x0400013B RID: 315
			Backward
		}

		// Token: 0x02000039 RID: 57
		private enum TextEditOp
		{
			// Token: 0x0400013D RID: 317
			MoveLeft,
			// Token: 0x0400013E RID: 318
			MoveRight,
			// Token: 0x0400013F RID: 319
			MoveUp,
			// Token: 0x04000140 RID: 320
			MoveDown,
			// Token: 0x04000141 RID: 321
			MoveLineStart,
			// Token: 0x04000142 RID: 322
			MoveLineEnd,
			// Token: 0x04000143 RID: 323
			MoveTextStart,
			// Token: 0x04000144 RID: 324
			MoveTextEnd,
			// Token: 0x04000145 RID: 325
			MovePageUp,
			// Token: 0x04000146 RID: 326
			MovePageDown,
			// Token: 0x04000147 RID: 327
			MoveGraphicalLineStart,
			// Token: 0x04000148 RID: 328
			MoveGraphicalLineEnd,
			// Token: 0x04000149 RID: 329
			MoveWordLeft,
			// Token: 0x0400014A RID: 330
			MoveWordRight,
			// Token: 0x0400014B RID: 331
			MoveParagraphForward,
			// Token: 0x0400014C RID: 332
			MoveParagraphBackward,
			// Token: 0x0400014D RID: 333
			MoveToStartOfNextWord,
			// Token: 0x0400014E RID: 334
			MoveToEndOfPreviousWord,
			// Token: 0x0400014F RID: 335
			SelectLeft,
			// Token: 0x04000150 RID: 336
			SelectRight,
			// Token: 0x04000151 RID: 337
			SelectUp,
			// Token: 0x04000152 RID: 338
			SelectDown,
			// Token: 0x04000153 RID: 339
			SelectTextStart,
			// Token: 0x04000154 RID: 340
			SelectTextEnd,
			// Token: 0x04000155 RID: 341
			SelectPageUp,
			// Token: 0x04000156 RID: 342
			SelectPageDown,
			// Token: 0x04000157 RID: 343
			ExpandSelectGraphicalLineStart,
			// Token: 0x04000158 RID: 344
			ExpandSelectGraphicalLineEnd,
			// Token: 0x04000159 RID: 345
			SelectGraphicalLineStart,
			// Token: 0x0400015A RID: 346
			SelectGraphicalLineEnd,
			// Token: 0x0400015B RID: 347
			SelectWordLeft,
			// Token: 0x0400015C RID: 348
			SelectWordRight,
			// Token: 0x0400015D RID: 349
			SelectToEndOfPreviousWord,
			// Token: 0x0400015E RID: 350
			SelectToStartOfNextWord,
			// Token: 0x0400015F RID: 351
			SelectParagraphBackward,
			// Token: 0x04000160 RID: 352
			SelectParagraphForward,
			// Token: 0x04000161 RID: 353
			Delete,
			// Token: 0x04000162 RID: 354
			Backspace,
			// Token: 0x04000163 RID: 355
			DeleteWordBack,
			// Token: 0x04000164 RID: 356
			DeleteWordForward,
			// Token: 0x04000165 RID: 357
			DeleteLineBack,
			// Token: 0x04000166 RID: 358
			Cut,
			// Token: 0x04000167 RID: 359
			Copy,
			// Token: 0x04000168 RID: 360
			Paste,
			// Token: 0x04000169 RID: 361
			SelectAll,
			// Token: 0x0400016A RID: 362
			SelectNone,
			// Token: 0x0400016B RID: 363
			ScrollStart,
			// Token: 0x0400016C RID: 364
			ScrollEnd,
			// Token: 0x0400016D RID: 365
			ScrollPageUp,
			// Token: 0x0400016E RID: 366
			ScrollPageDown
		}
	}
}
