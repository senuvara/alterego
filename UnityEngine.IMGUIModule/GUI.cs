﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;
using UnityEngine.Scripting;
using UnityEngineInternal;

namespace UnityEngine
{
	// Token: 0x02000007 RID: 7
	[NativeHeader("Modules/IMGUI/GUI.bindings.h")]
	[NativeHeader("Modules/IMGUI/GUISkin.bindings.h")]
	public class GUI
	{
		// Token: 0x0600004A RID: 74 RVA: 0x000032EC File Offset: 0x000014EC
		static GUI()
		{
		}

		// Token: 0x0600004B RID: 75 RVA: 0x0000338C File Offset: 0x0000158C
		public GUI()
		{
		}

		// Token: 0x1700001B RID: 27
		// (get) Token: 0x0600004C RID: 76 RVA: 0x00003394 File Offset: 0x00001594
		// (set) Token: 0x0600004D RID: 77 RVA: 0x000033A9 File Offset: 0x000015A9
		public static Color color
		{
			get
			{
				Color result;
				GUI.get_color_Injected(out result);
				return result;
			}
			set
			{
				GUI.set_color_Injected(ref value);
			}
		}

		// Token: 0x1700001C RID: 28
		// (get) Token: 0x0600004E RID: 78 RVA: 0x000033B4 File Offset: 0x000015B4
		// (set) Token: 0x0600004F RID: 79 RVA: 0x000033C9 File Offset: 0x000015C9
		public static Color backgroundColor
		{
			get
			{
				Color result;
				GUI.get_backgroundColor_Injected(out result);
				return result;
			}
			set
			{
				GUI.set_backgroundColor_Injected(ref value);
			}
		}

		// Token: 0x1700001D RID: 29
		// (get) Token: 0x06000050 RID: 80 RVA: 0x000033D4 File Offset: 0x000015D4
		// (set) Token: 0x06000051 RID: 81 RVA: 0x000033E9 File Offset: 0x000015E9
		public static Color contentColor
		{
			get
			{
				Color result;
				GUI.get_contentColor_Injected(out result);
				return result;
			}
			set
			{
				GUI.set_contentColor_Injected(ref value);
			}
		}

		// Token: 0x1700001E RID: 30
		// (get) Token: 0x06000052 RID: 82
		// (set) Token: 0x06000053 RID: 83
		public static extern bool changed { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700001F RID: 31
		// (get) Token: 0x06000054 RID: 84
		// (set) Token: 0x06000055 RID: 85
		public static extern bool enabled { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000020 RID: 32
		// (get) Token: 0x06000056 RID: 86
		// (set) Token: 0x06000057 RID: 87
		public static extern int depth { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000021 RID: 33
		// (get) Token: 0x06000058 RID: 88
		internal static extern bool usePageScrollbars { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000022 RID: 34
		// (get) Token: 0x06000059 RID: 89
		internal static extern Material blendMaterial { [FreeFunction("GetGUIBlendMaterial")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000023 RID: 35
		// (get) Token: 0x0600005A RID: 90
		internal static extern Material blitMaterial { [FreeFunction("GetGUIBlitMaterial")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000024 RID: 36
		// (get) Token: 0x0600005B RID: 91
		internal static extern Material roundedRectMaterial { [FreeFunction("GetGUIRoundedRectMaterial")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x0600005C RID: 92
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void GrabMouseControl(int id);

		// Token: 0x0600005D RID: 93
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool HasMouseControl(int id);

		// Token: 0x0600005E RID: 94
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void ReleaseMouseControl();

		// Token: 0x0600005F RID: 95
		[FreeFunction("GetGUIState().SetNameOfNextControl")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void SetNextControlName(string name);

		// Token: 0x06000060 RID: 96
		[FreeFunction("GetGUIState().GetNameOfFocusedControl")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern string GetNameOfFocusedControl();

		// Token: 0x06000061 RID: 97
		[FreeFunction("GetGUIState().FocusKeyboardControl")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void FocusControl(string name);

		// Token: 0x06000062 RID: 98
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern void InternalRepaintEditorWindow();

		// Token: 0x06000063 RID: 99
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern string Internal_GetTooltip();

		// Token: 0x06000064 RID: 100
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void Internal_SetTooltip(string value);

		// Token: 0x06000065 RID: 101
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern string Internal_GetMouseTooltip();

		// Token: 0x06000066 RID: 102 RVA: 0x000033F4 File Offset: 0x000015F4
		private static Rect Internal_DoModalWindow(int id, int instanceID, Rect clientRect, GUI.WindowFunction func, GUIContent content, GUIStyle style, object skin)
		{
			Rect result;
			GUI.Internal_DoModalWindow_Injected(id, instanceID, ref clientRect, func, content, style, skin, out result);
			return result;
		}

		// Token: 0x06000067 RID: 103 RVA: 0x00003414 File Offset: 0x00001614
		private static Rect Internal_DoWindow(int id, int instanceID, Rect clientRect, GUI.WindowFunction func, GUIContent title, GUIStyle style, object skin, bool forceRectOnLayout)
		{
			Rect result;
			GUI.Internal_DoWindow_Injected(id, instanceID, ref clientRect, func, title, style, skin, forceRectOnLayout, out result);
			return result;
		}

		// Token: 0x06000068 RID: 104 RVA: 0x00003436 File Offset: 0x00001636
		public static void DragWindow(Rect position)
		{
			GUI.DragWindow_Injected(ref position);
		}

		// Token: 0x06000069 RID: 105
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void BringWindowToFront(int windowID);

		// Token: 0x0600006A RID: 106
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void BringWindowToBack(int windowID);

		// Token: 0x0600006B RID: 107
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void FocusWindow(int windowID);

		// Token: 0x0600006C RID: 108
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void UnfocusWindow();

		// Token: 0x0600006D RID: 109
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void Internal_BeginWindows();

		// Token: 0x0600006E RID: 110
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void Internal_EndWindows();

		// Token: 0x0600006F RID: 111
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern string Internal_Concatenate(GUIContent first, GUIContent second);

		// Token: 0x17000025 RID: 37
		// (get) Token: 0x06000070 RID: 112 RVA: 0x00003440 File Offset: 0x00001640
		// (set) Token: 0x06000071 RID: 113 RVA: 0x00003459 File Offset: 0x00001659
		internal static int scrollTroughSide
		{
			[CompilerGenerated]
			get
			{
				return GUI.<scrollTroughSide>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				GUI.<scrollTroughSide>k__BackingField = value;
			}
		}

		// Token: 0x17000026 RID: 38
		// (get) Token: 0x06000072 RID: 114 RVA: 0x00003464 File Offset: 0x00001664
		// (set) Token: 0x06000073 RID: 115 RVA: 0x0000347D File Offset: 0x0000167D
		internal static DateTime nextScrollStepTime
		{
			[CompilerGenerated]
			get
			{
				return GUI.<nextScrollStepTime>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				GUI.<nextScrollStepTime>k__BackingField = value;
			}
		} = DateTime.Now;

		// Token: 0x17000027 RID: 39
		// (get) Token: 0x06000075 RID: 117 RVA: 0x00003494 File Offset: 0x00001694
		// (set) Token: 0x06000074 RID: 116 RVA: 0x00003485 File Offset: 0x00001685
		public static GUISkin skin
		{
			get
			{
				GUIUtility.CheckOnGUI();
				return GUI.s_Skin;
			}
			set
			{
				GUIUtility.CheckOnGUI();
				GUI.DoSetSkin(value);
			}
		}

		// Token: 0x06000076 RID: 118 RVA: 0x000034B3 File Offset: 0x000016B3
		internal static void DoSetSkin(GUISkin newSkin)
		{
			if (!newSkin)
			{
				newSkin = GUIUtility.GetDefaultSkin();
			}
			GUI.s_Skin = newSkin;
			newSkin.MakeCurrent();
		}

		// Token: 0x06000077 RID: 119 RVA: 0x000034D4 File Offset: 0x000016D4
		internal static void CleanupRoots()
		{
			GUI.s_Skin = null;
			GUIUtility.CleanupRoots();
			GUILayoutUtility.CleanupRoots();
			GUISkin.CleanupRoots();
			GUIStyle.CleanupRoots();
		}

		// Token: 0x17000028 RID: 40
		// (get) Token: 0x06000078 RID: 120 RVA: 0x000034F4 File Offset: 0x000016F4
		// (set) Token: 0x06000079 RID: 121 RVA: 0x0000350E File Offset: 0x0000170E
		public static Matrix4x4 matrix
		{
			get
			{
				return GUIClip.GetMatrix();
			}
			set
			{
				GUIClip.SetMatrix(value);
			}
		}

		// Token: 0x17000029 RID: 41
		// (get) Token: 0x0600007A RID: 122 RVA: 0x00003518 File Offset: 0x00001718
		// (set) Token: 0x0600007B RID: 123 RVA: 0x00003545 File Offset: 0x00001745
		public static string tooltip
		{
			get
			{
				string text = GUI.Internal_GetTooltip();
				string result;
				if (text != null)
				{
					result = text;
				}
				else
				{
					result = "";
				}
				return result;
			}
			set
			{
				GUI.Internal_SetTooltip(value);
			}
		}

		// Token: 0x1700002A RID: 42
		// (get) Token: 0x0600007C RID: 124 RVA: 0x00003550 File Offset: 0x00001750
		protected static string mouseTooltip
		{
			[CompilerGenerated]
			get
			{
				return GUI.Internal_GetMouseTooltip();
			}
		}

		// Token: 0x1700002B RID: 43
		// (get) Token: 0x0600007D RID: 125 RVA: 0x0000356C File Offset: 0x0000176C
		// (set) Token: 0x0600007E RID: 126 RVA: 0x00003586 File Offset: 0x00001786
		protected static Rect tooltipRect
		{
			get
			{
				return GUI.s_ToolTipRect;
			}
			set
			{
				GUI.s_ToolTipRect = value;
			}
		}

		// Token: 0x0600007F RID: 127 RVA: 0x0000358F File Offset: 0x0000178F
		public static void Label(Rect position, string text)
		{
			GUI.Label(position, GUIContent.Temp(text), GUI.s_Skin.label);
		}

		// Token: 0x06000080 RID: 128 RVA: 0x000035A8 File Offset: 0x000017A8
		public static void Label(Rect position, Texture image)
		{
			GUI.Label(position, GUIContent.Temp(image), GUI.s_Skin.label);
		}

		// Token: 0x06000081 RID: 129 RVA: 0x000035C1 File Offset: 0x000017C1
		public static void Label(Rect position, GUIContent content)
		{
			GUI.Label(position, content, GUI.s_Skin.label);
		}

		// Token: 0x06000082 RID: 130 RVA: 0x000035D5 File Offset: 0x000017D5
		public static void Label(Rect position, string text, GUIStyle style)
		{
			GUI.Label(position, GUIContent.Temp(text), style);
		}

		// Token: 0x06000083 RID: 131 RVA: 0x000035E5 File Offset: 0x000017E5
		public static void Label(Rect position, Texture image, GUIStyle style)
		{
			GUI.Label(position, GUIContent.Temp(image), style);
		}

		// Token: 0x06000084 RID: 132 RVA: 0x000035F5 File Offset: 0x000017F5
		public static void Label(Rect position, GUIContent content, GUIStyle style)
		{
			GUIUtility.CheckOnGUI();
			GUI.DoLabel(position, content, style);
		}

		// Token: 0x06000085 RID: 133 RVA: 0x00003605 File Offset: 0x00001805
		public static void DrawTexture(Rect position, Texture image)
		{
			GUI.DrawTexture(position, image, ScaleMode.StretchToFill);
		}

		// Token: 0x06000086 RID: 134 RVA: 0x00003610 File Offset: 0x00001810
		public static void DrawTexture(Rect position, Texture image, ScaleMode scaleMode)
		{
			GUI.DrawTexture(position, image, scaleMode, true);
		}

		// Token: 0x06000087 RID: 135 RVA: 0x0000361C File Offset: 0x0000181C
		public static void DrawTexture(Rect position, Texture image, ScaleMode scaleMode, bool alphaBlend)
		{
			GUI.DrawTexture(position, image, scaleMode, alphaBlend, 0f);
		}

		// Token: 0x06000088 RID: 136 RVA: 0x0000362D File Offset: 0x0000182D
		public static void DrawTexture(Rect position, Texture image, ScaleMode scaleMode, bool alphaBlend, float imageAspect)
		{
			GUI.DrawTexture(position, image, scaleMode, alphaBlend, imageAspect, GUI.color, 0f, 0f);
		}

		// Token: 0x06000089 RID: 137 RVA: 0x0000364C File Offset: 0x0000184C
		public static void DrawTexture(Rect position, Texture image, ScaleMode scaleMode, bool alphaBlend, float imageAspect, Color color, float borderWidth, float borderRadius)
		{
			Vector4 borderWidths = Vector4.one * borderWidth;
			GUI.DrawTexture(position, image, scaleMode, alphaBlend, imageAspect, color, borderWidths, borderRadius);
		}

		// Token: 0x0600008A RID: 138 RVA: 0x00003678 File Offset: 0x00001878
		public static void DrawTexture(Rect position, Texture image, ScaleMode scaleMode, bool alphaBlend, float imageAspect, Color color, Vector4 borderWidths, float borderRadius)
		{
			Vector4 borderRadiuses = Vector4.one * borderRadius;
			GUI.DrawTexture(position, image, scaleMode, alphaBlend, imageAspect, color, borderWidths, borderRadiuses);
		}

		// Token: 0x0600008B RID: 139 RVA: 0x000036A4 File Offset: 0x000018A4
		public static void DrawTexture(Rect position, Texture image, ScaleMode scaleMode, bool alphaBlend, float imageAspect, Color color, Vector4 borderWidths, Vector4 borderRadiuses)
		{
			GUIUtility.CheckOnGUI();
			if (Event.current.type == EventType.Repaint)
			{
				if (image == null)
				{
					Debug.LogWarning("null texture passed to GUI.DrawTexture");
				}
				else
				{
					if (imageAspect == 0f)
					{
						imageAspect = (float)image.width / (float)image.height;
					}
					Material mat;
					if (borderWidths != Vector4.zero || borderRadiuses != Vector4.zero)
					{
						mat = GUI.roundedRectMaterial;
					}
					else
					{
						mat = ((!alphaBlend) ? GUI.blitMaterial : GUI.blendMaterial);
					}
					Internal_DrawTextureArguments internal_DrawTextureArguments = new Internal_DrawTextureArguments
					{
						leftBorder = 0,
						rightBorder = 0,
						topBorder = 0,
						bottomBorder = 0,
						color = color,
						borderWidths = borderWidths,
						cornerRadiuses = borderRadiuses,
						texture = image,
						mat = mat
					};
					GUI.CalculateScaledTextureRects(position, scaleMode, imageAspect, ref internal_DrawTextureArguments.screenRect, ref internal_DrawTextureArguments.sourceRect);
					Graphics.Internal_DrawTexture(ref internal_DrawTextureArguments);
				}
			}
		}

		// Token: 0x0600008C RID: 140 RVA: 0x000037C0 File Offset: 0x000019C0
		internal static bool CalculateScaledTextureRects(Rect position, ScaleMode scaleMode, float imageAspect, ref Rect outScreenRect, ref Rect outSourceRect)
		{
			float num = position.width / position.height;
			bool result = false;
			if (scaleMode != ScaleMode.StretchToFill)
			{
				if (scaleMode != ScaleMode.ScaleAndCrop)
				{
					if (scaleMode == ScaleMode.ScaleToFit)
					{
						if (num > imageAspect)
						{
							float num2 = imageAspect / num;
							outScreenRect = new Rect(position.xMin + position.width * (1f - num2) * 0.5f, position.yMin, num2 * position.width, position.height);
							outSourceRect = new Rect(0f, 0f, 1f, 1f);
							result = true;
						}
						else
						{
							float num3 = num / imageAspect;
							outScreenRect = new Rect(position.xMin, position.yMin + position.height * (1f - num3) * 0.5f, position.width, num3 * position.height);
							outSourceRect = new Rect(0f, 0f, 1f, 1f);
							result = true;
						}
					}
				}
				else if (num > imageAspect)
				{
					float num4 = imageAspect / num;
					outScreenRect = position;
					outSourceRect = new Rect(0f, (1f - num4) * 0.5f, 1f, num4);
					result = true;
				}
				else
				{
					float num5 = num / imageAspect;
					outScreenRect = position;
					outSourceRect = new Rect(0.5f - num5 * 0.5f, 0f, num5, 1f);
					result = true;
				}
			}
			else
			{
				outScreenRect = position;
				outSourceRect = new Rect(0f, 0f, 1f, 1f);
				result = true;
			}
			return result;
		}

		// Token: 0x0600008D RID: 141 RVA: 0x0000396A File Offset: 0x00001B6A
		public static void DrawTextureWithTexCoords(Rect position, Texture image, Rect texCoords)
		{
			GUI.DrawTextureWithTexCoords(position, image, texCoords, true);
		}

		// Token: 0x0600008E RID: 142 RVA: 0x00003978 File Offset: 0x00001B78
		public static void DrawTextureWithTexCoords(Rect position, Texture image, Rect texCoords, bool alphaBlend)
		{
			GUIUtility.CheckOnGUI();
			if (Event.current.type == EventType.Repaint)
			{
				Material mat = (!alphaBlend) ? GUI.blitMaterial : GUI.blendMaterial;
				Internal_DrawTextureArguments internal_DrawTextureArguments = default(Internal_DrawTextureArguments);
				internal_DrawTextureArguments.texture = image;
				internal_DrawTextureArguments.mat = mat;
				internal_DrawTextureArguments.leftBorder = 0;
				internal_DrawTextureArguments.rightBorder = 0;
				internal_DrawTextureArguments.topBorder = 0;
				internal_DrawTextureArguments.bottomBorder = 0;
				internal_DrawTextureArguments.color = GUI.color;
				internal_DrawTextureArguments.screenRect = position;
				internal_DrawTextureArguments.sourceRect = texCoords;
				Graphics.Internal_DrawTexture(ref internal_DrawTextureArguments);
			}
		}

		// Token: 0x0600008F RID: 143 RVA: 0x00003A0E File Offset: 0x00001C0E
		public static void Box(Rect position, string text)
		{
			GUI.Box(position, GUIContent.Temp(text), GUI.s_Skin.box);
		}

		// Token: 0x06000090 RID: 144 RVA: 0x00003A27 File Offset: 0x00001C27
		public static void Box(Rect position, Texture image)
		{
			GUI.Box(position, GUIContent.Temp(image), GUI.s_Skin.box);
		}

		// Token: 0x06000091 RID: 145 RVA: 0x00003A40 File Offset: 0x00001C40
		public static void Box(Rect position, GUIContent content)
		{
			GUI.Box(position, content, GUI.s_Skin.box);
		}

		// Token: 0x06000092 RID: 146 RVA: 0x00003A54 File Offset: 0x00001C54
		public static void Box(Rect position, string text, GUIStyle style)
		{
			GUI.Box(position, GUIContent.Temp(text), style);
		}

		// Token: 0x06000093 RID: 147 RVA: 0x00003A64 File Offset: 0x00001C64
		public static void Box(Rect position, Texture image, GUIStyle style)
		{
			GUI.Box(position, GUIContent.Temp(image), style);
		}

		// Token: 0x06000094 RID: 148 RVA: 0x00003A74 File Offset: 0x00001C74
		public static void Box(Rect position, GUIContent content, GUIStyle style)
		{
			GUIUtility.CheckOnGUI();
			int controlID = GUIUtility.GetControlID(GUI.s_BoxHash, FocusType.Passive);
			if (Event.current.type == EventType.Repaint)
			{
				style.Draw(position, content, controlID);
			}
		}

		// Token: 0x06000095 RID: 149 RVA: 0x00003AB0 File Offset: 0x00001CB0
		public static bool Button(Rect position, string text)
		{
			return GUI.Button(position, GUIContent.Temp(text), GUI.s_Skin.button);
		}

		// Token: 0x06000096 RID: 150 RVA: 0x00003ADC File Offset: 0x00001CDC
		public static bool Button(Rect position, Texture image)
		{
			return GUI.Button(position, GUIContent.Temp(image), GUI.s_Skin.button);
		}

		// Token: 0x06000097 RID: 151 RVA: 0x00003B08 File Offset: 0x00001D08
		public static bool Button(Rect position, GUIContent content)
		{
			return GUI.Button(position, content, GUI.s_Skin.button);
		}

		// Token: 0x06000098 RID: 152 RVA: 0x00003B30 File Offset: 0x00001D30
		public static bool Button(Rect position, string text, GUIStyle style)
		{
			return GUI.Button(position, GUIContent.Temp(text), style);
		}

		// Token: 0x06000099 RID: 153 RVA: 0x00003B54 File Offset: 0x00001D54
		public static bool Button(Rect position, Texture image, GUIStyle style)
		{
			return GUI.Button(position, GUIContent.Temp(image), style);
		}

		// Token: 0x0600009A RID: 154 RVA: 0x00003B78 File Offset: 0x00001D78
		public static bool Button(Rect position, GUIContent content, GUIStyle style)
		{
			int controlID = GUIUtility.GetControlID(GUI.s_ButonHash, FocusType.Passive, position);
			return GUI.Button(position, controlID, content, style);
		}

		// Token: 0x0600009B RID: 155 RVA: 0x00003BA4 File Offset: 0x00001DA4
		internal static bool Button(Rect position, int id, GUIContent content, GUIStyle style)
		{
			GUIUtility.CheckOnGUI();
			return GUI.DoButton(position, id, content, style);
		}

		// Token: 0x0600009C RID: 156 RVA: 0x00003BC8 File Offset: 0x00001DC8
		public static bool RepeatButton(Rect position, string text)
		{
			return GUI.DoRepeatButton(position, GUIContent.Temp(text), GUI.s_Skin.button, FocusType.Passive);
		}

		// Token: 0x0600009D RID: 157 RVA: 0x00003BF4 File Offset: 0x00001DF4
		public static bool RepeatButton(Rect position, Texture image)
		{
			return GUI.DoRepeatButton(position, GUIContent.Temp(image), GUI.s_Skin.button, FocusType.Passive);
		}

		// Token: 0x0600009E RID: 158 RVA: 0x00003C20 File Offset: 0x00001E20
		public static bool RepeatButton(Rect position, GUIContent content)
		{
			return GUI.DoRepeatButton(position, content, GUI.s_Skin.button, FocusType.Passive);
		}

		// Token: 0x0600009F RID: 159 RVA: 0x00003C48 File Offset: 0x00001E48
		public static bool RepeatButton(Rect position, string text, GUIStyle style)
		{
			return GUI.DoRepeatButton(position, GUIContent.Temp(text), style, FocusType.Passive);
		}

		// Token: 0x060000A0 RID: 160 RVA: 0x00003C6C File Offset: 0x00001E6C
		public static bool RepeatButton(Rect position, Texture image, GUIStyle style)
		{
			return GUI.DoRepeatButton(position, GUIContent.Temp(image), style, FocusType.Passive);
		}

		// Token: 0x060000A1 RID: 161 RVA: 0x00003C90 File Offset: 0x00001E90
		public static bool RepeatButton(Rect position, GUIContent content, GUIStyle style)
		{
			return GUI.DoRepeatButton(position, content, style, FocusType.Passive);
		}

		// Token: 0x060000A2 RID: 162 RVA: 0x00003CB0 File Offset: 0x00001EB0
		private static bool DoRepeatButton(Rect position, GUIContent content, GUIStyle style, FocusType focusType)
		{
			GUIUtility.CheckOnGUI();
			int controlID = GUIUtility.GetControlID(GUI.s_RepeatButtonHash, focusType, position);
			EventType typeForControl = Event.current.GetTypeForControl(controlID);
			bool result;
			if (typeForControl != EventType.MouseDown)
			{
				if (typeForControl != EventType.MouseUp)
				{
					if (typeForControl != EventType.Repaint)
					{
						result = false;
					}
					else
					{
						style.Draw(position, content, controlID);
						result = (controlID == GUIUtility.hotControl && position.Contains(Event.current.mousePosition));
					}
				}
				else if (GUIUtility.hotControl == controlID)
				{
					GUIUtility.hotControl = 0;
					Event.current.Use();
					result = position.Contains(Event.current.mousePosition);
				}
				else
				{
					result = false;
				}
			}
			else
			{
				if (position.Contains(Event.current.mousePosition))
				{
					GUIUtility.hotControl = controlID;
					Event.current.Use();
				}
				result = false;
			}
			return result;
		}

		// Token: 0x060000A3 RID: 163 RVA: 0x00003D94 File Offset: 0x00001F94
		public static string TextField(Rect position, string text)
		{
			GUIContent guicontent = GUIContent.Temp(text);
			GUI.DoTextField(position, GUIUtility.GetControlID(FocusType.Keyboard, position), guicontent, false, -1, GUI.skin.textField);
			return guicontent.text;
		}

		// Token: 0x060000A4 RID: 164 RVA: 0x00003DD0 File Offset: 0x00001FD0
		public static string TextField(Rect position, string text, int maxLength)
		{
			GUIContent guicontent = GUIContent.Temp(text);
			GUI.DoTextField(position, GUIUtility.GetControlID(FocusType.Keyboard, position), guicontent, false, maxLength, GUI.skin.textField);
			return guicontent.text;
		}

		// Token: 0x060000A5 RID: 165 RVA: 0x00003E0C File Offset: 0x0000200C
		public static string TextField(Rect position, string text, GUIStyle style)
		{
			GUIContent guicontent = GUIContent.Temp(text);
			GUI.DoTextField(position, GUIUtility.GetControlID(FocusType.Keyboard, position), guicontent, false, -1, style);
			return guicontent.text;
		}

		// Token: 0x060000A6 RID: 166 RVA: 0x00003E40 File Offset: 0x00002040
		public static string TextField(Rect position, string text, int maxLength, GUIStyle style)
		{
			GUIContent guicontent = GUIContent.Temp(text);
			GUI.DoTextField(position, GUIUtility.GetControlID(FocusType.Keyboard, position), guicontent, false, maxLength, style);
			return guicontent.text;
		}

		// Token: 0x060000A7 RID: 167 RVA: 0x00003E74 File Offset: 0x00002074
		public static string PasswordField(Rect position, string password, char maskChar)
		{
			return GUI.PasswordField(position, password, maskChar, -1, GUI.skin.textField);
		}

		// Token: 0x060000A8 RID: 168 RVA: 0x00003E9C File Offset: 0x0000209C
		public static string PasswordField(Rect position, string password, char maskChar, int maxLength)
		{
			return GUI.PasswordField(position, password, maskChar, maxLength, GUI.skin.textField);
		}

		// Token: 0x060000A9 RID: 169 RVA: 0x00003EC4 File Offset: 0x000020C4
		public static string PasswordField(Rect position, string password, char maskChar, GUIStyle style)
		{
			return GUI.PasswordField(position, password, maskChar, -1, style);
		}

		// Token: 0x060000AA RID: 170 RVA: 0x00003EE4 File Offset: 0x000020E4
		public static string PasswordField(Rect position, string password, char maskChar, int maxLength, GUIStyle style)
		{
			GUIUtility.CheckOnGUI();
			string text = GUI.PasswordFieldGetStrToShow(password, maskChar);
			GUIContent guicontent = GUIContent.Temp(text);
			bool changed = GUI.changed;
			GUI.changed = false;
			if (TouchScreenKeyboard.isSupported)
			{
				GUI.DoTextField(position, GUIUtility.GetControlID(FocusType.Keyboard), guicontent, false, maxLength, style, password, maskChar);
			}
			else
			{
				GUI.DoTextField(position, GUIUtility.GetControlID(FocusType.Keyboard, position), guicontent, false, maxLength, style);
			}
			text = ((!GUI.changed) ? password : guicontent.text);
			GUI.changed = (GUI.changed || changed);
			return text;
		}

		// Token: 0x060000AB RID: 171 RVA: 0x00003F74 File Offset: 0x00002174
		internal static string PasswordFieldGetStrToShow(string password, char maskChar)
		{
			return (Event.current.type != EventType.Repaint && Event.current.type != EventType.MouseDown) ? password : "".PadRight(password.Length, maskChar);
		}

		// Token: 0x060000AC RID: 172 RVA: 0x00003FC0 File Offset: 0x000021C0
		public static string TextArea(Rect position, string text)
		{
			GUIContent guicontent = GUIContent.Temp(text);
			GUI.DoTextField(position, GUIUtility.GetControlID(FocusType.Keyboard, position), guicontent, true, -1, GUI.skin.textArea);
			return guicontent.text;
		}

		// Token: 0x060000AD RID: 173 RVA: 0x00003FFC File Offset: 0x000021FC
		public static string TextArea(Rect position, string text, int maxLength)
		{
			GUIContent guicontent = GUIContent.Temp(text);
			GUI.DoTextField(position, GUIUtility.GetControlID(FocusType.Keyboard, position), guicontent, true, maxLength, GUI.skin.textArea);
			return guicontent.text;
		}

		// Token: 0x060000AE RID: 174 RVA: 0x00004038 File Offset: 0x00002238
		public static string TextArea(Rect position, string text, GUIStyle style)
		{
			GUIContent guicontent = GUIContent.Temp(text);
			GUI.DoTextField(position, GUIUtility.GetControlID(FocusType.Keyboard, position), guicontent, true, -1, style);
			return guicontent.text;
		}

		// Token: 0x060000AF RID: 175 RVA: 0x0000406C File Offset: 0x0000226C
		public static string TextArea(Rect position, string text, int maxLength, GUIStyle style)
		{
			GUIContent guicontent = GUIContent.Temp(text);
			GUI.DoTextField(position, GUIUtility.GetControlID(FocusType.Keyboard, position), guicontent, true, maxLength, style);
			return guicontent.text;
		}

		// Token: 0x060000B0 RID: 176 RVA: 0x0000409F File Offset: 0x0000229F
		internal static void DoTextField(Rect position, int id, GUIContent content, bool multiline, int maxLength, GUIStyle style)
		{
			GUI.DoTextField(position, id, content, multiline, maxLength, style, null);
		}

		// Token: 0x060000B1 RID: 177 RVA: 0x000040B0 File Offset: 0x000022B0
		internal static void DoTextField(Rect position, int id, GUIContent content, bool multiline, int maxLength, GUIStyle style, string secureText)
		{
			GUI.DoTextField(position, id, content, multiline, maxLength, style, secureText, '\0');
		}

		// Token: 0x060000B2 RID: 178 RVA: 0x000040C4 File Offset: 0x000022C4
		internal static void DoTextField(Rect position, int id, GUIContent content, bool multiline, int maxLength, GUIStyle style, string secureText, char maskChar)
		{
			GUIUtility.CheckOnGUI();
			if (maxLength >= 0 && content.text.Length > maxLength)
			{
				content.text = content.text.Substring(0, maxLength);
			}
			TextEditor textEditor = (TextEditor)GUIUtility.GetStateObject(typeof(TextEditor), id);
			textEditor.text = content.text;
			textEditor.SaveBackup();
			textEditor.position = position;
			textEditor.style = style;
			textEditor.multiline = multiline;
			textEditor.controlID = id;
			textEditor.DetectFocusChange();
			if (TouchScreenKeyboard.isSupported)
			{
				GUI.HandleTextFieldEventForTouchscreen(position, id, content, multiline, maxLength, style, secureText, maskChar, textEditor);
			}
			else
			{
				GUI.HandleTextFieldEventForDesktop(position, id, content, multiline, maxLength, style, textEditor);
			}
			textEditor.UpdateScrollOffsetIfNeeded(Event.current);
		}

		// Token: 0x060000B3 RID: 179 RVA: 0x00004190 File Offset: 0x00002390
		private static void HandleTextFieldEventForTouchscreen(Rect position, int id, GUIContent content, bool multiline, int maxLength, GUIStyle style, string secureText, char maskChar, TextEditor editor)
		{
			Event current = Event.current;
			EventType type = current.type;
			if (type != EventType.MouseDown)
			{
				if (type == EventType.Repaint)
				{
					if (editor.keyboardOnScreen != null)
					{
						content.text = editor.keyboardOnScreen.text;
						if (maxLength >= 0 && content.text.Length > maxLength)
						{
							content.text = content.text.Substring(0, maxLength);
						}
						if (editor.keyboardOnScreen.status != TouchScreenKeyboard.Status.Visible)
						{
							editor.keyboardOnScreen = null;
							GUI.changed = true;
						}
					}
					string text = content.text;
					if (secureText != null)
					{
						content.text = GUI.PasswordFieldGetStrToShow(text, maskChar);
					}
					style.Draw(position, content, id, false);
					content.text = text;
				}
			}
			else if (position.Contains(current.mousePosition))
			{
				GUIUtility.hotControl = id;
				if (GUI.s_HotTextField != -1 && GUI.s_HotTextField != id)
				{
					TextEditor textEditor = (TextEditor)GUIUtility.GetStateObject(typeof(TextEditor), GUI.s_HotTextField);
					textEditor.keyboardOnScreen = null;
				}
				GUI.s_HotTextField = id;
				if (GUIUtility.keyboardControl != id)
				{
					GUIUtility.keyboardControl = id;
				}
				editor.keyboardOnScreen = TouchScreenKeyboard.Open(secureText ?? content.text, TouchScreenKeyboardType.Default, true, multiline, secureText != null);
				current.Use();
			}
		}

		// Token: 0x060000B4 RID: 180 RVA: 0x000042FC File Offset: 0x000024FC
		private static void HandleTextFieldEventForDesktop(Rect position, int id, GUIContent content, bool multiline, int maxLength, GUIStyle style, TextEditor editor)
		{
			Event current = Event.current;
			bool flag = false;
			switch (current.type)
			{
			case EventType.MouseDown:
				if (position.Contains(current.mousePosition))
				{
					GUIUtility.hotControl = id;
					GUIUtility.keyboardControl = id;
					editor.m_HasFocus = true;
					editor.MoveCursorToPosition(Event.current.mousePosition);
					if (Event.current.clickCount == 2 && GUI.skin.settings.doubleClickSelectsWord)
					{
						editor.SelectCurrentWord();
						editor.DblClickSnap(TextEditor.DblClickSnapping.WORDS);
						editor.MouseDragSelectsWholeWords(true);
					}
					if (Event.current.clickCount == 3 && GUI.skin.settings.tripleClickSelectsLine)
					{
						editor.SelectCurrentParagraph();
						editor.MouseDragSelectsWholeWords(true);
						editor.DblClickSnap(TextEditor.DblClickSnapping.PARAGRAPHS);
					}
					current.Use();
				}
				break;
			case EventType.MouseUp:
				if (GUIUtility.hotControl == id)
				{
					editor.MouseDragSelectsWholeWords(false);
					GUIUtility.hotControl = 0;
					current.Use();
				}
				break;
			case EventType.MouseDrag:
				if (GUIUtility.hotControl == id)
				{
					if (current.shift)
					{
						editor.MoveCursorToPosition(Event.current.mousePosition);
					}
					else
					{
						editor.SelectToPosition(Event.current.mousePosition);
					}
					current.Use();
				}
				break;
			case EventType.KeyDown:
				if (GUIUtility.keyboardControl != id)
				{
					return;
				}
				if (editor.HandleKeyEvent(current))
				{
					current.Use();
					flag = true;
					content.text = editor.text;
				}
				else
				{
					if (current.keyCode == KeyCode.Tab || current.character == '\t')
					{
						return;
					}
					char character = current.character;
					if (character == '\n' && !multiline && !current.alt)
					{
						return;
					}
					Font font = style.font;
					if (!font)
					{
						font = GUI.skin.font;
					}
					if (font.HasCharacter(character) || character == '\n')
					{
						editor.Insert(character);
						flag = true;
					}
					else if (character == '\0')
					{
						if (Input.compositionString.Length > 0)
						{
							editor.ReplaceSelection("");
							flag = true;
						}
						current.Use();
					}
				}
				break;
			case EventType.Repaint:
				if (GUIUtility.keyboardControl != id)
				{
					style.Draw(position, content, id, false);
				}
				else
				{
					editor.DrawCursor(content.text);
				}
				break;
			}
			if (GUIUtility.keyboardControl == id)
			{
				GUIUtility.textFieldInput = true;
			}
			if (flag)
			{
				GUI.changed = true;
				content.text = editor.text;
				if (maxLength >= 0 && content.text.Length > maxLength)
				{
					content.text = content.text.Substring(0, maxLength);
				}
				current.Use();
			}
		}

		// Token: 0x060000B5 RID: 181 RVA: 0x000045F8 File Offset: 0x000027F8
		public static bool Toggle(Rect position, bool value, string text)
		{
			return GUI.Toggle(position, value, GUIContent.Temp(text), GUI.s_Skin.toggle);
		}

		// Token: 0x060000B6 RID: 182 RVA: 0x00004624 File Offset: 0x00002824
		public static bool Toggle(Rect position, bool value, Texture image)
		{
			return GUI.Toggle(position, value, GUIContent.Temp(image), GUI.s_Skin.toggle);
		}

		// Token: 0x060000B7 RID: 183 RVA: 0x00004650 File Offset: 0x00002850
		public static bool Toggle(Rect position, bool value, GUIContent content)
		{
			return GUI.Toggle(position, value, content, GUI.s_Skin.toggle);
		}

		// Token: 0x060000B8 RID: 184 RVA: 0x00004678 File Offset: 0x00002878
		public static bool Toggle(Rect position, bool value, string text, GUIStyle style)
		{
			return GUI.Toggle(position, value, GUIContent.Temp(text), style);
		}

		// Token: 0x060000B9 RID: 185 RVA: 0x0000469C File Offset: 0x0000289C
		public static bool Toggle(Rect position, bool value, Texture image, GUIStyle style)
		{
			return GUI.Toggle(position, value, GUIContent.Temp(image), style);
		}

		// Token: 0x060000BA RID: 186 RVA: 0x000046C0 File Offset: 0x000028C0
		public static bool Toggle(Rect position, bool value, GUIContent content, GUIStyle style)
		{
			GUIUtility.CheckOnGUI();
			return GUI.DoToggle(position, GUIUtility.GetControlID(GUI.s_ToggleHash, FocusType.Passive, position), value, content, style);
		}

		// Token: 0x060000BB RID: 187 RVA: 0x000046F0 File Offset: 0x000028F0
		public static bool Toggle(Rect position, int id, bool value, GUIContent content, GUIStyle style)
		{
			GUIUtility.CheckOnGUI();
			return GUI.DoToggle(position, id, value, content, style);
		}

		// Token: 0x060000BC RID: 188 RVA: 0x00004718 File Offset: 0x00002918
		public static int Toolbar(Rect position, int selected, string[] texts)
		{
			return GUI.Toolbar(position, selected, GUIContent.Temp(texts), GUI.s_Skin.button);
		}

		// Token: 0x060000BD RID: 189 RVA: 0x00004744 File Offset: 0x00002944
		public static int Toolbar(Rect position, int selected, Texture[] images)
		{
			return GUI.Toolbar(position, selected, GUIContent.Temp(images), GUI.s_Skin.button);
		}

		// Token: 0x060000BE RID: 190 RVA: 0x00004770 File Offset: 0x00002970
		public static int Toolbar(Rect position, int selected, GUIContent[] contents)
		{
			return GUI.Toolbar(position, selected, contents, GUI.s_Skin.button);
		}

		// Token: 0x060000BF RID: 191 RVA: 0x00004798 File Offset: 0x00002998
		public static int Toolbar(Rect position, int selected, string[] texts, GUIStyle style)
		{
			return GUI.Toolbar(position, selected, GUIContent.Temp(texts), style);
		}

		// Token: 0x060000C0 RID: 192 RVA: 0x000047BC File Offset: 0x000029BC
		public static int Toolbar(Rect position, int selected, Texture[] images, GUIStyle style)
		{
			return GUI.Toolbar(position, selected, GUIContent.Temp(images), style);
		}

		// Token: 0x060000C1 RID: 193 RVA: 0x000047E0 File Offset: 0x000029E0
		public static int Toolbar(Rect position, int selected, GUIContent[] contents, GUIStyle style)
		{
			return GUI.Toolbar(position, selected, contents, null, style, GUI.ToolbarButtonSize.Fixed);
		}

		// Token: 0x060000C2 RID: 194 RVA: 0x00004800 File Offset: 0x00002A00
		public static int Toolbar(Rect position, int selected, GUIContent[] contents, GUIStyle style, GUI.ToolbarButtonSize buttonSize)
		{
			return GUI.Toolbar(position, selected, contents, null, style, buttonSize);
		}

		// Token: 0x060000C3 RID: 195 RVA: 0x00004824 File Offset: 0x00002A24
		internal static int Toolbar(Rect position, int selected, GUIContent[] contents, string[] controlNames, GUIStyle style, GUI.ToolbarButtonSize buttonSize)
		{
			GUIUtility.CheckOnGUI();
			GUIStyle firstStyle;
			GUIStyle midStyle;
			GUIStyle lastStyle;
			GUI.FindStyles(ref style, out firstStyle, out midStyle, out lastStyle, "left", "mid", "right");
			return GUI.DoButtonGrid(position, selected, contents, controlNames, contents.Length, style, firstStyle, midStyle, lastStyle, buttonSize);
		}

		// Token: 0x060000C4 RID: 196 RVA: 0x00004870 File Offset: 0x00002A70
		public static int SelectionGrid(Rect position, int selected, string[] texts, int xCount)
		{
			return GUI.SelectionGrid(position, selected, GUIContent.Temp(texts), xCount, null);
		}

		// Token: 0x060000C5 RID: 197 RVA: 0x00004894 File Offset: 0x00002A94
		public static int SelectionGrid(Rect position, int selected, Texture[] images, int xCount)
		{
			return GUI.SelectionGrid(position, selected, GUIContent.Temp(images), xCount, null);
		}

		// Token: 0x060000C6 RID: 198 RVA: 0x000048B8 File Offset: 0x00002AB8
		public static int SelectionGrid(Rect position, int selected, GUIContent[] content, int xCount)
		{
			return GUI.SelectionGrid(position, selected, content, xCount, null);
		}

		// Token: 0x060000C7 RID: 199 RVA: 0x000048D8 File Offset: 0x00002AD8
		public static int SelectionGrid(Rect position, int selected, string[] texts, int xCount, GUIStyle style)
		{
			return GUI.SelectionGrid(position, selected, GUIContent.Temp(texts), xCount, style);
		}

		// Token: 0x060000C8 RID: 200 RVA: 0x00004900 File Offset: 0x00002B00
		public static int SelectionGrid(Rect position, int selected, Texture[] images, int xCount, GUIStyle style)
		{
			return GUI.SelectionGrid(position, selected, GUIContent.Temp(images), xCount, style);
		}

		// Token: 0x060000C9 RID: 201 RVA: 0x00004928 File Offset: 0x00002B28
		public static int SelectionGrid(Rect position, int selected, GUIContent[] contents, int xCount, GUIStyle style)
		{
			if (style == null)
			{
				style = GUI.s_Skin.button;
			}
			return GUI.DoButtonGrid(position, selected, contents, null, xCount, style, style, style, style, GUI.ToolbarButtonSize.Fixed);
		}

		// Token: 0x060000CA RID: 202 RVA: 0x00004964 File Offset: 0x00002B64
		internal static void FindStyles(ref GUIStyle style, out GUIStyle firstStyle, out GUIStyle midStyle, out GUIStyle lastStyle, string first, string mid, string last)
		{
			if (style == null)
			{
				style = GUI.skin.button;
			}
			string name = style.name;
			midStyle = (GUI.skin.FindStyle(name + mid) ?? style);
			firstStyle = (GUI.skin.FindStyle(name + first) ?? midStyle);
			lastStyle = (GUI.skin.FindStyle(name + last) ?? midStyle);
		}

		// Token: 0x060000CB RID: 203 RVA: 0x000049E4 File Offset: 0x00002BE4
		internal static int CalcTotalHorizSpacing(int xCount, GUIStyle style, GUIStyle firstStyle, GUIStyle midStyle, GUIStyle lastStyle)
		{
			int result;
			if (xCount < 2)
			{
				result = 0;
			}
			else if (xCount == 2)
			{
				result = Mathf.Max(firstStyle.margin.right, lastStyle.margin.left);
			}
			else
			{
				int num = Mathf.Max(midStyle.margin.left, midStyle.margin.right);
				result = Mathf.Max(firstStyle.margin.right, midStyle.margin.left) + Mathf.Max(midStyle.margin.right, lastStyle.margin.left) + num * (xCount - 3);
			}
			return result;
		}

		// Token: 0x060000CC RID: 204 RVA: 0x00004A8C File Offset: 0x00002C8C
		private static bool DoControl(Rect position, int id, bool on, GUIContent content, GUIStyle style)
		{
			Event current = Event.current;
			switch (current.type)
			{
			case EventType.MouseDown:
				if (position.Contains(current.mousePosition))
				{
					GUI.GrabMouseControl(id);
					current.Use();
				}
				break;
			case EventType.MouseUp:
				if (GUI.HasMouseControl(id))
				{
					GUI.ReleaseMouseControl();
					current.Use();
					if (position.Contains(current.mousePosition))
					{
						GUI.changed = true;
						return !on;
					}
				}
				break;
			case EventType.MouseDrag:
				if (GUI.HasMouseControl(id))
				{
					current.Use();
				}
				break;
			case EventType.KeyDown:
				if (current.character == ' ' && GUIUtility.keyboardControl == id)
				{
					current.Use();
					GUI.changed = true;
					return !on;
				}
				break;
			case EventType.Repaint:
				style.Draw(position, content, id, on);
				break;
			}
			return on;
		}

		// Token: 0x060000CD RID: 205 RVA: 0x00004B9C File Offset: 0x00002D9C
		private static void DoLabel(Rect position, GUIContent content, GUIStyle style)
		{
			Event current = Event.current;
			if (current.type == EventType.Repaint)
			{
				style.Draw(position, content, false, false, false, false);
				if (!string.IsNullOrEmpty(content.tooltip) && position.Contains(current.mousePosition) && GUIClip.visibleRect.Contains(current.mousePosition))
				{
					GUIStyle.SetMouseTooltip(content.tooltip, position);
				}
			}
		}

		// Token: 0x060000CE RID: 206 RVA: 0x00004C14 File Offset: 0x00002E14
		internal static bool DoToggle(Rect position, int id, bool value, GUIContent content, GUIStyle style)
		{
			return GUI.DoControl(position, id, value, content, style);
		}

		// Token: 0x060000CF RID: 207 RVA: 0x00004C34 File Offset: 0x00002E34
		internal static bool DoButton(Rect position, int id, GUIContent content, GUIStyle style)
		{
			return GUI.DoControl(position, id, false, content, style);
		}

		// Token: 0x060000D0 RID: 208 RVA: 0x00004C54 File Offset: 0x00002E54
		private static int DoButtonGrid(Rect position, int selected, GUIContent[] contents, string[] controlNames, int xCount, GUIStyle style, GUIStyle firstStyle, GUIStyle midStyle, GUIStyle lastStyle, GUI.ToolbarButtonSize buttonSize)
		{
			GUIUtility.CheckOnGUI();
			int num = contents.Length;
			int result;
			if (num == 0)
			{
				result = selected;
			}
			else if (xCount <= 0)
			{
				Debug.LogWarning("You are trying to create a SelectionGrid with zero or less elements to be displayed in the horizontal direction. Set xCount to a positive value.");
				result = selected;
			}
			else
			{
				int num2 = num / xCount;
				if (num % xCount != 0)
				{
					num2++;
				}
				float num3 = (float)GUI.CalcTotalHorizSpacing(xCount, style, firstStyle, midStyle, lastStyle);
				float num4 = (float)(Mathf.Max(style.margin.top, style.margin.bottom) * (num2 - 1));
				float elemWidth = (position.width - num3) / (float)xCount;
				float elemHeight = (position.height - num4) / (float)num2;
				if (style.fixedWidth != 0f)
				{
					elemWidth = style.fixedWidth;
				}
				if (style.fixedHeight != 0f)
				{
					elemHeight = style.fixedHeight;
				}
				Rect[] array = GUI.CalcMouseRects(position, contents, xCount, elemWidth, elemHeight, style, firstStyle, midStyle, lastStyle, false, buttonSize);
				GUIStyle guistyle = null;
				int num5 = 0;
				for (int i = 0; i < num; i++)
				{
					Rect rect = array[i];
					GUIContent guicontent = contents[i];
					if (controlNames != null)
					{
						GUI.SetNextControlName(controlNames[i]);
					}
					int controlID = GUIUtility.GetControlID(GUI.s_ButtonGridHash, FocusType.Passive, rect);
					if (i == selected)
					{
						num5 = controlID;
					}
					switch (Event.current.GetTypeForControl(controlID))
					{
					case EventType.MouseDown:
						if (rect.Contains(Event.current.mousePosition))
						{
							GUIUtility.hotControl = controlID;
							Event.current.Use();
						}
						break;
					case EventType.MouseUp:
						if (GUIUtility.hotControl == controlID)
						{
							GUIUtility.hotControl = 0;
							Event.current.Use();
							GUI.changed = true;
							return i;
						}
						break;
					case EventType.MouseDrag:
						if (GUIUtility.hotControl == controlID)
						{
							Event.current.Use();
						}
						break;
					case EventType.Repaint:
					{
						GUIStyle guistyle2 = (num != 1) ? ((i != 0) ? ((i != num - 1) ? midStyle : lastStyle) : firstStyle) : style;
						bool flag = rect.Contains(Event.current.mousePosition);
						bool flag2 = GUIUtility.hotControl == controlID;
						if (selected != i)
						{
							guistyle2.Draw(rect, guicontent, flag && (GUI.enabled || flag2) && (flag2 || GUIUtility.hotControl == 0), GUI.enabled && flag2, false, false);
						}
						else
						{
							guistyle = guistyle2;
						}
						if (flag)
						{
							GUIUtility.mouseUsed = true;
							if (!string.IsNullOrEmpty(guicontent.tooltip))
							{
								GUIStyle.SetMouseTooltip(guicontent.tooltip, rect);
							}
						}
						break;
					}
					}
				}
				if (guistyle != null)
				{
					Rect position2 = array[selected];
					GUIContent content = contents[selected];
					bool flag3 = position2.Contains(Event.current.mousePosition);
					bool flag4 = GUIUtility.hotControl == num5;
					guistyle.Draw(position2, content, flag3 && (GUI.enabled || flag4) && (flag4 || GUIUtility.hotControl == 0), GUI.enabled && flag4, true, false);
				}
				result = selected;
			}
			return result;
		}

		// Token: 0x060000D1 RID: 209 RVA: 0x00004FC4 File Offset: 0x000031C4
		private static Rect[] CalcMouseRects(Rect position, GUIContent[] contents, int xCount, float elemWidth, float elemHeight, GUIStyle style, GUIStyle firstStyle, GUIStyle midStyle, GUIStyle lastStyle, bool addBorders, GUI.ToolbarButtonSize buttonSize)
		{
			int num = contents.Length;
			int num2 = 0;
			float x = position.xMin;
			float num3 = position.yMin;
			GUIStyle guistyle = style;
			Rect[] array = new Rect[num];
			if (num > 1)
			{
				guistyle = firstStyle;
			}
			for (int i = 0; i < num; i++)
			{
				float width = 0f;
				if (buttonSize != GUI.ToolbarButtonSize.Fixed)
				{
					if (buttonSize == GUI.ToolbarButtonSize.FitToContents)
					{
						width = guistyle.CalcSize(contents[i]).x;
					}
				}
				else
				{
					width = elemWidth;
				}
				if (!addBorders)
				{
					array[i] = new Rect(x, num3, width, elemHeight);
				}
				else
				{
					array[i] = guistyle.margin.Add(new Rect(x, num3, width, elemHeight));
				}
				array[i] = GUIUtility.AlignRectToDevice(array[i]);
				GUIStyle guistyle2 = midStyle;
				if (i == num - 2 || i == xCount - 2)
				{
					guistyle2 = lastStyle;
				}
				x = array[i].xMax + (float)Mathf.Max(guistyle.margin.right, guistyle2.margin.left);
				num2++;
				if (num2 >= xCount)
				{
					num2 = 0;
					num3 += elemHeight + (float)Mathf.Max(style.margin.top, style.margin.bottom);
					x = position.xMin;
					guistyle2 = firstStyle;
				}
				guistyle = guistyle2;
			}
			return array;
		}

		// Token: 0x060000D2 RID: 210 RVA: 0x00005158 File Offset: 0x00003358
		public static float HorizontalSlider(Rect position, float value, float leftValue, float rightValue)
		{
			return GUI.Slider(position, value, 0f, leftValue, rightValue, GUI.skin.horizontalSlider, GUI.skin.horizontalSliderThumb, true, 0);
		}

		// Token: 0x060000D3 RID: 211 RVA: 0x00005194 File Offset: 0x00003394
		public static float HorizontalSlider(Rect position, float value, float leftValue, float rightValue, GUIStyle slider, GUIStyle thumb)
		{
			return GUI.Slider(position, value, 0f, leftValue, rightValue, slider, thumb, true, 0);
		}

		// Token: 0x060000D4 RID: 212 RVA: 0x000051C0 File Offset: 0x000033C0
		public static float VerticalSlider(Rect position, float value, float topValue, float bottomValue)
		{
			return GUI.Slider(position, value, 0f, topValue, bottomValue, GUI.skin.verticalSlider, GUI.skin.verticalSliderThumb, false, 0);
		}

		// Token: 0x060000D5 RID: 213 RVA: 0x000051FC File Offset: 0x000033FC
		public static float VerticalSlider(Rect position, float value, float topValue, float bottomValue, GUIStyle slider, GUIStyle thumb)
		{
			return GUI.Slider(position, value, 0f, topValue, bottomValue, slider, thumb, false, 0);
		}

		// Token: 0x060000D6 RID: 214 RVA: 0x00005228 File Offset: 0x00003428
		public static float Slider(Rect position, float value, float size, float start, float end, GUIStyle slider, GUIStyle thumb, bool horiz, int id)
		{
			GUIUtility.CheckOnGUI();
			if (id == 0)
			{
				id = GUIUtility.GetControlID(GUI.s_SliderHash, FocusType.Passive, position);
			}
			SliderHandler sliderHandler = new SliderHandler(position, value, size, start, end, slider, thumb, horiz, id);
			return sliderHandler.Handle();
		}

		// Token: 0x060000D7 RID: 215 RVA: 0x00005278 File Offset: 0x00003478
		public static float HorizontalScrollbar(Rect position, float value, float size, float leftValue, float rightValue)
		{
			return GUI.Scroller(position, value, size, leftValue, rightValue, GUI.skin.horizontalScrollbar, GUI.skin.horizontalScrollbarThumb, GUI.skin.horizontalScrollbarLeftButton, GUI.skin.horizontalScrollbarRightButton, true);
		}

		// Token: 0x060000D8 RID: 216 RVA: 0x000052C4 File Offset: 0x000034C4
		public static float HorizontalScrollbar(Rect position, float value, float size, float leftValue, float rightValue, GUIStyle style)
		{
			return GUI.Scroller(position, value, size, leftValue, rightValue, style, GUI.skin.GetStyle(style.name + "thumb"), GUI.skin.GetStyle(style.name + "leftbutton"), GUI.skin.GetStyle(style.name + "rightbutton"), true);
		}

		// Token: 0x060000D9 RID: 217 RVA: 0x00005338 File Offset: 0x00003538
		internal static bool ScrollerRepeatButton(int scrollerID, Rect rect, GUIStyle style)
		{
			bool result = false;
			if (GUI.DoRepeatButton(rect, GUIContent.none, style, FocusType.Passive))
			{
				bool flag = GUI.s_ScrollControlId != scrollerID;
				GUI.s_ScrollControlId = scrollerID;
				if (flag)
				{
					result = true;
					GUI.nextScrollStepTime = DateTime.Now.AddMilliseconds(250.0);
				}
				else if (DateTime.Now >= GUI.nextScrollStepTime)
				{
					result = true;
					GUI.nextScrollStepTime = DateTime.Now.AddMilliseconds(30.0);
				}
				if (Event.current.type == EventType.Repaint)
				{
					GUI.InternalRepaintEditorWindow();
				}
			}
			return result;
		}

		// Token: 0x060000DA RID: 218 RVA: 0x000053EC File Offset: 0x000035EC
		public static float VerticalScrollbar(Rect position, float value, float size, float topValue, float bottomValue)
		{
			return GUI.Scroller(position, value, size, topValue, bottomValue, GUI.skin.verticalScrollbar, GUI.skin.verticalScrollbarThumb, GUI.skin.verticalScrollbarUpButton, GUI.skin.verticalScrollbarDownButton, false);
		}

		// Token: 0x060000DB RID: 219 RVA: 0x00005438 File Offset: 0x00003638
		public static float VerticalScrollbar(Rect position, float value, float size, float topValue, float bottomValue, GUIStyle style)
		{
			return GUI.Scroller(position, value, size, topValue, bottomValue, style, GUI.skin.GetStyle(style.name + "thumb"), GUI.skin.GetStyle(style.name + "upbutton"), GUI.skin.GetStyle(style.name + "downbutton"), false);
		}

		// Token: 0x060000DC RID: 220 RVA: 0x000054AC File Offset: 0x000036AC
		internal static float Scroller(Rect position, float value, float size, float leftValue, float rightValue, GUIStyle slider, GUIStyle thumb, GUIStyle leftButton, GUIStyle rightButton, bool horiz)
		{
			GUIUtility.CheckOnGUI();
			int controlID = GUIUtility.GetControlID(GUI.s_SliderHash, FocusType.Passive, position);
			Rect position2;
			Rect rect;
			Rect rect2;
			if (horiz)
			{
				position2 = new Rect(position.x + leftButton.fixedWidth, position.y, position.width - leftButton.fixedWidth - rightButton.fixedWidth, position.height);
				rect = new Rect(position.x, position.y, leftButton.fixedWidth, position.height);
				rect2 = new Rect(position.xMax - rightButton.fixedWidth, position.y, rightButton.fixedWidth, position.height);
			}
			else
			{
				position2 = new Rect(position.x, position.y + leftButton.fixedHeight, position.width, position.height - leftButton.fixedHeight - rightButton.fixedHeight);
				rect = new Rect(position.x, position.y, position.width, leftButton.fixedHeight);
				rect2 = new Rect(position.x, position.yMax - rightButton.fixedHeight, position.width, rightButton.fixedHeight);
			}
			value = GUI.Slider(position2, value, size, leftValue, rightValue, slider, thumb, horiz, controlID);
			bool flag = Event.current.type == EventType.MouseUp;
			if (GUI.ScrollerRepeatButton(controlID, rect, leftButton))
			{
				value -= 10f * ((leftValue >= rightValue) ? -1f : 1f);
			}
			if (GUI.ScrollerRepeatButton(controlID, rect2, rightButton))
			{
				value += 10f * ((leftValue >= rightValue) ? -1f : 1f);
			}
			if (flag && Event.current.type == EventType.Used)
			{
				GUI.s_ScrollControlId = 0;
			}
			if (leftValue < rightValue)
			{
				value = Mathf.Clamp(value, leftValue, rightValue - size);
			}
			else
			{
				value = Mathf.Clamp(value, rightValue, leftValue - size);
			}
			return value;
		}

		// Token: 0x060000DD RID: 221 RVA: 0x000056BE File Offset: 0x000038BE
		public static void BeginClip(Rect position, Vector2 scrollOffset, Vector2 renderOffset, bool resetOffset)
		{
			GUIUtility.CheckOnGUI();
			GUIClip.Push(position, scrollOffset, renderOffset, resetOffset);
		}

		// Token: 0x060000DE RID: 222 RVA: 0x000056CF File Offset: 0x000038CF
		public static void BeginGroup(Rect position)
		{
			GUI.BeginGroup(position, GUIContent.none, GUIStyle.none);
		}

		// Token: 0x060000DF RID: 223 RVA: 0x000056E2 File Offset: 0x000038E2
		public static void BeginGroup(Rect position, string text)
		{
			GUI.BeginGroup(position, GUIContent.Temp(text), GUIStyle.none);
		}

		// Token: 0x060000E0 RID: 224 RVA: 0x000056F6 File Offset: 0x000038F6
		public static void BeginGroup(Rect position, Texture image)
		{
			GUI.BeginGroup(position, GUIContent.Temp(image), GUIStyle.none);
		}

		// Token: 0x060000E1 RID: 225 RVA: 0x0000570A File Offset: 0x0000390A
		public static void BeginGroup(Rect position, GUIContent content)
		{
			GUI.BeginGroup(position, content, GUIStyle.none);
		}

		// Token: 0x060000E2 RID: 226 RVA: 0x00005719 File Offset: 0x00003919
		public static void BeginGroup(Rect position, GUIStyle style)
		{
			GUI.BeginGroup(position, GUIContent.none, style);
		}

		// Token: 0x060000E3 RID: 227 RVA: 0x00005728 File Offset: 0x00003928
		public static void BeginGroup(Rect position, string text, GUIStyle style)
		{
			GUI.BeginGroup(position, GUIContent.Temp(text), style);
		}

		// Token: 0x060000E4 RID: 228 RVA: 0x00005738 File Offset: 0x00003938
		public static void BeginGroup(Rect position, Texture image, GUIStyle style)
		{
			GUI.BeginGroup(position, GUIContent.Temp(image), style);
		}

		// Token: 0x060000E5 RID: 229 RVA: 0x00005748 File Offset: 0x00003948
		public static void BeginGroup(Rect position, GUIContent content, GUIStyle style)
		{
			GUI.BeginGroup(position, content, style, Vector2.zero);
		}

		// Token: 0x060000E6 RID: 230 RVA: 0x00005758 File Offset: 0x00003958
		internal static void BeginGroup(Rect position, GUIContent content, GUIStyle style, Vector2 scrollOffset)
		{
			GUIUtility.CheckOnGUI();
			int controlID = GUIUtility.GetControlID(GUI.s_BeginGroupHash, FocusType.Passive);
			if (content != GUIContent.none || style != GUIStyle.none)
			{
				EventType type = Event.current.type;
				if (type != EventType.Repaint)
				{
					if (position.Contains(Event.current.mousePosition))
					{
						GUIUtility.mouseUsed = true;
					}
				}
				else
				{
					style.Draw(position, content, controlID);
				}
			}
			GUIClip.Push(position, scrollOffset, Vector2.zero, false);
		}

		// Token: 0x060000E7 RID: 231 RVA: 0x000057E2 File Offset: 0x000039E2
		public static void EndGroup()
		{
			GUIUtility.CheckOnGUI();
			GUIClip.Internal_Pop();
		}

		// Token: 0x060000E8 RID: 232 RVA: 0x000057EF File Offset: 0x000039EF
		public static void BeginClip(Rect position)
		{
			GUIUtility.CheckOnGUI();
			GUIClip.Push(position, Vector2.zero, Vector2.zero, false);
		}

		// Token: 0x060000E9 RID: 233 RVA: 0x00005808 File Offset: 0x00003A08
		public static void EndClip()
		{
			GUIUtility.CheckOnGUI();
			GUIClip.Pop();
		}

		// Token: 0x060000EA RID: 234 RVA: 0x00005818 File Offset: 0x00003A18
		public static Vector2 BeginScrollView(Rect position, Vector2 scrollPosition, Rect viewRect)
		{
			return GUI.BeginScrollView(position, scrollPosition, viewRect, false, false, GUI.skin.horizontalScrollbar, GUI.skin.verticalScrollbar, GUI.skin.scrollView);
		}

		// Token: 0x060000EB RID: 235 RVA: 0x00005858 File Offset: 0x00003A58
		public static Vector2 BeginScrollView(Rect position, Vector2 scrollPosition, Rect viewRect, bool alwaysShowHorizontal, bool alwaysShowVertical)
		{
			return GUI.BeginScrollView(position, scrollPosition, viewRect, alwaysShowHorizontal, alwaysShowVertical, GUI.skin.horizontalScrollbar, GUI.skin.verticalScrollbar, GUI.skin.scrollView);
		}

		// Token: 0x060000EC RID: 236 RVA: 0x00005898 File Offset: 0x00003A98
		public static Vector2 BeginScrollView(Rect position, Vector2 scrollPosition, Rect viewRect, GUIStyle horizontalScrollbar, GUIStyle verticalScrollbar)
		{
			return GUI.BeginScrollView(position, scrollPosition, viewRect, false, false, horizontalScrollbar, verticalScrollbar, GUI.skin.scrollView);
		}

		// Token: 0x060000ED RID: 237 RVA: 0x000058C4 File Offset: 0x00003AC4
		public static Vector2 BeginScrollView(Rect position, Vector2 scrollPosition, Rect viewRect, bool alwaysShowHorizontal, bool alwaysShowVertical, GUIStyle horizontalScrollbar, GUIStyle verticalScrollbar)
		{
			return GUI.BeginScrollView(position, scrollPosition, viewRect, alwaysShowHorizontal, alwaysShowVertical, horizontalScrollbar, verticalScrollbar, GUI.skin.scrollView);
		}

		// Token: 0x060000EE RID: 238 RVA: 0x000058F4 File Offset: 0x00003AF4
		protected static Vector2 DoBeginScrollView(Rect position, Vector2 scrollPosition, Rect viewRect, bool alwaysShowHorizontal, bool alwaysShowVertical, GUIStyle horizontalScrollbar, GUIStyle verticalScrollbar, GUIStyle background)
		{
			return GUI.BeginScrollView(position, scrollPosition, viewRect, alwaysShowHorizontal, alwaysShowVertical, horizontalScrollbar, verticalScrollbar, background);
		}

		// Token: 0x060000EF RID: 239 RVA: 0x0000591C File Offset: 0x00003B1C
		internal static Vector2 BeginScrollView(Rect position, Vector2 scrollPosition, Rect viewRect, bool alwaysShowHorizontal, bool alwaysShowVertical, GUIStyle horizontalScrollbar, GUIStyle verticalScrollbar, GUIStyle background)
		{
			GUIUtility.CheckOnGUI();
			int controlID = GUIUtility.GetControlID(GUI.s_ScrollviewHash, FocusType.Passive);
			ScrollViewState scrollViewState = (ScrollViewState)GUIUtility.GetStateObject(typeof(ScrollViewState), controlID);
			if (scrollViewState.apply)
			{
				scrollPosition = scrollViewState.scrollPosition;
				scrollViewState.apply = false;
			}
			scrollViewState.position = position;
			scrollViewState.scrollPosition = scrollPosition;
			scrollViewState.visibleRect = (scrollViewState.viewRect = viewRect);
			scrollViewState.visibleRect.width = position.width;
			scrollViewState.visibleRect.height = position.height;
			GUI.s_ScrollViewStates.Push(scrollViewState);
			Rect screenRect = new Rect(position);
			EventType type = Event.current.type;
			if (type != EventType.Layout)
			{
				if (type != EventType.Used)
				{
					bool flag = alwaysShowVertical;
					bool flag2 = alwaysShowHorizontal;
					if (flag2 || viewRect.width > screenRect.width)
					{
						scrollViewState.visibleRect.height = position.height - horizontalScrollbar.fixedHeight + (float)horizontalScrollbar.margin.top;
						screenRect.height -= horizontalScrollbar.fixedHeight + (float)horizontalScrollbar.margin.top;
						flag2 = true;
					}
					if (flag || viewRect.height > screenRect.height)
					{
						scrollViewState.visibleRect.width = position.width - verticalScrollbar.fixedWidth + (float)verticalScrollbar.margin.left;
						screenRect.width -= verticalScrollbar.fixedWidth + (float)verticalScrollbar.margin.left;
						flag = true;
						if (!flag2 && viewRect.width > screenRect.width)
						{
							scrollViewState.visibleRect.height = position.height - horizontalScrollbar.fixedHeight + (float)horizontalScrollbar.margin.top;
							screenRect.height -= horizontalScrollbar.fixedHeight + (float)horizontalScrollbar.margin.top;
							flag2 = true;
						}
					}
					if (Event.current.type == EventType.Repaint && background != GUIStyle.none)
					{
						background.Draw(position, position.Contains(Event.current.mousePosition), false, flag2 && flag, false);
					}
					if (flag2 && horizontalScrollbar != GUIStyle.none)
					{
						scrollPosition.x = GUI.HorizontalScrollbar(new Rect(position.x, position.yMax - horizontalScrollbar.fixedHeight, screenRect.width, horizontalScrollbar.fixedHeight), scrollPosition.x, Mathf.Min(screenRect.width, viewRect.width), 0f, viewRect.width, horizontalScrollbar);
					}
					else
					{
						GUIUtility.GetControlID(GUI.s_SliderHash, FocusType.Passive);
						GUIUtility.GetControlID(GUI.s_RepeatButtonHash, FocusType.Passive);
						GUIUtility.GetControlID(GUI.s_RepeatButtonHash, FocusType.Passive);
						scrollPosition.x = ((horizontalScrollbar == GUIStyle.none) ? Mathf.Clamp(scrollPosition.x, 0f, Mathf.Max(viewRect.width - position.width, 0f)) : 0f);
					}
					if (flag && verticalScrollbar != GUIStyle.none)
					{
						scrollPosition.y = GUI.VerticalScrollbar(new Rect(screenRect.xMax + (float)verticalScrollbar.margin.left, screenRect.y, verticalScrollbar.fixedWidth, screenRect.height), scrollPosition.y, Mathf.Min(screenRect.height, viewRect.height), 0f, viewRect.height, verticalScrollbar);
					}
					else
					{
						GUIUtility.GetControlID(GUI.s_SliderHash, FocusType.Passive);
						GUIUtility.GetControlID(GUI.s_RepeatButtonHash, FocusType.Passive);
						GUIUtility.GetControlID(GUI.s_RepeatButtonHash, FocusType.Passive);
						scrollPosition.y = ((verticalScrollbar == GUIStyle.none) ? Mathf.Clamp(scrollPosition.y, 0f, Mathf.Max(viewRect.height - position.height, 0f)) : 0f);
					}
				}
			}
			else
			{
				GUIUtility.GetControlID(GUI.s_SliderHash, FocusType.Passive);
				GUIUtility.GetControlID(GUI.s_RepeatButtonHash, FocusType.Passive);
				GUIUtility.GetControlID(GUI.s_RepeatButtonHash, FocusType.Passive);
				GUIUtility.GetControlID(GUI.s_SliderHash, FocusType.Passive);
				GUIUtility.GetControlID(GUI.s_RepeatButtonHash, FocusType.Passive);
				GUIUtility.GetControlID(GUI.s_RepeatButtonHash, FocusType.Passive);
			}
			GUIClip.Push(screenRect, new Vector2(Mathf.Round(-scrollPosition.x - viewRect.x), Mathf.Round(-scrollPosition.y - viewRect.y)), Vector2.zero, false);
			return scrollPosition;
		}

		// Token: 0x060000F0 RID: 240 RVA: 0x00005DD8 File Offset: 0x00003FD8
		public static void EndScrollView()
		{
			GUI.EndScrollView(true);
		}

		// Token: 0x060000F1 RID: 241 RVA: 0x00005DE4 File Offset: 0x00003FE4
		public static void EndScrollView(bool handleScrollWheel)
		{
			GUIUtility.CheckOnGUI();
			ScrollViewState scrollViewState = (ScrollViewState)GUI.s_ScrollViewStates.Peek();
			GUIClip.Pop();
			GUI.s_ScrollViewStates.Pop();
			if (handleScrollWheel && Event.current.type == EventType.ScrollWheel && scrollViewState.position.Contains(Event.current.mousePosition))
			{
				scrollViewState.scrollPosition.x = Mathf.Clamp(scrollViewState.scrollPosition.x + Event.current.delta.x * 20f, 0f, scrollViewState.viewRect.width - scrollViewState.visibleRect.width);
				scrollViewState.scrollPosition.y = Mathf.Clamp(scrollViewState.scrollPosition.y + Event.current.delta.y * 20f, 0f, scrollViewState.viewRect.height - scrollViewState.visibleRect.height);
				if (scrollViewState.scrollPosition.x < 0f)
				{
					scrollViewState.scrollPosition.x = 0f;
				}
				if (scrollViewState.scrollPosition.y < 0f)
				{
					scrollViewState.scrollPosition.y = 0f;
				}
				scrollViewState.apply = true;
				Event.current.Use();
			}
		}

		// Token: 0x060000F2 RID: 242 RVA: 0x00005F44 File Offset: 0x00004144
		internal static ScrollViewState GetTopScrollView()
		{
			ScrollViewState result;
			if (GUI.s_ScrollViewStates.Count != 0)
			{
				result = (ScrollViewState)GUI.s_ScrollViewStates.Peek();
			}
			else
			{
				result = null;
			}
			return result;
		}

		// Token: 0x060000F3 RID: 243 RVA: 0x00005F80 File Offset: 0x00004180
		public static void ScrollTo(Rect position)
		{
			ScrollViewState topScrollView = GUI.GetTopScrollView();
			if (topScrollView != null)
			{
				topScrollView.ScrollTo(position);
			}
		}

		// Token: 0x060000F4 RID: 244 RVA: 0x00005FA4 File Offset: 0x000041A4
		public static bool ScrollTowards(Rect position, float maxDelta)
		{
			ScrollViewState topScrollView = GUI.GetTopScrollView();
			return topScrollView != null && topScrollView.ScrollTowards(position, maxDelta);
		}

		// Token: 0x060000F5 RID: 245 RVA: 0x00005FD4 File Offset: 0x000041D4
		public static Rect Window(int id, Rect clientRect, GUI.WindowFunction func, string text)
		{
			GUIUtility.CheckOnGUI();
			return GUI.DoWindow(id, clientRect, func, GUIContent.Temp(text), GUI.skin.window, GUI.skin, true);
		}

		// Token: 0x060000F6 RID: 246 RVA: 0x0000600C File Offset: 0x0000420C
		public static Rect Window(int id, Rect clientRect, GUI.WindowFunction func, Texture image)
		{
			GUIUtility.CheckOnGUI();
			return GUI.DoWindow(id, clientRect, func, GUIContent.Temp(image), GUI.skin.window, GUI.skin, true);
		}

		// Token: 0x060000F7 RID: 247 RVA: 0x00006044 File Offset: 0x00004244
		public static Rect Window(int id, Rect clientRect, GUI.WindowFunction func, GUIContent content)
		{
			GUIUtility.CheckOnGUI();
			return GUI.DoWindow(id, clientRect, func, content, GUI.skin.window, GUI.skin, true);
		}

		// Token: 0x060000F8 RID: 248 RVA: 0x00006078 File Offset: 0x00004278
		public static Rect Window(int id, Rect clientRect, GUI.WindowFunction func, string text, GUIStyle style)
		{
			GUIUtility.CheckOnGUI();
			return GUI.DoWindow(id, clientRect, func, GUIContent.Temp(text), style, GUI.skin, true);
		}

		// Token: 0x060000F9 RID: 249 RVA: 0x000060A8 File Offset: 0x000042A8
		public static Rect Window(int id, Rect clientRect, GUI.WindowFunction func, Texture image, GUIStyle style)
		{
			GUIUtility.CheckOnGUI();
			return GUI.DoWindow(id, clientRect, func, GUIContent.Temp(image), style, GUI.skin, true);
		}

		// Token: 0x060000FA RID: 250 RVA: 0x000060D8 File Offset: 0x000042D8
		public static Rect Window(int id, Rect clientRect, GUI.WindowFunction func, GUIContent title, GUIStyle style)
		{
			GUIUtility.CheckOnGUI();
			return GUI.DoWindow(id, clientRect, func, title, style, GUI.skin, true);
		}

		// Token: 0x060000FB RID: 251 RVA: 0x00006104 File Offset: 0x00004304
		public static Rect ModalWindow(int id, Rect clientRect, GUI.WindowFunction func, string text)
		{
			GUIUtility.CheckOnGUI();
			return GUI.DoModalWindow(id, clientRect, func, GUIContent.Temp(text), GUI.skin.window, GUI.skin);
		}

		// Token: 0x060000FC RID: 252 RVA: 0x0000613C File Offset: 0x0000433C
		public static Rect ModalWindow(int id, Rect clientRect, GUI.WindowFunction func, Texture image)
		{
			GUIUtility.CheckOnGUI();
			return GUI.DoModalWindow(id, clientRect, func, GUIContent.Temp(image), GUI.skin.window, GUI.skin);
		}

		// Token: 0x060000FD RID: 253 RVA: 0x00006174 File Offset: 0x00004374
		public static Rect ModalWindow(int id, Rect clientRect, GUI.WindowFunction func, GUIContent content)
		{
			GUIUtility.CheckOnGUI();
			return GUI.DoModalWindow(id, clientRect, func, content, GUI.skin.window, GUI.skin);
		}

		// Token: 0x060000FE RID: 254 RVA: 0x000061A8 File Offset: 0x000043A8
		public static Rect ModalWindow(int id, Rect clientRect, GUI.WindowFunction func, string text, GUIStyle style)
		{
			GUIUtility.CheckOnGUI();
			return GUI.DoModalWindow(id, clientRect, func, GUIContent.Temp(text), style, GUI.skin);
		}

		// Token: 0x060000FF RID: 255 RVA: 0x000061D8 File Offset: 0x000043D8
		public static Rect ModalWindow(int id, Rect clientRect, GUI.WindowFunction func, Texture image, GUIStyle style)
		{
			GUIUtility.CheckOnGUI();
			return GUI.DoModalWindow(id, clientRect, func, GUIContent.Temp(image), style, GUI.skin);
		}

		// Token: 0x06000100 RID: 256 RVA: 0x00006208 File Offset: 0x00004408
		public static Rect ModalWindow(int id, Rect clientRect, GUI.WindowFunction func, GUIContent content, GUIStyle style)
		{
			GUIUtility.CheckOnGUI();
			return GUI.DoModalWindow(id, clientRect, func, content, style, GUI.skin);
		}

		// Token: 0x06000101 RID: 257 RVA: 0x00006234 File Offset: 0x00004434
		private static Rect DoWindow(int id, Rect clientRect, GUI.WindowFunction func, GUIContent title, GUIStyle style, GUISkin skin, bool forceRectOnLayout)
		{
			return GUI.Internal_DoWindow(id, GUIUtility.s_OriginalID, clientRect, func, title, style, skin, forceRectOnLayout);
		}

		// Token: 0x06000102 RID: 258 RVA: 0x00006260 File Offset: 0x00004460
		private static Rect DoModalWindow(int id, Rect clientRect, GUI.WindowFunction func, GUIContent content, GUIStyle style, GUISkin skin)
		{
			return GUI.Internal_DoModalWindow(id, GUIUtility.s_OriginalID, clientRect, func, content, style, skin);
		}

		// Token: 0x06000103 RID: 259 RVA: 0x00006288 File Offset: 0x00004488
		[RequiredByNativeCode]
		internal static void CallWindowDelegate(GUI.WindowFunction func, int id, int instanceID, GUISkin _skin, int forceRect, float width, float height, GUIStyle style)
		{
			GUILayoutUtility.SelectIDList(id, true);
			GUISkin skin = GUI.skin;
			if (Event.current.type == EventType.Layout)
			{
				if (forceRect != 0)
				{
					GUILayoutOption[] options = new GUILayoutOption[]
					{
						GUILayout.Width(width),
						GUILayout.Height(height)
					};
					GUILayoutUtility.BeginWindow(id, style, options);
				}
				else
				{
					GUILayoutUtility.BeginWindow(id, style, null);
				}
			}
			else
			{
				GUILayoutUtility.BeginWindow(id, GUIStyle.none, null);
			}
			GUI.skin = _skin;
			func(id);
			if (Event.current.type == EventType.Layout)
			{
				GUILayoutUtility.Layout();
			}
			GUI.skin = skin;
		}

		// Token: 0x06000104 RID: 260 RVA: 0x00006330 File Offset: 0x00004530
		public static void DragWindow()
		{
			GUI.DragWindow(new Rect(0f, 0f, 10000f, 10000f));
		}

		// Token: 0x06000105 RID: 261 RVA: 0x00006354 File Offset: 0x00004554
		internal static void BeginWindows(int skinMode, int editorWindowInstanceID)
		{
			GUILayoutGroup topLevel = GUILayoutUtility.current.topLevel;
			GenericStack layoutGroups = GUILayoutUtility.current.layoutGroups;
			GUILayoutGroup windows = GUILayoutUtility.current.windows;
			Matrix4x4 matrix = GUI.matrix;
			GUI.Internal_BeginWindows();
			GUI.matrix = matrix;
			GUILayoutUtility.current.topLevel = topLevel;
			GUILayoutUtility.current.layoutGroups = layoutGroups;
			GUILayoutUtility.current.windows = windows;
		}

		// Token: 0x06000106 RID: 262 RVA: 0x000063B8 File Offset: 0x000045B8
		internal static void EndWindows()
		{
			GUILayoutGroup topLevel = GUILayoutUtility.current.topLevel;
			GenericStack layoutGroups = GUILayoutUtility.current.layoutGroups;
			GUILayoutGroup windows = GUILayoutUtility.current.windows;
			GUI.Internal_EndWindows();
			GUILayoutUtility.current.topLevel = topLevel;
			GUILayoutUtility.current.layoutGroups = layoutGroups;
			GUILayoutUtility.current.windows = windows;
		}

		// Token: 0x06000107 RID: 263
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void get_color_Injected(out Color ret);

		// Token: 0x06000108 RID: 264
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void set_color_Injected(ref Color value);

		// Token: 0x06000109 RID: 265
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void get_backgroundColor_Injected(out Color ret);

		// Token: 0x0600010A RID: 266
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void set_backgroundColor_Injected(ref Color value);

		// Token: 0x0600010B RID: 267
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void get_contentColor_Injected(out Color ret);

		// Token: 0x0600010C RID: 268
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void set_contentColor_Injected(ref Color value);

		// Token: 0x0600010D RID: 269
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void Internal_DoModalWindow_Injected(int id, int instanceID, ref Rect clientRect, GUI.WindowFunction func, GUIContent content, GUIStyle style, object skin, out Rect ret);

		// Token: 0x0600010E RID: 270
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void Internal_DoWindow_Injected(int id, int instanceID, ref Rect clientRect, GUI.WindowFunction func, GUIContent title, GUIStyle style, object skin, bool forceRectOnLayout, out Rect ret);

		// Token: 0x0600010F RID: 271
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void DragWindow_Injected(ref Rect position);

		// Token: 0x04000043 RID: 67
		private const float s_ScrollStepSize = 10f;

		// Token: 0x04000044 RID: 68
		private static int s_ScrollControlId;

		// Token: 0x04000045 RID: 69
		private static int s_HotTextField = -1;

		// Token: 0x04000046 RID: 70
		private static readonly int s_BoxHash = "Box".GetHashCode();

		// Token: 0x04000047 RID: 71
		private static readonly int s_ButonHash = "Button".GetHashCode();

		// Token: 0x04000048 RID: 72
		private static readonly int s_RepeatButtonHash = "repeatButton".GetHashCode();

		// Token: 0x04000049 RID: 73
		private static readonly int s_ToggleHash = "Toggle".GetHashCode();

		// Token: 0x0400004A RID: 74
		private static readonly int s_ButtonGridHash = "ButtonGrid".GetHashCode();

		// Token: 0x0400004B RID: 75
		private static readonly int s_SliderHash = "Slider".GetHashCode();

		// Token: 0x0400004C RID: 76
		private static readonly int s_BeginGroupHash = "BeginGroup".GetHashCode();

		// Token: 0x0400004D RID: 77
		private static readonly int s_ScrollviewHash = "scrollView".GetHashCode();

		// Token: 0x0400004E RID: 78
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private static int <scrollTroughSide>k__BackingField;

		// Token: 0x0400004F RID: 79
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private static DateTime <nextScrollStepTime>k__BackingField;

		// Token: 0x04000050 RID: 80
		private static GUISkin s_Skin;

		// Token: 0x04000051 RID: 81
		internal static Rect s_ToolTipRect;

		// Token: 0x04000052 RID: 82
		private static readonly GenericStack s_ScrollViewStates = new GenericStack();

		// Token: 0x02000008 RID: 8
		public enum ToolbarButtonSize
		{
			// Token: 0x04000054 RID: 84
			Fixed,
			// Token: 0x04000055 RID: 85
			FitToContents
		}

		// Token: 0x02000009 RID: 9
		// (Invoke) Token: 0x06000111 RID: 273
		public delegate void WindowFunction(int id);

		// Token: 0x0200000A RID: 10
		public abstract class Scope : IDisposable
		{
			// Token: 0x06000114 RID: 276 RVA: 0x0000338C File Offset: 0x0000158C
			protected Scope()
			{
			}

			// Token: 0x06000115 RID: 277
			protected abstract void CloseScope();

			// Token: 0x06000116 RID: 278 RVA: 0x00006410 File Offset: 0x00004610
			~Scope()
			{
				if (!this.m_Disposed)
				{
					Debug.LogError("Scope was not disposed! You should use the 'using' keyword or manually call Dispose.");
				}
			}

			// Token: 0x06000117 RID: 279 RVA: 0x00006450 File Offset: 0x00004650
			public void Dispose()
			{
				if (!this.m_Disposed)
				{
					this.m_Disposed = true;
					if (!GUIUtility.guiIsExiting)
					{
						this.CloseScope();
					}
				}
			}

			// Token: 0x04000056 RID: 86
			private bool m_Disposed;
		}

		// Token: 0x0200000B RID: 11
		public class GroupScope : GUI.Scope
		{
			// Token: 0x06000118 RID: 280 RVA: 0x0000647A File Offset: 0x0000467A
			public GroupScope(Rect position)
			{
				GUI.BeginGroup(position);
			}

			// Token: 0x06000119 RID: 281 RVA: 0x00006489 File Offset: 0x00004689
			public GroupScope(Rect position, string text)
			{
				GUI.BeginGroup(position, text);
			}

			// Token: 0x0600011A RID: 282 RVA: 0x00006499 File Offset: 0x00004699
			public GroupScope(Rect position, Texture image)
			{
				GUI.BeginGroup(position, image);
			}

			// Token: 0x0600011B RID: 283 RVA: 0x000064A9 File Offset: 0x000046A9
			public GroupScope(Rect position, GUIContent content)
			{
				GUI.BeginGroup(position, content);
			}

			// Token: 0x0600011C RID: 284 RVA: 0x000064B9 File Offset: 0x000046B9
			public GroupScope(Rect position, GUIStyle style)
			{
				GUI.BeginGroup(position, style);
			}

			// Token: 0x0600011D RID: 285 RVA: 0x000064C9 File Offset: 0x000046C9
			public GroupScope(Rect position, string text, GUIStyle style)
			{
				GUI.BeginGroup(position, text, style);
			}

			// Token: 0x0600011E RID: 286 RVA: 0x000064DA File Offset: 0x000046DA
			public GroupScope(Rect position, Texture image, GUIStyle style)
			{
				GUI.BeginGroup(position, image, style);
			}

			// Token: 0x0600011F RID: 287 RVA: 0x000064EB File Offset: 0x000046EB
			protected override void CloseScope()
			{
				GUI.EndGroup();
			}
		}

		// Token: 0x0200000C RID: 12
		public class ScrollViewScope : GUI.Scope
		{
			// Token: 0x06000120 RID: 288 RVA: 0x000064F3 File Offset: 0x000046F3
			public ScrollViewScope(Rect position, Vector2 scrollPosition, Rect viewRect)
			{
				this.handleScrollWheel = true;
				this.scrollPosition = GUI.BeginScrollView(position, scrollPosition, viewRect);
			}

			// Token: 0x06000121 RID: 289 RVA: 0x00006511 File Offset: 0x00004711
			public ScrollViewScope(Rect position, Vector2 scrollPosition, Rect viewRect, bool alwaysShowHorizontal, bool alwaysShowVertical)
			{
				this.handleScrollWheel = true;
				this.scrollPosition = GUI.BeginScrollView(position, scrollPosition, viewRect, alwaysShowHorizontal, alwaysShowVertical);
			}

			// Token: 0x06000122 RID: 290 RVA: 0x00006533 File Offset: 0x00004733
			public ScrollViewScope(Rect position, Vector2 scrollPosition, Rect viewRect, GUIStyle horizontalScrollbar, GUIStyle verticalScrollbar)
			{
				this.handleScrollWheel = true;
				this.scrollPosition = GUI.BeginScrollView(position, scrollPosition, viewRect, horizontalScrollbar, verticalScrollbar);
			}

			// Token: 0x06000123 RID: 291 RVA: 0x00006555 File Offset: 0x00004755
			public ScrollViewScope(Rect position, Vector2 scrollPosition, Rect viewRect, bool alwaysShowHorizontal, bool alwaysShowVertical, GUIStyle horizontalScrollbar, GUIStyle verticalScrollbar)
			{
				this.handleScrollWheel = true;
				this.scrollPosition = GUI.BeginScrollView(position, scrollPosition, viewRect, alwaysShowHorizontal, alwaysShowVertical, horizontalScrollbar, verticalScrollbar);
			}

			// Token: 0x06000124 RID: 292 RVA: 0x0000657C File Offset: 0x0000477C
			internal ScrollViewScope(Rect position, Vector2 scrollPosition, Rect viewRect, bool alwaysShowHorizontal, bool alwaysShowVertical, GUIStyle horizontalScrollbar, GUIStyle verticalScrollbar, GUIStyle background)
			{
				this.handleScrollWheel = true;
				this.scrollPosition = GUI.BeginScrollView(position, scrollPosition, viewRect, alwaysShowHorizontal, alwaysShowVertical, horizontalScrollbar, verticalScrollbar, background);
			}

			// Token: 0x1700002C RID: 44
			// (get) Token: 0x06000125 RID: 293 RVA: 0x000065B0 File Offset: 0x000047B0
			// (set) Token: 0x06000126 RID: 294 RVA: 0x000065CA File Offset: 0x000047CA
			public Vector2 scrollPosition
			{
				[CompilerGenerated]
				get
				{
					return this.<scrollPosition>k__BackingField;
				}
				[CompilerGenerated]
				private set
				{
					this.<scrollPosition>k__BackingField = value;
				}
			}

			// Token: 0x1700002D RID: 45
			// (get) Token: 0x06000127 RID: 295 RVA: 0x000065D4 File Offset: 0x000047D4
			// (set) Token: 0x06000128 RID: 296 RVA: 0x000065EE File Offset: 0x000047EE
			public bool handleScrollWheel
			{
				[CompilerGenerated]
				get
				{
					return this.<handleScrollWheel>k__BackingField;
				}
				[CompilerGenerated]
				set
				{
					this.<handleScrollWheel>k__BackingField = value;
				}
			}

			// Token: 0x06000129 RID: 297 RVA: 0x000065F7 File Offset: 0x000047F7
			protected override void CloseScope()
			{
				GUI.EndScrollView(this.handleScrollWheel);
			}

			// Token: 0x04000057 RID: 87
			[DebuggerBrowsable(DebuggerBrowsableState.Never)]
			[CompilerGenerated]
			private Vector2 <scrollPosition>k__BackingField;

			// Token: 0x04000058 RID: 88
			[DebuggerBrowsable(DebuggerBrowsableState.Never)]
			[CompilerGenerated]
			private bool <handleScrollWheel>k__BackingField;
		}

		// Token: 0x0200000D RID: 13
		public class ClipScope : GUI.Scope
		{
			// Token: 0x0600012A RID: 298 RVA: 0x00006605 File Offset: 0x00004805
			public ClipScope(Rect position)
			{
				GUI.BeginClip(position);
			}

			// Token: 0x0600012B RID: 299 RVA: 0x00006614 File Offset: 0x00004814
			internal ClipScope(Rect position, Vector2 scrollOffset)
			{
				GUI.BeginClip(position, scrollOffset, default(Vector2), false);
			}

			// Token: 0x0600012C RID: 300 RVA: 0x00006639 File Offset: 0x00004839
			protected override void CloseScope()
			{
				GUI.EndClip();
			}
		}

		// Token: 0x0200000E RID: 14
		internal struct ColorScope : IDisposable
		{
			// Token: 0x0600012D RID: 301 RVA: 0x00006641 File Offset: 0x00004841
			public ColorScope(Color newColor)
			{
				this.m_Disposed = false;
				this.m_PreviousColor = GUI.color;
				GUI.color = newColor;
			}

			// Token: 0x0600012E RID: 302 RVA: 0x0000665C File Offset: 0x0000485C
			public ColorScope(float r, float g, float b, float a = 1f)
			{
				this = new GUI.ColorScope(new Color(r, g, b, a));
			}

			// Token: 0x0600012F RID: 303 RVA: 0x0000666F File Offset: 0x0000486F
			public void Dispose()
			{
				if (!this.m_Disposed)
				{
					this.m_Disposed = true;
					GUI.color = this.m_PreviousColor;
				}
			}

			// Token: 0x04000059 RID: 89
			private bool m_Disposed;

			// Token: 0x0400005A RID: 90
			private Color m_PreviousColor;
		}
	}
}
