﻿using System;

namespace UnityEngine
{
	// Token: 0x02000027 RID: 39
	public enum TextClipping
	{
		// Token: 0x040000C6 RID: 198
		Overflow,
		// Token: 0x040000C7 RID: 199
		Clip
	}
}
