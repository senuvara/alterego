﻿using System;

namespace UnityEngine
{
	// Token: 0x02000005 RID: 5
	[Flags]
	public enum EventModifiers
	{
		// Token: 0x04000039 RID: 57
		None = 0,
		// Token: 0x0400003A RID: 58
		Shift = 1,
		// Token: 0x0400003B RID: 59
		Control = 2,
		// Token: 0x0400003C RID: 60
		Alt = 4,
		// Token: 0x0400003D RID: 61
		Command = 8,
		// Token: 0x0400003E RID: 62
		Numeric = 16,
		// Token: 0x0400003F RID: 63
		CapsLock = 32,
		// Token: 0x04000040 RID: 64
		FunctionKey = 64
	}
}
