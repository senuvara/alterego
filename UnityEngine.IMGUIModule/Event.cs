﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000002 RID: 2
	[NativeHeader("Modules/IMGUI/Event.bindings.h")]
	[StaticAccessor("GUIEvent", StaticAccessorType.DoubleColon)]
	[StructLayout(LayoutKind.Sequential)]
	public sealed class Event
	{
		// Token: 0x06000001 RID: 1 RVA: 0x00002050 File Offset: 0x00000250
		public Event()
		{
			this.m_Ptr = Event.Internal_Create(0);
		}

		// Token: 0x06000002 RID: 2 RVA: 0x00002065 File Offset: 0x00000265
		public Event(int displayIndex)
		{
			this.m_Ptr = Event.Internal_Create(displayIndex);
		}

		// Token: 0x06000003 RID: 3 RVA: 0x0000207A File Offset: 0x0000027A
		public Event(Event other)
		{
			if (other == null)
			{
				throw new ArgumentException("Event to copy from is null.");
			}
			this.m_Ptr = Event.Internal_Copy(other.m_Ptr);
		}

		// Token: 0x17000001 RID: 1
		// (get) Token: 0x06000004 RID: 4
		[NativeProperty("type", false, TargetType.Field)]
		public extern EventType rawType { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000002 RID: 2
		// (get) Token: 0x06000005 RID: 5 RVA: 0x000020A8 File Offset: 0x000002A8
		// (set) Token: 0x06000006 RID: 6 RVA: 0x000020BE File Offset: 0x000002BE
		[NativeProperty("mousePosition", false, TargetType.Field)]
		public Vector2 mousePosition
		{
			get
			{
				Vector2 result;
				this.get_mousePosition_Injected(out result);
				return result;
			}
			set
			{
				this.set_mousePosition_Injected(ref value);
			}
		}

		// Token: 0x17000003 RID: 3
		// (get) Token: 0x06000007 RID: 7 RVA: 0x000020C8 File Offset: 0x000002C8
		// (set) Token: 0x06000008 RID: 8 RVA: 0x000020DE File Offset: 0x000002DE
		[NativeProperty("delta", false, TargetType.Field)]
		public Vector2 delta
		{
			get
			{
				Vector2 result;
				this.get_delta_Injected(out result);
				return result;
			}
			set
			{
				this.set_delta_Injected(ref value);
			}
		}

		// Token: 0x17000004 RID: 4
		// (get) Token: 0x06000009 RID: 9
		// (set) Token: 0x0600000A RID: 10
		[NativeProperty("button", false, TargetType.Field)]
		public extern int button { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000005 RID: 5
		// (get) Token: 0x0600000B RID: 11
		// (set) Token: 0x0600000C RID: 12
		[NativeProperty("modifiers", false, TargetType.Field)]
		public extern EventModifiers modifiers { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000006 RID: 6
		// (get) Token: 0x0600000D RID: 13
		// (set) Token: 0x0600000E RID: 14
		[NativeProperty("pressure", false, TargetType.Field)]
		public extern float pressure { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000007 RID: 7
		// (get) Token: 0x0600000F RID: 15
		// (set) Token: 0x06000010 RID: 16
		[NativeProperty("clickCount", false, TargetType.Field)]
		public extern int clickCount { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000008 RID: 8
		// (get) Token: 0x06000011 RID: 17
		// (set) Token: 0x06000012 RID: 18
		[NativeProperty("character", false, TargetType.Field)]
		public extern char character { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000009 RID: 9
		// (get) Token: 0x06000013 RID: 19
		// (set) Token: 0x06000014 RID: 20
		[NativeProperty("keycode", false, TargetType.Field)]
		public extern KeyCode keyCode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700000A RID: 10
		// (get) Token: 0x06000015 RID: 21
		// (set) Token: 0x06000016 RID: 22
		[NativeProperty("displayIndex", false, TargetType.Field)]
		public extern int displayIndex { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700000B RID: 11
		// (get) Token: 0x06000017 RID: 23
		// (set) Token: 0x06000018 RID: 24
		public extern EventType type { [FreeFunction("GUIEvent::GetType", HasExplicitThis = true)] [MethodImpl(MethodImplOptions.InternalCall)] get; [FreeFunction("GUIEvent::SetType", HasExplicitThis = true)] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700000C RID: 12
		// (get) Token: 0x06000019 RID: 25
		// (set) Token: 0x0600001A RID: 26
		public extern string commandName { [FreeFunction("GUIEvent::GetCommandName", HasExplicitThis = true)] [MethodImpl(MethodImplOptions.InternalCall)] get; [FreeFunction("GUIEvent::SetCommandName", HasExplicitThis = true)] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x0600001B RID: 27
		[NativeMethod("Use")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void Internal_Use();

		// Token: 0x0600001C RID: 28
		[FreeFunction("GUIEvent::Internal_Create", IsThreadSafe = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern IntPtr Internal_Create(int displayIndex);

		// Token: 0x0600001D RID: 29
		[FreeFunction("GUIEvent::Internal_Destroy", IsThreadSafe = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void Internal_Destroy(IntPtr ptr);

		// Token: 0x0600001E RID: 30
		[FreeFunction("GUIEvent::Internal_Copy", IsThreadSafe = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern IntPtr Internal_Copy(IntPtr otherPtr);

		// Token: 0x0600001F RID: 31
		[FreeFunction("GUIEvent::GetTypeForControl", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern EventType GetTypeForControl(int controlID);

		// Token: 0x06000020 RID: 32
		[VisibleToOtherModules(new string[]
		{
			"UnityEngine.UIElementsModule"
		})]
		[FreeFunction("GUIEvent::CopyFromPtr", IsThreadSafe = true, HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern void CopyFromPtr(IntPtr ptr);

		// Token: 0x06000021 RID: 33
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern bool PopEvent(Event outEvent);

		// Token: 0x06000022 RID: 34
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern int GetEventCount();

		// Token: 0x06000023 RID: 35
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void Internal_SetNativeEvent(IntPtr ptr);

		// Token: 0x06000024 RID: 36 RVA: 0x000020E8 File Offset: 0x000002E8
		[RequiredByNativeCode]
		internal static void Internal_MakeMasterEventCurrent(int displayIndex)
		{
			if (Event.s_MasterEvent == null)
			{
				Event.s_MasterEvent = new Event(displayIndex);
			}
			Event.s_MasterEvent.displayIndex = displayIndex;
			Event.s_Current = Event.s_MasterEvent;
			Event.Internal_SetNativeEvent(Event.s_MasterEvent.m_Ptr);
		}

		// Token: 0x06000025 RID: 37 RVA: 0x00002124 File Offset: 0x00000324
		~Event()
		{
			if (this.m_Ptr != IntPtr.Zero)
			{
				Event.Internal_Destroy(this.m_Ptr);
				this.m_Ptr = IntPtr.Zero;
			}
		}

		// Token: 0x06000026 RID: 38 RVA: 0x0000217C File Offset: 0x0000037C
		internal static void CleanupRoots()
		{
			Event.s_Current = null;
			Event.s_MasterEvent = null;
		}

		// Token: 0x06000027 RID: 39 RVA: 0x0000218B File Offset: 0x0000038B
		[VisibleToOtherModules(new string[]
		{
			"UnityEngine.UIElementsModule"
		})]
		internal void CopyFrom(Event e)
		{
			if (e.m_Ptr != this.m_Ptr)
			{
				this.CopyFromPtr(e.m_Ptr);
			}
		}

		// Token: 0x1700000D RID: 13
		// (get) Token: 0x06000028 RID: 40 RVA: 0x000021B4 File Offset: 0x000003B4
		// (set) Token: 0x06000029 RID: 41 RVA: 0x000021D8 File Offset: 0x000003D8
		[Obsolete("Use HandleUtility.GUIPointToWorldRay(Event.current.mousePosition);", true)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public Ray mouseRay
		{
			get
			{
				return new Ray(Vector3.up, Vector3.up);
			}
			set
			{
			}
		}

		// Token: 0x1700000E RID: 14
		// (get) Token: 0x0600002A RID: 42 RVA: 0x000021DC File Offset: 0x000003DC
		// (set) Token: 0x0600002B RID: 43 RVA: 0x000021FF File Offset: 0x000003FF
		public bool shift
		{
			get
			{
				return (this.modifiers & EventModifiers.Shift) != EventModifiers.None;
			}
			set
			{
				if (!value)
				{
					this.modifiers &= ~EventModifiers.Shift;
				}
				else
				{
					this.modifiers |= EventModifiers.Shift;
				}
			}
		}

		// Token: 0x1700000F RID: 15
		// (get) Token: 0x0600002C RID: 44 RVA: 0x0000222C File Offset: 0x0000042C
		// (set) Token: 0x0600002D RID: 45 RVA: 0x0000224F File Offset: 0x0000044F
		public bool control
		{
			get
			{
				return (this.modifiers & EventModifiers.Control) != EventModifiers.None;
			}
			set
			{
				if (!value)
				{
					this.modifiers &= ~EventModifiers.Control;
				}
				else
				{
					this.modifiers |= EventModifiers.Control;
				}
			}
		}

		// Token: 0x17000010 RID: 16
		// (get) Token: 0x0600002E RID: 46 RVA: 0x0000227C File Offset: 0x0000047C
		// (set) Token: 0x0600002F RID: 47 RVA: 0x0000229F File Offset: 0x0000049F
		public bool alt
		{
			get
			{
				return (this.modifiers & EventModifiers.Alt) != EventModifiers.None;
			}
			set
			{
				if (!value)
				{
					this.modifiers &= ~EventModifiers.Alt;
				}
				else
				{
					this.modifiers |= EventModifiers.Alt;
				}
			}
		}

		// Token: 0x17000011 RID: 17
		// (get) Token: 0x06000030 RID: 48 RVA: 0x000022CC File Offset: 0x000004CC
		// (set) Token: 0x06000031 RID: 49 RVA: 0x000022EF File Offset: 0x000004EF
		public bool command
		{
			get
			{
				return (this.modifiers & EventModifiers.Command) != EventModifiers.None;
			}
			set
			{
				if (!value)
				{
					this.modifiers &= ~EventModifiers.Command;
				}
				else
				{
					this.modifiers |= EventModifiers.Command;
				}
			}
		}

		// Token: 0x17000012 RID: 18
		// (get) Token: 0x06000032 RID: 50 RVA: 0x0000231C File Offset: 0x0000051C
		// (set) Token: 0x06000033 RID: 51 RVA: 0x00002340 File Offset: 0x00000540
		public bool capsLock
		{
			get
			{
				return (this.modifiers & EventModifiers.CapsLock) != EventModifiers.None;
			}
			set
			{
				if (!value)
				{
					this.modifiers &= ~EventModifiers.CapsLock;
				}
				else
				{
					this.modifiers |= EventModifiers.CapsLock;
				}
			}
		}

		// Token: 0x17000013 RID: 19
		// (get) Token: 0x06000034 RID: 52 RVA: 0x0000236C File Offset: 0x0000056C
		// (set) Token: 0x06000035 RID: 53 RVA: 0x00002390 File Offset: 0x00000590
		public bool numeric
		{
			get
			{
				return (this.modifiers & EventModifiers.Numeric) != EventModifiers.None;
			}
			set
			{
				if (!value)
				{
					this.modifiers &= ~EventModifiers.Numeric;
				}
				else
				{
					this.modifiers |= EventModifiers.Numeric;
				}
			}
		}

		// Token: 0x17000014 RID: 20
		// (get) Token: 0x06000036 RID: 54 RVA: 0x000023BC File Offset: 0x000005BC
		public bool functionKey
		{
			[CompilerGenerated]
			get
			{
				return (this.modifiers & EventModifiers.FunctionKey) != EventModifiers.None;
			}
		}

		// Token: 0x17000015 RID: 21
		// (get) Token: 0x06000037 RID: 55 RVA: 0x000023E0 File Offset: 0x000005E0
		// (set) Token: 0x06000038 RID: 56 RVA: 0x000023FA File Offset: 0x000005FA
		public static Event current
		{
			get
			{
				return Event.s_Current;
			}
			set
			{
				Event.s_Current = (value ?? Event.s_MasterEvent);
				Event.Internal_SetNativeEvent(Event.s_Current.m_Ptr);
			}
		}

		// Token: 0x17000016 RID: 22
		// (get) Token: 0x06000039 RID: 57 RVA: 0x00002420 File Offset: 0x00000620
		public bool isKey
		{
			get
			{
				EventType type = this.type;
				return type == EventType.KeyDown || type == EventType.KeyUp;
			}
		}

		// Token: 0x17000017 RID: 23
		// (get) Token: 0x0600003A RID: 58 RVA: 0x0000244C File Offset: 0x0000064C
		public bool isMouse
		{
			get
			{
				EventType type = this.type;
				return type == EventType.MouseMove || type == EventType.MouseDown || type == EventType.MouseUp || type == EventType.MouseDrag || type == EventType.ContextClick || type == EventType.MouseEnterWindow || type == EventType.MouseLeaveWindow;
			}
		}

		// Token: 0x17000018 RID: 24
		// (get) Token: 0x0600003B RID: 59 RVA: 0x0000249C File Offset: 0x0000069C
		public bool isScrollWheel
		{
			get
			{
				EventType type = this.type;
				return type == EventType.ScrollWheel;
			}
		}

		// Token: 0x0600003C RID: 60 RVA: 0x000024BC File Offset: 0x000006BC
		public static Event KeyboardEvent(string key)
		{
			Event @event = new Event(0)
			{
				type = EventType.KeyDown
			};
			Event result;
			if (string.IsNullOrEmpty(key))
			{
				result = @event;
			}
			else
			{
				int num = 0;
				bool flag;
				do
				{
					flag = true;
					if (num >= key.Length)
					{
						break;
					}
					char c = key[num];
					switch (c)
					{
					case '#':
						@event.modifiers |= EventModifiers.Shift;
						num++;
						break;
					default:
						if (c != '^')
						{
							flag = false;
						}
						else
						{
							@event.modifiers |= EventModifiers.Control;
							num++;
						}
						break;
					case '%':
						@event.modifiers |= EventModifiers.Command;
						num++;
						break;
					case '&':
						@event.modifiers |= EventModifiers.Alt;
						num++;
						break;
					}
				}
				while (flag);
				string text = key.Substring(num, key.Length - num).ToLowerInvariant();
				switch (text)
				{
				case "[0]":
					@event.character = '0';
					@event.keyCode = KeyCode.Keypad0;
					goto IL_A8A;
				case "[1]":
					@event.character = '1';
					@event.keyCode = KeyCode.Keypad1;
					goto IL_A8A;
				case "[2]":
					@event.character = '2';
					@event.keyCode = KeyCode.Keypad2;
					goto IL_A8A;
				case "[3]":
					@event.character = '3';
					@event.keyCode = KeyCode.Keypad3;
					goto IL_A8A;
				case "[4]":
					@event.character = '4';
					@event.keyCode = KeyCode.Keypad4;
					goto IL_A8A;
				case "[5]":
					@event.character = '5';
					@event.keyCode = KeyCode.Keypad5;
					goto IL_A8A;
				case "[6]":
					@event.character = '6';
					@event.keyCode = KeyCode.Keypad6;
					goto IL_A8A;
				case "[7]":
					@event.character = '7';
					@event.keyCode = KeyCode.Keypad7;
					goto IL_A8A;
				case "[8]":
					@event.character = '8';
					@event.keyCode = KeyCode.Keypad8;
					goto IL_A8A;
				case "[9]":
					@event.character = '9';
					@event.keyCode = KeyCode.Keypad9;
					goto IL_A8A;
				case "[.]":
					@event.character = '.';
					@event.keyCode = KeyCode.KeypadPeriod;
					goto IL_A8A;
				case "[/]":
					@event.character = '/';
					@event.keyCode = KeyCode.KeypadDivide;
					goto IL_A8A;
				case "[-]":
					@event.character = '-';
					@event.keyCode = KeyCode.KeypadMinus;
					goto IL_A8A;
				case "[+]":
					@event.character = '+';
					@event.keyCode = KeyCode.KeypadPlus;
					goto IL_A8A;
				case "[=]":
					@event.character = '=';
					@event.keyCode = KeyCode.KeypadEquals;
					goto IL_A8A;
				case "[equals]":
					@event.character = '=';
					@event.keyCode = KeyCode.KeypadEquals;
					goto IL_A8A;
				case "[enter]":
					@event.character = '\n';
					@event.keyCode = KeyCode.KeypadEnter;
					goto IL_A8A;
				case "up":
					@event.keyCode = KeyCode.UpArrow;
					@event.modifiers |= EventModifiers.FunctionKey;
					goto IL_A8A;
				case "down":
					@event.keyCode = KeyCode.DownArrow;
					@event.modifiers |= EventModifiers.FunctionKey;
					goto IL_A8A;
				case "left":
					@event.keyCode = KeyCode.LeftArrow;
					@event.modifiers |= EventModifiers.FunctionKey;
					goto IL_A8A;
				case "right":
					@event.keyCode = KeyCode.RightArrow;
					@event.modifiers |= EventModifiers.FunctionKey;
					goto IL_A8A;
				case "insert":
					@event.keyCode = KeyCode.Insert;
					@event.modifiers |= EventModifiers.FunctionKey;
					goto IL_A8A;
				case "home":
					@event.keyCode = KeyCode.Home;
					@event.modifiers |= EventModifiers.FunctionKey;
					goto IL_A8A;
				case "end":
					@event.keyCode = KeyCode.End;
					@event.modifiers |= EventModifiers.FunctionKey;
					goto IL_A8A;
				case "pgup":
					@event.keyCode = KeyCode.PageDown;
					@event.modifiers |= EventModifiers.FunctionKey;
					goto IL_A8A;
				case "page up":
					@event.keyCode = KeyCode.PageUp;
					@event.modifiers |= EventModifiers.FunctionKey;
					goto IL_A8A;
				case "pgdown":
					@event.keyCode = KeyCode.PageUp;
					@event.modifiers |= EventModifiers.FunctionKey;
					goto IL_A8A;
				case "page down":
					@event.keyCode = KeyCode.PageDown;
					@event.modifiers |= EventModifiers.FunctionKey;
					goto IL_A8A;
				case "backspace":
					@event.keyCode = KeyCode.Backspace;
					@event.modifiers |= EventModifiers.FunctionKey;
					goto IL_A8A;
				case "delete":
					@event.keyCode = KeyCode.Delete;
					@event.modifiers |= EventModifiers.FunctionKey;
					goto IL_A8A;
				case "tab":
					@event.keyCode = KeyCode.Tab;
					goto IL_A8A;
				case "f1":
					@event.keyCode = KeyCode.F1;
					@event.modifiers |= EventModifiers.FunctionKey;
					goto IL_A8A;
				case "f2":
					@event.keyCode = KeyCode.F2;
					@event.modifiers |= EventModifiers.FunctionKey;
					goto IL_A8A;
				case "f3":
					@event.keyCode = KeyCode.F3;
					@event.modifiers |= EventModifiers.FunctionKey;
					goto IL_A8A;
				case "f4":
					@event.keyCode = KeyCode.F4;
					@event.modifiers |= EventModifiers.FunctionKey;
					goto IL_A8A;
				case "f5":
					@event.keyCode = KeyCode.F5;
					@event.modifiers |= EventModifiers.FunctionKey;
					goto IL_A8A;
				case "f6":
					@event.keyCode = KeyCode.F6;
					@event.modifiers |= EventModifiers.FunctionKey;
					goto IL_A8A;
				case "f7":
					@event.keyCode = KeyCode.F7;
					@event.modifiers |= EventModifiers.FunctionKey;
					goto IL_A8A;
				case "f8":
					@event.keyCode = KeyCode.F8;
					@event.modifiers |= EventModifiers.FunctionKey;
					goto IL_A8A;
				case "f9":
					@event.keyCode = KeyCode.F9;
					@event.modifiers |= EventModifiers.FunctionKey;
					goto IL_A8A;
				case "f10":
					@event.keyCode = KeyCode.F10;
					@event.modifiers |= EventModifiers.FunctionKey;
					goto IL_A8A;
				case "f11":
					@event.keyCode = KeyCode.F11;
					@event.modifiers |= EventModifiers.FunctionKey;
					goto IL_A8A;
				case "f12":
					@event.keyCode = KeyCode.F12;
					@event.modifiers |= EventModifiers.FunctionKey;
					goto IL_A8A;
				case "f13":
					@event.keyCode = KeyCode.F13;
					@event.modifiers |= EventModifiers.FunctionKey;
					goto IL_A8A;
				case "f14":
					@event.keyCode = KeyCode.F14;
					@event.modifiers |= EventModifiers.FunctionKey;
					goto IL_A8A;
				case "f15":
					@event.keyCode = KeyCode.F15;
					@event.modifiers |= EventModifiers.FunctionKey;
					goto IL_A8A;
				case "[esc]":
					@event.keyCode = KeyCode.Escape;
					goto IL_A8A;
				case "return":
					@event.character = '\n';
					@event.keyCode = KeyCode.Return;
					@event.modifiers &= ~EventModifiers.FunctionKey;
					goto IL_A8A;
				case "space":
					@event.keyCode = KeyCode.Space;
					@event.character = ' ';
					@event.modifiers &= ~EventModifiers.FunctionKey;
					goto IL_A8A;
				}
				if (text.Length != 1)
				{
					try
					{
						@event.keyCode = (KeyCode)Enum.Parse(typeof(KeyCode), text, true);
					}
					catch (ArgumentException)
					{
						Debug.LogError(UnityString.Format("Unable to find key name that matches '{0}'", new object[]
						{
							text
						}));
					}
				}
				else
				{
					@event.character = text.ToLower()[0];
					@event.keyCode = (KeyCode)@event.character;
					if (@event.modifiers != EventModifiers.None)
					{
						@event.character = '\0';
					}
				}
				IL_A8A:
				result = @event;
			}
			return result;
		}

		// Token: 0x0600003D RID: 61 RVA: 0x00002F6C File Offset: 0x0000116C
		public override int GetHashCode()
		{
			int num = 1;
			if (this.isKey)
			{
				num = (int)((ushort)this.keyCode);
			}
			if (this.isMouse)
			{
				num = this.mousePosition.GetHashCode();
			}
			return num * 37 | (int)this.modifiers;
		}

		// Token: 0x0600003E RID: 62 RVA: 0x00002FC4 File Offset: 0x000011C4
		public override bool Equals(object obj)
		{
			bool result;
			if (obj == null)
			{
				result = false;
			}
			else if (object.ReferenceEquals(this, obj))
			{
				result = true;
			}
			else if (obj.GetType() != base.GetType())
			{
				result = false;
			}
			else
			{
				Event @event = (Event)obj;
				if (this.type != @event.type || (this.modifiers & ~EventModifiers.CapsLock) != (@event.modifiers & ~EventModifiers.CapsLock))
				{
					result = false;
				}
				else if (this.isKey)
				{
					result = (this.keyCode == @event.keyCode);
				}
				else
				{
					result = (this.isMouse && this.mousePosition == @event.mousePosition);
				}
			}
			return result;
		}

		// Token: 0x0600003F RID: 63 RVA: 0x0000308C File Offset: 0x0000128C
		public override string ToString()
		{
			string result;
			if (this.isKey)
			{
				if (this.character == '\0')
				{
					result = UnityString.Format("Event:{0}   Character:\\0   Modifiers:{1}   KeyCode:{2}", new object[]
					{
						this.type,
						this.modifiers,
						this.keyCode
					});
				}
				else
				{
					result = string.Concat(new object[]
					{
						"Event:",
						this.type,
						"   Character:",
						(int)this.character,
						"   Modifiers:",
						this.modifiers,
						"   KeyCode:",
						this.keyCode
					});
				}
			}
			else if (this.isMouse)
			{
				result = UnityString.Format("Event: {0}   Position: {1} Modifiers: {2}", new object[]
				{
					this.type,
					this.mousePosition,
					this.modifiers
				});
			}
			else if (this.type == EventType.ExecuteCommand || this.type == EventType.ValidateCommand)
			{
				result = UnityString.Format("Event: {0}  \"{1}\"", new object[]
				{
					this.type,
					this.commandName
				});
			}
			else
			{
				result = "" + this.type;
			}
			return result;
		}

		// Token: 0x06000040 RID: 64 RVA: 0x00003208 File Offset: 0x00001408
		public void Use()
		{
			if (this.type == EventType.Repaint || this.type == EventType.Layout)
			{
				Debug.LogWarning(UnityString.Format("Event.Use() should not be called for events of type {0}", new object[]
				{
					this.type
				}));
			}
			this.Internal_Use();
		}

		// Token: 0x06000041 RID: 65
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_mousePosition_Injected(out Vector2 ret);

		// Token: 0x06000042 RID: 66
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_mousePosition_Injected(ref Vector2 value);

		// Token: 0x06000043 RID: 67
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_delta_Injected(out Vector2 ret);

		// Token: 0x06000044 RID: 68
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_delta_Injected(ref Vector2 value);

		// Token: 0x04000001 RID: 1
		[NonSerialized]
		internal IntPtr m_Ptr;

		// Token: 0x04000002 RID: 2
		private static Event s_Current;

		// Token: 0x04000003 RID: 3
		private static Event s_MasterEvent;

		// Token: 0x04000004 RID: 4
		[CompilerGenerated]
		private static Dictionary<string, int> <>f__switch$map0;
	}
}
