﻿using System;
using System.Reflection;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000028 RID: 40
	[AttributeUsage(AttributeTargets.Method)]
	public class GUITargetAttribute : Attribute
	{
		// Token: 0x06000328 RID: 808 RVA: 0x0000AD2E File Offset: 0x00008F2E
		public GUITargetAttribute()
		{
			this.displayMask = -1;
		}

		// Token: 0x06000329 RID: 809 RVA: 0x0000AD3E File Offset: 0x00008F3E
		public GUITargetAttribute(int displayIndex)
		{
			this.displayMask = 1 << displayIndex;
		}

		// Token: 0x0600032A RID: 810 RVA: 0x0000AD53 File Offset: 0x00008F53
		public GUITargetAttribute(int displayIndex, int displayIndex1)
		{
			this.displayMask = (1 << displayIndex | 1 << displayIndex1);
		}

		// Token: 0x0600032B RID: 811 RVA: 0x0000AD70 File Offset: 0x00008F70
		public GUITargetAttribute(int displayIndex, int displayIndex1, params int[] displayIndexList)
		{
			this.displayMask = (1 << displayIndex | 1 << displayIndex1);
			for (int i = 0; i < displayIndexList.Length; i++)
			{
				this.displayMask |= 1 << displayIndexList[i];
			}
		}

		// Token: 0x0600032C RID: 812 RVA: 0x0000ADC0 File Offset: 0x00008FC0
		[RequiredByNativeCode]
		private static int GetGUITargetAttrValue(Type klass, string methodName)
		{
			MethodInfo method = klass.GetMethod(methodName, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
			if (method != null)
			{
				object[] customAttributes = method.GetCustomAttributes(true);
				if (customAttributes != null)
				{
					for (int i = 0; i < customAttributes.Length; i++)
					{
						if (customAttributes[i].GetType() == typeof(GUITargetAttribute))
						{
							GUITargetAttribute guitargetAttribute = customAttributes[i] as GUITargetAttribute;
							return guitargetAttribute.displayMask;
						}
					}
				}
			}
			return -1;
		}

		// Token: 0x040000C8 RID: 200
		internal int displayMask;
	}
}
