﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;

namespace UnityEngine
{
	// Token: 0x0200000F RID: 15
	[NativeHeader("Modules/IMGUI/GUIClip.h")]
	[NativeHeader("Modules/IMGUI/GUIState.h")]
	[VisibleToOtherModules(new string[]
	{
		"UnityEngine.UIElementsModule"
	})]
	internal sealed class GUIClip
	{
		// Token: 0x06000130 RID: 304 RVA: 0x0000338C File Offset: 0x0000158C
		public GUIClip()
		{
		}

		// Token: 0x1700002E RID: 46
		// (get) Token: 0x06000131 RID: 305
		internal static extern bool enabled { [FreeFunction("GetGUIState().m_CanvasGUIState.m_GUIClipState.GetEnabled")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700002F RID: 47
		// (get) Token: 0x06000132 RID: 306 RVA: 0x00006694 File Offset: 0x00004894
		internal static Rect visibleRect
		{
			[FreeFunction("GetGUIState().m_CanvasGUIState.m_GUIClipState.GetVisibleRect")]
			get
			{
				Rect result;
				GUIClip.get_visibleRect_Injected(out result);
				return result;
			}
		}

		// Token: 0x17000030 RID: 48
		// (get) Token: 0x06000133 RID: 307 RVA: 0x000066AC File Offset: 0x000048AC
		internal static Rect topmostRect
		{
			[FreeFunction("GetGUIState().m_CanvasGUIState.m_GUIClipState.GetTopMostPhysicalRect")]
			[VisibleToOtherModules(new string[]
			{
				"UnityEngine.UIElementsModule"
			})]
			get
			{
				Rect result;
				GUIClip.get_topmostRect_Injected(out result);
				return result;
			}
		}

		// Token: 0x06000134 RID: 308 RVA: 0x000066C1 File Offset: 0x000048C1
		[VisibleToOtherModules(new string[]
		{
			"UnityEngine.UIElementsModule"
		})]
		internal static void Internal_Push(Rect screenRect, Vector2 scrollOffset, Vector2 renderOffset, bool resetOffset)
		{
			GUIClip.Internal_Push_Injected(ref screenRect, ref scrollOffset, ref renderOffset, resetOffset);
		}

		// Token: 0x06000135 RID: 309
		[VisibleToOtherModules(new string[]
		{
			"UnityEngine.UIElementsModule"
		})]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern void Internal_Pop();

		// Token: 0x06000136 RID: 310
		[VisibleToOtherModules(new string[]
		{
			"UnityEngine.UIElementsModule"
		})]
		[FreeFunction("GetGUIState().m_CanvasGUIState.m_GUIClipState.GetCount")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern int Internal_GetCount();

		// Token: 0x06000137 RID: 311 RVA: 0x000066D0 File Offset: 0x000048D0
		[VisibleToOtherModules(new string[]
		{
			"UnityEngine.UIElementsModule"
		})]
		[FreeFunction("GetGUIState().m_CanvasGUIState.m_GUIClipState.GetTopRect")]
		internal static Rect GetTopRect()
		{
			Rect result;
			GUIClip.GetTopRect_Injected(out result);
			return result;
		}

		// Token: 0x06000138 RID: 312 RVA: 0x000066E8 File Offset: 0x000048E8
		[FreeFunction("GetGUIState().m_CanvasGUIState.m_GUIClipState.Unclip")]
		private static Vector2 Unclip_Vector2(Vector2 pos)
		{
			Vector2 result;
			GUIClip.Unclip_Vector2_Injected(ref pos, out result);
			return result;
		}

		// Token: 0x06000139 RID: 313 RVA: 0x00006700 File Offset: 0x00004900
		[FreeFunction("GetGUIState().m_CanvasGUIState.m_GUIClipState.Unclip")]
		private static Rect Unclip_Rect(Rect rect)
		{
			Rect result;
			GUIClip.Unclip_Rect_Injected(ref rect, out result);
			return result;
		}

		// Token: 0x0600013A RID: 314 RVA: 0x00006718 File Offset: 0x00004918
		[FreeFunction("GetGUIState().m_CanvasGUIState.m_GUIClipState.Clip")]
		private static Vector2 Clip_Vector2(Vector2 absolutePos)
		{
			Vector2 result;
			GUIClip.Clip_Vector2_Injected(ref absolutePos, out result);
			return result;
		}

		// Token: 0x0600013B RID: 315 RVA: 0x00006730 File Offset: 0x00004930
		[FreeFunction("GetGUIState().m_CanvasGUIState.m_GUIClipState.Clip")]
		private static Rect Internal_Clip_Rect(Rect absoluteRect)
		{
			Rect result;
			GUIClip.Internal_Clip_Rect_Injected(ref absoluteRect, out result);
			return result;
		}

		// Token: 0x0600013C RID: 316 RVA: 0x00006748 File Offset: 0x00004948
		[FreeFunction("GetGUIState().m_CanvasGUIState.m_GUIClipState.UnclipToWindow")]
		private static Vector2 UnclipToWindow_Vector2(Vector2 pos)
		{
			Vector2 result;
			GUIClip.UnclipToWindow_Vector2_Injected(ref pos, out result);
			return result;
		}

		// Token: 0x0600013D RID: 317 RVA: 0x00006760 File Offset: 0x00004960
		[FreeFunction("GetGUIState().m_CanvasGUIState.m_GUIClipState.UnclipToWindow")]
		private static Rect UnclipToWindow_Rect(Rect rect)
		{
			Rect result;
			GUIClip.UnclipToWindow_Rect_Injected(ref rect, out result);
			return result;
		}

		// Token: 0x0600013E RID: 318 RVA: 0x00006778 File Offset: 0x00004978
		[FreeFunction("GetGUIState().m_CanvasGUIState.m_GUIClipState.ClipToWindow")]
		private static Vector2 ClipToWindow_Vector2(Vector2 absolutePos)
		{
			Vector2 result;
			GUIClip.ClipToWindow_Vector2_Injected(ref absolutePos, out result);
			return result;
		}

		// Token: 0x0600013F RID: 319 RVA: 0x00006790 File Offset: 0x00004990
		[FreeFunction("GetGUIState().m_CanvasGUIState.m_GUIClipState.ClipToWindow")]
		private static Rect ClipToWindow_Rect(Rect absoluteRect)
		{
			Rect result;
			GUIClip.ClipToWindow_Rect_Injected(ref absoluteRect, out result);
			return result;
		}

		// Token: 0x06000140 RID: 320 RVA: 0x000067A8 File Offset: 0x000049A8
		[FreeFunction("GetGUIState().m_CanvasGUIState.m_GUIClipState.GetAbsoluteMousePosition")]
		private static Vector2 Internal_GetAbsoluteMousePosition()
		{
			Vector2 result;
			GUIClip.Internal_GetAbsoluteMousePosition_Injected(out result);
			return result;
		}

		// Token: 0x06000141 RID: 321
		[VisibleToOtherModules(new string[]
		{
			"UnityEngine.UIElementsModule"
		})]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern void Reapply();

		// Token: 0x06000142 RID: 322 RVA: 0x000067C0 File Offset: 0x000049C0
		[VisibleToOtherModules(new string[]
		{
			"UnityEngine.UIElementsModule"
		})]
		[FreeFunction("GetGUIState().m_CanvasGUIState.m_GUIClipState.GetUserMatrix")]
		internal static Matrix4x4 GetMatrix()
		{
			Matrix4x4 result;
			GUIClip.GetMatrix_Injected(out result);
			return result;
		}

		// Token: 0x06000143 RID: 323 RVA: 0x000067D5 File Offset: 0x000049D5
		[VisibleToOtherModules(new string[]
		{
			"UnityEngine.UIElementsModule"
		})]
		internal static void SetMatrix(Matrix4x4 m)
		{
			GUIClip.SetMatrix_Injected(ref m);
		}

		// Token: 0x06000144 RID: 324 RVA: 0x000067E0 File Offset: 0x000049E0
		[VisibleToOtherModules(new string[]
		{
			"UnityEngine.UIElementsModule"
		})]
		[FreeFunction("GetGUIState().m_CanvasGUIState.m_GUIClipState.GetParentTransform")]
		internal static Matrix4x4 GetParentMatrix()
		{
			Matrix4x4 result;
			GUIClip.GetParentMatrix_Injected(out result);
			return result;
		}

		// Token: 0x06000145 RID: 325 RVA: 0x000067F5 File Offset: 0x000049F5
		[VisibleToOtherModules(new string[]
		{
			"UnityEngine.UIElementsModule"
		})]
		internal static void Internal_PushParentClip(Matrix4x4 objectTransform, Rect clipRect)
		{
			GUIClip.Internal_PushParentClip_Injected(ref objectTransform, ref clipRect);
		}

		// Token: 0x06000146 RID: 326
		[VisibleToOtherModules(new string[]
		{
			"UnityEngine.UIElementsModule"
		})]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern void Internal_PopParentClip();

		// Token: 0x06000147 RID: 327 RVA: 0x00006800 File Offset: 0x00004A00
		internal static void Push(Rect screenRect, Vector2 scrollOffset, Vector2 renderOffset, bool resetOffset)
		{
			GUIClip.Internal_Push(screenRect, scrollOffset, renderOffset, resetOffset);
		}

		// Token: 0x06000148 RID: 328 RVA: 0x0000680C File Offset: 0x00004A0C
		internal static void Pop()
		{
			GUIClip.Internal_Pop();
		}

		// Token: 0x06000149 RID: 329 RVA: 0x00006814 File Offset: 0x00004A14
		public static Vector2 Unclip(Vector2 pos)
		{
			return GUIClip.Unclip_Vector2(pos);
		}

		// Token: 0x0600014A RID: 330 RVA: 0x00006830 File Offset: 0x00004A30
		public static Rect Unclip(Rect rect)
		{
			return GUIClip.Unclip_Rect(rect);
		}

		// Token: 0x0600014B RID: 331 RVA: 0x0000684C File Offset: 0x00004A4C
		public static Vector2 Clip(Vector2 absolutePos)
		{
			return GUIClip.Clip_Vector2(absolutePos);
		}

		// Token: 0x0600014C RID: 332 RVA: 0x00006868 File Offset: 0x00004A68
		public static Rect Clip(Rect absoluteRect)
		{
			return GUIClip.Internal_Clip_Rect(absoluteRect);
		}

		// Token: 0x0600014D RID: 333 RVA: 0x00006884 File Offset: 0x00004A84
		public static Vector2 UnclipToWindow(Vector2 pos)
		{
			return GUIClip.UnclipToWindow_Vector2(pos);
		}

		// Token: 0x0600014E RID: 334 RVA: 0x000068A0 File Offset: 0x00004AA0
		public static Rect UnclipToWindow(Rect rect)
		{
			return GUIClip.UnclipToWindow_Rect(rect);
		}

		// Token: 0x0600014F RID: 335 RVA: 0x000068BC File Offset: 0x00004ABC
		public static Vector2 ClipToWindow(Vector2 absolutePos)
		{
			return GUIClip.ClipToWindow_Vector2(absolutePos);
		}

		// Token: 0x06000150 RID: 336 RVA: 0x000068D8 File Offset: 0x00004AD8
		public static Rect ClipToWindow(Rect absoluteRect)
		{
			return GUIClip.ClipToWindow_Rect(absoluteRect);
		}

		// Token: 0x06000151 RID: 337 RVA: 0x000068F4 File Offset: 0x00004AF4
		public static Vector2 GetAbsoluteMousePosition()
		{
			return GUIClip.Internal_GetAbsoluteMousePosition();
		}

		// Token: 0x06000152 RID: 338
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void get_visibleRect_Injected(out Rect ret);

		// Token: 0x06000153 RID: 339
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void get_topmostRect_Injected(out Rect ret);

		// Token: 0x06000154 RID: 340
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void Internal_Push_Injected(ref Rect screenRect, ref Vector2 scrollOffset, ref Vector2 renderOffset, bool resetOffset);

		// Token: 0x06000155 RID: 341
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void GetTopRect_Injected(out Rect ret);

		// Token: 0x06000156 RID: 342
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void Unclip_Vector2_Injected(ref Vector2 pos, out Vector2 ret);

		// Token: 0x06000157 RID: 343
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void Unclip_Rect_Injected(ref Rect rect, out Rect ret);

		// Token: 0x06000158 RID: 344
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void Clip_Vector2_Injected(ref Vector2 absolutePos, out Vector2 ret);

		// Token: 0x06000159 RID: 345
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void Internal_Clip_Rect_Injected(ref Rect absoluteRect, out Rect ret);

		// Token: 0x0600015A RID: 346
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void UnclipToWindow_Vector2_Injected(ref Vector2 pos, out Vector2 ret);

		// Token: 0x0600015B RID: 347
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void UnclipToWindow_Rect_Injected(ref Rect rect, out Rect ret);

		// Token: 0x0600015C RID: 348
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void ClipToWindow_Vector2_Injected(ref Vector2 absolutePos, out Vector2 ret);

		// Token: 0x0600015D RID: 349
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void ClipToWindow_Rect_Injected(ref Rect absoluteRect, out Rect ret);

		// Token: 0x0600015E RID: 350
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void Internal_GetAbsoluteMousePosition_Injected(out Vector2 ret);

		// Token: 0x0600015F RID: 351
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void GetMatrix_Injected(out Matrix4x4 ret);

		// Token: 0x06000160 RID: 352
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void SetMatrix_Injected(ref Matrix4x4 m);

		// Token: 0x06000161 RID: 353
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void GetParentMatrix_Injected(out Matrix4x4 ret);

		// Token: 0x06000162 RID: 354
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void Internal_PushParentClip_Injected(ref Matrix4x4 objectTransform, ref Rect clipRect);

		// Token: 0x02000010 RID: 16
		[VisibleToOtherModules(new string[]
		{
			"UnityEngine.UIElementsModule"
		})]
		internal struct ParentClipScope : IDisposable
		{
			// Token: 0x06000163 RID: 355 RVA: 0x0000690E File Offset: 0x00004B0E
			public ParentClipScope(Matrix4x4 objectTransform, Rect clipRect)
			{
				this.m_Disposed = false;
				GUIClip.Internal_PushParentClip(objectTransform, clipRect);
			}

			// Token: 0x06000164 RID: 356 RVA: 0x0000691F File Offset: 0x00004B1F
			public void Dispose()
			{
				if (!this.m_Disposed)
				{
					this.m_Disposed = true;
					GUIClip.Internal_PopParentClip();
				}
			}

			// Token: 0x0400005B RID: 91
			private bool m_Disposed;
		}
	}
}
