﻿using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000029 RID: 41
	[NativeHeader("Runtime/Utilities/CopyPaste.h")]
	[NativeHeader("Runtime/Input/InputManager.h")]
	[NativeHeader("Modules/IMGUI/GUIManager.h")]
	[NativeHeader("Modules/IMGUI/GUIUtility.h")]
	[NativeHeader("Runtime/Camera/RenderLayers/GUITexture.h")]
	public class GUIUtility
	{
		// Token: 0x0600032D RID: 813 RVA: 0x0000338C File Offset: 0x0000158C
		public GUIUtility()
		{
		}

		// Token: 0x17000078 RID: 120
		// (get) Token: 0x0600032E RID: 814
		public static extern bool hasModalWindow { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000079 RID: 121
		// (get) Token: 0x0600032F RID: 815
		[NativeProperty("GetGUIState().m_PixelsPerPoint", true, TargetType.Field)]
		internal static extern float pixelsPerPoint { [VisibleToOtherModules(new string[]
		{
			"UnityEngine.UIElementsModule"
		})] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700007A RID: 122
		// (get) Token: 0x06000330 RID: 816
		[NativeProperty("GetGUIState().m_OnGUIDepth", true, TargetType.Field)]
		internal static extern int guiDepth { [VisibleToOtherModules(new string[]
		{
			"UnityEngine.UIElementsModule"
		})] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700007B RID: 123
		// (get) Token: 0x06000331 RID: 817 RVA: 0x0000AE44 File Offset: 0x00009044
		// (set) Token: 0x06000332 RID: 818 RVA: 0x0000AE59 File Offset: 0x00009059
		internal static Vector2 s_EditorScreenPointOffset
		{
			[NativeMethod("GetGUIManager().GetGUIPixelOffset", true)]
			get
			{
				Vector2 result;
				GUIUtility.get_s_EditorScreenPointOffset_Injected(out result);
				return result;
			}
			[NativeMethod("GetGUIManager().SetGUIPixelOffset", true)]
			set
			{
				GUIUtility.set_s_EditorScreenPointOffset_Injected(ref value);
			}
		}

		// Token: 0x1700007C RID: 124
		// (get) Token: 0x06000333 RID: 819
		// (set) Token: 0x06000334 RID: 820
		[NativeProperty("GetGUIState().m_CanvasGUIState.m_IsMouseUsed", true, TargetType.Field)]
		internal static extern bool mouseUsed { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700007D RID: 125
		// (get) Token: 0x06000335 RID: 821
		// (set) Token: 0x06000336 RID: 822
		[StaticAccessor("GetInputManager()", StaticAccessorType.Dot)]
		internal static extern bool textFieldInput { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700007E RID: 126
		// (get) Token: 0x06000337 RID: 823
		// (set) Token: 0x06000338 RID: 824
		internal static extern bool manualTex2SRGBEnabled { [FreeFunction("GUITexture::IsManualTex2SRGBEnabled")] [VisibleToOtherModules(new string[]
		{
			"UnityEngine.UIElementsModule"
		})] [MethodImpl(MethodImplOptions.InternalCall)] get; [VisibleToOtherModules(new string[]
		{
			"UnityEngine.UIElementsModule"
		})] [FreeFunction("GUITexture::SetManualTex2SRGBEnabled")] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700007F RID: 127
		// (get) Token: 0x06000339 RID: 825
		// (set) Token: 0x0600033A RID: 826
		public static extern string systemCopyBuffer { [FreeFunction("GetCopyBuffer")] [MethodImpl(MethodImplOptions.InternalCall)] get; [FreeFunction("SetCopyBuffer")] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x0600033B RID: 827 RVA: 0x0000AE62 File Offset: 0x00009062
		[StaticAccessor("GetGUIState()", StaticAccessorType.Dot)]
		public static int GetControlID(int hint, FocusType focusType, Rect rect)
		{
			return GUIUtility.GetControlID_Injected(hint, focusType, ref rect);
		}

		// Token: 0x0600033C RID: 828
		[VisibleToOtherModules(new string[]
		{
			"UnityEngine.UIElementsModule"
		})]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern void BeginContainerFromOwner(ScriptableObject owner);

		// Token: 0x0600033D RID: 829
		[VisibleToOtherModules(new string[]
		{
			"UnityEngine.UIElementsModule"
		})]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern void BeginContainer(ObjectGUIState objectGUIState);

		// Token: 0x0600033E RID: 830
		[NativeMethod("EndContainer")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern void Internal_EndContainer();

		// Token: 0x0600033F RID: 831
		[FreeFunction("GetSpecificGUIState(0).m_EternalGUIState->GetNextUniqueID")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern int GetPermanentControlID();

		// Token: 0x06000340 RID: 832
		[VisibleToOtherModules(new string[]
		{
			"UnityEngine.UIElementsModule"
		})]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern int CheckForTabEvent(Event evt);

		// Token: 0x06000341 RID: 833
		[VisibleToOtherModules(new string[]
		{
			"UnityEngine.UIElementsModule"
		})]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern void SetKeyboardControlToFirstControlId();

		// Token: 0x06000342 RID: 834
		[VisibleToOtherModules(new string[]
		{
			"UnityEngine.UIElementsModule"
		})]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern void SetKeyboardControlToLastControlId();

		// Token: 0x06000343 RID: 835
		[VisibleToOtherModules(new string[]
		{
			"UnityEngine.UIElementsModule"
		})]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern bool HasFocusableControls();

		// Token: 0x06000344 RID: 836 RVA: 0x0000AE70 File Offset: 0x00009070
		public static Rect AlignRectToDevice(Rect rect, out int widthInPixels, out int heightInPixels)
		{
			Rect result;
			GUIUtility.AlignRectToDevice_Injected(ref rect, out widthInPixels, out heightInPixels, out result);
			return result;
		}

		// Token: 0x06000345 RID: 837 RVA: 0x0000AE8C File Offset: 0x0000908C
		[VisibleToOtherModules(new string[]
		{
			"UnityEngine.UIElementsModule"
		})]
		internal static Vector3 Internal_MultiplyPoint(Vector3 point, Matrix4x4 transform)
		{
			Vector3 result;
			GUIUtility.Internal_MultiplyPoint_Injected(ref point, ref transform, out result);
			return result;
		}

		// Token: 0x06000346 RID: 838
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern bool GetChanged();

		// Token: 0x06000347 RID: 839
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern void SetChanged(bool changed);

		// Token: 0x06000348 RID: 840
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern void SetDidGUIWindowsEatLastEvent(bool value);

		// Token: 0x06000349 RID: 841
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int Internal_GetHotControl();

		// Token: 0x0600034A RID: 842
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int Internal_GetKeyboardControl();

		// Token: 0x0600034B RID: 843
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void Internal_SetHotControl(int value);

		// Token: 0x0600034C RID: 844
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void Internal_SetKeyboardControl(int value);

		// Token: 0x0600034D RID: 845
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern object Internal_GetDefaultSkin(int skinMode);

		// Token: 0x0600034E RID: 846
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern Object Internal_GetBuiltinSkin(int skin);

		// Token: 0x0600034F RID: 847
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void Internal_ExitGUI();

		// Token: 0x06000350 RID: 848 RVA: 0x0000AEA8 File Offset: 0x000090A8
		private static Vector2 InternalWindowToScreenPoint(Vector2 windowPoint)
		{
			Vector2 result;
			GUIUtility.InternalWindowToScreenPoint_Injected(ref windowPoint, out result);
			return result;
		}

		// Token: 0x06000351 RID: 849 RVA: 0x0000AEC0 File Offset: 0x000090C0
		private static Vector2 InternalScreenToWindowPoint(Vector2 screenPoint)
		{
			Vector2 result;
			GUIUtility.InternalScreenToWindowPoint_Injected(ref screenPoint, out result);
			return result;
		}

		// Token: 0x06000352 RID: 850 RVA: 0x0000AED7 File Offset: 0x000090D7
		[RequiredByNativeCode]
		private static void MarkGUIChanged()
		{
			if (GUIUtility.enabledStateChanged != null)
			{
				GUIUtility.enabledStateChanged();
			}
		}

		// Token: 0x06000353 RID: 851 RVA: 0x0000AEF0 File Offset: 0x000090F0
		public static int GetControlID(FocusType focus)
		{
			return GUIUtility.GetControlID(0, focus);
		}

		// Token: 0x06000354 RID: 852 RVA: 0x0000AF0C File Offset: 0x0000910C
		public static int GetControlID(GUIContent contents, FocusType focus)
		{
			return GUIUtility.GetControlID(contents.hash, focus);
		}

		// Token: 0x06000355 RID: 853 RVA: 0x0000AF30 File Offset: 0x00009130
		public static int GetControlID(FocusType focus, Rect position)
		{
			return GUIUtility.GetControlID(0, focus, position);
		}

		// Token: 0x06000356 RID: 854 RVA: 0x0000AF50 File Offset: 0x00009150
		public static int GetControlID(GUIContent contents, FocusType focus, Rect position)
		{
			return GUIUtility.GetControlID(contents.hash, focus, position);
		}

		// Token: 0x06000357 RID: 855 RVA: 0x0000AF74 File Offset: 0x00009174
		public static int GetControlID(int hint, FocusType focus)
		{
			return GUIUtility.GetControlID(hint, focus, Rect.zero);
		}

		// Token: 0x06000358 RID: 856 RVA: 0x0000AF98 File Offset: 0x00009198
		public static object GetStateObject(Type t, int controlID)
		{
			return GUIStateObjects.GetStateObject(t, controlID);
		}

		// Token: 0x06000359 RID: 857 RVA: 0x0000AFB4 File Offset: 0x000091B4
		public static object QueryStateObject(Type t, int controlID)
		{
			return GUIStateObjects.QueryStateObject(t, controlID);
		}

		// Token: 0x17000080 RID: 128
		// (get) Token: 0x0600035A RID: 858 RVA: 0x0000AFD0 File Offset: 0x000091D0
		// (set) Token: 0x0600035B RID: 859 RVA: 0x0000AFE9 File Offset: 0x000091E9
		internal static bool guiIsExiting
		{
			[CompilerGenerated]
			get
			{
				return GUIUtility.<guiIsExiting>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				GUIUtility.<guiIsExiting>k__BackingField = value;
			}
		}

		// Token: 0x17000081 RID: 129
		// (get) Token: 0x0600035C RID: 860 RVA: 0x0000AFF4 File Offset: 0x000091F4
		// (set) Token: 0x0600035D RID: 861 RVA: 0x0000B00E File Offset: 0x0000920E
		public static int hotControl
		{
			get
			{
				return GUIUtility.Internal_GetHotControl();
			}
			set
			{
				GUIUtility.Internal_SetHotControl(value);
			}
		}

		// Token: 0x0600035E RID: 862 RVA: 0x0000B017 File Offset: 0x00009217
		[RequiredByNativeCode]
		internal static void TakeCapture()
		{
			if (GUIUtility.takeCapture != null)
			{
				GUIUtility.takeCapture();
			}
		}

		// Token: 0x0600035F RID: 863 RVA: 0x0000B030 File Offset: 0x00009230
		[RequiredByNativeCode]
		internal static void RemoveCapture()
		{
			if (GUIUtility.releaseCapture != null)
			{
				GUIUtility.releaseCapture();
			}
		}

		// Token: 0x17000082 RID: 130
		// (get) Token: 0x06000360 RID: 864 RVA: 0x0000B04C File Offset: 0x0000924C
		// (set) Token: 0x06000361 RID: 865 RVA: 0x0000B066 File Offset: 0x00009266
		public static int keyboardControl
		{
			get
			{
				return GUIUtility.Internal_GetKeyboardControl();
			}
			set
			{
				GUIUtility.Internal_SetKeyboardControl(value);
			}
		}

		// Token: 0x06000362 RID: 866 RVA: 0x0000B06F File Offset: 0x0000926F
		public static void ExitGUI()
		{
			GUIUtility.guiIsExiting = true;
			throw new ExitGUIException();
		}

		// Token: 0x06000363 RID: 867 RVA: 0x0000B080 File Offset: 0x00009280
		internal static GUISkin GetDefaultSkin(int skinMode)
		{
			return GUIUtility.Internal_GetDefaultSkin(skinMode) as GUISkin;
		}

		// Token: 0x06000364 RID: 868 RVA: 0x0000B0A0 File Offset: 0x000092A0
		internal static GUISkin GetDefaultSkin()
		{
			return GUIUtility.Internal_GetDefaultSkin(GUIUtility.s_SkinMode) as GUISkin;
		}

		// Token: 0x06000365 RID: 869 RVA: 0x0000B0C4 File Offset: 0x000092C4
		internal static GUISkin GetBuiltinSkin(int skin)
		{
			return GUIUtility.Internal_GetBuiltinSkin(skin) as GUISkin;
		}

		// Token: 0x06000366 RID: 870 RVA: 0x0000B0E4 File Offset: 0x000092E4
		[RequiredByNativeCode]
		internal static bool ProcessEvent(int instanceID, IntPtr nativeEventPtr)
		{
			return GUIUtility.processEvent != null && GUIUtility.processEvent(instanceID, nativeEventPtr);
		}

		// Token: 0x06000367 RID: 871 RVA: 0x0000B116 File Offset: 0x00009316
		[VisibleToOtherModules(new string[]
		{
			"UnityEngine.UIElementsModule"
		})]
		internal static void EndContainer()
		{
			GUIUtility.Internal_EndContainer();
			GUIUtility.Internal_ExitGUI();
		}

		// Token: 0x06000368 RID: 872 RVA: 0x0000B123 File Offset: 0x00009323
		internal static void CleanupRoots()
		{
			if (GUIUtility.cleanupRoots != null)
			{
				GUIUtility.cleanupRoots();
			}
		}

		// Token: 0x06000369 RID: 873 RVA: 0x0000B13C File Offset: 0x0000933C
		[RequiredByNativeCode]
		internal static void BeginGUI(int skinMode, int instanceID, int useGUILayout)
		{
			GUIUtility.s_SkinMode = skinMode;
			GUIUtility.s_OriginalID = instanceID;
			GUIUtility.ResetGlobalState();
			if (useGUILayout != 0)
			{
				GUILayoutUtility.Begin(instanceID);
			}
		}

		// Token: 0x0600036A RID: 874 RVA: 0x0000B160 File Offset: 0x00009360
		[RequiredByNativeCode]
		internal static void EndGUI(int layoutType)
		{
			try
			{
				if (Event.current.type == EventType.Layout)
				{
					if (layoutType != 0)
					{
						if (layoutType != 1)
						{
							if (layoutType == 2)
							{
								GUILayoutUtility.LayoutFromEditorWindow();
							}
						}
						else
						{
							GUILayoutUtility.Layout();
						}
					}
				}
				GUILayoutUtility.SelectIDList(GUIUtility.s_OriginalID, false);
				GUIContent.ClearStaticCache();
			}
			finally
			{
				GUIUtility.Internal_ExitGUI();
			}
		}

		// Token: 0x0600036B RID: 875 RVA: 0x0000B1E4 File Offset: 0x000093E4
		[RequiredByNativeCode]
		internal static bool EndGUIFromException(Exception exception)
		{
			GUIUtility.Internal_ExitGUI();
			return GUIUtility.ShouldRethrowException(exception);
		}

		// Token: 0x0600036C RID: 876 RVA: 0x0000B204 File Offset: 0x00009404
		[RequiredByNativeCode]
		internal static bool EndContainerGUIFromException(Exception exception)
		{
			return GUIUtility.endContainerGUIFromException != null && GUIUtility.endContainerGUIFromException(exception);
		}

		// Token: 0x0600036D RID: 877 RVA: 0x0000B235 File Offset: 0x00009435
		[VisibleToOtherModules(new string[]
		{
			"UnityEngine.UIElementsModule"
		})]
		internal static void ResetGlobalState()
		{
			GUI.skin = null;
			GUIUtility.guiIsExiting = false;
			GUI.changed = false;
		}

		// Token: 0x0600036E RID: 878 RVA: 0x0000B24C File Offset: 0x0000944C
		[VisibleToOtherModules(new string[]
		{
			"UnityEngine.UIElementsModule"
		})]
		internal static bool IsExitGUIException(Exception exception)
		{
			while (exception is TargetInvocationException && exception.InnerException != null)
			{
				exception = exception.InnerException;
			}
			return exception is ExitGUIException;
		}

		// Token: 0x0600036F RID: 879 RVA: 0x0000B290 File Offset: 0x00009490
		[VisibleToOtherModules(new string[]
		{
			"UnityEngine.UIElementsModule"
		})]
		internal static bool ShouldRethrowException(Exception exception)
		{
			return GUIUtility.IsExitGUIException(exception);
		}

		// Token: 0x06000370 RID: 880 RVA: 0x0000B2AB File Offset: 0x000094AB
		internal static void CheckOnGUI()
		{
			if (GUIUtility.guiDepth <= 0)
			{
				throw new ArgumentException("You can only call GUI functions from inside OnGUI.");
			}
		}

		// Token: 0x06000371 RID: 881 RVA: 0x0000B2C4 File Offset: 0x000094C4
		public static Vector2 GUIToScreenPoint(Vector2 guiPoint)
		{
			return GUIUtility.InternalWindowToScreenPoint(GUIClip.UnclipToWindow(guiPoint));
		}

		// Token: 0x06000372 RID: 882 RVA: 0x0000B2E4 File Offset: 0x000094E4
		internal static Rect GUIToScreenRect(Rect guiRect)
		{
			Vector2 vector = GUIUtility.GUIToScreenPoint(new Vector2(guiRect.x, guiRect.y));
			guiRect.x = vector.x;
			guiRect.y = vector.y;
			return guiRect;
		}

		// Token: 0x06000373 RID: 883 RVA: 0x0000B330 File Offset: 0x00009530
		public static Vector2 ScreenToGUIPoint(Vector2 screenPoint)
		{
			return GUIClip.ClipToWindow(GUIUtility.InternalScreenToWindowPoint(screenPoint));
		}

		// Token: 0x06000374 RID: 884 RVA: 0x0000B350 File Offset: 0x00009550
		public static Rect ScreenToGUIRect(Rect screenRect)
		{
			Vector2 vector = GUIUtility.ScreenToGUIPoint(new Vector2(screenRect.x, screenRect.y));
			screenRect.x = vector.x;
			screenRect.y = vector.y;
			return screenRect;
		}

		// Token: 0x06000375 RID: 885 RVA: 0x0000B39C File Offset: 0x0000959C
		public static void RotateAroundPivot(float angle, Vector2 pivotPoint)
		{
			Matrix4x4 matrix = GUI.matrix;
			GUI.matrix = Matrix4x4.identity;
			Vector2 vector = GUIClip.Unclip(pivotPoint);
			Matrix4x4 lhs = Matrix4x4.TRS(vector, Quaternion.Euler(0f, 0f, angle), Vector3.one) * Matrix4x4.TRS(-vector, Quaternion.identity, Vector3.one);
			GUI.matrix = lhs * matrix;
		}

		// Token: 0x06000376 RID: 886 RVA: 0x0000B410 File Offset: 0x00009610
		public static void ScaleAroundPivot(Vector2 scale, Vector2 pivotPoint)
		{
			Matrix4x4 matrix = GUI.matrix;
			Vector2 vector = GUIClip.Unclip(pivotPoint);
			Matrix4x4 lhs = Matrix4x4.TRS(vector, Quaternion.identity, new Vector3(scale.x, scale.y, 1f)) * Matrix4x4.TRS(-vector, Quaternion.identity, Vector3.one);
			GUI.matrix = lhs * matrix;
		}

		// Token: 0x06000377 RID: 887 RVA: 0x0000B480 File Offset: 0x00009680
		public static Rect AlignRectToDevice(Rect rect)
		{
			int num;
			int num2;
			return GUIUtility.AlignRectToDevice(rect, out num, out num2);
		}

		// Token: 0x06000378 RID: 888
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void get_s_EditorScreenPointOffset_Injected(out Vector2 ret);

		// Token: 0x06000379 RID: 889
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void set_s_EditorScreenPointOffset_Injected(ref Vector2 value);

		// Token: 0x0600037A RID: 890
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int GetControlID_Injected(int hint, FocusType focusType, ref Rect rect);

		// Token: 0x0600037B RID: 891
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void AlignRectToDevice_Injected(ref Rect rect, out int widthInPixels, out int heightInPixels, out Rect ret);

		// Token: 0x0600037C RID: 892
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void Internal_MultiplyPoint_Injected(ref Vector3 point, ref Matrix4x4 transform, out Vector3 ret);

		// Token: 0x0600037D RID: 893
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void InternalWindowToScreenPoint_Injected(ref Vector2 windowPoint, out Vector2 ret);

		// Token: 0x0600037E RID: 894
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void InternalScreenToWindowPoint_Injected(ref Vector2 screenPoint, out Vector2 ret);

		// Token: 0x040000C9 RID: 201
		[VisibleToOtherModules(new string[]
		{
			"UnityEngine.UIElementsModule"
		})]
		internal static int s_SkinMode;

		// Token: 0x040000CA RID: 202
		[VisibleToOtherModules(new string[]
		{
			"UnityEngine.UIElementsModule"
		})]
		internal static int s_OriginalID;

		// Token: 0x040000CB RID: 203
		[VisibleToOtherModules(new string[]
		{
			"UnityEngine.UIElementsModule"
		})]
		internal static Action takeCapture;

		// Token: 0x040000CC RID: 204
		[VisibleToOtherModules(new string[]
		{
			"UnityEngine.UIElementsModule"
		})]
		internal static Action releaseCapture;

		// Token: 0x040000CD RID: 205
		[VisibleToOtherModules(new string[]
		{
			"UnityEngine.UIElementsModule"
		})]
		internal static Func<int, IntPtr, bool> processEvent;

		// Token: 0x040000CE RID: 206
		[VisibleToOtherModules(new string[]
		{
			"UnityEngine.UIElementsModule"
		})]
		internal static Action cleanupRoots;

		// Token: 0x040000CF RID: 207
		[VisibleToOtherModules(new string[]
		{
			"UnityEngine.UIElementsModule"
		})]
		internal static Func<Exception, bool> endContainerGUIFromException;

		// Token: 0x040000D0 RID: 208
		[VisibleToOtherModules(new string[]
		{
			"UnityEngine.UIElementsModule"
		})]
		internal static Action enabledStateChanged;

		// Token: 0x040000D1 RID: 209
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private static bool <guiIsExiting>k__BackingField;
	}
}
