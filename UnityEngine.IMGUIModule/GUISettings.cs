﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;

namespace UnityEngine
{
	// Token: 0x0200001F RID: 31
	[NativeHeader("Modules/IMGUI/GUISkin.bindings.h")]
	[Serializable]
	public sealed class GUISettings
	{
		// Token: 0x06000252 RID: 594 RVA: 0x00009498 File Offset: 0x00007698
		public GUISettings()
		{
		}

		// Token: 0x06000253 RID: 595
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern float Internal_GetCursorFlashSpeed();

		// Token: 0x1700003A RID: 58
		// (get) Token: 0x06000254 RID: 596 RVA: 0x000094EC File Offset: 0x000076EC
		// (set) Token: 0x06000255 RID: 597 RVA: 0x00009507 File Offset: 0x00007707
		public bool doubleClickSelectsWord
		{
			get
			{
				return this.m_DoubleClickSelectsWord;
			}
			set
			{
				this.m_DoubleClickSelectsWord = value;
			}
		}

		// Token: 0x1700003B RID: 59
		// (get) Token: 0x06000256 RID: 598 RVA: 0x00009514 File Offset: 0x00007714
		// (set) Token: 0x06000257 RID: 599 RVA: 0x0000952F File Offset: 0x0000772F
		public bool tripleClickSelectsLine
		{
			get
			{
				return this.m_TripleClickSelectsLine;
			}
			set
			{
				this.m_TripleClickSelectsLine = value;
			}
		}

		// Token: 0x1700003C RID: 60
		// (get) Token: 0x06000258 RID: 600 RVA: 0x0000953C File Offset: 0x0000773C
		// (set) Token: 0x06000259 RID: 601 RVA: 0x00009557 File Offset: 0x00007757
		public Color cursorColor
		{
			get
			{
				return this.m_CursorColor;
			}
			set
			{
				this.m_CursorColor = value;
			}
		}

		// Token: 0x1700003D RID: 61
		// (get) Token: 0x0600025A RID: 602 RVA: 0x00009564 File Offset: 0x00007764
		// (set) Token: 0x0600025B RID: 603 RVA: 0x0000959B File Offset: 0x0000779B
		public float cursorFlashSpeed
		{
			get
			{
				float result;
				if (this.m_CursorFlashSpeed >= 0f)
				{
					result = this.m_CursorFlashSpeed;
				}
				else
				{
					result = GUISettings.Internal_GetCursorFlashSpeed();
				}
				return result;
			}
			set
			{
				this.m_CursorFlashSpeed = value;
			}
		}

		// Token: 0x1700003E RID: 62
		// (get) Token: 0x0600025C RID: 604 RVA: 0x000095A8 File Offset: 0x000077A8
		// (set) Token: 0x0600025D RID: 605 RVA: 0x000095C3 File Offset: 0x000077C3
		public Color selectionColor
		{
			get
			{
				return this.m_SelectionColor;
			}
			set
			{
				this.m_SelectionColor = value;
			}
		}

		// Token: 0x0400008A RID: 138
		[SerializeField]
		private bool m_DoubleClickSelectsWord = true;

		// Token: 0x0400008B RID: 139
		[SerializeField]
		private bool m_TripleClickSelectsLine = true;

		// Token: 0x0400008C RID: 140
		[SerializeField]
		private Color m_CursorColor = Color.white;

		// Token: 0x0400008D RID: 141
		[SerializeField]
		private float m_CursorFlashSpeed = -1f;

		// Token: 0x0400008E RID: 142
		[SerializeField]
		private Color m_SelectionColor = new Color(0.5f, 0.5f, 1f);
	}
}
