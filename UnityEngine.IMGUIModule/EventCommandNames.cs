﻿using System;

namespace UnityEngine
{
	// Token: 0x02000003 RID: 3
	internal static class EventCommandNames
	{
		// Token: 0x04000005 RID: 5
		public const string Cut = "Cut";

		// Token: 0x04000006 RID: 6
		public const string Copy = "Copy";

		// Token: 0x04000007 RID: 7
		public const string Paste = "Paste";

		// Token: 0x04000008 RID: 8
		public const string SelectAll = "SelectAll";

		// Token: 0x04000009 RID: 9
		public const string Duplicate = "Duplicate";

		// Token: 0x0400000A RID: 10
		public const string Delete = "Delete";

		// Token: 0x0400000B RID: 11
		public const string SoftDelete = "SoftDelete";

		// Token: 0x0400000C RID: 12
		public const string Find = "Find";

		// Token: 0x0400000D RID: 13
		public const string UndoRedoPerformed = "UndoRedoPerformed";

		// Token: 0x0400000E RID: 14
		public const string OnLostFocus = "OnLostFocus";

		// Token: 0x0400000F RID: 15
		public const string NewKeyboardFocus = "NewKeyboardFocus";

		// Token: 0x04000010 RID: 16
		public const string ModifierKeysChanged = "ModifierKeysChanged";

		// Token: 0x04000011 RID: 17
		public const string EyeDropperUpdate = "EyeDropperUpdate";

		// Token: 0x04000012 RID: 18
		public const string EyeDropperClicked = "EyeDropperClicked";

		// Token: 0x04000013 RID: 19
		public const string EyeDropperCancelled = "EyeDropperCancelled";

		// Token: 0x04000014 RID: 20
		public const string ColorPickerChanged = "ColorPickerChanged";

		// Token: 0x04000015 RID: 21
		public const string FrameSelected = "FrameSelected";

		// Token: 0x04000016 RID: 22
		public const string FrameSelectedWithLock = "FrameSelectedWithLock";
	}
}
