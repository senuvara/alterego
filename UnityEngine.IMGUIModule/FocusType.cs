﻿using System;

namespace UnityEngine
{
	// Token: 0x02000014 RID: 20
	public enum FocusType
	{
		// Token: 0x04000068 RID: 104
		[Obsolete("FocusType.Native now behaves the same as FocusType.Passive in all OS cases. (UnityUpgradable) -> Passive", false)]
		Native,
		// Token: 0x04000069 RID: 105
		Keyboard,
		// Token: 0x0400006A RID: 106
		Passive
	}
}
