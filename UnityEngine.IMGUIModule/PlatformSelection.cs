﻿using System;

namespace UnityEngine
{
	// Token: 0x02000020 RID: 32
	internal enum PlatformSelection
	{
		// Token: 0x04000090 RID: 144
		Native,
		// Token: 0x04000091 RID: 145
		Mac,
		// Token: 0x04000092 RID: 146
		Windows
	}
}
