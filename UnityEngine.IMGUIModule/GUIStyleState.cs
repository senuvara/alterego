﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine.Bindings;

namespace UnityEngine
{
	// Token: 0x02000024 RID: 36
	[NativeHeader("Modules/IMGUI/GUIStyle.bindings.h")]
	[Serializable]
	[StructLayout(LayoutKind.Sequential)]
	public sealed class GUIStyleState
	{
		// Token: 0x0600029E RID: 670 RVA: 0x0000A1A7 File Offset: 0x000083A7
		public GUIStyleState()
		{
			this.m_Ptr = GUIStyleState.Init();
		}

		// Token: 0x0600029F RID: 671 RVA: 0x0000A1BB File Offset: 0x000083BB
		private GUIStyleState(GUIStyle sourceStyle, IntPtr source)
		{
			this.m_SourceStyle = sourceStyle;
			this.m_Ptr = source;
		}

		// Token: 0x17000057 RID: 87
		// (get) Token: 0x060002A0 RID: 672
		// (set) Token: 0x060002A1 RID: 673
		[NativeProperty("Background", false, TargetType.Function)]
		public extern Texture2D background { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000058 RID: 88
		// (get) Token: 0x060002A2 RID: 674 RVA: 0x0000A1D4 File Offset: 0x000083D4
		// (set) Token: 0x060002A3 RID: 675 RVA: 0x0000A1EA File Offset: 0x000083EA
		[NativeProperty("textColor", false, TargetType.Field)]
		public Color textColor
		{
			get
			{
				Color result;
				this.get_textColor_Injected(out result);
				return result;
			}
			set
			{
				this.set_textColor_Injected(ref value);
			}
		}

		// Token: 0x060002A4 RID: 676
		[FreeFunction(Name = "GUIStyleState_Bindings::Init", IsThreadSafe = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern IntPtr Init();

		// Token: 0x060002A5 RID: 677
		[FreeFunction(Name = "GUIStyleState_Bindings::Cleanup", IsThreadSafe = true, HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void Cleanup();

		// Token: 0x060002A6 RID: 678 RVA: 0x0000A1F4 File Offset: 0x000083F4
		internal static GUIStyleState ProduceGUIStyleStateFromDeserialization(GUIStyle sourceStyle, IntPtr source)
		{
			return new GUIStyleState(sourceStyle, source);
		}

		// Token: 0x060002A7 RID: 679 RVA: 0x0000A214 File Offset: 0x00008414
		internal static GUIStyleState GetGUIStyleState(GUIStyle sourceStyle, IntPtr source)
		{
			return new GUIStyleState(sourceStyle, source);
		}

		// Token: 0x060002A8 RID: 680 RVA: 0x0000A234 File Offset: 0x00008434
		~GUIStyleState()
		{
			if (this.m_SourceStyle == null)
			{
				this.Cleanup();
				this.m_Ptr = IntPtr.Zero;
			}
		}

		// Token: 0x060002A9 RID: 681
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_textColor_Injected(out Color ret);

		// Token: 0x060002AA RID: 682
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_textColor_Injected(ref Color value);

		// Token: 0x040000AF RID: 175
		[NonSerialized]
		internal IntPtr m_Ptr;

		// Token: 0x040000B0 RID: 176
		private readonly GUIStyle m_SourceStyle;
	}
}
