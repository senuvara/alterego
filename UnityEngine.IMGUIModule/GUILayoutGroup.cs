﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;

namespace UnityEngine
{
	// Token: 0x0200002F RID: 47
	[VisibleToOtherModules(new string[]
	{
		"UnityEngine.UIElementsModule"
	})]
	internal class GUILayoutGroup : GUILayoutEntry
	{
		// Token: 0x0600039A RID: 922 RVA: 0x0000C164 File Offset: 0x0000A364
		public GUILayoutGroup() : base(0f, 0f, 0f, 0f, GUIStyle.none)
		{
		}

		// Token: 0x0600039B RID: 923 RVA: 0x0000C21C File Offset: 0x0000A41C
		public GUILayoutGroup(GUIStyle _style, GUILayoutOption[] options) : base(0f, 0f, 0f, 0f, _style)
		{
			if (options != null)
			{
				this.ApplyOptions(options);
			}
			this.m_MarginLeft = _style.margin.left;
			this.m_MarginRight = _style.margin.right;
			this.m_MarginTop = _style.margin.top;
			this.m_MarginBottom = _style.margin.bottom;
		}

		// Token: 0x1700008B RID: 139
		// (get) Token: 0x0600039C RID: 924 RVA: 0x0000C320 File Offset: 0x0000A520
		public override int marginLeft
		{
			[CompilerGenerated]
			get
			{
				return this.m_MarginLeft;
			}
		}

		// Token: 0x1700008C RID: 140
		// (get) Token: 0x0600039D RID: 925 RVA: 0x0000C33C File Offset: 0x0000A53C
		public override int marginRight
		{
			[CompilerGenerated]
			get
			{
				return this.m_MarginRight;
			}
		}

		// Token: 0x1700008D RID: 141
		// (get) Token: 0x0600039E RID: 926 RVA: 0x0000C358 File Offset: 0x0000A558
		public override int marginTop
		{
			[CompilerGenerated]
			get
			{
				return this.m_MarginTop;
			}
		}

		// Token: 0x1700008E RID: 142
		// (get) Token: 0x0600039F RID: 927 RVA: 0x0000C374 File Offset: 0x0000A574
		public override int marginBottom
		{
			[CompilerGenerated]
			get
			{
				return this.m_MarginBottom;
			}
		}

		// Token: 0x060003A0 RID: 928 RVA: 0x0000C390 File Offset: 0x0000A590
		public override void ApplyOptions(GUILayoutOption[] options)
		{
			if (options != null)
			{
				base.ApplyOptions(options);
				foreach (GUILayoutOption guilayoutOption in options)
				{
					switch (guilayoutOption.type)
					{
					case GUILayoutOption.Type.fixedWidth:
					case GUILayoutOption.Type.minWidth:
					case GUILayoutOption.Type.maxWidth:
						this.m_UserSpecifiedHeight = true;
						break;
					case GUILayoutOption.Type.fixedHeight:
					case GUILayoutOption.Type.minHeight:
					case GUILayoutOption.Type.maxHeight:
						this.m_UserSpecifiedWidth = true;
						break;
					case GUILayoutOption.Type.spacing:
						this.spacing = (float)((int)guilayoutOption.value);
						break;
					}
				}
			}
		}

		// Token: 0x060003A1 RID: 929 RVA: 0x0000C448 File Offset: 0x0000A648
		protected override void ApplyStyleSettings(GUIStyle style)
		{
			base.ApplyStyleSettings(style);
			RectOffset margin = style.margin;
			this.m_MarginLeft = margin.left;
			this.m_MarginRight = margin.right;
			this.m_MarginTop = margin.top;
			this.m_MarginBottom = margin.bottom;
		}

		// Token: 0x060003A2 RID: 930 RVA: 0x0000C494 File Offset: 0x0000A694
		public void ResetCursor()
		{
			this.m_Cursor = 0;
		}

		// Token: 0x060003A3 RID: 931 RVA: 0x0000C4A0 File Offset: 0x0000A6A0
		public Rect PeekNext()
		{
			if (this.m_Cursor < this.entries.Count)
			{
				GUILayoutEntry guilayoutEntry = this.entries[this.m_Cursor];
				return guilayoutEntry.rect;
			}
			throw new ArgumentException(string.Concat(new object[]
			{
				"Getting control ",
				this.m_Cursor,
				"'s position in a group with only ",
				this.entries.Count,
				" controls when doing ",
				Event.current.rawType,
				"\nAborting"
			}));
		}

		// Token: 0x060003A4 RID: 932 RVA: 0x0000C548 File Offset: 0x0000A748
		public GUILayoutEntry GetNext()
		{
			if (this.m_Cursor < this.entries.Count)
			{
				GUILayoutEntry result = this.entries[this.m_Cursor];
				this.m_Cursor++;
				return result;
			}
			throw new ArgumentException(string.Concat(new object[]
			{
				"Getting control ",
				this.m_Cursor,
				"'s position in a group with only ",
				this.entries.Count,
				" controls when doing ",
				Event.current.rawType,
				"\nAborting"
			}));
		}

		// Token: 0x060003A5 RID: 933 RVA: 0x0000C5FC File Offset: 0x0000A7FC
		public Rect GetLast()
		{
			Rect result;
			if (this.m_Cursor == 0)
			{
				Debug.LogError("You cannot call GetLast immediately after beginning a group.");
				result = GUILayoutEntry.kDummyRect;
			}
			else if (this.m_Cursor <= this.entries.Count)
			{
				GUILayoutEntry guilayoutEntry = this.entries[this.m_Cursor - 1];
				result = guilayoutEntry.rect;
			}
			else
			{
				Debug.LogError(string.Concat(new object[]
				{
					"Getting control ",
					this.m_Cursor,
					"'s position in a group with only ",
					this.entries.Count,
					" controls when doing ",
					Event.current.type
				}));
				result = GUILayoutEntry.kDummyRect;
			}
			return result;
		}

		// Token: 0x060003A6 RID: 934 RVA: 0x0000C6C9 File Offset: 0x0000A8C9
		public void Add(GUILayoutEntry e)
		{
			this.entries.Add(e);
		}

		// Token: 0x060003A7 RID: 935 RVA: 0x0000C6D8 File Offset: 0x0000A8D8
		public override void CalcWidth()
		{
			if (this.entries.Count == 0)
			{
				this.maxWidth = (this.minWidth = (float)base.style.padding.horizontal);
			}
			else
			{
				int num = 0;
				int num2 = 0;
				this.m_ChildMinWidth = 0f;
				this.m_ChildMaxWidth = 0f;
				this.m_StretchableCountX = 0;
				bool flag = true;
				if (this.isVertical)
				{
					foreach (GUILayoutEntry guilayoutEntry in this.entries)
					{
						guilayoutEntry.CalcWidth();
						if (guilayoutEntry.consideredForMargin)
						{
							if (!flag)
							{
								num = Mathf.Min(guilayoutEntry.marginLeft, num);
								num2 = Mathf.Min(guilayoutEntry.marginRight, num2);
							}
							else
							{
								num = guilayoutEntry.marginLeft;
								num2 = guilayoutEntry.marginRight;
								flag = false;
							}
							this.m_ChildMinWidth = Mathf.Max(guilayoutEntry.minWidth + (float)guilayoutEntry.marginHorizontal, this.m_ChildMinWidth);
							this.m_ChildMaxWidth = Mathf.Max(guilayoutEntry.maxWidth + (float)guilayoutEntry.marginHorizontal, this.m_ChildMaxWidth);
						}
						this.m_StretchableCountX += guilayoutEntry.stretchWidth;
					}
					this.m_ChildMinWidth -= (float)(num + num2);
					this.m_ChildMaxWidth -= (float)(num + num2);
				}
				else
				{
					int num3 = 0;
					foreach (GUILayoutEntry guilayoutEntry2 in this.entries)
					{
						guilayoutEntry2.CalcWidth();
						if (guilayoutEntry2.consideredForMargin)
						{
							int num4;
							if (!flag)
							{
								num4 = ((num3 <= guilayoutEntry2.marginLeft) ? guilayoutEntry2.marginLeft : num3);
							}
							else
							{
								num4 = 0;
								flag = false;
							}
							this.m_ChildMinWidth += guilayoutEntry2.minWidth + this.spacing + (float)num4;
							this.m_ChildMaxWidth += guilayoutEntry2.maxWidth + this.spacing + (float)num4;
							num3 = guilayoutEntry2.marginRight;
							this.m_StretchableCountX += guilayoutEntry2.stretchWidth;
						}
						else
						{
							this.m_ChildMinWidth += guilayoutEntry2.minWidth;
							this.m_ChildMaxWidth += guilayoutEntry2.maxWidth;
							this.m_StretchableCountX += guilayoutEntry2.stretchWidth;
						}
					}
					this.m_ChildMinWidth -= this.spacing;
					this.m_ChildMaxWidth -= this.spacing;
					if (this.entries.Count != 0)
					{
						num = this.entries[0].marginLeft;
						num2 = num3;
					}
					else
					{
						num2 = (num = 0);
					}
				}
				float num5;
				float num6;
				if (base.style != GUIStyle.none || this.m_UserSpecifiedWidth)
				{
					num5 = (float)Mathf.Max(base.style.padding.left, num);
					num6 = (float)Mathf.Max(base.style.padding.right, num2);
				}
				else
				{
					this.m_MarginLeft = num;
					this.m_MarginRight = num2;
					num6 = (num5 = 0f);
				}
				this.minWidth = Mathf.Max(this.minWidth, this.m_ChildMinWidth + num5 + num6);
				if (this.maxWidth == 0f)
				{
					this.stretchWidth += this.m_StretchableCountX + ((!base.style.stretchWidth) ? 0 : 1);
					this.maxWidth = this.m_ChildMaxWidth + num5 + num6;
				}
				else
				{
					this.stretchWidth = 0;
				}
				this.maxWidth = Mathf.Max(this.maxWidth, this.minWidth);
				if (base.style.fixedWidth != 0f)
				{
					this.maxWidth = (this.minWidth = base.style.fixedWidth);
					this.stretchWidth = 0;
				}
			}
		}

		// Token: 0x060003A8 RID: 936 RVA: 0x0000CB50 File Offset: 0x0000AD50
		public override void SetHorizontal(float x, float width)
		{
			base.SetHorizontal(x, width);
			if (this.resetCoords)
			{
				x = 0f;
			}
			RectOffset padding = base.style.padding;
			if (this.isVertical)
			{
				if (base.style != GUIStyle.none)
				{
					foreach (GUILayoutEntry guilayoutEntry in this.entries)
					{
						float num = (float)Mathf.Max(guilayoutEntry.marginLeft, padding.left);
						float x2 = x + num;
						float num2 = width - (float)Mathf.Max(guilayoutEntry.marginRight, padding.right) - num;
						if (guilayoutEntry.stretchWidth != 0)
						{
							guilayoutEntry.SetHorizontal(x2, num2);
						}
						else
						{
							guilayoutEntry.SetHorizontal(x2, Mathf.Clamp(num2, guilayoutEntry.minWidth, guilayoutEntry.maxWidth));
						}
					}
				}
				else
				{
					float num3 = x - (float)this.marginLeft;
					float num4 = width + (float)base.marginHorizontal;
					foreach (GUILayoutEntry guilayoutEntry2 in this.entries)
					{
						if (guilayoutEntry2.stretchWidth != 0)
						{
							guilayoutEntry2.SetHorizontal(num3 + (float)guilayoutEntry2.marginLeft, num4 - (float)guilayoutEntry2.marginHorizontal);
						}
						else
						{
							guilayoutEntry2.SetHorizontal(num3 + (float)guilayoutEntry2.marginLeft, Mathf.Clamp(num4 - (float)guilayoutEntry2.marginHorizontal, guilayoutEntry2.minWidth, guilayoutEntry2.maxWidth));
						}
					}
				}
			}
			else
			{
				if (base.style != GUIStyle.none)
				{
					float num5 = (float)padding.left;
					float num6 = (float)padding.right;
					if (this.entries.Count != 0)
					{
						num5 = Mathf.Max(num5, (float)this.entries[0].marginLeft);
						num6 = Mathf.Max(num6, (float)this.entries[this.entries.Count - 1].marginRight);
					}
					x += num5;
					width -= num6 + num5;
				}
				float num7 = width - this.spacing * (float)(this.entries.Count - 1);
				float t = 0f;
				if (this.m_ChildMinWidth != this.m_ChildMaxWidth)
				{
					t = Mathf.Clamp((num7 - this.m_ChildMinWidth) / (this.m_ChildMaxWidth - this.m_ChildMinWidth), 0f, 1f);
				}
				float num8 = 0f;
				if (num7 > this.m_ChildMaxWidth)
				{
					if (this.m_StretchableCountX > 0)
					{
						num8 = (num7 - this.m_ChildMaxWidth) / (float)this.m_StretchableCountX;
					}
				}
				int num9 = 0;
				bool flag = true;
				foreach (GUILayoutEntry guilayoutEntry3 in this.entries)
				{
					float num10 = Mathf.Lerp(guilayoutEntry3.minWidth, guilayoutEntry3.maxWidth, t);
					num10 += num8 * (float)guilayoutEntry3.stretchWidth;
					if (guilayoutEntry3.consideredForMargin)
					{
						int num11 = guilayoutEntry3.marginLeft;
						if (flag)
						{
							num11 = 0;
							flag = false;
						}
						int num12 = (num9 <= num11) ? num11 : num9;
						x += (float)num12;
						num9 = guilayoutEntry3.marginRight;
					}
					guilayoutEntry3.SetHorizontal(Mathf.Round(x), Mathf.Round(num10));
					x += num10 + this.spacing;
				}
			}
		}

		// Token: 0x060003A9 RID: 937 RVA: 0x0000CF18 File Offset: 0x0000B118
		public override void CalcHeight()
		{
			if (this.entries.Count == 0)
			{
				this.maxHeight = (this.minHeight = (float)base.style.padding.vertical);
			}
			else
			{
				int num = 0;
				int num2 = 0;
				this.m_ChildMinHeight = 0f;
				this.m_ChildMaxHeight = 0f;
				this.m_StretchableCountY = 0;
				if (this.isVertical)
				{
					int num3 = 0;
					bool flag = true;
					foreach (GUILayoutEntry guilayoutEntry in this.entries)
					{
						guilayoutEntry.CalcHeight();
						if (guilayoutEntry.consideredForMargin)
						{
							int num4;
							if (!flag)
							{
								num4 = Mathf.Max(num3, guilayoutEntry.marginTop);
							}
							else
							{
								num4 = 0;
								flag = false;
							}
							this.m_ChildMinHeight += guilayoutEntry.minHeight + this.spacing + (float)num4;
							this.m_ChildMaxHeight += guilayoutEntry.maxHeight + this.spacing + (float)num4;
							num3 = guilayoutEntry.marginBottom;
							this.m_StretchableCountY += guilayoutEntry.stretchHeight;
						}
						else
						{
							this.m_ChildMinHeight += guilayoutEntry.minHeight;
							this.m_ChildMaxHeight += guilayoutEntry.maxHeight;
							this.m_StretchableCountY += guilayoutEntry.stretchHeight;
						}
					}
					this.m_ChildMinHeight -= this.spacing;
					this.m_ChildMaxHeight -= this.spacing;
					if (this.entries.Count != 0)
					{
						num = this.entries[0].marginTop;
						num2 = num3;
					}
					else
					{
						num = (num2 = 0);
					}
				}
				else
				{
					bool flag2 = true;
					foreach (GUILayoutEntry guilayoutEntry2 in this.entries)
					{
						guilayoutEntry2.CalcHeight();
						if (guilayoutEntry2.consideredForMargin)
						{
							if (!flag2)
							{
								num = Mathf.Min(guilayoutEntry2.marginTop, num);
								num2 = Mathf.Min(guilayoutEntry2.marginBottom, num2);
							}
							else
							{
								num = guilayoutEntry2.marginTop;
								num2 = guilayoutEntry2.marginBottom;
								flag2 = false;
							}
							this.m_ChildMinHeight = Mathf.Max(guilayoutEntry2.minHeight, this.m_ChildMinHeight);
							this.m_ChildMaxHeight = Mathf.Max(guilayoutEntry2.maxHeight, this.m_ChildMaxHeight);
						}
						this.m_StretchableCountY += guilayoutEntry2.stretchHeight;
					}
				}
				float num5;
				float num6;
				if (base.style != GUIStyle.none || this.m_UserSpecifiedHeight)
				{
					num5 = (float)Mathf.Max(base.style.padding.top, num);
					num6 = (float)Mathf.Max(base.style.padding.bottom, num2);
				}
				else
				{
					this.m_MarginTop = num;
					this.m_MarginBottom = num2;
					num6 = (num5 = 0f);
				}
				this.minHeight = Mathf.Max(this.minHeight, this.m_ChildMinHeight + num5 + num6);
				if (this.maxHeight == 0f)
				{
					this.stretchHeight += this.m_StretchableCountY + ((!base.style.stretchHeight) ? 0 : 1);
					this.maxHeight = this.m_ChildMaxHeight + num5 + num6;
				}
				else
				{
					this.stretchHeight = 0;
				}
				this.maxHeight = Mathf.Max(this.maxHeight, this.minHeight);
				if (base.style.fixedHeight != 0f)
				{
					this.maxHeight = (this.minHeight = base.style.fixedHeight);
					this.stretchHeight = 0;
				}
			}
		}

		// Token: 0x060003AA RID: 938 RVA: 0x0000D338 File Offset: 0x0000B538
		public override void SetVertical(float y, float height)
		{
			base.SetVertical(y, height);
			if (this.entries.Count != 0)
			{
				RectOffset padding = base.style.padding;
				if (this.resetCoords)
				{
					y = 0f;
				}
				if (this.isVertical)
				{
					if (base.style != GUIStyle.none)
					{
						float num = (float)padding.top;
						float num2 = (float)padding.bottom;
						if (this.entries.Count != 0)
						{
							num = Mathf.Max(num, (float)this.entries[0].marginTop);
							num2 = Mathf.Max(num2, (float)this.entries[this.entries.Count - 1].marginBottom);
						}
						y += num;
						height -= num2 + num;
					}
					float num3 = height - this.spacing * (float)(this.entries.Count - 1);
					float t = 0f;
					if (this.m_ChildMinHeight != this.m_ChildMaxHeight)
					{
						t = Mathf.Clamp((num3 - this.m_ChildMinHeight) / (this.m_ChildMaxHeight - this.m_ChildMinHeight), 0f, 1f);
					}
					float num4 = 0f;
					if (num3 > this.m_ChildMaxHeight)
					{
						if (this.m_StretchableCountY > 0)
						{
							num4 = (num3 - this.m_ChildMaxHeight) / (float)this.m_StretchableCountY;
						}
					}
					int num5 = 0;
					bool flag = true;
					foreach (GUILayoutEntry guilayoutEntry in this.entries)
					{
						float num6 = Mathf.Lerp(guilayoutEntry.minHeight, guilayoutEntry.maxHeight, t);
						num6 += num4 * (float)guilayoutEntry.stretchHeight;
						if (guilayoutEntry.consideredForMargin)
						{
							int num7 = guilayoutEntry.marginTop;
							if (flag)
							{
								num7 = 0;
								flag = false;
							}
							int num8 = (num5 <= num7) ? num7 : num5;
							y += (float)num8;
							num5 = guilayoutEntry.marginBottom;
						}
						guilayoutEntry.SetVertical(Mathf.Round(y), Mathf.Round(num6));
						y += num6 + this.spacing;
					}
				}
				else if (base.style != GUIStyle.none)
				{
					foreach (GUILayoutEntry guilayoutEntry2 in this.entries)
					{
						float num9 = (float)Mathf.Max(guilayoutEntry2.marginTop, padding.top);
						float y2 = y + num9;
						float num10 = height - (float)Mathf.Max(guilayoutEntry2.marginBottom, padding.bottom) - num9;
						if (guilayoutEntry2.stretchHeight != 0)
						{
							guilayoutEntry2.SetVertical(y2, num10);
						}
						else
						{
							guilayoutEntry2.SetVertical(y2, Mathf.Clamp(num10, guilayoutEntry2.minHeight, guilayoutEntry2.maxHeight));
						}
					}
				}
				else
				{
					float num11 = y - (float)this.marginTop;
					float num12 = height + (float)base.marginVertical;
					foreach (GUILayoutEntry guilayoutEntry3 in this.entries)
					{
						if (guilayoutEntry3.stretchHeight != 0)
						{
							guilayoutEntry3.SetVertical(num11 + (float)guilayoutEntry3.marginTop, num12 - (float)guilayoutEntry3.marginVertical);
						}
						else
						{
							guilayoutEntry3.SetVertical(num11 + (float)guilayoutEntry3.marginTop, Mathf.Clamp(num12 - (float)guilayoutEntry3.marginVertical, guilayoutEntry3.minHeight, guilayoutEntry3.maxHeight));
						}
					}
				}
			}
		}

		// Token: 0x060003AB RID: 939 RVA: 0x0000D710 File Offset: 0x0000B910
		public override string ToString()
		{
			string text = "";
			string text2 = "";
			for (int i = 0; i < GUILayoutEntry.indent; i++)
			{
				text2 += " ";
			}
			string text3 = text;
			text = string.Concat(new object[]
			{
				text3,
				base.ToString(),
				" Margins: ",
				this.m_ChildMinHeight,
				" {\n"
			});
			GUILayoutEntry.indent += 4;
			foreach (GUILayoutEntry guilayoutEntry in this.entries)
			{
				text = text + guilayoutEntry.ToString() + "\n";
			}
			text = text + text2 + "}";
			GUILayoutEntry.indent -= 4;
			return text;
		}

		// Token: 0x040000E7 RID: 231
		public List<GUILayoutEntry> entries = new List<GUILayoutEntry>();

		// Token: 0x040000E8 RID: 232
		public bool isVertical = true;

		// Token: 0x040000E9 RID: 233
		public bool resetCoords = false;

		// Token: 0x040000EA RID: 234
		public float spacing = 0f;

		// Token: 0x040000EB RID: 235
		public bool sameSize = true;

		// Token: 0x040000EC RID: 236
		public bool isWindow = false;

		// Token: 0x040000ED RID: 237
		public int windowID = -1;

		// Token: 0x040000EE RID: 238
		private int m_Cursor = 0;

		// Token: 0x040000EF RID: 239
		protected int m_StretchableCountX = 100;

		// Token: 0x040000F0 RID: 240
		protected int m_StretchableCountY = 100;

		// Token: 0x040000F1 RID: 241
		protected bool m_UserSpecifiedWidth = false;

		// Token: 0x040000F2 RID: 242
		protected bool m_UserSpecifiedHeight = false;

		// Token: 0x040000F3 RID: 243
		protected float m_ChildMinWidth = 100f;

		// Token: 0x040000F4 RID: 244
		protected float m_ChildMaxWidth = 100f;

		// Token: 0x040000F5 RID: 245
		protected float m_ChildMinHeight = 100f;

		// Token: 0x040000F6 RID: 246
		protected float m_ChildMaxHeight = 100f;

		// Token: 0x040000F7 RID: 247
		protected int m_MarginLeft;

		// Token: 0x040000F8 RID: 248
		protected int m_MarginRight;

		// Token: 0x040000F9 RID: 249
		protected int m_MarginTop;

		// Token: 0x040000FA RID: 250
		protected int m_MarginBottom;
	}
}
