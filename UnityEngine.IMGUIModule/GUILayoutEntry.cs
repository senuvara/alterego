﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;

namespace UnityEngine
{
	// Token: 0x0200002B RID: 43
	[VisibleToOtherModules(new string[]
	{
		"UnityEngine.UIElementsModule"
	})]
	internal class GUILayoutEntry
	{
		// Token: 0x06000380 RID: 896 RVA: 0x0000B4A8 File Offset: 0x000096A8
		public GUILayoutEntry(float _minWidth, float _maxWidth, float _minHeight, float _maxHeight, GUIStyle _style)
		{
			this.minWidth = _minWidth;
			this.maxWidth = _maxWidth;
			this.minHeight = _minHeight;
			this.maxHeight = _maxHeight;
			if (_style == null)
			{
				_style = GUIStyle.none;
			}
			this.style = _style;
		}

		// Token: 0x06000381 RID: 897 RVA: 0x0000B520 File Offset: 0x00009720
		public GUILayoutEntry(float _minWidth, float _maxWidth, float _minHeight, float _maxHeight, GUIStyle _style, GUILayoutOption[] options)
		{
			this.minWidth = _minWidth;
			this.maxWidth = _maxWidth;
			this.minHeight = _minHeight;
			this.maxHeight = _maxHeight;
			this.style = _style;
			this.ApplyOptions(options);
		}

		// Token: 0x17000083 RID: 131
		// (get) Token: 0x06000382 RID: 898 RVA: 0x0000B594 File Offset: 0x00009794
		// (set) Token: 0x06000383 RID: 899 RVA: 0x0000B5AF File Offset: 0x000097AF
		public GUIStyle style
		{
			get
			{
				return this.m_Style;
			}
			set
			{
				this.m_Style = value;
				this.ApplyStyleSettings(value);
			}
		}

		// Token: 0x17000084 RID: 132
		// (get) Token: 0x06000384 RID: 900 RVA: 0x0000B5C0 File Offset: 0x000097C0
		public virtual int marginLeft
		{
			[CompilerGenerated]
			get
			{
				return this.style.margin.left;
			}
		}

		// Token: 0x17000085 RID: 133
		// (get) Token: 0x06000385 RID: 901 RVA: 0x0000B5E4 File Offset: 0x000097E4
		public virtual int marginRight
		{
			[CompilerGenerated]
			get
			{
				return this.style.margin.right;
			}
		}

		// Token: 0x17000086 RID: 134
		// (get) Token: 0x06000386 RID: 902 RVA: 0x0000B608 File Offset: 0x00009808
		public virtual int marginTop
		{
			[CompilerGenerated]
			get
			{
				return this.style.margin.top;
			}
		}

		// Token: 0x17000087 RID: 135
		// (get) Token: 0x06000387 RID: 903 RVA: 0x0000B62C File Offset: 0x0000982C
		public virtual int marginBottom
		{
			[CompilerGenerated]
			get
			{
				return this.style.margin.bottom;
			}
		}

		// Token: 0x17000088 RID: 136
		// (get) Token: 0x06000388 RID: 904 RVA: 0x0000B650 File Offset: 0x00009850
		public int marginHorizontal
		{
			[CompilerGenerated]
			get
			{
				return this.marginLeft + this.marginRight;
			}
		}

		// Token: 0x17000089 RID: 137
		// (get) Token: 0x06000389 RID: 905 RVA: 0x0000B674 File Offset: 0x00009874
		public int marginVertical
		{
			[CompilerGenerated]
			get
			{
				return this.marginBottom + this.marginTop;
			}
		}

		// Token: 0x0600038A RID: 906 RVA: 0x000021D8 File Offset: 0x000003D8
		public virtual void CalcWidth()
		{
		}

		// Token: 0x0600038B RID: 907 RVA: 0x000021D8 File Offset: 0x000003D8
		public virtual void CalcHeight()
		{
		}

		// Token: 0x0600038C RID: 908 RVA: 0x0000B695 File Offset: 0x00009895
		public virtual void SetHorizontal(float x, float width)
		{
			this.rect.x = x;
			this.rect.width = width;
		}

		// Token: 0x0600038D RID: 909 RVA: 0x0000B6B0 File Offset: 0x000098B0
		public virtual void SetVertical(float y, float height)
		{
			this.rect.y = y;
			this.rect.height = height;
		}

		// Token: 0x0600038E RID: 910 RVA: 0x0000B6CC File Offset: 0x000098CC
		protected virtual void ApplyStyleSettings(GUIStyle style)
		{
			this.stretchWidth = ((style.fixedWidth != 0f || !style.stretchWidth) ? 0 : 1);
			this.stretchHeight = ((style.fixedHeight != 0f || !style.stretchHeight) ? 0 : 1);
			this.m_Style = style;
		}

		// Token: 0x0600038F RID: 911 RVA: 0x0000B734 File Offset: 0x00009934
		public virtual void ApplyOptions(GUILayoutOption[] options)
		{
			if (options != null)
			{
				foreach (GUILayoutOption guilayoutOption in options)
				{
					switch (guilayoutOption.type)
					{
					case GUILayoutOption.Type.fixedWidth:
						this.minWidth = (this.maxWidth = (float)guilayoutOption.value);
						this.stretchWidth = 0;
						break;
					case GUILayoutOption.Type.fixedHeight:
						this.minHeight = (this.maxHeight = (float)guilayoutOption.value);
						this.stretchHeight = 0;
						break;
					case GUILayoutOption.Type.minWidth:
						this.minWidth = (float)guilayoutOption.value;
						if (this.maxWidth < this.minWidth)
						{
							this.maxWidth = this.minWidth;
						}
						break;
					case GUILayoutOption.Type.maxWidth:
						this.maxWidth = (float)guilayoutOption.value;
						if (this.minWidth > this.maxWidth)
						{
							this.minWidth = this.maxWidth;
						}
						this.stretchWidth = 0;
						break;
					case GUILayoutOption.Type.minHeight:
						this.minHeight = (float)guilayoutOption.value;
						if (this.maxHeight < this.minHeight)
						{
							this.maxHeight = this.minHeight;
						}
						break;
					case GUILayoutOption.Type.maxHeight:
						this.maxHeight = (float)guilayoutOption.value;
						if (this.minHeight > this.maxHeight)
						{
							this.minHeight = this.maxHeight;
						}
						this.stretchHeight = 0;
						break;
					case GUILayoutOption.Type.stretchWidth:
						this.stretchWidth = (int)guilayoutOption.value;
						break;
					case GUILayoutOption.Type.stretchHeight:
						this.stretchHeight = (int)guilayoutOption.value;
						break;
					}
				}
				if (this.maxWidth != 0f && this.maxWidth < this.minWidth)
				{
					this.maxWidth = this.minWidth;
				}
				if (this.maxHeight != 0f && this.maxHeight < this.minHeight)
				{
					this.maxHeight = this.minHeight;
				}
			}
		}

		// Token: 0x06000390 RID: 912 RVA: 0x0000B94C File Offset: 0x00009B4C
		public override string ToString()
		{
			string text = "";
			for (int i = 0; i < GUILayoutEntry.indent; i++)
			{
				text += " ";
			}
			return string.Concat(new object[]
			{
				text,
				UnityString.Format("{1}-{0} (x:{2}-{3}, y:{4}-{5})", new object[]
				{
					(this.style == null) ? "NULL" : this.style.name,
					base.GetType(),
					this.rect.x,
					this.rect.xMax,
					this.rect.y,
					this.rect.yMax
				}),
				"   -   W: ",
				this.minWidth,
				"-",
				this.maxWidth,
				(this.stretchWidth == 0) ? "" : "+",
				", H: ",
				this.minHeight,
				"-",
				this.maxHeight,
				(this.stretchHeight == 0) ? "" : "+"
			});
		}

		// Token: 0x06000391 RID: 913 RVA: 0x0000BAB9 File Offset: 0x00009CB9
		// Note: this type is marked as 'beforefieldinit'.
		static GUILayoutEntry()
		{
		}

		// Token: 0x040000D2 RID: 210
		public float minWidth;

		// Token: 0x040000D3 RID: 211
		public float maxWidth;

		// Token: 0x040000D4 RID: 212
		public float minHeight;

		// Token: 0x040000D5 RID: 213
		public float maxHeight;

		// Token: 0x040000D6 RID: 214
		public Rect rect = new Rect(0f, 0f, 0f, 0f);

		// Token: 0x040000D7 RID: 215
		public int stretchWidth;

		// Token: 0x040000D8 RID: 216
		public int stretchHeight;

		// Token: 0x040000D9 RID: 217
		public bool consideredForMargin = true;

		// Token: 0x040000DA RID: 218
		private GUIStyle m_Style = GUIStyle.none;

		// Token: 0x040000DB RID: 219
		internal static Rect kDummyRect = new Rect(0f, 0f, 1f, 1f);

		// Token: 0x040000DC RID: 220
		protected static int indent = 0;
	}
}
