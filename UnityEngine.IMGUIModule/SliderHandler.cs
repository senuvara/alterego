﻿using System;

namespace UnityEngine
{
	// Token: 0x02000034 RID: 52
	internal struct SliderHandler
	{
		// Token: 0x060003BC RID: 956 RVA: 0x0000DFE8 File Offset: 0x0000C1E8
		public SliderHandler(Rect position, float currentValue, float size, float start, float end, GUIStyle slider, GUIStyle thumb, bool horiz, int id)
		{
			this.position = position;
			this.currentValue = currentValue;
			this.size = size;
			this.start = start;
			this.end = end;
			this.slider = slider;
			this.thumb = thumb;
			this.horiz = horiz;
			this.id = id;
		}

		// Token: 0x060003BD RID: 957 RVA: 0x0000E03C File Offset: 0x0000C23C
		public float Handle()
		{
			float result;
			if (this.slider == null || this.thumb == null)
			{
				result = this.currentValue;
			}
			else
			{
				switch (this.CurrentEventType())
				{
				case EventType.MouseDown:
					return this.OnMouseDown();
				case EventType.MouseUp:
					return this.OnMouseUp();
				case EventType.MouseDrag:
					return this.OnMouseDrag();
				case EventType.Repaint:
					return this.OnRepaint();
				}
				result = this.currentValue;
			}
			return result;
		}

		// Token: 0x060003BE RID: 958 RVA: 0x0000E0DC File Offset: 0x0000C2DC
		private float OnMouseDown()
		{
			float result;
			if (!this.position.Contains(this.CurrentEvent().mousePosition) || this.IsEmptySlider())
			{
				result = this.currentValue;
			}
			else
			{
				GUI.scrollTroughSide = 0;
				GUIUtility.hotControl = this.id;
				this.CurrentEvent().Use();
				if (this.ThumbSelectionRect().Contains(this.CurrentEvent().mousePosition))
				{
					this.StartDraggingWithValue(this.ClampedCurrentValue());
					result = this.currentValue;
				}
				else
				{
					GUI.changed = true;
					if (this.SupportsPageMovements())
					{
						this.SliderState().isDragging = false;
						GUI.nextScrollStepTime = SystemClock.now.AddMilliseconds(250.0);
						GUI.scrollTroughSide = this.CurrentScrollTroughSide();
						result = this.PageMovementValue();
					}
					else
					{
						float num = this.ValueForCurrentMousePosition();
						this.StartDraggingWithValue(num);
						result = this.Clamp(num);
					}
				}
			}
			return result;
		}

		// Token: 0x060003BF RID: 959 RVA: 0x0000E1E4 File Offset: 0x0000C3E4
		private float OnMouseDrag()
		{
			float result;
			if (GUIUtility.hotControl != this.id)
			{
				result = this.currentValue;
			}
			else
			{
				SliderState sliderState = this.SliderState();
				if (!sliderState.isDragging)
				{
					result = this.currentValue;
				}
				else
				{
					GUI.changed = true;
					this.CurrentEvent().Use();
					float num = this.MousePosition() - sliderState.dragStartPos;
					float value = sliderState.dragStartValue + num / this.ValuesPerPixel();
					result = this.Clamp(value);
				}
			}
			return result;
		}

		// Token: 0x060003C0 RID: 960 RVA: 0x0000E26C File Offset: 0x0000C46C
		private float OnMouseUp()
		{
			if (GUIUtility.hotControl == this.id)
			{
				this.CurrentEvent().Use();
				GUIUtility.hotControl = 0;
			}
			return this.currentValue;
		}

		// Token: 0x060003C1 RID: 961 RVA: 0x0000E2AC File Offset: 0x0000C4AC
		private float OnRepaint()
		{
			this.slider.Draw(this.position, GUIContent.none, this.id);
			if (!this.IsEmptySlider() && this.currentValue >= this.MinValue() && this.currentValue <= this.MaxValue())
			{
				this.thumb.Draw(this.ThumbRect(), GUIContent.none, this.id);
			}
			float result;
			if (GUIUtility.hotControl != this.id || !this.position.Contains(this.CurrentEvent().mousePosition) || this.IsEmptySlider())
			{
				result = this.currentValue;
			}
			else if (this.ThumbRect().Contains(this.CurrentEvent().mousePosition))
			{
				if (GUI.scrollTroughSide != 0)
				{
					GUIUtility.hotControl = 0;
				}
				result = this.currentValue;
			}
			else
			{
				GUI.InternalRepaintEditorWindow();
				if (SystemClock.now < GUI.nextScrollStepTime)
				{
					result = this.currentValue;
				}
				else if (this.CurrentScrollTroughSide() != GUI.scrollTroughSide)
				{
					result = this.currentValue;
				}
				else
				{
					GUI.nextScrollStepTime = SystemClock.now.AddMilliseconds(30.0);
					if (this.SupportsPageMovements())
					{
						this.SliderState().isDragging = false;
						GUI.changed = true;
						result = this.PageMovementValue();
					}
					else
					{
						result = this.ClampedCurrentValue();
					}
				}
			}
			return result;
		}

		// Token: 0x060003C2 RID: 962 RVA: 0x0000E434 File Offset: 0x0000C634
		private EventType CurrentEventType()
		{
			return this.CurrentEvent().GetTypeForControl(this.id);
		}

		// Token: 0x060003C3 RID: 963 RVA: 0x0000E45C File Offset: 0x0000C65C
		private int CurrentScrollTroughSide()
		{
			float num = (!this.horiz) ? this.CurrentEvent().mousePosition.y : this.CurrentEvent().mousePosition.x;
			float num2 = (!this.horiz) ? this.ThumbRect().y : this.ThumbRect().x;
			return (num <= num2) ? -1 : 1;
		}

		// Token: 0x060003C4 RID: 964 RVA: 0x0000E4E8 File Offset: 0x0000C6E8
		private bool IsEmptySlider()
		{
			return this.start == this.end;
		}

		// Token: 0x060003C5 RID: 965 RVA: 0x0000E50C File Offset: 0x0000C70C
		private bool SupportsPageMovements()
		{
			return this.size != 0f && GUI.usePageScrollbars;
		}

		// Token: 0x060003C6 RID: 966 RVA: 0x0000E53C File Offset: 0x0000C73C
		private float PageMovementValue()
		{
			float num = this.currentValue;
			int num2 = (this.start <= this.end) ? 1 : -1;
			if (this.MousePosition() > this.PageUpMovementBound())
			{
				num += this.size * (float)num2 * 0.9f;
			}
			else
			{
				num -= this.size * (float)num2 * 0.9f;
			}
			return this.Clamp(num);
		}

		// Token: 0x060003C7 RID: 967 RVA: 0x0000E5B4 File Offset: 0x0000C7B4
		private float PageUpMovementBound()
		{
			float result;
			if (this.horiz)
			{
				result = this.ThumbRect().xMax - this.position.x;
			}
			else
			{
				result = this.ThumbRect().yMax - this.position.y;
			}
			return result;
		}

		// Token: 0x060003C8 RID: 968 RVA: 0x0000E618 File Offset: 0x0000C818
		private Event CurrentEvent()
		{
			return Event.current;
		}

		// Token: 0x060003C9 RID: 969 RVA: 0x0000E634 File Offset: 0x0000C834
		private float ValueForCurrentMousePosition()
		{
			float result;
			if (this.horiz)
			{
				result = (this.MousePosition() - this.ThumbRect().width * 0.5f) / this.ValuesPerPixel() + this.start - this.size * 0.5f;
			}
			else
			{
				result = (this.MousePosition() - this.ThumbRect().height * 0.5f) / this.ValuesPerPixel() + this.start - this.size * 0.5f;
			}
			return result;
		}

		// Token: 0x060003CA RID: 970 RVA: 0x0000E6C8 File Offset: 0x0000C8C8
		private float Clamp(float value)
		{
			return Mathf.Clamp(value, this.MinValue(), this.MaxValue());
		}

		// Token: 0x060003CB RID: 971 RVA: 0x0000E6F0 File Offset: 0x0000C8F0
		private Rect ThumbSelectionRect()
		{
			return this.ThumbRect();
		}

		// Token: 0x060003CC RID: 972 RVA: 0x0000E710 File Offset: 0x0000C910
		private void StartDraggingWithValue(float dragStartValue)
		{
			SliderState sliderState = this.SliderState();
			sliderState.dragStartPos = this.MousePosition();
			sliderState.dragStartValue = dragStartValue;
			sliderState.isDragging = true;
		}

		// Token: 0x060003CD RID: 973 RVA: 0x0000E740 File Offset: 0x0000C940
		private SliderState SliderState()
		{
			return (SliderState)GUIUtility.GetStateObject(typeof(SliderState), this.id);
		}

		// Token: 0x060003CE RID: 974 RVA: 0x0000E770 File Offset: 0x0000C970
		private Rect ThumbRect()
		{
			return (!this.horiz) ? this.VerticalThumbRect() : this.HorizontalThumbRect();
		}

		// Token: 0x060003CF RID: 975 RVA: 0x0000E7A4 File Offset: 0x0000C9A4
		private Rect VerticalThumbRect()
		{
			float num = this.ValuesPerPixel();
			Rect result;
			if (this.start < this.end)
			{
				result = new Rect(this.position.x + (float)this.slider.padding.left, (this.ClampedCurrentValue() - this.start) * num + this.position.y + (float)this.slider.padding.top, this.position.width - (float)this.slider.padding.horizontal, this.size * num + this.ThumbSize());
			}
			else
			{
				result = new Rect(this.position.x + (float)this.slider.padding.left, (this.ClampedCurrentValue() + this.size - this.start) * num + this.position.y + (float)this.slider.padding.top, this.position.width - (float)this.slider.padding.horizontal, this.size * -num + this.ThumbSize());
			}
			return result;
		}

		// Token: 0x060003D0 RID: 976 RVA: 0x0000E8F0 File Offset: 0x0000CAF0
		private Rect HorizontalThumbRect()
		{
			float num = this.ValuesPerPixel();
			Rect result;
			if (this.start < this.end)
			{
				result = new Rect((this.ClampedCurrentValue() - this.start) * num + this.position.x + (float)this.slider.padding.left, this.position.y + (float)this.slider.padding.top, this.size * num + this.ThumbSize(), this.position.height - (float)this.slider.padding.vertical);
			}
			else
			{
				result = new Rect((this.ClampedCurrentValue() + this.size - this.start) * num + this.position.x + (float)this.slider.padding.left, this.position.y, this.size * -num + this.ThumbSize(), this.position.height);
			}
			return result;
		}

		// Token: 0x060003D1 RID: 977 RVA: 0x0000EA18 File Offset: 0x0000CC18
		private float ClampedCurrentValue()
		{
			return this.Clamp(this.currentValue);
		}

		// Token: 0x060003D2 RID: 978 RVA: 0x0000EA3C File Offset: 0x0000CC3C
		private float MousePosition()
		{
			float result;
			if (this.horiz)
			{
				result = this.CurrentEvent().mousePosition.x - this.position.x;
			}
			else
			{
				result = this.CurrentEvent().mousePosition.y - this.position.y;
			}
			return result;
		}

		// Token: 0x060003D3 RID: 979 RVA: 0x0000EAA8 File Offset: 0x0000CCA8
		private float ValuesPerPixel()
		{
			float result;
			if (this.horiz)
			{
				result = (this.position.width - (float)this.slider.padding.horizontal - this.ThumbSize()) / (this.end - this.start);
			}
			else
			{
				result = (this.position.height - (float)this.slider.padding.vertical - this.ThumbSize()) / (this.end - this.start);
			}
			return result;
		}

		// Token: 0x060003D4 RID: 980 RVA: 0x0000EB38 File Offset: 0x0000CD38
		private float ThumbSize()
		{
			float result;
			if (this.horiz)
			{
				result = ((this.thumb.fixedWidth == 0f) ? ((float)this.thumb.padding.horizontal) : this.thumb.fixedWidth);
			}
			else
			{
				result = ((this.thumb.fixedHeight == 0f) ? ((float)this.thumb.padding.vertical) : this.thumb.fixedHeight);
			}
			return result;
		}

		// Token: 0x060003D5 RID: 981 RVA: 0x0000EBCC File Offset: 0x0000CDCC
		private float MaxValue()
		{
			return Mathf.Max(this.start, this.end) - this.size;
		}

		// Token: 0x060003D6 RID: 982 RVA: 0x0000EBFC File Offset: 0x0000CDFC
		private float MinValue()
		{
			return Mathf.Min(this.start, this.end);
		}

		// Token: 0x04000110 RID: 272
		private readonly Rect position;

		// Token: 0x04000111 RID: 273
		private readonly float currentValue;

		// Token: 0x04000112 RID: 274
		private readonly float size;

		// Token: 0x04000113 RID: 275
		private readonly float start;

		// Token: 0x04000114 RID: 276
		private readonly float end;

		// Token: 0x04000115 RID: 277
		private readonly GUIStyle slider;

		// Token: 0x04000116 RID: 278
		private readonly GUIStyle thumb;

		// Token: 0x04000117 RID: 279
		private readonly bool horiz;

		// Token: 0x04000118 RID: 280
		private readonly int id;
	}
}
