﻿using System;

namespace UnityEngine
{
	// Token: 0x02000026 RID: 38
	public enum ImagePosition
	{
		// Token: 0x040000C1 RID: 193
		ImageLeft,
		// Token: 0x040000C2 RID: 194
		ImageAbove,
		// Token: 0x040000C3 RID: 195
		ImageOnly,
		// Token: 0x040000C4 RID: 196
		TextOnly
	}
}
