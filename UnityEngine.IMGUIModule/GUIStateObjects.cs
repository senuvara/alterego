﻿using System;
using System.Collections.Generic;
using System.Security;

namespace UnityEngine
{
	// Token: 0x02000023 RID: 35
	internal class GUIStateObjects
	{
		// Token: 0x06000299 RID: 665 RVA: 0x0000338C File Offset: 0x0000158C
		public GUIStateObjects()
		{
		}

		// Token: 0x0600029A RID: 666 RVA: 0x0000A10C File Offset: 0x0000830C
		[SecuritySafeCritical]
		internal static object GetStateObject(Type t, int controlID)
		{
			object obj;
			if (!GUIStateObjects.s_StateCache.TryGetValue(controlID, out obj) || obj.GetType() != t)
			{
				obj = Activator.CreateInstance(t);
				GUIStateObjects.s_StateCache[controlID] = obj;
			}
			return obj;
		}

		// Token: 0x0600029B RID: 667 RVA: 0x0000A158 File Offset: 0x00008358
		internal static object QueryStateObject(Type t, int controlID)
		{
			object obj = GUIStateObjects.s_StateCache[controlID];
			object result;
			if (t.IsInstanceOfType(obj))
			{
				result = obj;
			}
			else
			{
				result = null;
			}
			return result;
		}

		// Token: 0x0600029C RID: 668 RVA: 0x0000A18E File Offset: 0x0000838E
		internal static void Tests_ClearObjects()
		{
			GUIStateObjects.s_StateCache.Clear();
		}

		// Token: 0x0600029D RID: 669 RVA: 0x0000A19B File Offset: 0x0000839B
		// Note: this type is marked as 'beforefieldinit'.
		static GUIStateObjects()
		{
		}

		// Token: 0x040000AE RID: 174
		private static Dictionary<int, object> s_StateCache = new Dictionary<int, object>();
	}
}
