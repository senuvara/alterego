﻿using System;

namespace UnityEngine
{
	// Token: 0x02000013 RID: 19
	public enum ScaleMode
	{
		// Token: 0x04000064 RID: 100
		StretchToFill,
		// Token: 0x04000065 RID: 101
		ScaleAndCrop,
		// Token: 0x04000066 RID: 102
		ScaleToFit
	}
}
