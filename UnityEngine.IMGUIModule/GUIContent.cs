﻿using System;
using System.Runtime.InteropServices;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000011 RID: 17
	[NativeHeader("Modules/IMGUI/GUIContent.h")]
	[RequiredByNativeCode(Optional = true, GenerateProxy = true)]
	[Serializable]
	[StructLayout(LayoutKind.Sequential)]
	public class GUIContent
	{
		// Token: 0x06000165 RID: 357 RVA: 0x0000693E File Offset: 0x00004B3E
		public GUIContent()
		{
		}

		// Token: 0x06000166 RID: 358 RVA: 0x0000695D File Offset: 0x00004B5D
		public GUIContent(string text) : this(text, null, string.Empty)
		{
		}

		// Token: 0x06000167 RID: 359 RVA: 0x0000696D File Offset: 0x00004B6D
		public GUIContent(Texture image) : this(string.Empty, image, string.Empty)
		{
		}

		// Token: 0x06000168 RID: 360 RVA: 0x00006981 File Offset: 0x00004B81
		public GUIContent(string text, Texture image) : this(text, image, string.Empty)
		{
		}

		// Token: 0x06000169 RID: 361 RVA: 0x00006991 File Offset: 0x00004B91
		public GUIContent(string text, string tooltip) : this(text, null, tooltip)
		{
		}

		// Token: 0x0600016A RID: 362 RVA: 0x0000699D File Offset: 0x00004B9D
		public GUIContent(Texture image, string tooltip) : this(string.Empty, image, tooltip)
		{
		}

		// Token: 0x0600016B RID: 363 RVA: 0x000069AD File Offset: 0x00004BAD
		public GUIContent(string text, Texture image, string tooltip)
		{
			this.text = text;
			this.image = image;
			this.tooltip = tooltip;
		}

		// Token: 0x0600016C RID: 364 RVA: 0x000069E4 File Offset: 0x00004BE4
		public GUIContent(GUIContent src)
		{
			this.text = src.m_Text;
			this.image = src.m_Image;
			this.tooltip = src.m_Tooltip;
		}

		// Token: 0x17000031 RID: 49
		// (get) Token: 0x0600016D RID: 365 RVA: 0x00006A34 File Offset: 0x00004C34
		// (set) Token: 0x0600016E RID: 366 RVA: 0x00006A4F File Offset: 0x00004C4F
		public string text
		{
			get
			{
				return this.m_Text;
			}
			set
			{
				this.m_Text = value;
			}
		}

		// Token: 0x17000032 RID: 50
		// (get) Token: 0x0600016F RID: 367 RVA: 0x00006A5C File Offset: 0x00004C5C
		// (set) Token: 0x06000170 RID: 368 RVA: 0x00006A77 File Offset: 0x00004C77
		public Texture image
		{
			get
			{
				return this.m_Image;
			}
			set
			{
				this.m_Image = value;
			}
		}

		// Token: 0x17000033 RID: 51
		// (get) Token: 0x06000171 RID: 369 RVA: 0x00006A84 File Offset: 0x00004C84
		// (set) Token: 0x06000172 RID: 370 RVA: 0x00006A9F File Offset: 0x00004C9F
		public string tooltip
		{
			get
			{
				return this.m_Tooltip;
			}
			set
			{
				this.m_Tooltip = value;
			}
		}

		// Token: 0x17000034 RID: 52
		// (get) Token: 0x06000173 RID: 371 RVA: 0x00006AAC File Offset: 0x00004CAC
		internal int hash
		{
			get
			{
				int result = 0;
				if (!string.IsNullOrEmpty(this.m_Text))
				{
					result = this.m_Text.GetHashCode() * 37;
				}
				return result;
			}
		}

		// Token: 0x06000174 RID: 372 RVA: 0x00006AE4 File Offset: 0x00004CE4
		internal static GUIContent Temp(string t)
		{
			GUIContent.s_Text.m_Text = t;
			GUIContent.s_Text.m_Tooltip = string.Empty;
			return GUIContent.s_Text;
		}

		// Token: 0x06000175 RID: 373 RVA: 0x00006B18 File Offset: 0x00004D18
		internal static GUIContent Temp(string t, string tooltip)
		{
			GUIContent.s_Text.m_Text = t;
			GUIContent.s_Text.m_Tooltip = tooltip;
			return GUIContent.s_Text;
		}

		// Token: 0x06000176 RID: 374 RVA: 0x00006B48 File Offset: 0x00004D48
		internal static GUIContent Temp(Texture i)
		{
			GUIContent.s_Image.m_Image = i;
			GUIContent.s_Image.m_Tooltip = string.Empty;
			return GUIContent.s_Image;
		}

		// Token: 0x06000177 RID: 375 RVA: 0x00006B7C File Offset: 0x00004D7C
		internal static GUIContent Temp(Texture i, string tooltip)
		{
			GUIContent.s_Image.m_Image = i;
			GUIContent.s_Image.m_Tooltip = tooltip;
			return GUIContent.s_Image;
		}

		// Token: 0x06000178 RID: 376 RVA: 0x00006BAC File Offset: 0x00004DAC
		internal static GUIContent Temp(string t, Texture i)
		{
			GUIContent.s_TextImage.m_Text = t;
			GUIContent.s_TextImage.m_Image = i;
			return GUIContent.s_TextImage;
		}

		// Token: 0x06000179 RID: 377 RVA: 0x00006BDC File Offset: 0x00004DDC
		[VisibleToOtherModules(new string[]
		{
			"UnityEngine.UIElementsModule"
		})]
		internal static void ClearStaticCache()
		{
			GUIContent.s_Text.m_Text = null;
			GUIContent.s_Text.m_Tooltip = string.Empty;
			GUIContent.s_Image.m_Image = null;
			GUIContent.s_Image.m_Tooltip = string.Empty;
			GUIContent.s_TextImage.m_Text = null;
			GUIContent.s_TextImage.m_Image = null;
		}

		// Token: 0x0600017A RID: 378 RVA: 0x00006C34 File Offset: 0x00004E34
		internal static GUIContent[] Temp(string[] texts)
		{
			GUIContent[] array = new GUIContent[texts.Length];
			for (int i = 0; i < texts.Length; i++)
			{
				array[i] = new GUIContent(texts[i]);
			}
			return array;
		}

		// Token: 0x0600017B RID: 379 RVA: 0x00006C74 File Offset: 0x00004E74
		internal static GUIContent[] Temp(Texture[] images)
		{
			GUIContent[] array = new GUIContent[images.Length];
			for (int i = 0; i < images.Length; i++)
			{
				array[i] = new GUIContent(images[i]);
			}
			return array;
		}

		// Token: 0x0600017C RID: 380 RVA: 0x00006CB4 File Offset: 0x00004EB4
		// Note: this type is marked as 'beforefieldinit'.
		static GUIContent()
		{
		}

		// Token: 0x0400005C RID: 92
		[SerializeField]
		private string m_Text = string.Empty;

		// Token: 0x0400005D RID: 93
		[SerializeField]
		private Texture m_Image;

		// Token: 0x0400005E RID: 94
		[SerializeField]
		private string m_Tooltip = string.Empty;

		// Token: 0x0400005F RID: 95
		private static readonly GUIContent s_Text = new GUIContent();

		// Token: 0x04000060 RID: 96
		private static readonly GUIContent s_Image = new GUIContent();

		// Token: 0x04000061 RID: 97
		private static readonly GUIContent s_TextImage = new GUIContent();

		// Token: 0x04000062 RID: 98
		public static GUIContent none = new GUIContent("");
	}
}
