﻿using System;
using System.ComponentModel;

namespace UnityEngine
{
	// Token: 0x02000004 RID: 4
	public enum EventType
	{
		// Token: 0x04000018 RID: 24
		MouseDown,
		// Token: 0x04000019 RID: 25
		MouseUp,
		// Token: 0x0400001A RID: 26
		MouseMove,
		// Token: 0x0400001B RID: 27
		MouseDrag,
		// Token: 0x0400001C RID: 28
		KeyDown,
		// Token: 0x0400001D RID: 29
		KeyUp,
		// Token: 0x0400001E RID: 30
		ScrollWheel,
		// Token: 0x0400001F RID: 31
		Repaint,
		// Token: 0x04000020 RID: 32
		Layout,
		// Token: 0x04000021 RID: 33
		DragUpdated,
		// Token: 0x04000022 RID: 34
		DragPerform,
		// Token: 0x04000023 RID: 35
		DragExited = 15,
		// Token: 0x04000024 RID: 36
		Ignore = 11,
		// Token: 0x04000025 RID: 37
		Used,
		// Token: 0x04000026 RID: 38
		ValidateCommand,
		// Token: 0x04000027 RID: 39
		ExecuteCommand,
		// Token: 0x04000028 RID: 40
		ContextClick = 16,
		// Token: 0x04000029 RID: 41
		MouseEnterWindow = 20,
		// Token: 0x0400002A RID: 42
		MouseLeaveWindow,
		// Token: 0x0400002B RID: 43
		[Obsolete("Use MouseDown instead (UnityUpgradable) -> MouseDown", true)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		mouseDown = 0,
		// Token: 0x0400002C RID: 44
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Obsolete("Use MouseUp instead (UnityUpgradable) -> MouseUp", true)]
		mouseUp,
		// Token: 0x0400002D RID: 45
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Obsolete("Use MouseMove instead (UnityUpgradable) -> MouseMove", true)]
		mouseMove,
		// Token: 0x0400002E RID: 46
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Obsolete("Use MouseDrag instead (UnityUpgradable) -> MouseDrag", true)]
		mouseDrag,
		// Token: 0x0400002F RID: 47
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Obsolete("Use KeyDown instead (UnityUpgradable) -> KeyDown", true)]
		keyDown,
		// Token: 0x04000030 RID: 48
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Obsolete("Use KeyUp instead (UnityUpgradable) -> KeyUp", true)]
		keyUp,
		// Token: 0x04000031 RID: 49
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Obsolete("Use ScrollWheel instead (UnityUpgradable) -> ScrollWheel", true)]
		scrollWheel,
		// Token: 0x04000032 RID: 50
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Obsolete("Use Repaint instead (UnityUpgradable) -> Repaint", true)]
		repaint,
		// Token: 0x04000033 RID: 51
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Obsolete("Use Layout instead (UnityUpgradable) -> Layout", true)]
		layout,
		// Token: 0x04000034 RID: 52
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Obsolete("Use DragUpdated instead (UnityUpgradable) -> DragUpdated", true)]
		dragUpdated,
		// Token: 0x04000035 RID: 53
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Obsolete("Use DragPerform instead (UnityUpgradable) -> DragPerform", true)]
		dragPerform,
		// Token: 0x04000036 RID: 54
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Obsolete("Use Ignore instead (UnityUpgradable) -> Ignore", true)]
		ignore,
		// Token: 0x04000037 RID: 55
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Obsolete("Use Used instead (UnityUpgradable) -> Used", true)]
		used
	}
}
