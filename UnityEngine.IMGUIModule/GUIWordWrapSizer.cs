﻿using System;

namespace UnityEngine
{
	// Token: 0x0200002E RID: 46
	internal sealed class GUIWordWrapSizer : GUILayoutEntry
	{
		// Token: 0x06000397 RID: 919 RVA: 0x0000BFEC File Offset: 0x0000A1EC
		public GUIWordWrapSizer(GUIStyle style, GUIContent content, GUILayoutOption[] options) : base(0f, 0f, 0f, 0f, style)
		{
			this.m_Content = new GUIContent(content);
			this.ApplyOptions(options);
			this.m_ForcedMinHeight = this.minHeight;
			this.m_ForcedMaxHeight = this.maxHeight;
		}

		// Token: 0x06000398 RID: 920 RVA: 0x0000C040 File Offset: 0x0000A240
		public override void CalcWidth()
		{
			if (this.minWidth == 0f || this.maxWidth == 0f)
			{
				float num;
				float num2;
				base.style.CalcMinMaxWidth(this.m_Content, out num, out num2);
				num = Mathf.Ceil(num);
				num2 = Mathf.Ceil(num2);
				if (this.minWidth == 0f)
				{
					this.minWidth = num;
				}
				if (this.maxWidth == 0f)
				{
					this.maxWidth = num2;
				}
			}
		}

		// Token: 0x06000399 RID: 921 RVA: 0x0000C0C4 File Offset: 0x0000A2C4
		public override void CalcHeight()
		{
			if (this.m_ForcedMinHeight == 0f || this.m_ForcedMaxHeight == 0f)
			{
				float num = base.style.CalcHeight(this.m_Content, this.rect.width);
				if (this.m_ForcedMinHeight == 0f)
				{
					this.minHeight = num;
				}
				else
				{
					this.minHeight = this.m_ForcedMinHeight;
				}
				if (this.m_ForcedMaxHeight == 0f)
				{
					this.maxHeight = num;
				}
				else
				{
					this.maxHeight = this.m_ForcedMaxHeight;
				}
			}
		}

		// Token: 0x040000E4 RID: 228
		private readonly GUIContent m_Content;

		// Token: 0x040000E5 RID: 229
		private readonly float m_ForcedMinHeight;

		// Token: 0x040000E6 RID: 230
		private readonly float m_ForcedMaxHeight;
	}
}
