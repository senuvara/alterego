﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;

namespace UnityEngine
{
	// Token: 0x02000031 RID: 49
	[NativeHeader("Modules/IMGUI/GUIState.h")]
	[VisibleToOtherModules(new string[]
	{
		"UnityEngine.UIElementsModule"
	})]
	internal class ObjectGUIState : IDisposable
	{
		// Token: 0x060003B1 RID: 945 RVA: 0x0000DC55 File Offset: 0x0000BE55
		public ObjectGUIState()
		{
			this.m_Ptr = ObjectGUIState.Internal_Create();
		}

		// Token: 0x060003B2 RID: 946 RVA: 0x0000DC69 File Offset: 0x0000BE69
		public void Dispose()
		{
			this.Destroy();
			GC.SuppressFinalize(this);
		}

		// Token: 0x060003B3 RID: 947 RVA: 0x0000DC78 File Offset: 0x0000BE78
		~ObjectGUIState()
		{
			this.Destroy();
		}

		// Token: 0x060003B4 RID: 948 RVA: 0x0000DCA8 File Offset: 0x0000BEA8
		private void Destroy()
		{
			if (this.m_Ptr != IntPtr.Zero)
			{
				ObjectGUIState.Internal_Destroy(this.m_Ptr);
				this.m_Ptr = IntPtr.Zero;
			}
		}

		// Token: 0x060003B5 RID: 949
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern IntPtr Internal_Create();

		// Token: 0x060003B6 RID: 950
		[NativeMethod(IsThreadSafe = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void Internal_Destroy(IntPtr ptr);

		// Token: 0x04000107 RID: 263
		internal IntPtr m_Ptr;
	}
}
