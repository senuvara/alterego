﻿using System;

namespace System.Numerics
{
	// Token: 0x02000006 RID: 6
	[AttributeUsage(AttributeTargets.Constructor | AttributeTargets.Method | AttributeTargets.Property)]
	internal class JitIntrinsicAttribute : Attribute
	{
		// Token: 0x06000012 RID: 18 RVA: 0x00002100 File Offset: 0x00000300
		public JitIntrinsicAttribute()
		{
		}
	}
}
