﻿using System;

namespace System.Runtime.CompilerServices
{
	// Token: 0x02000005 RID: 5
	internal class FriendAccessAllowedAttribute : Attribute
	{
		// Token: 0x06000011 RID: 17 RVA: 0x00002100 File Offset: 0x00000300
		public FriendAccessAllowedAttribute()
		{
		}
	}
}
